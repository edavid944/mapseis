
% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 61 $    $Date: 2008-10-13 11:39:22 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell
%% Import the data

close all
fname='ms_RateChange/scec-1975-19923-Mc16-LandersWiemer1994b_dec.mat'; %
dayinyear = 365.24219878;
%Landers
hypo_center = [-116.433 34.216  4];
%Big Bear
%hypo_center = [-116-49.6*10/600 34+12*10/600 4];

% Load ZMAP file
[evStruct] = ms_StartApp(fname);

% Note how the GUIs all change in response to change to underlying data
evStruct.setDepthRange('below',30);
evStruct.setMagnitudeRange('above',1.59);
evStruct.setTimeRange('before',datenum('1992-04-18 04:31:00',31));
evStruct.setTimeRange('after',datenum('1983-01-01 00:00:00',31));
allTimes = evStruct.getTimes()./365.24219878;
drawnow
ms_SetRegion(evStruct,'all')
drawnow
thetaVals = linspace(0,2*pi,101)';

Rad = 3:1:25;
PRes = [ ];
ZRes = [ ];

for i_rad = 1:length(Rad)

    evStruct.setSphereRange('sphere',Rad(i_rad),hypo_center);
    selectTimes = evStruct.getTimes()./dayinyear;
    drawnow

    t = 1987:0.1:max(allTimes)-0.5;
    for  i_T = 1:length(t)
        midT = t(i_T);
   
        [mLTA mProb] =   calc_zP_lta2(allTimes,selectTimes,min(allTimes),midT,midT,max(allTimes),300,'poisson');
        PRes(i_rad,i_T)  =  mProb;
        ZRes(i_rad,i_T)  =  mLTA;

    end
end

plot_optRTw

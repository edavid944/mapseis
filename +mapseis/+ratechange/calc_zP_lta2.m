function [mLTA mProb]=calc_zP_lta2(allCat, mCat,T1,T2,T3,T4,NSim,SimType) %#ok<FNDEF>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example: mLTA=calc_zlta(mCat,mCat20,params.fTstart, fT,fTw,nTbin, nN);
%
% This function calculates the rate changes (z-value) of earthquake
% occurrencs between two periods. This function calculates rate changes for
% all the grid nodes together. Input is either a single vector or a whole
% matrix columnswise only with dates (not the whole catalog). Output is the
% z(lta)- and its probability value for each grid point.
%
% Author: van Stiphout, Thomas
% Email: vanstiphout@sed.ethz.ch
% Created: 7. Aug. 2007
% Changed: 14. Aug.2007
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variables:
% mCat          Catalog of tmes (only origin time in decimal years)
% T1, T2, T3, T4 Interval times
%
%
% Output:
% mLTA           Scalar or vector with beta values for each input column
% mProb          Scalar or vector with probability for beta values for each
%                input column. The probability is either calculated based
%                on a synthetic catalog or a real-like catalog.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




nN = length(mCat(:,1));
TimeLength = (T4-T3)+(T2-T1);
Tbin = TimeLength/100;

% calculate histogram for different time periods
vR1=histc(mCat,T1 : Tbin : T2,1);
vR1=vR1(1:end-1,:);

% vR1=histc(mCat1,fTimeStart:fTimeSteps/365:fTimeCut+fTimeWindow);
vR2=histc(mCat,T3 : Tbin : T4,1);
vR2=vR2(1:end-1,:);


% calculate the mean rate for different periods
mean1=mean(vR1);
mean2=mean(vR2);

var1 = var(vR1);
var2 = var(vR2);

if isempty(vR1)
    disp('Warning - Time Period 1 is without any event');
elseif isempty(vR2)
    disp('Warning - Time Period 2 is without any event');
end;

% create synthetic catalogs to extimate significance level
% reset random number generator
rand('state',sum(100*clock));
switch SimType
    case 'data'
        vPos=ceil(rand(nN,NSim)*length(allCat));
        mSyn1=allCat(vPos);
    case 'poisson'
        mSyn1=rand(nN,NSim)*TimeLength+T1;
end
% apply histogram to synthetic catalog
vS1=histc(mSyn1,T1 : Tbin : T2,1);
vS1=vS1(1:end-1,:);
vS2=histc(mSyn1,T3 : Tbin : T4,1);
vS2=vS2(1:end-1,:);
% calculate the mean rate for different periods in synthetic catalog
mSynMean1=mean(vS1);
mSynMean2=mean(vS2);
% calculate variance for different periods in synthetic catalog
mSynVar1 = var(vS1);
mSynVar2 = var(vS2);
% calculate z(lta) values for synthetic catalog
mSynLTA=((mSynMean1-mSynMean2)./sqrt(mSynVar1./size(vS1,1)+mSynVar2./size(vS2,1)))';
% calculate values for normal distribution
% [jbt(i), jbp(i)]=jbtest(mSynLTA);
% llt(i)=lillietest(mSynLTA);
[mu,s] = normfit(mSynLTA);

% z(lta)
mLTA=((mean1-mean2)./sqrt(var1./size(vR1,1)+var2./size(vR2,1)))';
% calculate the probability of z(lta)-values
[mProb] = normcdf(mLTA,mu,s);
% figure;plot(mLTA,mProb,'.');
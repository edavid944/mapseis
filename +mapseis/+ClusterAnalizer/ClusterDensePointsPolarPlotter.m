function handle=ClusterDensePointsPolarPlotter(plotAxis,TimeVal,AzVal,DistVal,ModeSelector,LogSwitch)
	
	%plots the result of a cluster analysis into a density map with
	%Time (x) and distance (y). 
	
	%if AzSwitch is set to true, the azimute (direction) will be used instead
	%of the distance.
	
	%if LogSwitch is used a logarithmic scale for the time is used
	
	import mapseis.plot.*;
	import mapseis.converter.*;
	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end

	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	

	
	%treat distance and azimute together (as polar coordinates)
	CorrectedAngle=deg2rad(90-AzVal);
	
	switch ModeSelector
		case 'Spatial'
			XDir=DistVal.*cos(CorrectedAngle);
			YDir=DistVal.*sin(CorrectedAngle);
			Xlab=' X-Dir (km) ';
			Ylab=' Y-Dir (km) ';
			
		case 'AzTime'
			XDir=TimeVal.*cos(CorrectedAngle);
			YDir=TimeVal.*sin(CorrectedAngle);
			Xlab=' X  ';
			Ylab=' Y  ';
	end	
	
	
	%create plotdata
	PlotData=[XDir,YDir];
	absMaxAll=max([ceil(max(abs(XDir))),ceil(max(abs(YDir)))]);
	xlimiter=[-absMaxAll absMaxAll];
	ylimiter=[-absMaxAll absMaxAll];
		
	PlotConfig	= struct(	'PlotType','DensePoint',...
						'Data',[PlotData],...
						'MarkerStylePreset','normal',...
						'MarkerSize',150,...
						'X_Axis_Label',Xlab,...
						'Y_Axis_Label',Ylab,...
						'CustomColors','jet',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LogMode',true,...
						'LegendText','Time - Distance');
		
	

	
	%plot it all
	[handle legendentry] = PlotDensityPoints2D(plotAxis,PlotConfig);
	%set(gca,'Box','on','DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1])
	set(gca,'Box','on','DataAspectRatio',[1 1 1])
	
	hold on
	L1=plot(xlimiter,[0 0]);
	L2=plot([0 0],ylimiter);
	set([L1,L2],'Color',[0.7 0.7 0.7],'LineWidth',0.5,'LineStyle',':');
	hold off
	
	%here will be some commands to make it look nicer
	set(plotAxis,'LineWidth',2,'FontWeight','bold','FontSize',14);
	
	if LogSwitch
		%set x-axis to logarithmic scale
		%set(plotAxis,'XScale','log');
		
		
		
	end
	
end

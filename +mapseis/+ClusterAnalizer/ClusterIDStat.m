function CStats=ClusterIDStat(ClusterAddData,Datastore,RefFrame)
	%This function looks through the different clusterIDs and determine
	%how often point is "used" in the IDs. Also it creates logical vectors 
	%for all ref. points select the earthquakes which use them
	%It is also a bit of a prototype so new stuff may be added.
	%If UniBins is true the same bining will be used for all histogram of the 
	%same type.
	import mapseis.projector.*;
	import mapseis.util.*;
	
	%some config data, may be editable later
	UniBins=true;
	numBins=25;
	
	%get data from datastore
	Datastore.TypeUsed=[true;true;true];
	Datastore.ShowUnclassified=true;
	selected=true(Datastore.getRowCount,1);
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	%get decluster data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	%unpack from Ref. Frame
	SpaceFrame=RefFrame.SpaceFrame;
	SpaceLabel=RefFrame.SpaceLabel;
	TimeFrame=RefFrame.TimeFrame;
	TimeLabel=RefFrame.TimeLabel;
	
	%unpack additional data
	NearSpacePoint=ClusterAddData.NearSpacePoint;
	DistSpacePoint=ClusterAddData.DistSpacePoint;
	NearTimePoint=ClusterAddData.NearTimePoint;
	DistTimePoint=ClusterAddData.DistTimePoint;
	MagMain=ClusterAddData.MagMain;
	
	
	%to make life easier later
	MainMagHistAllX=[];
	MainMagHistAllY=[];
	AllMagHistAllX=[];
	AllMagHistAllY=[];
	
	MainDepthHistAllX=[];
	MainDepthHistAllY=[];
	AllDepthHistAllX=[];
	AllDepthHistAllY=[];
	
	MainDistHistAllX=[];
	MainDistHistAllY=[];
	AllDistHistAllX=[];
	AllDistHistAllY=[];
	
	TimeHistAllX=[];
	TimeHistAllY=[];
	
	if UniBins
		UsedInCluster=~isnan(MagMain);
		UsedInClusterMain=UsedInCluster&EventType==2;
		
		MainMagBin=linspace(min(MagMain(UsedInCluster)),max(MagMain(UsedInCluster)),numBins);
		MainDepthBin=linspace(min(ShortCat(UsedInClusterMain,3)),max(ShortCat(UsedInClusterMain,3)),numBins);
		MainDistBins=linspace(min(DistSpacePoint(UsedInClusterMain,1)),max(ShortCat(UsedInClusterMain,1)),numBins);
		AllMagBin=linspace(min(ShortCat(UsedInCluster,5)),max(ShortCat(UsedInCluster,5)),numBins);
		AllDepthBin=linspace(min(ShortCat(UsedInCluster,3)),max(ShortCat(UsedInCluster,3)),numBins);
		AllDistBins=MainDistBins; %No better info available without calculation
	else
		UsedInCluster=~isnan(MagMain);
		MainMagBin=numBins;
		MainDepthBin=numBins;
		MainDistBins=numBins;
		AllMagBin=numBins;
		AllDepthBin=numBins;
		AllDistBins=numBins;
	end
	
	%Spatial points first
	for i=1:numel(SpaceLabel)
		FirstNear=strcmp(NearSpacePoint(:,1),SpaceLabel{i});
		SecondNear=strcmp(NearSpacePoint(:,2),SpaceLabel{i});
		ThirdNear=strcmp(NearSpacePoint(:,3),SpaceLabel{i});
		UsedDist=FirstNear|SecondNear|ThirdNear;
		
		NrFirst(i)=sum(FirstNear);
		NrSecond(i)=sum(SecondNear);
		NrThird(i)=sum(ThirdNear);
		NrTotal(i)=sum(UsedDist);
		WeighedTotal(i)=(3*NrFirst(i)+2*NrSecond(i)+1*NrThird(i))/5;
	
		
		%Now some addition stats for now only with the nearest point
		%Mag
		MainMagThis=MagMain(FirstNear&UsedInCluster&EventType==2);
		MagMainStat= [min(MainMagThis),mean(MainMagThis),max(MainMagThis)];% [min mean max]
		[val bins]=hist(MainMagThis,MainMagBin);
		HistMagMain=[val', bins'];
		MainMagHistAllX(:,i)=[bins'];
		MainMagHistAllY(:,i)=[val'];
		
		AllMagThis=ShortCat(FirstNear,5);
		MagAllStat=  [min(AllMagThis),mean(AllMagThis),max(AllMagThis)];% [min mean max]
		[val bins]=hist(AllMagThis,AllMagBin);
		HistMagAll=[val', bins'];
		AllMagHistAllX(:,i)=[bins'];
		AllMagHistAllY(:,i)=[val'];
		
		
		%Depth
		MainDepthThis=ShortCat(FirstNear&EventType==2,3);
		DepthMainStat= [min(MainDepthThis),mean(MainDepthThis),max(MainDepthThis)];% [min mean max]
		[val bins]=hist(MainDepthThis,MainDepthBin);
		HistDepthMain=[val', bins'];
		MainDepthHistAllX(:,i)=[bins'];
		MainDepthHistAllY(:,i)=[val'];
		
		AllDepthThis=ShortCat(FirstNear,3);
		DepthAllStat= [min(AllDepthThis),mean(AllDepthThis),max(AllDepthThis)];% [min mean max]
		[val bins]=hist(AllDepthThis,AllDepthBin);
		HistDepthAll=[val', bins'];
		AllDepthHistAllX(:,i)=[bins'];
		AllDepthHistAllY(:,i)=[val'];
		
		
		%Distances to clusters
		MainDistThis=DistSpacePoint(FirstNear&EventType==2);
		DistMainStat= [min(MainDistThis),mean(MainDistThis),max(MainDistThis)];% [min mean max]
		[val bins]=hist(MainDistThis,MainDistBins);
		HistDistMain=[val', bins'];
		MainDistHistAllX(:,i)=[bins'];
		MainDistHistAllY(:,i)=[val'];
		
		AllDistThis=deg2km(distance(SpaceFrame(i,2),SpaceFrame(i,1),...
				ShortCat(FirstNear,2),ShortCat(FirstNear,1)));
		DistAllStat= [min(AllDistThis),mean(AllDistThis),max(AllDistThis)];% [min mean max]
		[val bins]=hist(AllDistThis,AllDistBins);
		HistDistAll=[val', bins'];
		AllDistHistAllX(:,i)=[bins'];
		AllDistHistAllY(:,i)=[val'];
		
		
		%Nr Time Points for this station (there may be a faster way to do it)
		NrTimePoints=[];
		for j=1:numel(TimeLabel)
			MyTime=strcmp(NearTimePoint(:,1),TimeLabel{j})&FirstNear;
			NrTimePoints(j)=sum(MyTime);
		end
		
		TimePoints= TimeFrame(:,2);
		TimeHistAllY(:,i)=NrTimePoints';
		TimeHistAllX(:,i)=TimePoints';
		
		%makes plotting them easier but is actuall in the frame already
		
		
		CStats.(['RefSpace',SpaceLabel{i}]).FirstNear=FirstNear;
		CStats.(['RefSpace',SpaceLabel{i}]).SecondNear=SecondNear;
		CStats.(['RefSpace',SpaceLabel{i}]).ThirdNear=ThirdNear;
		CStats.(['RefSpace',SpaceLabel{i}]).UsedDist=UsedDist;
		
		MagStat=struct(	'MainMagStat',[MagMainStat],...
				'HistMagMain',[HistMagMain],...
				'MagAllStat',[MagAllStat],...
				'HistMagAll',[HistMagAll]);
		CStats.(['RefSpace',SpaceLabel{i}]).MagStat=MagStat;
		
		DepthStat=struct(	'DepthMainStat',[DepthMainStat],...
					'HistDepthMain',[HistDepthMain],...
					'DepthAllStat',[DepthAllStat],...
					'HistDepthAll',[HistMagAll]);
		CStats.(['RefSpace',SpaceLabel{i}]).DepthStat=DepthStat;
		
		DistStat=struct(	'DistMainStat',[DistMainStat],...
					'HistDistMain',[HistDistMain],...
					'DistAllStat',[DistAllStat],...
					'HistDistAll',[HistDistAll]);
		CStats.(['RefSpace',SpaceLabel{i}]).DistStat=DistStat;
		
		TimeStat=struct(	'DistMainStat',[NrTimePoints],...
					'HistDistMain',[TimePoints]);
		CStats.(['RefSpace',SpaceLabel{i}]).TimeStat=TimeStat;
		
		
	end
	
	%Time points 
	for i=1:numel(TimeLabel)
		UsedTime=strcmp(NearTimePoint(:,1),TimeLabel{i});

		NrTotalTime(i)=sum(UsedTime);
		
		
		CStats.(['RefTime',TimeLabel{i}]).UsedTime=UsedTime;
		
	end
	
	CStats.SpaceNrFirst=NrFirst;
	CStats.SpaceNrSecond=NrSecond;
	CStats.SpaceNrThird=NrThird;
	CStats.SpaceNrTotal=NrTotal;
	CStats.SpaceWeighedTotal=WeighedTotal;
	CStats.TimeNrTotal=NrTotalTime;
	
	
	%Combined stuff
	CombinedHists=struct(	'MainMagHistAllX',[MainMagHistAllX],...
				'MainMagHistAllY',[MainMagHistAllY],...
				'AllMagHistAllX',[AllMagHistAllX],...
				'AllMagHistAllY',[AllMagHistAllY],...				
				'MainDepthHistAllX',[MainDepthHistAllX],...
				'MainDepthHistAllY',[MainDepthHistAllY],...
				'AllDepthHistAllX',[AllDepthHistAllX],...
				'AllDepthHistAllY',[AllDepthHistAllY],...				
				'MainDistHistAllX',[MainDistHistAllX],...
				'MainDistHistAllY',[MainDistHistAllY],...
				'AllDistHistAllX',[AllDistHistAllX],...
				'AllDistHistAllY',[AllDistHistAllY],...				
				'TimeHistAllX',[TimeHistAllX],...
				'TimeHistAllY',[TimeHistAllY]);
	CStats.CombinedHists=CombinedHists
	

end

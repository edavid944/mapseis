function RefPointPlotter(plotAxis,CStats,ClusterAddData,Datastore,RefFrame,WhichField,NumMode,PlotMode)
	%Function to plot the position of the reference points together with a
	%label and if wanted with some additional stat.
	
	%WhichField: cell with 	{1}='Mag','Depth','Dist'
	%			{2}='min','max','mean'
	%			{3}= true for all eq, false for only mainshocks 
	
	import mapseis.projector.*;
	import mapseis.util.*;
	
	if nargin<7
		PlotMode='pure';
		WhichField=[];
	end
	
	if isempty(plotAxis)
		plotAxis=gca;
	end
	
	TextShift=0.001;
	ColorResolution=256;
	ColorScale='jet';
	minColor=0.1;
	maxDense='unlimited';
	
	
	%get data from datastore
	Datastore.TypeUsed=[true;true;true];
	Datastore.ShowUnclassified=true;
	selected=true(Datastore.getRowCount,1);
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	%unpack from Ref. Frame
	SpaceFrame=RefFrame.SpaceFrame;
	SpaceLabel=RefFrame.SpaceLabel;
	TimeFrame=RefFrame.TimeFrame;
	TimeLabel=RefFrame.TimeLabel;
	
	%unpack additional data
	NearSpacePoint=ClusterAddData.NearSpacePoint;
	DistSpacePoint=ClusterAddData.DistSpacePoint;
	NearTimePoint=ClusterAddData.NearTimePoint;
	DistTimePoint=ClusterAddData.DistTimePoint;
	MagMain=ClusterAddData.MagMain;
	
	
	%get decluster data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	
	%generate additional data
	SpaceNrFirst=CStats.SpaceNrFirst;
	TotalEq=sum(SpaceNrFirst);
	MaxNr=max(SpaceNrFirst);
	SizeVector=SpaceNrFirst/TotalEq*(2500);
	
	if NumMode
		for i=1:numel(SpaceLabel)
			SpaceLabelPrint{i}=['_{',num2str(i),'}'];
		
		end
	else
		SpaceLabelPrint=SpaceLabel;
	end
	
	
	
	switch PlotMode
		case 'pure'
			
			hold on;
			
			
			for i=1:numel(SpaceLabel)
				
				TheColor=[rand,rand,rand];
				
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
				
				handMain=plot(LonRef,LatRef);
				set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',14);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			hold off
			
			
		case 'NrEvents'
			theScatman=scatter(SpaceFrame(:,1),SpaceFrame(:,2),SizeVector,'filled');	
			
			
			%Labels
			hold on;
			for i=1:numel(SpaceLabel)
				
				%TheColor=[rand,rand,rand];
				
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
			
				
				%handMain=plot(LonRef,LatRef);
				%set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',14);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			hold off
			
			
		case 'ColorIDMain'
			hold on;
			for i=1:numel(SpaceLabel)
				MrFirst=CStats.(['RefSpace',SpaceLabel{i}]).FirstNear;
				TheColor=[rand,rand,rand];
				
				%prepare plot data
				LonAfterShock=ShortCat(EventType==3&MrFirst,1);
				LatAfterShock=ShortCat(EventType==3&MrFirst,2);
				
				LonMainShock=ShortCat(EventType==2&MrFirst,1);
				LatMainShock=ShortCat(EventType==2&MrFirst,2);
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
				if any(MrFirst)
					
					%handAfter=plot(LonAfterShock,LatAfterShock);
					%set(handAfter,'LineStyle','none','Color',TheColor,'Marker','.');
					
					
					handMain=plot(LonMainShock,LatMainShock);
					set(handMain,'LineStyle','none','Color',TheColor,'Marker','x','MarkerSize',8);
					
				end
				
				handMain=plot(LonRef,LatRef);
				set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',10);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			hold off
		
		
		case 'ColorID'
			hold on
			for i=1:numel(SpaceLabel)
				MrFirst=CStats.(['RefSpace',SpaceLabel{i}]).FirstNear;
				TheColor=[rand,rand,rand];
				
				%prepare plot data
				LonAfterShock=ShortCat(EventType==3&MrFirst,1);
				LatAfterShock=ShortCat(EventType==3&MrFirst,2);
				
				LonMainShock=ShortCat(EventType==2&MrFirst,1);
				LatMainShock=ShortCat(EventType==2&MrFirst,2);
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
				if any(MrFirst)
					
					handAfter=plot(LonAfterShock,LatAfterShock);
					set(handAfter,'LineStyle','none','Color',TheColor,'Marker','.');
					
					
					handMain=plot(LonMainShock,LatMainShock);
					set(handMain,'LineStyle','none','Color',TheColor,'Marker','x','MarkerSize',8);
					
				end
				
				handMain=plot(LonRef,LatRef);
				set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',10);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			hold off
	
		case 'AddVal'
		
			if ~isempty(WhichField)
				switch WhichField{2}
					case 'min'
						StatSel=1;
					case 'max'
						StatSel=3;
					case 'mean'
						StatSel=2;
				end
				
				switch WhichField{1}
					case 'Mag'
						Fielder='MagStat'
						
						if WhichField{3}
							Fielder2='MagAllStat';
						else
							Fielder2='MainMagStat';
						end
						
					case 'Depth'
						Fielder='DepthStat';
						
						if WhichField{3}
							Fielder2='DepthAllStat';
						else
							Fielder2='DepthMainStat';
						end
						
					case 'Dist'
						Fielder='DistStat';
						
						if WhichField{3}
							Fielder2='DistAllStat';
						else
							Fielder2='DistMainStat';
						end
						
						
				end
			
			else
				error('Please specify additional Data Field');
			
			end
		
			
			%generate color data
			for i=1:numel(SpaceLabel)
				RawStatVal(i)=CStats.(['RefSpace',SpaceLabel{i}]).(Fielder).(Fielder2)(StatSel);
			end
			
			%colFun=str2func(ColorScale);
			%TheColors=colFun(ColorResolution);
			
			
			
			theScatman=scatter(SpaceFrame(:,1),SpaceFrame(:,2),SizeVector,RawStatVal,'filled');	
			
			
			%Labels
			hold on;
			for i=1:numel(SpaceLabel)
				
				%TheColor=[rand,rand,rand];
				
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
			
				
				%handMain=plot(LonRef,LatRef);
				%set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',14);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			hold off
	
			
	case 'AddValSeism'
		
			PlotAll=false;
			if ~isempty(WhichField)
				switch WhichField{2}
					case 'min'
						StatSel=1;
					case 'max'
						StatSel=3;
					case 'mean'
						StatSel=2;
				end
				
				switch WhichField{1}
					case 'Mag'
						Fielder='MagStat'
						
						if WhichField{3}
							Fielder2='MagAllStat';
							PlotAll=true;
						else
							Fielder2='MainMagStat';
						end
						
					case 'Depth'
						Fielder='DepthStat';
						
						if WhichField{3}
							Fielder2='DepthAllStat';
							PlotAll=true;
						else
							Fielder2='DepthMainStat';
						end
						
					case 'Dist'
						Fielder='DistStat';
						
						if WhichField{3}
							Fielder2='DistAllStat';
							PlotAll=true;
							
						else
							Fielder2='DistMainStat';
						end
						
						
				end
			
			else
				error('Please specify additional Data Field');
			
			end
		
			
			%generate color data
			for i=1:numel(SpaceLabel)
				RawStatVal(i)=CStats.(['RefSpace',SpaceLabel{i}]).(Fielder).(Fielder2)(StatSel);
			end
			
			%Color determination
			normfact=abs(max(RawStatVal)-min(RawStatVal));
			NormedStatVal=(RawStatVal-min(RawStatVal))./normfact;
			%disp(NormedStatVal)
			colFun=str2func(ColorScale);
			TheColors=colFun(ColorResolution);
			
			selNum=round(NormedStatVal*ColorResolution);
			selNum(selNum==0)=1;
			selNum(selNum>ColorResolution)=ColorResolution;	
			
			minIndex=round(ColorResolution*minColor);
			if minIndex<=0;
				minIndex=1;
			end
			
			selNum(selNum<minIndex)=minIndex;
			SelectedColor=TheColors(selNum,:);
			
			
			%theScatman=scatter(SpaceFrame(:,1),SpaceFrame(:,2),SizeVector,RawStatVal,'filled');	
			
			
			%Labels
			hold on;
			for i=1:numel(SpaceLabel)
				
				%TheColor=[rand,rand,rand];
				TheColor=SelectedColor(i,:);
				MrFirst=CStats.(['RefSpace',SpaceLabel{i}]).FirstNear;
				
				%prepare plot data
				LonAfterShock=ShortCat(EventType==3&MrFirst,1);
				LatAfterShock=ShortCat(EventType==3&MrFirst,2);
				
				LonMainShock=ShortCat(EventType==2&MrFirst,1);
				LatMainShock=ShortCat(EventType==2&MrFirst,2);
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
				if any(MrFirst)
					if PlotAll
						handAfter=plot(LonAfterShock,LatAfterShock);
						set(handAfter,'LineStyle','none','Color',TheColor,'Marker','.');
					end
					
					handMain=plot(LonMainShock,LatMainShock);
					set(handMain,'LineStyle','none','Color',TheColor,'Marker','x','MarkerSize',8);
					
				end
				
				handMain=plot(LonRef,LatRef);
				set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',10);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			hold off
		
		case 'AddValTwin'
		
			PlotAll=false;
			if ~isempty(WhichField)
				switch WhichField{2}
					case 'min'
						StatSel=1;
					case 'max'
						StatSel=3;
					case 'mean'
						StatSel=2;
				end
				
				switch WhichField{1}
					case 'Mag'
						Fielder='MagStat'
						
						if WhichField{3}
							Fielder2='MagAllStat';
							PlotAll=true;
						else
							Fielder2='MainMagStat';
						end
						
					case 'Depth'
						Fielder='DepthStat';
						
						if WhichField{3}
							Fielder2='DepthAllStat';
							PlotAll=true;
						else
							Fielder2='DepthMainStat';
						end
						
					case 'Dist'
						Fielder='DistStat';
						
						if WhichField{3}
							Fielder2='DistAllStat';
							PlotAll=true;
							
						else
							Fielder2='DistMainStat';
						end
						
						
				end
			
			else
				error('Please specify additional Data Field');
			
			end
		
			
			%generate color data
			for i=1:numel(SpaceLabel)
				RawStatVal(i)=CStats.(['RefSpace',SpaceLabel{i}]).(Fielder).(Fielder2)(StatSel);
			end
			
			%Color determination
			normfact=abs(max(RawStatVal)-min(RawStatVal));
			NormedStatVal=(RawStatVal-min(RawStatVal))./normfact;
			%disp(NormedStatVal)
			colFun=str2func(ColorScale);
			%TheColors=colFun(ColorResolution);
			%TheColors=flipud(gray(ColorResolution));
			TheColors=(gray(ColorResolution));
			
			minColor=0.125;
			RedReso=round(ColorResolution*1);
			selNum=round(NormedStatVal*RedReso);
			selNum(selNum==0)=1;
			selNum(selNum>RedReso)=RedReso;	
			
			minIndex=round(RedReso*minColor);
			if minIndex<=0;
				minIndex=1;
			end
			
			selNum(selNum<minIndex)=minIndex;
			SelectedColor=TheColors(selNum,:);
			
			
			%theScatman=scatter(SpaceFrame(:,1),SpaceFrame(:,2),SizeVector,RawStatVal,'filled');	
			
			
			%Labels
			hold on;
			for i=1:numel(SpaceLabel)
				
				%TheColor=[rand,rand,rand];
				TheColor=SelectedColor(i,:);
				MrFirst=CStats.(['RefSpace',SpaceLabel{i}]).FirstNear;
				
				%prepare plot data
				LonAfterShock=ShortCat(EventType==3&MrFirst,1);
				LatAfterShock=ShortCat(EventType==3&MrFirst,2);
				
				LonMainShock=ShortCat(EventType==2&MrFirst,1);
				LatMainShock=ShortCat(EventType==2&MrFirst,2);
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
				if any(MrFirst)
					if PlotAll
						handAfter=plot(LonAfterShock,LatAfterShock);
						set(handAfter,'LineStyle','none','Color',TheColor,'Marker','.');
					end
					
					handMain=plot(LonMainShock,LatMainShock);
					set(handMain,'LineStyle','none','Color',TheColor,'Marker','x','MarkerSize',8);
					
				end
				
				%handMain=plot(LonRef,LatRef);
				%set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',10);
				%myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabel{i});
				%set(myText,'FontSize',14,'FontWeight','bold');
				
					
			end
			
			theScatman=scatter(SpaceFrame(:,1),SpaceFrame(:,2),SizeVector,RawStatVal,'filled');	
			
			for i=1:numel(SpaceLabel)
				
				%TheColor=[rand,rand,rand];
				
				
				LonRef=SpaceFrame(i,1);
				LatRef=SpaceFrame(i,2);
				
			
				
				%handMain=plot(LonRef,LatRef);
				%set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',14);
				myText=text(LonRef+TextShift,LatRef+TextShift,SpaceLabelPrint{i});
				set(myText,'FontSize',14,'FontWeight','bold','Color','r');
				
					
			end
			
			hold off	
			set(plotAxis,'Color','k')
			
	end
	

end

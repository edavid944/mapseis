function AllHistory = PlotEmAll(OutStruct,SingleFolder,FolderSufix,WhichClusters,NumOfBin,FilePrefix,FolderPath)
	%The "I got bored script" it just calls all the the other mass plotter and
	%does everything needed.
	%FolderPath can be a cell array with up to 3 paths (one for normal one for
	%the histograms and one for the storage of the histograms).
	%WhichClusters can be a array with the wanted clusters, then only those
	%will be processed.
	
	import mapseis.ClusterAnalizer.*;
	
	
	if iscell(FolderPath)
		NormalPath=FolderPath{1};
		HistPath=FolderPath{2};
		SavePath=FolderPath{3};
	else
		NormalPath=FolderPath;
		HistPath=FolderPath;
		SavePath=FolderPath;
	end
	
	if ~isempty(WhichClusters)
		%correct clusterlist in case there are NaNs and doublets
		WhichClusters=WhichClusters(~isnan(WhichClusters));
		WhichClusters=unique(WhichClusters);
		FoundThem=false(numel(OutStruct.AnalysedClusters),1);	
		
		for i=1:numel(WhichClusters)
			FoundThem=FoundThem|OutStruct.AnalysedClusters==WhichClusters(i);
			disp(WhichClusters(i))
		end
		
		if sum(FoundThem)==0
			error('None of the clusters could be found')
		end
		
		ClusterData.SingleTimes=OutStruct.SingleTimes(FoundThem);
		ClusterData.SingleDistances=OutStruct.SingleDistances(FoundThem);
		ClusterData.SingleAzimutes=OutStruct.SingleAzimutes(FoundThem);
		ClusterData.MainShocks=OutStruct.MainShocks(FoundThem);
		ClusterData.Aftershocks=OutStruct.Aftershocks(FoundThem);
		ClusterData.AnalysedClusters=OutStruct.AnalysedClusters(FoundThem);
		
		if ~SingleFolder
			%The Summaries cannot be corrected so just add them
			ClusterData.AllTimes=OutStruct.AllTimes;
			ClusterData.AllDistances=OutStruct.AllDistances;
			ClusterData.AllAzimutes=OutStruct.AllAzimutes;
		end
		
	else
		ClusterData=OutStruct;
	end
	
	
	if SingleFolder
		%in this mode all events are printed into a single folder
		for i=1:numel(ClusterData.AnalysedClusters)
			%create a struct with only a single cluster
			TheOne=ClusterData.AnalysedClusters==ClusterData.AnalysedClusters(i);
			
			if sum(TheOne)==1
				if isempty(FolderSufix)
					EndFolder=num2str(ClusterData.AnalysedClusters(i));
				else
					EndFolder=FolderSufix{i};
				end
				
				NewFold=['SingleCluster_Clu_',EndFolder];
				
				%create the folders if needed;
				if exist([NormalPath,filesep,NewFold])~=7
					mkdir(NormalPath,NewFold);
				end
				
				if exist([HistPath,filesep,NewFold])~=7
					mkdir(HistPath,NewFold);
				end
				
				
				%build struct
				SingleData=[];
				SingleData.SingleTimes=ClusterData.SingleTimes(TheOne);
				SingleData.SingleDistances=ClusterData.SingleDistances(TheOne);
				SingleData.SingleAzimutes=ClusterData.SingleAzimutes(TheOne);
				SingleData.MainShocks=ClusterData.MainShocks(TheOne);
				SingleData.Aftershocks=ClusterData.Aftershocks(TheOne);
				SingleData.AnalysedClusters=ClusterData.AnalysedClusters(TheOne);
				
				%and do the work
				MassClusterPlotterPoints(SingleData,false,[FilePrefix,'_norm'],[NormalPath,filesep,NewFold]);
				MassClusterPlotterPoints(SingleData,true,[FilePrefix,'_log'],[NormalPath,filesep,NewFold]);
				MassClusterPlotterPointsPolar(SingleData,false,[FilePrefix,'_polar'],[NormalPath,filesep,NewFold]);
				SingleData = KillEmptyCluster(SingleData);
				SingleData=Add_3D_distance(SingleData);
				AllHistory = MassClusterBinManDepth(SingleData,NumOfBin,[FilePrefix,'_hist'],[HistPath,filesep,NewFold]);
				AllHistory = BinAreaCorrection(AllHistory,true,[FilePrefix,'_histCor'],[HistPath,filesep,NewFold]);
				save([SavePath,filesep,FilePrefix,'_HistogramData_Clu_',EndFolder,'.mat'],'AllHistory');
				
			end
			
		
		
		end
		
		%Now do all the stuff which can only be done together
		if exist([NormalPath,filesep,'Leftovers'])~=7
				mkdir(NormalPath,'Leftovers');
		end
		
		MagStruct = MainShockRelationTest(ClusterData,false,true,[FilePrefix,'_MagTest'],[NormalPath,filesep,'Leftovers']);
		
		
	else
		%normal easy version
		MassClusterPlotterPoints(ClusterData,false,[FilePrefix,'_norm'],NormalPath);
		MassClusterPlotterPoints(ClusterData,true,[FilePrefix,'_log'],NormalPath);
		MassClusterPlotterPointsPolar(ClusterData,false,[FilePrefix,'_polar'],NormalPath);
		ClusterData = KillEmptyCluster(ClusterData);
		ClusterData=Add_3D_distance(ClusterData);
		AllHistory = MassClusterBinManDepth(ClusterData,NumOfBin,[FilePrefix,'_hist'],HistPath);
		AllHistory = BinAreaCorrection(AllHistory,true,[FilePrefix,'_histCor'],HistPath);
		save([SavePath,filesep,'_HistogramData.mat'],'AllHistory');
		
		MagStruct = MainShockRelationTest(ClusterData,false,true,[FilePrefix,'_MagTest'],NormalPath);
		
		
		
	end

end

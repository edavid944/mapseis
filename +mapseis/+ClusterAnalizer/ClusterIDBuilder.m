function [UniqueID AdditionalData] = ClusterIDBuilder(Datastore,ReferenceFrame,IDConfig)
	%creates unique ids for cluster based on a reference frame, their mainshock time
	%location and magnitude.
	
	%Options contain for now: 
	%	NrSpaceRef:	Number of spatial reference points used (default=3)
	%	NrTimeRef:	Number of time reference points used (default=1);
	
	%The ID will be as following
	%ID = 'CID:1X9999:2X9999_3X9999:1Y+99999:M99:R99'
	%	CID:		Prefix containing string 'CID' for Cluster Index, it is 
	%			the same for every cluster
	%	1X,2X,3X:	1st,2nd,3rd nearest Reference point described by the 
	%			point's letters 
	%	9999:		distance to the reference point in km rounded to integer
	%			
	%	1Y:		The label of the nearest time point in case of combined mode 
	%			this will be empty __
	%	+99999:		distance to decyear point in days rounded to one decimal,
	%			so last digit is .0. In case of the combined Mode this is 
	%			replace by a random Hex code to prevent equal codes
	%	M99:		The letter M followed by the mainshock magnitude, rounded to 
	%			one decimal (e.g. 4.5 ==> M45)
	%	R99:		An 'R' followed by a random number from 00 to 99, this is to 
	%			prevent duplication of IDs;
	
	
	import mapseis.projector.*;
	import mapseis.util.*;
	
	
	if isempty(IDConfig)
		IDConfig.NrSpaceRef=3;
		IDConfig.NrTimeRef=1;
	end
	
	if isempty(Datastore.ClusterNR)
		error('No declustering present in the catalog');
	end

	if isnan(all(Datastore.ClusterNR))
		error('No declustering present in the catalog');
	end
	
	
	Hexxer='0123456789ABCDEF';
	
	%unpack from Ref. Frame
	SpaceFrame=ReferenceFrame.SpaceFrame;
	SpaceLabel=ReferenceFrame.SpaceLabel;
	TimeFrame=ReferenceFrame.TimeFrame;
	TimeLabel=ReferenceFrame.TimeLabel;
	
	
	%get data from datastore
	Datastore.TypeUsed=[true;true;true];
	Datastore.ShowUnclassified=true;
	selected=true(Datastore.getRowCount,1);
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	%get decluster data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	%get unique cluster numbers within the filterlist
	ClusterToCheck=unique(ClusterNR(~isnan(ClusterNR)));
	
	%variables to fill
	AlreadyUsedIDs={};
	UniqueID=cell(size(ClusterNR));
	NearSpacePoint=cell(length(ClusterNR),IDConfig.NrSpaceRef);
	DistSpacePoint=nan(length(ClusterNR),IDConfig.NrSpaceRef);
	NearTimePoint=cell(length(ClusterNR),IDConfig.NrTimeRef);
	DistTimePoint=nan(length(ClusterNR),IDConfig.NrTimeRef);
	MagMain=nan(size(ClusterNR));
	
	if ~ReferenceFrame.CombinedMode
		for i=1:numel(ClusterToCheck)
			%find all events of the clusters
			InTheClub=ClusterNR==ClusterToCheck(i);
			
			%get mainshock
			MrMain=InTheClub&EventType==2;
			
			%get aftershocks
			Followers=InTheClub&EventType==3;
			
			if sum(MrMain)>1
				disp('More than one mainshock... Corrected')
				tocheck=find(MrMain);
				MrMain=false(size(MrMain));
				MrMain(tocheck(1))=true;
				Followers(tocheck(2:end))=true;
			end
			
			%get distances to reference points
			HowNear=deg2km(distance(ShortCat(MrMain,2),ShortCat(MrMain,1),...
					SpaceFrame(:,2),SpaceFrame(:,1)));
			[ValSpace IDXSpace]=sort(HowNear);
			ThisDist=ValSpace(1:IDConfig.NrSpaceRef);
			ThisLabelDist=SpaceLabel(IDXSpace(1:IDConfig.NrSpaceRef));
			
			%get nearest time points
			HowRecent=TimeFrame(:,1)-ShortCat(MrMain,4);
			[ValTime IDXTime]=sort(abs(HowRecent));
			ThisTime=ValTime(1:IDConfig.NrTimeRef);
			ThisLabelTime=TimeLabel(IDXTime(1:IDConfig.NrTimeRef));
			
			%get mag
			ThisMag=round(ShortCat(MrMain,5)*10)/10;
			
			%now create the ID
			MyID='CIS';
			
			for j=1:IDConfig.NrSpaceRef
				TheVal=round(ThisDist(j));
				StrVal=[':',ThisLabelDist{j},num2str(TheVal,'%i')];
				MyID=[MyID,StrVal];
				
				NearSpacePoint(MrMain|Followers,j)=ThisLabelDist(j);
				DistSpacePoint(MrMain|Followers,j)=ThisDist(j);
			end
			
			for j=1:IDConfig.NrTimeRef
				TheVal=round(ThisTime(j)*10);
				StrVal=[':',ThisLabelDist{j},num2str(TheVal,'%+i')];
				MyID=[MyID,StrVal];
				
				NearTimePoint(MrMain|Followers,j)=ThisLabelTime(j);
				DistTimePoint(MrMain|Followers,j)=ThisTime(j);
			end
			
			MyID=[MyID,'M',num2str(ThisMag*10,'%02i')];
			
			MyID=[MyID,'R',num2str(randi(99),'%02i')];
			
			%fill data in
			UniqueID(MrMain|Followers)={MyID};
			%NearSpacePoint(MrMain|Followers,1:IDConfig.NrSpaceRef)=ThisLabelDist;
			%DistSpacePoint(MrMain|Followers,1:IDConfig.NrSpaceRef)=ThisDist(1:IDConfig.NrSpaceRef)
			%NearTimePoint(MrMain|Followers,1:IDConfig.NrTimeRef)=ThisLabelTime;
			%DistTimePoint(MrMain|Followers,1:IDConfig.NrTimeRef)=ThisTime(1:IDConfig.NrTimeRef);
			MagMain(MrMain|Followers)=ThisMag;
		end
		
	else
		%not available right now
	end
	
	%and pack it
	AdditionalData.NearSpacePoint=NearSpacePoint;
	AdditionalData.DistSpacePoint=DistSpacePoint;
	AdditionalData.NearTimePoint=NearTimePoint;
	AdditionalData.DistTimePoint=DistTimePoint;
	AdditionalData.MagMain=MagMain;
	
	
end

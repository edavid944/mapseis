function MassClusterPlotterPointsDepth(ResultStruct,LogSwitch,FilePrefix,FolderPath)
	%Plots every cluster in Density maps and saves them. Only cluster with 
	%more than 9 aftershocks are plotted
	
	import mapseis.ClusterAnalizer.*;
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	
	
	%save the old path
	oldFold=pwd;
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeVal=ResultStruct.SingleTimes{i};
		AzVal=ResultStruct.SingleAzimutes{i};
		DistVal=ResultStruct.Single3DDist{i};

		
		
		%check if it has to be printed
		if numel(TimeVal)>=10
			fig=figure;
			set(fig,'pos',[50 50 1200 800]);
			
			%Distance
			plotAxis=subplot(2,1,1)
			ClusterDensePointsPlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,false);
			xlabel('Distance 3D (km)  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%Azimute
			plotAxis=subplot(2,1,2)
			ClusterDensePointsPlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,true);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_ClusterNR_Dense3D_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			%same with points
			fig=figure;
			set(fig,'pos',[50 50 1200 800]);
			
			%Distance
			plotAxis=subplot(2,1,1)
			p1=plot(plotAxis,TimeVal,DistVal,'r');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			if LogSwitch
				set(plotAxis,'XScale','log');
			end
			
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			
			%Azimute
			plotAxis=subplot(2,1,2)
			p2=plot(plotAxis,TimeVal,AzVal,'b');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			set(tit,'FontSize',24,'FontWeight','bold');
			
			set([p1,p2],'LineStyle','none','Marker','.','MarkerSize',6);
			
			if LogSwitch
				set(plotAxis,'XScale','log');
			end
			
			%save it
			%create filename
			filename=[FilePrefix,'_ClusterNR_Point',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
		end
	end
	
	
	%and the full plot
	fig=figure;
	set(fig,'pos',[50 50 1200 800]);
	
	%get data
	TimeVal=ResultStruct.AllTimes;
	AzVal=ResultStruct.AllAzimutes;
	DistVal=ResultStruct.All3DDist;
	
	
	
	
	%Distance
	plotAxis=subplot(2,1,1)
	ClusterDensePointsPlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,false);
	xlabel('Distance 3D (km)  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
			
	%Writing title over top subplot
	tit=title('All Clusters  ');
			
	%Azimute
	plotAxis=subplot(2,1,2)
	ClusterDensePointsPlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,true);
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	set(tit,'FontSize',24,'FontWeight','bold');
	
			
	%save it
	%create filename
	filename=[FilePrefix,'_AllClusters_Dense3D.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
			
	close(fig);
	
	
	%same with points
	fig=figure;
	set(fig,'pos',[50 50 1200 800]);
			
	%Distance
	plotAxis=subplot(2,1,1)
	p1=plot(plotAxis,TimeVal,DistVal,'r');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	if LogSwitch
		set(plotAxis,'XScale','log');
	end
	
	
	%Writing title over top subplot
	tit=title('All Clusters  ');
	
	%Azimute
	plotAxis=subplot(2,1,2)
	p2=plot(plotAxis,TimeVal,AzVal,'b');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	set(tit,'FontSize',24,'FontWeight','bold');
			
	set([p1,p2],'LineStyle','none','Marker','.','MarkerSize',6);
	
	if LogSwitch
		set(plotAxis,'XScale','log');
	end
	
	%save it
	%create filename
	filename=[FilePrefix,'_AllClusters_Point.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
			
	close(fig);
	
end

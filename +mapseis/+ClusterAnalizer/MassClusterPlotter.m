function MassClusterPlotter(ResultStruct,LogSwitch,FilePrefix,FolderPath)
	%Plots every cluster in Density maps and saves them. Only cluster with 
	%more than 9 aftershocks are plotted
	
	import mapseis.ClusterAnalizer.*;
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	
	
	%save the old path
	oldFold=pwd;
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeVal=ResultStruct.SingleTimes{i};
		AzVal=ResultStruct.SingleAzimutes{i};
		DistVal=ResultStruct.SingleDistances{i};
		
		%create the spacings
		if ~LogSwitch
			SpacerT=(max(TimeVal)-min(TimeVal))/25;
		else
			MinTime=min(TimeVal);
			MaxTime=max(TimeVal);
			
			if MinTime<0
				MinTime=-log10(-MinTime);
				
			elseif MinTime>0
				MinTime=log10(MinTime)
			end
			
			if MaxTime<0
				MaxTime=-log10(-MaxTime);
				
			elseif MaxTime>0
				MaxTime=log10(MaxTime);
			end
			SpacerT=(MaxTime-MinTime)/25;
			
		end
		
		SpacerD=(max(DistVal)-min(DistVal))/25;
		SpacerA=5;
		
		
		%check if it has to be printed
		if numel(TimeVal)>=10
			fig=figure;
			set(fig,'pos',[50 50 1200 800]);
			
			%Distance
			plotAxis=subplot(2,1,1)
			ClusterDensePlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,[SpacerT SpacerD],false);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%Azimute
			plotAxis=subplot(2,1,2)
			ClusterDensePlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,[SpacerT SpacerA],true);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_ClusterNR_Dense_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			%same with points
			fig=figure;
			set(fig,'pos',[50 50 1200 800]);
			
			%Distance
			plotAxis=subplot(2,1,1)
			p1=plot(plotAxis,TimeVal,DistVal,'r');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			if LogSwitch
				set(plotAxis,'XScale','log');
			end
			
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			
			%Azimute
			plotAxis=subplot(2,1,2)
			p2=plot(plotAxis,TimeVal,AzVal,'b');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			set(tit,'FontSize',24,'FontWeight','bold');
			
			set([p1,p2],'LineStyle','none','Marker','.','MarkerSize',6);
			
			if LogSwitch
				set(plotAxis,'XScale','log');
			end
			
			%save it
			%create filename
			filename=[FilePrefix,'_ClusterNR_Point',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
		end
	end
	
	
	%and the full plot
	fig=figure;
	set(fig,'pos',[50 50 1200 800]);
	
	%get data
	TimeVal=ResultStruct.AllTimes;
	AzVal=ResultStruct.AllAzimutes;
	DistVal=ResultStruct.AllDistances;
	
	%create the spacings
	if ~LogSwitch
		SpacerT=(max(TimeVal)-min(TimeVal))/50;
	else
		MinTime=min(TimeVal);
		MaxTime=max(TimeVal);
			
		if MinTime<0
			MinTime=-log10(-MinTime);
				
		elseif MinTime>0
			MinTime=log10(MinTime)
		end
			
		if MaxTime<0
			MaxTime=-log10(-MaxTime);
				
		elseif MaxTime>0
			MaxTime=log10(MaxTime);
		end
		SpacerT=(MaxTime-MinTime)/50;
	end
	
	SpacerD=(max(DistVal)-min(DistVal))/50;
	SpacerA=5;
	
	
	%Distance
	plotAxis=subplot(2,1,1)
	ClusterDensePlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,[SpacerT SpacerD],false);
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
			
	%Writing title over top subplot
	tit=title('All Clusters  ');
			
	%Azimute
	plotAxis=subplot(2,1,2)
	ClusterDensePlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,[SpacerT SpacerA],true);
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	set(tit,'FontSize',24,'FontWeight','bold');
	
			
	%save it
	%create filename
	filename=[FilePrefix,'_AllClusters_Dense.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
			
	close(fig);
	
	
	%same with points
	fig=figure;
	set(fig,'pos',[50 50 1200 800]);
			
	%Distance
	plotAxis=subplot(2,1,1)
	p1=plot(plotAxis,TimeVal,DistVal,'r');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	if LogSwitch
		set(plotAxis,'XScale','log');
	end
	
	
	%Writing title over top subplot
	tit=title('All Clusters  ');
	
	%Azimute
	plotAxis=subplot(2,1,2)
	p2=plot(plotAxis,TimeVal,AzVal,'b');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	set(tit,'FontSize',24,'FontWeight','bold');
			
	set([p1,p2],'LineStyle','none','Marker','.','MarkerSize',6);
	
	if LogSwitch
		set(plotAxis,'XScale','log');
	end
	
	%save it
	%create filename
	filename=[FilePrefix,'_AllClusters_Point.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
			
	close(fig);
	
end

function plotColorID(CStats,ClusterAddData,Datastore,RefFrame,OnlyMain)

	import mapseis.projector.*;
	import mapseis.util.*;
	
	%get data from datastore
	Datastore.TypeUsed=[true;true;true];
	Datastore.ShowUnclassified=true;
	selected=true(Datastore.getRowCount,1);
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	%unpack from Ref. Frame
	SpaceFrame=RefFrame.SpaceFrame;
	SpaceLabel=RefFrame.SpaceLabel;
	TimeFrame=RefFrame.TimeFrame;
	TimeLabel=RefFrame.TimeLabel;
	
	%unpack additional data
	NearSpacePoint=ClusterAddData.NearSpacePoint;
	DistSpacePoint=ClusterAddData.DistSpacePoint;
	NearTimePoint=ClusterAddData.NearTimePoint;
	DistTimePoint=ClusterAddData.DistTimePoint;
	MagMain=ClusterAddData.MagMain;
	
	
	%get decluster data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	figure
	plotAxis=gca;
	hold on
	
	%Spatial points first
	for i=1:numel(SpaceLabel)
		MrFirst=CStats.(['RefSpace',SpaceLabel{i}]).FirstNear;
		TheColor=[rand,rand,rand];
		
		%prepare plot data
		LonAfterShock=ShortCat(EventType==3&MrFirst,1);
		LatAfterShock=ShortCat(EventType==3&MrFirst,2);
		
		LonMainShock=ShortCat(EventType==2&MrFirst,1);
		LatMainShock=ShortCat(EventType==2&MrFirst,2);
		
		LonRef=SpaceFrame(i,1);
		LatRef=SpaceFrame(i,2);
		
		if any(MrFirst)
			if ~OnlyMain
				handAfter=plot(LonAfterShock,LatAfterShock);
				set(handAfter,'LineStyle','none','Color',TheColor,'Marker','.');
			end
			
			handMain=plot(LonMainShock,LatMainShock);
			set(handMain,'LineStyle','none','Color',TheColor,'Marker','x','MarkerSize',8);
			
		end
		
		handMain=plot(LonRef,LatRef);
		set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',10);
			
	end
	
end	

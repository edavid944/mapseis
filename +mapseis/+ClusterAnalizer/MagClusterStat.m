function OutStruct = MagClusterStat(ResultStruct,Mode3D)
	%for now this only analyses, the distances (max min mean median)
	%against magnitude of the mainshock but I may add more to it later.
	
	%Added Time (Max and Mean)
	%Added AfterShockMag (Max and Mean)
	
	
	
	
	if nargin<2
		Mode3D=false
	end	
		
	
	MagVector=[];
	MaxDist=[];
	MinDist=[];
	MeanDist=[];
	MedianDist=[];
	
	MaxTime=[];
	MeanTime=[];
	
	MaxAMag=[];
	MeanAMag=[];
	
	
	if ~Mode3D
		%normal 2D distances used
		
		%loop over all
		for i=1:numel(ResultStruct.AnalysedClusters)
		
			%get data
			MainShockMag=ResultStruct.MainShocks{i}(6);
			AfterShocks=ResultStruct.Aftershocks{i};
			Dist=ResultStruct.SingleDistances{i};
			Times=ResultStruct.SingleTimes{i};
			
			
			%Magnitude
			MagVector(i)=MainShockMag;
			
			%Max Distance
			Maxxer=max(Dist);
			MaxDist(i)=Maxxer(1);
			
			%Min Distance
			Minner=min(Dist);
			MinDist(i)=Minner(1);
			
			%Mean Distance
			MeanDist(i)=mean(Dist);
			
			%Median Distance
			MedianDist(i)=median(Dist);
			
			
			%Max Time
			MaxxerTime=max(Times);
			MaxTime(i)=MaxxerTime(1);
			
			%Mean Time
			MeanTime(i)=mean(Times);
			
			
			%Max AftershockMag
			MaxxerAMag=max(AfterShocks(:,6));
			MaxAMag(i)=MaxxerAMag(1);
			
			%Mean AftershockMag
			MeanAMag(i)=mean(AfterShocks(:,6));
			
			
			
		
		end
	
		
		
	else
	
		%use 3D distances
		%loop over all
		for i=1:numel(ResultStruct.AnalysedClusters)
			
			%get data
			MainShockMag=ResultStruct.MainShocks{i}(6);
			AfterShocks=ResultStruct.Aftershocks{i};
			Dist=ResultStruct.Single3DDist{i};
			Times=ResultStruct.SingleTimes{i};
			
			
			%Magnitude
			MagVector(i)=MainShockMag;
			
			%Max Distance
			Maxxer=max(Dist);
			MaxDist(i)=Maxxer(1);
			
			%Min Distance
			Minner=min(Dist);
			MinDist(i)=Minner(1);
			
			%Mean Distance
			MeanDist(i)=mean(Dist);
			
			%Median Distance
			MedianDist(i)=median(Dist);
			
			
			%Max Time
			MaxxerTime=max(Times);
			MaxTime(i)=MaxxerTime(1);
			
			%Mean Time
			MeanTime(i)=mean(Times);
			
			
			%Max AftershockMag
			MaxxerAMag=max(AfterShocks(:,6));
			MaxAMag(i)=MaxxerAMag(1);
			
			%Mean AftershockMag
			MeanAMag(i)=mean(AfterShocks(:,6));
			
			
			
		
		end	
		
		
	
	end
	
	
	%pack it
	OutStruct.MagVector=MagVector;
	OutStruct.MaxDist=MaxDist;
	OutStruct.MinDist=MinDist;
	OutStruct.MeanDist=MeanDist;
	OutStruct.MedianDist=MedianDist;
	
	OutStruct.MaxTime=MaxTime;
	OutStruct.MeanTime=MeanTime;
	
	OutStruct.MaxAMag=MaxAMag;
	OutStruct.MeanAMag=MeanAMag;
	
end

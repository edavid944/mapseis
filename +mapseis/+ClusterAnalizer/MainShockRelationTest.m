function MagStruct = MainShockRelationTest(ResultStruct,Mode3D,DensePoints,FilePrefix,FolderPath)
	%Goes through all the clusters and plots the Main magnitude against 
	%various values of the aftershocks
	
	import mapseis.ClusterAnalizer.*;
	import mapseis.plot.*;
	
	%first set all the data 
	ResultStruct=KillEmptyCluster(ResultStruct);
	ResultStruct=Add_3D_distance(ResultStruct);
	MagStruct=MagClusterStat(ResultStruct,Mode3D);
	
	
	oldFold=pwd;
	
	
	
	%Relations to analyse
	FileSuffix={'MainMag_vs_MaxDist','MainMag_vs_MinDist','MainMag_vs_MeanDist',...
			'MainMag_vs_MedianDist','MainMag_vs_MaxTime','MainMag_vs_MeanTime',...
			'MainMag_vs_MaxAMag','MainMag_vs_MeanAMag'};
	FieldInData={'MaxDist','MinDist','MeanDist','MedianDist','MaxTime',...
			'MeanTime','MaxAMag','MeanAMag'};
	TitleNames={'Max. Distance','Min. Distance','Mean Distance','Median Distance','Max.Time',...
			'Mean Time','Max. Aftershock Mag.','Mean AfterShock Mag.'};	
	YAxisLabelNames={'Distance (km) ','Distance (km) ','Distance (km) ','Distance (km) ',...
			'Time (d) ','Time (d) ','AfterShock Mag. ','AfterShock Mag. '};
	
	%Lets bring some color order (point mode)
	minima='b';	%blue=minima
	maxima='r';	%red=maxima
	meanval='g';	%green=mean
	medval='m';	%magenta=median
	
	ColorList={maxima,minima,meanval,medval,maxima,meanval,maxima,meanval};
	
	%needed in all plots
	MagVector=MagStruct.MagVector;					
	xlimiter=[floor(min(MagVector)) ceil(max(MagVector))];	
	
	%default config:
	PlotConfig	= struct(	'PlotType','DensePoint',...
						'Data',[[]],...
						'MarkerStylePreset','normal',...
						'MarkerSize',150,...
						'X_Axis_Label','MainShock Magnitude ',...
						'Y_Axis_Label','Distance (km) ',...
						'CustomColors','jet',...
						'X_Axis_Limit',[xlimiter],...
						'Y_Axis_Limit',[[]],...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LogMode',true,...
						'LegendText','Time - Distance');

	
					
	if DensePoints
		
		for i=1:numel(FileSuffix)
			%get data:
			VersusData=MagStruct.(FieldInData{i});
			
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			plotAxis=gca;
			
			PlotData=[MagVector',VersusData'];
			ylimiter=[floor(min(VersusData)) ceil(max(VersusData))];
			
			%set plotdata
			PlotConfig.Data=PlotData;
			PlotConfig.Y_Axis_Limit=ylimiter;
			PlotConfig.Y_Axis_Label=YAxisLabelNames{i};
			
			%and plot it
			[handle legendentry] = PlotDensityPoints2D(plotAxis,PlotConfig);
	
	
			%here will be some commands to make it look nicer
			set(plotAxis,'LineWidth',2,'FontWeight','bold','FontSize',14,'Box','on');
			
			%set title
			tit=title(plotAxis,['Mainshock Magnitude vs. ',TitleNames{i},'  ']);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			
			
			%and save everything
			%create filename
			filename=[FilePrefix,'_Dense_',FileSuffix{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
					
			close(fig);           
			
		
			
		
		end
		
	else
		%only points
		for i=1:numel(FileSuffix)
			%get data:
			VersusData=MagStruct.(FieldInData{i});
			
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			plotAxis=gca;
			
			
			ylimiter=[floor(min(VersusData)) ceil(max(VersusData))];
			
			
			
			%and plot it
			p2=plot(plotAxis,MagVector,VersusData);
	
			%set everything in the plot right
			set(p2,'LineStyle','none','Marker','.','MarkerSize',6,'Color',ColorList{i});
			
			xlab=xlabel('MainShock Magnitude ');
			ylab=ylabel(YAxisLabelNames{i});
			
			xlim(xlimiter);
			ylim(ylimiter);
			
			%here will be some commands to make it look nicer
			set(plotAxis,'LineWidth',2,'FontWeight','bold','FontSize',14,'Box','on');
			
			%set title
			tit=title(plotAxis,['Mainshock Magnitude vs. ',TitleNames{i},'  ']);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			
			
			
			%and save everything
			%create filename
			filename=[FilePrefix,'_Points_',FileSuffix{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
					
			close(fig);           
			
			
			
		
		end
	end
	

	
	
end

function TheFrame = BuildReferenceFrame(Datastore,FilterList,NrSpace,NrTime,CombinedMode)
	%This function uses kmeans to build a reference frame for assigning cluster IDs. 
	%It can use the actuall earthquake location or the location of the clusters 
	%(means the mainshock) if CombinedMode is set to false. NrSpace sets the number 
	%of spatial points used, and NrTime sets the number of points in Time used
	%if CombinedMode is set to true, kmeans applies the points in 3 dimension 
	%at once (lon, lat, time) using NrSpace of nodes.
	
	%Labeling is done with letters
	
	import mapseis.projector.*;
	import mapseis.util.*;
	
	% [SpaceLabel SpaceFrame  TimeLabel TimeFrame]
	
	%some data needed
	LetterArray='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	
	%at the moment the system uses two letters for each reference point
	%which should be more than enough, if not it is easy to change
	if NrSpace>numel(LetterArray)^2;
		error('Too many spacial-points choosen')
	end	
	
	if NrTime>numel(LetterArray)^2;
		error('Too many time-points choosen')
	end	
	
	
	%get data from datastore with filterlist
	Datastore.TypeUsed=[true;true;true];
	Datastore.ShowUnclassified=true;
		
	%update Filterlist to catalog
	if ~isempty(FilterList)
		FilterList.changeData(Datastore)
		FilterList.update;
		selected=FilterList.getSelected;
		
	else
		selected=true(Datastore.getRowCount,1);
		
	end	
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	
	if CombinedMode
		%reference point search
		[IDX,TimeSpacePoints] = kmeans([ShortCat(:,1:2),ShortCat(:,4)],NrSpace);
		%sort them in time order
		[Temp Sorbe]=sort(TimeSpacePoints(:,3));
		TimeSpacePoints=TimeSpacePoints(Sorbe,:);
		
		%name the points
		if NrSpace<=numel(LetterArray)
			%could be done in vector, but it is more readable like this and for 
			%double letter mode it would difficult anyway (possible, but look messy)
			%And one should also code for humans first.
			for i=1:NrSpace
				SpaceLabel{i}=['_',LetterArray(i)];
			end
			TimeLabel=SpaceLabel;
			
		else
			for i=1:NrSpace
				
				%first letter
				FirstLetSel=floor(i/numel(LetterArray));
				
				if FirstLetSel==0
					FirstLetter='_';
				else
					FirstLetter=LetterArray(FirstLetSel);
				end
				
				%second letter
				SecLetSel=mod(i,numel(LetterArray))+1;
				SecondLetter=LetterArray(SecLetSel);
				
				%Label
				SpaceLabel{i}=[FirstLetter,SecondLetter];
				
			end
			
			TimeLabel=SpaceLabel;
			
		end
		
		%add decyear to data and assemble frames
		SpaceFrame = TimeSpacePoints(:,1:2);
		TimeFrame(:,1) = TimeSpacePoints(:,3);
		TimeFrame(:,2) = decyear(datevec(TimeSpacePoints(:,3)));
	
	else
		%reference point search
		[IDX,SpacePoints] = kmeans(ShortCat(:,1:2),NrSpace);
		[IDX,TimePoints] = kmeans(ShortCat(:,4),NrTime);
		
		%sort them in time order
		TimePoints=sort(TimePoints);
		
		%name the points
		if NrSpace<=numel(LetterArray)
			%could be done in vector, but it is more readable like this and for 
			%double letter mode it would difficult anyway (possible, but look messy)
			%And one should also code for humans first.
			for i=1:NrSpace
				SpaceLabel{i}=['_',LetterArray(i)];
			end
			
		else
			for i=1:NrSpace
				
				%first letter
				FirstLetSel=floor(i/numel(LetterArray));
				
				if FirstLetSel==0
					FirstLetter='_';
				else
					FirstLetter=LetterArray(FirstLetSel);
				end
				
				%second letter
				SecLetSel=mod(i,numel(LetterArray))+1;
				SecondLetter=LetterArray(SecLetSel);
				
				%Label
				SpaceLabel{i}=[FirstLetter,SecondLetter];
				
			end
			
			
		end
		
		%same for time
		if NrTime<=numel(LetterArray)
			%could be done in vector, but it is more readable like this and for 
			%double letter mode it would difficult anyway (possible, but look messy)
			%And one should also code for humans first.
			for i=1:NrTime
				TimeLabel{i}=['_',LetterArray(i)];
			end
			
		else
			for i=1:NrTime
				
				%first letter
				FirstLetSel=floor(i/numel(LetterArray));
				
				if FirstLetSel==0
					FirstLetter='_';
				else
					FirstLetter=LetterArray(FirstLetSel);
				end
				
				%second letter
				SecLetSel=mod(i,numel(LetterArray))+1;
				SecondLetter=LetterArray(SecLetSel);
				
				%Label
				TimeLabel{i}=[FirstLetter,SecondLetter];
				
			end
			
			
		end
		
		
		%add decyear to data and assemble frames
		SpaceFrame = SpacePoints;
		TimeFrame(:,1) = TimePoints;
		TimeFrame(:,2) = decyear(datevec(TimePoints));

		
	end
	
	
	
	%store data in output structur
	TheFrame.SpaceLabel = SpaceLabel;
	TheFrame.SpaceFrame = SpaceFrame;
	TheFrame.TimeLabel = TimeLabel;
	TheFrame.TimeFrame = TimeFrame;
	TheFrame.CombinedMode = CombinedMode;
	TheFrame.Version = 'v1.0';
	
	
	
end

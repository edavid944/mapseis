function AllHistory = MassClusterBinManDepth(ResultStruct,NumOfBin,FilePrefix,FolderPath)
	%Plots every cluster in a histogram and also outputs them into a file
	
	import mapseis.ClusterAnalizer.*;
	

	
	%save the old path
	oldFold=pwd;
	
	ClusterID=[];
	MainMags=[];
	TimeMain=[];
	DistHist={};
	AzHist={};
	TimeHist={};
	DepthHist={};
	Full3DHist={};
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeVal=ResultStruct.SingleTimes{i};
		AzVal=ResultStruct.SingleAzimutes{i};
		DistVal=ResultStruct.SingleDistances{i};
		DepthVal=ResultStruct.SingleDepthDist{i};
		Dist3DVal=ResultStruct.Single3DDist{i};
		
		if numel(TimeVal)>=10
		
			%Distance
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			
			%save keydata
			ClusterID(end+1)=ResultStruct.AnalysedClusters(i);
			MainMags(end+1)=ResultStruct.MainShocks{i}(6);
			TimeMain(end+1)=ResultStruct.MainShocks{i}(3);
			
			plotAxis=gca
			[Val Bins]=hist(DistVal,NumOfBin);
			hist(DistVal,NumOfBin);
			xlabel('Distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','r','EdgeColor','w')
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%save histogram
			DistHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Distance_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Azimute
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(AzVal,NumOfBin);
			hist(AzVal,NumOfBin);
			xlabel('Azimute (deg)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','g','EdgeColor','w')
			
			%save histogram
			AzHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Azimute_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Time
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(TimeVal,NumOfBin);
			hist(TimeVal,NumOfBin);
			xlabel('Time (d)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','b','EdgeColor','w')
			
			%save histogram
			TimeHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Time_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Depth
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(DepthVal,NumOfBin);
			hist(DepthVal,NumOfBin);
			xlabel('Depth Distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','c','EdgeColor','w')
			
			%save histogram
			DepthHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_DepthDist_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			%3DDist
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(Dist3DVal,NumOfBin);
			hist(Dist3DVal,NumOfBin);
			xlabel('3D distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','m','EdgeColor','w')
			
			%save histogram
			Full3DHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Dist3D_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
		end
	end
	
	
	
	
	
	if isfield(ResultStruct,'AllTimes');
		%Now for the summary
		%get data
		TimeVal=ResultStruct.AllTimes;
		AzVal=ResultStruct.AllAzimutes;
		DistVal=ResultStruct.AllDistances;
		DepthVal=ResultStruct.AllDepthDist;
		Dist3DVal=ResultStruct.All3DDist;
		
		%Distance
		fig=figure;
		set(fig,'pos',[50 50 800 600]);
		
		plotAxis=gca
		[Val Bins]=hist(DistVal,NumOfBin);
		hist(DistVal,NumOfBin);
		xlabel('Distance (km)  ');
		ylabel('Nr. Earthquake  ');
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		%color it
		h = findobj(gca,'Type','patch');
		set(h,'FaceColor','r','EdgeColor','w')
		
		%Writing title over top subplot
		tit=title(plotAxis,'All Clusters: Distance ');
		
		set(tit,'FontSize',24,'FontWeight','bold');
		
		%save histogram
		SumHistDist=[Bins',Val'];
		
		
		%save picture
		%create filename
		filename=[FilePrefix,'_Histogram_Distance_AllCluster.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
		
		close(fig);
		
		
		
		%Azimute
		fig=figure;
		set(fig,'pos',[50 50 800 600]);
	
		plotAxis=gca
		[Val Bins]=hist(AzVal,NumOfBin);
		hist(AzVal,NumOfBin);
		xlabel('Azimute (deg)  ');
		ylabel('Nr. Earthquake  ');
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		%Writing title over top subplot
		tit=title('All Clusters: Azimute ');
		
		set(tit,'FontSize',24,'FontWeight','bold');
		
		%color it
		h = findobj(gca,'Type','patch');
		set(h,'FaceColor','g','EdgeColor','w')
		
		%save histogram
		SumHistAz=[Bins',Val'];
		
		
		%save picture
		%create filename
		filename=[FilePrefix,'_Histogram_Azimute_AllCluster.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
		
		close(fig);
		
		
		
		%Time
		fig=figure;
		set(fig,'pos',[50 50 800 600]);
	
		plotAxis=gca
		[Val Bins]=hist(TimeVal,NumOfBin);
		hist(TimeVal,NumOfBin);
		xlabel('Time (d)  ');
		ylabel('Nr. Earthquake  ');
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		%Writing title over top subplot
		tit=title(plotAxis,'All Clusters:Time ');
		
		set(tit,'FontSize',24,'FontWeight','bold');
		
		%color it
		h = findobj(gca,'Type','patch');
		set(h,'FaceColor','b','EdgeColor','w')
		
		%save histogram
		SumHistTime=[Bins',Val'];
		
		
		%save picture
		%create filename
		filename=[FilePrefix,'_Histogram_Time_AllCluster.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
		
		close(fig);
		
		
		
		
		%Depth
		fig=figure;
		set(fig,'pos',[50 50 800 600]);
	
		plotAxis=gca
		[Val Bins]=hist(DepthVal,NumOfBin);
		hist(DepthVal,NumOfBin);
		xlabel('Depth Distance (km)  ');
		ylabel('Nr. Earthquake  ');
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		%Writing title over top subplot
		tit=title(plotAxis,'All Clusters: Depth Distance ');
		set(tit,'FontSize',24,'FontWeight','bold');
		
		%color it
		h = findobj(gca,'Type','patch');
		set(h,'FaceColor','c','EdgeColor','w')
		
		%save histogram
		SumDepthHist=[Bins',Val'];
		
		
		%save picture
		%create filename
		filename=[FilePrefix,'_Histogram_DepthDist_AllCluster.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
		
		close(fig);
		
		
		%3DDist
		fig=figure;
		set(fig,'pos',[50 50 800 600]);
	
		plotAxis=gca
		[Val Bins]=hist(Dist3DVal,NumOfBin);
		hist(Dist3DVal,NumOfBin);
		xlabel('3D distance (km)  ');
		ylabel('Nr. Earthquake  ');
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		%Writing title over top subplot
		tit=title(plotAxis,'All Clusters: 3D Distance ');
		set(tit,'FontSize',24,'FontWeight','bold');
		
		%color it
		h = findobj(gca,'Type','patch');
		set(h,'FaceColor','m','EdgeColor','w')
		
		%save histogram
		SumFull3DHist=[Bins',Val'];
		
		
		%save picture
		%create filename
		filename=[FilePrefix,'_Histogram_Dist3D_AllCluster.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
				
		close(fig);
	end
	
	
	
	
	
	%Pack it all together
	AllHistory.ClusterIDs=ClusterID;
	AllHistory.MainshockMags=MainMags;
	AllHistory.TimeAtMainshock=TimeMain;
	AllHistory.SingleHistDist=DistHist;
	AllHistory.SingleHistAz=AzHist;
	AllHistory.SingleHistTime=TimeHist;
	AllHistory.SingleHistDepth=DepthHist;
	AllHistory.SingleHistFull3D=Full3DHist;
	
	if isfield(ResultStruct,'AllTimes');
		AllHistory.SummaryHistDist=SumHistDist;
		AllHistory.SummaryHistAz=SumHistAz;
		AllHistory.SummaryHistTime=SumHistTime;
		AllHistory.SummaryHistDepth=SumDepthHist;
		AllHistory.SummaryHistFull3D=SumFull3DHist;
	end
	
	
	
	
	
end

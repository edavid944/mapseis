function OutStruct=Add_3D_distance(OutStruct)
	%adds a distance with depth to the clusters and plots DepthHist
	%if wanted
	
	
	%Empty Array needed
	Single3DDist={};
	SingleDepthDist={};
	All3DDist=[];
	AllDepthDist=[];
	
	
	%loop over all
	for i=1:numel(OutStruct.AnalysedClusters)
		if ~isempty(OutStruct.MainShocks{i});
			
			%get data
			MainShockDepth=OutStruct.MainShocks{i}(1,7);
			AfterShockDepth=OutStruct.Aftershocks{i}(:,7);
			Dist2D=OutStruct.SingleDistances{i};
			
			%Depth distances
			DepthDist=abs((MainShockDepth-AfterShockDepth));
			
			%calc new 3D dist
			Dist3D=(Dist2D.^2+(DepthDist).^2).^(1/2);
		
			%store it
			All3DDist=[All3DDist;Dist3D];
			AllDepthDist=[AllDepthDist;DepthDist];
			Single3DDist{i,1}=Dist3D;
			SingleDepthDist{i,1}=DepthDist;
		else
			Single3DDist{i,1}=[];
			SingleDepthDist{i,1}=[];
		
		end
	
	end
	
	
	%add to structure
	OutStruct.Single3DDist=Single3DDist;
	OutStruct.SingleDepthDist=SingleDepthDist;
	OutStruct.All3DDist=All3DDist;
	OutStruct.AllDepthDist=AllDepthDist;
	



end

function OutStruct = InterEventSpace(ShortCat,DistCons,TimeCons,MagCons,RealDist)
	%calculates the distances between all events
	import mapseis.util.stat.*;
	
	
	
	%DistCutOff,DistSpacer,TimeCutOff,TimeSpacer
	
	if nargin<5
		RealDist=false
	end
	
	%unpack data
	MinDist=DistCons(1);
	DistCutOff=DistCons(2);
	DistSpacer=DistCons(3);
	MinTime=TimeCons(1);
	TimeCutOff=TimeCons(2);
	TimeSpacer=TimeCons(3);
	
	
	if isempty(MagCons)
		%only one bin go for -20 to 20, that will be reached never anyway
		MinMag=0;
		MaxMag=0;
		MagBin=40;
	else
		MinMag=MagCons(1);
		MaxMag=MagCons(2);
		MagBin=MagCons(3);
	end
	
	
	
	%create bins and grid
	DistVec=MinDist:DistSpacer:DistCutOff;
	DistVecDeg=km2deg(MinDist:DistSpacer:DistCutOff);;
	
	TimeVec=MinTime:TimeSpacer:TimeCutOff;
	
	MagVec=MinMag:MagBin:MaxMag;
	
	[TimeGrid DistGrid] = meshgrid(TimeVec,DistVec);
	[TimeGrid DistGridDeg] = meshgrid(TimeVec,DistVecDeg);
	CountGrid=zeros(size(TimeGrid'));
	
	%set up magnitude bins 
	for i=1:numel(MagVec)
		MagHistGrid{i}=CountGrid;
	end
	
	%ElementInGrid=numel(CountGrid);
	
	%Spacings
	HalfSpaceDist=km2deg(DistSpacer/2);
	Temp=diff(TimeVec);
	HalfSpaceTime=Temp(1)/2;
	
	if ~RealDist
		%Now go for it
		tic
		for i = 1:length(ShortCat);
			disp(i)
			allDist=(((ShortCat(i,1)-ShortCat(:,1)).^2+(ShortCat(i,2)-ShortCat(:,2)).^2).^0.5);
			allTime=ShortCat(:,4)-ShortCat(i,4);
			
			
			%Loop over the result grid
			%This may could be improved, but leave it for now
			%WriteToMap();
			%WriteToMapV2();
			%CountGrid=CountGrid';
			
			%kick out not needed stuff
			TooFarAway=allDist>DistVecDeg(end);
			NotInTime=allTime<0&allTime>TimeCutOff;
			LeftDist=allDist(~TooFarAway&~NotInTime);
			LeftTime=allTime(~TooFarAway&~NotInTime);
			ShorterCat=ShortCat(~TooFarAway&~NotInTime,:);
			
			%find magbin
			MyMag=MagVec-MagBin/2<=ShortCat(i,5)&MagVec+MagBin/2>ShortCat(i,5);
			if any(MyMag)
				InMagRange=MagVec(MyMag)-MagBin/2<=ShorterCat(:,5)&MagVec(MyMag)+MagBin/2>ShorterCat(:,5);
				if sum(InMagRange)==0
					MagHistGrid{MyMag}=MagHistGrid{MyMag}+CountGrid;
				else
						MagHistGrid{MyMag}=MagHistGrid{MyMag}+hist2(LeftDist(InMagRange),LeftTime(InMagRange),DistVecDeg,TimeVec);
				end	
			end
			%CountGrid=CountGrid+hist2(allDist,allTime,DistVecDeg,TimeVec);
			%CountGrid=CountGrid+hist2(allTime,allDist,TimeVec,DistVecDeg);
			%CountGrid=CountGrid';
		end
		
		
		UsedTime=toc;
		disp(['Used calculation Time: ',num2str(UsedTime),' s  ']);
	
	else
		%Now go for it
		tic
		for i = 1:length(ShortCat);
			disp(i)
			allDist=distance(ShortCat(i,2),ShortCat(i,1),ShortCat(:,2),ShortCat(:,1));
			allTime=ShortCat(:,4)-ShortCat(i,4);
			
			
			%Loop over the result grid
			%This may could be improved, but leave it for now
			%WriteToMap();
			%WriteToMapV2();
			%CountGrid=CountGrid';
			%kick out not needed stuff
			TooFarAway=allDist>DistVecDeg(end);
			NotInTime=allTime<0&allTime>TimeCutOff;
			LeftDist=allDist(~TooFarAway&~NotInTime);
			LeftTime=allTime(~TooFarAway&~NotInTime);
			ShorterCat=ShortCat(~TooFarAway&~NotInTime,:);
			
			%find magbin
			MyMag=MagVec-MagBin/2<=ShortCat(i,5)&MagVec+MagBin/2>ShortCat(i,5);
			if any(MyMag)
				InMagRange=MagVec(MyMag)-MagBin/2<=ShorterCat(:,5)&MagVec(MyMag)+MagBin/2>ShorterCat(:,5);
				if sum(InMagRange)==0
					MagHistGrid{MyMag}=MagHistGrid{MyMag}+CountGrid;
				else
						MagHistGrid{MyMag}=MagHistGrid{MyMag}+hist2(LeftDist(InMagRange),LeftTime(InMagRange),DistVecDeg,TimeVec);
				end	
			end
			
			%CountGrid=CountGrid+hist2(allDist,allTime,DistVecDeg,TimeVec);
			%CountGrid=CountGrid+hist2(allTime,allDist,TimeVec,DistVecDeg);
			%CountGrid=CountGrid';
		end
		
		
		UsedTime=toc;
		disp(['Used calculation Time: ',num2str(UsedTime),' s  ']);
	end
	
	%sum up single mag bin histograms
	for i=1:numel(MagVec)
			CountGrid=CountGrid+MagHistGrid{i};
	end
	
	
	
	%store results
	OutStruct.TimeGrid=TimeGrid;
	OutStruct.DistGrid=DistGrid;
	OutStruct.DistGridDeg=DistGridDeg;
	OutStruct.TimeVec=TimeVec;
	OutStruct.DistVec=DistVec;
	OutStruct.DistVecDeg=DistVecDeg;
	OutStruct.MagVec=MagVec;
	OutStruct.AllMag=CountGrid;
	OutStruct.SingleMags=MagHistGrid;
	
	
	function WriteToMap()
		CountGrid=CountGrid';
		
		for j = 1:ElementInGrid
			InIt=(TimeGrid(j)-HalfSpaceTime)<=allTime & (TimeGrid(j)+HalfSpaceTime)>allTime &...			
			(DistGridDeg(j)-HalfSpaceDist)<=allDist & (DistGridDeg(j)+HalfSpaceDist)>allDist;
				CountGrid(ElementInGrid)=CountGrid(ElementInGrid)+sum(InIt);
		end
		CountGrid=CountGrid';
	
	end
	
	
	
	function WriteToMapV2()	
		
		%Kick away everthing out of range
		TooFarAway=allDist>DistVecDeg(end);
		NotInTime=allTime<0&allTime>TimeCutOff;
		LeftDist=allDist(~TooFarAway&~NotInTime);
		LeftTime=allTime(~TooFarAway&~NotInTime);
		
		for j=1:numel(LeftDist)
			InIt=(TimeGrid-HalfSpaceTime)<=LeftTime(j) & (TimeGrid+HalfSpaceTime)>LeftTime(j) &...			
			(DistGridDeg-HalfSpaceDist)<=LeftDist(j) & (DistGridDeg+HalfSpaceDist)>LeftDist(j);
			CountGrid(InIt)=CountGrid(InIt)+1;
		
		end
	
	end

end	

function [StatVals SplittedCat]=SingleCombClusterAnalyser(Datastore,FilterList,CombID,LowMagCut,ClustNum,Override)
	%This function takes a combined cluster and calculates the for every earthquake
	%the distance to the centroid and the time to the centroid
	
	import mapseis.projector.*;
	
	if nargin<6
		Override=false;
	end
	
	if isempty(Override)
		Override=false;
	end
	
	
	if isempty(Datastore.ClusterNR)
		error('No declustering present in the catalog');
	end

	if isnan(all(Datastore.ClusterNR))
		error('No declustering present in the catalog');
	end
	
	
	%init output
	TimeVal=[]; 
	DistVal=[];
	AzVal=[];
	AfterShocks=[];
	Centroid=[];
	MainShock=[];
	StatVals=[];
	SplittedCat=[];
	
	
	%get Data
	eventStruct=Datastore.getFields;
	
	if ~Override & ~isempty(FilterList)
		%turn the whole the declustering off first
		Datastore.TypeUsed=[true;true;true];
		Datastore.ShowUnclassified=true;
		
		%update Filterlist to catalog
		FilterList.changeData(Datastore)
		FilterList.update;
	end
	
	
	if ~isempty(FilterList)
		%get selected eq from the catalog
		selected=FilterList.getSelected;
	
	else
		%just use everything 
		selected=true(Datastore.getRowCount,1);
	end
	
	
	%Cut Magnitudes if needed
	if ~isempty(LowMagCut)
		%throw away everything belowe LowMagCut
		TheMags=eventStruct.mag;
		
		selected=selected & TheMags>=LowMagCut;
		
	end
	
	
	%get declustering data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	
	%Finally some real work 
	%----------------------
	
	%get all events in the cluster
	inCluster = strcmp(CombID,ClustNum)&selected;
	
	if sum(inCluster)==0
		%nothing found, return empty and stop
		disp(['Cluster number: ',num2str(ClustNum),' could not be found with this Filterlist']);
		return;
		
	end
	
	
	%find mainshock
	MainMan=inCluster&EventType==2;
	
	%find aftershocks
	Followers=inCluster&EventType==3;
	
	if sum(MainMan)==0
		%nothing found, return empty and stop
		disp(['Cluster number: ',num2str(ClustNum),' has no mainshock within the selected region']);
		return;
		
	end
	
	if sum(Followers)==0
		%nothing found, return empty and stop
		disp(['Cluster number: ',num2str(ClustNum),' has no aftershocks within the selected region']);
		return;
		
	end

	

	%Now we should have some some actual data to work with
	MainShock=getZMAPFormat(Datastore,MainMan,false);
	AfterShocks=getZMAPFormat(Datastore,Followers,false);
	EveryShock=getZMAPFormat(Datastore,inCluster,false);
	
	
	%Calculate the centroid (just the mean of everything)
	Centroid=mean(EveryShock);
	DateTimeCentroid=mean(eventStruct.dateNum(inCluster));
	
	%distance and azimute (mainshock)
	[DistValMain, AzValMain] = distance(Centroid(1,2),Centroid(1,1),MainShock(:,2),MainShock(:,1));
	DistValMain=deg2km(DistValMain);
	
	%...and time (in days) (mainshock
	TimeValMain=eventStruct.dateNum(MainMan)-DateTimeCentroid;
	
	
	%
	%distance and azimute (aftershocks)
	[DistValAfter, AzValAfter] = distance(Centroid(1,2),Centroid(1,1),AfterShocks(:,2),AfterShocks(:,1));
	DistValAfter=deg2km(DistValAfter);
	
	%...and time (in days) (aftershocks
	TimeValAfter=eventStruct.dateNum(Followers)-DateTimeCentroid;
	
	
	%
	%distance and azimute (everything)
	[DistValAll, AzValAll] = distance(Centroid(1,2),Centroid(1,1),EveryShock(:,2),EveryShock(:,1));
	DistValAll=deg2km(DistValAll);
	
	%...and time (in days) (aftershocks
	TimeValAll=eventStruct.dateNum(inCluster)-DateTimeCentroid;
	
	
	
	%pack it
	StatVals.DistValMain=DistValMain;
	StatVals.AzValMain=AzValMain;
	StatVals.TimeValMain=TimeValMain;
	StatVals.DistValAfter=DistValAfter;
	StatVals.AzValAfter=AzValAfter;
	StatVals.TimeValAfter=TimeValAfter;
	StatVals.DistValAll=DistValAll;
	StatVals.AzValAll=AzValAll;
	StatVals.TimeValAll=TimeValAll;
	
	SplittedCat.MainShock=MainShock;
	SplittedCat.AfterShocks=AfterShocks;
	SplittedCat.EveryShock=EveryShock;
	SplittedCat.Centroid=Centroid;
	
	
end

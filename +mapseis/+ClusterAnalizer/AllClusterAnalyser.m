function OutStruct = AllClusterAnalyser(Datastore,FilterList,LowMagCut,NoListSend)
	%Goes through all the clusters and analyses them
	%if NoListSend is true, the filterlist will not be send to the SingleClusterAnalyser
	%This will allows to only adress a certain magnitude of mainshocks but still adding 
	%every marked aftershock to it
	
	import mapseis.ClusterAnalizer.*;
	
	OutStruct=[];
	
	if nargin<4
		NoListSend=false;
	end
	
	if isempty(NoListSend)
		NoListSend=false;
	end
	
	
	
	if isempty(Datastore.ClusterNR)
		error('No declustering present in the catalog');
	end

	if isnan(all(Datastore.ClusterNR))
		error('No declustering present in the catalog');
	end

	
	%prepare data
	if ~isempty(FilterList)
		%get selected eq from the catalog
		Datastore.TypeUsed=[true;true;true];
		Datastore.ShowUnclassified=true;
		
		%update Filterlist to catalog
		FilterList.changeData(Datastore)
		FilterList.update;
		selected=FilterList.getSelected;
	
	else
		%just use everything 
		selected=true(Datastore.getRowCount,1);
	end
	
	
	%get all clusters
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	%get unique cluster numbers within the filterlist
	ClusterToCheck=unique(ClusterNR(~isnan(ClusterNR)&selected));
	
	
	%init cell arrays and arrays
	FullTime=[];
	FullDist=[];
	FullAzzel=[];
	
	SingleTime=cell(numel(ClusterToCheck),1);
	SingleDist=cell(numel(ClusterToCheck),1);
	SingleAzzel=cell(numel(ClusterToCheck),1);
	SingleMainShock=cell(numel(ClusterToCheck),1);
	SingleAfterShock=cell(numel(ClusterToCheck),1);
	
	
	if NoListSend
		TheFilter=[]
	else
		TheFilter=FilterList;
	end	
	
	
	parfor i=1:numel(ClusterToCheck)
		[SingleTime{i} SingleDist{i} SingleAzzel{i} SingleAfterShock{i} SingleMainShock{i}] = ...
			SingleClusterAnalyser(Datastore,TheFilter,LowMagCut,ClusterToCheck(i),true)
				
		FullTime=[FullTime;SingleTime{i}];
		FullDist=[FullDist;SingleDist{i}];
		FullAzzel=[FullAzzel;SingleAzzel{i}];
		
		disp(['Cluster ',num2str(i),' from ' ,num2str(numel(ClusterToCheck))]);
			
	end
	

	%Pack everthing in structure
	OutStruct.AllTimes = FullTime;
	OutStruct.AllDistances = FullDist;
	OutStruct.AllAzimutes = FullAzzel;
	OutStruct.SingleTimes = SingleTime;
	OutStruct.SingleDistances = SingleDist; 
	OutStruct.SingleAzimutes = SingleAzzel;
	OutStruct.MainShocks = SingleMainShock;
	OutStruct.Aftershocks = SingleAfterShock;
	OutStruct.AnalysedClusters = ClusterToCheck;
	
	
end

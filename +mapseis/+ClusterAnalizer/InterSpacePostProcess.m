function PostProcessRes = InterSpacePostProcess(OutStruct,ShortCat,WorkMode)
	%This function is meant as a prototype and testing platform to check
	%out different post-processing algorithmn to the InterEventSpace function
	%Output
	
	import mapseis.util.stat.*;
	
	switch WorkMode
		case 'CumNum'
			CumMagSlice={};
			AllTogether=zeros(size(OutStruct.AllMag));
			
			for i=1:numel(OutStruct.SingleMags)
				AllTogether=AllTogether+OutStruct.SingleMags{i};
				CumMagSlice{i}=AllTogether;
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.CumMagSlice=CumMagSlice;
			
			
		case 'CumNumNorm'
			CumMagSlice={};
			AllTogether=zeros(size(OutStruct.AllMag));
			TotalNum=sum(sum(OutStruct.AllMag));
			
			AllMagNorm=OutStruct.AllMag/TotalNum;
			
			
			for i=1:numel(OutStruct.SingleMags)
				AllTogether=AllTogether+OutStruct.SingleMags{i}/TotalNum;
				CumMagSlice{i}=AllTogether;
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.CumMagSliceNorm=CumMagSlice;
			PostProcessRes.AllMagNorm=AllMagNorm;
			PostProcessRes.TotalNum=TotalNum;
			
			
		case 'CumNumWeights'
			CumMagSlice={};
			AllTogether=zeros(size(OutStruct.AllMag));
			TotalNum=sum(sum(OutStruct.AllMag));
			MagBins=OutStruct.MagVec;
			AllMagNorm=OutStruct.AllMag/TotalNum;
			
			
			for i=1:numel(OutStruct.SingleMags)
				AllTogether=AllTogether+OutStruct.SingleMags{i}/TotalNum*10^MagBins(i);
				CumMagSlice{i}=AllTogether;
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.CumMagSliceWeight=CumMagSlice;
			PostProcessRes.AllMagWeight=AllTogether;
			PostProcessRes.TotalNum=TotalNum;
			
		
		case 'MagWeights'
			WeightMagSlice={};
			AllTogether=zeros(size(OutStruct.AllMag));
			TotalNum=sum(sum(OutStruct.AllMag));
			
			MagBins=OutStruct.MagVec;
			
			
			for i=1:numel(OutStruct.SingleMags)
				WeightMagSlice{i}=(OutStruct.SingleMags{i}/TotalNum)*10^MagBins(i);
				AllTogether=AllTogether+WeightMagSlice{i};
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.WeightMagSlice=WeightMagSlice;
			PostProcessRes.AllMagWeight=AllTogether;
			PostProcessRes.TotalNum=TotalNum;		
		
			
		case 'Normalize'
			NormMagSlice={};
			TotalNum=sum(sum(OutStruct.AllMag));
			AllMagNorm=OutStruct.AllMag/TotalNum;
			
			for i=1:numel(OutStruct.SingleMags)
				NormMagSlice{i}=OutStruct.SingleMags{i}/TotalNum;
				
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.MagSliceNorm=NormMagSlice;
			PostProcessRes.AllMagNorm=AllMagNorm;
			PostProcessRes.TotalNum=TotalNum;
			
			
		case 'NormDist'
			NormMagSlice={};
			%could be made faster, by vectorization
			
			for j=1:numel(OutStruct.DistVec)
				CurLine=OutStruct.AllMag(:,j);
				TotalNumLine=sum(CurLine);
				
				AllMagNorm(:,j)=CurLine/TotalNumLine;
				
				
				for i=1:numel(OutStruct.SingleMags)
					
					NormMagSlice{i}(:,j)=OutStruct.SingleMags{i}(:,j)/TotalNumLine;
					
				end
			
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.MagSliceNormDist=NormMagSlice;
			PostProcessRes.AllMagNormDist=AllMagNorm;
			
			
		case 'NormTime'
			NormMagSlice={};
			%could be made faster, by vectorization
			
			for j=1:numel(OutStruct.TimeVec)
				CurLine=OutStruct.AllMag(j,:);
				TotalNumLine=sum(CurLine);
				
				AllMagNorm(j,:)=CurLine/TotalNumLine;
				
				
				for i=1:numel(OutStruct.SingleMags)
					
					NormMagSlice{i}(j,:)=OutStruct.SingleMags{i}(j,:)/TotalNumLine;
					
				end
			
			end
			
			PostProcessRes=OutStruct;
			PostProcessRes.MagSliceNormTime=NormMagSlice;
			PostProcessRes.AllMagNormTime=AllMagNorm;
			
			
			
		case 'NormTiles'
		
		
	end	
	
	


end

function MassClusterPlotterPointsPolar(ResultStruct,LogSwitch,FilePrefix,FolderPath)
	%Plots every cluster in Density maps and saves them. Only cluster with 
	%more than 9 aftershocks are plotted
	
	import mapseis.ClusterAnalizer.*;
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	
	
	%save the old path
	oldFold=pwd;
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeVal=ResultStruct.SingleTimes{i};
		AzVal=ResultStruct.SingleAzimutes{i};
		DistVal=ResultStruct.SingleDistances{i};

		
		
		%check if it has to be printed
		if numel(TimeVal)>=10
			fig=figure;
			set(fig,'pos',[50 50 1200 600]);
			
			%Distance
			plotAxis=subplot(1,2,1)
			ClusterDensePointsPolarPlotter(plotAxis,TimeVal,AzVal,DistVal,'Spatial',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%Azimute
			plotAxis=subplot(1,2,2)
			ClusterDensePointsPolarPlotter(plotAxis,TimeVal,AzVal,DistVal,'AzTime',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_ClusterNR_Dense_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			
		end
	end
	
	if isfield(ResultStruct,'AllTimes');
		%and the full plot
		fig=figure;
		set(fig,'pos',[50 50 1200 800]);
		
		%get data
		TimeVal=ResultStruct.AllTimes;
		AzVal=ResultStruct.AllAzimutes;
		DistVal=ResultStruct.AllDistances;
		
		
		
		
		%Distance
		plotAxis=subplot(1,2,1)
		ClusterDensePointsPolarPlotter(plotAxis,TimeVal,AzVal,DistVal,'Spatial',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
				
		%Writing title over top subplot
		tit=title('All Clusters  ');
				
		%Azimute
		plotAxis=subplot(1,2,2)
		ClusterDensePointsPolarPlotter(plotAxis,TimeVal,AzVal,DistVal,'AzTime',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		set(tit,'FontSize',24,'FontWeight','bold');
		
				
		%save it
		%create filename
		filename=[FilePrefix,'_AllClusters_Dense.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
				
		close(fig);
	end
	
	
	
end

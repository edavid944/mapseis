function [TimeVal DistVal AzVal AfterShocks MainShock] = SingleClusterAnalyser(Datastore,FilterList,LowMagCut,ClustNum,Override)
	%This function takes a cluster and calculates the for every aftershock
	%the distance to the mainshock and the time to the mainshock
	
	import mapseis.projector.*;
	
	if nargin<5
		Override=false;
	end
	
	if isempty(Override)
		Override=false;
	end
	
	
	if isempty(Datastore.ClusterNR)
		error('No declustering present in the catalog');
	end

	if isnan(all(Datastore.ClusterNR))
		error('No declustering present in the catalog');
	end
	
	
	%init output
	TimeVal=[]; 
	DistVal=[];
	AzVal=[];
	AfterShocks=[];
	MainShock=[];
	
	
	%get Data
	eventStruct=Datastore.getFields;
	
	if ~Override & ~isempty(FilterList)
		%turn the whole the declustering off first
		Datastore.TypeUsed=[true;true;true];
		Datastore.ShowUnclassified=true;
		
		%update Filterlist to catalog
		FilterList.changeData(Datastore)
		FilterList.update;
	end
	
	
	if ~isempty(FilterList)
		%get selected eq from the catalog
		selected=FilterList.getSelected;
	
	else
		%just use everything 
		selected=true(Datastore.getRowCount,1);
	end
	
	
	%Cut Magnitudes if needed
	if ~isempty(LowMagCut)
		%throw away everything belowe LowMagCut
		TheMags=eventStruct.mag;
		
		selected=selected & TheMags>=LowMagCut;
		
	end
	
	
	%get declustering data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	
	%Finally some real work 
	%----------------------
	
	%get all events in the cluster
	inCluster = ClusterNR==ClustNum&selected;
	
	if sum(inCluster)==0
		%nothing found, return empty and stop
		disp(['Cluster number: ',num2str(ClustNum),' could not be found with this Filterlist']);
		return;
		
	end
	
	
	%find mainshock
	MainMan=inCluster&EventType==2;
	
	%find aftershocks
	Followers=inCluster&EventType==3;
	
	if sum(MainMan)==0
		%nothing found, return empty and stop
		disp(['Cluster number: ',num2str(ClustNum),' has no mainshock within the selected region']);
		return;
		
	end
	
	if sum(Followers)==0
		%nothing found, return empty and stop
		disp(['Cluster number: ',num2str(ClustNum),' has no aftershocks within the selected region']);
		return;
		
	end
	
	%correction in the case there are two mainshocks, make the second one an aftershock
	if sum(MainMan)>1
		disp('More than one mainshock... Corrected')
		tocheck=find(MainMan);
		MainMan=false(size(MainMan));
		MainMan(tocheck(1))=true;
		Followers(tocheck(2:end))=true;
		
	end
	
	
	
	%Now we should have some some actual data to work with
	MainShock=getZMAPFormat(Datastore,MainMan,false);
	AfterShocks=getZMAPFormat(Datastore,Followers,false);
	
	
	
	
	%distance and azimute
	[DistVal, AzVal] = distance(MainShock(1,2),MainShock(1,1),AfterShocks(:,2),AfterShocks(:,1));
	DistVal=deg2km(DistVal);
	
	%...and time (in days)
	TimeVal=eventStruct.dateNum(Followers)-eventStruct.dateNum(MainMan);
	
	
	
	
end

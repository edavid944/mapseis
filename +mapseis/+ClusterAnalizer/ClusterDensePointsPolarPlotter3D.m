function handle=ClusterDensePointsPolarPlotter3D(plotAxis,TimeVal,AzVal,DistVal,DepthVal,ModeSelector,LogSwitch)
	
	%plots the result of a cluster analysis into a density map with
	%Time (x) and distance (y). 
	
	%if AzSwitch is set to true, the azimute (direction) will be used instead
	%of the distance.
	
	%if LogSwitch is used a logarithmic scale for the time is used
	
	%In the 3D version depth distance is added as z component. The setting with 
	%Time, Azimute and depth may not make sense, but I left in it anyway, it was not
	%much effort anyway
	
	import mapseis.plot.*;
	import mapseis.converter.*;
	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end

	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	

	
	%treat distance and azimute together (as polar coordinates)
	CorrectedAngle=deg2rad(90-AzVal);
	
	switch ModeSelector
		case 'Spatial'
			XDir=DistVal.*cos(CorrectedAngle);
			YDir=DistVal.*sin(CorrectedAngle);
			Xlab=' X-Dir (km) ';
			Ylab=' Y-Dir (km) ';
			
		case 'AzTime'
			XDir=TimeVal.*cos(CorrectedAngle);
			YDir=TimeVal.*sin(CorrectedAngle);
			Xlab=' X  ';
			Ylab=' Y  ';
	end	
	
	
	%create plotdata
	PlotData=[XDir,YDir,DepthVal];
	absMaxAll=max([ceil(max(abs(XDir))),ceil(max(abs(YDir)))]);
	xlimiter=[-absMaxAll absMaxAll];
	ylimiter=[-absMaxAll absMaxAll];
	zlimiter=[floor(min(DepthVal)) ceil(max(DepthVal))];	
	
	PlotConfig	= struct(	'PlotType','DensePoint3D',...
						'Data',[PlotData],...
						'MarkerStylePreset','normal',...
						'MarkerSize',150,...
						'X_Axis_Label',Xlab,...
						'Y_Axis_Label',Ylab,...
						'Z_Axis_Label', 'Depth Distance (km) ',...
						'CustomColors','jet',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'Z_Axis_Limit',zlimiter,...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LogMode',true,...
						'LegendText','Time - Distance');
		
	

	
	%plot it all
	[handle legendentry] = PlotDensityPoints3D(plotAxis,PlotConfig);
	
	%create a ratio so the depths are not to flat or to tall
	
	set(gca,'Box','on','PlotBoxAspectRatio',[1 1 1],'ZDir','reverse')
	
	hold on
	L1=plot(xlimiter,[0 0]);
	L2=plot([0 0],ylimiter);
	set([L1,L2],'Color',[0.7 0.7 0.7],'LineWidth',0.5,'LineStyle',':');
	hold off
	
	%here will be some commands to make it look nicer
	set(plotAxis,'LineWidth',2,'FontWeight','bold','FontSize',14);
	
	if LogSwitch
		%set x-axis to logarithmic scale
		%set(plotAxis,'XScale','log');
		
		
		
	end
	
end

function OutStruct = AllCombClusterAnalyser(Datastore,FilterList,CombID,CombAddOn,LowMagCut,NoListSend)
	%Goes through all the combined clusters and analyses them like AllClusterAnalyser
	%if NoListSend is true, the filterlist will not be send to the SingleClusterAnalyser
	%This will allows to only adress a certain magnitude of mainshocks but still adding 
	%every marked aftershock to it
	
	%It is becoming a bit a mess with all the different declustering ID, I think
	%I have to sort that out eventually.
	
	import mapseis.ClusterAnalizer.*;
	
	OutStruct=[];
	
	if nargin<6
		NoListSend=false;
	end
	
	if isempty(NoListSend)
		NoListSend=false;
	end
	
	
	
	if isempty(Datastore.ClusterNR)
		error('No declustering present in the catalog');
	end

	if isnan(all(Datastore.ClusterNR))
		error('No declustering present in the catalog');
	end

	
	%prepare data
	if ~isempty(FilterList)
		%get selected eq from the catalog
		Datastore.TypeUsed=[true;true;true];
		Datastore.ShowUnclassified=true;
		
		%update Filterlist to catalog
		FilterList.changeData(Datastore)
		FilterList.update;
		selected=FilterList.getSelected;
	
	else
		%just use everything 
		selected=true(Datastore.getRowCount,1);
	end
	
	
	%get all clusters
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	
	
	%get unique cluster numbers within the filterlist
	ClusterToCheck=CombAddOn.UsedComb;
	
	
	%init cell arrays and arrays
	FullTimeMain=[];
	FullDistMain=[];
	FullAzzelMain=[];
	
	FullTimeAfter=[];
	FullDistAfter=[];
	FullAzzelAfter=[];
	
	FullTimeAll=[];
	FullDistAll=[];
	FullAzzelAll=[];
	
	
	SingleTimeMain=cell(numel(ClusterToCheck),1);
	SingleDistMain=cell(numel(ClusterToCheck),1);
	SingleAzzelMain=cell(numel(ClusterToCheck),1);
	
	SingleTimeAfter=cell(numel(ClusterToCheck),1);
	SingleDistAfter=cell(numel(ClusterToCheck),1);
	SingleAzzelAfter=cell(numel(ClusterToCheck),1);
	
	SingleTimeAll=cell(numel(ClusterToCheck),1);
	SingleDistAll=cell(numel(ClusterToCheck),1);
	SingleAzzelAll=cell(numel(ClusterToCheck),1);
	
	
	SingleCentroid=cell(numel(ClusterToCheck),1);
	SingleMainShock=cell(numel(ClusterToCheck),1);
	SingleAfterShock=cell(numel(ClusterToCheck),1);
	SingleEveryShock=cell(numel(ClusterToCheck),1);
	
	if NoListSend
		TheFilter=[]
	else
		TheFilter=FilterList;
	end	
	
	
	parfor i=1:numel(ClusterToCheck)
		
		[StatVals SplittedCat]= SingleCombClusterAnalyser(Datastore,TheFilter,CombID,LowMagCut,ClusterToCheck(i),true)
		
			
		%split the package
		SingleTimeMain{i}=StatVals.TimeValMain; 
		SingleDistMain{i}=StatVals.DistValMain;
		SingleAzzelMain{i}=StatVals.AzValMain;
		
		SingleTimeAfter{i}=StatVals.TimeValAfter; 
		SingleDistAfter{i}=StatVals.DistValAfter; 
		SingleAzzelAfter{i}=StatVals.AzValAfter; 
		
		SingleTimeAll{i}=StatVals.TimeValAll; 
		SingleDistAll{i}=StatVals.DistValAll; 
		SingleAzzelAll{i}=StatVals.AzValAll; 
		
		SingleAfterShock{i}=SplittedCat.AfterShocks;
		SingleMainShock{i}=SplittedCat.MainShock;
		SingleEveryShock{i}=SplittedCat.EveryShock;
		SingleCentroid{i}=SplittedCat.Centroid;
		
			
		FullTimeMain=[FullTimeMain;SingleTimeMain{i}];
		FullDistMain=[FullDistMain;SingleDistMain{i}];
		FullAzzelMain=[FullAzzelMain;SingleAzzelMain{i}];
		
		FullTimeAfter=[FullTimeAfter;SingleTimeAfter{i}];
		FullDistAfter=[FullDistAfter;SingleDistAfter{i}];
		FullAzzelAfter=[FullAzzelAfter;SingleAzzelAfter{i}];
		
		FullTimeAll=[FullTimeAll;SingleTimeAll{i}];
		FullDistAll=[FullDistAll;SingleDistAll{i}];
		FullAzzelAll=[FullAzzelAll;SingleAzzelAll{i}];
		
		
		disp(['Cluster ',num2str(i),' from ' ,num2str(numel(ClusterToCheck))]);
			
	end
	

	%Pack everthing in structure
	OutStruct.AllTimesMain = FullTimeMain;
	OutStruct.AllDistancesMain = FullDistMain;
	OutStruct.AllAzimutesMain = FullAzzelMain;
	
	OutStruct.AllTimesAfter = FullTimeAfter;
	OutStruct.AllDistancesAfter = FullDistAfter;
	OutStruct.AllAzimutesAfter = FullAzzelAfter;
	
	OutStruct.AllTimesAll = FullTimeAll;
	OutStruct.AllDistancesAll = FullDistAll;
	OutStruct.AllAzimutesAll = FullAzzelAll;
	
	OutStruct.SingleTimesMain = SingleTimeMain;
	OutStruct.SingleDistancesMain = SingleDistMain; 
	OutStruct.SingleAzimutesMain = SingleAzzelMain;
	
	OutStruct.SingleTimesAfter = SingleTimeAfter;
	OutStruct.SingleDistancesAfter = SingleDistAfter; 
	OutStruct.SingleAzimutesAfter = SingleAzzelAfter;
	
	OutStruct.SingleTimesAll = SingleTimeAll;
	OutStruct.SingleDistancesAll = SingleDistAll; 
	OutStruct.SingleAzimutesAll = SingleAzzelAll;
	
	OutStruct.MainShocks = SingleMainShock;
	OutStruct.Aftershocks = SingleAfterShock;
	OutStruct.EveryShocks = SingleEveryShock;
	OutStruct.Centroids = SingleCentroid;
	
	OutStruct.AnalysedClusters = ClusterToCheck;
	
	
end

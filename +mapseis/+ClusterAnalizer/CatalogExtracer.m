function TheCats = CatalogExtracer(OutStruct,ClusterIDs,SaveIt,FilePrefix,FolderPath)
	%extraxts the specified Clusters into a zmap catalog outputs them and save
	%them if SaveIt is true
	
	for i=1:numel(ClusterIDs)
		
		%find it
		LogicSearch=OutStruct.AnalysedClusters==ClusterIDs(i);
		
		if sum(LogicSearch)>0
			%only take the first cluster
			Temp=find(LogicSearch);
			ThisIsIt=Temp(1);
			
			TheAfterShocks=OutStruct.Aftershocks{ThisIsIt};
			TheMainShock=OutStruct.MainShocks{ThisIsIt};
			RawCat=[TheMainShock;TheAfterShocks];
			
			%sort it with time
			[val idx]=sort(RawCat(:,3));
			SingleCat=RawCat(idx,:);
			
			TheCats{i}=SingleCat;
			
			if SaveIt
				Filename=[FilePrefix,'_ClusterCat_',num2str(ClusterIDs(i)),'.mat'];
				FullPath=[FolderPath,filesep,Filename];
				a=SingleCat;
				save(FullPath,'a');
			end
			
			
			
		end
		
	end


end

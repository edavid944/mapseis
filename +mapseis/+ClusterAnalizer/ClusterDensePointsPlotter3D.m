function handle=ClusterDensePointsPlotter3D(plotAxis,TimeVal,AzVal,DistVal,DepthVal,LogSwitch,AzSwitch)
	
	%plots the result of a cluster analysis into a density map with
	%Time (x), distance (y) and the depth distance (z)
	
	%if AzSwitch is set to true, the azimute (direction) will be used instead
	%of the distance.
	
	%if LogSwitch is used a logarithmic scale for the time is used
	
	import mapseis.plot.*;
	import mapseis.converter.*;
	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end

	
	if nargin<7
		AzSwitch=false;
	end
	
	
	if isempty(AzSwitch)
		AzSwitch=false;
	end	
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	

	
	%create plotdata
	if ~AzSwitch
		PlotData=[TimeVal,DistVal,DepthVal];
		xlimiter=[floor(min(TimeVal)) ceil(max(TimeVal))];
		ylimiter=[floor(min(DistVal)) ceil(max(DistVal))];
		zlimiter=[floor(min(DepthVal)) ceil(max(DepthVal))];
		
		PlotConfig	= struct(	'PlotType','DensePoint3D',...
						'Data',[PlotData],...
						'MarkerStylePreset','normal',...
						'MarkerSize',150,...
						'X_Axis_Label','Time (d)',...
						'Y_Axis_Label','Distance (km) ',...
						'Z_Axis_Label', 'Depth Distance (km) ',...
						'CustomColors','jet',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'Z_Axis_Limit',zlimiter,...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LogMode',true,...
						'LegendText','Time - Distance');
		
		set(gca,'Box','on')
		
	else
	
		PlotData=[TimeVal,AzVal,DepthVal];
		xlimiter=[floor(min(TimeVal)) ceil(max(TimeVal))];
		ylimiter=[floor(min(AzVal)) ceil(max(AzVal))];
		zlimiter=[floor(min(DepthVal)) ceil(max(DepthVal))];
		
		
		PlotConfig	= struct(	'PlotType','DensePoint',...
						'Data',[PlotData],...
						'MarkerStylePreset','normal',...
						'MarkerSize',150,...
						'X_Axis_Label','Time (d)',...
						'Y_Axis_Label','Azimute (deg) ',...
						'Z_Axis_Label', 'Depth Distance (km) ',...
						'CustomColors','jet',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'Z_Axis_Limit',zlimiter,...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LogMode',true,...
						'LegendText','Time - Azimute');
						
	
		set(gca,'Box','on')				
	end
	
	
	
	%plot it all
	[handle legendentry] = PlotDensityPoints3D(plotAxis,PlotConfig);
	
	
	%here will be some commands to make it look nicer
	set(plotAxis,'LineWidth',2,'FontWeight','bold','FontSize',14);
	
	if LogSwitch
		%set x-axis to logarithmic scale
		set(plotAxis,'XScale','log');
		
		
		
	end
	
end

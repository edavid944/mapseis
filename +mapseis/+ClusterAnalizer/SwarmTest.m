function [MagTest TimeTest MagRatio TimeRatio] = SwarmTest(Datastore,WhichClust,ScriptMode,TimeThres,MagThres)
	%This function classifies a cluster as swarm or not depended on the magnitude distribution 
	%and the interevent time distribution.
	%It currently works on the normal clustering Id a version for the UniqueID comes later
	%not much difference anyway
	
	import mapseis.projector.*;
	
	MagTest=NaN; 
	TimeTest=NaN; 
	MagRatio=NaN;  
	TimeRatio=NaN; 
	
	%get decluster data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	%selected=true(Datastore.getRowCount,1);
	selected=ClusterNR==WhichClust;
	
	if ~any(selected)
		disp('Cluster not found');
		return	
	end
	
	%get data from datastore
	if ~ScriptMode
		Datastore.TypeUsed=[true;true;true];
		Datastore.ShowUnclassified=true;
	end
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	%MagRatio
	MagRatio=max(ShortCat(:,5))-mean(ShortCat(:,5));
	MagTest=MagRatio<=MagThres;
	
	%TimeRatio
	TimeDiff=diff((ShortCat(:,4)));
	Duration=max(ShortCat(:,4))-min(ShortCat(:,4));
	TimeRatio=Duration/mean(TimeDiff);
	TimeTest=TimeRatio<=TimeThres;
	


end

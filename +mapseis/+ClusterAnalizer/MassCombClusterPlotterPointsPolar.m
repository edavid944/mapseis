function MassCombClusterPlotterPointsPolar(ResultStruct,LogSwitch,FilePrefix,FolderPath)
	%Plots every cluster in Density maps and saves them. Only cluster with 
	%more than 9 aftershocks are plotted
	
	import mapseis.ClusterAnalizer.*;
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	
	
	%save the old path
	oldFold=pwd;
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeValMain=ResultStruct.SingleTimesMain{i};
		AzValMain=ResultStruct.SingleAzimutesMain{i};
		DistValMain=ResultStruct.SingleDistancesMain{i};

		TimeValAfter=ResultStruct.SingleTimesAfter{i};
		AzValAfter=ResultStruct.SingleAzimutesAfter{i};
		DistValAfter=ResultStruct.SingleDistancesAfter{i};

		TimeValAll=ResultStruct.SingleTimesAll{i};
		AzValAll=ResultStruct.SingleAzimutesAll{i};
		DistValAll=ResultStruct.SingleDistancesAll{i};

		
		
		%check if it has to be printed
		if numel(TimeValMain)>=10
			
			%MainShocks
			%----------
			fig=figure;
			set(fig,'pos',[50 50 1200 600]);
			
			%Distance
			plotAxis=subplot(1,2,1)
			ClusterDensePointsPolarPlotter(plotAxis,TimeValMain,AzValMain,DistValMain,'Spatial',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%Azimute
			plotAxis=subplot(1,2,2)
			ClusterDensePointsPolarPlotter(plotAxis,TimeValMain,AzValMain,DistValMain,'AzTime',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_CombClusterNR_Dense_MainShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%AfterShocks
			%----------
			fig=figure;
			set(fig,'pos',[50 50 1200 600]);
			
			%Distance
			plotAxis=subplot(1,2,1)
			ClusterDensePointsPolarPlotter(plotAxis,TimeValAfter,AzValAfter,DistValAfter,'Spatial',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['AfterShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%Azimute
			plotAxis=subplot(1,2,2)
			ClusterDensePointsPolarPlotter(plotAxis,TimeValAfter,AzValAfter,DistValAfter,'AzTime',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_CombClusterNR_Dense_AfterShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			%EveryShock
			%----------
			fig=figure;
			set(fig,'pos',[50 50 1200 600]);
			
			%Distance
			plotAxis=subplot(1,2,1)
			ClusterDensePointsPolarPlotter(plotAxis,TimeValAll,AzValAll,DistValAll,'Spatial',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['All Eq. Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%Azimute
			plotAxis=subplot(1,2,2)
			ClusterDensePointsPolarPlotter(plotAxis,TimeValAll,AzValAll,DistValAll,'AzTime',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_CombClusterNR_Dense_EveryShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
		end
	end
	
	if isfield(ResultStruct,'AllTimesMain');
		%and the full plot
		
		
		
		%get data
		TimeValMain=ResultStruct.AllTimesMain;
		AzValMain=ResultStruct.AllAzimutesMain;
		DistValMain=ResultStruct.AllDistancesMain;
		
		TimeValAfter=ResultStruct.AllTimesAfter;
		AzValAfter=ResultStruct.AllAzimutesAfter;
		DistValAfter=ResultStruct.AllDistancesAfter;
		
		TimeValAll=ResultStruct.AllTimesAll;
		AzValAll=ResultStruct.AllAzimutesAll;
		DistValAll=ResultStruct.AllDistancesAll;
		
		
		%MainShock
		%---------
		fig=figure;
		set(fig,'pos',[50 50 1200 800]);
		%Distance
		plotAxis=subplot(1,2,1)
		ClusterDensePointsPolarPlotter(plotAxis,TimeValMain,AzValMain,DistValMain,'Spatial',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
				
		%Writing title over top subplot
		tit=title('All Combined Clusters: MainShocks  ');
		
		%Azimute
		plotAxis=subplot(1,2,2)
		ClusterDensePointsPolarPlotter(plotAxis,TimeValMain,AzValMain,DistValMain,'AzTime',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
		
				
		%save it
		%create filename
		filename=[FilePrefix,'_AllCombClusters_MainShock_Dense.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
				
		close(fig);
		
		
		
		%AfterShock
		%---------
		fig=figure;
		set(fig,'pos',[50 50 1200 800]);
		%Distance
		plotAxis=subplot(1,2,1)
		ClusterDensePointsPolarPlotter(plotAxis,TimeValAfter,AzValAfter,DistValAfter,'Spatial',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
				
		%Writing title over top subplot
		tit=title('All Combined Clusters: AfterShocks  ');
		
		%Azimute
		plotAxis=subplot(1,2,2)
		ClusterDensePointsPolarPlotter(plotAxis,TimeValAfter,AzValAfter,DistValAfter,'AzTime',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
		
				
		%save it
		%create filename
		filename=[FilePrefix,'_AllCombClusters_AfterShock_Dense.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
				
		close(fig);
		
		
		%EveryShock
		%---------
		fig=figure;
		set(fig,'pos',[50 50 1200 800]);
		%Distance
		plotAxis=subplot(1,2,1)
		ClusterDensePointsPolarPlotter(plotAxis,TimeValAll,AzValAll,DistValAll,'Spatial',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
				
		%Writing title over top subplot
		tit=title('All Combined Clusters: All Eq.  ');
		
		%Azimute
		plotAxis=subplot(1,2,2)
		ClusterDensePointsPolarPlotter(plotAxis,TimeValAll,AzValAll,DistValAll,'AzTime',LogSwitch);
		set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
		set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
		
				
		%save it
		%create filename
		filename=[FilePrefix,'_AllCombClusters_EveryShock_Dense.eps'];
		cd(FolderPath)
		saveas(fig,filename,'psc');
		cd(oldFold);
				
		close(fig);
		
		
	end
	
	
	
end

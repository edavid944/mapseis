function plotColorIDComb(CombID,CombAddOn,Datastore,OnlyMain)

	import mapseis.projector.*;
	import mapseis.util.*;
	
	%get data from datastore
	Datastore.TypeUsed=[true;true;true];
	Datastore.ShowUnclassified=true;
	selected=true(Datastore.getRowCount,1);
	
	ShortCat=getShortCatalog(Datastore,selected);
	
	%unpack from Ref. Frame
	UsedComb=CombAddOn.UsedComb;
	RefCenters=CombAddOn.RefCenters;
	

	
	
	%get decluster data
	[EventType ClusterNR]=Datastore.getDeclusterData;
	
	figure
	plotAxis=gca;
	hold on
	
	%Spatial points first
	for i=1:numel(UsedComb)
		UsedBy=strcmp(UsedComb{i},CombID);
		TheColor=[rand,rand,rand];
		
		%prepare plot data
		LonAfterShock=ShortCat(EventType==3&UsedBy,1);
		LatAfterShock=ShortCat(EventType==3&UsedBy,2);
		
		LonMainShock=ShortCat(EventType==2&UsedBy,1);
		LatMainShock=ShortCat(EventType==2&UsedBy,2);
		
		LonRef=RefCenters(i,1);
		LatRef=RefCenters(i,2);
		
		if any(UsedBy)
			if ~OnlyMain
				handAfter=plot(LonAfterShock,LatAfterShock);
				set(handAfter,'LineStyle','none','Color',TheColor,'Marker','.');
			end
			
			handMain=plot(LonMainShock,LatMainShock);
			set(handMain,'LineStyle','none','Color',TheColor,'Marker','x','MarkerSize',8);
			
		end
		
		handMain=plot(LonRef,LatRef);
		set(handMain,'LineStyle','none','Color',TheColor,'Marker','o','MarkerSize',10);
			
	end
	
end	

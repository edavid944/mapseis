function MassClusterPlotterPointsPolar3D(ResultStruct,LogSwitch,FilePrefix,FolderPath)
	%Plots every cluster in Density maps and saves them. Only cluster with 
	%more than 9 aftershocks are plotted
	%3D version, time azimute depth may not make sense (don't know yet)
	
	import mapseis.ClusterAnalizer.*;
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	
	
	%save the old path
	oldFold=pwd;
	
	%check if depth available, and add if not so
	if ~isfield(ResultStruct,'All3DDist')
		ResultStruct=Add_3D_distance(ResultStruct);
	end
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeVal=ResultStruct.SingleTimes{i};
		AzVal=ResultStruct.SingleAzimutes{i};
		DistVal=ResultStruct.SingleDistances{i};
		DepthVal=ResultStruct.SingleDepthDist{i};

		
		
		%check if it has to be printed
		if numel(TimeVal)>=10
			fig=figure;
			set(fig,'pos',[50 50 1200 600]);
			
			%Distance
			plotAxis=subplot(1,2,1)
			ClusterDensePointsPolarPlotter3D(plotAxis,TimeVal,AzVal,DistVal,DepthVal,'Spatial',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%Azimute
			plotAxis=subplot(1,2,2)
			ClusterDensePointsPolarPlotter3D(plotAxis,TimeVal,AzVal,DistVal,DepthVal,'AzTime',LogSwitch);
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			
			
			%save it
			%create filename
			filename=[FilePrefix,'_ClusterNR_DenseFull3D_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			
		end
	end
	
	
	%and the full plot
	fig=figure;
	set(fig,'pos',[50 50 1200 800]);
	
	%get data
	TimeVal=ResultStruct.AllTimes;
	AzVal=ResultStruct.AllAzimutes;
	DistVal=ResultStruct.AllDistances;
	DepthVal=ResultStruct.AllDepthDist;
	
	
	
	%Distance
	plotAxis=subplot(1,2,1)
	ClusterDensePointsPolarPlotter3D(plotAxis,TimeVal,AzVal,DistVal,DepthVal,'Spatial',LogSwitch);
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');		
			
	%Writing title over top subplot
	tit=title('All Clusters  ');
			
	%Azimute
	plotAxis=subplot(1,2,2)
	ClusterDensePointsPolarPlotter3D(plotAxis,TimeVal,AzVal,DistVal,DepthVal,'AzTime',LogSwitch);
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	set(tit,'FontSize',24,'FontWeight','bold');
	
			
	%save it
	%create filename
	filename=[FilePrefix,'_AllClusters_DenseFull3D.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
			
	close(fig);
	
	
	
	
end

function SwarmStruct = CatalogSwarmTest(Datastore,TimeThres,MagThres)
	%goes through every cluster and does the Swarm test on it
	%a bit slow, may could be improved in a final version, when I know how to 
	%detect swarms best
	import mapseis.ClusterAnalizer.*;
	import mapseis.projector.*;
	
	[EventType ClusterNR]=Datastore.getDeclusterData;
	ClusterToCheck=unique(ClusterNR(~isnan(ClusterNR)));
	
	
	for i=1:numel(ClusterToCheck)
		[MagTest(i) TimeTest(i) MagRatio(i) TimeRatio(i)] = SwarmTest(Datastore,ClusterToCheck(i),true,TimeThres,MagThres);
		selected=ClusterNR==ClusterToCheck(i);
		
		ShortCat=getShortCatalog(Datastore,selected&EventType==2);
		MainShockMag(i)=ShortCat(1,5);
		disp([num2str(i),' of ' num2str(numel(ClusterToCheck))])
	end
	
	
	
	%Packit
	SwarmStruct.ClusterToCheck=ClusterToCheck;
	SwarmStruct.MagTest=MagTest;
	SwarmStruct.TimeTest=TimeTest;
	SwarmStruct.MagRatio=MagRatio;
	SwarmStruct.TimeRatio=TimeRatio;
	SwarmStruct.MainShockMag=MainShockMag;
	
	
end

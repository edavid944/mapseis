function OutStruct = KillEmptyCluster(ResultsStruct)
	%kicks out all empty cluster
	
	import mapseis.util.*;
	
	%get all the fields of the structure and number of single clusters
	TheFields=fieldnames(ResultsStruct);
	NrCluster=numel(ResultsStruct.MainShocks);
	
	%use mainshock as estimate if it is an empty entry
	EmptyEntry=emptier(ResultsStruct.MainShocks);
	
	%loop over all
	for i=1:numel(TheFields)
		if numel(ResultsStruct.(TheFields{i}))==NrCluster
			OutStruct.(TheFields{i}) = ResultsStruct.(TheFields{i})(~EmptyEntry);
		else
			OutStruct.(TheFields{i}) = ResultsStruct.(TheFields{i});
		end
		
	
	end
	



end

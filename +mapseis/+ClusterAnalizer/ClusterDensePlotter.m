function ClusterDensePlotter(plotAxis,TimeVal,AzVal,DistVal,LogSwitch,GridSize,AzSwitch)
	
	%plots the result of a cluster analysis into a density map with
	%Time (x) and distance (y). 
	
	%if AzSwitch is set to true, the azimute (direction) will be used instead
	%of the distance.
	
	%if LogSwitch is used a logarithmic scale for the time is used
	
	import mapseis.plot.*;
	import mapseis.converter.*;
	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end

	if nargin<7
		AzSwitch=false;
	end	
	
	if nargin<6
		AzSwitch=false;
		GridSize=[1,1]
	end
	
	
	if isempty(AzSwitch)
		AzSwitch=false;
	end	
	
	if isempty(LogSwitch)
		LogSwitch=false;
	end	
		
	spacingPara=GridSize/2;
	
	
	%create a grid
	if ~LogSwitch
		TimeSpace=min(TimeVal):GridSize(1):max(TimeVal);
	else
		MinTime=min(TimeVal);
		MaxTime=max(TimeVal);
		
		if MinTime<0
			MinTime=-log10(-MinTime);
			
		elseif MinTime>0
			MinTime=log10(MinTime)
		end
		
		if MaxTime<0
			MaxTime=-log10(-MaxTime);
			
		elseif MaxTime>0
			MaxTime=log10(MaxTime);
		end
		
		disp(MinTime)
		disp(MaxTime)
		disp(GridSize(1))
		RawTime=MinTime:GridSize(1):MaxTime;
		isneg=RawTime<0;
		TimeAxis=abs(RawTime);
		TimeAxis(isneg)=-TimeAxis(isneg);
		
		TimeSpace=10.^(abs(RawTime));
		TimeSpace(isneg)=-TimeSpace(isneg);
		TimeSpace2=TimeSpace;
		TimeSpace2(end+1)=TimeSpace2(end)+0.1;
	end
	
	if ~AzSwitch
		DistSpace=min(DistVal):GridSize(2):max(DistVal);
	else
		AzSpace=min(AzVal):GridSize(2):max(AzVal);
	end
	
	
	%go through the elements
	if ~AzSwitch
		disp('Distance')
		
		DenseMatrix=zeros(numel(DistSpace),numel(TimeSpace));
		if ~LogSwitch
			for i=1:numel(TimeSpace)
				for j=1:numel(DistSpace)
					EventsFound=(TimeVal>=(TimeSpace(i)-spacingPara(1))&...
						TimeVal<(TimeSpace(i)+spacingPara(1)))&...
						(DistVal>=(DistSpace(j)-spacingPara(2))&...
						DistVal<(DistSpace(j)+spacingPara(2)));
						
					
					if any(EventsFound)	
						DenseMatrix(j,i)=sum(EventsFound);
					else
						DenseMatrix(j,i)=0;
					end	
			
				end
			end
		else
		
		
			for i=2:numel(TimeSpace)
				for j=1:numel(DistSpace)
					EventsFound=(TimeVal>=(TimeSpace2(i-1))&...
						TimeVal<(TimeSpace2(i)))&...
						(DistVal>=(DistSpace(j)-spacingPara(2))&...
						DistVal<(DistSpace(j)+spacingPara(2)));
						
					
					if any(EventsFound)	
						DenseMatrix(j,i-1)=sum(EventsFound);
					else
						DenseMatrix(j,i-1)=0;
					end	
			
				end
			end
		
		
		end
			
	else
		disp('Azimute')
		
		DenseMatrix=zeros(numel(AzSpace),numel(TimeSpace));
		if ~LogSwitch
		
			for i=1:numel(TimeSpace)
				for j=1:numel(AzSpace)
					EventsFound=(TimeVal>=(TimeSpace(i)-spacingPara(1))&...
						TimeVal<(TimeSpace(i)+spacingPara(1)))&...
						(AzVal>=(AzSpace(j)-spacingPara(2))&...
						AzVal<(AzSpace(j)+spacingPara(2)));
						
					
					if any(EventsFound)	
						DenseMatrix(j,i)=sum(EventsFound);
						
					else
						DenseMatrix(j,i)=0;
					end	
			
				end
			end
		else
			
			for i=2:numel(TimeSpace)
				for j=1:numel(AzSpace)
					EventsFound=(TimeVal>=(TimeSpace2(i-1))&...
						TimeVal<(TimeSpace2(i)))&...
						(AzVal>=(AzSpace(j)-spacingPara(2))&...
						AzVal<(AzSpace(j)+spacingPara(2)));
						
					
					if any(EventsFound)	
						DenseMatrix(j,i-1)=sum(EventsFound);
					else
						DenseMatrix(j,i-1)=0;
					end	
			
				end
			end
		
		
		end
	end
	
	
	
	%set transparent when 0 
	AlphaData=double(~(DenseMatrix==0));
	
	%norm it 
	DenseMatrix=DenseMatrix/sum(sum(DenseMatrix));
	DenseMatrix(~(DenseMatrix==0))=log10(DenseMatrix(~(DenseMatrix==0)));
	
	%create plotdata
	if ~AzSwitch
		if ~LogSwitch
			PlotData={TimeSpace,DistSpace,DenseMatrix};
			xlimiter=[floor(min(TimeSpace)-spacingPara(1)) ceil(max(TimeSpace)+spacingPara(1))];
		else
			PlotData={TimeAxis,DistSpace,DenseMatrix};
			
			xlimiter=([floor(min(TimeAxis)-spacingPara(1)) ceil(max(TimeAxis)+spacingPara(1))]);
		end
		
		
		ylimiter=[floor(min(DistSpace)-spacingPara(2)) ceil(max(DistSpace)+spacingPara(2))];
		
		PlotConfig	= struct(	'PlotType','ColorPlot',...
						'Data',{PlotData},...
						'MapStyle','smooth',...
						'AlphaData',AlphaData,...
						'X_Axis_Label','Time (d)',...
						'Y_Axis_Label','Distance (km) ',...
						'Colors','jet',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LegendText','Time - Distance');
		
		set(gca,'Box','on')
		
	else
		if ~LogSwitch
			PlotData={TimeSpace,AzSpace,DenseMatrix};
			xlimiter=[floor(min(TimeSpace)-spacingPara(1)) ceil(max(TimeSpace)+spacingPara(1))];
		else
			PlotData={TimeAxis,AzSpace,DenseMatrix};
			xlimiter=([floor(min(TimeAxis)-spacingPara(1)) ceil(max(TimeAxis)+spacingPara(1))]);
		end	
		
		ylimiter=[floor(min(AzSpace)-spacingPara(2)) ceil(max(AzSpace)+spacingPara(2))];
		
		PlotConfig	= struct(	'PlotType','ColorPlot',...
						'Data',{PlotData},...
						'MapStyle','smooth',...
						'AlphaData',AlphaData,...
						'X_Axis_Label','Time (d)',...
						'Y_Axis_Label','Azimute (deg) ',...
						'Colors','jet',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'C_Axis_Limit','auto',...
						'ColorToggle',true,...
						'LegendText','Time - Azimute');
		set(gca,'Box','on')				
	end
	
	
	
	%plot it all
	[handle legendentry] = PlotColor(plotAxis,PlotConfig);
	
	
	%here will be some commands to make it look nicer
	set(plotAxis,'LineWidth',2,'FontWeight','bold','FontSize',14);
	
	if LogSwitch
		%set x-axis to logarithmic scale
		%set(plotAxis,'XScale','log');
		XTicker=get(plotAxis,'XTick')
		%set(plotAxis,'XTickLabel',num2str(10.^XTicker));
		
		
	end
	
end

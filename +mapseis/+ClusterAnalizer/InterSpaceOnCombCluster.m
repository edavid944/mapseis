function SpaceMan = InterSpaceOnCombCluster(Datastore,CombID,Spacings,MagCons,FilePrefix,FolderPath)
	%runs InterEventSpace on all of the cluster combination, saves the results and plot it
	
	import mapseis.util.*;
	import mapseis.projector.*;
	import mapseis.ClusterAnalizer.*;
	
	
	%save the old path
	oldFold=pwd;
	
	%get the unique ID combination
	IsUsed=~emptier(CombID);
	UsedComb=unique(CombID(IsUsed));
	
	DistSpacer=Spacings(1);
	TimeSpacer=Spacings(2);
	
	
	for i=1:numel(UsedComb)
		
		%search for the cluster
		selected=strcmp(CombID,UsedComb{i});
		disp(sum(selected));
		disp(size(selected));
		if sum(selected)>=10 
			ShortCat=getShortCatalog(Datastore,selected);	
		
		
			MaxTimeDist=abs(max(ShortCat(:,4))-min(ShortCat(:,4)));
			
			if MaxTimeDist<=100;
				MaxTimeDist=100;
			end
			
			TimeCons=[0, MaxTimeDist, TimeSpacer]
			DistCons=[0, 250, DistSpacer];
			
			
			disp(TimeCons);
			disp(DistCons);
			OutStruct = InterEventSpace(ShortCat,DistCons,TimeCons,MagCons,true);
		
			InterSpace{i,1}=OutStruct;
			
			fig=figure;
			set(fig,'pos',[50 50 1200 400]);
			plotAxis=gca;
			
			
			hh=image(OutStruct.TimeVec,OutStruct.DistVec,log(OutStruct.AllMag'));
			set(hh,'CDataMapping','scaled');
			set(plotAxis,'YDir','normal','LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			tit=title(plotAxis,['All Distance & Time: Comb.Cluster Nr. ',UsedComb{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%save it
			%create filename
			filename=[FilePrefix,'InterSpaceDist_CombClusterNR_',UsedComb{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
				
			close(fig);
			
		else
			InterSpace{i,1}=[];
		end	
	end


	SpaceMan.UsedCombID=UsedComb;
	SpaceMan.InterSpace=InterSpace;
end

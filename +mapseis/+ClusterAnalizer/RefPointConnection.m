function [CombID,CombAddOn] = RefPointConnection(ClusterAddData,RefFrame,PermuteComb,UseTime)
	%goes through all the ids (additional data) an searches for the unique combination
	%of stations used
	%If PermuteComb is set to true, the code will ignore the order of the stations, 
	%means _A _B _C will be equal to _C _A _B
	%If UseTime is true, the code will use the Time points as additional points, and 
	%searching also for combination with this 
	
	%There probably ways to make this faster, but it is just a prototype anyway
	
	import mapseis.ClusterAnalizer.*;
	import mapseis.util.*;
	
	%unpack from Ref. Frame
	SpaceFrame=RefFrame.SpaceFrame;
	SpaceLabel=RefFrame.SpaceLabel;
	TimeFrame=RefFrame.TimeFrame;
	TimeLabel=RefFrame.TimeLabel;
	
	%unpack additional data
	NearSpacePoint=ClusterAddData.NearSpacePoint;
	DistSpacePoint=ClusterAddData.DistSpacePoint;
	NearTimePoint=ClusterAddData.NearTimePoint;
	DistTimePoint=ClusterAddData.DistTimePoint;
	MagMain=ClusterAddData.MagMain;
	
	
	CombID=cell(length(NearSpacePoint),1);
	IsUsed=false(length(NearSpacePoint),1);
	
	if PermuteComb
		if UseTime
			for i=1:length(NearSpacePoint)
				if isempty(NearSpacePoint{i,1})
					CombID{i,1}=[];
					
				else
					SortPoint=sort(NearSpacePoint(i,:));
					SortPoint(end+1)=NearTimePoint(i);
					CombID(i,1)={[SortPoint{:}]};
					IsUsed(i)=true;
				end
			end
		else
			for i=1:length(NearSpacePoint)
				if isempty(NearSpacePoint{i,1})
					PointComb{i,1}=[];
					
				else
					SortPoint=sort(NearSpacePoint(i,:));
					CombID(i,1)={[SortPoint{:}]};
					IsUsed(i)=true;
				end
			end
			
		end
	
	else
		if UseTime
			for i=1:length(NearSpacePoint)
				if isempty(NearSpacePoint{i,1})
					CombID{i,1}=[];
					
				else
					SortPoint=(NearSpacePoint(i,:));
					SortPoint(end+1)=NearTimePoint(i);
					CombID(i,1)={[SortPoint{:}]};
					IsUsed(i)=true;
				end
			end
		else
			for i=1:length(NearSpacePoint)
				if isempty(NearSpacePoint{i,1})
					CombID{i,1}=[];
					
				else
					SortPoint=(NearSpacePoint(i,:));
					CombID(i,1)={[SortPoint{:}]};
					IsUsed(i)=true;
				end
			end
			
		end
		
	
	
	end
	
	
	UsedComb=unique(CombID(IsUsed));
	
	if UseTime
		TriRef=[];
		TriTime=[];
		
		for i=1:numel(UsedComb)
			%How many time used in total
			Busted=strcmp(UsedComb{i},CombID);
			NrUses(i)=sum(Busted);
			
			%build spatial midpoints (only works with 3 points)
			Ref1Label=UsedComb{i}(1:2);
			Ref2Label=UsedComb{i}(3:4);
			Ref3Label=UsedComb{i}(5:6);
			RefTimeLab=UsedComb{i}(7:8);
			
			CoordRef1=SpaceFrame(strcmp(Ref1Label,SpaceLabel),:);
			CoordRef2=SpaceFrame(strcmp(Ref2Label,SpaceLabel),:);
			CoordRef3=SpaceFrame(strcmp(Ref3Label,SpaceLabel),:);
			TimeCoord=TimeFrame(strcmp(RefTimeLab,TimeLabel),:);
			
			RefCenters(i,1)=mean([CoordRef1(1),CoordRef2(1),CoordRef3(1)]);
			RefCenters(i,2)=mean([CoordRef1(2),CoordRef2(2),CoordRef3(2)]);
			TimeCenter(i,:)=TimeCoord;
			
			
			TempTri=[CoordRef1;CoordRef2;CoordRef3;CoordRef1;NaN,NaN];
			TriRef=[TriRef;TempTri];
			
			TempTime=[TimeCoord;TimeCoord;TimeCoord;TimeCoord;NaN,NaN];
			TriTime=[TempTime;TriTime];

		end
	
		%Pack Addon
		CombAddOn.UsedComb=UsedComb;
		CombAddOn.NrUses=NrUses;
		CombAddOn.RefCenters=RefCenters;
		CombAddOn.TimeCenter=TimeCenter;
		CombAddOn.TriRef=TriRef;
		CombAddOn.TriTime=TriTime;
		
	
	else
		TriRef=[];
	
		for i=1:numel(UsedComb)
			%How many time used in total
			Busted=strcmp(UsedComb{i},CombID);
			NrUses(i)=sum(Busted);
			
			%build spatial midpoints (only works with 3 points)
			Ref1Label=UsedComb{i}(1:2);
			Ref2Label=UsedComb{i}(3:4);
			Ref3Label=UsedComb{i}(5:6);
			
			CoordRef1=SpaceFrame(strcmp(Ref1Label,SpaceLabel),:);
			CoordRef2=SpaceFrame(strcmp(Ref2Label,SpaceLabel),:);
			CoordRef3=SpaceFrame(strcmp(Ref3Label,SpaceLabel),:);
			
			RefCenters(i,1)=mean([CoordRef1(1),CoordRef2(1),CoordRef3(1)]);
			RefCenters(i,2)=mean([CoordRef1(2),CoordRef2(2),CoordRef3(2)]);
			
			TempTri=[CoordRef1;CoordRef2;CoordRef3;CoordRef1;NaN,NaN];
			TriRef=[TriRef;TempTri];
			
		end
		
		
		%Pack Addon
		CombAddOn.UsedComb=UsedComb;
		CombAddOn.NrUses=NrUses;
		CombAddOn.RefCenters=RefCenters;
		CombAddOn.TriRef=TriRef;
		
		
	
	end
	
	
	
	
end

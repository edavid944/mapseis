function AllHistoryCor = BinAreaCorrection(AllHistory,PrintFile,FilePrefix,FolderPath)
	%Plots every cluster in a histogram and also outputs them into a file
	
	import mapseis.ClusterAnalizer.*;
	

	
	%save the old path
	oldFold=pwd;
	
	SingleHistDistCorr={};
	SummaryHistDistCorr=[];

	
	%go ahead and plot
	for i=1:numel(AllHistory.SingleHistDist)
	
		%get data
		Bins=AllHistory.SingleHistDist{i}(:,1);
		Val=AllHistory.SingleHistDist{i}(:,2);
		
		temp=diff(Bins);
		BinSize=temp(1);
		
		%calculate area of rings
		SmallR=Bins-BinSize;
		BigR=Bins+BinSize;
		RingThing=pi*(BigR.^2-SmallR.^2);
		
		%normalize data;
		CorrVal=Val./RingThing;
		
		%store data
		SingleHistDistCorr{i}=[Bins,CorrVal];
		
		%print it if wanted
		if PrintFile
			fig=figure;
			set(fig,'pos',[50 50 800 600]);

			
			plotAxis=gca
			bar(Bins,CorrVal);
		
			xlabel('Distance/area (1/km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
		
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor',[0.8 0.3 0.1],'EdgeColor','w')
		
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(AllHistory.ClusterIDs(i)),...
					', Mainshock Mag: ',num2str(AllHistory.MainshockMags(i))]);
			set(tit,'FontSize',24,'FontWeight','bold');
		

		
		
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_DistanceCorr_ClusterNR_',num2str(AllHistory.ClusterIDs(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);	
		end
		
	end
	
	
	
	
	
	if isfield(AllHistory,'SummaryHistDist');
		%Now for the summary
		%get data
		Bins=AllHistory.SummaryHistDist(:,1);
		Val=AllHistory.SummaryHistDist(:,2);
			
		temp=diff(Bins);
		BinSize=temp(1);
			
		%calculate area of rings
		SmallR=Bins-BinSize;
		BigR=Bins+BinSize;
		RingThing=pi*(BigR.^2-SmallR.^2);
			
		%normalize data;
		CorrVal=Val./RingThing;
		
		%store data
		SummaryHistDistCorr=[Bins,CorrVal];
		
		
		
		%print it if wanted
		if PrintFile
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			
			plotAxis=gca
			bar(Bins,CorrVal);
			
			xlabel('Distance/area (1/km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor',[0.8 0.3 0.1],'EdgeColor','w')
			
			%Writing title over top subplot
			tit=title(plotAxis,'All Clusters: Distance ');
			
			set(tit,'FontSize',24,'FontWeight','bold');
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_DistanceCorr_AllCluster.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
		
		end
	end

	
	
	
	
	%Pack it all together
	AllHistoryCor=AllHistory;
	AllHistoryCor.SingleHistDistCorr=SingleHistDistCorr;
	
	if isfield(AllHistory,'SummaryHistDist');
		AllHistoryCor.SummaryHistDistCorr=SummaryHistDistCorr;
	end
	

	
	
	
	
	
	
end

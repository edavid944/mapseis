function AllHistory = MassClusterBinMan(ResultStruct,NumOfBin,FilePrefix,FolderPath)
	%Plots every cluster in a histogram and also outputs them into a file
	
	import mapseis.ClusterAnalizer.*;
	

	
	%save the old path
	oldFold=pwd;
	
	ClusterID=[];
	MainMags=[];
	TimeMain=[];
	DistHist={};
	AzHist={};
	TimeHist={};
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeVal=ResultStruct.SingleTimes{i};
		AzVal=ResultStruct.SingleAzimutes{i};
		DistVal=ResultStruct.SingleDistances{i};
		
		
		if numel(TimeVal)>=10
		
			%Distance
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			
			%save keydata
			ClusterID(end+1)=ResultStruct.AnalysedClusters(i);
			MainMags(end+1)=ResultStruct.MainShocks{i}(6);
			TimeMain(end+1)=ResultStruct.MainShocks{i}(3);
			
			plotAxis=gca
			[Val Bins]=hist(DistVal,NumOfBin);
			hist(DistVal,NumOfBin);
			xlabel('Distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','r','EdgeColor','w')
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%save histogram
			DistHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Distance_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Azimute
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(AzVal,NumOfBin);
			hist(AzVal,NumOfBin);
			xlabel('Azimute (deg)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','g','EdgeColor','w')
			
			%save histogram
			AzHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Azimute_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Time
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(TimeVal,NumOfBin);
			hist(TimeVal,NumOfBin);
			xlabel('Time (d)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['Cluster Nr.  ',num2str(ResultStruct.AnalysedClusters(i)),...
					', Mainshock Mag: ',num2str(ResultStruct.MainShocks{i}(6))]);
			set(tit,'FontSize',24,'FontWeight','bold');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','b','EdgeColor','w')
			
			%save histogram
			TimeHist{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Time_ClusterNR_',num2str(ResultStruct.AnalysedClusters(i)),'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
		end
	end
	
	
	
	
	
	
	%Now for the summary
	%get data
	TimeVal=ResultStruct.AllTimes;
	AzVal=ResultStruct.AllAzimutes;
	DistVal=ResultStruct.AllDistances;
	
	
	%Distance
	fig=figure;
	set(fig,'pos',[50 50 800 600]);
	
	plotAxis=gca
	[Val Bins]=hist(DistVal,NumOfBin);
	hist(DistVal,NumOfBin);
	xlabel('Distance (km)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','r','EdgeColor','w')
	
	%Writing title over top subplot
	tit=title(plotAxis,'All Clusters: Distance ');
	
	set(tit,'FontSize',24,'FontWeight','bold');
	
	%save histogram
	SumHistDist=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Distance_AllCluster.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Azimute
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(AzVal,NumOfBin);
	hist(AzVal,NumOfBin);
	xlabel('Azimute (deg)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title('All Clusters: Azimute ');
	
	set(tit,'FontSize',24,'FontWeight','bold');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','g','EdgeColor','w')
	
	%save histogram
	SumHistAz=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Azimute_AllCluster.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Time
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(TimeVal,NumOfBin);
	hist(TimeVal,NumOfBin);
	xlabel('Time (d)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title(plotAxis,'All Clusters:Time ');
	
	set(tit,'FontSize',24,'FontWeight','bold');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','b','EdgeColor','w')
	
	%save histogram
	SumHistTime=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Time_AllCluster.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	
	
	
	%Pack it all together
	AllHistory.ClusterIDs=ClusterID;
	AllHistory.MainshockMags=MainMags;
	AllHistory.TimeAtMainshock=TimeMain;
	AllHistory.SingleHistDist=DistHist;
	AllHistory.SingleHistAz=AzHist;
	AllHistory.SingleHistTime=TimeHist;
	AllHistory.SummaryHistDist=SumHistDist;
	AllHistory.SummaryHistAz=SumHistAz;
	AllHistory.SummaryHistTime=SumHistTime;
	

	
	
	
	
	
	
end

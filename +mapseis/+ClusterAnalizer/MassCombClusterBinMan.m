function AllHistory = MassCombClusterBinMan(ResultStruct,NumOfBin,FilePrefix,FolderPath)
	%Plots every cluster in a histogram and also outputs them into a file
	
	import mapseis.ClusterAnalizer.*;
	

	
	%save the old path
	oldFold=pwd;
	
	ClusterID={};
	
	DistHistMain={};
	AzHistMain={};
	TimeHistMain={};
	
	DistHistAfter={};
	AzHistAfter={};
	TimeHistAfter={};
	
	DistHistAll={};
	AzHistAll={};
	TimeHistAll={};
	
	
	%go ahead and plot
	for i=1:numel(ResultStruct.AnalysedClusters)
	
		%get data
		TimeValMain=ResultStruct.SingleTimesMain{i};
		AzValMain=ResultStruct.SingleAzimutesMain{i};
		DistValMain=ResultStruct.SingleDistancesMain{i};

		TimeValAfter=ResultStruct.SingleTimesAfter{i};
		AzValAfter=ResultStruct.SingleAzimutesAfter{i};
		DistValAfter=ResultStruct.SingleDistancesAfter{i};

		TimeValAll=ResultStruct.SingleTimesAll{i};
		AzValAll=ResultStruct.SingleAzimutesAll{i};
		DistValAll=ResultStruct.SingleDistancesAll{i};
		
		
		if numel(TimeValMain)>=10
			
		
			%MainShocks
			%----------
			
			%Distance
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			
			%save keydata
			ClusterID(end+1)=ResultStruct.AnalysedClusters(i);
		
			
			plotAxis=gca
			[Val Bins]=hist(DistValMain,NumOfBin);
			hist(DistValMain,NumOfBin);
			xlabel('Distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','r','EdgeColor','w')
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			
			%save histogram
			DistHistMain{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Distance_CombClusterNR_MainShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Azimute
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(AzValMain,NumOfBin);
			hist(AzValMain,NumOfBin);
			xlabel('Azimute (deg)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','g','EdgeColor','w')
			
			%save histogram
			AzHistMain{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Azimute_CombClusterNR_MainShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Time
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(TimeValMain,NumOfBin);
			hist(TimeValMain,NumOfBin);
			xlabel('Time (d)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','b','EdgeColor','w')
			
			%save histogram
			TimeHistMain{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Time_CombClusterNR_MainShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%AfterShocks
			%----------
			
			%Distance
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			
			%save keydata
			ClusterID(end+1)=ResultStruct.AnalysedClusters(i);
		
			
			plotAxis=gca
			[Val Bins]=hist(DistValAfter,NumOfBin);
			hist(DistValAfter,NumOfBin);
			xlabel('Distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','r','EdgeColor','w')
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			
			%save histogram
			DistHistAfter{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Distance_CombClusterNR_AfterShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Azimute
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(AzValAfter,NumOfBin);
			hist(AzValAfter,NumOfBin);
			xlabel('Azimute (deg)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','g','EdgeColor','w')
			
			%save histogram
			AzHistAfter{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Azimute_CombClusterNR_AfterShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Time
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(TimeValAfter,NumOfBin);
			hist(TimeValAfter,NumOfBin);
			xlabel('Time (d)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','b','EdgeColor','w')
			
			%save histogram
			TimeHistAfter{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Time_CombClusterNR_AfterShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			
			%EveryShocks
			%----------
			
			%Distance
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
			
			%save keydata
			ClusterID(end+1)=ResultStruct.AnalysedClusters(i);
		
			
			plotAxis=gca
			[Val Bins]=hist(DistValAll,NumOfBin);
			hist(DistValAll,NumOfBin);
			xlabel('Distance (km)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','r','EdgeColor','w')
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			
			%save histogram
			DistHistAll{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Distance_CombClusterNR_EveryShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Azimute
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(AzValAll,NumOfBin);
			hist(AzValAll,NumOfBin);
			xlabel('Azimute (deg)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','g','EdgeColor','w')
			
			%save histogram
			AzHistAll{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Azimute_CombClusterNR_EveryShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
			
			%Time
			fig=figure;
			set(fig,'pos',[50 50 800 600]);
	
			plotAxis=gca
			[Val Bins]=hist(TimeValAll,NumOfBin);
			hist(TimeValAll,NumOfBin);
			xlabel('Time (d)  ');
			ylabel('Nr. Earthquake  ');
			set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
			
			%Writing title over top subplot
			tit=title(plotAxis,['MainShocks Comb. Cluster Nr.  ',ResultStruct.AnalysedClusters{i}]);
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
			
			%color it
			h = findobj(gca,'Type','patch');
			set(h,'FaceColor','b','EdgeColor','w')
			
			%save histogram
			TimeHistAll{end+1}=[Bins',Val'];
			
			
			%save picture
			%create filename
			filename=[FilePrefix,'_Histogram_Time_CombClusterNR_EveryShocks_',ResultStruct.AnalysedClusters{i},'.eps'];
			cd(FolderPath)
			saveas(fig,filename,'psc');
			cd(oldFold);
			
			close(fig);
			
			
		end
	end
	
	
	
	
	
	
	%Now for the summary
	%get data
	TimeValMain=ResultStruct.AllTimesMain;
	AzValMain=ResultStruct.AllAzimutesMain;
	DistValMain=ResultStruct.AllDistancesMain;
		
	TimeValAfter=ResultStruct.AllTimesAfter;
	AzValAfter=ResultStruct.AllAzimutesAfter;
	DistValAfter=ResultStruct.AllDistancesAfter;
		
	TimeValAll=ResultStruct.AllTimesAll;
	AzValAll=ResultStruct.AllAzimutesAll;
	DistValAll=ResultStruct.AllDistancesAll;
	
	
	%MainShocks
	%----------
	
	%Distance
	fig=figure;
	set(fig,'pos',[50 50 800 600]);
	
	plotAxis=gca
	[Val Bins]=hist(DistValMain,NumOfBin);
	hist(DistValMain,NumOfBin);
	xlabel('Distance (km)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','r','EdgeColor','w')
	
	%Writing title over top subplot
	tit=title('All Combined Clusters MainShocks: Distance  ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%save histogram
	SumHistDistMain=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Distance_AllCombCluster_MainShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Azimute
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(AzValMain,NumOfBin);
	hist(AzValMain,NumOfBin);
	xlabel('Azimute (deg)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title('All Combined Clusters MainShocks: Azimute ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','g','EdgeColor','w')
	
	%save histogram
	SumHistAzMain=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Azimute_AllCluster_MainShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Time
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(TimeValMain,NumOfBin);
	hist(TimeValMain,NumOfBin);
	xlabel('Time (d)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title(plotAxis,'All Combined Clusters MainShock: Time ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','b','EdgeColor','w')
	
	%save histogram
	SumHistTimeMain=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Time_AllCluster_MainShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	





	%AfterShocks
	%----------
	
	%Distance
	fig=figure;
	set(fig,'pos',[50 50 800 600]);
	
	plotAxis=gca
	[Val Bins]=hist(DistValAfter,NumOfBin);
	hist(DistValAfter,NumOfBin);
	xlabel('Distance (km)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','r','EdgeColor','w')
	
	%Writing title over top subplot
	tit=title('All Combined Clusters AfterShocks: Distance  ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%save histogram
	SumHistDistAfter=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Distance_AllCombCluster_AfterShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Azimute
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(AzValAfter,NumOfBin);
	hist(AzValAfter,NumOfBin);
	xlabel('Azimute (deg)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title('All Combined Clusters AfterShocks: Azimute ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','g','EdgeColor','w')
	
	%save histogram
	SumHistAzAfter=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Azimute_AllCluster_AfterShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Time
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(TimeValAfter,NumOfBin);
	hist(TimeValAfter,NumOfBin);
	xlabel('Time (d)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title(plotAxis,'All Combined Clusters AfterShock: Time ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','b','EdgeColor','w')
	
	%save histogram
	SumHistTimeAfter=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Time_AllCluster_AfterShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);	
	
	

	%EveryShocks
	%----------
	
	%Distance
	fig=figure;
	set(fig,'pos',[50 50 800 600]);
	
	plotAxis=gca
	[Val Bins]=hist(DistValAll,NumOfBin);
	hist(DistValAll,NumOfBin);
	xlabel('Distance (km)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','r','EdgeColor','w')
	
	%Writing title over top subplot
	tit=title('All Combined Clusters EveryShocks: Distance  ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%save histogram
	SumHistDistAll=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Distance_AllCombCluster_EveryShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Azimute
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(AzValAll,NumOfBin);
	hist(AzValAll,NumOfBin);
	xlabel('Azimute (deg)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title('All Combined Clusters EveryShocks: Azimute ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','g','EdgeColor','w')
	
	%save histogram
	SumHistAzAll=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Azimute_AllCluster_EveryShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);
	
	
	
	%Time
	fig=figure;
	set(fig,'pos',[50 50 800 600]);

	plotAxis=gca
	[Val Bins]=hist(TimeValAll,NumOfBin);
	hist(TimeValAll,NumOfBin);
	xlabel('Time (d)  ');
	ylabel('Nr. Earthquake  ');
	set(plotAxis,'LineWidth',3,'FontWeight','bold','FontSize',14,'Box','on');
	
	%Writing title over top subplot
	tit=title(plotAxis,'All Combined Clusters EveryShock:Time ');
	
	set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
	
	%color it
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor','b','EdgeColor','w')
	
	%save histogram
	SumHistTimeAll=[Bins',Val'];
	
	
	%save picture
	%create filename
	filename=[FilePrefix,'_Histogram_Time_AllCluster_EveryShocks.eps'];
	cd(FolderPath)
	saveas(fig,filename,'psc');
	cd(oldFold);
	
	close(fig);	
	
	
	
	%Pack it all together
	AllHistory.ClusterIDs=ClusterID;
	
	AllHistory.SingleHistDistMain=DistHistMain;
	AllHistory.SingleHistAzMain=AzHistMain;
	AllHistory.SingleHistTimeMain=TimeHistMain;
	AllHistory.SummaryHistDistMain=SumHistDistMain;
	AllHistory.SummaryHistAzMain=SumHistAzMain;
	AllHistory.SummaryHistTimeMain=SumHistTimeMain;
	
	AllHistory.SingleHistDistAfter=DistHistAfter;
	AllHistory.SingleHistAzAfter=AzHistAfter;
	AllHistory.SingleHistTimeAfter=TimeHistAfter;
	AllHistory.SummaryHistDistAfter=SumHistDistAfter;
	AllHistory.SummaryHistAzAfter=SumHistAzAfter;
	AllHistory.SummaryHistTimeAfter=SumHistTimeAfter;
	
	AllHistory.SingleHistDistAll=DistHistAll;
	AllHistory.SingleHistAzAll=AzHistAll;
	AllHistory.SingleHistTimeAll=TimeHistAll;
	AllHistory.SummaryHistDistAll=SumHistDistAll;
	AllHistory.SummaryHistAzAll=SumHistAzAll;
	AllHistory.SummaryHistTimeAll=SumHistTimeAll;

	
	
	
	
	
	
end

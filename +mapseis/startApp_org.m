function [dataStore,guiWindowStruct,ListProxy] = startApp(fname)
% startApp : start the MapSeis GUI and return a handle to the data
%

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

import mapseis.datastore.*;
import mapseis.importfilter.*;
import mapseis.gui.*;
import mapseis.projector.*;
import mapseis.calc.*;
import mapseis.listenerproxy.*;


%create the Proxylistener (needed first)
ListProxy = addproxylistener;


% If no filename specified then load some default data
if nargin<1
     fname = ['Import',filesep,'scec_SearchResults.txt'];
%    fname = 'scec_SearchResults.txt';
end

% Have filters been loaded from input file?
filtersLoaded = false;

% Create the underlying data store
[pathStr,nameStr,extStr] = fileparts(fname); %#ok<NASGU>
if strcmpi(extStr,'.mat')
    % if the input filename is a .mat file then assume it is a saved event
    % data structure, including the filters
    prevData = load(fname);
    if isfield(prevData,'dataStore')
        % We have loaded a saved session, including filter values
        dataStore = prevData.dataStore;
        try
        	filterList = prevData.filterList;
        	filtersLoaded = true;
        
        	%add the new ListProxy
        	filterList.ListProxy = ListProxy;
        catch
        	filtersLoaded = false;
        end
        
        try %check wether the datastore is already buffered
                	temp=dataStore.getUserData('DecYear');
        catch
                		dataStore.bufferDate;
        end

        
    elseif isfield(prevData,'dataCols')
        % We have loaded only the data columns, which now need to be turned
        % into an mapseis.datastore.DataStore
        dataStore = DataStore(prevData.dataCols);
    elseif isfield(prevData,'a')
        % We have loaded in a ZMAP format file with data in matraix 'a'
        dataStore = DataStore(importZMAPCatRead(prevData.a));
    else
        error('startApp:unknown_mat_file','Unknown data in .mat file');
    end
else
    % otherwise the file needs to be imported with the appropriate filter
    % TO DO: add extra argument to startApp to specify input data
    % source, for the moment use SCEC as default
    dataStore = DataStore(importSCECCatRead(fname,'-file'));
end




%% Set the UserData used by the GUIs
setDefaultUserData(dataStore);
dataStore.SetPlotQuality;


%write filename and path to userdata
dataStore.setUserData('filename',nameStr);
dataStore.setUserData('filepath',fname);

%% Create the default FilterList
if ~filtersLoaded
    filterList = createDefaultFilters(dataStore,ListProxy);
    filterList

end

%if isempty(filterList.ListProxy)
%			filterList.ListProxy=ListProxy;
%end			

%% Create the GUI

% Create the main GUI window showing event locations, magnitudes and times
guiWindowStruct.mainStruct=GUI(dataStore,filterList,ListProxy);

%addlistener(filterList,'Update',...
%    @(s,e) guiWindowStruct.mainStruct.updateGUI());

% Create the GUI for setting filter parameters and other analysis
% parameters
guiWindowStruct.mainParamStruct=ParamGUI(dataStore,filterList,ListProxy); % ,filterList);

%link the parameter window to the interal property of the main gui
guiWindowStruct.mainStruct.setExistingWindows(guiWindowStruct.mainParamStruct);	


% Create a second window showing the results of averaging around the grid
% points.  Note that the calculation is not performed unless the grid
% spacing and radius are set, and a region of interest selected in the main
% GUI.

% Create the calculation object that will be called by the calc grid GUI
%bMeanMagCalcObject = mapseis.calc.Calculation(...
%    @mapseis.calc.CalcBMeanMag,@mapseis.projector.getMagnitudes,{},...
%    'Name','b Mean Mag');

% guiWindowStruct.calcStruct=CalcGridGUI(dataStore,'BMeanMagView',...
%    bMeanMagCalcObject,...
%    @(x) x.meanMag,filterList);

%addlistener(filterList,'Update',...
%    @(s,e) guiWindowStruct.mainStruct.updateGUI());
%addlistener(filterList,'Update',...
%    @(s,e) guiWindowStruct.mainParamStruct.updateGUI());

%replaced by the proxy
%ListProxy.listen(filterList,'Update',...
%    @(s,e) guiWindowStruct.mainStruct.updateGUI());

%ListProxy.listen(filterList,'Update',...
%    @(s,e) guiWindowStruct.mainParamStruct.updateGUI());

%addlistener(filterList,'Update',...
%    @(s,e) guiWindowStruct.calcStruct.updateGUI());


end

function filterList = createDefaultFilters(dataStore,ListProxy)
import mapseis.filter.*;
regionFilter = RegionFilter('all',[],'Region');
timeFilter = TimeRangeFilter('all',[-Inf Inf],'Time');
magFilter = MagFilter('all',[-Inf Inf],'Magnitude');
depthFilter = DepthFilter('all',[-Inf Inf],'Depth');
hourFilter = HourRangeFilter('all',[-Inf Inf],'Hour');
minuteFilter = MinuteRangeFilter('all',[-Inf Inf],'Minute');
% sphereFilter = SphereRangeFilter('all',[],'Sphere');

% Initialise the filters to make the selection vectors the correct length
regionFilter.execute(dataStore);
timeFilter.execute(dataStore);
magFilter.execute(dataStore);
depthFilter.execute(dataStore);
hourFilter.execute(dataStore);
minuteFilter.execute(dataStore);

% Composite FilterList object
filterList = FilterList({regionFilter,timeFilter,magFilter,...
    depthFilter,hourFilter,minuteFilter},dataStore,ListProxy); % ,sphereFilter});
end

function setDefaultUserData(dataStore)
% Set the default user data parameters for the application
import mapseis.plot.GetOverlay;

% Set the calculation grid spacing and radius of interest
gridPars.gridSep = 0.2;
gridPars.rad = 1;
dataStore.setUserData('gridPars',gridPars);

%Set Depth Grid
gridDepthPars.gridSep = 2;
gridDepthPars.rad = 5;
dataStore.setUserData('gridDepthPars',gridDepthPars);

%set profile width to default values
ProfileWidth = 0.5;
dataStore.setUserData('ProfileWidth',ProfileWidth);

% Set the selection radius to use for circular region selection
mainPars.selectRadius = 0.5;
% Histogram plot type
mainPars.histType = 'mag';
% Time plot type
mainPars.timeType = 'cumnum';

[Coast_PlotArgs, InternalBorder_PlotArgs] = GetOverlay(dataStore);

% Coastline data
mainPars.indexFileName = 'test';
dataStore.setUserData('mainPars',mainPars);
dataStore.setUserData('Coast_PlotArgs',Coast_PlotArgs);
dataStore.setUserData('InternalBorder_PlotArgs',InternalBorder_PlotArgs);
dataStore.setUserData('DistMode',true);

%set buffers
dataStore.bufferDate;
end

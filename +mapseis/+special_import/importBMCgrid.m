function eventStruct=importBMCgrid(inStr,varargin)
% Special import filter for Satsi Outputfiles

%% Import helper functions
import mapseis.util.importfilter.*;


onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'SpecialImportFilter for BMC grid (format: num lon lat value, and NaN=NA)' 
    	eventStruct.displayname = 'BMCgrid Specialimportfilter' ;
    	eventStruct.filetype = '.txt'    	
    	onlydescription= true;
    	
    end

end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the white space character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		
		
		% Pattern for row of data (first part)
		rowPat = [nm('NodeNR',rp.text),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('value',rp.anything)];
		
		planeStruct = readTable(inStr,rp.line,rowPat);

			
		%exchange the NA to NaN
		TheWrong=strncmp(planeStruct.value,'NA',2);
		planeStruct.value(TheWrong)={'NaN'};
		
		%% Define the fields that are present in the final output 
		numericFields = {'NodeNR','lon','lat','value'};
		
		
		
		
		
		
		%% Convert numerical fields to doubles
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		

end

end

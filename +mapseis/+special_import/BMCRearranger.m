function [ListedGrid LonGrid LatGrid ValGrid] = BMCRearranger(inGrid)
	%Rearranges a BMCgrid in a structur format into a list and a gridded
	%format
	
	%first the normal Listed Grid
	ListedGrid=[inGrid.lon,inGrid.lat,inGrid.value];
	
	%now the gridded
	uniLon=unique(inGrid.lon);
	uniLat=unique(inGrid.lat);
	ValGrid=reshape(inGrid.value,numel(uniLon),numel(uniLat));
	[LonGrid LatGrid]=meshgrid(uniLon,uniLat);

end

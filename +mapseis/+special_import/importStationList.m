function eventStruct=importStationList(inStr,varargin)
% Special import filter for Satsi Outputfiles

%% Import helper functions
import mapseis.util.importfilter.*;


onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'SpecialImportFilter for StationList (format: name lat lon elevation(km) start end)'
    	eventStruct.displayname = 'Stationlist Specialimportfilter' ;
    	eventStruct.filetype = '.txt'    	
    	onlydescription= true;
    	
    end

end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the white space character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		
		
		% Pattern for row of data (first part)
		rowPat_timevar = [nm('Name',rp.text),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('elevation',rp.realPat),ws,...
		    nm('starttime',rp.specialDate),ws,...
		    nm('endtime',rp.specialDate)];
		
		rowPat_static = [nm('Name',rp.text),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('elevation',rp.realPat)];
		
			    
				    
		%% Convert the input string into a struct of data
		try
			planeStruct = readTable(inStr,rp.line,rowPat_timevar);
			Listmode='timevar';
		catch	
			planeStruct = readTable(inStr,rp.line,rowPat_static);
			Listmode='static';
		end
		
		
		
		
		%% Define the fields that are present in the final output 
		numericFields = {'lat','lon','elevation'};
		DateFields = {'starttime','endtime'};
		
		
		
		
		
		%% Convert numerical fields to doubles
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		
		%now convert dates if available
		if strcmp(Listmode,'timevar')
		
		
			for i=1:numel(DateFields)
			    
			    notSet=strcmp(planeStruct.(DateFields{i}),'XXXX-XX-XX')|...
			    		strcmp(planeStruct.(DateFields{i}),'xxxx-xx-xx');
			    eventStruct.(DateFields{i})(notSet,1) = nan;
			    eventStruct.(DateFields{i})(~notSet,1) = ...
				cellfun(@(x) datenum(x,'yyyy-mm-dd'),planeStruct.(DateFields{i})(~notSet));
			end
			
		end
		
		

				  
		eventStruct.Name=planeStruct.Name;		  
		eventStruct.ListMode=Listmode;
		
end

end
function eventStruct=importSATSIStress(inStr,varargin)
% Special import filter for Satsi Outputfiles

%% Import helper functions
import mapseis.util.importfilter.*;
import mapseis.util.emptier;

onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'SpecialImportFilter for SATSI Stress Inversion results.'
    	eventStruct.displayname = 'SATSI Specialimportfilter' ;
    	eventStruct.filetype = '.txt'    	
    	onlydescription= true;
    	
    end

end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the white space character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		   
		% Pattern for row of data (first part)
		rowPat_A = [ws,nm('X',rp.realPat),ws,...
		    nm('Y',rp.realPat),ws,...
		    nm('See',rp.realPat),ws,...
		    nm('Sen',rp.realPat),ws,...
		    nm('Seu',rp.realPat),ws,...
		    nm('Snn',rp.realPat),ws,...
		    nm('Snu',rp.realPat),ws,...
		    nm('Suu',rp.realPat)];
		
		    
		% Pattern for row of data (second part) 
		%(strike= dip direction)
		rowPat_B = [ws,nm('strike',rp.realPat),ws,...
		    nm('dip',rp.realPat),ws,...
		    nm('rake',rp.realPat),ws,...
		    nm('fit_angle',rp.realPat),ws,...
		    nm('mag_tau',rp.realPat),ws,...
		    nm('X',rp.realPat),ws,...
		    nm('Y',rp.realPat)];    
		    
		rowPat_C1=    ['fit angle mean= ',nm('fit_angle_mean',rp.realPat),ws,...
		    'standard deviation= ',nm('std_dev_angle',rp.realPat)];
		rowPat_C2=    ['avg tau= ',nm('avg_tau',rp.realPat),ws,...
		    ', std. dev.= ',nm('std_dev_tau',rp.realPat)];
		    
		%% Convert the input string into a struct of data
		%planeStruct = readTable(inStr,rp.line,rowPat);
		
		if numel(inStr)==0
			eventStruct=[];
			disp('no result found')
			return;
		end
		
		%manual work is needed 
		lines = regexp(inStr,rp.line,'match');
		
		
		
		%cut last symbol away (eol \n)
		CutLines=cellfun( @(x) x(1:end-1),lines,'UniformOutput',false);
		
		%now search the three positions
		head1='X Y See Sen Seu Snn Snu Suu';
		head2='dip direction, dip, rake, fit angle, mag tau, X, Y';
		
		countArray=1:numel(lines);
		
		head1_pos=countArray(strcmp(CutLines,head1));
		head2_pos=countArray(strcmp(CutLines,head2));
		endPos=numel(lines)-3;
		
		%now split the whole line array into three parts
		linesA=lines(head1_pos+1:head2_pos-3);
		linesB=lines(head2_pos+1:endPos);
		LastPart=lines(endPos+2:end);
		
		%now do the rest for getting a planeStruct
		matches_A = regexp(linesA,rowPat_A,'names');
		matches_B = regexp(linesB,rowPat_B,'names');
		
		% Remove the non-matching cells
		matches_A = matches_A(cellfun(@(x) ~isempty(x),matches_A));
		matches_B = matches_B(cellfun(@(x) ~isempty(x),matches_B));
		
		%last line
		matches_C1=regexp(LastPart(1),rowPat_C1,'names');
		matches_C2=regexp(LastPart(2),rowPat_C2,'names');
		
		
		planeStruct_A = cellArrayToPlane(matches_A);
		planeStruct_B = cellArrayToPlane(matches_B);
		
		%% Define the fields that are present in the final output 
		numericFields_A = {'X','Y','See','Sen','Seu','Snn','Snu','Suu'};
		numericFields_B = {'strike','dip','rake','fit_angle','mag_tau','X','Y'};
		LastFields={'fit_angle_mean','std_dev_angle','avg_tau','std_dev_tau'};
		
		
		
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields_A)
			emp=emptier(planeStruct_A.(numericFields_A{i}));
			planeStruct_A.(numericFields_A{i})(emp)={'nan'};
			
			eventStruct_A.(numericFields_A{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct_A.(numericFields_A{i}));
		end
		
		for i=1:numel(numericFields_B)
			emp=emptier(planeStruct_B.(numericFields_B{i}));
			planeStruct_B.(numericFields_B{i})(emp)={'nan'};
			eventStruct_B.(numericFields_B{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct_B.(numericFields_B{i}));
		end
		
		if ~isempty(matches_C1{1})
			lastStruct.fit_angle_mean=matches_C1{1}.fit_angle_mean;
			lastStruct.std_dev_angle=matches_C1{1}.std_dev_angle;
		else
			lastStruct.fit_angle_mean='nan';
			lastStruct.std_dev_angle='nan';
		end
		
		if ~isempty(matches_C2{1})
			lastStruct.avg_tau=matches_C2{1}.avg_tau;
			lastStruct.std_dev_tau=matches_C2{1}.std_dev_tau;
		else
			lastStruct.avg_tau='nan';
			lastStruct.std_dev_tau='nan';
		end
		
		LastFields=fieldnames(lastStruct);
		
		%disp(lastStruct)
		for i=1:numel(LastFields)
			if ~isempty(lastStruct.(LastFields{i}))
				lastStruct.(LastFields{i}) = sscanf(lastStruct.(LastFields{i}),'%f');
			else
				lastStruct.(LastFields{i}) = nan;
			end
		end
				  
				  
		eventStruct={eventStruct_A,eventStruct_B,lastStruct};
		
end

end
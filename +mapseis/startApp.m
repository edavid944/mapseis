function [guiWindowStruct,ListProxy] = startApp
% startApp : start the MapSeis GUI and return a handle to the data
%

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

import mapseis.datastore.*;
import mapseis.importfilter.*;
import mapseis.gui.*;
import mapseis.projector.*;
import mapseis.calc.*;
import mapseis.listenerproxy.*;
warning off


%create the Proxylistener (needed first)
ListProxy = addproxylistener;



%if isempty(filterList.ListProxy)
%			filterList.ListProxy=ListProxy;
%end			

%% Create the GUI

% Create the main GUI window showing event locations, magnitudes and times
guiWindowStruct.mainStruct=GUI([],[],ListProxy);

%addlistener(filterList,'Update',...
%    @(s,e) guiWindowStruct.mainStruct.updateGUI());

% Create the GUI for setting filter parameters and other analysis
% parameters
guiWindowStruct.mainParamStruct=ParamGUI([],[],ListProxy); % ,filterList);

%link the parameter window to the interal property of the main gui
guiWindowStruct.mainStruct.setExistingWindows(guiWindowStruct.mainParamStruct);	





end



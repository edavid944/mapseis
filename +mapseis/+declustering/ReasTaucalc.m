function [tau] = ReasTaucalc(xk,EqMag,xmeff,bgdiff,P)

	%ORIGINAL FROM ZMAP: NEEDS WORK FOR MAPSEIS
	%SUBFUNCTION: ReasenbergDeclus.m MIGHT NOT BE NEEDED
	%---------------------------------------------------
	
	%Adopted for MapSeis (small changes)
	
	
	%tauclac.m                                         A.Allmann 
	%routine to claculate the look ahead time for clustered events
	%gives tau back
	
	% global newcat xk mbg xmeff k1 P
	% global top denom deltam bgdiff
	
	%No need to send to variables 1 is enough
	%EqMag=mbg(k1)
	
	deltam = (1-xk)*EqMag-xmeff;        %delta in magnitude
	if deltam<0
	 	deltam=0;
	end;
	
	
	denom  = 10^((deltam-1)*2/3);              %expected rate of aftershocks
	top    = -log(1-P)*bgdiff;
	tau    = top/denom;                        %equation out of Raesenberg paper
 
end 
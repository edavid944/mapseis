function filterupdater(pos,dataStore,regionFilter)
	import mapseis.region.*;
	import mapseis.Filter.*;
	import mapseis.util.*;
	
		 


	%Uses the input from imroi type objects and updates the filter 
	filterRegion = getRegion(regionFilter);
        pRegion = filterRegion{2};
        RegRange = filterRegion{1};

	switch RegRange
        				
        			case {'in','out'}
        					newRegion = Region(pos);
						regionFilter.setRegion(RegRange,newRegion);

                		case 'line'
            					regpar = regionFilter.getDepthRegion;
            					wid = regionFilter.Width;
            					
            					if isempty(wid)
            						wid = GetProfileWidth(dataStore);
            						regionFilter.setWidth(wid);
            					end
            					
            					dReg = regpar{3};
						depReg = dReg.getBoundary;
						[lat lon dep] = old2new(pos,depReg(:,2));
						newDregion  = DepthRegion(lat, lon, dep);
						Regio=Slicer(pos, wid);
            					
						regionFilter.setRegion(RegRange,Regio.pRegion);
            					regionFilter.setDepthRegion(newDregion,wid);
            			
            			case 'circle'		
            					if isempty(regionFilter.Radius)
            					oldgrid = dataStore.getUserData('gridPars');
						regionFilter.setRadius(oldgrid.rad);
						end
						
            					%dataStore.setUserData('gridPars',newgrid);
            					
            					%update filter
            					newRegion = [pos(1),pos(2)];
            					regionFilter.setRegion(RegRange,newRegion);
            					
            			case 'all'
            			%do nothing
          end	


		function [lat,lon,dep] = old2new(pos,depReg)
	 			%creates the input for the new depth region
	 			
	 			%create a new retangular for the depthRegion with 
	 			%minmax from the old one
            			depmin = min(depReg);
            			depmax = max(depReg);

	 			%first point
	 			lat(1) = pos(1,1);
	 			lon(1) = pos(1,2);
	 			dep(1) = depmin;
	 					
	 			%second dpoint
	 			lat(2) = pos(1,1);
	 			lon(2) = pos(1,2);
	 			dep(2) = depmax;
	 						
	 			%third dpoint
	 			lat(3) = pos(2,1);
	 			lon(3) = pos(2,2);
	 			dep(3) = depmax;
	 					
	 			%last dpoint
	 			lat(4) = pos(2,1);
	 			lon(4) = pos(2,2);
	 			dep(4) = depmin;
	 	end
	 	
	 	
	 	
end
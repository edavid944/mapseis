classdef Slicer < handle
	%module for creating depth slices
	%Version 1, Autor: David Eberhard

	%Save variables
	properties
		pLine
		LineWidth
		pRegion
	end


	%Public Methods
	methods
	
		function obj = Slicer(iLine,LWidth)
						
			import mapseis.region.*;
			
			
			%Constructor of the class
			
			obj.pLine = iLine;
			obj.LineWidth = LWidth;
			obj.pRegion=Region(obj.polySlice);
		
		
		end
	
		
		function pLvec = UnitVector(obj)
			%Creates a unit vector in directon of the profile line
			P = obj.pLine;
			vec = P(2,:) - P(1,:);
			pLvec = vec./(sqrt(vec(1,1)^2+vec(1,2)^2));
		
		end
	
		
		function pLvec = RecUnitVector(obj)
			%Creates a unit vector in directon of the profile line
			P = obj.pLine;
			vecno = P(2,:) - P(1,:);
			
			%normal to the vector
			vec(1,1)=1/vecno(1,1);
			vec(1,2)=-1/vecno(1,2);
			
			pLvec = vec./(sqrt(vec(1,1)^2+vec(1,2)^2));
			
			%erase nans
			pLvec(isnan(pLvec))=1;
		
		end

	
		
		function pol = polySlice(obj)

			%Creates a polygon with a given with given width from the line	
			nvec = RecUnitVector(obj);
			P = obj.pLine;
			w = obj.LineWidth;
			
			%normal vector to the unit vector
			%nvec(1,1) = 1/pLvec(1,1);
			%nvec(1,2) = -1/pLvec(1,2);
			
			%build the polygon with points of the original line included 
			%(pol(1,:) and pol(4,:)
			pol = [P(1,1) P(1,2); P(1,1)+w*nvec(1,1)  P(1,2)+w*nvec(1,2); ...
					P(2,1)+w*nvec(1,1) P(2,2)+w*nvec(1,2); ...
					P(2,1) P(2,2); P(2,1)-w*nvec(1,1) P(2,2)-w*nvec(1,2);...
					P(1,1)-w*nvec(1,1)  P(1,2)-w*nvec(1,2)];
		end
	
	
		%function 
		%calculates the projection of the earthquake locations on the profile line
		function linedist = LineProjection(obj,lat,lon);
			
			%get unitvector
			uvec=obj.UnitVector;
			
			P1=obj.pLine(1,:);
			
			rlat=lat-P1(1,1);
			rlon=lon-P1(1,2);		
			
			templat = (rlat*uvec(1,1));
			templon = (rlon*uvec(1,2));
			
			projlat = (templat+templon)*uvec(1,1);
			projlon = (templat+templon)*uvec(1,2);
			linedist = deg2km(distance(P1(1,1),P1(1,2),P1(1,1)+projlat(:,1),P1(1,2)+projlon(:,1)));
			%linedist = (projlat.^2+projlon.^2).^(0.5);
		
		
		
		end
	
	end







end
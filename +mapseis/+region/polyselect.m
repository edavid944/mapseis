classdef polyselect < impoly
% added the updateFilter listener to impoly


	properties        
    end
    
    events
    	FilterUpdated
    	Update
    end
    
    methods
        function obj = polyselect(varargin)
            % Constructor for the polyselect
            obj = obj@impoly(varargin{:});
            
           % cmenu = uicontextmenu('Parent',gcf);
            % Now make the menu be associated with the correct axis
           % set(obj,'UIContextMenu',cmenu);
            %set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different region selections
            %regionFilter = obj.FilterList.getByName('Region');
            %dataStore = obj.DataStore;
            %uimenu(cmenu,'Label','Edit',...
             %   'Callback',@(s,e) editor());

        end
        
        function pos = editor(obj)
        	pos=wait(obj);
        end
     	
         end
end
function CreateRegionModifyMenu(bdryHnd,eventStruct)
% CreateRegionModifyMenu : modify a selection region 
% Allow the user to modify the boundary of a selection region by moving the
% selected point, translating the selection region or rotating the
% selection region.

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 74 $    $Date: 2008-10-20 09:17:44 +0100 (Mon, 20 Oct 2008) $
% Author: Matt McDonnell

% To create the context menu we need to find the handle of the of the
% figure containing the plot axis that contains the line handle we have
% been passed as an input argument.

% Root handle of all handle graphics objects is 0, so travel towards the
% tree root until we find this.
figHnd = get(bdryHnd,'Parent');
axesHnd = figHnd;
while get(figHnd,'Parent') ~= 0 
    figHnd = get(figHnd,'Parent');
end

dataStore = eventStruct.dataStore;
regionFilter = eventStruct.regionFilter;
    
% Create the uicontext menu
cmenu = uicontextmenu('Parent',figHnd);
uimenu(cmenu,'Label','Move selected point','Callback',@(s,e) nMovePoint());
uimenu(cmenu,'Label','Translate region','Callback',@(s,e) nMoveRegion());
uimenu(cmenu,'Label','Rotate region','Callback',@(s,e) nRotateRegion());
uimenu(cmenu,'Label','Scale region','Callback',@(s,e) nScaleRegion());

% Add the context menu to the boundary
set(bdryHnd,'UIContextMenu',cmenu);

% Get the locations of the boundary points
bdryLocs = ...
    [reshape(get(bdryHnd,'XData'),[],1),reshape(get(bdryHnd,'YData'),[],1)];
% % Remove repeated points eg start and end of boundary are the same
% bdryLocs = unique(bdryLocs,'rows');
% Number of points on boundary
bdryPointCount = numel(bdryLocs(:,1));
% Centre of the region
regionCentre = mean(bdryLocs);
% Positions of boundary points relative to the centre of the polygon
bdryLocsRel = bdryLocs - repmat(regionCentre,bdryPointCount,1);

    function nMovePoint()
        import mapseis.util.*;
        
        % Move the selected point and redefine the region filter
                
        [currentPosition,pointIndex]=nGetClosestPoint();
        % Highlight the point on the axes
        nPlotBoundaryPoint(currentPosition);
        % Get the new point position
        newPosition = Ginput(1);
        % Set the new point into the boundary
        bdryLocs(pointIndex,:) = newPosition;
        % First and last points are coincident so change both
        if pointIndex==1  || pointIndex==bdryPointCount
            bdryLocs(1,:) = newPosition;
            bdryLocs(end,:) = newPosition;
        end
        nUpdateRegion();
    end

    function nMoveRegion()
        % Translate the entire region and redefine the region filter 
        
        [oldPosition, newPosition]=nGetRelativePositions('centre');
        % Translate the boundary points to be centred around the new
        % position
        % Set the new point into the boundary
        bdryLocs = nAffineTransformation(eye(2),regionCentre+newPosition);
        nUpdateRegion()
    end

    function nRotateRegion()
        % Rotate the region about the centre point
        
        [oldPosition, newPosition] = nGetRelativePositions();
        % Calculate the old and new angles of the point to the centre
        oldAngle = atan2(oldPosition(2),oldPosition(1));
        newAngle = atan2(newPosition(2),newPosition(1));
        % Rotate all of the boundary points
        rotMat = @(rotAngle) [cos(rotAngle) -sin(rotAngle); sin(rotAngle) cos(rotAngle)];
        % Set the new point into the boundary
        bdryLocs = nAffineTransformation(rotMat(newAngle-oldAngle),regionCentre);
        nUpdateRegion()
    end

    function nScaleRegion()
        % Rotate the region about the centre point
        
        [oldPosition, newPosition]=nGetRelativePositions();
        % Calculate the scale factor
        scaleFactor = norm(newPosition)/norm(oldPosition);
        % Scale all of the boundary points
        scaleMat = [scaleFactor 0; 0 scaleFactor];
        % Set the new point into the boundary
        bdryLocs = nAffineTransformation(scaleMat,regionCentre);
        nUpdateRegion()
    end

    function [currentPosition,pointIndex]=nGetClosestPoint()
        % Return the point on the boundary closest to the cursor position
        % when the context menu is called
        
        % Get the current point from the CurrentPoint property of the axis
        currentPoint = get(axesHnd,'CurrentPoint');
        currentPoint = currentPoint(1,1:2);
        % Find which point on the boundary this corresponds to by finding
        % the closest boundary point
        [minDistSqr, pointIndex] = ...
            min(sum(...
            (bdryLocs-repmat(currentPoint,bdryPointCount,1)).^2,...
            2));                
        currentPosition = bdryLocs(pointIndex,:);
    end

    function [oldPosition, newPosition]=nGetRelativePositions(highlightMode)
        % Get position of current point relative to centre of region, and
        % use Ginput to get the new position (what this means depends on
        % transformation mode)

        import mapseis.util.Ginput;
        
        % highlightMode = {'none','point','centre','both'} to choose which
        % point gets highlighted        
        if nargin<1
            highlightMode = 'both';
        end
        
        % Get the closest boundary point
        [currentPosition,pointIndex]=nGetClosestPoint();

        if strcmp(highlightMode,'both') || strcmp(highlightMode,'point')
            % Highlight the point on the axes
            nPlotBoundaryPoint(currentPosition);
        end
        
        if strcmp(highlightMode,'both') || strcmp(highlightMode,'centre')
            % Plot the centre of the region
            nPlotCentrePosition();
        end
        % Old position relative to centre
        oldPosition = bdryLocs(pointIndex,:);
        oldPosition = oldPosition-regionCentre;
        % Get the new point position
        newPosition = Ginput(1);
        newPosition = newPosition-regionCentre;
    end

    function nPlotBoundaryPoint(currentPosition)
        % Highlight the boundary point that has been selected
        currHnd=line(currentPosition(1),currentPosition(2));
        set(currHnd,'Marker','o','MarkerFaceColor','r','MarkerSize',9,...
            'MarkerEdgeColor','r');
    end

    function nPlotCentrePosition()
        % Plot centre of region
        centHnd=line(regionCentre(1),regionCentre(2));
        set(centHnd,'Marker','p','MarkerFaceColor','g','MarkerSize',14,...
            'MarkerEdgeColor','g');
    end
        
    function newBdryLocs=nAffineTransformation(transMat,offsetVect)
        affineTransMat = [transMat(1,:) offsetVect(1); ...
            transMat(2,:) offsetVect(2);...
            0 0 1];
        
        newBdryLocs = (affineTransMat*...
            ([bdryLocsRel, ones(bdryPointCount,1)]'))';
        newBdryLocs = newBdryLocs(:,[1 2]);
    end

    function nUpdateRegion()
        import mapseis.region.*;
        
        % Get the range specification of the current region
        regionParams = regionFilter.getRegion();
        % Set the new region
        regionFilter.setRegion(regionParams{1},Region(bdryLocs));
        % Execute the filter to update the views
        regionFilter.execute(dataStore);
    end
end
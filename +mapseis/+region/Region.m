classdef Region < handle
    % Region : map region to analyse seismic events
    %
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 73 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
    % Author: Matt McDonnell
    
    
    
    % Attributes
    properties
        PolyRegion % Polygon region of interest
        Offsets
        ShiftedBack
        UndoData
    end

    % Public Methods
    methods
        
    
    	function obj = Region(varargin)
            import mapseis.util.region.polyRegion;
            obj.PolyRegion = polyRegion(varargin{:});
            obj.Offsets=[];
            obj.ShiftedBack=true;
            obj.UndoData=[];
           
        end
        
        
        function setRegion(obj,varargin)
            import mapseis.util.region.polyRegion;
            
            % Reset the region
            obj.PolyRegion = polyRegion(varargin{:});
            
            obj.Offsets=[];
            obj.ShiftedBack=true;
            obj.UndoData=[];
            
        end
        
        
        
        function SetOffset(obj,Offsets)
        	%sets the offsets and set ShiftedBack to false, thus 
        	%assuming a shifted polygon, the variable is public so it 
        	%can be changed in case this is not true
        	 obj.Offsets=Offsets;
        	 obj.ShiftedBack=false;
        end
        
        
        
        function ShiftBack(obj,Offsets)
        	%Takes the old polyRegion and builds a new on with shifted coordinates
        	%Carefull it will be done reversed means Offsets = -Offsets, this allows to 
        	%This should be done if a shifted reference frame is used, by do this, the 
        	%polygon region will be converted into back into common reference frame
        	
        	import mapseis.util.ShiftCoords;
        	import mapseis.util.region.FixBoundPoly;
        	
        	pts=obj.PolyRegion.getBoundary();
        	
        	if ~isempty(obj.Offsets)&~isempty(Offsets)
        		%Check if the Offsets has to be reversed
        		if ~obj.ShiftedBack
        			[CorrectedPoints UndoData] = FixBoundPoly(pts,'Shifted',Offsets,false,obj.UndoData);
        			obj.Offsets=Offsets;
        			obj.UndoData=UndoData;
        			obj.ShiftedBack=true;
        			obj.PolyRegion = polyRegion(CorrectedPoints);
        			
        		else
        			%This is probably a bad idea, but so what, will probalby never be used
        			[ReWorkedPoints UndoData] = FixBoundPoly(pts,'undo',obj.Offsets,false,obj.UndoData);
        			[CorrectedPoints UndoData] = FixBoundPoly(ReWorkedPoints,'Shifted',Offsets,false,[]);
        			
        			obj.Offsets=Offsets;
        			obj.UndoData=UndoData;
        			obj.ShiftedBack=true;	
        			obj.PolyRegion = polyRegion(CorrectedPoints);
        			warning('Region has been reshifted');
        		end
        		
        		
        	elseif ~isempty(obj.Offsets)&isempty(Offsets)
        		if ~obj.ShiftedBack
        			[CorrectedPoints UndoData] = FixBoundPoly(pts,'Shifted',obj.Offsets,false,obj.UndoData);
        			obj.UndoData=UndoData;
        			obj.ShiftedBack=true;
        			obj.PolyRegion = polyRegion(CorrectedPoints);	
        		end
        		
        		
        	elseif ~isempty(Offsets)&isempty(obj.Offsets)
        		[CorrectedPoints UndoData] = FixBoundPoly(pts,'Shifted',Offsets,false,obj.UndoData);
        		obj.Offsets=Offsets;
        		obj.UndoData=UndoData;
        		obj.ShiftedBack=true;
        		obj.PolyRegion = polyRegion(CorrectedPoints);	
        		
        	end
        	
        end
        
        
        
        function pts=getBoundary(obj,Shifted)
            % Get the points defining the boundary of the region
            import mapseis.util.ShiftCoords;
            import mapseis.util.region.FixBoundPoly;
            
            if nargin<2
            	Shifted=false;
            end
            
            pts = obj.PolyRegion.getBoundary();
            
            if ~isempty(obj.Offsets)&Shifted&obj.ShiftedBack
            	%Rebuild the points
            	[pts UndoData] = FixBoundPoly(pts,'undo',obj.Offsets,false,obj.UndoData);
            end
            
        end
        
        
        
        function bBox=getBoundingBox(obj,Shifted)
            % Get the bounding box of the region
            % bBox = [minX maxX minY maxY]
            import mapseis.util.ShiftCoords;
            import mapseis.util.region.FixBoundPoly;
            
            if nargin<2
            	Shifted=false;
            end
            
            if ~isempty(obj.Offsets)
            	
            	%first get the boundingbox for the not backshift poly (shifted)
            	if obj.ShiftedBack
			pts = obj.PolyRegion.getBoundary();
			[CorrectedPoints UndoData] = FixBoundPoly(pts,'undo',obj.Offsets,false,obj.UndoData);
			ReworkPoly = polyRegion(CorrectedPoints);
			bBox = ReworkPoly.getBoundingBox();
		else
			bBox = obj.PolyRegion.getBoundingBox();
		end
           
            	
            	if ~Shifted
            		%If a UnShifted (original frame) poly is wanted it needs to be shift to the original 
            		%frame, it has to be done this way, because if not min max would always be -180/180 for
            		%for lon and -90/90 for lat incase there is a "jump" in the polygon.
            		%[pts UndoData] = FixBoundPoly(pts,'undo',obj.Offsets,false,obj.UndoData);
            		MrShifter=[bBox(1),bBox(3);bBox(2),bBox(4)];
            		ShiftBox=ShiftCoords(MrShifter,-obj.Offsets);
            		bBox=[ShiftBox(1,1),ShiftBox(1,2),ShiftBox(2,1),ShiftBox(2,2)];
            	end
            	
            else
               	%Classic mode without any shift
            	bBox = obj.PolyRegion.getBoundingBox();
            	
            	
            end
            
        end
        
        
        
        function b=isInside(obj,pts,Shifted)
            % Return a vector of logical indices showing whether a point is
            % within the region.  pts = [x y]
            %If set to Shifted the points will be compared to the Shifted polygon
            %instead of the normal unshifted on
            
            if nargin<3
            	Shifted=false;
            end
            
            if ~isempty(obj.Offsets)&Shifted&obj.ShiftedBack
            	orgP = obj.PolyRegion.getBoundary();
            	[CorrectedPoints UndoData] = FixBoundPoly(orgP,'undo',obj.Offsets,false,obj.UndoData);
            	ReworkPoly = polyRegion(CorrectedPoints);
            	b = ReworkPoly.isInside(pts);
            	
            else
            	b = obj.PolyRegion.isInside(pts);
            end
        end
        
        
        
        function gPts=getGrid(obj,Shifted,varargin)
            import mapseis.util.region.polyGrid;
            
            if (nargin-numel(varargin))<2
            	Shifted=false;
            end
           
            if ~isempty(obj.Offsets)&obj.ShiftedBack
            	pts = obj.PolyRegion.getBoundary();
		[CorrectedPoints UndoData] = FixBoundPoly(pts,'undo',obj.Offsets,false,obj.UndoData);
		ReworkPoly = polyRegion(CorrectedPoints);
            	rawGrid=polyGrid(ReworkPoly.getBoundary());
		gPts = rawGrid.getGrid(varargin{:});
		
		if ~Shifted
			gPts=ShiftCoords(gPts,-obj.Offsets);
				
		end
		
            else
            	% Get a grid of points on a rectangular lattice.
            	pGrid = polyGrid(obj.PolyRegion.getBoundary());
            	gPts = pGrid.getGrid(varargin{:});
            
            end	
        end
        
        
        function gPts=getHexGrid(obj,varargin)
            import mapseis.util.region.polyGrid;
            
            % Get a grid of points on a hexagonal lattice.
            pGrid = polyGrid(obj.PolyRegion.getBoundary(),@hexGridPoints);
            gPts = pGrid.getGrid(varargin{:});
        end
        
        %% Private Methods
        
    end
end
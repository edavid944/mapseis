classdef DepthRegion < handle
    % Region : map region to analyse seismic events
    %
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 73 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
    % Author: Matt McDonnell
    
    
    %Comment location shift: it seems like this modul does not need any modification
    %as distance is not depended on the jump at 180/-180 and 90/-90, I might still
    %add information about the shift, just in case it is needed somewhere.
    
    % Attributes
    properties
        latlondepRegion %coordinates in lon lat and depth
        PolyRegion % Polygon region of interest
        
    end

    % Public Methods
    methods
        function obj = DepthRegion(lat,lon,depth)
            import mapseis.util.region.polyRegion;
            
            obj.latlondepRegion = [lat(:),lon(:),depth(:)];
            
            %set first point as (0,0,depth)
       		origin = [lat(1),lon(1)];
       		
       		%calculate the distances
       		incoord = deg2km(distance(origin(1),origin(2),lat(:),lon(:)));
       		incoord = [incoord(:), depth(:)];
       		
            obj.PolyRegion = polyRegion(incoord);
        

        end
        
        function setRegion(obj,varargin)
            import mapseis.util.region.polyRegion;
            %DOES THAT WORK?? I have to check it
            
            % Reset the region
             obj.latlondepRegion = [lat(:),lon(:),depth(:)];
            
            %set first point as (0,0,depth)
       		origin = [lat(1),lon(1)];
       		
       		%calculate the distances
       		incoord = deg2km(distance(origin(1),origin(2),lat(:),lon(:)));
       		incoord = [incoord, depth];
       		
            obj.PolyRegion = polyRegion(incoord);

        end
        
        function pts=getLatLonpoints(obj)
        	pts =  obj.lonlatdepRegion;
        end
        		
                
        function pts=getBoundary(obj)
            % Get the points defining the boundary of the region
            pts = obj.PolyRegion.getBoundary();
        end
        
        function bBox=getBoundingBox(obj)
            % Get the bounding box of the region
            % bBox = [minX maxX minY maxY]
            bBox = obj.PolyRegion.getBoundingBox();
        end
        
        function b=isInside(obj,pts)
            % Return a vector of logical indices showing whether a point is
            % within the region.  pts = [x y]
            origin = [obj.latlondepRegion(1,1),latlondepRegion(1,2)];
       		
       		%calculate the distances
       		incoord = deg2km(distance(origin(1),origin(2),pts(:,1),pts(:,2)));
       		incoord = [incoord, pts(:,3)];
            b = obj.PolyRegion.isInside(incoord);
        
        end
        
        function gPts=getGrid(obj,varargin)
            import mapseis.util.region.polyGrid;
            
            % Get a grid of points on a rectangular lattice.
            pGrid = polyGrid(obj.PolyRegion.getBoundary());
            gPts = pGrid.getGrid(varargin{:});
        end
        
        function gPts=getHexGrid(obj,varargin)
            import mapseis.util.region.polyGrid;
            
            % Get a grid of points on a hexagonal lattice.
            pGrid = polyGrid(obj.PolyRegion.getBoundary(),@hexGridPoints);
            gPts = pGrid.getGrid(varargin{:});
        end
        
        %% Private Methods
        
    end
end
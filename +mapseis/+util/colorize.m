function RGBMatrix = colorize(indata,minmaxData,ColorStep,colorscheme)
%Normalizes the input data to 1 (with the minmaxData) and adds a color to every value

%normalize with minmax

MinVal=minmaxData(1);
MaxVal=minmaxData(2);

normedData=(indata-MinVal) / abs(MaxVal-MinVal);

%get size if input)
inSize=size(indata);

%set values <0 to 0 and values >1 to 1
lowZero=normedData<0;
highOne=normedData>1;

normedData(lowZero)=0;
normedData(highOne)=1;

%now multiply with the number of ColorSteps-1 add 1 and round to the next full number
ColorIndexes=round(normedData*(ColorStep-1)+1);

%set values NaN to 1
ColorIndexes(isnan(ColorIndexes))=1;

%build a map
colorfunc=str2func(colorscheme);
RawColors=colorfunc(ColorStep);

if all(inSize>1)
	tempMatr=RawColors(ColorIndexes,1);
	RGBMatrix(:,:,1)=reshape(tempMatr,inSize(1),inSize(2));
	
	tempMatr=RawColors(ColorIndexes,2);
	RGBMatrix(:,:,2)=reshape(tempMatr,inSize(1),inSize(2));
	
	tempMatr=RawColors(ColorIndexes,3);
	RGBMatrix(:,:,3)=reshape(tempMatr,inSize(1),inSize(2));

else
	RGBMatrix=RawColors(ColorIndexes,:);

end
 

end
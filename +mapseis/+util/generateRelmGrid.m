function [TheGrid TripleGrid isInIt] = generateRelmGrid(Filterlist,Datastore,GridSpacing,MagSpace,DepthSpace,KeyPoint,noround)
	%Uses the standard griding and produces a grid in the Style of RELM
	%used as basis for many forecast models.
	%At the moment this is just a draft and will be extended later.
	%MagSpace,set the used Magnitude bins [minMag maxMag MagSpacing], if the 
	%MagSpacing is missing only one bin will be used. DepthSpace is the same
	%for the depth
	%Instead of a Filterlist and Datastore, two ranges can be used. In
	%this case 'Filterlist' is used for the Longitude range and 'Datastore' for
	%the Latitude range
	%If KeyPoint is not empty, the function will try to incomporate the point into 
	%the Grid, in this case the boundaries are treated more flexible and there maybe
	%nodes slighty beyond the boundaries
	
	
	import mapseis.region.*;
	
	useMag=true;
	useDepth=true
	
	if nargin<7
		noround=false;
	end
	
	if numel(MagSpace)==2
		useMag=false;
	end
	
	
	if numel(DepthSpace)==2
		useDepth=false;
	end
	
	if ~isnumeric(Filterlist)
	
		regionFilter = Filterlist.getByName('Region');
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange=filterRegion{1};
		
	
		
				
		if strcmp(RegRange,'all');
			[locations temp]=getLocations(Datastore,selected);
			lon=locations(:,1);
			lat=locations(:,2);
			bounded=[min(lon) max(lon) min(lat) max(lat)];
		else
			bounded=pRegion.getBoundingBox;
			
		end
	
	else
		bounded=[Filterlist(1) Filterlist(2) Datastore(1) Datastore(2)];
		pRegion = Region([bounded(1) bounded(3); bounded(2) bounded(3);...
					bounded(2) bounded(4); bounded(1) bounded(4)]);
	end	


	% If only one value of point separation is defined then use same separation
	% in both longitude and latitude
	if numel(GridSpacing)<2
   	 	GridSpacing(end+1) = GridSpacing(1);
   	end

   	%halfSpacing
   	hSpaceX=GridSpacing(1)/2;
   	hSpaceY=GridSpacing(2)/2;
   	
   
   	NoKey=true;	
   	if ~isempty(KeyPoint)
   		KeyInside=(bounded(1)<=KeyPoint(1)&KeyPoint(1)<=bounded(2))&...
   			(bounded(3)<=KeyPoint(2)&KeyPoint(2)<=bounded(4))
   		NoKey=~KeyInside;
   	end
   	
   	if NoKey
		% Ranges for grid
		xRange = (bounded(1):GridSpacing(1):bounded(2))';
		yRange = (bounded(3):GridSpacing(2):bounded(4))';
	else
		%There is a point which should be considered
		
		%xRange
		%------
		%left and right node point of KeyPoint
		lowPtX=KeyPoint(1)-hSpaceX;
		hiPtX=KeyPoint(1)+hSpaceX;
		
		%Number of nodes left and right
		NrLowPts=ceil(abs(lowPtX-bounded(1))/GridSpacing(1));
		NrHiPts=ceil(abs(bounded(2)-hiPtX)/GridSpacing(1));
		
		%new RangePoints
		lowRangeX=lowPtX-(NrLowPts*GridSpacing(1));
		hiRangeX=hiPtX+(NrHiPts*GridSpacing(1));
		xRange = (lowRangeX:GridSpacing(1):hiRangeX)';
		
		%yRange
		%------
		%bottom and top node point of KeyPoint
		lowPtY=KeyPoint(2)-hSpaceY;
		hiPtY=KeyPoint(2)+hSpaceY;
		
		%Number of nodes left and right
		NrLowPts=ceil(abs(lowPtY-bounded(3))/GridSpacing(2));
		NrHiPts=ceil(abs(bounded(4)-hiPtY)/GridSpacing(2));
		
		%new RangePoints
		lowRangeY=lowPtY-(NrLowPts*GridSpacing(2));
		hiRangeY=hiPtY+(NrHiPts*GridSpacing(2));
		yRange = (lowRangeY:GridSpacing(2):hiRangeY)';
		
	
	end
	
	
	%Round the values
	if ~noround
		Rounders=ceil(abs(log10(GridSpacing)))+1;
		xRange=round(xRange*10^Rounders(1))/10^Rounders(1);
		yRange=round(yRange*10^Rounders(2))/10^Rounders(2);
	end
	
	% Element counts of rows and columns
	rowCount = numel(xRange);
	colCount = numel(yRange);


	% Define 2D matrices of ranges
	[XX,YY] = meshgrid(xRange,yRange);
	elementCount = numel(XX)
	
	xvec=reshape(XX,elementCount,1);
	yvec=reshape(YY,elementCount,1);
		
	%Cut away all nodes not in the polygon
	if NoKey
		isInIt=pRegion.isInside([xvec,yvec]);
	else
		%allow a gridcell more to be init
		xvec2=xvec;
		yvec2=yvec;
		
		%this is done by moving all cells a have spaceing into the region
		xvec2(xvec2<=KeyPoint(1))=xvec2(xvec2<=KeyPoint(1))+hSpaceX;
		xvec2(xvec2>KeyPoint(1))=xvec2(xvec2>KeyPoint(1))-hSpaceX;
		yvec2(yvec2<=KeyPoint(2))=yvec2(yvec2<=KeyPoint(2))+hSpaceY;
		yvec2(yvec2>KeyPoint(2))=yvec2(yvec2>KeyPoint(2))-hSpaceY;
		
		isInIt=pRegion.isInside([xvec2,yvec2]);
	end
		
	xvec=xvec(isInIt);
	yvec=yvec(isInIt);
	elementCount=numel(xvec);
	disp(sum(isInIt));
	%reshape it to the matrix format
	isInIt=reshape(isInIt,length(yRange),length(xRange));
	
	%make TripleS input grid
	TripleGrid(:,1)=xvec;
	TripleGrid(:,2)=yvec;

	
	%Prepare Raw Grid
	RawGrid(:,1)=xvec-hSpaceX;
	RawGrid(:,2)=xvec+hSpaceX;
	RawGrid(:,3)=yvec-hSpaceY;
	RawGrid(:,4)=yvec+hSpaceY;


	if ~useDepth
		RawGrid(:,5)=DepthSpace(1);
		RawGrid(:,6)=DepthSpace(2);
	else
		
		NoKey=true;	
		if ~isempty(KeyPoint)
			if numel(KeyPoint)>2
				KeyInside=(DepthSpace(1)<=KeyPoint(3)&KeyPoint(3)<=DepthSpace(2));
				NoKey=~KeyInside;
			end
		end
		
		if NoKey
			DepthVec=(DepthSpace(1)-DepthSpace(3)/2):DepthSpace(3):(DepthSpace(2)+DepthSpace(3)/2);
		else
			%DepthRange
			%------
			DSpacer=DepthSpace(3)/2;
			%top and bottom  node point of KeyPoint
			lowPtZ=KeyPoint(3)-DSpacer;
			hiPtZ=KeyPoint(3)+DSpacer;
			
			%Number of nodes left and right
			NrLowPts=ceil(abs(lowPtZ-DepthSpace(1))/DepthSpace(3));
			NrHiPts=ceil(abs(DepthSpace(2)-hiPtZ)/DepthSpace(3));
			
			%new RangePoints
			lowRangeZ=lowPtZ-(NrLowPts*DepthSpace(3));
			hiRangeZ=hiPtZ+(NrHiPts*DepthSpace(3));
			DepthVec = (lowRangeZ:DepthSpace(3):hiRangeZ)';
		end
			
		DColLow=DepthVec(1:end-1);
		DColHi=DepthVec(2:end);
		numDep=numel(DepthVec)-1;
		NewRaw=[];
		
		
		for i=1:elementCount
			DepthBlock=repmat(RawGrid(i,:),numDep,1);
			DepthBlock(:,5)=DColLow;
			DepthBlock(:,6)=DColHi;
			
			NewRaw=[NewRaw;DepthBlock];
		end
		
		RawGrid=NewRaw;
		elementCount=numel(RawGrid(:,1));
	end
		
	
	if ~useMag
		RawGrid(:,7)=MagSpace(1);
		RawGrid(:,8)=MagSpace(2);
	
	else
		MagVec=(MagSpace(1)-MagSpace(3)/2):MagSpace(3):(MagSpace(2)+MagSpace(3)/2);
		MagColLow=MagVec(1:end-1);
		MagColHi=MagVec(2:end);
		numMag=numel(MagVec)-1;
		NewRaw=[];
		
		
		for i=1:elementCount
			MagBlock=repmat(RawGrid(i,:),numMag,1);
			MagBlock(:,7)=MagColLow;
			MagBlock(:,8)=MagColHi;
			
			NewRaw=[NewRaw;MagBlock];
		end
		
		RawGrid=NewRaw;
		elementCount=numel(RawGrid(:,1));
	
	end
	
	%finish it
	TheGrid=RawGrid;
	


end

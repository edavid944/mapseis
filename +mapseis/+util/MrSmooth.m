function [OutData] = MrSmooth(RefGrid, Data,smoothfactor,parallelmode)
	%This function smoothes values calculated on self refining grid, by calculation a 
	%mean value with the neighbour cells, the smoothfactor determine how many 
	%grid cells are used for the mean
	

	
	
	if any(RefGrid(:,5)==2);
	
		OneGrid=RefGrid(RefGrid(:,5)==1,:);
		TwoGrid=RefGrid(RefGrid(:,5)==2,:);
		OneData=Data(RefGrid(:,5)==1,:);
		TwoData=Data(RefGrid(:,5)==2,:);
		

		
		OutData=nan(numel(RefGrid(:,1)),1);
		
		
		elementCount = numel(RefGrid(:,1));
		
		OutData=OutData';
		
		if parallelmode 
			matlabpool open;
			
			parfor i=1:elementCount
				thisGridPoint = [RefGrid(i,1), RefGrid(i,2)];
				thisSpace=smoothfactor*[RefGrid(i,3),RefGrid(i,4)];
				
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2)));
				%inOne=((((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
				%	(((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))))&...
				%	((((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
				%	(((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))));
				if any(inOne)
					%disp('inOne')
					OutData(i)=nanmean(OneData(inOne));
				else
				inTwo=(((TwoGrid(:,1)-TwoGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2))|...
					((TwoGrid(:,1)+TwoGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2)))&...
					(((TwoGrid(:,1)+TwoGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2))|...
					((TwoGrid(:,1)-TwoGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2)))&...
					(((TwoGrid(:,2)-TwoGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2))|...
					((TwoGrid(:,2)+TwoGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2)))&...
					(((TwoGrid(:,2)+TwoGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2))|...
					((TwoGrid(:,2)-TwoGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2)));
					
					if any(inTwo)
						%disp('inTwo')
						OutData(i)=nanmean(TwoData(inTwo));
					end
				end	
			
			end
			
			matlabpool close;
			
		else
			for i=1:elementCount
				thisGridPoint = [RefGrid(i,1), RefGrid(i,2)];
				thisSpace=smoothfactor*[RefGrid(i,3),RefGrid(i,4)];
				
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2)));
				%inOne=((((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
				%	(((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))))&...
				%	((((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
				%	(((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))));
				if any(inOne)
					%disp('inOne')
					OutData(i)=nanmean(OneData(inOne));
				else
				inTwo=(((TwoGrid(:,1)-TwoGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2))|...
					((TwoGrid(:,1)+TwoGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2)))&...
					(((TwoGrid(:,1)+TwoGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2))|...
					((TwoGrid(:,1)-TwoGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2)))&...
					(((TwoGrid(:,2)-TwoGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2))|...
					((TwoGrid(:,2)+TwoGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2)))&...
					(((TwoGrid(:,2)+TwoGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2))|...
					((TwoGrid(:,2)-TwoGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2)));
					
					if any(inTwo)
						%disp('inTwo')
						OutData(i)=nanmean(TwoData(inTwo));
					end
				end	
		
			end
		end	
		OutData=OutData';
		
	else
	
	
		OneGrid=RefGrid(RefGrid(:,5)==1,:);
		OneData=Data(RefGrid(:,5)==1,:);
				
		
		OutData=nan(numel(RefGrid(:,1)),1);
		
		
		elementCount = numel(RefGrid(:,1));
		
		OutData=OutData';
		
		if parallelmode 
			matlabpool open;
			
			parfor i=1:elementCount
				thisGridPoint = [RefGrid(i,1), RefGrid(i,2)];
				thisSpace=smoothfactor*[RefGrid(i,3),RefGrid(i,4)];
				
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2)));
				
				if any(inOne)
					OutData(i)=nanmean(OneData(inOne));
				end	
			
			end
			
			matlabpool close;
			
		else	
		
			for i=1:elementCount
				thisGridPoint = [RefGrid(i,1), RefGrid(i,2)];
				thisSpace=smoothfactor*[RefGrid(i,3),RefGrid(i,4)];
				
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-thisSpace(1)/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+thisSpace(1)/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-thisSpace(2)/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+thisSpace(2)/2)));
				
				if any(inOne)
					OutData(i)=nanmean(OneData(inOne));
				end	
			
			end
		
		
		end
		OutData=OutData';	
	
	
	
	
	end





end

function DayMode(plotAxis)
	%This sets the plot window of whatever plot used to white background
	%and black lines
	%Opposite of NightMode
	
	if nargin<1
		plotAxis=gca;
	end

	
	%get figure of the plotAxis
	CurFig=get(plotAxis,'Parent');
	
	%Do the work
	set(plotAxis,'Color','w','XColor','k','YColor','k','ZColor','k')
	set(CurFig,'Color','w');
	
	

end

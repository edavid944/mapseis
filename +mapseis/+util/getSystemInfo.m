function SysInfo = getSystemInfo
	%returns some information about the system in a structure
	%Not sure if it works with every system
	
	%Empty Template
	SysInfo=struct(	'ArchString',[[]],...
			'MatlabVersion',[[]],...
			'KernelString',[[]],...
			'CPU_Info',[[]],...
			'Mem_Info',[[]]);
			
	PCMan=ispc;
	UnixMan=isunix;
	MacMan=ismac;
	
	%similar for all
	SysInfo.ArchString=computer;
	SysInfo.MatlabVersion=version;
	
	if PCMan
		%Not all determined now (I don't use windows)
		
	elseif MacMan
		
		[Stat,SysInfo.KernelString]=unix('uname -a');
		[Stat,SysInfo.CPU_Info]=unix('sysctl -a | grep machdep.cpu');
		[Stat,SysInfo.Mem_Info]=unix('sysctl -a | grep machdep.memmap');
	
	elseif UnixMan
		[Stat,SysInfo.KernelString]=unix('uname -a');
		[Stat,SysInfo.CPU_Info]=unix('cat /proc/cpuinfo');
		[Stat,SysInfo.Mem_Info]=unix('cat /proc/meminfo');
	end
	
	
	

end

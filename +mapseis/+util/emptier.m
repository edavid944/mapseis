function logVec=emptier(Vec)

	logVec=false(size(Vec));
	
	if iscell(Vec)    
		for i=1:numel(Vec)
			logVec(i)=isempty(Vec{i});
		end
	else
		%for i=1:numel(Vec)
			%logVec(i)=isempty(Vec(i));
		%end
		logVec=isempty(Vec);
	end
end
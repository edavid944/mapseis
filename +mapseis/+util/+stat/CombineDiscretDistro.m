function CombDist=CombineDiscretDistro(DistroA,DistroB,CutLong)
	%This function is the first version of Combiner for discrectized 
	%Forecast distribution. Both input variables should be vectors with 
	%index giving the probability of i-1 earthquakes occuring (DistroA(1) 
	%gives for instance the probability of having 0 earthquakes).The sum of 
	%each distro should be 1 (approximately)
	
	if nargin<3
		CutLong=false;
	end	
	
	%only one Distro -> CombDist=Distro
	if isempty(DistroA)
		CombDist=DistroA;	
		return	
	elseif isempty(DistroB)
		CombDist=DistroB;
		return
	end
	
	
	CombDist=[];
	CheckA=round(sum(DistroA)*100000)/100000;
	CheckB=round(sum(DistroB)*100000)/100000;
	if CheckA~=1|CheckB~=1
		disp(CheckA)
		disp(CheckB)
		error('Both input distribution must sum to 1')
	end
	
	
	%Determine how high the maximum events can be
	maxValA=numel(DistroA)-1;
	maxValB=numel(DistroB)-1;
	maxValComb=maxValA+maxValB+1;
	
	%generate raw Output
	CombDist=zeros(maxValComb,1);
	
	
	%first version is just the looped version, to check if it works, I guess
	%can vectorize it later if I need to gain some speed.
	
	for i=1:maxValA+1
		for j=1:maxValB+1
			newProb=DistroA(i)*DistroB(j);
			CombDist(i+j-1)=CombDist(i+j-1)+newProb;
		
		
		end
	end
	
	if CutLong
		%throw away everything after the cumnum reaches sum-1<realmin
		
		normComb=CombDist/sum(CombDist);
		Diff2One=1-cumsum(normComb);
		
		CrossTo0=find(Diff2One<=realmin);
		
		if ~isempty(CrossTo0)
			NewDist=CombDist(1:CrossTo0(1));
			CombDist=NewDist/sum(NewDist);
		end
	end	
	
	
end

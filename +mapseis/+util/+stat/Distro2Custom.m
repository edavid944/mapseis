function DiscretDistro = Distro2Custom(DistPara,DistType,NormIt)
	%This function allows to discretize a Poisson distribution or a 
	%negative binomal one.
	
	DiscretDistro = [];
	
	if nargin<3
		NormIt=false;
	end	
	
	switch DistType
		case 'Poisson'
			ValVec = 0:(ceil(DistPara*5)+2);
			DiscretDistro = pdf('poiss',ValVec,DistPara);
			
			if NormIt
				DiscretDistro=DiscretDistro/sum(DiscretDistro);
			end
			
		case 'NegBin'
			ValVec = 0:(ceil(DistPara(1)/DistPara(2)*5)+1);
			DiscretDistro = pdf('nbin',ValVec,DistPara(1),DistPara(2));
			
			if NormIt
				DiscretDistro=DiscretDistro/sum(DiscretDistro);
			end
	end	
	


end

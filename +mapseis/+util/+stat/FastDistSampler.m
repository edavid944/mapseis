function x = FastDistSampler(p, n)
	%same as discretesample, but simpler and faster	
	%Somebody on the mathworks forum suggested this, 
	%and I think is brilliant
	
	[~,x] = histc(rand(1,n),[0;cumsum(p(:))/sum(p)]);
	
	
	
end

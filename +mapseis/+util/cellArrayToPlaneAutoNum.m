function outStruct = cellArrayToPlaneAutoNum(inCellArray)
	% cellArrayToPlane : similar to cellArrayToPlane but changes suitable numerical and
	% logical fields to arrays instead of cell array
	
	
	% Author: David Eberhard
	
	
	% Get the fieldnames from the first struct
	fldNames = fieldnames(inCellArray{1});
	% Number of fields in output structure
	numFlds = numel(fldNames);
	% Number of elements in each array of output structure
	
	ConvIt=false(size(fldNames));
	%check for convertable fields fields
	for i=1:numFlds
		
		TestVal=inCellArray{1}.(fldNames{i});
		Temp=whos('TestVal');
		TheType=Temp.class;
		ConvIt(i) = any(strcmp(TheType,{'double','logical','integer','numeric'}))&all(size(TestVal)==1);
		
		
	end
	
	
	[rower coler]=size(inCellArray);
	
	if rower==1|coler==1
		numData = numel(inCellArray);
		
		
		for i=1:numFlds
		    tempData = cell(numData,1);
		    for j=1:numData
			tempData{j}=inCellArray{j}.(fldNames{i});
		    end
		    outStruct.(fldNames{i}) = tempData;
		end
	else
		numData = numel(inCellArray);
		
		inCellArray=inCellArray';
		for i=1:numFlds
		    tempData = cell(rower,coler);
		    tempData=tempData';
		    for j=1:numData
			tempData{j}=inCellArray{j}.(fldNames{i});
		    end
		    tempData=tempData';
		    outStruct.(fldNames{i}) = tempData;
		end	
		inCellArray=inCellArray';
	end	

	%convert the needed fields
	ToConvert=fldNames(ConvIt);
	for i=1:numel(ToConvert)
		outStruct.(ToConvert{i})=cell2mat(outStruct.(ToConvert{i}));
	end
	
	
end

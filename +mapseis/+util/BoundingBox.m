function axisLimits = BoundingBox(locs)
% BoundingBox : Returns the bounding box of [lon, lat] location array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

axisLimits = [min(locs(:,1)) max(locs(:,1)) min(locs(:,2)) max(locs(:,2))];

end
function SetProfileWidth(datastore,width)
% SetProfileWidth : set the profile width used by calculation

% 
% $Revision: 1 $    $Date: 2008-11-12
% Author: David Eberhard


if nargin<3
    key='ProfileWidth';
end

datastore.setUserData(key,width);

end
function x=isleap(Year)
%ISLEAP True for leap year.
%     ISLEAP(Year) returns 1 if Year is a leap year and 0 otherwise.
%     ISLEAP is only set for gregorian calendar, so Year >= 1583
%
% Syntax: 	ISLEAP(YEAR)
%      
%     Inputs:
%           YEAR - Year of interest (default = current year). 
%           You can input a vector of years.
%     Outputs:
%           Logical vector.
%
%      Example: 
%
%           Calling on Matlab the function: isleap
%
%           Answer is: 0
%
%
%           Calling on Matlab the function: x=isleap([2007 2008])
%
%           Answer is:
%           x = 0 1
%
%           Created by Giuseppe Cardillo
%           CEINGE - Advanced Biotechnologies
%           Via Comunale Margherita, 482
%           80145
%           Napoli - Italy
%           cardillo@ceinge.unina.it


%Input Error handling
switch nargin
    case 0
        Year=str2double(datestr(date,10));
    case 1
        checkyear(Year)
end

% The Gregorian calendar has 97 leap years every 400 years: 
% Every year divisible by 4 is a leap year. 
% However, every year divisible by 100 is not a leap year. 
% However, every year divisible by 400 is a leap year after all. 
% So, 1700, 1800, 1900, 2100, and 2200 are not leap years, 
% but 1600, 2000, and 2400 are leap years.
A=mod(Year,4);
B=mod(Year,100);
C=mod(Year,400);
x=zeros(size(Year));
i=sort(find(C==0 | (A==0 & B>0)));
if isempty(i)==0
    x(i)=1;
end
return

function checkyear(Year)
num=isnumeric(Year);
fin=isfinite(Year);
if num==0 | fin(fin==0)
    error('Warning: Year values must be numeric and finite')
end
L=Year-floor(Year);
if L(L>0)
    error('Warning: Year values must be integer')
end
L=Year-1583;
if L(L<0)
    error('Warning: Every value of Year must be >1582')
end
return
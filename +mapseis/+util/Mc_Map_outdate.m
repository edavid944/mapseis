classdef Mc_Map_outdate < handle
		
	%This class is the data container for Mc maps, it allows to store a 4D Mc map 
	%and will return every Mc value for an inputed lon,lat,z,time position
	
	
	%THIS IS A OLD UNFINISHED VERSION WHICH IS NOT NEEDED ANYMORE,IT IS JUST THERE IN
	%CASE SOME PARTS OF IT ARE NEEDED.
	
	
	
	properties
		Mc_Maps
		std_Mc_Maps
		GridSpacings
		Meshed
		R_N_Map %should contain Radius and Number in some way
		MapDimension
		MapType
		Nr_Steps
		Start_Time
		End_Time
		Step_Times
		Interpol
		InterpolType
		Smoothing
		maxSelWidth
	end

	
	methods
	
	function obj = Mc_Map_outdate(Mc_Maps,std_Mc_Maps,GridSpacings,Meshed,MapType,Step_Times,Start_Time,End_Time,Orientation)
		%This will initalize the Mc map
		
		%all can be empty in this case the object will be creates as an 
		%empty map
		
		%R_N_Map should contain Radius and Number in some way, needed for
		%for some interpolation methods, but is not entirely sure what is 
		%exactly needed. I will define it as soon as I know.
		
		%GridSpacings contains the spacing of grids used in Mc_Maps, it can be
		%empty in this case the spacing will be set with the gridnode distance
		%Format gridspacing: cell array {num. Maps,3}: {lon lat depth}
		%Automatic gridspacing works only in regular grid, in the other cases, 
		%spacing should be included.
		
		%Meshed should be set true if the coordinates are already meshed, means every 
		%point is in the vectors, this is mostly the case with irregular grids.
		%If left empty, not meshed grids are assumed and Meshed is set to false.
		
		%Orientation is needed in case of lines and depth profile it is a matrix 2x3 or 2x2 with first line
		%start point second line end point (lon,lat,depth)
		
		% -------------------------------------------------------------------------------------------------------------
		%|Dev Note:												       |
		%|There is a function called poly2mask in the image processing toolbox, which might help with some problems.   |
		%|It allows to select gridcells with a polygon.The problem is it is only 2D and it is cartesian flat surface,  |
		%|This might cause problems on a spherical surface.							       |
		% -------------------------------------------------------------------------------------------------------------
		
		
		%init
		obj.Mc_Maps=[];
		obj.std_Mc_Maps=[];
		obj.R_N_Map=[];
		obj.GridSpacings=[];
		obj.MapType=[];
		obj.Nr_Steps=[];
		obj.Start_Time=[];
		obj.End_Time=[];
		obj.Step_Times=[];
		obj.Interpol=false;
		obj.InterpolType='normal'; %for later use
		obj.Smoothing='none';
		obj.Meshed=false;
		obj.Orientation=[];
		obj.maxSelWidth=[];
		
		%write values if needed
		if ~isempty(Mc_Maps)
			obj.Mc_Maps=Mc_Maps;
			obj.MagType=MagType;
			obj.Nr_Steps=numel(Mc_Maps(:,1));
			
			%check the dimension of the map
			if ~isempty(McMap{1,2})&~isempty(McMap{1,3})&...
				~isempty(McMap{1,4})
				obj.MapDimension='3D';
			elseif	~isempty(McMap{1,2})&~isempty(McMap{1,3})
				obj.MapDimension='2D';
			elseif	~isempty(McMap{1,3})&~isempty(McMap{1,4})
				obj.MapDimension='2DSlice';
			elseif 	~isempty(McMap{1,2})
				obj.MapDimension='Line';
			else
				%all empty
				obj.MapDimension='Constant';
			end
			
			if ~isempty(Meshed)
				obj.Meshed=Meshed;
			end	
			
			
			if ~isempty(Orientation)
				obj.Orientation=Orientation;
			end
			
			
			if ~isempty(GridSpacings)
				obj.GridSpacings=GridSpacings;
			else
				%build gridspacing by taking the difference between 
				%first and second node 
				
				if obj.Meshed
					for i=1:numel(obj.McMap(:,1))
						if ~isempty(obj.McMap(i,2)) 
							vecc=unique(obj.McMap{i,2});
							sort_vec=sort(vecc);
							obj.GridSpacings{i,1}=ones(numel(obj.McMap{i,2}),1)*...
										abs(sort_vec(2)-sort_vec(1));
						else
							obj.GridSpacings{i,1}=[];
										
						end
						
						if ~isempty(obj.McMap(i,3)) 
							vecc=unique(obj.McMap{i,3});
							sort_vec=sort(vecc);
							obj.GridSpacings{i,2}=ones(numel(obj.McMap{i,3}),1)*...
										abs(sort_vec(2)-sort_vec(1));
						else
							obj.GridSpacings{i,2}=[];
											
						
						end
						
						if ~isempty(obj.McMap(i,4)) 
							vecc=unique(obj.McMap{i,4});
							sort_vec=sort(vecc);
							obj.GridSpacings{i,3}=ones(numel(obj.McMap{i,4}),1)*...
										abs(sort_vec(2)-sort_vec(1));
						else
							obj.GridSpacings{i,3}=[];
											
						
						end
						
					end
				
				
				else
					
					for i=1:numel(obj.McMap(:,1))
						if ~isempty(obj.McMap(i,2)) 
							sort_vec=obj.McMap{i,2}
							obj.GridSpacings{i,1}=ones(numel(obj.McMap{i,2}),1)*...
										abs(sort_vec(2)-sort_vec(1));
						else
							obj.GridSpacings{i,1}=[];
										
						end
						
						if ~isempty(obj.McMap(i,3)) 
							sort_vec=obj.McMap{i,3}
							obj.GridSpacings{i,2}=ones(numel(obj.McMap{i,3}),1)*...
										abs(sort_vec(2)-sort_vec(1));
						else
							obj.GridSpacings{i,2}=[];
											
						
						end
						
						if ~isempty(obj.McMap(i,4)) 
							sort_vec=obj.McMap{i,4}
							obj.GridSpacings{i,3}=ones(numel(obj.McMap{i,4}),1)*...
										abs(sort_vec(2)-sort_vec(1));
						else
							obj.GridSpacings{i,3}=[];
											
						
						end
						
					end
				
				
				
				end
				
			end
			
			
			
			
			%set the maximum possible selection width for profiles and lines
			minLon=[];
			minLat=[];
			maxLon=[];
			maxLat=[];
			
			for i=1:numel(obj.McMap(:,1))
				%get min max
				minLon=min([minLon,min(obj.McMap{i,2}]);
				maxLon=max([maxLon,max(obj.McMap{i,2}]);
				minLat=min([minLat,min(obj.McMap{i,3}]);
				maxLat=max([maxLat,max(obj.McMap{i,3}]);
			end
			
			obj.maxSelWidth=deg2km(distance(minLat,minLon,maxLat,maxLon));
			
			
		end
		
		if ~isempty(std_Mc_Maps)
			%has to be the same dimension as Mc_maps, and the same number 
			%of maps, but the coordinates Maps{:,2:4} are not needed, as
			%they should be similar to Mc_Maps
			obj.std_Mc_Maps=std_Mc_Maps;
		end	
		
		
		if ~isempty(Step_Times)
			%actually only needed in time varying maps
			obj.Step_Times=Step_Times;
			obj.Start_TIme=Start_Time;
			obj.End_Time=End_Time;
		end
		
		
		
		
	
	
	end
	
	function setInterpol(obj,newstate)
		%allows to use interpolation in the time axis if set to on
		switch newstate
			case 'on'
				obj.Interpol=true;
			case 'off'
				obj.Interpol=false;
			case 'toggle'
				obj.Interpol=~obj.Interpol;
		end	
		
	
	end	
	
	
	function AddMap(obj,NewMap,std_Mc_Maps,EndTimeStep)
		%This allows to add a whole new map at once, steps will be added
		%and MapType changed if necessary
		%A Map consists of a cell array with NewMap{1,1}=Mc, NewMap{1,2}=lon
		%NewMap{1,3}=lat and NewMap{1,4}=z in case of 3D maps.
		%if an axis is not needed it should be left empty, this will tell the
		%object which dimension a map has
		
		
		
		
		
	
	end
	
	function AddMcVal(obj,value,Position)
		%This function will let a single value add to the existing map
		
	end
	
	
	
	function NewEmptyMap(obj,Coords,timestep)
		%This function will add a new empty map with defined Coordinates
		%{lon,lat,z} at the given time
		
	
	end
	
	
	
	function [map coords Spacing] = GetWholeMap(obj,time)
		%returns a map in the selected time or the "nearest map"
		%use GetInterpolatedMap for an interpolation on the time axis
		if strcmp(obj.MapType,'time_var')
			%check if the time is in the interval
			if sum(obj.Step_Times==time)>=1
				TimePos=obj.Step_Times==time;
			elseif obj.Start_Time==time
				TimePos=1;
			elseif obj.end_Time==time
				TimePos=numel(obj.McMaps(:,1));
			else	
			%check for the nearest map 
				timediff=abs(obj.Step_Times-time);
				[val TimePos]=min(timediff);
				
				%check if start time or end time is nearer
				startdiff=abs(obj.Start_Time-time);
				enddiff=abs(obj.End_Time-time);
				
				if startdiff<=val
					TimePos=1;
				end
				
				if enddiff<=val
					TimePos=numel(obj.McMaps(:,1));	
				end
			end
		
			%now do the same as in the constant type
			map=obj.Mc_Maps{TimePos,1}
			
			switch obj.MapDimension
				case '3D'
					coords = {obj.Mc_Maps{TimePos,2}),obj.Mc_Maps{TimePos,3},obj.Mc_Maps{TimePos,4}};
					Spacing={obj.GridSpacings{TimePos,1},obj.GridSpacings{TimePos,2},obj.GridSpacings{TimePos,3}};

				case '2D'
					coords = {obj.Mc_Maps{TimePos,2}),obj.Mc_Maps{TimePos,3}};
					Spacing={obj.GridSpacings{TimePos,1},obj.GridSpacings{TimePos,2}};
					
				case '2DSlice'
					coords = {obj.Mc_Maps{TimePos,3}),obj.Mc_Maps{TimePos,4}};
					Spacing={obj.GridSpacings{TimePos,2},obj.GridSpacings{TimePos,3}};
					
				case 'Line'
					coords = obj.Mc_Maps(TimePos,2);
					Spacing = obj.GridSpacings(TimePos,1);
					
				case 'Constant'
					coords=[];
					Spacing=[];
			
					
			end
			
			
		
		
		elseif strcmp(obj.MapType,'constant')
			%time is not needed in that case, just return the one and only map
			map=obj.Mc_Maps{1,1}
			
			switch obj.MapDimension
				case '3D'
					coords = {obj.Mc_Maps{1,2}),obj.Mc_Maps{1,3},obj.Mc_Maps{1,4}};
					Spacing={obj.GridSpacings{1,1},obj.GridSpacings{1,2},obj.GridSpacings{1,3}};
					
				case '2D'
					coords = {obj.Mc_Maps{1,2}),obj.Mc_Maps{1,3}};
					Spacing={obj.GridSpacings{1,1},obj.GridSpacings{1,2}};
					
				case '2DSlice'
					coords = {obj.Mc_Maps{1,3}),obj.Mc_Maps{1,4}};
					Spacing={obj.GridSpacings{1,2},obj.GridSpacings{1,3}};
					
				case 'Line'
					coords = obj.Mc_Maps(1,2);
					Spacing=obj.GridSpacings(1,1);
					
				case 'Constant'
					coords=[];
					Spacing=[];
					
			end
			
		end
		
		
		
	end
	
	
	
	function [map] = GetInterpolatedMap(obj,time,coords,Spacing,meshed,InterType,slice_depth,Selwidth)
		%similar to GetWholeMap, but will send back a resampled and interpolated
		%map, according to the entered time(frame) and coordinates.
		%time: either a single value or a array with two values for a timeframe
		%coords: first column lon, second lat third depth (z), on is left empty
		%	 only 2D or 1D map or constant will be returned, the last column 
		%	 can also be missing, this will be the same as an empty third column
		%Spacing: Same format as coords, contains the grid spacing for every gridnode
		%	  If left empty, the function will set the spacing itself, but this can only be done
		%	  in case of regular grid, where the gridspacing is the same in every node.
		%meshed: If set to false, the function will mesh the data back as map instead as vector
		%	 If set to true the coords should be already meshed, means every point is in the vectors
		%	 this has the advantages that also irregular grids are supported, the data will then be 
		%	 returned as vector of the length of the input vectors.
		%InterType: sets the interpolation resampling schema used for the map
		%	    	-simple:Will take the nearest value defined for Mc 
		%		-mean: 	a simple mean value of all grid cells in contained in the
		%			new grid cell
		%		-area: 	also a mean value but weigthed with the area/volume of the 
		%		       	cells cut by the new grid cell
		%		-radius:will use the radius / numberEq for the weigthing, has to be 
		%			present in the object. (do not know how to do it now)
		%		-stdDev:use the standard deviation of the calculation for the weigths
		%			again the standard deviation has to be present.
		%Other methods may be added or existing removed if necessary
		
		%mesh a grid first which defines all points wanted. The gridpoints will be defined in the
		%middle of the cell and the spacing 
		
		%init values
		lon=[];
		lat=[];
		depth=[];
		strike=[];
		d_lon=[];
		d_lat=[];
		d_depth=[];
		d_strike=[];
		
		
		if ~meshed
			if ~isempty(coords{1})&~isempty(coords{2})&...
				~isempty(coords{3})
				newDim='3D';
				[lon lat depth]=meshgrid(coords{1},coords{2},coords{3});
				
				
				dime=size(lon);
				le=sum(dime)
				
				lon=reshape(lon,le,1);
				lat=reshape(lat,le,1);
				depth=reshape(depth,le,1);
				
				%do the spacing
				if isempty(Spacing)
					d_lon=ones(le,1)*abs(coords{1}(2)-coords{1}(1));
					d_lat=ones(le,1)*abs(coords{2}(2)-coords{2}(1));
					d_depth=ones(le,1)*abs(coords{3}(2)-coords{3}(1));
				else
					[d_lon d_lat d_depth]=meshgrid(Spacing{1},Spacing{2},Spacing{3});
					d_lon=reshape(d_lon,le,1);
					d_lat=reshape(d_lat,le,1);
					d_depth=reshape(d_depth,le,1);
				end
				
				
			elseif	~isempty(coords{1})&~isempty(coords{2})
				newDim='2D';
				[lon lat]=meshgrid(coords{1},coords{2});
				
				dime=size(lon);
				le=sum(dime)
				
				lon=reshape(lon,le,1);
				lat=reshape(lat,le,1);
				
				%do the spacing
				if isempty(Spacing)
					d_lon=ones(le,1)*abs(coords{1}(2)-coords{1}(1));
					d_lat=ones(le,1)*abs(coords{2}(2)-coords{2}(1));
					
				else
					[d_lon d_lat]=meshgrid(Spacing{1},Spacing{2});
					d_lon=reshape(d_lon,le,1);
					d_lat=reshape(d_lat,le,1);
					
				end
				
				if isempty(slice_depth)
					slice_depth=0;
				end
				
				if SelWidth==inf|isempty(SelWidth)
					%There should be many data below 300km
					SelWidth=300;
				end
					
				
				
			elseif	~isempty(coords{2})&~isempty(coords{3})
				newDim='2DSlice';
				[strike depth]=meshgrid(coords{2},coords{3});
				
				dime=size(strike);
				le=sum(dime)
				
				strike=reshape(strike,le,1);
				depth=reshape(depth,le,1);
				
				%do the spacing
				if isempty(Spacing)
					d_strike=ones(le,1)*abs(coords{2}(2)-coords{2}(1));
					d_depth=ones(le,1)*abs(coords{3}(2)-coords{3}(1));
				else
					[d_strike d_depth]=meshgrid(Spacing{2},Spacing{3});
					d_strike=reshape(d_strike,le,1);
					d_depth=reshape(d_depth,le,1);
				end
				
				if SelWidth==inf|isempty(SelWidth)
					SelWidth=obj.maxSelWidth;
				end
				
				
			elseif 	~isempty(coords{1})
				newDim='Line';
				strike=coords{1};
				
				dime=numel(strike);
				
				if isempty(Spacing)
					d_strike=ones(dime,1)*abs(coords{1}(2)-coords{1}(1));
					
				else
					d_strike=Spacing{1};

				end
				
				if SelWidth==inf|isempty(SelWidth)
					SelWidth=obj.maxSelWidth;
				end
			
			else
				%all empty
				newDim='Constant';
				
				dime=1;
				
			end
			
			
		else
			if ~isempty(coords{1})&~isempty(coords{2})&...
				~isempty(coords{3})
				newDim='3D';
				lon=coords{1};
				lat=coords{2};
				depth=coords{3};
				
				%do the spacing
				if isempty(Spacing)
					d_lon=ones(numel(lon),1)*abs(coords{1}(2)-coords{1}(1));
					d_lat=ones(numel(lat),1)*abs(coords{2}(2)-coords{2}(1));
					d_depth=ones(numel(depth),1)*abs(coords{3}(2)-coords{3}(1));
				else
					
					d_lon=Spacing{1};
					d_lat=Spacing{2};
					d_depth=Spacing{3};
				end
					
				
			elseif	~isempty(coords{1})&~isempty(coords{2})
				newDim='2D';
				lon=coords{1};
				lat=coords{2};
				
				%do the spacing
				if isempty(Spacing)
					d_lon=ones(numel(lon),1)*abs(coords{1}(2)-coords{1}(1));
					d_lat=ones(numel(lat),1)*abs(coords{2}(2)-coords{2}(1));
					
				else
					d_lon=Spacing{1};
					d_lat=Spacing{2};
					
				end
				
				if isempty(slice_depth)
					slice_depth=0;
				end
				
				if SelWidth==inf|isempty(SelWidth)
					%There should be many data below 300km
					SelWidth=300;
				end
				
				
			elseif	~isempty(coords{2})&~isempty(coords{3})
				newDim='2DSlice';
				[strike depth]=meshgrid(coords{2},coords{3});
				
				strike=coords{2};
				depth=coords{3};
				
				%do the spacing
				if isempty(Spacing)
					d_strike=ones(numel(strike),1)*abs(coords{2}(2)-coords{2}(1));
					d_depth=ones(numel(depth),1)*abs(coords{3}(2)-coords{3}(1));
				else
					d_strike=Spacing{2};
					d_depth=Spacing{3};
					
				end
				
				if SelWidth==inf|isempty(SelWidth)
					SelWidth=obj.maxSelWidth;
				end
				
				
			elseif 	~isempty(coords{1})
				newDim='Line';
				strike=coords{2};
				
				%do the spacing
				if isempty(Spacing)
					d_strike=ones(numel(strike),1)*abs(coords{2}(2)-coords{2}(1));
					
				else
					d_strike=Spacing{1};
					
				end
				
				if SelWidth==inf|isempty(SelWidth)
					SelWidth=obj.maxSelWidth;
				end
							
			
			else
				%all empty
				newDim='Constant';
				
				dime=1;
				
			end
		
		end
		
		%=====================================
		
		%finally the interpolation can be done
		
		%pack everything needed into a structure
		coordStruct=struct(	'lon',lon,...
				   	'lat',lat,...
				   	'depth',depth,...
				   	'strike',strike,...
				   	'd_lon',d_lon,...
				   	'd_lat',d_lat,...
				   	'd_depth',d_depth,..
				   	'd_strike',d_strike,...
				   	'SelWidth',SelWidth,...
				   	'slice_depth',slice_depth);
		
		
		
		rawmap=obj.MapInterpolator(InterType,newDim,time,coordStruct);
		
		
		
		
		
		
		
	
	end
	
	
	
	function Map = MapInterpolator(obj,InterType,newGridType,time,newMapCoord)
		%This function does the actuall interpolation, it could be integrated in
		%GetInterpolatedMap but this way it is more readable.
		%newMapCoord is a structure containing all the necessary coordinates
		%(lon,lat,depth,strike,d_lon,d_lat,d_depth,d_strike)
		
		%unpack
		lon=newMapCoord.lon;
		lat=newMapCoord.lat;
		depth=newMapCoord.depth;
		strike=newMapCoord.strike;
		d_lon=newMapCoord.d_lon;
		d_lat=newMapCoord.d_lat;
		d_depth=newMapCoord.d_depth;
		d_strike=newMapCoord.d_strike;
		SelWidth=newMapCoord.SelWidth;
		slice_depth=newMapCoord.slice_depth;
		
		
		%Dev note:
		%---------
		%possible cases: 1.	3D to 3D,2D,2DSlide,Line,Constant
		%		 2.	2D to 3D,2D,2DSlide,Line,Constant
		%		 3.	2DSlide to 3D,2D,constant and 
		%			2DSlide and Line if direction similar
		%		 4.	Line to 3D,2D,constant and 
		%			2DSlide and Line if direction similar
		%		 5.	Constant to anything
		%Some of this possible conversion probably do not make much sense, as 
		%the conversion from line to 3D.
		
		%The principle is easy only 3 coordinates are really existing and so 3 spacings
		%(expect time, which is special anyhow). So if one or two dimension are missing, the spacing for
		%them is also 
		
		
		
		
		%probably will use different subroutines.
		switch InterType
				case 'simple'
					
				case 'mean'
				
				case 'radius'
			
				case 'stdDev'
		end	
	
	
	end
	
	
	function McVal = GetSingleMc(obj,pos,time,InterType)
		%Name says it, it will return the Magnitude of completeness for a 
		%certain position in time and space. The dimension does not matter, 
		%the algorithmen will return what is available.
		%InterType is similar to InterType in GetInterpolatedMap
		
		if isempty(InterType)
			InterType='simple';
		end
		
		switch InterType
			case 'simple'
			
			case 'mean'
			
			case 'radius'
		
			case 'stdDev'
		end	
		
		
	
	end
	
	
	function Smoothermap(obj,dim,SmoothFactor)
		%this will apply a smoothing filter to the data, either spatially
		%dim='spatial', timedirection, dim='timedir', or both, dim='both';
		
		%needs to be rewritten for meshed maps, so not for use now.
		
		
		
		hFilter = fspecial('gaussian', 5, 2.5);
		
		
		
		switch dim
			case 'spatial'
				for i=1:obj.NrSteps
					obj.Mc_Maps{i,1}=imfilter((obj.Mc_Maps{i,1} + SmoothFactor), hFilter, 'replicate');
				end
				
				
			case 'timedir'
				%most difficult one
				
				if strcmp(obj.MapType,'time_var')
					
					%steps:
						%-build datacubus
						%-process in time direction
						%-write back into cell array 
				
					switch obj.MapDimension
						case '3D'
							for i=1:obj.NrSteps
								datacubus(:,:,:,i)=obj.Mc_Maps{i,1};
							end
							
							%process
							for i=1:numel(obj.Mc_Maps{1,1}(:,1,1))
								for j=1:numel(obj.Mc_Maps{1,1}(1,:,1))
									for k=1:numel(obj.Mc_Maps{1,1}(1,1,:))
										datacubus(i,j,k,:)=imfilter((datacubus(i,j,k,:) + SmoothFactor), hFilter, 'replicate');		
									end
								end
							end
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(:,:,:,i);
							end
							
							
							
						case '2D'
							for i=1:obj.NrSteps
								datacubus(:,:,i)=obj.Mc_Maps{i,1};
							end
							
							%process
							for i=1:numel(obj.Mc_Maps{1,1}(:,1))
								for j=1:numel(obj.Mc_Maps{1,1}(1,:))
									datacubus(i,j,:)=imfilter((datacubus(i,j,:) + SmoothFactor), hFilter, 'replicate');		
								end
							end
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(:,:,i);
							end
							
						
						case '2DSlice'
							for i=1:obj.NrSteps
								datacubus(:,:,i)=obj.Mc_Maps{i,1};
							end
							
							%process
							for i=1:numel(obj.Mc_Maps{1,1}(:,1))
								for j=1:numel(obj.Mc_Maps{1,1}(1,:))
									datacubus(i,j,:)=imfilter((datacubus(i,j,:) + SmoothFactor), hFilter, 'replicate');		
								end
							end
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(:,:,i);
							end
						
							
						case 'Line'
							for i=1:obj.NrSteps
								datacubus(:,i)=obj.Mc_Maps{i,1};
							end
							
							for i=1:numel(obj.Mc_Maps{1,1}(:))
								
									datacubus(i)=imfilter((datacubus(i) + SmoothFactor), hFilter, 'replicate');		
							end
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(;,i);
							end
							
							
						case 'Constant'
							for i=1:obj.NrSteps
								datacubus(i)=obj.Mc_Maps{i,1};
							end
							
							datacubus=imfilter((datacubus + SmoothFactor), hFilter, 'replicate');		
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(i);
							end
						
					end
					
				end
			
			case 'both'
				switch obj.MapDimension
						case '3D'
							for i=1:obj.NrSteps
								datacubus(:,:,:,i)=obj.Mc_Maps{i,1};
							end
							
							datacubus=imfilter((datacubus + SmoothFactor), hFilter, 'replicate');
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(:,:,:,i);
							end
							
							
						case '2D'
							for i=1:obj.NrSteps
								datacubus(:,:,i)=obj.Mc_Maps{i,1};
							end
							
							datacubus=imfilter((datacubus + SmoothFactor), hFilter, 'replicate');
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(:,:,i);
							end
							
						
						case '2DSlice'
							for i=1:obj.NrSteps
								datacubus(:,:,i)=obj.Mc_Maps{i,1};
							end
							
							datacubus=imfilter((datacubus + SmoothFactor), hFilter, 'replicate');
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(:,:,i);
							end
							
						
						case 'Line'
							for i=1:obj.NrSteps
								datacubus(:,i)=obj.Mc_Maps{i,1};
							end
							
							datacubus=imfilter((datacubus + SmoothFactor), hFilter, 'replicate');
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(;,i);
							end
							
							
						case 'Constant'
							for i=1:obj.NrSteps
								datacubus(i)=obj.Mc_Maps{i,1};
							end
							
							datacubus=imfilter((datacubus + SmoothFactor), hFilter, 'replicate');
							
							%write back
							for i=1:obj.NrSteps
								obj.Mc_Maps{i,1}=datacubus(i);
							end
							
					end
			
			
			
			
		end
		
		obj.Smoothing=dim;
		
		%Maybe the stdDev_Mc_Map should also be smoothed?
		
		
	end
	
	


	
	end	


	methods (static)
	
	function [lat lon depth d_lat d_lon d_depth] = Strike2Geo(workmode,orientation,Strike,d_Strike,Depth,d_Depth,selWidth)
		%This function transforms a line or a depth profile with strike, orientation and depth into a 
		%lon, lat, depth based line or profile.
		
		%convert km into deg
		Strike=km2deg(Strike);
		d_Strike=km2deg(d_Strike);
		selWidth=km2deg(selWidth);
		Depth=km2deg(Depth);
		d_Depth=km2deg(d_Depth);
		
		switch workmode
			case 'profile'
				%first get the azimuth (angle) between point 1 and 2 in the orientation
				%azimuth(lat1,lon1,lat2,lon2)
				azimuthangle=azimuth('gc',orientation(1,2),orientation(1,1),orientation(2,2),orientation(2,1));
				
				LonZero=orientation(1,1);
				LatZero=orientation(1,2);
				
				%now convert the the strike cell to lat,lon cells
				for i=1:numel(Strike)
					[lat(i),lon(i)] = reckon(LatZero,LonZero,Strike(i),azimuthangle);
					
				
				end
				
				
			case 'line'
				%first get the azimuth (angle) between point 1 and 2 in the orientation
				%azimuth(lat1,lon1,lat2,lon2)
				[elevationangle,slantrange,azimuthangle] = elevation(orientation(1,2),orientation(1,1),orientation(1,3)*1000,...
										orientation(2,2),orientation(2,1)orientation(2,3)*1000);
				
				
				
				
				
		end		
		
		
	
	end
	
	
	
	
	end
	
	
end		

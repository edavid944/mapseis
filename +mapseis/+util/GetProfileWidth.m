function [width]=GetGridParameters(datastore,key)
% GetProfileWidth : returns the profile width used by calculation

% 
% $Revision: 1 $    $Date: 2008-11-12
% Author: David Eberhard
if nargin<2
    key='ProfileWidth';
end

try
    width = datastore.getUserData(key);

catch ME
    % Key not found, use default values and warn user
    width = 0.5;
    
    warning('GetGridParameters:unknown_key',['Unknown key: ', key]);
end


end
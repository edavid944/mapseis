%% Test grid script

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

gridFuns = {@gridPoints,@hexGridPoints,@closePackedGridPoints};
gfInd = 3;

sep = 0.03;
t = linspace(0,2*pi,100)';
pg = polyGrid([cos(t) sin(t)],gridFuns{gfInd});
gp = pg.getGrid(sep);
bPts = pg.getBoundary();



plot(gp(:,1),gp(:,2),'b.',bPts(:,1),bPts(:,2),'k--')

%% Draw cells around grid points
numGridPts = numel(gp(:,1));
for i=1:numGridPts
    thisPt = gp(i,:);
    if gfInd==1
        thetaVals = (0:4)/2*pi+pi/4;
        lenVal = sep/sqrt(2);
    elseif gfInd == 3
        thetaVals = (0:6)/3*pi+pi/6;
        lenVal = sep/sqrt(3);
    end
    xVect = cos(thetaVals)*lenVal;
    yVect = sin(thetaVals)*lenVal;
    line(thisPt(:,1) + xVect, thisPt(:,2) + yVect);
end
axis(1.1*[-1 1 -1 1])
axis square
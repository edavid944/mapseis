function funStruct = hexGridPoints(varargin)
% gridPoints : creates sets of grid points on a hexagonal grid

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Attributes

% Bounding box is specified as [minX maxX minY maxY]
gPts = gridPoints(varargin{:});

%% Interface
funStruct = struct(...
    'getGrid',@nGetGrid,... % get a grid of points with the specified spacing
    'setBoundingBox',@nSetBoundingBox); % Set the bounding box of the grid region

%% Public Methods

    function pts = nGetGrid(sep)
        % Create a hex grid of points returned as [x y] matrix with the
        % specified spacing
        gp1 = gPts.getGrid(sep*[3 sqrt(3)]);
        gp2 = gp1+repmat(sep*[0.5 sqrt(3)/2],numel(gp1(:,1)),1);
        gp3 = gp1+repmat(sep*[2 0],numel(gp1(:,1)),1);
        gp4 = gp1+repmat(sep*[1.5 sqrt(3)/2],numel(gp1(:,1)),1);
        pts = [gp1; gp2; gp3; gp4];
    end

    function nSetBoundingBox(newBB)
       % Set the bounding box of the grid.
       % newBB = [minX maxX minY maxY]
       gPts.setBoundingBox(newBB);
    end

%% Private Methods

end
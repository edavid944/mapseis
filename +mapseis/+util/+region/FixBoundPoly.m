function [FixedPoints UndoData] = FixBoundPoly(pts,workmode,offsets,LineMode,PrevRun)
	%This function separates a polygon region which crosses over 180/-180 
	%and 90/-10 boundaries into two or more subregions. It uses the more
	%likely solution producable by human hand (means no overborder lines in
	%the coordinate system the polygon was selected in) 
	
	%workmode: 	can be 'cut' for exclude points over -180/180 and -90/90 
	%	 	(e.g. 212, 95) and replace them with points at the border, also
	%		'cut' exclude every points perviously added if UndoData is present
	%		'Shifted' and 'UnShifted' can only be used in case of an offset, it will make
	%		two zone from one in cases the jump over the borders is
	%		due to an offset. Use 'Shifted' if the points inputed are shifted
	%		use 'UnShifted' if the points inputed are already shifted back
	%		The result will be in the original unshifted reference frame
	%		
	%		I may should a mode for circles if needed
	
	%offsets are only needed in case the map is shifted and workmode is set
	%to 
	%LineMode is needed in case it only is a line, will be important when there
	%are multi segment lines
	%The UndoData/PrevRun is needed for changing the polygon again and to prevent
	%multiple additions of point, if this is not present, equal points will be piled
	%up after every run.
	
	import mapseis.util.ShiftCoords;
	
	if nargin<3
		workmode='cut';
		offsets=[0,0];
		LineMode=false;
		PrevRun=[];
	end
	
	if nargin<4
		LineMode=false;
		PrevRun=[];
	end
	
	if nargin<5
		PrevRun=[];
	end
		
	if isempty(offsets)
		offsets=[0,0];
	end	
	
	
	%Remove points from previous runs
	if ~isempty(PrevRun)
		%A bit complicated, I may find a easier way
		MyNew(PrevRun(PrevRun~=0),:)=pts(PrevRun~=0,:)
		pts=MyNew;
	end
	
	
	
	if ~LineMode
		
		%Check if it is a 'looped' polygon, add point if needed 
		if ~(pts(1,1)==pts(end,1)&pts(1,2)==pts(end,2))
			pts(end+1,:)=pts(1,:);
		end
		
		%correct points overboarding
		%---------------------------
		
		%Lon
		pts(pts(:,1)>180,1)=180;
		pts(pts(:,1)<-180,1)=-180;
		
		%Lat
		pts(pts(:,2)>90,2)=90;
		pts(pts(:,2)<-90,2)=-90;
		
		
		
		switch workmode
			case 'Shifted'
				%create the two pointsets
				ShiftedPts=pts;
				UnShiftedPts=ShiftCoords(pts,-offsets);
				MarkerArray=(1:length(ShiftedPts))';
				
							
				%find the problematic points
				ShiDiff=diff(ShiftedPts);
				UnShiDiff=diff(UnShiftedPts);
				
				StartCorrLon=find(ShiDiff(:,1)~=UnShiDiff(:,1));
				%EndCorrLon=StartCorrLon+1;
				
				StartCorrLat=find(ShiDiff(:,2)~=UnShiDiff(:,2));
				%EndCorrLat=StartCorrLat+1;
				
				%Check if there are common points between Lon and Lat
				if ~isempty(StartCorrLon)&~isempty(StartCorrLat);
					Combined=find(ShiDiff(:,1)~=UnShiDiff(:,1)&...
								ShiDiff(:,2)~=UnShiDiff(:,2));
								
				end
				
				
				WhatIs=zeros(numel(ShiftedPts(:,1)),1);
				
				if ~isempty(StartCorrLon)
					WhatIs(StartCorrLon,1)=1;
				end
				
				if ~isempty(StartCorrLat)
					WhatIs(StartCorrLat,1)=2;
				end
				
				%Check if there are common points between Lon and Lat
				if ~isempty(StartCorrLon)&~isempty(StartCorrLat);
					Combined=find(ShiDiff(:,1)~=UnShiDiff(:,1)&...
								ShiDiff(:,2)~=UnShiDiff(:,2));
					WhatIs(Combined,1)=3;			
				end
				
				
				
				%interpolated the boundary points
				%---------------------------------
				
				%Build new structure			
				NewLength=numel(ShiftedPts(:,1))+2*numel(StartCorrLon)+2*numel(StartCorrLat);
				FixedPoints=zeros(NewLength,2);
				UndoData=zeros(NewLength,1);
				
				
				
				Counter=1;
				for i=1:numel(ShiftedPts(:,1));
					if WhatIs(i)==0
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
					elseif WhatIs(i)==1
						%Longitude Jump
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,1))-abs(UnShiftedPts(i,1)));
						LinInter=(UnShiftedPts(i+1,2)-UnShiftedPts(i,2))/InterDist;
						Dist180=abs(180-abs(UnShiftedPts(i,1)));
						InteValue=UnShiftedPts(i,2)+LinInter*(Dist180);
						
						%write
						FixedPoints(Counter,:)=[sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[-sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;		
								
					elseif WhatIs(i)==2
						%Latitude Jump
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,2))-abs(UnShiftedPts(i,2)));
						LinInter=(UnShiftedPts(i+1,1)-UnShiftedPts(i,1))/InterDist;
						Dist90=abs(90-abs(UnShiftedPts(i,2)));
						InteValue=UnShiftedPts(i,1)+LinInter*(Dist90);
						
						%write
						FixedPoints(Counter,:)=[InteValue,sign(UnShiftedPts(i,2))*90];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[InteValue,-sign(UnShiftedPts(i,2))*90];
						Counter=Counter+1;		
						
						
						
					elseif WhatIs(i)==3
						%Jump in Lon and Lat (first Lon)
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
						%longitude
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,1))-abs(UnShiftedPts(i,1)));
						LinInter=(UnShiftedPts(i+1,2)-UnShiftedPts(i,2))/InterDist;
						Dist180=abs(180-abs(UnShiftedPts(i,1)));
						InteValue=UnShiftedPts(i,2)+LinInter*(Dist180);
						
						%write
						FixedPoints(Counter,:)=[sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[-sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;	
						
						%Latitude
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,2))-abs(UnShiftedPts(i,2)));
						LinInter=(UnShiftedPts(i+1,1)-UnShiftedPts(i,1))/InterDist;
						Dist90=abs(90-abs(UnShiftedPts(i,2)));
						InteValue=UnShiftedPts(i,1)+LinInter*(Dist90);
						
						%write
						FixedPoints(Counter,:)=[InteValue,sign(UnShiftedPts(i,2))*90];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[InteValue,-sign(UnShiftedPts(i,2))*90];
						
					end
				
				end
				
				
				
				%Order the points after in which Quadrante they lie:
				%Q1: Lon+, Lat+; Q2: Lon-, Lat+; Q3: Lon- Lat-; Q4: Lon+;Lat-;
				Q1=FixedPoints(:,1)>=0&FixedPoints(:,2)>=0;
				Q2=FixedPoints(:,1)<0&FixedPoints(:,2)>=0;
				Q3=FixedPoints(:,1)<0&FixedPoints(:,2)<0;
				Q4=FixedPoints(:,1)>=0&FixedPoints(:,2)<0;
				
				
				%Now reorder the polygons
				RawPoints=FixedPoints;
								
				if all(WhatIs==0|WhatIs==1)
					%Link Q1, Q4 and Q2, Q3
					Pol14=RawPoints(Q1|Q4,:);
					Pol23=RawPoints(Q2|Q3,:);
					Undo14=UndoData(Q1|Q4);
					Undo23=UndoData(Q2|Q3);
					
					%Close polygons
					if ~(Pol14(1,1)==Pol14(end,1)&Pol14(1,2)==Pol14(end,2))
						Pol14(end+1,:)=Pol14(1,:);
						Undo14(end+1)=0;
					end
					
					if ~(Pol23(1,1)==Pol23(end,1)&Pol23(1,2)==Pol23(end,2))
						Pol23(end+1,:)=Pol23(1,:);
						Undo23(end+1)=0;
					end
					
					FixedPoints=[Pol14;Pol23];
					UndoData=[Undo14;Undo23];
					
				elseif all(WhatIs==0|WhatIs==2)
					%Link Q1, Q2 and Q3, Q4
					Pol12=RawPoints(Q1|Q2,:);
					Pol34=RawPoints(Q3|Q4,:);
					Undo12=UndoData(Q1|Q2);
					Undo34=UndoData(Q3|Q4);
					
					%Close polygons
					if ~(Pol12(1,1)==Pol12(end,1)&Pol12(1,2)==Pol12(end,2))
						Pol12(end+1,:)=Pol12(1,:);
						Undo12(end+1)=0;
					end
					
					if ~(Pol34(1,1)==Pol34(end,1)&Pol34(1,2)==Pol34(end,2))
						Pol34(end+1,:)=Pol34(1,:);
						Undo34(end+1)=0;
					end
					
					FixedPoints=[Pol12;Pol34];
					UndoData=[Undo12;Undo34];
					
				elseif any(WhatIs==3)
					%currently experimental
					Pol1=RawPoints(Q1,:);
					Pol2=RawPoints(Q2,:);
					Pol3=RawPoints(Q3,:);
					Pol4=RawPoints(Q4,:);
					Undo1=UndoData(Q1);
					Undo2=UndoData(Q2);
					Undo3=UndoData(Q3);
					Undo4=UndoData(Q4);
					
					
					%Close polygons
					if ~(Pol1(1,1)==Pol1(end,1)&Pol1(1,2)==Pol1(end,2))
						Pol1(end+1,:)=Pol1(1,:);
						Undo1(end+1)=0;
					end
					
					if ~(Pol2(1,1)==Pol2(end,1)&Pol2(1,2)==Pol2(end,2))
						Pol2(end+1,:)=Pol2(1,:);
						Undo2(end+1)=0;
					end
					
					if ~(Pol3(1,1)==Pol3(end,1)&Pol3(1,2)==Pol3(end,2))
						Pol3(end+1,:)=Pol3(1,:);
						Undo3(end+1)=0;
					end
					
					if ~(Pol4(1,1)==Pol4(end,1)&Pol4(1,2)==Pol4(end,2))
						Pol4(end+1,:)=Pol34(1,:);
						Undo4(end+1)=0;
					end
					
					FixedPoints=[Pol1;Pol2;Pol3;Pol4];
					UndoData=[Undo1;Undo2;Undo3;Undo4];
					
					
					
				end
				
				%if ~(FixedPoints(1,1)==FixedPoints(end,1)&FixedPoints(1,2)==FixedPoints(end,2))
				%		FixedPoints(end+1,:)=FixedPoints(1,:);
				%end
		
			
			case 'UnShifted'	
				%create the two pointsets
				ShiftedPts=ShiftCoords(pts,offsets);
				UnShiftedPts=pts;
				MarkerArray=(1:length(ShiftedPts))';
				
							
				%find the problematic points
				ShiDiff=diff(ShiftedPts);
				UnShiDiff=diff(UnShiftedPts);
				
				StartCorrLon=find(ShiDiff(:,1)~=UnShiDiff(:,1));
				%EndCorrLon=StartCorrLon+1;
				
				StartCorrLat=find(ShiDiff(:,2)~=UnShiDiff(:,2));
				%EndCorrLat=StartCorrLat+1;
				
				%Check if there are common points between Lon and Lat
				if ~isempty(StartCorrLon)&~isempty(StartCorrLat);
					Combined=find(ShiDiff(:,1)~=UnShiDiff(:,1)&...
								ShiDiff(:,2)~=UnShiDiff(:,2));
								
				end
				
				
				WhatIs=zeros(numel(ShiftedPts(:,1)),1);
				
				if ~isempty(StartCorrLon)
					WhatIs(StartCorrLon,1)=1;
				end
				
				if ~isempty(StartCorrLat)
					WhatIs(StartCorrLat,1)=2;
				end
				
				%Check if there are common points between Lon and Lat
				if ~isempty(StartCorrLon)&~isempty(StartCorrLat);
					Combined=find(ShiDiff(:,1)~=UnShiDiff(:,1)&...
								ShiDiff(:,2)~=UnShiDiff(:,2));
					WhatIs(Combined,1)=3;			
				end
				
				
				
				%interpolated the boundary points
				%---------------------------------
				
				%Build new structure			
				NewLength=numel(ShiftedPts(:,1))+2*numel(StartCorrLon)+2*numel(StartCorrLat);
				FixedPoints=zeros(NewLength,2);
				UndoData=zeros(NewLength,1);
				
				
				
				Counter=1;
				for i=1:numel(ShiftedPts(:,1));
					if WhatIs(i)==0
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
					elseif WhatIs(i)==1
						%Longitude Jump
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,1))-abs(UnShiftedPts(i,1)));
						LinInter=(UnShiftedPts(i+1,2)-UnShiftedPts(i,2))/InterDist;
						Dist180=abs(180-abs(UnShiftedPts(i,1)));
						InteValue=UnShiftedPts(i,2)+LinInter*(Dist180);
						
						%write
						FixedPoints(Counter,:)=[sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[-sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;		
								
					elseif WhatIs(i)==2
						%Latitude Jump
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,2))-abs(UnShiftedPts(i,2)));
						LinInter=(UnShiftedPts(i+1,1)-UnShiftedPts(i,1))/InterDist;
						Dist90=abs(90-abs(UnShiftedPts(i,2)));
						InteValue=UnShiftedPts(i,1)+LinInter*(Dist90);
						
						%write
						FixedPoints(Counter,:)=[InteValue,sign(UnShiftedPts(i,2))*90];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[InteValue,-sign(UnShiftedPts(i,2))*90];
						Counter=Counter+1;		
						
						
						
					elseif WhatIs(i)==3
						%Jump in Lon and Lat (first Lon)
						FixedPoints(Counter,:)=UnShiftedPts(i,:);
						UndoData(Counter)=MarkerArray(i);
						Counter=Counter+1;
						
						%longitude
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,1))-abs(UnShiftedPts(i,1)));
						LinInter=(UnShiftedPts(i+1,2)-UnShiftedPts(i,2))/InterDist;
						Dist180=abs(180-abs(UnShiftedPts(i,1)));
						InteValue=UnShiftedPts(i,2)+LinInter*(Dist180);
						
						%write
						FixedPoints(Counter,:)=[sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[-sign(UnShiftedPts(i,1))*180,InteValue];
						Counter=Counter+1;	
						
						%Latitude
						%interpolate
						InterDist=abs(abs(UnShiftedPts(i+1,2))-abs(UnShiftedPts(i,2)));
						LinInter=(UnShiftedPts(i+1,1)-UnShiftedPts(i,1))/InterDist;
						Dist90=abs(90-abs(UnShiftedPts(i,2)));
						InteValue=UnShiftedPts(i,1)+LinInter*(Dist90);
						
						%write
						FixedPoints(Counter,:)=[InteValue,sign(UnShiftedPts(i,2))*90];
						Counter=Counter+1;
						FixedPoints(Counter,:)=[InteValue,-sign(UnShiftedPts(i,2))*90];
						
					end
				
				end
				
				
				
				%Order the points after in which Quadrante they lie:
				%Q1: Lon+, Lat+; Q2: Lon-, Lat+; Q3: Lon- Lat-; Q4: Lon+;Lat-;
				Q1=FixedPoints(:,1)>=0&FixedPoints(:,2)>=0;
				Q2=FixedPoints(:,1)<0&FixedPoints(:,2)>=0;
				Q3=FixedPoints(:,1)<0&FixedPoints(:,2)<0;
				Q4=FixedPoints(:,1)>=0&FixedPoints(:,2)<0;
				
				
				%Now reorder the polygons
				RawPoints=FixedPoints;
								
				if all(WhatIs==0|WhatIs==1)
					%Link Q1, Q4 and Q2, Q3
					Pol14=RawPoints(Q1|Q4,:);
					Pol23=RawPoints(Q2|Q3,:);
					Undo14=UndoData(Q1|Q4);
					Undo23=UndoData(Q2|Q3);
					
					%Close polygons
					if ~(Pol14(1,1)==Pol14(end,1)&Pol14(1,2)==Pol14(end,2))
						Pol14(end+1,:)=Pol14(1,:);
						Undo14(end+1)=0;
					end
					
					if ~(Pol23(1,1)==Pol23(end,1)&Pol23(1,2)==Pol23(end,2))
						Pol23(end+1,:)=Pol23(1,:);
						Undo23(end+1)=0;
					end
					
					FixedPoints=[Pol14;Pol23];
					UndoData=[Undo14;Undo23];
					
				elseif all(WhatIs==0|WhatIs==2)
					%Link Q1, Q2 and Q3, Q4
					Pol12=RawPoints(Q1|Q2,:);
					Pol34=RawPoints(Q3|Q4,:);
					Undo12=UndoData(Q1|Q2);
					Undo34=UndoData(Q3|Q4);
					
					%Close polygons
					if ~(Pol12(1,1)==Pol12(end,1)&Pol12(1,2)==Pol12(end,2))
						Pol12(end+1,:)=Pol12(1,:);
						Undo12(end+1)=0;
					end
					
					if ~(Pol34(1,1)==Pol34(end,1)&Pol34(1,2)==Pol34(end,2))
						Pol34(end+1,:)=Pol34(1,:);
						Undo34(end+1)=0;
					end
					
					FixedPoints=[Pol12;Pol34];
					UndoData=[Undo12;Undo34];
					
				elseif any(WhatIs==3)
					%currently experimental
					Pol1=RawPoints(Q1,:);
					Pol2=RawPoints(Q2,:);
					Pol3=RawPoints(Q3,:);
					Pol4=RawPoints(Q4,:);
					Undo1=UndoData(Q1);
					Undo2=UndoData(Q2);
					Undo3=UndoData(Q3);
					Undo4=UndoData(Q4);
					
					
					%Close polygons
					if ~(Pol1(1,1)==Pol1(end,1)&Pol1(1,2)==Pol1(end,2))
						Pol1(end+1,:)=Pol1(1,:);
						Undo1(end+1)=0;
					end
					
					if ~(Pol2(1,1)==Pol2(end,1)&Pol2(1,2)==Pol2(end,2))
						Pol2(end+1,:)=Pol2(1,:);
						Undo2(end+1)=0;
					end
					
					if ~(Pol3(1,1)==Pol3(end,1)&Pol3(1,2)==Pol3(end,2))
						Pol3(end+1,:)=Pol3(1,:);
						Undo3(end+1)=0;
					end
					
					if ~(Pol4(1,1)==Pol4(end,1)&Pol4(1,2)==Pol4(end,2))
						Pol4(end+1,:)=Pol34(1,:);
						Undo4(end+1)=0;
					end
					
					FixedPoints=[Pol1;Pol2;Pol3;Pol4];
					UndoData=[Undo1;Undo2;Undo3;Undo4];
					
				end
				
				%if ~(FixedPoints(1,1)==FixedPoints(end,1)&FixedPoints(1,2)==FixedPoints(end,2))
				%		FixedPoints(end+1,:)=FixedPoints(1,:);
				%end
				
			case 'cut'
				FixedPoints=pts;
				UndoData=[];
				
			case 'undo'
				%Like cut it can be used to undo what have been done, but it
				%returns shifted data points, so what was normally inputed before
				
				UndoData=[];
				
				if ~isempty(offsets)
					FixedPoints=ShiftCoords(pts,offsets);
				else
					FixedPoints=pts;
				
				end
		
		end
	
		
		
	else
		%LineMode
		
		
		
	end	
	


end

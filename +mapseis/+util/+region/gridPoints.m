classdef gridPoints < handle
% gridPoints : creates sets of grid points 

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 82 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Attributes
properties
    BoundBox
end

%% Public Methods
methods
    function obj = gridPoints(varargin)
        % Bounding box is specified as [minX maxX minY maxY]
        obj.BoundBox = mapseis.util.region.gridPoints.initGrid(varargin{:});
    end


    function pts = getGrid(obj,sep,offset)
        % Create a grid of points returned as [x y] matrix with the
        % specified spacing
        
        if nargin<3
            offset=[0 0];
        end
        
        % Allow x and y separations to be specified separately, but if
        % called with a single number then use same spacing for both
        if numel(sep)==1
            sep(end+1)=sep(1);
        end
        assert(numel(sep)==2);
        % Create vectors of x and y grid points
        xVect = obj.BoundBox(1):sep(1):obj.BoundBox(2);
        yVect = obj.BoundBox(3):sep(2):obj.BoundBox(4);
        [XX,YY] = meshgrid(xVect,yVect);
        pts = [XX(:) YY(:)]+repmat(offset,numel(XX(:)),1);
    end

    function setBoundingBox(obj,newBB)
       % Set the bounding box of the grid.
       % newBB = [minX maxX minY maxY]
       obj.BoundBox = newBB;
    end

end

%% Private Methods
methods (Static, Access = private)
    function bBox = initGrid(varargin)
       % Initialise the grid object
       switch nargin
           case 0
               bBox = [0 1 0 1]; % Default bounding box is the unit square
           case 1
               bBox = varargin{1};
               assert(isnumeric(bBox));
               assert(numel(bBox)==4);
       end
    end
end

end
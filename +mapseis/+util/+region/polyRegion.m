function funStruct = polyRegion(varargin)
% polyRegion : polygon region allowing test for point being inside

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Imports

import mapseis.util.region.*;

%% Attributes

% Polygon is defined by the points on its boundary.  When the polygon
% region is created by combining two existing polygons remove the points in
% the intersection of the polygons

boundPoints = nMakePolyRegion(varargin{:});


%% Interface
funStruct = struct(...
    'getBoundary',@nGetBoundary,... % Get the boundary points as m x 2 array
    'isInside',@nIsInside,... % Test if point is inside polygon
    'getBoundingBox',@nGetBoundingBox); % Get the limits of the polygon


%% Public Methods

    function pts = nGetBoundary()
        pts = boundPoints;
    end

    function [inBound,onBound]=nIsInside(pts)
        [inBound,onBound]=inpolygon(pts(:,1),pts(:,2),...
            boundPoints(:,1),boundPoints(:,2));
    end

    function bb=nGetBoundingBox()
        bb = [nMinMax(boundPoints(:,1)),...
            nMinMax(boundPoints(:,2))];
    end

%% Private Methods

    function boundPoints = nMakePolyRegion(varargin)
        % Constructor for polygon region
        switch nargin
            case 1
                % Input is an array of [x y] cooridinates in columns
                try
                    boundPoints = varargin{1};
                    assert(isnumeric(boundPoints));
                    assert(numel(boundPoints(1,:))==2);
                    assert(numel(boundPoints(:,1))>2);
                    % Close the polygon if necessary
                    if any(boundPoints(1,:) ~= boundPoints(end,:))
                        boundPoints(end+1,:) = boundPoints(1,:);
                    end
                catch
                    error('polyRegion:non_matrix_constructor',...
                        'polyRegion needs m x 2 matrix of [x y] coords when called with single argument');
                end
            case 2
                % Input is two polyRegion objects
                % TODO : need to think about ordering a bit more, as it
                % stands this could create a region that is discontinuous
                % at the boundary of the two regions making it up.
                polyReg1 = varargin{1};
                polyReg2 = varargin{2};
                try
                    bPts1 = polyReg1.getBoundary();
                    bPts2 = polyReg2.getBoundary();
                catch
                    error('polyRegion:invalid_constructor_args',...
                        'polyRegion needs two polyRegion objects as input when called with two arguments');
                end
                % Remove intersection points
                bPts1 = bPts1(not(polyReg2.isInside(bPts1)));
                bPts2 = bPts2(not(polyReg1.isInside(bPts2)));
                boundPoints = [bPts1; bPts2];
            otherwise
                error('polyRegion:unknown_arguments','Unknown constructor arguments')
        end

    end

    function mmVal=nMinMax(vec)

        mmVal = [min(vec), max(vec)];
    end

end
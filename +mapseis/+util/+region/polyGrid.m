classdef polyGrid
    % polyGrid : Creates a grid within a specified polygonal region
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 82 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
    % Author: Matt McDonnell
    
    properties
        PolyRegion
        GridPoints
    end
    
    methods
        %% Constructor
        function obj = polyGrid(polyArgs,gridFun)
            import mapseis.util.region.*;
            
            % Create the bounding polygon from the input arguments
            if iscell(polyArgs)
                obj.PolyRegion = polyRegion(polyArgs{:});
            else
                obj.PolyRegion = polyRegion(polyArgs);
            end
            
            % Create a grid points object that uses the bounding box of the polygon
            if nargin==1
                gridFun=@gridPoints;
            end
            
            obj.GridPoints = gridFun(obj.PolyRegion.getBoundingBox());           
        end
        %% Public Methods
        function pts=getGrid(obj,varargin)
            % Get the grid points as a m x 2 matrix [x y]
            pts=obj.GridPoints.getGrid(varargin{:});
            logInd = obj.PolyRegion.isInside(pts); % Find points within region
            pts = [pts(logInd,1) pts(logInd,2)];
        end
        
        function bBox=getBoundingBox(obj)
            % Get the bounding box of the region as [xMin xMax yMin yMax]
            bBox = obj.PolyRegion.getBoundingBox();
        end
        
        function bPts=getBoundary(obj)
            % Get the points on the boundary of the region
            bPts = obj.PolyRegion.getBoundary();
        end        
    end
end
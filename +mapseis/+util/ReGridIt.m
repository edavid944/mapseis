function [OutData xvect yvect] = ReGridIt(RefGrid, Data,gridSpace, bbox,parallelmode)
	%This function takes the smallest gridspacing an interpolates the
	%the whole map on it. Alternativly the gridSpacing can also be defined
	%with the variable gridSpace
	
	%works but very time consuming on large grids
	
	if nargin<5
		gridSpace=[];
	end
	
	if nargin<4
		gridSpace=[];
		bbox=[];
	end
	
	
	if any(RefGrid(:,5)==2);
	
		OneGrid=RefGrid(RefGrid(:,5)==1,:);
		TwoGrid=RefGrid(RefGrid(:,5)==2,:);
		OneData=Data(RefGrid(:,5)==1,:);
		TwoData=Data(RefGrid(:,5)==2,:);
		
		if isempty(gridSpace)
			gridSpace=min(min(OneGrid(3:4,:)));
		end
		if isempty(bbox)
			minLon=min([min(OneGrid(:,1)),min(TwoGrid(:,1))]);
			maxLon=max([max(OneGrid(:,1)),max(TwoGrid(:,1))]);
			minLat=min([min(OneGrid(:,2)),min(TwoGrid(:,2))]);
			maxLat=max([max(OneGrid(:,2)),max(TwoGrid(:,2))]);
		else
			minLon=bbox(1)
			maxLon=bbox(2)
			minLat=bbox(3)
			maxLat=bbox(4)
		end	
		
		xvect=minLon:gridSpace:maxLon;
		yvect=minLat:gridSpace:maxLat;
		
		OutData=nan(numel(xvect),numel(yvect));
		
		[XX,YY] = meshgrid(xvect,yvect);
		elementCount = numel(XX);
		
		OutData=OutData';
		
		if parallelmode 
			matlabpool open;
			
			parfor i=1:elementCount
				thisGridPoint = [XX(i), YY(i)];
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2)));
				%inOne=((((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
				%	(((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))))&...
				%	((((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
				%	(((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))));
				if any(inOne)
					%disp('inOne')
					OutData(i)=nanmean(OneData(inOne));
				else
				inTwo=(((TwoGrid(:,1)-TwoGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
					((TwoGrid(:,1)+TwoGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
					(((TwoGrid(:,1)+TwoGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
					((TwoGrid(:,1)-TwoGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2)))&...
					(((TwoGrid(:,2)-TwoGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
					((TwoGrid(:,2)+TwoGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
					(((TwoGrid(:,2)+TwoGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
					((TwoGrid(:,2)-TwoGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2)));
					
					if any(inTwo)
						%disp('inTwo')
						OutData(i)=nanmean(TwoData(inTwo));
					end
				end	
			
			end
			
			matlabpool close;
			
		else
				for i=1:elementCount
				thisGridPoint = [XX(i), YY(i)];
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2)));
				%inOne=((((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
				%	(((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
				%	((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))))&...
				%	((((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
				%	(((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
				%	((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))));
				if any(inOne)
					%disp('inOne')
					OutData(i)=nanmean(OneData(inOne));
				else
				inTwo=(((TwoGrid(:,1)-TwoGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
					((TwoGrid(:,1)+TwoGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
					(((TwoGrid(:,1)+TwoGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
					((TwoGrid(:,1)-TwoGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2)))&...
					(((TwoGrid(:,2)-TwoGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
					((TwoGrid(:,2)+TwoGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
					(((TwoGrid(:,2)+TwoGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
					((TwoGrid(:,2)-TwoGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2)));
					
					if any(inTwo)
						%disp('inTwo')
						OutData(i)=nanmean(TwoData(inTwo));
					end
				end	
		
			end
		end	
		OutData=OutData';
		
	else
	
	
		OneGrid=RefGrid(RefGrid(:,5)==1,:);
		OneData=Data(RefGrid(:,5)==1,:);
				
		if isempty(gridSpace)
			gridSpace=min(min(OneGrid(3:4,:)));
		end
		if isempty(bbox)
			minLon=min(OneGrid(:,1));
			maxLon=max(OneGrid(:,1));
			minLat=min(OneGrid(:,2));
			maxLat=max(OneGrid(:,2));
		else
			minLon=bbox(1);
			maxLon=bbox(2);
			minLat=bbox(3);
			maxLat=bbox(4);
		end	
		
		xvect=minLon:gridSpace:maxLon;
		yvect=minLat:gridSpace:maxLat;
		
		OutData=nan(numel(xvect),numel(yvect));
		
		[XX,YY] = meshgrid(xvect,yvect);
		elementCount = numel(XX);
		
		OutData=OutData';
		
		if parallelmode 
			matlabpool open;
			
			parfor i=1:elementCount
				thisGridPoint = [XX(i), YY(i)];
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2)));
				
				if any(inOne)
					OutData(i)=nanmean(OneData(inOne));
				end	
			
			end
			
			matlabpool close;
			
		else	
		
			for i=1:elementCount
				thisGridPoint = [XX(i), YY(i)];
				inOne=(((OneGrid(:,1)-OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2))|...
					((OneGrid(:,1)+OneGrid(:,3)/2)>=(thisGridPoint(1)-gridSpace/2)))&...
					(((OneGrid(:,1)+OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2))|...
					((OneGrid(:,1)-OneGrid(:,3)/2)<=(thisGridPoint(1)+gridSpace/2)))&...
					(((OneGrid(:,2)-OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2))|...
					((OneGrid(:,2)+OneGrid(:,4)/2)>=(thisGridPoint(2)-gridSpace/2)))&...
					(((OneGrid(:,2)+OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2))|...
					((OneGrid(:,2)-OneGrid(:,4)/2)<=(thisGridPoint(2)+gridSpace/2)));
				
				if any(inOne)
					OutData(i)=nanmean(OneData(inOne));
				end	
			
			end
		
		
		end
		OutData=OutData';	
	
	
	
	
	end





end

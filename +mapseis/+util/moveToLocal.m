function moveToLocal( str )
	% MOVETOLOCAL Move a variable form the global to the local workspace
	% str should be the name of the variable that is already in the global workspace
	% that you wish to move to the local workspace of the calling function.
	% The variable will still exist in the global workspace but in the local
	% workspace of the calling function the variable will be scoped locally
	
	eval(['global ' str]) % bring the variable in from the global workspace 
	k=eval(str); % fetch the value of the variable from the global workspace 
	evalin( 'caller', ['clear ' str]); % clear the value from the caller's workspace 
	assignin( 'caller', str, k); % assign the value into the caller's local workspace

end

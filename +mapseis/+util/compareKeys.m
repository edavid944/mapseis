 function logInd = compareKeys(sav,key)
        	
        	n = numel(sav);
        	logInd = logical(zeros(n,1));
        	%logInd = [];
        	
        	%compare only the string of the function not the handle
        	key.Callback=func2str(key.Callback);
        	for c=1:n
        		
        		tempE=sav{c};
        		tempE.Callback=func2str(tempE.Callback);
        		
        		logInd(c) = isequal(key,tempE);
        	end
        end

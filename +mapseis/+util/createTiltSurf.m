function [Xmesh Ymesh Zmesh] = createTiltSurf(SurfaceConfig)
	%creates a a tilted surface for using with slice
	%the surface can either be defined by 3 points (corners) or 
	%by a line (2 points) and a horizontal offset in deg.
	
	
	%SurfaceConfig Fields
	%	Mode: 		Can be '3points' for the normal mode, 'offset' for the 
	%			deg/km offset and 'dip' for the dip mode.
	%	Orientation:	'vertical' for the a vertical plane and 'horizontal'
	%			for a horizontal plane neede only in 'offset' mode
	%			NOT IMPLEMENTED NOW
	%	Point1:		The first top corner
	%	Point2:		The second top corner
	%	Point3: 	The second lower corner (under the second top corner)
	%	OffSet:		Needed in case of offset mode defines the offset in
	%			deg (vertical) or km (horizontal)
	%	Angle:		Defines the dip of a surface
	%	MaxDepth:	Sets the maximum depth (needed in offset and angle mode)
	%	Wide:		Needed only in case of horizontal offset mode not needed now
	%	HorRes:		Number of steps in horizontal direction
	%	VerRes:		Number of steps in vertical direction
	
	%Matrix will have the size of (HorRes,VerRes)


	switch SurfaceConfig.Mode
	
		case '3points'
			%just get the points and add the depth [0 maxdepth]
			point1=SurfaceConfig.Point1;
			point1(3)=0;
			point2=SurfaceConfig.Point2;
			point2(3)=SurfaceConfig.MaxDepth;
			point3=SurfaceConfig.Point3;
			point3(3)=SurfaceConfig.MaxDepth;
			
			
			
		case 'offset'
			%point 3 has to be created by an offset	
			point1=SurfaceConfig.Point1;
			point2=SurfaceConfig.Point2;
			
			%create normal vector to unit vector;
			vecno = point2 - point1;
			Nvec(1,1)=1/vecno(1,1);
			Nvec(1,2)=-1/vecno(1,2);
			
			%normal unit vector
			normalUnitVec = Nvec./(sqrt(Nvec(1,1)^2+Nvec(1,2)^2));
			
			
			%create offset vector
			offsetVector=normalUnitVec*SurfaceConfig.OffSet;
			
			%build point 3
			point3=point2+offsetVector
			
			%add depths
			point1(3)=0;
			point2(3)=SurfaceConfig.MaxDepth;
			point3(3)=SurfaceConfig.MaxDepth;
			
			
			
		case 'dip'
			%create offset from dip angle and maxdepth
			offset=tan(SurfaceConfig.Angle)*SurfaceConfig.MaxDepth;
			
			%now continue like before
			point1=SurfaceConfig.Point1;
			point2=SurfaceConfig.Point2;
			
			%create normal vector to unit vector;
			vecno = point2 - point1;
			Nvec(1,1)=1/vecno(1,1);
			Nvec(1,2)=-1/vecno(1,2);
			
			%normal unit vector
			normalUnitVec = Nvec./(sqrt(Nvec(1,1)^2+Nvec(1,2)^2));
			
			
			%create offset vector
			offsetVector=normalUnitVec*offset;
			
			%build point 3
			point3=point2+offsetVector
			
			%add depths
			point1(3)=0;
			point2(3)=SurfaceConfig.MaxDepth;
			point3(3)=SurfaceConfig.MaxDepth;
			
			
	end
	
		
	
	
	%Points are existing so the matrixes can be build
	%------------------------------------------------
	
	%set resolution
	HorRes=SurfaceConfig.HorRes;
	VerRes=SurfaceConfig.VerRes;
	
	%check for reversed points
	XNeg=false;
	YNeg=false;
	ZNeg=false;
	
	%if the point coordinates are reverse, the coordinate will be set to negative
	%and the matrix set back to normal
	
	if point2(1)<point1(1)
		point1(1)=-point1(1);
		point2(1)=-point2(1);
		point3(1)=-point3(1);
		XNeg=true;
	end
	
	
	if point2(2)<point1(2)
		point1(2)=-point1(2);
		point2(2)=-point2(2);
		point3(2)=-point3(2);
		YNeg=true;
	end
	
	
	if point2(3)<point1(3)
		point1(3)=-point1(3);
		point2(3)=-point2(3);
		point3(3)=-point3(3);
		ZNeg=true;
	end
	
	
	%create the raw matrixes (vertical surface)
	XMat=repmat(linspace(point1(1),point2(1),HorRes),VerRes,1);
	YMat=repmat(linspace(point1(2),point2(2),HorRes),VerRes,1);
	ZMat=repmat(linspace(point1(3),point2(3),VerRes)',1,HorRes);
	
	%create the modification matrixes
	XModify=repmat(linspace(0,point3(1)-point2(1),VerRes)',1,HorRes);
	YModify=repmat(linspace(0,point3(2)-point2(2),VerRes)',1,HorRes);
	
	%create final matrixes
	Xmesh=XMat-XModify;
	Ymesh=YMat-YModify;
	Zmesh=ZMat;
	
	%reverse the negation
	if XNeg
		Xmesh=-Xmesh;
	end

	if YNeg
		Ymesh=-Ymesh;
	end

	if ZNeg
		Zmesh=-Zmesh;
	end	
	
	
end

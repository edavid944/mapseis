function [SmoothIndex NewSmooth MagField] = SmoothTest2D(MindField,NormIt,PlotIt)
	%A test for smoothness of 2D "images" like a the Relm style forecasts
	
	%How does it work:
	%-----------------
	%What we want to know is how smooth a forecast (or any 2D surface) is.
	%A smooth forecast would have broad structures and less sharp contrast.
	%In the frequency world this means many low frequencies (in the center
	%of the fft and less high frequencies (at the borders of the fft), 
	%because edges as a lot of high frequency power. 
	%With the 2D fft we get the frequency content of the map with low 
	%frequencies in the middle and higher frequencies in the outer regions.
	%(Remember: the 2D fft "splits" an image into horizontal and vertical
	%cosinus images) We care here for the smoothness more about the 
	%Magnitude of the Amplitude (which shows use the actual frequency 
	%content) than about the phase of the frequencies (which would only 
	%tell use where the frequencies are).This also results in that we are 
	%indepented from some "shifts", for instance does the fft not change in
	%case of rotated map, also does the repeation of the map only partly 
	%matter, the magnitude spectrum will have a similar shape but, due to 
	%full structur repetition the lowest frequencies will be increased (also
	%high frequencies will have higher magnitudes but less), resulting in
	%higher smoothIndex than without repetition. There are probably ways to
	%improve that behavior.
	%The actuall smoothIndex is defined here as the difference between 
	%maximum amplitude and mean values of all amplitudes, which summarizes
	%the distribution quite good for the most cases. Besides the problem 
	%with repetitions, there is also the case of an uniform distribution,
	%which results in values of almost 0 because of the mean value being 
	%almost the same (effect of the border) as the maximum, although one 
	%might argue that an uniform distribution is perfectly smooth. 
	%Some typical values for the SmoothIndex:
	%	Random Noise: 				~3 
	%	Single Vertical/Horizontal Cosine:	~35
	%	peaks function (matlab):		~24
	%	TripleS model (smooth):			~10
	%	KJSS (with strong peaks):		~6
	
	
	if nargin<3
		PlotIt=false;
	end
	
	if isempty(PlotIt)
		PlotIt=false;
	end

	if nargin<2
		PlotIt=false;
		NormIt=false;
	end
	
	if isempty(NormIt)
		NormIt=false;
	end
	
	if NormIt
		%norm input data to 1;
		MindField=(MindField-min(MindField(:)))/max(MindField(:));
	end
	
	
	%get mean value of the data (will be subtracted from the data)
	imMean = mean(MindField(:));

	%The actual fft 
	fim = fftshift(fft2(double(MindField) - imMean));
	
	%The zero frequencies will be shifted into the middle, this makes it 
	%easier to interpret. Also th mean is subtracted because we only 
	%interested in the deviation from it.
	
	%Magnitude of the amplitudes (10^-6 is added to prevent problems with 0)
	MagField = log(fim .* conj(fim)+ 10 ^ (-6));

	%Measurment of smoothness
	SmoothIndex=abs(max(MagField(:))-mean(MagField(:)));
	
	%New Measurement of Smoothness
	MaxAmp=max(MagField(:));
	MinAmp=min(MagField(:));
	MeanAmp=mean(MagField(:));
	NewSmooth=1/(abs((MaxAmp-MinAmp))+abs((MeanAmp-MinAmp)))*100;
	
	if PlotIt
		figure;
		s1=surf(MagField);
		set(s1,'LineStyle','none');
	
	end
	

end

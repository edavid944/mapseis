function [fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(fDy)
	% function [fYr, nMn, nDay, nHr, nMin, nSec] = decyear2matS(fDy)
	% ------------------------------------------------------------------------------------------------
	% Calculate decimal year format (from zmap-function decyear, i.e. 1998.734)
	% to matrix with columns year, month, day, hour, minute, and second. 
	% This function was programmed because datevec.m does not work with decimal
	% year input format
	%
	% Input parameters:
	%   fDy     Decimal year (like 1998.2515) as vector or float
	%
	% Output parameters:
	%   fYr     decimal year
	%   nMn     month
	%   nDay    day
	%   nHr     hour
	%   nMin    minute
	%   nSec    second
	%
	% Example [fYr, nMn, nDay, nHr, nMin, nSec]=decyear2mat(decyear([1989 12 31 10 35 44.3]))
	%
	% Thomas van Stiphout
	% Mai 9, 2007
	
	%Modified DE, vectorized function for performance increase
	
	%disp('~/zmap/src/decyear2matS.m')
	
	% save year in decimal format
	fYr=floor(fDy);
	% define leap years
	bLeapYr = rem(fix(fDy),4) == 0 & rem(fix(fDy),100) ~= 0 | rem(fix(fDy),400) == 0 ;
	%bLeapYr=isleap(floor(fYr))
	
	lep=(find(bLeapYr==1));


	% for leap years
	try
        mDay=[0,31,60,91,121,152,182,213,244,274,305,335]'; %leapyear
        dadi=mDay*ones(size(lep,1),1)';
        subtra=ones(1,size(mDay,1))'*(rem(fDy(lep),1).*366)'; 
        zer=(0<(dadi-subtra));
        [irt, jkl, v] = find(zer); 
        tse = logical(diff([0;jkl]));           % calculate year
        i = repmat(13, [size(zer,2) 1]);
        
        
        i(jkl(tse)) = irt(tse);  
        
        nMn(lep)=i-1;
        nDay(lep)=ceil(rem(fDy(lep),1).*366)-mDay(nMn(lep)); % calculate days
       
        
        nHr(lep)=rem(fDy(lep),1)'.*366.*24-(mDay(nMn(lep))'+nDay(lep)-1).*24; 
        nMin(lep)=rem(nHr(lep),1).*60;
        nHr(lep)=fix(nHr(lep)); % hours
        nSec(lep)=rem(nMin(lep),1).*60; % seconds
        nMin(lep)=fix(nMin(lep)); % minutes

        end
        
        try
        %normal years
        lep=(find(bLeapYr==0));
        mDay= [0,31,59,90,120,151,181,212,243,273,304,334]';%cumulative days in one year
           dadi=mDay*ones(size(lep,1),1)';
        
        
        subtra=ones(1,size(mDay,1))'*(rem(fDy(lep),1).*365)' ;
        zer=(0<(dadi-subtra));
        [irt, jkl, v] = find((0<(zer))); 
        tse = logical(diff([0;jkl]));           % calculate year
        i = repmat(13, [size(zer,2) 1]); 
        i(jkl(tse)) = irt(tse);
        nMn(lep)=i-1;
        
        nDay(lep)=ceil(rem(fDy(lep),1).*365)-mDay(nMn(lep)); % calculate days
 
        
        nHr(lep)=rem(fDy(lep),1)'.*365.*24-(mDay(nMn(lep))'+nDay(lep)-1).*24;
        nMin(lep)=rem(nHr(lep),1)*60;
        nHr(lep)=fix(nHr(lep));
        nSec(lep)=rem(nMin(lep),1)*60;
        nMin(lep)=fix(nMin(lep));
        end
end

function x=isleap(Year)
	%ISLEAP True for leap year.
	%     ISLEAP(Year) returns 1 if Year is a leap year and 0 otherwise.
	%     ISLEAP is only set for gregorian calendar, so Year >= 1583
	%
	% Syntax: 	ISLEAP(YEAR)
	%      
	%     Inputs:
	%           YEAR - Year of interest (default = current year). 
	%           You can input a vector of years.
	%     Outputs:
	%           Logical vector.
	%
	%      Example: 
	%
	%           Calling on Matlab the function: isleap
	%
	%           Answer is: 0
	%
	%
	%           Calling on Matlab the function: x=isleap([2007 2008])
	%
	%           Answer is:
	%           x = 0 1
	%
	%           Created by Giuseppe Cardillo
	%           CEINGE - Advanced Biotechnologies
	%           Via Comunale Margherita, 482
	%           80145
	%           Napoli - Italy
	%           cardillo@ceinge.unina.it
	
	
	%Input Error handling
	switch nargin
	    case 0
		Year=str2double(datestr(date,10));
	    case 1
		checkyear(Year)
	end
	
	% The Gregorian calendar has 97 leap years every 400 years: 
	% Every year divisible by 4 is a leap year. 
	% However, every year divisible by 100 is not a leap year. 
	% However, every year divisible by 400 is a leap year after all. 
	% So, 1700, 1800, 1900, 2100, and 2200 are not leap years, 
	% but 1600, 2000, and 2400 are leap years.
	A=mod(Year,4);
	B=mod(Year,100);
	C=mod(Year,400);
	x=zeros(size(Year));
	i=sort(find(C==0 | (A==0 & B>0)));
	if isempty(i)==0
	    x(i)=1;
	end
end

function checkyear(Year)
	num=isnumeric(Year);
	fin=isfinite(Year);
	if num==0 | fin(fin==0)
	    error('Warning: Year values must be numeric and finite')
	end
	L=Year-floor(Year);
	if L(L>0)
	    error('Warning: Year values must be integer')
	end
	L=Year-1583;
	if L(L<0)
	    error('Warning: Every value of Year must be >1582')
	end

end
        

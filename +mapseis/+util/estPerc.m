function [LowP HighP]=estPerc(DistValue,PercVal)
	%This function calculates at which values PercVal percent of the values 
	%are below (LowP) and at which values 1-PercVal percent are below.
	%e.g. 2.5 or 0.025 will give 95% intervall, 16% or 0.16 will give the 
	%68% interval.
	
	%NOT REALLY NEEDED, there is a matlab built in version
	
	if PercVal>1
		PercVal=PercVal/100;
	end	
	
	quan=quantile(DistValue,[PercVal 1-PercVal]);
	
	LowP=quan(1);
	HighP=quan(2);
	
	


end

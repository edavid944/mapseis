classdef PropertiesIO < handle
	%This allows to read an write java compatible property lists
	
	properties
		FileName
		FileHandle
		Keywords
		Values
		CommentLines
		LineOrder
		NoComment
		PointUnderScore
		ScoreStruct
        end
    

       
        methods 
        	function obj = PropertiesIO(FileName)
			%Constructor 
			
        		if nargin<1
        			FileName=[];
        		end
        	
        		
        		obj.FileName=[];
			obj.FileHandle=[];
			obj.Keywords=[];
			obj.Values=[];              		
			obj.CommentLines=[];
			obj.LineOrder=[];
			obj.NoComment=false;
			obj.PointUnderScore=false;
			obj.ScoreStruct=[];
			
			if ~isempty(FileName)
				obj.loadFile(FileName);
			end
        	end
        	
        	
        	function loadFile(obj,FileName)
        		%load existing file and analyses it
        		
        		obj.FileName=FileName;
        		
        		obj.analyseFile;
        	end
        	
        	
        	function setFile(obj,FileName)
        		%meant for new files in case a new prop file is wanted
        		obj.FileName=FileName;
        	end
        	
        	
        	function analyseFile(obj)
        		%This function does most of the work, it will load the file
        		%and check line for line for keywords and values
        		
        		import mapseis.util.importfilter.*;
        		
        		try
        			fid = fopen(obj.FileName);
        		catch
        			error('File could not be opened')
        		end
        		
        		inStr = fscanf(fid,'%c');
        		fclose(fid);
        		
        		if ~isempty(inStr)
        			rp = regexpPatternCatalog();	
        			rowSepPat=rp.line;
        			
        			TheLines= regexp(inStr,rowSepPat,'match');
        			
        			for i=1:numel(TheLines)
        				%first check if it is a comment
        				CurLine=TheLines{i};
        				
        				%remove end-of-line
        				EndLiner=regexp(CurLine, '\n');
        				if ~isempty(EndLiner)
        					CurLine(EndLiner)='';
        				end
        				
        				if isempty(CurLine)
        					obj.LineOrder{i,1}='e';
        					obj.LineOrder{i,2}=0;
        					continue;
        				end
        				
        				CheckLine=CurLine(CurLine~=' ');
        				
        				
        				if CheckLine(1)=='#'
        					%it's a comment
        					obj.CommentLines{end+1}=CurLine;
        					obj.LineOrder{i,1}='c';
        					obj.LineOrder{i,2}=numel(obj.CommentLines);
        					continue;
        				end
        				
        				%find the equal sign
        				EquPos=find(CurLine=='=');
        				
        				if isempty(EquPos)
        					warning('line could not be read')
        					obj.LineOrder{i,1}='s';
        					obj.LineOrder{i,2}=-1;
        					continue;
        				end
        				
        				TheKey = CurLine(1:EquPos-1);
        				
        				%remove spaces
        				TheKey(TheKey==' ')='';        				       				
        				
        				%store
        				obj.Keywords{end+1}=TheKey;
        				
        				
        				TheVal=CurLine(EquPos+1:end);
        				
        				%remove spaces at the end and begining
        				%not very elegant but the lines are small
        				%anyway
        				if TheVal(1)==' '
        					c=1;
        					while TheVal(c)==' '
        						TheVal(c)='';
        						c=c+1;
        					end
        				end
        				
        				if TheVal(end)==' '
        					c=numel(TheVal);
        					while TheVal(c)==' '
        						TheVal(c)='';
        						c=c-1;
        					end
        				end
        				
        				obj.Values{end+1}=TheVal;
        				
        				obj.LineOrder{i,1}='k';
        				obj.LineOrder{i,2}=numel(obj.Keywords);
        				
        				
        			end
        		end
        	end
        	
        	
        	
        	function PropVal = getProperty(obj,KeyWord)
        		%returns Property for a certian Keyword
        		%usage: PropVal =  obj.getProperty(KeyWord)
        		
        		PropVal=[];
        		
        		FindVal=strcmp(KeyWord,obj.Keywords);
        		
        		if any(FindVal)
        			if sum(FindVal)>1
        				error('More than one Key found')
        			end
        			
        			PropVal=obj.Values{FindVal};
        		else
        			warning('Key not found')
        		end
        		
        		
        	
        	end
        	
        	
        	function KeyList = getKeys(obj)
        		%Returns all existing keys
        		KeyList = obj.Keywords;
        	end
        	
        	
        	
        	function setProperty(obj,KeyWord,PropVal)
        		%Sets the property of a new or existing Keyword
        		%usage: obj.setProperty(KeyWord,PropVal)
        	
        		%check if key already exists
        		FindVal=strcmp(KeyWord,obj.Keywords);
        		
        		if isnumeric(PropVal)
        			%auto-convert, but I would suggest to convert
        			%values before, to have more control over the 
        			%format
        			PropVal=num2str(PropVal);
        		end
        		
        		if any(FindVal)
        			if sum(FindVal)>1
        				error('More than one Key found')
        			end
        			
        			disp('Value overwritten');
        			
        			obj.Values{FindVal} = PropVal;
        			
        		else
        			%new key, add it to the end
        			obj.Keywords{end+1}=KeyWord;
        			obj.Values{end+1}=PropVal;
        			obj.LineOrder{end+1,1}='k';
        			obj.LineOrder{end,2}=numel(obj.Keywords);
        		end
        	end
        	
        	
        	function removeProperty(obj,KeyWord)
        		%removes a key and its value from the key, this is done 
        		%by replacement of the key and the value to [] and by setting
        		%the LineOrder row to 's' -1. This means the LineOrder list 
        		%can be kept with re-ordering it, but in case of many ereased
        		%and added properties, the object will be unnecessary large.
        		%It should not be a problem, as config files are normally
        		%1000 of lines long, so it should be all very handable.
        		
        		FindVal=strcmp(KeyWord,obj.Keywords);
        		
        		if any(FindVal)
        			if sum(FindVal)>1
        				warning('More than one Key found')
        				FindVal=find(strcmp(KeyWord,obj.Keywords));
        				
        				for i=1:numel(FindVal)
        					obj.Keywords{FindVal(i)}=[];
        					obj.Values{FindVal(i)}=[];
        					
        					obj.LineOrder{FindVal(i),1}='s';
        					obj.LineOrder{FindVal(i),2}=-1;
        				end
        				
        				
        			else
        				obj.Keywords{FindVal}=[];
        				obj.Values{FindVal}=[];
        				
        				obj.LineOrder{FindVal,1}='s';
        				obj.LineOrder{FindVal,2}=-1;
        			end
        		
        		else
        			warning('Key not found')
        			
        		end
        		
        		
        	end
        	
        	
        	function resetProps(obj)
        		%reset everything apart from filename
        	
        		obj.Keywords=[];
			obj.Values=[];              		
			obj.CommentLines=[];
			obj.LineOrder=[];
			
			
        	end
        	
        	
        	function resetAll(obj)
        		%reset everything also the filename
        		obj.resetProps
        		obj.FileName=[];
        		
        	end
        	
        	
        	function TheStruct = exportStruct(obj)
        		%exports every property into a struct with 
        		%keynames as fieldnames
        		
        		TheStruct=[];
        		
        		for i=1:numel(obj.Keywords)
        			if ~isempty(obj.Keywords{i})
        				ThePoints=find(obj.Keywords{i}=='.');
        				if isempty(ThePoints)
        					TheStruct.(obj.Keywords{i})=obj.Values{i};
        				else
        					disp('. has been replaced with _')
        					obj.PointUnderScore=true;
        					TheKey=obj.Keywords{i};
        					TheKey(ThePoints)='_';
        					obj.ScoreStruct.(TheKey)=ThePoints;
        					TheStruct.(TheKey)=obj.Values{i};
        				end
        			end
        		end
        	end
        	
        	
        	function importStruct(obj,TheStruct)
        		%imports all entries of a struct, with the fieldnames being
        		%the keys
        		TheFields=fieldnames(TheStruct);
        		
        		if obj.PointUnderScore
        			%generate the point structure if needed
        			try %that should do the trick
        				Temp = obj.exportStruct;
        			end	
        		end
        		
        		if ~isempty(TheFields)
        			for i=1:numel(TheFields)
        				CurKey=TheFields{i};
        				TheVal=TheStruct.(TheFields{i});
        				
        				if isnumeric(TheVal)
        					TheVal=num2str(TheVal);
        				end
        				
        				if obj.PointUnderScore
        					UScore=find(CurKey=='_');
        					if ~isempty(UScore)
        						if isfield(obj.ScoreStruct,CurKey)
        							CurKey(obj.ScoreStruct.(CurKey))='.';
        						end
        					end
        				
        				end
        				
        				try
        					setProperty(obj,CurKey,TheVal);
        				catch
        					warning('could not import all values')
        				end
        			end
        			
        		end
        		
        		
        	end
        	
        	
        	function writeFile(obj,FileName)
        		%Writes all keys into a file. Either into the previous 
        		%defined FileName or into a new File with the Filename
        		%specified in the input
        		%usage: obj.writeFile(FileName) ->use new FileName
        		%       obj.writeFile -> use internal filename
        	
        	
        		if nargin<2
        			FileName=[]
        		end
        		
        		if isempty(FileName)
        			FileName=obj.FileName;
        			
        			%check again
        			if isempty(FileName)
        				error('No filename specified')
        			end
        		end
        		
        		
        		try
        			fid = fopen(FileName,'w');
        		catch
        			error('File could not be opened')
        		end
        		
        		for i=1:numel(obj.LineOrder(:,1))
        		
        			switch obj.LineOrder{i,1}
        				case 'c'
        					if ~obj.NoComment
        						TheLine=obj.CommentLines{obj.LineOrder{i,2}};
        						fprintf(fid,'%s',TheLine); 
        						fprintf(fid,'\n');
        					end
        					
        				case 'k'
        					TheLine=[obj.Keywords{obj.LineOrder{i,2}},'=',obj.Values{obj.LineOrder{i,2}}];
        					fprintf(fid,'%s',TheLine); 
        					fprintf(fid,'\n');
        					
        				case 'e'
        					fprintf(fid,'\n');
        				case 's'
        					%do nothing
        			end
        			
        			
        		end
        		
        		
        		fclose(fid);
        		
        	
        	
        	end
        	
        	
        	
        	
        	
        end

end	

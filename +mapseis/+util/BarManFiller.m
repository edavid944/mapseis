function [xData yData] = BarManFiller(YBar,XBar,YOffSet,YHigh,YBound)
	%converts a bar like plot into a scaled area data
	
	%bins have to be equally spaced
	myDiff=diff(XBar);
	binDiff=myDiff(1);
	
	%norm the YBar
	if isempty(YHigh)
		YHigh=max(YBar);
	end
	
	if isempty(YOffSet)
		YOffSet=0;
	end
	
	if isempty(YBound)
		YBound(1)=min(YBar);
		YBound(2)=max(YBar);
	end
	
	ConvY=YOffSet+(YBar-YBound(1))./(YBound(2)-YBound(1))*YHigh;	
	
	xData=[];
	yData=[];
	
	for i=1:numel(XBar)
		curX=[XBar(i)-binDiff/2,XBar(i)+binDiff/2,XBar(i)+binDiff/2,XBar(i)-binDiff/2,XBar(i)-binDiff/2];
		curY=[YOffSet,YOffSet,ConvY(i),ConvY(i),YOffSet];
		xData=[xData,curX];
		yData=[yData,curY];
	
	end


end

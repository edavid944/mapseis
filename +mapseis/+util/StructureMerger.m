function CombinedStruct=StructureMerger(InputStruct,CSEPMode)
	%Merges all structures order in a cell array together into one
	%In the CSEPMode it will take special care of CSEP test specific fields
	
	if nargin<2
		CSEPMode=false;
	end
	
	CombinedStruct=struct;
	
	for i=1:numel(InputStruct)
		NewFields=fieldnames(InputStruct{i});
		
		for j=1:numel(NewFields)
			
			if isfield(CombinedStruct,NewFields{j})
				%Field exists already add them up as cell array
				if iscell(CombinedStruct.(NewFields{j}))
					CombinedStruct.(NewFields{j})=...
						{CombinedStruct.(NewFields{j}){:},InputStruct{i}.(NewFields{j})};
				else
					CombinedStruct.(NewFields{j})={CombinedStruct.(NewFields{j}),InputStruct{i}.(NewFields{j})}
				end
			else
				CombinedStruct.(NewFields{j})=InputStruct{i}.(NewFields{j});
				
			end
		end

	end	
	
	disp(CombinedStruct)
	
	if CSEPMode
		CombinedStruct.Nr_MonteCarlo_Cell=CombinedStruct.Nr_MonteCarlo;
		CombinedStruct.Nr_MonteCarlo=CombinedStruct.Nr_MonteCarlo_Cell{1};
		CombinedStruct.Simulate_Catalog_Cell=CombinedStruct.Simulate_Catalog;
		CombinedStruct.Simulate_Catalog=CombinedStruct.Simulate_Catalog_Cell{1};
		
		%Correct the names
		CorNames={};
		for i=1:numel(InputStruct)
			CorNames={CorNames{:},InputStruct{i}.Names{:}};
		
		end
		
		CombinedStruct.Names=sort(CorNames);
	end

end

function ShiftedLocations=ShiftCoords(Locations,Offsets)
        %Shift the coordinates [lon lat] by the inputed offset
	%[LonOffset LatOffset]	
	
	%Apply Offset
	ShiftedLocations(:,1)=Locations(:,1)+Offsets(1);	
	ShiftedLocations(:,2)=Locations(:,2)+Offsets(2);	
		 	
	%correct overboarding coords
	%Lon
	LargerLocSel=ShiftedLocations(:,1)>180;
	SmallerLocSel=ShiftedLocations(:,1)<-180;
	
	ShiftedLocations(LargerLocSel,1)=ShiftedLocations(LargerLocSel,1)-360;
	ShiftedLocations(SmallerLocSel,1)=ShiftedLocations(SmallerLocSel,1)+360;
	
	
	%Lat
	LargerLocSel=ShiftedLocations(:,2)>90;
	SmallerLocSel=ShiftedLocations(:,2)<-90;
	 	
	ShiftedLocations(LargerLocSel,2)=ShiftedLocations(LargerLocSel,2)-180;
	ShiftedLocations(SmallerLocSel,2)=ShiftedLocations(SmallerLocSel,2)+180;
	

        	
end

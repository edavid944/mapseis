function SameCell=CellCompare(CellA,CellB,StrictMode)
	%Compares the values of two Cellarrays.
	
	
	import mapseis.util.*;
	

	if nargin<3
		StrictMode=true;
	end
	
	
	
	if numel(CellA)~=numel(CellB)
		SameCell=false;
		return;
	
	else
		
		AllEqual=true;
		for i=1:numel(CellA)
			%determine type of the fields
			ValA=CellA{i};
			ValB=CellB{i};
		
			Temp=whos('ValA');
			TypeA=Temp.class;
			
			Temp=whos('ValB');
			TypeB=Temp.class;
			
			if ~strcmp(TypeA,TypeB)
				SameCell=false;
				return;
			else
				%don't know if all this types will ever turn up in a 
				%'whos' output, but I want to be on the save side
				switch TypeA
					case {'double','logical','integer','numeric'}
						AllEqual=AllEqual&all(ValA(:)==ValB(:));
					
					case {'string','char'}
						AllEqual=AllEqual&strcmp(ValA,ValB);
						
					case 'cell'
						AllEqual=AllEqual&CellCompare(ValA,ValB);
					
					case 'struct'
						AllEqual=AllEqual&StructCompare(ValA,ValB,StrictMode);
					
					case '(unassigned)'
						AllEqual=AllEqual;
					
					otherwise	
						disp('Type not supported in Compare')
						SameCell=[];
						return;
				
				end
			
				if ~AllEqual
					SameCell=false;
					return
				end
			
			end
		end
		
		SameCell=true;
			
		
	end
	
	


end

classdef CountObject < handle
    %All it does is counting, and it is meant for grid testing and similar
    
    properties 
      	CurrentNumber
      	maxNumber
      	TheBar
    end
    
   
    
    methods 
        function obj=CountObject(startValue)
            if isempty(startValue)
            	obj.CurrentNumber=0;
            else
            	obj.CurrentNumber=startValue;
            end
            
        end
        
        
        function val = getCounter(obj)
        	val = obj.CurrentNumber;
        end
        
        
        function IncCounter(obj)
        	obj.CurrentNumber=obj.CurrentNumber+1;	
        end
        
        
        function setCounter(obj,Val)
            if isempty(Val)
            	obj.CurrentNumber=0;
            else
            	obj.CurrentNumber=Val;
            end
        
        end
        
        
        function AddProgressbar(obj,maxNumber)
        	 obj.TheBar = waitbar(2,'Calculating...');
        	 obj.maxNumber=maxNumber;
        	 waitbar(0,obj.TheBar);
        end
        
        function CountBar(obj)
        	obj.CurrentNumber=obj.CurrentNumber+1;		
        	
        	if rem(obj.CurrentNumber,50) == 0
        		val=obj.CurrentNumber./obj.maxNumber;
        		val(val>1)=1;
        		val(val<0)=0;
        		val(isempty(val))=0;
        		waitbar(val,obj.TheBar);
        	end
        	
        end
        
        function ResetBar
        	try
        		close(obj.TheBar)
        	end	
        end
        
        
    end
    
end    

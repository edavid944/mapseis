function SameStruct=StructCompare(StructA,StructB,StrictMode)
	%Compares the values of all fields. If StrictMode is true
	%both structures have to have the same fields. Without StrictMode
	%the fields of StructA matters, every additional field in structB will
	%be ignored
	
	import mapseis.util.*;
	
	if nargin<3
		StrictMode=true;
	end
	
		
	FieldsA=fieldnames(StructA);
	FieldsB=fieldnames(StructB);
	
	if StrictMode
		if numel(FieldsA)~=numel(FieldsB)
			SameStruct=false;
			return;
		end
	end
	
	
	if numel(FieldsA)>numel(FieldsB)
		SameStruct=false;
		return;
	
	else
		if ~all(isfield(StructB,FieldsA))
			SameStruct=false;
			return;
			
		else	
			AllEqual=true;
			for i=1:numel(FieldsA)
				%determine type of the fields
				ValA=StructA.(FieldsA{i});
				ValB=StructB.(FieldsA{i});
			
				Temp=whos('ValA');
				TypeA=Temp.class;
				
				Temp=whos('ValB');
				TypeB=Temp.class;
				
				if ~strcmp(TypeA,TypeB)
					SameStruct=false;
					return;
				else
					%don't know if all this types will ever turn up in a 
					%'whos' output, but I want to be on the save side
					switch TypeA
						case {'double','logical','integer','numeric'}
							AllEqual=AllEqual&all(ValA(:)==ValB(:));
						
						case {'string','char'}
							AllEqual=AllEqual&strcmp(ValA,ValB);
							
						case 'cell'
							AllEqual=AllEqual&CellCompare(ValA,ValB,StrictMode);
						
						case 'struct'
							AllEqual=AllEqual&StructCompare(ValA,ValB,StrictMode);
						
						case '(unassigned)'
							AllEqual=AllEqual;
						
						otherwise	
							disp('Type not supported in Compare')
							SameStruct=[];
							return;
					
					end
				
					if ~AllEqual
						SameStruct=false;
						return
					end
				
				end
			end
			
			SameStruct=true;
		end
	
		
	end
	
	


end

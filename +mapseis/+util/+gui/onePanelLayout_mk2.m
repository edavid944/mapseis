function guiHnd = onePanelLayout_mk2(figHand,figName,axProp)
% onePanelLayout : creates a figure with a single plot axis labeled 'axis'

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-10-13 12:17:41 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell


if nargin <3
	figHand=[];
end

if isempty(figHand)
	guiHnd = figure('name',figName,'visible','off');
else
	guiHnd=figHand;
	set(guiHnd,'name',figName,'visible','on');
end


%guiHnd = figure('name',figName,'visible','off');
ax1 = subplot(1,1,1);
set(ax1,'Tag','axis',axProp{:});

    

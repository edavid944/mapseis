classdef paramMinMaxWidget < handle
    % paramMinMaxWidget : GUI widget for selecting a min and max param range
    % The GUI consists of two edit boxes to select the parameter values,
    % together with two checkboxes to enable or disable the choice of min or
    % max parameter eg to select points above a certain value instead of
    % between values.
    % Arguments :
    %   parentHandle : handle of the parent GUI container
    %   outStrs : string arguments for the 4 possible enable checkbox states eg
    %    false,false -> 'all', false,true -> 'below', true,false -> 'above',
    %    true,true -> 'in'  for checkbox order Min, Max
    %   callbackFcn : function to be called when GUI state changes
    % Example:
    % f = figure;
    % paramMinMaxWidget(f,struct('min',0,'max',10,...
    %   'outStrs',{{'all','above','below','in'}},...
    %   'callbackFcn',@(outStr, outVal) disp([outStr,' ',num2str(outVal')])))
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    
    properties
        MinCheckBox
        MaxCheckBox
        MinEdit
        MaxEdit
        TypeStrs
        EditDisplayFcn
        Filter
        DataStore
    end
    
    
    methods
        function obj=paramMinMaxWidget(parentHandle,initPars,widgetLabel,dataStore,filterObj)           
            % Handle to the dataStore that the filter acts on
            obj.DataStore = dataStore;
            
            % Handle to the corresponding filter
            obj.Filter = filterObj;            
            
            % Strings for the different range types
            obj.TypeStrs = filterObj.TypeStr;
                        
            % Create the GUI widgets inside the parent handles
            obj.MinCheckBox = uicontrol(parentHandle,'Style','checkbox',...
                'Units','Normalized','Position',[0.25 0.5 0.25 0.5],'String','Min');
            obj.MaxCheckBox = uicontrol(parentHandle,'Style','checkbox',...
                'Units','Normalized','Position',[0.75 0.5 0.25 0.5],'String','Max');
            obj.MinEdit = uicontrol(parentHandle,'Style','edit',...
                'Units','Normalized','Position',[0 0 0.5 0.5]);
            obj.MaxEdit = uicontrol(parentHandle,'Style','edit',...
                'Units','Normalized','Position',[0.5 0 0.5 0.5]);
            
            % Create a label
%                 'FontWeight','bold','Fontsize',12,...
% 'String',paramStruct(paramIndex).label,...
%                     'Position',[0.05 ...
%                     (0.2*paramHeight+(1-paramHeight*paramIndex))...
%                     0.3 0.6*paramHeight]
            uicontrol(parentHandle,'Units','Normalized','style','text',...
                    'String',widgetLabel,...
                    'Position',[0 0.5 0.25 0.5]);
                
            % Function to print the parameter value in the edit box
            % By default this is num2str, could overwrite with eg datestr for date
            % fields.  Set by initPars.editDisplayFcn
            if isfield(initPars,'editDisplayFcn')
                obj.EditDisplayFcn = initPars.editDisplayFcn;
            else
                obj.EditDisplayFcn = @num2str;
            end
            
            obj.initGUI(initPars);           
        end
        
        
        function updateGUI(obj)
            % Update the GUI widgets based on the current state of the
            % underlying event data structure
            
            % For the min/max parameter widget the state of the parameter is
            % set by the type of range ('all','above','below','in' for example)
            % and the range itself
            
            % Get the appropriate range parameters
            rangePars = obj.Filter.getRange();
            if isempty(rangePars)
                return
            end
            % Get the range type from the first element of the cell array
            rangeType = rangePars{1};
            % Get the range itself from the second element of the cell array
            range = rangePars{2};
            % Set the checkboxes and edit boxes depending on the range type
            if strcmp(rangeType,obj.TypeStrs{1})
                % Both ranges off, uncheck the checkboxes and leave the edit
                % boxes unaltered
                set(obj.MinCheckBox,'Value',false);
                set(obj.MaxCheckBox,'Value',false);
            elseif strcmp(rangeType,obj.TypeStrs{2})
                % Minimum range set on (eg 'above' case)
                set(obj.MinCheckBox,'Value',true);
                set(obj.MaxCheckBox,'Value',false);
                % Set min edit box to range parameter
                set(obj.MinEdit,'String',obj.EditDisplayFcn(range(1)));
            elseif strcmp(rangeType,obj.TypeStrs{3})
                % Maximum range set on (eg 'below' case)
                set(obj.MinCheckBox,'Value',false);
                set(obj.MaxCheckBox,'Value',true);
                % Set max edit box to range parameter
                set(obj.MaxEdit,'String',obj.EditDisplayFcn(range(2)));
            elseif strcmp(rangeType,obj.TypeStrs{4})
                % Both range limits set (eg 'in')
                set(obj.MinCheckBox,'Value',true);
                set(obj.MaxCheckBox,'Value',true);
                % Set both edit boxes to range parameter
                set(obj.MinEdit,'String',obj.EditDisplayFcn(range(1)));
                set(obj.MaxEdit,'String',obj.EditDisplayFcn(range(2)));
            else
                error('paramMinMaxWidget:unknown_rangeType','Unknown range type');
            end
        end
        
        function initGUI(obj,initPars)
            % Set the initial state of the GUI depending on initPars, or give
            % default values if not specified
            
            % Default values
            minVal = 0;
            maxVal = 1;
            
            % Overwrite default values with values from initPars
            if isfield(initPars,'min')
                minVal = initPars.min;
            end
            
            if isfield(initPars,'max')
                maxVal = initPars.max;
            end
            
            if isfield(initPars,'editDisplayFcn')
                obj.EditDisplayFcn = initPars.editDisplayFcn;
            end
            
            % Set the properties of the GUI widgets
            set(obj.MinEdit,'String',minVal,...
                'Callback',@(s,e) obj.setFilterParams());
            set(obj.MaxEdit,'String',maxVal,...
                'Callback',@(s,e) obj.setFilterParams());
            set(obj.MinCheckBox,...
                'Callback',@(s,e) obj.setFilterParams());
            set(obj.MaxCheckBox,...
                'Callback',@(s,e) obj.setFilterParams());
            
            % Update the GUI
            obj.updateGUI()
        end
        
        function setFilterParams(obj)
            % Enable or disable edit boxes depending on checkbox states
            outArray = [nGetEditValue(obj.MinEdit);nGetEditValue(obj.MaxEdit)];
            
            % Get the current range
            currentRange = obj.Filter.Range;
            if ~nIsOn(obj.MinCheckBox)
                outArray(1) = currentRange(1);
            end
            
            if ~nIsOn(obj.MaxCheckBox)
                outArray(2) = currentRange(2);
            end
%             outArray = outArray([nIsOn(obj.MinCheckBox); nIsOn(obj.MaxCheckBox)]);
            
            % Set the output string argument depending on checkbox states
            outStr = nGetOutStr(obj.MinCheckBox,obj.MaxCheckBox);
            
            % Call the callback function
            obj.Filter.setRange(outStr,outArray);
            % Execute the filter but don't update the GUI yet (leave this
            % for the parent ParamGUI)
            obj.Filter.execute(obj.DataStore,false);         
            
            function onBool=nIsOn(cbHnd)
                % Check if a checkbox is selected, return true or false
                onBool = logical(get(cbHnd,'value'));
            end
            
            function editVal = nGetEditValue(editHnd)
                editStr = get(editHnd,'String');
                % Get the numeric value of a edit widget
                editVal = str2double(editStr);
                % If the value is NaN then try parsing it as a date instead in
                % format 31 : 'yyyy-mm-dd HH:MM:SS' eg 2000-03-01 15:45:17
                if isnan(editVal)
                    editVal = datenum(editStr,31);
                end
            end
            
            function outStr = nGetOutStr(cbHndMin,cbHndMax)
                % Return the string corresponding to checkbox select state
                if ~nIsOn(cbHndMin) && ~nIsOn(cbHndMax)
                    outStr = obj.TypeStrs{1};
                elseif nIsOn(cbHndMin) && ~nIsOn(cbHndMax)
                    outStr = obj.TypeStrs{2};
                elseif ~nIsOn(cbHndMin) && nIsOn(cbHndMax)
                    outStr = obj.TypeStrs{3};
                elseif nIsOn(cbHndMin) && nIsOn(cbHndMax)
                    outStr = obj.TypeStrs{4};
                else
                    error('paramMinMaxWidget:check_state','Inconsistent checkbox state');
                end
            end
            
        end
        
    end
    
end
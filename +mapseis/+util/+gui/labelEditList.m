function bgHandle = labelEditList(parentHandle,cbParamStruct)
% labelEditList : create a vertical labelled edit box layout
% cbParamStruct has fields :
%   String, Tag, Value which are mx1 arrays (or cell arrays)

% Copyright 2007-2007 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

% Create a button group
bgHandle = uibuttongroup('Parent',parentHandle,...
    'Units','Normalized');

cbStrings = cbParamStruct.String;
cbTags = cbParamStruct.Tag;
cbValues = cbParamStruct.Value;

% Number of checkboxes
numCheckBoxes = numel(cbValues);

for i=1:numCheckBoxes
    uicontrol('Parent',bgHandle,...
        'style','Text',...
        'String',cbStrings{i},...
        'Units','Normalized',...
        'Position',[0 1-i/numCheckBoxes 0.5 1/numCheckBoxes]);
    eb=uicontrol('Parent',bgHandle,...
        'style','edit',...
        'Tag',cbTags{i},...
        'Units','Normalized',...
        'Position',[0.5 1-i/numCheckBoxes 0.5 1/numCheckBoxes]);
    if isnumeric(cbValues{i}) 
        % If the value is numeric then convert to string to populate the
        % edit box. 
        set(eb,'Value',cbValues{i});
        set(eb,'String',num2str(cbValues{i}));
    elseif islogical(cbValues{i}) 
        % If the value is logical then change the style of the 'edit box'
        % to 'checkbox'
        set(eb,'Style','Checkbox');
        set(eb,'Value',cbValues{i});
    else
        set(eb,'String',cbValues{i});
    end
        
end

end
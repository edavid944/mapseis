function bgHandle = createButtonList(parentHandle,buttonStyle,buttonParamStruct)
% createCheckBoxList : create a vertical checkbox layout
% cbParamStruct has fields :
%   String, Tag, Value which are mx1 arrays (or cell arrays)

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

% Create a button group.  This acts the same as a uipanel unless the
% contents are radio buttons, in which case it handles the button selection
% logic.
bgHandle = uibuttongroup('Parent',parentHandle,...
    'Units','Normalized');

% Read the strings to display for the buttons from the input struct
buttonStrings = buttonParamStruct.String;
% Read the tags to use for each button from the input struct
buttonTags = buttonParamStruct.Tag;
% Read the default value for each button.  This will be true or false for
% radio butoons or checkboxes
buttonValues = buttonParamStruct.Value;

% Number of buttons 
buttonCount = numel(cbValues);

for i=1:buttonCount
    uicontrol('Parent',bgHandle,...
        'style',buttonStyle,...
        'String',buttonStrings{i},...
        'Tag',buttonTags{i},...
        'Value',buttonValues(i),...
        'Units','Normalized',...
        'Position',[0 1-i/buttonCount 1 1/buttonCount]);
end

end
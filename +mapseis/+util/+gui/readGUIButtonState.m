function outStruct = readGUIButtonState(fHandle,prefixName,fldNames)
% readGUIButtonState : read the elements tagged with fldnames from fig
% Create an output structure that contains the fields fldNames read from
% the tagged elements in the figure referenced by fHandle with the same
% names.

% Copyright 2007-2007 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

for i = 1:numel(fldNames)
    widgetHndl = findobj(fHandle,'Tag',[prefixName,'_',fldNames{i}]);
    widgetStyle = get(widgetHndl,'style');
    switch widgetStyle
        case {'radiobutton','checkbox','slider'}
            outStruct.(fldNames{i})=get(widgetHndl,'Value');
        case 'edit'
            widgetStr=get(widgetHndl,'String');
            widgetVal = str2double(widgetStr);
            if ~isnan(widgetVal)
                outStruct.(fldNames{i})=widgetVal;
            else
                outStruct.(fldNames{i})=widgetStr;
            end
    end            
end

end
function bgStruct=createMultipleRadio(fHandle,prefixName,inStruct,radioButtonFields)
% Create GUI with multiple panels, containing checkBoxes and radio buttons

% Copyright 2007-2007 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

% Strings and Tags are the fieldnames of the struct

allFldNames = fieldnames(inStruct);
totalNumFields = numel(allFldNames);
widgetHeight = 1/totalNumFields; % Height of widgets in normalised units

% Remove the radio button fields from the list of checkboxes
fldNames = setdiff(allFldNames,[radioButtonFields{:}]);

%% Create the checkboxes
cbParam.String = fldNames;
cbParam.Tag = fldNames;
cbParam.Value = zeros(numel(fldNames),1);

for i=1:numel(fldNames)
    cbParam.Tag{i} = [prefixName,'_',cbParam.Tag{i}];
    cbParam.Value(i) = inStruct.(fldNames{i});
end

%% Create the radio buttons

for i=1:numel(radioButtonFields)
    rbParam(i).String = radioButtonFields{i};
    rbParam(i).Tag = radioButtonFields{i};
    rbParam(i).Value = zeros(numel(radioButtonFields{i}),1);
    
    for j=1:numel(rbParam(i).Tag)
        rbParam(i).Value(j) = inStruct.(rbParam(i).Tag{j});
        rbParam(i).Tag{j}=[prefixName,'_',rbParam(i).Tag{j}];
    end
end
%% Create two uipanels and put the button groups into them
% fHandle = figure('visible','off');

cbPanelHeight = widgetHeight*numel(fldNames);

cbPanelHandle = uipanel('Parent',fHandle,'Units','Normalized',...
    'Position',[0 1-cbPanelHeight 1 cbPanelHeight]);

lastHeight = cbPanelHeight;
for i=1:numel(radioButtonFields)
    cbPanelHeight = widgetHeight*numel(radioButtonFields{i});

    rbPanelHandle(i) = uipanel('Parent',fHandle,'Units','Normalized',...
        'Position',[0 1-cbPanelHeight-lastHeight 1 cbPanelHeight]);
    lastHeight = lastHeight + cbPanelHeight;
end

cbBgHandle = createCheckBoxList(cbPanelHandle,cbParam);
for i=1:numel(radioButtonFields)
    %rbBgHandle(i) = ceateRadioButtonList(rbPanelHandle(i),rbParam(i));
    rbBgHandle(i) = createCheckBoxList(rbPanelHandle(i),rbParam(i));
end
bgStruct = struct('cbHandle',cbBgHandle,...
    'rbHandle',rbBgHandle);

%% Set figure to visible
set(fHandle,'Visible','on');
end
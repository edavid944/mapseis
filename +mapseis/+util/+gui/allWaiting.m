function allWaiting(TheState)
	%this functions sets all window cursor to hourclass
	
	
	Thefigs=findobj('type','figure');
	
	switch TheState
	case 'wait'
		set(Thefigs,'Pointer','watch');
        drawnow
	case 'move'
		set(Thefigs,'Pointer','arrow');
        drawnow
	end	
	
end

function ResObj = ResultGUILoader(CalcObj,FigureName,WindowPos,ListProxy,RegParam)
	%This function load the suitable version of the resultGUI
	
	import mapseis.gui.*;

	%get needed Version
	Versio = CalcObj.NeededResultGUI;
	
	%at the moment only one version available
	switch Versio
		case '1.0'
			ResObj=ResultViewerGUI(CalcObj,FigureName,WindowPos,ListProxy,RegParam)
		
	end	
end

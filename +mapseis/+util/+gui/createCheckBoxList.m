function bgHandle = createCheckBoxList(parentHandle,cbParamStruct)
% createCheckBoxList : create a vertical checkbox layout
% cbParamStruct has fields :
%   String, Tag, Value which are mx1 arrays (or cell arrays)

% Copyright 2007-2007 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

bgHandle=createButtonList(parentHandle,'checkbox',cbParamStruct);

end
function guiHnd= threePanelLayout(figName,figProperties,axesProperties,figHand)
% onePanelLayout : creates a figure with three plot axes
% The figure is divided into a plot axis occupying the entire left-hand
% side of the figure and two subplots on the right hand side of the figure.

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-10-13 12:17:41 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if nargin <4
	figHand=[];
end

% Create an array to store the subplot axes handles
axHnds = zeros(3,1);

if isempty(figHand)
	guiHnd = figure('name',figName,'visible','off',figProperties{:});
else
	guiHnd=figHand;
	set(guiHnd,'name',figName,'visible','on',figProperties{:});
end

axHnds(1) = subplot(2,2,[1 3]);
set(axHnds(1),'Tag','axis_left');
axHnds(2) = subplot(2,2,2);
set(axHnds(2),'Tag','axis_top_right');
axHnds(3) = subplot(2,2,4);
set(axHnds(3),'Tag','axis_bottom_right');

% If there is more than one input argument then assume varargin is a cell
% array of cell arrays of properties for the above axes, in the order given
for paramIndex = 1:numel(axesProperties)
    set(axHnds(paramIndex),axesProperties{paramIndex}{:});
end
function bgHandle=createLabelEditList(parentHandle,prefixName,inStruct)
% Test the checkBoxList function using inStruct

% Copyright 2007-2007 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

% Strings and Tags are the fieldnames of the struct

fldNames = fieldnames(inStruct);
cbParam.String = fldNames;
cbParam.Tag = fldNames;
cbParam.Value = cell(numel(fldNames),1);

for i=1:numel(fldNames)
    cbParam.Tag{i} = [prefixName,'_',cbParam.Tag{i}];
    cbParam.Value{i} = inStruct.(fldNames{i});
end

bgHandle = labelWidgetList(parentHandle,cbParam);

end
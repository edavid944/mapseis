function ResConf = ResultViewerPresets(mainPres)
	% Returns the configuration for the windows of the Resultviewer
	% mainPres: The wanted preset
	% ResConf: The configuration as structur

%Setting for the OneWindow
OneWindow.PlotNames={'mainPlot'};
OneWindow.Common.main.Tag='mainPlot';
OneWindow.SubplotIndices = [1,1];

OneWindow.Common.mainPlot.none = [0.09 0.12 0.82 0.8];

OneWindow.Common.mainPlot.Bottom=[0.09 0.19 0.82 0.75];

OneWindow.Common.mainPlot.Left=[0.25 0.10 0.70 0.80];

OneWindow.Common.mainPlot.Right=[0.06 0.10 0.70 0.80];

OneWindow.Common.mainPlot.all=NaN;

OneWindow.Single = OneWindow.Common;


%---------------------------------------------------------


%Setting for TwoWindowHor
TwoWindowHor.PlotNames={'TopPlot','BottomPlot'};
TwoWindowHor.SubplotIndices = [2,1];

%Common
TwoWindowHor.Common.TopPlot.Tag = 'TopPlot';
TwoWindowHor.Common.BottomPlot.Tag = 'BottomPlot';

TwoWindowHor.Common.TopPlot.none = [0.09 0.57 0.82 0.4];
TwoWindowHor.Common.BottomPlot.none = [0.09 0.08 0.82 0.4];

TwoWindowHor.Common.TopPlot.Bottom = [0.09 0.62 0.82 0.34];
TwoWindowHor.Common.BottomPlot.Bottom = [0.09 0.20 0.82 0.34];

TwoWindowHor.Common.TopPlot.Left = [0.20 0.57 0.75 0.4];
TwoWindowHor.Common.BottomPlot.Left = [0.20 0.08 0.75 0.4];

TwoWindowHor.Common.TopPlot.Right = [0.05 0.57 0.75 0.4];
TwoWindowHor.Common.BottomPlot.Right = [0.05 0.08 0.75 0.4];

TwoWindowHor.Common.TopPlot.all = NaN;
TwoWindowHor.Common.BottomPlot.all = NaN;


%Single
TwoWindowHor.Single.TopPlot.Tag = 'TopPlot';
TwoWindowHor.Single.BottomPlot.Tag = 'BottomPlot';

TwoWindowHor.Single.TopPlot.none = [0.09 0.57 0.82 0.4];
TwoWindowHor.Single.BottomPlot.none = [0.09 0.08 0.82 0.4];

TwoWindowHor.Single.TopPlot.Bottom = [0.09 0.65 0.82 0.33];
TwoWindowHor.Single.BottomPlot.Bottom = [0.09 0.16 0.82 0.33];

TwoWindowHor.Single.TopPlot.Left = [0.20 0.57 0.75 0.4];
TwoWindowHor.Single.BottomPlot.Left = [0.20 0.08 0.75 0.4];

TwoWindowHor.Single.TopPlot.Right = [0.05 0.57 0.75 0.4];
TwoWindowHor.Single.BottomPlot.Right = [0.05 0.08 0.75 0.4];

TwoWindowHor.Single.TopPlot.all = [0.09 0.57 0.82 0.4];
TwoWindowHor.Single.BottomPlot.all = [0.09 0.08 0.82 0.4];

%---------------------------------------------------------



%Setting for TwoWindowVer
TwoWindowVer.PlotNames={'LeftPlot','RightPlot'};
TwoWindowVer.SubplotIndices = [1,2];

%Common
TwoWindowVer.Common.LeftPlot.Tag = 'LeftPlot';
TwoWindowVer.Common.RightPlot.Tag = 'RightPlot';

TwoWindowVer.Common.LeftPlot.none = [0.07 0.09 0.4 0.85];
TwoWindowVer.Common.RightPlot.none = [0.56 0.09 0.4 0.85];

TwoWindowVer.Common.LeftPlot.Bottom = [0.07 0.21 0.4 0.74];
TwoWindowVer.Common.RightPlot.Bottom = [0.56 0.21 0.4 0.74];

TwoWindowVer.Common.LeftPlot.Left = [0.20 0.09 0.34 0.85];
TwoWindowVer.Common.RightPlot.Left = [0.61 0.09 0.34 0.85];

TwoWindowVer.Common.LeftPlot.Right = [0.06 0.09 0.31 0.85];
TwoWindowVer.Common.RightPlot.Right = [0.45 0.09 0.31 0.85];

TwoWindowVer.Common.LeftPlot.all = NaN;
TwoWindowVer.Common.RightPlot.all = NaN;



%single
TwoWindowVer.Single.LeftPlot.Tag = 'LeftPlot';
TwoWindowVer.Single.RightPlot.Tag = 'RightPlot';

TwoWindowVer.Single.LeftPlot.none = [0.07 0.09 0.4 0.85];
TwoWindowVer.Single.RightPlot.none = [0.56 0.09 0.4 0.85];

TwoWindowVer.Single.LeftPlot.Bottom = [0.07 0.21 0.4 0.74];
TwoWindowVer.Single.RightPlot.Bottom = [0.56 0.21 0.4 0.74];

TwoWindowVer.Single.LeftPlot.Left = [0.17 0.09 0.31 0.85];
TwoWindowVer.Single.RightPlot.Left = [0.66 0.09 0.31 0.85];

TwoWindowVer.Single.LeftPlot.Right = [0.06 0.09 0.31 0.85];
TwoWindowVer.Single.RightPlot.Right = [0.53 0.09 0.31 0.85];

TwoWindowVer.Single.LeftPlot.all = [0.07 0.09 0.4 0.85];
TwoWindowVer.Single.RightPlot.all = [0.56 0.09 0.4 0.85];

%---------------------------------------------------------



%Setting for ThreeWindowHor
ThreeWindowHor.PlotNames={'TopPlot','MiddlePlot','BottomPlot'};
ThreeWindowHor.SubplotIndices = [3,1];

%Common
ThreeWindowHor.Common.TopPlot.Tag = 'TopPlot';
ThreeWindowHor.Common.MiddlePlot.Tag = 'MiddlePlot';
ThreeWindowHor.Common.BottomPlot.Tag = 'BottomPlot';

ThreeWindowHor.Common.TopPlot.none = [0.08 0.72 0.85 0.25];
ThreeWindowHor.Common.MiddlePlot.none = [0.08 0.4 0.85 0.25];
ThreeWindowHor.Common.BottomPlot.none = [0.08 0.08 0.85 0.25];

ThreeWindowHor.Common.TopPlot.Bottom = [0.08 0.74 0.85 0.22];
ThreeWindowHor.Common.MiddlePlot.Bottom = [0.08 0.47 0.85 0.22];
ThreeWindowHor.Common.BottomPlot.Bottom = [0.08 0.20 0.85 0.22];

ThreeWindowHor.Common.TopPlot.Left = [0.20 0.72 0.75 0.25];
ThreeWindowHor.Common.MiddlePlot.Left = [0.20 0.4 0.75 0.25];
ThreeWindowHor.Common.BottomPlot.Left = [0.20 0.08 0.75 0.25];

ThreeWindowHor.Common.TopPlot.Right = [0.06 0.72 0.73 0.25];
ThreeWindowHor.Common.MiddlePlot.Right = [0.06 0.4 0.73 0.25];
ThreeWindowHor.Common.BottomPlot.Right = [0.06 0.08 0.73 0.25];

ThreeWindowHor.Common.TopPlot.all = NaN;
ThreeWindowHor.Common.MiddlePlot.all = NaN;
ThreeWindowHor.Common.BottomPlot.all = NaN;


%Single
ThreeWindowHor.Single.TopPlot.Tag = 'TopPlot';
ThreeWindowHor.Single.MiddlePlot.Tag = 'MiddlePlot';
ThreeWindowHor.Single.BottomPlot.Tag = 'BottomPlot';

ThreeWindowHor.Single.TopPlot.none = [0.08 0.72 0.85 0.25];
ThreeWindowHor.Single.MiddlePlot.none = [0.08 0.4 0.85 0.25];
ThreeWindowHor.Single.BottomPlot.none = [0.08 0.08 0.85 0.25];

ThreeWindowHor.Single.TopPlot.Bottom = [0.08 0.78 0.85 0.20];
ThreeWindowHor.Single.MiddlePlot.Bottom = [0.08 0.45 0.85 0.20];
ThreeWindowHor.Single.BottomPlot.Bottom = [0.08 0.12 0.85 0.20];

ThreeWindowHor.Single.TopPlot.Left = [0.20 0.72 0.75 0.25];
ThreeWindowHor.Single.MiddlePlot.Left = [0.20 0.4 0.75 0.25];
ThreeWindowHor.Single.BottomPlot.Left = [0.20 0.08 0.75 0.25];

ThreeWindowHor.Single.TopPlot.Right = [0.06 0.72 0.73 0.25];
ThreeWindowHor.Single.MiddlePlot.Right = [0.06 0.4 0.73 0.25];
ThreeWindowHor.Single.BottomPlot.Right = [0.06 0.08 0.73 0.25];

ThreeWindowHor.Single.TopPlot.all = [0.08 0.72 0.85 0.25];
ThreeWindowHor.Single.MiddlePlot.all = [0.08 0.4 0.85 0.25];
ThreeWindowHor.Single.BottomPlot.all = [0.08 0.08 0.85 0.25];

%---------------------------------------------------------



%Setting for ThreeWindowVer
ThreeWindowVer.PlotNames={'LeftPlot','MiddlePlot','RightPlot'};
ThreeWindowVer.SubplotIndices = [1,3];

%Common
ThreeWindowVer.Common.LeftPlot.Tag = 'LeftPlot';
ThreeWindowVer.Common.MiddlePlot.Tag = 'MiddlePlot';
ThreeWindowVer.Common.RightPlot.Tag = 'RightPlot';

ThreeWindowVer.Common.LeftPlot.none = [0.06 0.08 0.25 0.85];
ThreeWindowVer.Common.MiddlePlot.none = [0.38 0.08 0.25 0.85];
ThreeWindowVer.Common.RightPlot.none = [0.70 0.08 0.25 0.85];

ThreeWindowVer.Common.LeftPlot.Bottom = [0.06 0.19 0.25 0.77];
ThreeWindowVer.Common.MiddlePlot.Bottom = [0.38 0.19 0.25 0.77];
ThreeWindowVer.Common.RightPlot.Bottom = [0.70 0.19 0.25 0.77];

ThreeWindowVer.Common.LeftPlot.Left = [0.23 0.08 0.18 0.85];
ThreeWindowVer.Common.MiddlePlot.Left = [0.5 0.08 0.18 0.85];
ThreeWindowVer.Common.RightPlot.Left = [0.76 0.08 0.18 0.85];

ThreeWindowVer.Common.LeftPlot.Right = [0.06 0.08 0.17 0.85];
ThreeWindowVer.Common.MiddlePlot.Right = [0.32 0.08 0.17 0.85];
ThreeWindowVer.Common.RightPlot.Right = [0.58 0.08 0.17 0.85];

ThreeWindowVer.Common.LeftPlot.all = NaN;
ThreeWindowVer.Common.MiddlePlot.all = NaN;
ThreeWindowVer.Common.RightPlot.all = NaN;


%Single
ThreeWindowVer.Single.LeftPlot.Tag = 'LeftPlot';
ThreeWindowVer.Single.MiddlePlot.Tag = 'MiddlePlot';
ThreeWindowVer.Single.RightPlot.Tag = 'RightPlot';

ThreeWindowVer.Single.LeftPlot.none = [0.06 0.08 0.25 0.85];
ThreeWindowVer.Single.MiddlePlot.none = [0.38 0.08 0.25 0.85];
ThreeWindowVer.Single.RightPlot.none = [0.70 0.08 0.25 0.85];

ThreeWindowVer.Single.LeftPlot.Bottom = [0.06 0.19 0.25 0.77];
ThreeWindowVer.Single.MiddlePlot.Bottom = [0.38 0.19 0.25 0.77];
ThreeWindowVer.Single.RightPlot.Bottom = [0.70 0.19 0.25 0.77];

ThreeWindowVer.Single.LeftPlot.Left = [0.13 0.08 0.17 0.85];
ThreeWindowVer.Single.MiddlePlot.Left = [0.46 0.08 0.17 0.85];
ThreeWindowVer.Single.RightPlot.Left = [0.79 0.08 0.17 0.85];

ThreeWindowVer.Single.LeftPlot.Right = [0.05 0.08 0.16 0.85];
ThreeWindowVer.Single.MiddlePlot.Right = [0.37 0.08 0.16 0.85];
ThreeWindowVer.Single.RightPlot.Right = [0.69 0.08 0.16 0.85];

ThreeWindowVer.Single.LeftPlot.all = [0.06 0.08 0.25 0.85];
ThreeWindowVer.Single.MiddlePlot.all = [0.38 0.08 0.25 0.85];
ThreeWindowVer.Single.RightPlot.all = [0.70 0.08 0.25 0.85];

%---------------------------------------------------------



%Setting for FourWindowHor
FourWindowHor.PlotNames={'TopPlot','TopMidPlot','BotMidPlot','BottomPlot'};
FourWindowHor.SubplotIndices = [4,1];

%Common
FourWindowHor.Common.TopPlot.Tag = 'TopPlot';
FourWindowHor.Common.TopMidPlot.Tag = 'TopMidPlot';
FourWindowHor.Common.BotMidPlot.Tag = 'BotMidPlot';
FourWindowHor.Common.BottomPlot.Tag = 'BottomPlot';

FourWindowHor.Common.TopPlot.none = [0.08 0.80 0.85 0.16];
FourWindowHor.Common.TopMidPlot.none = [0.08 0.56 0.85 0.16];
FourWindowHor.Common.BotMidPlot.none = [0.08 0.32 0.85 0.16];
FourWindowHor.Common.BottomPlot.none = [0.08 0.08 0.85 0.16];

FourWindowHor.Common.TopPlot.Bottom = [0.08 0.84 0.85 0.14];
FourWindowHor.Common.TopMidPlot.Bottom = [0.08 0.63 0.85 0.14];
FourWindowHor.Common.BotMidPlot.Bottom = [0.08 0.41 0.85 0.14];
FourWindowHor.Common.BottomPlot.Bottom = [0.08 0.19 0.85 0.14];

FourWindowHor.Common.TopPlot.Left = [0.20 0.80 0.75 0.16];
FourWindowHor.Common.TopMidPlot.Left = [0.20 0.56 0.75 0.16];
FourWindowHor.Common.BotMidPlot.Left = [0.20 0.32 0.75 0.16];
FourWindowHor.Common.BottomPlot.Left = [0.20 0.08 0.75 0.16];

FourWindowHor.Common.TopPlot.Right = [0.06 0.80 0.74 0.16];
FourWindowHor.Common.TopMidPlot.Right = [0.06 0.56 0.74 0.16];
FourWindowHor.Common.BotMidPlot.Right = [0.06 0.32 0.74 0.16];
FourWindowHor.Common.BottomPlot.Right = [0.06 0.08 0.74 0.16];

FourWindowHor.Common.TopPlot.all = NaN;
FourWindowHor.Common.TopMidPlot.all = NaN;
FourWindowHor.Common.BotMidPlot.all = NaN;
FourWindowHor.Common.BottomPlot.all = NaN;
				
				
%Single
FourWindowHor.Single.TopPlot.Tag = 'TopPlot';
FourWindowHor.Single.TopMidPlot.Tag = 'TopMidPlot';
FourWindowHor.Single.BotMidPlot.Tag = 'BotMidPlot';
FourWindowHor.Single.BottomPlot.Tag = 'BottomPlot';

FourWindowHor.Single.TopPlot.none = [0.08 0.80 0.85 0.16];
FourWindowHor.Single.TopMidPlot.none = [0.08 0.56 0.85 0.16];
FourWindowHor.Single.BotMidPlot.none = [0.08 0.32 0.85 0.16];
FourWindowHor.Single.BottomPlot.none = [0.08 0.08 0.85 0.16];

FourWindowHor.Single.TopPlot.Bottom = [0.08 0.84 0.85 0.13];
FourWindowHor.Single.TopMidPlot.Bottom = [0.08 0.60 0.85 0.13];
FourWindowHor.Single.BotMidPlot.Bottom = [0.08 0.36 0.85 0.13];
FourWindowHor.Single.BottomPlot.Bottom = [0.08 0.12 0.85 0.13];

FourWindowHor.Single.TopPlot.Left = [0.20 0.80 0.75 0.16];
FourWindowHor.Single.TopMidPlot.Left = [0.20 0.56 0.75 0.16];
FourWindowHor.Single.BotMidPlot.Left = [0.20 0.32 0.75 0.16];
FourWindowHor.Single.BottomPlot.Left = [0.20 0.08 0.75 0.16];

FourWindowHor.Single.TopPlot.Right = [0.06 0.80 0.74 0.16];
FourWindowHor.Single.TopMidPlot.Right = [0.06 0.56 0.74 0.16];
FourWindowHor.Single.BotMidPlot.Right = [0.06 0.32 0.74 0.16];
FourWindowHor.Single.BottomPlot.Right = [0.06 0.08 0.74 0.16];

FourWindowHor.Single.TopPlot.all = [0.08 0.80 0.85 0.16];
FourWindowHor.Single.TopMidPlot.all = [0.08 0.56 0.85 0.16];
FourWindowHor.Single.BotMidPlot.all = [0.08 0.32 0.85 0.16];
FourWindowHor.Single.BottomPlot.all = [0.08 0.08 0.85 0.16];
%---------------------------------------------------------




%Setting for FourWindowVer
FourWindowVer.PlotNames={'LeftPlot','LeftMidPlot','RightMidPlot','RightPlot'};
FourWindowVer.SubplotIndices = [1,4];

%Common
FourWindowVer.Common.LeftPlot.Tag = 'LeftPlot';
FourWindowVer.Common.LeftMidPlot.Tag = 'LeftMidPlot';
FourWindowVer.Common.RightMidPlot.Tag = 'RightMidPlot';
FourWindowVer.Common.RightPlot.Tag = 'RightPlot';

FourWindowVer.Common.LeftPlot.none = [0.08 0.08 0.16 0.85];
FourWindowVer.Common.LeftMidPlot.none = [0.32 0.08 0.16 0.85];
FourWindowVer.Common.RightMidPlot.none = [0.56 0.08 0.16 0.85];
FourWindowVer.Common.RightPlot.none = [0.80 0.08 0.16 0.85];

FourWindowVer.Common.LeftPlot.Bottom = [0.08 0.20 0.16 0.75];
FourWindowVer.Common.LeftMidPlot.Bottom = [0.32 0.20 0.16 0.75];
FourWindowVer.Common.RightMidPlot.Bottom = [0.56 0.20 0.16 0.75];
FourWindowVer.Common.RightPlot.Bottom = [0.80 0.20 0.16 0.75];

FourWindowVer.Common.LeftPlot.Left = [0.20 0.08 0.13 0.85];
FourWindowVer.Common.LeftMidPlot.Left = [0.41 0.08 0.13 0.85];
FourWindowVer.Common.RightMidPlot.Left = [0.62 0.08 0.13 0.85];
FourWindowVer.Common.RightPlot.Left = [0.83 0.08 0.13 0.85];

FourWindowVer.Common.LeftPlot.Right = [0.07 0.08 0.13 0.85];
FourWindowVer.Common.LeftMidPlot.Right = [0.27 0.08 0.13 0.85];
FourWindowVer.Common.RightMidPlot.Right = [0.47 0.08 0.13 0.85];
FourWindowVer.Common.RightPlot.Right = [0.67 0.08 0.13 0.85];

FourWindowVer.Common.LeftPlot.all = NaN;
FourWindowVer.Common.LeftMidPlot.all = NaN;
FourWindowVer.Common.RightMidPlot.all = NaN;
FourWindowVer.Common.RightPlot.all = NaN;


%Single
FourWindowVer.Single.LeftPlot.Tag = 'LeftPlot';
FourWindowVer.Single.LeftMidPlot.Tag = 'LeftMidPlot';
FourWindowVer.Single.RightMidPlot.Tag = 'RightMidPlot';
FourWindowVer.Single.RightPlot.Tag = 'RightPlot';

FourWindowVer.Single.LeftPlot.none = [0.08 0.08 0.16 0.85];
FourWindowVer.Single.LeftMidPlot.none = [0.32 0.08 0.16 0.85];
FourWindowVer.Single.RightMidPlot.none = [0.56 0.08 0.16 0.85];
FourWindowVer.Single.RightPlot.none = [0.80 0.08 0.16 0.85];

FourWindowVer.Single.LeftPlot.Bottom = [0.08 0.20 0.16 0.75];
FourWindowVer.Single.LeftMidPlot.Bottom = [0.32 0.20 0.16 0.75];
FourWindowVer.Single.RightMidPlot.Bottom = [0.56 0.20 0.16 0.75];
FourWindowVer.Single.RightPlot.Bottom = [0.80 0.20 0.16 0.75];

FourWindowVer.Single.LeftPlot.Left = [0.11 0.08 0.12 0.85];
FourWindowVer.Single.LeftMidPlot.Left = [0.35 0.08 0.12 0.85];
FourWindowVer.Single.RightMidPlot.Left = [0.60 0.08 0.12 0.85];
FourWindowVer.Single.RightPlot.Left = [0.84 0.08 0.12 0.85];

FourWindowVer.Single.LeftPlot.Right = [0.05 0.08 0.11 0.85];
FourWindowVer.Single.LeftMidPlot.Right = [0.29 0.08 0.11 0.85];
FourWindowVer.Single.RightMidPlot.Right = [0.53 0.08 0.11 0.85];
FourWindowVer.Single.RightPlot.Right = [0.76 0.08 0.11 0.85];

FourWindowVer.Single.LeftPlot.all = [0.08 0.08 0.16 0.85];
FourWindowVer.Single.LeftMidPlot.all = [0.32 0.08 0.16 0.85];
FourWindowVer.Single.RightMidPlot.all = [0.56 0.08 0.16 0.85];
FourWindowVer.Single.RightPlot.all = [0.80 0.08 0.16 0.85];
%---------------------------------------------------------




%Setting for FourWindowTile
FourWindowTile.PlotNames={'TopLeftPlot','TopRightPlot','BottomLeftPlot','BottomRightPlot'};
FourWindowTile.SubplotIndices = [2,2];

%Common
FourWindowTile.Common.TopLeftPlot.Tag = 'TopLeftPlot';
FourWindowTile.Common.TopRightPlot.Tag = 'TopRightPlot';
FourWindowTile.Common.BottomLeftPlot.Tag = 'BottomLeftPlot';
FourWindowTile.Common.BottomRightPlot.Tag = 'BottomRightPlot';

FourWindowTile.Common.TopLeftPlot.none = [0.05 0.55 0.42 0.4];
FourWindowTile.Common.TopRightPlot.none = [0.54 0.55 0.42 0.4];
FourWindowTile.Common.BottomLeftPlot.none = [0.05 0.05 0.42 0.4];
FourWindowTile.Common.BottomRightPlot.none = [0.54 0.05 0.42 0.4];

FourWindowTile.Common.TopLeftPlot.Bottom = [0.05 0.61 0.4 0.36];
FourWindowTile.Common.TopRightPlot.Bottom = [0.55 0.61 0.4 0.36];
FourWindowTile.Common.BottomLeftPlot.Bottom = [0.05 0.17 0.4 0.36];
FourWindowTile.Common.BottomRightPlot.Bottom = [0.55 0.17 0.4 0.36];

FourWindowTile.Common.TopLeftPlot.Left = [0.19 0.55 0.35 0.4];
FourWindowTile.Common.TopRightPlot.Left = [0.60 0.55 0.35 0.4];
FourWindowTile.Common.BottomLeftPlot.Left = [0.19 0.05 0.35 0.4];
FourWindowTile.Common.BottomRightPlot.Left = [0.60 0.05 0.35 0.4];

FourWindowTile.Common.TopLeftPlot.Right = [0.05 0.55 0.35 0.4];
FourWindowTile.Common.TopRightPlot.Right = [0.45 0.55 0.35 0.4];
FourWindowTile.Common.BottomLeftPlot.Right = [0.05 0.05 0.35 0.4];
FourWindowTile.Common.BottomRightPlot.Right = [0.45 0.05 0.35 0.4];

FourWindowTile.Common.TopLeftPlot.all = NaN;
FourWindowTile.Common.TopRightPlot.all = NaN;
FourWindowTile.Common.BottomLeftPlot.all = NaN;
FourWindowTile.Common.BottomRightPlot.all = NaN;


%Single
FourWindowTile.Single.TopLeftPlot.Tag = 'TopLeftPlot';
FourWindowTile.Single.TopRightPlot.Tag = 'TopRightPlot';
FourWindowTile.Single.BottomLeftPlot.Tag = 'BottomLeftPlot';
FourWindowTile.Single.BottomRightPlot.Tag = 'BottomRightPlot';

FourWindowTile.Single.TopLeftPlot.none = [0.05 0.55 0.42 0.4];
FourWindowTile.Single.TopRightPlot.none = [0.54 0.55 0.42 0.4];
FourWindowTile.Single.BottomLeftPlot.none = [0.05 0.05 0.42 0.4];
FourWindowTile.Single.BottomRightPlot.none = [0.54 0.05 0.42 0.4];

FourWindowTile.Single.TopLeftPlot.Bottom = [0.05 0.64 0.42 0.35];
FourWindowTile.Single.TopRightPlot.Bottom = [0.54 0.64 0.42 0.35];
FourWindowTile.Single.BottomLeftPlot.Bottom = [0.05 0.12 0.42 0.35];
FourWindowTile.Single.BottomRightPlot.Bottom = [0.54 0.12 0.42 0.35];

FourWindowTile.Single.TopLeftPlot.Left = [0.15 0.55 0.32 0.4];
FourWindowTile.Single.TopRightPlot.Left = [0.65 0.55 0.32 0.4];
FourWindowTile.Single.BottomLeftPlot.Left = [0.15 0.05 0.32 0.4];
FourWindowTile.Single.BottomRightPlot.Left = [0.65 0.05 0.32 0.4];

FourWindowTile.Single.TopLeftPlot.Right = [0.04 0.55 0.32 0.4];
FourWindowTile.Single.TopRightPlot.Right = [0.53 0.55 0.32 0.4];
FourWindowTile.Single.BottomLeftPlot.Right = [0.04 0.05 0.32 0.4];
FourWindowTile.Single.BottomRightPlot.Right = [0.53 0.05 0.32 0.4];

FourWindowTile.Single.TopLeftPlot.all = [0.05 0.55 0.42 0.4];
FourWindowTile.Single.TopRightPlot.all = [0.54 0.55 0.42 0.4];
FourWindowTile.Single.BottomLeftPlot.all = [0.05 0.05 0.42 0.4];
FourWindowTile.Single.BottomRightPlot.all = [0.54 0.05 0.42 0.4];
%---------------------------------------------------------




%Setting for MainWindowLike
MainWindowLike.PlotNames={'MainWindowPlot','TopRightPlot','BottomRightPlot'};
MainWindowLike.SubplotIndices = [3,1];

%Common
MainWindowLike.Common.MainWindowPlot.Tag = 'MainWindowPlot';
MainWindowLike.Common.TopRightPlot.Tag = 'TopRightPlot';
MainWindowLike.Common.BottomRightPlot.Tag = 'BottomRightPlot';

MainWindowLike.Common.MainWindowPlot.none = [0.06 0.1 0.55 0.85];
MainWindowLike.Common.TopRightPlot.none = [0.7 0.58 0.25 0.37];
MainWindowLike.Common.BottomRightPlot.none = [0.7 0.1 0.25 0.4];

MainWindowLike.Common.MainWindowPlot.Bottom = [0.06 0.2 0.55 0.75];
MainWindowLike.Common.TopRightPlot.Bottom = [0.7 0.62 0.25 0.32];
MainWindowLike.Common.BottomRightPlot.Bottom = [0.7 0.2 0.25 0.32];

MainWindowLike.Common.MainWindowPlot.Left = [0.18 0.1 0.5 0.85];
MainWindowLike.Common.TopRightPlot.Left = [0.74 0.58 0.2 0.37];
MainWindowLike.Common.BottomRightPlot.Left = [0.74 0.1 0.2 0.4];

MainWindowLike.Common.MainWindowPlot.Right = [0.06 0.1 0.5 0.85];
MainWindowLike.Common.TopRightPlot.Right = [0.62 0.58 0.2 0.37];
MainWindowLike.Common.BottomRightPlot.Right = [0.62 0.1 0.2 0.4];

MainWindowLike.Common.MainWindowPlot.all = NaN;
MainWindowLike.Common.TopRightPlot.all = NaN;
MainWindowLike.Common.BottomRightPlot.all = NaN;



%Single
MainWindowLike.Single.MainWindowPlot.Tag = 'MainWindowPlot';
MainWindowLike.Single.TopRightPlot.Tag = 'TopRightPlot';
MainWindowLike.Single.BottomRightPlot.Tag = 'BottomRightPlot';

MainWindowLike.Single.MainWindowPlot.none = [0.06 0.1 0.55 0.85];
MainWindowLike.Single.TopRightPlot.none = [0.7 0.58 0.25 0.37];
MainWindowLike.Single.BottomRightPlot.none = [0.7 0.1 0.25 0.4];

MainWindowLike.Single.MainWindowPlot.Bottom = [0.06 0.2 0.55 0.75];
MainWindowLike.Single.TopRightPlot.Bottom = [0.7 0.64 0.25 0.31];
MainWindowLike.Single.BottomRightPlot.Bottom = [0.7 0.18 0.25 0.31];

MainWindowLike.Single.MainWindowPlot.Left = [0.14 0.1 0.48 0.85];
MainWindowLike.Single.TopRightPlot.Left = [0.76 0.58 0.2 0.37];
MainWindowLike.Single.BottomRightPlot.Left = [0.76 0.1 0.2 0.4];

MainWindowLike.Single.MainWindowPlot.Right = [0.05 0.1 0.46 0.85];
MainWindowLike.Single.TopRightPlot.Right = [0.7 0.58 0.19 0.37];
MainWindowLike.Single.BottomRightPlot.Right = [0.7 0.1 0.19 0.4];

MainWindowLike.Single.MainWindowPlot.all = [0.06 0.1 0.55 0.85];
MainWindowLike.Single.TopRightPlot.all = [0.7 0.58 0.25 0.37];
MainWindowLike.Single.BottomRightPlot.all = [0.7 0.1 0.25 0.4];



switch mainPres
	case 'OneWindow'
		ResConf = OneWindow;
	case 'TwoWindowHor'
		ResConf = TwoWindowHor;
	case 'TwoWindowVer'
		ResConf = TwoWindowVer;
 	case 'ThreeWindowVer'
 		ResConf = ThreeWindowVer;
 	case 'ThreeWindowHor'
 		ResConf = ThreeWindowHor;
 	case 'FourWindowVer'
 		ResConf = FourWindowVer;
 	case 'FourWindowHor'	
 		ResConf = FourWindowHor;
 	case 'FourWindowTile'
 		ResConf = FourWindowHor;
 	case 'MainWindowLike'
 		ResConf = MainWindowLike;
 	case 'AllPresets'
		ResConf = struct('OneWindow',OneWindow,...
						 'TwoWindowHor',TwoWindowHor,...
						 'TwoWindowVer',TwoWindowVer,...
						 'ThreeWindowVer',ThreeWindowVer,... 
						 'ThreeWindowHor',ThreeWindowHor,...
						 'FourWindowVer',FourWindowVer,...
						 'FourWindowHor',FourWindowHor,...
						 'FourWindowTile',FourWindowTile,...
						 'MainWindowLike',MainWindowLike);


end


end
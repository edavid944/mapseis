function ParamObj = CalcParamGUILoader(CalcObj,FigureName,WindowPos,ListProxy,RegParam)
	%This function load the suitable version of the CalcParamGUI
	
	import mapseis.gui.*;
	
	%get needed Version
	Versio = CalcObj.NeededParamGUI;
	
	%at the moment only one version available
	switch Versio
		case '1.0'
			ParamObj=CalcParamGUI(CalcObj,FigureName,WindowPos,ListProxy,RegParam)
		
	end	
	

end

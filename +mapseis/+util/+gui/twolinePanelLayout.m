function guiHnd = twolinePanelLayout(figName,figProperties,axesProperties)
% Layout for the rate compare and similar problems with three plots 


% $Revision: 1 $    $Date: 2008-15-12 $
% Author: David Eberhard

% Create an array to store the subplot axes handles
axHnds = zeros(2,1);

guiHnd = figure('name',figName,'visible','off',figProperties{:});
axHnds(1) = subplot(1,2,1);
set(axHnds(1),'Tag','axis_left');
axHnds(2) = subplot(1,2,2);
set(axHnds(2),'Tag','axis_right');


% If there is more than one input argument then assume varargin is a cell
% array of cell arrays of properties for the above axes, in the order given
for paramIndex = 1:numel(axesProperties)
    set(axHnds(paramIndex),axesProperties{paramIndex}{:});
end
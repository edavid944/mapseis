function bgHandle = labelWidgetList(parentHandle,paramStruct)
% labelWidgetList : create a vertical labelled widget layout
% paramStruct has fields :
%   String, Tag, Value which are mx1 arrays (or cell arrays)

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

% Create a button group
bgHandle = uibuttongroup('Parent',parentHandle,...
    'Units','Normalized');

paramStrings = paramStruct.String;
paramTags = paramStruct.Tag;
paramValues = paramStruct.Value;

% Number of widgets
widgetCount = numel(paramValues);
% Height of each widget in normalised units
widgetHeight = 1/widgetCount;

for i=1:widgetCount
    uicontrol('Parent',bgHandle,...
        'style','Text',...
        'String',paramStrings{i},...
        'Units','Normalized',...
        'Position',[0 1-(i+0.125)*widgetHeight 0.5 0.75*widgetHeight]);
    eb=uicontrol('Parent',bgHandle,...
        'style','edit',... % Default style is editbox
        'Tag',paramTags{i},...
        'Units','Normalized',...
        'Position',[0.5 1-i*widgetHeight 0.5 widgetHeight]);
    % Extract the current parameter value
    thisValue = paramValues{i};
    if isnumeric(thisValue) 
        % If the value is numeric then convert to string to populate the
        % edit box. 
        set(eb,'Value',thisValue);
        set(eb,'String',num2str(thisValue));
    elseif islogical(thisValue) 
        % If the value is logical then change the style of the 'edit box'
        % to 'checkbox'
        set(eb,'Style','Checkbox');
        set(eb,'Value',thisValue);
    elseif iscell(thisValue)
        % If the value is a cell array then it is either a list of strings
        % for a listbox or for a popup menu depending on the first element
        % of the list
        typeSelect = thisValue{1};
        listValues = thisValue(2:end);
        if iscell(typeSelect) || islogical(typeSelect)
            % If the first element of the cell array is a cell then this
            % is a listbox.  The first element is true if the listbox is
            % multiselect.  The other elements of the typeSelect cell are
            % the selected entries
            
            % Set the uicontrol style to listbox
            set(eb,'Style','listbox');
            % Set the strings to listValues
            set(eb,'String',listValues);
            % Set the selected values depending on the rest of the values
            % in typeSelect, or leave as default if no further elements
            if numel(typeSelect)>1
                % Set the max property to >1 for multiselect
                set(eb,'Max',1+typeSelect{1});
                set(eb,'Value', ismember(listValues,typeSelect(2:end)));
            else
                set(eb,'Max',1+typeSelect);
            end
        else
            % If there is only a single string in the first element of the
            % cell array then this is the selected element of a popupmenu
            % Set the uicontrol style to listbox
            set(eb,'Style','popupmenu');
            % Set the strings to listValues
            set(eb,'String',listValues);
            % Set the selected values depending on the rest of the values
            % in typeSelect, or leave as default if no further elements
            set(eb,'Value', find(ismember(listValues,typeSelect),1));
        end
    else
        % Assume the value is a string
        % If the value is a string then just use an edit box
        set(eb,'String',thisValue);
    end
        
end

end
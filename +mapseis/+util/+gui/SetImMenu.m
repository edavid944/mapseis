function SetImMenu(MainGUI,imobj,RegionFilter,addhandle)
	%the function takes the existing contextmenu and adds the new menupoints 
	%to the contextmenu
	%
	import mapseis.gui.*;
	
	if nargin<4
		addhandle=[];
	end
	
	regiontype=RegionFilter.RangeSpec;
	
	switch regiontype
		case {'in','out'}
			%find the parts and separate them
			childrens=get(imobj,'children');
			thepoints=findall(childrens,'type','hggroup');
			thelines=findall(childrens,'type','line');
			thepatch=findall(childrens,'type','patch');
			
			%get menu
			oldmenu=get(thepoints(1),'uicontextmenu');
			

			
			%create new entries
			Ppoint1=uimenu(oldmenu, 'Label', 'Export Points',...
					'Callback', @(s,e) PointExport(MainGUI,RegionFilter,'polygon'));

			Ppoint2=uimenu(oldmenu, 'Label', 'Import Points',...
					'Callback', @(s,e) PointImport(MainGUI,imobj,RegionFilter,'polygon'));		
			
			
			%add new menu to points and lines
			for i=1:numel(thepoints)
				set(thepoints(i),'uicontextmenu',oldmenu);
			end
			
			for i=1:numel(thelines)
				set(thelines(i),'uicontextmenu',oldmenu);
			end
			
			for i=1:numel(thepatch)
				set(thepatch(i),'uicontextmenu',oldmenu);
			end
			
			
		case 'line'
			%find the parts and separate them
			childrens=get(imobj,'children');
			thepoints=findall(childrens,'type','line');
			thelines=addhandle;
			
			%get menu
			oldmenu=get(thepoints(1),'uicontextmenu');
			
			%additional menu for the box
			boxmenu=uicontextmenu;
			
			%create new menus
			%oldmenu
			Ppoint1=uimenu(oldmenu,'Label','Set Profile Width',...
					'Callback',@(s,e) MainGUI.setProfWidth);
			
			Ppoint2=uimenu(oldmenu,'Label','Set Profile Parameter',...
					'Callback',@(s,e) SetProfileParameter(MainGUI,imobj,RegionFilter));
					
			Ppoint3=uimenu(oldmenu, 'Label', 'Export Profile',...
					'Callback', @(s,e) PointExport(MainGUI,RegionFilter,'line'));

			Ppoint4=uimenu(oldmenu, 'Label', 'Import Profile',...
					'Callback', @(s,e) PointImport(MainGUI,imobj,RegionFilter,'line'));		
			
						
			%box menu		
			Lpoint1=uimenu(boxmenu,'Label','Set Profile Width',...
					'Callback',@(s,e) MainGUI.setProfWidth);
			
			Lpoint2=uimenu(boxmenu,'Label','Set Profile Parameter',...
					'Callback',@(s,e) SetProfileParameter(MainGUI,imobj,RegionFilter));
					
			Lpoint3=uimenu(boxmenu, 'Label', 'Export Profile',...
					'Callback', @(s,e) PointExport(MainGUI,RegionFilter,'line'));

			Lpoint4=uimenu(boxmenu, 'Label', 'Import Profile',...
					'Callback', @(s,e) PointImport(MainGUI,imobj,RegionFilter,'line'));	
			
					
			%add new menu to points and lines
			for i=1:numel(thepoints)
				set(thepoints(i),'uicontextmenu',oldmenu);
			end
			
			for i=1:numel(thelines)
				set(thelines(i),'uicontextmenu',boxmenu);
			end
			

		
		case 'circle'
			%find the parts and separate them
			childrens=get(imobj,'children');
			thepoints=findall(childrens,'type','line');
			thelines=addhandle;
			disp('set the menu')
			disp(childrens)
			%get menu
			oldmenu=get(imobj,'uicontextmenu');
			
			%additional menu for the box
			circlemenu=uicontextmenu;
			
			%create new menus
			%oldmenu
			Ppoint1=uimenu(oldmenu,'Label','Set Radius',...
					'Callback',@(s,e) MainGUI.setCircleRadius);
			
			Ppoint2=uimenu(oldmenu,'Label','Set Circle Parameter',...
					'Callback',@(s,e) SetCircleParameter(MainGUI,imobj,RegionFilter));
					
			Ppoint3=uimenu(oldmenu, 'Label', 'Export Circle',...
					'Callback', @(s,e) PointExport(MainGUI,RegionFilter,'circle'));

			Ppoint4=uimenu(oldmenu, 'Label', 'Import Circle',...
					'Callback', @(s,e) PointImport(MainGUI,imobj,RegionFilter,'circle'));		
			
						
			%Circle Menu
			Lpoint1=uimenu(circlemenu,'Label','Set Radius',...
					'Callback',@(s,e) MainGUI.setCircleRadius);
			
			Lpoint2=uimenu(circlemenu,'Label','Set Circle Parameter',...
					'Callback',@(s,e) SetCircleParameter(MainGUI,imobj,RegionFilter));
					
			Lpoint3=uimenu(circlemenu, 'Label', 'Export Circle',...
					'Callback', @(s,e) PointExport(MainGUI,RegionFilter,'circle'));

			Lpoint4=uimenu(circlemenu, 'Label', 'Import Circle',...
					'Callback', @(s,e) PointImport(MainGUI,imobj,RegionFilter,'circle'));
					
			%add new menu to points and lines
			for i=1:numel(thepoints)
				set(thepoints(i),'uicontextmenu',oldmenu);
			end
			
			%the point itself
			set(imobj,'uicontextmenu',oldmenu);
			
			for i=1:numel(thelines)
				set(thelines(i),'uicontextmenu',circlemenu);
			end
			
			
		
	end
	
	
end

function mainParamWindow= startParamInitApp(appPars)
% Create a GUI to set application parameters by automatically creating GUI
% widgets from a structure

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2007-12-04 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Matt McDonnell$

% Use the fieldnames in appPars to create the distinct panels
panelNameList = fieldnames(appPars);
% fileParsFields = fieldnames(appPars.filePars);
% phaseMeasureParsFields = fieldnames(appPars.phaseMeasurePars);
% globalParsFields = fieldnames(appPars.globalPars);

% Number of subpanels for main GUI
panelCount = numel(panelNameList);

%% Create  windows for editing parameters
% Create the main GUI window and set the units to Normalized for easier
% positioning of the child components
mainParamWindow = figure('Units','Normalized');

% Set the portion of the GUI taken up by the parameter uipanel 
paramPanelLimits = [0 0 1 0.9];

% Create the panels and return their handles
panelHandles = nCreatePanels(mainParamWindow,panelNameList,paramPanelLimits);

% dataLocParent = uipanel(mainParamWindow,...
%     'Title','Data Location','Position',paramPanelLimits);
% phaseParamParent  =  uipanel(mainParamWindow,...
%     'Title','Phase Measurement','Position',paramPanelLimits);
% globalParamParent  =  uipanel(mainParamWindow,...
%     'Title','Global Parameters','Position',paramPanelLimits);

% Create the widget lists in their appropriate panels
nCreateWidgetLists(panelHandles);

% createLabelEditList(dataLocParent,'dataLoc',appPars.filePars);
% createLabelEditList(phaseParamParent,'phase',appPars.phaseMeasurePars);
% createLabelEditList(globalParamParent,'global',appPars.globalPars);

%% Create buttons to select the different parameters
buttonPanel=uibuttongroup('Parent',mainParamWindow,...
    'Position',[0 0.9 0.75 0.1],'Title','Selected Parameter');

% Create the panels and return their handles
radioButtonHandles = nCreateRadioButtons(buttonPanel,panelNameList);

% dataLocRadio =uicontrol(buttonPanel,...
%     'Style','radiobutton',...
%     'String','Data Location',...
%     'Units','Normalized',...
%     'Position',[0 0 0.33 1]);
% phaseRadio =uicontrol(buttonPanel,...
%     'Style','radiobutton',...
%     'String','Phase Measure',...
%     'Units','Normalized',...
%     'Position',[0.33 0 0.33 1]);
% globalRadio =uicontrol(buttonPanel,...
%     'Style','radiobutton',...
%     'String','Global Parameters',...
%     'Units','Normalized',...
%     'Position',[0.67 0 0.33 1]);

% Set the callbacks of the radio buttons to make the appropriate panel
% visible and the others invisible
nSetRadioButtonCallbacks(radioButtonHandles,panelNameList);
% set(dataLocRadio,'Callback',@(s,e) nSetPanel('dataLoc'));
% set(phaseRadio,'Callback',@(s,e) nSetPanel('phase'));
% set(globalRadio,'Callback',@(s,e) nSetPanel('global'));

%% Create a 'Run' button

runButton = uicontrol(mainParamWindow,...
    'Style','pushbutton',...
    'Units','Normalized',...
    'Position',[0.75 0.9 0.25 0.1],...
    'String','Init');

set(runButton,'Callback',@(s,e) nRunSimulation);
% Make the first panel visible
nSetPanel(panelNameList{1});

    function nRunSimulation
        % Read the parameters from the figure
        filePars=readGUIButtonState(mainParamWindow,'dataLoc',fileParsFields);
        phaseMeasurePars=readGUIButtonState(mainParamWindow,'phase',phaseMeasureParsFields);
        globalPars=readGUIButtonState(mainParamWindow,'global',globalParsFields);
        nSetGlobals(globalPars);
        newAppPars.filePars = filePars;
        newAppPars.phaseMeasurePars = phaseMeasurePars;
        startGUI(filePars.testGUI,newAppPars);
        closereq;
    end

    function nSetGlobals(globalPars)
        % Set the global parameters using eval.  This is not ideal.
        gFields = fieldnames(globalPars);
        for i=1:numel(gFields)
            thisField = gFields{i};
            evalStr = sprintf('global %s;\n%s = %s;',...
                thisField, thisField, nnToString(globalPars.(thisField)));
            eval(evalStr);
        end
        
        function outStr=nnToString(inArg)
            % Convert a value to a string for evaluation using eval
            if isnumeric(inArg) || islogical(inArg)
                % If numeric then use num2str
                outStr = num2str(inArg);
            else
                % else it is a string and we must escape the quotes
                outStr = sprintf('''%s''',inArg);
            end
        end
    end

    function nCreateWidgetLists(panelHandles)
       % Create vertical list of widgets to set parameter values
       % Determine the number of panels
         panelCount = numel(panelNameList);
       % Create the widget lists
         for panelIndex=1:panelCount
            createLabelEditList(panelHandles(panelIndex),...
                panelNameList{panelIndex},....
                appPars.(panelNameList{panelIndex}));
         end
        % 
    end

    function panelHandles = nCreatePanels(parentHandle,panelNameList,panelLimits)
         % Determine the number of panels
         panelCount = numel(panelNameList);
         % Preallocate array for panel handles
         panelHandles = zeros(panelCount,1);
         % Create the panels
         for panelIndex=1:panelCount
             panelHandles(panelIndex) = uipanel(parentHandle,...
                'Title',panelNameList{panelIndex},'Position',panelLimits);
         end
    end

    function radioButtonHandles = nCreateRadioButtons(buttonPanel,panelNameList)
         % Determine the number of buttons
         buttonCount = numel(panelNameList);
         % Width each button occupies is 1/(number of buttons)
         buttonWidth = 1/buttonCount;
         % Preallocate array for widget handles
         radioButtonHandles = zeros(panelCount,1);
         % Create the panels
         for buttonIndex=1:buttonCount
             radioButtonHandles(buttonIndex) = uicontrol(buttonPanel,...
                'Style','radiobutton',...
                'String',panelNameList{buttonIndex},...
                'Units','Normalized',...
                'Position',[(buttonIndex-1)*buttonWidth 0 buttonWidth 1]);
         end
    end

    function nSetRadioButtonCallbacks(radioButtonHandles,panelNameList)
        % Determine the number of panels
         panelCount = numel(panelNameList);
         for panelIndex=1:panelCount
             set(radioButtonHandles(panelIndex),'Callback',...
                @(s,e) nSetPanel(panelNameList{panelIndex}));
         end
    end

    function nSetPanel(panelName)
        % Sets the named panel to be visible, all others off
        
        % Determine the number of panels
         panelCount = numel(panelNameList);
         for panelIndex=1:panelCount
             thisPanelHandle = panelHandles(panelIndex);
            set(thisPanelHandle,'Visible',...
                nnVisState(panelNameList{panelIndex},panelName));
         end

        function visState=nnVisState(panelName,selectedName)
            if strcmp(panelName,selectedName)
                visState='on';
            else
                visState='off';
            end
        end
    end

end
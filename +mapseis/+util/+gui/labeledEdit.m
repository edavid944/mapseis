function funStruct = labeledEdit(parentHandle,label)


%% Attributes
hLabel = uicontrol('Parent',parentHandle,'Style','Text','String',[label,' : ']);

%% Interface
funStruct = struct('getValue',@nGetValue);
funStruct.widgetHandles = hLabel;

%% Public Methods

%% Private Methods

end
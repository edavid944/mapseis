function guiHnd = onePanelLayout(figName,varargin)
% onePanelLayout : creates a figure with a single plot axis labeled 'axis'

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-10-13 12:17:41 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell



guiHnd = figure('name',figName,'visible','off');


%guiHnd = figure('name',figName,'visible','off');
ax1 = subplot(1,1,1);
set(ax1,'Tag','axis',varargin{:});

    

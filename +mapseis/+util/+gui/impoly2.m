function h_group = impoly2(varargin)
%IMPOLY Create draggable, resizable polygon.
%   H = IMPOLY(HPARENT,POSITION) creates a draggable, resizable polygon on
%   the object specified by HPARENT. The function returns H, a handle to the
%   polygon, which is an hggroup object. HPARENT specifies the hggroup's
%   parent, which is typically an axes object, but can also be any other
%   object that can be the parent of an hggroup. POSITION is an N-by-2 array
%   that specifies the initial position of the vertices of the
%   polygon. POSITION has the form [X1,Y1;...;XN,YN]. You can use an empty
%   matrix ([]) for POSITION to place a polygon interactively.
%
%   The polygon has a context menu associated with it that allows you to
%   copy the current position to the clipboard and change the color used to
%   display the polygon.
%
%   H = IMPOLY(...,PARAM1,VAL1,PARAM2,VAL2,...) creates a draggable, resizable 
%   polygon, specifying parameters and corresponding values that control the 
%   behavior of the polygon. Parameter names can be abbreviated, and case does 
%   not matter.
%    
%   Parameters include:
%    
%   'Closed'                       A scalar logical that controls whether the 
%                                  polygon is closed. True creates a closed polygon. 
%                                  False creates an open polygon.  
%       
%   'PositionConstraintFcn'        Function handle fcn that is called whenever the 
%                                  mouse is dragged using the syntax: 
%                                  
%                                  constrained_position = fcn(new_position),
%    
%                                  where new_position is a four element position
%                                  vector. This allows a client, for example, 
%                                  to control where the polygon may be dragged.    
%
%   API Function Syntaxes
%   ---------------------
%   Each instance of impoly contains a structure of function handles, called
%   an API, that can be used to manipulate it.  To retrieve this structure
%   from the polygon, use the IPTGETAPI function.
%
%       API = IPTGETAPI(H)
%
%   Functions in the API, listed in the order of the structure fields, include:
%
%   setPosition
%
%       Sets the polygon to a new position.
%
%           api.setPosition(new_position)
%    
%       where new_position is an N-by-2 array [X1 Y1;...;XN YN] that
%       specifies the vertex positions of the polygon.
%
%   getPosition
%
%       Returns the vertex positions of the polygon.
%
%           pos = api.getPosition()
%    
%       where pos is an N-by-2 array [X1 Y1;...;XN YN].            
%
%   delete
%
%       Deletes the polygon associated with the API.
%
%           api.delete()
%
%   setColor
%
%       Sets the color used to draw the polygon.
%
%           api.setColor(new_color)
%    
%       where new_color can be a three-element vector specifying an RGB triplet,
%       or a text string specifying the long or short names of a predefined
%       color, such as 'white' or 'w'.  See the PLOT command for a list of
%       predefined colors.
%
%   addNewPositionCallback
%
%       Adds the function handle FCN to the list of new-position callback
%       functions.
%
%           id = api.addNewPositionCallback(fcn)
%
%       Whenever the polygon changes its position, each function in the
%       list is called with the syntax:
%
%           fcn(position)
%
%       The return value, id, is used only with
%       removeNewPositionCallback.
%
%   removeNewPositionCallback
%
%       Removes the corresponding function from the new-position callback
%       list.
%
%           api.removeNewPositionCallback(id)
%
%       where id is the identifier returned by
%       api.addNewPositionCallback.
%
%   setPositionConstraintFcn
%
%       Sets the position constraint function to be the specified function
%       handle, fcn.
%
%           api.setPositionConstraintFcn(fcn)
%
%       Whenever the polygon is moved because of a mouse drag, the
%       constraint function is called using the syntax:
%
%           constrained_position = fcn(new_position)
%
%       where constrained_position is a N-by-2 array [X1 Y1;...;XN YN]. 
%
%       This allows a client, for example, to control where the polygon may be
%       dragged.
%
%   getPositionConstraintFcn
%
%       Returns a function handle to the current position constraint function.
%
%           fcn = api.getPositionConstraintFcn()
%    
%   setConstrainedPosition
%
%       Sets the polygon to a new position.  The candidate position is subject to the position 
%       constraint function.
%
%           api.setConstrainedPosition(candidate_position)
%
%       where candidate_position is a four-element position vector.
%        
%   setVerticesDraggable(TF)
%
%       Sets the interactive behavior of the vertices of the polygon. True
%       means that the vertices of the polygon are draggable. False means
%       that the vertices of the polygon are not draggable.
%
%           api.setVerticesDraggable(TF)
%      
%       where TF is a logical scalar.
%
%   setClosed(TF)
% 
%      Sets the geometry of the polygon. True means that the polygon is
%      closed. False means that the polygon is an open polyline.
%
%           api.setClosed(TF)
%
%      where TF is a logical scalar.
%   
%   Remarks
%   -------    
%   If you use IMPOLY with an axis that contains an image object, and do not
%   specify a position constraint function, users can drag the polygon outside the
%   extent of the image and lose the polygon.  When used with an axis created
%   by the PLOT function, the axis limits automatically expand to accommodate
%   the movement of the polygon.
%    
%   Example 1
%   ---------    
%   Display updated position in the title. Specify a position constraint function
%   using makeConstainToRectFcn to keep the polygon inside the original xlim
%   and ylim ranges.
% 
%   figure, imshow('gantrycrane.png');
%   h = impoly(gca, [188,30; 189,142; 93,141; 13,41; 14,29]);
%   api = iptgetapi(h);
%   api.setColor('yellow');    
%   api.addNewPositionCallback(@(p) title(mat2str(p,3)));
%   fcn = makeConstrainToRectFcn('impoly',get(gca,'XLim'),get(gca,'YLim'));
%   api.setPositionConstraintFcn(fcn);
%    
%   Example 2
%   ---------
%   Create an open polygon with a custom color.
%    
%   figure, imshow('gantrycrane.png')
%   h = impoly(gca,[203,30; 202,142; 294,142],'Closed',false);
%   api = iptgetapi(h);
%   api.setColor([1 0 0]);
%
%   Example 3
%   ---------
%   Interactively place a polygon over an image by clicking to place
%   vertices. Complete interactive placement by right clicking or double
%   clicking.
% 
%   figure, imshow('gantrycrane.png');
%   h = impoly(gca,[]);
%
%   See also  IMFREEHAND, IMLINE, IMRECT, IMPOINT, IMELLIPSE, IPTGETAPI, makeConstrainToRectFcn.

%   Copyright 2007 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $ $Date: 2007/06/04 21:11:03 $

  
  [commonArgs,specificArgs] = roiParseInputs(2,6,varargin,mfilename,{'Closed'});
  
  xy_position_vectors_specified = (nargin > 2) && ...
                                  isnumeric(varargin{2}) && ...
                                  isnumeric(varargin{3});
  
  if xy_position_vectors_specified
      error(sprintf('Images:%s:invalidPosition',mfilename),...
            'Position must be specified in the form [X1,Y1;...;XN,YN].');
  end
  
  position              = commonArgs.Position;
  interactive_placement = commonArgs.InteractivePlacement;
  h_parent              = commonArgs.Parent;
  h_axes                = commonArgs.Axes;
  h_fig                 = commonArgs.Fig;
  
  positionConstraintFcn = commonArgs.PositionConstraintFcn;
  if isempty(positionConstraintFcn)
      % constraint_function is used by dragMotion() to give a client the
      % opportunity to constrain where the point can be dragged.
      positionConstraintFcn = identityFcn;
  end

  is_closed = specificArgs.Closed;
     
  try
    h_group = hggroup('Parent', h_parent,'Tag','impoly');
  catch
    error(sprintf('Images:%s:noAxesAncestor',mfilename), ...
          'HPARENT must be able to have an hggroup object as a child.');
  end
   
  draw_api = polygonSymbol();
  
  basicPolygonAPI = basicPolygon(h_group,draw_api,positionConstraintFcn);
  
  % Handles to each of the polygon vertices
  h_vertices = [];
  
  % Handle to currently active vertex
  h_active_vertex = [];
  
  % Alias functions defined in basicPolygonAPI to shorten calling syntax in
  % impoly.
  setPosition               = basicPolygonAPI.setPosition;
  setConstrainedPosition    = basicPolygonAPI.setConstrainedPosition;
  getPosition               = basicPolygonAPI.getPosition;
  setClosed                 = basicPolygonAPI.setClosed;
  setVisible                = basicPolygonAPI.setVisible;
  setPolygonPointerBehavior = basicPolygonAPI.setPolygonPointerBehavior;
  updateView                = basicPolygonAPI.updateView;
  addNewPositionCallback    = basicPolygonAPI.addNewPositionCallback;
  deletePolygon             = basicPolygonAPI.delete;
     
  if interactive_placement
  	  setClosed(false);
      setVisible(true);
  	  animate_id = iptaddcallback(h_fig,'WindowButtonMotionFcn',@animateLine);
      placement_aborted = manageInteractivePlacement(h_axes,h_group,@placePolygon,@buttonUpPlacement);
  	  iptremovecallback(h_fig,'WindowButtonMotionFcn',animate_id);
      if placement_aborted
          h_group = [];
          return;
      end
  else
      % Create vertices in initial locations specified by user.    
      setPosition(position);
      createVertices();
  end
  
  setClosed(is_closed);
  setVisible(true);
  setPolygonPointerBehavior();
  
  % Create context menu for polygon body and vertices once initial placement
  % of polygon is complete. Create context menus after possible
  % buttonUp event during interactive placement to avoid posting context
  % menus during right click interactive placement gestures.
  
  % setColor called within createROIContextMenu requires that cmenu_poly is
  % an initialized variable.
  cmenu_poly =[];
  cmenu_vertices = [];
  
  cmenu_poly     = createROIContextMenu(h_fig,getPosition,@setColor);
  setContextMenu(cmenu_poly);
 
  cmenu_vertices = createVertexContextMenu();
  setVertexContextMenu(cmenu_vertices);
    
  set(h_group,'DeleteFcn',@deleteContextMenu)
  
  % Wire new position callback to update vertex position whenever position
  % matrix of polygon changes
  addNewPositionCallback(@updateVertexPositions);
      
  % Define API
  api.setPosition               = setPosition;
  api.setConstrainedPosition    = setConstrainedPosition;
  api.getPosition               = getPosition;
  api.setClosed                 = setClosed;
  api.addNewPositionCallback    = addNewPositionCallback;
  api.delete                    = deletePolygon;
  api.setVerticesDraggable      = draw_api.showVertices;
  api.removeNewPositionCallback = basicPolygonAPI.removeNewPositionCallback;
  api.getPositionConstraintFcn  = basicPolygonAPI.getPositionConstraintFcn;
  api.setPositionConstraintFcn  = basicPolygonAPI.setPositionConstraintFcn;
  api.setColor                  = @setColor;
  
  % Undocumented API methods.
  api.setContextMenu       = @setContextMenu;
  api.getContextMenu       = @getContextMenu;
  api.setVertexContextMenu = @setVertexContextMenu;
  api.getVertexContextMenu = @getVertexContextMenu;
  
  iptsetapi(h_group,api);
  
  updateView(getPosition());
  
  % Create update function that knows how to get the position it needs when it
  % will be called from HG contexts where it may not have access to the position
  % otherwise.
  update_fcn = @(varargin) updateView(getPosition());
  
  updateAncestorListeners(h_group,update_fcn);

    %-----------------------
    function setColor(color)
        if ishandle(getContextMenu())
            updateColorContextMenu(getVertexContextMenu(),color);
            updateColorContextMenu(getContextMenu(),color);
        end
        draw_api.setColor(color);
    end

    %----------------------------- 
    function setContextMenu(cmenu_new)
      
       cmenu_obj = findobj(h_group,'Type','line','-or','Type','patch');  
       set(cmenu_obj,'uicontextmenu',cmenu_new);
       
       cmenu_poly = cmenu_new;
        
    end
    
    %-------------------------------------
    function context_menu = getContextMenu
       
        context_menu = cmenu_poly;
    
    end
  
    %-----------------------------------
    function setVertexContextMenu(cmenu_new)
       
        for i = 1:getNumVert()
           set(h_vertices(i),'UIContextMenu',cmenu_new); 
        end
        
        cmenu_vertices = cmenu_new;
          
    end

    %-------------------------------------------
    function vertex_cmenu = getVertexContextMenu
    
        % All of the vertices in the polygon shares the same UIContextMenu
        % object. Obtain the shared uicontextmenu from the first vertex in
        % the polygon.
        vertex_cmenu = get(h_vertices(1),'UIContextMenu');
        
    end
        
    %----------------------------------- 
    function deleteContextMenu(varargin)
        if ishandle(cmenu_poly)
            delete([cmenu_poly,cmenu_vertices]);
        end
    end
        
    %-----------------------------
	function animateLine(varargin)
        		
		[x_init,y_init] = getCurrentPoint(h_axes);
        animate_pos = [getPosition(); x_init, y_init];
		updateView(animate_pos)
		
	end %animateLine
 
    %-----------------------------------------------
	function completed = placePolygon(x,y)
      	
	    is_double_click = strcmp(get(h_fig,'SelectionType'),'open');
	    is_right_click  = strcmp(get(h_fig,'SelectionType'),'alt');
	    
	    h_hit_test = hittest(h_fig);
        
        clicked_on_first_vertex = ~isempty(h_vertices) && h_hit_test == h_vertices(1);
	    
        completed = is_double_click || (is_closed && clicked_on_first_vertex);
	    
        % Distinction between right click and other completion gestures is
        % that right click placement ends on buttonUp.
        if completed || is_right_click
            setVertexPointerBehavior();
        else
            addVertex([x,y],getNumVert()+1);

            % Provide circle affordance to first vertex to communicate that polygon can
            % be closed by clicking on first vertex.
            if ( is_closed && (getNumVert() == 1) )
                setVertexPointerBehavior();
            end

        end
         
    end %placePolygon
    
    %-------------------------------------
    function completed = buttonUpPlacement
       
        completed  = strcmp(get(h_fig,'SelectionType'),'alt');
        
    end
  
    %--------------------------------
	function setVertexPointerBehavior

		for i = 1:getNumVert()
			iptSetPointerBehavior(h_vertices(i),...
				@(h_fig,loc) set(h_fig,'Pointer','circle'));
		end

	end %setVertexPointerBehavior

    %-----------------------------
    function num_vert = getNumVert
       
        num_vert = size(getPosition(),1);
        
    end
    
    %---------------------------------------
    function active_vertex = getActiveVertex
       
        active_vertex = h_active_vertex;
        
    end
    
    %----------------------------------
    function updateVertexPositions(pos)
        
        num_vert_drawn = length(h_vertices);
        if num_vert_drawn ~= getNumVert()
        
            for i = 1:num_vert_drawn
                vertex_api = iptgetapi(h_vertices(i));
                vertex_api.delete();
            end
            createVertices();
        else
                    
            for i = 1:getNumVert()
                vertex_api = iptgetapi(h_vertices(i));
                vertex_api.setPosition(pos(i,:));
            end
        end
               
    end %setPosition
                
    %--------------------------
    function vertexDragged(pos)
        
        loc = h_vertices == getActiveVertex();
        
        candidate_position = getPosition();
        candidate_position(loc,:) = pos;
        
        setConstrainedPosition(candidate_position);
                  
    end %vertexDragged
    
    %-------------------------------------------
    function vertexButtonDown(h_vertex,varargin) 
    
        h_active_vertex = h_vertex;
        
    end %vertexButtonDown
	      
    %---------------------------
    function createVertices
    
        % If the number of vertices being drawn is being adjusted via a
        % call to setPosition, re-initialize h_vertices
        h_vertices = [];
        
        pos = getPosition();
        for i = 1:getNumVert()
            h_vertices(i) =  createVertex(pos(i,1),pos(i,2));
        end
        setVertexPointerBehavior();
       	  
    end %createVertices
    
    %--------------------------
    function addVertex(pos,idx)
    % addVertex adds the vertex position pos to index idx of the resized (N+1)
    % by 2 position matrix.
        
		position = getPosition();
        num_vert = getNumVert() + 1;
              
        position_new = zeros(num_vert,2);
        h_vertices_new = zeros(1,num_vert);
        
        position_new(idx,:) = pos;
        h_vertices_new(idx) = createVertex(pos(1),pos(2));
        
        if num_vert > 1
      	  
      	  left_ind = 1:idx-1;
      	  right_ind = idx+1:num_vert;
      	  
      	  position_new(left_ind,:) = position(left_ind,:);
            h_vertices_new(left_ind) = h_vertices(left_ind);
      	  
      	  position_new(right_ind,:) = position(right_ind-1,:);
      	  h_vertices_new(right_ind) = h_vertices(right_ind-1);
      	  
        end	 
        
        h_vertices = h_vertices_new;
        
		setPosition(position_new);
       
    end %addVertex
      
    %----------------------------------------------
    function vertex_cmenu = createVertexContextMenu
    % createVertexContextMenu creates a single context menu at the figure level
    % that is shared by all of the impoint instances used to define
    % vertices.  
        
        vertex_cmenu = createROIContextMenu(h_fig,getPosition,@setColor);
        uimenu(vertex_cmenu,...
            'Label','Delete Vertex',...
            'Tag','delete vertex cmenu item',...
            'Callback',@deleteVertex);
        
        %------------------------------
    	function deleteVertex(varargin)
    		
            %Each vertex has a buttonDown callback wired to it which caches the last
            %vertex that was clicked on. The last vertex that received
            %buttonDown is the vertex which posted the delete context menu
            %option.
            h_vertex = getActiveVertex();
            
            vertex_api = iptgetapi(h_vertex);
            
    		idx = h_vertex == h_vertices;
    	
			position = getPosition();
    	
    		h_vertices = h_vertices(~idx);
    		position_new = position(~idx,:);
    		
    		vertex_api.delete();
    		
    		if ~isempty(position_new)
    			setPosition(position_new);
    		else
    			% If the last vertex has been deleted, the entire polygon
    			% should be destroyed.
    			deletePolygon();
    		end
    		
    	end %deleteVertex
        
    end %createVertexContextMenu

    %--------------------------------------------------
    function h_vertex = createVertex(init_x,init_y)
    % Make custom impoint ROIs wired for use as polygon vertices
    
    	h_vertex = impoint(h_group,init_x,init_y);
    	vertex_api = iptgetapi(h_vertex);
    	vertex_api.addCallback(@vertexDragged,'translateDrag');
        
        % Remove default pointer behavior established by impoint
        iptSetPointerBehavior(h_vertex,[]);
        
        % Remove default context menu created by impoint
        delete(get(h_vertex,'UIContextMenu'));
        set(h_vertex,'UIContextMenu',[]);
        
    	iptaddcallback(h_vertex,'ButtonDownFcn',@vertexButtonDown);
                			
    end %createVertex
    
end %impoly
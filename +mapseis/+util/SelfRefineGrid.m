function RefGrid = SelfRefineGrid(locations,SetupParameter,theState)
	%This function will build a variable grid based on the data density
	%at the moment it is a prototype function
	
	%SetupParameter
	%StartGrid: 	Defines the initalgrid used with a classical [x y dx dy NodeUsed] array
	%		x,y give the middle point of the nodes and dx,dy determine the gridspacing
	%		NodeUsed can be 0 for not used 1 for used and 2 for "has childrens" it will be 
	%		set by the algorithmen so it can be any value for init but it is suggest to use
	%		1 in case it will be used in some future version of the code.
	%Radius_coeff: 	The Selection radius will be determine by 
	%		squrt(dx^2+dy^2)*Radius_coeff
	%maxNumber:	The minimum number of dots per node allowed lower nodes
	%		will not be splitted anymore
	%minNumber:	Nodes with less events than minNumber will not be used.
	%minSpacing:	This [min_dx min_dy] will limit the minimal gridsize, to avoid to small cells
	%		if now limit is wanted, just set to [-inf -inf]
	%parallelmode: 	should turn the parallel processing mode, but does
	%KeepTop:	if this is set to true, the grid will at least contain the inputgrid.
	%StrictMode:	Similar to KeepTop, but in this case the larger cell will always be kept if not 
	%		fully replaced by the underlying cells (on dim smaller)
	
	if isfield(SetupParameter,'parallelmode')
		parall=SetupParameter.parallelmode;
	else
		parall=false;
	end
	
	
	if ~isfield(SetupParameter,'minSpacing')
		SetupParameter.minSpacing=[-inf -inf];
	end	
	
	if ~isfield(SetupParameter,'KeepTop')
		SetupParameter.KeepTop=false;
	end	
	
	if ~isfield(SetupParameter,'StrictMode')
		SetupParameter.StrictMode=false;
	end	
	
	%theState is either 'inital' for starting a run or 'recurs' for recursion
	%runs
	RefGrid=[];
	CellularGrid={};
	import mapseis.util.*;
	
	switch theState
		case 'initial'
			
			currGrid=SetupParameter.StartGrid;
			rowCount=numel(locations(:,1));
			if ~parall
				for i=1:numel(currGrid(:,1))
					
					SelRadius = sqrt(currGrid(i,3)^2+currGrid(i,4)^2)/2*...
							SetupParameter.Radius_coeff;
					inCircle=sqrt(sum(...
					((locations-repmat(currGrid(i,1:2),rowCount,1)).^2),2)...
					) < SelRadius;
					
					if sum(inCircle)>=SetupParameter.maxNumber&currGrid(i,3)/2>=SetupParameter.minSpacing(1)&...
						currGrid(i,4)/2>=SetupParameter.minSpacing(2)
						NewStartGrid=[ currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1];
						newSetup=SetupParameter;
						newSetup.StartGrid=NewStartGrid;
						
						childGrid=SelfRefineGrid(locations,newSetup,'recurs');
						curGridLine=[currGrid(i,:),SelRadius];
						
						if all(childGrid(:,5)==0)
							curGridLine(1,5)=1;
							
							
						%elseif SetupParameter.KeepTop&any(childGrid(:,5)==0)
						%	curGridLine(1,5)=1;
							
						else
							curGridLine(1,5)=2;
							
						end
						
						RefGrid=[RefGrid;curGridLine;childGrid];
							
							
					elseif	sum(inCircle)>=SetupParameter.minNumber
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=1;
						RefGrid=[RefGrid;curGridLine];
					else
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=0;
						RefGrid=[RefGrid;curGridLine];
					end	
				end
				
			else
				%Some changes are needed here, the variable cannot grow like this
				matlabpool open
				parfor i=1:numel(currGrid(:,1))
					
					SelRadius = sqrt(currGrid(i,3)^2+currGrid(i,4)^2)/2*...
							SetupParameter.Radius_coeff;
					inCircle=sqrt(sum(...
					((locations-repmat(currGrid(i,1:2),rowCount,1)).^2),2)...
					) < SelRadius;
					
					if sum(inCircle)>=SetupParameter.maxNumber&currGrid(i,3)/2>=SetupParameter.minSpacing(1)&...
						currGrid(i,4)/2>=SetupParameter.minSpacing(2)
						NewStartGrid=[ currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1];
						newSetup=SetupParameter;
						newSetup.StartGrid=NewStartGrid;
						
						childGrid=SelfRefineGrid(locations,newSetup,'recurs');
						curGridLine=[currGrid(i,:),SelRadius];
						
						if all(childGrid(:,5)==0)
							curGridLine(1,5)=1;
						else
							curGridLine(1,5)=2;
						end
							%newlength=numel(childGrid(:,1));
							%RefGrid(end+1:end+newlength+1,:)=[curGridLine;childGrid];
							CellularGrid{i}=[curGridLine;childGrid];
							
					elseif	sum(inCircle)>=SetupParameter.minNumber
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=1;
						%RefGrid(end+1,:)=[curGridLine];
						CellularGrid{i}=curGridLine;
					else
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=0;
						%RefGrid(end+1,:)=[curGridLine];
						CellularGrid{i}=curGridLine;
					end	
				end
				
				matlabpool close
				tempCell=[cellfun( @ctranspose, CellularGrid,'UniformOutput',false)];

				RefGrid=[tempCell{:}]';
			end
			
		case 'recurs'
		
			currGrid=SetupParameter.StartGrid;
			rowCount=numel(locations(:,1));
			if ~parall
				for i=1:numel(currGrid(:,1))
					
					SelRadius = sqrt(currGrid(i,3)^2+currGrid(i,4)^2)/2*...
							SetupParameter.Radius_coeff;
					inCircle=sqrt(sum(...
					((locations-repmat(currGrid(i,1:2),rowCount,1)).^2),2)...
					) < SelRadius;
					
					if sum(inCircle)>=SetupParameter.maxNumber&currGrid(i,3)/2>=SetupParameter.minSpacing(1)&...
						currGrid(i,4)/2>=SetupParameter.minSpacing(2)
						NewStartGrid=[ currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1];
						newSetup=SetupParameter;
						newSetup.StartGrid=NewStartGrid;
						
						childGrid=SelfRefineGrid(locations,newSetup,'recurs');
						curGridLine=[currGrid(i,:),SelRadius];
						
						if all(childGrid(:,5)==0)
							curGridLine(1,5)=1;
						else
							curGridLine(1,5)=2;
						end
						
							RefGrid=[RefGrid;curGridLine;childGrid];
							
							
					elseif	sum(inCircle)>=SetupParameter.minNumber
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=1;
						RefGrid=[RefGrid;curGridLine];
					else
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=0;
						RefGrid=[RefGrid;curGridLine];
					end	
				end
				
			else
				%matlabpool open
				parfor i=1:numel(currGrid(:,1))
					
					SelRadius = sqrt(currGrid(i,3)^2+currGrid(i,4)^2)/2*...
							SetupParameter.Radius_coeff;
					inCircle=sqrt(sum(...
					((locations-repmat(currGrid(i,1:2),rowCount,1)).^2),2)...
					) < SelRadius;
					
					if sum(inCircle)>=SetupParameter.maxNumber&currGrid(i,3)/2>=SetupParameter.minSpacing(1)&...
						currGrid(i,4)/2>=SetupParameter.minSpacing(2)
						NewStartGrid=[ currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)+currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)+currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1;...
								currGrid(i,1)-currGrid(i,3)/4,currGrid(i,2)-currGrid(i,4)/4,...
								currGrid(i,3)/2,currGrid(i,4)/2,1];
						newSetup=SetupParameter;
						newSetup.StartGrid=NewStartGrid;
						
						childGrid=SelfRefineGrid(locations,newSetup,'recurs');
						curGridLine=[currGrid(i,:),SelRadius];
						
						if all(childGrid(:,5)==0)
							curGridLine(1,5)=1;
						else
							curGridLine(1,5)=2;
						end
						%newlength=numel(childGrid(:,1));
						%RefGrid(end+1:end+newlength+1,:)=[curGridLine;childGrid];
						
						CellularGrid{i}=[curGridLine;childGrid];
							
					elseif	sum(inCircle)>=SetupParameter.minNumber
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=1;
						%RefGrid(end+1,:)=[curGridLine];
						CellularGrid{i}=curGridLine;
					else
						curGridLine=[currGrid(i,:),SelRadius];
						curGridLine(1,5)=0;
						%RefGrid(end+1,:)=[curGridLine];
						CellularGrid{i}=curGridLine;
					end	
				end
				
				tempCell=[cellfun( @ctranspose, CellularGrid,'UniformOutput',false)];

				RefGrid=[tempCell{:}]';
				
				%matlabpool close
			end
			
	end
		
	
	
	
	






end

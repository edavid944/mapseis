function [XFine YFine Greypic]=LicBuilderTest(Streamers,Oversample,xgrid,ygrid,NoiseModel,BGBlack)
	%Just a small test script to build Lic plots
	
	

	xmin=min(min(xgrid));
	xmax=max(max(xgrid));
	ymin=min(min(ygrid));
	ymax=max(max(ygrid));
	TheSize=size(xgrid);
	
	%Build the new grid
	[XFine,YFine]=meshgrid(linspace(xmin,xmax,TheSize(2)*Oversample),linspace(ymin,ymax,TheSize(1)*Oversample));
	
	%Build the random data
	switch NoiseModel
		case 'uniform'
			LicTable=rand(size(XFine));
		case 'normal'
			LicTable=randn(size(XFine));
	end
	
	%Build raw Data
	if BGBlack
		Greypic=ones(size(XFine));
		RepVal=1;
		
	else
		Greypic=zeros(size(XFine));
		RepVal=0;
	end	
	
	%Greypic=LicTable;
	
	Sizer=size(XFine);
	selX=reshape(XFine,Sizer(1)*Sizer(2),1);
	selY=reshape(YFine,Sizer(1)*Sizer(2),1);
	
	%now the lic image can be calculated
	for i=1:numel(Streamers)
		CurStream=Streamers{i};
		%disp(i)
		%only take the parts with no nan
		CurStream=CurStream(~isnan(CurStream(:,1)),:);
		
		%only do something if it is more than a point
		if numel(CurStream)>2
			%seems like I have to go through every line node
			Noder=[];
			for j=1:numel(CurStream)/2;
				TheX=abs(selX)-abs(CurStream(j,1));
				TheY=abs(selY)-abs(CurStream(j,2));
				Dist=(TheX.^2+TheY.^2).^0.5;
				
				%find minimum distance
				[val idx]=min(Dist);
				
				%write position of the minima into a vector
				Noder(j)=idx(1);
				
			end
			
			%get mean value;
			CurColVal=mean(LicTable(Noder));
			
			%write to map
			Greypic(Noder)=CurColVal;
			
			
		end
		
		
	end
	
	
	%normalize picture
	
	overallMean=mean(Greypic(Greypic~=RepVal));
	Greypic(Greypic==RepVal)=overallMean;
	
	minVal=min(min(Greypic-overallMean));
	maxVal=max(max(Greypic-overallMean));
	
	Greypic=(Greypic-overallMean)./(maxVal-minVal);
	
end

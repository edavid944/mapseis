function absPath = PathMaker(relPath)
	%So simple it is almost embarrassing, still handy. It converts a
	%a relative path into a absolute path, if the path exists
	
	curDir=pwd;
	
	CheckExist=exist(relPath);
	
	
	if CheckExist==0	
		warning('Path does not exist')
		absPath=relPath;
	
	elseif CheckExist==7
		cd(relPath);
		absPath=pwd;
		cd(curDir);
	
	else
		%in case it is the folder to a file
		[pathstr, name, ext] = fileparts(relPath);
		cd(pathstr);
		thepath=pwd;
		cd(curDir);
		absPath=[thepath,filesep,name,ext];
	end


end

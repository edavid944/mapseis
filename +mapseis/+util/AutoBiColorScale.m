function biScale=AutoBiColorScale(minVal,maxVal,midGreyVal)
	%This function builds a bicolor scale with blue-green(-) grey (+)yellow-red
	%Applying the colorscale and setting the coloraxis (caxis) has to be done
	%manually
	
	%basic parameters
	Steps=256;
	greyman=[ 0.9333    0.9333    0.9333];
	
	if midGreyVal>=maxVal
		biScale=flipud(cool(Steps-3));
		biScale(Steps-2,:)=greyman;
		biScale(Steps-1,:)=greyman;
		biScale(Steps,:)=greyman;
		
		
	elseif midGreyVal<=minVal
		rawColor=flipud(autumn(Steps-3));
		biScale=[greyman;greyman;greyman;rawColor];
	
	else
		MidScale=round(((midGreyVal-minVal)/abs(maxVal-minVal))*Steps);
		lowScale=flipud(cool(MidScale-2));
		highScale=flipud(autumn(Steps-(MidScale+2)));
		biScale=[lowScale;greyman;greyman;greyman;greyman;highScale];
	end
	
	

end

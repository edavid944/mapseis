function planeStruct = readTable(inStr,rowSepPat,rowPat)
% readTable : Read table of data into a struct
%   Reads in a table to data and outputs a struct with fieldnames specifiec
%   by the <name> elements of rowPat.  Each column of the table becomes a
%   vector in the struct (ie plane-based data rodering)

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Import utility functions
import mapseis.util.importfilter.*;

%% Break up the input file into lines
lines = regexp(inStr,rowSepPat,'match');
% Return the matches as a struct array, with field names as in the rowPat
% pattern
matches = regexp(lines,rowPat,'names');

% Remove the non-matching cells
Empty=cellfun(@(x) ~isempty(x),matches);
matches = matches(Empty);


%% Convert the matches to a plane-based data type
% See Structures :: Data Types (MATLAB Programming) in MATLAB doc

planeStruct = cellArrayToPlane(matches);

end
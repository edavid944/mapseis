function patStruct = regexpPatternCatalog()
% regexpPatternCatalog : Catalog of useful regular expressions (regexp)
% Returns a struct of regexp patterns and useful functions for matching
% imported data

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Functions to alter regexp patterns

% Define function to add name to pattern for named pattern matching
patStruct.name = @(name,pat) ['(?<',name,'>',pat,')'];

%% General patterns
% Date in YYYY/MM/DD format
patStruct.datePat = '\d\d\d\d/\d\d/\d\d';

% Time in HH:mm:SS.ss
patStruct.timePat = '\d\d:\d\d:\d\d\.\d\d';
patStruct.timePatCSV = '\d\d:\d\d:\d\d';

%special data pattern yyyy-mm-dd or XXX-XX-XX
patStruct.specialDate='\w\w\w\w-\w\w-\w\w';

% Real number
patStruct.realPat = '[+|-]?\d*((\.\d*)?)';

% Floating point number
patStruct.floatPat = '[+|-]?\d*((\.\d*)?)[E|e][+|-]?\d*';

% Integer
patStruct.intPat = '\d*';
patStruct.intPatNegPos ='[+|-]?\d*';

%actually needless
patStruct.anything = '[^;]*';
patStruct.anythingC = '[^,]*'; %same as anything but with a comma instead of semicomma
patStruct.anythungW= '\S+';
%'\w*';


patStruct.MarkedText = '(\" \w* \")|\w*';

patStruct.text = '\w*';

% White Space
patStruct.ws = '\s+';

% semicolon
patStruct.sc = ';';

% Comma
patStruct.cs = ',';

% Underscore
patStruct.us= '_';

% End of line
patStruct.eol = '\r?\n';

% Line
patStruct.line = ['[^\r\n]*',patStruct.eol];

%% SCES specifc patterns
% Event type
% le - Local event 
% ts - Teleseismic event 
% re - Regional event 
% qb - Quarry blast 
% nt - Nuclear blast 
% sn - Sonic Blast 
% st - Subnet Trigger 
% uk - Unknown
patStruct.evTypePat = '(le|ts|re|qb|nt|sn|st|uk)';

% M - magtype	 type of magnitude 
%     'e' energy magnitude
%     'w' moment magnitude
%     'b' body-wave magnitude
%     's' surface-wave magnitude
%     'l' local (WOOD-ANDERSON) magnitude
%     'c' coda amplitude
%     'h' helicorder magnitude (short-period Benioff) 
%     'd' coda duration magnitude
%     'n' no magnitude	
patStruct.mPat = '(e|w|b|s|l|c|h|d|n)';

% Q - location quality
%     'A'  +- 1 km horizontal distance
%          +- 2 km depth
%     'B'  +- 2 km horizontal distance
%          +- 5 km depth
%     'C'  +- 5 km horizontal distance 
%             no depth restriction
%     'D'  >+- 5 km horizontal distance
%     'Z'	 no quality listed in database
patStruct.qPat = '(A|B|C|D|Z)';

%nph         number of picked phases
%ngrams      number of grams (i.e. # of station traces)

end
function outStruct = elementArrayToPlane(inStructArray)
% elementArrayToPlane : change struct array to struct of arrays
% Converts a struct array to the equivalent data structure indexed by fieldname

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

c = struct2cell(inStructArray);
fldNames = fieldnames(inStructArray);
numFlds = numel(fldNames);

for i=1:numFlds
    outStruct.(fldNames{i}) = [squeeze(c(i,:,:))];
end

end
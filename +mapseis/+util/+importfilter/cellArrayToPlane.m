function outStruct = cellArrayToPlane(inCellArray)
% cellArrayToPlane : change cell array of structs to struct of arrays
% Converts a cell array of structs to the equivalent data structure indexed
% by fieldname

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 62 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell


% Get the fieldnames from the first struct
fldNames = fieldnames(inCellArray{1});
% Number of fields in output structure
numFlds = numel(fldNames);
% Number of elements in each array of output structure

[rower coler]=size(inCellArray);

if rower==1|coler==1
	numData = numel(inCellArray);
	
	
	for i=1:numFlds
	    tempData = cell(numData,1);
	    for j=1:numData
		tempData{j}=inCellArray{j}.(fldNames{i});
	    end
	    outStruct.(fldNames{i}) = tempData;
	end
else
	numData = numel(inCellArray);
	
	inCellArray=inCellArray';
	for i=1:numFlds
	    tempData = cell(rower,coler);
	    tempData=tempData';
	    for j=1:numData
		tempData{j}=inCellArray{j}.(fldNames{i});
	    end
	    tempData=tempData';
	    outStruct.(fldNames{i}) = tempData;
	end	
	inCellArray=inCellArray';
end	
end
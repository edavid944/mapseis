function NightMode(plotAxis)
	%This sets the plot window of whatever plot used to black background
	%and white lines
	
	if nargin<1
		plotAxis=gca;
	end

	
	%get figure of the plotAxis
	CurFig=get(plotAxis,'Parent');
	
	%Do the work
	set(plotAxis,'Color','k','XColor','w','YColor','w','ZColor','w')
	set(CurFig,'Color','k');
	
	

end

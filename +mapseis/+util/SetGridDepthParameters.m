function SetGridDepthParameters(datastore,gridPars,key)
% SetGridParameters : set the grid parameters used by calculation

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if nargin<3
    key='gridDepthPars';
end

datastore.setUserData(key,gridPars);

end
function [gridSep,rad]=GetGridParameters(datastore,key)
% GetGridParameters : extract the grid parameters used by calculation

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if nargin<2
    key='gridPars';
end

try
    gridPars = datastore.getUserData(key);
    % Separation of grid points
    gridSep = gridPars.gridSep;
    % Radius of selection of events around each grid point
    rad = gridPars.rad;
catch ME
    % Key not found, use default values and warn user
    gridSep = 0.2;
    rad = 1;
    warning('GetGridParameters:unknown_key',['Unknown key: ', key]);
end


end
function FaultSize = calcWellsCopperLength(Mag,FaultType,isLength)
	%estimates Faultlenght (isLength=true) or the 
	%FaultArea based on the Magnitude according to Wells and Coppersmith 1994
	%Input for FaultSize has to be km or km^2
	%FaultType can either be a cell array with 'all', 'strike', 'thrust' or
	%'normal' for each fault or shorter a normal vector with
	%0=all; 1=strike; 2=thrust and 3=normal. 
	
	
	%DE 2013
	
	if iscell(FaultType)
		isAll = strcmp(FaultType,'all');
		isStrike = strcmp(FaultType,'strike');
		isThrust = strcmp(FaultType,'thrust')|strcmp(FaultType,'reverse');
		isNormal = strcmp(FaultType,'normal');
		
	else		
		isAll = FaultType==0;
		isStrike = FaultType==1;
		isThrust = FaultType==2;
		isNormal = FaultType==3;
	end
	

	FaultSize=NaN(size(Mag));
	
	if isLength
		FaultSize(isAll) = 10.^(-3.22+0.69*Mag(isAll));
		FaultSize(isStrike) = 10.^(-3.55+0.74*Mag(isStrike));
		FaultSize(isThrust) = 10.^(-2.86+0.63*Mag(isThrust));
		FaultSize(isNormal) = 10.^(-2.01+0.50*Mag(isNormal));
	
	
	else
		FaultSize(isAll) = 10.^(-3.49+0.9*Mag(isAll));
		FaultSize(isStrike) = 10.^(-3.42+0.90*Mag(isStrike));
		FaultSize(isThrust) = 10.^(-3.99+0.98*Mag(isThrust));
		FaultSize(isNormal) = 10.^(-2.87+0.82*Mag(isNormal));
	
	
	
	end


end

function Mag = calcHankBakunMag(FaultArea)
	%estimates Magnitude based on Fault Area according to Hank and Bakun 2002&2008
	
	%Input for FaultSize has to be km^2
	
	
	
	%DE 2013
	
	

	
	
	Mag=NaN(size(FaultArea));
	largerArea=FaultArea>537;
	
	Mag(~largerArea) = 3.98+log10(FaultArea(~largerArea));
	Mag(largerArea) = 3.07+(4/3)*log10(FaultArea(largerArea));
	
	

end

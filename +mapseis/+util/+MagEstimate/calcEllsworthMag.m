function Mag = calcEllsworthMag(FaultArea,Equation)
	%estimates Magnitude based on FaultArea according to Ellsworth 2003
	%Input for FaultArea has to be km^2
	%Equation can be 'A', 'B' or 'C' in a cell array or 1,2,3 in a normal array
	%and corresponds to equations 'EllsworthA','EllsworthB' and'EllsworthC'
	
	
	%DE 2013
	
	if iscell(Equation)
		isA = strcmp(Equation,'A');
		isB = strcmp(Equation,'B');
		isC = strcmp(Equation,'C');
		
	else		
		isA = Equation==1;
		isB = Equation==2;
		isC = Equation==3;
		
	end
	

	Mag=NaN(size(FaultArea));
	
	
	Mag(isA) = 4.1+log10(FaultArea(isA));
	Mag(isB) = 4.2+log10(FaultArea(isB));
	Mag(isC) = 4.3+log10(FaultArea(isC));
	
	

end

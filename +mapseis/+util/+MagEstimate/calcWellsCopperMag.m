function Mag = calcWellsCopperMag(FaultSize,FaultType,isLength)
	%estimates Magnitude based on Faultlength (isLength=true) or the 
	%FaultArea according to Wells and Coppersmith 1994
	%Input for FaultSize has to be km or km^2
	%FaultType can either be a cell array with 'all', 'strike', 'thrust' or
	%'normal' for each fault or shorter a normal vector with
	%0=all; 1=strike; 2=thrust and 3=normal. 
	
	
	%DE 2013
	
	if iscell(FaultType)
		isAll = strcmp(FaultType,'all');
		isStrike = strcmp(FaultType,'strike');
		isThrust = strcmp(FaultType,'thrust')|strcmp(FaultType,'reverse');
		isNormal = strcmp(FaultType,'normal');
		
	else		
		isAll = FaultType==0;
		isStrike = FaultType==1;
		isThrust = FaultType==2;
		isNormal = FaultType==3;
	end
	

	Mag=NaN(size(FaultSize));
	
	if isLength
		Mag(isAll) = 5.08+1.16*log10(FaultSize(isAll));
		Mag(isStrike) = 5.16+1.12*log10(FaultSize(isStrike));
		Mag(isThrust) = 5.00+1.22*log10(FaultSize(isThrust));
		Mag(isNormal) = 4.86+1.36*log10(FaultSize(isNormal));
	
	
	else
		Mag(isAll) = 4.07+0.98*log10(FaultSize(isAll));
		Mag(isStrike) = 3.98+1.02*log10(FaultSize(isStrike));
		Mag(isThrust) = 4.33+0.90*log10(FaultSize(isThrust));
		Mag(isNormal) = 3.93+1.02*log10(FaultSize(isNormal));
	
	
	
	end
	

end

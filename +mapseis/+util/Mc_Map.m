classdef Mc_Map < handle
		
	%This class is the data container for Mc maps, it allows to store a 4D Mc map 
	%and will return every Mc value for an inputed lon,lat,z,time position
	
	
	
	
	
	properties
		Mc_Maps
		std_Mc_Maps
		ProfileCoords
		InputDimension
		MapDimension
		MapType
		Nr_Steps
		Step_Times
		Interpol
		InterpolType
		Smoothing
		Orientation
		maxSelWidth
		
	end

	
	methods
	
	function obj = Mc_Map(MapType)
		%This will initalize the Mc map
		
		%The new version of Mc_map is based on TriScatteredInterp, the interpolation is mostly done by this module 
		%only in case of 4D maps this modul has to do additional interpolation task.
		
		%Maps should be added later only the MapType has to be set
		
		%Sketch of the data structure
		%each timestep: Mc_Maps [std_Mc_Maps] startTime endTime (in StepTimes)
		
		%Possible MapModes: 2D,Time_2D, 2D_Profile, Time_2D_Profile, 3D and Time_3D 
		
		%init
		obj.McMap=[];
		obj.std_Mc_Maps=[];
		obj.ProfileCoords=[];
		
		
		obj.MapType=MapType;
		
		obj.Nr_Steps=[];
		obj.Step_Times=[];
		obj.Interpol=false;
		obj.InterpolType='normal'; %for later use
		obj.Smoothing='none';
		obj.Meshed=false;
		obj.Orientation=[];
		obj.maxSelWidth=[];
		obj.InputDimension=[];
		
		
		
		
	
	end
	
	function setInterpol(obj,newstate)
		%allows to use interpolation in the time axis if set to on
		switch newstate
			case 'on'
				obj.Interpol=true;
			case 'off'
				obj.Interpol=false;
			case 'toggle'
				obj.Interpol=~obj.Interpol;
		end	
		
	
	end	
	
	
	function AddMap(obj,NewMap,std_Mc_Maps,StartTime,EndTime,ProfileLine,ProfileWidth)
		%This allows to add a whole new map at once, steps will be added
		%and MapType changed if necessary
		%A Map consists of a cell array with NewMap{1,1}=Mc, NewMap{1,2}=lon
		%NewMap{1,3}=lat and NewMap{1,4}=z in case of 3D maps.
		%if an axis is not needed it should be left empty, this will tell the
		%object which dimension a map has
		
		%ProfileLine & ProfileWidth is only needed in case of profiles
		%StartTime &EndTime is only needed in case of time varying maps.
		
		
		
		switch obj.MapType
			case '2D'
				%easiest case 
				lon=NewMap{1,2};
				lat=NewMap{1,3};
				McVal=NewMap{1,1};
				
				if ~isempty(std_Mc_Maps);
					stdMc=stMcMaps{1,1};
				else
					stdMc=[];
				end
				
				Sizer=size(McVal);
				obj.InputDimension=Sizer;
				
				if ~any(Sizer==1)
					%reshape needed
					lon=reshape(lon,1,Sizer(1)*Sizer(2));
					lat=reshape(lat,1,Sizer(1)*Sizer(2));
					McVal=reshape(McVal,1,Sizer(1)*Sizer(2));
					
					if ~isempty(stdMc)
						stdMc=reshape(stdMc,1,Sizer(1)*Sizer(2));
					end
						
				end	
				
				
				
				%check if the dimension is right or if a reshape has to be done
				
				McMapObject=TriScatteredInterp([lon lat],McVal);
				obj.McMap{1}=McMapObject;
				
				obj.Step_Times={NaN,NaN};
				
				if ~isempty(stdMc)
					stdMcMapObject=TriScatteredInterp([lon lat],stdMc);
					obj.std_Mc_Maps{1}=stdMcMapObject;
					
					
				end
				
				obj.Nr_Steps=1;
				
				
			case 'Time_2D'
				%Times needed here
				
				%first prepare the data
				for i=1:numel(NewMap(:,1))
					lon=NewMap{i,2};
					lat=NewMap{i,3};
					McVal=NewMap{i,1};
					
					if ~isempty(std_Mc_Maps);
						stdMc=stMcMaps{i,1};
					else
						stdMc=[];
					end
					
					Sizer=size(McVal);
					InputDim(i,:)=Sizer;
					
					if ~any(Sizer==1)
						%reshape needed
						lon=reshape(lon,1,Sizer(1)*Sizer(2));
						lat=reshape(lat,1,Sizer(1)*Sizer(2));
						McVal=reshape(McVal,1,Sizer(1)*Sizer(2));
						
						if ~isempty(stdMc)
							stdMc=reshape(stdMc,1,Sizer(1)*Sizer(2));
						end
							
					end	
					
					McMapObject=TriScatteredInterp([lon lat],McVal);
					TheMcMaps{i}=McMapObject;
				
					StepTimer(i,:)={StartTime(i), EndTime(i)};
					
					if ~isempty(stdMc)
						stdMcMapObject=TriScatteredInterp([lon lat],stdMc);
						TheStdMaps{i}=stdMcMapObject;
					
					end
				
				end
				
				
				
				
				
				%check where it should be written into
				if isempty(obj.McMap)
					%easy case everything is empty
					obj.McMap=TheMcMaps;
					obj.Step_Times=StepTimer;
					obj.InputDimension=InputDim;
					
					if ~isempty(stdMc) 
						%might not exist
						obj.std_Mc_Maps=TheStdMaps;
					
					end
					
					
				else
					%go check where it has to be placed in
					for i=1:numel(TheMcMaps)
						TimStarter=StepTimer(i,1);
						TimEnder=StepTimer(i,2);
						
						EqualTime=(TimStarter==obj.Step_Times(:,1))&(TimEnder==obj.Step_Times(:,2));
						if any(EqualTime)
							obj.McMap(EqualTime)=TheMcMaps(i);
							obj.Step_Times(EqualTime,:)=StepTimer(i,:);
							obj.InputDimension(EqualTime,:)=InputDim(i,:);
							
							if ~isempty(stdMc) 
								%might not exist
								obj.std_Mc_Maps(EqualTime)=TheStdMaps(i);
							
							end
							
							
						else
							%put it to the end and sort it later
							obj.McMap(end+1)=TheMcMaps(i);
							obj.Step_Times(end+1,:)=StepTimer(i,:);
							obj.InputDimension(end+1,:)=InputDim(i,:);
							
							if ~isempty(stdMc) 
								%might not exist
								obj.std_Mc_Maps(end+1)=TheStdMaps(i);
							
							end
							
						
						end
					end
					
					%Now sort the Maps according to StartTime;
					[temp,SortArray] = sort(obj.Step_Times(:,1));
					
					obj.McMap=obj.McMap(SortArray);
					obj.Step_Times=obj.Step_Times(SortArray,:);
					obj.InputDimension=obj.InputDimension(SortArray,:);
					
					if ~isempty(stdMc) 
							%might not exist
							obj.std_Mc_Maps=obj.std_Mc_Maps(SortArray);
							
					end
					
					
					
					
				end
				
				
				obj.Nr_Steps=numel(obj.McMap);
				
			
			
			case '2D_Profile'
				%The Profile Data is needed here to get "absolute" position back
				%The input has to be depth and distance along the profile in km, 
				%posisition will be saved as both, but the depth/distance coordinates,
				%will only be used in case of not interpolated maps.
				
				
				%save the Proil Coordinates
				dist=NewMap{1,2};
				depth=NewMap{1,3};
				McVal=NewMap{1,1};
				
				
				
				if ~isempty(std_Mc_Maps);
					stdMc=stMcMaps{1,1};
				else
					stdMc=[];
				end
				
				Sizer=size(McVal);
				obj.InputDimension=Sizer;
				
				
				if ~any(Sizer==1)
					%reshape needed
					dist=reshape(dist,1,Sizer(1)*Sizer(2));
					depth=reshape(depth,1,Sizer(1)*Sizer(2));
					McVal=reshape(McVal,1,Sizer(1)*Sizer(2));
					
					if ~isempty(stdMc)
						stdMc=reshape(stdMc,1,Sizer(1)*Sizer(2));
					end
						
				end	
				
				
				
				obj.ProfileCoords=[dist,depth];
				obj.Orientation=ProfileLine;
				obj.maxSelWidth=ProfileWidth;
				
				%now converte to absolute spherical 3D coordinates
				[lat lon depth d_lat d_lon d_depth] =...
					mapseis.util.Mc_Map.Strike2Geo('profile',[],obj.Orientation,dist,[],depth,obj.maxSelWidth)
				
				
				
				
				
				
				%check if the dimension is right or if a reshape has to be done
				
				McMapObject=TriScatteredInterp([lon lat depth],McVal);
				obj.McMap{1}=McMapObject;
				
				obj.Step_Times={NaN,NaN};
				
				if ~isempty(stdMc)
					stdMcMapObject=TriScatteredInterp([lon lat depth],stdMc);
					obj.std_Mc_Maps{1}=stdMcMapObject;
					
					
				end
				
				obj.Nr_Steps=1;
				
				
				
				
				
				
			
			case 'Time_2D_Profile'
				%Times needed here
				
				%The Common data, the profile itself, but not necessary the grid on the profile.
				obj.Orientation=ProfileLine;
				obj.maxSelWidth=ProfileWidth;
				
				%first prepare the data
				for i=1:numel(NewMap(:,1))
					dist=NewMap{i,2};
					depth=NewMap{i,3};
					McVal=NewMap{i,1};
					
					if ~isempty(std_Mc_Maps);
						stdMc=stMcMaps{i,1};
					else
						stdMc=[];
					end
					
					Sizer=size(McVal);
					InputDim(i,:)=Sizer;
					
					if ~any(Sizer==1)
						%reshape needed
						dist=reshape(dist,1,Sizer(1)*Sizer(2));
						depth=reshape(depth,1,Sizer(1)*Sizer(2));
						McVal=reshape(McVal,1,Sizer(1)*Sizer(2));
						
						if ~isempty(stdMc)
							stdMc=reshape(stdMc,1,Sizer(1)*Sizer(2));
						end
							
					end	
				
				
				
					
					ProfiCoordi(i,:)=[dist,depth];
					
					%now converte to absolute spherical 3D coordinates
					[lat lon depth d_lat d_lon d_depth] =...
					mapseis.util.Mc_Map.Strike2Geo('profile',[],obj.Orientation,dist,[],depth,obj.maxSelWidth)
				
				
					
					
					
					
					McMapObject=TriScatteredInterp([lon lat depth],McVal);
					TheMcMaps{i}=McMapObject;
				
					StepTimer(i,:)={StartTime(i), EndTime(i)};
					
					if ~isempty(stdMc)
						stdMcMapObject=TriScatteredInterp([lon lat depth],stdMc);
						TheStdMaps{i}=stdMcMapObject;
					
					end
				
				end
				
				%check where it should be written into
				if isempty(obj.McMap)
					%easy case everything is empty
					obj.McMap=TheMcMaps;
					obj.Step_Times=StepTimer;
					obj.ProfileCoords=ProfiCoordi;
					obj.InputDimension=InputDim;
					
					if ~isempty(stdMc) 
						%might not exist
						obj.std_Mc_Maps=TheStdMaps;
					
					end
					
					
				else
					%go check where it has to be placed in
					for i=1:numel(TheMcMaps)
						TimStarter=StepTimer(i,1);
						TimEnder=StepTimer(i,2);
						
						EqualTime=(TimStarter==obj.Step_Times(:,1))&(TimEnder==obj.Step_Times(:,2));
						if any(EqualTime)
							obj.McMap(EqualTime)=TheMcMaps(i);
							obj.Step_Times(EqualTime,:)=StepTimer(i,:);
							obj.ProfileCoords(EqualTime,:)=ProfiCoordi(i,:);
							obj.InputDimension(EqualTime,:)=InputDim(i,:);
							
							if ~isempty(stdMc) 
								%might not exist
								obj.std_Mc_Maps(EqualTime)=TheStdMaps(i);
							
							end
							
							
						else
							%put it to the end and sort it later
							obj.McMap(end+1)=TheMcMaps(i);
							obj.Step_Times(end+1,:)=StepTimer(i,:);
							obj.ProfileCoords(end+1,:)=ProfiCoordi(i,:);
							obj.InputDimension(end+1,:)=InputDim(i,:);
							
							if ~isempty(stdMc) 
								%might not exist
								obj.std_Mc_Maps(end+1)=TheStdMaps(i);
							
							end
							
						
						end
					end
					
					%Now sort the Maps according to StartTime;
					[temp,SortArray] = sort(obj.Step_Times(:,1));
					
					obj.McMap=obj.McMap(SortArray);
					obj.Step_Times=obj.Step_Times(SortArray,:);
					obj.ProfileCoords=obj.ProfileCoords(SortArray,:);
					obj.InputDimension=obj.InputDimension(SortArray,:);
					
					if ~isempty(stdMc) 
							%might not exist
							obj.std_Mc_Maps=obj.std_Mc_Maps(SortArray);
							
					end
					
					
					
					
				end
				
				
				obj.Nr_Steps=numel(obj.McMap);
				
				
				
			
			case '3D'
			
				%3D case 
				lon=NewMap{1,2};
				lat=NewMap{1,3};
				depth=NewMap{1,4};
				McVal=NewMap{1,1};
				
				if ~isempty(std_Mc_Maps);
					stdMc=stMcMaps{1,1};
				else
					stdMc=[];
				end
				
				Sizer=size(McVal);
				obj.InputDimension=Sizer;
				
				if ~any(Sizer==1)
					%reshape needed
					lon=reshape(lon,1,Sizer(1)*Sizer(2)*Sizer(3));
					lat=reshape(lat,1,Sizer(1)*Sizer(2)*Sizer(3));
					depth=reshape(depth,1,Sizer(1)*Sizer(2)*Sizer(3));
					McVal=reshape(McVal,1,Sizer(1)*Sizer(2)*Sizer(3));
					
					if ~isempty(stdMc)
						stdMc=reshape(stdMc,1,Sizer(1)*Sizer(2)*Sizer(3));
					end
						
				end	
				
				
				
				%check if the dimension is right or if a reshape has to be done
				
				McMapObject=TriScatteredInterp([lon lat depth],McVal);
				obj.McMap{1}=McMapObject;
				
				obj.Step_Times={NaN,NaN};
				
				if ~isempty(stdMc)
					stdMcMapObject=TriScatteredInterp([lon lat depth],stdMc);
					obj.std_Mc_Maps{1}=stdMcMapObject;
					
					
				end
				
				obj.Nr_Steps=1;
				
			
				
			
			case 'Time_3D'
				
				%first prepare the data
				for i=1:numel(NewMap(:,1))
					lon=NewMap{i,2};
					lat=NewMap{i,3};
					depth=NewMap{i,4};
					McVal=NewMap{i,1};
					
					if ~isempty(std_Mc_Maps);
						stdMc=stMcMaps{i,1};
					else
						stdMc=[];
					end
					
					Sizer=size(McVal);
					InputDim(i,:)=Sizer;
					
					if ~any(Sizer==1)
						%reshape needed
						lon=reshape(lon,1,Sizer(1)*Sizer(2)*Sizer(3));
						lat=reshape(lat,1,Sizer(1)*Sizer(2)*Sizer(3));
						depth=reshape(depth,1,Sizer(1)*Sizer(2)*Sizer(3));
						McVal=reshape(McVal,1,Sizer(1)*Sizer(2)*Sizer(3));
						
						if ~isempty(stdMc)
							stdMc=reshape(stdMc,1,Sizer(1)*Sizer(2)*Sizer(3));
						end
							
					end	
					
					McMapObject=TriScatteredInterp([lon lat depth],McVal);
					TheMcMaps{i}=McMapObject;
				
					StepTimer(i,:)={StartTime(i), EndTime(i)};
					
					if ~isempty(stdMc)
						stdMcMapObject=TriScatteredInterp([lon lat depth],stdMc);
						TheStdMaps{i}=stdMcMapObject;
					
					end
				
				end
				
				
				
				
				
				%check where it should be written into
				if isempty(obj.McMap)
					%easy case everything is empty
					obj.McMap=TheMcMaps;
					obj.Step_Times=StepTimer;
					obj.InputDimension=InputDim;
					
					if ~isempty(stdMc) 
						%might not exist
						obj.std_Mc_Maps=TheStdMaps;
					
					end
					
					
				else
					%go check where it has to be placed in
					for i=1:numel(TheMcMaps)
						TimStarter=StepTimer(i,1);
						TimEnder=StepTimer(i,2);
						
						EqualTime=(TimStarter==obj.Step_Times(:,1))&(TimEnder==obj.Step_Times(:,2));
						if any(EqualTime)
							obj.McMap(EqualTime)=TheMcMaps(i);
							obj.Step_Times(EqualTime,:)=StepTimer(i,:);
							obj.InputDimension(EqualTime,:)=InputDim(i,:);
							
							if ~isempty(stdMc) 
								%might not exist
								obj.std_Mc_Maps(EqualTime)=TheStdMaps(i);
							
							end
							
							
						else
							%put it to the end and sort it later
							obj.McMap(end+1)=TheMcMaps(i);
							obj.Step_Times(end+1,:)=StepTimer(i,:);
							obj.InputDimension(end+1,:)=InputDim(i,:);
					
							if ~isempty(stdMc) 
								%might not exist
								obj.std_Mc_Maps(end+1)=TheStdMaps(i);
							
							end
							
						
						end
					end
					
					%Now sort the Maps according to StartTime;
					[temp,SortArray] = sort(obj.Step_Times(:,1));
					
					obj.McMap=obj.McMap(SortArray);
					obj.Step_Times=obj.Step_Times(SortArray,:);
					obj.InputDimension=obj.InputDimension(SortArray,:);
					
					if ~isempty(stdMc) 
							%might not exist
							obj.std_Mc_Maps=obj.std_Mc_Maps(SortArray);
							
					end
					
					
					
					
				end
				
				
				obj.Nr_Steps=numel(obj.McMap);	
			
			
			
			
		end	
		
		
		
	
	end
	
	
	

	
	function [maps coords InputSize ProfileCoords] = GetWholeMap(obj,Mrtime)
		%Returns the uninterpolated map
		maps=[];
		coords=[];
		ProfileCoords=[];
		
		switch obj.MapType
			case '2D'
				%no time needed just return the map
				maps{1}=obj.McMap.V;
				coords=obj.McMap.X;
				
				%in case a reshape has to be done later
				InputSize=obj.InputDimension;
				
				if ~isempty(obj.std_Mc_Maps)
					maps{2}=obj.std_Mc_Maps.V;
				end
				
				
				
			case 'Time_2D'
				%search the right position
				TheTime=logical(zeros(numel(obj.Step_Time(:,1)),1));
				if Mrtime>=obj.Step_Times(end,2);
					TheTime(end)=true;
				
				elseif Mrtime<=obj.Step_Times(1,1);
					TheTime(1)=true;
				
				else
					TheTime=(Mrtime>=obj.Step_Times(:,1))&(Mrtime<=obj.Step_Times(:,2));
				
				end
				
				
				if ~any(TheTime)
					%Try find the nearest map (needed in case of holes)
					LargerTime=(Mrtime>=obj.Step_Times(:,1));
					countArray=1:numel(obj.Step_Time(:,1));
					counted=countArray(LargerTime);
					TheTime=logical(zeros(numel(obj.Step_Time(:,1)),1));
					TheTime(counted)=true;
				end
				
				if sum(TheTime)>1
					%more than one found, use the first one
					countArray=1:numel(obj.Step_Time(:,1));
					counted=countArray(TheTime);
					TheTime=countArray(countArray==min(counted));
					
				end
				
				maps{1}=obj.McMap{TheTime}.V;
				coords=obj.McMap{TheTime}.X;
				
				%in case a reshape has to be done later
				InputSize=obj.InputDimension(TheTime,:);
				
				if ~isempty(obj.std_Mc_Maps)
					maps{2}=obj.std_Mc_Maps{TheTime}.V;
				end
				
				
				
			case '2D_Profile'
				%no time needed just return the map
				maps{1}=obj.McMap.V;
				coords=obj.McMap.X;
				ProfileCoords=obj.ProfileCoords;
				
				%in case a reshape has to be done later
				InputSize=obj.InputDimension;
				
				if ~isempty(obj.std_Mc_Maps)
					maps{2}=obj.std_Mc_Maps.V;
				end
				
				
			
			case 'Time_2D_Profile'
				%search the right position
				TheTime=logical(zeros(numel(obj.Step_Time(:,1)),1));
				if Mrtime>=obj.Step_Times(end,2);
					TheTime(end)=true;
				
				elseif Mrtime<=obj.Step_Times(1,1);
					TheTime(1)=true;
				
				else
					TheTime=(Mrtime>=obj.Step_Times(:,1))&(Mrtime<=obj.Step_Times(:,2));
				
				end	
				
				
				
				if ~any(TheTime)
					%Try find the nearest map (needed in case of holes)
					LargerTime=(Mrtime>=obj.Step_Times(:,1));
					countArray=1:numel(obj.Step_Time(:,1));
					counted=countArray(LargerTime);
					TheTime=logical(zeros(numel(obj.Step_Time(:,1)),1));
					TheTime(counted)=true;
				end
				
				if sum(TheTime)>1
					%more than one found, use the first one
					countArray=1:numel(obj.Step_Time(:,1));
					counted=countArray(TheTime);
					TheTime=countArray(countArray==min(counted));
					
				end
				
				maps{1}=obj.McMap{TheTime}.V;
				coords=obj.McMap{TheTime}.X;
				ProfileCoords=obj.ProfileCoords(TheTime,:);
				
				%in case a reshape has to be done later
				InputSize=obj.InputDimension(TheTime,:);
				
				if ~isempty(obj.std_Mc_Maps)
					maps{2}=obj.std_Mc_Maps{TheTime}.V;
				end
			
				
			
			case '3D'
				%no time needed just return the map
				maps{1}=obj.McMap.V;
				coords=obj.McMap.X;
				
				%in case a reshape has to be done later
				InputSize=obj.InputDimension;
				
				if ~isempty(obj.std_Mc_Maps)
					maps{2}=obj.std_Mc_Maps.V;
				end
				
				
				
			case 'Time_3D'
				%search the right position
				TheTime=logical(zeros(numel(obj.Step_Time(:,1)),1));
				if Mrtime>=obj.Step_Times(end,2);
					TheTime(end)=true;
				
				elseif Mrtime<=obj.Step_Times(1,1);
					TheTime(1)=true;
				
				else
					TheTime=(Mrtime>=obj.Step_Times(:,1))&(Mrtime<=obj.Step_Times(:,2));
				
				end
				
				
				if ~any(TheTime)
					%Try find the nearest map (needed in case of holes)
					LargerTime=(Mrtime>=obj.Step_Times(:,1));
					countArray=1:numel(obj.Step_Time(:,1));
					counted=countArray(LargerTime);
					TheTime=logical(zeros(numel(obj.Step_Time(:,1)),1));
					TheTime(counted)=true;
				end
				
				if sum(TheTime)>1
					%more than one found, use the first one
					countArray=1:numel(obj.Step_Time(:,1));
					counted=countArray(TheTime);
					TheTime=countArray(countArray==min(counted));
					
				end
				
				maps{1}=obj.McMap{TheTime}.V;
				coords=obj.McMap{TheTime}.X;
				
				%in case a reshape has to be done later
				InputSize=obj.InputDimension(TheTime,:);
				
				if ~isempty(obj.std_Mc_Maps)
					maps{2}=obj.std_Mc_Maps{TheTime}.V;
				end
				
				
				
			
		end
		
			
	
		
	end
	
	
	
	function [maps] = GetInterpolatedMap(obj,MrTime,coords,InterType)
		%Similar to GetWholeMap but allows to interpolate on the map and 
		%between the maps.
		
		%InterType sets the interpolation mode on the map possible is
		%'nearest', 'natural' and 'linear', see doc TriScatteredInterp for
		%more information.
		
		
		if narginy<4
			InterType='nearest';
		end
		
		if isempty(InterType)
			InterType='nearest';
		end
		
		
		if numel(MrTime)==1
			%only one time used
			switch obj.MapType
				case '2D'
					
				case 'Time_2D'
				case '2D_Profile'
				case 'Time_2D_Profile'
				case '3D'
				case 'Time_3D'
				
			end
		
		else
			%interpolation over different maps have to be done.
			switch obj.MapType
				case '2D'
					
				case 'Time_2D'
				case '2D_Profile'
				case 'Time_2D_Profile'
				case '3D'
				case 'Time_3D'
				
			end
		
		
		end
		
		
		
	
	end
	
	
	
	function Map = MapInterpolator(obj,InterType,newGridType,time,newMapCoord)
		
	
	end
	
	
	function McVal = GetSingleMc(obj,pos,time,InterType)
		
		
		
	
	end
	
	
	function Smoothermap(obj,dim,SmoothFactor)
		
		
		
	end
	
	


	
	end	


	methods (static)
	
	function [lat lon depth d_lat d_lon d_depth] = Strike2Geo(workmode,orientation,Strike,d_Strike,Depth,d_Depth,selWidth)
		%This function transforms a line or a depth profile with strike, orientation and depth into a 
		%lon, lat, depth based line or profile.
		
		%convert km into deg
		Strike=km2deg(Strike);
		d_Strike=km2deg(d_Strike);
		selWidth=km2deg(selWidth);
		depth=km2deg(Depth);
		d_depth=km2deg(d_Depth);
		
		switch workmode
			case 'profile'
				%first get the azimuth (angle) between point 1 and 2 in the orientation
				%azimuth(lat1,lon1,lat2,lon2)
				azimuthangle=azimuth('gc',orientation(1,2),orientation(1,1),orientation(2,2),orientation(2,1));
				
				LonZero=orientation(1,1);
				LatZero=orientation(1,2);
				
				%now convert the the strike cell to lat,lon cells
				for i=1:numel(Strike)
					[lat(i),lon(i)] = reckon(LatZero,LonZero,Strike(i),azimuthangle);
					
				
				end
				
				
			case 'line'
				%first get the azimuth (angle) between point 1 and 2 in the orientation
				%azimuth(lat1,lon1,lat2,lon2)
				[elevationangle,slantrange,azimuthangle] = elevation(orientation(1,2),orientation(1,1),orientation(1,3)*1000,...
										orientation(2,2),orientation(2,1)orientation(2,3)*1000);
				
				
				
				
				
		end		
		
		
	
	end
	
	
	
	
	end
	
	
end		

function filterList = DefaultFilters(dataStore,ListProxy)
import mapseis.filter.*;
import mapseis.datastore.*;
import mapseis.plot.*;

regionFilter = RegionFilter('all',[],'Region');
timeFilter = TimeRangeFilter('all',[-Inf Inf],'Time');
magFilter = MagFilter('all',[-Inf Inf],'Magnitude');
depthFilter = DepthFilter('all',[-Inf Inf],'Depth');
hourFilter = HourRangeFilter('all',[-Inf Inf],'Hour');
minuteFilter = MinuteRangeFilter('all',[-Inf Inf],'Minute');

% Initialise the filters to make the selection vectors the correct length
regionFilter.execute(dataStore);
timeFilter.execute(dataStore);
magFilter.execute(dataStore);
depthFilter.execute(dataStore);
hourFilter.execute(dataStore);
minuteFilter.execute(dataStore);

%Add Region to regionfilter
SetRegion([],regionFilter,'all',dataStore);

% Composite FilterList object
filterList = FilterList({regionFilter,timeFilter,magFilter,...
    depthFilter,hourFilter,minuteFilter},dataStore,ListProxy);


end
classdef CatalogRandomizer < mapseis.datastore.DataStore



	properties
		%FieldNames
		%DataStruct % Property holding the main application data
		%UserData % User-specific settings eg filter settings etc
		%Routing % sets the needed fields
		%NumberedUserData %has to be filled with UserData names which contain data which is
				 %available for each catalog entry (those events will be automatically
				 %filtered incase filtered data is edited)		 
		%LonOffset
		%LatOffset
		%EventType	%for decluster info
		%ClusterNR	%for decluster info
		%TypeUsed	%for decluster info
		PertubeID
		PertubeSwitches		
		PertubeErrors
		PertubeDistro
		UnModDataStruct
		LonField
		LatField
		LocationLink
	end
	
	
	methods 
		function obj=CatalogRandomizer(datastore)
			%Clone the original datastore	
			InterData = SpitOut(datastore);
			
			obj = obj@mapseis.datastore.DataStore(InterData.DataStruct);
			obj.FieldNames=InterData.FieldNames;
			obj.DataStruct=InterData.DataStruct;
			obj.UserData=InterData.UserData;
			obj.Routing=InterData.Routing;
			obj.NumberedUserData=InterData.NumberedUserData;
			obj.LonOffset=InterData.LonOffset;
			obj.LatOffset=InterData.LatOffset;
			obj.EventType=InterData.EventType;
			obj.ClusterNR=InterData.ClusterNR;
			obj.TypeUsed=InterData.TypeUsed;
		
			obj.RandomID=InterData.RandomID;
			obj.Version=InterData.Version;
			obj.UseShift=InterData.UseShift;
			obj.ShowUnclassified=InterData.ShowUnclassified;
			obj.DeclusterMetaData=InterData.DeclusterMetaData;
		
			%New Variables
			obj.PertubeID=-9; %this will be changed everytime the catalog is shaked
			obj.PertubeSwitches=[];
			obj.PertubeErrors=[];
			obj.PertubeDistro=[];
			obj.UnModDataStruct=obj.DataStruct;
			
			%Seems like all the fields have to public to set them right
			
			%Now define which are the lon lat fields of the catalog
			if any(strcmp(obj.FieldNames,'lon'));
				obj.LonField={'lon'};
			else
				RoutFound=strcmp(obj.Routing(:,1),'lon');
				obj.LonField={'lon',obj.Routing{RoutFound,2}};
			end
			
			if any(strcmp(obj.FieldNames,'lat'));
				obj.LatField={'lat'};
			else
				RoutFound=strcmp(obj.Routing(:,1),'lat');
				obj.LatField={'lat',obj.Routing{RoutFound,2}};
			end
			
			%If this is set to true, one location error (Location) is assumed
			%and will be distributed to lon and lat.
			obj.LocationLink=true;
			
			
			%The field 'Location' in the PertubeSwitches structure turns
			%on the location pertubation and uses LocationLink to automatically 
			%pertube lon and lat 
			
					
			%for whatever reason this has to be done in matlab 2010+ if the there is 
			%problem whit existing shakerCatalogs, just use this command and save it 
			%again, it should work
			ReapplyData(obj);
			
		end
		
		
		function ReapplyData(obj)
			obj.setRouting(obj.Routing);
			obj.UserData=obj.UserData;
		end
		
		
		
		function ShakeCatalog(obj)
			%randomizes the catalog
			import mapseis.util.stat.laprnd;
			
			
			if ~isempty(obj.PertubeSwitches)
				%reset original catalog 
				obj.DataStruct=obj.UnModDataStruct;
				Switcher=fields(obj.PertubeSwitches);
				
				rowCount=getRawRowCount(obj);
				
				for i=1:numel(Switcher)
					if obj.PertubeSwitches.(Switcher{i})
						if ~isfield(obj.PertubeDistro,Switcher{i})	
							%to avoid empty distro fields set 'Gauss'
							%as default
							obj.PertubeDistro.(Switcher{i})='Gauss';
						end
						
						if ~strcmp(Switcher{i},'Location')
							%special field needs special measurments
							if strcmp(obj.PertubeDistro.(Switcher{i}),'Gauss');
								absPert=(normrnd(zeros(rowCount,1),ones(rowCount,1)).*obj.PertubeErrors.(Switcher{i}));
							elseif strcmp(obj.PertubeDistro.(Switcher{i}),'Laplace');
								absPert=laprnd(rowCount,1,0,obj.PertubeErrors.(Switcher{i}));
							end
							
							obj.DataStruct.(Switcher{i})=obj.DataStruct.(Switcher{i})+absPert;
							%disp(Switcher{i})
						else
							%location error is special 
							
							if obj.LocationLink
								absPert=abs(normrnd(zeros(rowCount,1),ones(rowCount,1)).*obj.PertubeErrors.Location);
								
								SelMan=rand(rowCount,1);
								LatDirect=ones(rowCount,1);
								LatDirect(SelMan<0.5)=-1;
								
								SelMan=rand(rowCount,1);
								LonDirect=ones(rowCount,1);
								LonDirect(SelMan<0.5)=-1;
								
								latPert=rand(rowCount,1).*absPert;
								lonPert=sqrt(absPert.^2 -latPert.^2);
								
								obj.LonAtPoint(lonPert,LonDirect);
								obj.LatAtPoint(latPert,LatDirect);
								
							else
								%not really that needed, just use the "normal" way
								absPertLon=(normrnd(zeros(rowCount,1),ones(rowCount,1)).*obj.PertubeErrors.(obj.LonField{1}));
								absPertLat=(normrnd(zeros(rowCount,1),ones(rowCount,1)).*obj.PertubeErrors.(obj.LatField{1}));
								TheDirect=ones(rowCount,1);
								obj.LonAtPoint(absPertLon,TheDirect);
								obj.LatAtPoint(absPertLat,TheDirect);
							end
						
						end
					
					end
				end
				
								
				%update routing buffers no problem here but it can cause some load in case the converters used are "expensive"
				obj.BufferRouting('all');
				
				%set permute it
				obj.PertubeID=rand;
			end
			
		end
		
		
		function ResetShaker(obj)
			%set the catalog back to the untouched values
			obj.DataStruct=obj.UnModDataStruct;
			obj.BufferRouting('all');
		end
		
		function SetError(obj,NameOfField,TheError)
			%sets the error of a field, the error can either be a vector with error values
			%for every field or a single value
			obj.PertubeErrors.(NameOfField)=TheError;
			
			
		end
		
		
		function SetDistro(obj,NameOfField,DistroType)
			%sets the distribition used for the error (currently only allowed for no location
			%fields). Use 'Gauss' for normal distributed errors and 'Laplace' for Laplace (or
			%double exponetial) distributed errors
			
			obj.PertubeDistro.(NameOfField)=DistroType;
		
		end
		
		function SetSwitches(obj,NameOfField,TheState)
			%Sets the pertubation on a field on and off 
			%If the switch already existed and was set to on, the field in
			%the shaken catalog is replaced by unmodified data
			
			if isfield(obj.PertubeSwitches,NameOfField)
				if obj.PertubeSwitches.(NameOfField)&~TheState
					if strcmp(NameOfField,'Location')
						obj.DataStruct.(obj.LonField{end})=obj.UnModDataStruct.(obj.LonField{end});
						obj.DataStruct.(obj.LatField{end})=obj.UnModDataStruct.(obj.LatField{end});
					else
						obj.DataStruct.(NameOfField)=obj.UnModDataStruct.(NameOfField);	
					
					end
					
					obj.BufferRouting('all');
					
				end
				
				obj.PertubeSwitches.(NameOfField)=TheState;
				
			else
				obj.PertubeSwitches.(NameOfField)=TheState;
				
			end
			
			
		end
		
		
		function LonAtPoint(obj,Pertub,TheDir)
			% just to kept the original stuff shorter
			radiusOfEarth = 6378.1;
			lon=degtorad(obj.DataStruct.(obj.LonField{end}));
			newLon=lon + TheDir.*(Pertub/radiusOfEarth);
			
			obj.DataStruct.(obj.LonField{end})=radtodeg(newLon);
			
			
		end
		
		
		
		function LatAtPoint(obj,Pertub,TheDir)
			% just to kept the original stuff shorter
			radiusOfEarth = 6378.1;
			lat=degtorad(obj.DataStruct.(obj.LatField{end}));
			newLat=lat + TheDir.*(Pertub/radiusOfEarth);
			
			obj.DataStruct.(obj.LatField{end})=radtodeg(newLat);
			
		end
		
		
		
	end
	
	
end	

function LoadFaults(faultData,DataStore);
	%This function takes the input, a structure builded with
	%faultData = load myfile.mat and checks wether there is any fault data
	%existing and writes them to the user data of the DataStore
	
	%check what data it is 
	if isfield(faultData,'dataStore')
		try 
			faults=faultData.dataStore.getUserData('faults')
		catch
			faults=[];
		end

	elseif	isfield(faultData,'faults')
		faults=faultData.faults;
	
	else
		faults=[];

	end
	
	DataStore.setUserData('faults',faults);
	
end

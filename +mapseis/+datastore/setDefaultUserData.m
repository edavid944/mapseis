function setDefaultUserData(dataStore)
% Set the default user data parameters for the application
import mapseis.plot.GetOverlay;

% Set the calculation grid spacing and radius of interest
gridPars.gridSep = 0.2;
gridPars.rad = 1;
dataStore.setUserData('gridPars',gridPars);

%Set Depth Grid
gridDepthPars.gridSep = 2;
gridDepthPars.rad = 5;
dataStore.setUserData('gridDepthPars',gridDepthPars);

%set profile width to default values
ProfileWidth = 0.5;
dataStore.setUserData('ProfileWidth',ProfileWidth);

% Set the selection radius to use for circular region selection
mainPars.selectRadius = 0.5;
% Histogram plot type
mainPars.histType = 'mag';
% Time plot type
mainPars.timeType = 'cumnum';

[Coast_PlotArgs, InternalBorder_PlotArgs] = GetOverlay(dataStore);

% Coastline data
mainPars.indexFileName = 'test';
dataStore.setUserData('mainPars',mainPars);
dataStore.setUserData('Coast_PlotArgs',Coast_PlotArgs);
dataStore.setUserData('InternalBorder_PlotArgs',InternalBorder_PlotArgs);
dataStore.setUserData('DistMode',true);

%set buffers
dataStore.bufferDate;
end

classdef assocArray < handle
    % assocArray : associative array for storing key-value pairs
    % The associative array (AKA 'hash' (Perl) or 'map') is defined by a set of
    % unique keys and the values that the map to.  This implementation defines
    % the following operations:
    %   set(Key, Value), getKeys(), lookup(Key), remove(Key), isempty()
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 61 $    $Date: 2008-10-13 11:39:22 +0100 (Mon, 13 Oct 2008) $
    % Author: Matt McDonnell
    
    %% Atributes
    properties
        data
    end
    % %% Interface
    %
    % funStruct = struct(...
    %     'set',@nSet,...
    %     'getKeys',@nGetKeys,...
    %     'lookup',@nLookup,...
    %     'isempty',@nIsEmpty,...
    %     'remove',@nRemove);
    
    %% Public Methods
    methods
        function obj = assocArray(varargin)
            % Constructor for assocArray
            % Initialise to empty cell array with 2 columns
            obj.data = cell(0,2);
           
        end
        
        function set(obj,key,val)
            % Define a key-value pair
            logInd = strcmp(obj.data(:,1),key);
            if isempty(logInd) || all(~logInd)
                obj.data(end+1,:) = {key, val};
            else
                obj.data{logInd,2} = val;
            end
        end
        
        function keys=getKeys(obj)
            % Get the keys as a cell array
            keys = obj.data(:,1);
        end
        
        function val=lookup(obj,key)
            % Lookup a key value
            logInd = strcmp(obj.data(:,1),key);
            val = obj.data{logInd,2};
        end
        
        function b=isEmpty(obj)
            % Test whether the associative array is empty
            b = isempty(obj.data);
        end
        
        function remove(obj,key)
            % Remove a key and value from the array
            logInd = strcmp(obj.data(:,1),key);
            obj.data = obj.data(~logInd,:);
        end    
        
        function hashcookie = ExportHash(obj)
        	%Simple function export all data with keys at once
		hashcookie=obj.data;
        end
        
        function replaceHash(obj,newHash)
        	%The function allows to replace the data in hash, all old data
        	%will be deleted. 
        	obj.data=newHash;
        end
        
        
    end

    
end
classdef DataStore < handle
    % DataStore : Central repository for MapSeis event data
    % The DataStore object is initialised by a struct produced by importing
    % data from an earthquake catalog using Import(xxxx)Catalog
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 86 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
    % Author: Matt McDonnell
    
    %Version 1.1: 	New is the Routing which allows to set the fields of the catalog
    %			freely to the default fields of mapseis, this allows to change the
    %			default fields anytime wanted, an example could be the useage of different
    %			magnitudes 
    %			The routing is momentary in experimental state, some changes may be added 
    %			later if needed.
    
    %Version 1.2:	Support for location offsets, this allows to shift lon and lat by a certain
    %			deg, it is usefull in case the region of interest has a shift (e.g. Lon 180 to -180) 
    %			in it.
    
    %Version 1.3:	Support for decluster algorithmen. 
    %				EventType:	 gives the type determined by the decluster algorithmen
    %						(1:single event, 2: mainshock, 3:aftershock and 0: unclassified)
    %				ClusterNR:	sets the number of the cluster a event belongs to
    %						is NaN in case of a single event
    %				TypeUsed:	is a 3 element vector and sets which eventtypes are
    %						used in the datastore [singleToggle mainshockToggle aftershockToggle]
    %				DeclusterMetaData: is an optional field and is used to store additional info of the 
    %						   decluster algorithmen
    %Note: 	the declustering should work, but still is a bit messy, it is possible, that the system will change later to 
    %		an external filtering.
    
    
    %% Attributes
    properties (GetAccess = 'public', SetAccess = 'public')
        FieldNames
        DataStruct % Property holding the main application data
        UserData % User-specific settings eg filter settings etc
        Routing % sets the needed fields
        NumberedUserData %has to be filled with UserData names which contain data which is
        		 %available for each catalog entry (those events will be automatically
        		 %filtered incase filtered data is edited)		 
        LonOffset
        LatOffset
        EventType	%for decluster info
        ClusterNR	%for decluster info
        TypeUsed	%for decluster info
        
    end
    
    properties %the not private part
    	RandomID %will be set different each time the datastore is modified
    	Version
    	UseShift %not functional in the modul itself more like a marker
    	ShowUnclassified
    	DeclusterMetaData %Optional is used in the decluster algorithmen
    end
    
    events
        Update
    end
    
    methods (Access='public')
        function obj=DataStore(varargin)
            import mapseis.datastore.assocArray;
            
            switch nargin
                case 1
                    % Argument is a structure
                    obj.DataStruct = varargin{1};
                    obj.FieldNames = fieldnames(obj.DataStruct);
                case 2
                    % Load the data store from a file.  First argument is the filename,
                    % second is either a function handle to the importer function or
                    % '-load' to load from a .mat file
                    if isa(varargin{2},'function_handle')
                        obj.innerFunStruct = varargin{2}(varargin{1}{:});
                    else
                        obj.innerFunStruct = load(fname);
                    end
            end
            
            % Create an associative array to store UserData associated with a set of
            % events.  This can be used to store GUI-specific settings such as grid
            % spacing, selection radius etc.
            % NB : this UserData will depend on the specific GUI configuration and
            % hence loading a DataStore structure that has been saved from one GUI
            % setup into another GUI setup will fail unless the GUIs are designed to
            % use default parameters if they can't read them from the DataStore.
            obj.NumberedUserData={};
            obj.UserData = assocArray();
            
            %set empty routing
            obj.Routing={};
            
            %set offset to 0
            obj.LonOffset=0;
            obj.LatOffset=0;
            obj.UseShift=false;
            
            %the declusterfields
            obj.EventType=[];
            obj.ClusterNR=[];
            obj.TypeUsed=[];
            obj.ShowUnclassified=true;
            obj.DeclusterMetaData=[];
            
            %Version, existing datastore files will not have a version id or an other
            %number if they created by a older version of Datastore, this allows to use
            %a converter to transform old datastores in new datastores.
            obj.Version=1.3;
            
                     
            
            obj.RandomID=rand;
        
        end
       
	function InterData = SpitOut(obj)
		%This function spits out all internal data and is ment for coning of the object
		InterData.FieldNames=obj.FieldNames;
		InterData.DataStruct=obj.DataStruct;
		InterData.UserData=obj.UserData;
		InterData.Routing=obj.Routing;
		InterData.NumberedUserData=obj.NumberedUserData;
		InterData.LonOffset=obj.LonOffset;
		InterData.LatOffset=obj.LatOffset;
		InterData.EventType=obj.EventType;
		InterData.ClusterNR=obj.ClusterNR;
		InterData.TypeUsed=obj.TypeUsed;
		
		InterData.RandomID=obj.RandomID;
		InterData.Version=obj.Version;
		InterData.UseShift=obj.UseShift;
		InterData.ShowUnclassified=obj.ShowUnclassified;
		InterData.DeclusterMetaData=obj.DeclusterMetaData;
		
        end
        
        
        function setNumberedFields(obj,NumberedUserData)
        	obj.NumberedUserData=NumberedUserData;
        end
        
        
        function NumberedUserData=getNumberedFields(obj)
        	NumberedUserData=obj.NumberedUserData;
        end
        
        
        
        function setLocationOffset(obj,LonOff,LatOff)
        	%sets the longitude and latitude offset
        	
        	if ~isempty(LonOff)
        		obj.LonOffset=LonOff;
        	end
        	
        	if ~isempty(LatOff)
        		obj.LatOffset=LatOff;
        	end
        	
        	
        	updateObservers(obj);
        	
        
        end
        
        
        function [LonOff LatOff]=getLocationOffset(obj)
        	%returns the Offsets, also sets them to 0 if not set before
        	
        	if isempty(obj.LonOffset)
        		obj.LonOffset=0;
        	end
        	
        	if isempty(obj.LatOffset)
        		obj.LatOffset=0;
        	end
        	
        	LonOff=obj.LonOffset;
        	LatOff=obj.LatOffset;
        end
        
        
        function [LocSelected LocUnselected]=getShiftedData(obj,rowIndices)
        	
        	 [dSelected, dUnselected]=getFields(obj,{'lon','lat'},rowIndices)
        	 
        	 LocSelected=[dSelected.lon, dSelected.lat];
        	 LocUnselected=[dUnselected.lon, dUnselected.lat];
        	 
        	 if ~isempty(obj.LonOffset)
			 if ~obj.LonOffset==0
			 	%Apply Offset
			 	LocSelected(:,1)=LocSelected(:,1)+obj.LonOffset;	
			 	LocUnselected(:,1)=LocUnselected(:,1)+obj.LonOffset;
			 	
			 	%correct overboarding coords
			 	LargerLocSel=LocSelected(:,1)>180;
			 	LargerLocUnSel=LocUnselected(:,1)>180;
			 	SmallerLocSel=LocSelected(:,1)<-180;
			 	SmallerLocUnSel=LocUnselected(:,1)<-180;
			 	
			 	LocSelected(LargerLocSel,1)=LocSelected(LargerLocSel,1)-360;
			 	LocUnSelected(LargerLocUnSel,1)=LocUnSelected(LargerLocUnSel,1)-360;
			 	LocSelected(SmallerLocSel,1)=LocSelected(SmallerLocSel,1)+360;
			 	LocUnSelected(SmallerLocUnSel,1)=LocUnSelected(SmallerLocUnSel,1)+360;
			 end
        	 end
        	 
        	 if ~isempty(obj.LatOffset)
			 if ~obj.LatOffset==0
			 	%Apply Offset
			 	LocSelected(:,2)=LocSelected(:,2)+obj.LatOffset;	
			 	LocUnselected(:,2)=LocUnselected(:,2)+obj.LatOffset;
			 	
			 	%correct overboarding coords
			 	LargerLocSel=LocSelected(:,2)>90;
			 	LargerLocUnSel=LocUnselected(:,2)>90;
			 	SmallerLocSel=LocSelected(:,2)<-90;
			 	SmallerLocUnSel=LocUnselected(:,2)<-90;
			 	
			 	LocSelected(LargerLocSel,2)=LocSelected(LargerLocSel,2)-180;
			 	LocUnSelected(LargerLocUnSel,2)=LocUnSelected(LargerLocUnSel,2)-180;
			 	LocSelected(SmallerLocSel,2)=LocSelected(SmallerLocSel,2)+180;
			 	LocUnSelected(SmallerLocUnSel,2)=LocUnSelected(SmallerLocUnSel,2)+180;
			 
			 
			 end
        	 end
        	
        end
        
        
        
        function ShowParts=getUsedType(obj)
        	%shows which parts of the catalogs are shown (decluster)

        	ShowParts=obj.TypeUsed;
        
        end
        
        
        function Declustered=checkCluster(obj)
        	%simply shows if any decluster data is existing
        	Declustered=~isempty(obj.EventType);
        
        end
        
        
        function setUsedType(obj,ShowParts)
        	%sets which types are used
        	obj.TypeUsed=ShowParts;
        	
        	updateObservers(obj);
        	
        	if isempty(obj.ShowUnclassified)
        		obj.ShowUnclassified=true;
        		
        	end
        	
        	updateObservers(obj);
        	
        end
        
        
        function [EventType ClusterNR] = getDeclusterData(obj)
        	%returns the declustering data from the datastore
        	
        	EventType=obj.EventType;
        	ClusterNR=obj.ClusterNR;
        	
        end
        
        
        
        
        function setDeclusterData(obj,EventType,ClusterNR,selected,ShowParts)
        	%sets the declustering data 
        	%ShowParts can be empty, in this case it will be set to true for
        	%all parts or will be kept if TypeUsed is already defined
        	if nargin<5
        		ShowParts=[];
        	end
        	
        	if isempty(obj.EventType)
        		%create new arrays with unclasified data
        		obj.EventType=zeros(obj.getRawRowCount,1);
        		obj.ClusterNR=NaN(obj.getRawRowCount,1);
        	
        	end
        	
        	
        	obj.EventType(selected)=EventType;
        	obj.ClusterNR(selected)=ClusterNR;
        	
        	if ~isempty(ShowParts)
        		obj.TypeUsed=ShowParts;
        		
        	elseif isempty(obj.TypeUsed)
        		obj.TypeUsed=true(3,1);
        	end
        	
        	if isempty(obj.ShowUnclassified)
        		obj.ShowUnclassified=true;
        		
        	end
        
        	updateObservers(obj);
        end
        
        
        function resetDeclusterData(obj)
        	%incase the declustering made a mess
        	%create new arrays with unclasified data
        	obj.EventType=zeros(obj.getRawRowCount,1);
        	obj.ClusterNR=NaN(obj.getRawRowCount,1);
        end
        
        
        
        function outData=getFieldNames(obj)
        	outData=ReturnFields(obj);
        		
        end
        
        
        
        function outData=getOriginalFieldNames(obj)
        	outData=obj.FieldNames;
        end
        
        
        
        function rowCount=getRowCount(obj)
        	
             if ~isempty(obj.EventType)
            	unclas=obj.EventType==0;
            	singleEq=obj.EventType==1;
            	mainEq=obj.EventType==2;
            	afterEq=obj.EventType==3;
            	declustSelect = (unclas&obj.ShowUnclassified)|...
            			(singleEq&obj.TypeUsed(1))|...
            			(mainEq&obj.TypeUsed(2))|...
            			(afterEq&obj.TypeUsed(3));
            else	
            	declustSelect=true(obj.getRawRowCount,1);
            	
            end
            
        	rowCount = sum(declustSelect);
        	
        end
        
        
        function DeclusSelect = getDeclustLogic(obj)
		   %returns a logic vector with the used events
		   if ~isempty(obj.EventType)
			unclas=obj.EventType==0;
			singleEq=obj.EventType==1;
			mainEq=obj.EventType==2;
			afterEq=obj.EventType==3;
			DeclusSelect = (unclas&obj.ShowUnclassified)|...
					(singleEq&obj.TypeUsed(1))|...
					(mainEq&obj.TypeUsed(2))|...
					(afterEq&obj.TypeUsed(3));
		    else	
			DeclusSelect=true(obj.getRawRowCount,1);
			
		    end
          
        end
        
        
        
        function rowCount=getRawRowCount(obj)
            rowCount = numel(obj.DataStruct.(obj.FieldNames{1}));
        end
        
        
        function [dSelected, dUnselected]=getFieldsClust(obj,fieldNames,rowIndices)
        	%wrap around function, it adds the decluster info to the returned 
        	%structure, sometimes handy
        	
        	%get the structure first
        	[dSelected, dUnselected]=getFields(obj,fieldNames,rowIndices)
        	
        	%add the data 
        	if ~isempty(obj.EventType)
        		unclas=obj.EventType==0;
        		singleEq=obj.EventType==1;
        		mainEq=obj.EventType==2;
        		afterEq=obj.EventType==3;
        		declustSelect = (unclas&obj.ShowUnclassified)|...
            				(singleEq&obj.TypeUsed(1))|...
            				(mainEq&obj.TypeUsed(2))|...
            				(afterEq&obj.TypeUsed(3));
            		if nargin==1
            			rowIndices = true(obj.getRawRowCount,1);
            		end
            		
            		if nargin<3 || ischar(rowIndices)
            			% If rowIndicies are unspecified then get all rows or
            			% rowIndices = 'all'
            			rowIndices = true(obj.getRowCount,1);
            		end
            		
            		
            		dSelected.dclust_EventType=obj.EventType(rowIndices&declustSelect);
            		dUnselected.dclust_EventType=obj.EventType(~rowIndices&declustSelect);
            		dSelected.dclust_ID=obj.ClusterNR(rowIndices&declustSelect);
            		dUnselected.dclust_ID=obj.ClusterNR(~rowIndices&declustSelect);
        	end
        
        end
        
        
        
        
        
        function [dSelected, dUnselected]=getFields(obj,fieldNames,rowIndices)
            
        
            %Apply routing if needed
            if ~isempty(obj.Routing)
            	TheStruct=obj.BuildDataStruct;
            else	
            	TheStruct=obj.DataStruct;
            	
            end
            
            
            if ~isempty(obj.EventType)
            	unclas=obj.EventType==0;
            	singleEq=obj.EventType==1;
            	mainEq=obj.EventType==2;
            	afterEq=obj.EventType==3;
            	declustSelect = (unclas&obj.ShowUnclassified)|...
            			(singleEq&obj.TypeUsed(1))|...
            			(mainEq&obj.TypeUsed(2))|...
            			(afterEq&obj.TypeUsed(3));
            			
            	%check if the rowIndices
		
            else	
            	declustSelect=true(obj.getRawRowCount,1);
            	
            end
            
            
            if nargin==1&isempty(obj.EventType)
                dSelected=TheStruct;
                dUnselected=[];
                return
                
            elseif nargin==1&~isempty(obj.EventType)
            	rowIndices=true(obj.getRawRowCount,1);
            	
            end
            
            if nargin<3 || ischar(rowIndices)
                % If rowIndicies are unspecified then get all rows or
                % rowIndices = 'all'
                rowIndices = true(obj.getRawRowCount(),1);
            end
            
            if nargin<2 || isempty(fieldNames)
                % If fieldNames are unspecified then get all fields
                %fieldNames = obj.FieldNames;
                fieldNames = fields(TheStruct);
            end
            
            if numel(rowIndices)~=numel(declustSelect);
            	temp=rowIndices;
            	rowIndices=false(obj.getRawRowCount,1);
            	rowIndices(declustSelect)=temp;
            end
            
            %Added for error if just a string as entry is used -> convert
            %to a cell array
            if isstr(fieldNames)
            	fieldNames={fieldNames};
            end
            
            dSelected = struct;
            dUnselected = struct;
            for i=1:numel(fieldNames)
                thisField = fieldNames{i};
                dataCol = TheStruct.(thisField);
                dSelected.(thisField) = dataCol(rowIndices&declustSelect);
                dUnselected.(thisField) = dataCol(not(rowIndices)&declustSelect);
            end            
        end                                     
        
        
         
        function [dSelected, dUnselected]=getFieldsNoCluster(obj,fieldNames,rowIndices)
            
        
            %Apply routing if needed
            if ~isempty(obj.Routing)
            	TheStruct=obj.BuildDataStruct;
            else	
            	TheStruct=obj.DataStruct;
            	
            end
            
            
           
           declustSelect=true(obj.getRawRowCount,1);
            	
                      
            
            if nargin==1&isempty(obj.EventType)
                dSelected=TheStruct;
                dUnselected=[];
                return
                
            elseif nargin==1&~isempty(obj.EventType)
            	rowIndices=true(obj.getRawRowCount,1);
            	
            end
            
            if nargin<3 || ischar(rowIndices)
                % If rowIndicies are unspecified then get all rows or
                % rowIndices = 'all'
                rowIndices = true(obj.getRawRowCount(),1);
            end
            
            if nargin<2 || isempty(fieldNames)
                % If fieldNames are unspecified then get all fields
                %fieldNames = obj.FieldNames;
                fieldNames = fields(TheStruct);
            end
            
            %Added for error if just a string as entry is used -> convert
            %to a cell array
            if isstr(fieldNames)
            	fieldNames={fieldNames};
            end
            
            dSelected = struct;
            dUnselected = struct;
            for i=1:numel(fieldNames)
                thisField = fieldNames{i};
                dataCol = TheStruct.(thisField);
                dSelected.(thisField) = dataCol(rowIndices&declustSelect);
                dUnselected.(thisField) = dataCol(not(rowIndices)&declustSelect);
            end            
        end                                     
        
        
        
        function [dSelected, dUnselected]=getRawFields(obj,fieldNames,rowIndices)
            %Same as the "normal getFields but it uses the unrouted Catalog instead 
            %This also not uses the decluster info
            
            
            %Go for the unrouted catalog
            TheStruct=obj.DataStruct;
            	

            if nargin==1
                dSelected=TheStruct;
                dUnselected=[];
                return
            end
            
            if nargin<3 || ischar(rowIndices)
                % If rowIndicies are unspecified then get all rows or
                % rowIndices = 'all'
                rowIndices = true(obj.getRowCount(),1);
            end
            
            if nargin<2 || isempty(fieldNames)
                % If fieldNames are unspecified then get all fields
                %fieldNames = obj.FieldNames;
                fieldNames = fields(TheStruct);
            end
            
            %Added for error if just a string as entry is used -> convert
            %to a cell array
            if isstr(fieldNames)
            	fieldNames={fieldNames};
            end
            
            dSelected = struct;
            dUnselected = struct;
            for i=1:numel(fieldNames)
                thisField = fieldNames{i};
                dataCol = TheStruct.(thisField);
                dSelected.(thisField) = dataCol(rowIndices);
                dUnselected.(thisField) = dataCol(not(rowIndices));
            end            
        end                                     
        
        
        
        % Getter methods for UserData        
        function dataVal=getUserData(obj,key)
            % Get a previously saved data value from the UserData
            % It is the responsibility of the caller to check that the key
            % exists
            dataVal=obj.UserData.lookup(key);
            
            if any(strcmp(key,obj.NumberedUserData))
            	%filtering needed
            	DeclusSelect = getDeclustLogic(obj);
            	dataVal=dataVal(DeclusSelect);
            end
            
        end
        
        
        
        function keys=getUserDataKeys(obj)
            % Get the list of data keys from the assocArray
            keys=obj.UserData.getKeys();
        end
        
        
        
        
        % Setter methods
        function setData(obj,newData)
            % Set the underlying table of events
            obj.DataStruct = newData;
            
            %reset random ID
            obj.RandomID=rand;
            
            % Tell the observers to update
            updateObservers(obj);
        end   
        
        
        
        
        % Setter methods for UserData
        function setUserData(obj,key,value)
            % Set the UserData attribute of the dataStore
            obj.UserData.set(key,value);
            
            %reset Random ID
            obj.RandomID=rand;
            
        end
        
        
        
        
        function subscribe(obj, viewName, callbackFcn)
            % Subscribe the view obs to the Event Store object
            addlistener(obj,'Update',@(s,e) callbackFcn());
        end
    	
        
        
        
    	%Buffer the Dates 
    	function bufferDate(obj)
    		import mapseis.util.decyear;
    		
    		%Date Buffer
			dateNumVect = obj.getFieldsNoCluster({'dateNum'}).dateNum;
			obj.setUserData('Month', str2num(datestr(dateNumVect,'mm')));
			obj.setUserData('Day', str2num(datestr(dateNumVect,'dd')));
			obj.setUserData('Hour', str2num(datestr(dateNumVect,'HH')));
			obj.setUserData('Minute', str2num(datestr(dateNumVect,'MM')));
			obj.setUserData('Second', str2num(datestr(dateNumVect,'SS')));
			obj.setUserData('MilliSecond', str2num(datestr(dateNumVect,'FFF')));
			obj.setUserData('DecYear', decyear(datevec(dateNumVect)));
		
		obj.NumberedUserData=union(obj.NumberedUserData,{'Month','Day','Hour','Minute','Second','MilliSecond','DecYear'});	
			
	end

	
	
	
	%sets the quality according to the number of events
	function SetPlotQuality(obj)
		rownumber=obj.getRowCount;
		
		if rownumber <= 10000
			obj.setUserData('PlotQuality','hi');
		elseif rownumber <= 25000
			obj.setUserData('PlotQuality','med');
		else
			obj.setUserData('PlotQuality','low');
		end		
			
		
	
	end
	

	
	
    	function CutCatalog(obj,FilterIndex)
    		%uses a logical index from filters to cut down the catalog
    		
    		
    		
    		
    		%the buffers
    		hours = obj.getUserData('Hour');
    		minutes = obj.getUserData('Minute');
    		years = obj.getUserData('DecYear');
    		    		
    		%cut and set (
    		if numel(years)~=sum(FilterIndex)
    			disp('Cut User Data')
    			obj.setUserData('Hour', hours(FilterIndex));	
    			obj.setUserData('Minute', minutes(FilterIndex));
    			obj.setUserData('DecYear', years(FilterIndex));
    		else
    			%cutting already done by setUserData
    			%obj.setUserData('Hour', hours);	
    			%obj.setUserData('Minute', minutes);
    			%obj.setUserData('DecYear', years);
    			disp('User Data already cutted')
    		end
    		
    		
    		
    		try
    			second = obj.getUserData('Second');
    			month = obj.getUserData('Month');
    			day = obj.getUserData('Day');
    			
    			if numel(second)~=sum(FilterIndex)
				obj.setUserData('Second', second(FilterIndex));	
				obj.setUserData('Month', month(FilterIndex));
				obj.setUserData('Day', day(FilterIndex));
			else
				%cutting already done by setUserData
				%obj.setUserData('Second', second);	
				%obj.setUserData('Month', month);
				%obj.setUserData('Day', day);
			
			end
    		catch
    			disp('no seconds, months and days in the buffer')
    		end
    		
    		%the main data
    		dataCols = obj.getRawFields([],FilterIndex);
    		obj.DataStruct = dataCols;
    		
    		%change quality if needed
    		obj.SetPlotQuality;
    		
    		%set buffers of routing
		obj.BufferRouting('all');
    		
    		%reset Random ID
    		obj.RandomID=rand;
    		
    		if ~isempty(obj.EventType)
    			%cut the EventType and clusterNR
    			obj.EventType=obj.EventType(FilterIndex);
    			obj.ClusterNR=obj.ClusterNR(FilterIndex);
    		end
    		
    		
    		%update the observer
    		updateObservers(obj);
    		
    	end
    	
    	
    	function ApplyDecluster(obj,FilterIndex)
    		%This applies the declustering and kicks out all not used events
    		
    		if nargin==1
    			FilterIndex=true(obj.getRowCount,1);
    		end
    		
    		if ~isempty(obj.EventType)
        		unclas=obj.EventType==0;
        		singleEq=obj.EventType==1;
        		mainEq=obj.EventType==2;
        		afterEq=obj.EventType==3;
        		declustSelect = (unclas&obj.ShowUnclassified)|...
            				(singleEq&obj.TypeUsed(1))|...
            				(mainEq&obj.TypeUsed(2))|...
            				(afterEq&obj.TypeUsed(3));
            		obj.CutCatalog(	declustSelect&FilterIndex);	
            				
    		end

    	end
    	
    	
    	
    	function ReplaceInternalData(obj,eventStruct);
    		%this function will replace the internal DataStruct with the new 
    		%data and set the buffers new. It is like 
    		%creating a new datastore.
    		%The Routing will be used in this one
    		
    		import mapseis.datastore.assocArray;
    		
    		allfields=fieldnames(eventStruct)
    		
    		%go throught the fields, get the actuall field and convert data if needed.
    		for i=1:numel(allfields)
    			[orgField TheData]=LookupField(obj,eventStruct.(allfields{i}),allfields{i})
    			CorrectStruct.(orgField)=TheData;
    		end
    		
    		obj.DataStruct=CorrectStruct;
    		obj.FieldNames = fieldnames(obj.DataStruct);
    		
    		%reset UserData
    		%obj.UserData = assocArray();
    		
    		%set buffers
    		obj.bufferDate;
    		
    		%change quality if needed
    		obj.SetPlotQuality;
    		
    		%set buffers of routing
		obj.BufferRouting('all');
    		
    		%reset Random ID
    		obj.RandomID=rand;
    		
    		%update the observer
    		updateObservers(obj);
    	
    	end
    	
    	
    	function ReplaceInternalRawData(obj,eventStruct);
    		%Similar to the normal Replace Internal Data but it does not
    		%use the routing, and does not change it either.
    		
    		import mapseis.datastore.assocArray;
    		obj.DataStruct=eventStruct;
    		obj.FieldNames = fieldnames(obj.DataStruct);
    		
    		%reset UserData
    		%obj.UserData = assocArray();
    		
    		%set buffers
    		obj.bufferDate;
    		
    		%change quality if needed
    		obj.SetPlotQuality;
    		
    		%set buffers of routing
		obj.BufferRouting('all');
    		
    		%reset Random ID
    		obj.RandomID=rand;
    		
    		%update the observer
    		updateObservers(obj);
    	
    	end
    	
    	
   	
    	
    	
    	function EditFilteredData(obj,eventStruct,LogIndex,NumberIndex);
    		%get fields from input
    		%The NumberIndex will be used to fill the holes in the new data if specified
    		%the number of coulomb has to be equal to the eventStructs (for each event an index)
    		if nargin<4
    			NumberIndex=[];
    		end
    		
    		
    		
    		FieldNames = fieldnames(eventStruct);
    		rowCount = numel(obj.DataStruct.(obj.FieldNames{1}));
    		
    		
    		if ~isempty(obj.EventType)
			unclas=obj.EventType==0;
			singleEq=obj.EventType==1;
			mainEq=obj.EventType==2;
			afterEq=obj.EventType==3;
			declustSelect = (unclas&obj.ShowUnclassified)|...
					(singleEq&obj.TypeUsed(1))|...
					(mainEq&obj.TypeUsed(2))|...
					(afterEq&obj.TypeUsed(3));
		else	
			declustSelect=true(obj.getRowCount,1);
			
		end
    		
		LogIndex=LogIndex&declustSelect;
    		
    		if sum(LogIndex)==numel(eventStruct.(FieldNames{1}))
			%go through all the fields
			for i=1:numel(FieldNames)
				%look up the fields
				
				[orgField TheData]=LookupField(obj,eventStruct.(FieldNames{i}),FieldNames{i});
				
				if ~isempty(orgField)
					%might be unnecessary as non existing field returns
					%a empty orgField
					if isfield(obj.DataStruct,orgField)
						obj.DataStruct.(orgField)(LogIndex)=TheData;
					else
						%build new field
						obj.DataStruct.(orgField)=ones(rowCount,1)*NaN;
						obj.DataStruct.(orgField)(LogIndex)=TheData;
					end
				else
					%build new field
					obj.DataStruct.(FieldNames{i})=ones(rowCount,1)*NaN;
					obj.DataStruct.(FieldNames{i})(LogIndex)=TheData;
				
				end
					
			end
    		
		else
			%rework for routing!!!!!!!!!!!!!!!!!!!! -> finished
			
    			%there are some events more or some are missing, erease the block
    			%and add new
    			totalnumber=rowCount-numel(eventStruct.(FieldNames{1}))+sum(LogIndex);
    			numberlasting=rowCount-numel(eventStruct.(FieldNames{1}));
    			
    			oldFieldNames=obj.ReturnFields;
    			
    			%olddata
    			oldData=obj.getFields;
    			
    			%not existing fields in one or the other datastructure
    			newinold=ismember(oldFieldNames,FieldNames);
    			Fields_nio=oldFieldNames(~newinold);
    			oldinnew=ismember(FieldNames,oldFieldNames);
    			Fields_oin=FieldNames(~oldinnew);
    			
    			allFields=unique([FieldNames;oldFieldNames]);
    			
    			
    			%build unchange events
    			for i=1:numel(oldFieldNames)
    				unchanged.(oldFieldNames{i})=oldData.(oldFieldNames{i})(~LogIndex);
    			end
    			
    			%build NaNs for not existing new fields
    			for i=1:numel(Fields_oin)
    				if ~iscell(eventStruct.(Fields_oin{i}))
    					unchanged.(Fields_oin{i})=ones(numberlasting,1)*NaN;
    				else
    					unchanged.(Fields_oin{i})=cell(numberlasting,1);
    				end
    					
    			end
    			
    			%build NaNs for not existing old fields in eventStruct
    			%build NaNs for not existing new fields
    			if isempty(NumberIndex)
				for i=1:numel(Fields_nio)
					if ~iscell(obj.DataStruct.(Fields_nio{i}))
						eventStruct.(Fields_nio{i})=ones(sum(LogIndex),1)*NaN;
					else
						eventStruct.(Fields_nio{i})=cell(sum(LogIndex),1);
					end
						
				end
    			else
    				%index is existing, so just take the values from the old fields
    				for i=1:numel(Fields_nio)
    					eventStruct.(Fields_nio{i})=oldData.(Fields_nio{i})(NumberIndex);	
    				end
    			end
    			%build new set
    			for i=1:numel(allFields)
    				newDatastruct.(allFields{i})=[unchanged.(allFields{i});eventStruct.(allFields{i})];
    			end
    			
    			%almost done, just a rearrangement of the sort order is needed
    			[sorted newsort]=sort(newDatastruct.dateNum)
    			
    			for i=1:numel(allFields)
    				newDatastruct.(allFields{i})=newDatastruct.(allFields{i})(newsort);
    			end
    			
    			%rename and use convertes if needed
    			%This has to be done in seperate loop because of the sorting (The field for dateNum may be not
    			%always called so in the "real" data).
    			if ~isempty(obj.Routing)
				for i=1:numel(allFields)
					%get corrected data and Field
					[orgField TheData]=obj.LookupField(obj,newDatastruct.(allFields{i}),allFields{i})
					
					%remove old field
					newDataStruct=rmfield(newDataStruct,allFields{i});
					
					%add new one
					newDataStruct.(orgField)=TheData;
				end
    			end
    			%..and finally the finish
    			obj.DataStruct=newDatastruct;
    			
    			
    			
    		end	
    		
    		%reapply buffers
    		obj.bufferDate;
    		
    		%rebuild fieldnames
    		obj.FieldNames = fieldnames(obj.DataStruct);
    		
    		%reset Random ID
    		obj.RandomID=rand;
    		
    		%set buffers of routing
		obj.BufferRouting('all');
    		
    		%check if numbered user data is existing
    		%(NOT YET IMPLEMENTED, NOT KNOWN IF NEEDED)
    		
    		
    		%update the observer
    		updateObservers(obj);
    	end
    	
    
    	
    	function ResetRandID(obj)
    		%forces a new Random ID
    		obj.RandomID=rand;
    	
    	end
    	
    	
    	
    	function RandID=GiveID(obj)
    		%Returns the current random ID
    		%reset Random ID
    		RandID=obj.RandomID;
	end
	
	
	
	function setRouting(obj,RoutingEntry)
		%allows to set the fields of the catalog to the default fields of
		%MapSeis, incase no routing is set, the normal fields will be used.
		%It is also possible to use convertion filter for the different fields
		%e.g. Coordinate transformations.
		%The Routing Entry has to be a cell array with the following rows
		%{MapSeisFieldName, CatalogField, @TransformFilter}, the TransformFilter handle
		%can be empty, in this case no converter will be used for this field. The transformfilter
		%itself needs to have two input variable, first one for the data, the second for the 
		%direction setting, 'Catalog2Map' will convert from the Catalog to the Mapseis variable and
		%'Map2Catalog' in opposite direction.
		%To erease a routing, just set the Catalogfield to empty array;
		
		if isempty(RoutingEntry)
			obj.RouterRepair('reset');
			disp('Routing Reseted');
			return
		end
		
		for i=1:numel(RoutingEntry(:,1))
			%go through all the lines
			if ~isempty(obj.Routing)
				%check if existing
				exstRout=strcmp(obj.Routing(:,1),RoutingEntry{i,1});
				
				if sum(exstRout)>0
					%was already set once, reset it
					obj.Routing(exstRout,2)=RoutingEntry(i,2);
					obj.Routing(exstRout,3)=RoutingEntry(i,3);
					
				else
					%not existing build new entry
					obj.Routing(end+1,2)=RoutingEntry(i,2);
					obj.Routing(end,3)=RoutingEntry(i,3);
					obj.Routing(end,1)=RoutingEntry(i,1);
				end
			else
				%nothing exist, so just make the first entry	
				obj.Routing{1,2}=RoutingEntry{i,2};
				obj.Routing{1,3}=RoutingEntry{i,3};
				obj.Routing{1,1}=RoutingEntry{i,1};
	
			end
			
		
		end
		
		%clean it
		obj.RouterRepair('clean');
		
		%set buffers
		obj.BufferRouting('all');
		
	end
	
	
	
	function RouterTable=getRouting(obj)
		%gives the existing routing of the RoutingTable, without the field handle
		if ~isempty(obj.Routing)
			RouterTable=obj.Routing;
		else
			RouterTable={};
		end	
	end
	
	
	
	function RouterRepair(obj,whatTo)
		%This function contains some functions allowing to reset and repair the 
		%routing table, may not needed but who knows, they just should be existing
		import mapseis.datastore.emptier;
		
		
		switch whatTo
			case 'reset'
				%empty the RoutingTable
				obj.Routing={};
			case 'clean'
				%kicks out all routes which are not set anymore
				emptyRoute=emptier(obj.Routing(:,2));
				obj.Routing=obj.Routing(~emptyRoute,:);
				

		end
	
	
	end
	
	
	
	
	function RoutedStruct=BuildDataStruct(obj)
		%This function applies the routing to DataStruct
		
		RoutedStruct=obj.DataStruct;
		
		for i=1:numel(obj.Routing(:,1))
			if ~isempty(obj.Routing{i,2})
				if isempty(obj.Routing{i,3})
					RoutedStruct.(obj.Routing{i,1})=obj.DataStruct.(obj.Routing{i,2});
				else
					%try if a buffer is available
					buffername=['buffer_',obj.Routing{i,1}];
					try 
						RoutedStruct.(obj.Routing{i,1})=obj.getUserData(buffername);
					catch
					
						RoutedStruct.(obj.Routing{i,1})=obj.Routing{i,3}(...
								obj.DataStruct.(obj.Routing{i,2}),'Catalog2Map');
					        obj.setUserData(buffername,RoutedStruct.(obj.Routing{i,1}));
					end			
				end	
				
				%remove other field if not the same name, to prevent problems with filtercuts
				if ~strcmp(obj.Routing{i,2},obj.Routing{i,1})
					RoutedStruct=rmfield(RoutedStruct,obj.Routing{i,2});
				end	
			end
			
		end
		
	end
	
	
	function BufferRouting(obj,whichField)
		%convertes data and write it into userdata, only done with fields needing a converter
		
		
		if ~isempty(obj.Routing)
		
			if strcmp(whichField,'all')
				%buffer all fields
						
				for i=1:numel(obj.Routing(:,1))
					if ~isempty(obj.Routing{i,2})
						if ~isempty(obj.Routing{i,3})
							%try if a buffer is available
							buffername=['buffer_',obj.Routing{i,1}];
							
								Convertedfield=obj.Routing{i,3}(...
										obj.DataStruct.(obj.Routing{i,2}),'Catalog2Map');
								obj.setUserData(buffername,Convertedfield);
									
						end	
					
					end
				
				end	
			
			
			else
				foundEntry=strcmp(obj.Routing(:,1),whichField);
				
				if any(foundEntry)
					if ~isempty(obj.Routing{foundEntry,2})
						if ~isempty(obj.Routing{foundEntry,3})
							%try if a buffer is available
							buffername=['buffer_',obj.Routing{foundEntry,1}];
							
							Convertedfield=obj.Routing{foundEntry,3}(...
									obj.DataStruct.(obj.Routing{foundEntry,2}),'Catalog2Map');
							obj.setUserData(buffername,Convertedfield);
									
						end	
					
					end
				
				end
			
			end
		
		end
	end
	
	
	function [orgField TheData]=LookupField(obj,NewData,TheField)
		%The function checks if the given field is part of the routing table
		%replaces it if necessary and converts data if necessary
		%And returns it back to the caller, this has to be done because, often a
		%logical index is used with the data;
		orgField=TheField;
		TheData=NewData;
		
		if ~isempty(obj.Routing)
			%Check if the field is in Routing
			foundIn=strcmp(obj.Routing(:,1),TheField);
			%The orgField
			if sum(foundIn)>0
				orgField=obj.Routing{foundIn,2};

			end
			
			if sum(foundIn)>0 & ~isempty(obj.Routing{foundIn,3})
				TheData=obj.Routing{foundIn,3}(NewData,'Map2Catalog');
		
			end	
		
		end
	
	end


	
	function correctFieldList=ReturnFields(obj)
		%The function returns all the correct fields of the Datastruct with applied Routing
		
		if ~isempty(obj.Routing)
			rawList=obj.FieldNames;
			FieldsReplace=obj.Routing(:,2);
			[tf loc] = ismember(FieldsReplace,rawList);
			
			select=logical(ones(numel(rawList),1));
			select(loc)=false;
			
			correctFieldList=[rawList(select);obj.Routing(:,1)];
			
		else
			correctFieldList=obj.FieldNames;
		end
		
		
	end	
	
    end
    
   
    
    
    
    %% Private Methods
    methods (Access = 'private')
        function updateObservers(obj)
            % Notify the observers of this object that a change has
            % occurred
            notify(obj,'Update');
        end
    end
end
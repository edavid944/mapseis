classdef FilteredDataStore < handle
    % filteredDataStore : object to access imported data and filter
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 72 $    $Date: 2008-10-14 18:03:12 +0100 (Tue, 14 Oct 2008) $
    % Author: Matt McDonnell
    
    %% Properties
    properties
        DataStruct
        FieldNames
        FilterStruct
    end
    
    
    %% Initialise the data store from an input struct
    % The data is a 2D table with individual data points making up the rows of
    % the table and the columns being named by the parameters of the data.  We
    % want to be able to access a subset of the total data by specifying a
    % vector of row indices (or logical indices), with these indices being
    % calculated from the data parameters.
    
    methods
        function obj = FilteredDataStore(varargin)
            
            try
                switch nargin
                    case 1
                        % Input must be a struct
                        assert(isstruct(varargin{1}));
                        obj.DataStruct = varargin{1};
                end
            catch ME
                error('filteredDataStore:unknown_constructor_arg',...
                    'FilteredDataStore : unknown input argument');
            end
            
            
            %% Filter struct
            % Store the named filters in a struct
            % filterStruct.(filterName) = filter logical index column vector
            
            obj.FilterStruct = struct; % Start with an empty filter array
            
            obj.FieldNames = fieldnames(obj.DataStruct);
            
        end
        %% Public methods
        
        function fldNames = getFields(obj)
            fldNames = obj.FieldNames;
        end
        
        function [dSelected, dUnselected]=getData(obj,filterName)
            % getData : returns the data filtered by the named filter or all
            % data if filter is unspecified
            
            try
                if 1==nargin
                    dSelected=obj.DataStruct;
                    dUnselected=[];
                else
                    dSelected = struct;
                    dUnselected = struct;
                    for i=1:numel(obj.FieldNames)
                        dataCol = obj.DataStruct.(obj.FieldNames{i});
                        dSelected.(obj.FieldNames{i}) = dataCol(obj.FilterStruct.(filterName));
                        dUnselected.(obj.FieldNames{i}) = dataCol(not(obj.FilterStruct.(filterName)));
                    end
                end
            catch ME
                dSelected=obj.DataStruct;
                dUnselected=[];
                warning('filteredDataStore:nGetData_unknown_filter',...
                    ['Unknown filter:',filterName,'.  Returning all data']);
            end
        end
        
        function setData(obj,newData)
            % setData : set the data to a new struct, clear all of the filters
            obj.DataStruct = newData;
            obj.FilterStruct = struct;
            obj.FieldNames = fieldnames(newData);
        end
        
        function setFilter(obj,filterName,filterFun,combArg)
            % Create a new filter named filterName that tests the field
            % fieldName with function testFun.  The optional combArg combines
            % the new filter with the existing filter using 'and' or 'or'.

            try
                % Calculate the filter logical index array (ie selected rows)
                logIndVect = filterFun(obj.DataStruct);
                if nargin<4
                    combArg = 'replace';
                end
                nnCombineFilters(lower(combArg));
            catch ME
                warning('filteredDataStore:nSetFilter_unknown_combination_mode',...
                    ['filteredDataStore:Unable to set filter ',lasterr]);
            end
            
            %% Private methods
            function nnCombineFilters(nCombArg)
                % Combine the new filter with the existing filter if any
                if ~isfield(obj.FilterStruct,filterName) || strcmp(nCombArg,'replace')
                    obj.FilterStruct.(filterName) = logIndVect;
                elseif strcmp(nCombArg,'and')
                    obj.FilterStruct.(filterName) = ...
                        obj.FilterStruct.(filterName) & logIndVect;
                elseif strcmp(nCombArg,'or')
                    obj.FilterStruct.(filterName) = ...
                        obj.FilterStruct.(filterName) | logIndVect;
                else
                    warning('filteredDataStore:nSetFilter_unknown_combination_mode',...
                        'filteredDataStore:Unable to set filter, unknown combination argument');
                end
            end
        end
        
    end
end
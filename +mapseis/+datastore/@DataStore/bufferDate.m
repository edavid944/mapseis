function bufferDate(obj)
	%buffers thes different part of the data calculated from the datenum format.
	%This allows to save a big chunk of computing time, because the datevec function
	%is very cpu hungry, by buffering all the datas the processing speed can be increased  
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	import mapseis.util.decyear;
	
	%Date Buffer
	dateNumVect = obj.getFieldsNoCluster({'dateNum'}).dateNum;
	obj.setUserData('Month', str2num(datestr(dateNumVect,'mm')));
	obj.setUserData('Day', str2num(datestr(dateNumVect,'dd')));
	obj.setUserData('Hour', str2num(datestr(dateNumVect,'HH')));
	obj.setUserData('Minute', str2num(datestr(dateNumVect,'MM')));
	obj.setUserData('Second', str2num(datestr(dateNumVect,'SS')));
	obj.setUserData('MilliSecond', str2num(datestr(dateNumVect,'FFF')));
	obj.setUserData('DecYear', decyear(datevec(dateNumVect)));
	
	obj.NumberedUserData=union(obj.NumberedUserData,{'Month','Day','Hour','Minute','Second','MilliSecond','DecYear'});	

end
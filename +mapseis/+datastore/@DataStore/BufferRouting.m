function BufferRouting(obj,whichField)
	%convertes data and write it into userdata, only done with fields needing a converter
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	if ~isempty(obj.Routing)
		
		if strcmp(whichField,'all')
			%buffer all fields
			
			for i=1:numel(obj.Routing(:,1))
				if ~isempty(obj.Routing{i,2})
					if ~isempty(obj.Routing{i,3})
						%try if a buffer is available
						buffername=['buffer_',obj.Routing{i,1}];
						
						Convertedfield=obj.Routing{i,3}(...
						obj.DataStruct.(obj.Routing{i,2}),'Catalog2Map');
						obj.setUserData(buffername,Convertedfield);
					end	
				end
			end	
			
		else
			foundEntry=strcmp(obj.Routing(:,1),whichField);
			
			if any(foundEntry)
				if ~isempty(obj.Routing{foundEntry,2})
					if ~isempty(obj.Routing{foundEntry,3})
						%try if a buffer is available
						buffername=['buffer_',obj.Routing{foundEntry,1}];
						
						Convertedfield=obj.Routing{foundEntry,3}(...
						obj.DataStruct.(obj.Routing{foundEntry,2}),'Catalog2Map');
						obj.setUserData(buffername,Convertedfield);
						
					end	
				end		
			end
		end
		
	end
end
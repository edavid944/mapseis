function correctFieldList=ReturnFields(obj)
	%The function returns all the correct fields of the Datastruct with applied Routing
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	if ~isempty(obj.Routing)
		rawList=obj.FieldNames;
		FieldsReplace=obj.Routing(:,2);
		[tf loc] = ismember(FieldsReplace,rawList);
		
		select=logical(ones(numel(rawList),1));
		select(loc)=false;
		
		correctFieldList=[rawList(select);obj.Routing(:,1)];
		
	else
		correctFieldList=obj.FieldNames;
	end
	
end	
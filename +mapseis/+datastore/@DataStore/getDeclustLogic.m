function DeclusSelect = getDeclustLogic(obj)
	%returns a logic vector with the used events

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	if ~isempty(obj.EventType)
		unclas=obj.EventType==0;
		singleEq=obj.EventType==1;
		mainEq=obj.EventType==2;
		afterEq=obj.EventType==3;
		DeclusSelect = (unclas&obj.ShowUnclassified)|...
		(singleEq&obj.TypeUsed(1))|...
		(mainEq&obj.TypeUsed(2))|...
		(afterEq&obj.TypeUsed(3));
	else	
		DeclusSelect=true(obj.getRawRowCount,1);
	end

end
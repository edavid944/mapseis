function [dSelected, dUnselected]=getFieldsNoCluster(obj,fieldNames,rowIndices)
	% similar to getFields but ignores the declustering informations.
	% see getFields for the argument infos
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	%Apply routing if needed
	if ~isempty(obj.Routing)
		TheStruct=obj.BuildDataStruct;
	else	
		TheStruct=obj.DataStruct;
	
	end
	
	
	
	declustSelect=true(obj.getRawRowCount,1);
	
	
	
	if nargin==1&isempty(obj.EventType)
		dSelected=TheStruct;
		dUnselected=[];
		return
	
	elseif nargin==1&~isempty(obj.EventType)
		rowIndices=true(obj.getRawRowCount,1);
	
	end
	
	if nargin<3 || ischar(rowIndices)
		% If rowIndicies are unspecified then get all rows or
		% rowIndices = 'all'
		rowIndices = true(obj.getRawRowCount(),1);
	end
	
	if nargin<2 || isempty(fieldNames)
		% If fieldNames are unspecified then get all fields
		%fieldNames = obj.FieldNames;
		fieldNames = fields(TheStruct);
	end
	
	%Added for error if just a string as entry is used -> convert
	%to a cell array
	if isstr(fieldNames)
		fieldNames={fieldNames};
	end
	
	dSelected = struct;
	dUnselected = struct;
	for i=1:numel(fieldNames)
		thisField = fieldNames{i};
		dataCol = TheStruct.(thisField);
		dSelected.(thisField) = dataCol(rowIndices&declustSelect);
		dUnselected.(thisField) = dataCol(not(rowIndices)&declustSelect);
	end            
end
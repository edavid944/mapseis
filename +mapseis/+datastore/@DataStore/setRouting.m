function setRouting(obj,RoutingEntry)
	%allows to set the fields of the catalog to the default fields of
	%MapSeis, incase no routing is set, the normal fields will be used.
	%It is also possible to use convertion filter for the different fields
	%e.g. Coordinate transformations.
	%The Routing Entry has to be a cell array with the following rows
	%{MapSeisFieldName, CatalogField, @TransformFilter}, the TransformFilter handle
	%can be empty, in this case no converter will be used for this field. The transformfilter
	%itself needs to have two input variable, first one for the data, the second for the 
	%direction setting, 'Catalog2Map' will convert from the Catalog to the Mapseis variable and
	%'Map2Catalog' in opposite direction.
	%To erease a routing, just set the Catalogfield to empty array;


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	

	if isempty(RoutingEntry)
		obj.RouterRepair('reset');
		disp('Routing Reseted');
		return
	end
	
	for i=1:numel(RoutingEntry(:,1))
		%go through all the lines
		if ~isempty(obj.Routing)
			%check if existing
			exstRout=strcmp(obj.Routing(:,1),RoutingEntry{i,1});
		
			if sum(exstRout)>0
				%was already set once, reset it
				obj.Routing(exstRout,2)=RoutingEntry(i,2);
				obj.Routing(exstRout,3)=RoutingEntry(i,3);
			
			else
				%not existing build new entry
				obj.Routing(end+1,2)=RoutingEntry(i,2);
				obj.Routing(end,3)=RoutingEntry(i,3);
				obj.Routing(end,1)=RoutingEntry(i,1);
			end
		else
			%nothing exist, so just make the first entry	
			obj.Routing{1,2}=RoutingEntry{i,2};
			obj.Routing{1,3}=RoutingEntry{i,3};
			obj.Routing{1,1}=RoutingEntry{i,1};
			
		end
		
		
	end
	
	%clean it
	obj.RouterRepair('clean');
	
	%set buffers
	obj.BufferRouting('all');

end
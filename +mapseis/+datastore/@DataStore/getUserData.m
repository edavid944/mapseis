function dataVal=getUserData(obj,key)
	% Get a previously saved data value from the UserData
	% It is the responsibility of the caller to check that the key
	% exists (use getUserDataKeys)
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	dataVal=obj.UserData.lookup(key);
	
	if any(strcmp(key,obj.NumberedUserData))
		%filtering needed
		DeclusSelect = getDeclustLogic(obj);
		dataVal=dataVal(DeclusSelect);
	end

end
function RoutedStruct=BuildDataStruct(obj)
	%This function applies the routing to DataStruct
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	RoutedStruct=obj.DataStruct;
	
	for i=1:numel(obj.Routing(:,1))
		if ~isempty(obj.Routing{i,2})
			if isempty(obj.Routing{i,3})
				RoutedStruct.(obj.Routing{i,1})=obj.DataStruct.(obj.Routing{i,2});
			else
				%try if a buffer is available
				buffername=['buffer_',obj.Routing{i,1}];
				try 
					RoutedStruct.(obj.Routing{i,1})=obj.getUserData(buffername);
				catch
					
					RoutedStruct.(obj.Routing{i,1})=obj.Routing{i,3}(...
					obj.DataStruct.(obj.Routing{i,2}),'Catalog2Map');
					obj.setUserData(buffername,RoutedStruct.(obj.Routing{i,1}));
				end			
			end	
		
			%remove other field if not the same name, to prevent problems with filtercuts
			if ~strcmp(obj.Routing{i,2},obj.Routing{i,1})
				RoutedStruct=rmfield(RoutedStruct,obj.Routing{i,2});
			end	
		end
	
	end

end
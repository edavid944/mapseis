function [LonOff LatOff]=getLocationOffset(obj)
	%returns the Offsets, also sets them to 0 if not set before
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	if isempty(obj.LonOffset)
		obj.LonOffset=0;
	end
	
	if isempty(obj.LatOffset)
		obj.LatOffset=0;
	end
	
	LonOff=obj.LonOffset;
	LatOff=obj.LatOffset;
	
end
classdef DataStore < handle
	% DataStore : Central repository for MapSeis event data
	% The DataStore object is initialised by a struct produced by importing
	% data from an earthquake catalog using Import(xxxx)Catalog
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	
	%Version 1.1: 	New is the Routing which allows to set the fields of the catalog
	%			freely to the default fields of mapseis, this allows to change the
	%			default fields anytime wanted, an example could be the useage of different
	%			magnitudes 
	%			The routing is momentary in experimental state, some changes may be added 
	%			later if needed.
	
	%Version 1.2:	Support for location offsets, this allows to shift lon and lat by a certain
	%			deg, it is usefull in case the region of interest has a shift (e.g. Lon 180 to -180) 
	%			in it.
	
	%Version 1.3:	Support for decluster algorithmen. 
	%				EventType:	 gives the type determined by the decluster algorithmen
	%						(1:single event, 2: mainshock, 3:aftershock and 0: unclassified)
	%				ClusterNR:	sets the number of the cluster a event belongs to
	%						is NaN in case of a single event
	%				TypeUsed:	is a 3 element vector and sets which eventtypes are
	%						used in the datastore [singleToggle mainshockToggle aftershockToggle]
	%				DeclusterMetaData: is an optional field and is used to store additional info of the 
	%						   decluster algorithmen
	%Note: 	the declustering should work, but still is a bit messy, it is possible, that the system will change later to 
	%		an external filtering.
	
	%Version 1.4 Repacking into folder structure
	
	%% Attributes
	properties (GetAccess = 'public', SetAccess = 'public')
		FieldNames
		DataStruct % Property holding the main application data
		UserData % User-specific settings eg filter settings etc
		Routing % sets the needed fields
		NumberedUserData %has to be filled with UserData names which contain data which is
		%available for each catalog entry (those events will be automatically
		%filtered incase filtered data is edited)		 
		LonOffset
		LatOffset
		EventType	%for decluster info
		ClusterNR	%for decluster info
		TypeUsed	%for decluster info
		
	end
	
	properties %the not private part
		RandomID %will be set different each time the datastore is modified
		Version
		UseShift %not functional in the modul itself more like a marker
		ShowUnclassified
		DeclusterMetaData %Optional is used in the decluster algorithmen
	end
	
	events
		Update
	end
	
	methods (Access='public')
		function obj=DataStore(varargin)
			import mapseis.datastore.assocArray;
			
			switch nargin
				case 1
					% Argument is a structure
					obj.DataStruct = varargin{1};
					obj.FieldNames = fieldnames(obj.DataStruct);
				case 2
					% Load the data store from a file.  First argument is the filename,
					% second is either a function handle to the importer function or
					% '-load' to load from a .mat file
					if isa(varargin{2},'function_handle')
						obj.innerFunStruct = varargin{2}(varargin{1}{:});
					else
						obj.innerFunStruct = load(fname);
					end
			end
			
			% Create an associative array to store UserData associated with a set of
			% events.  This can be used to store GUI-specific settings such as grid
			% spacing, selection radius etc.
			% NB : this UserData will depend on the specific GUI configuration and
			% hence loading a DataStore structure that has been saved from one GUI
			% setup into another GUI setup will fail unless the GUIs are designed to
			% use default parameters if they can't read them from the DataStore.
			obj.NumberedUserData={};
			obj.UserData = assocArray();
			
			%set empty routing
			obj.Routing={};
			
			%set offset to 0
			obj.LonOffset=0;
			obj.LatOffset=0;
			obj.UseShift=false;
			
			%the declusterfields
			obj.EventType=[];
			obj.ClusterNR=[];
			obj.TypeUsed=[];
			obj.ShowUnclassified=true;
			obj.DeclusterMetaData=[];
			
			%Version, existing datastore files will not have a version id or an other
			%number if they created by a older version of Datastore, this allows to use
			%a converter to transform old datastores in new datastores.
			obj.Version=1.4;
						
			obj.RandomID=rand;
		
		end

		
		
		%external file methods
		
		InterData = SpitOut(obj)
		
		setNumberedFields(obj,NumberedUserData)
		
		NumberedUserData=getNumberedFields(obj)
		
		setLocationOffset(obj,LonOff,LatOff)
		
		[LonOff LatOff]=getLocationOffset(obj)
		
		[LocSelected LocUnselected]=getShiftedData(obj,rowIndices)
		
		ShowParts=getUsedType(obj)
		
		Declustered=checkCluster(obj)
		
		setUsedType(obj,ShowParts)
		
		[EventType ClusterNR] = getDeclusterData(obj)
		
		setDeclusterData(obj,EventType,ClusterNR,selected,ShowParts)
		
		resetDeclusterData(obj)
		
		outData=getFieldNames(obj)
		
		outData=getOriginalFieldNames(obj)
		
		rowCount=getRowCount(obj)
		
		DeclusSelect = getDeclustLogic(obj)
		
		rowCount=getRawRowCount(obj)
		
		[dSelected, dUnselected]=getFieldsClust(obj,fieldNames,rowIndices)
		
		[dSelected, dUnselected]=getFields(obj,fieldNames,rowIndices)
		
		[dSelected, dUnselected]=getFieldsNoCluster(obj,fieldNames,rowIndices)
		
		[dSelected, dUnselected]=getRawFields(obj,fieldNames,rowIndices)
		
		dataVal=getUserData(obj,key)
		
		keys=getUserDataKeys(obj)
		
		setData(obj,newData)
		
		setUserData(obj,key,value)
		
		subscribe(obj, viewName, callbackFcn)
		
		bufferDate(obj)
		
		SetPlotQuality(obj)
		
		CutCatalog(obj,FilterIndex)
		
		ApplyDecluster(obj,FilterIndex)
		
		ReplaceInternalData(obj,eventStruct)
		
		ReplaceInternalRawData(obj,eventStruct)
		
		EditFilteredData(obj,eventStruct,LogIndex,NumberIndex)
		
		ResetRandID(obj)
		
		RandID=GiveID(obj)
		
		setRouting(obj,RoutingEntry)
		
		RouterTable=getRouting(obj)
		
		RouterRepair(obj,whatTo)
		
		RoutedStruct=BuildDataStruct(obj)
		
		BufferRouting(obj,whichField)
		
		[orgField TheData]=LookupField(obj,NewData,TheField)
		
		correctFieldList=ReturnFields(obj)
	end
	
	methods (Access = 'private')
		updateObservers(obj)   
    end
	
	
end	
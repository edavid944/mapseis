function [LocSelected LocUnselected]=getShiftedData(obj,rowIndices)
	%returns the location shifted by the internal shift system if switched on
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	

	
	
	[dSelected, dUnselected]=getFields(obj,{'lon','lat'},rowIndices)
	
	LocSelected=[dSelected.lon, dSelected.lat];
	LocUnselected=[dUnselected.lon, dUnselected.lat];
	
	if ~isempty(obj.LonOffset)
		if ~obj.LonOffset==0
			%Apply Offset
			LocSelected(:,1)=LocSelected(:,1)+obj.LonOffset;	
			LocUnselected(:,1)=LocUnselected(:,1)+obj.LonOffset;
			
			%correct overboarding coords
			LargerLocSel=LocSelected(:,1)>180;
			LargerLocUnSel=LocUnselected(:,1)>180;
			SmallerLocSel=LocSelected(:,1)<-180;
			SmallerLocUnSel=LocUnselected(:,1)<-180;
			
			LocSelected(LargerLocSel,1)=LocSelected(LargerLocSel,1)-360;
			LocUnSelected(LargerLocUnSel,1)=LocUnSelected(LargerLocUnSel,1)-360;
			LocSelected(SmallerLocSel,1)=LocSelected(SmallerLocSel,1)+360;
			LocUnSelected(SmallerLocUnSel,1)=LocUnSelected(SmallerLocUnSel,1)+360;
		end
	end
	
	if ~isempty(obj.LatOffset)
		if ~obj.LatOffset==0
			%Apply Offset
			LocSelected(:,2)=LocSelected(:,2)+obj.LatOffset;	
			LocUnselected(:,2)=LocUnselected(:,2)+obj.LatOffset;
			
			%correct overboarding coords
			LargerLocSel=LocSelected(:,2)>90;
			LargerLocUnSel=LocUnselected(:,2)>90;
			SmallerLocSel=LocSelected(:,2)<-90;
			SmallerLocUnSel=LocUnselected(:,2)<-90;
			
			LocSelected(LargerLocSel,2)=LocSelected(LargerLocSel,2)-180;
			LocUnSelected(LargerLocUnSel,2)=LocUnSelected(LargerLocUnSel,2)-180;
			LocSelected(SmallerLocSel,2)=LocSelected(SmallerLocSel,2)+180;
			LocUnSelected(SmallerLocUnSel,2)=LocUnSelected(SmallerLocUnSel,2)+180;
			
		end
	end

end
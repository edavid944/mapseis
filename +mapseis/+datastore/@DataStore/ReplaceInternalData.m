function ReplaceInternalData(obj,eventStruct)
	%this function will replace the internal DataStruct with the new 
	%data and set the buffers new. It is like 
	%creating a new datastore.
	%The Routing will be used in this one
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	import mapseis.datastore.assocArray;
	
	allfields=fieldnames(eventStruct)
	
	%go throught the fields, get the actuall field and convert data if needed.
	for i=1:numel(allfields)
		[orgField TheData]=LookupField(obj,eventStruct.(allfields{i}),allfields{i})
		CorrectStruct.(orgField)=TheData;
	end
	
	obj.DataStruct=CorrectStruct;
	obj.FieldNames = fieldnames(obj.DataStruct);
	
	%reset UserData
	%obj.UserData = assocArray();
	
	%set buffers
	obj.bufferDate;
	
	%change quality if needed
	obj.SetPlotQuality;
	
	%set buffers of routing
	obj.BufferRouting('all');
	
	%reset Random ID
	obj.RandomID=rand;
	
	%update the observer
	updateObservers(obj);

end
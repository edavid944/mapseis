function CutCatalog(obj,FilterIndex)
	%uses a logical index from filters to cut down the catalog
	%everything not selected by the FilterIndex will be removed from
	%the catalogue
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	%the buffers
	hours = obj.getUserData('Hour');
	minutes = obj.getUserData('Minute');
	years = obj.getUserData('DecYear');
	
	%cut and set (
	if numel(years)~=sum(FilterIndex)
		disp('Cut User Data')
		obj.setUserData('Hour', hours(FilterIndex));	
		obj.setUserData('Minute', minutes(FilterIndex));
		obj.setUserData('DecYear', years(FilterIndex));
	else
		%cutting already done by setUserData
		%obj.setUserData('Hour', hours);	
		%obj.setUserData('Minute', minutes);
		%obj.setUserData('DecYear', years);
		disp('User Data already cutted')
	end
	
	
	
	try
		second = obj.getUserData('Second');
		month = obj.getUserData('Month');
		day = obj.getUserData('Day');
		
		if numel(second)~=sum(FilterIndex)
			obj.setUserData('Second', second(FilterIndex));	
			obj.setUserData('Month', month(FilterIndex));
			obj.setUserData('Day', day(FilterIndex));
		else
			%cutting already done by setUserData
			%obj.setUserData('Second', second);	
			%obj.setUserData('Month', month);
			%obj.setUserData('Day', day);
			
		end
		
	catch
		disp('no seconds, months and days in the buffer')
	end
	
	%the main data
	dataCols = obj.getRawFields([],FilterIndex);
	obj.DataStruct = dataCols;
	
	%change quality if needed
	obj.SetPlotQuality;
	
	%set buffers of routing
	obj.BufferRouting('all');
	
	%reset Random ID
	obj.RandomID=rand;
	
	if ~isempty(obj.EventType)
		%cut the EventType and clusterNR
		obj.EventType=obj.EventType(FilterIndex);
		obj.ClusterNR=obj.ClusterNR(FilterIndex);
	end
	
	
	%update the observer
	updateObservers(obj);
	
end
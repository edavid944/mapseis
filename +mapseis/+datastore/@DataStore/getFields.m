function [dSelected, dUnselected]=getFields(obj,fieldNames,rowIndices)
	% returns the wanted fields (fieldNames) of a catalogue in a structure. 
	% if fieldNames is left empty, all available fields will be returned
	% rowIndices is a logical vector, specifying all the wanted earthquake
	% if rowIndices is left empty or missing, all earthquakes will be returned.
	% if the method is called without any arguments, everything inside the catalogue 
	% will be returned
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	%Apply routing if needed
	if ~isempty(obj.Routing)
		TheStruct=obj.BuildDataStruct;
	else	
		TheStruct=obj.DataStruct;
	
	end
	
	
	if ~isempty(obj.EventType)
		unclas=obj.EventType==0;
		singleEq=obj.EventType==1;
		mainEq=obj.EventType==2;
		afterEq=obj.EventType==3;
		declustSelect = (unclas&obj.ShowUnclassified)|...
		(singleEq&obj.TypeUsed(1))|...
		(mainEq&obj.TypeUsed(2))|...
		(afterEq&obj.TypeUsed(3));
		
		%check if the rowIndices
		
	else	
		declustSelect=true(obj.getRawRowCount,1);
	
	end
	
	
	if nargin==1&isempty(obj.EventType)
		dSelected=TheStruct;
		dUnselected=[];
		return
		
	elseif nargin==1&~isempty(obj.EventType)
		rowIndices=true(obj.getRawRowCount,1);
		
	end
	
	if nargin<3 || ischar(rowIndices)
		% If rowIndicies are unspecified then get all rows or
		% rowIndices = 'all'
		rowIndices = true(obj.getRawRowCount(),1);
	end
	
	if nargin<2 || isempty(fieldNames)
		% If fieldNames are unspecified then get all fields
		%fieldNames = obj.FieldNames;
		fieldNames = fields(TheStruct);
	end
	
	if numel(rowIndices)~=numel(declustSelect);
		temp=rowIndices;
		rowIndices=false(obj.getRawRowCount,1);
		rowIndices(declustSelect)=temp;
	end
	
	%Added for error if just a string as entry is used -> convert
	%to a cell array
	if isstr(fieldNames)
		fieldNames={fieldNames};
	end
	
	dSelected = struct;
	dUnselected = struct;
	
	for i=1:numel(fieldNames)
		thisField = fieldNames{i};
		dataCol = TheStruct.(thisField);
		dSelected.(thisField) = dataCol(rowIndices&declustSelect);
		dUnselected.(thisField) = dataCol(not(rowIndices)&declustSelect);
	end            
end                                
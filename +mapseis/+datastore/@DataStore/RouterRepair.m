function RouterRepair(obj,whatTo)
	%This function contains some functions allowing to reset and repair the 
	%routing table, may not needed but who knows, they just should be existing
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	import mapseis.datastore.emptier;
	
	
	switch whatTo
		case 'reset'
			%empty the RoutingTable
			obj.Routing={};
		case 'clean'
			%kicks out all routes which are not set anymore
			emptyRoute=emptier(obj.Routing(:,2));
			obj.Routing=obj.Routing(~emptyRoute,:);
		
		
	end
	

end
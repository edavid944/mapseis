function InterData = SpitOut(obj)
	%This function spits out all internal data and is ment for coning of the object
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	InterData.FieldNames=obj.FieldNames;
	InterData.DataStruct=obj.DataStruct;
	InterData.UserData=obj.UserData;
	InterData.Routing=obj.Routing;
	InterData.NumberedUserData=obj.NumberedUserData;
	InterData.LonOffset=obj.LonOffset;
	InterData.LatOffset=obj.LatOffset;
	InterData.EventType=obj.EventType;
	InterData.ClusterNR=obj.ClusterNR;
	InterData.TypeUsed=obj.TypeUsed;
	
	InterData.RandomID=obj.RandomID;
	InterData.Version=obj.Version;
	InterData.UseShift=obj.UseShift;
	InterData.ShowUnclassified=obj.ShowUnclassified;
	InterData.DeclusterMetaData=obj.DeclusterMetaData;

end
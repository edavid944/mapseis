function [orgField TheData]=LookupField(obj,NewData,TheField)
	%The function checks if the given field is part of the routing table
	%replaces it if necessary and converts data if necessary
	%And returns it back to the caller, this has to be done because, often a
	%logical index is used with the data;
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	
	orgField=TheField;
	TheData=NewData;
	
	if ~isempty(obj.Routing)
		%Check if the field is in Routing
		foundIn=strcmp(obj.Routing(:,1),TheField);
		%The orgField
		if sum(foundIn)>0
			orgField=obj.Routing{foundIn,2};
		end
		
		if sum(foundIn)>0 & ~isempty(obj.Routing{foundIn,3})
			TheData=obj.Routing{foundIn,3}(NewData,'Map2Catalog');
		end	
	
	end
	
end
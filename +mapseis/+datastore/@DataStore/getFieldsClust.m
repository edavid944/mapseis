function [dSelected, dUnselected]=getFieldsClust(obj,fieldNames,rowIndices)
	%wrap around function, it adds the decluster info to the returned 
	%structure, sometimes handy
	
	%arguments see getFields
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	%get the structure first
	[dSelected, dUnselected]=getFields(obj,fieldNames,rowIndices)
	
	%add the data 
	if ~isempty(obj.EventType)
		unclas=obj.EventType==0;
		singleEq=obj.EventType==1;
		mainEq=obj.EventType==2;
		afterEq=obj.EventType==3;
		declustSelect = (unclas&obj.ShowUnclassified)|...
		(singleEq&obj.TypeUsed(1))|...
		(mainEq&obj.TypeUsed(2))|...
		(afterEq&obj.TypeUsed(3));
		if nargin==1
			rowIndices = true(obj.getRawRowCount,1);
		end
		
		if nargin<3 || ischar(rowIndices)
			% If rowIndicies are unspecified then get all rows or
			% rowIndices = 'all'
			rowIndices = true(obj.getRowCount,1);
		end
		
		
		dSelected.dclust_EventType=obj.EventType(rowIndices&declustSelect);
		dUnselected.dclust_EventType=obj.EventType(~rowIndices&declustSelect);
		dSelected.dclust_ID=obj.ClusterNR(rowIndices&declustSelect);
		dUnselected.dclust_ID=obj.ClusterNR(~rowIndices&declustSelect);
	end

end
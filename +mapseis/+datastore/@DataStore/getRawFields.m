function [dSelected, dUnselected]=getRawFields(obj,fieldNames,rowIndices)
	%Same as the "normal getFields but it uses the unrouted Catalog instead 
	%This also not uses the decluster info
	
	%see getFields for input argmunent
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	%Go for the unrouted catalog
	TheStruct=obj.DataStruct;
	
	
	if nargin==1
		dSelected=TheStruct;
		dUnselected=[];
		return
	end
	
	if nargin<3 || ischar(rowIndices)
		% If rowIndicies are unspecified then get all rows or
		% rowIndices = 'all'
		rowIndices = true(obj.getRowCount(),1);
	end
	
	if nargin<2 || isempty(fieldNames)
		% If fieldNames are unspecified then get all fields
		%fieldNames = obj.FieldNames;
		fieldNames = fields(TheStruct);
	end
	
	%Added for error if just a string as entry is used -> convert
	%to a cell array
	if isstr(fieldNames)
		fieldNames={fieldNames};
	end
	
	dSelected = struct;
	dUnselected = struct;
	for i=1:numel(fieldNames)
		thisField = fieldNames{i};
		dataCol = TheStruct.(thisField);
		dSelected.(thisField) = dataCol(rowIndices);
		dUnselected.(thisField) = dataCol(not(rowIndices));
	end            
end                         
function EditFilteredData(obj,eventStruct,LogIndex,NumberIndex)
	%get fields from input
	%The NumberIndex will be used to fill the holes in the new data if specified
	%the number of coulomb has to be equal to the eventStructs (for each event an index)
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	
	if nargin<4
		NumberIndex=[];
	end
	
	FieldNames = fieldnames(eventStruct);
	rowCount = numel(obj.DataStruct.(obj.FieldNames{1}));
	
	
	if ~isempty(obj.EventType)
		unclas=obj.EventType==0;
		singleEq=obj.EventType==1;
		mainEq=obj.EventType==2;
		afterEq=obj.EventType==3;
		declustSelect = (unclas&obj.ShowUnclassified)|...
						(singleEq&obj.TypeUsed(1))|...
						(mainEq&obj.TypeUsed(2))|...
						(afterEq&obj.TypeUsed(3));
	else	
		declustSelect=true(obj.getRowCount,1);
	
	end
	
	LogIndex=LogIndex&declustSelect;
	
	if sum(LogIndex)==numel(eventStruct.(FieldNames{1}))
		%go through all the fields
		for i=1:numel(FieldNames)
			%look up the fields
			
			[orgField TheData]=LookupField(obj,eventStruct.(FieldNames{i}),FieldNames{i});
			
			if ~isempty(orgField)
				%might be unnecessary as non existing field returns
				%a empty orgField
				if isfield(obj.DataStruct,orgField)
					obj.DataStruct.(orgField)(LogIndex)=TheData;
				else
					%build new field
					obj.DataStruct.(orgField)=ones(rowCount,1)*NaN;
					obj.DataStruct.(orgField)(LogIndex)=TheData;
				end
			else
				%build new field
				obj.DataStruct.(FieldNames{i})=ones(rowCount,1)*NaN;
				obj.DataStruct.(FieldNames{i})(LogIndex)=TheData;
				
			end
	
		end
	
	else
		%rework for routing!!!!!!!!!!!!!!!!!!!! -> finished
		
		%there are some events more or some are missing, erease the block
		%and add new
		totalnumber=rowCount-numel(eventStruct.(FieldNames{1}))+sum(LogIndex);
		numberlasting=rowCount-numel(eventStruct.(FieldNames{1}));
		
		oldFieldNames=obj.ReturnFields;
		
		%olddata
		oldData=obj.getFields;
		
		%not existing fields in one or the other datastructure
		newinold=ismember(oldFieldNames,FieldNames);
		Fields_nio=oldFieldNames(~newinold);
		oldinnew=ismember(FieldNames,oldFieldNames);
		Fields_oin=FieldNames(~oldinnew);
		
		allFields=unique([FieldNames;oldFieldNames]);
		
		%build unchange events
		for i=1:numel(oldFieldNames)
			unchanged.(oldFieldNames{i})=oldData.(oldFieldNames{i})(~LogIndex);
		end
	
		%build NaNs for not existing new fields
		for i=1:numel(Fields_oin)
			if ~iscell(eventStruct.(Fields_oin{i}))
				unchanged.(Fields_oin{i})=ones(numberlasting,1)*NaN;
			else
				unchanged.(Fields_oin{i})=cell(numberlasting,1);
			end
		
		end
	
		%build NaNs for not existing old fields in eventStruct
		%build NaNs for not existing new fields
		if isempty(NumberIndex)
			for i=1:numel(Fields_nio)
				if ~iscell(obj.DataStruct.(Fields_nio{i}))
					eventStruct.(Fields_nio{i})=ones(sum(LogIndex),1)*NaN;
				else
					eventStruct.(Fields_nio{i})=cell(sum(LogIndex),1);
				end
			end
			
		else
			%index is existing, so just take the values from the old fields
			for i=1:numel(Fields_nio)
				eventStruct.(Fields_nio{i})=oldData.(Fields_nio{i})(NumberIndex);	
			end
		end
		
		%build new set
		for i=1:numel(allFields)
			newDatastruct.(allFields{i})=[unchanged.(allFields{i});eventStruct.(allFields{i})];
		end
	
		%almost done, just a rearrangement of the sort order is needed
		[sorted newsort]=sort(newDatastruct.dateNum)
		
		for i=1:numel(allFields)
			newDatastruct.(allFields{i})=newDatastruct.(allFields{i})(newsort);
		end
		
		%rename and use convertes if needed
		%This has to be done in seperate loop because of the sorting (The field for dateNum may be not
		%always called so in the "real" data).
		if ~isempty(obj.Routing)
			for i=1:numel(allFields)
				%get corrected data and Field
				[orgField TheData]=obj.LookupField(obj,newDatastruct.(allFields{i}),allFields{i})
				
				%remove old field
				newDataStruct=rmfield(newDataStruct,allFields{i});
				
				%add new one
				newDataStruct.(orgField)=TheData;
			end
		end
		%..and finally the finish
		obj.DataStruct=newDatastruct;
	
	end	
	
	%reapply buffers
	obj.bufferDate;
	
	%rebuild fieldnames
	obj.FieldNames = fieldnames(obj.DataStruct);
	
	%reset Random ID
	obj.RandomID=rand;
	
	%set buffers of routing
	obj.BufferRouting('all');
	
	%check if numbered user data is existing
	%(NOT YET IMPLEMENTED, NOT KNOWN IF NEEDED)
	
	
	%update the observer
	updateObservers(obj);
	
end
function logVec=emptier(Vec)

logVec=[];

if iscell(Vec)    
	for i=1:numel(Vec)
    	logVec(i)=isempty(Vec{i});
	end
else
	for i=1:numel(Vec)
    	logVec(i)=isempty(Vec(i));
	end
end
end
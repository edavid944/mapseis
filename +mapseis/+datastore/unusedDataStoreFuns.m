%% Properties
% % Filter parameters for the default filters
%         regionPoly = {};
%         timeRange = {};
%         magRange = {};
%         depthRange = {};
%         hourRange = {};
%         sphereRange = {};
%         innerFunStruct %
%         defaultFilters
%         defaultFilterFun

%% Old DataStore constructor code
%             
%             % Set data names of interest
%             obj.innerFunStruct.setDataName('loc',@LocProj);
%             obj.innerFunStruct.setDataName('time',@TimeProj);
%             obj.innerFunStruct.setDataName('mag',@MagProj);
%             obj.innerFunStruct.setDataName('depth',@DepthProj);
%             obj.innerFunStruct.setDataName('hour',@HourProj);
%             obj.innerFunStruct.setDataName('sphere',@SphereProj);
%             
%             % Define a set of standard filters that are combined to filter events based
%             % on time, magnitude and location
%             
%             % Initially have an empty set of filters to apply to the data
%             obj.defaultFilters = assocArray();
%             obj.defaultFilterFun = [];
%             
%             % Set the default filter
%             setDefaultFilter(obj);


function [selected,unselected] = getLocations(obj,varargin)
            [selected,unselected] = obj.getProcData('loc',varargin{:});
        end
        
        function [selected,unselected] = getTimes(obj,varargin)
            [selected,unselected] = obj.getProcData('time',varargin{:});
        end
        
        function [selected,unselected] = getDepths(obj,varargin)
            [selected,unselected] = obj.getProcData('depth',varargin{:});
        end
        
        function [selected,unselected] = getHour(obj,varargin)
            [selected,unselected] = obj.getProcData('hour',varargin{:});
        end
        
        function [selected,unselected] = getSphere(obj,varargin)
            [selected,unselected] = obj.getProcData('sphere',varargin{:});
        end
        
        function [selected,unselected] = getMagnitudes(obj,varargin)
            [selected,unselected] = obj.getProcData('mag',varargin{:});
        end
        
        function filterRange = getRegion(obj)
            filterRange = obj.getFilterRange('region');
        end
        
        function filterRange = getTimeRange(obj)
            filterRange = obj.getFilterRange('time');
        end
        
        function filterRange = getDepthRange(obj)
            filterRange = obj.getFilterRange('depth');
        end
        
        function filterRange = getHourRange(obj)
            filterRange = obj.getFilterRange('hour');
        end
        
        function filterRange = getSphereRange(obj)
            filterRange = obj.getFilterRange('sphere');
        end
        
        function filterRange = getMagnitudeRange(obj)
            filterRange = obj.getFilterRange('mag');
        end

        function f=getDefaultFilter(obj)
            f = obj.defaultFilterFun;
        end
        
        function setFilter(obj,varargin)
            obj.innerFunStruct.setFilter(varargin{:});
            % Notify the observers that something has happened
            updateObservers(obj);
        end
        
        function setRegion(obj,varargin)
            import mapseis.filter.*;
    
            % Set the region of interest
            try
                obj.regionPoly = varargin;
                obj.defaultFilters.set('region',RegionFilter(varargin{:}));
                setDefaultFilter(obj);
            catch ME
                warning('DataStore:setRegion_error','Unable to set region');
            end
        end
        
        function setTimeRange(obj,varargin)
            import mapseis.filter.*;
            
            % Set the time range of interest
            try
                obj.timeRange = varargin;
                obj.defaultFilters.set('time',TimeRangeFilter(varargin{:}));
                setDefaultFilter(obj);
            catch ME
                warning('DataStore:setTimeRange_error',...
                    'Unable to set time range');
            end
        end
        
        function setDepthRange(obj,varargin)
            import mapseis.filter.*;
            
            % Set the depth range of interest
            try
                obj.depthRange = varargin;
                obj.defaultFilters.set('depth',DepthFilter(varargin{:}));
                setDefaultFilter(obj);
            catch ME
                warning('DataStore:setDepthRange_error',...
                    'Unable to set depth range');
            end
        end
        
        function setHourRange(obj,varargin)
            import mapseis.filter.*;
            
            % Set the hour range of interest
            try
                obj.hourRange = varargin;
                obj.defaultFilters.set('hour',HourRangeFilter(varargin{:}));
                setDefaultFilter(obj);
            catch ME
                warning('DataStore:setHourRange_error',...
                    'Unable to set hour range');
            end
        end
        
        function setSphereRange(obj,varargin)
            import mapseis.filter.*;
            
            % Set the hour range of interest
            try
                obj.hourRange = varargin;
                obj.defaultFilters.set('sphere',HourRangeFilter(varargin{:}));
                setDefaultFilter(obj);
            catch ME
                warning('DataStore:setSphereRange_error',...
                    'Unable to set sphere range');
            end
        end
        
        function setMagnitudeRange(obj,varargin)
            import mapseis.filter.*;
            
            % Set the magnitude range of interest
            try
                obj.magRange = varargin;
                obj.defaultFilters.set('magnitude',MagFilter(varargin{:}));
                setDefaultFilter(obj);
            catch ME
                warning('DataStore:setMagnitudeRange_error',...
                    'Unable to set magnitude range');
            end
        end
        
        function setDefaultFilter(obj)
            import mapseis.filter.*;
            
            % Set the default filter based on theFilters
            % Set the event filter to initially pass all events
            if obj.defaultFilters.isEmpty()
                obj.defaultFilterFun = @AllEventsFilter;
                obj.innerFunStruct.setFilter('evFilter',@AllEventsFilter);
            else
                filterKeys = obj.defaultFilters.getKeys();
                % Initialise the accumulator with the first filter function
                accumulator = obj.defaultFilters.lookup(filterKeys{1});
                % loop through the rest of the filters and combine them
                for i=2:numel(filterKeys)
                    accumulator = CombineFilter(...
                        obj.defaultFilters.lookup(filterKeys{i}),accumulator);
                end
                obj.defaultFilterFun = accumulator;
                obj.innerFunStruct.setFilter('evFilter',accumulator);
            end
            % Notify all of the observers
            updateObservers(obj)
        end
        
        function clearFilters(obj)
            % Clear the filters on events
            filterKeys = obj.defaultFilters.getKeys();
            for i=1:numel(filterKeys)
                obj.defaultFilters.remove(filterKeys{i});
            end
            setDefaultFilter(obj);
        end
        
         function [selected,unselected]=getProcData(obj,dataName,filterName)
            % Get the locations of events satisfying the filter criteria.  This
            % is a generalisation of the nGetLocations, nGetTimes etc functions
            % that were present in a previous iteration of the program.
            if  nargin==2
                filterName='evFilter';
            end
            [selected,unselected]=obj.innerFunStruct.getData(filterName,dataName);
        end
        
        function filterRange=getFilterRange(obj,filterName)
            switch filterName
                case 'time'
                    filterRange = obj.timeRange;
                case 'mag'
                    filterRange = obj.magRange;
                case 'depth'
                    filterRange = obj.depthRange;
                case 'hour'
                    filterRange = obj.hourRange;
                case 'region'
                    filterRange = obj.regionPoly;
                case 'sphere'
                    filterRange = obj.sphereRange;
            end
        end
classdef EventStore < mapseis.datastore.DataStore
    % EventStore : MapSeis event data with Subject design pattern
    % The EventStore ADT is initialised by a struct produced by importing
    % data from an earthquake catalog using Import(xxxx)Catalog.  This ADT
    % wraps the underlying DataStore in the Subject design pattern that
    % allows viewers to be attached to the data.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
    % Author: Matt McDonnell
    
    properties        
    end
    
    events
    end
    
    methods
        function obj = EventStore(varargin)
            % Constructor for the event store
            obj = obj@mapseis.datastore.DataStore(varargin{:});
        end
        
        function subscribe(obj, viewName, callbackFcn)
            % Subscribe the view obs to the Event Store object
            addlistener(obj,'Update',@(s,e) callbackFcn());
        end
    end
    
end
classdef ProcessedDataStore < handle
    % processedDataStore : object to access data through named parameters
    %
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 70 $    $Date: 2008-10-13 18:30:38 +0100 (Mon, 13 Oct 2008) $
    % Author: Matt McDonnell
    
    %% Attributes
    
    properties
        DataStruct
        NameStruct
    end
    
    % Filtered data store object
    % Initialise the data store from an input struct
    % The data is a 2D table with individual data points making up the rows of
    % the table and the columns being named by the parameters of the data.  We
    % want to be able to access a subset of the total data by specifying a
    % vector of row indices (or logical indices), with these indices being
    % calculated from the data parameters.
    
    
    %% Public Methods
    methods
        function obj = ProcessedDataStore(varargin)
            % Constructor
            
            try
                switch nargin
                    case 1
                        % Input must be a struct
                        assert(isa(varargin{1},'mapseis.datastore.FilteredDataStore'));
                        obj.DataStruct = varargin{1};
                end
            catch ME
                error('ProcessedDataStore:unknown_constructor_arg',...
                    'ProcessedDataStore : unknown input argument');
            end
            
            % Parameter names and processing functions
            obj.NameStruct = struct;
        end
        
        
        function fldNames=getFields(obj)
            % Get the fieldnames of the filtered data structure.  These
            % fieldnames are used to specify data filters and processing
            % functions for the filtered data
            fldNames=obj.DataStruct.getFields();
        end
        
        function setDataName(obj,dataName,procFun)
            % Define a named data element and the processing function needed to
            % derive that element from the filtered data structure.  For
            % example, if the filtered data structure has fields for m x 1 latitude
            % and longitude vectors, define a 'location' data name that combines these
            % into a m x 2 array using the procFun @(x) [x.lat, x.lon]
            obj.NameStruct.(dataName) = procFun;
        end
        
        function [selected,unselected]=getData(obj,filterName,dataName)
            % Get filtered data from the filtered data structure and apply the
            % processing function dataName to this output.  If dataName is
            % undefined then return all data fields, if filterName is undefined don't
            % filter the data
            switch nargin
                case 1
                    [selected,unselected] = obj.DataStruct.getData();
                case 2
                    [selected,unselected] = obj.DataStruct.getData(filterName);
                case 3
                    [sel,unsel] = obj.DataStruct.getData(filterName);
                    selected = obj.NameStruct.(dataName)(sel);
                    unselected = obj.NameStruct.(dataName)(unsel);
            end
        end
        
        function setData(obj,newData)
            % Set the underlying data, clearing filters but not processing
            % functions
            obj.DataStruct.setData(newData);
        end
        
        function setFilter(obj,filterName,filterFun)
            % Set the filter that is applied to select elements from the
            % filtered data structure
            obj.DataStruct.setFilter(filterName,filterFun);
        end
    end
    %% Private Methods
    
end
classdef loaderGUI < handle
%gui for the loader, which is saved as a mat file
%

	properties
		loader_figure
		RecentFiles	
		Recentmenu
		datastorer
		mapseisgui
		ListProxy
		recallButton
		panelHnd
	end

	events
        changed
    end

	methods

			function obj = loaderGUI
				obj.loader_figure = [];
				obj.RecentFiles = {};			
				obj.ListProxy = [];
			
			end
		
		
			function createGUI(obj)
				%produce gui
				obj.loader_figure = figure(...       
                 		   				'MenuBar','none', ...
                    					'Toolbar','none', ...
                   						'HandleVisibility','callback', ...
           			    			    'Color', get(0,...
                        			    'defaultuicontrolbackgroundcolor'));


				set(obj.loader_figure, 'Position', [50 50 320 200]);
				
				obj.recallButton = uicontrol('Parent',obj.loader_figure,...
										 'Style','pushbutton',...
										 'String','Rebuild last Session',...
          								 'Position',[80,80,160,40],...
         								 'Callback',@(s,e) obj.rebuildSession);				
				
				obj.FileMenuBuilder;
				obj.RecentMenuBuilder;	
				
				%load monitor
				obj.panelHnd = uipanel('Parent',obj.loader_figure,'BorderType','none',...
                    'Units','Normalized',...
                    'Position',[0.01 0.01 0.98 0.75]);
                set(obj.panelHnd,'Title','Idle','TitlePosition','centertop',...
                	'FontWeight','bold','FontSize',12)
				
				%%set to green
				set(obj.loader_figure,'Color',[0.702 1 0.702]);
				set(obj.panelHnd,'BackgroundColor',[0.702 1 0.702])
				
			end
		
		
		
		
		
			function FileMenuBuilder(obj)
				%the menu with the load different load commands
			 	hFileMenu = uimenu(...       % File menu
                  					      'Parent',obj.loader_figure,...
           					              'HandleVisibility','callback', ...
              					          'Label','File');
				
							uimenu(...       % open mapseis session
                        				  'Parent',hFileMenu,...
                        					'Label','Open Mapseis Session',...
                       						 'HandleVisibility','callback', ...
                    					    'Callback', @(s,e) obj.openMapSeis);
				
							uimenu(...       % open zmap file
                        				  'Parent',hFileMenu,...
                        					'Label','Open Zmap catalog',...
                       						 'HandleVisibility','callback', ...
                    					    'Callback', @(s,e) obj.openZmap);

							uimenu(...       % open txt file
                        				  'Parent',hFileMenu,...
                        					'Label','Import Catalog',...
                       						 'HandleVisibility','callback', ...
                    					    'Callback', @(s,e) obj.ImportCatalog);

			
			end
			
			
			
			function RecentMenuBuilder(obj)
				
				try
					set(obj.Recentmenu,'visible','off');
				end
				
				
				obj.Recentmenu = uimenu(...       % Recent menu
                  					      'Parent',obj.loader_figure,...
           					              'HandleVisibility','callback', ...
              					          'Label','Recent');

				
								
				
				nr_entry = numel(obj.RecentFiles);
				
					for ent = 1:nr_entry
							uimenu(...     
                        				  'Parent',obj.Recentmenu,...
                        					'Label',obj.RecentFiles{ent}.name,...
                       						 'HandleVisibility','callback', ...
                    					    'Callback', @(s,e) obj.RecentFiles{ent}.callfunction(...
                    					    				obj.RecentFiles{ent}.path));
	
					
					
					end	
					
					
				%menu point to delete all entries in the recent list
								uimenu(...      
                        				  'Parent',obj.Recentmenu,...
                        					'Label','Clear List',...
                       						 'HandleVisibility','callback', ...
                    					    'Separator','on',...
                    					    'Callback', @(s,e) obj.ClearEntries);
	
					
			end			
			
			
			
			function openMapSeis(obj,filename)
				%opens a old mapseis session
				
				import mapseis.*;
				obj.loadmonitor('busy');
					 
					 %open load dialog if no filename specified
					 if nargin<2
						[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
          					  if isequal(fName,0) || isequal(pName,0)
               						 disp('No filename selected. ');
           					  else
								     filename=fullfile(pName,fName);
							  end	     
					 end
				try	 
					[obj.datastorer obj.mapseisgui obj.ListProxy] = startApp(filename);
				
					[pathstr, name, ext] = fileparts(filename);
	 	
					%set the recent file menu	 	
					obj.newRecentEntry(name,filename,@obj.openMapSeis);	 	
				catch
					disp('something went wrong');
			 	
				end
				obj.loadmonitor('idle');
			end
			
			
			
			function openZmap(obj,filename)
				%opens a old mapseis session
				
				import mapseis.*;
				obj.loadmonitor('busy');
					 %open load dialog if no filename specified
					 if nargin<2
						[fName,pName] = uigetfile('*.mat','Enter Zmap *.mat file name...');
          					  if isequal(fName,0) || isequal(pName,0)
               						 disp('No filename selected. ');
           					  else
								     filename=fullfile(pName,fName);
							  end	     
					 end
				try	 
					[obj.datastorer obj.mapseisgui obj.ListProxy] = startApp(filename);
				
					[pathstr, name, ext] = fileparts(filename);
	 	
					%set the recent file menu	 	
					obj.newRecentEntry(name,filename,@obj.openZmap);	 	
				catch
					disp('something went wrong');
			 	
				end
				
				obj.loadmonitor('idle');
			end
			

			function ImportCatalog(obj,filename)
				%opens a old mapseis session
				
				import mapseis.*;
				obj.loadmonitor('busy');
					 %open load dialog if no filename specified
					 if nargin<2
						[fName,pName] = uigetfile('*.txt','Enter scec *.txt file name...');
          					  if isequal(fName,0) || isequal(pName,0)
               						 disp('No filename selected. ');
           					  else
								     filename=fullfile(pName,fName);
							  end	     
					 end
				try	 
					[obj.datastorer obj.mapseisgui obj.ListProxy] = startApp(filename);
				
					[pathstr, name, ext] = fileparts(filename);
	 	
					%set the recent file menu	 	
					obj.newRecentEntry(name,filename,@obj.ImportCatalog);	 	
				catch
					disp('something went wrong');
			 	
				end
				
				obj.loadmonitor('idle');
			end

			
			
			function newRecentEntry(obj,fName,filepath,handl)
				%updates the RecentFiles list
						
						
				%build new structur
				newentry = struct('name', fName,...
								  'path', filepath,...
								  'callfunction', handl);		
				
				%get it into the list
				entries = numel(obj.RecentFiles);
				
					if entries == 0
						obj.RecentFiles{1} = newentry;
					elseif entries == 5
						obj.RecentFiles = {newentry,obj.RecentFiles{1:4}};
					else
						obj.RecentFiles = {newentry,obj.RecentFiles{1:end}};	 	
					end
			obj.RecentMenuBuilder;
			
			%now save the object with the interface	
			obj.updateObservers;
			
			end
			
		
		
			function ClearEntries(obj)
				%clears the recent entries updates the gui and notifies listeners
				
				obj.RecentFiles = [];
				obj.RecentMenuBuilder;
				obj.updateObservers;
					
			end
		
		
		
		
		
		
		
			function [dataStore,guiWindowStruct,ListProxy] = getMapseisData(obj)
				%returns the loaded datastore and gui from mapseis to the interface
				dataStore = obj.datastorer; 
				guiWindowStruct = obj.mapseisgui;
				ListProxy = obj.ListProxy;
			end
			
		
		

			
			function subscribe(obj, viewName, callbackFcn)
            	% Subscribe the view obs to the Event Store object
            	addlistener(obj,'changed',@(s,e) callbackFcn());
        	end



			function rebuildSession(obj)
				%Used to rebuild the interface of stored session
				obj.loadmonitor('busy');
				try
					%clear listeners first
					obj.ListProxy.shoutup;

					
					obj.mapseisgui.mainStruct.rebuildGUI;
					obj.mapseisgui.mainParamStruct.rebuildGUI;
					
										
					%reapply listeners
					gui = obj.mapseisgui;
					filterList = gui.mainStruct.FilterList;
				
					%reapply listeners
					obj.ListProxy.listen(filterList,'Update',...
  								  @(s,e) gui.mainStruct.updateGUI());
					obj.ListProxy.listen(filterList,'Update',...
   								 @(s,e) gui.mainParamStruct.updateGUI());
					
					
					filterList.reInitList;
					
					

					
				
				end
				obj.loadmonitor('idle');		
			end
				
			
			function loadmonitor(obj,state)
				%red = working, grey = idle
				switch state
					case 'busy'
							set(obj.panelHnd,'Title','Busy','TitlePosition','centertop',...
                				'FontWeight','bold','FontSize',12)
				
							%%set to green
							set(obj.loader_figure,'Color',[1 0.702 0.702]);
							set(obj.panelHnd,'BackgroundColor',[1 0.702 0.702])
							
					case 'idle'
							set(obj.panelHnd,'Title','Idle','TitlePosition','centertop',...
                				'FontWeight','bold','FontSize',12)
				
							%%set to green
							set(obj.loader_figure,'Color',[0.702 1 0.702]);
							set(obj.panelHnd,'BackgroundColor',[0.702 1 0.702])
							
					case 'deactivate'
							set(obj.panelHnd,'Title','','TitlePosition','centertop',...
                				'FontWeight','bold','FontSize',12)
				
							%%set to green
							set(obj.loader_figure,'Color',[0.702 0.702 0.702]);
							set(obj.panelHnd,'BackgroundColor',[0.702 0.702 0.702])
		

				end		
				
				
				
			end

					
	end
	
	
	
	
	
	
	  %% Private Methods
   	methods (Access = 'private')
        function updateObservers(obj)
            % Notify the observers of this object that a change has
            % occurred
            notify(obj,'changed');
        end
	end

end
classdef loaderInterface < handle

	properties
		theLoader 
		datastorer
	 	mapseisgui
	 	ListProxy
	end


	methods
			function obj = loaderInterface
				 import mapseis.loader.*;	
				 
				 
				 %first see if an old loader is saved as .mat file
				 try 
				 		warning off all;
				 		load 'recentfiles.mat';
				 		warning on all;
				 		obj.theLoader = recentfiles;
				 		obj.theLoader.createGUI;
				 		
				 						 		
				 		[obj.datastorer, obj.mapseisgui obj.ListProxy] = obj.theLoader.getMapseisData;
				 		
				 		%clear listeners first
						obj.ListProxy.shoutup;

				 		
				 						 				 		
									 		
				 catch 		
				 		obj.theLoader = loaderGUI;
				 		obj.theLoader.createGUI;
				 		
				 		recentfiles = obj.theLoader;
				 		save 'recentfiles.mat' 'recentfiles';
				 end
				 %subscribe to the loader to allow autosave
				 obj.theLoader.subscribe('Interface',@() Update(obj));
				 
				 
			end
			
				 
			
			function Update(obj)
				%the loader did something, save it and get the data
				import mapseis.loader.*;
				
				[obj.datastorer, obj.mapseisgui obj.ListProxy] = obj.theLoader.getMapseisData;
				
				recentfiles = obj.theLoader;
				save 'recentfiles.mat' 'recentfiles';
				
				
			end	 
			
			
			function [datastorer guiw ListProxy] = returnData(obj)
				%returns the handles to the datastore and the mapseis gui
				
				datastorer = obj.datastorer;
				guiw = obj.mapseisgui;
				ListProxy = obj.ListProxy; 
							
			end
			
			
			function ClearSaveDate(obj)
				%deletes the saved recentfiles file, useful if the loaderGUI changed
				
				try 
					delete 'recentfiles.mat'
				end	
			
			end	
	end


end
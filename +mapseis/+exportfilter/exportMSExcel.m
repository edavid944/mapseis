function InfoOut = exportMSExcel(dataStore,rowIndices,FileName,varargin)
%Used to export mapseis data into a excel file, in this version only the basic files
%are written out, a later version will feature full output. But this version works also
%if excel is not installed (cvs)

%Typical call: exportMSExcel(dataStore,[],'./SuperCatalog.mat','-file')


%% Import helper functions
import mapseis.util.importfilter.*;
import mapseis.projector.*;

onlydescription=false;
InfoOut=[];
if nargin>=4
    if strcmp(varargin{1},'-file')
       
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	InfoOut.description = 'This exportfilter writes the datastore into a excel (.xls) file as used by Microsoft Excel. In this version only the basic files are outputed (similar to ZMAP format). The functions used in this filter only work if excel is installed, if this is not case a .cvs file will be generated instead';
    	InfoOut.displayname = 'Excel formated-ascii basic catalogs v1' ;
    	InfoOut.filetype = '*.xls'
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	
	if isempty(rowIndices)
		%use all events
		  rowIndices = true(dataStore.getRowCount(),1);
	end
	
	%only original locations are supported at the moment (more useful anyway)
	PaleRider=true;
	
	
	%get the data from datastore
	%---------------------------
	
	%zmap catalog is a good start basis
	a=getZMAPFormat(dataStore,rowIndices,PaleRider);
	
	%save(FileName,'a','-ascii');
	xlswrite(FileName, a);
	
	
	
end

end

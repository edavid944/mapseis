function InfoOut = exportZMAPCatalogASCII_Focal(dataStore,rowIndices,FileName,varargin)
	%Used to export mapseis data into a binary (.mat) zmap file, this version only supports
	%catalogs with seconds and not with the focal mechanism
	
	%Typical call: exportZMAPCatalogBinary(dataStore,[],'./SuperCatalog.mat','-file')
	
	%It is probably the most simple exportfilter which can be build
	
	
	%% Import helper functions
	import mapseis.util.importfilter.*;
	import mapseis.projector.*;
	
	onlydescription=false;
	InfoOut=[];
	if nargin>=4
	    if strcmp(varargin{1},'-file')
	       
		
	    elseif strcmp(varargin{1},'-description')
		%new option: returns a description of the import filter for the gui
		InfoOut.description = ['This exportfilter writes the datastore into a Zmap',...
					' formated ascii file (.dat) importable into Zmap ',...
					'and MapSeis. Focal mechanism are used instead of seconds if ',...
					'if availavble'];
		InfoOut.displayname = 'ZMAP formated-ascii catalogs v1 (Focals)' ;
		InfoOut.filetype = '*.dat'
		onlydescription= true;
		
	    end
	
	end
	
	
	if ~onlydescription
		
		if isempty(rowIndices)
			%use all events
			  rowIndices = true(dataStore.getRowCount(),1);
		end
		
		%only original locations are supported at the moment (more useful anyway)
		PaleRider=true;
		
		
		%get the data from datastore
		%---------------------------
		
		%zmap catalog is a good start basis
		a=getZMAPFormat(dataStore,rowIndices,PaleRider);
		foc=getFocals(dataStore,rowIndices);
		
		if ~(all(all(isnan(foc))))
			a(:,10:12) = foc;
		end
		
		%save(FileName,'a','-ascii');
		dlmwrite(FileName, a, 'delimiter', '\t', ...
		 'precision', 12)
		
		
		
	end

end

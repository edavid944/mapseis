function InfoOut = exportNTSCatalog(dataStore,rowIndices,FileName,varargin)
%Used to export mapseis data into a .nts formated catalog like the ones used in 
%STEP (Java version). It is also the first real export filter which will be 
%later supported by a similar gui like the importfilter, so it can also be seen 
%as a template for future exportfilter.
%This files should be located in the folder +exportfilter . There also is 
%a folder +export which is used for general exporting function, it may be kick
%out of the mapseis later.

%Typical call: exportNTSCatalog(dataStore,[],'./SuperCatalog.nts','-file')

%The exported catalog may look different to the original imported one (from nts
%file), as some of the fields have quite a large "range" in the field and the 
%number sometimes are then written at the end or beginning of the range. It should
%still work however.


%% Import helper functions
import mapseis.util.importfilter.*;
import mapseis.projector.*;

onlydescription=false;
InfoOut=[];
if nargin>=4
    if strcmp(varargin{1},'-file')
       
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	InfoOut.description = 'This exportfilter writes the datastore into a nts formated ascii file as used by STEP model (java version). At the moment only fields used by STEP are supported.';
    	InfoOut.displayname = 'Nts formated-ascii catalogs v1' ;
    	InfoOut.filetype = '*.nts'
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	
	if isempty(rowIndices)
		%use all events
		  rowIndices = true(dataStore.getRowCount(),1);
	end
	
	%only original locations are supported at the moment (more useful anyway)
	PaleRider=true;
	
	
	%get the data from datastore
	%---------------------------
	
	%zmap catalog is a good start basis
	ZmapCat=getZMAPFormat(dataStore,rowIndices,PaleRider);
	
	
	
	%MilliSecond have to treated specially
	try
		rawMilliSec=dataStore.getUserData('MilliSecond');
		
	catch
		temp=dataStore.getFields('dateNum');
		dateNumer=temp.dateNum;
		rawMilliSec=str2num(datestr(dateNumer,'FFF'));
		dataStore.setUserData('MilliSecond',rawMilliSec);
		disp('Buffered MilliSecond')
	end	
	ZmapCat(:,11) = rawMilliSec(rowIndices);
	
	%write out the parts
	Year=floor(ZmapCat(:,3));
	Month=ZmapCat(:,4);
	Day=ZmapCat(:,5);
	Hour=ZmapCat(:,8);
	Minute=ZmapCat(:,9);
	Second=ZmapCat(:,10)*10;
	MilliSec=round(ZmapCat(:,11)/100);
	
	%This is to prevent 10 tenth seconds, which would cause and error later
	%By rounding them down to 0.9sec instead of 1sec a small error is done, 
	%but the otherway round would cause a difficult correction in some cases
	%(e.g. 1999.12.31.23:59:59.999), and the error is actually very small. 
	MilliSec(MilliSec==10)=9;
	
	Latitude=round(ZmapCat(:,2)*10000);
	Longitude=round(ZmapCat(:,1)*10000);
	Depth=round(ZmapCat(:,7)*10);
	Magnitude=round(ZmapCat(:,6)*10);
	
	%get rest of the catalog
	eventStruct=dataStore.getFields([],rowIndices);
	
	%length of catalog
	catlength=length(Year);
	
	
	%check if there is some additional data available (if it was a nts catalog
	%in where the data was imported from).
	imNTS=	isfield(eventStruct,'EventType')&isfield(eventStruct,'EventID')&...
		isfield(eventStruct,'DataSource')&isfield(eventStruct,'EventVersion');
	%At the moment I'm using this 4 fields to check if it was a .nts catalog
	%may have to change that if those fieldnames are reused somewhere else, 
	%which I will probably avoid now.
	

		
	
	if imNTS
		%it was a .nts or similar catalog where the date where imported from
		%so the additional fields should be available.
		EventType=eventStruct.EventType;
		EventID=eventStruct.EventID;
		DataSource=eventStruct.DataSource;
		EventVersion=eventStruct.EventVersion;
		
		HorzError=eventStruct.HorzError*10;
		VertError=eventStruct.VertError*10;
		
		MagType=eventStruct.MagType;
		MagError=eventStruct.MagError*10;
		
	else
		%Some fields are missing, some generic data has to be made up,
		%maybe in future release it would be possible to use the routing
		%for exporting additional data of none-nts catalog
		
		
		EventType=repmat({'E'},catlength,1);
		EventID=repmat({'11223344'},catlength,1);
		DataSource=repmat({'MA'},catlength,1);
		EventVersion=repmat({'1'},catlength,1);
		
		HorzError=repmat(0,catlength,1);
		VertError=repmat(0,catlength,1);
		
		MagType=repmat({'L'},catlength,1);
		MagError=repmat(0,catlength,1);
	end
	

	
	%Build a raw line of 80 spaces
	rawLine=repmat(' ',1,80);
	
	
	%open file to write in
	fid1 = fopen(FileName,'w'); 
	
	
	%Go through the catalog and write to file
	for i=1:catlength
		
		%Java code from STEP
		%  String sEventId = obsEqkEvent.substring(2, 10).trim();
		%  String sDataSource = obsEqkEvent.substring(10, 12).trim();
		%  char sEventVersion = obsEqkEvent.substring(12, 13).trim().charAt(0);
		%  String sYear = obsEqkEvent.substring(13, 17).trim();
		%  String sMonth = obsEqkEvent.substring(17, 19).trim();
		%  String sDay = obsEqkEvent.substring(19, 21).trim();
		%  String sHour = obsEqkEvent.substring(21, 23).trim();
		%  String sMinute = obsEqkEvent.substring(23, 25).trim();
		%  String sSecond = divideAndGetString(obsEqkEvent.substring(25, 28), 10);
		%  //sSecond = sSecond.substring(0, sSecond.indexOf('.')); //This is to get rid of the decimal point in seconds
		%  String sLatitude = divideAndGetString(obsEqkEvent.substring(28, 35),
					%  10000);
		%  String sLongitude = divideAndGetString(obsEqkEvent.substring(35, 43),
					%  10000);
		%  String sDepth = divideAndGetString(obsEqkEvent.substring(43, 47), 10);
		%  double lat = Double.parseDouble(sLatitude);
		%  double lon = Double.parseDouble(sLongitude);
		%  double depth = Double.parseDouble(sDepth);
%  
		%  //if lat or lon of the events are outside the region bounds then neglect them.
		%  if(lat < RegionDefaults.searchLatMin || lat >RegionDefaults.searchLatMax)
			%  return null;
		%  if(lon < RegionDefaults.searchLongMin || lon > RegionDefaults.searchLongMax)
			%  return null;
%  
		%  String sMagnitude = divideAndGetString(obsEqkEvent.substring(47, 49), 10);
		%  //if(sMagnitude ==null || sMagnitude.equals(""))
		%  //return null;
		%  //String sNst = sRecord.substring(49, 52).trim();
		%  //String sNph = sRecord.substring(52, 55).trim();
		%  //String sDmin = divideAndGetString(sRecord.substring(55, 59), 10);
		%  //String sRmss = divideAndGetString(sRecord.substring(59, 63), 100);
		%  String sErho = divideAndGetString(obsEqkEvent.substring(63, 67), 10);
		%  String sErzz = divideAndGetString(obsEqkEvent.substring(67, 71), 10);
		%  //String sGap = multiplyAndGetString(sRecord.substring(71, 73), 3.6);
		%  String sMagnitudeType = obsEqkEvent.substring(73, 74).trim();
		%  //String sNumberOfStations = obsEqkEvent.substring(74, 76).trim();
		%  String sMagnitudeError = divideAndGetString(obsEqkEvent.substring(76, 78),
					%  10);
		
					
		%example Line
%E 11111111CI62008 4010325 24 342133-1185370  5310.0                     0L  10
%E 11223344MA1200906021106348 638925-222926   39 7              0   0     L  0   
					
		%go through all of 5 the lines
		%-----------------------------
		%=============================
		
		%I'm not hungarian... no s,i,f,m,v ...
		
		
		%New Line
		NewLine=rawLine;
		
		
		%Write into the line 
		%----------------
		NewLine(1)=EventType{i,1};
		NewLine(3:10)=EventID{i,1};
		NewLine(11:12)=DataSource{i,1};
		NewLine(13:13)=EventVersion{i,1};
		
		
		
		
		%date and time
		%-------------
		NewLine(14:17)=num2str(Year(i,1));
		if Month(i,1)>=10
			NewLine(18:19)=num2str(Month(i,1));
		else
			NewLine(18:19)=['0',num2str(Month(i,1))];
		end
		
		if Day(i,1)>=10
			NewLine(20:21)=num2str(Day(i,1));
		else
			NewLine(20:21)=['0',num2str(Day(i,1))];
		end
		
		
		if Hour(i,1)>=10
			NewLine(22:23)=num2str(Hour(i,1));
		else
			NewLine(22:23)=['0',num2str(Hour(i,1))];
		end
		
		
		if Minute(i,1)>=10
			NewLine(24:25)=num2str(Minute(i,1));
		else
			NewLine(24:25)=['0',num2str(Minute(i,1))];
		end
		
		
		if Second(i,1)>=100
			NewLine(26:28)=num2str(Second(i,1));
		elseif Second(i,1)>=10
			NewLine(26:28)=['0',num2str(Second(i,1))];
		else
			NewLine(26:28)=['0',num2str(Second(i,1)),'0'];
		end
		
		%and the tenth second
		NewLine(28:28)=num2str(MilliSec(i,1));
		
		
		
		%Locations
		%---------
		
		%this more complicated: possible Range lat 0 to -90 or 90
		%it can have up 7 digits (with '-') in the file if it is lower it should be 
		%buffered by 0 (changed to use ' ' instead of '0', should be safer against 
		%misread)
		rawLat=num2str(Latitude(i,1));
		
		if Latitude(i,1)>=0
			rawLat=[' ',rawLat]; 
		end
		
		if numel(rawLat)<7 
			rawLat=[repmat(' ',1,(7-numel(rawLat))),rawLat];
		end
		NewLine(29:35)=rawLat;
		
		
		%even a bit more difficult the long: 0 to -180 or 180
		rawLon=num2str(Longitude(i,1));
		
		if Longitude(i,1)>=0
			rawLon=[' ',rawLon];
		end
		
		if numel(rawLon)<8 
			rawLon=[repmat(' ',1,(8-numel(rawLon))),rawLon];
		end
		NewLine(36:43)=rawLon;
		
		
		%again...
		RawDepth=num2str(Depth(i,1));
		if numel(RawDepth)<4 
			RawDepth=[repmat(' ',1,(4-numel(RawDepth))),RawDepth];
		end
		NewLine(44:47)=RawDepth;
		
		
		%and again...
		RawHE=num2str(HorzError(i,1));
		if numel(RawHE)<4 
			RawHE=[RawHE,repmat(' ',1,(4-numel(RawHE)))];
		end
		NewLine(64:67)=RawHE;
		
		
		%got used to it
		RawVE=num2str(VertError(i,1));
		if numel(RawVE)<4 
			RawVE=[RawVE,repmat(' ',1,(4-numel(RawVE)))];
		end
		NewLine(68:71)=RawVE;
		
		
		
		%Magnitude
		%---------
		%here a bit simpler
		RawMag=num2str(Magnitude(i,1));
		
		%hopefully that not happens to often...
		if Magnitude(i,1)<0
			Mag=round(Magnitude(i,1)/10);
			if Mag<=-10
				Mag=-9;
			end
			RawMag=num2str(Mag);
			%disp(RawMag)
		end
		
		if numel(RawMag)<2
			RawMag=[' ',RawMag];
		end
		NewLine(48:49)=RawMag;
		
		
		%Something more norma
		NewLine(74:74)=MagType{i,1};
		
		%and the last one
		if MagError(i,1)<10
			NewLine(77:78)=[num2str(MagError(i,1)),' '];
		else
			NewLine(77:78)=num2str(MagError(i,1));
		end
		
		%Now write the line
		fprintf(fid1,'%s',NewLine); 
		fprintf(fid1,'\n');
	end
	
	
	%Close file
	fclose(fid1);
	
	
end

end

function InfoOut = exportCSEPCatalog(dataStore,rowIndices,FileName,varargin)
	%Used to export mapseis data into a ascii csep file, the format is very similar 
	%to the zmap catalog, it just has some additional rows.
	
	%Typical call: exportCSEPCatalog(dataStore,[],'./SuperCatalog.mat','-file')
	
	
	%% Import helper functions
	import mapseis.util.importfilter.*;
	import mapseis.projector.*;
	
	onlydescription=false;
	InfoOut=[];
	if nargin>=4
	    if strcmp(varargin{1},'-file')
	       
		
	    elseif strcmp(varargin{1},'-description')
		%new option: returns a description of the import filter for the gui
		InfoOut.description = 'This exportfilter writes the datastore into a CSEP formated ascii file (.dat) importable into Zmap and MapSeis';
		InfoOut.displayname = 'CSEP formated-ascii catalogs v1' ;
		InfoOut.filetype = '*.dat'
		onlydescription= true;
		
	    end
	
	end
	
	
	if ~onlydescription
		
		if isempty(rowIndices)
			%use all events
			  rowIndices = true(dataStore.getRowCount(),1);
		end
		
		%only original locations are supported at the moment (more useful anyway)
		PaleRider=true;
		
		
		%get the data from datastore
		%---------------------------
		
		%zmap catalog is a good start basis
		a=getZMAPFormat(dataStore,rowIndices,PaleRider);
		
		%check for additional fields
		
		TheFields=dataStore.getFieldNames;
		
		if isfield(TheFields,'horz_err')
			Temp=dataStore.getFields({'horz_err'},rowIndices);
			horz_err=Temp.horz_err;
			
			a(:,11)=horz_err;
		else
			a(:,11)=zeros(sum(rowIndices),1);
		end
		
		
		if isfield(TheFields,'depth_err')
			Temp=dataStore.getFields({'depth_err'},rowIndices);
			depth_err=Temp.depth_err;
			
			a(:,12)=depth_err;
		else
			a(:,12)=zeros(sum(rowIndices),1);
		end
		
		if isfield(TheFields,'mag_err')
			Temp=dataStore.getFields({'mag_err'},rowIndices);
			mag_err=Temp.mag_err;
			
			a(:,13)=mag_err;
		else
			a(:,13)=zeros(sum(rowIndices),1);
		end
		
		
		%There are two additional rows, which I have no clue what they
		%are all about
		%->just add them (14 seems to be 0 all the time, 15 mostly 1)
		a(:,14)=zeros(sum(rowIndices),1);
		a(:,15)=ones(sum(rowIndices),1);
		
		%save(FileName,'a','-ascii');
		dlmwrite(FileName, a, 'delimiter', '\t', ...
		 'precision', 12)
		
		 
		 %  
		 %  rowPat = [nm('lon',rp.floatPat),ws,...
		    %  nm('lat',rp.floatPat),ws,...
		    %  nm('decyear',rp.floatPat),ws,...
		    %  nm('month',rp.floatPat),ws,...
		    %  nm('day',rp.floatPat),ws,...
		    %  nm('mag',rp.floatPat),ws,...
		    %  nm('depth',rp.floatPat),ws,...
		    %  nm('hour',rp.floatPat),ws,...
		    %  nm('minute',rp.floatPat),ws,...
		    %  nm('second',rp.floatPat),ws,...
		    %  nm('horz_err',rp.floatPat),ws,...
		    %  nm('depth_err',rp.floatPat),ws,...
		    %  nm('mag_err',rp.floatPat)];
		%  
		
	end

end

function InfoOut = exportETASDeclustCatalog(dataStore,rowIndices,FileName,varargin)
	%Used to export mapseis data into a ascii csep file, the format is very similar 
	%to the zmap catalog, it just has some additional rows.
	
	%Typical call: exportCSEPCatalog(dataStore,[],'./SuperCatalog.mat','-file')
	
	
	%% Import helper functions
	import mapseis.util.importfilter.*;
	import mapseis.projector.*;
	
	onlydescription=false;
	InfoOut=[];
	if nargin>=4
	    if strcmp(varargin{1},'-file')
	       
		
	    elseif strcmp(varargin{1},'-description')
		%new option: returns a description of the import filter for the gui
		InfoOut.description = 'This exportfilter writes the datastore into a formated ascii file (.dat) used for ETAS stochastic declustering by Zhuang';
		InfoOut.displayname = 'ETAS-decluster formated-ascii catalogs v1' ;
		InfoOut.filetype = '*.dat'
		onlydescription= true;
		
	    end
	
	end
	
	
	if ~onlydescription
		
		%'formatted_for_etas'
		%1  142.53450   39.34330         4.6     0.00000    0.00         1926  1  8
		%2  141.52250   35.84350         5.6     2.74842  -24.00         1926  1 10
		%3  141.80380   36.36230         5.2     2.77104  -14.00         1926  1 10

	
		if isempty(rowIndices)
			%use all events
			  rowIndices = true(dataStore.getRowCount(),1);
		end
		
		%only original locations are supported at the moment (more useful anyway)
		PaleRider=true;
		
		
		%get the data from datastore
		%---------------------------
		
		%zmap catalog is a good start basis
		a=getZMAPFormat(dataStore,rowIndices,PaleRider);
		
		%check for additional fields
		temp=dataStore.getFields({'dateNum'},rowIndices);
		dater=temp.dateNum;
		
		%create output
		OutData(:,1)=(1:numel(a(:,1)))';
		OutData(:,2)=a(:,1);
		OutData(:,3)=a(:,2);
		OutData(:,4)=a(:,6);
		OutData(:,5)=dater-min(dater);
		OutData(:,6)=-a(:,7);
		OutData(:,7)=floor(a(:,3));
		OutData(:,8)=a(:,4);
		OutData(:,9)=a(:,5);
		

		%save(FileName,'a','-ascii');
		dlmwrite(FileName, OutData, 'delimiter', '\t', ...
		 'precision', 12)
		
		 
		 %  
		 %  rowPat = [nm('lon',rp.floatPat),ws,...
		    %  nm('lat',rp.floatPat),ws,...
		    %  nm('decyear',rp.floatPat),ws,...
		    %  nm('month',rp.floatPat),ws,...
		    %  nm('day',rp.floatPat),ws,...
		    %  nm('mag',rp.floatPat),ws,...
		    %  nm('depth',rp.floatPat),ws,...
		    %  nm('hour',rp.floatPat),ws,...
		    %  nm('minute',rp.floatPat),ws,...
		    %  nm('second',rp.floatPat),ws,...
		    %  nm('horz_err',rp.floatPat),ws,...
		    %  nm('depth_err',rp.floatPat),ws,...
		    %  nm('mag_err',rp.floatPat)];
		%  
		
	end

end

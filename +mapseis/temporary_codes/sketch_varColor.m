%Some Lines needed for ColorMapping

%set the plot color
set(scatMan,'CData',NCData)

%for colorbar
protoMaop(:,:,1)=uint8(round(NewCMap(:,1)*255))
protoMaop(:,:,2)=uint8(round(NewCMap(:,2)*255))
protoMaop(:,:,3)=uint8(round(NewCMap(:,3)*255))


%used to set the axis ticks right
caxis([min max])

%set the colorbar with new data
thebar=colorbar
realbar=get(thebar,'Children')
set(realbar,'CData',protoMaop)

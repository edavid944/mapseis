 function loadMapSeisData(obj)
            which DefaultFilters
            import mapseis.*;
            import mapseis.gui.*;
            import mapseis.datastore.*;
			%import mapseis.datastore.DefaultFilters;
			
            % Load an existing MapSeis session
            %[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
            
            	pName = './'
                fName = 'sed_eb7508_mapseis.mat'

            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected. ');
            else
                % Create a new dataStore for the data
                                
                prevData = load(fullfile(pName,fName));
                %prevData = load('./sed_eb7508_mapseis.mat');
               
                if isfield(prevData,'dataStore')
                    % We have loaded a saved session, including filter values
                    dataStore = prevData.dataStore;
                    try
                    	filterList = prevData.filterList
						filterList.ListProxy = obj.ListProxy;
                    catch
                    	filterList = DefaultFilters(dataStore,obj.ListProxy);
                    	%filterList = obj.FilterList;	
                    end
                    	
                elseif isfield(prevData,'dataCols')
                    % We have loaded only the data columns, which now need to be turned
                    % into an mapseis.datastore.DataStore
                    dataStore = mapseis.datastore.DataStore(prevData.dataCols);
                    setDefaultUserData(dataStore);
                    
                    %create new filterlist
               		filterList = DefaultFilters(dataStore,obj.ListProxy);

                elseif isfield(prevData,'a')
                    % We have loaded in a ZMAP format file with data in matrix 'a'
                    dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
                    setDefaultUserData(dataStore)
                    
                    %create new filterlist
               		filterList = DefaultFilters(dataStore,obj.ListProxy);
                    
                else
                    error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');
                end
                
                %create new filterlist
               		%filterList = DefaultFilters(dataStore,obj.ListProxy);
                	%filterList = obj.FilterList
                % names for saving in the datastore
                	fullname=fullfile(pName,fName);
                	nameStr=fName;
   			
           			if isempty(nameStr) 
            				nameStr= 'noname';
           		 	end	 
            
            		dataentry = struct('Name',nameStr,...
            				  		   'Datastore',dataStore,...
            				           'Filterlist',filterList);
            
           			obj.DataHash.set(num2str(obj.NextID),dataentry);
            		obj.CurrentDataID = obj.NextID;
            		obj.NextID = obj.NextID+1;

                
                
                % Set the filterList to refer to this dataStore
                	
                	try %check wether the datastore is already buffered
                		temp=dataStore.getUserData('DecYear');
                		catch
                		dataStore.bufferDate;
                	end
                	
                	try %check wether the plot quality is already set
                		temp=datStore.getUserData('PlotQuality');
                		catch
                		dataStore.SetPlotQuality;
                	end
                		
                obj.FilterList=filterList;	
                obj.ParamGUI.FilterList=filterList;
                obj.FilterList.update();
                
                % Get the data out of the file we have loaded
                %newData = dataStore.getFields();
                
                % Set the data of the current DataStore property to be the
                % data that we've just loaded in.
                %obj.DataStore.setData(newData);
				obj.DataStore = dataStore;
                %                 % Create the main GUI window showing event locations, magnitudes and times
                %                 guiWindowStruct.mainStruct=GUI(dataStore);
                %
                %                 % Create the GUI for setting filter parameters and other analysis
                %                 % parameters
                %                 guiWindowStruct.mainParamStruct=ParamGUI(dataStore);
            
            	%reapply the listeners
            	relisten(obj);
            	
            	% Update the GUI to plot the initial data
		        updateGUI(obj);
            
            end
            
            
        end


		
		function selectionList = createList(obj,WhichOne)
			%old list creater from Commander GUI, does not work
			
			%creates the list for the popups menu with the datahash
			selectionList = cell(1,2);	 	
					
			switch WhichOne
			
				case 'Cata'
					keys=DataHash.getKeys;
		
					for i=1:1:numel(keys)
						datatemp=DataHash.lookup(keys(i));
						dataname = datatemp.Name;
						selectionList{i,1} = dataname;
						selectionList{i,2} = @(s,e) selecta(keys(i),'Cata');		
				
					end
		

					
					
				case 'FilterList'
					keys=FilterHash.getKeys;
					
					for i=1:1:numel(keys)
						filtertemp=FilterHash.lookup(keys(i));
						filtername = filtertemp.Name
						selectionList{i,1} = filtername;
						selectionList{i,2} = @(s,e) obj.selecta(keys(i),'Filter');		
				
					end	
			end	
				
		
		end
		
%calls plot-figloglike_rate and calculates rates and omoriparameters

% Get input parameters
global cval
global newt2
disp('This is call_llforecastrates')
if length(maepi(:,1)) > 1 ; maepi = maepi(1,:) ; end 
prompt  = {'Enter model number (1:pck, 2:pckk, 3:ppckk, 4:ppcckk: 5:pk, fixed c'};
title   = 'Model selection for fitting aftershock sequence';
lines= 1;
def     = {'1'};
answer  = inputdlg(prompt,title,lines,def);
nMod = str2double(answer{1});

if nMod==5
    prompt  = {'Enter c value'};
    title   = 'Fixed c value';
    lines= 1;
    def     = {'0.01'};
    answer  = inputdlg(prompt,title,lines,def);
    cval = str2double(answer{1});
end

prompt  = {'Enter length of learning period (days)','Enter forecast period (days):','Enter number of bootstraps:', 'Enter Background Rate (evt/days)'};
title   = 'Parameters ';
lines= 1;
def     = {'200','50000','100', '3.3837e-04'};
answer  = inputdlg(prompt,title,lines,def);
time = str2double(answer{1});
timef = str2double(answer{2});
bootloops = str2double(answer{3});
bgd=str2double(answer{4});

[tavgrlp, tavgrall,tavgrfc, texp,rate_bstmod, rate_fc ,rate_med, rate_lp ,rate_exp, rate_bstmexp, ratelp, ratefc, ompara, kspara]= plot_fitloglike_rate(newt2,time,timef,bootloops,maepi,bgd, nMod, 1);

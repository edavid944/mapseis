function [tavgrlp, tavgrall,tavgrfc, texp,rate_bstmod, rate_fc ,rate_med, rate_lp ,rate_exp, rate_bstmexp, ratelp, ratefc, ompara, kspara]=plot_fitloglike_rate(a,time,timef,bootloops,maepi, bgd, nMod, isplot);
% function plot_fitloglike_a2(a,time,timef,bootloops,maepi);
% --------------------------------------------------
% Plots Rates for observed and modelled data for specified time windows
% with choosing the model for the learning period
% Input variables:
% a         : earthquake catalog
% time      : learning period fo fit Omori parameters
% timef     : forecast period
% bootloops : Number of bootstraps
% maepi     : mainshock
% bgd       : background rate
% nMod      : Model to fit the data
%
%Output
% tavgrlp           : Learning period
% tavgrall          : Time up to the forecast time
% tavgrfc           : Time after end of learning period
% rate_bstm         : bootstrap models
% rate_fc           : forecasted model
% rate_med          : forecasted  mean model
% rate_lp           : model to data within learning period
% rate_exp          : extrapolated mean model, if time+timef > time_all
% ratelp, fc        : binned rates lp = within learning perdio, fc= after
% ompara            : [p1, std(p1), c1, std(c1), k1, std(k1)] or [p1,
%                      std(p1), p2, std(p2)...]
% kspara            : [H KSSTAT P fRMS fAIC fStdBst fDiff_max]
%
%
% C. Bachmann, last update 07.08.07

% Surpress warnings from fmincon
warning off;

global cval
%
%%

%convert date into matlab time, use first line of maepi as mainshock
date_matlab = datenum(floor(a(:,3)),a(:,4),a(:,5),a(:,8),a(:,9),zeros(size(a,1),1));
date_main = datenum(floor(maepi(1,3)),maepi(1,4),maepi(1,5),maepi(1,8),maepi(1,9),0);
time_aftershock = date_matlab-date_main;

% Select biggest aftershock earliest in time, but more than 1 day after mainshock
fDay = 1;
ft_c=fDay/365; % Time not considered to find biggest aftershock
vSel = (a(:,3) > maepi(1,3)+ft_c & a(:,3)<= maepi(1,3)+time/365);
mCat = a(vSel,:);
vSel = mCat(:,6) == max(mCat(:,6));
vBigAf = mCat(vSel,:);
if length(mCat(:,1)) > 1
    vSel = vBigAf(:,3) == min(vBigAf(:,3));
    vBigAf = vBigAf(vSel,:);
end;

date_biga = datenum(floor(vBigAf(3)),vBigAf(4),vBigAf(5),vBigAf(8),vBigAf(9),0);
fT1 = date_biga - date_main; % Time of big aftershock


% Aftershock times
l = time_aftershock(:) > 0;
tas = time_aftershock(l);
eqcatalogue = a(l,:);

% time_lp: Learning period
l = tas <= time+1;
time_lp=tas(l);

% time_all: from zero up to end forecast period
lf = tas <= time+timef ;
time_all= [tas(lf) ];
time_all=sort(time_all);

%time_fc: from end learning period to end forecast period
lfc = time_all >time-1 ;
time_fc=time_all(lfc);


%make bins (exponential binning)

numvlp=[];numvall=[];numvfc=[];

tminlp = min(time_lp); tmaxlp = max(time_lp);
tminall = min(time_all); tmaxall = max(time_all);
tminfc = min(time_fc); tmaxfc = max(time_fc);
tintlp = [tminlp tmaxlp];
tintall = [tminall tmaxall];
tintfc = [tminfc tmaxfc];
power = -12:0.25:12;
lenPow = length(power);


%only events within tmin and tmax
for i = 1:lenPow
    Tbinslp(i) = 2^(power(i));
    if ((Tbinslp(i) <= tminlp) | (Tbinslp(i) >= tmaxlp))
        Tbinslp(i) = 0;
    end
    Tbinsall(i) = 2^(power(i));
    if ((Tbinsall(i) <= tminall) | (Tbinsall(i) >= tmaxall))
        Tbinsall(i) = 0;
    end
    if isempty(tminfc) == 0
        Tbinsfc(i) = 2^(power(i));
        if ((Tbinsfc(i) <= tminfc) | (Tbinsfc(i) >= tmaxfc))
            Tbinsfc(i) = 0;
        end
    else
        Tbinsfc=[];
    end
end

%delete events with time zero
limit1 = (Tbinslp > 0);
Tbinslp = Tbinslp(:,limit1);
limit2 = (Tbinsall > 0);
Tbinsall = Tbinsall(:,limit2);
limit3 = (Tbinsfc > 0);
Tbinsfc = Tbinsfc(:,limit3);

Tbinslp = Tbinslp';
Tbinsall = Tbinsall';
Tbinsfc = Tbinsfc';

lenPowlp = length(Tbinslp);
lenPowall=length(Tbinsall);
lenPowfc=length(Tbinsfc);


%durations and averages times
for j = 1:(lenPowlp-1)
    durlp(j) = Tbinslp(j+1) - Tbinslp(j);
    tavglp(j) = (Tbinslp(j+1)*Tbinslp(j))^(0.5);
end
for j = 1:(lenPowall-1)
    durall(j) = Tbinsall(j+1) - Tbinsall(j);
    tavgall(j) = (Tbinsall(j+1)*Tbinsall(j))^(0.5);
end
%check if more than one event is in the forecast bin 
if lenPowfc>1
    for j= 1:(lenPowfc-1)
    durfc(j) = Tbinsfc(j+1) - Tbinsfc(j);
    tavgfc(j) = (Tbinsfc(j+1)*Tbinsfc(j))^(0.5);
    end
else
    durfc = max(time_fc)-min(time_fc);
    tavgfc = Tbinsfc;
end
    

%get amount of times within Tbins(j) and Tbins(j+1)
for j = 1:(lenPowlp-1)
    num1=sum(time_lp>Tbinslp(j)&time_lp<Tbinslp(j+1));
    numvlp = [numvlp; num1];
    num1 = 0; %#ok<NASGU>
end
for j = 1:(lenPowall-1)
    num2=sum(time_all>Tbinsall(j)&time_all<Tbinsall(j+1));
    numvall = [numvall; num2];
    num2 = 0; %#ok<NASGU>
end

%check if more than one event is in the forecast bin 
if lenPowfc>1
    for j = 1:(lenPowfc-1)
    num3=sum(time_fc>Tbinsfc(j)&time_fc<Tbinsfc(j+1));
    numvfc = [numvfc; num3];
    num3 = 0; %#ok<NASGU>
    end
else
    numvfc = length(Tbinsfc);
end

numvlp = numvlp';
numvall = numvall';
numvfc=numvfc';

%calculate rates (number / duration)
for j = 1:(lenPowlp-1)
    ratelp(j) = numvlp(j)/durlp(j);
end
for j = 1:(lenPowall-1)
    rateall(j) = numvall(j)/durall(j);
end
if lenPowfc>1
    for j = 1:(lenPowfc-1)
    ratefc(j) = numvfc(j)/durfc(j);
    end
else
    ratefc=numvfc/durfc;
end

%add beginning and end time to average times
tavgrlp = [tintlp(1) tavglp tintlp(2)];
if lenPowfc >1
    tavgrfc = [tintfc(1) tavgfc tintfc(2)];
else
    %end of learning period until end of all events
    tavgrfc = [tintlp(2) tintall(2)];
end
tavgrall = [tintall(1) tavgall tintall(2)];



% Calculate uncertainty and mean values of p,c,and k
[mMedModF, mStdL, loopout] = brutebootloglike_a2(time_lp, time_all, bootloops,fT1,nMod);
pmed1 = mMedModF(1,1);
pmed2 = mMedModF(1,3);
cmed1 = mMedModF(1,5);
cmed2 = mMedModF(1,7);
kmed1 = mMedModF(1,9);
kmed2 = mMedModF(1,11);



% Compute model parameters p,c,k according to model choice
if nMod == 5
    [pval1, pval2, cval1, cval2, kval1, kval2, fAIC, fL] = bruteforceloglike_a2(time_lp,fT1,nMod);
elseif nMod == 1
    [pval1, pval2, cval1, cval2, kval1, kval2, fAIC, fL] = bruteforceloglike_a2(time_lp,fT1,nMod);
elseif nMod == 2
    [pval1, pval2, cval1, cval2, kval1, kval2, fAIC, fL] = bruteforceloglike_a2(time_lp,fT1,nMod);
elseif nMod == 3
    [pval1, pval2, cval1, cval2, kval1, kval2 , fAIC, fL] = bruteforceloglike_a2(time_lp,fT1,nMod);
else
    [pval1, pval2, cval1, cval2, kval1, kval2, fAIC, fL] = bruteforceloglike_a2(time_lp,fT1,nMod);
end;

% Calculate goodness of fit with KS-Test and RMS
[H,P,KSSTAT,fRMS] = calc_llkstest_a2(time_lp,fT1,pval1, pval2, cval1, cval2, kval1, kval2, nMod);


if (isnan(pval1) == 0 && isnan(pval2) == 0)
    loopout = [ loopout , loopout(:,1)*0];
   
%% Calculate bootstraped rates
    rate_bstmod=[];
    for j = 1:length(loopout(:,1));
        %cumnr = (1:length(time_asf))';
        rate_bstm = [];
        pval1 = loopout(j,1);
        pval2 = loopout(j,2);
        cval1 = loopout(j,3);
        cval2 = loopout(j,4);
        kval1 = loopout(j,5);
        kval2 = loopout(j,6);
        %for k = 1:length(tavgrall)
        %    if nMod ==1 || nMod ==5
        %        rb1 = kval1 / ((tavgrall(k) + cval1)^pval1);
        %    else
        %        rb = kval1 / ((tavgrall(k) + cval1)^pval1) +  kval2 / ((tavgrall(k) + cval2)^pval2);
        %    end
        % rate_bstm = [rate_bstm; rb1];

        %end;

        if nMod ==1 || nMod ==5
            rate_bstm=kval1 ./ ((time_all + cval1).^pval1);
        else
            rate_bstm = kval1 ./ ((tavgrall + cval1).^pval1) +  kval2 ./ ((tavgrall(k) + cval2).^pval2);
        end


        %collect all bootstrap models in rate_bstmod for plotting and
        %output
        rate_bstmod=[rate_bstmod rate_bstm];
    end
    
    
    % 2nd moment of bootstrap number of forecasted number of events
    fStdBst = calc_StdDev(loopout(:,9));


%% Calculate the forecasted model
rate_fc = [];
if nMod == 1 || nMod == 5
        rb = kval1 ./ ((tavgrfc + cval1).^pval1);
        rate_fc = rb';
    else
    for i=1:length(tavgrfc)
        %check if time is smaller than second aftershock
        if tavgrfc(i) <= fT1
            rb = kval1 / ((tavgrfc(i) + cval1)^pval1);
            rate_fc = [rate_fc; rb];

        else %if time is larger than larger aftershock
            rb = kval1 / ((tavgrfc(i) + cval1)^pval1) +  kval2 / ((tavgrfc(i) + cval2)^pval2);
            rate_fc = [rate_fc; rb];
        end;

    end;

end
    %% calculate extrapolated model
    if (time+timef)>max(tavgrall)
        dt=(timef-max(tavgrall))/50;
        texp=max(tavgrall):dt:(time+timef);

        if nMod == 1 || nMod == 5
            for i=1:length(texp)
                rb = kval1 / ((texp(i) + cval1)^pval1);
                rate_fc = [rate_fc; rb];
            end % END of FOR on length(time_asf)
        else
            for i=1:length(texp)
                %check if time is smaller than second aftershock
                if texp(i) <= fT1
                    rb = kval1 / ((texp(i) + cval1)^pval1);
                    rate_fc = [rate_fc; rb];

                else %if time is larger than larger aftershock
                    rb = kval1 / ((texp(i) + cval1)^pval1) +  kval2 / ((texp(i) + cval2)^pval2);
                    rate_fc = [rate_fc; rb];
                end;

            end;

        end
    end
    
    texp_fc=[tavgrfc';texp'];
    

%% Calculate fit to observed data

    rate_lp = [];
    if nMod == 1 || nMod ==5
        for i=1:length(tavgrlp)
            rb = kval1 / ((tavgrlp(i) + cval1)^pval1);
            rate_lp = [rate_lp; rb];
        end
    else
        for i=1:length(tavgrlp)
            if tavgrlp(i) <= fT1
                rb = kval1 / ((tavgrlp(i) + cval1)^pval1);
                rate_lp = [rate_lp; rb];
            else
                rb = kval1 / ((tavgrlp(i) + cval1)^pval1)+ kval2 / ((tavgrlp(i) + cval2)^pval2) ;
                rate_lp = [rate_lp; rb];
            end;
        end; 
    end; 

%% Calculate forecast from median value
    rate_med = [];
    if nMod == 1 || nMod ==5
        for i=1:length(tavgrall)
            rb = kmed1 / ((tavgrall(i) + cmed1)^pmed1);
            rate_med=[rate_med; rb];
        end
    else
        for i=1:length(tavgrall)
            if tavgrall(i) <= fT1
                rb = kmed1 / ((tavgrall(i) + cmed1)^pmed1);
                rate_med=[rate_med; rb];
            else
                rb = kmed1 / ((tavgrall(i) + cmed1)^pmed1) +  kmed2 / ((tavgrall(i) + cmed2)^pmed2);
                rate_med=[rate_med; rb];
            end
        end
    end
    
%%  calculate extrapolated median model  
if (time+timef)>max(tavgrall)
    dt=(timef-max(tavgrall))/50;
    texp=max(tavgrall):dt:(time+timef);
    rate_exp=[];rate_bstmodexp=[];
    if nMod == 1 || nMod ==5
        for i=1:length(texp)
            rb = kmed1 / ((texp(i) + cmed1)^pmed1);
            rate_exp=[rate_exp; rb];
        end
    else
        for i=1:length(texp)
            if texp(i) <= fT1
                rb = kmed1 / ((texp(i) + cmed1)^pmed1);
                rate_exp=[rate_exp; rb];
            else
                rb = kmed1 / ((texp(i) + cmed1)^pmed1) +  kmed2 / ((texp(i) + cmed2)^pmed2);
                rate_exp=[rate_exp; rb];
            end
        end
    end
    for j = 1:length(loopout(:,1));

        rate_bstmexp = [];
        pval1 = loopout(j,1);
        pval2 = loopout(j,2);
        cval1 = loopout(j,3);
        cval2 = loopout(j,4);
        kval1 = loopout(j,5);
        kval2 = loopout(j,6);
        for k = 1:length(texp)
            if nMod ==1 || nMod ==5
                rb = kval1 / ((texp(k) + cval1)^pval1);
            else
                rb = kval1 / ((texp(k) + cval1)^pval1) +  kval2 / ((texp(k) + cval2)^pval2);
            end
        rate_bstmexp = [rate_bstmexp; rb];
        end;

        %collect all bootstrap models in rate_bstmod for plotting and
        %output
        rate_bstmodexp=[rate_bstmodexp rate_bstmexp];
    end
else rate_exp=[];rate_bstmodexp=[];
end

%%

    % Difference of modeled and observed number of events at time_as
    %fDiff_timeas = rate_med(length(tavglp))-ratelp(length(tavglp));
    %ratefc = ratefc+fDiff_timeas;

    % Find amount of events in forecast period for modeled data
    nummod = max(rate_med)-rate_med(length(tavglp));

    % Find amount of  events in forecast period for observed data
    l = time_all <=time+timef & time_all > time;
    numreal = sum(l); % observed number of aftershocks

    %fRc_Bst2 = (numreal-nummod2)/fStdBst;
    fRc_Bst = (numreal-nummod)/fStdBst;

    %Get difference between median forecasted and observed rate at the end of the
    %forecast period
    for i=1:length(loopout(:,1));
        if size(ratefc,2)>1
            fDiff_all(i)=rate_bstmod(((size(ratelp,2) +size(ratefc,2))-1),i) - ratefc(size(ratefc,1));
        else
            fDiff_all(i)=rate_bstmod(size(rate_med,2),i);
        end
    end
    fDiff_med=median(fDiff_all);
    fDiff_std=std(fDiff_all);
    
    %fDiff_max=rate_med(length(rate_med)) - ratefc(length(ratefc));

    if nMod ==1 || nMod == 5
        ompara=[pval1 mStdL(1,1) cval1 mStdL(1,3) kmed1 mStdL(1,5)];
    else
        ompara=[pval1 mStdL(1,1) pval2 mStdL(1,2) cval1 mStdL(1,3) cval2 mStdL(1,4) kmed1 mStdL(1,5) kmed2 mStdL(1,6)];
    end

    kspara=[H KSSTAT P fRMS fAIC fStdBst fDiff_med fDiff_std];

%%  plotting
    %start plotting
    if isplot ==1
        pfig=figure('Numbertitle','off','Name','Forecast aftershock occurence','Position',[ 200 200 600 600]); %#ok<NASGU>


        pbst=loglog(time_all, rate_bstmod,'color',[0.8 0.8 0.8]); %#ok<NASGU>
        hold on
        if (time+timef)>max(tavgrall)
            pbstex=loglog(texp, rate_bstmodexp,'color',[0.8 0.8 0.8]); %#ok<NASGU>
            %pexp=loglog(texp, rate_exp, 'color', [255/255 193/255 37/255],'linestyle', '-.','Linewidth',2); %#ok<NASGU>
        end
        
        pf1=loglog(texp_fc, rate_fc, 'color', [50/255 205/255 50/255],'linestyle', '-.','Linewidth',2);

        
        
        pmed=loglog(tavgrall, rate_med,'color', [255/255 193/255 37/255],'linestyle','-.','Linewidth',2);
        
        p1=loglog(tavgrlp, rate_lp, 'r','Linewidth',2,'Linestyle','--');
        p2=loglog(tavgrlp(1:length(ratelp)), ratelp,  '-b','LineStyle', 'none', 'Marker', 'o','MarkerSize',7);
        hold on
        pf3=loglog(tavgrfc(1:length(ratefc)), ratefc,  '-m','LineStyle', 'none', 'Marker', 'o','MarkerSize',7);
        xlabel('Time from Mainshock (days)','FontWeight','bold','FontSize',12);
        ylabel('No. of Earthquakes / Day','FontWeight','bold','FontSize',12);
        set(gca, 'xlim', [0 time+timef]);
        xx=get(gca, 'xlim');
        bg1=bgd+bgd/3;
        bg2=bgd-bgd/3;
        pbg=plot([min(tavgrall) xx(2)],[bgd bgd],  'color', [32/255 178/255 170/255], 'Linestyle', '-');
        plot([min(tavgrall) xx(2)],[bg1 bg1],  'color', [32/255 178/255 170/255], 'Linestyle', '--');
        plot([min(tavgrall) xx(2)],[bg2 bg2],  'color', [32/255 178/255 170/255], 'Linestyle', '--');
        yy = get(gca,'ylim');
        plot([max(tavgrlp) max(tavgrlp)], [yy(1) yy(2)], 'k', 'Linestyle', '-.')


           % Round values for output

        pval1 = round(100*pval1)/100;
        pval2 = round(100*pval2)/100;
        cval1 = round(1000*cval1)/1000;
        cval2 = round(1000*cval2)/1000;
        kval1 = round(10*kval1)/10;
        kval2 = round(10*kval2)/10;
        pmed1 = round(100*pmed1)/100; mStdL(1,1) = round(100*mStdL(1,1))/100;
        pmed2 = round(100*pmed2)/100; mStdL(1,2) = round(100*mStdL(1,2))/100;
        cmed1 = round(1000*cmed1)/1000; mStdL(1,3) = round(1000*mStdL(1,3))/1000;
        cmed2 = round(1000*cmed2)/1000; mStdL(1,4) = round(1000*mStdL(1,4))/1000;
        kmed1 = round(10*kmed1)/10; mStdL(1,5) = round(100*mStdL(1,5))/100;
        kmed2 = round(10*kmed2)/10; mStdL(1,6)= round(100*mStdL(1,6))/100;
        fAIC = round(100*fAIC)/100;
        fStdBst = round(100*fStdBst)/100;
        fRc_Bst = round(100*fRc_Bst)/100;
        
        %Text with Results
        xx=get(gca, 'xlim');
        if nMod == 1 || nMod == 5
            string1=['p = ' num2str(pval1) '; c = ' num2str(cval1) '; k = ' num2str(kval1) ];
            string3=['pm = ' num2str(pmed1) '+-' num2str(mStdL(1,1)) '; cm = ' num2str(cmed1) '+-' num2str(mStdL(1,3)) '; km = ' num2str(kmed1) '+-' num2str(mStdL(1,5))];
            text(min(tavgrall)+1/xx(2),(yy(2)*0.6),string1,'FontSize',10, 'EdgeColor', 'r');
            text(min(tavgrall)+1/xx(2),yy(2)*0.35,string3,'FontSize',10, 'EdgeColor',  [255/255 193/255 37/255]);
        elseif nMod == 2
            string1=['p = ' num2str(pval1) '; c = ' num2str(cval1) '; k1 = ' num2str(kval1) '; k2 = ' num2str(kval2) ];
            string3=['pm = ' num2str(pmed1) '+-' num2str(mStdL(1,1)) '; cm = ' num2str(cmed1) '+-' num2str(mStdL(1,3)) '; km1 = ' num2str(kmed1) '+-' num2str(mStdL(1,5)) '; km2 = ' num2str(kmed2) '+-' num2str(mStdL(1,6))];
            text(min(tavgrall)+1/xx(2),(yy(2)*0.6),string1,'FontSize',10, 'EdgeColor', 'r');
            text(min(tavgrall)+1/xx(2),yy(2)*0.35,string3,'FontSize',10, 'EdgeColor',  [255/255 193/255 37/255]);
        elseif nMod == 3
            string1=['p1 = ' num2str(pval1) '; c = ' num2str(cval1) '; k1 = ' num2str(kval1) ];
            string2=['p2 = ' num2str(pval2) '; k2 = ' num2str(kval2) ];
            string3=['pm1 = ' num2str(pmed1) '+-' num2str(mStdL(1,1)) '; cm = ' num2str(cmed1) '+-' num2str(mStdL(1,3)) '; km1 = ' num2str(kmed1) '+-' num2str(mStdL(1,5))];
            string4=['pm2 = ' num2str(pmed2) '+-' num2str(mStdL(1,2)) '; km2 = ' num2str(kmed2) '+-' num2str(mStdL(1,6))];
            text(min(tavgrall)+1/xx(2),(yy(2)*0.6),string1,'FontSize',8, 'EdgeColor', 'r');
            text(min(tavgrall)+1/xx(2),yy(2)*0.43,string2,'FontSize',8, 'EdgeColor', 'r');
            text(min(tavgrall)+1/xx(2),(yy(2)*0.32),string3,'FontSize',8, 'EdgeColor',  [255/255 193/255 37/255]);
            text(min(tavgrall)+1/xx(2),yy(2)*0.23,string4,'FontSize',8, 'EdgeColor',  [255/255 193/255 37/255]);
        else
            string1=['p1 = ' num2str(pval1) '; c1 = ' num2str(cval1) '; k1 = ' num2str(kval1) ];
            string2=['p2 = ' num2str(pval2) '; c2 = ' num2str(cval2) '; k2 = ' num2str(kval2) ];
            string3=['pm1 = ' num2str(pmed1) '+-' num2str(mStdL(1,1)) '; cm1 = ' num2str(cmed1) '+-' num2str(mStdL(1,3)) '; km1 = ' num2str(kmed1) '+-' num2str(mStdL(1,5))];
            string4=['pm2 = ' num2str(pmed2) '+-' num2str(mStdL(1,2)) '; cm2 = ' num2str(cmed2) '+-' num2str(mStdL(1,4)) '; km2 = ' num2str(kmed2) '+-' num2str(mStdL(1,6))];
            text(min(tavgrall)+1/xx(2),(yy(2)*0.6),string1,'FontSize',8, 'EdgeColor', 'r');
            text(min(tavgrall)+1/xx(2),yy(2)*0.43,string2,'FontSize',8, 'EdgeColor', 'r');
            text(min(tavgrall)+1/xx(2),(yy(2)*0.32),string3,'FontSize',8, 'EdgeColor',  [255/255 193/255 37/255]);
            text(min(tavgrall)+1/xx(2),yy(2)*0.23,string4,'FontSize',8, 'EdgeColor',  [255/255 193/255 37/255]);
        end
        xx=get(gca, 'xlim');
        string=['\sigma(Bst) = ' num2str(fStdBst) ' Rc = ' num2str(fRc_Bst) ];%' Rc(Obfit) = ' num2str(fRc_Bst2)];
        text(min(tavgrall)+1/xx(2),yy(2)*0.0014,string,'FontSize',8);
        sAIC = ['AIC = ' num2str(fAIC)];
        text(min(tavgrall)+1/xx(2),yy(2)*0.0009,sAIC,'FontSize',8);
        %if length(ratefc)>1
        %    ps2=plot([tavgrall(length(ratelp)+length(ratefc)-1) tavgrall(length(ratelp)+length(ratefc)-1)],[rate_med(length(ratelp)+length(ratefc)-1) (rate_med(length(ratelp)+length(ratefc)-1)-fDiff_med)]);
        %    set(ps2,'Linewidth',2,'Color',[1 0 0])
        %end
        string2=['Fc - Obs (end) = ' num2str(fDiff_med), '+/-', num2str(fDiff_std)];
        text(min(tavgrall)+1/xx(2),yy(2)*0.0002, string2, 'FontSize',8);

        sGoodfit = ['KS Test: H = ' num2str(H) ', KS statistic = ' num2str(KSSTAT) ' P value = ' num2str(P) '; RMS = ' num2str(fRMS)];
        text(min(tavgrall)+1/xx(2),yy(2)*0.0019,sGoodfit,'FontSize',8);
        % Legend
        if ~isempty(pf3)

            if nMod == 2 || nMod == 3 || nMod ==4
                paf = plot(fT1, yy(1),'h','MarkerFaceColor',[1 1 0],'MarkerSize',12,'MarkerEdgeColor',[0 0 0] );
                legend([p2 p1 pf1 pf3 pmed pbg paf],'data','model to data','forecast','observed', 'Mean Bst-model','Background Rate','Sec. AF',1);
            else
                legend([p2 p1 pf1 pf3 pmed pbg],'data','model to data','forecast','observed', 'Mean Bst-model','Background Rate',1);
            end
        else
            if nMod == 2 || nMod == 3 || nMod ==4
                paf = plot(fT1, yy(1),'h','MarkerFaceColor',[1 1 0],'MarkerSize',12,'MarkerEdgeColor',[0 0 0] );
                legend([p2 p1  pmed pbg paf],'data','model to data', 'Mean Bst-model','Background Rate','Sec. AF',1);
            else
                legend([p2 p1  pmed pbg],'data','model to data', 'Mean Bst-model','Background Rate',1);
            end
        end
    end
end



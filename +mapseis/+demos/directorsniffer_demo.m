function functable = directorsniffer_demo(whichdir)
	%this is a demo for testing the possebility to call every function in a directory
	%works at the moment only in unix systems
	import mapseis.util.importfilter.*;
	import mapseis.importfilter.*;
	
	rp = regexpPatternCatalog();
	
	if nargin==0
		%use current directory
		[sta res] = unix('ls -1 *.m');
		
		lines = regexp(res,rp.line,'match');

		for i=1:numel(lines)
	
			[pathStr,nameStr{i},extStr] = fileparts(lines{i});
			%functable{i} = str2func(nameStr); 

	
		end

		functable=cellfun(@str2func, nameStr, ...
        	           'UniformOutput', false);


		
		
	else	
		curdir=pwd;
		
		cd(whichdir);
		
		[sta res] = unix(['ls -1 *.m']);
		
		lines = regexp(res,rp.line,'match');
		
		
		for i=1:numel(lines)
	
			[pathStr,nameStr{i},extStr] = fileparts(lines{i});
			%functable{i} = str2func(nameStr); 
			
			
			%this has to be done for a certain object structur
			%nameStr{i} = ['mapseis.importfilter.',nameStr{i}];
	
		end

		functable=cellfun(@str2func, nameStr, ...
                   'UniformOutput', false);
		
		
		%get back to original dir
		cd(curdir);
		
		
		
		
		
	end	
	
	end

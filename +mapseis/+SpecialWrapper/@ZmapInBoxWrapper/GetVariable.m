function GetVariable(obj,whichone)
	%gets the variable defined as string with the parameter whichone
	%and adds it to the current datastores user data	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard

	
	fs=filesep;
	
	%get variable
	evalin('base',['save([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempin.mat''],''',whichone,''')']);
	%evalin('base',['save ./Temporary_Files/tempin.mat ',whichone]);
	zmapdata=load([obj.orgPath,fs,'Temporary_Files',fs,'tempin.mat']);
	
	%get datastore
	datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
	
	%add data
	datastore.setUserData(whichone,zmapdata.(whichone));
	

end
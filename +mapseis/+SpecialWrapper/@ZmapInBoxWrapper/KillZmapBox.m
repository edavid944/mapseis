function KillZmapBox(obj)
 	%Similar to the original CloseZmapInBox method but it not only removes
 	%the menu but also cleans up all variables related to ZMAP and closes 
 	%windows belonging to ZMAP. It will be called CloseZmapInBox if HardKill
 	%is true
 	
 	%find the zmap windows and close them
 	MenWin=findobj('Name','ZMAP 6.0 - Menu');
 	if ~isempty(MenWin)
 		set(MenWin,'Visible','off');
 		close(MenWin);
 	end
 	
 	SeismMapWin=findobj('Name','Seismicity Map');
 	if ~isempty(SeismMapWin)
 		set(SeismMapWin,'Visible','off');
 		close(SeismMapWin);
 	end
 	
 	MesWin=findobj('Name','Message Window');
 	if ~isempty(MesWin)
 	 	set(MesWin,'Visible','off');
 		close(MesWin);
 	end
 	
 	CumWin=findobj('Name','Cumulative Number');
 	if ~isempty(CumWin)
 		set(CumWin,'Visible','off');
 		close(CumWin);
 	end
 	

 	%clear variables
 	for vc=1:numel(obj.ZmapVariableList)
 		evalin('base',['clear(''',obj.ZmapVariableList{vc},''')']);
 	end
 	
end
function ExistingVariables(obj)
	%opens a dialogbox with the existing datastore user variables.
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard


	datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
	Msgfields=datastore.getUserDataKeys;
	MsgTitle='Existing variables in the selected datastore';
	msgbox(Msgfields,MsgTitle,'help'); 
	
end
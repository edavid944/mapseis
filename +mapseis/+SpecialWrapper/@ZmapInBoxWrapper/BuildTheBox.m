function BuildTheBox(obj)
	%This function builds the gui which is in this case only
	%menu with certain commands
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard

	
	%clear GUI if possible
	try 
		set(obj.GUIMenu,'Visible','off');
		clear obj.GUIMenu;
		obj.GUIMenu=[];
	end
	
	%get CommanderGUI
	g=obj.Commander.MyGUI;
	
	obj.GUIMenu=uimenu(g,'Label','- Zmap');
	
	uimenu(obj.GUIMenu,'Label','Start ZMAP',...
	'Callback',@(s,e) obj.StartZmap);
	obj.Focalmenu=uimenu(obj.GUIMenu,'Label','Send Focal Mechanism',...
	'Callback',@(s,e) obj.FocalSender,'Checked', 'off');
	uimenu(obj.GUIMenu,'Label','Send Catalog','Separator','on',...
	'Callback',@(s,e) obj.SendToZmap);
	uimenu(obj.GUIMenu,'Label','Send Variable',...
	'Callback',@(s,e) obj.VariableGUI('toZmap'));
	uimenu(obj.GUIMenu,'Label','Get Catalog','Separator','on',...
	'Callback',@(s,e) obj.GetFromZmap('filtered'));
	uimenu(obj.GUIMenu,'Label','Get/Replace Catalog',...
	'Callback',@(s,e) obj.GetFromZmap('replace'));
	uimenu(obj.GUIMenu,'Label','Build New Catalog',...
	'Callback',@(s,e) obj.GetFromZmap('new'));
	uimenu(obj.GUIMenu,'Label','Show Saved Variables',...
	'Callback',@(s,e) obj.ExistingVariables);			
	uimenu(obj.GUIMenu,'Label','Get Variable',...
	'Callback',@(s,e) obj.VariableGUI('fromZmap'));
	
	uimenu(obj.GUIMenu,'Label','Close ZmapInBox','Separator','on',...
	'Callback',@(s,e) obj.CloseZmapInBox);	
	

end
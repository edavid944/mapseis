function CloseZmapInBox(obj)
	%removes the menu from the Commmander and closes everything
	%related to zmapinBox. Does at the moment not work all, because
	%I would have to delete all variable of zmap, and I don't now all 
	%all of them. So at the moment only all zmapinbox stuff will be remove
	%and not any zmap stuff
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard
	
	if obj.HardKill
		obj.KillZmapBox;
	end
	
	try 
		set(obj.GUIMenu,'Visible','off');
		clear obj.GUIMenu;
		obj.Commander.RemovePlugIn(obj,'ZmapInBox');

	end
end
	
function SendVariable(obj,whichone)
	%sends the selected userdata of the currentdatastore to zmap
	%reverse function of GetVariable
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard
	
	fs=filesep;
		
	%get datastore
	datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
	
	try
		toZmap=datastore.getUserData(whichone);
		succ=true;
	catch
		succ=false;
	end
	
	if succ
		%save
		save([obj.orgPath,fs,'Temporary_Files',fs,tempout.mat'],'toZmap');
		
		%get to Zmap
		%evalin('base','load(''./Temporary_Files/tempout.mat'');');
		evalin('base',['load([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempout.mat''])']);
		evalin('base',[whichone,'=toZmap;']);
		evalin('base','clear toZmap');
	end	

end
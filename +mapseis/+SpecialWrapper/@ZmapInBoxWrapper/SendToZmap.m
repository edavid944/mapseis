function SendToZmap(obj)
	%takes the current datastore and filterlist and builds the 
	%zmap catalog and upates zmap
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard
	
	
	import mapseis.export.Export2Zmap;
	
	%get Ids
	obj.DataStoreID = getCurrentDatastoreNumber(obj.Commander);
	obj.FilterListID = getCurrentFilterlistNumber(obj.Commander);
	
	%the data
	datastore = getCurrentDatastore(obj.Commander);
	filterlist = getCurrentFilterlist(obj.Commander);
	
	%build the zmapcatalog
	selected=filterlist.getSelected;
	obj.SendedEvents=selected;
	
	a=Export2Zmap(datastore,selected,obj.SendFocal);
	
	fs=filesep;
	
	%get to the base workspace
	save([obj.orgPath,fs,'Temporary_Files',fs,'tempout.mat'],'a');
	evalin('base',['load([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempout.mat''])']);
	
	
	%try building the coastline
	try
		rawcoast=datastore.getUserData('Coast_PlotArgs');
		coastline(:,1)=rawcoast{1};
		coastline(:,2)=rawcoast{2};
	catch
		disp('no coastline available');
		coastline = [];
	end	
	
	%get to the base workspace
	save([obj.orgPath,fs,'Temporary_Files',fs,'tempout.mat'],'coastline');
	evalin('base',['load([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempout.mat''])']);
	
	
	%load parameter selection, without window
	evalin('base','inpuNoMenu;');	
	
	
end
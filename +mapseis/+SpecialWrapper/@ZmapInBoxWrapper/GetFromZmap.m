function GetFromZmap(obj,How)
	%this function gets the current zmap catalog and either builds
	%a new datastore (How='new') replaces the whole datastore 
	%(How='replace') or replaces only the events originally sended to
	%zmap (How='filtered'),means only the on selected by the filterlist
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard


	
	import mapseis.datastore.*;
	import mapseis.importfilter.importZMAPCatRead;
	
	fs=filesep;
	
	%get the data from zmap over the disk
	evalin('base',['save([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempin.mat''],''a'')']);
	zmapdata=load([obj.orgPath,fs,'Temporary_Files',fs,'tempin.mat']);
	a=zmapdata.a;
	
	try
		indexes=a(:,13);
	catch
		indexes=[];
	end	
	
	neweventStruct=importZMAPCatRead(a,'FocalSelect',obj.SendFocal);
	disp(neweventStruct);
	switch How
		case 'new'
			datastore=DataStore(neweventStruct);
			setDefaultUserData(datastore);
			datastore.setUserData('Name',['Zmap_Import_',num2str(obj.counter)]);
			
			%pack
			newdata.Datastore=datastore;
			newdata.DataName=['Zmap_Import_',num2str(obj.counter)];
			
			%increament counter
			obj.counter=obj.counter+1;
			
			obj.Commander.pushIt(newdata);
			
		case 'replace'
			datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
			try
				Thename=datastore.getUserData('Name');
			catch
				Thename=['Zmap_Import_',num2str(obj.counter)];
				obj.counter=obj.counter+1;
			end
			
			datastore.ReplaceInternalData(neweventStruct);
			setDefaultUserData(datastore);
			
			%rename
			datastore.setUserData('Name',Thename);
			
		case 'filtered'
			datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
			datastore.EditFilteredData(neweventStruct,obj.SendedEvents,indexes);
		
	end
	
	
end
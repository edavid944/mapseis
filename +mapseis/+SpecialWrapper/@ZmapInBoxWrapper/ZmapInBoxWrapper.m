classdef ZmapInBoxWrapper < handle
	
	%This object contains the interface for direct communication between zmap
	%and mapseis. It allows to start zmap inside mapseis and exchange catalog data
	%between the two in realtime.
	%It mimics a bit the api of the wrapper class but is not directly based on it.
	%It has it's one build in GUI as not make sense to use the normal resultgui and
	%CalcParameterGUI
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2012 David Eberhard
	
	
	properties
		Commander
		ListProxy
		GUIMenu
		DataStoreID
		FilterListID
		SendedEvents
		SendFocal
		Focalmenu
		ZmapPath
		orgPath
		counter
		HardKill
		ZmapVariableList
	end
	
	
	events
		ZmapUpdate
	end
	
	
	methods
		function obj = ZmapInBoxWrapper(ListProxy,Commander,GUISwitch,ZmapPath)
			%set zmappath
			obj.orgPath=Commander.MapSeisFolder;
			fs=filesep;
			
			if nargin<4
				ZmapPath=[];
			end	
			
			if isempty(ZmapPath)
				%use default path
				ZmapPath=[obj.orgPath,fs,'AddOneFiles',fs,'ZmapInBox'];
			end	
						
			obj.ZmapPath=ZmapPath;
				
						
			obj.ListProxy=ListProxy;
			obj.Commander=Commander;
			obj.SendFocal=false;
			SendedEvenst=[];
			obj.orgPath=pwd;
			obj.counter=1;
			obj.HardKill=true;
			
			%generate default variable list
			obj.generateZMAPVariableList;
			
			%Register in the Commander
			obj.Commander.AddPlugIn(obj,'ZmapInBox');
			
			%get Ids
			obj.DataStoreID = getCurrentDatastoreNumber(obj.Commander);
			obj.FilterListID = getCurrentFilterlistNumber(obj.Commander);
			
			if GUISwitch
				obj.BuildTheBox;
			end
			
			
			
		end	
	
	
		%external file methods
		BuildTheBox(obj)
		
		StartZmap(obj)
		
		VariableGUI(obj,whichdirection)
		
		SendToZmap(obj)
		
		GetFromZmap(obj,How)
		
		GetVariable(obj,whichone)
		
		SendVariable(obj,whichone)
		
		ExistingVariables(obj)
		
		CloseZmapInBox(obj)
		
		FocalSender(obj)
		
		generateZMAPVariableList(obj)
		
		KillZmapBox(obj)
		
	end
	
end

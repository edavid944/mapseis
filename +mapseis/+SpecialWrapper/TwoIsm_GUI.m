classdef TwoIsm_GUI < handle
    % Allows to compare rates of the catalogs/filterlist with a FMD. Maybe the function will be added
    %to the mainGUI later as plugin or similar (two catalog plot option)
    
    % 
    % $Revision: 1 $    $Date: 2010-05-25  $
    % Author: David Eberhard
    
    %This version can be used with the Commander
    
    properties
        DataStore_A
        DataStore_B
        FilterList_A
        FilterList_B
        LatLonBox
        DepthBox
        CircleRadius
        ProfileWidth
        MainGUIDataStore
        MainGUIFilterList
        Commander
        GUIHandle
        CatController
	ListProxy
	FilterGUI
	%FilterGUI_B
        shownParameter
        CoastToggle
        BorderToggle
        LegendToggle
        OptionMenu
        WindowName
        PlotQuality
    end
    
    methods
        
        function obj = TwoIsm_GUI(viewName,Commander,ListProxy)
            % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.listenerproxy.*;
            import mapseis.SpecialWrapper.*;
            
            %turn mainGUI off
            Commander.AutoUpdate('off');
            
            if isempty(ListProxy)
            	ListProxy = addproxylistener;
            end
            
            
            obj.WindowName = viewName;
            obj.ListProxy = ListProxy;
            obj.Commander=Commander;
            obj.PlotQuality = 'low';
            obj.CoastToggle = true;
	    obj.BorderToggle = true;
	    obj.LegendToggle = true;
 
            % Create the figure window that contains the GUI.  Use a one panel layout.
            obj.GUIHandle = onePanelLayout('Twin Catalog Main Window',...
                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
            set(obj.GUIHandle,'Position',[ 50 300 1100 690 ],'Renderer','painters');
            set(obj.GUIHandle,'DeleteFcn',@(s,e) obj.Commander.AutoUpdate('on'));
            
            %create the controller and listen to it
            obj.CatController =  TwoIsm_ParamGUI(obj.Commander);
          
           
            obj.ListProxy.listen(obj.CatController, 'Calculate', @() obj.EatData);          

            
            
            %set basic setting
            obj.shownParameter = 'map';
            
            
            %set default ProfileWidth and circle radius;
            obj.CircleRadius = [1 1];
            obj.ProfileWidth = [0.5 0.5];
            
            
            %obj.updateGUI();
            
            % Make the GUI visible
            set(obj.GUIHandle,'Visible','on');                        
            
            %create Rightbutton mouse menu
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            %obj.createContextMenu(plotAxis);
            
            % Create the menu (here we refer to the object itself)
            
            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
            uimenu(obj.OptionMenu,'Label','Zmap style',...
	                'Callback',@(s,e) obj.setqual('low'));
	    uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
	                'Callback',@(s,e) obj.setqual('med'));
	    uimenu(obj.OptionMenu,'Label','High Quality Plot',...
	                'Callback',@(s,e) obj.setqual('hi'));
            
            uimenu(obj.OptionMenu,'Label','Show Coastline','Separator','on',...
                'Callback',@(s,e) obj.setparam('Show Coastline','CoastToggle'));    
            uimenu(obj.OptionMenu,'Label','Show Internal Borders',...
                'Callback',@(s,e) obj.setparam('Show Internal Borders','BorderToggle'));    
                
            uimenu(obj.OptionMenu,'Label','Set Selection Radius','Separator','on',...
                'Callback',@(s,e) obj.setAddPars('Radius'));    
            uimenu(obj.OptionMenu,'Label','Set Profile Widths',...
                'Callback',@(s,e) obj.setAddPars('Width'));             
	    obj.setqual('chk');

	    %obj.EatData;
            
	    plotmenu = uimenu(obj.GUIHandle,'Label','- Plots');
            uimenu(plotmenu,'Label','Map','Callback',...
                @(s,e) setParameterType(obj,'map'));
            uimenu(plotmenu,'Label','Cumulative FMD',...
                'Callback',@(s,e) setParameterType(obj,'FMD'));   
                            
        end
        
        function EatData(obj)
            import mapseis.util.gui.allWaiting;
            import mapseis.catalogtools.*;
            import mapseis.gui.*;
            import mapseis.projector.*;
            %gets the data from the Commander and brings everything into the right order, and opens needed windows
            %(yes, I did write this function right before lunch)
            
            %Color data
            allWaiting('wait');
            %mute all eventlisteners
            try
            	obj.ListProxy.quiet(obj.DataStore_A, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            try
            	obj.ListProxy.quiet(obj.DataStore_B, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            try
            	obj.ListProxy.quiet(obj.FilterList_A, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            try
            	obj.ListProxy.quiet(obj.FilterList_B, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            
            
            %get data and parameters from the controller and save
            obj.DataStore_A =  obj.Commander.Keyresolver(obj.CatController.DataStore_A_ID,'data');
            obj.DataStore_B = obj.Commander.Keyresolver(obj.CatController.DataStore_B_ID,'data');
            obj.FilterList_A = obj.Commander.Keyresolver(obj.CatController.FilterList_A_ID,'filter');
            obj.FilterList_B = obj.Commander.Keyresolver(obj.CatController.FilterList_B_ID,'filter');
            
            %create data for some filter settings.
            if obj.DataStore_A == obj.DataStore_B
           	LonLatMatrix = getLocations(obj.DataStore_A);
           	lons = LonLatMatrix(:,1);
           	lats = LonLatMatrix(:,2);
            	minimax.minLat=min(lats);
            	minimax.maxLat=max(lats);
            	minimax.minLon=min(lons);
            	minimax.maxLon=max(lons);
           	obj.LatLonBox=minimax;
            	
           	[Selected,Unselected] = getDepths(obj.DataStore_A);
           	obj.DepthBox=[min(Selected) max(Selected)];
           	
           	
            	obj.ListProxy.listen(obj.DataStore_A, 'Update', @() obj.updateGUI,'Twoism');	
           	
            else
            	LonLatMatrixA = getLocations(obj.DataStore_A);
           	lonsA = LonLatMatrixA(:,1);
           	latsA = LonLatMatrixA(:,2);
           	
           	LonLatMatrixB = getLocations(obj.DataStore_B);
           	lonsB = LonLatMatrixB(:,1);
           	latsB = LonLatMatrixB(:,2);
           	
            	minimax.minLat=min([min(latsA) min(latsB)]);
            	minimax.maxLat=max([max(latsA) max(latsB)]);
            	minimax.minLon=min([min(lonsA) min(lonsB)]);
            	minimax.maxLon=max([max(lonsA) max(lonsB)]);
           	obj.LatLonBox=minimax;
            	
           	[SelectedA,Unselected] = getDepths(obj.DataStore_A);
           	[SelectedB,Unselected] = getDepths(obj.DataStore_B);
           	obj.DepthBox=[min([min(SelectedA) min(SelectedB)]) ...
           			max([max(SelectedA) max(SelectedB)])];
           			
           	obj.ListProxy.listen(obj.DataStore_A, 'Update', @() obj.updateGUI,'Twoism');
           	obj.ListProxy.listen(obj.DataStore_B, 'Update', @() obj.updateGUI,'Twoism');		
            
            end
            
            
            
            %save the datastore and filterlist of the maingui (needed to set the change filterlist back to the 
            %pervious datastore)
            obj.MainGUIDataStore=obj.Commander.MainGUI.DataStore;
            obj.MainGUIFilterList=obj.Commander.MainGUI.FilterList;
            
            %Data is here so now check what is needed to do for the filterlist.
            if obj.FilterList_A == obj.FilterList_B
            	TheColor={'magenta';'magenta'};
            	obj.ListProxy.listen(obj.FilterList_A, 'Update', @() obj.updateGUI,'Twoism');
            else           	
            	TheColor={'blue';'red'};
            	obj.ListProxy.listen(obj.FilterList_A, 'Update', @() obj.updateGUI,'Twoism');
            	obj.ListProxy.listen(obj.FilterList_B, 'Update', @() obj.updateGUI,'Twoism');
            end
            %only one FilterGUI needed
            try 
                close(obj.FilterGUI.mainGUI);
                
            	clear(obj.FilterGUI);

            end	
            
            obj.FilterGUI=ParamGUI_Multi({obj.DataStore_A;obj.DataStore_B},...
            				{obj.FilterList_A;obj.FilterList_B},...
            				TheColor,obj.ListProxy);
            
            %update the gui
            obj.updateGUI;
            
            allWaiting('move');
            
        end
        
        
        
        
        function updateGUI(obj)
           % This method creates the accord function to plot the wanted parameter
           % 
           import mapseis.plot.*;
           import mapseis.projector.*;
           import mapseis.util.gui.allWaiting;
           
           
           allWaiting('wait');
           
           %determine which plot
           plotparam = obj.shownParameter;
           
           %dataA=obj.DataStore_A.getFields;
           %dataB=obj.DataStore_B.getFields;
            
           
            %mute all eventlisteners, this has to be done because else the updateGUI function would be
            %repeated as many eventlistener there are around (up to 4 times), it is not the most elegant 
            %version but it works, and performance does not seem to be an issue at the moment.
            
            try
            	obj.ListProxy.quiet(obj.DataStore_A, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            try
            	obj.ListProxy.quiet(obj.DataStore_B, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            try
            	obj.ListProxy.quiet(obj.FilterList_A, 'Update', @() obj.updateGUI,'Twoism');
            end
            
            try
            	obj.ListProxy.quiet(obj.FilterList_B, 'Update', @() obj.updateGUI,'Twoism');
            end
            
           
            
           plotAxis = findobj(obj.GUIHandle,'Tag','axis');
           	
           figure(obj.GUIHandle);
           plotAxis = subplot(1,1,1);
           
           
           set(plotAxis,'Tag','axis','TickDir','out','color','r','FontWeight','bold','FontSize',12);
           
	   
			
			%get the region which as to be plotted
           			if obj.DataStore_A==obj.DataStore_B
           				lonlat_A=getLocations(obj.DataStore_A);
           				lonRange=[min(lonlat_A(:,1)) max(lonlat_A(:,1))];
           				latRange=[min(lonlat_A(:,2)) max(lonlat_A(:,2))];
           			else
           				lonlat_A=getLocations(obj.DataStore_A);
           				lonlat_B=getLocations(obj.DataStore_B);
           				lonRange=[min([min(lonlat_A(:,1)) min(lonlat_B(:,1))])...
           					  max([max(lonlat_A(:,1)) max(lonlat_B(:,1))])];
           				latRange=[min([min(lonlat_A(:,2)) min(lonlat_B(:,2))])...
           					  max([max(lonlat_A(:,2)) max(lonlat_B(:,2))])];	  
           			
           			end
           			
           			%get the selected data
           			if obj.DataStore_A==obj.FilterList_A.DataStore
           				%already linked datastore
           				obj.FilterList_A.update;
           				select_A=obj.FilterList_A.getSelected;
           				Unselect_A=obj.FilterList_A.getUnselected;
           			else
           				%get data, replace it, and go back afterwards
           				tempData=obj.FilterList_A.DataStore;
           				
           				%change data
           				obj.FilterList_A.changeData(obj.DataStore_A);
           				obj.FilterList_A.update;
           				
           				%get the wanted stuff
           				select_A=obj.FilterList_A.getSelected;
           				Unselect_A=obj.FilterList_A.getUnselected;
           				
           				%and back to normal
           				obj.FilterList_A.changeData(tempData);
           				obj.FilterList_A.update;
           				
           			end
           			
           			
           			%same for B
           			if obj.DataStore_B==obj.FilterList_B.DataStore
           				%already linked datastore
           				obj.FilterList_B.update;
           				select_B=obj.FilterList_B.getSelected;
           				Unselect_B=obj.FilterList_B.getUnselected;
           			else
           				%get data, replace it, and go back afterwards
           				tempData=obj.FilterList_B.DataStore;
           				
           				%change data
           				obj.FilterList_B.changeData(obj.DataStore_B);
           				obj.FilterList_B.update;
           				
           				%get the wanted stuff
           				select_B=obj.FilterList_B.getSelected;
           				Unselect_B=obj.FilterList_B.getUnselected;
           				
           				%and back to normal
           				obj.FilterList_B.changeData(tempData);
           				obj.FilterList_B.update;
           				
           			end
           			
			
	   
	   		
           switch plotparam
           		case 'map'
  				
           			
           			set(plotAxis,'pos',[0.1300    0.1100    0.7750    0.8150])
           			
           			%coastlines and borders are used from the fis	
           			if obj.CoastToggle 
           				 CoastConf= struct('PlotType','Coastline',...
           				 			'Data',obj.DataStore_A,...
           				 			'X_Axis_Label','Longitude ',...
           				 			'Y_Axis_Label','Latitude ',...
           				 			'LineStylePreset','normal',...
           				 			'Colors','blue');
           				 [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
           				 hold on;
           			
           			end
           			
           			
           			if obj.BorderToggle
           				BorderConf= struct('PlotType','Border',...
           						'Data',obj.DataStore_A,...
           						'X_Axis_Label','Longitude ',...
           						'Y_Axis_Label','Latitude ',...
           						'LineStylePreset','normal',...
           						'Colors','black');
           				[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
           				hold on;
           			
           			end
           		
				
           			%now the locations
           			
           			if obj.DataStore_A==obj.DataStore_B
           				%both catalogs are equal, so plot the "background events in the same color
           				
           				%first plot the unSelected
					EqConfigUnSel= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_A,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','unselected',...
							'SelectedEvents',select_B&select_A,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes')
							
					EqConfigBothSel =EqConfigUnSel;
					EqConfigBothSel.SelectionSwitch='selected';
					EqConfigBothSel.Colors='magenta';		
					
							
           				[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigUnSel);
           				hold on
           					
           				if sum(select_B&select_A)>=0	
           					[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigBothSel);
           				end
           				
           				
           				
           				EqConfigSelA= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_A,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',select_A&~select_B,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','blue',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
           				
					EqConfigSelB= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_B,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',~select_A&select_B,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','red',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');		
           				
					if sum(select_A&~select_B)>=0
						[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigSelA);
						hold on
					end
					
					if sum(~select_A&select_B)>=0
						[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigSelB);
						hold on
					end	
					
					%now check if a region is selected in the filterlist
					
					if obj.FilterList_A==obj.FilterList_B
						%actually a bit a stupid case (equal data, equal filter), but just
						%for safety reason
						obj.PlotPolygon(plotAxis,'A',obj.FilterList_A,'m');
						
						
					else
						obj.PlotPolygon(plotAxis,'A',obj.FilterList_A,'b');
						obj.PlotPolygon(plotAxis,'B',obj.FilterList_B,'r');
						
					end
           			
           			else
           				%both catalogs are equal, so plot the "background events in the same color
           				
           				%first plot the unSelected
					EqConfigUnSelA= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_A,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','unselected',...
							'SelectedEvents',select_A,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','blue',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
					
					EqConfigUnSelB= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_B,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','unselected',...
							'SelectedEvents',select_B,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','red',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
							
           				[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigUnSelA);
           				
           				hold on
           				
           				[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigUnSelB);
           				
           				EqConfigSelA= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_A,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',select_A,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','blue',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
           				
					EqConfigSelB= struct(	'PlotType','Earthquakes',...
							'Data',obj.DataStore_B,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',select_B,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','red',...
							'X_Axis_Limit',lonRange,...
							'Y_Axis_Limit',latRange,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');		
           				
					[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigSelA);
					[handle1 entry1] = PlotEarthquake(plotAxis,EqConfigSelB);
					
					
					%now check if a region is selected in the filterlist
					
					if obj.FilterList_A==obj.FilterList_B
						obj.PlotPolygon(plotAxis,'A',obj.FilterList_A,'m');
						
						
					else
						obj.PlotPolygon(plotAxis,'A',obj.FilterList_A,'b');
						obj.PlotPolygon(plotAxis,'B',obj.FilterList_B,'r');
						
					end
           			
           			
		                end
		                
		                
		                
           			
		                %will be back as soon as I'm happy with it 
           			%build legend
           			%legend(plotAxis, obj.CalcRes.filenames{:},'Interpreter','none');
           			%	if obj.LegendToggle
           			%		legend(plotAxis, 'show');
           			%	else 	
           			%		legend(plotAxis, 'hide');
           			%	end	
           					
           					

           			 
           			 
           			 %somehow the limit in the PlotEarthquake did not work
           			 %axis(plotAxis,[lonRange(1) lonRange(2) latRange(1) latRange(2)]);
           			 latlim = get(plotAxis,'Ylim');
           			 set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           			 set(plotAxis,'DrawMode','fast');
            
   
           			
           			

           			set(plotAxis,'Tag','axis');
           			hold off
           		
           		case 'FMD'
           		
           		
           			set(plotAxis,'pos',[0.1300    0.2500    0.7750    0.65])
           			%position of the texts may have to be modified
           			
           			
           			%custom calcconfig with bootstrap
           			CalcConfig=struct(	'Mc_method',1,...
							'Use_bootstrap',true,...
							'MinNumber',10,...
							'Mc_Binning',0.1,...
							'Nr_bootstraps',100,...
							'Mc_correction',0,...
							'NoCurve',false);
							
           			if obj.DataStore_A==obj.DataStore_B&obj.FilterList_A==obj.FilterList_B
           				%only one FMD is needed in this case.
           				 %now the data
           				 FMDConfig	= struct('PlotType','FMDex',...
           				 			'Data',obj.DataStore_A,...
								 'BinSize',0.1,...
								 'b_line',true,...
								 'Selected',select_A,...
								 'Mc_Method',1,...
								 'CustomCalcParameter',CalcConfig,...
								 'Write_Text',false,...
								 'Value_Output','all',...
								 'LineStylePreset','normal',...
								 'X_Axis_Label','Magnitude ',...
								 'Y_Axis_Label','cum. Number of Events ',...
								 'Colors','magenta',...
								 'X_Axis_Limit','auto',...
								 'Y_Axis_Limit','auto',...
								 'LegendText','FMD');
           			
					 [handle DataOut] = PlotFMD_expanded(plotAxis,FMDConfig);
					 txt1=text(0, -.11,DataOut.Text{1},'FontSize',10,'Color','m','Units','normalized');
					 set(txt1,'FontWeight','normal')
					 text(0, -.08,DataOut.Text{2},'FontSize',10,'Color','m','Units','normalized');
					 text(0, -.14,DataOut.Text{3},'FontSize',10,'Color','m','Units','normalized');
					 
					 
					 
           			else
           				 FMDConfigA	= struct('PlotType','FMDex',...
           				 			'Data',obj.DataStore_A,...
								 'BinSize',0.1,...
								 'b_line',true,...
								 'Selected',select_A,...
								 'Mc_Method',1,...
								 'CustomCalcParameter',CalcConfig,...
								 'Write_Text',false,...
								 'Value_Output','all',...
								 'LineStylePreset','normal',...
								 'X_Axis_Label','Magnitude ',...
								 'Y_Axis_Label','cum. Number of Events ',...
								 'Colors','blue',...
								 'X_Axis_Limit','auto',...
								 'Y_Axis_Limit','auto',...
								 'LegendText','FMD');
								 
					 FMDConfigB	= struct('PlotType','FMDex',...
           				 			'Data',obj.DataStore_B,...
								 'BinSize',0.1,...
								 'b_line',true,...
								 'Selected',select_B,...
								 'Mc_Method',1,...
								 'CustomCalcParameter',CalcConfig,...
								 'Write_Text',false,...
								 'Value_Output','all',...
								 'LineStylePreset','normal',...
								 'X_Axis_Label','Magnitude ',...
								 'Y_Axis_Label','cum. Number of Events ',...
								 'Colors','red',...
								 'X_Axis_Limit','auto',...
								 'Y_Axis_Limit','auto',...
								 'LegendText','FMD');
								 
					 [handle DataOutA] = PlotFMD_expanded(plotAxis,FMDConfigA);
					 hold on
					 [handle DataOutB] = PlotFMD_expanded(plotAxis,FMDConfigB);
					 
					 txt1=text(0, -.11,DataOutA.Text{1},'FontSize',10,'Color','b','Units','normalized');
					 set(txt1,'FontWeight','normal')
					 text(0, -.08,DataOutA.Text{2},'FontSize',10,'Color','b','Units','normalized');
					 text(0, -.14,DataOutA.Text{3},'FontSize',10,'Color','b','Units','normalized');
					 
					 txt1=text(.6, -.11,DataOutB.Text{1},'FontSize',10,'Color','r','Units','normalized');
					 set(txt1,'FontWeight','normal')
					 text(.6, -.08,DataOutB.Text{2},'FontSize',10,'Color','r','Units','normalized');
					 text(.6, -.14,DataOutB.Text{3},'FontSize',10,'Color','r','Units','normalized');
           			
           			end
           	
           	
           			
           			
           	end
           		
           	
           	set(plotAxis,'Tag','axis');
           	
           	obj.createContextMenu(plotAxis);
           	
           	hold off;
           		
           	
           	
           	%reset listeners
           	if obj.DataStore_A == obj.DataStore_B
           		obj.ListProxy.listen(obj.DataStore_A, 'Update', @() obj.updateGUI,'Twoism');
           	else
           		obj.ListProxy.listen(obj.DataStore_A, 'Update', @() obj.updateGUI,'Twoism');
           		obj.ListProxy.listen(obj.DataStore_B, 'Update', @() obj.updateGUI,'Twoism');	
           	end
            
           	
           	
           	
           	if obj.FilterList_A == obj.FilterList_B
            	   	obj.ListProxy.listen(obj.FilterList_A, 'Update', @() obj.updateGUI,'Twoism');
           	
           	else
           		obj.ListProxy.listen(obj.FilterList_A, 'Update', @() obj.updateGUI,'Twoism');
           		obj.ListProxy.listen(obj.FilterList_B, 'Update', @() obj.updateGUI,'Twoism');
           	end
            
           	
            	allWaiting('move');
        
        end
        
               
        
         function createContextMenu(obj,parentAxis)
            % Create a context menu for the histogram in the top right subplot,
            % that selects the histogram type
            
            import mapseis.plot.*;
            
            
            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.GUIHandle);
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            % Now make the menu be associated with the correct axis
            set(obj.GUIHandle,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different histogram types
            uimenu(cmenu,'Label','Map','Callback',...
                @(s,e) setParameterType(obj,'map'));
            uimenu(cmenu,'Label','Cumulative FMD',...
                'Callback',@(s,e) setParameterType(obj,'FMD'));       
                
                
            if strcmp(obj.shownParameter,'map')
            	%in the map mode regionfilters can be used.
            	%build additional data
            	AddPar=obj.LatLonBox;
            	AddPar.depths=obj.DepthBox;
            	
            	if obj.FilterList_A==obj.FilterList_B
            		AddPar.width=obj.ProfileWidth(1);
            		AddPar.rad=obj.CircleRadius(1);
            		
            		regionFilter = obj.FilterList_A.getByName('Region');
            		
            		FilterA=uimenu(cmenu,'Label','Filter A');          
            		
            		uimenu(FilterA,'Label','Select all',...
            		     'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilter,'all',AddPar));
            		uimenu(FilterA,'Label','Select inside',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilter,'in',AddPar));
            		uimenu(FilterA,'Label','Select Profile',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilter,'polygon',AddPar));
            		uimenu(FilterA,'Label','Select outside',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilter,'out',AddPar));
            		uimenu(FilterA,'Label','Select circular region',...
            		            'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilter,'circle',AddPar));
            		            
            		FilterB=uimenu(cmenu,'Label','Filter B','Enable','off');
            	else
            		
            		AddPar.width=obj.ProfileWidth(1);
            		AddPar.rad=obj.CircleRadius(1);
            		
            		regionFilterA = obj.FilterList_A.getByName('Region');
            		regionFilterB = obj.FilterList_B.getByName('Region');
            		
            		FilterA=uimenu(cmenu,'Label','Filter A');          
            		
            		uimenu(FilterA,'Label','Select all',...
            		     'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterA,'all',AddPar));
            		uimenu(FilterA,'Label','Select inside',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterA,'in',AddPar));
            		uimenu(FilterA,'Label','Select Profile',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterA,'polygon',AddPar));
            		uimenu(FilterA,'Label','Select outside',...
            		           'Callback',@(s,e) SetRegion_NoData(plotAxis,regionFilterA,'out',AddPar));
            		uimenu(FilterA,'Label','Select circular region',...
            		            'Callback',@(s,e) SetRegion_NoData(plotAxis,regionFilterA,'circle',AddPar));
            		   
            		
            		AddPar.width=obj.ProfileWidth(2);
            		AddPar.rad=obj.CircleRadius(2);
            		            
            		FilterB=uimenu(cmenu,'Label','Filter B');
            		
            		uimenu(FilterB,'Label','Select all',...
            		     'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterB,'all',AddPar));
            		uimenu(FilterB,'Label','Select inside',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterB,'in',AddPar));
            		uimenu(FilterB,'Label','Select Profile',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterB,'polygon',AddPar));
            		uimenu(FilterB,'Label','Select outside',...
            		           'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterB,'out',AddPar));
            		uimenu(FilterB,'Label','Select circular region',...
            		            'Callback',@(s,e) Wrap_SetReg(obj,plotAxis,regionFilterB,'circle',AddPar));
            	
            	end
            
            
            end
            
            
          
            uimenu(cmenu,'Label','Plot this frame in new window ',...
                'Callback',...
                @(s,e) plotInNewFigure(obj,'Histogram',@plotHist));
        end

        
        function Wrap_SetReg(obj,plotAxis,regionFilter,regionType,AddPar)
        	%just a wrapper which allows to add a update function without a eventlistener
        	import mapseis.plot.*;
        	
        	SetRegion_NoData(plotAxis,regionFilter,regionType,AddPar);
        	
        	obj.updateGUI;
        
        
        end
        
        
        
        function setAddPars(obj,WhichOne)
        	%Allows to set the Radius and Profile Width of Filter A & B
        	
        	
        	
        	
        	switch WhichOne
        		case 'Radius'
        			oldRadii=deg2km(obj.CircleRadius);
        			promptValues = {'Radius Filter A [km]' 'Radius Filter B [km]'};
        			dlgTitle = 'Selection of the circle radii';
        			gParams = inputdlg(promptValues,dlgTitle,2,...
        			cellfun(@num2str,{oldRadii(1) oldRadii(2)},'UniformOutput',false));
        			newR = str2double(gParams{:});
        			obj.CircleRadius = km2deg(newR);
        			
        			
        		case 'Width'
        			oldWid=deg2km(obj.ProfileWidth);
        			promptValues = {'Profile Width Filter A [km]' 'Profile Width Filter B [km]'};
        			dlgTitle = 'Selection of the profile width';
        			gParams = inputdlg(promptValues,dlgTitle,2,...
        			cellfun(@num2str,{oldWid(1) oldWid(2)},'UniformOutput',false));
        			newWid = str2double(gParams{:});
        			obj.ProfileWidth = km2deg(newWid);
        			
        	end	
        	
        
        
        end
        
        

        function setParameterType(obj,newHistType)
            % Set the parameter to plot on the histogram plot
            obj.shownParameter = newHistType;
            % Update the GUI
            updateGUI(obj);
        end

        
        
        
        
            
    
        
        function clearCalculate(obj)
            % Clear the calculated values and replot the events.
            obj.CalcVals = {};
            obj.updateGUI();
        end
        
        
        
		
	
	function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
			Menu2Check(3) = findobj(obj.OptionMenu,'Label','Zmap style');
			Menu2Check(2) = findobj(obj.OptionMenu,'Label','Medium Quality Plot');
			Menu2Check(1) = findobj(obj.OptionMenu,'Label','High Quality Plot');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
 				case 'low'
 					%dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotQuality='low';
 					obj.updateGUI;
                		
				case 'med'
					%dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotQuality='med';
					obj.updateGUI;
					
				case 'hi'
					%dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotQuality='hi';
					obj.updateGUI;
					
				case 'chk'
					%try StoredQuality = dataStore.getUserData('PlotQuality');
					%catch dataStore.setUserData('PlotQuality','med');
					%StoredQuality = dataStore.getUserData('PlotQuality');		
					%end
					StoredQuality = obj.PlotQuality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
	end
		
		
		
		
		
		
	function setparam(obj,mlabel,guipar)
			%sets the plot parameter and writes it to the datastore and updates the gui
			
		 	dataStore = obj.DataStore_A;
			Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
			
			%get pararmeter from gui
			cstate=obj.(guipar);

			set(Menu2Check, 'Checked', 'off');
			
			%change the to the opposite
			cstate=~cstate;
			
			%write into gui and datastore	
			obj.(guipar)=cstate;
			dataStore.setUserData(guipar,cstate);
			
			%change check
				if cstate
					set(Menu2Check, 'Checked', 'on');
				end
			obj.updateGUI;
	end
		
		
	function getparam(obj,mlabel,guipar)
			%sets the plot parameter and writes it to the datastore and updates the gui
			
		 	dataStore = obj.DataStore_A;
			Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
			
			%get pararmeter from datastore
			try cstate = dataStore.getUserData(guipar);
			catch dataStore.setUserData(guipar,true);
				  cstate = dataStore.getUserData(guipar);	
			end				  
			
			obj.(guipar) = cstate;
				
			set(Menu2Check, 'Checked', 'off');
			
				if cstate
					set(Menu2Check, 'Checked', 'on');
				end
			
	end

		
		
		
		function legendary(obj,com)
			%switches the legend in the plot on and off
			%or just checks it
			
			dataStore = obj.DataStore_A;
			Menu2Check= findobj(obj.OptionMenu,'Label','Legend on/off');
			
			switch com
				case 'chk'
					try leg = dataStore.getUserData('LegendToggle');
						catch dataStore.setUserData('LegendToggle',true);
							  leg = dataStore.getUserData('LegendToggle');	
					end
					
						obj.LegendToggle=leg;
						
						set(Menu2Check, 'Checked', 'off');
			
						if leg
							set(Menu2Check, 'Checked', 'on');
						end

				case 'set'		
					obj.LegendToggle=~obj.LegendToggle;
					dataStore.setUserData('LegendToggle',obj.LegendToggle);
					leg=obj.LegendToggle;
					
					g = obj.GUIHandle;
					plotAxis = findobj(obj.GUIHandle,'Tag','axis');
					
											
						if leg
							set(Menu2Check, 'Checked', 'on');
							legend(plotAxis,'show');
						else
							set(Menu2Check, 'Checked', 'off');
							legend(plotAxis,'hide');
						end

			end	
							  

		
		end
		
		
		function PlotPolygon(obj,plotAxis,AorB,FilterList,Color)
			%Function to plot a polygon
			
			%not finished yet
			import mapseis.region.*;
			import mapseis.plot.drawCircle;	
			import mapseis.plot.drawBox;
			import mapseis.util.gui.SetImMenu;
			import mapseis.region.filterupdater_noData;
			
			regionFilter = FilterList.getByName('Region');
			filterRegion = getRegion(regionFilter);
			pRegion = filterRegion{2};
			RegRange=filterRegion{1};
			
			%if ~isempty(pRegion)

			if isobject(pRegion)
				rBdry = pRegion.getBoundary();	
			else 
				rBdry = pRegion; %circle
				%modify on radius
				%latlim = get(plotAxis,'Ylim');
				%rBdry(4)= rBdry(3)*cos(pi/180*mean(latlim));
			end
			
			%get radius and width
			switch AorB
				case 'A'
					rad= obj.CircleRadius(1);
					wid = obj.ProfileWidth(1);
				case 'B'
					rad= obj.CircleRadius(2);
					wid = obj.ProfileWidth(2);
			end	
			
			
			
			%really needed the axes command
			axes(plotAxis)    
			
			%recreate boundaries object if needed
			 switch RegRange
					case {'in','out'}
						selection=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
						selection.addNewPositionCallback(@(p) ...
						filterupdater_noData(p,[],regionFilter));
						selection.setColor(Color);		
						%set the menu point depth slice to 'off'
						%anamenu=get(obj.AnalysisMenu,'Children');
						%set(anamenu(4),'Enable','off');
						
						%add menus to the object
						SetImMenu(obj,selection,regionFilter,[]);
						
					case 'line'
						selection=imline(plotAxis, [rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)]);
						
						hold on 
						handle = drawBox(plotAxis,false,rBdry);
						
						hold off
											
						selection.addNewPositionCallback(@(p) ...
								filterupdater_noData(p,wid,regionFilter));
						selection.setColor(Color);
						
						
						%set the menu point depth slice to 'on'
						%anamenu=get(obj.AnalysisMenu,'Children');
						%set(anamenu(4),'Enable','on');			
						
						%add menus to the object
						SetImMenu(obj,selection,regionFilter,handle);
											
					case 'circle'
						selection = impoint(plotAxis,rBdry(1:2));
						
						rBdry(3)=regionFilter.Radius;
						if isempty(rBdry(3))
							%oldgrid = datastore.getUserData('gridPars');
							rBdry(3) = rad;
							regionFilter.setRadius(rBdry(3));
						end	
						
						%draw circle
						hold on
						handle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
						hold off
						
						%selection.setFixedAspectRatioMode(1)
						selection.addNewPositionCallback(@(p) ...
								filterupdater_noData([p(1),p(2),rBdry(3)],...
								rad,regionFilter));
						
						selection.setColor(Color);
								
						%anamenu=get(obj.AnalysisMenu,'Children');
						%set(anamenu(4),'Enable','off');			
						
						%add menus to the object
						SetImMenu(obj,selection,regionFilter,handle);
					
					case 'all'
						%set the menu point depth slice to 'off'
						%anamenu=get(obj.AnalysisMenu,'Children');
						%set(anamenu(4),'Enable','off');		
						
						
			end	
		
		
		
		
		
		
		end
		
		
		
		
				
	function rebuildGUI(obj)
		%NEEDS TO BE CHANGED, BUT FOR KNOW IT IS NOT NEEDED	

		%allows to rebuild a closed GUI window
			
			
	    	
			   % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.filter.*;
            
            
            try 
            	set(obj.GUIHandle,'Visible','on');
            
            catch		
	            dataStore =  obj.DataStore;
	            filterList = obj.FilterList;
	                        
	            % Create the figure window that contains the GUI.  Use a one panel layout.
	            obj.GUIHandle = onePanelLayout('MapSeis Grid Calculation Plot',...
	                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
	            
	                        
	              % Register this view with the model.  The subject of the view uses this
	            % function handle to update the view.
	            dataStore.subscribe(obj.WindowName,@() updateGUI(obj));
	            
	            % Update the GUI to plot the initial data
	            obj.updateGUI();
	            
	            % Make the GUI visible
	            set(obj.GUIHandle,'Visible','on');                        
	            
	            % Create the menu (here we refer to the object itself)
	            
	            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
	            uimenu(obj.OptionMenu,'Label','Zmap style',...
	                'Callback',@(s,e) obj.setqual('low'));
	            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
	                'Callback',@(s,e) obj.setqual('med'));
	            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
	                'Callback',@(s,e) obj.setqual('hi'));
	            
				obj.setqual('chk');
	
	            
	            
	            obj.GridMenu=uimenu(obj.GUIHandle,'Label','  --  Grid');
	            uimenu(obj.GridMenu,'Label','Set grid',...
	                'Callback',@obj.setGridParams);
	            uimenu(obj.GridMenu,'Label','Calculate',...
	                'Callback',@(s,e) obj.calculate());
	            uimenu(obj.GridMenu,'Label','Clear',...
	                'Callback',@(s,e) obj.clearCalculate());
			end
			
		
		end
		
		
		
        
    end
    
end
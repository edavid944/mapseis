classdef ZmapInBoxWrapper < handle
	
	%This object contains the interface for direct communication between zmap
	%and mapseis. It allows to start zmap inside mapseis and exchange catalog data
	%between the two in realtime.
	%It mimics a bit the api of the wrapper class but is not directly based on it.
	%It has it's one build in GUI as not make sense to use the normal resultgui and
	%CalcParameterGUI
	
	
	properties
		Commander
		ListProxy
		GUIMenu
		DataStoreID
		FilterListID
		SendedEvents
		SendFocal
		Focalmenu
		ZmapPath
		orgPath
		counter
	end
	
	
	events
		ZmapUpdate
	end
	
	
	methods
		function obj = ZmapInBoxWrapper(ListProxy,Commander,GUISwitch,ZmapPath)
			%set zmappath
			obj.orgPath=Commander.MapSeisFolder;
			
			if nargin<4
				%use default path
				if ~ispc
					obj.ZmapPath=[obj.orgPath,'/AddOneFiles/ZmapInBox'];
				else
					obj.ZmapPath=[obj.orgPath,'\AddOneFiles\ZmapInBox'];
				end
			
			else
				if isempty(ZmapPath)
					%use default path
					if ~ispc
						obj.ZmapPath=[obj.orgPath,'/AddOneFiles/ZmapInBox'];
					else
						obj.ZmapPath=[obj.orgPath,'\AddOneFiles\ZmapInBox'];
					end
				else
					obj.ZmapPath=ZmapPath;
				end	
			
				
			end	
			
			
			
			
			obj.ListProxy=ListProxy;
			obj.Commander=Commander;
			obj.SendFocal=false;
			SendedEvenst=[];
			obj.orgPath=pwd;
			obj.counter=1;
			
			%Register in the Commander
			obj.Commander.AddPlugIn(obj,'ZmapInBox');
			
			%get Ids
			obj.DataStoreID = getCurrentDatastoreNumber(obj.Commander);
			obj.FilterListID = getCurrentFilterlistNumber(obj.Commander);
			
			if GUISwitch
				obj.BuildTheBox;
			end
			
			
			
			
		end	
		
		
		function BuildTheBox(obj)
			%This function builds the gui which is in this case only
			%menu with certain commands
			
			%clear GUI if possible
			try 
				set(obj.GUIMenu,'Visible','off');
				clear obj.GUIMenu;
				obj.GUIMenu=[];
			end
			
			%get CommanderGUI
			g=obj.Commander.MyGUI;
			
			obj.GUIMenu=uimenu(g,'Label','- Zmap');
			
			uimenu(obj.GUIMenu,'Label','start Zmap',...
			'Callback',@(s,e) obj.StartZmap);
			obj.Focalmenu=uimenu(obj.GUIMenu,'Label','Send Focal mechanism',...
			'Callback',@(s,e) obj.FocalSender,'Checked', 'off');
			uimenu(obj.GUIMenu,'Label','Send Catalog','Separator','on',...
			'Callback',@(s,e) obj.SendToZmap);
			uimenu(obj.GUIMenu,'Label','Send Variable',...
			'Callback',@(s,e) obj.VariableGUI('toZmap'));
			uimenu(obj.GUIMenu,'Label','Get Catalog','Separator','on',...
			'Callback',@(s,e) obj.GetFromZmap('filtered'));
			uimenu(obj.GUIMenu,'Label','Get/Replace Catalog',...
			'Callback',@(s,e) obj.GetFromZmap('replace'));
			uimenu(obj.GUIMenu,'Label','Build New Catalog',...
			'Callback',@(s,e) obj.GetFromZmap('new'));
			uimenu(obj.GUIMenu,'Label','Show saved variable',...
			'Callback',@(s,e) obj.ExistingVariables);			
			uimenu(obj.GUIMenu,'Label','Get Variable',...
			'Callback',@(s,e) obj.VariableGUI('fromZmap'));
			
			uimenu(obj.GUIMenu,'Label','Close ZmapInBox','Separator','on',...
			'Callback',@(s,e) obj.CloseZmapInBox);	
		end
		
		function StartZmap(obj)
			%This function starts zmap
			if ~strcmp(pwd,obj.ZmapPath)	
				cd(obj.ZmapPath);
			end

			evalin('base','zmap;');
			
			cd(obj.orgPath);
			
			
		end
		
		function VariableGUI(obj,whichdirection)
			%Start simple gui which lets select a variable and send it 
			%to the function defined with whichdirection
			switch whichdirection
				case 'toZmap'
					% Dialog fields 
					promptValues = {'Variable Name'};
					dlgTitle = 'Variable send from Datastore send to Zmap';
					gParams = inputdlg(promptValues,dlgTitle,1,{''});
					
					%execute
					obj.SendVariable(gParams{1});
					
				case 'fromZmap'
					% Dialog fields 
					promptValues = {'Variable Name'};
					dlgTitle = 'Which Variable should be recieved from Zmap and stored in the datastore?';
					gParams = inputdlg(promptValues,dlgTitle,1,{''});
					
					%execute
					obj.GetVariable(gParams{1});
					
				
				
			end	
		end
		
		
		
		function SendToZmap(obj)
			%takes the current datastore and filterlist and builds the 
			%zmap catalog and upates zmap
			import mapseis.export.Export2Zmap;
			
			%get Ids
			obj.DataStoreID = getCurrentDatastoreNumber(obj.Commander);
			obj.FilterListID = getCurrentFilterlistNumber(obj.Commander);
			
			%the data
			datastore = getCurrentDatastore(obj.Commander);
			filterlist = getCurrentFilterlist(obj.Commander);
			
			%build the zmapcatalog
			selected=filterlist.getSelected;
			obj.SendedEvents=selected;
			
			a=Export2Zmap(datastore,selected,obj.SendFocal);
			
			fs=filesep;
			
			%get to the base workspace
			save([obj.orgPath,fs,'Temporary_Files',fs,'tempout.mat'],'a');
			evalin('base',['load([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempout.mat''])']);
			
			
			%try building the coastline
			try
				rawcoast=datastore.getUserData('Coast_PlotArgs');
				coastline(:,1)=rawcoast{1};
				coastline(:,2)=rawcoast{2};
			catch
				disp('no coastline available');
				coastline = [];
			end	
			%get to the base workspace
			save([obj.orgPath,fs,'Temporary_Files',fs,'tempout.mat'],'coastline');
			evalin('base',['load([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempout.mat''])']);
			
			
			%load parameter selection, without window
			evalin('base','inpuNoMenu;');	
				
			

			
		end
		
		
		function GetFromZmap(obj,How)
			%this function gets the current zmap catalog and either builds
			%a new datastore (How='new') replaces the whole datastore 
			%(How='replace') or replaces only the events originally sended to
			%zmap (How='filtered'),means only the on selected by the filterlist
			
			import mapseis.datastore.*;
			import mapseis.importfilter.importZMAPCatRead;
			
			fs=filesep;
			
			%get the data from zmap over the disk
			evalin('base',['save([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempin.mat''],''a'')']);
			zmapdata=load([obj.orgPath,fs,'Temporary_Files',fs,'tempin.mat']);
			a=zmapdata.a;
			try
				indexes=a(:,13);
			catch
				indexes=[];
			end	
			neweventStruct=importZMAPCatRead(a,'FocalSelect',obj.SendFocal);
			disp(neweventStruct);
			switch How
				case 'new'
					datastore=DataStore(neweventStruct);
					setDefaultUserData(datastore);
					datastore.setUserData('Name',['Zmap_Import_',num2str(obj.counter)]);
					
					%pack
					newdata.Datastore=datastore;
					newdata.DataName=['Zmap_Import_',num2str(obj.counter)];
					
					%increament counter
					obj.counter=obj.counter+1;
					
					obj.Commander.pushIt(newdata);
					
				case 'replace'
					datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
					try
						Thename=datastore.getUserData('Name');
					catch
						Thename=['Zmap_Import_',num2str(obj.counter)];
						obj.counter=obj.counter+1;
					end
					
					datastore.ReplaceInternalData(neweventStruct);
					setDefaultUserData(datastore);
					
					%rename
					datastore.setUserData('Name',Thename);
					
				case 'filtered'
					datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
					datastore.EditFilteredData(neweventStruct,obj.SendedEvents,indexes);
			
			end
			
			
		end
		
		
		
		function GetVariable(obj,whichone)
			%gets the variable defined as string with the parameter whichone
			%and adds it to the current datastores user data
			
			fs=filesep;
			
			%get variable
			evalin('base',['save([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempin.mat''],''',whichone,''')']);
			%evalin('base',['save ./Temporary_Files/tempin.mat ',whichone]);
			zmapdata=load([obj.orgPath,fs,'Temporary_Files',fs,'tempin.mat']);
			
			%get datastore
			datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
			
			%add data
			datastore.setUserData(whichone,zmapdata.(whichone));
			
			
		end
		
		function SendVariable(obj,whichone)
			%sends the selected userdata of the currentdatastore to zmap
			%reverse function of GetVariable
			%get datastore
			fs=filesep;
			datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
			try
				toZmap=datastore.getUserData(whichone);
				succ=true;
			catch
				succ=false;
			end
			
			if succ
				%save
				save([obj.orgPath,fs,'Temporary_Files',fs,tempout.mat'],'toZmap');
			
				%get to Zmap
				%evalin('base','load(''./Temporary_Files/tempout.mat'');');
				evalin('base',['load([''',obj.orgPath,fs,''',''Temporary_Files'',''',fs,''',''tempout.mat''])']);
				evalin('base',[whichone,'=toZmap;']);
				evalin('base','clear toZmap');
			end	
			
		end
		
		
		function ExistingVariables(obj)
			%opens a dialogbox with the existing datastore user variables.
			datastore = obj.Commander.resolveDataEntry(obj.DataStoreID);
			Msgfields=datastore.getUserDataKeys;
			MsgTitle='Existing variables in the selected datastore';
			msgbox(Msgfields,MsgTitle,'help'); 
			
			
		end
		
		function CloseZmapInBox(obj)
			%removes the menu from the Commmander and closes everything
			%related to zmapinBox. Does at the moment not work all, because
			%I would have to delete all variable of zmap, and I don't now all 
			%all of them. So at the moment only all zmapinbox stuff will be remove
			%and not any zmap stuff
			try 
				set(obj.GUIMenu,'Visible','off');
				clear obj.GUIMenu;
				obj.Commander.RemovePlugIn(obj,'ZmapInBox');
			end
			
		
		end
		
		function FocalSender(obj)
			obj.SendFocal=~obj.SendFocal;
			
			%set menu (un)checked
			if obj.SendFocal
				set(obj.Focalmenu, 'Checked', 'on');
			else
				set(obj.Focalmenu, 'Checked', 'off');
			end
			
		end
		
		
		
	end
	
	




end

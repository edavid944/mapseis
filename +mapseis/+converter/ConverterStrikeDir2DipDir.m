function outData=ConverterStrikeDir2DipDir(inData,varargin)
%converts strike-directions to dip-directions (+90deg)



onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Strike-Direction to Dip-Direction';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'Converts strike-directions angles to dip-directions angles (+90deg)';
    	outData.displayname = 'Strike-Direction to Dip-Direction';
    	outData.InputType = 'double';
    	outData.OutputType = 'double';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			
			outData=mod(inData+90,360);
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			outData=mod(inData+270,360);
			
	
	end



end

end

function outData=ConverterTemplate(inData,varargin)
%Template for a converter, at this is just converter with a passthrought



onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Converter Template';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'This is only a Template for building a Converter, DO NOT USE IT.';
    	outData.displayname = 'Converter Template';
    	outData.InputType = 'temp';
    	outData.OutputType = 'temp';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			
			outData=inData;
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			
			outData=inData;
			
	
	end



end

end
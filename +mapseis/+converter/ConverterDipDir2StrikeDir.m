function outData=ConverterDipDir2StrikeDir(inData,varargin)
%Converts dip-directions angles to strike-directions angles (-90deg)



onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Dip-Direction to Strike-Direction';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'Converts dip-directions angles to strike-directions angles (-90deg)';
    	outData.displayname = 'Dip-Direction to Strike-Direction';
    	outData.InputType = 'double';
    	outData.OutputType = 'double';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			outData=mod(inData+270,360);
			
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			outData=mod(inData+90,360);
	
	end



end

end

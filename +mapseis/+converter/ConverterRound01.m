function outData=ConverterRound01(inData,varargin)
%Template for a converter, at this is just converter with a passthrought



onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Round to 0.1';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'General purpose rounding converter, rounds to 0.1';
    	outData.displayname = 'Round to 0.1';
    	outData.InputType = 'double';
    	outData.OutputType = 'double';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			
			outData=round(10*inData)/10;
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			%one way ticket.
			outData=inData;
			
	
	end



end

end
function outData=ConverterAddOne(inData,varargin)
%Adds value+1 and rounds it to 0.1, used as a test in Iceland test region



onlydescription=false;

AddValue=1;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Add 1 to value';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'Adds value+1 and rounds it to 0.1, used as a test in Iceland test region';
    	outData.displayname = 'Add 1 to value';
    	outData.InputType = 'double';
    	outData.OutputType = 'double';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			
			outData=round(10*(inData+AddValue))/10;
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			%one way ticket.
			%the rounding cannot be undone, but the incremeant can
			outData=inData-AddValue;
			
	
	end



end

end

function outData=ConverterNaN(inData,varargin)
%Template for a converter, at this is just converter with a passthrought



onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Set NaN values to -.999';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'General purpose NaN filter, all values set NaN will be set to the value -0.999';
    	outData.displayname = 'Set NaN values to -.999';
    	outData.InputType = 'double';
    	outData.OutputType = 'double';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			inData(isnan(inData))=-.999;
			outData=inData;
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			%one way ticket.
			outData=inData;
			
	
	end



end

end

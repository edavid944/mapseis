function outData=IcelandMwToMw(inData,varargin)
%Template for a converter, at this is just converter with a passthrought



onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-name')
    	outData='Iceland Mw to "original" Mw';
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	outData.description = 'This uses Mw=2/3*log10(M0)-10.7';
    	outData.displayname = 'Iceland Mw converter';
    	outData.InputType = 'double';
    	outData.OutputType = 'double';
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	switch varargin{1}
		case 'Catalog2Map'
			%Catalog to Mapseis	
			%To org Mw
			
			%outData=inData;
			Ice_Mw=inData;
			
			DyneFactor=1/(10^(-7));
    
			%Iceland constanst
			a = 2;
			b = 1/0.9;  
			c = 1.6/0.8;  
			d = 0.8/0.7;
			e = 1;
			f = 1;
			 
			%select the ranges
			sel_low2=Ice_Mw<=2;
			sel_2to3=Ice_Mw>2&Ice_Mw<=3;
			sel_3to46=Ice_Mw>3&Ice_Mw<=4.6;
			sel_46to54=Ice_Mw>4.6&Ice_Mw<=5.4;
			sel_54to59=Ice_Mw>5.4&Ice_Mw<=5.9;
			sel_59to63=Ice_Mw>5.9&Ice_Mw<=6.3;
			sel_hi63=Ice_Mw>6.3;
			 
			    % m = log10(Mo) - 10  
			    % M = m                               if          M <= 2.0
			    % M = 2.0 + (m-a)*0.9                       2.0 < M <= 3.0
			    %	m=(M-2.0)/0.9+a
			    
			    % M = 3.0 + (m-a-b)*0.8                     3.0 < M <= 4.6
			    %	m=(M-3)/0.8+a+b
			    
			    % M = 4.6 + (m-a-b-c)*0.7                   4.6 < M <= 5.4
			    %	m=(M-4.6)/0.7+a+b+c
			    
			    % M = 5.4 + (m-a-b-c-d)*0.5                 5.4 < M <= 5.9
			    %	m=(M-5.4)/0.5+a+b+c+d		
			    
			    % M = 5.9 + (m-a-b-c-d-e)*0.4               5.9 < M >= 6.3
			    %	m=(M-5.9)/0.4+a+b+c+d+e
			    
			    % M = 6.3 + (m-a-b-c-d-e-f)*0.35            6.3 < M
			    %	m=(M-6.3)/0.35+a+b+c+d+e+f
			    
			   
			    %      a = 2,  b = 1/0.9,  c = 1.6/0.8,  d = 0.8/0.7,  e = f = 1
			
			%create seismic moment (in dyne)
			M0=zeros(numel(Ice_Mw),1);
			small_m=M0;
			small_m(sel_low2)=Ice_Mw(sel_low2);
			small_m(sel_2to3)=(Ice_Mw(sel_2to3)-2)/0.9+a;
			small_m(sel_3to46)=(Ice_Mw(sel_3to46)-3)/0.8+a+b;
			small_m(sel_46to54)=(Ice_Mw(sel_46to54)-4.6)/0.7+a+b+c;
			small_m(sel_54to59)=(Ice_Mw(sel_54to59)-5.4)/0.5+a+b+c+d;
			small_m(sel_59to63)=(Ice_Mw(sel_59to63)-5.9)/0.4+a+b+c+d+e;
			small_m(sel_hi63)=(Ice_Mw(sel_hi63)-6.3)/0.35+a+b+c+d+e+f;
			
			M0=(10.^(small_m+10))*DyneFactor;
				
			orgMw=(2/3)*log10(M0)-10.7;
			
			outData=orgMw;
			
			
			
		case 'Map2Catalog'
			%Mapseis to Catalog
			%to Mw Iceland
			
			%outData=inData;
			
			orgMw=inData;
			
			DyneFactor=(10^(-7));
    
			%Iceland constanst
			a = 2;
			b = 1/0.9;  
			c = 1.6/0.8;  
			d = 0.8/0.7;
			e = 1;
			f = 1;
			
			%min max m
			
			m_2=2;
			m_3=(3-2)/0.9+a;
			m_46=(4.6-3)/0.8+a+b;
			m_54=(5.4-4.6)/0.7+a+b+c;
			m_59=(5.9-5.4)/0.5+a+b+c+d;
			m_63=(6.3-5.9)/0.4+a+b+c+d+e;
			
			
			
			%back to M0
			M0=10^((3/2)*(orgMw+10.7))*DyneFactor;
			
			%to m 
			m = log10(Mo) - 10;
			
			%now select the intervalls
			sel_low2=orgMw<=m_2;
			sel_2to3=orgMw>m_2&orgMw<=m_3;
			sel_3to46=orgMw>m_3&orgMw<=m_46;
			sel_46to54=orgMw>m_46&orgMw<=m_54;
			sel_54to59=orgMw>m_54&orgMw<=m_59;
			sel_59to63=orgMw>m_59&orgMw<=m_63;
			sel_hi63=orgMw>m_63;
			
			Ice_Mw=zeros(numel(orgMw),1);
			
			Ice_Mw(sel_low2) = m(sel_low2);
			Ice_Mw(sel_2to3) = 2.0 + (m(sel_2to3)-a)*0.9;
			Ice_Mw(sel_3to46) = 3.0 + (m(sel_3to46)-a-b)*0.8;
			Ice_Mw(sel_46to54) = 4.6 + (m(sel_46to54)-a-b-c)*0.7;
			Ice_Mw(sel_54to59) = 5.4 + (m(sel_54to59)-a-b-c-d)*0.5; 
			Ice_Mw(sel_59to63) = 5.9 + (m(sel_59to63)-a-b-c-d-e)*0.4;
			Ice_Mw(sel_hi63) = 6.3 + (m(sel_hi63)-a-b-c-d-e-f)*0.35; 
			
			outData=Ice_Mw;
			
	end



end

end
function eventStruct=importJapanTensor(inStr,varargin)
	% importCSEPcat : import from catalog used in csep based forecast (e.g.STEP)
	
	%DE 2012
	
	
	%% Import helper functions
	import mapseis.util.importfilter.*;
	
	%% Read in file if necessary
	
	onlydescription=false;
	
	if 2==nargin
	    if strcmp(varargin{1},'-file')
		fid = fopen(inStr);
		inStr = fscanf(fid,'%c'); % Read the entire text file into an array
		fclose(fid);
	    elseif strcmp(varargin{1},'-description')
		%new option: returns a description of the import filter for the gui
		eventStruct.description = [	'A filter to import a special extracted JMA catalogue ',...
						'with focal mechanism'];
		eventStruct.displayname = 'Japan Special Tensor Importfilter' ;
		eventStruct.filetype = '.txt'
		onlydescription= true;
		
	    
	    end
	end
	
	if ~onlydescription
		%#F-net Focal Mechanism (FM) information from 1997 to 2013/04/29
		%#The symbols S1, S2, D1, D2, R1, R2 are the two Strike, Dip and Rake angles respectively, in degree.
		%#Both the JMA and F-net determined depth and magnitude are listed.
		%#The F-net depth is a centroid depth; the F-net magnitude is a moment magnitude (Mw).
		%#Regarding the variance reduction (last column), better use a threshold of 80, for further analysis.
		%########################################################################################################
		%Year Mo Day H Min Sec    Lat      Lon       Depth   M    S1  S2    D1   D2   R1   R2   Depth   M    Var.
		%					    (JMA) (JMA)                               (F-net)(F-net)
		%########################################################################################################
		%1997  1 11  5 50 21.71  31.5645  131.8818   40.32  4.8   16  195   53   37   91   89   26.00  5.4  77.39
		%1997  1 14  8 48 35.66  33.2268  132.3592    9.95  3.6  252  356   76   45  134   19   26.00  3.5  62.82
	
	
		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the whitespace character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		% Pattern for row of data
		rowPat = [nm('year',rp.intPat),ws,...
		    	nm('month',rp.intPat),ws,...
		    	nm('day',rp.intPat),ws,...
		    	nm('hour',rp.intPat),ws,...
		    	nm('minute',rp.intPat),ws,...
		    	nm('second',rp.realPat),ws,...
		    	nm('lat',rp.realPat),ws,...
		    	nm('lon',rp.realPat),ws,...
		    	nm('depth_jma',rp.realPat),ws,...
		    	nm('magnitude_jma',rp.realPat),ws,...
		    	nm('S1',rp.intPatNegPos),ws,...
		    	nm('S2',rp.intPatNegPos),ws,...
		    	nm('D1',rp.intPatNegPos),ws,...
		    	nm('D2',rp.intPatNegPos),ws,...
		    	nm('R1',rp.intPatNegPos),ws,...
		    	nm('R2',rp.intPatNegPos),ws,...
		    	nm('depth_fnet',rp.realPat),ws,...
		    	nm('magnitude_fnet',rp.realPat),ws,...
		    	nm('var',rp.realPat)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'year','month','day','hour','minute','second',...
				'lat','lon','depth_jma','magnitude_jma','S1','S2','D1',...
				'D2','R1','R2','depth_fnet','magnitude_fnet','var'};
		nonNumericFields = {};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
		    NumVal=cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}),'UniformOutput',false);
		    NumVal=cell2mat(NumVal);
		    eventStruct.(numericFields{i}) = NumVal;
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
		DateVector=[eventStruct.year,eventStruct.month,eventStruct.day,...
				eventStruct.hour,eventStruct.minute,eventStruct.second];
		eventStruct.dateNum =  datenum(DateVector); % Format string for datenum
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
		%eventStruct.dataSource = repmat({'CSEP'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end
	
	
		%Routing Info
		Routing(1,:)={'mag', 'magnitude_jma',[]};
		Routing(2,:)={'depth', 'depth_jma',[]};
		Routing(3,:)={'focal_strike', 'S1',[]};
		Routing(4,:)={'focal_dip', 'D1',[]};
		Routing(5,:)={'focal_rake', 'R1',[]};
		
		eventStruct.ROUTING_INFO=Routing;
		
		
		%Field Description
		eventStruct.CATALOG_FIELD_DESCRIPTION={	'year','year of the earthquake';...
							'month','month given in catalog ';...
							'day','day given in catalog ';...
							'hour','hour given in catalog ';...
							'minute','minute given in catalog ';...
							'second','seconds given in catalog ';...
							'lat','Latitude of event in degrees ';...
							'lon','Longitude of event in degrees ';...
							'depth_jma','depth of event in km according to the JMA catalogue ';...
							'magnitude_jma','Reported magnitude in the JMA catalogue ';...
							'S1','Strike of the first focal plane ';...
							'S2','Strike of the second focal plane ';...
							'D1','Dip of the first focal plane ';...
							'D2','Dip of the second focal plane ';...
							'R1','Rake of the first focal plane ';...
							'R2','Rake of the second focal plane ';...
							'depth_fnet','depth of event in km according to the f-net ';...
							'magnitude_fnet','Reported magnitude in the f-net ';...
							'var','not specified '};
	
		
		%Catalog Comments
		eventStruct.CATALOG_INFO_TEXT=[	'Imported by the Japan Special Tensor Importfilter',...
						'Values which are 0 might be undefined in the catalog'	];
			
		
	end

end

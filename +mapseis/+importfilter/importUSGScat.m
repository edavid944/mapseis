function eventStruct=importUSGScat(inStr,varargin)
% importUSGScat : import from USGS/NEIC (PDE) catalog
%    http://earthquake.usgs.gov/earthquakes/eqarchives/epic/epic_global.php
%    (export by using "Screen File Format (80 columns)")

% Zürich, 2012-12-11 (heavy winter time)
% by Marcus Herrmann
% v1.1


%% Import helper functions
import mapseis.util.importfilter.*;

onlydescription=false;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = [	'This importfilter reads events formated in the ',...
    					'online export format of the USGS/NEIC catalog'];
    	eventStruct.displayname = 'USGS/NEIC Catalog Importfilter' ;
    	eventStruct.filetype = '.txt';
    	onlydescription= true;
    end
end

if ~onlydescription
		
		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the whitespace character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		% Pattern for row of data
		rowPat = [ws,nm('cat','PDE[^ ]*'),ws,...
            nm('year',rp.intPat),ws,...
		    nm('month',rp.intPat),ws,...
		    nm('day',rp.intPat),ws,...
		    nm('time','[^ ]*'),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('depth',rp.intPat),ws,...
		    nm('mag',rp.realPat),ws,...
		    nm('magscale',rp.text),ws,...
		    nm('crap',rp.anything)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'year','month','day','lat','lon','depth','mag'};
		nonNumericFields = {'cat','time','magscale','crap'};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
            
            %replace empty values by something reasonable
            planeStruct.(numericFields{i})(strcmp(planeStruct.(numericFields{i}),''))={'nan'};
        
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
        % slow, but correct:
        for i=1:length(eventStruct.time)
            timeChar=eventStruct.time{i,1};
            if length(timeChar)>=6
                [~,~,~,hr(i,1),min(i,1),sec(i,1)] = datevec(timeChar,'HHMMSS');
            elseif length(timeChar)==4
                [~,~,~,hr(i,1),min(i,1),sec(i,1)] = datevec(timeChar,'HHMM');
            elseif length(timeChar)<=2
                [~,~,~,hr(i,1),min(i,1),sec(i,1)] = datevec(timeChar,'HH');
            end
        end
        % The following is faster, but not quite correct, because the
        % time string can be given as as |HHMM     | instead of |HHMM00.00|
        % so it will be interpreted wrongly as MM=HH,SS=MM (00MMSS.00)
%         [~,~,~,hr,min,sec] = ...
%             datevec(sprintf('%09.2f',str2double(eventStruct.time)),'HHMMSS');
		eventStruct.dateNum = datenum(... % Format string for dateNum
            eventStruct.year,eventStruct.month,eventStruct.day,hr,min,sec);
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
% 		eventStruct.dataSource = repmat({'USGS'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
        
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
        end
		
        %Field Description
		eventStruct.CATALOG_FIELD_DESCRIPTION={
            'cat','The type of catalog (e.g. PDE-Q for most recent events ';...
            'year','year ';...
            'month','month given in catalog ';...
            'day','day given in catalog ';...
            'time','time string given in the original catalog ';...
            'lat','Latitude of event in degrees ';...
            'lon','Longitude of event in degrees ';...
            'depth','depth of event in km ';...
            'mag','Reported magnitude in the catalog ';...
            'magscale','The type of the scale used for the magnitud ';...
            'crap','Some yet unknown additional information given at the end ';...
            'dateNum','The string containing the date and the time '};

        %Catalog Comments
        eventStruct.CATALOG_INFO_TEXT=['Imported by the USGS/NEIC importfilter v1'];
		
end

end

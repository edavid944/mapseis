function eventStruct=importTemplate(inStr,varargin)
% This a Template with important ingridient of a Importfilter, see the exist ones 
% for an Example

%% Import helper functions
import mapseis.util.importfilter.*;


onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This is only a Template for building a Importfilter, DO NOT USE IT.';
    	eventStruct.displayname = 'Import Template' ;
    	eventStruct.filetype = '.txt'
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();



end

end
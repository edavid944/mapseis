function eventStruct=importNTSCatalog(inStr,varargin)
%Import filter for .nts formated catalogs as used by the STEP model (java version)
%I don't know where else this format is used 

%% Import helper functions
import mapseis.util.importfilter.*;


onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = [	'This importfilter is for ascii format catalogs in the nts ',... 
    					'format as used by STEP model (java version). At the moment',...
    					'only fields used by STEP are supported. This format has no',...
    					'native support for negative magnitudes, but the filter will',...
    					'still import them as mags with a value of -0.1 to -0.9',...
    					'if the negative mags are stored differently (e.g. Offset),',...
    					'suitable routing filter have to be used'];
    	eventStruct.displayname = 'Nts formated catalogs v1' ;
    	eventStruct.filetype = '*.nts'
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();
	
	%get the lines
	thelines = regexp(inStr,rp.line,'match');
	


	
	
	%go throught the lines and build the structur
	count=1;
	for i=1:numel(thelines)
		%try
		Line1=thelines{i};
		
		%disp(count)
		
		%Java code from STEP
		%  String sEventId = obsEqkEvent.substring(2, 10).trim();
		%  String sDataSource = obsEqkEvent.substring(10, 12).trim();
		%  char sEventVersion = obsEqkEvent.substring(12, 13).trim().charAt(0);
		%  String sYear = obsEqkEvent.substring(13, 17).trim();
		%  String sMonth = obsEqkEvent.substring(17, 19).trim();
		%  String sDay = obsEqkEvent.substring(19, 21).trim();
		%  String sHour = obsEqkEvent.substring(21, 23).trim();
		%  String sMinute = obsEqkEvent.substring(23, 25).trim();
		%  String sSecond = divideAndGetString(obsEqkEvent.substring(25, 28), 10);
		%  //sSecond = sSecond.substring(0, sSecond.indexOf('.')); //This is to get rid of the decimal point in seconds
		%  String sLatitude = divideAndGetString(obsEqkEvent.substring(28, 35),
					%  10000);
		%  String sLongitude = divideAndGetString(obsEqkEvent.substring(35, 43),
					%  10000);
		%  String sDepth = divideAndGetString(obsEqkEvent.substring(43, 47), 10);
		%  double lat = Double.parseDouble(sLatitude);
		%  double lon = Double.parseDouble(sLongitude);
		%  double depth = Double.parseDouble(sDepth);
%  
		%  //if lat or lon of the events are outside the region bounds then neglect them.
		%  if(lat < RegionDefaults.searchLatMin || lat >RegionDefaults.searchLatMax)
			%  return null;
		%  if(lon < RegionDefaults.searchLongMin || lon > RegionDefaults.searchLongMax)
			%  return null;
%  
		%  String sMagnitude = divideAndGetString(obsEqkEvent.substring(47, 49), 10);
		%  //if(sMagnitude ==null || sMagnitude.equals(""))
		%  //return null;
		%  //String sNst = sRecord.substring(49, 52).trim();
		%  //String sNph = sRecord.substring(52, 55).trim();
		%  //String sDmin = divideAndGetString(sRecord.substring(55, 59), 10);
		%  //String sRmss = divideAndGetString(sRecord.substring(59, 63), 100);
		%  String sErho = divideAndGetString(obsEqkEvent.substring(63, 67), 10);
		%  String sErzz = divideAndGetString(obsEqkEvent.substring(67, 71), 10);
		%  //String sGap = multiplyAndGetString(sRecord.substring(71, 73), 3.6);
		%  String sMagnitudeType = obsEqkEvent.substring(73, 74).trim();
		%  //String sNumberOfStations = obsEqkEvent.substring(74, 76).trim();
		%  String sMagnitudeError = divideAndGetString(obsEqkEvent.substring(76, 78),
					%  10);
		
					
		%example Line
		%E 11111111CI62008 4010325 24 342133-1185370  5310.0                     0L  10
					
		%go through all of 5 the lines
		%-----------------------------
		%=============================
		
		%I'm not hungarian... no s,i,f,m,v ...
		
		%Line 1
		%++++++
		
		%Datasource stuff
		%----------------
		eventStruct.EventType{count,1}=Line1(1);
		eventStruct.EventID{count,1}=Line1(3:10);
		eventStruct.DataSource{count,1}=Line1(11:12);
		eventStruct.EventVersion{count,1}=Line1(13:13);
		
		%set all char=' ' to '0', this is to avoid errors due to empty strings
		%it should not be a problem but I have to be carefull and check if the 
		%result is sensible.(it can be very dangerous, if the numbers are "left aligned")
		Line1(Line1==' ')='0';
		
		
		%date and time
		%-------------
		%(I'm just lazy be reformating string to the input template, but it should not matter
		DateTimeString=[Line1(14:17),'/',Line1(18:19),'/',Line1(20:21),...
				'T',Line1(22:23),':',Line1(24:25),':',Line1(26:27),'.',Line1(28:28),'00']
		eventStruct.dateNum(count,1) = datenum(DateTimeString,'yyyy/mm/ddTHH:MM:SS.FFF');
		
		eventStruct.Year(count,1)=str2num(Line1(14:17));
		eventStruct.Month(count,1)=str2num(Line1(18:19));
		eventStruct.Day(count,1)=str2num(Line1(20:21));
		eventStruct.Hour(count,1)=str2num(Line1(22:23));
		eventStruct.Minute(count,1)=str2num(Line1(24:25));
		eventStruct.Second(count,1)=str2num(Line1(26:28))/10;
		
		%Locations
		%---------
		eventStruct.Latitude(count,1)=str2num(Line1(29:35))/10000;
		eventStruct.Longitude(count,1)=str2num(Line1(36:43))/10000;
		eventStruct.Depth(count,1)=str2num(Line1(44:47))/10;
		eventStruct.HorzError(count,1)=str2num(Line1(64:67))/10;
		eventStruct.VertError(count,1)=str2num(Line1(68:71))/10;
		
		%Magnitude
		%---------
		eventStruct.Magnitude(count,1)=str2num(Line1(48:49))/10;
		eventStruct.MagType{count,1}=Line1(74:74);
		eventStruct.MagError(count,1)=str2num(Line1(77:78))/10;
		
				
		
		count=count+1;
		
	end
	
	

	
	%Routing Info
	Routing(1,:)={'mag', 'Magnitude',[]};
	Routing(2,:)={'lon', 'Longitude',[]};
	Routing(3,:)={'lat', 'Latitude',[]};
	Routing(4,:)={'depth', 'Depth',[]};
	
	eventStruct.ROUTING_INFO=Routing;
	
		
	%Field Description
	eventStruct.CATALOG_FIELD_DESCRIPTION={'EventType','Says it all E=Earthquake ';...
					       'EventID','ID given to the event by the catalog ';...
					       'DataSource','Source of the catalog ';...
					       'EventVersion','Revision number of the event ';...
					       'dateNum','The string containing the date and the time ';...
					       'Year','Year given in catalog ';...
					       'Month','Month given in catalog ';...
					       'Day','Day given in catalog ';...
					       'Hour','Hour given in catalog ';...
					       'Minute','Minute given in catalog ';...
					       'Second','Second and 10th seconds given in catalog ';...
					       'Latitude', 'Latitude of event in degrees ';...
					       'Longitude','Longitude of event in degrees ';...
					       'Depth','Depth of event in km ';...
					       'HorzError','Horizontal Error of the localization ';...
					       'VertError','Vertical Error of the localization ';...
					       'Magnitude','Reported magnitude ';...
					       'MagType','Type of the Magnitude used for the event ';...
					       'MagError','Error of the Magnitude '};
	
	
	%Catalog Comments
	eventStruct.CATALOG_INFO_TEXT=[	'Imported by the .nts importfilter v1 (only fields used by STEP).',...
					'Values which are 0 might be undefined in the catalog'	];
					
end

end

function eventStruct=importUSGScat_TempSite(inStr,varargin)
% importSCECCatRead : import from SCEC database
% Used to import files from  http://earthquake.usgs.gov/earthquakes/eqarchives/epic/epic_global.php
% at the moment there page is linking do a beta version of the new system, this 
% filter allows to import files from this new version, but it will likely change later


%DE Aug. 2013

%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = ['This importfilter reads events formated in the ',...
    					'online export format of the USGS/NEIC catalog, the temorary site (Aug. 2013)',...
    					' the filter will likely change later, when the final page is available'];
    	eventStruct.displayname = 'USGS/NEIC Catalog Importfilter Temporary Site' ;
    	eventStruct.filetype = '.txt'
    	onlydescription= true;
    	
    
    end
end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the whitespace character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		%#YYY/MM/DD	HH	mm	SS.ss	MAG	LAT	LON
		%DATE_TIME             	    LAT	     LON       	  DEP	MAG	MT	SC
		%--------------------------------------------------------------------------
		%2013-08-24 17:18:18.00	 36.711	  14.996	 10.0	4.4	mb	us
		
		% Special date format (YYYY-MM-DD)
		specDatePat = rp.specialDate;
		specTimePat = rp.timePat;
		
		% Pattern for row of data
		rowPat = [nm('date',specDatePat),ws,...
		    nm('time',specTimePat),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('depth',rp.realPat),ws,...
		    nm('mag',rp.realPat),ws,...
		    nm('magscale',rp.text),ws,...
		    nm('SC',rp.text)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'mag','lat','lon','depth'};
		nonNumericFields = {'magscale','SC'};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
		%it could be vectorized with cell fun, but as this probably is
		%a single use filter anyway go for the loop.
		%not the most sophisticate code, just a fast fix.
		TheDate=nan(size(eventStruct.lat));
		for i=1:numel(planeStruct.date)
			StringDate=[planeStruct.date{i},' ',planeStruct.time{i}];
			%disp(StringDate)
			
			
			TheDate(i)=datenum(StringDate,'yyyy-mm-dd HH:MM:SS.FFF');
		end
		
		eventStruct.dateNum=TheDate;
						
		%eventStruct.dateNum = ...
		%    datenum(... % MATLAB built in function, use datestr to get a string back
		%      [vertcat(planeStruct.date{:}),...
		%       repmat(' ',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.hr{:}),...
		%       repmat(':',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.min{:}),...
		%       repmat(':',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.sec{:})],...
		%    'mm/dd/yy HH:MM:SS.FFF'); % Format string for datenum
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
		eventStruct.dataSource = repmat({'PDE'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end
		
		 %Field Description
		eventStruct.CATALOG_FIELD_DESCRIPTION={
            'lat','Latitude of event in degrees ';...
            'lon','Longitude of event in degrees ';...
            'depth','depth of event in km ';...
            'mag','Reported magnitude in the catalog ';...
            'magscale','The type of the scale used for the magnitud ';...
            'SC','Some yet unknown additional information given at the end ';...
            'dateNum','The string containing the date and the time '};

            %Catalog Comments
            eventStruct.CATALOG_INFO_TEXT=['Imported by the USGS/NEIC importfilter TempSite'];
		

end

end

function eventStruct=importPEGASOSCatRead(inStr,varargin)
% importSCECCatRead : import from SCEC database
% Reads the earthquake event data returned by accessing the SCEC 
% database into an event structure

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This importfilter was build for importing the catalogs for the PEGASOS project. Version 1.0';
    	eventStruct.displayname = 'PEGASOS Importfilter' ;
    	eventStruct.filetype = '.txt'
    	onlydescription= true;
    	
    end
    
end

if ~onlydescription

	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();
	
	% Import the white space character 
	ws = rp.ws;
	
	%Import semicolon
	sc = rp.sc;
	
	% Define function to add name to pattern for named pattern matching
	nm = rp.name;
	
	% Pattern for row of data
	rowPat = [nm('org_ID',rp.anything),sc,...
		nm('catname',rp.anything),sc,...
		nm('providing_agency',rp.anything),sc,...
		nm('cited_agency',rp.anything),sc,...
		nm('delivery_year',rp.realPat),sc,...
	    nm('year',rp.intPat),sc,...
	    nm('month',rp.intPat),sc,...
	    nm('day',rp.intPat),sc,...
	    nm('hr',rp.intPat),sc,...
	    nm('min',rp.intPat),sc,...
	    nm('sec',rp.realPat),sc,...
	    nm('lat',rp.realPat),sc,...
	    nm('errlat_deg',rp.realPat),sc,...
	    nm('errlat_km',rp.realPat),sc,...
	    nm('lon',rp.realPat),sc,...
	    nm('errlon_deg',rp.realPat),sc,...
	    nm('errlon_km',rp.realPat),sc,...
	    nm('depth',rp.intPat),sc,...
	    nm('errdepth_km',rp.realPat),sc,...
	    nm('depth_method',rp.anything),sc,...
	    nm('loc_agency',rp.anything),sc,...
	    nm('mag',rp.realPat),sc,...
	    nm('errmag',rp.realPat),sc,...
		nm('magtype',rp.anything),sc,...
		nm('mag_method',rp.anything),sc,...
	    nm('mag_agency',rp.anything),sc,...
	    nm('RMS',rp.realPat),sc,...
	    nm('GAP',rp.intPat),sc,...
	    nm('mindist',rp.intPat),sc,...
	    nm('io',rp.realPat),sc,...
	    nm('io_method',rp.anything),sc,...
	    nm('io_agency',rp.anything),sc,...
	    nm('NO',rp.intPat),sc,...
	    nm('place',rp.anything),sc,...
	    nm('eventype',rp.anything),sc,...
	    nm('appraisal',rp.anything)];
	
	%% Convert the input string into a struct of data
	planeStruct = readTable(inStr,rp.line,rowPat);
	
	%% Define the fields that are present in the final output 
	numericFields = {'year','month','day','hr','min','sec','lat','errlat_deg','errlat_km',...
						'lon','errlon_deg','errlon_km','depth','errdepth_km','mag','errmag',...
						'RMS','GAP','mindist','io','NO'};
	nonNumericFields = {'org_ID','catname','providing_agency','cited_agency','depth_method',...
						'loc_agency','magtype','mag_method','mag_agency','io_method',...
						'io_agency','place','eventype','appraisal'};
	
	%replace empty values by something reasonable
	
	
	%% Convert numerical fields to doubles
	for i=1:numel(numericFields)
		
		%replace empty values by something reasonable
		planeStruct.(numericFields{i})(strcmp(planeStruct.(numericFields{i}),''))={'nan'};
		
	    eventStruct.(numericFields{i}) = ...
	        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
	end
	
	%% Include the non-numeric fields directly into the output structure
	for i=1:numel(nonNumericFields)
	    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
	end
	
	%% Convert the date and time to a number
	
	%eventStruct.dateNum = ...
	%    datenum(... % MATLAB built in function, use datestr to get a string back
	%      [vertcat(planeStruct.date{:}),...
	%       repmat(' ',numel(planeStruct.date),1),...
	%       vertcat(planeStruct.time{:})],...
	%    'yyyy/mm/dd HH:MM:SS.FFF'); % Format string for datenum
	
	%first replace  hr, min, sec fields which are 'nan' with '0' 
	%if not done, the dateNum will be 'nan' in those cases.
	%Added month day as well, in case it is a historic date

	eventStruct.hr(isnan(eventStruct.month))=1;
	eventStruct.hr(isnan(eventStruct.day))=1;
	eventStruct.hr(isnan(eventStruct.hr))=1;
	eventStruct.min(isnan(eventStruct.min))=1;
	eventStruct.sec(isnan(eventStruct.sec))=1;
	
	eventStruct.dateNum = datenum([ eventStruct.year, eventStruct.month,...
									eventStruct.day, eventStruct.hr,...
									 eventStruct.min, eventStruct.sec]);
	
	%% Add a field for where the data comes from
	% This is useful since the event ID is only unique for a particular data
	% source, so [dataSource,'_',ID] should be unique
	eventStruct.dataSource = repmat({'SED'},size(eventStruct.dateNum));
	
	%% Sort the events by time ordering
	
	[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
	fldNames = fieldnames(eventStruct);
	for i=1:numel(fldNames)
	    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
	       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
	    end
	end
end

end
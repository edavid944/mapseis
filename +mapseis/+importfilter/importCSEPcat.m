function eventStruct=importCSEPcat(inStr,varargin)
% importCSEPcat : import from catalog used in csep based forecast (e.g.STEP)

%DE 2012


%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = [	'This importfilter reads catalogs formated in the ',...
    					'format used by csep forecast models like STEP'];
    	eventStruct.displayname = 'CSEP Catalog Importfilter' ;
    	eventStruct.filetype = '.txt'
    	onlydescription= true;
    	
    
    end
end

if ~onlydescription
		
%		1				2				3	
%4				5				6			
%7				8				9			
%10				11				12		
%13				14				15				 
%lon			lat			dec year	
%month			day			mag		
%depth			hour			minute			sec		
%horz.err		depth.err		mag.err			


		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the whitespace character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		% Pattern for row of data
		rowPat = [nm('lon',rp.floatPat),ws,...
		    nm('lat',rp.floatPat),ws,...
		    nm('decyear',rp.floatPat),ws,...
		    nm('month',rp.floatPat),ws,...
		    nm('day',rp.floatPat),ws,...
		    nm('mag',rp.floatPat),ws,...
		    nm('depth',rp.floatPat),ws,...
		    nm('hour',rp.floatPat),ws,...
		    nm('minute',rp.floatPat),ws,...
		    nm('second',rp.floatPat),ws,...
		    nm('horz_err',rp.floatPat),ws,...
		    nm('depth_err',rp.floatPat),ws,...
		    nm('mag_err',rp.floatPat)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'lon','lat','decyear','month','day','mag',...
				'depth','hour','minute','second','horz_err','depth_err','mag_err'};
		nonNumericFields = {};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
		DateVector=[floor(eventStruct.decyear),eventStruct.month,eventStruct.day,...
				eventStruct.hour,eventStruct.minute,eventStruct.second]
		eventStruct.dateNum =  datenum(DateVector); % Format string for datenum
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
		%eventStruct.dataSource = repmat({'CSEP'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end

	
	
		
		%Field Description
		eventStruct.CATALOG_FIELD_DESCRIPTION={'lon','Longitude of event in degrees ';...
					       'lat','Latitude of event in degrees ';...
					       'decyear','decimal year ';...
					       'month','month given in catalog ';...
					       'day','day given in catalog ';...
					       'mag','Reported magnitude in the catalog ';...
					       'depth','depth of event in km ';...
					       'hour','hour given in catalog ';...
					       'minute','minute given in catalog ';...
					       'second','seconds given in catalog ';...
					       'horz_err','Horizontal Error of the localization ';...
					       'depth_err','Vertical Error of the localization ';...
					       'mag_err','Error of the Magnitude ';...
					       'dateNum','The string containing the date and the time '};
	
	
	%Catalog Comments
	eventStruct.CATALOG_INFO_TEXT=[	'Imported by the csep importfilter v1',...
					'Values which are 0 might be undefined in the catalog'	];
		
		
end

end

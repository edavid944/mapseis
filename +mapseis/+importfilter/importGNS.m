function eventStruct=importGNS(inStr,varargin)
% importGNS : import from GNS database
% Reads the earthquake event data returned by accessing the GNS 
% database into an event structure



%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This import filter is for importing GNS type data found under http://magma.geonet.org.nz/resources/quakesearch/';
    	eventStruct.displayname = 'GNS Importfilter' ;
    	eventStruct.filetype = '.csv'
    	onlydescription= true;
    	
    end
    
end

if ~onlydescription

	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();
	
	% Import the white space character 
	ws = rp.ws;
	
	%Import comma
	cs = rp.cs;
	
	% Define function to add name to pattern for named pattern matching
	nm = rp.name;
	
	%entries in catalog
	%CUSP_ID LAT LONG NZMGE	NZMGN ORI_YEAR ORI_MONTH ORI_DAY ORI_HOUR ORI_MINUTE ORI_SECOND	MAG DEPTH

	% Pattern for row of data
	rowPat = [nm('CUSP_ID',rp.realPat),cs,...
		nm('LAT',rp.realPat),cs,...
		nm('LONG',rp.realPat),cs,...
		nm('NZMGE',rp.realPat),cs,...
		nm('NZMGN',rp.realPat),cs,...
	    nm('ORI_YEAR',rp.realPat),cs,...
	    nm('ORI_MONTH',rp.realPat),cs,...
	    nm('ORI_DAY',rp.realPat),cs,...
	    nm('ORI_HOUR',rp.realPat),cs,...
	    nm('ORI_MINUTE',rp.realPat),cs,...
	    nm('ORI_SECOND',rp.realPat),cs,...
	    nm('mag',rp.realPat),cs,...
	    nm('depth',rp.realPat)];
	
	%% Convert the input string into a struct of data
	planeStruct = readTable(inStr,rp.line,rowPat);
	
	%% Define the fields that are present in the final output 
	numericFields = {'LAT','LONG','NZMGE','NZMGN','ORI_YEAR','ORI_MONTH','ORI_DAY','ORI_HOUR','ORI_MINUTE',...
						'ORI_SECOND','mag','depth'};
	nonNumericFields = {'CUSP_ID'};
	
	%replace empty values by something reasonable
	
	
	%% Convert numerical fields to doubles
	for i=1:numel(numericFields)
		
		%replace empty values by something reasonable
		planeStruct.(numericFields{i})(strcmp(planeStruct.(numericFields{i}),''))={'nan'};
		
	    eventStruct.(numericFields{i}) = ...
	        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
	end
	
	%% Include the non-numeric fields directly into the output structure
	for i=1:numel(nonNumericFields)
	    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
	end
	
	%% Convert the date and time to a number
	
	%eventStruct.dateNum = ...
	%    datenum(... % MATLAB built in function, use datestr to get a string back
	%      [vertcat(planeStruct.date{:}),...
	%       repmat(' ',numel(planeStruct.date),1),...
	%       vertcat(planeStruct.time{:})],...
	%    'yyyy/mm/dd HH:MM:SS.FFF'); % Format string for datenum
	
	%first replace  hr, min, sec fields which are 'nan' with '0' 
	%if not done, the dateNum will be 'nan' in those cases.
	eventStruct.ORI_MONTH(isnan(eventStruct.ORI_MONTH))=1;
	eventStruct.ORI_DAY(isnan(eventStruct.ORI_DAY))=1;
	eventStruct.ORI_HOUR(isnan(eventStruct.ORI_HOUR))=1;
	eventStruct.ORI_MINUTE(isnan(eventStruct.ORI_MINUTE))=1;
	eventStruct.ORI_SECOND(isnan(eventStruct.ORI_SECOND))=1;
	eventStruct.mag(isnan(eventStruct.mag))=0;
	eventStruct.depth(isnan(eventStruct.depth))=0;
	
	eventStruct.dateNum = datenum([ eventStruct.ORI_YEAR, eventStruct.ORI_MONTH,...
									eventStruct.ORI_DAY, eventStruct.ORI_HOUR,...
									 eventStruct.ORI_MINUTE, eventStruct.ORI_SECOND]);
	
	%% Add a field for where the data comes from
	% This is useful since the event ID is only unique for a particular data
	% source, so [dataSource,'_',ID] should be unique
	eventStruct.dataSource = repmat({'GNS'},size(eventStruct.dateNum));
	
	%% Sort the events by time ordering
	
	[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
	fldNames = fieldnames(eventStruct);
	for i=1:numel(fldNames)
	    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
	       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
	    end
	end
	
	%Routing Info
	Routing(1,:)={'lon', 'LONG',[]};
	Routing(2,:)={'lat', 'LAT',[]};
	
	
	eventStruct.ROUTING_INFO=Routing;
	
end

end
function eventStruct=importCSV_MinCat(inStr,varargin)
	% importCSV_MinCat : imports a csv file semicolon separated as produce by excel 
	% with following column: lon;lat;date;time;mag ;depth  
	% e.g.:  lon;lat;date;time;mag ;depth
	%	-120;45;2001-04-21;13:30:24;1.3;11
	%	-120.1;45;2001-04-22;14:23:00;2.5;12
	

	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2014 David Eberhard
	
	
	%% Import helper functions
	import mapseis.util.importfilter.*;
	
	%% Read in file if necessary
	% If called with -file argument then open the file and read it into a
	% string.  Assume that we have enough memory to read the file into memory
	% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
	
	onlydescription=false;
	
	if 2==nargin
	    if strcmp(varargin{1},'-file')
		fid = fopen(inStr);
		inStr = fscanf(fid,'%c'); % Read the entire text file into an array
		fclose(fid);
	    elseif strcmp(varargin{1},'-description')
		%new option: returns a description of the import filter for the gui
		eventStruct.description = ['This importfilter reads events formated in the ',...
						'minimal catalog csv format. The format is a ; separate table file with ',...
						'the following fields: lon;lat;date;time;mag;depth, and format ',... 
						'yyyy-mm-dd for date and HH_MM:SS for time. Such a file can ',...
						'produced with table calculation programs like Excel'];
		eventStruct.displayname = 'CVS_minCat' ;
		eventStruct.filetype = '.csv'
		onlydescription= true;
		
	    
	    end
	end
	
	if ~onlydescription
	
			%% Define the regular expressions needed to access the data
			rp = regexpPatternCatalog();
			
			% Import the semicolon separator 
			sc = rp.sc;
			
			% Define function to add name to pattern for named pattern matching
			nm = rp.name;
		
			
			% Special date format (YYYY-MM-DD)
			specDatePat = rp.specialDate;
			specTimePat = rp.timePatCSV;
			
			% Pattern for row of data
			rowPat = [nm('lon',rp.realPat),sc,...
				nm('lat',rp.realPat),sc,...
				nm('date',specDatePat),sc,...
				nm('time',specTimePat),sc,...
				nm('mag',rp.realPat),sc,...
				nm('depth',rp.realPat)];
			
			%% Convert the input string into a struct of data
			planeStruct = readTable(inStr,rp.line,rowPat);
			
			%% Define the fields that are present in the final output 
			numericFields = {'mag','lat','lon','depth'};
			nonNumericFields = {};
			
			%% Convert numerical fields to doubles
			
			for i=1:numel(numericFields)
			    eventStruct.(numericFields{i}) = ...
				cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
			end
			
			%% Include the non-numeric fields directly into the output structure
			for i=1:numel(nonNumericFields)
			    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
			end
			
			%% Convert the date and time to a number
			%it could be vectorized with cell fun, but as this probably is
			%a single use filter anyway go for the loop.
			%not the most sophisticate code, just a fast fix.
			TheDate=nan(size(eventStruct.lat));
			for i=1:numel(planeStruct.date)
				StringDate=[planeStruct.date{i},' ',planeStruct.time{i}];
				%disp(StringDate)
				
				
				TheDate(i)=datenum(StringDate,'yyyy-mm-dd HH:MM:SS');
			end
			
			eventStruct.dateNum=TheDate;
		
			%% Add a field for where the data comes from
			% This is useful since the event ID is only unique for a particular data
			% source, so [dataSource,'_',ID] should be unique
			eventStruct.dataSource = repmat({'MCvs'},size(eventStruct.dateNum));
			
			%% Sort the events by time ordering
			
			[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
			fldNames = fieldnames(eventStruct);
			for i=1:numel(fldNames)
			    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
			       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
			    end
			end
			
			 %Field Description
			eventStruct.CATALOG_FIELD_DESCRIPTION={
					'lat','Latitude of event in degrees ';...
					'lon','Longitude of event in degrees ';...
					'depth','depth of event in km ';...
					'mag','Reported magnitude in the catalog ';...
					'dateNum','The string containing the date and the time '};
	
		    %Catalog Comments
		    eventStruct.CATALOG_INFO_TEXT=['Imported by the MinCat CSV importfilter'];
			
	
	end

end

function eventStruct=importSeismoXML(inStr,varargin)
%This function imports xml files published on seismo

%% Import helper functions
import mapseis.util.importfilter.*;


%Note: urlread can be used to load content from the web

onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
    	%add path for the xmltree toolbox
        addpath('./AddOneFiles/xmltree/')
        %create the tree, this may take some time
	try
        	XTree=xmltree(inStr);
        catch
        	%maybe the tool was not compiled, so compile it
        	oldpath=pwd
        	cd('./AddOneFiles/xmltree/@xmltree/private');
        	mex -O xml_findstr.c
        	cd(oldpath);
        	%try again
        	XTree=xmltree(inStr);
        end	
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This function imports XML based Catalogs from seismo.ethz.ch homepage';
    	eventStruct.displayname = 'SeimoXML import' ;
    	eventStruct.filetype = '.xml'
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	%The tree should now exist, get the structur from it	
	RawStruct=convert(XTree);
	EQStruct=cellArrayToPlane(RawStruct.EARTHQUAKE);
	
	%The numeric and none numeric fields
	
	%% Define the fields that are present in the final output 
	numericFields = {'YMD','HM','SEC','LAT','LON','DEPTH','FC','SD','R0',...
					'MW','MWERR','ML','NSTNS','PGA10','PGV10'};
	nonNumericFields = {'PICK'};
	
	for i=1:numel(numericFields)
		
		%replace empty values by something reasonable
		EQStruct.(numericFields{i})(strcmp(EQStruct.(numericFields{i}),''))={'nan'};
		
	    eventStruct.(numericFields{i}) = ...
	        cellfun(@(x) sscanf(x,'%f'),EQStruct.(numericFields{i}));
	end
	
	%% Include the non-numeric fields directly into the output structure
	for i=1:numel(nonNumericFields)
	    eventStruct.(nonNumericFields{i}) = EQStruct.(nonNumericFields{i});
	end
	
	
	%Now some additional fields have to be created
	
	%create a timestring with following properties: yyyymmddTHHMMSS.FFF 
	TimeStr=cellfun(@(x,y,z) [x 'T' y z '0'], EQStruct.YMD, EQStruct.HM, EQStruct.SEC, 'UniformOutput',false );
	
	%now to datenum 
	eventStruct.dateNum = datenum(TimeStr,'yyyymmddTHHMMSS.FFF');
	
	
	%now edit the fields and add fields to have suitable names
	%eventStruct.depth=eventStruct.DEPTH;
	%rmfield(eventStruct,'DEPTH');
	
	%eventStruct.lon=eventStruct.LON;
	%rmfield(eventStruct,'LON');
	
	%eventStruct.lat=eventStruct.LAT;
	%rmfield(eventStruct,'LAT');
	
	%choose MW as default magnitude
	%eventStruct.mag=eventStruct.MW;
	
	%add an id
	eventStruct.dataSource = repmat({'SEDXML'},size(eventStruct.dateNum));
	
	%sort them
	[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
	fldNames = fieldnames(eventStruct);
	for i=1:numel(fldNames)
	    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
	       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
	    end
	end

	
	%Add RoutingStructur
	Routing(1,:)={'depth', 'DEPTH',[]};
	Routing(2,:)={'lon', 'LON',[]};
	Routing(3,:)={'lat', 'LAT',[]};
	Routing(4,:)={'mag', 'MW',[]};
	
	eventStruct.ROUTING_INFO=Routing;
end

end
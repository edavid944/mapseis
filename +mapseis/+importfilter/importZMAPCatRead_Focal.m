function eventStruct=importZMAPCatRead_Focal(inStr,varargin)
% importZMAPCatRead : import from ZMAP Format files  


% Copyright 2007-2007 The MathWorks, Inc.
% $Revision: 86 $    $Date: 2007-05-02 15:19:00 +0100 (Wed, 02 May 2007)$
% $Author: Stefan Wiemer$

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
% if 2==nargin
%     if strcmp(varargin{1},'-file')
%         load(inStr);
%     end
% end

onlydescription=false;

a=inStr;

GetFocal=true;

if 2==nargin
    if strcmp(varargin{1},'-file')
        load(inStr);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = ['This importfilter works with old Zmap arrays named "a" and saved as .mat files. ',...
    				'Only basic information of the array (Location, Magnitude, Depth and Time) ',...
    				  'is imported by this version of the filter, this version imports catalogs with focal mechanism and no seconds'];
    	eventStruct.displayname = 'Zmap Importfilter (focal)' ;
    	eventStruct.filetype = '.mat'    	
    	onlydescription= true;
    	
    end
    
    GetFocal=true;
end

if 3==nargin
    if strcmp(varargin{1},'-file')
        load(inStr);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = ['This importfilter works with old Zmap arrays named "a" and saved as .mat files. ',...
    							  'Only basic information of the array (Location, Magnitude, Depth and Time) ',...
    							  'is imported by this version of the filter, this version imports catalogs with focal mechanism and no seconds'];
    	eventStruct.displayname = 'Zmap Importfilter (focal)' ;
    	eventStruct.filetype = '.mat'    	
    	onlydescription= true;

    end
	
    GetFocal=varargin{2};  

end



if ~onlydescription
		
		
		if ~GetFocal
			eventStruct.mag = a(:,6);
			eventStruct.depth = a(:,7);
			eventStruct.lat = a(:,2);
			eventStruct.lon = a(:,1);
			if length(a(1,:))==9
			    eventStruct.dateNum = datenum([ floor(a(:,3)), a(:,4), a(:,5), a(:,8) a(:,9) a(:,9)*0 ]);
			else
			    eventStruct.dateNum = datenum([ floor(a(:,3)), a(:,4), a(:,5), a(:,8), a(:,9) a(:,10)]);
			end
		else
			%get with focal mechanisms
			eventStruct.mag = a(:,6);
			eventStruct.depth = a(:,7);
			eventStruct.lat = a(:,2);
			eventStruct.lon = a(:,1);
			eventStruct.dateNum = datenum([ floor(a(:,3)), a(:,4), a(:,5), a(:,8) a(:,9) a(:,9)*0 ]);
			eventStruct.focal_strike=a(:,10);
			eventStruct.focal_dip=a(:,11);
			eventStruct.focal_rake=a(:,12);
		end
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end
end

end
function eventStruct=importSCECShortCatRead(inStr,varargin)
% importSCECCatRead : import from SCEC database
% Shorted catalog, probably never needed again
% Reads the earthquake event data returned by accessing the SCEC 
% database into an event structure

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = ['This importfilter reads shorted catalogs by the South California Earthquake Data Center (www.data.scec.org). ',...
    								'The files have to saved as normal text files in order to read them'];
    	eventStruct.displayname = 'SCEC short Importfilter' ;
    	eventStruct.filetype = '.txt'
    	onlydescription= true;
    	
    
    end
end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the whitespace character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		%#YYY/MM/DD	HH	mm	SS.ss	MAG	LAT	LON
		
		% Special date format (mm/dd/yy)
		specDatePat = '\d*/\d*/\d\d';
		
		% Pattern for row of data
		rowPat = [nm('date',specDatePat),ws,...
		    nm('hr',rp.realPat),ws,...
		    nm('min',rp.realPat),ws,...
		    nm('sec',rp.realPat),ws,...
		    nm('mag',rp.realPat),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'mag','lat','lon','hr','min','sec'};
		nonNumericFields = {};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
		%it could be vectorized with cell fun, but as this probably is
		%a single use filter anyway go for the loop.
		%not the most sophisticate code, just a fast fix.
		TheDate=nan(size(eventStruct.hr));
		for i=1:numel(planeStruct.date)
			StringDate=[planeStruct.date{i},' '];
			
			if eventStruct.hr(i)<10
				%needs an additional zero
				StringDate = [StringDate,'0',planeStruct.hr{i},':'];
			else
				StringDate = [StringDate,planeStruct.hr{i},':'];
			end

			if eventStruct.min(i)<10
				%needs an additional zero
				StringDate = [StringDate,'0',planeStruct.min{i},':'];
			else
				StringDate = [StringDate,planeStruct.min{i},':'];
			end
			
			
			
			if eventStruct.sec(i)<10
				%needs an additional zero
				StringDate = [StringDate,'0',num2str(eventStruct.sec(i),'%1.3f')];
			else
				StringDate = [StringDate,num2str(eventStruct.sec(i),'%1.3f')];
			end
			
			TheDate(i)=datenum(StringDate,'mm/dd/yy HH:MM:SS.FFF');
		end
		
		eventStruct.dateNum=TheDate;
		
		%add depth 
		eventStruct.depth=zeros(size(eventStruct.hr));
		
		%eventStruct.dateNum = ...
		%    datenum(... % MATLAB built in function, use datestr to get a string back
		%      [vertcat(planeStruct.date{:}),...
		%       repmat(' ',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.hr{:}),...
		%       repmat(':',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.min{:}),...
		%       repmat(':',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.sec{:})],...
		%    'mm/dd/yy HH:MM:SS.FFF'); % Format string for datenum
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
		eventStruct.dataSource = repmat({'SCEC'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end

end

end

function eventStruct=importZMAPinBox(inStr,varargin)
% importZMAPinBox : imports files using the original ZMAP importer function 

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2014 David Eberhard

	

	onlydescription=false;
	
	a=inStr;
	
	GetFocal=false;
	
	if 2==nargin
	    if strcmp(varargin{1},'-file')
	        
	        
	    elseif strcmp(varargin{1},'-description')
	    	%new option: returns a description of the import filter for the gui
	    	eventStruct.description = ['This importfilter works as passthru to the original ZMAP importer method.',...
	    							  'Only basic information of the array (Location, Magnitude, Depth and Time) ',...
	    							  'is imported by this version of the filter'];
	    	eventStruct.displayname = 'Zmap-in-Box Importer' ;
	    	eventStruct.filetype = '.*'    	
	    	onlydescription= true;
	    	
	    end
	    
	    GetFocal=false;
	end
	
	if 3==nargin
	    if strcmp(varargin{1},'-file')
	        
	        
	    elseif strcmp(varargin{1},'-description')
	    	%new option: returns a description of the import filter for the gui
	    	eventStruct.description = ['This importfilter works as passthru to the original ZMAP importer method.',...
	    							  'Only basic information of the array (Location, Magnitude, Depth and Time) ',...
	    							  'is imported by this version of the filter'];
	    	eventStruct.displayname = 'Zmap-in-Box Importer' ;
	    	eventStruct.filetype = '.*'    	
	    	onlydescription= true;
	
	    end
		
	    GetFocal=varargin{2};  
	
	end
	
	
	
	if ~onlydescription
			fs=filesep;
			%oldfold=pwd
			%cd(['.',fs,'AddOneFiles',fs,'ZmapInBox']);
			%addpath(['.',fs,'src']);		
			addpath(['.',fs,'AddOneFiles',fs,'ZmapInBox']);
			addpath(['.',fs,'AddOneFiles',fs,'ZmapInBox',fs,'src']);	
			addpath(['.',fs,'AddOneFiles',fs,'ZmapInBox',fs, 'importfilters']);			
			
			[a] = import_start(['.',fs,'AddOneFiles',fs,'ZmapInBox',fs, 'importfilters'], inStr);
			%[a] = import_start(['.',fs, 'importfilters'], inStr);
			
			%cd(oldfold);
			
			if all(isnan(a))
				return
			end	
			
			if ~GetFocal
				eventStruct.mag = a(:,6);
				eventStruct.depth = a(:,7);
				eventStruct.lat = a(:,2);
				eventStruct.lon = a(:,1);
				if length(a(1,:))==9
				    eventStruct.dateNum = datenum([ floor(a(:,3)), a(:,4), a(:,5), a(:,8) a(:,9) a(:,9)*0 ]);
				else
				    eventStruct.dateNum = datenum([ floor(a(:,3)), a(:,4), a(:,5), a(:,8), a(:,9) a(:,10)]);
				end
			else
				%get with focal mechanisms
				eventStruct.mag = a(:,6);
				eventStruct.depth = a(:,7);
				eventStruct.lat = a(:,2);
				eventStruct.lon = a(:,1);
				eventStruct.dateNum = datenum([ floor(a(:,3)), a(:,4), a(:,5), a(:,8) a(:,9) a(:,9)*0 ]);
				eventStruct.focal_strike=a(:,10);
				eventStruct.focal_dip=a(:,11);
				eventStruct.focal_rake=a(:,12);
			end
			
			%% Sort the events by time ordering
			
			[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
			fldNames = fieldnames(eventStruct);
			for i=1:numel(fldNames)
			    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
			       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
			    end
			end
	end

end
function eventStruct=importANSSCatRead(inStr,varargin)
% importANSSCatRead : import from NCEDC database
% Reads the earthquake event data returned by accessing the NCEDC 
% database into an event structure

%DE 07/05/12

%% Import helper functions
import mapseis.util.importfilter.*;

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = ['This importfilter reads catalogs by the Northern California Earthquake Data Center (http://www.ncedc.org/anss/). ',...
    					'Also know as the ANSS catalog.',...
    					'The files have to saved as normal text files in order to read them'];
    	eventStruct.displayname = 'ANSS Importfilter' ;
    	eventStruct.filetype = '.txt'
    	onlydescription= true;
    	
    
    end
end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the whitespace character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		%Example of the the data
		%Date       Time             Lat       Lon  Depth   Mag Magt  Nst Gap  Clo  RMS  SRC   Event ID
		%2006/02/16 17:47:59.62  37.9805 -118.7746  13.20  4.28   ML   28  74   20 0.14  NN        175768
		
		
		% Pattern for row of data
		rowPat = [nm('date',rp.datePat),ws,...
		    nm('time',rp.timePat),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('depth',rp.realPat),ws,...
		    nm('mag',rp.realPat),ws,...
		    nm('MagnitudeType',rp.text),ws,...
		    nm('NumStat',rp.intPat),ws,...
		    nm('MaxGap',rp.intPat),ws,...
		    nm('ClosDist',rp.intPat),ws,...
		    nm('RMS',rp.realPat),ws,...
		    nm('Source',rp.text),ws,...
		    nm('EventID',rp.text)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'lat','lon','depth','mag','NumStat','MaxGap','ClosDist','RMS'};
		nonNumericFields = {'MagnitudeType','Source','EventID'};
		
		
		%add default values for undefined fields which where left empty
		%(MagType,NumStat,MaxGap,ClosDist,RMS,Source,EventID)
		planeStruct.MagnitudeType(strcmp(planeStruct.MagnitudeType,''))={'XX'};
		planeStruct.NumStat(strcmp(planeStruct.NumStat,''))={'-1'};
		planeStruct.MaxGap(strcmp(planeStruct.MaxGap,''))={'-1'};
		planeStruct.ClosDist(strcmp(planeStruct.ClosDist,''))={'-1'};
		planeStruct.RMS(strcmp(planeStruct.RMS,''))={'-1'};
		planeStruct.Source(strcmp(planeStruct.Source,''))={'XX'};
		planeStruct.EventID(strcmp(planeStruct.EventID,''))={'XXXX'};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
		
		eventStruct.dateNum = ...
		    datenum(... % MATLAB built in function, use datestr to get a string back
		      [vertcat(planeStruct.date{:}),...
		       repmat(' ',numel(planeStruct.date),1),...
		       vertcat(planeStruct.time{:})],...
		    'yyyy/mm/dd HH:MM:SS.FFF'); % Format string for datenum
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
		%eventStruct.dataSource = repmat({'ANSS'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end
		
		eventStruct.CATALOG_FIELD_DESCRIPTION={'lat','Latitude (in decimal degrees)';...
					       'lon','Longitude (in decimal degrees)';...
					       'depth','Depth (km).';...
					       'mag',' The magnitude of the earthquake.';...
					       'MagnitudeType','Identification of the magnitude type.';...
					       'NumStat','Number of stations (or phases) used to compute the location';...
					       'MaxGap','Maxumum Azimuthal gap ';...
					       'ClosDist',' Distance of closest station to event (km).';...
					       'RMS',' The root mean square of the travel-time residuals';...
					       'Source','Source of event info';...
					       'EventID','Event_id'};

		%Catalog Comments
		eventStruct.CATALOG_INFO_TEXT=[	'Imported by the ANSS importfilter v1',...
						'Not specified fields are set to -1 for numerical data and',...
						'to XX or XXXX for character data',... 
						'Commonly used Magnitude (MagnitudeType) identifiers are: ',...
						'Mw ',...
						'ML,MLt,MLn,MLm,MLb - Local Richter magnitudes',...
						'Mc - Coda amplitude magnitude',...
						'Md - Coda duration magnitude',...
						'Mx - Max amplitude magnitude',...
						'Mavg - Average of Mx and Md magnitudes',...
						'mb - Body-wave magnitude',...
						'Ms - Surface-wave magnitude',...
						'Me - Energy magnitude',...
						'Mlg - Lg magnitude'];
end

end
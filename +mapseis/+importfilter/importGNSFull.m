function eventStruct=importGNSFull(inStr,varargin)
% importGNS : import from GNS database
% Reads the earthquake event data returned by accessing the GNS 
% database into an event structure



%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This import filter is for importing GNS type data with full dataset found under http://magma.geonet.org.nz/resources/quakesearch/';
    	eventStruct.displayname = 'GNS Full Importfilter' ;
    	eventStruct.filetype = '.csv'
    	onlydescription= true;
    	
    end
    
end

if ~onlydescription

	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();
	
	% Import the white space character 
	ws = rp.ws;
	
	%Import separator
	cs = rp.sc;
	
	% Define function to add name to pattern for named pattern matching
	nm = rp.name;
	
	%entries in catalog
	%  EVENT_ID: The primary event ID number.
	%  CUSP_ID: The event reference number.
	%  NAME: The name of the earthquake (if one has been assigned due to its significance).
	%  DESCRIPTION: Any special event information.
	%  FELT:Felt, or blank if not felt.
	%  EVENT_TYPE: Type of seismic event: Regional or Volcanic earthquake; World (teleseismic) earthquakes and Quarry blasts are also recorded in the database, but are not returned by this search.
	%  STATUS_CODE: Status code: Autolocation, Deleted, Final, Provisional, Reviewed, Trigger or - (not assigned).
	%  SOLN_CODE: Solution type code. 1 Computer solution - velocity model 1, 2 Computer solution - velocity model 2, Aftershock solution - epicentre taken from main shock, Confused reference (eg. to volcanic phenomena, waves of non-seismic origin etc.), Felt solution determined from small number of felt reports, Graphical solution, Isoseismal pattern determined from large number of felt reports, Mistaken report (ie. no real earthquake), Special study, Teleseismic solution by an international agency or - (not assigned).
	%  ORI_YEAR: The year of the event (Universal Time).
	%  ORI_MONTH: The month of the event, where known (Universal Time).
	%  ORI_DAY: The day of the event, where known (Universal Time).
	%  ORI_HOUR: The hour of the event, where known (Universal Time).
	%  ORI_MINUTE: The minute of the event, where known (Universal Time).
	%  ORI_SECOND: The second of the event, where known (Universal Time).
	%  ERT: Standard error of origin time (seconds).
	%  ORI_CODE: If present, G indicates time fixed by a geophysicist.
	%  LAT: The latitude of the event (decimal degrees, NZGD49 datum).
	%  ERLAT: Standard error of latitude (decimal degrees).
	%  LAT_CODE: If present, G indicates latitude fixed by a geophysicist.
	%  LON: The longitude of the event (decimal degrees, NZGD49 datum).
	%  ERLON: Standard error of longitude (decimal degrees).
	%  LON_CODE: If present, G indicates latitude fixed by a geophysicist.
	%  NZMG_E: New Zealand Map Grid Easting (m), for earthquakes near the New Zealand main islands.
	%  NZMG_N: New Zealand Map Grid Northing (m), for earthquakes near the New Zealand main islands.
	%  NZTM_E: New Zealand Transverse Mercator Easting (m), for earthquakes near the New Zealand main islands.
	%  NZTM_N: New Zealand Transverse Mercator Northing (m), for earthquakes near the New Zealand main islands.
	%  Z_CLASS: Depth class: Shallow, Crustal, Normal (lower crustal), # (not assigned), - (not applicable).
	%  Z: Focal depth (km).
	%  ERZ: Standard error of depth (km).
	%  Z_CODE: Depth code: R Depth restricted, G Depth fixed by a geophysicist, ? Depth doubtful.
	%  ACC_CLASS: Accuracy class (for historic earthquakes): A Within 8km of true position, B Within 16 km of true position, C Within 24 km of true position, D Uncertain location, # (not assigned), - (not applicable).
	%  RMS: Standard error of residuals (seconds).
	%  N_PH: Number of phases used.
	%  N_STN: Number of stations used.
	%  DMIN: Distance to closest station (km).
	%  DMAX: Distance to farthest station (km).
	%  AZGAP: Largest azimuthal gap (degrees).
	%  CORR: Correlation coefficient.
	%  N_UP: Number of upward first motions.
	%  N_DO: Number of downward first motions.
	%  LOC_REMARK: Any special location remarks.
	%  MAG_TYPE: Magnitude type: ML Local, MD Duration, MB Body wave, MS Surface wave, MW Moment, # (not assigned).
	%  MAG_CLASS: Magnitude class (for historic earthquakes): A > 7.5, B 6 - 7.5, C 4.5 - 6, D < 4.5, U Uncalibrated, # (not assigned), - (not applicable).
	%  MAG: Magnitude.
	%  MAG_UPPER: Upper bound of magnitude if a range was assigned; MAG is the lower bound.
	%  ERMAG: Standard error of magnitude.
	%  MAG_CODE: If present, G indicates magnitude fixed by a geophysicist.
	%  N_MAG: Number of magnitudes.
	%  N_MAG_STN: Number of magnitude stations.
	%  MAG_REMARK: Any special magnitude remarks.
	
	%typical line
	%441,2178084,"Cheviot",,F,R,F,I,1901,11,15,20,15,,,G,-42.75,,G,173.35001,,G,2538675,5828725,1628656,5267074,-,12,,G,#,,,,,,,,,,,MW,-,6.8,,,G,,,,

	
	
	% Pattern for row of data
	rowPat = [	nm('EVENT_ID',rp.realPat),cs,...
			nm('CUSP_ID',rp.anything),cs,...
			nm('NAME',rp.anything),cs,...
			nm('DESCRIPTION',rp.anything),cs,...
			nm('FELT',rp.anything),cs,...
			nm('EVENT_TYPE',rp.anything),cs,...
			nm('STATUS_CODE',rp.anything),cs,...
			nm('SOLN_CODE',rp.anything),cs,...
			nm('ORI_YEAR',rp.realPat),cs,...
			nm('ORI_MONTH',rp.realPat),cs,...
			nm('ORI_DAY',rp.realPat),cs,...
			nm('ORI_HOUR',rp.realPat),cs,...
			nm('ORI_MINUTE',rp.realPat),cs,...
			nm('ORI_SECOND',rp.realPat),cs,...
			nm('ERT',rp.realPat),cs,...
			nm('ORI_CODE',rp.anything),cs,...
			nm('LAT',rp.realPat),cs,...
			nm('ERLAT',rp.realPat),cs,...
			nm('LAT_CODE',rp.anything),cs,...
			nm('LON',rp.realPat),cs,...
			nm('ERLON',rp.realPat),cs,...
			nm('LON_CODE',rp.anything),cs,...
			nm('NZMG_E',rp.realPat),cs,...
			nm('NZMG_N',rp.realPat),cs,...
			nm('NZTM_E',rp.realPat),cs,...
			nm('NZTM_N',rp.realPat),cs,...
			nm('Z_CLASS',rp.anything),cs,...
			nm('Z',rp.realPat),cs,...
			nm('ERZ',rp.realPat),cs,...
			nm('Z_CODE',rp.anything),cs,...
			nm('ACC_CLASS',rp.anything),cs,...
			nm('RMS',rp.realPat),cs,...
			nm('N_PH',rp.realPat),cs,...
			nm('N_STN',rp.realPat),cs,...
			nm('DMIN',rp.realPat),cs,...
			nm('DMAX',rp.realPat),cs,...
			nm('AZGAP',rp.realPat),cs,...
			nm('CORR',rp.realPat),cs,...
			nm('N_UP',rp.realPat),cs,...
			nm('N_DO',rp.realPat),cs,...
			nm('LOC_REMARK',rp.anything),cs,...
			nm('MAG_TYPE',rp.anything),cs,...
			nm('MAG_CLASS',rp.anything),cs,...	  
			nm('MAG',rp.realPat),cs,...
			nm('MAG_UPPER',rp.realPat),cs,...
			nm('ERMAG',rp.realPat),cs,...
			nm('MAG_CODE',rp.anything),cs,...	 
			nm('N_MAG',rp.realPat),cs,...
			nm('N_MAG_STN',rp.realPat),cs,...
			nm('MAG_REMARK',rp.anything)];
	
			
	%PreProcessing of the string
	%This can take a while, I would like to improve that step
	%tic
	%ToSet=true;
	%for i=1:numel(inStr)
	  
	%    	if strcmp(inStr(i),'"');
	%		  ToSet=~ToSet;
	%		%  %inStr(i)=' ';
	%	end
		 
	%	if strcmp(inStr(i),',')&ToSet
	%		inStr(i)=';';
		  
	%	elseif strcmp(inStr(i),';')&~ToSet
	%		inStr(i)='/';
	%	end
	%end
	%disp(toc);
	
	%second version of the preprocessing (vectorized)
	tic;
	TheComma=inStr==',';
	TheSemi=inStr==';';
	TheMark=true(size(inStr));
	
	MarkPos=find(inStr=='"');
	for i=1:2:(numel(MarkPos))
		TheMark(MarkPos(i):MarkPos(i+1))=false;
	end

	%No set it 
	inStr(TheMark&TheComma)=';';
	inStr(~TheMark&TheSemi)='/';
	disp(toc);
	
	
	%% Convert the input string into a struct of data
	planeStruct = readTable(inStr,rp.line,rowPat);
	
	%% Define the fields that are present in the final output 
	numericFields = {'ORI_YEAR','ORI_MONTH','ORI_DAY','ORI_HOUR','ORI_MINUTE','ORI_SECOND',...
			'ERT','LAT','ERLAT','LON','ERLON','NZMG_E','NZMG_N','NZTM_E','NZTM_N','Z',...
			'ERZ','RMS','N_PH','N_STN','DMIN','DMAX','AZGAP','CORR','N_UP','N_DO',...
			'MAG','MAG_UPPER','ERMAG','N_MAG','N_MAG_STN'};
	nonNumericFields = {'EVENT_ID','CUSP_ID','NAME','DESCRIPTION','FELT','EVENT_TYPE','STATUS_CODE',...
				'SOLN_CODE','ORI_CODE','LAT_CODE','LON_CODE','Z_CLASS','Z_CODE','ACC_CLASS',...
				'LOC_REMARK','MAG_TYPE','MAG_CLASS','MAG_CODE','MAG_REMARK'};
	
	%replace empty values by something reasonable
	
	
	%% Convert numerical fields to doubles
	for i=1:numel(numericFields)
		
		%replace empty values by something reasonable
		planeStruct.(numericFields{i})(strcmp(planeStruct.(numericFields{i}),''))={'nan'};
		
	    eventStruct.(numericFields{i}) = ...
	        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
	end
	
	%% Include the non-numeric fields directly into the output structure
	for i=1:numel(nonNumericFields)
	    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
	end
	
	%% Convert the date and time to a number
	
	%eventStruct.dateNum = ...
	%    datenum(... % MATLAB built in function, use datestr to get a string back
	%      [vertcat(planeStruct.date{:}),...
	%       repmat(' ',numel(planeStruct.date),1),...
	%       vertcat(planeStruct.time{:})],...
	%    'yyyy/mm/dd HH:MM:SS.FFF'); % Format string for datenum
	
	%first replace  hr, min, sec fields which are 'nan' with '0' 
	%if not done, the dateNum will be 'nan' in those cases.
	eventStruct.ORI_MONTH(isnan(eventStruct.ORI_MONTH))=1;
	eventStruct.ORI_DAY(isnan(eventStruct.ORI_DAY))=1;
	eventStruct.ORI_HOUR(isnan(eventStruct.ORI_HOUR))=1;
	eventStruct.ORI_MINUTE(isnan(eventStruct.ORI_MINUTE))=1;
	eventStruct.ORI_SECOND(isnan(eventStruct.ORI_SECOND))=1;
	eventStruct.MAG(isnan(eventStruct.MAG))=0;
	eventStruct.Z(isnan(eventStruct.Z))=0;
	
	eventStruct.dateNum = datenum([ eventStruct.ORI_YEAR, eventStruct.ORI_MONTH,...
									eventStruct.ORI_DAY, eventStruct.ORI_HOUR,...
									 eventStruct.ORI_MINUTE, eventStruct.ORI_SECOND]);
	
	%% Add a field for where the data comes from
	% This is useful since the event ID is only unique for a particular data
	% source, so [dataSource,'_',ID] should be unique
	eventStruct.dataSource = repmat({'GNS'},size(eventStruct.dateNum));
	
	%% Sort the events by time ordering
	
	[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
	fldNames = fieldnames(eventStruct);
	for i=1:numel(fldNames)
	    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
	       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
	    end
	end
	
	%Routing Info
	Routing(1,:)={'lon', 'LON',[]};
	Routing(2,:)={'lat', 'LAT',[]};
	Routing(3,:)={'mag', 'MAG',[]};
	Routing(4,:)={'depth', 'Z',[]};
	
	
	eventStruct.ROUTING_INFO=Routing;
	
	eventStruct.CATALOG_FIELD_DESCRIPTION={	'EVENT_ID', 'The primary event ID number.';...
						'CUSP_ID', 'The event reference number.';...
						'NAME', 'The name of the earthquake (if one has been assigned due to its significance).';...
						'DESCRIPTION', 'Any special event information.';...
						'FELT',	'Felt, or blank if not felt.';...
						'EVENT_TYPE', 'Type of seismic event: Regional or Volcanic earthquake; World (teleseismic) earthquakes and Quarry blasts are also recorded in the database, but are not returned by this search.';...
						'STATUS_CODE', 'Status code: Autolocation, Deleted, Final, Provisional, Reviewed, Trigger or - (not assigned).';...
						'SOLN_CODE', 'Solution type code. 1 Computer solution - velocity model 1, 2 Computer solution - velocity model 2, Aftershock solution - epicentre taken from main shock, Confused reference (eg. to volcanic phenomena, waves of non-seismic origin etc.), Felt solution determined from small number of felt reports, Graphical solution, Isoseismal pattern determined from large number of felt reports, Mistaken report (ie. no real earthquake), Special study, Teleseismic solution by an international agency or - (not assigned).';...
						'ORI_YEAR', 'The year of the event (Universal Time).';...
						'ORI_MONTH', 'The month of the event, where known (Universal Time).';...
						'ORI_DAY', 'The day of the event, where known (Universal Time).';...
						'ORI_HOUR', 'The hour of the event, where known (Universal Time).';...
						'ORI_MINUTE', 'The minute of the event, where known (Universal Time).';...
						'ORI_SECOND', 'The second of the event, where known (Universal Time).';...
						'ERT', 'Standard error of origin time (seconds).';...
						'ORI_CODE', 'If present, G indicates time fixed by a geophysicist.';...
						'LAT', 'The latitude of the event (decimal degrees, NZGD49 datum).';...
						'ERLAT', 'Standard error of latitude (decimal degrees).';...
						'LAT_CODE', 'If present, G indicates latitude fixed by a geophysicist.';...
						'LON', 'The longitude of the event (decimal degrees, NZGD49 datum).';...
						'ERLON', 'Standard error of longitude (decimal degrees).';...
						'LON_CODE', 'If present, G indicates latitude fixed by a geophysicist.';...
						'NZMG_E', 'New Zealand Map Grid Easting (m), for earthquakes near the New Zealand main islands.';...
						'NZMG_N', 'New Zealand Map Grid Northing (m), for earthquakes near the New Zealand main islands.';...
						'NZTM_E', 'New Zealand Transverse Mercator Easting (m), for earthquakes near the New Zealand main islands.';...
						'NZTM_N', 'New Zealand Transverse Mercator Northing (m), for earthquakes near the New Zealand main islands.';...
						'Z_CLASS', 'Depth class: Shallow, Crustal, Normal (lower crustal), # (not assigned), - (not applicable).';...
						'Z', 'Focal depth (km).';...
						'ERZ', 'Standard error of depth (km).';...
						'Z_CODE', 'Depth code: R Depth restricted, G Depth fixed by a geophysicist, ? Depth doubtful.';...
						'ACC_CLASS', 'Accuracy class (for historic earthquakes): A Within 8km of true position, B Within 16 km of true position, C Within 24 km of true position, D Uncertain location, # (not assigned), - (not applicable).';...
						'RMS', 'Standard error of residuals (seconds).';...
						'N_PH', 'Number of phases used.';...
						'N_STN', 'Number of stations used.';...
						'DMIN', 'Distance to closest station (km).';...
						'DMAX', 'Distance to farthest station (km).';...
						'AZGAP', 'Largest azimuthal gap (degrees).';...
						'CORR', 'Correlation coefficient.';...
						'N_UP', 'Number of upward first motions.';...
						'N_DO', 'Number of downward first motions.';...
	  					'LOC_REMARK', 'Any special location remarks.';...
	  					'MAG_TYPE', 'Magnitude type: ML Local, MD Duration, MB Body wave, MS Surface wave, MW Moment, # (not assigned).';...
	  					'MAG_CLASS', 'Magnitude class (for historic earthquakes): A > 7.5, B 6 - 7.5, C 4.5 - 6, D < 4.5, U Uncalibrated, # (not assigned), - (not applicable).';...
	  					'MAG', 'Magnitude.';...
	  					'MAG_UPPER', 'Upper bound of magnitude if a range was assigned; MAG is the lower bound.';...
	  					'ERMAG', 'Standard error of magnitude.';...
	  					'MAG_CODE', 'If present, G indicates magnitude fixed by a geophysicist.';...
	  					'N_MAG', 'Number of magnitudes.';...
	  					'N_MAG_STN', 'Number of magnitude stations';....
	  					'MAG_REMARK', 'Any special magnitude remarks.'};
	
end

end
function eventStruct=importECOSFinalCatRead(inStr,varargin)
% importSCECCatRead : import from SCEC database
% Reads the earthquake event data returned by accessing the SCEC 
% database into an event structure

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Import helper functions
import mapseis.util.importfilter.*;

%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)

onlydescription=false;

if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This importfilter for the final ECOS09 catalog';
    	eventStruct.displayname = 'ECOS09 Importfilter' ;
    	eventStruct.filetype = '.txt';
    	onlydescription= true;
    	
    end
    
end

if ~onlydescription

	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();
	
	% Import the white space character 
	ws = rp.ws;
	
	%Import semicolon
	sc = rp.sc;
	%sc=',';
	% Define function to add name to pattern for named pattern matching
	nm = rp.name;
	
	% Pattern for row of data
	rowPat = [nm('DATANR',rp.realPat),sc,...
		nm('EVENTTYPE',rp.anything),sc,...
		nm('APPRAISAL',rp.realPat),sc,...
		nm('year',rp.realPat),sc,...
		nm('month',rp.realPat),sc,...
	    nm('day',rp.realPat),sc,...
	    nm('hr',rp.realPat),sc,...
	    nm('min',rp.realPat),sc,...
	    nm('sec',rp.realPat),sc,...
	    nm('lat',rp.realPat),sc,...
	    nm('lon',rp.realPat),sc,...
	    nm('LOCATION_AG',rp.realPat),sc,...
	    nm('H',rp.realPat),sc,...
	    nm('MWECOS09',rp.realPat),sc,...
	    nm('MLECOS09',rp.realPat),sc,...
	    nm('AX',rp.anything)];
	
	%% Convert the input string into a struct of data
	planeStruct = readTable(inStr,rp.line,rowPat);
	
	%% Define the fields that are present in the final output 
	numericFields = {'DATANR','APPRAISAL','year','month','day','hr','min','sec','lat',...
					'lon','LOCATION_AG','H','MWECOS09','MLECOS09'};
	nonNumericFields = {'EVENTTYPE','AX'};
	
	%replace empty values by something reasonable
	
	
	%% Convert numerical fields to doubles
	for i=1:numel(numericFields)
		
		%replace empty values by something reasonable
		planeStruct.(numericFields{i})(strcmp(planeStruct.(numericFields{i}),''))={'nan'};
		
	    eventStruct.(numericFields{i}) = ...
	        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
	end
	
	%% Include the non-numeric fields directly into the output structure
	for i=1:numel(nonNumericFields)
	    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
	end
	
	%% Convert the date and time to a number
	
	%eventStruct.dateNum = ...
	%    datenum(... % MATLAB built in function, use datestr to get a string back
	%      [vertcat(planeStruct.date{:}),...
	%       repmat(' ',numel(planeStruct.date),1),...
	%       vertcat(planeStruct.time{:})],...
	%    'yyyy/mm/dd HH:MM:SS.FFF'); % Format string for datenum
	
	%first replace  hr, min, sec fields which are 'nan' with '0' 
	%if not done, the dateNum will be 'nan' in those cases.
	%Added month day as well, in case it is a historic date

	eventStruct.month(isnan(eventStruct.month))=1;
	eventStruct.day(isnan(eventStruct.day))=1;
	eventStruct.hr(isnan(eventStruct.hr))=1;
	eventStruct.min(isnan(eventStruct.min))=1;
	eventStruct.sec(isnan(eventStruct.sec))=1;
	
	eventStruct.dateNum = datenum([ eventStruct.year, eventStruct.month,...
									eventStruct.day, eventStruct.hr,...
									 eventStruct.min, eventStruct.sec]);
	
	%use MW for the mags
	eventStruct.mag=eventStruct.MWECOS09;
	
	%use H for the depth
	eventStruct.depth=eventStruct.H;
	
	
	
	%% Add a field for where the data comes from
	% This is useful since the event ID is only unique for a particular data
	% source, so [dataSource,'_',ID] should be unique
	eventStruct.dataSource = repmat({'SED'},size(eventStruct.dateNum));
	
	%% Sort the events by time ordering
	
	[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
	fldNames = fieldnames(eventStruct);
	for i=1:numel(fldNames)
	    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
	       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
	    end
	end
end

end
function eventStruct=importIcelandSIL(inStr,varargin)
% This a Template with important ingridient of a Importfilter, see the exist ones 
% for an Example

%% Import helper functions
import mapseis.util.importfilter.*;


onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This importfilter is for ascii format catalogs from the Icelandic Meterologic Office';
    	eventStruct.displayname = 'Iceland SIL' ;
    	eventStruct.filetype = '*.*'
    	onlydescription= true;
    	
    end

end


if ~onlydescription
	%% Define the regular expressions needed to access the data
	rp = regexpPatternCatalog();
	
	%get the lines
	thelines = regexp(inStr,rp.line,'match');
	
	%Format of the SIL Catalog
	
	%  1- 8   Date of event in yyyymmdd
	% 10-19   Origin time of event in hhmmss.sss
	% 20-28   Latitude of event in degrees
	% 29-38   Longitude of event in degrees
	% 40-45   Depth of event in km
	% 47-51   Local magnitude calculated from seismic moment according to (1) (**)
	% 53-57   Local magnitude calculated from amplitude and distance according to (2)
	% 59-60   Number of stations supplying arrival times
	% 62-63   Number of P arrival time picks
	% 65-66   Number of S arrival time picks
	% 69-70   Number of polarities
	% 73-76   Accuracy in origin time, in seconds
	% 78-82   Accuracy in latitude, in degrees (1 std. dev. confidence interval)
	% 84-88   Accuracy in longitude, in degrees (1 std. dev. confidence interval)
	% 92-94   Accuracy in depth, in kilometers (if =0.0, then depth is fixed)
	% 98-101  rms error of time residuals in arrival time picks, in seconds
	%104-108  Smallest distance to recording station, in kilometers
	%110-114  Largest azimuthal gap between recording stations, in degrees
	%116      Velocity model used in location (1=SIL, 2=Northern, 3=Iceland)
	
	
	%go throught the lines and build the structur
	for i=1:numel(thelines)
		currLine=thelines{i};
		%first create the datenum out of the date and the time
		
		thedate=currLine(1:8);
		thetime=currLine(10:19);
		
		eventStruct.dateNum(i,1) = datenum([thedate,'T',thetime],'yyyymmddTHHMMSS.FFF');
		
		%now all other parameters
		eventStruct.lat_ice_coord(i,1)=str2num(currLine(20:28));
		eventStruct.lon_ice_coord(i,1)=str2num(currLine(29:38));
		eventStruct.depth(i,1)=str2num(currLine(40:45));
		eventStruct.Ml_from_moment(i,1)=str2num(currLine(47:51));
		eventStruct.Ml_from_amp(i,1)=str2num(currLine(53:57));
		eventStruct.nr_of_Stations(i,1)=str2num(currLine(59:60));
		eventStruct.nr_of_P(i,1)=str2num(currLine(62:63));
		eventStruct.nr_of_S(i,1)=str2num(currLine(65:66));
		eventStruct.nr_of_Polarities(i,1)=str2num(currLine(69:70));
		eventStruct.error_time(i,1)=str2num(currLine(73:76));
		eventStruct.error_lat_ice(i,1)=str2num(currLine(78:82));
		eventStruct.error_lon_ice(i,1)=str2num(currLine(84:88));
		eventStruct.error_depth(i,1)=str2num(currLine(92:94));
		eventStruct.rms_err_timeres(i,1)=str2num(currLine(98:101));
		eventStruct.smallest_dist(i,1)=str2num(currLine(104:108));
		eventStruct.largest_az_gap(i,1)=str2num(currLine(110:114));
		eventStruct.velocity_model(i,1)=str2num(currLine(116));
		
	end
	
	
	%Routing Info
	Routing(1,:)={'lon', 'lon_ice_coord',[]};
	Routing(2,:)={'lat', 'lat_ice_coord',[]};
	Routing(3,:)={'mag', 'Ml_from_moment',[]};
	
	eventStruct.ROUTING_INFO=Routing;
	
		
	%Field Description
	eventStruct.CATALOG_FIELD_DESCRIPTION={'dateNum','The string containing the date and the time ';...					
					'lat_ice_coord', 'Latitude of event in degrees (icelandic coordinates) ';...
					'lon_ice_coord','Longitude of event in degrees (icelandic coordinates) ';...
					'depth','Depth of event in km ';...
					'Ml_from_moment','Local magnitude calculated from seismic moment according to (1) (**) ';...
					'Ml_from_amp','Local magnitude calculated from amplitude and distance according to (2) ';...
					'nr_of_Stations','Number of stations supplying arrival times ';...
					'nr_of_P','Number of P arrival time picks ';...
					'nr_of_S','Number of S arrival time picks ';...
					'nr_of_Polarities','Number of polarities ';...
					'error_time','Accuracy in origin time, in seconds ';...
					'error_lat_ice','Accuracy in latitude, in degrees (1 std. dev. confidence interval) ';...
					'error_lon_ice','Accuracy in longitude, in degrees (1 std. dev. confidence interval) ';...
					'error_depth','Accuracy in depth, in kilometers (if =0.0, then depth is fixed) ';...
					'rms_err_timeres','rms error of time residuals in arrival time picks, in seconds ';...
					'smallest_dist','Smallest distance to recording station, in kilometers ';...
					'largest_az_gap','Largest azimuthal gap between recording stations, in degrees ';...
					'velocity_model','Velocity model used in location (1=SIL, 2=Northern, 3=Iceland)'};
	
	
	%Catalog Comments
	eventStruct.CATALOG_INFO_TEXT=[	   '(1) m = log10(Mo) - 10 ',... 
					   'M = m                               if          M <= 2.0 ',...
					   'M = 2.0 + (m-a)*0.9                       2.0 < M <= 3.0 ',...
					   'M = 3.0 + (m-a-b)*0.8                     3.0 < M <= 4.6 ',...
					   'M = 4.6 + (m-a-b-c)*0.7                   4.6 < M <= 5.4 ',...
					   'M = 5.4 + (m-a-b-c-d)*0.5                 5.4 < M <= 5.9 ',...
					   'M = 5.9 + (m-a-b-c-d-e)*0.4               5.9 < M >= 6.3 ',...
					   'M = 6.3 + (m-a-b-c-d-e-f)*0.35            6.3 < M ',...
					   'where Mo = seismic moment in Nm and ',...
						  'a = 2,  b = 1/0.9,  c = 1.6/0.8,  d = 0.8/0.7,  e = f = 1 ',...
					   '(**)  Note: Seismic moment of events >= 4 may be severely underestimated. ',... 
					   '(2) M = log10(amp) + 2.1 * log10(dist.-in-km) - 4.8, ',...
					   'where amplitude is estimated in a 10 second window around the S arrival.'];
					
end

end
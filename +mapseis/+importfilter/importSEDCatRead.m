function eventStruct=importSEDCatRead(inStr,varargin)
% importSCECCatRead : import from SCEC database
% Reads the earthquake event data returned by accessing the SCEC 
% database into an event structure

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell

%% Import helper functions
import mapseis.util.importfilter.*;


onlydescription=false;
%% Read in file if necessary
% If called with -file argument then open the file and read it into a
% string.  Assume that we have enough memory to read the file into memory
% (ie event catalog <100MB ish for a 2007 Lenovo T60 laptop)
if 2==nargin
    if strcmp(varargin{1},'-file')
        fid = fopen(inStr);
        inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        fclose(fid);
        
    elseif strcmp(varargin{1},'-description')
    	%new option: returns a description of the import filter for the gui
    	eventStruct.description = 'This importfilter was used with a catalog of the SED, may be changed or deleted later.';
    	eventStruct.displayname = 'SED Importfilter' ;
    	eventStruct.filetype = '.txt'    	
    	onlydescription= true;
    	
    end

end

if ~onlydescription

		%% Define the regular expressions needed to access the data
		rp = regexpPatternCatalog();
		
		% Import the white space character 
		ws = rp.ws;
		
		% Define function to add name to pattern for named pattern matching
		nm = rp.name;
		
		% Pattern for row of data
		rowPat = [nm('ID',rp.intPat),ws,...
		    nm('year',rp.intPat),ws,...
		    nm('month',rp.intPat),ws,...
		    nm('day',rp.intPat),ws,...
		    nm('hr',rp.intPat),ws,...
		    nm('min',rp.intPat),ws,...
		    nm('sec',rp.realPat),ws,...
		    nm('lat',rp.realPat),ws,...
		    nm('lon',rp.realPat),ws,...
		    nm('xcor',rp.intPat),ws,...
		    nm('ycor',rp.intPat),ws,...
		    nm('depth',rp.intPat),ws,...
		    nm('mag',rp.realPat),ws,...
		    nm('RMS',rp.realPat),ws,...
		    nm('GAP',rp.intPat),ws,...
		    nm('DM',rp.intPat),ws,...
		    nm('NO',rp.intPat)];
		
		%% Convert the input string into a struct of data
		planeStruct = readTable(inStr,rp.line,rowPat);
		
		%% Define the fields that are present in the final output 
		numericFields = {'year','month','day','hr','min','sec','lat','lon',...
						'xcor','ycor','depth','mag','RMS','GAP','DM','NO'};
		nonNumericFields = {'ID'};
		
		%% Convert numerical fields to doubles
		
		for i=1:numel(numericFields)
		    eventStruct.(numericFields{i}) = ...
		        cellfun(@(x) sscanf(x,'%f'),planeStruct.(numericFields{i}));
		end
		
		%% Include the non-numeric fields directly into the output structure
		for i=1:numel(nonNumericFields)
		    eventStruct.(nonNumericFields{i}) = planeStruct.(nonNumericFields{i});
		end
		
		%% Convert the date and time to a number
		
		%eventStruct.dateNum = ...
		%    datenum(... % MATLAB built in function, use datestr to get a string back
		%      [vertcat(planeStruct.date{:}),...
		%       repmat(' ',numel(planeStruct.date),1),...
		%       vertcat(planeStruct.time{:})],...
		%    'yyyy/mm/dd HH:MM:SS.FFF'); % Format string for datenum
		
		eventStruct.dateNum = datenum([ eventStruct.year, eventStruct.month,...
										eventStruct.day, eventStruct.hr,...
										 eventStruct.min, eventStruct.sec]);
		
		%% Add a field for where the data comes from
		% This is useful since the event ID is only unique for a particular data
		% source, so [dataSource,'_',ID] should be unique
		eventStruct.dataSource = repmat({'SED'},size(eventStruct.dateNum));
		
		%% Sort the events by time ordering
		
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		fldNames = fieldnames(eventStruct);
		for i=1:numel(fldNames)
		    if numel(eventStruct.(fldNames{i})) == numel(sortInds)
		       eventStruct.(fldNames{i})= eventStruct.(fldNames{i})(sortInds);
		    end
		end
end

end
classdef MiniWrapperTemplate < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Filterlist
		SendedEvents
		ZmapCatalog
		ZmapVariables
		CalcResult
		CalcParameter
		ErrorDialog
		ParallelMode
		PackList
	end

	events
		CalcDone

	end


	methods
	
	function obj = MiniWrapperTemplate(ListProxy,Commander,GUIswitch)
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		obj.Commander = Commander; 
		
		%get the current datastore and filterlist and build zmap catalog
		obj.Datastore = obj.Commander.getCurrentDatastore;
		obj.Filterlist = obj.Commander.getCurrentFilterlist; 
		selected=filterlist.getSelected;
		obj.SendedEvents=selected;
		obj.ZmapCatalog = Export2Zmap(datastore,selected);
		
		
		
		%Init the needed zmap variables
		obj.InitZmapVar;
		
		%Init Grid if needed
		obj.Gridit(true);
		
		%get CalcParameter
		obj.openCalcParamWindow;
		
		%Build the grid with new parameters if needed
		obj.Gridit(false);
		
		%Calculation
		obj.doTheCalcThing;
		
		
		
	end

	function InitZmapVar(obj)
		%here all variable should be set used by a zmap calculation
		%They are saved in the structur ZmapVariables with there name 
		%as there fieldname.
		
		
		%Set the packlist, the mentioned variables will be packed into the structure
		%They have to be defined in here or it will cause an error.
		obj.PackList={'fs';'hodo';'hoda';'p';'fs8';'fs10';'fs12';'fs14';'fs16';'ms6';'ms10';'ms12';...
				'ty';'ty1';'ty2';'ty3';'typele';'sel';'lth1';'lth15';'lth2';'wex';'wey';...
				'fipo';'r';'term';'welx';'wely';'winx';'winy';'rad';'ic';'ya0';'xa0';'iwl3';...
				'iwl2';'step';'ni';'name';'strib';'stri2';'ho';'ho2';'infstri';'maix';'maiy';...
				'tresh';'wi';'rotationangle';'fre';'c1';'c2';'c3';'cb1';'cb2';'cb3';'in';'ldx';...
				'tlap';'ca';'vi';'sha';'inb1';'inb2';'inda';'ra';'co';'par1';'minmag';'sys';...
				'cputype';'ve';'my_dir'};
		
		% set some of the paths from the zmap.m procedure, some may be deleted later
		r = ceil(rand(1,1)*21);
		fipo = get(0,'ScreenSize');hodi = cd;
		fipo(4) = fipo(4)-150;
		term = get(0,'ScreenDepth');
		
		sys = computer;
		cputype = computer;


		ve = version;
		ve = str2num(ve(1:3));

		
		fs = filesep;
		hodo = [hodi fs 'out' fs];
		hoda = [hodi fs 'eq_data' fs];
		p = path;
		addpath([hodi fs 'myfiles'],[hodi fs 'src'],[hodi fs 'src' fs 'utils'],[hodi fs 'src' fs 'declus'],...
		[hodi fs 'src' fs 'fractal'], [hodi fs 'help'],[hodi fs 'dem'],[hodi fs 'zmapwww'],[hodi fs 'importfilters'],...
		[hodi fs  fs 'src' fs 'utils' fs 'eztool'],[hodi fs 'm_map'],[hodi fs 'src' fs 'pvals'],[hodi], [hodi fs 'src' fs 'synthetic'], ...
		[hodi fs 'src' fs 'movies'],[hodi fs 'src' fs 'danijel'],[hodi fs 'src' fs 'danijel' fs 'calc'],...
		[hodi fs 'src' fs 'danijel' fs 'ex'],[hodi fs 'src' fs 'danijel' fs 'gui'],...
		[hodi fs 'src' fs 'danijel' fs 'focal'],...
		[hodi fs 'src' fs 'danijel' fs 'plot'],[hodi fs 'src' fs 'danijel' fs 'probfore'],...
		[hodi fs 'src' fs 'jochen'], [hodi fs 'src' fs 'jochen' fs 'seisvar' fs 'calc'],...
		[hodi fs 'src' fs 'jochen' fs 'seisvar'], [hodi fs 'src' fs 'jochen' fs 'ex'],...
		[hodi fs 'src' fs 'thomas' fs 'slabanalysis'], [hodi fs 'src' fs 'thomas' fs 'seismicrates'],[hodi fs 'src' fs 'thomas' fs 'montereason'],...
		[hodi fs 'src' fs 'thomas' fs 'gui'],...
		[hodi fs 'src' fs 'jochen' fs 'plot'], [hodi fs 'src' fs 'jochen' fs 'stressinv'], [hodi fs 'src' fs 'jochen' fs 'auxfun'],...
		[hodi fs 'src' fs 'thomas'], ...
		[hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
		[hodi fs 'src' fs 'thomas' fs 'montereason'], ...
		[hodi fs 'src' fs 'thomas' fs 'etas'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
		[hodi fs 'src' fs 'thomas'], ...
		[hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
		[hodi fs 'src' fs 'thomas' fs 'montereason'], ...
		[hodi fs 'src' fs 'thomas' fs 'etas'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
		[hodi fs 'src' fs 'juerg' fs 'misc'],...
		[hodi fs 'src' fs 'afterrate']);
		
		%variables from ini_zmap (some may be kicked later)
		
		% Set the font size
		%
		fs8 = 10;
		fs10 = 12;
		fs12 = 14;
		fs14 = 16;
		fs16 = 18;
		
		% Marker sizes
		ms6 = 3;
		ms10 = 10;
		ms12 = 12;
		
		% Marker type
		ty ='.';
		ty1 ='+';
		ty2 = 'o';
		ty3 ='x';
		typele = 'dep';
		sel  = 'in';
		
		
		
		% Line Thickness
		lth1 = 1.0;
		lth15 = 1.5;
		lth2 = 2.0;
		
		% set up Window size
		%
		% Welcome window
		wex = 80;
		wey = fipo(4)-380;
		welx = 340;
		wely = 300;
		
		% Map window
		%
		winx = 750;
		winy = 650;
		
		% Various setups
		%
		rad = 50.;
		ic = 0;
		ya0 = 0.;
		xa0 = 0.;
		iwl3 = 1.;
		iwl2 = 1.5;
		step = 3;
		ni = 100;
		
		name = [' '];
		strib = [' '];
		stri2 = [];
		ho ='noho';
		ho2 = 'noho';
		infstri = ' Please enter information about the | current dataset here';
		maix = [];
		maiy = [];
		
		
		% Initial Time setting
		
		% Tresh is the radius in km below which blocks 
		% in the zmap's will be plotted 
		% 
		tresh = 50;
		wi = 10 ;   % initial width of crossections
		rotationangle = 10; % initial rotation angle in cross-section window
		fre = 0;
		
		
		% set the background color (c1 = red, c2 = green, c3 = blue)
		% default: light gray 0.9 0.9 0.9 
		c1 = 0.9;
		c2 = 0.9;
		c3 = 0.9;
		
		% Set the Background color for the plot 
		% default \: light yellow 1 1 0.6
		cb1 = 1.0;
		cb2 = 1.0;
		cb3 = 1.0;	
		
		in = 'initf';
		
		% seislap default para,eters
		ldx = 100;
		tlap = 100;
		
		ca = 1;
		vi ='on';
		sha ='fl';
		inb1=1;inb2=1;
		inda = 1;
		ra = 5;
		
		co = 'w';
		par1 = 14;
		minmag = 8;
		
		%set the recursion slightly, to avoid error (specialy with the function ploop2.m
		%set(0,'RecursionLimit',750)
		%-----------------------------------------------------------------
		
		
		
		
		%variables from zmap.m
		my_dir = hodi;
		
		
		%variables from other functions needed here
		
		
		
		
		%Pack da shit
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(PackList{i})=eval(PackList{i});
		
		end
		
	
	end
	
	
	function Gridit(obj,initswitch)
		
		%unpack the variables,
		for i=1:numel(obj.PackList)
			assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
		
		end
		
		
		if initswitch
			%This has to be done just once, hence the switch;
			%update packlist
			obj.PackList=[PackList; {'dx';'dy';'ni';'newgri';'xvect';'yvect';...
					'll';'gx';'gy'}];
			
			
			%get parameter from the datastore
			
			%build grid
			gridpars=obj.Datastore.getUserData('gridPars')
			
			if numel(gridpars.gridSep)<2
				dx=gridpars.gridSep;
				dy=gridpars.gridSep;
			else
				dx=gridpars.gridSep(1);
				dy=gridpars.gridSep(2);
			end	
			
			rad=deg2km(gridpars.rad);
			
			ni=100;
			Nmin = 50;
			
		end
		
		%extracted from CalcGridGUI, delete later what's not needed.
		regionFilter = obj.Filterlist.getByName('Region');
		regionParms = regionFilter.getRegion();
            
		theRegion = regionParms{2};
		bBox=theRegion.getBoundingBox();
                
                %xRange = (bBox(1):ptSep(1):bBox(2))';
                %yRange = (bBox(3):ptSep(2):bBox(4))';
		%vX = [bBox(1); bBox(1); bBox(2); bBox(2)];
		%vY = [bBox(4); bBox(3); bBox(3); bBox(4)];
		
		
		%from ex_selectgrid; (it is used in the fullarea mode, the data is filtered anyway)
		%==================
		%Could maybe be optimized, but leave it at the moment as it is
		
		% Create a rectangular grid
		vXVector = [bBox(1):ptSep(1):bBox(2)];
		vYVector = [bBox(3):ptSep(2):bBox(4)];
		mGrid = zeros((length(vXVector) * length(vYVector)), 2);
		nTotal = 0;
		for i = 1:length(vXVector)
		   for j = 1:length(vYVector)
		     nTotal = nTotal + 1;
		     mGrid(nTotal,:) = [vXVector(i) vYVector(j)];
		   end;
		end;
		  
		% Extract all gridpoints in chosen polygon
		XI=mGrid(:,1);
		YI=mGrid(:,2);
  
		m = length(vX)-1;      %  number of coordinates of polygon
		l = 1:length(XI);
		l = (l*0)';
		vUsedNodes = l;               %  Algorithm to select points inside a closed
		%  polygon based on Analytic Geometry    R.Z. 4/94
		for i = 1:m;
		  
			l= ((vY(i)-YI < 0) & (vY(i+1)-YI >= 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0) | ...
		  		((vY(i)-YI >= 0) & (vY(i+1)-YI < 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0);
		    
		  	if i ~= 1 
		  		vUsedNodes(l) = 1 - vUsedNodes(l);
		  	else
		  		vUsedNodes = l; 
		  	end;         
		    
		end;         
		
		%grid points in polygon
		mGrid = mGrid(vUsedNodes,:);
		
		%======================
		
		%newgri, xvect, yvect, ll
		
		%set the values
		newgri=mGrid;
		xvect=vXVector;
		yvect=vYVector;
		ll=vUsedNodes;
		
		gx = xvect;
		gy = yvect;
		
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(PackList{i})=eval(PackList{i});
		
		end
	
	end
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		%kind of a demo
		
		
		
		
	end
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
	
	
	end

end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	
	%THE FUNCTIONS UNDERNEATH ARE NOT ALWAYS DIRECTLY USEABLE, AS THEY OFTEN NEED DIRECT ACCESS
	%TO THE VARIABLE, SO JUST COPY IF NEEDED.
	function TheEvalString = UnpackStruct(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		TheEvalString=[];
		
		for i=1:numel(thefields)
			
			TheEvalString=[TheEvalString; thefields{i},'=' num2str(TheStruct.(thefields{i})),';'];
		end
		 %can be done easier directly in the function which should be calling this, it is more sort of 
		 %a sketch
		
		
	
	end
	
	
	function CellData = UnpackStruct2Cell(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		
		
		for i=1:numel(thefields)
			
			CellData{i,1}=thefields{i}
			CellData{i,2}=TheStruct.(thefields{i});
		end
		
		%assignin can be used to set the variable on the string.
	end
	
	function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itself
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end
	
	
	function TheStruct = PackAllVariable()
		%The opposite of UnPack, it takes every variable in the current Workspace an packs it 
		%into a structure
		
		varilist=who;
		
		for i=1:numel(varilist)
			TheStruct.(varilist{i})=eval(varilist{i});
		
		end
	
	
	end
	
	%---------------
end
	
	

end
classdef McBvalTimeWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		CalcParameter
		ProfileSwitch
		ThirdDimension
		ErrorDialog
		ParallelMode
		StartCalc
		Loaded
		CalcName
		Region
		AlphaMap
		ResultGUI
		PlotOptions
		BorderToogle
		CoastToogle
		EQToogle
		PlotQuality
		LastPlot
		MapLastPlot
		HistoryExist
		TheAspect
		Error2Alpha
		ErrorFactor
		ProfExist
		MarkEq
		MaxEq
	end

	events
		CalcDone

	end


	methods
	
	
	function obj = McBvalTimeWrapper(ListProxy,Commander,GUISwitch,CalcParameter)
		import mapseis.datastore.*;
		
		%derivated from the McBvalMappingWrapper
		
		%------------------------
		%at the moment unfinished
		%------------------------
		
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		
		if isempty(Commander)
			obj.Commander=[];
			%go into manul mode, the object will be created but everything
			%has to be done manually (useful for scripting)
		else
		
			obj.Commander = Commander; 
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Region=[];
			obj.AlphaMap=[];
			obj.ProfExist=false;
			obj.ProfileSwitch=false;
			obj.Loaded=false;	
			obj.Error2Alpha=false;
			obj.ErrorFactor=1;
			obj.ResultGUI=[];
			obj.PlotOptions=[];
			obj.BorderToogle=true;
			obj.CoastToogle=true;
			obj.EQToogle=false;
			obj.PlotQuality = 'low';
			obj.LastPlot=[];
			obj.HistoryExist=false;
			obj.TheAspect=2;
			obj.CalcName='b-value with time';
			
			%get the current datastore and filterlist and build zmap catalog
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			
			EveryThingGood=true;
			
			%check if old data exists
			try
				PreviousCalc=obj.Datastore.getUserData('bval-time-calc');
			catch
				PreviousCalc=[];
			end
			
			if ~isempty(PreviousCalc)
				obj.HistoryExist=true;
			end
			
			
			
			regionFilter = obj.Filterlist.getByName('Region');
			filterRegion = getRegion(regionFilter);
			RegRange=filterRegion{1};	
			if ~strcmp(RegRange,'line')
				obj.ProfExist=true;	
			end
			
			
			if EveryThingGood
				
				if ~isempty(CalcParameter)
					%Parameters are include, no default parameter will
					%be set
					obj.CalcParameter=CalcParameter;
				else
					obj.InitVariables
				end	
				
				if GUISwitch
					
					%let the user modify the parameters
					obj.openCalcParamWindow;
					
					if obj.StartCalc
						
						if ~obj.Loaded
							%Calc
							obj.doTheCalcThing;
						end
						
						%Build resultGUI
						obj.BuildResultGUI
						
						%plot results
						obj.plotResult('bvalue')
					end	
					
					
				end
				

			
			end
		end
		
	end

	
	
	
	function InitVariables(obj)
		%Two optionsets here, one for the temporal side, one for the
		%optional mapping
		
		
		MappinParam = struct(		'dx',0.5,...
						'dy',0.5,...
						'dz',0.5,...
						'BootSwitch',0,...
						'BootSamples',100,...
						'Sel_Radius',10,...
						'NumEvents',100,...
						'MinNumber',50,...
						'Selection_Method',1,...
						'SmoothMode',0,...
						'SmoothFactor',0,...
						'GridMethod','regular',...
						'Step2Mode',0,...
						'McLoad',0,...
						'ProfileSwitch',obj.ProfileSwitch);
						
						
		
		obj.CalcParameter = struct('Mc_Method',1,...
					   'BootSwitch',1,...
					   'BootSamples',300,...
					   'McCorr',0.1,...
					   'Binning_Method',1,...
					   'NumEvents',500,...
					   'TimeBin',0.03,...
					   'Overlap',4,...
					   'MultiBinning',0,...
					   'SmoothingSwitch',0,...
					   'Smoothing',10,...
					   'MinNumber',50,...
					   'LoadSwitch',0,...
					   'Overall_bval',1,...
					   'Calc_sign_bval',0,...
					   'MappingMode',0,...
					   'MappingParameter',MappinParam,...
					   'bvalMapping',0,...
					   'SecondRun',0,...
					   'PrevCalc',[[]]);
					   
								
		
		%GridMethod cannot be changed in the GUI for now until I improved
		%the selfrefining grid algorithmen. (options: 'regular' and 'selfRef')
		
		%McLoad (load McMap for 2step calc) and Step2Mode (uses 2step calculation)
		%will be used later when the mode is available.
		
		%The SecondRun and PrevCalc slots are for a potential future use, no function now.
		%Also bvalMapping is in a first version not working.
		
	
	end
	
	
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		%Note Smoothing: Smoothing is a post processing in the original
		%and is done similar to the Smoothing in the mapping
		
		%In the original code the following was used:
		
		% mMc = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,2));
		% mMc(1:nWindowSize,1)=mResult(1:nWindowSize,2);
		% mMcstd1 = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,3));
		% mMcstd1(1:nWindowSize,1)=mResult(1:nWindowSize,3);
		
		%With nWindowSize the smoothing factor (default 10) and mResult being the result
		
		
		
		%Set Standard parameters:
			Title = 'b-value, a-value and Mc with time v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Binning Method:','Binning_Method';...
				'Number of Events:','NumEvents';...
				'Timestep (decyear):','TimeBin';...
				'Overlap','Overlap',...
				'Use Smoothing','SmoothingSwitch',...
				'Smoothing','Smoothing',...
				'Min. Num. > Mc:','MinNumber';...
				'Mc Correction for Maxc:','McCorr';....
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval';...
				'Calculate Density Map','MappingMode';...
				'Map b-values (experimental)','bvalMapping'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';
				'10: MBass'}; 
		
		
							
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Constant Binsize','Constant Timestep'};
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			
			%NumberEvents
			Formats(5,1).type='edit';
			Formats(5,1).format='integer';
			Formats(5,1).limits = [0 999999];
			Formats(5,1).size = [-1 0];
			Formats(5,2).limits = [0 1];
			Formats(5,2).type='none';
			
			%Timestep
			Formats(6,1).type='edit';
			Formats(6,1).format='float';
			Formats(6,1).limits = [0 999999];
			Formats(6,1).size = [-1 0];
			Formats(6,2).limits = [0 1];
			Formats(6,2).type='none';
			
			%Overlap
			Formats(7,1).type='edit';
			Formats(7,1).format='integer';
			Formats(7,1).limits = [0 999999];
			Formats(7,1).size = [-1 0];
			Formats(7,2).limits = [0 1];
			Formats(7,2).type='none';
			
			%Smoothing
			Formats(8,1).type='check';
			
			%Smoothing Parameter
			Formats(8,2).type='edit';
			Formats(8,2).format='float';
			Formats(8,2).limits = [0 999999];
								
			%Min Number
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 999999];
			Formats(8,1).size = [-1 0];
			Formats(8,2).limits = [0 1];
			Formats(8,2).type='none';
			
			%Mc Correction
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			
			%sign bval switch
			Formats(11,1).type='check';
			
			%overall b-val
			Formats(11,2).type='edit';
			Formats(11,2).format='float';
			Formats(11,2).limits = [0 9999];
			
			%Mapping
			Formats(12,1).type='check';
			
			%b-val Mapping
			Formats(12,2).type='check';
			
			
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
		
			
			if obj.HistoryExist
				Prompt(end+1,:)={'Load Calculation?','LoadSwitch'};
				
				
				%LoadToggle toggle
				Formats(13,1).type='check';
				Formats(13,1).size = [-1 0];
				Formats(13,2).limits = [0 1];
				Formats(13,2).type='none';
			end
			
			
			%open the dialog window
			[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,obj.CalcParameter,Options); 
		
			if obj.HistoryExist&Canceled==0&NewParameter.LoadSwitch==1
				%Load data
				obj.StartCalc=false;
				obj.loadCalc([]);
			
			elseif Canceled==0
				if NewParameter.MappingMode==1
						Title = 'Mapping Parameter';
						
						
						%default values
						
											
						%Selection Method
						Formats2(1,1).type='list';
						Formats2(1,1).style='togglebutton';
						Formats2(1,1).items={'Number of Events','Constant Radius','Both'}
						Formats2(1,1).size = [-1 0];
						Formats2(1,2).limits = [0 1];
						Formats2(1,2).type='none';
						
						%bootstrap toggle
						Formats(2,1).type='check';
						
						%bootstrap number
						Formats(2,2).type='edit';
						Formats(2,2).format='integer';
						Formats(2,2).limits = [0 9999];
						
						%Smoothing
						Formats2(3,1).type='check';
						
						%Smoothing Parameter
						Formats2(3,2).type='edit';
						Formats2(3,2).format='float';
						Formats2(3,2).limits = [0 999999];
						
						%NumberEvents
						Formats2(4,1).type='edit';
						Formats2(4,1).format='integer';
						Formats2(4,1).limits = [0 999999];
					
						%Radius
						Formats2(4,2).type='edit';
						Formats2(4,2).format='float';
						Formats2(4,2).limits = [0 999999];
						
						%Grid space x
						Formats2(5,1).type='edit';
						Formats2(5,1).format='float';
						Formats2(5,1).limits = [0 99999];
						
						%Grid space y
						Formats2(5,2).type='edit';
						Formats2(5,2).format='float';
						Formats2(5,2).limits = [0 99999];
						
						
						
						if obj.ProfExist
						
							Prompt2={'Selection Method:','Selection_Method';...
								'Mc bootstraps:','BootSwitch';...
								'Number of Bootstraps:','BootSamples';...
								'Smoothing?','SmoothMode';...
								'Smoothing Factor','SmoothFactor';...
								'Number of Events:','NumEvents';...
								'Selection Radius (km):','Sel_Radius';...
								'Grid spacing x (deg|km):','dx';...
								'Grid spacing y (deg):','dy';...
								'Calculate on Depth Profile?','ProfileSwitch'
								'Grid spacing z (km):','dz';...
								'Min. Num. > Mc:','MinNumber');
								
								%ProfileSwitch
								Formats(6,1).type='check';
								
								%Grid space z
								Formats2(6,2).type='edit';
								Formats2(6,2).format='float';
								Formats2(6,2).limits = [0 999];
								
								%Min Number
								Formats2(7,1).type='edit';
								Formats2(7,1).format='float';
								Formats2(7,1).limits = [0 9999];
								Formats2(7,1).size = [-1 0];
								Formats2(7,2).limits = [0 1];
								Formats2(7,2).type='none';
										
								
						else
							Prompt2={'Selection Method:','Selection_Method';...
								'Mc bootstraps:','BootSwitch';...
								'Number of Bootstraps:','BootSamples';...
								'Smoothing?','SmoothMode';...
								'Smoothing Factor','SmoothFactor';...
								'Number of Events:','NumEvents';...
								'Selection Radius (km):','Sel_Radius';...
								'Grid spacing x (deg):','dx';...
								'Grid spacing y (deg):','dy';...
								'Min. Num. > Mc:','MinNumber');
							
							%Min Number
							Formats2(6,1).type='edit';
							Formats2(6,1).format='float';
							Formats2(6,1).limits = [0 9999];
							Formats2(6,1).size = [-1 0];
							Formats2(6,2).limits = [0 1];
							Formats2(6,2).type='none'
						
						end
							
						TheCalcParam=obj.CalcParameter.MappinParam;
											
						[NewMapPara,Canceled] = inputsdlg(Prompt2,Title,Formats2,TheCalcParam,Options); 				
						
						
						if Canceled==0
							%Prepare data for calc
							NewMapPara.Mc_Method=NewParameter.Mc_Method;
							NewMapPara.Overall_bval=NewParameter.Overall_bval;
							NewMapPara.Calc_sign_bval=NewParameter.Calc_sign_bval;
							NewMapPara.McCorr=NewParameter.McCorr;
							
							obj.ProfileSwitch=NewMapPara.ProfileSwitch;
							
							NewParameter.MappingParameter=NewMapPara;
						end
						
				end
			
				
				
				%Maybe something for multibinning is needed
				if NewParameter.MultiBinning==1
				
				
				end
			
				
				%final check , because of parts between
				if Canceled==0
					obj.CalcParameter=NewParameter;
					obj.StartCalc=true;
				else
					obj.StartCalc=false;
			
					
				end
			
				
			elseif 	Canceled==1
				obj.StartCalc=false;
			
			
			end
		
		
		
		
		
	end
	
	
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.Mc_calc.*;
		import mapseis.calc.*;
		import mapseis.util.importfilter.cellArrayToPlane;
		
			%CalcConfig and CalcObject (similar for all calc)
			
			
			
		
			
			MapConfig=struct( 'Mc_method',obj.CalcParameter.Mc_Method,...
						'Use_bootstrap',obj.CalcParameter.MappingParameter.BootSwitch,...
						'Nr_bootstraps',obj.CalcParameter.MappingParameter.BootSamples,...
						'Mc_correction',obj.CalcParameter.McCorr,...
						'MinNumber',obj.CalcParameter.MappingParameter.MinNumber,....
						'Mc_Binning',0.1,...
						'FixedMc',0,...
						'Calc_sign_bval',obj.CalcParameter.Calc_sign_bval,...
						'Regional_bval',obj.CalcParameter.Overall_bval,...
						'NoCurve',true);
			
						
			%Grid configuration for the mapping			
			if ~obj.ProfileSwitch
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.MappingParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.MappingParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.MappingParameter.Selection_Method==3
					SelMode='Both';
				end
				
								
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				GridConfig=struct(	'boundaryBox',[bounded],...
							'Gridspacing',[[obj.CalcParameter.MappingParameter.dx obj.MappingParameter.CalcParameter.dy]],...
							'SelectionRadius',km2deg(obj.CalcParameter.MappingParameter.Sel_Radius),...
							'SelectedEvents',[[]],...
							'SelectionNumber',obj.CalcParameter.MappingParameter.NumEvents,...
							'SelectionMode',SelMode,...
							'DistanceSend',true,...
							'ParallelCalc',false);
			
							
							
			else
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				%disp(obj.CalcParameter.Selection_Method)
				
				if obj.CalcParameter.MappingParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.MappingParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.MappingParameter.Selection_Method==3
					SelMode='Both';
				end
				
				
				
				GridConfig=struct(	'boundaryBox',[pRegion.getBoundingBox],...
							'Gridspacing',[[obj.CalcParameter.MappingParameter.dx obj.MappingParameter.CalcParameter.dz]],...
							'SelectionRadius',obj.CalcParameter.MappingParameter.Sel_Radius,...
							'SelectedEvents',[[]],...
							'Sel_Nr',obj.CalcParameter.MappingParameter.NumEvents,...
							'Sel_Mode',SelMode,...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'DistanceSend',true,...
							'ParallelCalc',false);		
						
						
			end			
			
			
			
			
			%The config is doubled to prevent reformation of the config inside the calculaten 
			%every step.			
			
			CalcConfig=struct(	'Mc_method',obj.CalcParameter.Mc_Method,...
						'Use_bootstrap',obj.CalcParameter.BootSwitch,...
						'Nr_bootstraps',obj.CalcParameter.BootSamples,...
						'Mc_correction',obj.CalcParameter.McCorr,...
						'MinNumber',obj.CalcParameter.MinNumber,....
						'Mc_Binning',0.1,...
						'FixedMc',0,...
						'Calc_sign_bval',obj.CalcParameter.Calc_sign_bval,...
						'Regional_bval',obj.CalcParameter.Overall_bval,...
						'NoCurve',true,...
						'MappingMode',obj.CalcParameter.MappingMode,...
						'bvalMapping',obj.CalcParameter.bvalMapping,...
						'ProfileSwitch',obj.ProfileSwitch,...
						'CalcParameter',MapConfig,...
						'GridConfig',GridConfig);
			
			


			%Build calc object:
			McBvalCalc = mapseis.calc.CalculationEX(...
		 		@mapseis.calc.Calc_Time_Mc_bval,@mapseis.projector.sendDatastore,...
		   		CalcConfig,'Name','BvalCalcTime');		


			
			
		   		
		   	struct('Mc_Method',1,...
					   'BootSwitch',1,...
					   'BootSamples',300,...
					   'McCorr',0.1,...
					   'Binning_Method',1,...
					   'NumEvents',500,...
					   'TimeBin',0.03,...
					   'Overlap',4,...
					   'MultiBinning',0,...
					   'SmoothingSwitch',0,...
					   'Smoothing',10,...
					   'MinNumber',50,...
					   'LoadSwitch',0,...
					   'Overall_bval',1,...
					   'Calc_sign_bval',0,...
					   'MappingMode',0,...
					   'MappingParameter',MappinParam,...
					   'bvalMapping',0,...
					   'SecondRun',0,...
					   'PrevCalc',[[]]);	
		    	
		    						
			regionFilter = obj.Filterlist.getByName('Region');
			filterRegion = getRegion(regionFilter);
			pRegion = filterRegion{2};
			RegRange=filterRegion{1};
				
			if obj.CalcParameter.Binning_Method==1
				BinMode='fixnumber';
			elseif obj.CalcParameter.Binning_Method==2
				BinMode='fixrange';
			end
				
			
			
				
			selected=obj.SendedEvents;
				
				
				
			CalcParam1D=struct(	'Intervall',[[]],...
						'SelectedEvents',[selected],...
						'BinningType',BinMode,...
						'Binsize',obj.CalcParameter.TimeBin,...
						'Binnumber',obj.CalcParameter.NumEvents,...
						'Overlap',obj.CalcParameter.Overlap,...
						'MinNumber',obj.CalcParameter.MinNumber,...
						'LogicSend',true,...
						'ParallelCalc',obj.ParallelMode);
				
				

			if ~obj.CalcParameter.MultiBinning
				%normal calculation mode			
				[varRange,calcRes]=OneDGridder(obj.Datastore,McBvalCalc,CalcParam,'DecYear',true);
		    		
			
				
				
				
				
			else
			
			
			
				%Later
			
			
			
			
			end
			
			
			
			
				RawRes=cellArrayToPlane(calcRes);
				%disp(RawRes)
				ufields=fieldnames(RawRes);
				
				%exclude the two "not numeric" fields;
				doNotUse=strcmp(ufields,'MappingResult')
				fields=ufields(~doNotUse);
				
				%disp(RawRes.a_value_annual)
				for i=1:numel(fields)
					%disp((fields{i}))
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				%Add the missing one
				if any(doNotUse)
					obj.CalcRes.MappingResults=RawRes.MappingResults;
					%build the AlphaMap
					regionFilter = obj.Filterlist.getByName('Region');
					regionParms = regionFilter.getRegion();
					
					if ~obj.ProfileSwitch
						if ~strcmp(regionParms{1},'all')
							theRegion = regionParms{2};	
							RawAlpha=theRegion.isInside([obj.CalcRes.MappingResults{1}.xvec,...
									obj.CalcRes.MappingResults{1}.yvec]);
							try
								obj.AlphaMap=~logical(reshape(RawAlpha,length(obj.CalcRes.MappingResults{1}.yRange),...
										length(obj.CalcRes.MappingResults{1}.xRange)));
							catch
								%means the polygon is badly shaped
								obj.AlphaMap=[];
								disp('could not restore AlphaMap')
							end	
							
						end
						
					else
						%no alphamap needed
					
					
					end
					
				end
				
						    		
		    		
		    	
		    	
		    		
		
		
	end
	
	
	
	
	
	
	function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('Mc-Bval-Timeseries',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Mc-Bval-Timeseries',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',[ 50 500 1000 400 ],'Renderer','OpenGL');
		   plotAxis=gca;
		   set(plotAxis,'Tag','TimeResultAxis');
		   
		   obj.buildMenus;
		   
		   %obj.buildMouseMenu([]);
		   
		end
		
	end
	
	function BuildMapResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('Mc-Bval-map',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Mc-Bval-map',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',[ 50 300 1100 690 ],'Renderer','OpenGL');
		   plotAxis=gca;
		   set(plotAxis,'Tag','MapResultAxis');
		   
		   obj.buildMapMenus;
		   
		   obj.buildMapMouseMenu([]);
		   
		end
		
	end
	
	
	
	function buildMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
	
		goError='autoColor';
		
				
		
		%first the general menus (similar for both)
		OptionMenu = uimenu( obj.ResultGUI,'Label','- Load/Save');
		uimenu(OptionMenu,'Label','Calculate again',...
		'Callback',@(s,e) obj.CalcAgain);
		uimenu(OptionMenu,'Label','Show calculation parameters',...
		'Callback',@(s,e) obj.showCalcParameter);
		uimenu(OptionMenu,'Label','Write to Datastore','Separator','on',... 
		'Callback',@(s,e) obj.saveCalc);
		uimenu(OptionMenu,'Label','Load from Datastore',...
		'Callback',@(s,e) obj.loadUpdate([]));
			
			
		%Plot option menu, allows to selected plot options like plotting earthquakes
		%coast- & borderlines
		obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
		
		uimenu(obj.PlotOptions,'Label','Redraw',... 
		'Callback',@(s,e) obj.updateGUI);
				
		uimenu(obj.PlotOptions,'Label','Mark Large Earthquakes',... 
		'Callback',@(s,e) obj.MarkLargeEq('switch'));
		uimenu(obj.PlotOptions,'Label','Set Large Earthquakes',... 
		'Callback',@(s,e) obj.MarkLargeEq('factor'));
		
		%LargeEq not written yet.
		
		uimenu(obj.PlotOptions,'Label','plot Coastlines','Separator','on',... 
		'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
		 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
		'Callback',@(s,e) obj.TooglePlotOptions('Border'));
		 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
		'Callback',@(s,e) obj.TooglePlotOptions('EQ'));					
		uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
		'Callback',@(s,e) obj.setqual('low'));
		uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
		'Callback',@(s,e) obj.setqual('med'));
		uimenu(obj.PlotOptions,'Label','High Quality Plot',...
		'Callback',@(s,e) obj.setqual('hi'));
		
		uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
		'Callback',@(s,e) obj.plot_modify([],'interp',[]));
		uimenu(obj.PlotOptions,'Label','Shading Flat',... 
		'Callback',@(s,e) obj.plot_modify([],'flat',[]));
		uimenu(obj.PlotOptions,'Label','Brighten +0.4',... 
		'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
		 uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
		'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
			
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.setqual('chk');
			
				
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Time Series');
			 uimenu(MapMenu,'Label','b-value with time (max likelihood)',...
			'Callback',@(s,e) obj.plotVariable([],'b_value',goError,'b_value_bootstrap'));
			
			
			uimenu(MapMenu,'Label','Significant b-value map with time',...
			'Callback',@(s,e) obj.plotVariable([],'sign_bvalue','colormap_bicolor')); 
			
			uimenu(MapMenu,'Label','Magnitude of completness map',...
			'Callback',@(s,e) obj.plotVariable([],'Mc',goError,'Mc_bootstrap'));
			
			 uimenu(MapMenu,'Label','Goodness of fit to power law map',...
			'Callback',@(s,e) obj.plotVariable([],'PowerLawFit','colormap'));
			
			
			uimenu(MapMenu,'Label','a-value map',...
			'Callback',@(s,e) obj.plotVariable([],'a_value','colormap'));
			
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));
			uimenu(MapMenu,'Label','Smoothing Factor',...
			'Callback',@(s,e) obj.Smoother('factor'));
			uimenu(MapMenu,'Label','Error to alpha channel','Separator','on',...
			'Callback',@(s,e) obj.ErrorSwitcher('switch'));
			uimenu(MapMenu,'Label','Error-Alpha Factor',...
			'Callback',@(s,e) obj.ErrorSwitcher('factor'));
		
		
		
			obj.Smoother('init');
			obj.ErrorSwitcher('init')
		else
			%later
			
		
		
		
		end
		
		
	end
	
	
	
	
	function buildMapMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
	
		goError='autoColor';
		
		if ~obj.ThirdDimension	
		
		
			%first the general menus (similar for both)
			 OptionMenu = uimenu( obj.ResultGUI,'Label','- Load/Save');
			 uimenu(OptionMenu,'Label','Calculate again',...
			'Callback',@(s,e) obj.CalcAgain);
			 uimenu(OptionMenu,'Label','Show calculation parameters',...
			'Callback',@(s,e) obj.showCalcParameter);
			uimenu(OptionMenu,'Label','Write to Datastore','Separator','on',... 
			'Callback',@(s,e) obj.saveCalc);
			uimenu(OptionMenu,'Label','Load from Datastore',...
			'Callback',@(s,e) obj.loadUpdate([]));
			
			
			%Plot option menu, allows to selected plot options like plotting earthquakes
			%coast- & borderlines
			obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
			
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			
			if obj.ProfileSwitch
				uimenu(obj.PlotOptions,'Label','Set superelevation',... 
				'Callback',@(s,e) obj.setSuperElevation);
			end
			
			uimenu(obj.PlotOptions,'Label','Set FMD selection radius',... 
				'Callback',@(s,e) obj.setFMDRadius);
			
			uimenu(obj.PlotOptions,'Label','plot Coastlines','Separator','on',... 
			'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
			 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
			'Callback',@(s,e) obj.TooglePlotOptions('Border'));
			 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
			'Callback',@(s,e) obj.TooglePlotOptions('EQ'));					
			uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			
			uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
			'Callback',@(s,e) obj.plot_modify([],'interp',[]));
			uimenu(obj.PlotOptions,'Label','Shading Flat',... 
			'Callback',@(s,e) obj.plot_modify([],'flat',[]));
			uimenu(obj.PlotOptions,'Label','Brighten +0.4',... 
			'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
			 uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
			'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
			
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.setqual('chk');
			
				
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','b-value map (max likelihood)',...
			'Callback',@(s,e) obj.plotVariable([],'b_value',goError,'b_value_bootstrap'));
			 uimenu(MapMenu,'Label','Standard deviation of b-Value (max likelihood) map',...
			'Callback',@(s,e) obj.plotVariable([],'b_value_bootstrap','colormap'));
			uimenu(MapMenu,'Label','Significant b-value map',...
			'Callback',@(s,e) obj.plotVariable([],'sign_bvalue','colormap_bicolor')); 
			uimenu(MapMenu,'Label','Magnitude of completness map',...
			'Callback',@(s,e) obj.plotVariable([],'Mc',goError,'Mc_bootstrap'));
			uimenu(MapMenu,'Label','Standard deviation of magnitude of completness',...
			'Callback',@(s,e) obj.plotVariable([],'Mc_bootstrap','colormap'));
			 uimenu(MapMenu,'Label','Goodness of fit to power law map',...
			'Callback',@(s,e) obj.plotVariable([],'PowerLawFit','colormap'));
			uimenu(MapMenu,'Label','Resolution map',...
			'Callback',@(s,e) obj.plotVariable([],'Resolution','colormap'));
			uimenu(MapMenu,'Label','Earthquake density map',...
			'Callback',@(s,e) obj.plotVariable([],'EqDensity','colormap'));
			uimenu(MapMenu,'Label','a-value map',...
			'Callback',@(s,e) obj.plotVariable([],'a_value','colormap'));
			
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));
			uimenu(MapMenu,'Label','Smoothing Factor',...
			'Callback',@(s,e) obj.Smoother('factor'));
			uimenu(MapMenu,'Label','Error to alpha channel','Separator','on',...
			'Callback',@(s,e) obj.ErrorSwitcher('switch'));
			uimenu(MapMenu,'Label','Error-Alpha Factor',...
			'Callback',@(s,e) obj.ErrorSwitcher('factor'));
		
		
		
			obj.Smoother('init');
			obj.ErrorSwitcher('init')
		else
			%later
			
		
		
		
		end
		
		
	end
	
	
	function buildMouseMenu(obj,graphobj)
		%get plotAxis if needed
		
		if isempty(graphobj)
			plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
				
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
			
			
			cmenu = uicontextmenu('Parent',obj.ResultGUI);
			% Now make the menu be associated with the correct axis
			set(obj.ResultGUI,'UIContextMenu',cmenu);
			set(plotAxis,'UIContextMenu',cmenu);
			% Add uimenu items for the different region selections
			% regionFilter = obj.FilterList.getByName('Region');
			% dataStore = obj.DataStore;
			
			uimenu(cmenu,'Label','Select inside',...
				'Callback',@(s,e) SetFMDRegion(obj,'in'));
			uimenu(cmenu,'Label','Select outside',...
				'Callback',@(s,e) SetFMDRegion(obj,'out'));
			uimenu(cmenu,'Label','Select circular region',...
				'Callback',@(s,e) SetFMDRegion(obj,'circle'));
			uimenu(cmenu,'Label','Close FMD',...
				'Callback',@(s,e) SetFMDRegion(obj,'closeIt'));	
			uimenu(cmenu,'Label','Redraw Map',...
				'Callback',@(s,e) updateGUI(obj));
		else
			cmenu = uicontextmenu('Parent',obj.ResultGUI);
			% Now make the menu be associated with the correct axis
			set(obj.ResultGUI,'UIContextMenu',cmenu);
			set(graphobj,'UIContextMenu',cmenu);
			% Add uimenu items for the different region selections
			% regionFilter = obj.FilterList.getByName('Region');
			% dataStore = obj.DataStore;
			
			uimenu(cmenu,'Label','Select inside',...
				'Callback',@(s,e) SetFMDRegion(obj,'in'));
			uimenu(cmenu,'Label','Select outside',...
				'Callback',@(s,e) SetFMDRegion(obj,'out'));
			uimenu(cmenu,'Label','Select circular region',...
				'Callback',@(s,e) SetFMDRegion(obj,'circle'));
			uimenu(cmenu,'Label','Close FMD',...
				'Callback',@(s,e) SetFMDRegion(obj,'closeIt'));	
			uimenu(cmenu,'Label','Redraw Map',...
				'Callback',@(s,e) updateGUI(obj));
		
		
		end
	
	
	end
	
	
	function Smoother(obj,DoWhat)
		%Turns the smoothing on and off after calculation
		SmoothMenu = findobj(obj.ResultGUI,'Label','Smoothing');
		set(SmoothMenu, 'Checked', 'off');
		
		
		switch DoWhat
			case 'switch'
				if obj.CalcParameter.SmoothMode==1
					obj.CalcParameter.SmoothMode=0;
					set(SmoothMenu, 'Checked', 'off');
				else
					obj.CalcParameter.SmoothMode=1;
					set(SmoothMenu, 'Checked', 'on');
				end
				obj.updateGUI;
				
				
			case 'factor'
				promptValues = {'Smoothing Factor'};
				dlgTitle = 'Set Smoothing Factor';
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.CalcParameter.SmoothFactor},'UniformOutput',false));
				obj.CalcParameter.SmoothFactor=	str2double(gParams{1});
				
				if obj.CalcParameter.SmoothMode==1
					obj.updateGUI;
					set(SmoothMenu, 'Checked', 'on');
				end
			
				
			case 'init'
				set(SmoothMenu, 'Checked', 'off');
				if obj.CalcParameter.SmoothMode==1
					set(SmoothMenu, 'Checked', 'on');
				end
				
		end
	
		
	end
	
	function ErrorSwitcher(obj,DoWhat)
		%Turns the Error 2 Alpha on and off after calculation
		alphaMenu = findobj(obj.ResultGUI,'Label','Error to alpha channel');
		set(alphaMenu, 'Checked', 'off');
		
		
		switch DoWhat
			case 'switch'
				if obj.Error2Alpha==1
					obj.Error2Alpha=0;
					set(alphaMenu, 'Checked', 'off');
				else
					obj.Error2Alpha=1;
					set(alphaMenu, 'Checked', 'on');
				end
				obj.updateGUI;
				
				
			case 'factor'
				
				promptValues = {'AlphaError Factor'};
				dlgTitle = 'Set Alpha Error Factor';
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.ErrorFactor},'UniformOutput',false));
				obj.ErrorFactor=str2double(gParams{1});
				
				if obj.Error2Alpha==1
					obj.updateGUI;
					set(alphaMenu, 'Checked', 'on');
				end
			
				
			case 'init'
				set(alphaMenu, 'Checked', 'off');
				if obj.Error2Alpha==1
					set(alphaMenu, 'Checked', 'on');
				end
				
		end
	
		
	end
	
	
	function MarkLargeEq(obj,DoWhat)
		%Turns the Marker for large earthquake on and off
		MarkMenu = findobj(obj.ResultGUI,'Label','Mark Large Earthquakes');
		set(MarkMenu, 'Checked', 'off');
		
		
		switch DoWhat
			case 'switch'
				if obj.CalcParameter.MarkEq==1
					obj.CalcParameter.SmoothMode=0;
					set(MarkMenu, 'Checked', 'off');
				else
					obj.CalcParameter.MarkEq=1;
					set(MarkMenu, 'Checked', 'on');
				end
				obj.updateGUI;
				
				
			case 'factor'
				promptValues = {'maximal marked Eq'};
				dlgTitle = 'Set maximum Eq';
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.MaxEq},'UniformOutput',false));
				obj.MaxEq=	str2double(gParams{1});
				
				if isempty(obj.MaxEq)
					obj.MaxEq='max';
				end
				
				if obj.CalcParameter.MarkEq==1
					obj.updateGUI;
					set(MarkMenu, 'Checked', 'on');
				end
			
				
			case 'init'
				set(MarkMenu, 'Checked', 'off');
				if obj.CalcParameter.MarkEq==1
					set(MarkMenu, 'Checked', 'on');
				end
				
		end
	
		
	end
	
	
	
	
	function TooglePlotOptions(obj,WhichOne)
		%This functions sets the toogles in this modul
		%WhichOne can be the following strings:
		%	'Border':	Borderlines
		%	'Coast'	:	Coastlines
		%	'EQ'	:	Earthquake locations
		%	'-chk'	:	This will set all checks in the menu
		%			to the in toggles set state.
		
		CoastMenu = findobj(obj.PlotOptions,'Label','plot Coastlines');
		BorderMenu = findobj(obj.PlotOptions,'Label','plot Country Borders');
		EqMenu = findobj(obj.PlotOptions,'Label','plot Earthquake Locations');
		
		switch WhichOne
			case '-chk'
				if obj.BorderToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				if obj.CoastToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				if obj.EQToogle
					set(EqMenu, 'Checked', 'on');
				else
					set(EqMenu, 'Checked', 'off');
				end
				
				
			case 'Border'
				
				obj.BorderToogle=~obj.BorderToogle;
				
				if obj.BorderToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'Coast'
				
				obj.CoastToogle=~obj.CoastToogle;
				
				if obj.CoastToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'EQ'
				
				obj.EQToogle=~obj.EQToogle;
				
				if obj.EQToogle
					set(EqMenu, 'Checked', 'on');
				else
					set(EqMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
		
		end
		
		
	end
	
	
	function plotResult(obj,whichplot)
		%more of a relict dummy function 
		%it allows a bit less complicated plotting, more for scripting
		%it calls plotVariable
		
		
		
		
		switch whichplot
				case 'bvalue'
					%just plot phi (s2/s1 relation)
					obj.plotVariable([],'b_value','colormap');
					
				case 'Mc'
					obj.plotVariable([],'Mc','colormap');	
				
				%case 'Faulting'
				%	obj.plotVariable([],'fAphi','colormap');
				
				%case 'S1dir'	
				%	obj.plotVariable([],'fS1Trend',StrikeMapping);
				
				%case 'S2dir'	
				%	obj.plotVariable([],'fS2Trend',StrikeMapping);
				
				%case 'S3dir'	
					obj.plotVariable([],'fS3Trend',StrikeMapping);
					
		end
		
			
		
		
	
	end
	
	
	
	function plotTimeVariable(obj,plotAxis,ValField,PlotType,AddParam)
		%AddParam is needed for some plots
		import mapseis.plot.*;
		import mapseis.util.AutoBiColorScale;
		
		if nargin<5
			AddParam=[];
		end
		
		
		
				
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','TimeResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','TimeResultAxis');
    			end	
		end
		
		%backup config
		obj.LastPlot={ValField,PlotType,AddParam};
		
		
		
		%Two modes 'singleLine', 'errorLines'
		
		
	
	
	
	
	end
	
	
	
	function plotMapVariable(obj,plotAxis,ValField,PlotType,AddParam)
		%will plot the selected field of the CalcRes structur into the selected plotaxis
		%the PlotType can either be 'colormap' or 'quiver' (have to angles for a quiver plot)
		%AddParam is needed for some plots
		import mapseis.plot.*;
		import mapseis.util.AutoBiColorScale;
		
		if nargin<5
			AddParam=[];
		end
		
		
		
				
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		%backup config
		obj.MapLastPlot={ValField,PlotType,AddParam};
		
		if strcmp(PlotType,'autoColor')
			if obj.Error2Alpha==1
				PlotType='colormap_errorAlpha';
			else
				PlotType='colormap';
			end
		
		end
		
		%get data
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		TheVal=obj.CalcRes.(ValField)';
		
		AlphaMap=~isnan(TheVal);
		
		%This is to prevent colorscale differences after smoothing;
		
		
		if ~isempty(obj.AlphaMap)
				minVal=min(min(TheVal(~obj.AlphaMap&AlphaMap)));	
				maxVal=max(max(TheVal(~obj.AlphaMap&AlphaMap)));
			else
				minVal=min(min(TheVal(AlphaMap)));
				maxVal=max(max(TheVal(AlphaMap)));
		end
		
		caxislimit=[floor(10*minVal)/10 ceil(10*maxVal)/10];
		if isempty(caxislimit)|any(isnan(caxislimit))
			caxislimit='auto';
		end
		%disp(caxislimit)
		
		if obj.CalcParameter.SmoothMode==1;
			%Smooth the map before plotting
			TheValNaN=isnan(TheVal);
			
			%use the mean of the data instead of 0
			if ~isempty(obj.AlphaMap)
				ValMean=mean(mean(TheVal(~obj.AlphaMap&AlphaMap)));
			else
				ValMean=mean(mean(TheVal(AlphaMap)));
			end
			
			
			
			if ~isempty(ValMean)
				TheVal(TheValNaN)=ValMean;
			else
				TheVal(TheValNaN)=0;
			end
			
			
			
			hFilter = fspecial('gaussian', 5, 2.5);
			TheVal= imfilter((TheVal + obj.CalcParameter.SmoothFactor), hFilter, 'replicate');
			
			%Smoothing adds to the total sum, subtract that to compensate.
			%TheVal=TheVal-obj.CalcParameter.SmoothFactor;
			
			TheVal(TheValNaN)=NaN;
			
			if ~isstr(caxislimit)
				%add the factor to caxislimit
				caxislimit=caxislimit+obj.CalcParameter.SmoothFactor;
			end
			
		end
		
		%limits
		xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
		ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
		
		%disp(xlimiter)
		%disp(ylimiter)
		if obj.ProfileSwitch
			xlab='Profile [km] ';
			ylab='Depth [km] ';
		else
			xlab='Longitude ';
			ylab='Latitude ';
		end	
		
		
		
		%set all axes right
		xlim(plotAxis,xlimiter);
		ylim(plotAxis,ylimiter);
		
		latlim = get(plotAxis,'Ylim');
           	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           	set(plotAxis,'DrawMode','fast');
		
           	
           	if ~isempty(obj.AlphaMap)
           		TheVal(obj.AlphaMap)=NaN;
           	end	
		
		%determine if a quiver is needed 
		switch PlotType
			case 'colormap'
				
				
				
				PlotData={xdir,ydir,TheVal};
				
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
				  				'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData','auto',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit,...
								'ColorToggle',true,...
								'LegendText',ValField);
				 %disp(plotAxis)				
				 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
				 buildMouseMenu(obj,handle);
				 
				 
			case 'colormap_bicolor'
				%same as colormap but with a special scale		
				PlotData={xdir,ydir,TheVal};
				
				NewAlpha=ones(size(TheVal));
				NewAlpha(isnan(TheVal))=0;
				RegVal=TheVal==obj.CalcParameter.Overall_bval;
				NewAlpha(RegVal)=0.5;
								
				
				
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
				  				'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData',NewAlpha,...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit,...
								'ColorToggle',true,...
								'LegendText',ValField);
				 %disp(plotAxis)				
				 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
				 buildMouseMenu(obj,handle);		
				 prevCaxis=caxis(plotAxis);
				 minCal=floor(prevCaxis(1)*10)/10;
				 maxCal=ceil(prevCaxis(2)*10)/10;
				 overallVal=obj.CalcParameter.Overall_bval;
				
				 if obj.CalcParameter.SmoothMode==1;
				 	%correct for smoothing
				 	%minCal=minCal+obj.CalcParameter.SmoothFactor;
				 	%maxCal=maxCal+obj.CalcParameter.SmoothFactor;
				 	overallVal=overallVal+obj.CalcParameter.SmoothFactor;
				 end	
				 
				 
				 %build scale
				 biScale=AutoBiColorScale(minCal,maxCal,overallVal);
			
				 %set axis
				 caxis(plotAxis,[minCal maxCal]);
				 colormap(plotAxis,biScale);

				 
				 
			case 'colormap_errorAlpha'
				%The Additonal Parameter has here to name a field which 
				%will be used as AlphaData
				try
					TheError=obj.CalcRes.(AddParam)';
				catch 	
					TheError=NaN;
					disp('Field did not exist');
				end
				
								
				if ~all(isnan(TheError))
					minErr=nanmin(nanmin(TheError));	
					maxErr=nanmax(nanmax(TheError));
					NewAlpha=ones(size(TheError))-obj.ErrorFactor*(TheError-minErr)/abs(maxErr-minErr);
					NewAlpha(isnan(TheVal))=0;
				
				else
					NewAlpha='auto';
								
				end
				
			
				PlotData={xdir,ydir,TheVal};
				
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
				  				'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData',NewAlpha,...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit,...
								'ColorToggle',true,...
								'LegendText',ValField);
				 %disp(plotAxis)				
				 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
				 buildMouseMenu(obj,handle);	
			
				 
			case 'quiver'
				
					
				if obj.CalcParameter.Calc_Method==1
					xdir=obj.CalcRes.Xmeshed;
					ydir=obj.CalcRes.Ymeshed;
					TheVal=reshape(TheVal,numel(xdir),1);
					AlphaMap=reshape(AlphaMap,numel(xdir),1);
					xdir=xdir(AlphaMap);
					ydir=ydir(AlphaMap);
					TheVal=TheVal(AlphaMap);
					
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				%calculate dx and dy for the angle (TheVal
				%only up 180� used
				le=0.2;
				
				TheVal=mod(TheVal,180);

				%first the angles smaller than 90�
				sma=TheVal<90;

				%the larger than 90� angels
				lar=not(sma);

				%the x coordinate
				avect(:,1) = sin(deg2rad(TheVal))*le;

				%the sma part
				avect(sma,2) = cos(deg2rad(TheVal(sma)))*le;
				
				%the lar part
				avect(lar,2) = -cos(deg2rad(TheVal(lar) - 90))*le;
				
				PlotData=[xdir,ydir,avect(:,1),avect(:,2)];
				
				PlotConfig	= struct(	'PlotType','Vector2D',...
				  				'Data',[PlotData],...
								'MapStyle','not needed',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','red',...
								'LineStylePreset','normal',...
								'MarkerStylePreset','none',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'ColorToggle',true,...
								'LegendText',ValField);
				disp(plotAxis)
			         [handle legendentry] = PlotVector2D(plotAxis,PlotConfig);
		end	
		
		if ~obj.ProfileSwitch
			%at the moment only available in the map view
			%plot coastline, borders and earthquake locations if wanted
			if obj.BorderToogle
				hold on
				BorderConf= struct('PlotType','Border',...
							'Data',obj.Datastore,...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'LineStylePreset','normal',...
							'Colors','black');
				[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
				
			end
			
			
			if obj.CoastToogle
				hold on
				CoastConf= struct( 'PlotType','Coastline',...
						   'Data',obj.Datastore,...
						   'X_Axis_Label','Longitude ',...
						   'Y_Axis_Label','Latitude ',...
						   'LineStylePreset','Fatline',...
						   'Colors','blue');
				[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
			
			end
			
			
			if obj.EQToogle
				hold on
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',obj.SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
			
				
				[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
			end
				
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			latlim = get(plotAxis,'Ylim');
           		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           		set(plotAxis,'DrawMode','fast');
			set(plotAxis,'LineWidth',2,'FontSize',12);
			
			%set title
			theTit=title(ValField);
			set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
			
			
			%rebuild Polygons
			hold on
			obj.renewPoly(plotAxis);
			hold off
			
		else
			
			
			%The filterlist has to available for now, means now eq plot on
			%profiles with loaded data and missing filter.
			if obj.EQToogle&~isempty(obj.Filterlist)
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				
				hold on
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',obj.SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Distance [km] ',...
							'Y_Axis_Label','Depth [km] ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
			
				
				[handle1 entry1] = PlotEarthquakeSlice(plotAxis,EqConfig);
			end
				
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			%depthlim = get(plotAxis,'Ylim');
			%distlim = get(plotAxis,'Xlim');
			%deSpan=10*abs(depthlim(1)-depthlim(2));
			%diSpan=abs(distlim(1)-distlim(2));
			
			%set(plotAxis,'dataaspect',[1 diSpan/deSpan 1]);
			set(plotAxis,'dataaspect',[obj.TheAspect 1 1]);
			set(plotAxis,'YDir','reverse');
			set(plotAxis,'LineWidth',2,'FontSize',12);
			
			%set title
			theTit=title(ValField);
			set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
			
			%rebuild Polygons
			hold on
			obj.renewPoly(plotAxis);
			hold off
		end
	end
	
	
	function updateGUI(obj)
		%uses the last plot option and plot it again
		ValField=obj.LastPlot{1};
		PlotType=obj.LastPlot{2};
		AddParam=obj.LastPlot{3};
		
		%only needed for quality change a similar stuff
		obj.plotVariable([],ValField,PlotType,AddParam);
		
	end
	
	
	function plot_modify(obj,bright,interswitch,circleGrid)
		%allows to modify the plot (some legacy stuff which will be changed
		%later if needed is in here)
		 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(plotAxis)
			 %try retag
			 %get all childrens of the main gui
    			gracomp = get(obj.ResultGUI, 'Children');
    			
    			%find axis and return axis
    			plotAxis = findobj(gracomp,'Type','axes','Tag','');
    			set(plotAxis,'Tag','MainResultAxis');
    		end	
		
    		if ~isempty(bright)
			axes(plotAxis);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			
			switch interswitch
				case 'flat'
					axes(plotAxis);
					shading flat;
					
				case 'interp'
					axes(plotAxis);
					shading interp;
					
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		
	end
	
	
	function adju(obj,asel, whichplot)
		%function version of adju
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;


		prompt={'Enter the minimum magnitude cut-off','Enter the maximum radius cut-off:','Enter the minimum goodness of fit percatge'};
		def={'nan','nan','nan'};
		dlgTitle='Input Map subselection Criteria';
		lineNo=1;
		answer=inputdlg(prompt,dlgTitle,lineNo,def);
		re4 = re3;
		l = answer{1,1}; Mmin = str2num(l) ;
		l = answer{2,1}; tresh = str2num(l) ;
		l = answer{3,1}; minpe = str2num(l) ;
		
		
		obj.PackList=[obj.PackList; {'re4';'l';'asel'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

		%back to plot
		obj.plot_bval(whichplot);
		
	end
	
	function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	
	
	function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		EveryThingGood=true;
		
		
		if isempty(obj.Filterlist)
			warndlg('New filterlist will be set')
		end
		
		
		%get parameters
		obj.openCalcParamWindow;
		
		
		
		if obj.StartCalc
			
			%figNr=findobj('Name','b-value-map');
			
			%if ~isempty(figNr)
			%	close(figNr);
			%end
		
			%refresh catalogs
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			%obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
						
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the current regionfilter');
					obj.ErrorDialog='No profile selected in the current regionfilter';
				 end
			end
			if EveryThingGood
				%Calculation
				obj.doTheCalcThing;
				
				obj.updateGUI;
				
				%Build resultGUI
				%obj.BuildResultGUI
						
				%plot results
				%obj.plotResult('stress')
			end	
		end
	
	
	end
	
	
	function saveCalc(obj)
		%This will save the calculation in the datastore and ask to open it next time
		%The filterlist will be saved within the datastore with this and reopened next time
		%when the calculation is loaded. It has to be done in this way or else nobody knows 
		%what filtersetting was used in the calculation.
		
		%Ask for a name
		gParams = inputdlg('Calculation Name','Name of the Calculation',1,{obj.CalcName});
		if ~isempty(gParams)
			obj.CalcName=gParams{1};
			
			%New Save algorithm without filterlist
			SaveStruct=struct(	'Selected',obj.SendedEvents,...
						'CalcResult',obj.CalcRes,...
						'CalcParameter',obj.CalcParameter,...
						'ProfileSwitch',obj.ProfileSwitch,...
						'ThirdDimension',obj.ThirdDimension,...
						'CalcName',obj.CalcName,...
						'AlphaMap',obj.AlphaMap,...
						'VersionCode','v1.55');
						
			
						
			CalcObject={obj.CalcName,SaveStruct};
			
			%try getting the CalcObject from the datastore
			
			try 
				Calculations=obj.Datastore.getUserData('new-bval-calc');
				
			catch
				Calculations={};
			end
			
			if ~isempty(Calculations)
				%try finding the calc
					founded=strcmp(Calculations(:,1),obj.CalcName);
					if sum(founded)>0
						Calculations(founded,:)=CalcObject;
					else
						Calculations(end+1,:)=CalcObject;
					end	
			else
				Calculations(1,:)=CalcObject;
			end	
			
			obj.Datastore.setUserData('new-bval-calc',Calculations);
			
		end
		
		
		
		%write to the Commander (always do this even if canceled, this will update
		%the buffers)
		obj.Commander.pushWithKey(obj.Keys.ShownDataID,obj.Datastore);
		obj.Commander.switcher('Cata');
		
	end
	
	
	function showCalcParameter(obj)
		%opens a message box and shows the parameter, usefull if a
		%calculation is loaded;
		
		TheFields=fieldnames(obj.CalcParameter);
		
		for i=1:numel(TheFields)
			TextBuild(i)={[TheFields{i},': ',num2str(obj.CalcParameter.(TheFields{i}))]};
			
		end
		
		msgbox(TextBuild,'Parameters of the current calculation');
		
		
	end
	
	
	function loadUpdate(obj,oldCalcs)
		%Just calles load and runs an update afterwards
		%It is needed for the menu
		
		loadCalc(obj,oldCalcs);
		obj.updateGUI;
	
	end
	
	
	function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		if isempty(oldCalcs)
			%check if old calculations are avialable
			try
				oldCalcs=obj.Datastore.getUserData('new-bval-calc');
			catch
				disp('no calculation available in this datastore')
				return;
			end
		end
		
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
			obj.Loaded=false;
		else
			obj.StartCalc=true;
			obj.Loaded=true;
		end
		
		%kill FMDfilters if needed
		obj.FMDSuicide;
		
		if obj.StartCalc
			TheCalc=oldCalcs{NewParameter.WhichOne,2};
			
			
			%Now set the data
			obj.Filterlist = [];
			%obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			obj.SendedEvents=TheCalc.Selected;
			obj.CalcParameter=TheCalc.CalcParameter;
			obj.ProfileSwitch=TheCalc.ProfileSwitch;
			obj.ThirdDimension=TheCalc.ThirdDimension;
			obj.CalcName=TheCalc.CalcName;
			obj.AlphaMap=TheCalc.AlphaMap;
			obj.CalcRes=TheCalc.CalcResult;
			
			
			
			
		end
		
		if strcmp(TheCalc.VersionCode,'v1.5')
			%add missing parameter if needed
			obj.CalcParameter.Overall_bval=1;
			obj.CalcParameter.Calc_sign_bval=0;
			MapSize=size(obj.CalcRes.Mc);
			
			obj.CalcRes.sign_bvalue=NaN(MapSize);
			obj.CalcRes.sign_WinningModel=NaN(MapSize);
			obj.CalcRes.sign_likelihood1=NaN(MapSize);
			obj.CalcRes.sign_likelihood2=NaN(MapSize);
			obj.CalcRes.sign_akaike1=NaN(MapSize);
			obj.CalcRes.sign_akaike2=NaN(MapSize);
			
			msgbox('Missing results were added as NaN')
			
		end
		
	end
	
	function setSuperElevation(obj)
		promptValues = {'SuperElevation Factor'};
		dlgTitle = 'Set SuperElevation Factor';
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{obj.TheAspect},'UniformOutput',false));
		obj.TheAspect=	str2double(gParams{1});
				
				
		obj.updateGUI;
				
	
	
	end
	
	
	
	
	
	function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
			Menu2Check(3) = findobj(obj.PlotOptions,'Label','Zmap style');
			Menu2Check(2) = findobj(obj.PlotOptions,'Label','Medium Quality Plot');
			Menu2Check(1) = findobj(obj.PlotOptions,'Label','High Quality Plot');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
 				case 'low'
 					%dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotQuality='low';
 					obj.updateGUI;
                		
				case 'med'
					%dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotQuality='med';
					obj.updateGUI;
					
				case 'hi'
					%dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotQuality='hi';
					obj.updateGUI;
					
				case 'chk'
					%try StoredQuality = dataStore.getUserData('PlotQuality');
					%catch dataStore.setUserData('PlotQuality','med');
					%StoredQuality = dataStore.getUserData('PlotQuality');		
					%end
					StoredQuality = obj.PlotQuality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
	end
	
	
	
	
	%functions for the FMD window and picker
	%=======================================
	
	function setFMDRadius(obj)
		promptValues = {'FMD Selection Radius'};
		dlgTitle = 'Set FMD Selection Radius';
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{deg2km(obj.FMDRad)},'UniformOutput',false));
		obj.FMDRad=	km2deg(str2double(gParams{1}));
				
	
	
	end
	
	
	
	function renewPoly(obj,plotAxis)
		import mapseis.plot.*;
		
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		
		if ~isempty(obj.FMDFilter)
			filterRegion = getRegion(obj.FMDFilter);
			pRegion = filterRegion{2};
			RegRange=filterRegion{1};
			
			if isobject(pRegion)
				rBdry = pRegion.getBoundary();	
                	else 
                		rBdry = pRegion;
			end
			
					
			switch RegRange
        			case {'in','out'}
                			selection=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
                			selection.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,p,obj.FMDFilter));
                			selection.setColor('r');		
                			%set the menu point depth slice to 'off'
                			
                			%add menus to the object
                			%SetImMenu(obj,selection,regionFilter,[]);
                			        
                			obj.FMDPoly{1}=selection;
                			
                			
            			case 'circle'
            				rBdry(3)=obj.FMDFilter.Radius;
            				
            				
            				
            				%draw circle
            				hold on
            				handle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
            				selection = impoint(plotAxis,rBdry(1:2));
            				hold off
            				
            				%selection.setFixedAspectRatioMode(1)
					selection.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,[p(1),p(2),rBdry(3)],...
                					obj.FMDFilter));
            				
                			selection.setColor('r');
                			
                			obj.FMDPoly{1}=selection;
                			obj.FMDPoly{2}=handle;
            				
                			%add menus to the object
                			%SetImMenu(obj,selection,obj.FMDFilter,handle);
                		
                			
                		case 'closeIt'
                			%should never happen, because it should be already killed
					obj.FMDSuicide;	
                			
                			
                	end
                	
		end
	
	
	end
	
	
	
	
	function SetFMDRegion(obj,WhichOne)
		import mapseis.plot.*;
         	import mapseis.filter.*;
		import mapseis.util.*;
		import mapseis.region.*;
		import mapseis.projector.*;
		import mapseis.util.gui.SetImMenu;

		if strcmp(WhichOne,'closeIt')
         		obj.FMDSuicide;	
		else
		
			%Check if FMD filter is existing and if it is the right one
			if isempty(obj.FMDFilter)
				if obj.ProfileSwitch&~obj.ThirdDimension
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter=ProfileRegionFilter('all',...
						profLine,ProfWidth,obj.SendedEvents,[],'ProfileRegion');
					
					
				elseif ~obj.ProfileSwitch&~obj.ThirdDimension
					obj.FMDFilter=RegionFilter('all',[],'Region');
					
					
					
				else
					%not supported at the moment
				
				end
			else
			
				%check if the filter is right for the purpose
				if obj.ProfileSwitch&~obj.FMDFilter.isProfileFilter
					%wrong filter, change
					clear obj.FMDFilter;
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter=ProfileRegionFilter('all',...
						profLine,ProfWidth,obj.SendedEvents,[],'ProfileRegion');
						
					
				elseif obj.ProfileSwitch&obj.FMDFilter.isProfileFilter
					%right filter but may needs correction
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter.setProfile(profLine,ProfWidth);
					obj.FMDFilter.setMasterSelection(obj.SendedEvents);
					
					
				elseif ~obj.ProfileSwitch&obj.FMDFilter.isProfileFilter	
					%also wrong filter
					clear obj.FMDFilter;
					obj.FMDFilter=RegionFilter('all',[],'Region');
				end	
			end
			
			
			
		 
			dataStore = obj.Datastore;
			
			
			
			
		
			plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
					
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
			
			
			
			
			    switch WhichOne
				case {'in','out'}
				    	axes(plotAxis);
				    	obj.FMDPoly{1}=impoly;
				    	newRegion = Region(obj.FMDPoly{1}.getPosition);
				   
				    
				    	obj.FMDPoly{1}.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,p,obj.FMDFilter));
                		    	
                			obj.FMDPoly{1}.setColor('r');
                			obj.FMDFilter.setRegion(WhichOne,newRegion);
				    
				    
				
				
			
				case {'circle'}
					%creates a circle for the selection
					
					axes(plotAxis);
					circ=Ginput(1);%polyg.getPosition;
					
					%correct the position to the lower left corner (imellipse)
					
					if ~obj.ProfileSwitch
						obj.FMDFilter.setRadius(obj.FMDRad);
					else
						obj.FMDFilter.setRadius(deg2km(obj.FMDRad));
					end
					
					newRegion = [circ(1),circ(2)];
					
										
					hold on
					handle=drawCircle(plotAxis,true,circ(1:2),obj.FMDRad);
            				selection = impoint(plotAxis,circ(1:2));
					hold off
					
            				selection.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,[circ(1),circ(2),obj.FMDRad],...
                					obj.FMDFilter));
            				
                			selection.setColor('r');
                			
                			obj.FMDPoly{1}=selection;
                			obj.FMDPoly{2}=handle;
            				
					obj.FMDFilter.setRegion(WhichOne,newRegion);
				
			    end
			    
			% Run the filter
			obj.FMDFilter.execute(obj.Datastore);

			obj.CreateFMDGUI;
			obj.UpdateFMD;
			
         	end

	end
	
	
	
	
	function CreateFMDGUI(obj)
		%creates the gui if needed or updates it
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		
		if ~isempty(obj.FMDWindow)
			%if clf would be called with empty obj.FMDWindow
			%it would deform to clf([])=clf() and clear every figure
			%in function focus.
			try
				%window exists and is fine
				clf(obj.FMDWindow);
			end
			
			if ~ishandle(obj.FMDWindow)
				obj.FMDWindow=[];
			end	
		end
		
		 obj.FMDWindow = onePanelLayout_mk2(obj.FMDWindow,'FMD-sidekick',...
		   {'TickDir','out','color','r','FontWeight','bold','FontSize',12});
		   set(obj.FMDWindow,'Position',[ 400 200 800 600 ],'visible','on');

		
		FMDAxis=gca;
		set(FMDAxis,'Tag','FMDResultAxis');
		   
		%build button
		updateButton = uicontrol(obj.FMDWindow, 'Units', 'Normalized',...
                		'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update FMD',...
                		'Callback',@(s,e) obj.UpdateFMD);
		set(obj.FMDWindow,'DeleteFcn',@(s,e) obj.FMDSuicide);
                		
                		
	end
	
	
	
	
	function UpdateFMD(obj)
		%say it all updates the gui
		import mapseis.plot.*;
		
		if ~isempty(obj.FMDFilter)
			
			
		
			FMDAxis = findobj(obj.FMDWindow,'Tag','FMDResultAxis');
			if isempty(FMDAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.FMDWindow, 'Children');
					
				%find axis and return axis
				FMDAxis = findobj(gracomp,'Type','axes','Tag','');
				set(FMDAxis,'Tag','FMDResultAxis');
			end	
			
			%get selection of events
			obj.FMDFilter.execute(obj.Datastore);
			selected=obj.FMDFilter.getSelected&obj.SendedEvents;
			
			set(FMDAxis,'pos',[0.1300    0.2500    0.7750    0.65]);

						
			%create plot config
			Params =struct('Mc_method',obj.CalcParameter.Mc_Method,...
					'NoCurve',false,...
					'Use_bootstrap',obj.CalcParameter.BootSwitch,...
					'Nr_bootstraps',obj.CalcParameter.BootSamples,...		
					'Mc_correction',obj.CalcParameter.McCorr);
			
			FMDConfig	= struct('PlotType','FMDex',...
					'Data',obj.Datastore,...
					'BinSize',0.1,...
					'b_line',true,...
					'CustomCalcParameter',Params,...
					'Selected',selected,...
					'Mc_Method',1,...
					'Write_Text',false,...
					'Value_Output','all',...
					'LineStylePreset','Fatline',...
					'X_Axis_Label','Magnitude ',...
					'Y_Axis_Label','cum. Number of Events ',...
					'Colors','red',...
					'X_Axis_Limit','auto',...
					'Y_Axis_Limit','auto',...
					'LegendText','FMD');
			
			%plot da shit
			[handle DataOut] = PlotFMD_expanded(FMDAxis,FMDConfig);
			 txt1=text(0, -.11,DataOut.Text{1},'FontSize',10,'Color','k','Units','normalized');
			 set(txt1,'FontWeight','normal')
			 text(0, -.08,DataOut.Text{2},'FontSize',10,'Color','k','Units','normalized');
			 text(0, -.14,DataOut.Text{3},'FontSize',10,'Color','k','Units','normalized');
			 
			 
			 obj.updateGUI;
			 
			 %focus back to the FMD
			 axes(FMDAxis);
		end
		
	
	end
	
	
	function FMDSuicide(obj)
		%closes the GUI and deletes the filter
		try
			obj.FMDPoly{1}.delete;
		end
		
		
		try
			%will be deleted in next update
			set(obj.FMDPoly{2},'visible','off')
		end
		
		
		try
			close(obj.FMDWindow);
		end
		
		
		try	
			clear obj.FMDWindow;
		end
		
		try	
			clear obj.FMDFilter;
			obj.FMDFilter=[];
		catch
			%at least empty the filterslot 
			obj.FMDFilter=[];
		end
		
		
	end
	
	
	
	function FMDfilterupdater(obj,pos,regionFilter)
		import mapseis.region.*;
		import mapseis.Filter.*;
		import mapseis.util.*;
		
		%special version of the filter updater	 
	
	
		%Uses the input from imroi type objects and updates the filter 
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange = filterRegion{1};
	
		switch RegRange
						
			case {'in','out'}
				newRegion = Region(pos);
				regionFilter.setRegion(RegRange,newRegion);
	
				
			case 'circle'		
						
				if ~obj.ProfileSwitch
					regionFilter.setRadius(obj.FMDRad);
				else
					regionFilter.setRadius(deg2km(obj.FMDRad));
				end
						
						
				%dataStore.setUserData('gridPars',newgrid);
							
				%update filter
				newRegion = [pos(1),pos(2)];
				regionFilter.setRegion(RegRange,newRegion);
							
			case 'closeIt'
				obj.FMDSuicide;
		  end	
          end
	

		
	
	
end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	
	%THE FUNCTIONS UNDERNEATH ARE NOT ALWAYS DIRECTLY USEABLE, AS THEY OFTEN NEED DIRECT ACCESS
	%TO THE VARIABLE, SO JUST COPY IF NEEDED.
	function TheEvalString = UnpackStruct(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		TheEvalString=[];
		
		for i=1:numel(thefields)
			
			TheEvalString=[TheEvalString; thefields{i},'=' num2str(TheStruct.(thefields{i})),';'];
		end
		 %can be done easier directly in the function which should be calling this, it is more sort of 
		 %a sketch
		
		
	
	end
	
	
	function CellData = UnpackStruct2Cell(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		
		
		for i=1:numel(thefields)
			
			CellData{i,1}=thefields{i}
			CellData{i,2}=TheStruct.(thefields{i});
		end
		
		%assignin can be used to set the variable on the string.
	end
	
	function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itselectorf
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end
	
	
	function TheStruct = PackAllVariable()
		%The opposite of UnPack, it takes every variable in the current Workspace an packs it 
		%into a structure
		
		varilist=who;
		
		for i=1:numel(varilist)
			TheStruct.(varilist{i})=eval(varilist{i});
		
		end
	
	
	end
	
	

	
	%---------------
end
	
	

end

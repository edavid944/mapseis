classdef MiniWrapperBvalgrid_v2 < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Selected
		GUISwitch
		CalcWhat
		CalcMode
		GridMode
		Keys
		Filterlist
		CalcResult
		CalcParameter
		ErrorDialog
		ParallelMode
		StartCalc
		CalcName
		Region
		AlphaMap
	end

	events
		CalcDone

	end


	methods
	
	function obj = MiniWrapperBvalgrid_v2(ListProxy,Commander,GUISwitch,CalcParameter)
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		%This is a first application of the MiniWrapperTemplate
		%maybe later the Template will be used as SuperClass, but for 
		%this case the template is used directly
		
		%CalcWhat can either be 'bval' for b-values and or 'Mc' to calculate only Mc
		%or 'both' (will be set in the GUI)
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		obj.Commander = Commander; 
		obj.ParallelMode=obj.Commander.ParallelMode;
		
		obj.Region=[];
		obj.AlphaMap=[];
		obj.GUISwitch=GUISwitch;
		obj.CalcWhat='both';
		
		%get the current datastore and filterlist and build zmap catalog
		obj.Datastore = obj.Commander.getCurrentDatastore;
		obj.Filterlist = obj.Commander.getCurrentFilterlist; 
		obj.Keys=obj.Commander.getMarkedKey;
		selected=obj.Filterlist.getSelected;
		obj.Selected=selected;
		
		
		
		%kick out later load will be integrated
		%Save name used later just here to remember it
		Savename='Map-Calc-v2';
		
		
		if ~isempty(CalcParameter)
			%Parameters are include, no default parameter will
			%be set
			obj.CalcParameter=CalcParameter;
			
			%try if some mode data is include
			try 
				obj.CalcWhat=CalcParameter.CalcWhat;
			catch
				obj.CalcWhat='both';
			end
			
			try 
				obj.CalcMode=CalcParameter.CalcMode;
			catch
				obj.CalcMode='1-step';
			end
			
			try 
				obj.GridMode=CalcParameter.GridMode;
			catch
				obj.GridMode='regular';
			end
			
			%parameter should be set, no set to ready
			 obj.StartCalc=true;
		
		else
			obj.InitVariables;
			
			
		end	
		
		
		
		if obj.GUIswitch
			%start mini window 
			obj.StartMiniGUI
		end
		
		if obj.StartCalc
			obj.CalcName='b-value-calc'
			
			%Build the grid with new parameters if needed
			obj.Gridit(false);
			
			%Calculation
			obj.doTheCalcThing;
			
			%Plotting
			obj.plot_bval('mBvalue');
		
		end
		
		else
			%Init the needed zmap variables (here done only because
			%of the paths);
			obj.InitVariables;
			
			obj.loadCalc(PreviousCalc);
			
			if obj.StartCalc
				obj.plot_bval('mBvalue');
			end
		end	
		
		
	end

	
	
	function StartMiniGUI(obj)
		%This will start a small gui with submenus allowing to set all the necessary 
		%Parameters
		
	
	end
	
	function InitVariables(obj)
		%In the new version parameters are directly writen to the structur,
		%no "load and save", and a lot of unneeded variables are kicked out
		
		if isempty(obj.CalcWhat);
			obj.CalcWhat='both';
		end
		
		obj.CalcMode='1-step';
		obj.GridMode='regular';
		
		
		
		%Not all parameter are always needed.
		obj.CalcParameter = struct(	'MinNumber',50...
						'fixMcValue',2.2,...
						'Bootstrap',false,...
						'Bootstrap_Sample',100,...
						'Mc_Correction',0.2,...
						'Mc_Binning',0.1,...
						'dx',1,...
						'dy',1,...
						'dz',1,...
						'NumEvents',100,...
						'Sel_Radius',5,...
						'Selection_Method','Num',...
						'SmoothMode',0,...
						'SmoothFactor',0.2,...
						'McMethod',1,...
						'bvalMethod',1,...
						'previous_Mc',false,...
						'McMap',[],...
						'SRG_Radius_coeff',1,...
						'SRG_minSpacing',[-inf -inf],...
						'SRG_KeepTop',false,...
						'SRG_maxNumber',400,...
						'SRG_minNumber',50,...
						'SRG_StrictMode',false);
						
		
		
	
	end
	
	
	function Gridit(obj,GridType)
		
		%not needed in the same way as in v1, this will build 
		%necessary grid parameters and start SelfRefineGrid if needed
		%disp(obj.ZmapVariables)
		%unpack the variables,
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		
		if initswitch
			%This has to be done just once, hence the switch;
			%update packlist
			obj.PackList=[obj.PackList; {'dx';'dy';'ni';'newgri';'xvect';'yvect';...
					'll';'gx';'gy'}];
			
			
			%get parameter from the datastore
			
			%build grid
			gridpars=obj.Datastore.getUserData('gridPars')
			
			if numel(gridpars.gridSep)<2
				dx=gridpars.gridSep;
				dy=gridpars.gridSep;
			else
				dx=gridpars.gridSep(1);
				dy=gridpars.gridSep(2);
			end	
			
			ra=deg2km(gridpars.rad);
			
			ni=100;
			Nmin = 50;
			
		end
		
		%extracted from CalcGridGUI, delete later what's not needed.
		regionFilter = obj.Filterlist.getByName('Region');
		regionParms = regionFilter.getRegion();
		
		if ~strcmp(regionParms{1},'all')
			theRegion = regionParms{2};
			bBox=theRegion.getBoundingBox();
			obj.Region=theRegion;
                else
                	bBox(1)=min(obj.ZmapCatalog(:,1));
                	bBox(2)=max(obj.ZmapCatalog(:,1));
                	bBox(3)=min(obj.ZmapCatalog(:,2));
                	bBox(4)=max(obj.ZmapCatalog(:,2));
                end
                %xRange = (bBox(1):ptSep(1):bBox(2))';
                %yRange = (bBox(3):ptSep(2):bBox(4))';
		vX = [bBox(1); bBox(1); bBox(2); bBox(2)];
		vY = [bBox(4); bBox(3); bBox(3); bBox(4)];
		vX = [vX; vX(1)];
		vY = [vY; vY(1)]; 
		
		%from ex_selectgrid; (it is used in the fullarea mode, the data is filtered anyway)
		%==================
		%Could maybe be optimized, but leave it at the moment as it is
		
		% Create a rectangular grid
		vXVector = [bBox(1):dx:bBox(2)];
		vYVector = [bBox(3):dy:bBox(4)];
		mGrid = zeros((length(vXVector) * length(vYVector)), 2);
		nTotal = 0;
		for i = 1:length(vXVector)
		   for j = 1:length(vYVector)
		     nTotal = nTotal + 1;
		     mGrid(nTotal,:) = [vXVector(i) vYVector(j)];
		   end;
		end;
		  
		% Extract all gridpoints in chosen polygon
		XI=mGrid(:,1);
		YI=mGrid(:,2);
  
		m = length(vX)-1;      %  number of coordinates of polygon
		l = 1:length(XI);
		l = (l*0)';
		vUsedNodes = l;               %  Algorithm to select points inside a closed
		%  polygon based on Analytic Geometry    R.Z. 4/94
		for i = 1:m;
		  
			l= ((vY(i)-YI < 0) & (vY(i+1)-YI >= 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0) | ...
		  		((vY(i)-YI >= 0) & (vY(i+1)-YI < 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0);
		    
		  	if i ~= 1 
		  		vUsedNodes(l) = 1 - vUsedNodes(l);
		  	else
		  		vUsedNodes = l; 
		  	end;         
		    
		end;         
		
		%grid points in polygon
		mGrid = mGrid(vUsedNodes,:);
		
		%======================
		
		%newgri, xvect, yvect, ll
		
		%set the values
		newgri=mGrid;
		xvect=vXVector;
		yvect=vYVector;
		ll=vUsedNodes;
		
		gx = xvect;
		gy = yvect;
		
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
	
	end
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		%and it will be used here.
		import mapseis.calc.Mc_calc.calc_Mc;
		
		
		%unpack again all the variables
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		%Needed Parameterlist
		%dim = [8 2];
		
		Title = 'b-value, a-value and Mc';
		Prompt={'Mc Method:', 'inb2';...
			'Mc bootstraps:','bBst_button';...
			'Number of Bootstraps:','nBstSample';...
			'Selection Method:','SelMethod';...
			'Smoothing?','SmoothMode';...
			'Smoothing Factor','SmoothFactor';...
			'Number of Events:','ni';...
			'Selection Radius (km):','ra';...
			'Grid spacing x (deg):','dx';...
			'Grid spacing y (deg):','dy';...
			'Min. Num. > Mc:','Nmin';...
			'Fixed Mc (only in "Fixed Mc"):','fMcFix';...			
			'Mc Correction for Maxc:','fMccorr'};
		
		labelList2 =  {'1: Maximum curvature'; ...
			'2: Fixed Mc = minimum magnitude (Mmin)'; ...
			'3: Mc90 (90% probability)'; ...
			'4: Mc95 (95% probability)'; ...
			'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
			'6: EMR-method'; ...
			'7: Mc due b using Shi & Bolt uncertainty'; ...
			'8: Mc due b using bootstrap uncertainty'; ...
			'9: Mc due b Cao-criterion'}; 
		
		%Mc Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=labelList2;
		Formats(1,2).type='none';
		Formats(1,1).size = [-1 0];
		Formats(1,2).limits = [0 1];
		
		%bootstrap toggle
		Formats(2,1).type='check';
		
		%bootstrap number
		Formats(2,2).type='edit';
		Formats(2,2).format='integer';
		Formats(2,2).limits = [0 9999];
		
		%Selection Method
		Formats(4,1).type='list';
		Formats(4,1).style='togglebutton';
		Formats(4,1).items={'Number of Events','Constant Radius'}
		Formats(4,1).size = [-1 0];
		Formats(4,2).limits = [0 1];
		Formats(4,2).type='none';
		
		%Smoothing
		Formats(5,1).type='check';
		
		%Smoothing Parameter
		Formats(5,2).type='edit';
		Formats(5,2).format='float';
		Formats(5,2).limits = [0 9999];
		
		%NumberEvents
		Formats(6,1).type='edit';
		Formats(6,1).format='integer';
		Formats(6,1).limits = [0 9999];
	
		%Radius
		Formats(6,2).type='edit';
		Formats(6,2).format='float';
		Formats(6,2).limits = [0 9999];
		
		%Grid space x
		Formats(7,1).type='edit';
		Formats(7,1).format='float';
		Formats(7,1).limits = [0 999];
		
		%Grid space y
		Formats(7,2).type='edit';
		Formats(7,2).format='float';
		Formats(7,2).limits = [0 999];
		
		%Min Number
		Formats(8,1).type='edit';
		Formats(8,1).format='float';
		Formats(8,1).limits = [0 9999];
		Formats(8,1).size = [-1 0];
		Formats(8,2).limits = [0 1];
		Formats(8,2).type='none';
		
		%Fixed Mc
		Formats(9,1).type='edit';
		Formats(9,1).format='float';
		Formats(9,1).limits = [-99 99];
		Formats(9,1).size = [-1 0];
		Formats(9,2).limits = [0 1];
		Formats(9,2).type='none';
		
		%Mc Correction
		Formats(10,1).type='edit';
		Formats(10,1).format='float';
		Formats(10,1).limits = [-99 99];
		Formats(10,1).size = [-1 0];
		Formats(10,2).limits = [0 1];
		Formats(10,2).type='none';
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('inb2',inb2,...
			'bBst_button',bBst_button,...
			'nBstSample',nBstSample,...
			'SmoothMode',SmoothMode,...
			'SmoothFactor',SmoothFactor,...
			'SelMethod',SelMethod,...
			'ni',ni,...
			'ra',ra,...
			'dx',dx,...
			'dy',dy,...
			'Nmin',Nmin,...
			'fMcFix',fMcFix,...			
			'fMccorr',fMccorr);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		%Now the new parameters should be existing format them if needed
		%and save them in the datastore if needed
		
		%datastore stuff
		%build grid
		gridpars.gridSep(1)=NewParameter.dx;
		gridpars.gridSep(2)=NewParameter.dy;
		gridpars.rad=km2deg(NewParameter.ra);
		
		obj.Datastore.setUserData('gridPars',gridpars);
		
		%unpack from the structure
		inb2=NewParameter.inb2;
		bBst_button=NewParameter.bBst_button;
		nBstSample=NewParameter.nBstSample;
		SelMethod=NewParameter.SelMethod;
		SmoothMode=NewParameter.SmoothMode;
		SmoothFactor=NewParameter.SmoothFactor;
		ni=NewParameter.ni;
		ra=NewParameter.ra;
		dx=NewParameter.dx;
		dy=NewParameter.dy;
		Nmin=NewParameter.Nmin;
		fMcFix=NewParameter.fMcFix;			
		fMccorr=NewParameter.fMccorr;
		
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
		
	end
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
						
		if SelMethod==1
			tgl1=1;
			tgl2=0;
		elseif SelMethod==2
			tgl1=0;
			tgl2=1;
		end
		
		
		
		%New Calculation
		%---------------
		
		%create needed Calculation objects
		
		
		
		%use grid to calculate the needed results
		
		
		
		%post process them if needed
		
		
		
		
		
		
		
		
		if strcmp('ca', selector)
		    %map = findobj('Name','Seismicity Map');
		    
		    
		    
		   
		    
		    
		    % Plot all grid points
		    %plot(newgri(:,1),newgri(:,2),'+k','era','back')
		    
		    %welcome(' ','Running... ');think
		    %  make grid, calculate start- endtime etc.  ...
		    % 
		    t0b = a(1,3)  ;
		    n = length(a(:,1));
		    teb = a(n,3) ;
		    tdiff = round((teb - t0b)*365/par1);
		    loc = zeros(3,length(gx)*length(gy));
		    
		    % loop over  all points
		    % 
		    bvg = [];
		    allcount = 0.;
		    
		    
		    % Overall b-value
		    [bv magco stan av me mer me2 pr] =  bvalca3(a,inb1,inb2);
		    
		    itotal = length(newgri(:,1));
		    bvg = zeros(itotal,14)*nan;
		    bo1 = bv; no1 = length(a(:,1));
		    
		    RawAlpha=[];
		    
		    % loop over all points
		    
		    if obj.ParallelMode
		    
		    
		    	%not working at the moment
		    	
		    	
		    	%parallel mode seems to dislike some names, don't know why
		    	selRad=ra;
		    	NumbIn=ni;
		    	minNumber=Nmin;
		    	inb1=inb1;
		    	fBinning=fBinning;
		    	fMccorr=fMccorr;
		    	nBstSample=nBstSample;
		    	bBst_button=bBst_button;
		    	
		    	import mapseis.calc.Mc_calc.*;
		    	matlabpool open;
		    	    parfor i= 1:length(newgri(:,1))
				x = newgri(i,1);y = newgri(i,2);
				%allcount = allcount + 1.;
				allcount=i;
				% calculate distance from center point and sort wrt distance
				l = sqrt(((a(:,1)-x)*cos(pi/180*y)*111).^2 + ((a(:,2)-y)*111).^2) ;
				[s,is] = sort(l);
				b = a(is(:,1),:) ;       % re-orders matrix to agree row-wise
				
				if tgl1 == 0   % take point within r 
				    l3 = l <= selRad;
				    b = a(l3,:);      % new data per grid point (b) is sorted in distanc
				    rd = selRad; 
				else  
				    % take first ni points
				    b = b(1:NumbIn,:);      % new data per grid point (b) is sorted in distance
				    l2 = sort(l); rd = l2(NumbIn);            
				end
				
				% Number of earthquakes per node
				[nX,nY] = size(b); 
				
				% Estimate the completeness and b-value
				newt2 = b;
				
				if length(b) >= minNumber  % enough events? 
				    % Added to obtain goodness-of-fit to powerlaw value
				    [magco prf Mc90 Mc95]=mcperc_ca3(b);
				    
				    [fMc] = calc_Mc(b, inb1, fBinning, fMccorr);
				    l = b(:,6) >= fMc-(fBinning/2);
				    if length(b(l,:)) >= minNumber 
					[fMeanMag, fBValue, fStd_B, fAValue] =  calc_bmemag(b(l,:), fBinning);
				    else 
					%fMc = nan; 
					fBValue = nan; fStd_B = nan; fAValue= nan; 
				    end   
				    % Set standard deviation of a-value to nan;
				    fStd_A= nan; fStd_Mc = nan;
				    
				    % Bootstrap uncertainties
				    if bBst_button == 1
					% Check Mc from original catalog
					l = b(:,6) >= fMc-(fBinning/2);
					if length(b(l,:)) >= minNumber
					    [fMc, fStd_Mc, fBValue, fStd_B, fAValue, fStd_A, vMc, mBvalue] = calc_McBboot(b, fBinning, nBstSample, inb1);
					else 
					    fMc = nan; 
					    %fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A= nan;
					end;
				    else
					% Set standard deviation of a-value to nan;
					fStd_A= nan; fStd_Mc = nan;
				    end;   
				    
			
				else % of if length(b) >= Nmin
				    fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A = nan;
				    %bv = nan; bv2 = nan; stan = nan; stan2 = nan; prf = nan; magco = nan; av = nan; av2 = nan; 
				    fStdDevB = nan; 
				    fStdDevMc = nan; 
				    prf = nan;
				    b = [ nan nan nan nan nan nan nan nan nan];
				    nX = nan;
				end;
				mab = max(b(:,6)) ; if isempty(mab)  == 1; mab = nan; end 
				
				% Result matrix
				%bvg(allcount,:)  = [ bv magco x y rd bv2 stan2 av stan prf  mab av2 fStdDevB fStdDevMc nX];
				bvg(i,:)  = [ fMc fStd_Mc x y rd fBValue fStd_B fAValue fStd_A prf mab fStdDevB fStdDevMc nX];
				
			    end  % for  newgri
			    
			    matlabpool close
		    	
		    
		    else
		    
		    	    wai = waitbar(0,' Please Wait ...  ');
		    	    set(wai,'NumberTitle','off','Name','b-value grid - percent done');;
		    	    drawnow
		    	    
			    for i= 1:length(newgri(:,1))
				x = newgri(i,1);y = newgri(i,2);
				allcount = allcount + 1.;
				
				if ~isempty(obj.Region);
					RawAlpha(i)=obj.Region.isInside([x,y]);
				end
				
				% calculate distance from center point and sort wrt distance
				l = sqrt(((a(:,1)-x)*cos(pi/180*y)*111).^2 + ((a(:,2)-y)*111).^2) ;
				[s,is] = sort(l);
				b = a(is(:,1),:) ;       % re-orders matrix to agree row-wise
				
				if tgl1 == 0   % take point within r 
				    l3 = l <= ra;
				    b = a(l3,:);      % new data per grid point (b) is sorted in distanc
				    rd = ra; 
				else  
				    % take first ni points
				    b = b(1:ni,:);      % new data per grid point (b) is sorted in distance
				    l2 = sort(l); rd = l2(ni);            
				end
				
				% Number of earthquakes per node
				[nX,nY] = size(b); 
				
				% Estimate the completeness and b-value
				newt2 = b;
				
				if length(b) >= Nmin  % enough events? 
				    % Added to obtain goodness-of-fit to powerlaw value
				    mcperc_ca3;
				    
				    [fMc] = calc_Mc(b, inb1, fBinning, fMccorr);
				    l = b(:,6) >= fMc-(fBinning/2);
				    if length(b(l,:)) >= Nmin 
					[fMeanMag, fBValue, fStd_B, fAValue] =  calc_bmemag(b(l,:), fBinning);
				    else 
					%fMc = nan; 
					fBValue = nan; fStd_B = nan; fAValue= nan; 
				    end   
				    % Set standard deviation of a-value to nan;
				    fStd_A= nan; fStd_Mc = nan;
				    
				    % Bootstrap uncertainties
				    if bBst_button == 1
					% Check Mc from original catalog
					l = b(:,6) >= fMc-(fBinning/2);
					if length(b(l,:)) >= Nmin
					    [fMc, fStd_Mc, fBValue, fStd_B, fAValue, fStd_A, vMc, mBvalue] = calc_McBboot(b, fBinning, nBstSample, inb1);
					else 
					    fMc = nan; 
					    %fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A= nan;
					end;
				    else
					% Set standard deviation of a-value to nan;
					fStd_A= nan; fStd_Mc = nan;
				    end;   
				    
			
				else % of if length(b) >= Nmin
				    fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A = nan;
				    %bv = nan; bv2 = nan; stan = nan; stan2 = nan; prf = nan; magco = nan; av = nan; av2 = nan; 
				    fStdDevB = nan; 
				    fStdDevMc = nan; 
				    prf = nan;
				    b = [ nan nan nan nan nan nan nan nan nan];
				    nX = nan;
				end;
				mab = max(b(:,6)) ; if isempty(mab)  == 1; mab = nan; end 
				
				% Result matrix
				%bvg(allcount,:)  = [ bv magco x y rd bv2 stan2 av stan prf  mab av2 fStdDevB fStdDevMc nX];
				bvg(allcount,:)  = [ fMc fStd_Mc x y rd fBValue fStd_B fAValue fStd_A prf mab fStdDevB fStdDevMc nX];
				waitbar(allcount/itotal)
			    end  % for  newgri
			    close(wai);
		   end
			
		   % [''save('' ''wholePath'' '', ''''a'''', ''''faults'''', ''''main'''', ''''mainfault'''', ''''coastline'''', ''''infstri'''', ''''well'''')''],',...
			  
			%changed the none error with the positioning of the window
		   
		    watchoff
		    
		    % plot the results 
		    % old and re3 (initially ) is the b-value matrix
		    % 
		    
		  
		    
		    % normlap2=ones(length(tmpgri(:,1)),1)*nan;
		    normlap2=ones(length(ll),1)*nan;
		    if ~isempty(RawAlpha)
		    	alphaint(ll)= RawAlpha;
		    	try
		    		obj.AlphaMap=~logical(reshape(alphaint,length(yvect),length(xvect)));
		    	catch
		    		%means the polygon is badly shaped
		    		obj.AlphaMap=[];
		    	end	
		    end
		    % Mc map
		    normlap2(ll)= bvg(:,1);
		    mMc =reshape(normlap2,length(yvect),length(xvect));
		    % Standard deviation Mc
		    normlap2(ll)= bvg(:,2);
		    mStdMc=reshape(normlap2,length(yvect),length(xvect));
		    % Radius resolution
		    normlap2(ll)= bvg(:,5);
		    r=reshape(normlap2,length(yvect),length(xvect));
		    % b-value
		    normlap2(ll)= bvg(:,6);
		    mBvalue=reshape(normlap2,length(yvect),length(xvect));
		    % Standard deviation b-value
		    normlap2(ll)= bvg(:,7);
		    mStdB=reshape(normlap2,length(yvect),length(xvect));
		    % a-value M(0)
		    normlap2(ll)= bvg(:,8);
		    mAvalue=reshape(normlap2,length(yvect),length(xvect));
		    % Standard deviation a-value
		    normlap2(ll)= bvg(:,9);
		    mStdA=reshape(normlap2,length(yvect),length(xvect));
		    % Goodness of fit to power-law map
		    normlap2(ll)= bvg(:,10);
		    Prmap=reshape(normlap2,length(yvect),length(xvect));
		    % Whatever this is
		    normlap2(ll)= bvg(:,11);
		    ro=reshape(normlap2,length(yvect),length(xvect));
		    % Additional runs
		    normlap2(ll)= bvg(:,12);
		    mStdDevB = reshape(normlap2,length(yvect),length(xvect));
		    % 
		    normlap2(ll)= bvg(:,13);
		    mStdDevMc = reshape(normlap2,length(yvect),length(xvect));
		    
		    normlap2(ll)= bvg(:,14);
		    mNumEq = reshape(normlap2,length(yvect),length(xvect));
		    
		    %apply smoothing
		    if SmoothMode==1;
		    	hFilter = fspecial('gaussian', 5, 2.5);
		    	mMc = imfilter((mMc + SmoothFactor), hFilter, 'replicate');
		    	mStdMc = imfilter((mStdMc + SmoothFactor), hFilter, 'replicate');
		    	mBvalue = imfilter((mBvalue + SmoothFactor), hFilter, 'replicate');
		    	mStdB = imfilter((mStdB + SmoothFactor), hFilter, 'replicate');
		    	mAvalue = imfilter((mAvalue + SmoothFactor), hFilter, 'replicate');
		    	mStdA = imfilter((mStdA + SmoothFactor), hFilter, 'replicate');
		    	Prmap = imfilter((Prmap + SmoothFactor), hFilter, 'replicate');
		    	mStdDevB = imfilter((mStdDevB + SmoothFactor), hFilter, 'replicate');
		    	mStdDevMc = imfilter((mStdDevMc + SmoothFactor), hFilter, 'replicate');
		    	
		    end
		    kll = ll;
		    
		    re3 = mBvalue;
		    old = re3;
		    
		    
		    
		    %update Packlist (maybe later check if this needed, as the 
		    %calculation maybe repeated
		    obj.PackList=[obj.PackList; {'normlap2';'mMc';'mStdMc';'mBvalue';'mStdB';'mAvalue';'mStdA';...
					'Prmap';'ro';'mStdDevB';'fStdDevB';'mStdDevMc';'mNumEq';'kll';...
					're3';'old';'t0b';'teb';'newt2'}];
		    
		    %Pack it again, Sam.
		    for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		    end
		    
		    % View the b-value map
		    %view_bva
		    %uiwait;
		   
		    
		end   % if selector = na
		

		

		
		
		
		
	end
	
	
	
	function plot_bval(obj,whichplot)
		%this is kind of a wrapper for view_bva
		
		%unpack
		%unpack the variables,
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		%switch re variable around and so on
		switch whichplot
			case 'mBvalue'
				re3 = mBvalue;
				lab1 ='b-value';
			case 'mStdB'
				lab1='SdtDev b-Value';
				re3 = mStdB;
			case 'mMc'
				lab1 = 'Mcomp';
				re3 = mMc;
			case 'mStdMc'
				lab1 = 'Mcomp'; 
				re3 = mStdMc;
			case 'Prmap'
				lab1 = ' % '; 
				re3 = Prmap;	
			case 'r'
				lab1='Radius in [km]';
				re3 = r;
			case 'dense'
				lab1='log(EQ per km^2)';
				re3 = log10(mNumEq./(r.^2*pi)); 
			case 'mAvalue'
				lab1='a-value';
				re3 = mAvalue; 
			case 'mStdDevB'
				lab1='standard deviation of b-value'; 
				re3 = mStdDevB; 
			case 'mStdDevMc'
				lab1='standard deviation of Mc'; 
				re3 = mStdDevMc;
			case 'mBvalueLay'
				lab1='b-value'; 
				re3 = mBvalue; 
				bOverlayTransparentStdDev = 1;
		end	
				
		% This .m file "view_maxz.m" plots the maxz LTA values calculated 
		% with maxzlta.m or other similar values as a color map 
		% needs re3, gx, gy, stri
		%
		% define size of the plot etc. 
		% 
		
		if exist('Prmap')  == 0 
		    Prmap = re3*nan;
		end
		if isempty(Prmap) >  0 
		    Prmap = re3*nan;
		end
		
		if isempty(name) >  0 
		    name = '  '
		end
		think
		disp('This is /src/view_bva.m')
		co = 'w';
		
		
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('b-value-map',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		    bmap = figure( ...
			'Name','b-value-map',...
			'NumberTitle','off', ...
			'MenuBar','none', ...
			'NextPlot','new', ...
			'backingstore','on',...
			'Visible','off', ...
			'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
		    % make menu bar
		    matdraw
		    
		    %save bmap into he structur
		    obj.PackList=[obj.PackList; {'bmap';'cal9';'ploeq';'re4';'tresh'}];
		    %obj.
		    
		    lab1 = 'b-value:';
		    
		    symbolmenu = uimenu('Label',' Symbol ');
		    SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
		    TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
		    ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
		    
		    uimenu(SizeMenu,'Label','3','Callback',@(s,e) obj.universalCalc('ms6 =3;eval(cal9)'));
		    uimenu(SizeMenu,'Label','6','Callback',@(s,e) obj.universalCalc('ms6 =6;eval(cal9)'));
		    uimenu(SizeMenu,'Label','9','Callback',@(s,e) obj.universalCalc('ms6 =9;eval(cal9)'));
		    uimenu(SizeMenu,'Label','12','Callback',@(s,e) obj.universalCalc('ms6 =12;eval(cal9)'));
		    uimenu(SizeMenu,'Label','14','Callback',@(s,e) obj.universalCalc('ms6 =14;eval(cal9)'));
		    uimenu(SizeMenu,'Label','18','Callback',@(s,e) obj.universalCalc('ms6 =18;eval(cal9)'));
		    uimenu(SizeMenu,'Label','24','Callback',@(s,e) obj.universalCalc('ms6 =24;eval(cal9)'));
		    
		    uimenu(TypeMenu,'Label','dot','Callback',@(s,e) obj.universalCalc('ty =''.'';eval(cal9)'));
		    uimenu(TypeMenu,'Label','+','Callback',@(s,e) obj.universalCalc('ty=''+'';eval(cal9)'));
		    uimenu(TypeMenu,'Label','o','Callback',@(s,e) obj.universalCalc('ty=''o'';eval(cal9)'));
		    uimenu(TypeMenu,'Label','x','Callback',@(s,e) obj.universalCalc('ty=''x'';eval(cal9)'));
		    uimenu(TypeMenu,'Label','*','Callback',@(s,e) obj.universalCalc('ty=''*'';eval(cal9)'));
		    uimenu(TypeMenu,'Label','none','Callback',@(s,e) obj.universalCalc('vi=''off'';set(ploeq,''visible'',''off''); '));
		    
		    uimenu(ColorMenu,'Label','black','Callback',@(s,e) obj.universalCalc('co=''k'';eval(cal9)'));
		    uimenu(ColorMenu,'Label','white','Callback',@(s,e) obj.universalCalc('co=''w'';eval(cal9)'));
		    uimenu(ColorMenu,'Label','white','Callback',@(s,e) obj.universalCalc('co=''r'';eval(cal9)'));
		    uimenu(ColorMenu,'Label','yellow','Callback',@(s,e) obj.universalCalc('co=''y'';eval(cal9)'));
		    
		    cal9 = ...
			[ 'vi=''on'';set(ploeq,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];
		    
		    
		    %   uicontrol('BackGroundColor','w','Units','normal',... 
		    %      'Position',[.1 .23 .08 .06],'String','Info ',...
		    %      'callback',' web([''file:'' hodi ''/zmapwww/chp11.htm#996756'']) '); 
		    
		    
		    
		    options = uimenu('Label',' Select ');
		    uimenu('Parent',options,'Label','Refresh ',...
			'callback',@(s,e) obj.plot_bval(whichplot))
		    uimenu('Parent',options,'Label','Calculate again ','Separator','on',...
		    	'callback',@(s,e) obj.CalcAgain);
		    uimenu('Parent',options,'Label','Write to datastore ',...
			'callback',@(s,e) obj.saveCalc);
		    uimenu(options,'Label','Select EQ in Circle','Separator','on',...
			'callback',@(s,e) obj.universalCalc('h1 = gca;met = ''ni''; ho=''noho'';cirbva;watchoff(bmap)',{'met';'ho'}))
		    uimenu(options,'Label','Select EQ in Circle - Constant R',...
			'callback',@(s,e) obj.universalCalc('h1 = gca;met = ''ra''; ho=''noho'';cirbva;watchoff(bmap)',{'met';'ho'}))
		    uimenu(options,'Label','Select EQ in Circle - Overlay existing plot',...
			'callback',@(s,e) obj.universalCalc('h1 = gca;ho = ''hold'';cirbva;watchoff(bmap)',{'ho'}))
		    
		    uimenu(options,'Label','Select EQ in Polygon -new ',...
			'callback',@(s,e) obj.universalCalc('h1 = gca;cufi = gcf;ho = ''noho'';selectp2'))  
		    uimenu(options,'Label','Select EQ in Polygon - hold ',...
			'callback',@(s,e) obj.universalCalc('h1 = gca;cufi = gcf;ho = ''hold'';selectp2'))  
		    
		    
		    op1 = uimenu('Label',' Maps ');
		    
		    adjmenu =  uimenu(op1,'Label','Adjust Map Display Parameters'),...
			uimenu(adjmenu,'Label','Adjust Mmin cut',...
			'callback',@(s,e) obj.adju('mag', whichplot))
			uimenu(adjmenu,'Label','Adjust Rmax cut',...
			    'callback',@(s,e) obj.adju('rmax', whichplot))
			uimenu(adjmenu,'Label','Adjust goodness of fit cut',...
			    'callback',@(s,e) obj.adju('gofi', whichplot))
		    
		    
		    uimenu('Parent',op1,'Label','b-value map (max likelihood)',...
			'callback',@(s,e) obj.plot_bval('mBvalue'))
		    uimenu('Parent',op1,'Label','Standard deviation of b-Value (max likelihood) map',...
			'callback',@(s,e) obj.plot_bval('mStdB'))
		    uimenu('Parent',op1,'Label','Magnitude of completness map',...
			'callback',@(s,e) obj.plot_bval('mMc'))
		    uimenu('Parent',op1,'Label','Standard deviation of magnitude of completness',...
			'callback',@(s,e) obj.plot_bval('mStdMc'))
		    uimenu('Parent',op1,'Label','Goodness of fit to power law map',...
			'callback',@(s,e) obj.plot_bval('Prmap'))
		    uimenu('Parent',op1,'Label','Resolution map',...
			'callback',@(s,e) obj.plot_bval('r'))
		    uimenu('Parent',op1,'Label','Earthquake density map',...
			'callback',@(s,e) obj.plot_bval('dense'))
		    uimenu('Parent',op1,'Label','a-value map',...
			'callback',@(s,e) obj.plot_bval('mAvalue'))
		  
		    
		    if exist('mStdDevB')
			AverageStdDevMenu = uimenu(op1,'Label', 'Additional random simulation');
			uimenu(AverageStdDevMenu,'Label', 'Bootstrapped standard deviation of b-value',...
			    'callback',@(s,e) obj.plot_bval('mStdDevB'))
			uimenu(AverageStdDevMenu,'Label', 'Bootstrapped standard deviation of Mc',...
			    'callback',@(s,e)obj.plot_bval('mStdDevMc'))
			uimenu(AverageStdDevMenu,'Label', 'b-value map (max likelihood) with std. deviation',...
			    'callback',@(s,e)obj.plot_bval('mBvalueLay'))
		    end;
		    
		    
		    makert = ...
			['def = {''6''};m = inputdlg(''Magnitude of projected mainshock?'',''Input'',1,def);',...
			    'm1 = m{:}; m = str2num(m1);',...
			    'lab1 = ''Tr (yrs) (sm. values only)'';',...
			    're3 =(teb - t0b)./(10.^(mAvalue-m*mBvalue)); mrt = m; view_bva'];
		    
		    
		    makertp = ...
			['def = {''6''};m = inputdlg(''Magnitude of projected mainshock?'',''Input'',1,def);',...
			    'm1 = m{:}; m = str2num(m1);',...
			    'lab1 = ''1/Tr/area '';',...
			    're3 =(teb - t0b)./(10.^(mAvalue-m*mBvalue)); re3 = 1./re3/(2*pi*ra*ra);  mrt = m; view_bva'];
		    
		    
		    recmenu =  uimenu(op1,'Label','recurrence time map '),...
			
		    uimenu(recmenu,'Label','recurrence time map ',...
			'callback',@(s,e) obj.universalCalc(makert))
		    
		    uimenu(recmenu,'Label','(1/Tr)/area map ',...
			'callback',@(s,e) obj.universalCalc(makertp))
		    
		    uimenu(recmenu,'Label','recurrence time percentage ',...
			'callback',@(s,e) obj.universalCalc('recperc'))
		    
		    
		    
		    uimenu(op1,'Label','Histogram ','callback','zhist')
		    uimenu(op1,'Label','Reccurrence Time Histogram ','callback','rechist')
		    uimenu(op1,'Label','Save map to ASCII file ','callback',...
		    		@(s,e) obj.universalCalc('savemap'))
		    
		    op2e = uimenu('Label',' Display ');
		    uimenu(op2e,'Label','Fix color (z) scale','callback',@(s,e) obj.universalCalc('fixax2 '))
		    uimenu(op2e,'Label','Plot Map in lambert projection using m_map ','callback',...
		    			@(s,e) obj.universalCalc('h1 = gca; plotmap '))
		    uimenu(op2e,'Label','Plot map on top of topography (white background)',...
			'callback',@(s,e) obj.universalCalc('colback = 1; dramap2_z'))
		    uimenu(op2e,'Label','Plot map on top of topography (black background)',...
			'callback',@(s,e) obj.universalCalc('colback = 2; dramap2_z'))
		    uimenu(op2e,'Label','Show Grid ',...
			'callback',@(s,e) obj.plot_modify([],[],'grid',[]))
		    uimenu(op2e,'Label','Show Circles ','callback',@(s,e) obj.plot_modify([],[],'circle',[]))
		    uimenu(op2e,'Label','Colormap InvertGray',...
			'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
		    uimenu(op2e,'Label','Colormap Invertjet',...
			'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
		    uimenu(op2e,'Label','shading flat',...
			'callback',@(s,e) obj.plot_modify([],'flat',[],[]))
		    uimenu(op2e,'Label','shading interpolated',...
			'callback',@(s,e) obj.plot_modify([],'interp',[],[]))
		    uimenu(op2e,'Label','Brigten +0.4',...
			'callback',@(s,e) obj.plot_modify(0.4,[],[],[]))
		    uimenu(op2e,'Label','Brigten -0.4',...
			'callback',@(s,e) obj.plot_modify(-0.4,[],[],[]))
		    uimenu(op2e,'Label','Redraw Overlay',...
			'callback',@(s,e) obj.plot_modify([],[],[],1))
		    
		    tresh = nan; re4 = re3;
		    
		    colormap(jet)
		    tresh = nan; minpe = nan; Mmin = nan;    
		    
		end   % This is the end of the figure setup
		
		% Now lets plot the color-map of the z-value
		%
		figure(bmap)
		delete(gca)
		delete(gca)
		delete(gca)
		dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
		reset(gca)
		cla
		hold off
		watchon;
		set(gca,'visible','off','FontSize',fs10,'FontWeight','normal',...
		    'FontWeight','normal','LineWidth',[1.],...
		    'Box','on','drawmode','fast')
		
		rect = [0.18,  0.10, 0.7, 0.75];
		rect1 = rect;
		
		% find max and min of data for automatic scaling
		% 
		maxc = max(max(re3));
		maxc = fix(maxc)+1;
		minc = min(min(re3));
		minc = fix(minc)-1;
		
		% plot image
		% 
		orient landscape
		%set(gcf,'PaperPosition', [0.5 1 9.0 4.0])
		
		axes('position',rect)
		hold on
		
		if ~isempty(obj.AlphaMap)
			re3(obj.AlphaMap)=NaN;
		end
		
		pco1 = pcolor(gx,gy,re3);
		%pco1 = image(gx,gy,re3);
		%set(pco1,'CDataMapping','scaled');
		
		
		axis([ min(gx) max(gx) min(gy) max(gy)])
		set(gca,'dataaspect',[1 cos(pi/180*nanmean(a(:,2))) 1]);
		hold on
		if sha == 'fl'
		    shading flat
		else
		    shading interp
		end
		% make the scaling for the recurrence time map reasonable
		if lab1(1) =='T'
		    l = isnan(re3);
		    re = re3;
		    re(l) = [];
		    caxis([min(re) 5*min(re)]);
		end
		if fre == 1
		    caxis([fix1 fix2])
		end
		
		title2([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
		    'Color','r','FontWeight','normal')
		
		xlabel2('Longitude [deg]','FontWeight','normal','FontSize',fs10)
		ylabel2('Latitude [deg]','FontWeight','normal','FontSize',fs10)
		
		% plot overlay
		% 
		hold on
		overlay_
		ploeq = plot(a(:,1),a(:,2),'k.');
		set(ploeq,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)
		
		
		set(gca,'visible','on','FontSize',fs10,'FontWeight','normal',...
		    'FontWeight','normal','LineWidth',[1.],...
		    'Box','on','TickDir','out')
		
		h1 = gca;
		hzma = gca;
		 %save bmap into he structur
		    obj.PackList=[obj.PackList; {'hzma'}];
		
		
		% Create a colorbar
		%
		h5 = colorbar('horiz');
		set(h5,'Pos',[0.35 0.07 0.4 0.02],...
		    'FontWeight','normal','FontSize',fs10,'TickDir','out')
		
		rect = [0.00,  0.0, 1 1];
		axes('position',rect)
		axis('off')
		%  Text Object Creation 
		txt1 = text(... 
		    'Color',[ 0 0 0 ],... 
		    'EraseMode','normal',... 
		    'Units','normalized',...
		    'Position',[ 0.2 0.06 0 ],...
		    'HorizontalAlignment','right',...
		    'Rotation',[ 0 ],...
		    'FontSize',fs10,.... 
		    'FontWeight','normal',...
		    'String',lab1); 
		
		% Make the figure visible
		% 
		set(gca,'FontSize',fs10,'FontWeight','normal',...
		    'FontWeight','normal','LineWidth',[1.],...
		    'Box','on','TickDir','out')
		figure(bmap);
		%sizmap = signatur('ZMAP','',[0.01 0.04]);
		%set(sizmap,'Color','k')
		axes(h1)
		set(gcf,'color','w');
		watchoff(bmap)
		%whitebg(gcf,[ 0 0 0 ])
		done
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
	
	end
	
	function plot_modify(obj,bright,interswitch,circleGrid,overlay)
		%modifys the graphic
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		if ~isempty(bright)
			axes(hzma);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			obj.PackList=[obj.PackList; {'sha'}];
			switch interswitch
				case 'flat'
					axes(hzma);
					shading flat;
					sha='fl';
				case 'interp'
					axes(hzma);
					shading interp;
					sha='in';
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		if ~isempty(overlay)
			hold on;
			overlay;
			
		end
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end
	end
	
	
	function adju(obj,asel, whichplot)
		%function version of adju
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;


		prompt={'Enter the minimum magnitude cut-off','Enter the maximum radius cut-off:','Enter the minimum goodness of fit percatge'};
		def={'nan','nan','nan'};
		dlgTitle='Input Map subselection Criteria';
		lineNo=1;
		answer=inputdlg(prompt,dlgTitle,lineNo,def);
		re4 = re3;
		l = answer{1,1}; Mmin = str2num(l) ;
		l = answer{2,1}; tresh = str2num(l) ;
		l = answer{3,1}; minpe = str2num(l) ;
		
		
		obj.PackList=[obj.PackList; {'re4';'l';'asel'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

		%back to plot
		obj.plot_bval(whichplot);
		
	end
	
	function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	
	
	function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		
		
		%get parameters
		obj.openCalcParamWindow;
		
		if obj.StartCalc
			
			figNr=findobj('Name','b-value-map');
			
			if ~isempty(figNr)
				close(figNr);
			end
		
			%refresh catalogs
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%Build the grid with new parameters if needed
			obj.Gridit(false);
			
			%Calculation
			obj.doTheCalcThing;
			
			%Plotting
			obj.plot_bval('mBvalue');
		end
	
	
	end
	
	
	function saveCalc(obj)
		%This will save the calculation in the datastore and ask to open it next time
		%The filterlist will be saved within the datastore with this and reopened next time
		%when the calculation is loaded. It has to be done in this way or else nobody knows 
		%what filtersetting was used in the calculation.
		
		%Ask for a name
		gParams = inputdlg('Calculation Name','Name of the Calculation',1,{obj.CalcName});
		if ~isempty(gParams)
			obj.CalcName=gParams{1};
			
			%Maybe the Filterlist needs to be cloned, but for now try without
			obj.Filterlist.PackIt;
			
			CalcObject={obj.CalcName,obj.ZmapVariables,obj.Filterlist};
			
			%try getting the CalcObject from the datastore
			
			try 
				Calculations=obj.Datastore.getUserData('zmap-bval-calc');
				
			catch
				Calculations={};
			end
			
			if ~isempty(Calculations)
				%try finding the calc
					founded=strcmp(Calculations(:,1),obj.CalcName);
					if sum(founded)>0
						Calculations(founded,:)=CalcObject;
					else
						Calculations(end+1,:)=CalcObject;
					end	
			else
				Calculations(1,:)=CalcObject;
			end	
			
			obj.Datastore.setUserData('zmap-bval-calc',Calculations);
			
		end
		
		
		
		%write to the Commander (always do this even if canceled, this will update
		%the buffers)
		obj.Commander.pushWithKey(obj.Keys.ShownDataID,obj.Datastore);
		obj.Commander.switcher('Cata');
		
	end
	
	
	
	function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne';...
			'Overwrite FilterList?','OverWriteFilter'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%Overwrite
		Formats(2,2).type='list';
		Formats(2,2).style='togglebutton';
		Formats(2,2).items={'Yes','No'};
		Formats(2,2).size = [-1 0];
		
		Formats(2,3).limits = [0 1];
		Formats(2,3).type='none';
		Formats(2,1).limits = [0 -1];
		Formats(2,1).type='none';
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1,...
				'OverWriteFilter',2);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		if obj.StartCalc
			TheCalc=oldCalcs(NewParameter.WhichOne,:);
			
			%Overwrite Filter 
			if NewParameter.OverWriteFilter==1
				Keys=obj.Commander.getMarkedKey;
				FilterKey=Keys.ShownFilterID;
				
				obj.Commander.pushWithKey(TheCalc{3},FilterKey);
				obj.Commander.switcher('Filter');
				
			else %Create new filter
				newdata.Filterlist=TheCalc{3};
				obj.Commander.pushIt(newdata);
				obj.Commander.switcher('Filter');
			end
			
			%Now set the data
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			obj.ZmapVariables=TheCalc{2};
			
			%add missing variables if needed
			if ~isfield(obj.ZmapVariables,'file1')
				obj.ZmapVariables.file1=[];
			end
			if ~isfield(obj.ZmapVariables,'h1')
				obj.ZmapVariables.h1=[];
			end
			if ~isfield(obj.ZmapVariables,'maepi')
				obj.ZmapVariables.maepi=[];
			end
			obj.PackList=fields(obj.ZmapVariables);
			
			obj.CalcName=TheCalc{1};
			
			%try rebuild the Alphamap
			regionFilter = obj.Filterlist.getByName('Region');
			regionParms = regionFilter.getRegion();
			obj.AlphaMap=[];
			
			if ~strcmp(regionParms{1},'all')
				theRegion = regionParms{2};
				obj.Region=theRegion;
				
				RawAlpha=[];
				newgri=obj.ZmapVariables.newgri;
				ll=obj.ZmapVariables.ll;
				yvect=obj.ZmapVariables.yvect;
				xvect=obj.ZmapVariables.xvect;
				disp('Please wait, Alphamap is rebuild')
				 %for i= 1:length(newgri(:,1))
				 
				 	x = newgri(:,1);y = newgri(:,2);
				 	RawAlpha=obj.Region.isInside([x,y]);
				 %end
				 normlap2=ones(length(ll),1)*nan;
				 if ~isempty(RawAlpha)
				 	
				 	alphaint(ll) = RawAlpha;
				 	try
				 		obj.AlphaMap=~logical(reshape(alphaint,length(yvect),length(xvect)));
				 	catch
				 		%means the polygon is badly shaped
				 		obj.AlphaMap=[];
				 		disp('could not restore AlphaMap')
				 	end	
				 end
			else
				obj.Region=[];
				obj.AlphaMap=[];
			end
			
			
			
			
		end
		
	end
	
	
end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	
	%THE FUNCTIONS UNDERNEATH ARE NOT ALWAYS DIRECTLY USEABLE, AS THEY OFTEN NEED DIRECT ACCESS
	%TO THE VARIABLE, SO JUST COPY IF NEEDED.
	function TheEvalString = UnpackStruct(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		TheEvalString=[];
		
		for i=1:numel(thefields)
			
			TheEvalString=[TheEvalString; thefields{i},'=' num2str(TheStruct.(thefields{i})),';'];
		end
		 %can be done easier directly in the function which should be calling this, it is more sort of 
		 %a sketch
		
		
	
	end
	
	
	function CellData = UnpackStruct2Cell(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		
		
		for i=1:numel(thefields)
			
			CellData{i,1}=thefields{i}
			CellData{i,2}=TheStruct.(thefields{i});
		end
		
		%assignin can be used to set the variable on the string.
	end
	
	function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itselectorf
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end
	
	
	function TheStruct = PackAllVariable()
		%The opposite of UnPack, it takes every variable in the current Workspace an packs it 
		%into a structure
		
		varilist=who;
		
		for i=1:numel(varilist)
			TheStruct.(varilist{i})=eval(varilist{i});
		
		end
	
	
	end
	
	%---------------
end
	
	

end
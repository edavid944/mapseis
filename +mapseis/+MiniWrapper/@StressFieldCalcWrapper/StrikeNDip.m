function [s1_strike s1_dip s2_strike s2_dip s3_strike s3_dip] =StrikeNDip(eigenVectors)
		%calculates the strike and dip of the three eigenvectors
		%can be used in cellfun command
		
		%and how to do it in a effectiv way?
			%---> principal vector = (p1,p2,p3)
			%---> strike = arccos(p2/sqrt(p1^2+p2^2)) 
			%---> dip = acos((p1^2+p2^2)/(sqrt(p1^2+p2^2)*sqrt(p1^2+p2^2+p3^2)))
			%		maybe this can be simplified
			%---> value = eigenvalue?
			%This whole transformation has to be tested, I'm not fully convinced it works 
			
		princ1=eigenVectors(:,1);
		princ2=eigenVectors(:,2);
		princ3=eigenVectors(:,3);
		
		s1_strike=rad2deg(acos(princ1(2)/sqrt(princ1(2)^2+princ1(1)^2)));
		s2_strike=rad2deg(acos(princ2(2)/sqrt(princ2(2)^2+princ2(1)^2)));
		s3_strike=rad2deg(acos(princ3(2)/sqrt(princ3(2)^2+princ3(1)^2)));
			
		s1_dip=rad2deg(acos((princ1(1)^2+princ1(2)^2)/...
			(sqrt(princ1(1)^2+princ1(2)^2)*sqrt(princ1(1)^2+princ1(2)^2+princ1(3)^2))));
		s2_dip=rad2deg(acos((princ2(1)^2+princ2(2)^2)/...
			(sqrt(princ2(1)^2+princ2(2)^2)*sqrt(princ2(1)^2+princ2(2)^2+princ2(3)^2))));
		s3_dip=rad2deg(acos((princ3(1)^2+princ3(2)^2)/...
			(sqrt(princ3(1)^2+princ3(2)^2)*sqrt(princ3(1)^2+princ3(2)^2+princ3(3)^2))));	
	
			
	
end
function StressTensors = FormTensor(S11,S12,S13,S22,S23,S33)
		
		RawArray=[S11,S12,S13,S12,S22,S23,S13,S23,S33];
		
		StressTensors=reshape(RawArray',numel(S11),3,3);
		
		%this would pack the matrices into single cells 
		%num2cell(StressTensors,[2 3]);
end
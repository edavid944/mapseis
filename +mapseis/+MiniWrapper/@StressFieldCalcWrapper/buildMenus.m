function buildMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
		
		if ~obj.ProfileSwitch
			StrikeMapping='quiver';
			DipMapping='colormap';
		else
			StrikeMapping='colormap';
			DipMapping='quiver';
		end
		
		
		
		%first the general menus (similar for both)
		 OptionMenu = uimenu( obj.ResultGUI,'Label','- Load/Save');
		 		uimenu(OptionMenu,'Label','Calculate again',...
                'Callback',@(s,e) obj.CalcAgain);
                uimenu(OptionMenu,'Label','Write to Datastore',...
                'Callback',@(s,e) obj.saveCalc);
                uimenu(OptionMenu,'Label','Load from Datastore',...
                'Callback',@(s,e) obj.loadCalc([]));
		
		
                %Plot option menu, allows to selected plot options like plotting earthquakes
                %coast- & borderlines
                obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
			 	uimenu(obj.PlotOptions,'Label','plot Coastlines',...
                'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
                 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
                'Callback',@(s,e) obj.TooglePlotOptions('Border'));
                 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
                'Callback',@(s,e) obj.TooglePlotOptions('EQ'));
                
                 uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
                'Callback',@(s,e) obj.setqual('low'));
                uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
                'Callback',@(s,e) obj.setqual('med'));
                uimenu(obj.PlotOptions,'Label','High Quality Plot',...
                'Callback',@(s,e) obj.setqual('hi'));
                
              %  uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
              %  'Callback',@(s,e) obj.plot_modify([],'interp',[]));
              %  uimenu(obj.PlotOptions,'Label','Shading Flat',... 
              %  'Callback',@(s,e) obj.plot_modify([],'flat',[]));
                uimenu(obj.PlotOptions,'Label','Brighten +0.4','Separator','on',... 
                'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
                 uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
                'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
                
                %set the checks right
                obj.TooglePlotOptions('-chk');
                obj.setqual('chk');
                
                %now the type depended map menu
                
		if obj.CalcParameter.Calc_Method==1|obj.CalcParameter.Calc_Method==3
			%conventional Michael
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','S2/S1 relative size',...
			'Callback',@(s,e) obj.plotVariable([],'fPhi','colormap'));
			 uimenu(MapMenu,'Label','S1 strike',...
			'Callback',@(s,e) obj.plotVariable([],'fS1Trend',StrikeMapping));
			 uimenu(MapMenu,'Label','S1 dip',...
			'Callback',@(s,e) obj.plotVariable([],'fS1Plunge',DipMapping));
			uimenu(MapMenu,'Label','S2 strike',...
			'Callback',@(s,e) obj.plotVariable([],'fS2Trend',StrikeMapping));
			 uimenu(MapMenu,'Label','S2 dip',...
			'Callback',@(s,e) obj.plotVariable([],'fS2Plunge',DipMapping));
			uimenu(MapMenu,'Label','S3 strike',...
			'Callback',@(s,e) obj.plotVariable([],'fS3Trend',StrikeMapping));
			uimenu(MapMenu,'Label','S3 dip',...
			'Callback',@(s,e) obj.plotVariable([],'fS3Plunge',DipMapping));
			uimenu(MapMenu,'Label','Focal mechanism diversity',...
			'Callback',@(s,e) obj.plotVariable([],'fRms','colormap'));
			uimenu(MapMenu,'Label','Faulting Style',...
			'Callback',@(s,e) obj.plotVariable([],'fAphi','colormap'));
			uimenu(MapMenu,'Label','Earthquake density',...
			'Callback',@(s,e) obj.plotVariable([],'dCount','colormap'));
			
			
			TensorMenu = uimenu( MapMenu,'Label','Stress Tensor');
			uimenu(TensorMenu,'Label','S11',...
			'Callback',@(s,e) obj.plotVariable([],'fS11','colormap'));
			uimenu(TensorMenu,'Label','S12',...
			'Callback',@(s,e) obj.plotVariable([],'fS12','colormap'));
			uimenu(TensorMenu,'Label','S13',...
			'Callback',@(s,e) obj.plotVariable([],'fS13','colormap'));
			uimenu(TensorMenu,'Label','S22',...
			'Callback',@(s,e) obj.plotVariable([],'fS22','colormap'));
			uimenu(TensorMenu,'Label','S23',...
			'Callback',@(s,e) obj.plotVariable([],'fS23','colormap'));
			uimenu(TensorMenu,'Label','S33',...
			'Callback',@(s,e) obj.plotVariable([],'fS33','colormap'));
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));
			uimenu(MapMenu,'Label','Smoothing Factor',...
			'Callback',@(s,e) obj.Smoother('factor'));
			
			
			%additional entries for load/save
			uimenu(OptionMenu,'Label','export Stress Field (matlab)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu(OptionMenu,'Label','export Stress Field (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu(OptionMenu,'Label','export Stress Field (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu(OptionMenu,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
			
		elseif 	obj.CalcParameter.Calc_Method==2
			%Satsi
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','S2/S1 relative size',...
			'Callback',@(s,e) obj.plotVariable([],'S2_S1_rel','colormap'));
			 uimenu(MapMenu,'Label','S1 strike',...
			'Callback',@(s,e) obj.plotVariable([],'s1_strike',StrikeMapping));
			 uimenu(MapMenu,'Label','S1 dip',...
			'Callback',@(s,e) obj.plotVariable([],'s1_dip',DipMapping));
			uimenu(MapMenu,'Label','S2 strike',...
			'Callback',@(s,e) obj.plotVariable([],'s2_strike',StrikeMapping));
			 uimenu(MapMenu,'Label','S2 dip',...
			'Callback',@(s,e) obj.plotVariable([],'s2_dip',DipMapping));
			uimenu(MapMenu,'Label','S3 strike',...
			'Callback',@(s,e) obj.plotVariable([],'s3_strike',StrikeMapping));
			uimenu(MapMenu,'Label','S3 dip',...
			'Callback',@(s,e) obj.plotVariable([],'s3_dip',DipMapping));
			uimenu(MapMenu,'Label','Focal mechanism diversity',...
			'Callback',@(s,e) obj.plotVariable([],'Rms','colormap'));
			uimenu(MapMenu,'Label','Faulting Style',...
			'Callback',@(s,e) obj.plotVariable([],'FaultStyle','colormap'));
		
						
			TensorMenu = uimenu( MapMenu,'Label','Stress Tensor');
			uimenu(TensorMenu,'Label','S11',...
			'Callback',@(s,e) obj.plotVariable([],'See','colormap'));
			uimenu(TensorMenu,'Label','S12',...
			'Callback',@(s,e) obj.plotVariable([],'Sen','colormap'));
			uimenu(TensorMenu,'Label','S13',...
			'Callback',@(s,e) obj.plotVariable([],'Seu','colormap'));
			uimenu(TensorMenu,'Label','S22',...
			'Callback',@(s,e) obj.plotVariable([],'Snn','colormap'));
			uimenu(TensorMenu,'Label','S23',...
			'Callback',@(s,e) obj.plotVariable([],'Snu','colormap'));
			uimenu(TensorMenu,'Label','S33',...
			'Callback',@(s,e) obj.plotVariable([],'Suu','colormap'));
			
			
			%additional entries for load/save
			uimenu(OptionMenu,'Label','export Full Result','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
		end	
		
		
		obj.Smoother('init');
		
		
end
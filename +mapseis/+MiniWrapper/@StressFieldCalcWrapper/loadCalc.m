function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		if isempty(oldCalcs)
			%check if old calculations are avialable
			try
				oldCalcs=obj.Datastore.getUserData('StressInversion-v1');
			catch
				disp('no calculation available in this datastore')
				return;
			end
		end
		
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne';...
			'Overwrite FilterList?','OverWriteFilter'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%Overwrite
		Formats(2,2).type='list';
		Formats(2,2).style='togglebutton';
		Formats(2,2).items={'Yes','No'};
		Formats(2,2).size = [-1 0];
		
		Formats(2,3).limits = [0 1];
		Formats(2,3).type='none';
		Formats(2,1).limits = [0 -1];
		Formats(2,1).type='none';
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1,...
				'OverWriteFilter',2);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
			obj.Loaded=false;
		else
			obj.StartCalc=true;
			obj.Loaded=true;
		end
		
		if obj.StartCalc
			TheCalc=oldCalcs(NewParameter.WhichOne,:);
			
			%Overwrite Filter 
			if NewParameter.OverWriteFilter==1
				Keys=obj.Commander.getMarkedKey;
				FilterKey=Keys.ShownFilterID;
				
				obj.Commander.pushWithKey(TheCalc{4},FilterKey);
				obj.Commander.switcher('Filter');
				
			else %Create new filter
				newdata.Filterlist=TheCalc{4};
				obj.Commander.pushIt(newdata);
				obj.Commander.switcher('Filter');
			end
			
			
			%Now set the data
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.CalcParameter=TheCalc{2};
			obj.ProfileSwitch=obj.CalcParameter.ProfileSwitch;
			obj.CalcResult=TheCalc{3};
			
			
			%check if smoothing parameters are included, if not add them
			if ~isfield(obj.CalcParameter,'SmoothKernelSize');
				obj.CalcParameter.SmoothKernelSize=3;
			end
			
			if ~isfield(obj.CalcParameter,'SmoothSigma');
				obj.CalcParameter.SmoothSigma=1;
			end
			
			
		end
		
end
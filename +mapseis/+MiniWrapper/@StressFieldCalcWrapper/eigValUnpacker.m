function [s1_value s2_value s3_value] = eigValUnpacker(eigVal)
		%simple function, gets the eigenvalues from the stupid matrix
		%used in a cellfun
		
		s1_value=eigVal(1,1);
		s2_value=eigVal(2,2);
		s3_value=eigVal(3,3);
		
end
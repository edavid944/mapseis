function plotResult(obj,whichplot)
		%more of a relict dummy function 
		%it allows a bit less complicated plotting, more for scripting
		%it calls plotVariable
		
		if ~obj.ProfileSwitch
			StrikeMapping='quiver';
			DipMapping='colormap';
		else
			StrikeMapping='colormap';
			DipMapping='quiver';
		end
		
		
		
		if obj.CalcParameter.Calc_Method==1|obj.CalcParameter.Calc_Method==3
			switch whichplot
				case 'stress'
					%just plot phi (s2/s1 relation)
					obj.plotVariable([],'fPhi','colormap');
					
				case 'Diversity'
					obj.plotVariable([],'fRms','colormap');	
				
				case 'Faulting'
					obj.plotVariable([],'fAphi','colormap');
				
				case 'S1dir'	
					obj.plotVariable([],'fS1Trend',StrikeMapping);
				
				case 'S2dir'	
					obj.plotVariable([],'fS2Trend',StrikeMapping);
				
				case 'S3dir'	
					obj.plotVariable([],'fS3Trend',StrikeMapping);
					
			end
			%more later
			
		
		elseif obj.CalcParameter.Calc_Method==2
			switch whichplot
				case 'stress'
					%just plot phi (s2/s1 relation)
					obj.plotVariable([],'S2_S1_rel','colormap');
					
				case 'Diversity'
					obj.plotVariable([],'Rms','colormap');	
				
				case 'Faulting'
					obj.plotVariable([],'FaultStyle','colormap');
				
				case 'S1dir'	
					obj.plotVariable([],'s1_strike',StrikeMapping);
					
				case 'S2dir'	
					obj.plotVariable([],'s2_strike',StrikeMapping);
					
				case 'S3dir'	
					obj.plotVariable([],'s3_strike',StrikeMapping);
						
			end
			%more later
		
		
		
		end
	
end
function plotVariable(obj,plotAxis,ValField,PlotType)
		%will plot the selected field of the CalcRes structur into the selected plotaxis
		%the PlotType can either be 'colormap' or 'quiver' (have to angles for a quiver plot)
		import mapseis.plot.*;
		
		
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		%backup config
		obj.LastPlot={ValField,PlotType};
		
		%get data
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		TheVal=obj.CalcRes.(ValField)';
		
		AlphaMap=~isnan(TheVal);
		
		
		if obj.CalcParameter.SmoothMode==1;
			%Smooth the map before plotting
			TheValNaN=isnan(TheVal);
			%TheVal(TheValNaN)=0;
			
			%use the mean of the data instead of 0
			if ~isempty(obj.AlphaMap)
				ValMean=mean(mean(TheVal(~obj.AlphaMap&AlphaMap)));
			else
				ValMean=mean(mean(TheVal(AlphaMap)));
			end
			
			
			if ~isempty(ValMean)
				TheVal(TheValNaN)=ValMean;
			else
				TheVal(TheValNaN)=0;
			end
			
			
			hFilter = fspecial('gaussian',obj.CalcParameter.SmoothKernelSize, obj.CalcParameter.SmoothSigma);
			TheVal= imfilter((TheVal), hFilter, 'replicate');
				
			%Smoothing adds to the total sum, subtract that to compensate.
			%TheVal=TheVal-obj.CalcParameter.SmoothFactor;
				
			TheVal(TheValNaN)=NaN;
			
			%hFilter = fspecial('gaussian', 5, 2.5);
			%TheVal= imfilter((TheVal + obj.CalcParameter.SmoothFactor), hFilter, 'replicate');
			%TheVal(TheValNaN)=NaN;
			
			
			
		end
		
		%limits
		xlimiter=[floor(min(xdir)) ceil(max(xdir))];
		ylimiter=[floor(min(ydir)) ceil(max(ydir))];
		
		if obj.ProfileSwitch
			xlab='Profile [km] ';
			ylab='Depth [km] ';
		else
			xlab='Longitude ';
			ylab='Latitude ';
		end	
		
		
		
		%set all axes right
		xlim(plotAxis,xlimiter);
		ylim(plotAxis,ylimiter);
		
		latlim = get(plotAxis,'Ylim');
           	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           	set(plotAxis,'DrawMode','fast');
		
		
		%determine if a quiver is needed 
		switch PlotType
			case 'colormap'
				
				if obj.CalcParameter.Calc_Method==1|obj.CalcParameter.Calc_Method==3
					%TheVal=reshape(TheVal,numel(ydir),numel(xdir));
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				PlotData={xdir,ydir,TheVal};
			
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
				  				'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData','auto',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'ColorToggle',true,...
								'LegendText',ValField);
				 disp(plotAxis)				
				 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
								
								
			case 'quiver'
				
					
				if obj.CalcParameter.Calc_Method==1|obj.CalcParameter.Calc_Method==3
					xdir=obj.CalcRes.Xmeshed;
					ydir=obj.CalcRes.Ymeshed;
					TheVal=reshape(TheVal,numel(xdir),1);
					AlphaMap=reshape(AlphaMap,numel(xdir),1);
					xdir=xdir(AlphaMap);
					ydir=ydir(AlphaMap);
					TheVal=TheVal(AlphaMap);
					
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				%calculate dx and dy for the angle (TheVal
				%only up 180° used
				le=0.2;
				
				TheVal=mod(TheVal,180);

				%first the angles smaller than 90°
				sma=TheVal<90;

				%the larger than 90° angels
				lar=not(sma);

				%the x coordinate
				avect(:,1) = sin(deg2rad(TheVal))*le;

				%the sma part
				avect(sma,2) = cos(deg2rad(TheVal(sma)))*le;
				
				%the lar part
				avect(lar,2) = -cos(deg2rad(TheVal(lar) - 90))*le;
				
				PlotData=[xdir,ydir,avect(:,1),avect(:,2)];
				
				PlotConfig	= struct(	'PlotType','Vector2D',...
				  				'Data',[PlotData],...
								'MapStyle','not needed',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','red',...
								'LineStylePreset','normal',...
								'MarkerStylePreset','normal',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'DirectionMode',true,...
								'ColorToggle',true,...
								'LegendText',ValField);
				disp(plotAxis)
			         [handle legendentry] = PlotVector2D(plotAxis,PlotConfig);
		end	
		
		if ~obj.ProfileSwitch
			%at the moment only available in the map view
			%plot coastline, borders and earthquake locations if wanted
			if obj.BorderToogle
				hold on
				BorderConf= struct('PlotType','Border',...
							'Data',obj.Datastore,...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'LineStylePreset','normal',...
							'Colors','black');
				[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
				
			end
			
			
			if obj.CoastToogle
				hold on
				CoastConf= struct( 'PlotType','Coastline',...
						   'Data',obj.Datastore,...
						   'X_Axis_Label','Longitude ',...
						   'Y_Axis_Label','Latitude ',...
						   'LineStylePreset','normal',...
						   'Colors','blue');
				[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
			
			end
			
			
			if obj.EQToogle
				hold on
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',obj.SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
			
				
				[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
			end
				
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			latlim = get(plotAxis,'Ylim');
           		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           		set(plotAxis,'DrawMode','fast');
			
			
			hold off
			
		else
			
		
			if obj.EQToogle
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				
				hold on
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',obj.SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Distance [km] ',...
							'Y_Axis_Label','Depth [km] ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
			
				
				[handle1 entry1] = PlotEarthquakeSlice(plotAxis,EqConfig);
			end
				
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			%depthlim = get(plotAxis,'Ylim');
			%distlim = get(plotAxis,'Xlim');
			%deSpan=10*abs(depthlim(1)-depthlim(2));
			%diSpan=abs(distlim(1)-distlim(2));
			
			%set(plotAxis,'dataaspect',[1 diSpan/deSpan 1]);
			set(plotAxis,'dataaspect',[2 1 1]);
			set(plotAxis,'YDir','reverse');
			hold off
		end
end
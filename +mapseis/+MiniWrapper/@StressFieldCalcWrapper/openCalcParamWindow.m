function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		%check if old calculations are avialable
		try
			PreviousCalc=obj.Datastore.getUserData('StressInversion-v1');
		catch
			PreviousCalc=[];
		end

		if ~obj.ProfileSwitch;
		
			Title = 'Stress Tensor Inversion';
			Prompt={'Method:', 'Calc_Method';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size:','SmoothKernelSize';...
				'Smooth Kernel Sigma:','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (deg):','dx';...
				'Grid spacing y (deg):','dy';...
				'Minimum Number:','MinNumber';...
				'Damping (SATSI):','DampSatsi';...
				'Nr. of Initial Nodes (SATSI):','NrNodes';...
				'Use Automatic Grid (SATSI):','AutoGrid'};
			
			MethodList =  {'Slick (Michael 1984)'; ...
				'SATSI (Hardebeck & Michael 2005)';...
				'SATSI Node Mode (Hardebeck & Michael 2005)'}; 
				
				
			%default values
			defval =struct('Calc_Method',obj.CalcParameter.Calc_Method,...
				'SmoothMode',obj.CalcParameter.SmoothMode,...
				'SmoothKernelSize',obj.CalcParameter.SmoothKernelSize,...
				'SmoothSigma',obj.CalcParameter.SmoothSigma,...
				'Selection_Method',obj.CalcParameter.Selection_Method,...
				'NumEvents',obj.CalcParameter.NumEvents,...
				'Sel_Radius',obj.CalcParameter.Sel_Radius,...
				'dx',obj.CalcParameter.dx,...
				'dy',obj.CalcParameter.dy,...
				'MinNumber',obj.CalcParameter.MinNumber,...
				'DampSatsi',obj.CalcParameter.DampSatsi,...
				'NrNodes',obj.CalcParameter.NrNodes,...
				'AutoGrid',obj.CalcParameter.AutoGrid);	
				
		else
			Title = 'Stress Tensor Inversion (Depth Profile)';
			Prompt={'Method:', 'Calc_Method';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size:','SmoothKernelSize';...
				'Smooth Kernel Sigma:','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (km):','dx';...
				'Grid spacing z (deg):','dz';...
				'Minimum Number:','MinNumber';...
				'Damping (SATSI):','DampSatsi';...
				'Nr. of Initial Nodes (SATSI):','NrNodes';...
				'Use Automatic Grid (SATSI):','AutoGrid'};
			
			MethodList =  {'Slick (Michael 1984)'; ...
				'SATSI (Hardebeck & Michael 2005)';...
				'SATSI Node Mode (Hardebeck & Michael 2005)'}; 
				
			%default values
			defval =struct('Calc_Method',obj.CalcParameter.Calc_Method,...
				'SmoothMode',obj.CalcParameter.SmoothMode,...
				'SmoothKernelSize',obj.CalcParameter.SmoothKernelSize,...
				'SmoothSigma',obj.CalcParameter.SmoothSigma,...
				'Selection_Method',obj.CalcParameter.Selection_Method,...
				'NumEvents',obj.CalcParameter.NumEvents,...
				'Sel_Radius',obj.CalcParameter.Sel_Radius,...
				'dx',obj.CalcParameter.dx,...
				'dz',obj.CalcParameter.dz,...
				'MinNumber',obj.CalcParameter.MinNumber,...
				'DampSatsi',obj.CalcParameter.DampSatsi,...
				'NrNodes',obj.CalcParameter.NrNodes,...
				'AutoGrid',obj.CalcParameter.AutoGrid);	
		end		
		
		
		
		%Mc Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=MethodList;
		Formats(1,2).type='none';
		Formats(1,1).size = [-1 0];
		Formats(1,2).limits = [0 1];
		
		%Selection Method
		Formats(2,1).type='list';
		Formats(2,1).style='togglebutton';
		Formats(2,1).items={'Number of Events','Constant Radius','Both'}
		Formats(2,1).size = [-1 0];
		Formats(2,2).limits = [0 1];
		Formats(2,2).type='none';
		
		%Smoothing
		Formats(3,1).type='check';
		
		%Smoothing Parameter
		Formats(4,1).type='edit';
		Formats(4,1).format='float';
		Formats(4,1).limits = [0 9999];
		
		%Smoothing Parameter
		Formats(4,2).type='edit';
		Formats(4,2).format='float';
		Formats(4,2).limits = [0 9999];
		
		
		%NumberEvents
		Formats(5,1).type='edit';
		Formats(5,1).format='integer';
		Formats(5,1).limits = [0 9999];
	
		%Radius
		Formats(5,2).type='edit';
		Formats(5,2).format='float';
		Formats(5,2).limits = [0 9999];
		
		%Grid space x
		Formats(6,1).type='edit';
		Formats(6,1).format='float';
		Formats(6,1).limits = [0 999];
		
		%Grid space y or z
		Formats(6,2).type='edit';
		Formats(6,2).format='float';
		Formats(6,2).limits = [0 999];
		
		%Min Number
		Formats(7,1).type='edit';
		Formats(7,1).format='float';
		Formats(7,1).limits = [0 9999];
		Formats(7,1).size = [-1 0];
		Formats(7,2).limits = [0 1];
		Formats(7,2).type='none';
		
		%Damping
		Formats(8,1).type='edit';
		Formats(8,1).format='float';
		Formats(8,1).limits = [0 9999];
		Formats(8,1).size = [-1 0];
		Formats(8,2).limits = [0 1];
		Formats(8,2).type='none';
		
		
		%Nr Nodes
		Formats(9,1).type='edit';
		Formats(9,1).format='float';
		Formats(9,1).limits = [0 9999];
		Formats(9,1).size = [-1 0];
		Formats(9,2).limits = [0 1];
		Formats(9,2).type='none';
		
		
		%AutoGrid toggle
		Formats(10,1).type='check';
		Formats(10,1).size = [-1 0];
		Formats(10,2).limits = [0 1];
		Formats(10,2).type='none';	
			
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		if ~isempty(PreviousCalc)
			Prompt(end+1,:)={'Load Calculation?','LoadSwitch'};
		
			%LoadToggle toggle
			Formats(11,1).type='check';
			Formats(11,1).size = [-1 0];
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';	
			
			defval.LoadSwitch=0;
		end
		
		
		
		
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		if ~isempty(PreviousCalc)&Canceled==0&NewParameter.LoadSwitch==1
			%Load data
			obj.StartCal=false;
			obj.loadCalc(PreviousCalc);
			
		else
		
			if ~obj.ProfileSwitch
				%unpack from the structure
				obj.CalcParameter.Calc_Method=NewParameter.Calc_Method;
				obj.CalcParameter.Selection_Method=NewParameter.Selection_Method;
				obj.CalcParameter.SmoothMode=NewParameter.SmoothMode;
				obj.CalcParameter.SmoothKernelSize=NewParameter.SmoothKernelSize;
				obj.CalcParameter.SmoothSigma=NewParameter.SmoothSigma;
				obj.CalcParameter.NumEvents=NewParameter.NumEvents;
				obj.CalcParameter.Sel_Radius=NewParameter.Sel_Radius;
				obj.CalcParameter.dx=NewParameter.dx;
				obj.CalcParameter.dy=NewParameter.dy;
				obj.CalcParameter.MinNumber=NewParameter.MinNumber;
				obj.CalcParameter.DampSatsi=NewParameter.DampSatsi;
				obj.CalcParameter.NrNodes=NewParameter.NrNodes;
				obj.CalcParameter.AutoGrid=NewParameter.AutoGrid;
				
			else
				%unpack from the structure
				obj.CalcParameter.Calc_Method=NewParameter.Calc_Method;
				obj.CalcParameter.Selection_Method=NewParameter.Selection_Method;
				obj.CalcParameter.SmoothMode=NewParameter.SmoothMode;
				obj.CalcParameter.SmoothKernelSize=NewParameter.SmoothKernelSize;
				obj.CalcParameter.SmoothSigma=NewParameter.SmoothSigma;
				obj.CalcParameter.NumEvents=NewParameter.NumEvents;
				obj.CalcParameter.Sel_Radius=NewParameter.Sel_Radius;
				obj.CalcParameter.dx=NewParameter.dx;
				obj.CalcParameter.dz=NewParameter.dz;
				obj.CalcParameter.MinNumber=NewParameter.MinNumber;
				obj.CalcParameter.DampSatsi=NewParameter.DampSatsi;
				obj.CalcParameter.NrNodes=NewParameter.NrNodes;
				obj.CalcParameter.AutoGrid=NewParameter.AutoGrid;
			end
			
			
			
			if Canceled==1
				obj.StartCalc=false;
			else
				obj.StartCalc=true;
			end
		
		end
		
		
end
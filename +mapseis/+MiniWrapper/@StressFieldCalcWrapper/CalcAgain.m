function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		EveryThingGood=true;
		
		%get parameters
		obj.openCalcParamWindow;
		
		if obj.StartCalc
			
			%figNr=findobj('Name','b-value-map');
			
			%if ~isempty(figNr)
			%	close(figNr);
			%end
		
			%refresh catalogs
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			%obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%check if the focal mechanism are available
			try
				obj.Datastore.getFields({'focal_strike','focal_dip','focal_rake'});
			catch	
				EveryThingGood=false;
				errordlg('No focal mechanism available in the new catalog');
				obj.ErrorDialog='No focal mechanism available in the new catalog';
			end
			
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the current regionfilter');
					obj.ErrorDialog='No profile selected in the current regionfilter';
				 end
			end
			if EveryThingGood
				%Calculation
				obj.doTheCalcThing;
				
				obj.updateGUI;
				
				%Build resultGUI
				%obj.BuildResultGUI
						
				%plot results
				%obj.plotResult('stress')
			end	
		end
	
	
end
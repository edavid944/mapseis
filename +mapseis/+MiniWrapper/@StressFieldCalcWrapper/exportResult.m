function exportResult(obj,FileType,Filename) 
          	%saves the stress field into a mat or ascii file
          	
          	if nargin<3
          		Filename=[];
          	end
          
          	%get data
          	if ~isempty(obj.CalcRes)&~strcmp(FileType,'full_result')
          		xdir=obj.CalcRes.X;
			ydir=obj.CalcRes.Y;
          		S1Trend=obj.CalcRes.fS1Trend';
          		S1Plunge=obj.CalcRes.fS1Plunge';
          		S2Trend=obj.CalcRes.fS2Trend';
          		S2Plunge=obj.CalcRes.fS2Plunge';
          		S3Trend=obj.CalcRes.fS3Trend';
          		S3Plunge=obj.CalcRes.fS3Plunge';
          		s11=obj.CalcRes.fS11';
          		s12=obj.CalcRes.fS12';
          		s13=obj.CalcRes.fS13';
          		s22=obj.CalcRes.fS22';
          		s23=obj.CalcRes.fS23';
          		s33=obj.CalcRes.fS33';
          		
          		
          		
		elseif isempty(obj.CalcRes)
			return
		end
		
	
          	switch FileType
          		case 'matlab'
          			if isempty(Filename)
          				%get filename
          				[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','S1Trend','S1Plunge',...
						'S2Trend','S2Plunge','S3Trend','S3Plunge',...
						's11','s12','s13','s22','s23','s33');
				
          		case 'ascii'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','S1Trend','S1Plunge',...
						'S2Trend','S2Plunge','S3Trend','S3Plunge',...
						's11','s12','s13','s22','s23','s33','-ascii');
				
			case 'ascii_trio'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,['Xdir_',fName]),'xdir','-ascii');
				save(fullfile(pName,['Ydir_',fName]),'ydir','-ascii');
				save(fullfile(pName,['S1Trend_',fName]),'S1Trend','-ascii');
				save(fullfile(pName,['S1Plunge_',fName]),'S1Plunge','-ascii');
				save(fullfile(pName,['S2Trend_',fName]),'S2Trend','-ascii');
				save(fullfile(pName,['S2Plunge_',fName]),'S2Plunge','-ascii');
				save(fullfile(pName,['S3Trend_',fName]),'S3Trend','-ascii');
				save(fullfile(pName,['S3Plunge_',fName]),'S3Plunge','-ascii');
				save(fullfile(pName,['s11_',fName]),'s11','-ascii');
				save(fullfile(pName,['s12_',fName]),'s12','-ascii');
				save(fullfile(pName,['s13_',fName]),'s13','-ascii');
				save(fullfile(pName,['s22_',fName]),'s22','-ascii');
				save(fullfile(pName,['s23_',fName]),'s23','-ascii');
				save(fullfile(pName,['s33_',fName]),'s33','-ascii');
				
				
			case 'full_result'
				FullCalc=obj.CalcRes;
				
				if isempty(Filename)
					[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				if ~isempty(FullCalc)
					save(fullfile(pName,fName),'FullCalc');
				end
          	
          	end
 
end
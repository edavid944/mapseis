classdef StressFieldCalcWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		CalcParameter
		ProfileSwitch
		ErrorDialog
		ParallelMode
		StartCalc
		Loaded
		CalcName
		Region
		AlphaMap
		ResultGUI
		PlotOptions
		BorderToogle
		CoastToogle
		EQToogle
		PlotQuality
		LastPlot
	end

	events
		CalcDone

	end


	methods
			function obj = StressFieldCalcWrapper(ListProxy,Commander,GUISwitch,ProfileSwitch,CalcParameter)
			import mapseis.datastore.*;
			
			%This wrapper allows to use either the Micheals (1984) ot the 
			%SATSI (Hardebeck and Michael 2005) stress tensor inversion 
			%algorithmen with MapSeis.
			
			
			
			%In this constructor everything is set, and the 
			obj.ListProxy=ListProxy;
			
			if isempty(Commander)
				obj.Commander=[];
				%go into manul mode, the object will be created but everything
				%has to be done manually (useful for scripting)
			else
			
				obj.Commander = Commander; 
				obj.ParallelMode=obj.Commander.ParallelMode;
				obj.Region=[];
				obj.AlphaMap=[];
				obj.ProfileSwitch=ProfileSwitch;
				obj.Loaded=false;
				obj.ResultGUI=[];
				obj.PlotOptions=[];
				obj.BorderToogle=true;
				obj.CoastToogle=true;
				obj.EQToogle=false;
				obj.PlotQuality = 'low';
				obj.LastPlot=[];
				
				%get the current datastore and filterlist and build zmap catalog
				obj.Datastore = obj.Commander.getCurrentDatastore;
				obj.Filterlist = obj.Commander.getCurrentFilterlist; 
				obj.Keys=obj.Commander.getMarkedKey;
				selected=obj.Filterlist.getSelected;
				obj.SendedEvents=selected;
				
				EveryThingGood=true;
				
				
				%check if the focal mechanism are available
				try
					obj.Datastore.getFields({'focal_strike','focal_dip','focal_rake'});
				catch	
					EveryThingGood=false;
					errordlg('No focal mechanism available in this catalog');
					obj.ErrorDialog='No focal mechanism available in this catalog';
				end
				
				if ProfileSwitch
					 regionFilter = obj.Filterlist.getByName('Region');
					 filterRegion = getRegion(regionFilter);
					 RegRange=filterRegion{1};	
					 if ~strcmp(RegRange,'line')
						EveryThingGood=false;
						errordlg('No profile selected in the regionfilter');
						obj.ErrorDialog='No profile selected in the regionfilter';
					 end
				end
				
				if EveryThingGood
					
					if ~isempty(CalcParameter)
						%Parameters are include, no default parameter will
						%be set
						obj.CalcParameter=CalcParameter;
					else
						obj.InitVariables
					end	
					
					if GUISwitch
						
						%let the user modify the parameters
						obj.openCalcParamWindow;
						
						if obj.StartCalc
							
							if ~obj.Loaded
								%Calc
								obj.doTheCalcThing;
							end
							
							if isempty(obj.CalcRes)
								return
							end
								
							
							%Build resultGUI
							obj.BuildResultGUI
							
							%plot results
							obj.plotResult('stress')
						end	
						
						
					end
					
	
				
				end
			end
			
		end

		
		%external file functions
		%-----------------------

		InitVariables(obj)

		openCalcParamWindow(obj)

		doTheCalcThing(obj)

		BuildResultGUI(obj)

		buildMenus(obj)

		Smoother(obj,DoWhat)

		TooglePlotOptions(obj,WhichOne)

		plotResult(obj,whichplot)

		plotVariable(obj,plotAxis,ValField,PlotType)
		
		updateGUI(obj)

		plot_modify(obj,bright,interswitch,circleGrid)

		adju(obj,asel, whichplot)

		universalCalc(obj,commandstring,newvariables)

		CalcAgain(obj)

		saveCalc(obj)
				
		loadCalc(obj,oldCalcs)

		setqual(obj,qual)
	
		exportResult(obj,FileType,Filename) 

		

	end

	%the ext file static methods
	%---------------------------
	methods (Static)
		TheDateString=decyear2string(decYR)

		decimalYear=string2decyear(instring)

		TheEvalString = UnpackStruct(TheStruct)

		CellData = UnpackStruct2Cell(TheStruct)

		TheStruct = PackVariable(variablelist)

		TheStruct = PackAllVariable()

		StressTensors = FormTensor(S11,S12,S13,S22,S23,S33)

		[s1_strike s1_dip s2_strike s2_dip s3_strike s3_dip] =StrikeNDip(eigenVectors)

		[s1_value s2_value s3_value] = eigValUnpacker(eigVal)

		


	end



end
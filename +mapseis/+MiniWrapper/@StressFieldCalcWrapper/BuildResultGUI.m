function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('Stress-Inv-map',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Stress-Inv-map',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',[ 50 300 1100 690 ],'Renderer','OpenGL');
		   plotAxis=gca;
		   set(plotAxis,'Tag','MainResultAxis');
		   
		   obj.buildMenus;
		   
		end
		
end
function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.StressInv.*;
		import mapseis.calc.*;
		import mapseis.util.importfilter.cellArrayToPlane;
		
		if obj.CalcParameter.Calc_Method==1
			%regular mode
			%build the calculation object
			StressCalc = mapseis.calc.Calculation(...
		    			@mapseis.calc.StressInv.CalcStressInv_node,@mapseis.projector.getFocalsDipDir,{},...
		    			'Name','StressInversion');
		    	
		    	if ~obj.ProfileSwitch
				%normal lon lat grid
				
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.Filterlist.getSelected;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				CalcParam=struct(	'boundaryBox',[bounded],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy]],...
							'SelectionRadius',km2deg(obj.CalcParameter.Sel_Radius),...
							'SelectedEvents',[selected],...
							'SelectionNumber',obj.CalcParameter.NumEvents,...
							'SelectionMode',SelMode,...
							'DistanceSend',false,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=norm2DGridder(obj.Datastore,StressCalc,CalcParam);
		    		
				
				RawRes=cellArrayToPlane(calcRes);
				
				fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    	else
		    		
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.Filterlist.getSelected;
				
				CalcParam=struct(	'boundaryBox',[pRegion.getBoundingBox],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dz]],...
							'SelectionRadius',obj.CalcParameter.Sel_Radius,...
							'SelectedEvents',[selected],...
							'Sel_Nr',obj.CalcParameter.NumEvents,...
							'Sel_Mode',SelMode,...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'DistanceSend',false,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=Profile2DGridder(obj.Datastore,StressCalc,CalcParam);
		    		
				RawRes=cellArrayToPlane(calcRes);
				%obj.CalcRes.X=xRange;
		    		%obj.CalcRes.Y=yRange;
		    		
		    		fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    	end
		    	
		    	
		    		
		elseif obj.CalcParameter.Calc_Method==2
			
			%Satsi method
			if obj.CalcParameter.AutoGrid==1
				RawCalcRes = calcStress_Satsi(obj.Datastore,obj.Filterlist,obj.CalcParameter.NrNodes,...
						obj.ProfileSwitch,obj.CalcParameter.DampSatsi);
			else
				if ~obj.ProfileSwitch 
					GridSpace=[obj.CalcParameter.dx obj.CalcParameter.dy]
				else
					GridSpace=[obj.CalcParameter.dx obj.CalcParameter.dz]
				end
				
				RawCalcRes = calcStress_Satsi(obj.Datastore,obj.Filterlist,GridSpace,...
						obj.ProfileSwitch,obj.CalcParameter.DampSatsi);
			
			end
			%build the CalcRes for the plotting
			%if ~isempty(RawCalcRes)
			%	h = msgbox('Nothing has been calculated','Error','error')
			%	return
			%end
			
			%single solutions (not needed now)
			obj.CalcRes.Y_s = RawCalcRes{2}.Y;
			obj.CalcRes.X_s = RawCalcRes{2}.X;
			obj.CalcRes.strike_s = RawCalcRes{2}.strike;
			obj.CalcRes.dip_s = RawCalcRes{2}.dip;
			obj.CalcRes.rake_s = RawCalcRes{2}.rake;
			obj.CalcRes.fit_angle_s = RawCalcRes{2}.fit_angle;
			obj.CalcRes.mag_tau_s = RawCalcRes{2}.mag_tau;
			
			obj.CalcRes.X=RawCalcRes{1}.X;
			obj.CalcRes.Y=RawCalcRes{1}.Y;
			obj.CalcRes.See=RawCalcRes{1}.See;
			obj.CalcRes.Sen=RawCalcRes{1}.Sen;
			obj.CalcRes.Seu=RawCalcRes{1}.Seu;
			obj.CalcRes.Snn=RawCalcRes{1}.Snn;
			obj.CalcRes.Snu=RawCalcRes{1}.Snu;
			obj.CalcRes.Suu=RawCalcRes{1}.Suu;
			
			obj.CalcRes.overall_fit_angle_mean=RawCalcRes{3}.fit_angle_mean;
			obj.CalcRes.overall_std_dev_angle=RawCalcRes{3}.std_dev_angle;
			obj.CalcRes.overall_avg_tau=RawCalcRes{3}.avg_tau;
			obj.CalcRes.overall_std_dev_tau=RawCalcRes{3}.std_dev_tau;
			
			%delete later, just a note
			%numericFields_A = {'X','Y','See','Sen','Seu','Snn','Snu','Suu'};
			%numericFields_B = {'strike','dip','rake','fit_angle','mag_tau','X','Y'};
			%LastFields={'fit_angle_mean','std_dev_angle','avg_tau','std_dev_tau'}
			
			%The Satsi Algorithmen seems to only return the stresstensor. For plotting the principal
			%stress axis are needed, so they have to be calculated here.
			%eigenvalues and vectors
			
			%get Stresstensors
			RawStressTensors = mapseis.MiniWrapper.StressFieldCalcWrapper.FormTensor(...
					obj.CalcRes.See,obj.CalcRes.Sen,obj.CalcRes.Seu,obj.CalcRes.Snn,...
					obj.CalcRes.Snu,obj.CalcRes.Suu);
			
			
					
			%convert into cell		
			%StressTensors=num2cell(RawStressTensors,[2 3]);
			%StressTensors=cellfun(@(x) reshape(x,3,3),StressTensors,'UniformOutput',false);
			if numel(RawStressTensors)>9
				%StressTensors=num2cell(RawStressTensors,[1 2]);
				StressTensors=num2cell(RawStressTensors,[2 3]);
				StressTensors=cellfun(@(x) reshape(x,3,3),StressTensors,'UniformOutput',false);
			else
				%pack manually
				Tenss(1,:)=RawStressTensors(:,:,1);
				Tenss(2,:)=RawStressTensors(:,:,2);
				Tenss(3,:)=RawStressTensors(:,:,3);
				StressTensors{1}=Tenss;
				
			end
			disp(RawStressTensors);
			
			%get eigenvals and eigenvectors
			[eigvec eigval]=cellfun(@eig,StressTensors,'UniformOutput',false);
			
			%now I have to get the strike and val and dip of the principal axis somehow?
			%how?
			%and how to do it in a effectiv way?
			%---> principal vector = (p1,p2,p3)
			%---> strike = arccos(p2/sqrt(p1^2+p2^2)) 
			%---> dip = (p1^2+p2^2)/(sqrt(p1^2+p2^2)*sqrt(p1^2+p2^2+p3^2))
			%		maybe this can be simplified
			%---> value = eigenvalue?
			%This whole transformation has to be tested, I'm not fully convinced it works 
			
			%reform eigval
			if iscell(eigval)
				eigval=eigval(1,:)';
			else
				eigval={eigval};
				eigvec={eigvec};
			end
			disp(eigval)
			disp(eigvec)
			
			[s1_strike s1_dip s2_strike s2_dip s3_strike s3_dip] =...
				cellfun(@mapseis.MiniWrapper.StressFieldCalcWrapper.StrikeNDip,...
					eigvec,'UniformOutput',false);
					
			[s1_value s2_value s3_value] = ...
				cellfun(@mapseis.MiniWrapper.StressFieldCalcWrapper.eigValUnpacker,...
					eigval,'UniformOutput',false);		
					
			obj.CalcRes.s1_strike=cell2mat(s1_strike);
			obj.CalcRes.s1_dip=cell2mat(s1_dip);
			obj.CalcRes.s1_value=cell2mat(s1_value);
			
			obj.CalcRes.s2_strike=cell2mat(s2_strike);
			obj.CalcRes.s2_dip=cell2mat(s2_dip);
			obj.CalcRes.s2_value=cell2mat(s2_value);
			
			obj.CalcRes.s3_strike=cell2mat(s3_strike);
			obj.CalcRes.s3_dip=cell2mat(s3_dip);
			obj.CalcRes.s3_value=cell2mat(s3_value);
			
			%similar to Phi on confentional method, don't know if this is right, might change
			%it later
			obj.CalcRes.S2_S1_rel=obj.CalcRes.s2_value./obj.CalcRes.s1_value;
			
			
			CalcRes=obj.CalcRes;
			CalcRes.RawStressTensors=RawStressTensors;
			save('__StressOut.mat','CalcRes');
			
			%calc faulting style and RMS (later)
			for i=1:numel(obj.CalcRes.Y)
				rStress.fS11=obj.CalcRes.See(i);
				rStress.fS12=obj.CalcRes.Sen(i);
				rStress.fS13=obj.CalcRes.Seu(i);
				rStress.fS22=obj.CalcRes.Snn(i);
				rStress.fS23=obj.CalcRes.Snu(i);
				rStress.fS33=obj.CalcRes.Suu(i);
				
				rStress.fS1Plunge=obj.CalcRes.s1_dip(i);
				rStress.fS2Plunge=obj.CalcRes.s2_dip(i);
				rStress.fS3Plunge=obj.CalcRes.s3_dip(i);
				rStress.fPhi=obj.CalcRes.S2_S1_rel(i);
				
				% fAphi   : 0 <= fAphi < 1 Normal faulting
				%           1 <= fAphi < 2 Strike slip
				%           2 <= fAphi < 3 Thrust faulting
				obj.CalcRes.FaultStyle(i)=calc_FaultStyle(rStress);
				
				%RMS
				inNode=obj.CalcRes.Y(i)==obj.CalcRes.Y_s&obj.CalcRes.X(i)==obj.CalcRes.X_s;
				
				%focals
				vDipDir=obj.CalcRes.strike_s(inNode);
				vDip=obj.CalcRes.dip_s(inNode);
				vRake=obj.CalcRes.rake_s(inNode);
				
			
				obj.CalcRes.Rms = calc_FMdiversity(vDipDir,vDip,vRake);
			end
			
			
		elseif obj.CalcParameter.Calc_Method==3
			%SATSI NODE MODE
			%NOT FINISHED YET
			
			%build the calculation object
			StressCalc = mapseis.calc.Calculation(...
		    			@mapseis.calc.StressInv.calcStress_SatsiNode,@mapseis.projector.getFocalsDipDir,{obj.CalcParameter.DampSatsi},...
		    			'Name','StressInversion');
		    	
		    	if ~obj.ProfileSwitch
				%normal lon lat grid
				
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.Filterlist.getSelected;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				CalcParam=struct(	'boundaryBox',[bounded],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy]],...
							'SelectionRadius',km2deg(obj.CalcParameter.Sel_Radius),...
							'SelectedEvents',[selected],...
							'SelectionNumber',obj.CalcParameter.NumEvents,...
							'SelectionMode',SelMode,...
							'DistanceSend',false,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=norm2DGridder(obj.Datastore,StressCalc,CalcParam);
		    		
				
				RawRes=cellArrayToPlane(calcRes);
				
				fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    	else
		    		
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.Filterlist.getSelected;
				
				CalcParam=struct(	'boundaryBox',[pRegion.getBoundingBox],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dz]],...
							'SelectionRadius',obj.CalcParameter.Sel_Radius,...
							'SelectedEvents',[selected],...
							'Sel_Nr',obj.CalcParameter.NumEvents,...
							'Sel_Mode',SelMode,...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'DistanceSend',false,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=Profile2DGridder(obj.Datastore,StressCalc,CalcParam);
		    		
				RawRes=cellArrayToPlane(calcRes);
				%obj.CalcRes.X=xRange;
		    		%obj.CalcRes.Y=yRange;
		    		
		    		fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    	end	
			
		end
		
end
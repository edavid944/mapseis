function plot_modify(obj,bright,interswitch,circleGrid)
		%allows to modify the plot (some legacy stuff which will be changed
		%later if needed is in here)
		 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(plotAxis)
			 %try retag
			 %get all childrens of the main gui
    			gracomp = get(obj.ResultGUI, 'Children');
    			
    			%find axis and return axis
    			plotAxis = findobj(gracomp,'Type','axes','Tag','');
    			set(plotAxis,'Tag','MainResultAxis');
    		end	
		
    		if ~isempty(bright)
			axes(plotAxis);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			
			switch interswitch
				case 'flat'
					axes(plotAxis);
					shading flat;
					
				case 'interp'
					axes(plotAxis);
					shading interp;
					
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		
end
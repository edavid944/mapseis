classdef UtilCalcWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		RawCalcRes
		CalcParameter
		ErrorDialog
		ParallelMode
		StartCalc
		Loaded
		CalcName
		Region
		AlphaMap
		ResultGUI
		PlotOptions
		BorderToogle
		CoastToogle
		EQToogle
		PlotQuality
		LastPlot
		CustomScriptMode
	end

	events
		CalcDone

	end


	methods
	
	
	function obj = UtilCalcWrapper(ListProxy,Commander,GUISwitch,CalcParameter)
		import mapseis.datastore.*;
		
		%This object is only for testing and similar, at moment very rought, will improve it
		%if needed.
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		
		if isempty(Commander)
			obj.Commander=[];
			%go into manul mode, the object will be created but everything
			%has to be done manually (useful for scripting)
		else
		
			obj.Commander = Commander; 
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Region=[];
			obj.AlphaMap=[];
			obj.Loaded=false;
			obj.ResultGUI=[];
			obj.PlotOptions=[];
			obj.BorderToogle=true;
			obj.CoastToogle=true;
			obj.EQToogle=false;
			obj.PlotQuality = 'low';
			obj.LastPlot=[];
			obj.RawCalcRes=[];
			obj.CustomScriptMode=false;
			
			%get the current datastore and filterlist and build zmap catalog
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			
			EveryThingGood=true;
			
			
		
			if EveryThingGood
				
				if ~isempty(CalcParameter)
					%Parameters are include, no default parameter will
					%be set
					obj.CalcParameter=CalcParameter;
				else
					obj.InitVariables
				end	
				
				if GUISwitch
					
					%let the user modify the parameters
					obj.openCalcParamWindow;
					
					if obj.StartCalc
						
						if ~obj.Loaded
							%Calc
							obj.doTheCalcThing;
						end
						
						%Build resultGUI
						obj.BuildResultGUI
						
						%plot results
						obj.plotResult('Standard')
					end	
					
					
				end
				

			
			end
		end
		
	end

	
	
	
	function InitVariables(obj)
		%Now Zmap stuff needed anymore
		%some basic parameters, I will add addition parameters later 
		%if needed e.g. bootstrap option
		obj.CalcParameter = struct(	'MinNumber',0,...
						'dx',1,...
						'dy',1,...
						'dz',1,...
						'NumEvents',50,...
						'Sel_Radius',5,...
						'Selection_Method',1,...
						'SmoothMode',0,...
						'SmoothFactor',0.2,...
						'Calc_Method',1,...
						'GridMethod','regular');
						
		
		%Calc_Method can be 'michaels' or 'satsi'
		%GridMethod cannot be changed in the GUI for now until I improved
		%the selfrefining grid algorithmen. (options: 'regular' and 'selfRef')
		
		
		
	
	end
	
	
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		obj.CustomScriptMode=false;
		
				
		Title = 'Util Modul';
		Prompt={'Type:', 'Calc_Method';...
			'Selection Method:','Selection_Method';...
			'Smoothing?','SmoothMode';...
			'Smoothing Factor','SmoothFactor';...
			'Number of Events:','NumEvents';...
			'Selection Radius (km):','Sel_Radius';...
			'Grid spacing x (deg):','dx';...
			'Grid spacing y (deg):','dy';...
			'Mininum Number:','MinNumber'};
			
		MethodList =  {'Density';'Echo';'Benchmark';'MeanMag';'Counter';'BarCounter';'CustomCalc'}; 
				
				
		%default values
		defval =struct('Calc_Method',obj.CalcParameter.Calc_Method,...
				'SmoothMode',obj.CalcParameter.SmoothMode,...
				'SmoothFactor',obj.CalcParameter.SmoothFactor,...
				'Selection_Method',obj.CalcParameter.Selection_Method,...
				'NumEvents',obj.CalcParameter.NumEvents,...
				'Sel_Radius',obj.CalcParameter.Sel_Radius,...
				'dx',obj.CalcParameter.dx,...
				'dy',obj.CalcParameter.dy,...
				'MinNumber',obj.CalcParameter.MinNumber);
		
		
		
		
		%Mc Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=MethodList;
		Formats(1,2).type='none';
		Formats(1,1).size = [-1 0];
		Formats(1,2).limits = [0 1];
		
		%Selection Method
		Formats(2,1).type='list';
		Formats(2,1).style='togglebutton';
		Formats(2,1).items={'Number of Events','Constant Radius','Both'}
		Formats(2,1).size = [-1 0];
		Formats(2,2).limits = [0 1];
		Formats(2,2).type='none';
		
		%Smoothing
		Formats(3,1).type='check';
		
		%Smoothing Parameter
		Formats(3,2).type='edit';
		Formats(3,2).format='float';
		Formats(3,2).limits = [0 9999];
		
		%NumberEvents
		Formats(4,1).type='edit';
		Formats(4,1).format='integer';
		Formats(4,1).limits = [0 9999];
	
		%Radius
		Formats(4,2).type='edit';
		Formats(4,2).format='float';
		Formats(4,2).limits = [0 9999];
		
		%Grid space x
		Formats(5,1).type='edit';
		Formats(5,1).format='float';
		Formats(5,1).limits = [0 999];
		
		%Grid space y or z
		Formats(5,2).type='edit';
		Formats(5,2).format='float';
		Formats(5,2).limits = [0 999];
		
		%Min Number
		Formats(6,1).type='edit';
		Formats(6,1).format='float';
		Formats(6,1).limits = [0 9999];
		Formats(6,1).size = [-1 0];
		Formats(6,2).limits = [0 1];
		Formats(6,2).type='none';
		

		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		
		
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
	
		
		
		%unpack from the structure
		obj.CalcParameter.Calc_Method=NewParameter.Calc_Method;
		obj.CalcParameter.Selection_Method=NewParameter.Selection_Method;
		obj.CalcParameter.SmoothMode=NewParameter.SmoothMode;
		obj.CalcParameter.SmoothFactor=NewParameter.SmoothFactor;
		obj.CalcParameter.NumEvents=NewParameter.NumEvents;
		obj.CalcParameter.Sel_Radius=NewParameter.Sel_Radius;
		obj.CalcParameter.dx=NewParameter.dx;
		obj.CalcParameter.dy=NewParameter.dy;
		obj.CalcParameter.MinNumber=NewParameter.MinNumber;
		
		%done like this to be save
		TheMethod=MethodList{obj.CalcParameter.Calc_Method};
		disp(TheMethod)
		if strcmp(TheMethod,'CustomCalc')
			
			currDir=pwd;
			try
				cd('./Calc_Scripts');
			end
			
			[fName,pName] = uigetfile('*.m','Enter MapSeis Calc Scriptname...');
			if isequal(fName,0) || isequal(pName,0)
					disp('No filename selected. ');
					 Canceled=1;
			else
			
				
				cd(pName);
				[pathStr,nameStr,extStr] = fileparts(fName);
				ScriptHandle=str2func(nameStr);
				cd(currDir);
				obj.CalcParameter.ScriptHandle=ScriptHandle;
				obj.CustomScriptMode=true;
		
			end
			cd(currDir);
		end
		
		
			
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		
		
	end
	
	
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.*;
		import mapseis.util.importfilter.cellArrayToPlane;
		import mapseis.util.*;
		
		%MethodList =  {'Density';'Echo';'Benchmark';'MeanMag';'Counter';};
		
		%____________
		%
		%NOT FINISHED (works well enough)
		%____________
		
		%regular mode
		%build the calculation object
		
		MethodList =  {'Density';'Echo';'Benchmark';'MeanMag';'Counter';'BarCounter';'CustomCalc'};
		TheMode= MethodList{obj.CalcParameter.Calc_Method};
		CalcConfig.Mode=TheMode;
		if strcmp(TheMode,'Counter')|strcmp(TheMode,'BarCounter')
			CalcConfig.Counter=CountObject(1);	
		end
		
		if obj.CustomScriptMode
			CalcConfig.ScriptHandle=obj.CalcParameter.ScriptHandle;
			
			%The whole catalog is send, gives more freedom
			UtilCalc = mapseis.calc.CalculationEX(...
		   		@mapseis.calc.TestBenchmarkCalc,@mapseis.projector.getFullCatalog,...
		   		CalcConfig,'Name','UtilCalcer');
			
		else
		
			UtilCalc = mapseis.calc.CalculationEX(...
		   		@mapseis.calc.TestBenchmarkCalc,@mapseis.projector.getZMAPFormat,...
		   		CalcConfig,'Name','UtilCalcer');
		end    	
		%normal lon lat grid
			
		regionFilter = obj.Filterlist.getByName('Region');
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange=filterRegion{1};
		
		if obj.CalcParameter.Selection_Method==1
			SelMode='Number';
		elseif obj.CalcParameter.Selection_Method==2
			SelMode='Radius';
		elseif obj.CalcParameter.Selection_Method==3
			SelMode='Both';
		end
			
		selected=obj.Filterlist.getSelected;
		
		if strcmp(RegRange,'all');
			[locations temp]=getLocations(obj.Datastore,selected);
			lon=locations(:,1);
			lat=locations(:,2);
			bounded=[min(lon) max(lon) min(lat) max(lat)];
		else
			bounded=pRegion.getBoundingBox;
			
		end
		
		if strcmp(TheMode,'BarCounter')
			CalcConfig.Counter=CountObject(1);
			maxNumber=numel(bounded(1):obj.CalcParameter.dx:bounded(2))*...
				numel(bounded(3):obj.CalcParameter.dy:bounded(4));
				disp(maxNumber)
			CalcConfig.Counter.AddProgressbar(maxNumber)
		end
		
		CalcParam=struct(	'boundaryBox',[bounded],...
					'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy]],...
					'SelectionRadius',km2deg(obj.CalcParameter.Sel_Radius),...
					'SelectedEvents',[selected],...
					'SelectionNumber',obj.CalcParameter.NumEvents,...
					'SelectionMode',SelMode,...
					'DistanceSend',false,...
					'ParallelCalc',obj.ParallelMode);
		disp(CalcParam)
					
		[xRange,yRange,calcRes]=norm2DGridder(obj.Datastore,UtilCalc,CalcParam);
		obj.RawCalcRes.Unprocessed=calcRes;
		obj.RawCalcRes.xRange=xRange;
		obj.RawCalcRes.yRange=yRange;
		  		
			
		RawRes=cellArrayToPlane(calcRes);
		obj.RawCalcRes.celledRes=RawRes;
		
		
		fields=fieldnames(RawRes);
		obj.CalcRes.TheFields=fields;
		%disp(RawRes)
		disp(fields)
		
		for i=1:numel(fields)
			%disp(RawRes.(fields{i}))
			obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
		end
			
		[xx yy]=meshgrid(xRange,yRange);
		le=numel(xx);
		xvec=reshape(xx,le,1);
		yvec=reshape(yy,le,1);
		obj.CalcRes.X=xRange;
		obj.CalcRes.Y=yRange;
		obj.CalcRes.Xmeshed=xvec;
		obj.CalcRes.Ymeshed=yvec;
		    		
		    	
		    		
		
		
	end
	
	
	
	
	function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('Util-map',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Util-map',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',[ 50 300 1100 690 ],'Renderer','painters');
		   plotAxis=gca;
		   set(plotAxis,'Tag','MainResultAxis');
		   
		   obj.buildMenus;
		   
		end
		
	end
	
	
	function buildMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
		
		
		
		
		
		%first the general menus (similar for both)
		%at the moment only colormaps
                %now the type depended map menu
                if obj.CustomScriptMode
                	TheFields=obj.CalcRes.TheFields;
                
                	MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
                	
                		for i=1:numel(TheFields)
				 	uimenu(MapMenu,'Label',['Plot ',TheFields{i}],...
				 	'Callback',@(s,e) obj.plotVariable([],TheFields{i},'colormap'));
				 end
                
                else
		
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
				 uimenu(MapMenu,'Label','Standard test Output',...
				 'Callback',@(s,e) obj.plotVariable([],'StdOut','colormap'));
		
			
		end	
			
			
			
		
		
		
		
	end
	
	
	
	
	function plotResult(obj,whichplot)
		%more of a relict dummy function 
		%it allows a bit less complicated plotting, more for scripting
		%it calls plotVariable
		
			switch whichplot
				case 'Standard'
					if obj.CustomScriptMode
						obj.plotVariable([],obj.CalcRes.TheFields{1},'colormap');
					else	
						%just plot phi (s2/s1 relation)
						obj.plotVariable([],'StdOut','colormap');
					end
					 
					
				
					
			end
		
	
	end
	
	
	function plotVariable(obj,plotAxis,ValField,PlotType)
		%will plot the selected field of the CalcRes structur into the selected plotaxis
		%the PlotType can either be 'colormap' or 'quiver' (have to angles for a quiver plot)
		import mapseis.plot.*;
		
		
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		%backup config
		obj.LastPlot={ValField,PlotType};
		
		%get data
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		TheVal=obj.CalcRes.(ValField)';
		
		AlphaMap=~isnan(TheVal);
		
		
		if obj.CalcParameter.SmoothMode==1;
			%Smooth the map before plotting
			hFilter = fspecial('gaussian', 5, 2.5);
			TheVal= imfilter((TheVal + obj.CalcParameter.SmoothFactor), hFilter, 'replicate');
		end
		
		%limits
		xlimiter=[floor(min(xdir)) ceil(max(xdir))];
		ylimiter=[floor(min(ydir)) ceil(max(ydir))];
		
		
		xlab='Longitude ';
		ylab='Latitude ';
		
		
		
		
		%set all axes right
		xlim(plotAxis,xlimiter);
		ylim(plotAxis,ylimiter);
		
		latlim = get(plotAxis,'Ylim');
           	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           	set(plotAxis,'DrawMode','fast');
		
		
		%determine if a quiver is needed 
		switch PlotType
			case 'colormap'
				
				if obj.CalcParameter.Calc_Method==1
					%TheVal=reshape(TheVal,numel(ydir),numel(xdir));
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				PlotData={xdir,ydir,TheVal};
			
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
				  				'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData','auto',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'ColorToggle',true,...
								'LegendText',ValField);
				 disp(plotAxis)				
				 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
								
								
			case 'quiver'
				
					
				if obj.CalcParameter.Calc_Method==1
					xdir=obj.CalcRes.Xmeshed;
					ydir=obj.CalcRes.Ymeshed;
					TheVal=reshape(TheVal,numel(xdir),1);
					AlphaMap=reshape(AlphaMap,numel(xdir),1);
					xdir=xdir(AlphaMap);
					ydir=ydir(AlphaMap);
					TheVal=TheVal(AlphaMap);
					
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				%calculate dx and dy for the angle (TheVal
				%only up 180� used
				le=0.2;
				
				TheVal=mod(TheVal,180);

				%first the angles smaller than 90�
				sma=TheVal<90;

				%the larger than 90� angels
				lar=not(sma);

				%the x coordinate
				avect(:,1) = sin(deg2rad(TheVal))*le;

				%the sma part
				avect(sma,2) = cos(deg2rad(TheVal(sma)))*le;
				
				%the lar part
				avect(lar,2) = -cos(deg2rad(TheVal(lar) - 90))*le;
				
				PlotData=[xdir,ydir,avect(:,1),avect(:,2)];
				
				PlotConfig	= struct(	'PlotType','Vector2D',...
				  				'Data',[PlotData],...
								'MapStyle','not needed',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','red',...
								'LineStylePreset','normal',...
								'MarkerStylePreset','normal',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'ColorToggle',true,...
								'LegendText',ValField);
				disp(plotAxis)
			         [handle legendentry] = PlotVector2D(plotAxis,PlotConfig);
		end	
		
	
	end
	
	
	function updateGUI(obj)
		%uses the last plot option and plot it again
		ValField=obj.LastPlot{1};
		PlotType=obj.LastPlot{2};
		
		%only needed for quality change a similar stuff
		obj.plotVariable([],ValField,PlotType);
		
	end
	
	
	function plot_modify(obj,bright,interswitch,circleGrid)
		%allows to modify the plot (some legacy stuff which will be changed
		%later if needed is in here)
		 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(plotAxis)
			 %try retag
			 %get all childrens of the main gui
    			gracomp = get(obj.ResultGUI, 'Children');
    			
    			%find axis and return axis
    			plotAxis = findobj(gracomp,'Type','axes','Tag','');
    			set(plotAxis,'Tag','MainResultAxis');
    		end	
		
    		if ~isempty(bright)
			axes(plotAxis);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			
			switch interswitch
				case 'flat'
					axes(plotAxis);
					shading flat;
					
				case 'interp'
					axes(plotAxis);
					shading interp;
					
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		
	end
	
	
	function adju(obj,asel, whichplot)
		%function version of adju
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;


		prompt={'Enter the minimum magnitude cut-off','Enter the maximum radius cut-off:','Enter the minimum goodness of fit percatge'};
		def={'nan','nan','nan'};
		dlgTitle='Input Map subselection Criteria';
		lineNo=1;
		answer=inputdlg(prompt,dlgTitle,lineNo,def);
		re4 = re3;
		l = answer{1,1}; Mmin = str2num(l) ;
		l = answer{2,1}; tresh = str2num(l) ;
		l = answer{3,1}; minpe = str2num(l) ;
		
		
		obj.PackList=[obj.PackList; {'re4';'l';'asel'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

		%back to plot
		obj.plot_bval(whichplot);
		
	end
	
	function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	
	
	function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		EveryThingGood=true;
		
		%get parameters
		obj.openCalcParamWindow;
		
		if obj.StartCalc
			
			%figNr=findobj('Name','b-value-map');
			
			%if ~isempty(figNr)
			%	close(figNr);
			%end
		
			%refresh catalogs
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			%obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%check if the focal mechanism are available
			try
				obj.Datastore.getFields({'focal_strike','focal_dip','focal_rake'});
			catch	
				EveryThingGood=false;
				errordlg('No focal mechanism available in the new catalog');
				obj.ErrorDialog='No focal mechanism available in the new catalog';
			end
			
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the current regionfilter');
					obj.ErrorDialog='No profile selected in the current regionfilter';
				 end
			end
			if EveryThingGood
				%Calculation
				obj.doTheCalcThing;
				
				obj.updateGUI;
				
				%Build resultGUI
				%obj.BuildResultGUI
						
				%plot results
				%obj.plotResult('stress')
			end	
		end
	
	
	end
	
	

	
	
	

	

end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	

end
	
	

end

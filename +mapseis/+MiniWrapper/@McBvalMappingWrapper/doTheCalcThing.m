function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.Mc_calc.*;
		import mapseis.calc.*;
		import mapseis.util.importfilter.cellArrayToPlane;
		import mapseis.util.importfilter.cellArrayToPlane3D;
		
			%CalcConfig and CalcObject (similar for all calc)
			CalcConfig=struct(	'Mc_method',obj.CalcParameter.Mc_Method,...
						'Use_bootstrap',obj.CalcParameter.BootSwitch,...
						'Nr_bootstraps',obj.CalcParameter.BootSamples,...
						'Mc_correction',obj.CalcParameter.McCorr,...
						'MinNumber',obj.CalcParameter.MinNumber,....
						'Mc_Binning',0.1,...
						'FixedMc',obj.CalcParameter.FixedMc,...
						'Calc_sign_bval',obj.CalcParameter.Calc_sign_bval,...
						'Regional_bval',obj.CalcParameter.Overall_bval,...
						'NoCurve',true);
						
						
			%calc object			
			DenseConfig.CalcFunction=@mapseis.calc.Calc_Master_Mc_bval;
			DenseConfig.FunctionConfig=CalcConfig;
			DenseConfig.ProfileSwitch=obj.ProfileSwitch|obj.ThirdDimension;
			
			McBvalCalc = mapseis.calc.CalculationEX(...
		 		@mapseis.calc.DensityAdder,@mapseis.projector.getZMAPFormat,...
		   		DenseConfig,'Name','BvalCalc');		
		    	
		    			
		    	if ~obj.ProfileSwitch&~obj.ThirdDimension
				%normal lon lat grid
				
							
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.SendedEvents;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				CalcParam=struct(	'boundaryBox',[bounded],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy]],...
							'SelectionRadius',km2deg(obj.CalcParameter.Sel_Radius),...
							'SelectedEvents',[selected],...
							'SelectionNumber',obj.CalcParameter.NumEvents,...
							'SelectionMode',SelMode,...
							'DistanceSend',true,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=norm2DGridder(obj.Datastore,McBvalCalc,CalcParam);
		    		
				
				RawRes=cellArrayToPlane(calcRes);
				%disp(RawRes)
				fields=fieldnames(RawRes);
				%disp(RawRes.a_value_annual)
				for i=1:numel(fields)
					%disp((fields{i}))
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    		
		    		%build the AlphaMap
		    		regionFilter = obj.Filterlist.getByName('Region');
		    		regionParms = regionFilter.getRegion();

		    		if ~strcmp(regionParms{1},'all')
		    			theRegion = regionParms{2};	
		    			RawAlpha=theRegion.isInside([xvec,yvec]);
		    			try
				 		obj.AlphaMap=~logical(reshape(RawAlpha,length(yRange),length(xRange)));
				 	catch
				 		%means the polygon is badly shaped
				 		obj.AlphaMap=[];
				 		disp('could not restore AlphaMap')
				 	end	
		    			
		    		end
		    		
		    	elseif obj.ProfileSwitch&~obj.ThirdDimension
		    		%Profile mode	
		    	
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				%disp(obj.CalcParameter.Selection_Method)
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.SendedEvents;
				
				CalcParam=struct(	'boundaryBox',[pRegion.getBoundingBox],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dz]],...
							'SelectionRadius',obj.CalcParameter.Sel_Radius,...
							'SelectedEvents',[selected],...
							'Sel_Nr',obj.CalcParameter.NumEvents,...
							'Sel_Mode',SelMode,...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'DistanceSend',true,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=Profile2DGridder(obj.Datastore,McBvalCalc,CalcParam);
		    		
				RawRes=cellArrayToPlane(calcRes);
				%obj.CalcRes.X=xRange;
		    		%obj.CalcRes.Y=yRange;
		    		
		    		fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    	
		    		
		    		
		    	elseif ~obj.ProfileSwitch&obj.ThirdDimension
		    		%3D mode
		    		
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.SendedEvents;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				if strcmp(obj.CalcParameter.GridMethod,'regular')
				
					[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
					
					CalcParam=struct(	'boundaryBox',[bounded],...
								'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy obj.CalcParameter.dz]],...
								'vertical_Min_Max',[[floor(min(selectedDepth)),ceil(max(selectedDepth))]],...
								'SelectionRadius',obj.CalcParameter.Sel_Radius,...
								'SelectedEvents',[selected],...
								'SelectionNumber',obj.CalcParameter.NumEvents,...
								'SelectionMode',SelMode,...
								'DistanceSend',true,...
								'ParallelCalc',obj.ParallelMode);
								
					[xRange,yRange,zRange,calcRes]=regular3DGridder(obj.Datastore,McBvalCalc,CalcParam);
					
					
					RawRes=cellArrayToPlane3D(calcRes);
					
					fields=fieldnames(RawRes);
					for i=1:numel(fields)
						obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
					end
					
					
					
					%build the AlphaMap
					[xx2d yy2d]=meshgrid(xRange,yRange);
					le2d=numel(xx2d);
					xvec2d=reshape(xx2d,le2d,1);
					yvec2d=reshape(yy2d,le2d,1);
					
					
					%build the AlphaMap
					regionFilter = obj.Filterlist.getByName('Region');
					regionParms = regionFilter.getRegion();
	
					if ~strcmp(regionParms{1},'all')
						theRegion = regionParms{2};	
						RawAlpha=theRegion.isInside([xvec2d,yvec2d]);
						try
							obj.AlphaMap=~logical(reshape(RawAlpha,length(yRange),length(xRange)));
						catch
							%means the polygon is badly shaped
							obj.AlphaMap=[];
							disp('could not restore AlphaMap')
						end	
						
					end
					
					[xx yy zz]=meshgrid(xRange,yRange,zRange);
					le=numel(xx);
					xvec=reshape(xx,le,1);
					yvec=reshape(yy,le,1);
					zvec=reshape(zz,le,1);
					obj.CalcRes.X=xRange;
					obj.CalcRes.Y=yRange;
					obj.CalcRes.Z=zRange;
					obj.CalcRes.Xmeshed=xvec;
					obj.CalcRes.Ymeshed=yvec;
					obj.CalcRes.Zmeshed=zvec;
				
				elseif strcmp(obj.CalcParameter.GridMethod,'point')
					CalcParam=struct(	'ThePoints','selffeed',...
								'SelectionRadius',obj.CalcParameter.Sel_Radius,...
								'SelectedEvents',[selected],...
								'SelectionNumber',obj.CalcParameter.NumEvents,...
								'SelectionMode',SelMode,...
								'DistanceSend',true,...
								'ParallelCalc',obj.ParallelMode);
								
					[xRange,yRange,zRange,calcRes]=point3DGridder(obj.Datastore,McBvalCalc,CalcParam);		
				
					RawRes=cellArrayToPlane(calcRes);
					
					fields=fieldnames(RawRes);
					for i=1:numel(fields)
						obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
					end
				
				
				end
		    	end
		    	
		    %set FMDradius, it is easiest here	
		    obj.FMDRad=0.1*(max(obj.CalcRes.X)-min(obj.CalcRes.X));		
		
		
	end

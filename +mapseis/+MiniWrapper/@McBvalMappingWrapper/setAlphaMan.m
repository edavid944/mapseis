function setAlphaMan(obj)
	promptValues = {'Transparency Factor'};
	dlgTitle = 'Set Transparency (set to 1 for none)';
	
	Alpha=obj.AlphaVal;
				
	gParams = inputdlg(promptValues,dlgTitle,1,...
			cellfun(@num2str,{Alpha},'UniformOutput',false));
		
	if ~isempty(gParams)		
		AlphaMan = str2double(gParams{1});			
		obj.AlphaVal=AlphaMan;
						
		obj.updateGUI;
	end			
	
	
end

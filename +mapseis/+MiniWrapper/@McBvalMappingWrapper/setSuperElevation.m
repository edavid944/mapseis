function setSuperElevation(obj)
	
	promptValues = {'SuperElevation Factor'};
	dlgTitle = 'Set SuperElevation Factor';
	gParams = inputdlg(promptValues,dlgTitle,1,...
			cellfun(@num2str,{obj.TheAspect},'UniformOutput',false));
		
	if ~isempty(gParams)		
		obj.TheAspect=	str2double(gParams{1});					
		obj.updateGUI;
	end			
		
end
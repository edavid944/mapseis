function FMDSuicide(obj)
		%closes the GUI and deletes the filter
		
		for i=1:obj.FMDMax
			try
				obj.FMDPoly{i,1}.delete;
			end
		
		
			try
				%will be deleted in next update
				set(obj.FMDPoly{i,2},'visible','off')
			end
		end
		
		try
			close(obj.FMDWindow);
		end
		
		
		try	
			clear obj.FMDWindow;
		end
		
		try	
			clear obj.FMDFilter;
			obj.FMDFilter=cell(obj.FMDMax,1);;
		catch
			%at least empty the filterslot 
			obj.FMDFilter=cell(obj.FMDMax,1);;
		end
		
		obj.FMDNext=0;
		
	end

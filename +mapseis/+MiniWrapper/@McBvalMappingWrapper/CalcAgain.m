function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		EveryThingGood=true;
		
		
		if isempty(obj.Filterlist)
			warndlg('New filterlist will be set')
		end
		
		
		%get parameters
		obj.openCalcParamWindow;
		
		
		
		if obj.StartCalc
			
			%figNr=findobj('Name','b-value-map');
			
			%if ~isempty(figNr)
			%	close(figNr);
			%end
		
			%refresh catalogs
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ProfileLine=[];
			obj.ProfileWidth=[];
			%obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
						
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the current regionfilter');
					obj.ErrorDialog='No profile selected in the current regionfilter';
				 else
				 	regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					obj.ProfileWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					obj.ProfileLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				 
				 end
			end
			
			if EveryThingGood
				%Calculation
				obj.doTheCalcThing;
				obj.CalcRecurrence('MenuCheck');
				
				%check that no non existing field is selected
				if ~isfield(obj.CalcRes,obj.LastPlot{1})
					%set to b-value
					obj.LastPlot{1}='b_value';
					if ~obj.ThirdDimension
						obj.LastPlot{2}='colormap';
					else
						obj.LastPlot{2}='Volume3D';
					end
					
					obj.LastPlot{3}=[];
				end
				
				obj.updateGUI;
				
				%Build resultGUI
				%obj.BuildResultGUI
						
				%plot results
				%obj.plotResult('stress')
			end	
		end
	
	
	end

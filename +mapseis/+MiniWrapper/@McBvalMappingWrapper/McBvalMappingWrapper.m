classdef McBvalMappingWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		CalcParameter
		ProfileSwitch
		ThirdDimension
		ErrorDialog
		ParallelMode
		StartCalc
		Loaded
		CalcName
		Region
		AlphaMap
		ResultGUI
		PlotOptions
		RecSubMenu
		BorderToogle
		CoastToogle
		EQToogle
		ShadowToogle
		PlotQuality
		LastPlot
		HistoryExist
		TheAspect
		ProfileLine
		ProfileWidth
		Error2Alpha
		ErrorFactor
		FMDFilter
		FMDWindow
		FMDRad
		FMDPoly
		FMDNext
		FMDColors
		FMDMax
		IsoSurfers
		ColLimit
		ColLimitSwitch
		DrawCaps
		AlphaVal
		RoundIt
		SurferDude
		SliceIt
		SalamiConfig
		LightMyFire
		ShowItAll
	end

	events
		CalcDone

	end


	methods
		%constructor
		function obj = McBvalMappingWrapper(ListProxy,Commander,GUISwitch,CalcMode,CalcParameter)
			import mapseis.datastore.*;
					
			
			%In this constructor everything is set, and the 
			obj.ListProxy=ListProxy;
			
			if isempty(Commander)
				obj.Commander=[];
				%go into manul mode, the object will be created but everything
				%has to be done manually (useful for scripting)
			else
			
				obj.Commander = Commander; 
				obj.ParallelMode=obj.Commander.ParallelMode;
				obj.Region=[];
				obj.AlphaMap=[];
				obj.FMDMax=2;
				obj.FMDFilter=cell(obj.FMDMax,1);
				obj.FMDWindow=cell(obj.FMDMax,2);
				obj.FMDNext=0;
				obj.FMDRad=km2deg(5);
				obj.FMDColors={'r','b','g','c','m'};
				obj.FMDMax=2;
				obj.IsoSurfers=[];
				obj.DrawCaps=true;
				obj.AlphaVal=0.75;
				obj.TheAspect=2;
				obj.RoundIt=false;
				obj.SliceIt=false;
				obj.SalamiConfig=[];
				obj.SurferDude=true;
				obj.LightMyFire=true;
				obj.ShowItAll=false;
				obj.ColLimit=[];
				obj.ColLimitSwitch=[];
				
				%set calcmode
				switch CalcMode
					case '2Dmap'
						obj.ThirdDimension=false;
						obj.ProfileSwitch=false;
					case 'Profile'
						obj.ThirdDimension=false;
						obj.ProfileSwitch=true;
					case '3D'
						obj.ThirdDimension=true;
						obj.ProfileSwitch=false;
						obj.TheAspect=50;	
				end
				
			
							
				obj.Loaded=false;
				obj.Error2Alpha=false;
				obj.ErrorFactor=1;
				obj.ResultGUI=[];
				obj.PlotOptions=[];
				obj.BorderToogle=true;
				obj.CoastToogle=true;
				obj.EQToogle=false;
				obj.ShadowToogle=true;
				obj.PlotQuality = 'low';
				obj.LastPlot=[];
				obj.HistoryExist=false;
				
				obj.CalcName='b-value map';
				
				%get the current datastore and filterlist and build zmap catalog
				obj.Datastore = obj.Commander.getCurrentDatastore;
				obj.Filterlist = obj.Commander.getCurrentFilterlist; 
				obj.Keys=obj.Commander.getMarkedKey;
				selected=obj.Filterlist.getSelected;
				obj.SendedEvents=selected;
				obj.ProfileWidth=[];
				obj.ProfileLine=[];
				
				EveryThingGood=true;
				
				%check if old data exists
				try
					PreviousCalc=obj.Datastore.getUserData('new-bval-calc');
				catch
					PreviousCalc=[];
				end
				
				if ~isempty(PreviousCalc)
					obj.HistoryExist=true;
				end
				
				
				if obj.ProfileSwitch
					 regionFilter = obj.Filterlist.getByName('Region');
					 filterRegion = getRegion(regionFilter);
					 RegRange=filterRegion{1};	
					 if ~strcmp(RegRange,'line')
						EveryThingGood=false;
						errordlg('No profile selected in the regionfilter');
						obj.ErrorDialog='No profile selected in the regionfilter';
					 else
					 	regionFilter = obj.Filterlist.getByName('Region');
						filterRegion = getDepthRegion(regionFilter);
						pRegion = filterRegion{2};
						RegRange=filterRegion{1};
						obj.ProfileWidth=filterRegion{4};
						rBdry = pRegion.getBoundary();
						obj.ProfileLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
					 
					 end
					 
					 
					 
				end
				
				if EveryThingGood
					
					if ~isempty(CalcParameter)
						%Parameters are include, no default parameter will
						%be set
						obj.CalcParameter=CalcParameter;
					else
						obj.InitVariables
					end	
					
					if GUISwitch
						
						%let the user modify the parameters
						obj.openCalcParamWindow;
						
						if obj.StartCalc
							
							if ~obj.Loaded
								%Calc
								obj.doTheCalcThing;
							end
							
							%Build resultGUI
							obj.BuildResultGUI
							
							%plot results
							obj.plotResult('bvalue')
						end	
						
						
					end
					
	
				
				end
			end
			
		end
	
		
		%other Methods used in the Class
		%-------------------------------
		InitVariables(obj)
		

		openCalcParamWindow(obj)
		
		
		doTheCalcThing(obj)
		

		BuildResultGUI(obj)
		

		buildMenus(obj)
		

		buildMouseMenu(obj,graphobj)	
		

		Smoother(obj,DoWhat)	
		

		ColorRanger(obj,DoWhat)
		

		ErrorSwitcher(obj,DoWhat)
		

		TooglePlotOptions(obj,WhichOne)
		

		ToogleAddPlotOptions(obj,WhichOne)
		
		
		plotResult(obj,whichplot)
		

		plotVariable(obj,plotAxis,ValField,PlotType,AddParam)
		

		plotVariable3D(obj,plotAxis,ValField,PlotType,AddParam)
		

		updateGUI(obj)
		
		
		plot_modify(obj,bright,interswitch,circleGrid)
		
		
		adju(obj,asel, whichplot)
		
	
		universalCalc(obj,commandstring,newvariables)
		

		CalcAgain(obj)
		

		saveCalc(obj)
		

		showCalcParameter(obj)
		

		loadUpdate(obj,oldCalcs)
		

		loadCalc(obj,oldCalcs)
		

		setSuperElevation(obj)
		

		setqual(obj,qual)
		

		getFilterlist(obj)
		
		
		setAlphaMan(obj)
		

		setIsoSurfaces(obj)
		
		
		setSlices(obj)
		

		CalcRecurrence(obj,DoWhat)
		

		exportResult(obj,FileType,Filename)
		

		exportResult3D(obj,FileType,Filename) 
		


		%functions for the FMD window and picker
		%=======================================
		setFMDRadius(obj)
		

		renewPoly(obj,plotAxis)
		

		SetFMDRegion(obj,WhichOne)
		

		CreateFMDGUI(obj)
		

		UpdateFMD(obj)
		

		FMDSuicide(obj)
		

		FMDDelete(obj,PolyID)
		

		FMDfilterupdater(obj,pos,regionFilter)
		

		AddPolyMenu(obj,imobj,RegionFilter,FMD_ID,addhandle)
		

	end

	%static function 
	%---------------
	methods (Static)
		TheDateString=decyear2string(decYR)

		decimalYear=string2decyear(instring)
		
		TheEvalString = UnpackStruct(TheStruct)

		CellData = UnpackStruct2Cell(TheStruct)

		TheStruct = PackVariable(variablelist)

		TheStruct = PackAllVariable()

		
	end


end

function plotResult(obj,whichplot)
	%more of a relict dummy function 
	%it allows a bit less complicated plotting, more for scripting
	%it calls plotVariable
	
	
	if obj.ThirdDimension
		switch whichplot
			case 'bvalue'
				obj.plotVariable([],'b_value','Volume3D',[]);				
			case 'Mc'
				obj.plotVariable([],'Mc','Volume3D',[]);	

		end
		
	else
		goError='autoColor';
		switch whichplot
			case 'bvalue'
				obj.plotVariable([],'b_value',goError,'b_value_bootstrap');				
			case 'Mc'
				obj.plotVariable([],'Mc',goError,'Mc_bootstrap');	
	
		end
	end
	

end
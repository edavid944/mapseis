function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		if ~obj.ProfileSwitch&~obj.ThirdDimension;
			Title = 'b-value, a-value and Mc v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size','SmoothKernelSize';...
				'Smooth Kernel Sigma','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (deg):','dx';...
				'Grid spacing y (deg):','dy';...
				'Min. Num. > Mc:','MinNumber';...
				'Fixed Mc (only in "Fixed Mc"):','FixedMc';...			
				'Mc Correction:','McCorr';....
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 
				
				
				
							
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Number of Events','Constant Radius','Both'}
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			
			%Smoothing
			Formats(5,1).type='check';
			
			%Smoothing Kernel Size
			Formats(6,1).type='edit';
			Formats(6,1).format='integer';
			Formats(6,1).limits = [0 9999];
			
			%Smoothing Sigma
			Formats(6,2).type='edit';
			Formats(6,2).format='float';
			Formats(6,2).limits = [0 9999];
			
			%NumberEvents
			Formats(7,1).type='edit';
			Formats(7,1).format='integer';
			Formats(7,1).limits = [0 9999];
		
			%Radius
			Formats(7,2).type='edit';
			Formats(7,2).format='float';
			Formats(7,2).limits = [0 9999];
			
			%Grid space x
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 999];
			
			%Grid space y
			Formats(8,2).type='edit';
			Formats(8,2).format='float';
			Formats(8,2).limits = [0 999];
			
			%Min Number
			Formats(9,1).type='edit';
			Formats(9,1).format='float';
			Formats(9,1).limits = [0 9999];
			Formats(9,1).size = [-1 0];
			Formats(9,2).limits = [0 1];
			Formats(9,2).type='none';
			
			%Fixed Mc
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			
			%Mc Correction
			Formats(11,1).type='edit';
			Formats(11,1).format='float';
			Formats(11,1).limits = [-99 99];
			Formats(11,1).size = [-1 0];
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';
			
			%sign bval switch
			Formats(12,1).type='check';
			
			%overall b-val
			Formats(12,2).type='edit';
			Formats(12,2).format='float';
			Formats(12,2).limits = [0 9999];
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
			
				
		elseif obj.ProfileSwitch&~obj.ThirdDimension;
			
			Title = 'b-value, a-value and Mc v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size','SmoothKernelSize';...
				'Smooth Kernel Sigma','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (km):','dx';...
				'Grid spacing z (km):','dz';...
				'Min. Num. > Mc:','MinNumber';...
				'Fixed Mc (only in "Fixed Mc"):','FixedMc';...			
				'Mc Correction:','McCorr';...
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 
				
				
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Number of Events','Constant Radius','Both'}
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			
			%Smoothing
			Formats(5,1).type='check';
			
			%Smoothing Kernel Size
			Formats(6,1).type='edit';
			Formats(6,1).format='integer';
			Formats(6,1).limits = [0 9999];
			
			%Smoothing Sigma
			Formats(6,2).type='edit';
			Formats(6,2).format='float';
			Formats(6,2).limits = [0 9999];
			
			%NumberEvents
			Formats(7,1).type='edit';
			Formats(7,1).format='integer';
			Formats(7,1).limits = [0 9999];
		
			%Radius
			Formats(7,2).type='edit';
			Formats(7,2).format='float';
			Formats(7,2).limits = [0 9999];
			
			%Grid space x
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 999];
			
			%Grid space y
			Formats(8,2).type='edit';
			Formats(8,2).format='float';
			Formats(8,2).limits = [0 999];
			
			%Min Number
			Formats(9,1).type='edit';
			Formats(9,1).format='float';
			Formats(9,1).limits = [0 9999];
			Formats(9,1).size = [-1 0];
			Formats(9,2).limits = [0 1];
			Formats(9,2).type='none';
			
			%Fixed Mc
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			
			%Mc Correction
			Formats(11,1).type='edit';
			Formats(11,1).format='float';
			Formats(11,1).limits = [-99 99];
			Formats(11,1).size = [-1 0];
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';
			
			%sign bval switch
			Formats(12,1).type='check';
			
			%overall b-val
			Formats(12,2).type='edit';
			Formats(12,2).format='float';
			Formats(12,2).limits = [0 9999];
			
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
				
				
		  elseif ~obj.ProfileSwitch&obj.ThirdDimension;
			
			Title = 'b-value, a-value and Mc v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size','SmoothKernelSize';...
				'Smooth Kernel Sigma','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (deg):','dx';...
				'Grid spacing y (deg):','dy';...
				'Grid spacing z (km):','dz';...
				'Min. Num. > Mc:','MinNumber';...
				'Fixed Mc (only in "Fixed Mc"):','FixedMc';...			
				'Mc Correction:','McCorr';...
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 		
			
				
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			Formats(1,3).type='none';
			Formats(1,3).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='none'
			Formats(2,3).type='edit';
			Formats(2,3).format='integer';
			Formats(2,3).limits = [0 9999];
			
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Number of Events','Constant Radius','Both'}
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			Formats(4,3).limits = [0 1];
			Formats(4,3).type='none';
			
			
			%Smoothing
			Formats(5,1).type='check';
			
			%Smoothing Kernel Size
			Formats(5,2).type='edit';
			Formats(5,2).format='integer';
			Formats(5,2).limits = [0 9999];
			
			%Smoothing sigma
			Formats(5,3).type='edit';
			Formats(5,3).format='float';
			Formats(5,3).limits = [0 9999];
			
			%NumberEvents
			Formats(6,1).type='edit';
			Formats(6,1).format='integer';
			Formats(6,1).limits = [0 9999];
			
			Formats(6,2).limits = [0 1];
			Formats(6,2).type='none';
			
			%Radius
			Formats(6,3).type='edit';
			Formats(6,3).format='float';
			Formats(6,3).limits = [0 9999];
			
			%Grid space x
			Formats(7,1).type='edit';
			Formats(7,1).format='float';
			Formats(7,1).limits = [0 999];
			
			%Grid space y
			Formats(7,2).type='edit';
			Formats(7,2).format='float';
			Formats(7,2).limits = [0 999];
			
			%Grid space y
			Formats(7,3).type='edit';
			Formats(7,3).format='float';
			Formats(7,3).limits = [0 999];
			
			%Min Number
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 9999];
			Formats(8,1).size = [-1 0];
			Formats(8,2).limits = [0 1];
			Formats(8,2).type='none';
			Formats(8,3).limits = [0 1];
			Formats(8,3).type='none';
			
			%Fixed Mc
			Formats(9,1).type='edit';
			Formats(9,1).format='float';
			Formats(9,1).limits = [-99 99];
			Formats(9,1).size = [-1 0];
			Formats(9,2).limits = [0 1];
			Formats(9,2).type='none';
			Formats(9,3).limits = [0 1];
			Formats(9,3).type='none';
			
			%Mc Correction
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			Formats(10,3).limits = [0 1];
			Formats(10,3).type='none';
			
			%sign bval switch
			Formats(11,1).type='check';
			
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';
			
			%overall b-val
			Formats(11,3).type='edit';
			Formats(11,3).format='float';
			Formats(11,3).limits = [0 9999];
			
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
		end		
		
		
		

		
		if obj.HistoryExist
			Prompt(end+1,:)={'Load Calculation?','LoadSwitch'};
			
			
			if obj.ThirdDimension
				Formats(12,1).type='check';
				Formats(12,1).size = [-1 0];
				Formats(12,2).limits = [0 1];
				Formats(12,2).type='none';	
				Formats(12,3).limits = [0 1];
				Formats(12,3).type='none';
			else
				Formats(13,1).type='check';
				Formats(13,1).size = [-1 0];
				Formats(13,2).limits = [0 1];
				Formats(13,2).type='none';	
			
			end
			
		end
		
		
		
		
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,obj.CalcParameter,Options); 
		
		if obj.HistoryExist&Canceled==0&NewParameter.LoadSwitch==1
			%Load data
			obj.StartCalc=false;
			obj.loadCalc([]);
			
		else
			obj.CalcParameter=NewParameter;
			
			
			
			if Canceled==1
				obj.StartCalc=false;
			else
				obj.StartCalc=true;
			end
		
		end
		
		
	end

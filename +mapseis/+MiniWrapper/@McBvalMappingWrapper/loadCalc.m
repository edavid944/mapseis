function loadCalc(obj,oldCalcs)
	%asks if any of the old calcs should be loaded and loads it or build new one
	import mapseis.datastore.*;
	import mapseis.export.Export2Zmap;
	
	if isempty(oldCalcs)
		%check if old calculations are avialable
		try
			oldCalcs=obj.Datastore.getUserData('new-bval-calc');
		catch
			disp('no calculation available in this datastore')
			return;
		end
	end
	
	
	
	Title = 'Select Calculation';
	Prompt={'Which Calculation:', 'WhichOne'};
	
	AvailCalc=oldCalcs(:,1);			


	%Calc
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=AvailCalc;
	Formats(1,1).size = [-1 0];
	
	Formats(1,2).type='none';
	Formats(1,2).limits = [0 1];
	Formats(1,3).type='none';
	Formats(1,3).limits = [0 1];
	
	
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';

	
	
	%default values
	defval =struct('WhichOne',1);
		
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
	
	
	if Canceled==1
		obj.StartCalc=false;
		obj.Loaded=false;
		return;
	else
		obj.StartCalc=true;
		obj.Loaded=true;
	end
	
	%kill FMDfilters if needed
	obj.FMDSuicide;
	
	if obj.StartCalc
		TheCalc=oldCalcs{NewParameter.WhichOne,2};
		
		
		%Now set the data
		obj.Filterlist = [];
		%obj.Commander.getCurrentFilterlist; 
		obj.Keys=obj.Commander.getMarkedKey;
		obj.SendedEvents=TheCalc.Selected;
		obj.CalcParameter=TheCalc.CalcParameter;
		obj.ProfileSwitch=TheCalc.ProfileSwitch;
		obj.ThirdDimension=TheCalc.ThirdDimension;
		obj.CalcName=TheCalc.CalcName;
		obj.AlphaMap=TheCalc.AlphaMap;
		obj.CalcRes=TheCalc.CalcResult;
		
		
		
		
	end
	
	if strcmp(TheCalc.VersionCode,'v1.5')
		%add missing parameter if needed
		obj.CalcParameter.Overall_bval=1;
		obj.CalcParameter.Calc_sign_bval=0;
		MapSize=size(obj.CalcRes.Mc);
		
		obj.CalcRes.sign_bvalue=NaN(MapSize);
		obj.CalcRes.sign_WinningModel=NaN(MapSize);
		obj.CalcRes.sign_likelihood1=NaN(MapSize);
		obj.CalcRes.sign_likelihood2=NaN(MapSize);
		obj.CalcRes.sign_akaike1=NaN(MapSize);
		obj.CalcRes.sign_akaike2=NaN(MapSize);
		
		msgbox('Missing results were added as NaN')
		
	end
	
	if any(strcmp(TheCalc.VersionCode,{'v1.55','v1.5'}))
		obj.ProfileWidth=[];
		obj.ProfileLine=[];
	
	else
		obj.ProfileWidth=TheCalc.ProfileWidth;
		obj.ProfileLine=TheCalc.ProfileLine;
	
	end
	
	if isfield(TheCalc,'ColLimitSwitch')
		obj.ColLimitSwitch=TheCalc.ColLimitSwitch;
		obj.ColLimit=TheCalc.ColLimit;
	else
		obj.ColLimitSwitch=[];
		obj.ColLimit=[];
	end
	
	
	if isfield(obj.CalcParameter,'SmoothFactor')
		%remove old smoothing parameter and add new
		obj.CalcParameter=rmfield(obj.CalcParameter,'SmoothFactor');
		obj.CalcParameter.SmoothKernelSize=5;
		obj.CalcParameter.SmoothSigma=2.5;
		disp('Kernel Parameter set');
	else
		disp(obj.CalcParameter);
		if ~isfield(obj.CalcParameter,'SmoothKernelSize')
			obj.CalcParameter.SmoothKernelSize=5;
		end
		
		if ~isfield(obj.CalcParameter,'SmoothSigma')
			obj.CalcParameter.SmoothSigma=2,5;
		end
		
	end
	
	
	if ~isfield(obj.CalcParameter,'RecMag')
		obj.CalcParameter.RecMag=6;
	end
	
	
	try
		obj.IsoSurfers=TheCalc.IsoSurfers;
	end
	
	
	try
		obj.SalamiConfig=TheCalc.SliceConfig;
	end
	
	%reset FMDrad
	obj.FMDRad=0.1*(max(obj.CalcRes.X)-min(obj.CalcRes.X));
	
end

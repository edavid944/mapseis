function ErrorSwitcher(obj,DoWhat)
	%Turns the Error 2 Alpha on and off after calculation
	alphaMenu = findobj(obj.ResultGUI,'Label','Error to alpha channel');
	set(alphaMenu, 'Checked', 'off');
	
	
	switch DoWhat
		case 'switch'
			%just in case
			if isempty(obj.Error2Alpha)
				obj.Error2Alpha=false;
			end
			
			obj.Error2Alpha=~obj.Error2Alpha;
			
			if ~obj.Error2Alpha
				set(alphaMenu, 'Checked', 'off');
			else
				set(alphaMenu, 'Checked', 'on');
			end
			
			obj.updateGUI;
			
			
		case 'factor'
			
			promptValues = {'AlphaError Factor'};
			dlgTitle = 'Set Alpha Error Factor';
			gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{obj.ErrorFactor},'UniformOutput',false));
				
			if ~isempty(gParams)	
				obj.ErrorFactor=str2double(gParams{1});
				
				if obj.Error2Alpha==1
					obj.updateGUI;
					set(alphaMenu, 'Checked', 'on');
				end
			end
			
		case 'init'
			set(alphaMenu, 'Checked', 'off');
			if obj.Error2Alpha==1
				set(alphaMenu, 'Checked', 'on');
			end
			
	end

	
end

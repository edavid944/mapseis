function setSlices(obj)
		%allows to set the
		ValField=obj.LastPlot{1};
		
		if isfield(obj.SalamiConfig,ValField)
			CustomXSwitch=obj.SalamiConfig.(ValField){1,1};
			TheXSlice=obj.SalamiConfig.(ValField){1,2};
			CustomXSlice=obj.SalamiConfig.(ValField){1,3};
					
			CustomYSwitch=obj.SalamiConfig.(ValField){2,1};
			TheYSlice=obj.SalamiConfig.(ValField){2,2};
			CustomYSlice=obj.SalamiConfig.(ValField){2,3};
			
			CustomZSwitch=obj.SalamiConfig.(ValField){3,1};
			TheZSlice=obj.SalamiConfig.(ValField){3,2};
			CustomZSlice=obj.SalamiConfig.(ValField){3,3};
		else
			errordlg('No data availalbe');
			return;
		end
		
		if ~isempty(TheXSlice);
			minValX=num2str(TheXSlice(1));
			maxValX=num2str(TheXSlice(2));
			SpacingX=num2str(TheXSlice(3));
		else
			minValX='';
			maxValX='';
			SpacingX='';
		end
		
		
		if ~isempty(TheYSlice);
			minValY=num2str(TheYSlice(1));
			maxValY=num2str(TheYSlice(2));
			SpacingY=num2str(TheYSlice(3));
		else
			minValY='';
			maxValY='';
			SpacingY='';
		end
		
		
		if ~isempty(TheZSlice);
			minValZ=num2str(TheZSlice(1));
			maxValZ=num2str(TheZSlice(2));
			SpacingZ=num2str(TheZSlice(3));
		else
			minValZ='';
			maxValZ='';
			SpacingZ='';
		end
		
		Title = 'Set Slices';
		Prompt={'Use Custom Slices X:', 'CustomXSwitch';...
			'Min. Value X:','minValX';...
			'Max. Value X:','maxValX';...
			'Surface Spacing X:','SpacingX';...
			'Custom Slices X;','CustomXSlice';...
			'Use Custom Slices Y:', 'CustomYSwitch';...
			'Min. Value Y:','minValY';...
			'Max. Value Y:','maxValY';...
			'Surface Spacing Y:','SpacingY';...
			'Custom Slices Y;','CustomYSlice';...
			'Use Custom Slices Z:', 'CustomZSwitch';...
			'Min. Value Z:','minValZ';...
			'Max. Value Z:','maxValZ';...
			'Surface Spacing Z:','SpacingZ';...
			'Custom Slices Z;','CustomZSlice'};
			
							
		defvals=struct(	'CustomXSwitch',CustomXSwitch,...
				'minValX',minValX,...
				'maxValX',maxValX,...
				'SpacingX',SpacingX,...
				'CustomXSlice',num2str(CustomXSlice),...
				'CustomYSwitch',CustomYSwitch,...
				'minValY',minValY,...
				'maxValY',maxValY,...
				'SpacingY',SpacingY,...
				'CustomYSlice',num2str(CustomYSlice),...
				'CustomZSwitch',CustomZSwitch,...
				'minValZ',minValZ,...
				'maxValZ',maxValZ,...
				'SpacingZ',SpacingZ,...
				'CustomZSlice',num2str(CustomZSlice));
			
		%CustomX?
		Formats(1,1).type='check';
		%Formats(1.1).size = -1;
		Formats(1,2).limits = [0 1];
		Formats(1,2).type='none';
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%min X
		Formats(2,1).type='edit';
		Formats(2,1).format='text';
		Formats(2,1).limits = [0 1];
		
		%max X
		Formats(2,2).type='edit';
		Formats(2,2).format='text';
		Formats(2,2).limits = [0 1];
			
		%Spacing X
		Formats(2,3).type='edit';
		Formats(2,3).format='text';
		Formats(2,3).limits = [0 1];
			
		%Custom X
		Formats(3,1).type='edit';
		Formats(3,1).format='text';
		Formats(3,1).size=-1;
		Formats(3,1).limits = [0 1];
		Formats(3,2).limits = [0 1];
		Formats(3,2).type='none';
		Formats(3,3).type='none';
		Formats(3,3).limits = [0 1];
		
		
		
		%CustomY?
		Formats(4,1).type='check';
		%Formats(4.1).size = -1;
		Formats(4,2).limits = [0 1];
		Formats(4,2).type='none';
		Formats(4,3).type='none';
		Formats(4,3).limits = [0 1];
		
		%min Y
		Formats(5,1).type='edit';
		Formats(5,1).format='text';
		Formats(5,1).limits = [0 1];
		
		%max y
		Formats(5,2).type='edit';
		Formats(5,2).format='text';
		Formats(5,2).limits = [0 1];
			
		%Spacing Y
		Formats(5,3).type='edit';
		Formats(5,3).format='text';
		Formats(5,3).limits = [0 1];
			
		%Custom Y
		Formats(6,1).type='edit';
		Formats(6,1).format='text';
		Formats(6,1).size=-1;
		Formats(6,1).limits = [0 1];
		Formats(6,2).limits = [0 1];
		Formats(6,2).type='none';
		Formats(6,3).type='none';
		Formats(6,3).limits = [0 1];
		
		%CustomY?
		Formats(7,1).type='check';
		%Formats(7.1).size = -1;
		Formats(7,2).limits = [0 1];
		Formats(7,2).type='none';
		Formats(7,3).type='none';
		Formats(7,3).limits = [0 1];
		
		%min Y
		Formats(8,1).type='edit';
		Formats(8,1).format='text';
		Formats(8,1).limits = [0 1];
		
		%max y
		Formats(8,2).type='edit';
		Formats(8,2).format='text';
		Formats(8,2).limits = [0 1];
			
		%Spacing Y
		Formats(8,3).type='edit';
		Formats(8,3).format='text';
		Formats(8,3).limits = [0 1];
			
		%Custom X
		Formats(9,1).type='edit';
		Formats(9,1).format='text';
		Formats(9,1).size=-1;
		Formats(9,1).limits = [0 1];
		Formats(9,2).limits = [0 1];
		Formats(9,2).type='none';
		Formats(9,3).type='none';
		Formats(9,3).limits = [0 1];
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
			
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defvals,Options); 
		
		if ~Canceled==1
			
			if ~isempty(NewParameter.minValX)&~isempty(NewParameter.maxValX)&~isempty(NewParameter.SpacingX)
				XSlice=[str2num(NewParameter.minValX), str2num(NewParameter.maxValX), str2num(NewParameter.SpacingX)];
			else
				XSlice=[];
			end
			
			
			if ~isempty(NewParameter.minValY)&~isempty(NewParameter.maxValY)&~isempty(NewParameter.SpacingY)
				YSlice=[str2num(NewParameter.minValY), str2num(NewParameter.maxValY), str2num(NewParameter.SpacingY)];
			else
				YSlice=[];
			end
			
			
			if ~isempty(NewParameter.minValZ)&~isempty(NewParameter.maxValZ)&~isempty(NewParameter.SpacingZ)
				ZSlice=[str2num(NewParameter.minValZ), str2num(NewParameter.maxValZ), str2num(NewParameter.SpacingZ)];
			else
				ZSlice=[];
			end
			
			
		
			obj.SalamiConfig.(ValField){1,1}=logical(NewParameter.CustomXSwitch);
			obj.SalamiConfig.(ValField){1,2}=XSlice;
			obj.SalamiConfig.(ValField){1,3}=str2num(NewParameter.CustomXSlice);
				
			obj.SalamiConfig.(ValField){2,1}=logical(NewParameter.CustomYSwitch);
			obj.SalamiConfig.(ValField){2,2}=YSlice;
			obj.SalamiConfig.(ValField){2,3}=str2num(NewParameter.CustomYSlice);
			
			obj.SalamiConfig.(ValField){3,1}=logical(NewParameter.CustomZSwitch);
			obj.SalamiConfig.(ValField){3,2}=ZSlice;
			obj.SalamiConfig.(ValField){3,3}=str2num(NewParameter.CustomZSlice);
		
						
			obj.updateGUI;
		
		
		end
		
		
	end
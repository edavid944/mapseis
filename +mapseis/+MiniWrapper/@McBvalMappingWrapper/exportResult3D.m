function exportResult3D(obj,FileType,Filename) 
	%saves the currently plotted result into a mat or ascii file (3D version)
	%Comment: the fileparts command would not always be necessary, but 
	%makes the code more consistent (case with multiple files) and it
	%is time critical)
	
	if nargin<3
		Filename=[];
	end
	
	%get data
	if ~isempty(obj.LastPlot)&~strcmp(FileType,'full_result')
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		ydir=obj.CalcRes.Z;
		ResValue=obj.CalcRes.(obj.LastPlot{1})';
	elseif isempty(obj.LastPlot)
		return
	end

	
	switch FileType
		case 'matlab'
			if isempty(Filename)
				%get filename
				[fName,pName] = uiputfile('*.mat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
			
			if ~isempty(fName)		
				%save
				save(fullfile(pName,fName),'xdir','ydir','zdir','ResValue');
			end
			
		case 'ascii'
			if isempty(Filename)
				%get filename
				[fName,pName] = uiputfile('*.dat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
		
			if ~isempty(fName)
				%save
				save(fullfile(pName,fName),'xdir','ydir','zdir','ResValue','-ascii');
			end
		
		case 'ascii_trio'
			if isempty(Filename)
				%get filename
				[fName,pName] = uiputfile('*.dat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
		
			if ~isempty(fName)
				%save
				save(fullfile(pName,['Xdir_',fName]),'xdir','-ascii');
				save(fullfile(pName,['Ydir_',fName]),'ydir','-ascii');
				save(fullfile(pName,['Zdir_',fName]),'zdir','-ascii');
				save(fullfile(pName,['Value_',fName]),'ResValue','-ascii');
			end
			
		case 'full_result'
			FullCalc=obj.CalcRes;
		
			if isempty(Filename)
				[fName,pName] = uiputfile('*.mat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
		
			if ~isempty(FullCalc)&~isempty(fName)
				save(fullfile(pName,fName),'FullCalc');
			end	

	end
  
end


function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itselectorf
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end

function exportResult(obj,FileType,Filename)
	%saves the currently plotted result into a mat or ascii file
	%Comment: the fileparts command would not always be necessary, but 
	%makes the code more consistent (case with multiple files) and it
	%is time critical)
	
	if nargin<3
		Filename=[];
	end
	
	if obj.ThirdDimension
		%Just for making the function not to messy
		obj.exportResult3D(FileType,Filename);
		return
	end
	
	%get data
	if ~isempty(obj.LastPlot)&~strcmp(FileType,'full_result')
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		ResValue=obj.CalcRes.(obj.LastPlot{1})';
	
	elseif isempty(obj.LastPlot)
		return
	end
	
	
	switch FileType
		case 'matlab'
			if isempty(Filename)
				%get filename
				[fName,pName] = uiputfile('*.mat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
			
			if ~isempty(fName)
				%save
				save(fullfile(pName,fName),'xdir','ydir','ResValue');
			end	
			
		case 'ascii'
			if isempty(Filename)
				%get filename
				[fName,pName] = uiputfile('*.dat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
			
			if ~isempty(fName)
				%save
				save(fullfile(pName,fName),'xdir','ydir','ResValue','-ascii');
			end
			
		case 'ascii_trio'
			if isempty(Filename)
				%get filename
				[fName,pName] = uiputfile('*.dat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
			
			if ~isempty(fName)
				%save
				save(fullfile(pName,['Xdir_',fName]),'xdir','-ascii');
				save(fullfile(pName,['Ydir_',fName]),'ydir','-ascii');
				save(fullfile(pName,['Value_',fName]),'ResValue','-ascii');
			end
			
			
		case 'full_result'
			FullCalc=obj.CalcRes;
			if isempty(Filename)
				[fName,pName] = uiputfile('*.mat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
		
			
			if ~isempty(FullCalc) & ~isempty(pName)
				save(fullfile(pName,fName),'FullCalc');
			end
	end
  
end
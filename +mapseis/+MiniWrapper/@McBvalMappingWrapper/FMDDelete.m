function FMDDelete(obj,PolyID)
        	%Similar to FMDSuicide but does only delete on of the FMDs
        	import mapseis.util.emptier;
        	
        	
        	if nargin<2
        		obj.FMDSuicide;
        	else
        	
        	
			try
				obj.FMDPoly{PolyID,1}.delete;
			end
			
			
			try
				%will be deleted in next update
				set(obj.FMDPoly{PolyID,2},'visible','off')
			end
			
			
			try
				%at least empty the filterslot 
				obj.FMDFilter{PolyID}=[];
			end
			
			obj.FMDNext=PolyID-1;
			
			%Close window if nothing left
			if all(emptier(obj.FMDFilter))
				disp('No FMD left')
				try
					close(obj.FMDWindow);
				end
				
				
				try	
					clear obj.FMDWindow;
				end
				
				obj.FMDNext=0;
			end
			
			obj.UpdateFMD;
        	
		end
          
end
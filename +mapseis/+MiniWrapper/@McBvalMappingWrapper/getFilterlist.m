function getFilterlist(obj)
		%meant for reapplying filterlist in Profilemode (needed with old saves)
		
		%ask if your sure
		button = questdlg('Reload current Filterlist from Commander?')
		if strcmp(button,'Yes')
			
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			TheKeys=obj.Commander.getMarkedKey;
			obj.Keys.ShownFilterID=TheKeys.ShownFilterID;
			
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			try
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				obj.ProfileWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				obj.ProfileLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
			catch
				obj.ProfileWidth=[];
				obj.ProfileLine=[];
				disp('Profile available');
			end	
				
		end	
		
		
	end
	

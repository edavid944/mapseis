function saveCalc(obj)
	%This will save the calculation in the datastore and ask to open it next time
	%The filterlist will be saved within the datastore with this and reopened next time
	%when the calculation is loaded. It has to be done in this way or else nobody knows 
	%what filtersetting was used in the calculation.
	
	%Ask for a name
	gParams = inputdlg('Calculation Name','Name of the Calculation',1,{obj.CalcName});
	if ~isempty(gParams)
		obj.CalcName=gParams{1};
		
		%New Save algorithm without filterlist
		SaveStruct=struct(	'Selected',obj.SendedEvents,...
					'CalcResult',obj.CalcRes,...
					'CalcParameter',obj.CalcParameter,...
					'ProfileSwitch',obj.ProfileSwitch,...
					'ThirdDimension',obj.ThirdDimension,...
					'CalcName',obj.CalcName,...
					'AlphaMap',obj.AlphaMap,...
					'ProfileWidth',obj.ProfileWidth,...
					'ProfileLine',obj.ProfileLine,...
					'IsoSurfers',obj.IsoSurfers,...
					'SliceConfig',obj.SalamiConfig,...
					'ColLimitSwitch',obj.ColLimitSwitch,...
					'ColLimit',obj.ColLimit,...
					'VersionCode','v1.56');
					
		
					
		CalcObject={obj.CalcName,SaveStruct};
		
		%try getting the CalcObject from the datastore
		
		try 
			Calculations=obj.Datastore.getUserData('new-bval-calc');
			
		catch
			Calculations={};
		end
		
		if ~isempty(Calculations)
			%try finding the calc
				founded=strcmp(Calculations(:,1),obj.CalcName);
				if sum(founded)>0
					Calculations(founded,:)=CalcObject;
				else
					Calculations(end+1,:)=CalcObject;
				end	
		else
			Calculations(1,:)=CalcObject;
		end	
		
		obj.Datastore.setUserData('new-bval-calc',Calculations);
		
	end
	
	
	
	%write to the Commander (always do this even if canceled, this will update
	%the buffers)
	obj.Commander.pushWithKey(obj.Keys.ShownDataID,obj.Datastore);
	obj.Commander.switcher('Cata');
	
end

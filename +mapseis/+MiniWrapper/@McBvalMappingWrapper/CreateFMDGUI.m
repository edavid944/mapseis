function CreateFMDGUI(obj)
		%creates the gui if needed or updates it
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		
		if ~isempty(obj.FMDWindow)
			%if clf would be called with empty obj.FMDWindow
			%it would deform to clf([])=clf() and clear every figure
			%in function focus.
			try
				%window exists and is fine
				clf(obj.FMDWindow);
			end
			
			if ~ishandle(obj.FMDWindow)
				obj.FMDWindow=[];
			end	
		end
		
		 obj.FMDWindow = onePanelLayout_mk2(obj.FMDWindow,'FMD-sidekick',...
		   {'TickDir','out','color','r','FontWeight','bold','FontSize',12});
		   set(obj.FMDWindow,'Position',[ 400 200 800 600 ],'visible','on');

		
		FMDAxis=gca;
		set(FMDAxis,'Tag','FMDResultAxis','Color','w');
		   
		%build button
		updateButton = uicontrol(obj.FMDWindow, 'Units', 'Normalized',...
                		'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update FMD',...
                		'Callback',@(s,e) obj.UpdateFMD);
		set(obj.FMDWindow,'DeleteFcn',@(s,e) obj.FMDSuicide);
                		
                		
	end

function ToogleAddPlotOptions(obj,WhichOne)
	%This functions sets the toogles in this modul
	%WhichOne can be the following strings:
	%	'Border':	Borderlines
	%	'Coast'	:	Coastlines
	%	'-chk'	:	This will set all checks in the menu
	%			to the in toggles set state.
	

	ShadowMenu = findobj(obj.PlotOptions,'Label','Draw Shadows');
	CapsMenu= findobj(obj.PlotOptions,'Label','Draw isocaps');
	RoundMenu= findobj(obj.PlotOptions,'Label','Round values');
	SurfMenu= findobj(obj.PlotOptions,'Label','Draw IsoSurfaces');
	SliceMenu= findobj(obj.PlotOptions,'Label','Draw Slices');
	LightMenu= findobj(obj.PlotOptions,'Label','Use Lighting');
	
	switch WhichOne
		case '-chk'

			if obj.ShadowToogle
				set(ShadowMenu, 'Checked', 'on');
			else
				set(ShadowMenu, 'Checked', 'off');
			end

			
			if obj.DrawCaps
				set(CapsMenu, 'Checked', 'on');
			else
				set(CapsMenu, 'Checked', 'off');
			end
			
			
			if obj.RoundIt
				set(RoundMenu, 'Checked', 'on');
			else
				set(RoundMenu, 'Checked', 'off');
			end
			
			
			if obj.SurferDude
				set(SurfMenu, 'Checked', 'on');
			else
				set(SurfMenu, 'Checked', 'off');
			end
			
			
			if obj.SliceIt
				set(SliceMenu, 'Checked', 'on');
			else
				set(SliceMenu, 'Checked', 'off');
			end
			
			
			if obj.LightMyFire
				set(LightMenu, 'Checked', 'on');
			else
				set(LightMenu, 'Checked', 'off');
			end
			
			
			
			
		case 'Shadow'
			
			obj.ShadowToogle=~obj.ShadowToogle;
			
			if obj.ShadowToogle
				set(ShadowMenu, 'Checked', 'on');
			else
				set(ShadowMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
			
		case 'Caps'
			obj.DrawCaps=~obj.DrawCaps;
			
			if obj.DrawCaps
				set(CapsMenu, 'Checked', 'on');
			else
				set(CapsMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
			
		case 'Rounder'
			obj.RoundIt=~obj.RoundIt;
			
			if obj.RoundIt
				set(RoundMenu, 'Checked', 'on');
			else
				set(RoundMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
			
		case 'Surfer'
			obj.SurferDude=~obj.SurferDude;
			
			if obj.SurferDude
				set(SurfMenu, 'Checked', 'on');
			else
				set(SurfMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;	
			
		case 'Slicer'
			obj.SliceIt=~obj.SliceIt;
			
			if obj.SliceIt
				set(SliceMenu, 'Checked', 'on');
			else
				set(SliceMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;	
		
		case 'LightUp'
			obj.LightMyFire=~obj.LightMyFire;
			if obj.LightMyFire
				set(LightMenu, 'Checked', 'on');
				lighting phong;
			else
				set(LightMenu, 'Checked', 'off');
				lighting none;
				
			end
		
	end
	
	
end
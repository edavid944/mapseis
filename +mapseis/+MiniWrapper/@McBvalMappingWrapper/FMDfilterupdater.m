function FMDfilterupdater(obj,pos,regionFilter)
		import mapseis.region.*;
		import mapseis.Filter.*;
		import mapseis.util.*;
		
		%special version of the filter updater	 
	
	
		%Uses the input from imroi type objects and updates the filter 
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange = filterRegion{1};
	
		switch RegRange
						
			case {'in','out'}
				newRegion = Region(pos);
				regionFilter.setRegion(RegRange,newRegion);
	
				
			case 'circle'		
						
				if ~obj.ProfileSwitch
					regionFilter.setRadius(obj.FMDRad);
				else
					regionFilter.setRadius(deg2km(obj.FMDRad));
				end
						
						
				%dataStore.setUserData('gridPars',newgrid);
							
				%update filter
				newRegion = [pos(1),pos(2)];
				regionFilter.setRegion(RegRange,newRegion);
							
			case 'closeIt'
				obj.FMDSuicide;
		  end	
end

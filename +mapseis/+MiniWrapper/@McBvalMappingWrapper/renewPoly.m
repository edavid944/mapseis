function renewPoly(obj,plotAxis)
		import mapseis.plot.*;
		import mapseis.util.emptier;
		
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		
		AnyEmpty=emptier(obj.FMDFilter);
		
		
		
		if ~all(AnyEmpty)
		
			%get the used filters
			UsedSlots=find(AnyEmpty==0);
			
			for i=UsedSlots'
		
				filterRegion = getRegion(obj.FMDFilter{i});
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if isobject(pRegion)
					rBdry = pRegion.getBoundary();	
				else 
					rBdry = pRegion;
				end
				
						
				switch RegRange
					case {'in','out'}
						selection=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
						selection.addNewPositionCallback(@(p) ...
								FMDfilterupdater(obj,p,obj.FMDFilter{i}));
						selection.setColor(obj.FMDColors{i});		
						%set the menu point depth slice to 'off'
						
						%add menus to the object
						%SetImMenu(obj,selection,regionFilter,[]);
							
						obj.FMDPoly{i,1}=selection;
						
						obj.AddPolyMenu(selection,obj.FMDFilter{i},i,[]);
						
						
					case 'circle'
						rBdry(3)=obj.FMDFilter{i}.Radius;
						
						
						
						%draw circle
						hold on
						handle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
						selection = impoint(plotAxis,rBdry(1:2));
						hold off
						
						%selection.setFixedAspectRatioMode(1)
						selection.addNewPositionCallback(@(p) ...
								FMDfilterupdater(obj,[p(1),p(2),rBdry(3)],...
								obj.FMDFilter));
						
						selection.setColor(obj.FMDColors{i});
						set(handle(1),'Color',obj.FMDColors{i});
						
						obj.FMDPoly{i,1}=selection;
						obj.FMDPoly{i,2}=handle;
						obj.AddPolyMenu(selection,obj.FMDFilter{i},i,handle);
						
						%add menus to the object
						%SetImMenu(obj,selection,obj.FMDFilter,handle);
					
						
					case 'closeIt'
						%should never happen, because it should be already killed
						obj.FMDSuicide;	
						
						
				end
				
				hold on;
				
				
                	end
		end
	
	
	end

function SetFMDRegion(obj,WhichOne)
		import mapseis.plot.*;
         	import mapseis.filter.*;
		import mapseis.util.*;
		import mapseis.region.*;
		import mapseis.projector.*;
		import mapseis.util.gui.SetImMenu;

		if strcmp(WhichOne,'closeIt')
         		obj.FMDSuicide;	
		else
			%"find" next free slot
			AnyEmpty=emptier(obj.FMDFilter);
			obj.FMDNext=mod(obj.FMDNext+1,obj.FMDMax)+1;
			
			if ~isempty(obj.FMDFilter{obj.FMDNext})&any(AnyEmpty)
				disp('Check for free slots')
				%slot is not empty and there is an alternative
				freeOnes=find(AnyEmpty==1)
				obj.FMDNext=freeOnes(1);
				
				
			end
			
			
		
			%Check if FMD filter is existing and if it is the right one
			if isempty(obj.FMDFilter{obj.FMDNext})
				if obj.ProfileSwitch&~obj.ThirdDimension
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter{obj.FMDNext}=ProfileRegionFilter('all',...
						profLine,ProfWidth,obj.SendedEvents,[],'ProfileRegion');
					
					
				elseif ~obj.ProfileSwitch&~obj.ThirdDimension
					obj.FMDFilter{obj.FMDNext}=RegionFilter('all',[],'Region');
					
					
					
				else
					%not supported at the moment
				
				end
			else
			
				%check if the filter is right for the purpose
				if obj.ProfileSwitch&~obj.FMDFilter{obj.FMDNext}.isProfileFilter
					%wrong filter, change
					clear obj.FMDFilter{obj.FMDNext};
					obj.FMDFilter{obj.FMDNext}=[];
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter{obj.FMDNext}=ProfileRegionFilter('all',...
						profLine,ProfWidth,obj.SendedEvents,[],'ProfileRegion');
						
					
				elseif obj.ProfileSwitch&obj.FMDFilter{obj.FMDNext}.isProfileFilter
					%right filter but may needs correction
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter{obj.FMDNext}.setProfile(profLine,ProfWidth);
					obj.FMDFilter{obj.FMDNext}.setMasterSelection(obj.SendedEvents);
					
					
				elseif ~obj.ProfileSwitch&obj.FMDFilter{obj.FMDNext}.isProfileFilter	
					%also wrong filter
					clear obj.FMDFilter{obj.FMDNext};
					obj.FMDFilter{obj.FMDNext}=[];
					obj.FMDFilter{obj.FMDNext}=RegionFilter('all',[],'Region');
				end	
			end
			
			
			
		 
			dataStore = obj.Datastore;
			
			
			
			
		
			plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
					
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
			
			
			
			%Set color
			CurCol=obj.FMDColors{obj.FMDNext};
			
			switch WhichOne
				case {'in','out'}
				    	axes(plotAxis);
				    	obj.FMDPoly{obj.FMDNext,1}=impoly;
				    	newRegion = Region(obj.FMDPoly{obj.FMDNext,1}.getPosition);
				   
				    
				    	obj.FMDPoly{obj.FMDNext,1}.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,p,obj.FMDFilter{obj.FMDNext}));
                		    	
                			obj.FMDPoly{obj.FMDNext,1}.setColor(CurCol);
                			obj.FMDFilter{obj.FMDNext}.setRegion(WhichOne,newRegion);
				    
				    
				
				
			
				case {'circle'}
					%creates a circle for the selection
					
					axes(plotAxis);
					circ=Ginput(1);%polyg.getPosition;
					
					%correct the position to the lower left corner (imellipse)
					
					if ~obj.ProfileSwitch
						obj.FMDFilter{obj.FMDNext}.setRadius(obj.FMDRad);
					else
						obj.FMDFilter{obj.FMDNext}.setRadius(deg2km(obj.FMDRad));
					end
					
					newRegion = [circ(1),circ(2)];
					
										
					hold on
					handle=drawCircle(plotAxis,true,circ(1:2),obj.FMDRad);
            				selection = impoint(plotAxis,circ(1:2));
					hold off
					
            				selection.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,[circ(1),circ(2),obj.FMDRad],...
                					obj.FMDFilter{obj.FMDNext}));
            				
                			selection.setColor(CurCol);
                			
                			obj.FMDPoly{obj.FMDNext,1}=selection;
                			obj.FMDPoly{obj.FMDNext,2}=handle;
            				
					obj.FMDFilter{obj.FMDNext}.setRegion(WhichOne,newRegion);
				
			    end
			    
			% Run the filter
			obj.FMDFilter{obj.FMDNext}.execute(obj.Datastore);

			obj.CreateFMDGUI;
			obj.UpdateFMD;
			
         	end

	end
	
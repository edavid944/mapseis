function ColorRanger(obj,DoWhat)
	%Turns the smoothing on and off after calculation
	ColMenu = findobj(obj.ResultGUI,'Label','Color Axis Limited');
	set(ColMenu, 'Checked', 'off');
	
	
			
	switch DoWhat
		case 'switch'
			ValField=obj.LastPlot{1};
			
			%add fields if needed
			if isempty(obj.ColLimitSwitch)
				obj.ColLimitSwitch.(ValField)=true;
				obj.ColLimit.(ValField)=[];
			end
			
			if ~isfield(obj.ColLimitSwitch,ValField)
				obj.ColLimitSwitch.(ValField)=true;
				obj.ColLimit.(ValField)=[];
			
			end
			
			
			%It would be possible to directly change the color, but the 
			%axis are not really known
			if obj.ColLimitSwitch.(ValField)
				obj.ColLimitSwitch.(ValField)=false;
				set(ColMenu, 'Checked', 'off');
			else
				obj.ColLimitSwitch.(ValField)=true;
				set(ColMenu, 'Checked', 'on');
			end
			
			obj.updateGUI;
			
			
		case 'factor'
			ValField=obj.LastPlot{1};
			
			%add fields if needed
			if isempty(obj.ColLimitSwitch)
				obj.ColLimitSwitch.(ValField)=true;
				obj.ColLimit.(ValField)=[];
			end
			
			if ~isfield(obj.ColLimitSwitch,ValField)
				obj.ColLimitSwitch.(ValField)=true;
				obj.ColLimit.(ValField)=[];
			
			end
			
			
			promptValues = {'min. color Value','max. color Value '};
			dlgTitle = 'Set Color Axis Limits';
			if ~isempty(obj.ColLimit.(ValField))
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.ColLimit.(ValField)(1),obj.ColLimit.(ValField)(2)},'UniformOutput',false));
			else
				%probalby never happens
				gParams = inputdlg(promptValues,dlgTitle,1,{'',''});
			end
			
			if isempty(gParams)
				return
			end
			
			obj.ColLimit.(ValField)(1)=str2double(gParams{1});
			obj.ColLimit.(ValField)(2)=str2double(gParams{2});
			
			
			%always set to on
			obj.ColLimitSwitch.(ValField)=true;
			set(ColMenu, 'Checked', 'on');
			obj.updateGUI;
			
			
		case 'init'
			set(ColMenu, 'Checked', 'off');
			if ~isempty(obj.LastPlot)
				ValField=obj.LastPlot{1};
				if obj.ColLimitSwitch.(ValField)
					set(ColMenu, 'Checked', 'on');
				end
			end
	end

	
end
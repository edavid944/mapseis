function buildMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
	
		goError='autoColor';
		goErrorbi='bi_autoColor';
		
		
		if ~obj.ThirdDimension	
		
		
			%first the general menus (similar for both)
			OptionMenu = uimenu( obj.ResultGUI,'Label','- Data');
			uimenu(OptionMenu,'Label','Calculate again',...
			'Callback',@(s,e) obj.CalcAgain);
			uimenu(OptionMenu,'Label','Show calculation parameters',...
			'Callback',@(s,e) obj.showCalcParameter);
			uimenu(OptionMenu,'Label','Reload Filterlist from Commander',...
			'Callback',@(s,e) obj.getFilterlist);
			uimenu(OptionMenu,'Label','Write to Datastore','Separator','on',... 
			'Callback',@(s,e) obj.saveCalc);
			uimenu(OptionMenu,'Label','Load from Datastore',...
			'Callback',@(s,e) obj.loadUpdate([]));
			uimenu(OptionMenu,'Label','export Result (.mat)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu(OptionMenu,'Label','export Result (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu(OptionMenu,'Label','export Result (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu(OptionMenu,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
			%Plot option menu, allows to selected plot options like plotting earthquakes
			%coast- & borderlines
			obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
			
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			
			if obj.ProfileSwitch
				uimenu(obj.PlotOptions,'Label','Set superelevation',... 
				'Callback',@(s,e) obj.setSuperElevation);
			end
			
			uimenu(obj.PlotOptions,'Label','Set FMD selection radius',... 
				'Callback',@(s,e) obj.setFMDRadius);
			
			uimenu(obj.PlotOptions,'Label','plot Coastlines','Separator','on',... 
			'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
			 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
			'Callback',@(s,e) obj.TooglePlotOptions('Border'));
			 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
			'Callback',@(s,e) obj.TooglePlotOptions('EQ'));	
			uimenu(obj.PlotOptions,'Label','Plot Full Bounding Box',...
			'Callback',@(s,e) obj.TooglePlotOptions('ShowIt'));
			uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			
			uimenu(obj.PlotOptions,'Label','Color Axis Limited','Separator','on',... ...
			'Callback',@(s,e) obj.ColorRanger('switch'));
			uimenu(obj.PlotOptions,'Label','Set Color Axis',...
			'Callback',@(s,e) obj.ColorRanger('factor'));
			
			uimenu(obj.PlotOptions,'Label','Error to alpha channel','Separator','on',...
			'Callback',@(s,e) obj.ErrorSwitcher('switch'));
			uimenu(obj.PlotOptions,'Label','Error-Alpha Factor',...
			'Callback',@(s,e) obj.ErrorSwitcher('factor'));
			
			%uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
			%'Callback',@(s,e) obj.plot_modify([],'interp',[]));
			%uimenu(obj.PlotOptions,'Label','Shading Flat',... 
			%'Callback',@(s,e) obj.plot_modify([],'flat',[]));
			uimenu(obj.PlotOptions,'Label','Brighten +0.4','Separator','on',...  
			'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
			 uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
			'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
			
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.setqual('chk');
			
				
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','b-value map (max likelihood)',...
			'Callback',@(s,e) obj.plotVariable([],'b_value',goError,'b_value_bootstrap'));
			 uimenu(MapMenu,'Label','Standard deviation of b-Value (max likelihood) map',...
			'Callback',@(s,e) obj.plotVariable([],'b_value_bootstrap','colormap'));
			uimenu(MapMenu,'Label','Significant b-value map',...
			'Callback',@(s,e) obj.plotVariable([],'sign_bvalue',goErrorbi,'b_value_bootstrap')); 
			uimenu(MapMenu,'Label','Magnitude of completeness map',...
			'Callback',@(s,e) obj.plotVariable([],'Mc',goError,'Mc_bootstrap'));
			uimenu(MapMenu,'Label','Standard deviation of magnitude of completeness',...
			'Callback',@(s,e) obj.plotVariable([],'Mc_bootstrap','colormap'));
			 uimenu(MapMenu,'Label','Goodness of fit to power law map',...
			'Callback',@(s,e) obj.plotVariable([],'PowerLawFit','colormap'));
			uimenu(MapMenu,'Label','Resolution map',...
			'Callback',@(s,e) obj.plotVariable([],'Resolution','colormap'));
			uimenu(MapMenu,'Label','Earthquake density map',...
			'Callback',@(s,e) obj.plotVariable([],'EqDensity','colormap'));
			uimenu(MapMenu,'Label','a-value map',...
			'Callback',@(s,e) obj.plotVariable([],'a_value','colormap'));
			
			uimenu(MapMenu,'Label','Calc Recurrence Times','Separator','on',... ...
			'Callback',@(s,e) obj.CalcRecurrence('Calc'));
			
			obj.RecSubMenu=uimenu( MapMenu,'Label','Recurrence Times','Visible','off');
			
			uimenu(obj.RecSubMenu,'Label','Recurrence time map ',...
			'Callback',@(s,e) obj.plotVariable([],'RecTime','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVol','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolLog','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area const map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConst','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area const map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConstLog','colormap'));
			%uimenu(obj.RecSubMenu,'Label','recurrence time percentage',...
			%'Callback',@(s,e) obj.plot2DLine([],'RecTimePerc','Line'));
			
			
			
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));
			uimenu(MapMenu,'Label','Smoothing Parameters',...
			'Callback',@(s,e) obj.Smoother('factor'));
			
		
		
			
			obj.ColorRanger('init');
			obj.Smoother('init');
			obj.ErrorSwitcher('init');
			obj.CalcRecurrence('MenuCheck');
			
		else
		
		
			%later
			%first the general menus (similar for both)
			 OptionMenu = uimenu( obj.ResultGUI,'Label','- Data');
			uimenu(OptionMenu,'Label','Calculate again',...
			'Callback',@(s,e) obj.CalcAgain);
			uimenu(OptionMenu,'Label','Show calculation parameters',...
			'Callback',@(s,e) obj.showCalcParameter);
			uimenu(OptionMenu,'Label','Reload Filterlist from Commander',...
			'Callback',@(s,e) obj.getFilterlist);
			uimenu(OptionMenu,'Label','Write to Datastore','Separator','on',... 
			'Callback',@(s,e) obj.saveCalc);
			uimenu(OptionMenu,'Label','Load from Datastore',...
			'Callback',@(s,e) obj.loadUpdate([]));
			uimenu(OptionMenu,'Label','export Result (.mat)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu(OptionMenu,'Label','export Result (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu(OptionMenu,'Label','export Result (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu(OptionMenu,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
			%Plot option menu, allows to selected plot options like plotting earthquakes
			%coast- & borderlines
			obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
			
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			
			
			uimenu(obj.PlotOptions,'Label','Set superelevation',... 
			'Callback',@(s,e) obj.setSuperElevation);
			uimenu(obj.PlotOptions,'Label','Set Transparency',... 
			'Callback',@(s,e) obj.setAlphaMan);
			uimenu(obj.PlotOptions,'Label','Edit IsoSurfaces',... 
			'Callback',@(s,e) obj.setIsoSurfaces);
			uimenu(obj.PlotOptions,'Label','Edit Slices',... 
			'Callback',@(s,e) obj.setSlices);

									
			uimenu(obj.PlotOptions,'Label','plot Coastlines','Separator','on',... 
			'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
			 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
			'Callback',@(s,e) obj.TooglePlotOptions('Border'));
			 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
			'Callback',@(s,e) obj.TooglePlotOptions('EQ'));	
			uimenu(obj.PlotOptions,'Label','Plot Full Bounding Box',...
			'Callback',@(s,e) obj.TooglePlotOptions('ShowIt'));
			uimenu(obj.PlotOptions,'Label','Draw Shadows',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Shadow'));	
			uimenu(obj.PlotOptions,'Label','Draw Isocaps',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Caps'));
			uimenu(obj.PlotOptions,'Label','Draw Isosurfaces',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Surfer'));
			uimenu(obj.PlotOptions,'Label','Draw Slices',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Slicer'));
			uimenu(obj.PlotOptions,'Label','Round values',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Rounder'));
			
			
		
			
			uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			
			uimenu(obj.PlotOptions,'Label','Color Axis Limited','Separator','on',... ...
			'Callback',@(s,e) obj.ColorRanger('switch'));
			uimenu(obj.PlotOptions,'Label','Set Color Axis',...
			'Callback',@(s,e) obj.ColorRanger('factor'));
			
			%uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
			%'Callback',@(s,e) obj.plot_modify([],'interp',[]));
			%uimenu(obj.PlotOptions,'Label','Shading Flat',... 
			%'Callback',@(s,e) obj.plot_modify([],'flat',[]));
			uimenu(obj.PlotOptions,'Label','Brighten +0.4','Separator','on',...  
			'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
			uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
			'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
			uimenu(obj.PlotOptions,'Label','Use Lighting',... 
			'Callback',@(s,e) obj.ToogleAddPlotOptions('LightUp'));
			
			
			
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.ToogleAddPlotOptions('-chk');
			obj.setqual('chk');
			
				
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','b-value map (max likelihood)',...
			'Callback',@(s,e) obj.plotVariable([],'b_value','Volume3D',[]));
			 uimenu(MapMenu,'Label','Standard deviation of b-Value (max likelihood) map',...
			'Callback',@(s,e) obj.plotVariable([],'b_value_bootstrap','Volume3D',[]));
			uimenu(MapMenu,'Label','Significant b-value map',...
			'Callback',@(s,e) obj.plotVariable([],'sign_bvalue','Volume3D_bicolor',[]));
			uimenu(MapMenu,'Label','Magnitude of completeness map',...
			'Callback',@(s,e) obj.plotVariable([],'Mc','Volume3D',[]));
			uimenu(MapMenu,'Label','Standard deviation of magnitude of completeness',...
			'Callback',@(s,e) obj.plotVariable([],'Mc_bootstrap','Volume3D',[]));
			 uimenu(MapMenu,'Label','Goodness of fit to power law map',...
			'Callback',@(s,e) obj.plotVariable([],'PowerLawFit','Volume3D',[]));
			uimenu(MapMenu,'Label','Resolution map',...
			'Callback',@(s,e) obj.plotVariable([],'Resolution','Volume3D',[]));
			uimenu(MapMenu,'Label','Earthquake density map',...
			'Callback',@(s,e) obj.plotVariable([],'EqDensity','Volume3D',[]));
			uimenu(MapMenu,'Label','a-value map',...
			'Callback',@(s,e) obj.plotVariable([],'a_value','Volume3D',[]));
			
			uimenu(MapMenu,'Label','Calc Recurrence Times','Separator','on',... ...
			'Callback',@(s,e) obj.CalcRecurrence('Calc'));
			
			obj.RecSubMenu=uimenu( MapMenu,'Label','Recurrence Times','Visible','off');
			
			uimenu(obj.RecSubMenu,'Label','Recurrence time map ',...
			'Callback',@(s,e) obj.plotVariable([],'RecTime','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVol','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolLog','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume const map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConst','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume const map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConstLog','Volume3D'));
			%uimenu(obj.RecSubMenu,'Label','recurrence time percentage',...
			%'Callback',@(s,e) obj.plot2DLine([],'RecTimePerc','Line'));
			
			
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));

			
		
		
			obj.ColorRanger('init');
			obj.Smoother('init');
			obj.ErrorSwitcher('init');
			obj.CalcRecurrence('MenuCheck');
		
		
		end
		
		
	end

function buildMouseMenu(obj,graphobj)
		%get plotAxis if needed
		
		if isempty(graphobj)
			plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
				
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
			
			
			cmenu = uicontextmenu('Parent',obj.ResultGUI);
			% Now make the menu be associated with the correct axis
			set(obj.ResultGUI,'UIContextMenu',cmenu);
			set(plotAxis,'UIContextMenu',cmenu);
			% Add uimenu items for the different region selections
			% regionFilter = obj.FilterList.getByName('Region');
			% dataStore = obj.DataStore;
			
			uimenu(cmenu,'Label','Select inside',...
				'Callback',@(s,e) SetFMDRegion(obj,'in'));
			uimenu(cmenu,'Label','Select outside',...
				'Callback',@(s,e) SetFMDRegion(obj,'out'));
			uimenu(cmenu,'Label','Select circular region',...
				'Callback',@(s,e) SetFMDRegion(obj,'circle'));
			uimenu(cmenu,'Label','Close FMD',...
				'Callback',@(s,e) SetFMDRegion(obj,'closeIt'));	
			uimenu(cmenu,'Label','Redraw Map',...
				'Callback',@(s,e) updateGUI(obj));
		else
			cmenu = uicontextmenu('Parent',obj.ResultGUI);
			% Now make the menu be associated with the correct axis
			set(obj.ResultGUI,'UIContextMenu',cmenu);
			set(graphobj,'UIContextMenu',cmenu);
			% Add uimenu items for the different region selections
			% regionFilter = obj.FilterList.getByName('Region');
			% dataStore = obj.DataStore;
			
			uimenu(cmenu,'Label','Select inside',...
				'Callback',@(s,e) SetFMDRegion(obj,'in'));
			uimenu(cmenu,'Label','Select outside',...
				'Callback',@(s,e) SetFMDRegion(obj,'out'));
			uimenu(cmenu,'Label','Select circular region',...
				'Callback',@(s,e) SetFMDRegion(obj,'circle'));
			uimenu(cmenu,'Label','Close FMD',...
				'Callback',@(s,e) SetFMDRegion(obj,'closeIt'));	
			uimenu(cmenu,'Label','Redraw Map',...
				'Callback',@(s,e) updateGUI(obj));
		
		
		end
	
	
	end

function plotVariable(obj,plotAxis,ValField,PlotType,AddParam)
		%will plot the selected field of the CalcRes structur into the selected plotaxis
		%the PlotType can either be 'colormap' or 'quiver' (have to angles for a quiver plot)
		%AddParam is needed for some plots
		import mapseis.plot.*;
		import mapseis.util.AutoBiColorScale;
		
		if nargin<5
			AddParam=[];
		end
		
		if obj.ThirdDimension
			%Just for making the function not to messy
			plotVariable3D(obj,plotAxis,ValField,PlotType,AddParam);
			
		else	
		
			%get plotAxis if needed
			if isempty(plotAxis)
				 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
				 if isempty(plotAxis)
					%try retag
					%get all childrens of the main gui
					gracomp = get(obj.ResultGUI, 'Children');
					
					%find axis and return axis
					plotAxis = findobj(gracomp,'Type','axes','Tag','');
					set(plotAxis,'Tag','MainResultAxis');
				end	
			end
			
			%backup config
			obj.LastPlot={ValField,PlotType,AddParam};
			
			if strcmp(PlotType,'autoColor')
				if obj.Error2Alpha
					PlotType='colormap_errorAlpha';
				else
					PlotType='colormap';
				end
			
			end
			
			if strcmp(PlotType,'bi_autoColor')
				if obj.Error2Alpha
					PlotType='colormap_bicolor_errorAlpha';
				else
					PlotType='colormap_bicolor';
				end
			
			end
			
			%get data
			xdir=obj.CalcRes.X;
			ydir=obj.CalcRes.Y;
			TheVal=obj.CalcRes.(ValField)';
			
			%stop if nan
			stopCrit=all(all(isnan(TheVal)));
			if stopCrit
				plot(mean(xdir),mean(ydir),'.w');
				hold on
				text(mean(xdir),mean(ydir),'Map not available');
				hold off
				
				
				xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
				ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
				
			
				if obj.ProfileSwitch
					xlab='Profile [km] ';
					ylab='Depth [km] ';
				else
					xlab='Longitude ';
					ylab='Latitude ';
				end	
				xlim(xlimiter);	
				ylim(ylimiter);
				xlabel(xlab);
				ylabel(ylab);
				latlim = get(plotAxis,'Ylim');
				set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
				set(plotAxis,'DrawMode','fast');
				
				return
			end
				
			AlphaMap=~isnan(TheVal);
			
			%This is to prevent colorscale differences after smoothing;
			
			
			if ~isempty(obj.AlphaMap)
					minVal=min(min(TheVal(~obj.AlphaMap&AlphaMap)));	
					maxVal=max(max(TheVal(~obj.AlphaMap&AlphaMap)));
				else
					minVal=min(min(TheVal(AlphaMap)));
					maxVal=max(max(TheVal(AlphaMap)));
			end
			
			caxislimit=[floor(10*minVal)/10 ceil(10*maxVal)/10];
			if isempty(caxislimit)|any(isnan(caxislimit))
				caxislimit='auto';
			end
			%disp(caxislimit)
			
			if obj.CalcParameter.SmoothMode==1;
				%Smooth the map before plotting
				TheValNaN=isnan(TheVal);
				
				%use the mean of the data instead of 0
				if ~isempty(obj.AlphaMap)
					ValMean=mean(mean(TheVal(~obj.AlphaMap&AlphaMap)));
				else
					ValMean=mean(mean(TheVal(AlphaMap)));
				end
				
				
				
				if ~isempty(ValMean)
					TheVal(TheValNaN)=ValMean;
				else
					TheVal(TheValNaN)=0;
				end
				
				
				
				hFilter = fspecial('gaussian',obj.CalcParameter.SmoothKernelSize, obj.CalcParameter.SmoothSigma);
				TheVal= imfilter((TheVal), hFilter, 'replicate');
				
				%Smoothing adds to the total sum, subtract that to compensate.
				%TheVal=TheVal-obj.CalcParameter.SmoothFactor;
				
				TheVal(TheValNaN)=NaN;
				
				if ~isstr(caxislimit)
					%add the factor to caxislimit
					caxislimit=caxislimit;
				end
				
			end
			
			%limits
			xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
			ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
			
			%disp(xlimiter)
			%disp(ylimiter)
			if obj.ProfileSwitch
				xlab='Profile [km] ';
				ylab='Depth [km] ';
			else
				xlab='Longitude ';
				ylab='Latitude ';
			end	
			
			
			disp(plotAxis)
			disp(xlimiter)
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			latlim = get(plotAxis,'Ylim');
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis,'DrawMode','fast');
			
			
			if ~isempty(obj.AlphaMap)&~obj.ShowItAll
				TheVal(obj.AlphaMap)=NaN;
			end	
			
			%determine if a quiver is needed 
			switch PlotType
				case 'colormap'
					
					
					
					PlotData={xdir,ydir,TheVal};
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData','auto',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);
					 
					 
				case 'colormap_bicolor'
					%same as colormap but with a special scale		
					PlotData={xdir,ydir,TheVal};
					
					NewAlpha=ones(size(TheVal));
					NewAlpha(isnan(TheVal))=0;
					RegVal=TheVal==obj.CalcParameter.Overall_bval;
					NewAlpha(RegVal)=0.5;
									
					
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData',NewAlpha,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);		
					 prevCaxis=caxis(plotAxis);
					 minCal=floor(prevCaxis(1)*10)/10;
					 maxCal=ceil(prevCaxis(2)*10)/10;
					 overallVal=obj.CalcParameter.Overall_bval;
					
								 
					 
					 %build scale
					 biScale=AutoBiColorScale(minCal,maxCal,overallVal);
				
					 %set axis
					 caxis(plotAxis,[minCal maxCal]);
					 colormap(plotAxis,biScale);
	
					 
					 
				case 'colormap_errorAlpha'
					%The Additonal Parameter has here to name a field which 
					%will be used as AlphaData
					try
						TheError=obj.CalcRes.(AddParam)';
					catch 	
						TheError=NaN;
						disp('Field did not exist');
					end
					
									
					if ~all(isnan(TheError))
						if obj.CalcParameter.SmoothMode==1;
							%smooth error if needed
							TheErrorNaN=isnan(TheError);
							
							ErrorMean=nanmean(nanmean(TheError));
							
							if ~isempty(ValMean)
								TheError(TheErrorNaN)=ErrorMean;
							else
								TheError(TheErrorNaN)=0;
							end
							
							hFilter = fspecial('gaussian',obj.CalcParameter.SmoothKernelSize, obj.CalcParameter.SmoothSigma);
							TheError= imfilter((TheError), hFilter, 'replicate');
							
							TheError(TheErrorNaN)=NaN;
						end
						
						
						
						minErr=nanmin(nanmin(TheError));	
						maxErr=nanmax(nanmax(TheError));
						NewAlpha=ones(size(TheError))-((TheError-minErr)/abs(maxErr-minErr)).^(1/obj.ErrorFactor);
						NewAlpha(isnan(TheVal))=0;
						
						
					else
						NewAlpha='auto';
								
					end
					
				
					PlotData={xdir,ydir,TheVal};
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData',NewAlpha,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);	
				
				
				case 'colormap_bicolor_errorAlpha'
					%same as colormap but with a special scale		
					try
						TheError=obj.CalcRes.(AddParam)';
					catch 	
						TheError=NaN;
						disp('Field did not exist');
					end
					
									
					if ~all(isnan(TheError))
						if obj.CalcParameter.SmoothMode==1;
							%smooth error if needed
							TheErrorNaN=isnan(TheError);
							
							ErrorMean=nanmean(nanmean(TheError));
							
							if ~isempty(ValMean)
								TheError(TheErrorNaN)=ErrorMean;
							else
								TheError(TheErrorNaN)=0;
							end
							
							hFilter = fspecial('gaussian',obj.CalcParameter.SmoothKernelSize, obj.CalcParameter.SmoothSigma);
							TheError= imfilter((TheError), hFilter, 'replicate');
							
							TheError(TheErrorNaN)=NaN;
						end
						
						
						
						minErr=nanmin(nanmin(TheError));	
						maxErr=nanmax(nanmax(TheError));
						NewAlpha=ones(size(TheError))-((TheError-minErr)/abs(maxErr-minErr)).^(1/obj.ErrorFactor);
						NewAlpha(isnan(TheVal))=0;
						
						
					else
						NewAlpha='auto';
								
					end
					
					
					
					PlotData={xdir,ydir,TheVal};
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData',NewAlpha,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);		
					 prevCaxis=caxis(plotAxis);
					 minCal=floor(prevCaxis(1)*10)/10;
					 maxCal=ceil(prevCaxis(2)*10)/10;
					 overallVal=obj.CalcParameter.Overall_bval;
					
								 
					 
					 %build scale
					 biScale=AutoBiColorScale(minCal,maxCal,overallVal);
				
					 %set axis
					 caxis(plotAxis,[minCal maxCal]);
					 colormap(plotAxis,biScale);	 
					 
					 
				case 'quiver'
					
						
					if obj.CalcParameter.Calc_Method==1
						xdir=obj.CalcRes.Xmeshed;
						ydir=obj.CalcRes.Ymeshed;
						TheVal=reshape(TheVal,numel(xdir),1);
						AlphaMap=reshape(AlphaMap,numel(xdir),1);
						xdir=xdir(AlphaMap);
						ydir=ydir(AlphaMap);
						TheVal=TheVal(AlphaMap);
						
					else
						%might need to be done something, but I will see
						%SATSI has its problems anyway
					end
					
					%calculate dx and dy for the angle (TheVal
					%only up 180° used
					le=0.2;
					
					TheVal=mod(TheVal,180);
	
					%first the angles smaller than 90°
					sma=TheVal<90;
	
					%the larger than 90° angels
					lar=not(sma);
	
					%the x coordinate
					avect(:,1) = sin(deg2rad(TheVal))*le;
	
					%the sma part
					avect(sma,2) = cos(deg2rad(TheVal(sma)))*le;
					
					%the lar part
					avect(lar,2) = -cos(deg2rad(TheVal(lar) - 90))*le;
					
					PlotData=[xdir,ydir,avect(:,1),avect(:,2)];
					
					PlotConfig	= struct(	'PlotType','Vector2D',...
									'Data',[PlotData],...
									'MapStyle','not needed',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','red',...
									'LineStylePreset','normal',...
									'MarkerStylePreset','none',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit','auto',...
									'ColorToggle',true,...
									'LegendText',ValField);
					disp(plotAxis)
					 [handle legendentry] = PlotVector2D(plotAxis,PlotConfig);
			end	
			
			if ~obj.ProfileSwitch
				%at the moment only available in the map view
				%plot coastline, borders and earthquake locations if wanted
				if obj.BorderToogle
					hold on
					BorderConf= struct('PlotType','Border',...
								'Data',obj.Datastore,...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'LineStylePreset','normal',...
								'Colors','black');
					[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
					
				end
				
				
				if obj.CoastToogle
					hold on
					CoastConf= struct( 'PlotType','Coastline',...
							   'Data',obj.Datastore,...
							   'X_Axis_Label','Longitude ',...
							   'Y_Axis_Label','Latitude ',...
							   'LineStylePreset','Fatline',...
							   'Colors','blue');
					[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
				
				end
				
				
				if obj.EQToogle
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'LegendText','Earthquakes');
				
					
					[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
				end
					
				%set all axes right
				xlim(plotAxis,xlimiter);
				ylim(plotAxis,ylimiter);
				
				latlim = get(plotAxis,'Ylim');
				set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
				set(plotAxis,'DrawMode','fast');
				set(plotAxis,'LineWidth',2,'FontSize',12);
				
				%set title
				theTit=title(ValField);
				set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
				
				
				%rebuild Polygons
				hold on
				obj.renewPoly(plotAxis);
				hold off
				
			else
				
				
				%The filterlist has to available for now, means now eq plot on
				%profiles with loaded data and missing filter.
				if obj.EQToogle&~isempty(obj.ProfileWidth)&~isempty(obj.ProfileLine)
					
			
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'ProfileLine',obj.ProfileLine,...
								'ProfileWidth',obj.ProfileWidth,...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Distance [km] ',...
								'Y_Axis_Label','Depth [km] ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'LegendText','Earthquakes');
				
					
					[handle1 entry1] = PlotEarthquakeSlice(plotAxis,EqConfig);
				end
					
				%set all axes right
				xlim(plotAxis,xlimiter);
				ylim(plotAxis,ylimiter);
				
				%depthlim = get(plotAxis,'Ylim');
				%distlim = get(plotAxis,'Xlim');
				%deSpan=10*abs(depthlim(1)-depthlim(2));
				%diSpan=abs(distlim(1)-distlim(2));
				
				%set(plotAxis,'dataaspect',[1 diSpan/deSpan 1]);
				set(plotAxis,'dataaspect',[obj.TheAspect 1 1]);
				set(plotAxis,'YDir','reverse');
				set(plotAxis,'LineWidth',2,'FontSize',12);
				
				%set title
				theTit=title(ValField);
				set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
				
				%rebuild Polygons
				hold on
				obj.renewPoly(plotAxis);
				hold off
			end
			
			
		end	
		
		
		%Complicated looking but should be save, and automatically set coloraxis when needed
		%set color axis for both 3D and 2D
		ColMenu = findobj(obj.ResultGUI,'Label','Color Axis Limited');
		set(ColMenu, 'Checked', 'off');
		
		if ~isempty(obj.ColLimitSwitch)
			if isfield(obj.ColLimitSwitch,ValField)
				if isfield(obj.ColLimit,ValField)
					if obj.ColLimitSwitch.(ValField)
						if ~isempty(obj.ColLimit.(ValField))
							caxis(obj.ColLimit.(ValField))
							set(ColMenu, 'Checked', 'on');
						else
	
							obj.ColLimit.(ValField)=caxis;
						end	
					else
						if isempty(obj.ColLimit.(ValField))
							obj.ColLimit.(ValField)=caxis;
						end	
					end	
				else
					obj.ColLimit.(ValField)=caxis;
					
				end
			
			else
				obj.ColLimitSwitch.(ValField)=false;
				obj.ColLimit.(ValField)=caxis;	
			end
			
		
		else
			obj.ColLimitSwitch.(ValField)=false;
			obj.ColLimit.(ValField)=caxis;		
		
		end
		
		
	end

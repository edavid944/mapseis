function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	

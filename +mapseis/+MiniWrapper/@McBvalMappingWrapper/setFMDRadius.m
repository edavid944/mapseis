function setFMDRadius(obj)
	promptValues = {'FMD Selection Radius'};
	dlgTitle = 'Set FMD Selection Radius';
	gParams = inputdlg(promptValues,dlgTitle,1,...
			cellfun(@num2str,{deg2km(obj.FMDRad)},'UniformOutput',false));
		
	if ~isempty(gParams)		
		obj.FMDRad = km2deg(str2double(gParams{1}));
	end			
	
	
end
function AddPolyMenu(obj,imobj,RegionFilter,FMD_ID,addhandle)
          	%Sets the menu of the impoly object and the filter.
          	%only on entry has to be added, "Delete FMD"
          	import mapseis.gui.*;
	
		if nargin<5
			addhandle=[];
		end
		
		regiontype=RegionFilter.RangeSpec;
		
		switch regiontype
			case {'in','out'}
				%find the parts and separate them
				childrens=get(imobj,'children');
				thepoints=findall(childrens,'type','hggroup');
				thelines=findall(childrens,'type','line');
				thepatch=findall(childrens,'type','patch');
				
				%get menu
				oldmenu=get(thepoints(1),'uicontextmenu');
				
	
				
				%create new entry
				Ppoint1=uimenu(oldmenu, 'Label', 'Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
									
				
				%add new menu to points and lines
				for i=1:numel(thepoints)
					set(thepoints(i),'uicontextmenu',oldmenu);
				end
				
				for i=1:numel(thelines)
					set(thelines(i),'uicontextmenu',oldmenu);
				end
				
				for i=1:numel(thepatch)
					set(thepatch(i),'uicontextmenu',oldmenu);
				end
				
				
			case 'line'
				%find the parts and separate them
				childrens=get(imobj,'children');
				thepoints=findall(childrens,'type','line');
				thelines=addhandle;
				
				%get menu
				oldmenu=get(thepoints(1),'uicontextmenu');
				
				%additional menu for the box
				boxmenu=uicontextmenu;
				
				%create new menus
				%oldmenu
				Ppoint1=uimenu(oldmenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
				
		
				
							
				%box menu		
				Lpoint1=uimenu(boxmenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
					
				
						
				%add new menu to points and lines
				for i=1:numel(thepoints)
					set(thepoints(i),'uicontextmenu',oldmenu);
				end
				
				for i=1:numel(thelines)
					set(thelines(i),'uicontextmenu',boxmenu);
				end
				
	
			
			case 'circle'
				%find the parts and separate them
				childrens=get(imobj,'children');
				thepoints=findall(childrens,'type','line');
				thelines=addhandle;
		
				%get menu
				oldmenu=get(imobj,'uicontextmenu');
				
				%additional menu for the box
				circlemenu=uicontextmenu;
				
				%create new menus
				%oldmenu
				Ppoint1=uimenu(oldmenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
				
							
				%Circle Menu
				Lpoint1=uimenu(circlemenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
				
						
				%add new menu to points and lines
				for i=1:numel(thepoints)
					set(thepoints(i),'uicontextmenu',oldmenu);
				end
				
				%the point itself
				set(imobj,'uicontextmenu',oldmenu);
				
				for i=1:numel(thelines)
					set(thelines(i),'uicontextmenu',circlemenu);
				end
				
				
			
		end
          	
end

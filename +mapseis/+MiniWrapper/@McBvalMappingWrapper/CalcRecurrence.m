function CalcRecurrence(obj,DoWhat)
		%This function calculates a recurrence map and turns on the plot menus for it
		
		import mapseis.projector.*;
		
		switch DoWhat
		
			case 'Calc'
				%go for a whole calculation
				
				%Get data
				RecMag=obj.CalcParameter.RecMag;
				Sel_Radius=obj.CalcParameter.Sel_Radius;
				b_value=obj.CalcRes.b_value;
				a_value=obj.CalcRes.a_value;
				Res=obj.CalcRes.Resolution;
				
				%input magnitude
				Title = 'Magnitude of projected mainshock?';
				Prompt={'Magnitude: ', 'RecMag'};
					
									
				defvals=struct(	'RecMag',RecMag);
					
				%RecMag
				Formats(1,1).type='edit';
				Formats(1,1).format='float';
				Formats(1,1).limits = [-99 99];
				
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
				
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defvals,Options); 
				
							
				if Canceled==1
					TRCalc=false;
				else
					TRCalc=true;
					obj.CalcParameter.RecMag=NewParameter.RecMag;
					RecMag=NewParameter.RecMag;
				end
				
				if TRCalc		
					%Get time data
					TheYears=getDecYear(obj.Datastore,obj.SendedEvents);
					minYear=min(TheYears);
					maxYear=max(TheYears);
					
					%calc recurrence map
					ReTime=(maxYear - minYear)./(10.^(a_value-RecMag*b_value));
								  
					
					%calc 1/Tr
					if ~obj.ThirdDimension
						%2D
						ReTimeArea=1./ReTime./(2*pi*Res.*Res);
						ReTimeCArea=1./ReTime./(2*pi*Sel_Radius.*Sel_Radius);
					else
						%3D
						ReTimeArea=1./ReTime./(4/3*pi*Res.*Res.*Res);
						ReTimeCArea=1./ReTime./(4/3*pi*Sel_Radius.*Sel_Radius.*Sel_Radius);
					end
					
					%calc percentage map
					Nanner = isnan(b_value); 
					bvalTemp = b_value; 
					bvalTemp(Nanner) = []; 
					
					len = length(bvalTemp); 
					perc = []; 
					miniB=min(min(bvalTemp));
					
					for i =miniB:miniB/10:miniB*10;
						Nanner = bvalTemp <= i; 
						perc = [ perc ; length(bvalTemp(Nanner))/len*100 i]; 
					end
				
					
					ReTimePerc=perc;
					
					%Write it into the results structure
					obj.CalcRes.RecTime=ReTime;
					obj.CalcRes.RecTimeArVol=ReTimeArea;
					obj.CalcRes.RecTimeArVolConst=ReTimeCArea;
					obj.CalcRes.RecTimePerc=ReTimePerc;
					
					%Log version
					ReTimeAreaLog=ReTimeArea;
					notZero=ReTimeAreaLog~=0;
					ReTimeAreaLog(notZero)=log10(ReTimeAreaLog(notZero));
					ReTimeCAreaLog=ReTimeCArea;
					notZero=ReTimeCAreaLog~=0;
					ReTimeCAreaLog(notZero)=log10(ReTimeCAreaLog(notZero));
					
					obj.CalcRes.RecTimeArVolLog=ReTimeAreaLog;
					obj.CalcRes.RecTimeArVolConstLog=ReTimeCAreaLog;
					
					
					%set colorbars right
					minVal=min(min(ReTime(~isnan(ReTime))));
					if minVal~=0;
						obj.ColLimitSwitch.RecTime=true;
						obj.ColLimit.RecTime=[minVal 10*minVal];	
					end	
					
					%set colorbars right
					minVal=min(min(ReTimeArea(~isnan(ReTimeArea))));
					if minVal~=0;
						obj.ColLimitSwitch.RecTimeArVol=true;
						obj.ColLimit.RecTimeArVol=[minVal 10*minVal];	
					end	
					
					%set colorbars right
					minVal=min(min(ReTimeCArea(~isnan(ReTimeCArea))));
					if minVal~=0;
						obj.ColLimitSwitch.RecTimeArVolConst=true;
						obj.ColLimit.RecTimeArVolConst=[minVal 10*minVal];	
					end	
					
					obj.updateGUI;
					msgbox('Finished');
					
				end
				
				%update menus (check this in any case)
				if isfield(obj.CalcRes,'RecTime')
					set(obj.RecSubMenu,'Visible','on');
				else
					set(obj.RecSubMenu,'Visible','off');
				end
				
				
			case 'MenuCheck'
				%See if some results already exists and set menu to 
				%visible if so
				if isfield(obj.CalcRes,'RecTime')
					set(obj.RecSubMenu,'Visible','on');
				else
					set(obj.RecSubMenu,'Visible','off');
				end
			
		end	
		
		
	
	
	end
	

function Smoother(obj,DoWhat)
	%Turns the smoothing on and off after calculation
	SmoothMenu = findobj(obj.ResultGUI,'Label','Smoothing');
	set(SmoothMenu, 'Checked', 'off');
	
	
			
	switch DoWhat
		case 'switch'
			if obj.CalcParameter.SmoothMode==1
				obj.CalcParameter.SmoothMode=0;
				set(SmoothMenu, 'Checked', 'off');
			else
				obj.CalcParameter.SmoothMode=1;
				set(SmoothMenu, 'Checked', 'on');
			end
			obj.updateGUI;
			
			
		case 'factor'
			promptValues = {'Smooth Kernel Size','Smooth Kernel Sigma'};
			dlgTitle = 'Set Smoothing Parameters';
			gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{obj.CalcParameter.SmoothKernelSize,obj.CalcParameter.SmoothSigma},'UniformOutput',false));
			
			if ~isempty(gParams)
				obj.CalcParameter.SmoothKernelSize=round(str2double(gParams{1}));
				obj.CalcParameter.SmoothSigma=	str2double(gParams{2});
				
				if obj.CalcParameter.SmoothMode==1
					obj.updateGUI;
					set(SmoothMenu, 'Checked', 'on');
				end
			end
			
		case 'init'
			set(SmoothMenu, 'Checked', 'off');
			if obj.CalcParameter.SmoothMode==1
				set(SmoothMenu, 'Checked', 'on');
			end
			
	end

	
end

function TooglePlotOptions(obj,WhichOne)
	%This functions sets the toogles in this modul
	%WhichOne can be the following strings:
	%	'Border':	Borderlines
	%	'Coast'	:	Coastlines
	%	'EQ'	:	Earthquake locations
	%	'-chk'	:	This will set all checks in the menu
	%			to the in toggles set state.
	
	CoastMenu = findobj(obj.PlotOptions,'Label','plot Coastlines');
	BorderMenu = findobj(obj.PlotOptions,'Label','plot Country Borders');
	EqMenu = findobj(obj.PlotOptions,'Label','plot Earthquake Locations');
	CutMenu= findobj(obj.PlotOptions,'Label','Plot Full Bounding Box');
	
	
	switch WhichOne
		case '-chk'
			if obj.CoastToogle
				set(CoastMenu, 'Checked', 'on');
			else
				set(CoastMenu, 'Checked', 'off');
			end
			
			if obj.BorderToogle
				set(BorderMenu, 'Checked', 'on');
			else
				set(BorderMenu, 'Checked', 'off');
			end
			
			if obj.EQToogle
				set(EqMenu, 'Checked', 'on');
			else
				set(EqMenu, 'Checked', 'off');
			end
			
			
			if obj.ShowItAll
				set(CutMenu, 'Checked', 'on');
			else
				set(CutMenu, 'Checked', 'off');
			end
			
			
		case 'Border'
			
			obj.BorderToogle=~obj.BorderToogle;
			
			if obj.BorderToogle
				set(CoastMenu, 'Checked', 'on');
			else
				set(CoastMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
		
		case 'Coast'
			
			obj.CoastToogle=~obj.CoastToogle;
			
			if obj.CoastToogle
				set(BorderMenu, 'Checked', 'on');
			else
				set(BorderMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
		
		case 'EQ'
			
			obj.EQToogle=~obj.EQToogle;
			
			if obj.EQToogle
				set(EqMenu, 'Checked', 'on');
			else
				set(EqMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
		
			
		case 'ShowIt'
			obj.ShowItAll=~obj.ShowItAll;
			
			if obj.ShowItAll
				set(CutMenu, 'Checked', 'on');
			else
				set(CutMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;	
		
			
	end
	
	
end

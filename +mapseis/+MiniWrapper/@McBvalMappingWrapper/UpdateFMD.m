function UpdateFMD(obj)
		%say it all updates the gui
		import mapseis.plot.*;
		import mapseis.util.emptier;
		
		
		TextXPos=[1,0];
		TextAlling={'right','left'};
		
		%Now check which FMDs have to be plotted
		AnyEmpty=emptier(obj.FMDFilter);
		
		if ~all(AnyEmpty);
			
			UsedSlots=find(AnyEmpty==0);
						
		
			FMDAxis = findobj(obj.FMDWindow,'Tag','FMDResultAxis');
			if isempty(FMDAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.FMDWindow, 'Children');
					
				%find axis and return axis
				FMDAxis = findobj(gracomp,'Type','axes','Tag','');
				set(FMDAxis,'Tag','FMDResultAxis','Color','w');
			end	
			
			
			hold(FMDAxis,'off')
			for i=UsedSlots'
				disp(['i=',num2str(i)])
				%get selection of events
				obj.FMDFilter{i}.execute(obj.Datastore);
				selected=obj.FMDFilter{i}.getSelected&obj.SendedEvents;
				
				set(FMDAxis,'pos',[0.1300    0.2500    0.7750    0.65]);
	
							
				%create plot config
				Params =struct('Mc_method',obj.CalcParameter.Mc_Method,...
						'NoCurve',false,...
						'Use_bootstrap',obj.CalcParameter.BootSwitch,...
						'Nr_bootstraps',obj.CalcParameter.BootSamples,...		
						'Mc_correction',obj.CalcParameter.McCorr);
				
				FMDConfig= struct('PlotType','FMDex',...
						'Data',obj.Datastore,...
						'BinSize',0.1,...
						'b_line',true,...
						'CustomCalcParameter',Params,...
						'Selected',selected,...
						'Mc_Method',1,...
						'Write_Text',false,...
						'Value_Output','all',...
						'LineStylePreset','Fatline',...
						'X_Axis_Label','Magnitude ',...
						'Y_Axis_Label','cum. Number of Events ',...
						'Colors',obj.FMDColors{i},...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'LegendText','FMD');
				
						
				%plot da shit
				[handle DataOut] = PlotFMD_expanded(FMDAxis,FMDConfig);
				hold on;
				
				txt1=text(TextXPos(i), -.11,DataOut.Text{1},'FontSize',10,'Color',obj.FMDColors{i},...
						'Units','normalized','HorizontalAlignment',TextAlling{i});
				set(txt1,'FontWeight','normal')
				text(TextXPos(i), -.08,DataOut.Text{2},'FontSize',10,'Color',obj.FMDColors{i},...
						'Units','normalized','HorizontalAlignment',TextAlling{i});
				text(TextXPos(i), -.14,DataOut.Text{3},'FontSize',10,'Color',obj.FMDColors{i},...
						'Units','normalized','HorizontalAlignment',TextAlling{i});
				selected=[];
				%obj.updateGUI;
				 
				%focus back to the FMD
				axes(FMDAxis);
				 
			end	 
			
			%only needed once
			obj.updateGUI;
				 
			%focus back to the FMD
			axes(FMDAxis);
			%somehow the axis is somewhere set to linear until I find it I use this
			set(FMDAxis,'YScale','log');
			hold off;
		end
		
	
	end
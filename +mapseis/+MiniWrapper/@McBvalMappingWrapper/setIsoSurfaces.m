function setIsoSurfaces(obj)
		%allows to set the
		ValField=obj.LastPlot{1};
		
		if isfield(obj.IsoSurfers,ValField)
			CustomSwitch=obj.IsoSurfers.(ValField){1};
			TheSurfs=obj.IsoSurfers.(ValField){2};
			CustomSurf=obj.IsoSurfers.(ValField){3};
		else
			errordlg('No data availalbe');
			return;
		end
		
		
		Title = 'Set IsoSurfaces';
		Prompt={'Use Custom Surfaces:', 'CustomSwitch';...
			'Create Median Surfaces:', 'AutoSurf';...
			'Number of Surfaces:','NumSurf';...
			'Min. Value:','minVal';...
			'Max. Value:','maxVal';...
			'Surface Spacing:','Spacing';...
			'Custom Surfaces;','CustomSurf'};
			
							
		defvals=struct(	'CustomSwitch',CustomSwitch,...
				'AutoSurf',false,...
				'NumSurf',10,...
				'minVal',TheSurfs(1),...
				'maxVal',TheSurfs(2),...
				'Spacing',TheSurfs(3),...
				'CustomSurf',num2str(CustomSurf));
			
		%Custom?
		Formats(1,1).type='check';
		%Formats(1.1).size = -1;
		%Formats(1,2).limits = [0 1];
		
		%AutoSurf
		Formats(1,2).type='check';
		
		%NumSurf (for AutoSurf only)
		Formats(1,3).type='edit';
		Formats(1,3).format='float';
		Formats(1,3).limits = [1 9999];
		
		%min
		Formats(2,1).type='edit';
		Formats(2,1).format='float';
		Formats(2,1).limits = [-9999 9999];
		
		%max
		Formats(2,2).type='edit';
		Formats(2,2).format='float';
		Formats(2,2).limits = [-9999 9999];
			
		%min
		Formats(2,3).type='edit';
		Formats(2,3).format='float';
		Formats(2,3).limits = [-9999 9999];
			
		%Custom
		Formats(3,1).type='edit';
		Formats(3,1).format='text';
		Formats(3,1).size=-1;
		Formats(3,1).limits = [0 1];
		Formats(3,2).limits = [0 1];
		Formats(3,2).type='none';
		Formats(3,3).type='none';
		Formats(3,3).limits = [0 1];
			
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
			
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defvals,Options); 
		
		if ~Canceled==1
			if NewParameter.AutoSurf
				%create Median based isosurfaces
				TheMin=nanmin(obj.CalcRes.(ValField)(:));
				TheMax=nanmax(obj.CalcRes.(ValField)(:));
				TheMed=nanmedian(obj.CalcRes.(ValField)(:));
				
				%TheSurfaces
				Surf1=linspace(TheMin,TheMed,NewParameter.NumSurf);
				Surf2=linspace(TheMed,TheMax,NewParameter.NumSurf+1);
				CustomSurf=[Surf1,Surf2(2:end)];
				
				%add them to the data
				obj.IsoSurfers.(ValField){1}=true;
				obj.IsoSurfers.(ValField){3}=CustomSurf;
				
			else
			
				obj.IsoSurfers.(ValField){1}=logical(NewParameter.CustomSwitch);
				obj.IsoSurfers.(ValField){2}=[NewParameter.minVal, NewParameter.maxVal, NewParameter.Spacing];
				obj.IsoSurfers.(ValField){3}=str2num(NewParameter.CustomSurf);
			
			end
			
			obj.updateGUI;
		
		
		end
		
		
	end

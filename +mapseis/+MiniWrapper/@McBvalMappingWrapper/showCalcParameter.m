function showCalcParameter(obj)
		%opens a message box and shows the parameter, usefull if a
		%calculation is loaded;
		
		TheFields=fieldnames(obj.CalcParameter);
		
		for i=1:numel(TheFields)
			TextBuild(i)={[TheFields{i},': ',num2str(obj.CalcParameter.(TheFields{i}))]};
			
		end
		
		msgbox(TextBuild,'Parameters of the current calculation');
		
		
	end

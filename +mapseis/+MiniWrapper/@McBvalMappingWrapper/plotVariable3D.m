function plotVariable3D(obj,plotAxis,ValField,PlotType,AddParam)
		%same in 3D with a little less options.
		import mapseis.plot.*;
		import mapseis.util.AutoBiColorScale;
		import mapseis.projector.*;
		
		if nargin<5
			AddParam=[];
		end
		
	
		
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
				
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
		end
			
		%backup config
		obj.LastPlot={ValField,PlotType,AddParam};
			
		
		
		%get data
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		zdir=obj.CalcRes.Z;
		
		TheVal=obj.CalcRes.(ValField);
		
		
		
		%limits
		xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
		ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
		zlimiter=[(min(zdir))-0.5 (max(zdir))+0.5];
		
		%labels
		xlab='Longitude (deg) ';
		ylab='Latitude (deg) ';
		zlab='Depth (km) ';
		
		%stop if nan
		stopCrit=all(all(all(isnan(TheVal))));
		if stopCrit
			plot3(mean(xdir),mean(ydir),mean(zdir),'.w');
			hold on
			text(mean(xdir),mean(ydir),mean(zdir),'Map not available');
			hold off
			xlabel(xlab);
			ylabel(ylab);
			zlabel(zlab);
			xlim(xlimiter);
			ylim(ylimiter);
			zlim(zlimiter);
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(ylimiter)) obj.TheAspect]);
			set(plotAxis,'ZDir','reverse');
			
			return
		end
		
		if obj.RoundIt
			%round values to 0.1 accuracy
			TheVal=round(TheVal*10)/10;
		
		end
		
		
		%clear all;
		plot3(plotAxis,mean(xlimiter),mean(ylimiter),0,'Color','w');
		
		switch PlotType
			case 'Volume3D'
				
				if ~isempty(obj.AlphaMap)&~obj.ShowItAll
					for i=1:numel(TheVal(1,1,:))
						TempVal=TheVal(:,:,i);
						TempVal(obj.AlphaMap)=NaN;
						TheVal(:,:,i)=TempVal;
					end
				
				end
				minVal=nanmin(nanmin(nanmin(TheVal)));
				maxVal=nanmax(nanmax(nanmax(TheVal)));
				caxislimit=[minVal maxVal];
				
				if obj.CalcParameter.SmoothMode==1;
					TheVal=smooth3(TheVal,'box',3);
				end
				
				%Set default surfaces if needed
				if isfield(obj.IsoSurfers,ValField)
					CustomSwitch=obj.IsoSurfers.(ValField){1};
					TheSurfs=obj.IsoSurfers.(ValField){2};
					CustomSurf=obj.IsoSurfers.(ValField){3};
				else
					CustomSwitch=false;
					TheSurfs=[minVal maxVal abs(maxVal-minVal)/10];
					CustomSurf=TheSurfs(1):TheSurfs(3):TheSurfs(2);
					obj.IsoSurfers.(ValField){3}=CustomSurf;
					obj.IsoSurfers.(ValField){2}=TheSurfs;
					obj.IsoSurfers.(ValField){1}=CustomSwitch;
				
				end
				
				
				%Set default slice if needed
				if isfield(obj.SalamiConfig,ValField)
					CustomXSwitch=obj.SalamiConfig.(ValField){1,1};
					TheXSlice=obj.SalamiConfig.(ValField){1,2};
					CustomXSlice=obj.SalamiConfig.(ValField){1,3};
					
					CustomYSwitch=obj.SalamiConfig.(ValField){2,1};
					TheYSlice=obj.SalamiConfig.(ValField){2,2};
					CustomYSlice=obj.SalamiConfig.(ValField){2,3};
					
					CustomZSwitch=obj.SalamiConfig.(ValField){3,1};
					TheZSlice=obj.SalamiConfig.(ValField){3,2};
					CustomZSlice=obj.SalamiConfig.(ValField){3,3};
					
				else
					%only build vertical slices in one direction (larger one)
					if numel(xdir)>=numel(ydir)
						CustomXSwitch=false;
						TheXSlice=[min(xdir) max(xdir) abs(max(xdir)-min(xdir))/6];
						CustomXSlice=TheXSlice(1):TheXSlice(3):TheXSlice(2);
						
						CustomYSwitch=false;
						TheYSlice=[];
						CustomYSlice=[];
						
					else
						CustomXSwitch=false;
						TheXSlice=[];
						CustomXSlice=[];
					
						CustomYSwitch=false;
						TheYSlice=[min(ydir) max(ydir) abs(max(ydir)-min(ydir))/6];
						CustomYSlice=TheYSlice(1):TheYSlice(3):TheYSlice(2);
						
						
					end
					
					%Standard horizotal 4 slices
					CustomZSwitch=false;
					TheZSlice=[min(zdir) max(zdir) abs(max(zdir)-min(zdir))/4];
					CustomZSlice=TheZSlice(1):TheZSlice(3):TheZSlice(2);
					
					
					obj.SalamiConfig.(ValField){1,1}=CustomXSwitch;
					obj.SalamiConfig.(ValField){1,2}=TheXSlice;
					obj.SalamiConfig.(ValField){1,3}=CustomXSlice;
					
					obj.SalamiConfig.(ValField){2,1}=CustomYSwitch;
					obj.SalamiConfig.(ValField){2,2}=TheYSlice;
					obj.SalamiConfig.(ValField){2,3}=CustomYSlice;
					
					obj.SalamiConfig.(ValField){3,1}=CustomZSwitch;
					obj.SalamiConfig.(ValField){3,2}=TheZSlice;
					obj.SalamiConfig.(ValField){3,3}=CustomZSlice;
				
				end
				
				
				
				PlotData={xdir,ydir,zdir,TheVal};
				
				
				
				if obj.SurferDude
				
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Volume3D',...
									'Data',{PlotData},...
									'Surfaces',TheSurfs,...
									'UseCustom',CustomSwitch,...
									'CustomSurfaces',CustomSurf,...
									'DrawCaps',obj.DrawCaps,...
									'AlphaValue',obj.AlphaVal,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotVolume3D(plotAxis,PlotConfig); 
				end
				
				
				if obj.SliceIt
					hold on
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Slice3D',...
									'Data',{PlotData},...
									'ZSlice',TheZSlice,...
									'UseCustom_Z',CustomZSwitch,...
									'CustomZSlice',CustomZSlice,...
									'XSlice',TheXSlice,...
									'UseCustom_X',CustomXSwitch,...
									'CustomXSlice',CustomXSlice,...
									'YSlice',TheYSlice,...
									'UseCustom_Y',CustomYSwitch,...
									'CustomYSlice',CustomYSlice,...
									'Use3DSlice',false,...
									'Custom3DSlice',[[]],...
									'AlphaValue',obj.AlphaVal,...
									'ShadeMode','flat',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotSlice3D(plotAxis,PlotConfig); 
					
				
				
				end
				
				
				if obj.EQToogle
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit','auto',...
								'Y_Axis_Limit','auto',...
								'Z_Axis_Limit','auto',...
								'C_Axis_Limit','auto',...
								'DepthReverse',true,...
								'FastMode',false,...
								'LegendText','Earthquakes')	
				
					
					[handle1 entry1] = PlotEarthquake3D(plotAxis,EqConfig);
					
					if obj.ShadowToogle
						hold on
						%prepare data
						[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
						[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
						[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
						maxDepth=zlimiter(2)+1;
						NrEvents=numel(selectedDepth);
						NrNot=numel(unselectedDepth);
						
						seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
						
						ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',false,...
						'LegendText','Earthquakes');
						
						[shadhandle2 shadentry2] = PlotEarthquake3D(plotAxis,ShadConfig);
					
					end
					
					
					
				end
				
				%obj.BorderToogle=false;
				if obj.BorderToogle
					  hold on	
					  BorderConf= struct(	'PlotType','Border',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','normal',...
								  'Colors','black');
								  [BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
					  
				end
					  
				  
				%obj.CoastToogle=false;	
				if obj.CoastToogle
					  hold on
					  CoastConf= struct( 	'PlotType','Coastline',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','Fatline',...
								  'Colors','blue');
					  [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
					  
				end
				
				camlight;
				camlight('headlight');
				
				if obj.LightMyFire
					lighting phong;
				else
					lighting none;
				end
				
				hold off
				
				
			case 'Volume3D_bicolor'
				if ~isempty(obj.AlphaMap)&~obj.ShowItAll
					for i=1:numel(TheVal(1,1,:))
						TempVal=TheVal(:,:,i);
						TempVal(obj.AlphaMap)=NaN;
						TheVal(:,:,i)=TempVal;
					end
				end	
								
				
				minVal=nanmin(nanmin(nanmin(TheVal)));
				maxVal=nanmax(nanmax(nanmax(TheVal)));
				caxislimit=[minVal maxVal];
				
				if obj.CalcParameter.SmoothMode==1;
					TheVal=smooth3(TheVal,'box',3);
				end
				
				if isfield(obj.IsoSurfers,ValField)
					CustomSwitch=obj.IsoSurfers.(ValField){1};
					TheSurfs=obj.IsoSurfers.(ValField){2};
					CustomSurf=obj.IsoSurfers.(ValField){3};
				else
					CustomSwitch=false;
					TheSurfs=[minVal maxVal abs(maxVal-minVal)/10];
					CustomSurf=TheSurfs(1):TheSurfs(3):TheSurfs(2);
					obj.IsoSurfers.(ValField){3}=CustomSurf;
					obj.IsoSurfers.(ValField){2}=TheSurfs;
					obj.IsoSurfers.(ValField){1}=CustomSwitch;
				
				end
				
				%create colorbar
				minVal=nanmin(nanmin(nanmin(TheVal)));
				maxVal=nanmax(nanmax(nanmax(TheVal)));
				caxislimit=[minVal maxVal];
				%build scale
				overallVal=obj.CalcParameter.Overall_bval;
				biScale=AutoBiColorScale(minVal,maxVal,overallVal);
				
				
				
				%Set default slice if needed
				if isfield(obj.SalamiConfig,ValField)
					CustomXSwitch=obj.SalamiConfig.(ValField){1,1};
					TheXSlice=obj.SalamiConfig.(ValField){1,2};
					CustomXSlice=obj.SalamiConfig.(ValField){1,3};
					
					CustomYSwitch=obj.SalamiConfig.(ValField){2,1};
					TheYSlice=obj.SalamiConfig.(ValField){2,2};
					CustomYSlice=obj.SalamiConfig.(ValField){2,3};
					
					CustomZSwitch=obj.SalamiConfig.(ValField){3,1};
					TheZSlice=obj.SalamiConfig.(ValField){3,2};
					CustomZSlice=obj.SalamiConfig.(ValField){3,3};
					
				else
					%only build vertical slices in one direction (larger one)
					if numel(xdir)>=numel(ydir)
						CustomXSwitch=false;
						TheXSlice=[min(xdir) max(xdir) abs(max(xdir)-min(xdir))/6];
						CustomXSlice=TheXSlice(1):TheXSlice(3):TheXSlice(2);
						
						CustomYSwitch=false;
						TheYSlice=[];
						CustomYSlice=[];
						
					else
						CustomXSwitch=false;
						TheXSlice=[];
						CustomXSlice=[];
					
						CustomYSwitch=false;
						TheYSlice=[min(ydir) max(ydir) abs(max(ydir)-min(ydir))/6];
						CustomYSlice=TheYSlice(1):TheYSlice(3):TheYSlice(2);
						
						
					end
					
					%Standard horizotal 4 slices
					CustomZSwitch=false;
					TheZSlice=[min(zdir) max(zdir) abs(max(zdir)-min(zdir))/4];
					CustomZSlice=TheZSlice(1):TheZSlice(3):TheZSlice(2);
					
					
					obj.SalamiConfig.(ValField){1,1}=CustomXSwitch;
					obj.SalamiConfig.(ValField){1,2}=TheXSlice;
					obj.SalamiConfig.(ValField){1,3}=CustomXSlice;
					
					obj.SalamiConfig.(ValField){2,1}=CustomYSwitch;
					obj.SalamiConfig.(ValField){2,2}=TheYSlice;
					obj.SalamiConfig.(ValField){2,3}=CustomYSlice;
					
					obj.SalamiConfig.(ValField){3,1}=CustomZSwitch;
					obj.SalamiConfig.(ValField){3,2}=TheZSlice;
					obj.SalamiConfig.(ValField){3,3}=CustomZSlice;
				
				end
				
				
				PlotData={xdir,ydir,zdir,TheVal};
				
				if obj.SurferDude
				
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Volume3D',...
									'Data',{PlotData},...
									'Surfaces',TheSurfs,...
									'UseCustom',CustomSwitch,...
									'CustomSurfaces',CustomSurf,...
									'DrawCaps',obj.DrawCaps,...
									'AlphaValue',obj.AlphaVal,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotVolume3D(plotAxis,PlotConfig); 
				
				end
				
				
				if obj.SliceIt
					hold on
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Slice3D',...
									'Data',{PlotData},...
									'ZSlice',TheZSlice,...
									'UseCustom_Z',CustomZSwitch,...
									'CustomZSlice',CustomZSlice,...
									'XSlice',TheXSlice,...
									'UseCustom_X',CustomXSwitch,...
									'CustomXSlice',CustomXSlice,...
									'YSlice',TheYSlice,...
									'UseCustom_Y',CustomYSwitch,...
									'CustomYSlice',CustomYSlice,...
									'Use3DSlice',false,...
									'Custom3DSlice',[[]],...
									'AlphaValue',obj.AlphaVal,...
									'ShadeMode','flat',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotSlice3D(plotAxis,PlotConfig); 
					
				
				
				end
				
				if obj.EQToogle
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit','auto',...
								'Y_Axis_Limit','auto',...
								'Z_Axis_Limit','auto',...
								'C_Axis_Limit','auto',...
								'DepthReverse',true,...
								'FastMode',false,...
								'LegendText','Earthquakes')	
				
					
					[handle1 entry1] = PlotEarthquake3D(plotAxis,EqConfig);
					
					if obj.ShadowToogle
						hold on
						%prepare data
						[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
						[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
						[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
						maxDepth=zlimiter(2)+1;
						NrEvents=numel(selectedDepth);
						NrNot=numel(unselectedDepth);
						
						seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
						
						ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',false,...
						'LegendText','Earthquakes');
						
						[shadhandle2 shadentry2] = PlotEarthquake3D(plotAxis,ShadConfig);
					
					end
					
					
					
				end
				
				%obj.BorderToogle=false;
				if obj.BorderToogle
					  hold on	
					  BorderConf= struct(	'PlotType','Border',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','normal',...
								  'Colors','black');
								  [BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
					  
				end
					  
				  
				%obj.CoastToogle=false;	
				if obj.CoastToogle
					  hold on
					  CoastConf= struct( 	'PlotType','Coastline',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','Fatline',...
								  'Colors','blue');
					  [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
					  
				end
				
				%%set axis
				caxis(plotAxis,[minVal maxVal]);
				colormap(plotAxis,biScale);
				camlight;
				camlight('headlight');
				
				if obj.LightMyFire
					lighting phong;
				else
					lighting none;
				end
				
				hold off
			
				
			case 'Scatter3D'
				xdir=obj.CalcRes.Xmeshed;
				ydir=obj.CalcRes.Ymeshed;
				zdir=obj.CalcRes.Zmeshed;
				if ~isempty(obj.AlphaMap)
					TheVal(obj.AlphaMap)=NaN;
				end
				
				%LATER, when I finished scatter options
				
				
			
			case 'Scatter3D_bicolor'
				xdir=obj.CalcRes.Xmeshed;
				ydir=obj.CalcRes.Ymeshed;
				zdir=obj.CalcRes.Zmeshed;
				if ~isempty(obj.AlphaMap)
					TheVal(obj.AlphaMap)=NaN;
				end
		
				%LATER, when I finished scatter options
		end
			
		
				
			
			
			
			
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			zlim(plotAxis,zlimiter);
		
				
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(ylimiter)) obj.TheAspect]);
			set(plotAxis,'ZDir','reverse');
			box off
			set(plotAxis,'DrawMode','fast','XColor',[0.7 0.7 0.7],'YColor',[0.7 0.7 0.7],'ZColor',[0.7 0.7 0.7]);
			hold off
			
			
		
	
	
	end

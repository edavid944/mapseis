classdef DeclusterWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		CalcParameter
		CalcMode
		ErrorDialog
		ParallelMode
		StartCalc
		ResultGUI
		CalcTime
		PlotOptions
		BorderToogle
		CoastToogle
		PlotQuality
		LastPlot
		
	end

	events
		CalcDone

	end


	methods
	
	
	function obj = DeclusterWrapper(ListProxy,Commander,GUISwitch,CalcMode,CalcParameter)
		import mapseis.datastore.*;
			
		%A gui for the new mapseis Decluster algorithmen	
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		
		if isempty(Commander)
			obj.Commander=[];
			%go into manul mode, the object will be created but everything
			%has to be done manually (useful for scripting)
		else
		
			obj.Commander = Commander; 
			obj.ParallelMode=obj.Commander.ParallelMode;
			
			
			
			
		
						
			
			obj.ResultGUI=[];
			obj.PlotOptions=[];
			obj.BorderToogle=true;
			obj.CoastToogle=true;
			obj.PlotQuality = 'low';
			obj.LastPlot=[];
			obj.CalcMode=CalcMode;
			obj.CalcTime=[];
			
					
			%get the current datastore and filterlist and build zmap catalog
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			
			
			EveryThingGood=true;
			
			
			
			
			
			if EveryThingGood
				
				if ~isempty(CalcParameter)
					%Parameters are include, no default parameter will
					%be set
					obj.CalcParameter=CalcParameter;
				else
					obj.InitVariables
				end	
				
				
				
				switch CalcMode
				
					case 'Reasenberg'
						
						if GUISwitch
							
							%let the user modify the parameters
							obj.openCalcParamWindow;
							
							if obj.StartCalc
								
								%Calc
								obj.doTheCalcThing;
								
								%display all info 
								obj.DisplayResultInfo;
								
					
								
								
							end	
							
							
						end
					
					case 'MonteReasenberg'
					
					
					
					case 'Gardner-Knopoff'
						
						if GUISwitch
							
							%let the user modify the parameters
							obj.openCalcParamWindowGK;
							
							if obj.StartCalc
								
								%Calc
								obj.doTheCalcThingGK;
								
								%display all info 
								obj.DisplayResultInfo;
								
					
								
								
							end	
							
							
						end
					
					
					case 'SLIDER'
						if GUISwitch
							
							%let the user modify the parameters
							obj.openCalcParamWindowSLI;
							
							if obj.StartCalc
								
								%Calc
								obj.doTheCalcThingSLI;
								
								%display all info 
								obj.DisplayResultInfo;
								
					
								
								
							end	
							
							
						end
					
						
					case 'Viewer'
				
					
				end
			
			end
			
		end
		
	end

	function InitVariables(obj)
		%No Zmap stuff needed anymore
		%some basic parameters, I will add addition parameters later 
		%if needed e.g. bootstrap option
		obj.CalcParameter = struct(	'Tau_min',1,...
						'Tau_max',10,...
						'XMagEff',1.5,...
						'XMagFactor',0.5,...
						'RadiusFactor',10,...
						'ProbObs',0.95,...
						'EpiError',1.5,...
						'DepthError',2,...
						'GK_Method',1,...
						'GK_SetMain',false,...
						'Sli_Type',1,...
						'MainMag',4,...
						'BackTime',30,...
						'ForeTime',30,...
						'MinRad',5,...
						'MagRadScale',0.59,...
						'MagRadConst',2.44);
						

						
		%from Reasenberg decluster (variable Info)
		% variables given by inputwindow
		%
		% RadiusFactor:	rfact  	is factor for interaction radius for dependent events (default 10)
		% XMagEff:	xmeff  	is "effective" lower magnitude cutoff for catalog,it is raised 
		%         		by a factor xk*cmag1 during clusters (default 1.5)
		% XMagFactor:	xk	is the factor used in xmeff    (default .5)
		% Tau_min:	taumin 	is look ahead time for not clustered events (default one day)
		% Tau_max:	taumax 	is maximum look ahead time for clustered events (default 10 days)
		% ProbObs:	P      	to be P confident that you are observing the next event in 
		%       		the sequence (default is 0.95)
		
		
		%From Slider 
		%Selection Method:	Time: earthquakes are process in temporal order (clusterSLIDERtime)
		%			Magnitude: earthquakes are process by magnitude size (clusterSLIDERmag)
		%MainMag:		minimum mainmag magnitude
		%BackTime:		time before an earthquake in days
		%ForeTime:		time window following an earthquake in days
		%MinRad:		Minimum radius used for searching triggered events
		%MagRadScale:		Sets how much the search radius varies with magnitude size
		%MagRadConst:		Constant subtracted from the magnitude scaled search radius
	
	end
	
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		Title = 'Reasenberg Decluster ';
		Prompt={'Min. forward time (taumin):', 'Tau_min';...
			'Max. forward time (taumax):', 'Tau_max';...
			'Radius factor (rfact):','RadiusFactor';...
			'Effective mag. cutoff (xmeff):','XMagEff';...
			'Mag. factor (xk):','XMagFactor';...
			'Probability of Observation (P):','ProbObs';...
			'Epicenter error (err):','EpiError';...
			'Depth error (derr):','DepthError'};
			
				
				
				
			
			
		%taumin
		Formats(1,1).type='edit';
		Formats(1,1).format='float';
		Formats(1,1).limits = [0 9999];
		
		%taumax
		Formats(2,1).type='edit';
		Formats(2,1).format='float';
		Formats(2,1).limits = [0 9999];
		
		%rfact
		Formats(3,1).type='edit';
		Formats(3,1).format='float';
		Formats(3,1).limits = [0 9999];
		
		%xmeff
		Formats(4,1).type='edit';
		Formats(4,1).format='float';
		Formats(4,1).limits = [0 9999];
		
		%xk
		Formats(5,1).type='edit';
		Formats(5,1).format='float';
		Formats(5,1).limits = [0 9999];
		
		%P
		Formats(6,1).type='edit';
		Formats(6,1).format='float';
		Formats(6,1).limits = [0 1];
		
		%err
		Formats(7,1).type='edit';
		Formats(7,1).format='float';
		Formats(7,1).limits = [0 9999];
			
		%derr
		Formats(8,1).type='edit';
		Formats(8,1).format='float';
		Formats(8,1).limits = [0 9999];
			
			
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,obj.CalcParameter,Options); 
		
		
		obj.CalcParameter=NewParameter;
		
			
			
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		
			
			
			
	end		
	
	
	function openCalcParamWindowGK(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		Title = 'Gardner-Knopoff Decluster ';
		Prompt={'Window Method:', 'GK_Method';...
			'Force MainShock:', 'GK_SetMain'};
			
				
		LabelList2 =  {	'Gardener & Knopoff (table), 1974';...
				'Gardener & Knopoff (formula), 1974'; ...
				'Gruenthal pers. communication'; ...
				'Urhammer, 1986'}; 		
				
		
		%Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=LabelList2;
		
					
		%SetMain
		Formats(2,1).type='check';
			
			
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,obj.CalcParameter,Options); 
		
		
		obj.CalcParameter=NewParameter;
		
			
			
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		
			
			
			
	end		
	
	function openCalcParamWindowSLI(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		Title = 'SLIDER Decluster ';
		Prompt={'Selection Method:', 'Sli_Type';...
			'Min. Mainshock Magnitude:','MainMag';...
			'Time before Eq (d):', 'BackTime';...
			'Time after Eq (d):', 'ForeTime';...
			'Min. search radius (km):', 'MinRad';...
			'Mag. radius scaler:', 'MagRadScale';...
			'Mag. radius constant:','MagRadConst'};
			
				
		LabelList2 =  {	'Magnitude';...
				'Time'}; 		
				
		
		%Sli Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=LabelList2;
		
		
		%min MainMag 
		Formats(2,1).type='edit';
		Formats(2,1).format='float';
		Formats(2,1).limits = [-99 99];
					
		%Time before eq
		Formats(3,1).type='edit';
		Formats(3,1).format='float';
		Formats(3,1).limits = [0 9999];
		
		%Time after eq
		Formats(4,1).type='edit';
		Formats(4,1).format='float';
		Formats(4,1).limits = [0 9999];
		
		%Min Search Radius
		Formats(5,1).type='edit';
		Formats(5,1).format='float';
		Formats(5,1).limits = [0 9999];
		
		%Mag Search Radius Scaler
		Formats(6,1).type='edit';
		Formats(6,1).format='float';
		Formats(6,1).limits = [0 9999];
		
		%Mag Search Radius Constant
		Formats(7,1).type='edit';
		Formats(7,1).format='float';
		Formats(7,1).limits = [0 9999];
			
			
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,obj.CalcParameter,Options); 
		
		
		obj.CalcParameter=NewParameter;
		
			
			
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		
			
			
			
	end		
	
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.declustering.*;
		
		
		%get parameter into variables
		ShortCatalog = getShortCatalog(obj.Datastore,obj.SendedEvents);
		taumin=obj.CalcParameter.Tau_min;
		taumax=obj.CalcParameter.Tau_max;
		xk=obj.CalcParameter.XMagFactor;
		xmeff=obj.CalcParameter.XMagEff;
		P=obj.CalcParameter.ProbObs;
		rfact=obj.CalcParameter.RadiusFactor;
		err=obj.CalcParameter.EpiError;
		derr=obj.CalcParameter.DepthError;
		
		
		%run declustering
		disp('please wait....');
		tic;
		[clusterID,EventType,AlgoInfo] = ReasenbergDecluster(taumin,taumax,xk,xmeff,P,rfact,err,derr,ShortCatalog);
		obj.CalcTime=toc;
		disp('finished!');
		
		%store result
		obj.CalcRes.clusterID=clusterID;
		obj.CalcRes.EventType=EventType;
		obj.CalcRes.AlgoInfo=AlgoInfo;
		
		%write to datastore
		obj.Datastore.setDeclusterData(EventType,clusterID,obj.SendedEvents,[]);
		obj.Datastore.DeclusterMetaData=AlgoInfo;
		
		%correct for old datastore version, it makes sense to do this here, because it is needed only by the declustering
		NumberedUserData=getNumberedFields(obj.Datastore);
		setNumberedFields(obj.Datastore,union(NumberedUserData,{'Month','Day','Hour','Minute','Second','MilliSecond' ,'DecYear'}));
    

     
        
		
		
	
	end
	
	
	
	
	
	
	function doTheCalcThingMonte(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.declustering.*;
		
		
		%Calculation with an added MonteCarlo simulation
		%(the things done by Thomas Van Stiphout) 
		%LATER, first to the normal version
		
		
		%What the monte-carlo reasenberg does is simple
		
		
		%get parameter into variables
		ShortCatalog = getShortCatalog(obj.Datastore,obj.SendedEvents);
		taumin=obj.CalcParameter.Tau_min;
		taumax=obj.CalcParameter.Tau_max;
		xk=obj.CalcParameter.XMagFactor;
		xmeff=obj.CalcParameter.XMagEff;
		P=obj.CalcParameter.ProbObs;
		rfact=obj.CalcParameter.RadiusFactor;
		err=obj.CalcParameter.EpiError;
		derr=obj.CalcParameter.DepthError;
		
		
		%run declustering
		disp('please wait....');
		
		tic;
		[clusterID,EventType,AlgoInfo] = ReasenbergDecluster(taumin,taumax,xk,xmeff,P,rfact,err,derr,ShortCat);
		obj.CalcTime=toc;
		disp('finished!');
		
		
		%store result
		obj.CalcRes.clusterID=clusterID;
		obj.EventType=EventType;
		obj.AlgoInfo=AlgoInfo;
		
		%write to datastore
		obj.Datastore.setDeclusterData(EventType,clusterID,obj.SendedEvents,[]);
		obj.Datastore.DeclusterMetaData=AlgoInfo;
		
		%correct for old datastore version, it makes sense to do this here, because it is needed only by the declustering
		NumberedUserData=getNumberedFields(obj.Datastore);
		setNumberedFields(obj.Datastore,union(NumberedUserData,{'Month','Day','Hour','Minute','Second','MilliSecond' ,'DecYear'}));
		
		
	
	end
	
	
	
	
	function doTheCalcThingGK(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.declustering.*;
		
		
		%get parameter into variables
		ZmapCatalog = getZMAPFormat(obj.Datastore,obj.SendedEvents);
		WinMethod=obj.CalcParameter.GK_Method;
		SetMain=obj.CalcParameter.GK_SetMain;
	
		
		
		
		%run declustering
		disp('please wait....');
		tic;
		[clusterID,EventType,AlgoInfo] = calc_decluster_gardKnop(ZmapCatalog,WinMethod,SetMain);
		obj.CalcTime=toc;
		disp('finished!');
		
		%store result
		obj.CalcRes.clusterID=clusterID;
		obj.CalcRes.EventType=EventType;
		obj.CalcRes.AlgoInfo=AlgoInfo;
		
		%write to datastore
		obj.Datastore.setDeclusterData(EventType,clusterID,obj.SendedEvents,[]);
		obj.Datastore.DeclusterMetaData=AlgoInfo;
		
		%correct for old datastore version, it makes sense to do this here, because it is needed only by the declustering
		NumberedUserData=getNumberedFields(obj.Datastore);
		setNumberedFields(obj.Datastore,union(NumberedUserData,{'Month','Day','Hour','Minute','Second','MilliSecond' ,'DecYear'}));
    

     
        
		
		
	
	end
	
	
	function doTheCalcThingSLI(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.declustering.*;
		
		
		%get parameter into variables
		ShortCat = getShortCatalog(obj.Datastore,obj.SendedEvents);
		Sli_Type=obj.CalcParameter.Sli_Type;
		MainMag=obj.CalcParameter.MainMag;
		BackTime=obj.CalcParameter.BackTime;
		ForeTime=obj.CalcParameter.ForeTime;
		MinRad=obj.CalcParameter.MinRad;
		MagRadScale=obj.CalcParameter.MagRadScale;
		MagRadConst=obj.CalcParameter.MagRadConst;

	
		
		
		if Sli_Type==1
			%run declustering with magnitude
			disp('please wait....');
			tic;
			[clusterID,EventType,AlgoInfo] = clusterSLIDERmag(ShortCat,MainMag,BackTime,ForeTime,MinRad,MagRadScale,MagRadConst);
			obj.CalcTime=toc;
			disp('finished!');
			
		elseif Sli_Type==2
			%run declustering with time
			disp('please wait....');
			tic;
			[clusterID,EventType,AlgoInfo] = clusterSLIDERtime(ShortCat,MainMag,BackTime,ForeTime,MinRad,MagRadScale,MagRadConst);
			obj.CalcTime=toc;
			disp('finished!');
		
		end
		
		
		%store result
		obj.CalcRes.clusterID=clusterID;
		obj.CalcRes.EventType=EventType;
		obj.CalcRes.AlgoInfo=AlgoInfo;
		
		%write to datastore
		obj.Datastore.setDeclusterData(EventType,clusterID,obj.SendedEvents,[]);
		obj.Datastore.DeclusterMetaData=AlgoInfo;
		
		%correct for old datastore version, it makes sense to do this here, because it is needed only by the declustering
		NumberedUserData=getNumberedFields(obj.Datastore);
		setNumberedFields(obj.Datastore,union(NumberedUserData,{'Month','Day','Hour','Minute','Second','MilliSecond','DecYear'}));
    

     
        
		
		
	
	end
	
	
	
	
	function DisplayResultInfo(obj)
		%displays result infos and asks for further analysis
		
		switch obj.CalcMode
			case 'Reasenberg' 
				foundClusters=numel(unique(obj.CalcRes.clusterID(~isnan(obj.CalcRes.clusterID))));
				inCluster=sum(obj.CalcRes.EventType>1);
				notCluster=sum(obj.CalcRes.EventType<=1);
				if iscell(obj.CalcRes.AlgoInfo.ClusterLengths);
					longestClust=max(obj.CalcRes.AlgoInfo.ClusterLengths{2});
				else
					longestClust=max(obj.CalcRes.AlgoInfo.ClusterLengths(:,2));
				end
				
				infoText={'Reasenberg declustering finished';...
					  ['Calculation Time: ',num2str(obj.CalcTime/60),' min'];... 
					  ['Number of Clusters: ',num2str(foundClusters)];...
					  ['Total Eq in the Clusters: ',num2str(inCluster)];...
					  ['Total Eq unclustered: ',num2str(notCluster)];...
					  ['LongestCluster: ',num2str(longestClust)]};
					  
				h = msgbox('Declustering is finished and has been applied to the catalog, the different parts can be switch on and off in the Commander','Calculation finished','none');
				h1 = msgbox(infoText,'Declustering Info','help');	  
					  
				%ask if further analysis is wanted;
				%Later, I need the viewer first
				
				
			case 'MonteReasenberg'
			
			
			
			case 'Gardner-Knopoff'
				foundClusters=numel(unique(obj.CalcRes.clusterID(~isnan(obj.CalcRes.clusterID))));
				inCluster=sum(obj.CalcRes.EventType>1);
				notCluster=sum(obj.CalcRes.EventType<=1);
				if iscell(obj.CalcRes.AlgoInfo.ClusterLengths);
					longestClust=max(obj.CalcRes.AlgoInfo.ClusterLengths{1});
				else
					longestClust=max(obj.CalcRes.AlgoInfo.ClusterLengths(:,1));
				end
				
				infoText={'Gardner-Knopoff declustering finished';...
					  ['Calculation Time: ',num2str(obj.CalcTime/60),' min'];... 
					  ['Number of Clusters: ',num2str(foundClusters)];...
					  ['Total Eq in the Clusters: ',num2str(inCluster)];...
					  ['Total Eq unclustered: ',num2str(notCluster)];...
					  ['LongestCluster: ',num2str(longestClust)]};
					  
				h = msgbox('Declustering is finished and has been applied to the catalog, the different parts can be switch on and off in the Commander','Calculation finished','none');
				h1 = msgbox(infoText,'Declustering Info','help');	  
					  
				%ask if further analysis is wanted;
				%Later, I need the viewer first
				
			case 'SLIDER'
				foundClusters=numel(unique(obj.CalcRes.clusterID(~isnan(obj.CalcRes.clusterID))));
				inCluster=sum(obj.CalcRes.EventType>1);
				notCluster=sum(obj.CalcRes.EventType<=1);
				
				infoText={'SLIDER declustering finished';...
					  ['Calculation Time: ',num2str(obj.CalcTime/60),' min'];... 
					  ['Number of Clusters: ',num2str(foundClusters)];...
					  ['Total Eq in the Clusters: ',num2str(inCluster)];...
					  ['Total Eq unclustered: ',num2str(notCluster)]};
			
				
				h = msgbox('Declustering is finished and has been applied to the catalog, the different parts can be switch on and off in the Commander','Calculation finished','none');
				h1 = msgbox(infoText,'Declustering Info','help');
	
		end
		
		
		
	
	end
	
	
	
	function ClusterViewer(obj)
		%allows to view the different clusters.
		
		
	end
	
	
	end
	
end	

classdef McBvalMappingWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		CalcParameter
		ProfileSwitch
		ThirdDimension
		ErrorDialog
		ParallelMode
		StartCalc
		Loaded
		CalcName
		Region
		AlphaMap
		ResultGUI
		PlotOptions
		RecSubMenu
		BorderToogle
		CoastToogle
		EQToogle
		ShadowToogle
		PlotQuality
		LastPlot
		HistoryExist
		TheAspect
		ProfileLine
		ProfileWidth
		Error2Alpha
		ErrorFactor
		FMDFilter
		FMDWindow
		FMDRad
		FMDPoly
		FMDNext
		FMDColors
		FMDMax
		IsoSurfers
		ColLimit
		ColLimitSwitch
		DrawCaps
		AlphaVal
		RoundIt
		SurferDude
		SliceIt
		SalamiConfig
		LightMyFire
		ShowItAll
	end

	events
		CalcDone

	end


	methods
	
	
	function obj = McBvalMappingWrapper(ListProxy,Commander,GUISwitch,CalcMode,CalcParameter)
		import mapseis.datastore.*;
				
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		
		if isempty(Commander)
			obj.Commander=[];
			%go into manul mode, the object will be created but everything
			%has to be done manually (useful for scripting)
		else
		
			obj.Commander = Commander; 
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Region=[];
			obj.AlphaMap=[];
			obj.FMDMax=2;
			obj.FMDFilter=cell(obj.FMDMax,1);
			obj.FMDWindow=cell(obj.FMDMax,2);
			obj.FMDNext=0;
			obj.FMDRad=km2deg(5);
			obj.FMDColors={'r','b','g','c','m'};
			obj.FMDMax=2;
			obj.IsoSurfers=[];
			obj.DrawCaps=true;
			obj.AlphaVal=0.75;
			obj.TheAspect=2;
			obj.RoundIt=false;
			obj.SliceIt=false;
			obj.SalamiConfig=[];
			obj.SurferDude=true;
			obj.LightMyFire=true;
			obj.ShowItAll=false;
			obj.ColLimit=[];
			obj.ColLimitSwitch=[];
			
			%set calcmode
			switch CalcMode
				case '2Dmap'
					obj.ThirdDimension=false;
					obj.ProfileSwitch=false;
				case 'Profile'
					obj.ThirdDimension=false;
					obj.ProfileSwitch=true;
				case '3D'
					obj.ThirdDimension=true;
					obj.ProfileSwitch=false;
					obj.TheAspect=50;	
			end
			
		
						
			obj.Loaded=false;
			obj.Error2Alpha=false;
			obj.ErrorFactor=1;
			obj.ResultGUI=[];
			obj.PlotOptions=[];
			obj.BorderToogle=true;
			obj.CoastToogle=true;
			obj.EQToogle=false;
			obj.ShadowToogle=true;
			obj.PlotQuality = 'low';
			obj.LastPlot=[];
			obj.HistoryExist=false;
			
			obj.CalcName='b-value map';
			
			%get the current datastore and filterlist and build zmap catalog
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ProfileWidth=[];
			obj.ProfileLine=[];
			
			EveryThingGood=true;
			
			%check if old data exists
			try
				PreviousCalc=obj.Datastore.getUserData('new-bval-calc');
			catch
				PreviousCalc=[];
			end
			
			if ~isempty(PreviousCalc)
				obj.HistoryExist=true;
			end
			
			
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the regionfilter');
					obj.ErrorDialog='No profile selected in the regionfilter';
				 else
				 	regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					obj.ProfileWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					obj.ProfileLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				 
				 end
				 
				 
				 
			end
			
			if EveryThingGood
				
				if ~isempty(CalcParameter)
					%Parameters are include, no default parameter will
					%be set
					obj.CalcParameter=CalcParameter;
				else
					obj.InitVariables
				end	
				
				if GUISwitch
					
					%let the user modify the parameters
					obj.openCalcParamWindow;
					
					if obj.StartCalc
						
						if ~obj.Loaded
							%Calc
							obj.doTheCalcThing;
						end
						
						%Build resultGUI
						obj.BuildResultGUI
						
						%plot results
						obj.plotResult('bvalue')
					end	
					
					
				end
				

			
			end
		end
		
	end

	
	
	
	function InitVariables(obj)
		%Now Zmap stuff needed anymore
		%some basic parameters, I will add addition parameters later 
		%if needed e.g. bootstrap option
		obj.CalcParameter = struct(	'Mc_Method',1,...
						'BootSwitch',0,...
						'BootSamples',100,...
						'McCorr',0.1,...
						'FixedMc',2.2,...
						'MinNumber',50,...
						'dx',1,...
						'dy',1,...
						'dz',1,...
						'NumEvents',100,...
						'Sel_Radius',5,...
						'Selection_Method',1,...
						'SmoothMode',0,...
						'SmoothKernelSize',5,...
						'SmoothSigma',2.5,...
						'GridMethod','regular',...
						'RecMag',6,...
						'LoadSwitch',0,...
						'Overall_bval',1,...
						'Calc_sign_bval',0,...
						'Step2Mode',0,...
						'McLoad',0);
						
		
		%GridMethod cannot be changed in the GUI for now until I improved
		%the selfrefining grid algorithmen. (options: 'regular','point' and 'selfRef')
		
		%McLoad (load McMap for 2step calc) and Step2Mode (uses 2step calculation)
		%will be used later when the mode is available.
		
		%does FixedMc really work in the original code???
		%---->NO, what to do about it?
		
	
	end
	
	
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		if ~obj.ProfileSwitch&~obj.ThirdDimension;
			Title = 'b-value, a-value and Mc v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size','SmoothKernelSize';...
				'Smooth Kernel Sigma','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (deg):','dx';...
				'Grid spacing y (deg):','dy';...
				'Min. Num. > Mc:','MinNumber';...
				'Fixed Mc (only in "Fixed Mc"):','FixedMc';...			
				'Mc Correction for Maxc:','McCorr';....
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 
				
				
				
							
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Number of Events','Constant Radius','Both'}
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			
			%Smoothing
			Formats(5,1).type='check';
			
			%Smoothing Kernel Size
			Formats(6,1).type='edit';
			Formats(6,1).format='integer';
			Formats(6,1).limits = [0 9999];
			
			%Smoothing Sigma
			Formats(6,2).type='edit';
			Formats(6,2).format='float';
			Formats(6,2).limits = [0 9999];
			
			%NumberEvents
			Formats(7,1).type='edit';
			Formats(7,1).format='integer';
			Formats(7,1).limits = [0 9999];
		
			%Radius
			Formats(7,2).type='edit';
			Formats(7,2).format='float';
			Formats(7,2).limits = [0 9999];
			
			%Grid space x
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 999];
			
			%Grid space y
			Formats(8,2).type='edit';
			Formats(8,2).format='float';
			Formats(8,2).limits = [0 999];
			
			%Min Number
			Formats(9,1).type='edit';
			Formats(9,1).format='float';
			Formats(9,1).limits = [0 9999];
			Formats(9,1).size = [-1 0];
			Formats(9,2).limits = [0 1];
			Formats(9,2).type='none';
			
			%Fixed Mc
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			
			%Mc Correction
			Formats(11,1).type='edit';
			Formats(11,1).format='float';
			Formats(11,1).limits = [-99 99];
			Formats(11,1).size = [-1 0];
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';
			
			%sign bval switch
			Formats(12,1).type='check';
			
			%overall b-val
			Formats(12,2).type='edit';
			Formats(12,2).format='float';
			Formats(12,2).limits = [0 9999];
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
			
				
		elseif obj.ProfileSwitch&~obj.ThirdDimension;
			
			Title = 'b-value, a-value and Mc v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size','SmoothKernelSize';...
				'Smooth Kernel Sigma','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (km):','dx';...
				'Grid spacing z (km):','dz';...
				'Min. Num. > Mc:','MinNumber';...
				'Fixed Mc (only in "Fixed Mc"):','FixedMc';...			
				'Mc Correction for Maxc:','McCorr';...
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 
				
				
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Number of Events','Constant Radius','Both'}
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			
			%Smoothing
			Formats(5,1).type='check';
			
			%Smoothing Kernel Size
			Formats(6,1).type='edit';
			Formats(6,1).format='integer';
			Formats(6,1).limits = [0 9999];
			
			%Smoothing Sigma
			Formats(6,2).type='edit';
			Formats(6,2).format='float';
			Formats(6,2).limits = [0 9999];
			
			%NumberEvents
			Formats(7,1).type='edit';
			Formats(7,1).format='integer';
			Formats(7,1).limits = [0 9999];
		
			%Radius
			Formats(7,2).type='edit';
			Formats(7,2).format='float';
			Formats(7,2).limits = [0 9999];
			
			%Grid space x
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 999];
			
			%Grid space y
			Formats(8,2).type='edit';
			Formats(8,2).format='float';
			Formats(8,2).limits = [0 999];
			
			%Min Number
			Formats(9,1).type='edit';
			Formats(9,1).format='float';
			Formats(9,1).limits = [0 9999];
			Formats(9,1).size = [-1 0];
			Formats(9,2).limits = [0 1];
			Formats(9,2).type='none';
			
			%Fixed Mc
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			
			%Mc Correction
			Formats(11,1).type='edit';
			Formats(11,1).format='float';
			Formats(11,1).limits = [-99 99];
			Formats(11,1).size = [-1 0];
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';
			
			%sign bval switch
			Formats(12,1).type='check';
			
			%overall b-val
			Formats(12,2).type='edit';
			Formats(12,2).format='float';
			Formats(12,2).limits = [0 9999];
			
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
				
				
		  elseif ~obj.ProfileSwitch&obj.ThirdDimension;
			
			Title = 'b-value, a-value and Mc v1.5';
			Prompt={'Mc Method:', 'Mc_Method';...
				'Mc bootstraps:','BootSwitch';...
				'Number of Bootstraps:','BootSamples';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smooth Kernel Size','SmoothKernelSize';...
				'Smooth Kernel Sigma','SmoothSigma';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (deg):','dx';...
				'Grid spacing y (deg):','dy';...
				'Grid spacing z (km):','dz';...
				'Min. Num. > Mc:','MinNumber';...
				'Fixed Mc (only in "Fixed Mc"):','FixedMc';...			
				'Mc Correction for Maxc:','McCorr';...
				'Calculate sign. b-values','Calc_sign_bval';...
				'Overall b-value (for sign. b-value)','Overall_bval'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 		
			
				
			%default values
			%Not needed, use CalcParameter directly
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 1];
			Formats(1,3).type='none';
			Formats(1,3).limits = [0 1];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='none'
			Formats(2,3).type='edit';
			Formats(2,3).format='integer';
			Formats(2,3).limits = [0 9999];
			
			
			%Selection Method
			Formats(4,1).type='list';
			Formats(4,1).style='togglebutton';
			Formats(4,1).items={'Number of Events','Constant Radius','Both'}
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			Formats(4,3).limits = [0 1];
			Formats(4,3).type='none';
			
			
			%Smoothing
			Formats(5,1).type='check';
			
			%Smoothing Kernel Size
			Formats(5,2).type='edit';
			Formats(5,2).format='integer';
			Formats(5,2).limits = [0 9999];
			
			%Smoothing sigma
			Formats(5,3).type='edit';
			Formats(5,3).format='float';
			Formats(5,3).limits = [0 9999];
			
			%NumberEvents
			Formats(6,1).type='edit';
			Formats(6,1).format='integer';
			Formats(6,1).limits = [0 9999];
			
			Formats(6,2).limits = [0 1];
			Formats(6,2).type='none';
			
			%Radius
			Formats(6,3).type='edit';
			Formats(6,3).format='float';
			Formats(6,3).limits = [0 9999];
			
			%Grid space x
			Formats(7,1).type='edit';
			Formats(7,1).format='float';
			Formats(7,1).limits = [0 999];
			
			%Grid space y
			Formats(7,2).type='edit';
			Formats(7,2).format='float';
			Formats(7,2).limits = [0 999];
			
			%Grid space y
			Formats(7,3).type='edit';
			Formats(7,3).format='float';
			Formats(7,3).limits = [0 999];
			
			%Min Number
			Formats(8,1).type='edit';
			Formats(8,1).format='float';
			Formats(8,1).limits = [0 9999];
			Formats(8,1).size = [-1 0];
			Formats(8,2).limits = [0 1];
			Formats(8,2).type='none';
			Formats(8,3).limits = [0 1];
			Formats(8,3).type='none';
			
			%Fixed Mc
			Formats(9,1).type='edit';
			Formats(9,1).format='float';
			Formats(9,1).limits = [-99 99];
			Formats(9,1).size = [-1 0];
			Formats(9,2).limits = [0 1];
			Formats(9,2).type='none';
			Formats(9,3).limits = [0 1];
			Formats(9,3).type='none';
			
			%Mc Correction
			Formats(10,1).type='edit';
			Formats(10,1).format='float';
			Formats(10,1).limits = [-99 99];
			Formats(10,1).size = [-1 0];
			Formats(10,2).limits = [0 1];
			Formats(10,2).type='none';
			Formats(10,3).limits = [0 1];
			Formats(10,3).type='none';
			
			%sign bval switch
			Formats(11,1).type='check';
			
			Formats(11,2).limits = [0 1];
			Formats(11,2).type='none';
			
			%overall b-val
			Formats(11,3).type='edit';
			Formats(11,3).format='float';
			Formats(11,3).limits = [0 9999];
			
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
		end		
		
		
		

		
		if obj.HistoryExist
			Prompt(end+1,:)={'Load Calculation?','LoadSwitch'};
			
			
			if obj.ThirdDimension
				Formats(12,1).type='check';
				Formats(12,1).size = [-1 0];
				Formats(12,2).limits = [0 1];
				Formats(12,2).type='none';	
				Formats(12,3).limits = [0 1];
				Formats(12,3).type='none';
			else
				Formats(13,1).type='check';
				Formats(13,1).size = [-1 0];
				Formats(13,2).limits = [0 1];
				Formats(13,2).type='none';	
			
			end
			
		end
		
		
		
		
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,obj.CalcParameter,Options); 
		
		if obj.HistoryExist&Canceled==0&NewParameter.LoadSwitch==1
			%Load data
			obj.StartCalc=false;
			obj.loadCalc([]);
			
		else
			obj.CalcParameter=NewParameter;
			
			
			
			if Canceled==1
				obj.StartCalc=false;
			else
				obj.StartCalc=true;
			end
		
		end
		
		
	end
	
	
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.Mc_calc.*;
		import mapseis.calc.*;
		import mapseis.util.importfilter.cellArrayToPlane;
		import mapseis.util.importfilter.cellArrayToPlane3D;
		
			%CalcConfig and CalcObject (similar for all calc)
			CalcConfig=struct(	'Mc_method',obj.CalcParameter.Mc_Method,...
						'Use_bootstrap',obj.CalcParameter.BootSwitch,...
						'Nr_bootstraps',obj.CalcParameter.BootSamples,...
						'Mc_correction',obj.CalcParameter.McCorr,...
						'MinNumber',obj.CalcParameter.MinNumber,....
						'Mc_Binning',0.1,...
						'FixedMc',obj.CalcParameter.FixedMc,...
						'Calc_sign_bval',obj.CalcParameter.Calc_sign_bval,...
						'Regional_bval',obj.CalcParameter.Overall_bval,...
						'NoCurve',true);
						
						
			%calc object			
			DenseConfig.CalcFunction=@mapseis.calc.Calc_Master_Mc_bval;
			DenseConfig.FunctionConfig=CalcConfig;
			DenseConfig.ProfileSwitch=obj.ProfileSwitch|obj.ThirdDimension;
			
			McBvalCalc = mapseis.calc.CalculationEX(...
		 		@mapseis.calc.DensityAdder,@mapseis.projector.getZMAPFormat,...
		   		DenseConfig,'Name','BvalCalc');		
		    	
		    			
		    	if ~obj.ProfileSwitch&~obj.ThirdDimension
				%normal lon lat grid
				
							
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.SendedEvents;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				CalcParam=struct(	'boundaryBox',[bounded],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy]],...
							'SelectionRadius',km2deg(obj.CalcParameter.Sel_Radius),...
							'SelectedEvents',[selected],...
							'SelectionNumber',obj.CalcParameter.NumEvents,...
							'SelectionMode',SelMode,...
							'DistanceSend',true,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=norm2DGridder(obj.Datastore,McBvalCalc,CalcParam);
		    		
				
				RawRes=cellArrayToPlane(calcRes);
				%disp(RawRes)
				fields=fieldnames(RawRes);
				%disp(RawRes.a_value_annual)
				for i=1:numel(fields)
					%disp((fields{i}))
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    		
		    		%build the AlphaMap
		    		regionFilter = obj.Filterlist.getByName('Region');
		    		regionParms = regionFilter.getRegion();

		    		if ~strcmp(regionParms{1},'all')
		    			theRegion = regionParms{2};	
		    			RawAlpha=theRegion.isInside([xvec,yvec]);
		    			try
				 		obj.AlphaMap=~logical(reshape(RawAlpha,length(yRange),length(xRange)));
				 	catch
				 		%means the polygon is badly shaped
				 		obj.AlphaMap=[];
				 		disp('could not restore AlphaMap')
				 	end	
		    			
		    		end
		    		
		    	elseif obj.ProfileSwitch&~obj.ThirdDimension
		    		%Profile mode	
		    	
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				%disp(obj.CalcParameter.Selection_Method)
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.SendedEvents;
				
				CalcParam=struct(	'boundaryBox',[pRegion.getBoundingBox],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dz]],...
							'SelectionRadius',obj.CalcParameter.Sel_Radius,...
							'SelectedEvents',[selected],...
							'Sel_Nr',obj.CalcParameter.NumEvents,...
							'Sel_Mode',SelMode,...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'DistanceSend',true,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=Profile2DGridder(obj.Datastore,McBvalCalc,CalcParam);
		    		
				RawRes=cellArrayToPlane(calcRes);
				%obj.CalcRes.X=xRange;
		    		%obj.CalcRes.Y=yRange;
		    		
		    		fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    	
		    		
		    		
		    	elseif ~obj.ProfileSwitch&obj.ThirdDimension
		    		%3D mode
		    		
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.SendedEvents;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				if strcmp(obj.CalcParameter.GridMethod,'regular')
				
					[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
					
					CalcParam=struct(	'boundaryBox',[bounded],...
								'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy obj.CalcParameter.dz]],...
								'vertical_Min_Max',[[floor(min(selectedDepth)),ceil(max(selectedDepth))]],...
								'SelectionRadius',obj.CalcParameter.Sel_Radius,...
								'SelectedEvents',[selected],...
								'SelectionNumber',obj.CalcParameter.NumEvents,...
								'SelectionMode',SelMode,...
								'DistanceSend',true,...
								'ParallelCalc',obj.ParallelMode);
								
					[xRange,yRange,zRange,calcRes]=regular3DGridder(obj.Datastore,McBvalCalc,CalcParam);
					
					
					RawRes=cellArrayToPlane3D(calcRes);
					
					fields=fieldnames(RawRes);
					for i=1:numel(fields)
						obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
					end
					
					
					
					%build the AlphaMap
					[xx2d yy2d]=meshgrid(xRange,yRange);
					le2d=numel(xx2d);
					xvec2d=reshape(xx2d,le2d,1);
					yvec2d=reshape(yy2d,le2d,1);
					
					
					%build the AlphaMap
					regionFilter = obj.Filterlist.getByName('Region');
					regionParms = regionFilter.getRegion();
	
					if ~strcmp(regionParms{1},'all')
						theRegion = regionParms{2};	
						RawAlpha=theRegion.isInside([xvec2d,yvec2d]);
						try
							obj.AlphaMap=~logical(reshape(RawAlpha,length(yRange),length(xRange)));
						catch
							%means the polygon is badly shaped
							obj.AlphaMap=[];
							disp('could not restore AlphaMap')
						end	
						
					end
					
					[xx yy zz]=meshgrid(xRange,yRange,zRange);
					le=numel(xx);
					xvec=reshape(xx,le,1);
					yvec=reshape(yy,le,1);
					zvec=reshape(zz,le,1);
					obj.CalcRes.X=xRange;
					obj.CalcRes.Y=yRange;
					obj.CalcRes.Z=zRange;
					obj.CalcRes.Xmeshed=xvec;
					obj.CalcRes.Ymeshed=yvec;
					obj.CalcRes.Zmeshed=zvec;
				
				elseif strcmp(obj.CalcParameter.GridMethod,'point')
					CalcParam=struct(	'ThePoints','selffeed',...
								'SelectionRadius',obj.CalcParameter.Sel_Radius,...
								'SelectedEvents',[selected],...
								'SelectionNumber',obj.CalcParameter.NumEvents,...
								'SelectionMode',SelMode,...
								'DistanceSend',true,...
								'ParallelCalc',obj.ParallelMode);
								
					[xRange,yRange,zRange,calcRes]=point3DGridder(obj.Datastore,McBvalCalc,CalcParam);		
				
					RawRes=cellArrayToPlane(calcRes);
					
					fields=fieldnames(RawRes);
					for i=1:numel(fields)
						obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
					end
				
				
				end
		    	end
		    	
		    %set FMDradius, it is easiest here	
		    obj.FMDRad=0.1*(max(obj.CalcRes.X)-min(obj.CalcRes.X));		
		
		
	end
	
	
	
	
	function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('Mc-Bval-map',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Mc-Bval-map',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',[ 50 300 1100 690 ],'Renderer','OpenGL');
		   plotAxis=gca;
		   set(plotAxis,'Tag','MainResultAxis');
		   
		   obj.buildMenus;
		   
		   obj.buildMouseMenu([]);
		   
		end
		
	end
	
	
	function buildMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
	
		goError='autoColor';
		
		if ~obj.ThirdDimension	
		
		
			%first the general menus (similar for both)
			OptionMenu = uimenu( obj.ResultGUI,'Label','- Load/Save');
			uimenu(OptionMenu,'Label','Calculate again',...
			'Callback',@(s,e) obj.CalcAgain);
			uimenu(OptionMenu,'Label','Show calculation parameters',...
			'Callback',@(s,e) obj.showCalcParameter);
			uimenu(OptionMenu,'Label','Reload Filterlist from Commander',...
			'Callback',@(s,e) obj.getFilterlist);
			uimenu(OptionMenu,'Label','Write to Datastore','Separator','on',... 
			'Callback',@(s,e) obj.saveCalc);
			uimenu(OptionMenu,'Label','Load from Datastore',...
			'Callback',@(s,e) obj.loadUpdate([]));
			uimenu(OptionMenu,'Label','export Result (.mat)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu(OptionMenu,'Label','export Result (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu(OptionMenu,'Label','export Result (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu(OptionMenu,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
			%Plot option menu, allows to selected plot options like plotting earthquakes
			%coast- & borderlines
			obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
			
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			
			if obj.ProfileSwitch
				uimenu(obj.PlotOptions,'Label','Set superelevation',... 
				'Callback',@(s,e) obj.setSuperElevation);
			end
			
			uimenu(obj.PlotOptions,'Label','Set FMD selection radius',... 
				'Callback',@(s,e) obj.setFMDRadius);
			
			uimenu(obj.PlotOptions,'Label','plot Coastlines','Separator','on',... 
			'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
			 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
			'Callback',@(s,e) obj.TooglePlotOptions('Border'));
			 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
			'Callback',@(s,e) obj.TooglePlotOptions('EQ'));	
			uimenu(obj.PlotOptions,'Label','Plot Full Bounding Box',...
			'Callback',@(s,e) obj.TooglePlotOptions('ShowIt'));
			uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			
			uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
			'Callback',@(s,e) obj.plot_modify([],'interp',[]));
			uimenu(obj.PlotOptions,'Label','Shading Flat',... 
			'Callback',@(s,e) obj.plot_modify([],'flat',[]));
			uimenu(obj.PlotOptions,'Label','Brighten +0.4',... 
			'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
			 uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
			'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
			
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.setqual('chk');
			
				
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','b-value map (max likelihood)',...
			'Callback',@(s,e) obj.plotVariable([],'b_value',goError,'b_value_bootstrap'));
			 uimenu(MapMenu,'Label','Standard deviation of b-Value (max likelihood) map',...
			'Callback',@(s,e) obj.plotVariable([],'b_value_bootstrap','colormap'));
			uimenu(MapMenu,'Label','Significant b-value map',...
			'Callback',@(s,e) obj.plotVariable([],'sign_bvalue','colormap_bicolor')); 
			uimenu(MapMenu,'Label','Magnitude of completness map',...
			'Callback',@(s,e) obj.plotVariable([],'Mc',goError,'Mc_bootstrap'));
			uimenu(MapMenu,'Label','Standard deviation of magnitude of completness',...
			'Callback',@(s,e) obj.plotVariable([],'Mc_bootstrap','colormap'));
			 uimenu(MapMenu,'Label','Goodness of fit to power law map',...
			'Callback',@(s,e) obj.plotVariable([],'PowerLawFit','colormap'));
			uimenu(MapMenu,'Label','Resolution map',...
			'Callback',@(s,e) obj.plotVariable([],'Resolution','colormap'));
			uimenu(MapMenu,'Label','Earthquake density map',...
			'Callback',@(s,e) obj.plotVariable([],'EqDensity','colormap'));
			uimenu(MapMenu,'Label','a-value map',...
			'Callback',@(s,e) obj.plotVariable([],'a_value','colormap'));
			
			uimenu(MapMenu,'Label','Calc Recurrence Times','Separator','on',... ...
			'Callback',@(s,e) obj.CalcRecurrence('Calc'));
			
			obj.RecSubMenu=uimenu( MapMenu,'Label','Recurrence Times','Visible','off');
			
			uimenu(obj.RecSubMenu,'Label','Recurrence time map ',...
			'Callback',@(s,e) obj.plotVariable([],'RecTime','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVol','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolLog','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area const map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConst','colormap'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/area const map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConstLog','colormap'));
			%uimenu(obj.RecSubMenu,'Label','recurrence time percentage',...
			%'Callback',@(s,e) obj.plot2DLine([],'RecTimePerc','Line'));
			
			
			uimenu(MapMenu,'Label','Color Axis Limited','Separator','on',... ...
			'Callback',@(s,e) obj.ColorRanger('switch'));
			uimenu(MapMenu,'Label','Set Color Axis',...
			'Callback',@(s,e) obj.ColorRanger('factor'));
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));
			uimenu(MapMenu,'Label','Smoothing Parameters',...
			'Callback',@(s,e) obj.Smoother('factor'));
			uimenu(MapMenu,'Label','Error to alpha channel','Separator','on',...
			'Callback',@(s,e) obj.ErrorSwitcher('switch'));
			uimenu(MapMenu,'Label','Error-Alpha Factor',...
			'Callback',@(s,e) obj.ErrorSwitcher('factor'));
		
		
			
			obj.ColorRanger('init');
			obj.Smoother('init');
			obj.ErrorSwitcher('init');
			obj.CalcRecurrence('MenuCheck');
			
		else
		
		
			%later
			%first the general menus (similar for both)
			 OptionMenu = uimenu( obj.ResultGUI,'Label','- Load/Save');
			uimenu(OptionMenu,'Label','Calculate again',...
			'Callback',@(s,e) obj.CalcAgain);
			uimenu(OptionMenu,'Label','Show calculation parameters',...
			'Callback',@(s,e) obj.showCalcParameter);
			uimenu(OptionMenu,'Label','Reload Filterlist from Commander',...
			'Callback',@(s,e) obj.getFilterlist);
			uimenu(OptionMenu,'Label','Write to Datastore','Separator','on',... 
			'Callback',@(s,e) obj.saveCalc);
			uimenu(OptionMenu,'Label','Load from Datastore',...
			'Callback',@(s,e) obj.loadUpdate([]));
			uimenu(OptionMenu,'Label','export Result (.mat)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu(OptionMenu,'Label','export Result (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu(OptionMenu,'Label','export Result (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu(OptionMenu,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
			%Plot option menu, allows to selected plot options like plotting earthquakes
			%coast- & borderlines
			obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
			
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			
			
			uimenu(obj.PlotOptions,'Label','Set superelevation',... 
			'Callback',@(s,e) obj.setSuperElevation);
			uimenu(obj.PlotOptions,'Label','Set Transparency',... 
			'Callback',@(s,e) obj.setAlphaMan);
			uimenu(obj.PlotOptions,'Label','Edit IsoSurfaces',... 
			'Callback',@(s,e) obj.setIsoSurfaces);
			uimenu(obj.PlotOptions,'Label','Edit Slices',... 
			'Callback',@(s,e) obj.setSlices);

									
			uimenu(obj.PlotOptions,'Label','plot Coastlines','Separator','on',... 
			'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
			 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
			'Callback',@(s,e) obj.TooglePlotOptions('Border'));
			 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
			'Callback',@(s,e) obj.TooglePlotOptions('EQ'));	
			uimenu(obj.PlotOptions,'Label','Plot Full Bounding Box',...
			'Callback',@(s,e) obj.TooglePlotOptions('ShowIt'));
			uimenu(obj.PlotOptions,'Label','Draw Shadows',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Shadow'));	
			uimenu(obj.PlotOptions,'Label','Draw isocaps',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Caps'));
			uimenu(obj.PlotOptions,'Label','Draw IsoSurfaces',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Surfer'));
			uimenu(obj.PlotOptions,'Label','Draw Slices',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Slicer'));
			uimenu(obj.PlotOptions,'Label','Round values',...
			'Callback',@(s,e) obj.ToogleAddPlotOptions('Rounder'));
			
			
		
			
			uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			
			uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
			'Callback',@(s,e) obj.plot_modify([],'interp',[]));
			uimenu(obj.PlotOptions,'Label','Shading Flat',... 
			'Callback',@(s,e) obj.plot_modify([],'flat',[]));
			uimenu(obj.PlotOptions,'Label','Brighten +0.4',... 
			'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
			uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
			'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
			uimenu(obj.PlotOptions,'Label','Use Lighting',... 
			'Callback',@(s,e) obj.ToogleAddPlotOptions('LightUp'));
			
			
			
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.ToogleAddPlotOptions('-chk');
			obj.setqual('chk');
			
				
		
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','b-value map (max likelihood)',...
			'Callback',@(s,e) obj.plotVariable([],'b_value','Volume3D',[]));
			 uimenu(MapMenu,'Label','Standard deviation of b-Value (max likelihood) map',...
			'Callback',@(s,e) obj.plotVariable([],'b_value_bootstrap','Volume3D',[]));
			uimenu(MapMenu,'Label','Significant b-value map',...
			'Callback',@(s,e) obj.plotVariable([],'sign_bvalue','Volume3D_bicolor',[]));
			uimenu(MapMenu,'Label','Magnitude of completness map',...
			'Callback',@(s,e) obj.plotVariable([],'Mc','Volume3D',[]));
			uimenu(MapMenu,'Label','Standard deviation of magnitude of completness',...
			'Callback',@(s,e) obj.plotVariable([],'Mc_bootstrap','Volume3D',[]));
			 uimenu(MapMenu,'Label','Goodness of fit to power law map',...
			'Callback',@(s,e) obj.plotVariable([],'PowerLawFit','Volume3D',[]));
			uimenu(MapMenu,'Label','Resolution map',...
			'Callback',@(s,e) obj.plotVariable([],'Resolution','Volume3D',[]));
			uimenu(MapMenu,'Label','Earthquake density map',...
			'Callback',@(s,e) obj.plotVariable([],'EqDensity','Volume3D',[]));
			uimenu(MapMenu,'Label','a-value map',...
			'Callback',@(s,e) obj.plotVariable([],'a_value','Volume3D',[]));
			
			uimenu(MapMenu,'Label','Calc Recurrence Times','Separator','on',... ...
			'Callback',@(s,e) obj.CalcRecurrence('Calc'));
			
			obj.RecSubMenu=uimenu( MapMenu,'Label','Recurrence Times','Visible','off');
			
			uimenu(obj.RecSubMenu,'Label','Recurrence time map ',...
			'Callback',@(s,e) obj.plotVariable([],'RecTime','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVol','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolLog','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume const map',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConst','Volume3D'));
			uimenu(obj.RecSubMenu,'Label','(1/Tr)/volume const map (Log10)',...
			'Callback',@(s,e) obj.plotVariable([],'RecTimeArVolConstLog','Volume3D'));
			%uimenu(obj.RecSubMenu,'Label','recurrence time percentage',...
			%'Callback',@(s,e) obj.plot2DLine([],'RecTimePerc','Line'));
			
			uimenu(MapMenu,'Label','Color Axis Limited','Separator','on',... ...
			'Callback',@(s,e) obj.ColorRanger('switch'));
			uimenu(MapMenu,'Label','Set Color Axis',...
			'Callback',@(s,e) obj.ColorRanger('factor'));
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));

			
		
		
			obj.ColorRanger('init');
			obj.Smoother('init');
			obj.ErrorSwitcher('init');
			obj.CalcRecurrence('MenuCheck');
		
		
		end
		
		
	end
	
	
	function buildMouseMenu(obj,graphobj)
		%get plotAxis if needed
		
		if isempty(graphobj)
			plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
				
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
			
			
			cmenu = uicontextmenu('Parent',obj.ResultGUI);
			% Now make the menu be associated with the correct axis
			set(obj.ResultGUI,'UIContextMenu',cmenu);
			set(plotAxis,'UIContextMenu',cmenu);
			% Add uimenu items for the different region selections
			% regionFilter = obj.FilterList.getByName('Region');
			% dataStore = obj.DataStore;
			
			uimenu(cmenu,'Label','Select inside',...
				'Callback',@(s,e) SetFMDRegion(obj,'in'));
			uimenu(cmenu,'Label','Select outside',...
				'Callback',@(s,e) SetFMDRegion(obj,'out'));
			uimenu(cmenu,'Label','Select circular region',...
				'Callback',@(s,e) SetFMDRegion(obj,'circle'));
			uimenu(cmenu,'Label','Close FMD',...
				'Callback',@(s,e) SetFMDRegion(obj,'closeIt'));	
			uimenu(cmenu,'Label','Redraw Map',...
				'Callback',@(s,e) updateGUI(obj));
		else
			cmenu = uicontextmenu('Parent',obj.ResultGUI);
			% Now make the menu be associated with the correct axis
			set(obj.ResultGUI,'UIContextMenu',cmenu);
			set(graphobj,'UIContextMenu',cmenu);
			% Add uimenu items for the different region selections
			% regionFilter = obj.FilterList.getByName('Region');
			% dataStore = obj.DataStore;
			
			uimenu(cmenu,'Label','Select inside',...
				'Callback',@(s,e) SetFMDRegion(obj,'in'));
			uimenu(cmenu,'Label','Select outside',...
				'Callback',@(s,e) SetFMDRegion(obj,'out'));
			uimenu(cmenu,'Label','Select circular region',...
				'Callback',@(s,e) SetFMDRegion(obj,'circle'));
			uimenu(cmenu,'Label','Close FMD',...
				'Callback',@(s,e) SetFMDRegion(obj,'closeIt'));	
			uimenu(cmenu,'Label','Redraw Map',...
				'Callback',@(s,e) updateGUI(obj));
		
		
		end
	
	
	end
	
	
	function Smoother(obj,DoWhat)
		%Turns the smoothing on and off after calculation
		SmoothMenu = findobj(obj.ResultGUI,'Label','Smoothing');
		set(SmoothMenu, 'Checked', 'off');
		
		
				
		switch DoWhat
			case 'switch'
				if obj.CalcParameter.SmoothMode==1
					obj.CalcParameter.SmoothMode=0;
					set(SmoothMenu, 'Checked', 'off');
				else
					obj.CalcParameter.SmoothMode=1;
					set(SmoothMenu, 'Checked', 'on');
				end
				obj.updateGUI;
				
				
			case 'factor'
				promptValues = {'Smooth Kernel Size','Smooth Kernel Sigma'};
				dlgTitle = 'Set Smoothing Parameters';
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.CalcParameter.SmoothKernelSize,obj.CalcParameter.SmoothSigma},'UniformOutput',false));
				obj.CalcParameter.SmoothKernelSize=round(str2double(gParams{1}));
				obj.CalcParameter.SmoothSigma=	str2double(gParams{2});
				
				if obj.CalcParameter.SmoothMode==1
					obj.updateGUI;
					set(SmoothMenu, 'Checked', 'on');
				end
			
				
			case 'init'
				set(SmoothMenu, 'Checked', 'off');
				if obj.CalcParameter.SmoothMode==1
					set(SmoothMenu, 'Checked', 'on');
				end
				
		end
	
		
	end
	
	
	
	function ColorRanger(obj,DoWhat)
		%Turns the smoothing on and off after calculation
		ColMenu = findobj(obj.ResultGUI,'Label','Color Axis Limited');
		set(ColMenu, 'Checked', 'off');
		
		
				
		switch DoWhat
			case 'switch'
				ValField=obj.LastPlot{1};
				
				%add fields if needed
				if isempty(obj.ColLimitSwitch)
					obj.ColLimitSwitch.(ValField)=true;
					obj.ColLimit.(ValField)=[];
				end
				
				if ~isfield(obj.ColLimitSwitch,ValField)
					obj.ColLimitSwitch.(ValField)=true;
					obj.ColLimit.(ValField)=[];
				
				end
				
				
				%It would be possible to directly change the color, but the 
				%axis are not really known
				if obj.ColLimitSwitch.(ValField)
					obj.ColLimitSwitch.(ValField)=false;
					set(ColMenu, 'Checked', 'off');
				else
					obj.ColLimitSwitch.(ValField)=true;
					set(ColMenu, 'Checked', 'on');
				end
				
				obj.updateGUI;
				
				
			case 'factor'
				ValField=obj.LastPlot{1};
				
				%add fields if needed
				if isempty(obj.ColLimitSwitch)
					obj.ColLimitSwitch.(ValField)=true;
					obj.ColLimit.(ValField)=[];
				end
				
				if ~isfield(obj.ColLimitSwitch,ValField)
					obj.ColLimitSwitch.(ValField)=true;
					obj.ColLimit.(ValField)=[];
				
				end
				
				
				promptValues = {'min. color Value','max. color Value '};
				dlgTitle = 'Set Color Axis Limits';
				if ~isempty(obj.ColLimit.(ValField))
					gParams = inputdlg(promptValues,dlgTitle,1,...
						cellfun(@num2str,{obj.ColLimit.(ValField)(1),obj.ColLimit.(ValField)(2)},'UniformOutput',false));
				else
					%probalby never happens
					gParams = inputdlg(promptValues,dlgTitle,1,{'',''});
				end
					
				obj.ColLimit.(ValField)(1)=str2double(gParams{1});
				obj.ColLimit.(ValField)(2)=str2double(gParams{2});
				
				
				%always set to on
				obj.ColLimitSwitch.(ValField)=true;
				set(ColMenu, 'Checked', 'on');
				obj.updateGUI;
				
				
			case 'init'
				set(ColMenu, 'Checked', 'off');
				if ~isempty(obj.LastPlot)
					ValField=obj.LastPlot{1};
					if obj.ColLimitSwitch.(ValField)
						set(ColMenu, 'Checked', 'on');
					end
				end
		end
	
		
	end
	
	
	function ErrorSwitcher(obj,DoWhat)
		%Turns the Error 2 Alpha on and off after calculation
		alphaMenu = findobj(obj.ResultGUI,'Label','Error to alpha channel');
		set(alphaMenu, 'Checked', 'off');
		
		
		switch DoWhat
			case 'switch'
				if obj.Error2Alpha==1
					obj.Error2Alpha=0;
					set(alphaMenu, 'Checked', 'off');
				else
					obj.Error2Alpha=1;
					set(alphaMenu, 'Checked', 'on');
				end
				obj.updateGUI;
				
				
			case 'factor'
				
				promptValues = {'AlphaError Factor'};
				dlgTitle = 'Set Alpha Error Factor';
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.ErrorFactor},'UniformOutput',false));
				obj.ErrorFactor=str2double(gParams{1});
				
				if obj.Error2Alpha==1
					obj.updateGUI;
					set(alphaMenu, 'Checked', 'on');
				end
			
				
			case 'init'
				set(alphaMenu, 'Checked', 'off');
				if obj.Error2Alpha==1
					set(alphaMenu, 'Checked', 'on');
				end
				
		end
	
		
	end
	
	
	
	
	function TooglePlotOptions(obj,WhichOne)
		%This functions sets the toogles in this modul
		%WhichOne can be the following strings:
		%	'Border':	Borderlines
		%	'Coast'	:	Coastlines
		%	'EQ'	:	Earthquake locations
		%	'-chk'	:	This will set all checks in the menu
		%			to the in toggles set state.
		
		CoastMenu = findobj(obj.PlotOptions,'Label','plot Coastlines');
		BorderMenu = findobj(obj.PlotOptions,'Label','plot Country Borders');
		EqMenu = findobj(obj.PlotOptions,'Label','plot Earthquake Locations');
		CutMenu= findobj(obj.PlotOptions,'Label','Plot Full Bounding Box');
		
		
		switch WhichOne
			case '-chk'
				if obj.CoastToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				if obj.BorderToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				if obj.EQToogle
					set(EqMenu, 'Checked', 'on');
				else
					set(EqMenu, 'Checked', 'off');
				end
				
				
				if obj.ShowItAll
					set(CutMenu, 'Checked', 'on');
				else
					set(CutMenu, 'Checked', 'off');
				end
				
				
			case 'Border'
				
				obj.BorderToogle=~obj.BorderToogle;
				
				if obj.BorderToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'Coast'
				
				obj.CoastToogle=~obj.CoastToogle;
				
				if obj.CoastToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'EQ'
				
				obj.EQToogle=~obj.EQToogle;
				
				if obj.EQToogle
					set(EqMenu, 'Checked', 'on');
				else
					set(EqMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
				
			case 'ShowIt'
				obj.ShowItAll=~obj.ShowItAll;
				
				if obj.ShowItAll
					set(CutMenu, 'Checked', 'on');
				else
					set(CutMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;	
			
				
		end
		
		
	end
	
	
	function ToogleAddPlotOptions(obj,WhichOne)
		%This functions sets the toogles in this modul
		%WhichOne can be the following strings:
		%	'Border':	Borderlines
		%	'Coast'	:	Coastlines
		%	'-chk'	:	This will set all checks in the menu
		%			to the in toggles set state.
		

		ShadowMenu = findobj(obj.PlotOptions,'Label','Draw Shadows');
		CapsMenu= findobj(obj.PlotOptions,'Label','Draw isocaps');
		RoundMenu= findobj(obj.PlotOptions,'Label','Round values');
		SurfMenu= findobj(obj.PlotOptions,'Label','Draw IsoSurfaces');
		SliceMenu= findobj(obj.PlotOptions,'Label','Draw Slices');
		LightMenu= findobj(obj.PlotOptions,'Label','Use Lighting');
		
		switch WhichOne
			case '-chk'

				if obj.ShadowToogle
					set(ShadowMenu, 'Checked', 'on');
				else
					set(ShadowMenu, 'Checked', 'off');
				end

				
				if obj.DrawCaps
					set(CapsMenu, 'Checked', 'on');
				else
					set(CapsMenu, 'Checked', 'off');
				end
				
				
				if obj.RoundIt
					set(RoundMenu, 'Checked', 'on');
				else
					set(RoundMenu, 'Checked', 'off');
				end
				
				
				if obj.SurferDude
					set(SurfMenu, 'Checked', 'on');
				else
					set(SurfMenu, 'Checked', 'off');
				end
				
				
				if obj.SliceIt
					set(SliceMenu, 'Checked', 'on');
				else
					set(SliceMenu, 'Checked', 'off');
				end
				
				
				if obj.LightMyFire
					set(LightMenu, 'Checked', 'on');
				else
					set(LightMenu, 'Checked', 'off');
				end
				
				
				
				
			case 'Shadow'
				
				obj.ShadowToogle=~obj.ShadowToogle;
				
				if obj.ShadowToogle
					set(ShadowMenu, 'Checked', 'on');
				else
					set(ShadowMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
				
			case 'Caps'
				obj.DrawCaps=~obj.DrawCaps;
				
				if obj.DrawCaps
					set(CapsMenu, 'Checked', 'on');
				else
					set(CapsMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
				
			case 'Rounder'
				obj.RoundIt=~obj.RoundIt;
				
				if obj.RoundIt
					set(RoundMenu, 'Checked', 'on');
				else
					set(RoundMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
				
			case 'Surfer'
				obj.SurferDude=~obj.SurferDude;
				
				if obj.SurferDude
					set(SurfMenu, 'Checked', 'on');
				else
					set(SurfMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;	
				
			case 'Slicer'
				obj.SliceIt=~obj.SliceIt;
				
				if obj.SliceIt
					set(SliceMenu, 'Checked', 'on');
				else
					set(SliceMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;	
			
			case 'LightUp'
				obj.LightMyFire=~obj.LightMyFire;
				if obj.LightMyFire
					set(LightMenu, 'Checked', 'on');
					lighting phong;
				else
					set(LightMenu, 'Checked', 'off');
					lighting none;
					
				end
			
		end
		
		
	end
	
	
	
	function plotResult(obj,whichplot)
		%more of a relict dummy function 
		%it allows a bit less complicated plotting, more for scripting
		%it calls plotVariable
		
		
		if obj.ThirdDimension
			PlotType='Volume3D';
		else
			PlotType='colormap';
		end
		
		switch whichplot
				case 'bvalue'
					%just plot phi (s2/s1 relation)
					obj.plotVariable([],'b_value',PlotType);
					
				case 'Mc'
					obj.plotVariable([],'Mc',PlotType);	
				
				%case 'Faulting'
				%	obj.plotVariable([],'fAphi','colormap');
				
				%case 'S1dir'	
				%	obj.plotVariable([],'fS1Trend',StrikeMapping);
				
				%case 'S2dir'	
				%	obj.plotVariable([],'fS2Trend',StrikeMapping);
				
				%case 'S3dir'	
					%obj.plotVariable([],'fS3Trend',StrikeMapping);
					
		end
		
			
		
		
	
	end
	
	
	function plotVariable(obj,plotAxis,ValField,PlotType,AddParam)
		%will plot the selected field of the CalcRes structur into the selected plotaxis
		%the PlotType can either be 'colormap' or 'quiver' (have to angles for a quiver plot)
		%AddParam is needed for some plots
		import mapseis.plot.*;
		import mapseis.util.AutoBiColorScale;
		
		if nargin<5
			AddParam=[];
		end
		
		if obj.ThirdDimension
			%Just for making the function not to messy
			plotVariable3D(obj,plotAxis,ValField,PlotType,AddParam);
			
		else	
		
			%get plotAxis if needed
			if isempty(plotAxis)
				 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
				 if isempty(plotAxis)
					%try retag
					%get all childrens of the main gui
					gracomp = get(obj.ResultGUI, 'Children');
					
					%find axis and return axis
					plotAxis = findobj(gracomp,'Type','axes','Tag','');
					set(plotAxis,'Tag','MainResultAxis');
				end	
			end
			
			%backup config
			obj.LastPlot={ValField,PlotType,AddParam};
			
			if strcmp(PlotType,'autoColor')
				if obj.Error2Alpha==1
					PlotType='colormap_errorAlpha';
				else
					PlotType='colormap';
				end
			
			end
			
			%get data
			xdir=obj.CalcRes.X;
			ydir=obj.CalcRes.Y;
			TheVal=obj.CalcRes.(ValField)';
			
			AlphaMap=~isnan(TheVal);
			
			%This is to prevent colorscale differences after smoothing;
			
			
			if ~isempty(obj.AlphaMap)
					minVal=min(min(TheVal(~obj.AlphaMap&AlphaMap)));	
					maxVal=max(max(TheVal(~obj.AlphaMap&AlphaMap)));
				else
					minVal=min(min(TheVal(AlphaMap)));
					maxVal=max(max(TheVal(AlphaMap)));
			end
			
			caxislimit=[floor(10*minVal)/10 ceil(10*maxVal)/10];
			if isempty(caxislimit)|any(isnan(caxislimit))
				caxislimit='auto';
			end
			%disp(caxislimit)
			
			if obj.CalcParameter.SmoothMode==1;
				%Smooth the map before plotting
				TheValNaN=isnan(TheVal);
				
				%use the mean of the data instead of 0
				if ~isempty(obj.AlphaMap)
					ValMean=mean(mean(TheVal(~obj.AlphaMap&AlphaMap)));
				else
					ValMean=mean(mean(TheVal(AlphaMap)));
				end
				
				
				
				if ~isempty(ValMean)
					TheVal(TheValNaN)=ValMean;
				else
					TheVal(TheValNaN)=0;
				end
				
				
				
				hFilter = fspecial('gaussian',obj.CalcParameter.SmoothKernelSize, obj.CalcParameter.SmoothSigma);
				TheVal= imfilter((TheVal), hFilter, 'replicate');
				
				%Smoothing adds to the total sum, subtract that to compensate.
				%TheVal=TheVal-obj.CalcParameter.SmoothFactor;
				
				TheVal(TheValNaN)=NaN;
				
				if ~isstr(caxislimit)
					%add the factor to caxislimit
					caxislimit=caxislimit;
				end
				
			end
			
			%limits
			xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
			ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
			
			%disp(xlimiter)
			%disp(ylimiter)
			if obj.ProfileSwitch
				xlab='Profile [km] ';
				ylab='Depth [km] ';
			else
				xlab='Longitude ';
				ylab='Latitude ';
			end	
			
			
			disp(plotAxis)
			disp(xlimiter)
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			latlim = get(plotAxis,'Ylim');
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis,'DrawMode','fast');
			
			
			if ~isempty(obj.AlphaMap)&~obj.ShowItAll
				TheVal(obj.AlphaMap)=NaN;
			end	
			
			%determine if a quiver is needed 
			switch PlotType
				case 'colormap'
					
					
					
					PlotData={xdir,ydir,TheVal};
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData','auto',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);
					 
					 
				case 'colormap_bicolor'
					%same as colormap but with a special scale		
					PlotData={xdir,ydir,TheVal};
					
					NewAlpha=ones(size(TheVal));
					NewAlpha(isnan(TheVal))=0;
					RegVal=TheVal==obj.CalcParameter.Overall_bval;
					NewAlpha(RegVal)=0.5;
									
					
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData',NewAlpha,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);		
					 prevCaxis=caxis(plotAxis);
					 minCal=floor(prevCaxis(1)*10)/10;
					 maxCal=ceil(prevCaxis(2)*10)/10;
					 overallVal=obj.CalcParameter.Overall_bval;
					
								 
					 
					 %build scale
					 biScale=AutoBiColorScale(minCal,maxCal,overallVal);
				
					 %set axis
					 caxis(plotAxis,[minCal maxCal]);
					 colormap(plotAxis,biScale);
	
					 
					 
				case 'colormap_errorAlpha'
					%The Additonal Parameter has here to name a field which 
					%will be used as AlphaData
					try
						TheError=obj.CalcRes.(AddParam)';
					catch 	
						TheError=NaN;
						disp('Field did not exist');
					end
					
									
					if ~all(isnan(TheError))
						minErr=nanmin(nanmin(TheError));	
						maxErr=nanmax(nanmax(TheError));
						NewAlpha=ones(size(TheError))-obj.ErrorFactor*(TheError-minErr)/abs(maxErr-minErr);
						NewAlpha(isnan(TheVal))=0;
					
					else
						NewAlpha='auto';
									
					end
					
				
					PlotData={xdir,ydir,TheVal};
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','ColorPlot',...
									'Data',{PlotData},...
									'MapStyle','smooth',...
									'AlphaData',NewAlpha,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					 %disp(plotAxis)				
					 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
					 buildMouseMenu(obj,handle);	
				
					 
				case 'quiver'
					
						
					if obj.CalcParameter.Calc_Method==1
						xdir=obj.CalcRes.Xmeshed;
						ydir=obj.CalcRes.Ymeshed;
						TheVal=reshape(TheVal,numel(xdir),1);
						AlphaMap=reshape(AlphaMap,numel(xdir),1);
						xdir=xdir(AlphaMap);
						ydir=ydir(AlphaMap);
						TheVal=TheVal(AlphaMap);
						
					else
						%might need to be done something, but I will see
						%SATSI has its problems anyway
					end
					
					%calculate dx and dy for the angle (TheVal
					%only up 180� used
					le=0.2;
					
					TheVal=mod(TheVal,180);
	
					%first the angles smaller than 90�
					sma=TheVal<90;
	
					%the larger than 90� angels
					lar=not(sma);
	
					%the x coordinate
					avect(:,1) = sin(deg2rad(TheVal))*le;
	
					%the sma part
					avect(sma,2) = cos(deg2rad(TheVal(sma)))*le;
					
					%the lar part
					avect(lar,2) = -cos(deg2rad(TheVal(lar) - 90))*le;
					
					PlotData=[xdir,ydir,avect(:,1),avect(:,2)];
					
					PlotConfig	= struct(	'PlotType','Vector2D',...
									'Data',[PlotData],...
									'MapStyle','not needed',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Colors','red',...
									'LineStylePreset','normal',...
									'MarkerStylePreset','none',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'C_Axis_Limit','auto',...
									'ColorToggle',true,...
									'LegendText',ValField);
					disp(plotAxis)
					 [handle legendentry] = PlotVector2D(plotAxis,PlotConfig);
			end	
			
			if ~obj.ProfileSwitch
				%at the moment only available in the map view
				%plot coastline, borders and earthquake locations if wanted
				if obj.BorderToogle
					hold on
					BorderConf= struct('PlotType','Border',...
								'Data',obj.Datastore,...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'LineStylePreset','normal',...
								'Colors','black');
					[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
					
				end
				
				
				if obj.CoastToogle
					hold on
					CoastConf= struct( 'PlotType','Coastline',...
							   'Data',obj.Datastore,...
							   'X_Axis_Label','Longitude ',...
							   'Y_Axis_Label','Latitude ',...
							   'LineStylePreset','Fatline',...
							   'Colors','blue');
					[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
				
				end
				
				
				if obj.EQToogle
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'LegendText','Earthquakes');
				
					
					[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
				end
					
				%set all axes right
				xlim(plotAxis,xlimiter);
				ylim(plotAxis,ylimiter);
				
				latlim = get(plotAxis,'Ylim');
				set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
				set(plotAxis,'DrawMode','fast');
				set(plotAxis,'LineWidth',2,'FontSize',12);
				
				%set title
				theTit=title(ValField);
				set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
				
				
				%rebuild Polygons
				hold on
				obj.renewPoly(plotAxis);
				hold off
				
			else
				
				
				%The filterlist has to available for now, means now eq plot on
				%profiles with loaded data and missing filter.
				if obj.EQToogle&~isempty(obj.ProfileWidth)&~isempty(obj.ProfileLine)
					
			
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'ProfileLine',obj.ProfileLine,...
								'ProfileWidth',obj.ProfileWidth,...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Distance [km] ',...
								'Y_Axis_Label','Depth [km] ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'LegendText','Earthquakes');
				
					
					[handle1 entry1] = PlotEarthquakeSlice(plotAxis,EqConfig);
				end
					
				%set all axes right
				xlim(plotAxis,xlimiter);
				ylim(plotAxis,ylimiter);
				
				%depthlim = get(plotAxis,'Ylim');
				%distlim = get(plotAxis,'Xlim');
				%deSpan=10*abs(depthlim(1)-depthlim(2));
				%diSpan=abs(distlim(1)-distlim(2));
				
				%set(plotAxis,'dataaspect',[1 diSpan/deSpan 1]);
				set(plotAxis,'dataaspect',[obj.TheAspect 1 1]);
				set(plotAxis,'YDir','reverse');
				set(plotAxis,'LineWidth',2,'FontSize',12);
				
				%set title
				theTit=title(ValField);
				set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
				
				%rebuild Polygons
				hold on
				obj.renewPoly(plotAxis);
				hold off
			end
			
			
		end	
		
		
		%Complicated looking but should be save, and automatically set coloraxis when needed
		%set color axis for both 3D and 2D
		ColMenu = findobj(obj.ResultGUI,'Label','Color Axis Limited');
		set(ColMenu, 'Checked', 'off');
		
		if ~isempty(obj.ColLimitSwitch)
			if isfield(obj.ColLimitSwitch,ValField)
				if isfield(obj.ColLimit,ValField)
					if obj.ColLimitSwitch.(ValField)
						if ~isempty(obj.ColLimit.(ValField))
							caxis(obj.ColLimit.(ValField))
							set(ColMenu, 'Checked', 'on');
						else
	
							obj.ColLimit.(ValField)=caxis;
						end	
					else
						if isempty(obj.ColLimit.(ValField))
							obj.ColLimit.(ValField)=caxis;
						end	
					end	
				else
					obj.ColLimit.(ValField)=caxis;
					
				end
			
			else
				obj.ColLimitSwitch.(ValField)=false;
				obj.ColLimit.(ValField)=caxis;	
			end
			
		
		else
			obj.ColLimitSwitch.(ValField)=false;
			obj.ColLimit.(ValField)=caxis;		
		
		end
		
		
	end
	
	
	
	
	
	
	
	function plotVariable3D(obj,plotAxis,ValField,PlotType,AddParam)
		%same in 3D with a little less options.
		import mapseis.plot.*;
		import mapseis.util.AutoBiColorScale;
		import mapseis.projector.*;
		
		if nargin<5
			AddParam=[];
		end
		
	
		
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
				
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
		end
			
		%backup config
		obj.LastPlot={ValField,PlotType,AddParam};
			
		
		
		%get data
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		zdir=obj.CalcRes.Z;
		
		TheVal=obj.CalcRes.(ValField);
		
		%limits
		xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
		ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
		zlimiter=[(min(zdir))-0.5 (max(zdir))+0.5];
		
		%labels
		xlab='Longitude (deg) ';
		ylab='Latitude (deg) ';
		zlab='Depth (km) ';
		
		if obj.RoundIt
			%round values to 0.1 accuracy
			TheVal=round(TheVal*10)/10;
		
		end
		
		
		%clear all;
		plot3(plotAxis,mean(xlimiter),mean(ylimiter),0,'Color','w');
		
		switch PlotType
			case 'Volume3D'
				
				if ~isempty(obj.AlphaMap)&~obj.ShowItAll
					for i=1:numel(TheVal(1,1,:))
						TempVal=TheVal(:,:,i);
						TempVal(obj.AlphaMap)=NaN;
						TheVal(:,:,i)=TempVal;
					end
				
				end
				minVal=nanmin(nanmin(nanmin(TheVal)));
				maxVal=nanmax(nanmax(nanmax(TheVal)));
				caxislimit=[minVal maxVal];
				
				if obj.CalcParameter.SmoothMode==1;
					TheVal=smooth3(TheVal,'box',3);
				end
				
				%Set default surfaces if needed
				if isfield(obj.IsoSurfers,ValField)
					CustomSwitch=obj.IsoSurfers.(ValField){1};
					TheSurfs=obj.IsoSurfers.(ValField){2};
					CustomSurf=obj.IsoSurfers.(ValField){3};
				else
					CustomSwitch=false;
					TheSurfs=[minVal maxVal abs(maxVal-minVal)/10];
					CustomSurf=TheSurfs(1):TheSurfs(3):TheSurfs(2);
					obj.IsoSurfers.(ValField){3}=CustomSurf;
					obj.IsoSurfers.(ValField){2}=TheSurfs;
					obj.IsoSurfers.(ValField){1}=CustomSwitch;
				
				end
				
				
				%Set default slice if needed
				if isfield(obj.SalamiConfig,ValField)
					CustomXSwitch=obj.SalamiConfig.(ValField){1,1};
					TheXSlice=obj.SalamiConfig.(ValField){1,2};
					CustomXSlice=obj.SalamiConfig.(ValField){1,3};
					
					CustomYSwitch=obj.SalamiConfig.(ValField){2,1};
					TheYSlice=obj.SalamiConfig.(ValField){2,2};
					CustomYSlice=obj.SalamiConfig.(ValField){2,3};
					
					CustomZSwitch=obj.SalamiConfig.(ValField){3,1};
					TheZSlice=obj.SalamiConfig.(ValField){3,2};
					CustomZSlice=obj.SalamiConfig.(ValField){3,3};
					
				else
					%only build vertical slices in one direction (larger one)
					if numel(xdir)>=numel(ydir)
						CustomXSwitch=false;
						TheXSlice=[min(xdir) max(xdir) abs(max(xdir)-min(xdir))/6];
						CustomXSlice=TheXSlice(1):TheXSlice(3):TheXSlice(2);
						
						CustomYSwitch=false;
						TheYSlice=[];
						CustomYSlice=[];
						
					else
						CustomXSwitch=false;
						TheXSlice=[];
						CustomXSlice=[];
					
						CustomYSwitch=false;
						TheYSlice=[min(ydir) max(ydir) abs(max(ydir)-min(ydir))/6];
						CustomYSlice=TheYSlice(1):TheYSlice(3):TheYSlice(2);
						
						
					end
					
					%Standard horizotal 4 slices
					CustomZSwitch=false;
					TheZSlice=[min(zdir) max(zdir) abs(max(zdir)-min(zdir))/4];
					CustomZSlice=TheZSlice(1):TheZSlice(3):TheZSlice(2);
					
					
					obj.SalamiConfig.(ValField){1,1}=CustomXSwitch;
					obj.SalamiConfig.(ValField){1,2}=TheXSlice;
					obj.SalamiConfig.(ValField){1,3}=CustomXSlice;
					
					obj.SalamiConfig.(ValField){2,1}=CustomYSwitch;
					obj.SalamiConfig.(ValField){2,2}=TheYSlice;
					obj.SalamiConfig.(ValField){2,3}=CustomYSlice;
					
					obj.SalamiConfig.(ValField){3,1}=CustomZSwitch;
					obj.SalamiConfig.(ValField){3,2}=TheZSlice;
					obj.SalamiConfig.(ValField){3,3}=CustomZSlice;
				
				end
				
				
				
				PlotData={xdir,ydir,zdir,TheVal};
				
				
				
				if obj.SurferDude
				
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Volume3D',...
									'Data',{PlotData},...
									'Surfaces',TheSurfs,...
									'UseCustom',CustomSwitch,...
									'CustomSurfaces',CustomSurf,...
									'DrawCaps',obj.DrawCaps,...
									'AlphaValue',obj.AlphaVal,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotVolume3D(plotAxis,PlotConfig); 
				end
				
				
				if obj.SliceIt
					hold on
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Slice3D',...
									'Data',{PlotData},...
									'ZSlice',TheZSlice,...
									'UseCustom_Z',CustomZSwitch,...
									'CustomZSlice',CustomZSlice,...
									'XSlice',TheXSlice,...
									'UseCustom_X',CustomXSwitch,...
									'CustomXSlice',CustomXSlice,...
									'YSlice',TheYSlice,...
									'UseCustom_Y',CustomYSwitch,...
									'CustomYSlice',CustomYSlice,...
									'Use3DSlice',false,...
									'Custom3DSlice',[[]],...
									'AlphaValue',obj.AlphaVal,...
									'ShadeMode','flat',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotSlice3D(plotAxis,PlotConfig); 
					
				
				
				end
				
				
				if obj.EQToogle
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit','auto',...
								'Y_Axis_Limit','auto',...
								'Z_Axis_Limit','auto',...
								'C_Axis_Limit','auto',...
								'DepthReverse',true,...
								'FastMode',false,...
								'LegendText','Earthquakes')	
				
					
					[handle1 entry1] = PlotEarthquake3D(plotAxis,EqConfig);
					
					if obj.ShadowToogle
						hold on
						%prepare data
						[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
						[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
						[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
						maxDepth=zlimiter(2)+1;
						NrEvents=numel(selectedDepth);
						NrNot=numel(unselectedDepth);
						
						seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
						
						ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',false,...
						'LegendText','Earthquakes');
						
						[shadhandle2 shadentry2] = PlotEarthquake3D(plotAxis,ShadConfig);
					
					end
					
					
					
				end
				
				%obj.BorderToogle=false;
				if obj.BorderToogle
					  hold on	
					  BorderConf= struct(	'PlotType','Border',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','normal',...
								  'Colors','black');
								  [BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
					  
				end
					  
				  
				%obj.CoastToogle=false;	
				if obj.CoastToogle
					  hold on
					  CoastConf= struct( 	'PlotType','Coastline',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','Fatline',...
								  'Colors','blue');
					  [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
					  
				end
				
				camlight;
				camlight('headlight');
				
				if obj.LightMyFire
					lighting phong;
				else
					lighting none;
				end
				
				hold off
				
				
			case 'Volume3D_bicolor'
				if ~isempty(obj.AlphaMap)&~obj.ShowItAll
					for i=1:numel(TheVal(1,1,:))
						TempVal=TheVal(:,:,i);
						TempVal(obj.AlphaMap)=NaN;
						TheVal(:,:,i)=TempVal;
					end
				end	
								
				
				minVal=nanmin(nanmin(nanmin(TheVal)));
				maxVal=nanmax(nanmax(nanmax(TheVal)));
				caxislimit=[minVal maxVal];
				
				if obj.CalcParameter.SmoothMode==1;
					TheVal=smooth3(TheVal,'box',3);
				end
				
				if isfield(obj.IsoSurfers,ValField)
					CustomSwitch=obj.IsoSurfers.(ValField){1};
					TheSurfs=obj.IsoSurfers.(ValField){2};
					CustomSurf=obj.IsoSurfers.(ValField){3};
				else
					CustomSwitch=false;
					TheSurfs=[minVal maxVal abs(maxVal-minVal)/10];
					CustomSurf=TheSurfs(1):TheSurfs(3):TheSurfs(2);
					obj.IsoSurfers.(ValField){3}=CustomSurf;
					obj.IsoSurfers.(ValField){2}=TheSurfs;
					obj.IsoSurfers.(ValField){1}=CustomSwitch;
				
				end
				
				%create colorbar
				minVal=nanmin(nanmin(nanmin(TheVal)));
				maxVal=nanmax(nanmax(nanmax(TheVal)));
				caxislimit=[minVal maxVal];
				%build scale
				overallVal=obj.CalcParameter.Overall_bval;
				biScale=AutoBiColorScale(minVal,maxVal,overallVal);
				
				
				
				%Set default slice if needed
				if isfield(obj.SalamiConfig,ValField)
					CustomXSwitch=obj.SalamiConfig.(ValField){1,1};
					TheXSlice=obj.SalamiConfig.(ValField){1,2};
					CustomXSlice=obj.SalamiConfig.(ValField){1,3};
					
					CustomYSwitch=obj.SalamiConfig.(ValField){2,1};
					TheYSlice=obj.SalamiConfig.(ValField){2,2};
					CustomYSlice=obj.SalamiConfig.(ValField){2,3};
					
					CustomZSwitch=obj.SalamiConfig.(ValField){3,1};
					TheZSlice=obj.SalamiConfig.(ValField){3,2};
					CustomZSlice=obj.SalamiConfig.(ValField){3,3};
					
				else
					%only build vertical slices in one direction (larger one)
					if numel(xdir)>=numel(ydir)
						CustomXSwitch=false;
						TheXSlice=[min(xdir) max(xdir) abs(max(xdir)-min(xdir))/6];
						CustomXSlice=TheXSlice(1):TheXSlice(3):TheXSlice(2);
						
						CustomYSwitch=false;
						TheYSlice=[];
						CustomYSlice=[];
						
					else
						CustomXSwitch=false;
						TheXSlice=[];
						CustomXSlice=[];
					
						CustomYSwitch=false;
						TheYSlice=[min(ydir) max(ydir) abs(max(ydir)-min(ydir))/6];
						CustomYSlice=TheYSlice(1):TheYSlice(3):TheYSlice(2);
						
						
					end
					
					%Standard horizotal 4 slices
					CustomZSwitch=false;
					TheZSlice=[min(zdir) max(zdir) abs(max(zdir)-min(zdir))/4];
					CustomZSlice=TheZSlice(1):TheZSlice(3):TheZSlice(2);
					
					
					obj.SalamiConfig.(ValField){1,1}=CustomXSwitch;
					obj.SalamiConfig.(ValField){1,2}=TheXSlice;
					obj.SalamiConfig.(ValField){1,3}=CustomXSlice;
					
					obj.SalamiConfig.(ValField){2,1}=CustomYSwitch;
					obj.SalamiConfig.(ValField){2,2}=TheYSlice;
					obj.SalamiConfig.(ValField){2,3}=CustomYSlice;
					
					obj.SalamiConfig.(ValField){3,1}=CustomZSwitch;
					obj.SalamiConfig.(ValField){3,2}=TheZSlice;
					obj.SalamiConfig.(ValField){3,3}=CustomZSlice;
				
				end
				
				
				PlotData={xdir,ydir,zdir,TheVal};
				
				if obj.SurferDude
				
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Volume3D',...
									'Data',{PlotData},...
									'Surfaces',TheSurfs,...
									'UseCustom',CustomSwitch,...
									'CustomSurfaces',CustomSurf,...
									'DrawCaps',obj.DrawCaps,...
									'AlphaValue',obj.AlphaVal,...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotVolume3D(plotAxis,PlotConfig); 
				
				end
				
				
				if obj.SliceIt
					hold on
					
					%build config for the plot
					PlotConfig	= struct(	'PlotType','Slice3D',...
									'Data',{PlotData},...
									'ZSlice',TheZSlice,...
									'UseCustom_Z',CustomZSwitch,...
									'CustomZSlice',CustomZSlice,...
									'XSlice',TheXSlice,...
									'UseCustom_X',CustomXSwitch,...
									'CustomXSlice',CustomXSlice,...
									'YSlice',TheYSlice,...
									'UseCustom_Y',CustomYSwitch,...
									'CustomYSlice',CustomYSlice,...
									'Use3DSlice',false,...
									'Custom3DSlice',[[]],...
									'AlphaValue',obj.AlphaVal,...
									'ShadeMode','flat',...
									'X_Axis_Label',xlab,...
									'Y_Axis_Label',ylab,...
									'Z_Axis_Label',zlab,...
									'Colors','jet',...
									'X_Axis_Limit',xlimiter,...
									'Y_Axis_Limit',ylimiter,...
									'Z_Axis_Limit',zlimiter,...
									'C_Axis_Limit',caxislimit,...
									'ColorToggle',true,...
									'LegendText',ValField);
					%disp(plotAxis)				
					[handle legendentry] = PlotSlice3D(plotAxis,PlotConfig); 
					
				
				
				end
				
				if obj.EQToogle
					hold on
					EqConfig= struct(	'PlotType','Earthquakes',...
								'Data',obj.Datastore,...
								'PlotMode','old',...
								'PlotQuality',obj.PlotQuality,...
								'SelectionSwitch','selected',...
								'SelectedEvents',obj.SendedEvents,...
								'MarkedEQ','max',...
								'X_Axis_Label','Longitude ',...
								'Y_Axis_Label','Latitude ',...
								'MarkerSize','none',...
								'MarkerStylePreset','none',...
								'Colors','black',...
								'X_Axis_Limit','auto',...
								'Y_Axis_Limit','auto',...
								'Z_Axis_Limit','auto',...
								'C_Axis_Limit','auto',...
								'DepthReverse',true,...
								'FastMode',false,...
								'LegendText','Earthquakes')	
				
					
					[handle1 entry1] = PlotEarthquake3D(plotAxis,EqConfig);
					
					if obj.ShadowToogle
						hold on
						%prepare data
						[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
						[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
						[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
						maxDepth=zlimiter(2)+1;
						NrEvents=numel(selectedDepth);
						NrNot=numel(unselectedDepth);
						
						seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
						
						ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',false,...
						'LegendText','Earthquakes');
						
						[shadhandle2 shadentry2] = PlotEarthquake3D(plotAxis,ShadConfig);
					
					end
					
					
					
				end
				
				%obj.BorderToogle=false;
				if obj.BorderToogle
					  hold on	
					  BorderConf= struct(	'PlotType','Border',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','normal',...
								  'Colors','black');
								  [BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
					  
				end
					  
				  
				%obj.CoastToogle=false;	
				if obj.CoastToogle
					  hold on
					  CoastConf= struct( 	'PlotType','Coastline',...
								  'Data',obj.Datastore,...
								  'X_Axis_Label','Longitude ',...
								  'Y_Axis_Label','Latitude ',...
								  'LineStylePreset','Fatline',...
								  'Colors','blue');
					  [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
					  
				end
				
				%%set axis
				caxis(plotAxis,[minVal maxVal]);
				colormap(plotAxis,biScale);
				camlight;
				camlight('headlight');
				
				if obj.LightMyFire
					lighting phong;
				else
					lighting none;
				end
				
				hold off
			
				
			case 'Scatter3D'
				xdir=obj.CalcRes.Xmeshed;
				ydir=obj.CalcRes.Ymeshed;
				zdir=obj.CalcRes.Zmeshed;
				if ~isempty(obj.AlphaMap)
					TheVal(obj.AlphaMap)=NaN;
				end
				
				%LATER, when I finished scatter options
				
				
			
			case 'Scatter3D_bicolor'
				xdir=obj.CalcRes.Xmeshed;
				ydir=obj.CalcRes.Ymeshed;
				zdir=obj.CalcRes.Zmeshed;
				if ~isempty(obj.AlphaMap)
					TheVal(obj.AlphaMap)=NaN;
				end
		
				%LATER, when I finished scatter options
		end
			
		
				
			
			
			
			
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			zlim(plotAxis,zlimiter);
		
				
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(ylimiter)) obj.TheAspect]);
			set(plotAxis,'ZDir','reverse');
			box off
			set(plotAxis,'DrawMode','fast','XColor',[0.7 0.7 0.7],'YColor',[0.7 0.7 0.7],'ZColor',[0.7 0.7 0.7]);
			hold off
			
			
		
	
	
	end
	
	
	function updateGUI(obj)
		%uses the last plot option and plot it again
		ValField=obj.LastPlot{1};
		PlotType=obj.LastPlot{2};
		AddParam=obj.LastPlot{3};
		
		%only needed for quality change a similar stuff
		obj.plotVariable([],ValField,PlotType,AddParam);
		
		
	end
	
	
	function plot_modify(obj,bright,interswitch,circleGrid)
		%allows to modify the plot (some legacy stuff which will be changed
		%later if needed is in here)
		 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(plotAxis)
			 %try retag
			 %get all childrens of the main gui
    			gracomp = get(obj.ResultGUI, 'Children');
    			
    			%find axis and return axis
    			plotAxis = findobj(gracomp,'Type','axes','Tag','');
    			set(plotAxis,'Tag','MainResultAxis');
    		end	
		
    		if ~isempty(bright)
			axes(plotAxis);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			
			switch interswitch
				case 'flat'
					axes(plotAxis);
					shading flat;
					
				case 'interp'
					axes(plotAxis);
					shading interp;
					
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		
	end
	
	
	function adju(obj,asel, whichplot)
		%function version of adju
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;


		prompt={'Enter the minimum magnitude cut-off','Enter the maximum radius cut-off:','Enter the minimum goodness of fit percatge'};
		def={'nan','nan','nan'};
		dlgTitle='Input Map subselection Criteria';
		lineNo=1;
		answer=inputdlg(prompt,dlgTitle,lineNo,def);
		re4 = re3;
		l = answer{1,1}; Mmin = str2num(l) ;
		l = answer{2,1}; tresh = str2num(l) ;
		l = answer{3,1}; minpe = str2num(l) ;
		
		
		obj.PackList=[obj.PackList; {'re4';'l';'asel'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

		%back to plot
		obj.plot_bval(whichplot);
		
	end
	
	function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	
	
	function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		EveryThingGood=true;
		
		
		if isempty(obj.Filterlist)
			warndlg('New filterlist will be set')
		end
		
		
		%get parameters
		obj.openCalcParamWindow;
		
		
		
		if obj.StartCalc
			
			%figNr=findobj('Name','b-value-map');
			
			%if ~isempty(figNr)
			%	close(figNr);
			%end
		
			%refresh catalogs
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ProfileLine=[];
			obj.ProfileWidth=[];
			%obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
						
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the current regionfilter');
					obj.ErrorDialog='No profile selected in the current regionfilter';
				 else
				 	regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					obj.ProfileWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					obj.ProfileLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				 
				 end
			end
			
			if EveryThingGood
				%Calculation
				obj.doTheCalcThing;
				obj.CalcRecurrence('MenuCheck');
				
				%check that no non existing field is selected
				if ~isfield(obj.CalcRes,obj.LastPlot{1})
					%set to b-value
					obj.LastPlot{1}='b_value';
					if ~obj.ThirdDimension
						obj.LastPlot{2}='colormap';
					else
						obj.LastPlot{2}='Volume3D';
					end
					
					obj.LastPlot{3}=[];
				end
				
				obj.updateGUI;
				
				%Build resultGUI
				%obj.BuildResultGUI
						
				%plot results
				%obj.plotResult('stress')
			end	
		end
	
	
	end
	
	
	function saveCalc(obj)
		%This will save the calculation in the datastore and ask to open it next time
		%The filterlist will be saved within the datastore with this and reopened next time
		%when the calculation is loaded. It has to be done in this way or else nobody knows 
		%what filtersetting was used in the calculation.
		
		%Ask for a name
		gParams = inputdlg('Calculation Name','Name of the Calculation',1,{obj.CalcName});
		if ~isempty(gParams)
			obj.CalcName=gParams{1};
			
			%New Save algorithm without filterlist
			SaveStruct=struct(	'Selected',obj.SendedEvents,...
						'CalcResult',obj.CalcRes,...
						'CalcParameter',obj.CalcParameter,...
						'ProfileSwitch',obj.ProfileSwitch,...
						'ThirdDimension',obj.ThirdDimension,...
						'CalcName',obj.CalcName,...
						'AlphaMap',obj.AlphaMap,...
						'ProfileWidth',obj.ProfileWidth,...
						'ProfileLine',obj.ProfileLine,...
						'IsoSurfers',obj.IsoSurfers,...
						'SliceConfig',obj.SalamiConfig,...
						'ColLimitSwitch',obj.ColLimitSwitch,...
						'ColLimit',obj.ColLimit,...
						'VersionCode','v1.56');
						
			
						
			CalcObject={obj.CalcName,SaveStruct};
			
			%try getting the CalcObject from the datastore
			
			try 
				Calculations=obj.Datastore.getUserData('new-bval-calc');
				
			catch
				Calculations={};
			end
			
			if ~isempty(Calculations)
				%try finding the calc
					founded=strcmp(Calculations(:,1),obj.CalcName);
					if sum(founded)>0
						Calculations(founded,:)=CalcObject;
					else
						Calculations(end+1,:)=CalcObject;
					end	
			else
				Calculations(1,:)=CalcObject;
			end	
			
			obj.Datastore.setUserData('new-bval-calc',Calculations);
			
		end
		
		
		
		%write to the Commander (always do this even if canceled, this will update
		%the buffers)
		obj.Commander.pushWithKey(obj.Keys.ShownDataID,obj.Datastore);
		obj.Commander.switcher('Cata');
		
	end
	
	
	function showCalcParameter(obj)
		%opens a message box and shows the parameter, usefull if a
		%calculation is loaded;
		
		TheFields=fieldnames(obj.CalcParameter);
		
		for i=1:numel(TheFields)
			TextBuild(i)={[TheFields{i},': ',num2str(obj.CalcParameter.(TheFields{i}))]};
			
		end
		
		msgbox(TextBuild,'Parameters of the current calculation');
		
		
	end
	
	
	function loadUpdate(obj,oldCalcs)
		%Just calles load and runs an update afterwards
		%It is needed for the menu
		
		loadCalc(obj,oldCalcs);
		obj.updateGUI;
	
	end
	
	
	function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		if isempty(oldCalcs)
			%check if old calculations are avialable
			try
				oldCalcs=obj.Datastore.getUserData('new-bval-calc');
			catch
				disp('no calculation available in this datastore')
				return;
			end
		end
		
		
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
			obj.Loaded=false;
		else
			obj.StartCalc=true;
			obj.Loaded=true;
		end
		
		%kill FMDfilters if needed
		obj.FMDSuicide;
		
		if obj.StartCalc
			TheCalc=oldCalcs{NewParameter.WhichOne,2};
			
			
			%Now set the data
			obj.Filterlist = [];
			%obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			obj.SendedEvents=TheCalc.Selected;
			obj.CalcParameter=TheCalc.CalcParameter;
			obj.ProfileSwitch=TheCalc.ProfileSwitch;
			obj.ThirdDimension=TheCalc.ThirdDimension;
			obj.CalcName=TheCalc.CalcName;
			obj.AlphaMap=TheCalc.AlphaMap;
			obj.CalcRes=TheCalc.CalcResult;
			
			
			
			
		end
		
		if strcmp(TheCalc.VersionCode,'v1.5')
			%add missing parameter if needed
			obj.CalcParameter.Overall_bval=1;
			obj.CalcParameter.Calc_sign_bval=0;
			MapSize=size(obj.CalcRes.Mc);
			
			obj.CalcRes.sign_bvalue=NaN(MapSize);
			obj.CalcRes.sign_WinningModel=NaN(MapSize);
			obj.CalcRes.sign_likelihood1=NaN(MapSize);
			obj.CalcRes.sign_likelihood2=NaN(MapSize);
			obj.CalcRes.sign_akaike1=NaN(MapSize);
			obj.CalcRes.sign_akaike2=NaN(MapSize);
			
			msgbox('Missing results were added as NaN')
			
		end
		
		if any(strcmp(TheCalc.VersionCode,{'v1.55','v1.5'}))
			obj.ProfileWidth=[];
			obj.ProfileLine=[];
		
		else
			obj.ProfileWidth=TheCalc.ProfileWidth;
			obj.ProfileLine=TheCalc.ProfileLine;
		
		end
		
		if isfield(TheCalc,'ColLimitSwitch')
			obj.ColLimitSwitch=TheCalc.ColLimitSwitch;
			obj.ColLimit=TheCalc.ColLimit;
		else
			obj.ColLimitSwitch=[];
			obj.ColLimit=[];
		end
		
		
		if isfield(obj.CalcParameter,'SmoothFactor')
			%remove old smoothing parameter and add new
			obj.CalcParameter=rmfield(obj.CalcParameter,'SmoothFactor');
			obj.CalcParameter.SmoothKernelSize=5;
			obj.CalcParameter.SmoothSigma=2.5;
			disp('Kernel Parameter set');
		else
			disp(obj.CalcParameter);
			if ~isfield(obj.CalcParameter,'SmoothKernelSize')
				obj.CalcParameter.SmoothKernelSize=5;
			end
			
			if ~isfield(obj.CalcParameter,'SmoothSigma')
				obj.CalcParameter.SmoothSigma=2,5;
			end
			
		end
		
		
		if ~isfield(obj.CalcParameter,'RecMag')
			obj.CalcParameter.RecMag=6;
		end
		
		
		try
			obj.IsoSurfers=TheCalc.IsoSurfers;
		end
		
		
		try
			obj.SalamiConfig=TheCalc.SliceConfig;
		end
		
		%reset FMDrad
		obj.FMDRad=0.1*(max(obj.CalcRes.X)-min(obj.CalcRes.X));
		
	end
	
	
	
	function setSuperElevation(obj)
		promptValues = {'SuperElevation Factor'};
		dlgTitle = 'Set SuperElevation Factor';
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{obj.TheAspect},'UniformOutput',false));
		obj.TheAspect=	str2double(gParams{1});
				
				
		obj.updateGUI;
				
	
	
	end
	
	
	
	
	
	
	
	function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
			Menu2Check(3) = findobj(obj.PlotOptions,'Label','Zmap style');
			Menu2Check(2) = findobj(obj.PlotOptions,'Label','Medium Quality Plot');
			Menu2Check(1) = findobj(obj.PlotOptions,'Label','High Quality Plot');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
 				case 'low'
 					%dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotQuality='low';
 					obj.updateGUI;
                		
				case 'med'
					%dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotQuality='med';
					obj.updateGUI;
					
				case 'hi'
					%dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotQuality='hi';
					obj.updateGUI;
					
				case 'chk'
					%try StoredQuality = dataStore.getUserData('PlotQuality');
					%catch dataStore.setUserData('PlotQuality','med');
					%StoredQuality = dataStore.getUserData('PlotQuality');		
					%end
					StoredQuality = obj.PlotQuality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
	end
	
	
	function getFilterlist(obj)
		%meant for reapplying filterlist in Profilemode (needed with old saves)
		
		%ask if your sure
		button = questdlg('Reload current Filterlist from Commander?')
		if strcmp(button,'Yes')
			
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			TheKeys=obj.Commander.getMarkedKey;
			obj.Keys.ShownFilterID=TheKeys.ShownFilterID;
			
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			try
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				obj.ProfileWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				obj.ProfileLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
			catch
				obj.ProfileWidth=[];
				obj.ProfileLine=[];
				disp('Profile available');
			end	
				
		end	
		
		
	end
	
	
	
	function setAlphaMan(obj)
		promptValues = {'Transparency Factor'};
		dlgTitle = 'Set Transparency (set to 1 for none)';
	
		Alpha=obj.AlphaVal;
				
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{Alpha},'UniformOutput',false));
		AlphaMan=	str2double(gParams{1});
		
		
		obj.AlphaVal=AlphaMan;
		
		
				
		obj.updateGUI;
				
	
	
	end
	
	
	function setIsoSurfaces(obj)
		%allows to set the
		ValField=obj.LastPlot{1};
		
		if isfield(obj.IsoSurfers,ValField)
			CustomSwitch=obj.IsoSurfers.(ValField){1};
			TheSurfs=obj.IsoSurfers.(ValField){2};
			CustomSurf=obj.IsoSurfers.(ValField){3};
		else
			errordlg('No data availalbe');
			return;
		end
		
		
		Title = 'Set IsoSurfaces';
		Prompt={'Use Custom Surfaces:', 'CustomSwitch';...
			'Create Median Surfaces:', 'AutoSurf';...
			'Number of Surfaces:','NumSurf';...
			'Min. Value:','minVal';...
			'Max. Value:','maxVal';...
			'Surface Spacing:','Spacing';...
			'Custom Surfaces;','CustomSurf'};
			
							
		defvals=struct(	'CustomSwitch',CustomSwitch,...
				'AutoSurf',false,...
				'NumSurf',10,...
				'minVal',TheSurfs(1),...
				'maxVal',TheSurfs(2),...
				'Spacing',TheSurfs(3),...
				'CustomSurf',num2str(CustomSurf));
			
		%Custom?
		Formats(1,1).type='check';
		%Formats(1.1).size = -1;
		%Formats(1,2).limits = [0 1];
		
		%AutoSurf
		Formats(1,2).type='check';
		
		%NumSurf (for AutoSurf only)
		Formats(1,3).type='edit';
		Formats(1,3).format='float';
		Formats(1,3).limits = [1 9999];
		
		%min
		Formats(2,1).type='edit';
		Formats(2,1).format='float';
		Formats(2,1).limits = [-9999 9999];
		
		%max
		Formats(2,2).type='edit';
		Formats(2,2).format='float';
		Formats(2,2).limits = [-9999 9999];
			
		%min
		Formats(2,3).type='edit';
		Formats(2,3).format='float';
		Formats(2,3).limits = [-9999 9999];
			
		%Custom
		Formats(3,1).type='edit';
		Formats(3,1).format='text';
		Formats(3,1).size=-1;
		Formats(3,1).limits = [0 1];
		Formats(3,2).limits = [0 1];
		Formats(3,2).type='none';
		Formats(3,3).type='none';
		Formats(3,3).limits = [0 1];
			
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
			
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defvals,Options); 
		
		if ~Canceled==1
			if NewParameter.AutoSurf
				%create Median based isosurfaces
				TheMin=nanmin(obj.CalcRes.(ValField)(:));
				TheMax=nanmax(obj.CalcRes.(ValField)(:));
				TheMed=nanmedian(obj.CalcRes.(ValField)(:));
				
				%TheSurfaces
				Surf1=linspace(TheMin,TheMed,NewParameter.NumSurf);
				Surf2=linspace(TheMed,TheMax,NewParameter.NumSurf+1);
				CustomSurf=[Surf1,Surf2(2:end)];
				
				%add them to the data
				obj.IsoSurfers.(ValField){1}=true;
				obj.IsoSurfers.(ValField){3}=CustomSurf;
				
			else
			
				obj.IsoSurfers.(ValField){1}=logical(NewParameter.CustomSwitch);
				obj.IsoSurfers.(ValField){2}=[NewParameter.minVal, NewParameter.maxVal, NewParameter.Spacing];
				obj.IsoSurfers.(ValField){3}=str2num(NewParameter.CustomSurf);
			
			end
			
			obj.updateGUI;
		
		
		end
		
		
	end
	
	
	
	
	function setSlices(obj)
		%allows to set the
		ValField=obj.LastPlot{1};
		
		if isfield(obj.SalamiConfig,ValField)
			CustomXSwitch=obj.SalamiConfig.(ValField){1,1};
			TheXSlice=obj.SalamiConfig.(ValField){1,2};
			CustomXSlice=obj.SalamiConfig.(ValField){1,3};
					
			CustomYSwitch=obj.SalamiConfig.(ValField){2,1};
			TheYSlice=obj.SalamiConfig.(ValField){2,2};
			CustomYSlice=obj.SalamiConfig.(ValField){2,3};
			
			CustomZSwitch=obj.SalamiConfig.(ValField){3,1};
			TheZSlice=obj.SalamiConfig.(ValField){3,2};
			CustomZSlice=obj.SalamiConfig.(ValField){3,3};
		else
			errordlg('No data availalbe');
			return;
		end
		
		if ~isempty(TheXSlice);
			minValX=num2str(TheXSlice(1));
			maxValX=num2str(TheXSlice(2));
			SpacingX=num2str(TheXSlice(3));
		else
			minValX='';
			maxValX='';
			SpacingX='';
		end
		
		
		if ~isempty(TheYSlice);
			minValY=num2str(TheYSlice(1));
			maxValY=num2str(TheYSlice(2));
			SpacingY=num2str(TheYSlice(3));
		else
			minValY='';
			maxValY='';
			SpacingY='';
		end
		
		
		if ~isempty(TheZSlice);
			minValZ=num2str(TheZSlice(1));
			maxValZ=num2str(TheZSlice(2));
			SpacingZ=num2str(TheZSlice(3));
		else
			minValZ='';
			maxValZ='';
			SpacingZ='';
		end
		
		Title = 'Set Slices';
		Prompt={'Use Custom Slices X:', 'CustomXSwitch';...
			'Min. Value X:','minValX';...
			'Max. Value X:','maxValX';...
			'Surface Spacing X:','SpacingX';...
			'Custom Slices X;','CustomXSlice';...
			'Use Custom Slices Y:', 'CustomYSwitch';...
			'Min. Value Y:','minValY';...
			'Max. Value Y:','maxValY';...
			'Surface Spacing Y:','SpacingY';...
			'Custom Slices Y;','CustomYSlice';...
			'Use Custom Slices Z:', 'CustomZSwitch';...
			'Min. Value Z:','minValZ';...
			'Max. Value Z:','maxValZ';...
			'Surface Spacing Z:','SpacingZ';...
			'Custom Slices Z;','CustomZSlice'};
			
							
		defvals=struct(	'CustomXSwitch',CustomXSwitch,...
				'minValX',minValX,...
				'maxValX',maxValX,...
				'SpacingX',SpacingX,...
				'CustomXSlice',num2str(CustomXSlice),...
				'CustomYSwitch',CustomYSwitch,...
				'minValY',minValY,...
				'maxValY',maxValY,...
				'SpacingY',SpacingY,...
				'CustomYSlice',num2str(CustomYSlice),...
				'CustomZSwitch',CustomZSwitch,...
				'minValZ',minValZ,...
				'maxValZ',maxValZ,...
				'SpacingZ',SpacingZ,...
				'CustomZSlice',num2str(CustomZSlice));
			
		%CustomX?
		Formats(1,1).type='check';
		%Formats(1.1).size = -1;
		Formats(1,2).limits = [0 1];
		Formats(1,2).type='none';
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%min X
		Formats(2,1).type='edit';
		Formats(2,1).format='text';
		Formats(2,1).limits = [0 1];
		
		%max X
		Formats(2,2).type='edit';
		Formats(2,2).format='text';
		Formats(2,2).limits = [0 1];
			
		%Spacing X
		Formats(2,3).type='edit';
		Formats(2,3).format='text';
		Formats(2,3).limits = [0 1];
			
		%Custom X
		Formats(3,1).type='edit';
		Formats(3,1).format='text';
		Formats(3,1).size=-1;
		Formats(3,1).limits = [0 1];
		Formats(3,2).limits = [0 1];
		Formats(3,2).type='none';
		Formats(3,3).type='none';
		Formats(3,3).limits = [0 1];
		
		
		
		%CustomY?
		Formats(4,1).type='check';
		%Formats(4.1).size = -1;
		Formats(4,2).limits = [0 1];
		Formats(4,2).type='none';
		Formats(4,3).type='none';
		Formats(4,3).limits = [0 1];
		
		%min Y
		Formats(5,1).type='edit';
		Formats(5,1).format='text';
		Formats(5,1).limits = [0 1];
		
		%max y
		Formats(5,2).type='edit';
		Formats(5,2).format='text';
		Formats(5,2).limits = [0 1];
			
		%Spacing Y
		Formats(5,3).type='edit';
		Formats(5,3).format='text';
		Formats(5,3).limits = [0 1];
			
		%Custom Y
		Formats(6,1).type='edit';
		Formats(6,1).format='text';
		Formats(6,1).size=-1;
		Formats(6,1).limits = [0 1];
		Formats(6,2).limits = [0 1];
		Formats(6,2).type='none';
		Formats(6,3).type='none';
		Formats(6,3).limits = [0 1];
		
		%CustomY?
		Formats(7,1).type='check';
		%Formats(7.1).size = -1;
		Formats(7,2).limits = [0 1];
		Formats(7,2).type='none';
		Formats(7,3).type='none';
		Formats(7,3).limits = [0 1];
		
		%min Y
		Formats(8,1).type='edit';
		Formats(8,1).format='text';
		Formats(8,1).limits = [0 1];
		
		%max y
		Formats(8,2).type='edit';
		Formats(8,2).format='text';
		Formats(8,2).limits = [0 1];
			
		%Spacing Y
		Formats(8,3).type='edit';
		Formats(8,3).format='text';
		Formats(8,3).limits = [0 1];
			
		%Custom X
		Formats(9,1).type='edit';
		Formats(9,1).format='text';
		Formats(9,1).size=-1;
		Formats(9,1).limits = [0 1];
		Formats(9,2).limits = [0 1];
		Formats(9,2).type='none';
		Formats(9,3).type='none';
		Formats(9,3).limits = [0 1];
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
			
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defvals,Options); 
		
		if ~Canceled==1
			
			if ~isempty(NewParameter.minValX)&~isempty(NewParameter.maxValX)&~isempty(NewParameter.SpacingX)
				XSlice=[str2num(NewParameter.minValX), str2num(NewParameter.maxValX), str2num(NewParameter.SpacingX)];
			else
				XSlice=[];
			end
			
			
			if ~isempty(NewParameter.minValY)&~isempty(NewParameter.maxValY)&~isempty(NewParameter.SpacingY)
				YSlice=[str2num(NewParameter.minValY), str2num(NewParameter.maxValY), str2num(NewParameter.SpacingY)];
			else
				YSlice=[];
			end
			
			
			if ~isempty(NewParameter.minValZ)&~isempty(NewParameter.maxValZ)&~isempty(NewParameter.SpacingZ)
				ZSlice=[str2num(NewParameter.minValZ), str2num(NewParameter.maxValZ), str2num(NewParameter.SpacingZ)];
			else
				ZSlice=[];
			end
			
			
		
			obj.SalamiConfig.(ValField){1,1}=logical(NewParameter.CustomXSwitch);
			obj.SalamiConfig.(ValField){1,2}=XSlice;
			obj.SalamiConfig.(ValField){1,3}=str2num(NewParameter.CustomXSlice);
				
			obj.SalamiConfig.(ValField){2,1}=logical(NewParameter.CustomYSwitch);
			obj.SalamiConfig.(ValField){2,2}=YSlice;
			obj.SalamiConfig.(ValField){2,3}=str2num(NewParameter.CustomYSlice);
			
			obj.SalamiConfig.(ValField){3,1}=logical(NewParameter.CustomZSwitch);
			obj.SalamiConfig.(ValField){3,2}=ZSlice;
			obj.SalamiConfig.(ValField){3,3}=str2num(NewParameter.CustomZSlice);
		
						
			obj.updateGUI;
		
		
		end
		
		
	end
	
	
	function CalcRecurrence(obj,DoWhat)
		%This function calculates a recurrence map and turns on the plot menus for it
		
		import mapseis.projector.*;
		
		switch DoWhat
		
			case 'Calc'
				%go for a whole calculation
				
				%Get data
				RecMag=obj.CalcParameter.RecMag;
				Sel_Radius=obj.CalcParameter.Sel_Radius;
				b_value=obj.CalcRes.b_value;
				a_value=obj.CalcRes.a_value;
				Res=obj.CalcRes.Resolution;
				
				%input magnitude
				Title = 'Magnitude of projected mainshock?';
				Prompt={'Magnitude: ', 'RecMag'};
					
									
				defvals=struct(	'RecMag',RecMag);
					
				%RecMag
				Formats(1,1).type='edit';
				Formats(1,1).format='float';
				Formats(1,1).limits = [-99 99];
				
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
				
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defvals,Options); 
				
							
				if Canceled==1
					TRCalc=false;
				else
					TRCalc=true;
					obj.CalcParameter.RecMag=NewParameter.RecMag;
					RecMag=NewParameter.RecMag;
				end
				
				if TRCalc		
					%Get time data
					TheYears=getDecYear(obj.Datastore,obj.SendedEvents);
					minYear=min(TheYears);
					maxYear=max(TheYears);
					
					%calc recurrence map
					ReTime=(maxYear - minYear)./(10.^(a_value-RecMag*b_value));
								  
					
					%calc 1/Tr
					if ~obj.ThirdDimension
						%2D
						ReTimeArea=1./ReTime./(2*pi*Res.*Res);
						ReTimeCArea=1./ReTime./(2*pi*Sel_Radius.*Sel_Radius);
					else
						%3D
						ReTimeArea=1./ReTime./(4/3*pi*Res.*Res.*Res);
						ReTimeCArea=1./ReTime./(4/3*pi*Sel_Radius.*Sel_Radius.*Sel_Radius);
					end
					
					%calc percentage map
					Nanner = isnan(b_value); 
					bvalTemp = b_value; 
					bvalTemp(Nanner) = []; 
					
					len = length(bvalTemp); 
					perc = []; 
					miniB=min(min(bvalTemp));
					
					for i =miniB:miniB/10:miniB*10;
						Nanner = bvalTemp <= i; 
						perc = [ perc ; length(bvalTemp(Nanner))/len*100 i]; 
					end
				
					
					ReTimePerc=perc;
					
					%Write it into the results structure
					obj.CalcRes.RecTime=ReTime;
					obj.CalcRes.RecTimeArVol=ReTimeArea;
					obj.CalcRes.RecTimeArVolConst=ReTimeCArea;
					obj.CalcRes.RecTimePerc=ReTimePerc;
					
					%Log version
					ReTimeAreaLog=ReTimeArea;
					notZero=ReTimeAreaLog~=0;
					ReTimeAreaLog(notZero)=log10(ReTimeAreaLog(notZero));
					ReTimeCAreaLog=ReTimeCArea;
					notZero=ReTimeCAreaLog~=0;
					ReTimeCAreaLog(notZero)=log10(ReTimeCAreaLog(notZero));
					
					obj.CalcRes.RecTimeArVolLog=ReTimeAreaLog;
					obj.CalcRes.RecTimeArVolConstLog=ReTimeCAreaLog;
					
					
					%set colorbars right
					minVal=min(min(ReTime(~isnan(ReTime))));
					if minVal~=0;
						obj.ColLimitSwitch.RecTime=true;
						obj.ColLimit.RecTime=[minVal 10*minVal];	
					end	
					
					%set colorbars right
					minVal=min(min(ReTimeArea(~isnan(ReTimeArea))));
					if minVal~=0;
						obj.ColLimitSwitch.RecTimeArVol=true;
						obj.ColLimit.RecTimeArVol=[minVal 10*minVal];	
					end	
					
					%set colorbars right
					minVal=min(min(ReTimeCArea(~isnan(ReTimeCArea))));
					if minVal~=0;
						obj.ColLimitSwitch.RecTimeArVolConst=true;
						obj.ColLimit.RecTimeArVolConst=[minVal 10*minVal];	
					end	
					
					obj.updateGUI;
					msgbox('Finished');
					
				end
				
				%update menus (check this in any case)
				if isfield(obj.CalcRes,'RecTime')
					set(obj.RecSubMenu,'Visible','on');
				else
					set(obj.RecSubMenu,'Visible','off');
				end
				
				
			case 'MenuCheck'
				%See if some results already exists and set menu to 
				%visible if so
				if isfield(obj.CalcRes,'RecTime')
					set(obj.RecSubMenu,'Visible','on');
				else
					set(obj.RecSubMenu,'Visible','off');
				end
			
		end	
		
		
	
	
	end
	
	
	
	
	%functions for the FMD window and picker
	%=======================================
	
	function setFMDRadius(obj)
		promptValues = {'FMD Selection Radius'};
		dlgTitle = 'Set FMD Selection Radius';
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{deg2km(obj.FMDRad)},'UniformOutput',false));
		obj.FMDRad=	km2deg(str2double(gParams{1}));
				
	
	
	end
	
	
	
	function renewPoly(obj,plotAxis)
		import mapseis.plot.*;
		import mapseis.util.emptier;
		
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		
		AnyEmpty=emptier(obj.FMDFilter);
		
		
		
		if ~all(AnyEmpty)
		
			%get the used filters
			UsedSlots=find(AnyEmpty==0);
			
			for i=UsedSlots'
		
				filterRegion = getRegion(obj.FMDFilter{i});
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if isobject(pRegion)
					rBdry = pRegion.getBoundary();	
				else 
					rBdry = pRegion;
				end
				
						
				switch RegRange
					case {'in','out'}
						selection=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
						selection.addNewPositionCallback(@(p) ...
								FMDfilterupdater(obj,p,obj.FMDFilter{i}));
						selection.setColor(obj.FMDColors{i});		
						%set the menu point depth slice to 'off'
						
						%add menus to the object
						%SetImMenu(obj,selection,regionFilter,[]);
							
						obj.FMDPoly{i,1}=selection;
						
						obj.AddPolyMenu(selection,obj.FMDFilter{i},i,[]);
						
						
					case 'circle'
						rBdry(3)=obj.FMDFilter{i}.Radius;
						
						
						
						%draw circle
						hold on
						handle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
						selection = impoint(plotAxis,rBdry(1:2));
						hold off
						
						%selection.setFixedAspectRatioMode(1)
						selection.addNewPositionCallback(@(p) ...
								FMDfilterupdater(obj,[p(1),p(2),rBdry(3)],...
								obj.FMDFilter));
						
						selection.setColor(obj.FMDColors{i});
						set(handle(1),'Color',obj.FMDColors{i});
						
						obj.FMDPoly{i,1}=selection;
						obj.FMDPoly{i,2}=handle;
						obj.AddPolyMenu(selection,obj.FMDFilter{i},i,handle);
						
						%add menus to the object
						%SetImMenu(obj,selection,obj.FMDFilter,handle);
					
						
					case 'closeIt'
						%should never happen, because it should be already killed
						obj.FMDSuicide;	
						
						
				end
				
				hold on;
				
				
                	end
		end
	
	
	end
	
	
	
	
	function SetFMDRegion(obj,WhichOne)
		import mapseis.plot.*;
         	import mapseis.filter.*;
		import mapseis.util.*;
		import mapseis.region.*;
		import mapseis.projector.*;
		import mapseis.util.gui.SetImMenu;

		if strcmp(WhichOne,'closeIt')
         		obj.FMDSuicide;	
		else
			%"find" next free slot
			AnyEmpty=emptier(obj.FMDFilter);
			obj.FMDNext=mod(obj.FMDNext+1,obj.FMDMax)+1;
			
			if ~isempty(obj.FMDFilter{obj.FMDNext})&any(AnyEmpty)
				disp('Check for free slots')
				%slot is not empty and there is an alternative
				freeOnes=find(AnyEmpty==1)
				obj.FMDNext=freeOnes(1);
				
				
			end
			
			
		
			%Check if FMD filter is existing and if it is the right one
			if isempty(obj.FMDFilter{obj.FMDNext})
				if obj.ProfileSwitch&~obj.ThirdDimension
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter{obj.FMDNext}=ProfileRegionFilter('all',...
						profLine,ProfWidth,obj.SendedEvents,[],'ProfileRegion');
					
					
				elseif ~obj.ProfileSwitch&~obj.ThirdDimension
					obj.FMDFilter{obj.FMDNext}=RegionFilter('all',[],'Region');
					
					
					
				else
					%not supported at the moment
				
				end
			else
			
				%check if the filter is right for the purpose
				if obj.ProfileSwitch&~obj.FMDFilter{obj.FMDNext}.isProfileFilter
					%wrong filter, change
					clear obj.FMDFilter{obj.FMDNext};
					obj.FMDFilter{obj.FMDNext}=[];
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter{obj.FMDNext}=ProfileRegionFilter('all',...
						profLine,ProfWidth,obj.SendedEvents,[],'ProfileRegion');
						
					
				elseif obj.ProfileSwitch&obj.FMDFilter{obj.FMDNext}.isProfileFilter
					%right filter but may needs correction
					regionFilter = obj.Filterlist.getByName('Region');
					filterRegion = getDepthRegion(regionFilter);
					pRegion = filterRegion{2};
					RegRange=filterRegion{1};
					ProfWidth=filterRegion{4};
					rBdry = pRegion.getBoundary();
					profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
	
					obj.FMDFilter{obj.FMDNext}.setProfile(profLine,ProfWidth);
					obj.FMDFilter{obj.FMDNext}.setMasterSelection(obj.SendedEvents);
					
					
				elseif ~obj.ProfileSwitch&obj.FMDFilter{obj.FMDNext}.isProfileFilter	
					%also wrong filter
					clear obj.FMDFilter{obj.FMDNext};
					obj.FMDFilter{obj.FMDNext}=[];
					obj.FMDFilter{obj.FMDNext}=RegionFilter('all',[],'Region');
				end	
			end
			
			
			
		 
			dataStore = obj.Datastore;
			
			
			
			
		
			plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			if isempty(plotAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.ResultGUI, 'Children');
					
				%find axis and return axis
				plotAxis = findobj(gracomp,'Type','axes','Tag','');
				set(plotAxis,'Tag','MainResultAxis');
			end	
			
			
			
			%Set color
			CurCol=obj.FMDColors{obj.FMDNext};
			
			switch WhichOne
				case {'in','out'}
				    	axes(plotAxis);
				    	obj.FMDPoly{obj.FMDNext,1}=impoly;
				    	newRegion = Region(obj.FMDPoly{obj.FMDNext,1}.getPosition);
				   
				    
				    	obj.FMDPoly{obj.FMDNext,1}.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,p,obj.FMDFilter{obj.FMDNext}));
                		    	
                			obj.FMDPoly{obj.FMDNext,1}.setColor(CurCol);
                			obj.FMDFilter{obj.FMDNext}.setRegion(WhichOne,newRegion);
				    
				    
				
				
			
				case {'circle'}
					%creates a circle for the selection
					
					axes(plotAxis);
					circ=Ginput(1);%polyg.getPosition;
					
					%correct the position to the lower left corner (imellipse)
					
					if ~obj.ProfileSwitch
						obj.FMDFilter{obj.FMDNext}.setRadius(obj.FMDRad);
					else
						obj.FMDFilter{obj.FMDNext}.setRadius(deg2km(obj.FMDRad));
					end
					
					newRegion = [circ(1),circ(2)];
					
										
					hold on
					handle=drawCircle(plotAxis,true,circ(1:2),obj.FMDRad);
            				selection = impoint(plotAxis,circ(1:2));
					hold off
					
            				selection.addNewPositionCallback(@(p) ...
                					FMDfilterupdater(obj,[circ(1),circ(2),obj.FMDRad],...
                					obj.FMDFilter{obj.FMDNext}));
            				
                			selection.setColor(CurCol);
                			
                			obj.FMDPoly{obj.FMDNext,1}=selection;
                			obj.FMDPoly{obj.FMDNext,2}=handle;
            				
					obj.FMDFilter{obj.FMDNext}.setRegion(WhichOne,newRegion);
				
			    end
			    
			% Run the filter
			obj.FMDFilter{obj.FMDNext}.execute(obj.Datastore);

			obj.CreateFMDGUI;
			obj.UpdateFMD;
			
         	end

	end
	
	
	
	
	function CreateFMDGUI(obj)
		%creates the gui if needed or updates it
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		
		if ~isempty(obj.FMDWindow)
			%if clf would be called with empty obj.FMDWindow
			%it would deform to clf([])=clf() and clear every figure
			%in function focus.
			try
				%window exists and is fine
				clf(obj.FMDWindow);
			end
			
			if ~ishandle(obj.FMDWindow)
				obj.FMDWindow=[];
			end	
		end
		
		 obj.FMDWindow = onePanelLayout_mk2(obj.FMDWindow,'FMD-sidekick',...
		   {'TickDir','out','color','r','FontWeight','bold','FontSize',12});
		   set(obj.FMDWindow,'Position',[ 400 200 800 600 ],'visible','on');

		
		FMDAxis=gca;
		set(FMDAxis,'Tag','FMDResultAxis','Color','w');
		   
		%build button
		updateButton = uicontrol(obj.FMDWindow, 'Units', 'Normalized',...
                		'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update FMD',...
                		'Callback',@(s,e) obj.UpdateFMD);
		set(obj.FMDWindow,'DeleteFcn',@(s,e) obj.FMDSuicide);
                		
                		
	end
	
	
	
	
	function UpdateFMD(obj)
		%say it all updates the gui
		import mapseis.plot.*;
		import mapseis.util.emptier;
		
		
		%Now check which FMDs have to be plotted
		AnyEmpty=emptier(obj.FMDFilter);
		
		if ~all(AnyEmpty);
			
			UsedSlots=find(AnyEmpty==0);
						
		
			FMDAxis = findobj(obj.FMDWindow,'Tag','FMDResultAxis');
			if isempty(FMDAxis)
				%try retag
				%get all childrens of the main gui
				gracomp = get(obj.FMDWindow, 'Children');
					
				%find axis and return axis
				FMDAxis = findobj(gracomp,'Type','axes','Tag','');
				set(FMDAxis,'Tag','FMDResultAxis','Color','w');
			end	
			
			
			hold(FMDAxis,'off')
			for i=UsedSlots'
				disp(['i=',num2str(i)])
				%get selection of events
				obj.FMDFilter{i}.execute(obj.Datastore);
				selected=obj.FMDFilter{i}.getSelected&obj.SendedEvents;
				
				set(FMDAxis,'pos',[0.1300    0.2500    0.7750    0.65]);
	
							
				%create plot config
				Params =struct('Mc_method',obj.CalcParameter.Mc_Method,...
						'NoCurve',false,...
						'Use_bootstrap',obj.CalcParameter.BootSwitch,...
						'Nr_bootstraps',obj.CalcParameter.BootSamples,...		
						'Mc_correction',obj.CalcParameter.McCorr);
				
				FMDConfig= struct('PlotType','FMDex',...
						'Data',obj.Datastore,...
						'BinSize',0.1,...
						'b_line',true,...
						'CustomCalcParameter',Params,...
						'Selected',selected,...
						'Mc_Method',1,...
						'Write_Text',false,...
						'Value_Output','all',...
						'LineStylePreset','Fatline',...
						'X_Axis_Label','Magnitude ',...
						'Y_Axis_Label','cum. Number of Events ',...
						'Colors',obj.FMDColors{i},...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'LegendText','FMD');
				
						
				%plot da shit
				[handle DataOut] = PlotFMD_expanded(FMDAxis,FMDConfig);
				hold on;
				
				txt1=text(0, -.11,DataOut.Text{1},'FontSize',10,'Color','k','Units','normalized');
				set(txt1,'FontWeight','normal')
				text(0, -.08,DataOut.Text{2},'FontSize',10,'Color','k','Units','normalized');
				text(0, -.14,DataOut.Text{3},'FontSize',10,'Color','k','Units','normalized');
				selected=[];
				%obj.updateGUI;
				 
				%focus back to the FMD
				axes(FMDAxis);
				 
			end	 
			
			%only needed once
			obj.updateGUI;
				 
			%focus back to the FMD
			axes(FMDAxis);
			%somehow the axis is somewhere set to linear until I find it I use this
			set(FMDAxis,'YScale','log');
			hold off;
		end
		
	
	end
	
	
	function FMDSuicide(obj)
		%closes the GUI and deletes the filter
		
		for i=1:obj.FMDMax
			try
				obj.FMDPoly{i,1}.delete;
			end
		
		
			try
				%will be deleted in next update
				set(obj.FMDPoly{i,2},'visible','off')
			end
		end
		
		try
			close(obj.FMDWindow);
		end
		
		
		try	
			clear obj.FMDWindow;
		end
		
		try	
			clear obj.FMDFilter;
			obj.FMDFilter=cell(obj.FMDMax,1);;
		catch
			%at least empty the filterslot 
			obj.FMDFilter=cell(obj.FMDMax,1);;
		end
		
		obj.FMDNext=0;
		
	end
	
	
	 
        function FMDDelete(obj,PolyID)
        	%Similar to FMDSuicide but does only delete on of the FMDs
        	import mapseis.util.emptier;
        	
        	
        	if nargin<2
        		obj.FMDSuicide;
        	else
        	
        	
			try
				obj.FMDPoly{PolyID,1}.delete;
			end
			
			
			try
				%will be deleted in next update
				set(obj.FMDPoly{PolyID,2},'visible','off')
			end
			
			
			try
				%at least empty the filterslot 
				obj.FMDFilter{PolyID}=[];
			end
			
			obj.FMDNext=PolyID-1;
			
			%Close window if nothing left
			if all(emptier(obj.FMDFilter))
				disp('No FMD left')
				try
					close(obj.FMDWindow);
				end
				
				
				try	
					clear obj.FMDWindow;
				end
				
				obj.FMDNext=0;
			end
			
			obj.UpdateFMD;
        	
		end
          
        end
          
	
	
	
	function FMDfilterupdater(obj,pos,regionFilter)
		import mapseis.region.*;
		import mapseis.Filter.*;
		import mapseis.util.*;
		
		%special version of the filter updater	 
	
	
		%Uses the input from imroi type objects and updates the filter 
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange = filterRegion{1};
	
		switch RegRange
						
			case {'in','out'}
				newRegion = Region(pos);
				regionFilter.setRegion(RegRange,newRegion);
	
				
			case 'circle'		
						
				if ~obj.ProfileSwitch
					regionFilter.setRadius(obj.FMDRad);
				else
					regionFilter.setRadius(deg2km(obj.FMDRad));
				end
						
						
				%dataStore.setUserData('gridPars',newgrid);
							
				%update filter
				newRegion = [pos(1),pos(2)];
				regionFilter.setRegion(RegRange,newRegion);
							
			case 'closeIt'
				obj.FMDSuicide;
		  end	
          end
	
          
          
          function AddPolyMenu(obj,imobj,RegionFilter,FMD_ID,addhandle)
          	%Sets the menu of the impoly object and the filter.
          	%only on entry has to be added, "Delete FMD"
          	import mapseis.gui.*;
	
		if nargin<5
			addhandle=[];
		end
		
		regiontype=RegionFilter.RangeSpec;
		
		switch regiontype
			case {'in','out'}
				%find the parts and separate them
				childrens=get(imobj,'children');
				thepoints=findall(childrens,'type','hggroup');
				thelines=findall(childrens,'type','line');
				thepatch=findall(childrens,'type','patch');
				
				%get menu
				oldmenu=get(thepoints(1),'uicontextmenu');
				
	
				
				%create new entry
				Ppoint1=uimenu(oldmenu, 'Label', 'Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
									
				
				%add new menu to points and lines
				for i=1:numel(thepoints)
					set(thepoints(i),'uicontextmenu',oldmenu);
				end
				
				for i=1:numel(thelines)
					set(thelines(i),'uicontextmenu',oldmenu);
				end
				
				for i=1:numel(thepatch)
					set(thepatch(i),'uicontextmenu',oldmenu);
				end
				
				
			case 'line'
				%find the parts and separate them
				childrens=get(imobj,'children');
				thepoints=findall(childrens,'type','line');
				thelines=addhandle;
				
				%get menu
				oldmenu=get(thepoints(1),'uicontextmenu');
				
				%additional menu for the box
				boxmenu=uicontextmenu;
				
				%create new menus
				%oldmenu
				Ppoint1=uimenu(oldmenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
				
		
				
							
				%box menu		
				Lpoint1=uimenu(boxmenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
					
				
						
				%add new menu to points and lines
				for i=1:numel(thepoints)
					set(thepoints(i),'uicontextmenu',oldmenu);
				end
				
				for i=1:numel(thelines)
					set(thelines(i),'uicontextmenu',boxmenu);
				end
				
	
			
			case 'circle'
				%find the parts and separate them
				childrens=get(imobj,'children');
				thepoints=findall(childrens,'type','line');
				thelines=addhandle;
		
				%get menu
				oldmenu=get(imobj,'uicontextmenu');
				
				%additional menu for the box
				circlemenu=uicontextmenu;
				
				%create new menus
				%oldmenu
				Ppoint1=uimenu(oldmenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
				
							
				%Circle Menu
				Lpoint1=uimenu(circlemenu,'Label','Delete FMD',...
						'Callback', @(s,e) obj.FMDDelete(FMD_ID));
				
						
				%add new menu to points and lines
				for i=1:numel(thepoints)
					set(thepoints(i),'uicontextmenu',oldmenu);
				end
				
				%the point itself
				set(imobj,'uicontextmenu',oldmenu);
				
				for i=1:numel(thelines)
					set(thelines(i),'uicontextmenu',circlemenu);
				end
				
				
			
		end
          	
          	
          
          end
          
          
          function exportResult(obj,FileType,Filename)
          	%saves the currently plotted result into a mat or ascii file
          	%Comment: the fileparts command would not always be necessary, but 
          	%makes the code more consistent (case with multiple files) and it
          	%is time critical)
          	
          	if nargin<3
          		Filename=[];
          	end
          	
          	if obj.ThirdDimension
			%Just for making the function not to messy
			obj.exportResult3D(FileType,Filename);
			return
		end
		
          	%get data
          	if ~isempty(obj.LastPlot)&~strcmp(FileType,'full_result')
          		xdir=obj.CalcRes.X;
			ydir=obj.CalcRes.Y;
          		ResValue=obj.CalcRes.(obj.LastPlot{1})';
		
          	elseif isempty(obj.LastPlot)
			return
		end
          	
          	switch FileType
          		case 'matlab'
          			if isempty(Filename)
          				%get filename
          				[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','ResValue');
				
          		case 'ascii'
          			if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','ResValue','-ascii');
				
			case 'ascii_trio'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,['Xdir_',fName]),'xdir','-ascii');
				save(fullfile(pName,['Ydir_',fName]),'ydir','-ascii');
				save(fullfile(pName,['Value_',fName]),'ResValue','-ascii');
          	
			case 'full_result'
				FullCalc=obj.CalcRes;
				if isempty(Filename)
					[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
					
				if ~isempty(FullCalc)
					save(fullfile(pName,fName),'FullCalc');
				end
          	end
          
          end
          
          
           
          function exportResult3D(obj,FileType,Filename) 
          	%saves the currently plotted result into a mat or ascii file (3D version)
          	%Comment: the fileparts command would not always be necessary, but 
          	%makes the code more consistent (case with multiple files) and it
          	%is time critical)
          	
		if nargin<3
          		Filename=[];
          	end
          	
          	%get data
          	if ~isempty(obj.LastPlot)&~strcmp(FileType,'full_result')
          		xdir=obj.CalcRes.X;
			ydir=obj.CalcRes.Y;
			ydir=obj.CalcRes.Z;
          		ResValue=obj.CalcRes.(obj.LastPlot{1})';
		elseif isempty(obj.LastPlot)
			return
		end
          	
          	switch FileType
          		case 'matlab'
          			if isempty(Filename)
          				%get filename
          				[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','zdir','ResValue');
				
          		case 'ascii'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','zdir','ResValue','-ascii');
				
			case 'ascii_trio'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,['Xdir_',fName]),'xdir','-ascii');
				save(fullfile(pName,['Ydir_',fName]),'ydir','-ascii');
				save(fullfile(pName,['Zdir_',fName]),'zdir','-ascii');
				save(fullfile(pName,['Value_',fName]),'ResValue','-ascii');
				
			case 'full_result'
				FullCalc=obj.CalcRes;
				
				if isempty(Filename)
					[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				if ~isempty(FullCalc)
					save(fullfile(pName,fName),'FullCalc');
				end	
          	
          	end
          
          end
	
	
          
          
	
end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	
	%THE FUNCTIONS UNDERNEATH ARE NOT ALWAYS DIRECTLY USEABLE, AS THEY OFTEN NEED DIRECT ACCESS
	%TO THE VARIABLE, SO JUST COPY IF NEEDED.
	function TheEvalString = UnpackStruct(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		TheEvalString=[];
		
		for i=1:numel(thefields)
			
			TheEvalString=[TheEvalString; thefields{i},'=' num2str(TheStruct.(thefields{i})),';'];
		end
		 %can be done easier directly in the function which should be calling this, it is more sort of 
		 %a sketch
		
		
	
	end
	
	
	function CellData = UnpackStruct2Cell(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		
		
		for i=1:numel(thefields)
			
			CellData{i,1}=thefields{i}
			CellData{i,2}=TheStruct.(thefields{i});
		end
		
		%assignin can be used to set the variable on the string.
	end
	
	function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itselectorf
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end
	
	
	function TheStruct = PackAllVariable()
		%The opposite of UnPack, it takes every variable in the current Workspace an packs it 
		%into a structure
		
		varilist=who;
		
		for i=1:numel(varilist)
			TheStruct.(varilist{i})=eval(varilist{i});
		
		end
	
	
	end
	
	

	
	%---------------
end
	
	

end

classdef StressFieldCalcWrapper < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		CalcRes
		CalcParameter
		ProfileSwitch
		ErrorDialog
		ParallelMode
		StartCalc
		Loaded
		CalcName
		Region
		AlphaMap
		ResultGUI
		PlotOptions
		BorderToogle
		CoastToogle
		EQToogle
		PlotQuality
		LastPlot
	end

	events
		CalcDone

	end


	methods
	
	
	function obj = StressFieldCalcWrapper(ListProxy,Commander,GUISwitch,ProfileSwitch,CalcParameter)
		import mapseis.datastore.*;
		
		%This wrapper allows to use either the Micheals (1984) ot the 
		%SATSI (Hardebeck and Michael 2005) stress tensor inversion 
		%algorithmen with MapSeis.
		
		
		
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		
		if isempty(Commander)
			obj.Commander=[];
			%go into manul mode, the object will be created but everything
			%has to be done manually (useful for scripting)
		else
		
			obj.Commander = Commander; 
			obj.ParallelMode=obj.Commander.ParallelMode;
			obj.Region=[];
			obj.AlphaMap=[];
			obj.ProfileSwitch=ProfileSwitch;
			obj.Loaded=false;
			obj.ResultGUI=[];
			obj.PlotOptions=[];
			obj.BorderToogle=true;
			obj.CoastToogle=true;
			obj.EQToogle=false;
			obj.PlotQuality = 'low';
			obj.LastPlot=[];
			
			%get the current datastore and filterlist and build zmap catalog
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			
			EveryThingGood=true;
			
			
			%check if the focal mechanism are available
			try
				obj.Datastore.getFields({'focal_strike','focal_dip','focal_rake'});
			catch	
				EveryThingGood=false;
				errordlg('No focal mechanism available in this catalog');
				obj.ErrorDialog='No focal mechanism available in this catalog';
			end
			
			if ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the regionfilter');
					obj.ErrorDialog='No profile selected in the regionfilter';
				 end
			end
			
			if EveryThingGood
				
				if ~isempty(CalcParameter)
					%Parameters are include, no default parameter will
					%be set
					obj.CalcParameter=CalcParameter;
				else
					obj.InitVariables
				end	
				
				if GUISwitch
					
					%let the user modify the parameters
					obj.openCalcParamWindow;
					
					if obj.StartCalc
						
						if ~obj.Loaded
							%Calc
							obj.doTheCalcThing;
						end
						
						%Build resultGUI
						obj.BuildResultGUI
						
						%plot results
						obj.plotResult('stress')
					end	
					
					
				end
				

			
			end
		end
		
	end

	
	
	
	function InitVariables(obj)
		%Now Zmap stuff needed anymore
		%some basic parameters, I will add addition parameters later 
		%if needed e.g. bootstrap option
		obj.CalcParameter = struct(	'MinNumber',0,...
						'dx',1,...
						'dy',1,...
						'dz',1,...
						'NumEvents',50,...
						'Sel_Radius',5,...
						'Selection_Method',1,...
						'SmoothMode',0,...
						'SmoothFactor',0.2,...
						'Calc_Method',1,...
						'GridMethod','regular',...
						'DampSatsi',1);
						
		
		%Calc_Method can be 'michaels' or 'satsi'
		%GridMethod cannot be changed in the GUI for now until I improved
		%the selfrefining grid algorithmen. (options: 'regular' and 'selfRef')
		
		
		
	
	end
	
	
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		
		
		%check if old calculations are avialable
		try
			PreviousCalc=obj.Datastore.getUserData('StressInversion-v1');
		catch
			PreviousCalc=[];
		end

		if ~obj.ProfileSwitch;
		
			Title = 'Stress Tensor Inversion';
			Prompt={'Mc Method:', 'Calc_Method';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smoothing Factor','SmoothFactor';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (deg):','dx';...
				'Grid spacing y (deg):','dy';...
				'Mininum Number:','MinNumber';...
				'Dampting (SATSI):','DampSatsi'};
			
			MethodList =  {'Michael Method (1984)'; ...
				'SATSI (Hardebeck & Michael 2005)'}; 
				
				
			%default values
			defval =struct('Calc_Method',obj.CalcParameter.Calc_Method,...
				'SmoothMode',obj.CalcParameter.SmoothMode,...
				'SmoothFactor',obj.CalcParameter.SmoothFactor,...
				'Selection_Method',obj.CalcParameter.Selection_Method,...
				'NumEvents',obj.CalcParameter.NumEvents,...
				'Sel_Radius',obj.CalcParameter.Sel_Radius,...
				'dx',obj.CalcParameter.dx,...
				'dy',obj.CalcParameter.dy,...
				'MinNumber',obj.CalcParameter.MinNumber,...
				'DampSatsi',obj.CalcParameter.DampSatsi);	
				
		else
			Title = 'Stress Tensor Inversion (Depth Profile)';
			Prompt={'Mc Method:', 'Calc_Method';...
				'Selection Method:','Selection_Method';...
				'Smoothing?','SmoothMode';...
				'Smoothing Factor','SmoothFactor';...
				'Number of Events:','NumEvents';...
				'Selection Radius (km):','Sel_Radius';...
				'Grid spacing x (km):','dx';...
				'Grid spacing z (deg):','dz';...
				'Mininum Number:','MinNumber';...
				'Dampting (SATSI):','DampSatsi'};
			
			MethodList =  {'Michael Method (1984)'; ...
				'SATSI (Hardebeck & Michael 2005)'}; 
				
			%default values
			defval =struct('Calc_Method',obj.CalcParameter.Calc_Method,...
				'SmoothMode',obj.CalcParameter.SmoothMode,...
				'SmoothFactor',obj.CalcParameter.SmoothFactor,...
				'Selection_Method',obj.CalcParameter.Selection_Method,...
				'NumEvents',obj.CalcParameter.NumEvents,...
				'Sel_Radius',obj.CalcParameter.Sel_Radius,...
				'dx',obj.CalcParameter.dx,...
				'dz',obj.CalcParameter.dz,...
				'MinNumber',obj.CalcParameter.MinNumber,...
				'DampSatsi',obj.CalcParameter.DampSatsi);	
		end		
		
		
		
		%Mc Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=MethodList;
		Formats(1,2).type='none';
		Formats(1,1).size = [-1 0];
		Formats(1,2).limits = [0 1];
		
		%Selection Method
		Formats(2,1).type='list';
		Formats(2,1).style='togglebutton';
		Formats(2,1).items={'Number of Events','Constant Radius','Both'}
		Formats(2,1).size = [-1 0];
		Formats(2,2).limits = [0 1];
		Formats(2,2).type='none';
		
		%Smoothing
		Formats(3,1).type='check';
		
		%Smoothing Parameter
		Formats(3,2).type='edit';
		Formats(3,2).format='float';
		Formats(3,2).limits = [0 9999];
		
		%NumberEvents
		Formats(4,1).type='edit';
		Formats(4,1).format='integer';
		Formats(4,1).limits = [0 9999];
	
		%Radius
		Formats(4,2).type='edit';
		Formats(4,2).format='float';
		Formats(4,2).limits = [0 9999];
		
		%Grid space x
		Formats(5,1).type='edit';
		Formats(5,1).format='float';
		Formats(5,1).limits = [0 999];
		
		%Grid space y or z
		Formats(5,2).type='edit';
		Formats(5,2).format='float';
		Formats(5,2).limits = [0 999];
		
		%Min Number
		Formats(6,1).type='edit';
		Formats(6,1).format='float';
		Formats(6,1).limits = [0 9999];
		Formats(6,1).size = [-1 0];
		Formats(6,2).limits = [0 1];
		Formats(6,2).type='none';
		
		%Min Number
		Formats(7,1).type='edit';
		Formats(7,1).format='float';
		Formats(7,1).limits = [0 9999];
		Formats(7,1).size = [-1 0];
		Formats(7,2).limits = [0 1];
		Formats(7,2).type='none';
		
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		if ~isempty(PreviousCalc)
			Prompt(end+1,:)={'Load Calculation?','LoadSwitch'};
		
			%LoadToggle toggle
			Formats(8,1).type='check';
			Formats(8,1).size = [-1 0];
			Formats(8,2).limits = [0 1];
			Formats(8,2).type='none';	
			
			defval.LoadSwitch=0;
		end
		
		
		
		
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		if ~isempty(PreviousCalc)&Canceled==0&NewParameter.LoadSwitch==1
			%Load data
			obj.StartCal=false;
			obj.loadCalc(PreviousCalc);
			
		else
		
			if ~obj.ProfileSwitch
				%unpack from the structure
				obj.CalcParameter.Calc_Method=NewParameter.Calc_Method;
				obj.CalcParameter.Selection_Method=NewParameter.Selection_Method;
				obj.CalcParameter.SmoothMode=NewParameter.SmoothMode;
				obj.CalcParameter.SmoothFactor=NewParameter.SmoothFactor;
				obj.CalcParameter.NumEvents=NewParameter.NumEvents;
				obj.CalcParameter.Sel_Radius=NewParameter.Sel_Radius;
				obj.CalcParameter.dx=NewParameter.dx;
				obj.CalcParameter.dy=NewParameter.dy;
				obj.CalcParameter.MinNumber=NewParameter.MinNumber;
				obj.CalcParameter.DampSatsi=NewParameter.DampSatsi;
				
			else
				%unpack from the structure
				obj.CalcParameter.Calc_Method=NewParameter.Calc_Method;
				obj.CalcParameter.Selection_Method=NewParameter.Selection_Method;
				obj.CalcParameter.SmoothMode=NewParameter.SmoothMode;
				obj.CalcParameter.SmoothFactor=NewParameter.SmoothFactor;
				obj.CalcParameter.NumEvents=NewParameter.NumEvents;
				obj.CalcParameter.Sel_Radius=NewParameter.Sel_Radius;
				obj.CalcParameter.dx=NewParameter.dx;
				obj.CalcParameter.dz=NewParameter.dz;
				obj.CalcParameter.MinNumber=NewParameter.MinNumber;
				obj.CalcParameter.DampSatsi=NewParameter.DampSatsi;
			
			
			end
			
			
			
			if Canceled==1
				obj.StartCalc=false;
			else
				obj.StartCalc=true;
			end
		
		end
		
		
	end
	
	
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		import mapseis.projector.*;
		import mapseis.calc.StressInv.*;
		import mapseis.calc.*;
		import mapseis.util.importfilter.cellArrayToPlane;
		
		if obj.CalcParameter.Calc_Method==1
			%regular mode
			%build the calculation object
			StressCalc = mapseis.calc.Calculation(...
		    			@mapseis.calc.StressInv.CalcStressInv_node,@mapseis.projector.getFocals,{},...
		    			'Name','StressInversion');
		    	
		    	if ~obj.ProfileSwitch
				%normal lon lat grid
				
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.Filterlist.getSelected;
				
				if strcmp(RegRange,'all');
					[locations temp]=getLocations(obj.Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
				else
					bounded=pRegion.getBoundingBox;
					
				end
				
				CalcParam=struct(	'boundaryBox',[bounded],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dy]],...
							'SelectionRadius',km2deg(obj.CalcParameter.Sel_Radius),...
							'SelectedEvents',[selected],...
							'SelectionNumber',obj.CalcParameter.NumEvents,...
							'SelectionMode',SelMode,...
							'DistanceSend',false,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=norm2DGridder(obj.Datastore,StressCalc,CalcParam);
		    		
				
				RawRes=cellArrayToPlane(calcRes);
				
				fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    	else
		    		
		    		regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				
				if obj.CalcParameter.Selection_Method==1
					SelMode='Number';
				elseif obj.CalcParameter.Selection_Method==2
					SelMode='Radius';
				elseif obj.CalcParameter.Selection_Method==3
					SelMode='Both';
				end
				
				selected=obj.Filterlist.getSelected;
				
				CalcParam=struct(	'boundaryBox',[pRegion.getBoundingBox],...
							'Gridspacing',[[obj.CalcParameter.dx obj.CalcParameter.dz]],...
							'SelectionRadius',obj.CalcParameter.Sel_Radius,...
							'SelectedEvents',[selected],...
							'Sel_Nr',obj.CalcParameter.NumEvents,...
							'Sel_Mode',SelMode,...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'DistanceSend',false,...
							'ParallelCalc',obj.ParallelMode);
							
				[xRange,yRange,calcRes]=Profile2DGridder(obj.Datastore,StressCalc,CalcParam);
		    		
				RawRes=cellArrayToPlane(calcRes);
				%obj.CalcRes.X=xRange;
		    		%obj.CalcRes.Y=yRange;
		    		
		    		fields=fieldnames(RawRes);
				for i=1:numel(fields)
					obj.CalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
				end
				
				[xx yy]=meshgrid(xRange,yRange);
				le=numel(xx);
				xvec=reshape(xx,le,1);
				yvec=reshape(yy,le,1);
				obj.CalcRes.X=xRange;
		    		obj.CalcRes.Y=yRange;
		    		obj.CalcRes.Xmeshed=xvec;
		    		obj.CalcRes.Ymeshed=yvec;
		    		
		    	end
		    	
		    	
		    		
		elseif obj.CalcParameter.Calc_Method==2
			
			%Satsi method
			RawCalcRes = calcStress_Satsi(obj.Datastore,obj.Filterlist,obj.ProfileSwitch,obj.CalcParameter.DampSatsi)
			
			%build the CalcRes for the plotting
			
			%single solutions (not needed now)
			obj.CalcRes.Y_s = RawCalcRes{2}.Y;
			obj.CalcRes.X_s = RawCalcRes{2}.X;
			obj.CalcRes.strike_s = RawCalcRes{2}.strike;
			obj.CalcRes.dip_s = RawCalcRes{2}.dip;
			obj.CalcRes.rake_s = RawCalcRes{2}.rake;
			obj.CalcRes.fit_angle_s = RawCalcRes{2}.fit_angle;
			obj.CalcRes.mag_tau_s = RawCalcRes{2}.mag_tau;
			
			obj.CalcRes.X=RawCalcRes{1}.X;
			obj.CalcRes.Y=RawCalcRes{1}.Y;
			obj.CalcRes.See=RawCalcRes{1}.See;
			obj.CalcRes.Sen=RawCalcRes{1}.Sen;
			obj.CalcRes.Seu=RawCalcRes{1}.Seu;
			obj.CalcRes.Snn=RawCalcRes{1}.Snn;
			obj.CalcRes.Snu=RawCalcRes{1}.Snu;
			obj.CalcRes.Suu=RawCalcRes{1}.Suu;
			
			obj.CalcRes.overall_fit_angle_mean=RawCalcRes{3}.fit_angle_mean;
			obj.CalcRes.overall_std_dev_angle=RawCalcRes{3}.std_dev_angle;
			obj.CalcRes.overall_avg_tau=RawCalcRes{3}.avg_tau;
			obj.CalcRes.overall_std_dev_tau=RawCalcRes{3}.std_dev_tau;
			
			%delete later, just a note
			%numericFields_A = {'X','Y','See','Sen','Seu','Snn','Snu','Suu'};
			%numericFields_B = {'strike','dip','rake','fit_angle','mag_tau','X','Y'};
			%LastFields={'fit_angle_mean','std_dev_angle','avg_tau','std_dev_tau'}
			
			%The Satsi Algorithmen seems to only return the stresstensor. For plotting the principal
			%stress axis are needed, so they have to be calculated here.
			%eigenvalues and vectors
			
			%get Stresstensors
			RawStressTensors = mapseis.MiniWrapper.StressFieldCalcWrapper.FormTensor(...
					obj.CalcRes.See,obj.CalcRes.Sen,obj.CalcRes.Seu,obj.CalcRes.Snn,...
					obj.CalcRes.Snu,obj.CalcRes.Suu);
			
			%convert into cell		
			%StressTensors=num2cell(RawStressTensors,[2 3]);
			%StressTensors=cellfun(@(x) reshape(x,3,3),StressTensors,'UniformOutput',false);
			if numel(RawStressTensors)>9
				StressTensors=num2cell(RawStressTensors,[1 2]);
			else
				%pack manually
				Tenss(1,:)=RawStressTensors(:,:,1);
				Tenss(2,:)=RawStressTensors(:,:,2);
				Tenss(3,:)=RawStressTensors(:,:,3);
				StressTensors{1}=Tenss;
				
			end
			%disp(RawStressTensors);
			
			%get eigenvals and eigenvectors
			[eigvec eigval]=cellfun(@eig,StressTensors,'UniformOutput',false);
			
			%now I have to get the strike and val and dip of the principal axis somehow?
			%how?
			%and how to do it in a effectiv way?
			%---> principal vector = (p1,p2,p3)
			%---> strike = arccos(p2/sqrt(p1^2+p2^2)) 
			%---> dip = (p1^2+p2^2)/(sqrt(p1^2+p2^2)*sqrt(p1^2+p2^2+p3^2))
			%		maybe this can be simplified
			%---> value = eigenvalue?
			%This whole transformation has to be tested, I'm not fully convinced it works 
			
			%reform eigval
			if iscell(eigval)
				eigval=eigval(1,:)';
			else
				eigval={eigval};
				eigvec={eigvec};
			end
			disp(eigval)
			disp(eigvec)
			
			[s1_strike s1_dip s2_strike s2_dip s3_strike s3_dip] =...
				cellfun(@mapseis.MiniWrapper.StressFieldCalcWrapper.StrikeNDip,...
					eigvec,'UniformOutput',false);
					
			[s1_value s2_value s3_value] = ...
				cellfun(@mapseis.MiniWrapper.StressFieldCalcWrapper.eigValUnpacker,...
					eigval,'UniformOutput',false);		
					
			obj.CalcRes.s1_strike=cell2mat(s1_strike);
			obj.CalcRes.s1_dip=cell2mat(s1_dip);
			obj.CalcRes.s1_value=cell2mat(s1_value);
			
			obj.CalcRes.s2_strike=cell2mat(s2_strike);
			obj.CalcRes.s2_dip=cell2mat(s2_dip);
			obj.CalcRes.s2_value=cell2mat(s2_value);
			
			obj.CalcRes.s3_strike=cell2mat(s3_strike);
			obj.CalcRes.s3_dip=cell2mat(s3_dip);
			obj.CalcRes.s3_value=cell2mat(s3_value);
			
			%similar to Phi on confentional method, don't know if this is right, might change
			%it later
			obj.CalcRes.S2_S1_rel=obj.CalcRes.s2_value/obj.CalcRes.s1_value;
			
			
			%calc faulting style and RMS (later)
			for i=1:numel(obj.CalcRes.Y)
				rStress.fS11=obj.CalcRes.See(i);
				rStress.fS12=obj.CalcRes.Sen(i);
				rStress.fS13=obj.CalcRes.Seu(i);
				rStress.fS22=obj.CalcRes.Snn(i);
				rStress.fS23=obj.CalcRes.Snu(i);
				rStress.fS33=obj.CalcRes.Suu(i);
				
				rStress.fS1Plunge=obj.CalcRes.s1_dip(i);
				rStress.fS2Plunge=obj.CalcRes.s2_dip(i);
				rStress.fS3Plunge=obj.CalcRes.s3_dip(i);
				rStress.fPhi=obj.CalcRes.S2_S1_rel(i);
				
				% fAphi   : 0 <= fAphi < 1 Normal faulting
				%           1 <= fAphi < 2 Strike slip
				%           2 <= fAphi < 3 Thrust faulting
				obj.CalcRes.FaultStyle(i)=calc_FaultStyle(rStress);
				
				%RMS
				inNode=obj.CalcRes.Y(i)==obj.CalcRes.Y_s&obj.CalcRes.X(i)==obj.CalcRes.X_s;
				
				%focals
				vDipDir=obj.CalcRes.strike_s(inNode);
				vDip=obj.CalcRes.dip_s(inNode);
				vRake=obj.CalcRes.rake_s(inNode);
				
			
				obj.CalcRes.Rms = calc_FMdiversity(vDipDir,vDip,vRake);
			end
			
			
		end
		
	end
	
	
	
	
	function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
		% Find out of figure already exists
		%
		[existFlag,figNumber]=figflag('Stress-Inv-map',1);
		newbmapWindowFlag=~existFlag;                          
		
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Stress-Inv-map',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',[ 50 300 1100 690 ],'Renderer','OpenGL');
		   plotAxis=gca;
		   set(plotAxis,'Tag','MainResultAxis');
		   
		   obj.buildMenus;
		   
		end
		
	end
	
	
	function buildMenus(obj)
		%builds the menu in the resultgui, no plot will be done automatically
		
		%This is partly depended on the selected calculation type (Michael or SATSI)
		
		%Parameters which should be plottable:
		%	relative Stress
		%	Orientation of s1,s2 and s3
		%	faultstyle
		%	s1/s3 ratio
		%	Element of the Stresstensor? (submenu)
		
		
		if ~obj.ProfileSwitch
			StrikeMapping='quiver';
			DipMapping='colormap';
		else
			StrikeMapping='colormap';
			DipMapping='quiver';
		end
		
		
		
		%first the general menus (similar for both)
		 OptionMenu = uimenu( obj.ResultGUI,'Label','- Load/Save');
		 uimenu(OptionMenu,'Label','Calculate again',...
                'Callback',@(s,e) obj.CalcAgain);
                uimenu(OptionMenu,'Label','Write to Datastore',...
                'Callback',@(s,e) obj.saveCalc);
                uimenu(OptionMenu,'Label','Load from Datastore',...
                'Callback',@(s,e) obj.loadCalc([]));
		
		
                %Plot option menu, allows to selected plot options like plotting earthquakes
                %coast- & borderlines
                obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
		 uimenu(obj.PlotOptions,'Label','plot Coastlines',...
                'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
                 uimenu(obj.PlotOptions,'Label','plot Country Borders',...
                'Callback',@(s,e) obj.TooglePlotOptions('Border'));
                 uimenu(obj.PlotOptions,'Label','plot Earthquake Locations',...
                'Callback',@(s,e) obj.TooglePlotOptions('EQ'));
                
                 uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
                'Callback',@(s,e) obj.setqual('low'));
                uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
                'Callback',@(s,e) obj.setqual('med'));
                uimenu(obj.PlotOptions,'Label','High Quality Plot',...
                'Callback',@(s,e) obj.setqual('hi'));
                
                uimenu(obj.PlotOptions,'Label','Shading Interpolated','Separator','on',... 
                'Callback',@(s,e) obj.plot_modify([],'interp',[]));
                uimenu(obj.PlotOptions,'Label','Shading Flat',... 
                'Callback',@(s,e) obj.plot_modify([],'flat',[]));
                uimenu(obj.PlotOptions,'Label','Brighten +0.4',... 
                'Callback',@(s,e) obj.plot_modify(0.4,[],[]));
                 uimenu(obj.PlotOptions,'Label','Darken -0.4',... 
                'Callback',@(s,e) obj.plot_modify(-0.4,[],[]));
                
                %set the checks right
                obj.TooglePlotOptions('-chk');
                obj.setqual('chk');
                
                %now the type depended map menu
                
		if obj.CalcParameter.Calc_Method==1
			%conventional Michael
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','S2/S1 relative size',...
			'Callback',@(s,e) obj.plotVariable([],'fPhi','colormap'));
			 uimenu(MapMenu,'Label','S1 strike',...
			'Callback',@(s,e) obj.plotVariable([],'fS1Trend',StrikeMapping));
			 uimenu(MapMenu,'Label','S1 dip',...
			'Callback',@(s,e) obj.plotVariable([],'fS1Plunge',DipMapping));
			uimenu(MapMenu,'Label','S2 strike',...
			'Callback',@(s,e) obj.plotVariable([],'fS2Trend',StrikeMapping));
			 uimenu(MapMenu,'Label','S2 dip',...
			'Callback',@(s,e) obj.plotVariable([],'fS2Plunge',DipMapping));
			uimenu(MapMenu,'Label','S3 strike',...
			'Callback',@(s,e) obj.plotVariable([],'fS3Trend',StrikeMapping));
			uimenu(MapMenu,'Label','S3 dip',...
			'Callback',@(s,e) obj.plotVariable([],'fS3Plunge',DipMapping));
			uimenu(MapMenu,'Label','Focal mechanism diversity',...
			'Callback',@(s,e) obj.plotVariable([],'fRms','colormap'));
			uimenu(MapMenu,'Label','Faulting Style',...
			'Callback',@(s,e) obj.plotVariable([],'fAphi','colormap'));
			uimenu(MapMenu,'Label','Earthquake density',...
			'Callback',@(s,e) obj.plotVariable([],'dCount','colormap'));
			
			
			TensorMenu = uimenu( MapMenu,'Label','Stress Tensor');
			uimenu(TensorMenu,'Label','S11',...
			'Callback',@(s,e) obj.plotVariable([],'fS11','colormap'));
			uimenu(TensorMenu,'Label','S12',...
			'Callback',@(s,e) obj.plotVariable([],'fS12','colormap'));
			uimenu(TensorMenu,'Label','S13',...
			'Callback',@(s,e) obj.plotVariable([],'fS13','colormap'));
			uimenu(TensorMenu,'Label','S22',...
			'Callback',@(s,e) obj.plotVariable([],'fS22','colormap'));
			uimenu(TensorMenu,'Label','S23',...
			'Callback',@(s,e) obj.plotVariable([],'fS23','colormap'));
			uimenu(TensorMenu,'Label','S33',...
			'Callback',@(s,e) obj.plotVariable([],'fS33','colormap'));
			
			uimenu(MapMenu,'Label','Smoothing','Separator','on',... ...
			'Callback',@(s,e) obj.Smoother('switch'));
			uimenu(MapMenu,'Label','Smoothing Factor',...
			'Callback',@(s,e) obj.Smoother('factor'));
			
			
			%additional entries for load/save
			uimenu(OptionMenu,'Label','export Stress Field (matlab)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu(OptionMenu,'Label','export Stress Field (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu(OptionMenu,'Label','export Stress Field (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu(OptionMenu,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
			
		elseif 	obj.CalcParameter.Calc_Method==2
			%Satsi
			MapMenu = uimenu( obj.ResultGUI,'Label','- Maps');
			 uimenu(MapMenu,'Label','S2/S1 relative size',...
			'Callback',@(s,e) obj.plotVariable([],'S2_S1_rel','colormap'));
			 uimenu(MapMenu,'Label','S1 strike',...
			'Callback',@(s,e) obj.plotVariable([],'s1_strike',StrikeMapping));
			 uimenu(MapMenu,'Label','S1 dip',...
			'Callback',@(s,e) obj.plotVariable([],'s1_dip',DipMapping));
			uimenu(MapMenu,'Label','S2 strike',...
			'Callback',@(s,e) obj.plotVariable([],'s2_strike',StrikeMapping));
			 uimenu(MapMenu,'Label','S2 dip',...
			'Callback',@(s,e) obj.plotVariable([],'s2_dip',DipMapping));
			uimenu(MapMenu,'Label','S3 strike',...
			'Callback',@(s,e) obj.plotVariable([],'s3_strike',StrikeMapping));
			uimenu(MapMenu,'Label','S3 dip',...
			'Callback',@(s,e) obj.plotVariable([],'s3_dip',DipMapping));
			uimenu(MapMenu,'Label','Focal mechanism diversity',...
			'Callback',@(s,e) obj.plotVariable([],'Rms','colormap'));
			uimenu(MapMenu,'Label','Faulting Style',...
			'Callback',@(s,e) obj.plotVariable([],'FaultStyle','colormap'));
		
						
			TensorMenu = uimenu( MapMenu,'Label','Stress Tensor');
			uimenu(TensorMenu,'Label','S11',...
			'Callback',@(s,e) obj.plotVariable([],'See','colormap'));
			uimenu(TensorMenu,'Label','S12',...
			'Callback',@(s,e) obj.plotVariable([],'Sen','colormap'));
			uimenu(TensorMenu,'Label','S13',...
			'Callback',@(s,e) obj.plotVariable([],'Seu','colormap'));
			uimenu(TensorMenu,'Label','S22',...
			'Callback',@(s,e) obj.plotVariable([],'Snn','colormap'));
			uimenu(TensorMenu,'Label','S23',...
			'Callback',@(s,e) obj.plotVariable([],'Snu','colormap'));
			uimenu(TensorMenu,'Label','S33',...
			'Callback',@(s,e) obj.plotVariable([],'Suu','colormap'));
			
			
			%additional entries for load/save
			uimenu(OptionMenu,'Label','export Full Result','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('full_result'));
			
		end	
		
		
		obj.Smoother('init');
		
		
	end
	
	
	
	function Smoother(obj,DoWhat)
		%Turns the smoothing on and off after calculation
		SmoothMenu = findobj(obj.ResultGUI,'Label','Smoothing');
		set(SmoothMenu, 'Checked', 'off');
		
		
		switch DoWhat
			case 'switch'
				if obj.CalcParameter.SmoothMode==1
					obj.CalcParameter.SmoothMode=0;
					set(SmoothMenu, 'Checked', 'off');
				else
					obj.CalcParameter.SmoothMode=1;
					set(SmoothMenu, 'Checked', 'on');
				end
				obj.updateGUI;
				
				
			case 'factor'
				promptValues = {'Smoothing Factor'};
				dlgTitle = 'Set Smoothing Factor';
				gParams = inputdlg(promptValues,dlgTitle,1,...
					cellfun(@num2str,{obj.CalcParameter.SmoothFactor},'UniformOutput',false));
				obj.CalcParameter.SmoothFactor=	str2double(gParams{1});
				
				if obj.CalcParameter.SmoothMode==1
					obj.updateGUI;
				end
			
				
			case 'init'
				set(SmoothMenu, 'Checked', 'off');
				if obj.CalcParameter.SmoothMode==1
					set(SmoothMenu, 'Checked', 'on');
				end
				
		end
	
		
	end
	
	
	
	function TooglePlotOptions(obj,WhichOne)
		%This functions sets the toogles in this modul
		%WhichOne can be the following strings:
		%	'Border':	Borderlines
		%	'Coast'	:	Coastlines
		%	'EQ'	:	Earthquake locations
		%	'-chk'	:	This will set all checks in the menu
		%			to the in toggles set state.
		
		CoastMenu = findobj(obj.PlotOptions,'Label','plot Coastlines');
		BorderMenu = findobj(obj.PlotOptions,'Label','plot Country Borders');
		EqMenu = findobj(obj.PlotOptions,'Label','plot Earthquake Locations');
		
		switch WhichOne
			case '-chk'
				if obj.BorderToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				if obj.CoastToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				if obj.EQToogle
					set(EqMenu, 'Checked', 'on');
				else
					set(EqMenu, 'Checked', 'off');
				end
				
				
			case 'Border'
				
				obj.BorderToogle=~obj.BorderToogle;
				
				if obj.BorderToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'Coast'
				
				obj.CoastToogle=~obj.CoastToogle;
				
				if obj.CoastToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'EQ'
				
				obj.EQToogle=~obj.EQToogle;
				
				if obj.EQToogle
					set(EqMenu, 'Checked', 'on');
				else
					set(EqMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
		
		end
		
		
	end
	
	
	function plotResult(obj,whichplot)
		%more of a relict dummy function 
		%it allows a bit less complicated plotting, more for scripting
		%it calls plotVariable
		
		if ~obj.ProfileSwitch
			StrikeMapping='quiver';
			DipMapping='colormap';
		else
			StrikeMapping='colormap';
			DipMapping='quiver';
		end
		
		
		
		if obj.CalcParameter.Calc_Method==1
			switch whichplot
				case 'stress'
					%just plot phi (s2/s1 relation)
					obj.plotVariable([],'fPhi','colormap');
					
				case 'Diversity'
					obj.plotVariable([],'fRms','colormap');	
				
				case 'Faulting'
					obj.plotVariable([],'fAphi','colormap');
				
				case 'S1dir'	
					obj.plotVariable([],'fS1Trend',StrikeMapping);
				
				case 'S2dir'	
					obj.plotVariable([],'fS2Trend',StrikeMapping);
				
				case 'S3dir'	
					obj.plotVariable([],'fS3Trend',StrikeMapping);
					
			end
			%more later
			
		
		elseif obj.CalcParameter.Calc_Method==2
			switch whichplot
				case 'stress'
					%just plot phi (s2/s1 relation)
					obj.plotVariable([],'S2_S1_rel','colormap');
					
				case 'Diversity'
					obj.plotVariable([],'Rms','colormap');	
				
				case 'Faulting'
					obj.plotVariable([],'FaultStyle','colormap');
				
				case 'S1dir'	
					obj.plotVariable([],'s1_strike',StrikeMapping);
					
				case 'S2dir'	
					obj.plotVariable([],'s2_strike',StrikeMapping);
					
				case 'S3dir'	
					obj.plotVariable([],'s3_strike',StrikeMapping);
						
			end
			%more later
		
		
		
		end
	
	end
	
	
	function plotVariable(obj,plotAxis,ValField,PlotType)
		%will plot the selected field of the CalcRes structur into the selected plotaxis
		%the PlotType can either be 'colormap' or 'quiver' (have to angles for a quiver plot)
		import mapseis.plot.*;
		
		
		%get plotAxis if needed
		if isempty(plotAxis)
			 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(plotAxis,'Tag','MainResultAxis');
    			end	
		end
		
		%backup config
		obj.LastPlot={ValField,PlotType};
		
		%get data
		xdir=obj.CalcRes.X;
		ydir=obj.CalcRes.Y;
		TheVal=obj.CalcRes.(ValField)';
		
		AlphaMap=~isnan(TheVal);
		
		
		if obj.CalcParameter.SmoothMode==1;
			%Smooth the map before plotting
			TheValNaN=isnan(TheVal);
			TheVal(TheValNaN)=0;
			
			hFilter = fspecial('gaussian', 5, 2.5);
			TheVal= imfilter((TheVal + obj.CalcParameter.SmoothFactor), hFilter, 'replicate');
			TheVal(TheValNaN)=NaN;
			
		end
		
		%limits
		xlimiter=[floor(min(xdir)) ceil(max(xdir))];
		ylimiter=[floor(min(ydir)) ceil(max(ydir))];
		
		if obj.ProfileSwitch
			xlab='Profile [km] ';
			ylab='Depth [km] ';
		else
			xlab='Longitude ';
			ylab='Latitude ';
		end	
		
		
		
		%set all axes right
		xlim(plotAxis,xlimiter);
		ylim(plotAxis,ylimiter);
		
		latlim = get(plotAxis,'Ylim');
           	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           	set(plotAxis,'DrawMode','fast');
		
		
		%determine if a quiver is needed 
		switch PlotType
			case 'colormap'
				
				if obj.CalcParameter.Calc_Method==1
					%TheVal=reshape(TheVal,numel(ydir),numel(xdir));
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				PlotData={xdir,ydir,TheVal};
			
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
				  				'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData','auto',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'ColorToggle',true,...
								'LegendText',ValField);
				 disp(plotAxis)				
				 [handle legendentry] = PlotColor(plotAxis,PlotConfig); 
								
								
			case 'quiver'
				
					
				if obj.CalcParameter.Calc_Method==1
					xdir=obj.CalcRes.Xmeshed;
					ydir=obj.CalcRes.Ymeshed;
					TheVal=reshape(TheVal,numel(xdir),1);
					AlphaMap=reshape(AlphaMap,numel(xdir),1);
					xdir=xdir(AlphaMap);
					ydir=ydir(AlphaMap);
					TheVal=TheVal(AlphaMap);
					
				else
					%might need to be done something, but I will see
					%SATSI has its problems anyway
				end
				
				%calculate dx and dy for the angle (TheVal
				%only up 180� used
				le=0.2;
				
				TheVal=mod(TheVal,180);

				%first the angles smaller than 90�
				sma=TheVal<90;

				%the larger than 90� angels
				lar=not(sma);

				%the x coordinate
				avect(:,1) = sin(deg2rad(TheVal))*le;

				%the sma part
				avect(sma,2) = cos(deg2rad(TheVal(sma)))*le;
				
				%the lar part
				avect(lar,2) = -cos(deg2rad(TheVal(lar) - 90))*le;
				
				PlotData=[xdir,ydir,avect(:,1),avect(:,2)];
				
				PlotConfig	= struct(	'PlotType','Vector2D',...
				  				'Data',[PlotData],...
								'MapStyle','not needed',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','red',...
								'LineStylePreset','normal',...
								'MarkerStylePreset','normal',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit','auto',...
								'DirectionMode',true,...
								'ColorToggle',true,...
								'LegendText',ValField);
				disp(plotAxis)
			         [handle legendentry] = PlotVector2D(plotAxis,PlotConfig);
		end	
		
		if ~obj.ProfileSwitch
			%at the moment only available in the map view
			%plot coastline, borders and earthquake locations if wanted
			if obj.BorderToogle
				hold on
				BorderConf= struct('PlotType','Border',...
							'Data',obj.Datastore,...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'LineStylePreset','normal',...
							'Colors','black');
				[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
				
			end
			
			
			if obj.CoastToogle
				hold on
				CoastConf= struct( 'PlotType','Coastline',...
						   'Data',obj.Datastore,...
						   'X_Axis_Label','Longitude ',...
						   'Y_Axis_Label','Latitude ',...
						   'LineStylePreset','normal',...
						   'Colors','blue');
				[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
			
			end
			
			
			if obj.EQToogle
				hold on
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',obj.SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
			
				
				[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
			end
				
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			latlim = get(plotAxis,'Ylim');
           		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
           		set(plotAxis,'DrawMode','fast');
			
			
			hold off
			
		else
			
		
			if obj.EQToogle
				regionFilter = obj.Filterlist.getByName('Region');
				filterRegion = getDepthRegion(regionFilter);
				pRegion = filterRegion{2};
				RegRange=filterRegion{1};
				ProfWidth=filterRegion{4};
				rBdry = pRegion.getBoundary();
				profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
				
				hold on
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'ProfileLine',profLine,...
							'ProfileWidth',ProfWidth,...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',obj.SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Distance [km] ',...
							'Y_Axis_Label','Depth [km] ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
			
				
				[handle1 entry1] = PlotEarthquakeSlice(plotAxis,EqConfig);
			end
				
			%set all axes right
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
			
			%depthlim = get(plotAxis,'Ylim');
			%distlim = get(plotAxis,'Xlim');
			%deSpan=10*abs(depthlim(1)-depthlim(2));
			%diSpan=abs(distlim(1)-distlim(2));
			
			%set(plotAxis,'dataaspect',[1 diSpan/deSpan 1]);
			set(plotAxis,'dataaspect',[2 1 1]);
			set(plotAxis,'YDir','reverse');
			hold off
		end
	end
	
	
	function updateGUI(obj)
		%uses the last plot option and plot it again
		ValField=obj.LastPlot{1};
		PlotType=obj.LastPlot{2};
		
		%only needed for quality change a similar stuff
		obj.plotVariable([],ValField,PlotType);
		
	end
	
	
	function plot_modify(obj,bright,interswitch,circleGrid)
		%allows to modify the plot (some legacy stuff which will be changed
		%later if needed is in here)
		 plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(plotAxis)
			 %try retag
			 %get all childrens of the main gui
    			gracomp = get(obj.ResultGUI, 'Children');
    			
    			%find axis and return axis
    			plotAxis = findobj(gracomp,'Type','axes','Tag','');
    			set(plotAxis,'Tag','MainResultAxis');
    		end	
		
    		if ~isempty(bright)
			axes(plotAxis);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			
			switch interswitch
				case 'flat'
					axes(plotAxis);
					shading flat;
					
				case 'interp'
					axes(plotAxis);
					shading interp;
					
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		
	end
	
	
	function adju(obj,asel, whichplot)
		%function version of adju
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;


		prompt={'Enter the minimum magnitude cut-off','Enter the maximum radius cut-off:','Enter the minimum goodness of fit percatge'};
		def={'nan','nan','nan'};
		dlgTitle='Input Map subselection Criteria';
		lineNo=1;
		answer=inputdlg(prompt,dlgTitle,lineNo,def);
		re4 = re3;
		l = answer{1,1}; Mmin = str2num(l) ;
		l = answer{2,1}; tresh = str2num(l) ;
		l = answer{3,1}; minpe = str2num(l) ;
		
		
		obj.PackList=[obj.PackList; {'re4';'l';'asel'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

		%back to plot
		obj.plot_bval(whichplot);
		
	end
	
	function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	
	
	function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		EveryThingGood=true;
		
		%get parameters
		obj.openCalcParamWindow;
		
		if obj.StartCalc
			
			%figNr=findobj('Name','b-value-map');
			
			%if ~isempty(figNr)
			%	close(figNr);
			%end
		
			%refresh catalogs
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			%obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%check if the focal mechanism are available
			try
				obj.Datastore.getFields({'focal_strike','focal_dip','focal_rake'});
			catch	
				EveryThingGood=false;
				errordlg('No focal mechanism available in the new catalog');
				obj.ErrorDialog='No focal mechanism available in the new catalog';
			end
			
			if obj.ProfileSwitch
				 regionFilter = obj.Filterlist.getByName('Region');
				 filterRegion = getRegion(regionFilter);
				 RegRange=filterRegion{1};	
				 if ~strcmp(RegRange,'line')
					EveryThingGood=false;
					errordlg('No profile selected in the current regionfilter');
					obj.ErrorDialog='No profile selected in the current regionfilter';
				 end
			end
			if EveryThingGood
				%Calculation
				obj.doTheCalcThing;
				
				obj.updateGUI;
				
				%Build resultGUI
				%obj.BuildResultGUI
						
				%plot results
				%obj.plotResult('stress')
			end	
		end
	
	
	end
	
	
	function saveCalc(obj)
		%This will save the calculation in the datastore and ask to open it next time
		%The filterlist will be saved within the datastore with this and reopened next time
		%when the calculation is loaded. It has to be done in this way or else nobody knows 
		%what filtersetting was used in the calculation.
		
		%Ask for a name
		gParams = inputdlg('Calculation Name','Name of the Calculation',1,{obj.CalcName});
		if ~isempty(gParams)
			obj.CalcName=gParams{1};
			
			%Maybe the Filterlist needs to be cloned, but for now try without
			obj.Filterlist.PackIt;
			
			%add ProfileSwitch to CalcParameter
			SaveCalcPar=obj.CalcParameter;
			SaveCalcPar.ProfileSwitch=obj.ProfileSwitch;
			
			CalcObject={obj.CalcName,SaveCalcPar,obj.CalcResult,obj.Filterlist};
			
			%try getting the CalcObject from the datastore
			
			try 
				Calculations=obj.Datastore.getUserData('StressInversion-v1');
				
			catch
				Calculations={};
			end
			
			if ~isempty(Calculations)
				%try finding the calc
					founded=strcmp(Calculations(:,1),obj.CalcName);
					if sum(founded)>0
						Calculations(founded,:)=CalcObject;
					else
						Calculations(end+1,:)=CalcObject;
					end	
			else
				Calculations(1,:)=CalcObject;
			end	
			
			obj.Datastore.setUserData('StressInversion-v1',Calculations);
			
		end
		
		
		
		%write to the Commander (always do this even if canceled, this will update
		%the buffers)
		obj.Commander.pushWithKey(obj.Keys.ShownDataID,obj.Datastore);
		obj.Commander.switcher('Cata');
		
	end
	
	
	
	function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		if isempty(oldCalcs)
			%check if old calculations are avialable
			try
				oldCalcs=obj.Datastore.getUserData('StressInversion-v1');
			catch
				disp('no calculation available in this datastore')
				return;
			end
		end
		
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne';...
			'Overwrite FilterList?','OverWriteFilter'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%Overwrite
		Formats(2,2).type='list';
		Formats(2,2).style='togglebutton';
		Formats(2,2).items={'Yes','No'};
		Formats(2,2).size = [-1 0];
		
		Formats(2,3).limits = [0 1];
		Formats(2,3).type='none';
		Formats(2,1).limits = [0 -1];
		Formats(2,1).type='none';
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1,...
				'OverWriteFilter',2);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
			obj.Loaded=false;
		else
			obj.StartCalc=true;
			obj.Loaded=true;
		end
		
		if obj.StartCalc
			TheCalc=oldCalcs(NewParameter.WhichOne,:);
			
			%Overwrite Filter 
			if NewParameter.OverWriteFilter==1
				Keys=obj.Commander.getMarkedKey;
				FilterKey=Keys.ShownFilterID;
				
				obj.Commander.pushWithKey(TheCalc{4},FilterKey);
				obj.Commander.switcher('Filter');
				
			else %Create new filter
				newdata.Filterlist=TheCalc{4};
				obj.Commander.pushIt(newdata);
				obj.Commander.switcher('Filter');
			end
			
			%Now set the data
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.CalcParameter=TheCalc{2};
			obj.ProfileSwitch=obj.CalcParameter.ProfileSwitch;
			obj.CalcResult=TheCalc{3};
			
			
			
			
		end
		
	end
	
	function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
			Menu2Check(3) = findobj(obj.PlotOptions,'Label','Zmap style');
			Menu2Check(2) = findobj(obj.PlotOptions,'Label','Medium Quality Plot');
			Menu2Check(1) = findobj(obj.PlotOptions,'Label','High Quality Plot');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
 				case 'low'
 					%dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotQuality='low';
 					obj.updateGUI;
                		
				case 'med'
					%dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotQuality='med';
					obj.updateGUI;
					
				case 'hi'
					%dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotQuality='hi';
					obj.updateGUI;
					
				case 'chk'
					%try StoredQuality = dataStore.getUserData('PlotQuality');
					%catch dataStore.setUserData('PlotQuality','med');
					%StoredQuality = dataStore.getUserData('PlotQuality');		
					%end
					StoredQuality = obj.PlotQuality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
	end

	
	
	 function exportResult(obj,FileType,Filename) 
          	%saves the stress field into a mat or ascii file
          	
          	if nargin<3
          		Filename=[];
          	end
          
          	%get data
          	if ~isempty(obj.CalcRes)&~strcmp(FileType,'full_result')
          		xdir=obj.CalcRes.X;
			ydir=obj.CalcRes.Y;
          		S1Trend=obj.CalcRes.fS1Trend';
          		S1Plunge=obj.CalcRes.fS1Plunge';
          		S2Trend=obj.CalcRes.fS2Trend';
          		S2Plunge=obj.CalcRes.fS2Plunge';
          		S3Trend=obj.CalcRes.fS3Trend';
          		S3Plunge=obj.CalcRes.fS3Plunge';
          		s11=obj.CalcRes.fS11';
          		s12=obj.CalcRes.fS12';
          		s13=obj.CalcRes.fS13';
          		s22=obj.CalcRes.fS22';
          		s23=obj.CalcRes.fS23';
          		s33=obj.CalcRes.fS33';
          		
          		
          		
		elseif isempty(obj.CalcRes)
			return
		end
		
	
          	switch FileType
          		case 'matlab'
          			if isempty(Filename)
          				%get filename
          				[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','S1Trend','S1Plunge',...
						'S2Trend','S2Plunge','S3Trend','S3Plunge',...
						's11','s12','s13','s22','s23','s33');
				
          		case 'ascii'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,fName),'xdir','ydir','S1Trend','S1Plunge',...
						'S2Trend','S2Plunge','S3Trend','S3Plunge',...
						's11','s12','s13','s22','s23','s33','-ascii');
				
			case 'ascii_trio'
				if isempty(Filename)
					%get filename
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				%save
				save(fullfile(pName,['Xdir_',fName]),'xdir','-ascii');
				save(fullfile(pName,['Ydir_',fName]),'ydir','-ascii');
				save(fullfile(pName,['S1Trend_',fName]),'S1Trend','-ascii');
				save(fullfile(pName,['S1Plunge_',fName]),'S1Plunge','-ascii');
				save(fullfile(pName,['S2Trend_',fName]),'S2Trend','-ascii');
				save(fullfile(pName,['S2Plunge_',fName]),'S2Plunge','-ascii');
				save(fullfile(pName,['S3Trend_',fName]),'S3Trend','-ascii');
				save(fullfile(pName,['S3Plunge_',fName]),'S3Plunge','-ascii');
				save(fullfile(pName,['s11_',fName]),'s11','-ascii');
				save(fullfile(pName,['s12_',fName]),'s12','-ascii');
				save(fullfile(pName,['s13_',fName]),'s13','-ascii');
				save(fullfile(pName,['s22_',fName]),'s22','-ascii');
				save(fullfile(pName,['s23_',fName]),'s23','-ascii');
				save(fullfile(pName,['s33_',fName]),'s33','-ascii');
				
				
			case 'full_result'
				FullCalc=obj.CalcRes;
				
				if isempty(Filename)
					[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				
				if ~isempty(FullCalc)
					save(fullfile(pName,fName),'FullCalc');
				end
          	
          	end
          
          end
          
          
	




	
	
end	





methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	
	%THE FUNCTIONS UNDERNEATH ARE NOT ALWAYS DIRECTLY USEABLE, AS THEY OFTEN NEED DIRECT ACCESS
	%TO THE VARIABLE, SO JUST COPY IF NEEDED.
	function TheEvalString = UnpackStruct(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		TheEvalString=[];
		
		for i=1:numel(thefields)
			
			TheEvalString=[TheEvalString; thefields{i},'=' num2str(TheStruct.(thefields{i})),';'];
		end
		 %can be done easier directly in the function which should be calling this, it is more sort of 
		 %a sketch
		
		
	
	end
	
	
	function CellData = UnpackStruct2Cell(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		
		
		for i=1:numel(thefields)
			
			CellData{i,1}=thefields{i}
			CellData{i,2}=TheStruct.(thefields{i});
		end
		
		%assignin can be used to set the variable on the string.
	end
	
	function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itselectorf
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end
	
	
	function TheStruct = PackAllVariable()
		%The opposite of UnPack, it takes every variable in the current Workspace an packs it 
		%into a structure
		
		varilist=who;
		
		for i=1:numel(varilist)
			TheStruct.(varilist{i})=eval(varilist{i});
		
		end
	
	
	end
	
	
	function StressTensors = FormTensor(S11,S12,S13,S22,S23,S33)
		
		RawArray=[S11,S12,S13,S12,S22,S23,S13,S23,S33];
		
		StressTensors=reshape(RawArray',numel(S11),3,3);
		
		%this would pack the matrices into single cells 
		%num2cell(StressTensors,[2 3]);
	end
	
	
	function [s1_strike s1_dip s2_strike s2_dip s3_strike s3_dip] =StrikeNDip(eigenVectors)
		%calculates the strike and dip of the three eigenvectors
		%can be used in cellfun command
		
		%and how to do it in a effectiv way?
			%---> principal vector = (p1,p2,p3)
			%---> strike = arccos(p2/sqrt(p1^2+p2^2)) 
			%---> dip = acos((p1^2+p2^2)/(sqrt(p1^2+p2^2)*sqrt(p1^2+p2^2+p3^2)))
			%		maybe this can be simplified
			%---> value = eigenvalue?
			%This whole transformation has to be tested, I'm not fully convinced it works 
			
		princ1=eigenVectors(:,1);
		princ2=eigenVectors(:,2);
		princ3=eigenVectors(:,3);
		
		s1_strike=acos(princ1(2)/sqrt(princ1(2)^2+princ1(1)^2));
		s2_strike=acos(princ2(2)/sqrt(princ2(2)^2+princ2(1)^2));
		s3_strike=acos(princ3(2)/sqrt(princ3(2)^2+princ3(1)^2));
			
		s1_dip=acos((princ1(1)^2+princ1(2)^2)/...
			(sqrt(princ1(1)^2+princ1(2)^2)*sqrt(princ1(1)^2+princ1(2)^2+princ1(3)^2)));
		s2_dip=acos((princ2(1)^2+princ2(2)^2)/...
			(sqrt(princ2(1)^2+princ2(2)^2)*sqrt(princ2(1)^2+princ2(2)^2+princ2(3)^2)));
		s3_dip=acos((princ3(1)^2+princ3(2)^2)/...
			(sqrt(princ3(1)^2+princ3(2)^2)*sqrt(princ3(1)^2+princ3(2)^2+princ3(3)^2)));	
	
			
	
	end
	
	function [s1_value s2_value s3_value] = eigValUnpacker(eigVal)
		%simple function, gets the eigenvalues from the stupid matrix
		%used in a cellfun
		
		s1_value=eigVal(1,1);
		s2_value=eigVal(2,2);
		s3_value=eigVal(3,3);
		
	end
	
	%---------------
end
	
	

end
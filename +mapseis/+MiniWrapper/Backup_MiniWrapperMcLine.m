classdef MiniWrapperMcLine < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		ZmapCatalog
		ZmapVariables
		CalcResult
		CalcParameter
		ErrorDialog
		ParallelMode
		PackList
		StartCalc
		CalcName
		CalcType
		savePos
	end

	events
		CalcDone

	end


	methods
	
	function obj = MiniWrapperMcLine(ListProxy,Commander,CalcType,Override)
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		%This is a first application of the MiniWrapperTemplate
		%maybe later the Template will be used as SuperClass, but for 
		%this case the template is used directly
		
		%This models the function bdiff2 and some related function
		%it might be replaced later by the bigger McLineCalcWrapper
		
		%The override switch also to create a the object without any 
		%command started.
		if nargin<4
			Override=false;
		end
		%In this constructor everything is set, and the 
		obj.ListProxy=ListProxy;
		obj.Commander = Commander; 
		obj.ParallelMode=obj.Commander.ParallelMode;
		
		if ~Override
			%get the current datastore and filterlist and build zmap catalog
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%set The calcType
			obj.CalcType=CalcType;
			
			switch obj.CalcType
				case 'Mcauto'
					obj.savePos='zmap-Mcauto-calc';
					basName='Mcauto-calc';
					
				case 'McEstimate'
					obj.savePos='zmap-McEstimate-calc';
					basName='McEstimate-calc';
				
				case 'McTime'
					obj.savePos='zmap-McTime-calc';
					basName='McTime-calc';
					
				case 'bdepth'
					obj.savePos='zmap-bdepth-calc';
					basName='bdepth-calc';
					
				case 'btime'
					obj.savePos='zmap-btime-calc';
					basName='btime-calc';
					
				case 'bmag'
					obj.savePos='zmap-bmag-calc';
					basName='bmag-calc';
			end
			
			
			%check if there are any saved calculations
			try
				PreviousCalc=obj.Datastore.getUserData(obj.savePos);
			catch
				PreviousCalc=[];
			end
			
			if ~isempty(PreviousCalc)
				 button = questdlg('There are existing Calculations in this catalog, do you want to load them?',...
							'Load Calculations?','Yes','No','No');
			else
				button = 'No';
			end
			
			
			if strcmp(button,'No');
			%The signal to start, this will be set to true if the parameter 
			%inputdlg is ended with "Ok"
			obj.StartCalc=false;
			
			%Init the needed zmap variables
			obj.InitZmapVar;
			
			%Init Grid if needed
			%obj.Gridit(true);
			
			%get CalcParameter
			obj.openCalcParamWindow;
			
			if obj.StartCalc
				obj.CalcName=basName;
				
				%Build the grid with new parameters if needed
				%obj.Gridit(false);
				
				%Calculation
				obj.doTheCalcThing;
				
				%Plotting
				obj.plot_bval;
			
			end
			
			else
				%Init the needed zmap variables (here done only because
				%of the paths);
				obj.InitZmapVar;
				
				obj.loadCalc(PreviousCalc);
				
				if obj.StartCalc
					obj.plot_bval;
				end
			end	
			
		end
	end

	function InitZmapVar(obj)
		%here all variable should be set used by a zmap calculation
		%They are saved in the structur ZmapVariables with there name 
		%as there fieldname.
		
		%warning off;
		
		%Set the packlist, the mentioned variables will be packed into the structure
		%They have to be defined in here or it will cause an error.
		obj.PackList={'fs';'hodo';'hoda';'p';'fs8';'fs10';'fs12';'fs14';'fs16';'ms6';'ms10';'ms12';...
				'ty';'ty1';'ty2';'ty3';'typele';'sel';'selector';'lth1';'lth15';'lth2';'wex';'wey';...
				'fipo';'r';'term';'welx';'wely';'winx';'winy';'rad';'ic';'ya0';'xa0';'iwl3';...
				'iwl2';'step';'ni';'name';'strib';'stri2';'ho';'ho2';'infstri';'maix';'maiy';...
				'tresh';'wi';'rotationangle';'fre';'c1';'c2';'c3';'cb1';'cb2';'cb3';'in';'ldx';...
				'tlap';'ca';'vi';'sha';'inb1';'inb2';'inda';'ra';'co';'par1';'minmag';'sys';...
				'cputype';'ve';'my_dir';'mess';'plt'};
		
		% set some of the paths from the zmap.m procedure, some may be deleted later
		r = ceil(rand(1,1)*21);
		fipo = get(0,'ScreenSize');holii = cd;
		fipo(4) = fipo(4)-150;
		term = get(0,'ScreenDepth');
		
		sys = computer;
		cputype = computer;


		ve = version;
		ve = str2num(ve(1:3));

		


		
		
		fs = filesep;
		hodi=[holii fs 'AddOneFiles' fs 'ZmapInBox'];
		hodo = [hodi fs 'out' fs];
		hoda = [hodi fs 'eq_data' fs];
		p = path;
		addpath([hodi fs 'myfiles'],[hodi fs 'src'],[hodi fs 'src' fs 'utils'],[hodi fs 'src' fs 'declus'],...
		[hodi fs 'src' fs 'fractal'], [hodi fs 'help'],[hodi fs 'dem'],[hodi fs 'zmapwww'],[hodi fs 'importfilters'],...
		[hodi fs  fs 'src' fs 'utils' fs 'eztool'],[hodi fs 'm_map'],[hodi fs 'src' fs 'pvals'],[hodi], [hodi fs 'src' fs 'synthetic'], ...
		[hodi fs 'src' fs 'movies'],[hodi fs 'src' fs 'danijel'],[hodi fs 'src' fs 'danijel' fs 'calc'],...
		[hodi fs 'src' fs 'danijel' fs 'ex'],[hodi fs 'src' fs 'danijel' fs 'gui'],...
		[hodi fs 'src' fs 'danijel' fs 'focal'],...
		[hodi fs 'src' fs 'danijel' fs 'plot'],[hodi fs 'src' fs 'danijel' fs 'probfore'],...
		[hodi fs 'src' fs 'jochen'], [hodi fs 'src' fs 'jochen' fs 'seisvar' fs 'calc'],...
		[hodi fs 'src' fs 'jochen' fs 'seisvar'], [hodi fs 'src' fs 'jochen' fs 'ex'],...
		[hodi fs 'src' fs 'thomas' fs 'slabanalysis'], [hodi fs 'src' fs 'thomas' fs 'seismicrates'],[hodi fs 'src' fs 'thomas' fs 'montereason'],...
		[hodi fs 'src' fs 'thomas' fs 'gui'],...
		[hodi fs 'src' fs 'jochen' fs 'plot'], [hodi fs 'src' fs 'jochen' fs 'stressinv'], [hodi fs 'src' fs 'jochen' fs 'auxfun'],...
		[hodi fs 'src' fs 'thomas'], ...
		[hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
		[hodi fs 'src' fs 'thomas' fs 'montereason'], ...
		[hodi fs 'src' fs 'thomas' fs 'etas'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
		[hodi fs 'src' fs 'thomas'], ...
		[hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
		[hodi fs 'src' fs 'thomas' fs 'montereason'], ...
		[hodi fs 'src' fs 'thomas' fs 'etas'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
		[hodi fs 'src' fs 'juerg' fs 'misc'],...
		[hodi fs 'src' fs 'afterrate']);
		
		%variables from ini_zmap (some may be kicked later)
		
		% Set the font size
		%
		fs8 = 10;
		fs10 = 12;
		fs12 = 14;
		fs14 = 16;
		fs16 = 18;
		
		% Marker sizes
		ms6 = 3;
		ms10 = 10;
		ms12 = 12;
		
		% Marker type
		ty ='.';
		ty1 ='+';
		ty2 = 'o';
		ty3 ='x';
		typele = 'dep';
		sel  = 'ca';
		selector = 'ca'; %this was added because sel is a function, and there 
				%was a problem setting this again with eval
		
		
		% Line Thickness
		lth1 = 1.0;
		lth15 = 1.5;
		lth2 = 2.0;
		
		% set up Window size
		%
		% Welcome window
		wex = 80;
		wey = fipo(4)-380;
		welx = 340;
		wely = 300;
		
		% Map window
		%
		winx = 750;
		winy = 650;
		
		% Various setups
		%
		rad = 50.;
		ic = 0;
		ya0 = 0.;
		xa0 = 0.;
		iwl3 = 1.;
		iwl2 = 1.5;
		step = 3;
		ni = 100;
		
		name = [' '];
		strib = [' '];
		stri2 = [];
		ho ='noho';
		ho2 = 'noho';
		infstri = ' Please enter information about the | current dataset here';
		maix = [];
		maiy = [];
		
		
		% Initial Time setting
		
		% Tresh is the radius in km below which blocks 
		% in the zmap's will be plotted 
		% 
		tresh = 50;
		wi = 10 ;   % initial width of crossections
		rotationangle = 10; % initial rotation angle in cross-section window
		fre = 0;
		
		
		% set the background color (c1 = red, c2 = green, c3 = blue)
		% default: light gray 0.9 0.9 0.9 
		c1 = 0.9;
		c2 = 0.9;
		c3 = 0.9;
		
		% Set the Background color for the plot 
		% default \: light yellow 1 1 0.6
		cb1 = 1.0;
		cb2 = 1.0;
		cb3 = 1.0;	
		
		in = 'initf';
		
		% seislap default para,eters
		ldx = 100;
		tlap = 100;
		
		ca = 1;
		vi ='on';
		sha ='fl';
		inb1=1;inb2=1;
		inda = 1;
		ra = 5;
		
		co = 'w';
		par1 = 14;
		minmag = 8;
		
		
		%dummy figure
		figure;
		set(gcf,...  
			'Name','Message Window',...
			'NumberTitle','off', ...
			'MenuBar','none', ...
			'Visible','off', ...
			'Position',[ wex wey welx wely ],...
			'visible','on');

		mess=gcf;
		
		set(mess,'visible','off');
		%set the recursion slightly, to avoid error (specialy with the function ploop2.m
		%set(0,'RecursionLimit',750)
		%-----------------------------------------------------------------
		
		
		%I set plt to 'plo2' this topography is available in the ZmapInBox folder and should 
		%work, it can be changed if another type is wanted and available.
		plt='lo2';
		%use lo2 instead, seems like this is for loading, and it cannot be created first
		
		
		
		%Add the raw data some are needed in some function, coastline may added later
		main = [];
		mainfault = [];
		coastline = [];
		well = [];
		stat = [];
		faults = [];
		
		
		%try building the coastline
		try
			rawcoast=obj.Datastore.getUserData('Coast_PlotArgs');
			coastline(:,1)=rawcoast{1};
			coastline(:,2)=rawcoast{2};
		catch
			disp('no coastline available');
			coastline = [];
		end	
		
		
		obj.PackList=[obj.PackList; {'main';'mainfault';'coastline';'well';'stat';'faults'}];
		
		%variables from zmap.m
		my_dir = hodi;
		
		
		%variables from other functions needed here
		

		% Default value Mcauto
		nBstSample = 100;
		fMccorr = 0;
		fBinning = 0.1;
		bBst_button=0;
		inpr1=1;
		
		
		%Mc estimate		
		dat = []; 

		
		
		%McTime	& b with time 			
		nSampleSize = 500;
		nOverlap = 4;
		nMethod = 1;
		
		if any(strcmp(obj.CalcType,{'btime','McTime'}))
			nBstSample = 200;
		end
		
		nMinNumberevents = 50;
		%fBinning = 0.1;
		nWindowSize = 5;
		%fMcCorr = 0;
		selt= 'ca';
		
		%selt = 'in'; sPar = 'mc'; %mc
		%selt ='in'; sPar = 'b'; %b
		
		
		%b with depth
		BV = [];
		BV3 = [];
		mag = [];
		me = [];
		av2=[];
		fs12 = 10;
		Nmin = 50;
		ni = 150;
		ofac = 5;
		ButtonName='Automatic';
		
		%b with mag
		%BV = [];
		%BV3 = [];
		%mag = [];
		%me = [];
		%av2=[];
		%fs12 = 10;
		
		if strcmp(obj.CalcType,'bmag')
			Nmin = 20;
		end
		
		%update packlist for the additional variables
		obj.PackList=[obj.PackList; {'nBstSample';'fMccorr';'fBinning';'dat';'nSampleSize';'nOverlap';'nMethod';...
					'nMinNumberevents';'nWindowSize';'selt';'BV';'BV3';'mag';'me';'av2';'Nmin';'ni';...
					'bBst_button';'ofac';'ButtonName';'inpr1'}];
		
		%Pack da shit
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
		
	
	end
	
	
	function Gridit(obj,initswitch)
		%disp(obj.ZmapVariables)
		%unpack the variables,
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		
		if initswitch
			%This has to be done just once, hence the switch;
			%update packlist
			obj.PackList=[obj.PackList; {'dx';'dy';'ni';'newgri';'xvect';'yvect';...
					'll';'gx';'gy'}];
			
			
			%get parameter from the datastore
			
			%build grid
			gridpars=obj.Datastore.getUserData('gridPars')
			
			if numel(gridpars.gridSep)<2
				dx=gridpars.gridSep;
				dy=gridpars.gridSep;
			else
				dx=gridpars.gridSep(1);
				dy=gridpars.gridSep(2);
			end	
			
			ra=deg2km(gridpars.rad);
			
			ni=100;
			Nmin = 50;
			
		end
		
		%extracted from CalcGridGUI, delete later what's not needed.
		regionFilter = obj.Filterlist.getByName('Region');
		regionParms = regionFilter.getRegion();
		
		if ~strcmp(regionParms{1},'all')
			theRegion = regionParms{2};
			bBox=theRegion.getBoundingBox();
                else
                	bBox(1)=min(obj.ZmapCatalog(:,1));
                	bBox(2)=max(obj.ZmapCatalog(:,1));
                	bBox(3)=min(obj.ZmapCatalog(:,2));
                	bBox(4)=max(obj.ZmapCatalog(:,2));
                end
                %xRange = (bBox(1):ptSep(1):bBox(2))';
                %yRange = (bBox(3):ptSep(2):bBox(4))';
		vX = [bBox(1); bBox(1); bBox(2); bBox(2)];
		vY = [bBox(4); bBox(3); bBox(3); bBox(4)];
		vX = [vX; vX(1)];
		vY = [vY; vY(1)]; 
		
		%from ex_selectgrid; (it is used in the fullarea mode, the data is filtered anyway)
		%==================
		%Could maybe be optimized, but leave it at the moment as it is
		
		% Create a rectangular grid
		vXVector = [bBox(1):dx:bBox(2)];
		vYVector = [bBox(3):dy:bBox(4)];
		mGrid = zeros((length(vXVector) * length(vYVector)), 2);
		nTotal = 0;
		for i = 1:length(vXVector)
		   for j = 1:length(vYVector)
		     nTotal = nTotal + 1;
		     mGrid(nTotal,:) = [vXVector(i) vYVector(j)];
		   end;
		end;
		  
		% Extract all gridpoints in chosen polygon
		XI=mGrid(:,1);
		YI=mGrid(:,2);
  
		m = length(vX)-1;      %  number of coordinates of polygon
		l = 1:length(XI);
		l = (l*0)';
		vUsedNodes = l;               %  Algorithm to select points inside a closed
		%  polygon based on Analytic Geometry    R.Z. 4/94
		for i = 1:m;
		  
			l= ((vY(i)-YI < 0) & (vY(i+1)-YI >= 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0) | ...
		  		((vY(i)-YI >= 0) & (vY(i+1)-YI < 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0);
		    
		  	if i ~= 1 
		  		vUsedNodes(l) = 1 - vUsedNodes(l);
		  	else
		  		vUsedNodes = l; 
		  	end;         
		    
		end;         
		
		%grid points in polygon
		mGrid = mGrid(vUsedNodes,:);
		
		%======================
		
		%newgri, xvect, yvect, ll
		
		%set the values
		newgri=mGrid;
		xvect=vXVector;
		yvect=vYVector;
		ll=vUsedNodes;
		
		gx = xvect;
		gy = yvect;
		
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
	
	end
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		%and it will be used here.
		import mapseis.calc.Mc_calc.calc_Mc;
		
		
		%unpack again all the variables
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		%Needed Parameterlist
		%dim = [8 2];
		
		switch obj.CalcType
			case 'Mcauto'
				Title = 'Mc Input Parameter';
				Prompt={'Mc Method:', 'inpr1';...
					'Mc bootstraps:','bBst_button';...
					'Number of Bootstraps:','nBstSample';...
					'Mc Correction for Maxc:','fMccorr';
					'Magnitude Bin Size:','fBinning'};
				
				labelList2 =  {'1: Maximum curvature'; ...
					'2: Fixed Mc = minimum magnitude (Mmin)'; ...
					'3: Mc90 (90% probability)'; ...
					'4: Mc95 (95% probability)'; ...
					'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
					'6: EMR-method'; ...
					'7: Mc due b using Shi & Bolt uncertainty'; ...
					'8: Mc due b using bootstrap uncertainty'; ...
					'9: Mc due b Cao-criterion';...
					'10: MBass'}; 
				
				%Mc Method
				Formats(1,1).type='list';
				Formats(1,1).style='popupmenu';
				Formats(1,1).items=labelList2;
				Formats(1,2).type='none';
				Formats(1,1).size = [-1 0];
				Formats(1,2).limits = [0 99];
				
				%bootstrap toggle
				Formats(2,1).type='check';
				
				%bootstrap number
				Formats(2,2).type='edit';
				Formats(2,2).format='integer';
				Formats(2,2).limits = [0 9999];
				
				
				
				%Mc Correction
				Formats(3,1).type='edit';
				Formats(3,1).format='float';
				Formats(3,1).limits = [-99 99];
				Formats(3,1).size = [-1 0];
				Formats(3,2).limits = [0 1];
				Formats(3,2).type='none';
				
				%Mag Binning
				Formats(4,1).type='edit';
				Formats(4,1).format='float';
				Formats(4,1).limits = [-99 99];
				Formats(4,1).size = [-1 0];
				Formats(4,2).limits = [0 1];
				Formats(4,2).type='none';
				
		
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
		
				
				
				%default values
				defval =struct('inpr1',inpr1,...
					'bBst_button',bBst_button,...
					'nBstSample',nBstSample,...
					'fBinning',fBinning,...
					'fMccorr',fMccorr);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				
				%Now the new parameters should be existing format them if needed
				%and save them in the datastore if needed
				
				
				%unpack from the structure
				inpr1=NewParameter.inpr1;
				inb2=inpr1; %just in case
				bBst_button=NewParameter.bBst_button;
				nBstSample=NewParameter.nBstSample;		
				fMccorr=NewParameter.fMccorr;
				fBinning=NewParameter.fBinning;
				
				
			case 'McEstimate'
				%no config needed
				Canceled=0;
			case {'McTime','btime'}
				Title = 'Mc Input Parameter';
				Prompt={'Mc Method:', 'nMethod';...
					'Sample Window Size:','nSampleSize';...
					'Overlap:','nOverlap';...
					'Number of Bootstraps:','nBstSample';...
					'Binning:','fBinning';...
					'Mc Correction: ','fMccorr';...
					'Smooth Plot:', 'nWindowSize';...
					'Minimum Number:', 'nMinNumberevents'};
				
				labelList2 =  {'1: Maximum curvature'; ...
					'2: Fixed Mc = minimum magnitude (Mmin)'; ...
					'3: Mc90 (90% probability)'; ...
					'4: Mc95 (95% probability)'; ...
					'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
					'6: EMR-method'; ...
					'7: Mc due b using Shi & Bolt uncertainty'; ...
					'8: Mc due b using bootstrap uncertainty'; ...
					'9: Mc due b Cao-criterion';
					'10: MBass'}; 
				
				%Mc Method
				Formats(1,1).type='list';
				Formats(1,1).style='popupmenu';
				Formats(1,1).items=labelList2;
				Formats(1,2).type='none';
				Formats(1,1).size = [-1 0];
				Formats(1,2).limits = [0 99];
				
				%Sample Window
				Formats(2,1).type='edit';
				Formats(2,1).format='integer';
				Formats(2,1).limits = [0 9999];
				
				%Overlap
				Formats(2,2).type='edit';
				Formats(2,2).format='integer';
				Formats(2,2).limits = [0 9999];
				
				
				%bootstrap number
				Formats(3,1).type='edit';
				Formats(3,1).format='integer';
				Formats(3,1).limits = [0 9999];
				
				%Binning
				Formats(3,2).type='edit';
				Formats(3,2).format='float';
				Formats(3,2).limits = [0 99];
				
				%Mc Correction
				Formats(4,1).type='edit';
				Formats(4,1).format='float';
				Formats(4,1).limits = [-99 99];
				
				%Smooth plot
				Formats(4,2).type='edit';
				Formats(4,2).format='integer';
				Formats(4,2).limits = [0 9999];
				
				Formats(5,1).type='edit';
				Formats(5,1).format='integer';
				Formats(5,1).limits = [0 9999];
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
		

				%default values
				defval =struct(	'nMethod',nMethod,...
						'nSampleSize',nSampleSize,...
						'nOverlap',nOverlap,...
						'nBstSample',nBstSample,...
						'fMccorr',fMccorr,...
						'fBinning',fBinning,...
						'nWindowSize',nWindowSize,...
						'nMinNumberevents',nMinNumberevents);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				
				%Now the new parameters should be existing format them if needed
				%and save them in the datastore if needed
				
				
				%unpack from the structure
				nMethod=NewParameter.nMethod;
				nSampleSize=NewParameter.nSampleSize;
				nBstSample=NewParameter.nBstSample;
				nOverlap=NewParameter.nOverlap;
				fMccorr=NewParameter.fMccorr;	
				nMinNumberevents=NewParameter.nMinNumberevents;
				fBinning=NewParameter.fBinning;
				nWindowSize=NewParameter.nWindowSize;

			
				
			case 'bdepth'
				Title = 'b with depth input parameters';
				Prompt={'Number of events each window:', 'ni';...
					'Overlap factor:','ofac';...
					'Mc determination?','ButtonNr'};
				
				
				
				
				
				%bootstrap number
				Formats(1,1).type='edit';
				Formats(1,1).format='integer';
				Formats(1,1).limits = [0 9999];
				
				
				%Mc Correction
				Formats(2,1).type='edit';
				Formats(2,1).format='integer';
				Formats(2,1).limits = [0 9999];

				Formats(3,1).type='list';
				Formats(3,1).style='togglebutton';
				Formats(3,1).items={'Automatic','Fixed Mc=Mmin';}
				
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
				
				
				
				%default values
				switch ButtonName,
				 	case 'Automatic'
				 		ButtonNr=1;
				 	case 'Fixed Mc=Mmin'	
				 		ButtonNr=2;
				end		
				defval =struct(	'ni',ni,...
						'ofac',ofac,...
						'ButtonNr',ButtonNr);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				
				%Now the new parameters should be existing format them if needed
				%and save them in the datastore if needed
				
				
				%unpack from the structure
				ni=NewParameter.ni;
				ofac=NewParameter.ofac;
				
				if NewParameter.ButtonNr==1;
					ButtonName='Automatic';
				elseif NewParameter.ButtonNr==2;
					ButtonName='Fixed Mc=Mmin';
				end
				
			case 'bmag'
				%no parameters to set
				Canceled=0;
				
			
			
		end	
			
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
		
	end
	
	
	
	
	function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		
		
		import mapseis.calc.Mc_calc.*;
		
		%unpack again all the variables
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		newt2=a;
		%ra=rad;
		%now the code from bvalgrid
		%all used functions and procedures should be in the ZmapinBox folder and existing
		
		%========
		
		

		
		
		switch obj.CalcType
			case 'Mcauto'
			    maxmag = ceil(10*max(newt2(:,6)))/10;
			    mima = min(newt2(:,6));
			    if mima > 0 ; mima = 0 ; end;
			
			
			    %%
			    %
			    % bval contains the number of events in each bin
			    % bvalsum is the cum. sum in each bin
			    % bval2 is number events in each bin, in reverse order
			    % bvalsum3 is reverse order cum. sum.
			    % xt3 is the step in magnitude for the bins == .1
			    %
			    %%
			
			    [bval,xt2] = hist(newt2(:,6),(mima:fBinning:maxmag));
			    bvalsum = cumsum(bval); % N for M <=
			    bval2 = bval(length(bval):-1:1);
			    bvalsum3 = cumsum(bval(length(bval):-1:1));    % N for M >= (counted backwards)
			    xt3 = (maxmag:-fBinning:mima);
			
			    backg_ab = log10(bvalsum3);
			
			
			    %%
			    % Estimate the b value
			    %
			    % calculates max likelihood b value(bvml) && WLS(bvls)
			    %
			    %%
			
			    %% SET DEFAULTS TO BE ADDED INTERACTIVLY LATER
			    %Nmin = 10;
			
			    bvs=newt2;
			    b=newt2;
			
			    %% enough events??
			    if length(bvs(:,6)) >= Nmin
				% Added to obtain goodness-of-fit to powerlaw value
				mcperc_ca3_org;
				%[magco prf Mc90 Mc95]=mcperc_ca3(newt2);
				
				[fMc] = calc_Mc(bvs, inpr1, fBinning, fMccorr);
				l = bvs(:,6) >= fMc-(fBinning/2);
				if length(bvs(l,:)) >= Nmin
				    [fMeanMag, fBValue, fStd_B, fAValue] =  calc_bmemag(bvs(l,:), fBinning);
				else
				    fMc = nan; fBValue = nan; fStd_B = nan; fAValue= nan;
				end;
				% Set standard deviation of a-value to nan;
				fStd_A= nan; fStd_Mc = nan;
			
				% Bootstrap uncertainties
				if bBst_button == 1
				    % Check Mc from original catalog
				    l = bvs(:,6) >= fMc-(fBinning/2);
				    if length(bvs(l,:)) >= Nmin
					[fMc, fStd_Mc, fBValue, fStd_B, fAValue, fStd_A, vMc, mBvalue] = calc_McBboot(bvs, fBinning, nBstSample, inpr1, Nmin, fMccorr);
				    else
					fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A= nan;
				    end;
				else
				    % Set standard deviation of a-value to nan;
				    fStd_A= nan; fStd_Mc = nan;
				end; % of bBst_button
			    else
				fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A = nan;
				fStdDevB = nan;
				fStdDevMc = nan;
			    end;% of if length(bvs) >= Nmin
			    %%
			    % calculate limits of line to plot for b value line
			    %%
			    % For ZMAP
			    magco = fMc;
			    index_low=find(xt3 < magco+.05 & xt3 > magco-.05);
			
			    mag_hi = xt3(1);
			    index_hi = 1;
			    mz = xt3 <= mag_hi & xt3 >= magco-.0001;
			    mag_zone=xt3(mz);
			
			    y = backg_ab(mz);
			
			   
			
			    %%
			    % Set to correct method, maximum like or least squares
			    %%
			    if bBst_button == 0
				sol_type = 'Maximum Likelihood Solution';
				bw=fBValue;%bvml;
				aw=fAValue;%avml;
				ew=fStd_B;%stanml;
				%     end
			    else
				sol_type = 'Maximum Likelihood Estimate, Uncertainties by bootstrapping';
				bw=fBValue;
				aw=fAValue;
				ew=fStd_B;
			    end; %bBst_button
			    %%
			    % create and draw a line corresponding to the b value
			    %%
			    
			    %p = [ -1*bw aw];
			    p = [ -1*bw aw];
			    %[p,S] = polyfit(mag_zone,y,1);
			    f = polyval(p,mag_zone);
			    f = 10.^f;
			    
			    std_backg = ew;      % standard deviation of fit
			
			    %%
			    % Error Bar Calculation -- call to pdf_calc.m
			    %%
			
			   
			
			
			    p=-p(1,1);
			    p=fix(100*p)/100;
			    tt1=num2str(bw,3);
			    tt2=num2str(std_backg,1);
			    %not used anymore
			    %tt4=num2str(bv,3);
			    %tt5=num2str(si,2);
			
			
			    tmc=num2str(magco,2);
			
			    rect=[0 0 1 1];
			    h2=axes('position',rect);
			    set(h2,'visible','off');
			
			    a0 = aw-log10(max(b(:,3))-min(b(:,3)));
			
			    %Change it for each calc
			    obj.PackList=[obj.PackList; {'maxmag';'mima';'bval';'xt2';'bvalsum';'bval2';'bvalsum3';...
					'xt3';'backg_ab';'l';'index_low';'mag_zone';'tt1';'tt2';'f';'b';'sol_type';...
					'tmc';'fBValue';'fStd_B';'aw';'a0';'newt2';'fStd_Mc'}];
			    
				
			case 'McEstimate'
			    	import mapseis.calc.synth_catalogs.*;
			    	
				[bv magco0 stan av ] =  bvalca3(newt2,inb1,inb2);


				dat = []; 
				
				for i = magco0 - 0.9:0.1:magco0+1.5
				    i
				   l = newt2(:,6) >= (i - 0.0499); 
				   nu = length(newt2(l,6));
				   %[bv magco stan av] =  bvalca3(newt2(l,:),2,2); 
				   [mw bv2 stan2 av] =  bmemag(newt2(l,:));
				   %synthb_aut
				   resCatalog = synthb_aut(newt2,bv2,i,l);
				   %res0 = res; 
				   res0=resCatalog;
				   xt2=[];
				  % bv = bv + stan ; synthb_aut; res1 = res; 
				  % bv = bv - 2*stan ; synthb_aut; res2 = res;
				
				   nc = 10.^(av - bv2*(i+0.05)) ;
				   nc1 = 10.^(av - (bv2-stan/2)*(i+0.05)) ;
				   nc2 = 10.^(av - (bv2+stan/2)*(i+0.05)) ;
				
				   dat = [ dat ; i nc nu nu/nc nu/nc1 nu/nc2 res0  ];
				   %display(['Completeness Mc: ' num2str(i) ';  rati = ' num2str(nu/nc)]); 
				
				end
				
				
				disp(min(dat(:,7)))
				j =  min(find(dat(:,7) < 10 )); 
				if isempty(j)
					Mc90 = nan; 
				else; 
				   Mc90 = dat(j,1); 
				end
				   
				j =  min(find(dat(:,7) < 5 )); 
				
				if isempty(j) 
					Mc95 = nan; 
				else; 
				   Mc95 = dat(j,1); 
				end
				
				display(['Completeness Mc at 90% confidence: ' num2str(Mc90) ]); 
				display(['Completeness Mc at 95% confidence: ' num2str(Mc95) ]); 
				
				
				obj.PackList=[obj.PackList; {'dat';'Mc90';'Mc95';'xt2';'bv';'magco0';'stan';...
					'av';'newt2'}];



				
			
			case 'McTime'
			
				if (obj.ParallelMode)
					matlabpool open;
			
				end
			
				% Set minimum number to number of moving window
				%nMinNumberevents = nSampleSize;
				% Calulate time series
				[mResult] = calc_McBwtime(newt2, nSampleSize, nOverlap, nMethod, nBstSample, nMinNumberevents, fBinning,fMccorr,obj.ParallelMode);
				    
				% Plot Mc time series
				%   if sPar == 'mc'
				
				mMc = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,2));
				mMc(1:nWindowSize,1)=mResult(1:nWindowSize,2);
				mMcstd1 = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,3));
				mMcstd1(1:nWindowSize,1)=mResult(1:nWindowSize,3);
					
				
				obj.PackList=[obj.PackList; {'mResult';'mMc';'mMcstd1';'newt2'}];
				
				
				
				
			case 'bdepth'
				
				[s,is] = sort(newt2(:,7));
				newt1 = newt2(is(:,1),:) ;
				watchon;
				allcount = 0.;
				
				
				itotal=numel(1:ni/ofac:length(newt1)-ni);
				
				if ~obj.ParallelMode
					wai = waitbar(0,' Please Wait ...  ');
					set(wai,'NumberTitle','off','Name','b-value with depth - percent done');;
					drawnow
					
					for t = 1:ni/ofac:length(newt1)-ni
					    allcount=allcount+1;
					    % calculate b-value based an weighted LS
					    b = newt1(t:t+ni,:);   
					    
					    switch ButtonName,
					    case 'Automatic'
						
					    	[magco prf Mc90 Mc95] = mcperc_ca3(b);  
						
						
						if isnan(Mc95) == 0 ; 
						    magco = Mc95; 
						elseif isnan(Mc90) == 0 ; 
						    magco = Mc90; 
						else 
						    [bv magco stan av me mer me2 pr] =  bvalca3(b,1,1); 
						end   
					    case 'Fixed Mc=Mmin'
						magco = min(newt1(:,6))
					    end
					    
					    l = b(:,6) >= magco-0.05; 
					    if length(b(l,:)) >= Nmin
						%[bv magco0 stan av me mer me2 pr] =  bvalca3(b(l,:),2,2); 
						[mea bv stan av] =  bmemag(b(l,:));
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV = [BV ; bv newt1(t,7) ; bv newt1(t+ni,7) ; inf inf];
					    BV3 = [BV3 ; bv newt1(t+round(ni/2),7) stan ];
					    %mag = [ mag ; av newt1(t+round(ni/2),7)];
					    
					    % calculate b-value based on maximum likelihood
					    %av2 = [ av2 ;   av  newt1(t+round(ni/2),7) stan bv];
					    
					    waitbar(allcount/itotal);
					end
					close(wai);
				
				else
					%Go Parallel
					NumberArray=1:ni/ofac:length(newt1)-ni;
					
					%if (obj.ParallelMode)
					%	matlabpool open;
					%end
					
					for i = 1:numel(NumberArray)
					    
					    t=NumberArray(i);
					    
					    % calculate b-value based an weighted LS
					    b = newt1(t:t+ni,:);   
					    
					    switch ButtonName,
					    case 'Automatic'
						mcperc_ca3;  
						if isnan(Mc95) == 0 ; 
						    magco = Mc95; 
						elseif isnan(Mc90) == 0 ; 
						    magco = Mc90; 
						else 
						    [bv magco stan av me mer me2 pr] =  bvalca3(b,1,1); 
						end   
					    case 'Fixed Mc=Mmin'
						magco = min(newt1(:,6))
					    end
					    
					    l = b(:,6) >= magco-0.05; 
					    if length(b(l,:)) >= Nmin
						%[bv magco0 stan av me mer me2 pr] =  bvalca3(b(l,:),2,2); 
						[mea bv stan av] =  bmemag(b(l,:));
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV(i,:) = [bv newt1(t,7) ; bv newt1(t+ni,7) ; inf inf];
					    BV3(i,:) = [bv newt1(t+round(ni/2),7) stan ];
					    %mag = [ mag ; av newt1(t+round(ni/2),7)];
					    
					    % calculate b-value based on maximum likelihood
					    %av2 = [ av2 ;   av  newt1(t+round(ni/2),7) stan bv];
					    
					    
					end
				
				
				end
				
				
				watchoff
				
				obj.PackList=[obj.PackList; {'newt2'}];
				
			
			
				
			case 'btime'
				% Set minimum number to number of moving window
				%nMinNumberevents = nSampleSize;
				% Calulate time series
				
				if (obj.ParallelMode)
					matlabpool open;
					
				end
				[mResult] = calc_McBwtime(newt2, nSampleSize, nOverlap, nMethod, nBstSample, nMinNumberevents, fBinning,fMccorr,obj.ParallelMode);
				
				mB = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,4));
				mB(1:nWindowSize,1)=mResult(1:nWindowSize,4);
				mBstd1 = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,3));
				mBstd1(1:nWindowSize,1)=mResult(1:nWindowSize,3);
				
				obj.PackList=[obj.PackList; {'mResult';'mB';'mBstd1';'newt2'}];
			
			
				
				
			case 'bmag'
				
				[s,is] = sort(newt2(:,6));
				newt1 = newt2(is(:,1),:) ;
				watchon;
				itotal=numel(min(newt1(:,6)):0.1:max(newt1(:,6)));
				allcount=0;
				
				
				if ~obj.ParallelMode
					wai = waitbar(0,' Please Wait ...  ');
					set(wai,'NumberTitle','off','Name','b-value with Magnitude - percent done');;
					drawnow
					
					for t = min(newt1(:,6)):0.1:max(newt1(:,6));
					    allcount=allcount+1;
					    
					    % calculate b-value based an weighted LS
					    l = newt1(:,6) >= t -0.05;
					    b = newt1(l,:);   
					    
					    if length(b(:,1)) >= Nmin
						[bv magco stan av me mer me2 pr] =  bvalca3(b,2,2); 
						[mea bv stan2 av] =  bmemag(b);
					
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV3 = [BV3 ; bv t stan ];
					    
					    
					     waitbar(allcount/itotal);
					end
					
					close(wai);
				else
					%Go Parallel
					NumberArray=min(newt1(:,6)):0.1:max(newt1(:,6));
					
					%if (obj.ParallelMode)
					%	matlabpool open;
					%end
					
					for i = 1:numel(NumberArray)
					    t=NumberArray(i);
					    
					    % calculate b-value based an weighted LS
					    l = newt1(:,6) >= t -0.05;
					    b = newt1(l,:);   
					    
					    if length(b(:,1)) >= Nmin
						[bv magco stan av me mer me2 pr] =  bvalca3(b,2,2); 
						[mea bv stan2 av] =  bmemag(b);
					
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV3(i,:) = [bv t stan ];
					    
					    
					    
					end
					
				
				end
				
				watchoff
				
				
				obj.PackList=[obj.PackList; {'newt2'}];
			
			
				
		end
		
		
		
		%Change it for each calc
		%obj.PackList=[obj.PackList; {'normlap2';'mMc';'mStdMc';'mBvalue';'mStdB';'mAvalue';'mStdA';...
		%			'Prmap';'ro';'mStdDevB';'fStdDevB';'mStdDevMc';'mNumEq';'kll';...
		%			're3';'old';'t0b';'teb';'newt2'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
			
		end
		    
		if (obj.ParallelMode)
			try
				matlabpool close;
			end
		end

		

		
		
		
		
	end
	
	
	
	function plot_bval(obj)
		%this is kind of a wrapper for view_bva
		
		%unpack
		%unpack the variables,
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		
		switch obj.CalcType
			case 'Mcauto'
				 [existFlag,figNumber]=figflag('Frequency-magnitude distribution',1);
				 if existFlag
				 	% figure(bfig);
					bfig = figNumber;
				 else
					bfig=figure(...                  %build figure for plot
					    'Units','normalized','NumberTitle','off',...
					    'Name','Frequency-magnitude distribution',...
					    'MenuBar','none',...
					    'visible','off',...
					    'pos',[ 0.300  0.3 0.4 0.6]);
				
					ho = 'noho';
					makebut2
					matdraw
					
					
				 	optio = uimenu('Label','ZTools ');
					uimenu(optio,'Label','Estimate recurrence time/probability','callback',@(s,e) obj.universalCalc('plorem'));
					uimenu(optio,'Label','Manual fit of b-value','callback',@(s,e) obj.universalCalc('bfitnew(newcat)'));
					uimenu(optio,'Label','Plot time series','callback',@(s,e) obj.universalCalc('newcat = newt2; timeplot'));
					uimenu(optio,'Label','Do not show discrete curve','callback',@(s,e) obj.universalCalc('delete(pl1)'));
					uimenu(optio,'Label','Save values to file','callback',@(s,e) obj.universalCalc(calSave9));
				    end; % existflag
				
				    calSave9 =...
					[ 'welcome(''Save Data'',''  '');think;',...
					'[file1,path1] = uiputfile([ hodi fs ''out'' fs ''*.dat''], ''Filename ? '');',...
					's=[xt3'' bvalsum3'' ];',...
					'fid = fopen([path1 file1],''w'') ;',...
					'fprintf(fid,''%6.2f  %6.2f\n'',s'');',...
					'fclose(fid) ;',...
					'done';];
				    if ho(1:2) == 'ho'
					axes(cua)
					disp('hold on')
					hold on
				    else
					figure(bfig);delete(gca);delete(gca); delete(gca); delete(gca)
					rect = [0.22,  0.3, 0.65, 0.6];           % plot Freq-Mag curves
					axes('position',rect);
				    end; % ho
				
				    %%
				    % plot the cum. sum in each bin  %%
				    %%
				
				    pl =semilogy(xt3,bvalsum3,'sb');
				    set(pl,'LineWidth',[1.0],'MarkerSize',[6],...
					'MarkerFaceColor','w','MarkerEdgeColor','k');
				    hold on
				    pl1 =semilogy(xt3,bval2,'^b');
				    set(pl1,'LineWidth',[1.0],'MarkerSize',[4],...
					'MarkerFaceColor',[0.7 0.7 .7],'MarkerEdgeColor','k');
				
				    %%
				    % CALCULATE the diff in cum sum from the previous biin
				    %%
				
				
				    xlabel('Magnitude','FontWeight','bold','FontSize',fs12)
				    ylabel('Cumulative Number','FontWeight','bold','FontSize',fs12)
				    set(gca,'visible','on','FontSize',fs12,'FontWeight','normal',...
					'FontWeight','bold','LineWidth',[1.0],'TickDir','out','Ticklength',[0.02 0.02],...
					'Box','on','Tag','cufi','color','w')
				
				    cua = gca;
					
				     %%
				    % PLOTS an 'x' in the point of Mc
				    %%
				
				    te = semilogy(xt3(index_low),bvalsum3(index_low)*1.5,'vb');
				    set(te,'LineWidth',[1.0],'MarkerSize',7)
				
				    te = text(xt3(index_low)+0.2,bvalsum3(index_low)*1.5,'Mc');
				    set(te,'FontWeight','bold','FontSize',fs10,'Color','b')
				    
				    
				    figure(bfig)
				    
				    hold on
				    ttm= semilogy(mag_zone,f,'r');                         % plot linear fit to backg
				    set(ttm,'LineWidth',[1.])
				    
				    %pdf_calc;
				    set(gca,'XLim',[min(b(:,6))-0.5  max(b(:,6))+0.5])
				    set(gca,'YLim',[0.9 length(b(:,3)+30)*2.5]);
				    
				    if ho(1:2) == 'ho'
					set(pl,'LineWidth',[1.0],'MarkerSize',[8],...
					    'MarkerFaceColor','k','MarkerEdgeColor','k','Marker','^');
					%set(pl3,'LineWidth',[1.0],'MarkerSize',[6],...
					%'MarkerFaceColor','c','MarkerEdgeColor','m','Marker','s');
					%   txt1=text(.16, .06,['b-value (w LS, M  >= ', num2str(M1b(1)) '): ',tt1, ' +/- ', tt2 ', a-value = ' , num2str(aw) ]);
					set(txt1,'FontWeight','normal','FontSize',fs10,'Color','r')
				    else
					if bBst_button == 0
					    txt1=text(.16, .11,['b-value = ',tt1,' +/- ',tt2,',  a value = ',num2str(aw,3) ',  a value (annual) = ', num2str(a0,3)],'FontSize',fs10);
					    set(txt1,'FontWeight','normal')
					    set(gcf,'PaperPosition',[0.5 0.5 4.0 5.5])
					    text(.16, .14,sol_type,'FontSize',fs10 );
					    text(.16, .08,['Magnitude of Completeness = ',tmc],'FontSize',fs10);
					else
					    txt1=text(.16, .11,['b-value = ',num2str(round(100*fBValue)/100),' +/- ',num2str(round(100*fStd_B)/100),',  a value = ',num2str(aw,3) ',  a value (annual) = ', num2str(a0,3)],'FontSize',fs10);
					    set(txt1,'FontWeight','normal')
					    set(gcf,'PaperPosition',[0.5 0.5 4.0 5.5])
					    text(.16, .14,sol_type,'FontSize',fs10 );
					    text(.16, .08,['Magnitude of Completeness = ',tmc ' +/- ', num2str(round(100*fStd_Mc)/100)],'FontSize',fs10);
					end;
				    end; % ho
			
				    set(gcf,'visible','on','Color','w');
				    %welcome('  ','Done')
				    %done
				
				    if ho(1:2) == 'ho'
					% calculate the probability that the two distributins are differnt
					%l = newt2(:,6) >=  M1b(1);
					b2 = str2num(tt1); n2 = M1b(2);
					n = n1+n2;
					da = -2*n*log(n) + 2*n1*log(n1+n2*b1/b2) + 2*n2*log(n1*b2/b1+n2) -2;
					pr = exp(-da/2-2);
					disp(['Probability: ',  num2str(pr)]);
					txt1=text(.60, .85,['p=  ', num2str(pr,2)],'Units','normalized');
					set(txt1,'FontWeight','normal','FontSize',fs10)
					txt1=text(.60, .80,[ 'n1: ' num2str(n1) ', n2: '  num2str(n2) ', b1: ' num2str(b1)  ', b2: ' num2str(b2)]);
					set(txt1,'FontSize',[8],'Units','normalized')
				    end
				    
				    %set packlist
				    obj.PackList=[obj.PackList; {'cua';'bfig';'pl1';'calSave9'}];
				    
				
				    
			case 'McEstimate'
				fi = findobj('tag','mcfig'); 
				if isempty(fi) == 1
				   figure('pos',[300 300 600 300],...
				      'tag','mcfig');
				else
				   figure(fi); delete(gca);delete(gca);
				end
				
				
				axes('pos',[0.15 0.2 0.7 0.65])
				plot(dat(:,1),dat(:,7),'k','LineWidth',[1.])
				hold on
				pl = plot(dat(:,1),dat(:,7),'b^')
				set(pl,'LineWidth',[1.0],'MarkerSize',[8],...
				   'MarkerFaceColor','y','MarkerEdgeColor','b');
				
				%errorbar(dat(:,1),dat(:,7),abs(dat(:,7) - dat(:,8)),abs(dat(:,7) - dat(:,9)))
				grid
				
				set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
				    'TickDir','out','LineWidth',[1.0],...
				    'Box','on')
				 xlabel('Magnitude')
				 ylabel('Residual in %')
				 title('Goodness of FMD fit to power law');
				 te = text(0.6,0.8,['Mc at 90% confidence: ' num2str(Mc90) ],...
				    'Units','normalized','FontWeight','bold'); 
				 te = text(0.6,0.9,['Mc at 95% confidence: ' num2str(Mc95) ],...
				    'Units','normalized','FontWeight','bold'); 

				obj.PackList=[obj.PackList; {'fi'}];

				
				
			
			case 'McTime'
				figure('tag','Mc time series', 'visible','on');
				
				
				plot(mResult(:,1),mMc,'-','Linewidth',2,'Color',[0.2 0.2 0.2]);
				hold on;
				plot(mResult(:,1),mMc-mMcstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				plot(mResult(:,1),mMc+mMcstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				ylabel('Mc','Fontweight','bold','Fontsize',12)
				set(gca,'tag','smooth_btime_n1000')
				%set(gca,'XTickLabel',[])
				xlim([(min(mResult(:,1))) (max(mResult(:,1)))])
				ylim([floor(min(mMc(:,1))) ceil(max(mMc(:,1)))])
				xlabel('Time / [dec. year]','Fontweight','bold','Fontsize',12)% ylim([0.85 1.1])
				l1=legend('Mc','\delta Mc');
				%l1=legend('EMR','Std(EMR)','MaxM');
				set(l1,'Fontweight','bold')
				set(gca,'Fontweight','bold','Fontsize',10,'Linewidth',2,'Tickdir','out')
				

				
			case 'bdepth'
				% Find out of figure already exists
				%
				figNumber=findobj('Name','b-value with depth');
				%[existFlag,figNumber]=figflag('b-value with depth',1);
				newdepWindowFlag=isempty(figNumber);
				bdep= figNumber;
				
				% Set up the Cumulative Number window
				
				if newdepWindowFlag,
				    bdep = figure( ...
					'Name','b-value with depth',...
					'NumberTitle','off', ...
					'MenuBar','none', ...
					'NextPlot','add', ...
					'backingstore','on',...
					'Visible','on', ...
					'Position',[ 150 150 winx-50 winy-20]);
				    
				    makebut2
				    uicontrol('BackGroundColor','y','Units','normal',...
					'Position',[.0 .85 .08 .06],'String','Info ',...
					'callback','infoz(1)');
				    
				    matdraw
				end
				
				figure(bdep)
				delete(gca)
				delete(gca)
				delete(gca)
				delete(gca)
				hold off
				
				axis off
				hold on
				orient tall
				%rect = [ 0.15 0.65 0.7 0.25];
				rect = [ 0.25 0.15 0.5 0.75];
				axes('position',rect)
				ple = errorbar(BV3(:,2),BV3(:,1),BV3(:,3),BV3(:,3),'k')
				set(ple(1),'color',[0.5 0.5 0.5]);
				
				hold on
				pl = plot(BV(:,2),BV(:,1),'color',[0.5 0.5 0.5]);
				
				pl = plot(BV3(:,2),BV3(:,1),'sk')
				
				set(pl,'LineWidth',[1.0],'MarkerSize',[4],...
				    'MarkerFaceColor','w','MarkerEdgeColor','k','Marker','s');
				
				set(gca,'box','on',...
				    'DrawMode','fast','TickDir','out','FontWeight',...
				    'bold','FontSize',fs12,'Linewidth',[1.],'Ticklength',[ 0.02 0.02])
				
				bax = gca;
				strib = [name ', ni = ' num2str(ni), ', Mmin = ' num2str(min(newt2(:,6))) ];
				ylabel('b-value')
				xlabel('Depth [km]')
				title2(strib,'FontWeight','bold',...
				    'FontSize',fs12,...
				    'Color','k')
				
				xl = get(gca,'Xlim');
				view([90 90]),

				
				
			case 'btime'
				figure('tag','b-value time series', 'visible','on')
				
				
				
				hp1=plot(mResult(:,1),mB,'-','Linewidth',2,'Color',[0.2 0.2 0.2]);
				hold on;
				hp2=plot(mResult(:,1),mB-mBstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				plot(mResult(:,1),mB+mBstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				ylabel('b-value','Fontweight','bold','Fontsize',12)
				xlim([(min(mResult(:,1))) (max(mResult(:,1)))])
				ylim([floor(min(mB(:,1))) ceil(max(mB(:,1)))])
				xlabel('Time / [dec. year]','Fontweight','bold','Fontsize',12)
				l1=legend([hp1 hp2],'b-value','\delta b');
				set(l1,'Fontweight','bold')
				set(gca,'Fontweight','bold','Fontsize',10,'Linewidth',2,'Tickdir','out')
				
				
				
			case 'bmag'
				% Find out of figure already exists
				%
				%[existFlag,figNumber]=figflag('b-value with magnitude',1);
				figNumber=findobj('Name','b-value with magnitude');
				newdepWindowFlag=isempty(figNumber);
				bdep= figNumber;
				
				
				% Set up the Cumulative Number window
				
				if newdepWindowFlag,
				    bdep = figure( ...
					'Name','b-value with magnitude',...
					'NumberTitle','off', ...
					'MenuBar','none', ...
					'NextPlot','add', ...
					'backingstore','on',...
					'Visible','on', ...
					'Position',[ 150 150 600 500]);
				    
				    makebut2
				    
				    matdraw
				end
				
				
				
				figure(bdep)
				delete(gca)
				delete(gca)
				delete(gca)
				delete(gca)
				hold off
				
				axis off
				hold on
				orient tall
				%rect = [ 0.15 0.65 0.7 0.25];
				rect = [ 0.15 0.15 0.7 0.7];
				axes('position',rect)
				ple = errorbar(BV3(:,2),BV3(:,1),BV3(:,3),BV3(:,3),'k')
				set(ple(1),'color',[0.5 0.5 0.5]);
				
				hold on
				
				pl = plot(BV3(:,2),BV3(:,1),'sk')
				
				set(pl,'LineWidth',[1.0],'MarkerSize',[4],...
				    'MarkerFaceColor','w','MarkerEdgeColor','k','Marker','s');
				
				set(gca,'box','on',...
				    'DrawMode','fast','TickDir','out','FontWeight',...
				    'bold','FontSize',fs12,'Linewidth',[1.],'Ticklength',[ 0.02 0.02])
				
				bax = gca;
				strib = [name ', ni = ' num2str(ni), ', Mmin = ' num2str(min(newt2(:,6))) ];
				ylabel('b-value')
				xlabel('Magnitude')
				title(strib,'FontWeight','bold',...
				    'FontSize',fs12,...
				    'Color','k')
				
				xl = get(gca,'Xlim');

		end
		
		%Add Data Menu
		options = uimenu('Label',' Data ');
		uimenu('Parent',options,'Label','Calculate again ',...
			'Callback',@(s,e) obj.CalcAgain);
		uimenu('Parent',options,'Label','Write to datastore ',...
			'Callback',@(s,e) obj.saveCalc);
		uimenu('Parent',options,'Label','export Result (.mat)','Separator','on',... 
			'Callback',@(s,e) obj.exportResult('matlab'));
		uimenu('Parent',options,'Label','export Result (ascii)',... 
			'Callback',@(s,e) obj.exportResult('ascii'));
		uimenu('Parent',options,'Label','export Result (multiple files)',... 
			'Callback',@(s,e) obj.exportResult('ascii_trio'));
		uimenu('Parent',options,'Label','export Full Result',... 
			'Callback',@(s,e) obj.exportResult('full_result'));	
		
		
		
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
	
	end
	
	function plot_modify(obj,bright,interswitch,circleGrid,overlay)
		%modifys the graphic
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		if ~isempty(bright)
			axes(hzma);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			obj.PackList=[obj.PackList; {'sha'}];
			switch interswitch
				case 'flat'
					axes(hzma);
					shading flat;
					sha='fl';
				case 'interp'
					axes(hzma);
					shading interp;
					sha='in';
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		if ~isempty(overlay)
			hold on;
			overlay;
			
		end
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end
	end
	
	
	function adju(obj,asel, whichplot)
		%function version of adju
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;


		prompt={'Enter the minimum magnitude cut-off','Enter the maximum radius cut-off:','Enter the minimum goodness of fit percatge'};
		def={'nan','nan','nan'};
		dlgTitle='Input Map subselection Criteria';
		lineNo=1;
		answer=inputdlg(prompt,dlgTitle,lineNo,def);
		re4 = re3;
		l = answer{1,1}; Mmin = str2num(l) ;
		l = answer{2,1}; tresh = str2num(l) ;
		l = answer{3,1}; minpe = str2num(l) ;
		
		
		obj.PackList=[obj.PackList; {'re4';'l';'asel'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

		%back to plot
		obj.plot_bval(whichplot);
		
	end
	
	function universalCalc(obj,commandstring,newvariables)
		%An experiment, the function will open all variables and eval
		%the string and save variables again
		%newvariables can be used to add variables to the packlist
		%newvariables has to be a a cell array with strings if used.
		
		if nargin<3
			newvariables=[];
		end
		
		%disp(obj.ZmapVariables);
		%unpack
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		eval(commandstring);
		
		if ~isempty(newvariables)
			obj.PackList=[obj.PackList; newvariables];
		end	
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end

	end	
	
	function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		
		
		%get parameters
		obj.openCalcParamWindow;
		
		if obj.StartCalc
			
			figNr=findobj('Name','b-value-map');
			
			if ~isempty(figNr)
				close(figNr);
			end
		
			%refresh catalogs
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%Build the grid with new parameters if needed
			%obj.Gridit(false);
			
			%Calculation
			obj.doTheCalcThing;
			
			%Plotting
			obj.plot_bval;
		end
	
	
	end
	
	
	function saveCalc(obj)
		%This will save the calculation in the datastore and ask to open it next time
		%The filterlist will be saved within the datastore with this and reopened next time
		%when the calculation is loaded. It has to be done in this way or else nobody knows 
		%what filtersetting was used in the calculation.
		
		%Ask for a name
		gParams = inputdlg('Calculation Name','Name of the Calculation',1,{obj.CalcName});
		if ~isempty(gParams)
			obj.CalcName=gParams{1};
			
			%Maybe the Filterlist needs to be cloned, but for now try without
			obj.Filterlist.PackIt;
			
			CalcObject={obj.CalcName,obj.ZmapVariables,obj.Filterlist};
			
			%try getting the CalcObject from the datastore
			
			try 
				Calculations=obj.Datastore.getUserData(obj.savePos);
				
			catch
				Calculations={};
			end
			
			if ~isempty(Calculations)
				%try finding the calc
					founded=strcmp(Calculations(:,1),obj.CalcName);
					if sum(founded)>0
						Calculations(founded,:)=CalcObject;
					else
						Calculations(end+1,:)=CalcObject;
					end	
			else
				Calculations(1,:)=CalcObject;
			end	
			
			obj.Datastore.setUserData(obj.savePos,Calculations);
			
		end
		
		
		
		%write to the Commander (always do this even if canceled, this will update
		%the buffers)
		obj.Commander.pushWithKey(obj.Keys.ShownDataID,obj.Datastore);
		obj.Commander.switcher('Cata');
		
	end
	
	
	
	function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne';...
			'Overwrite FilterList?','OverWriteFilter'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%Overwrite
		Formats(2,2).type='list';
		Formats(2,2).style='togglebutton';
		Formats(2,2).items={'Yes','No'};
		Formats(2,2).size = [-1 0];
		
		Formats(2,3).limits = [0 1];
		Formats(2,3).type='none';
		Formats(2,1).limits = [0 -1];
		Formats(2,1).type='none';
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1,...
				'OverWriteFilter',2);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		if obj.StartCalc
			TheCalc=oldCalcs(NewParameter.WhichOne,:);
			
			%Overwrite Filter 
			if NewParameter.OverWriteFilter==1
				Keys=obj.Commander.getMarkedKey;
				FilterKey=Keys.ShownFilterID;
				
				obj.Commander.pushWithKey(TheCalc{3},FilterKey);
				obj.Commander.switcher('Both');
				
			else %Create new filter
				newdata.Filterlist=TheCalc{3};
				obj.Commander.pushIt(newdata);
				obj.Commander.switcher('Both');
			end
			
			%Now set the data
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			obj.ZmapVariables=TheCalc{2};
			obj.PackList=fields(obj.ZmapVariables);
			
			obj.CalcName=TheCalc{1};
		end
		
	end
	
	
	
        function exportResult(obj,FileType,Filename) 
          	%saves the currently plotted result into a mat or ascii file
          	
          	if nargin<3
          		Filename=[];
          	end
          	
          	if isempty(obj.ZmapVariables)
          		warning('nothing calculated')
          	end	
          
          	%get file name
          	switch FileType
          		case {'matlab','full_result'}
          			%get filename
          			if isempty(Filename)
					[fName,pName] = uiputfile('*.mat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;			
				end
				saveConf='-mat';
			case {'ascii','ascii_trio'}
				if isempty(Filename)
					[fName,pName] = uiputfile('*.dat','Enter save file name...');
				else
					[pathstr,name,ext] = fileparts(Filename)
					fName=[name,ext];
					pName=pathstr;
				end
				saveConf='-ascii';
		end
		
          	%get data
          	%case with full result
          	if strcmp(FileType,'full_result')
          		ZmapVariables=obj.ZmapVariables;
          		save(fullfile(pName,fName),'ZmapVariables',saveConf);
          		return
          		
          	end
          	
          	switch obj.CalcType
          		case 'Mcauto'
          			XVector=obj.ZmapVariables.xt3;
         			bvalSum=obj.ZmapVariables.bvalsum3;
         			bval=obj.ZmapVariables.bval2;
          			XLine=obj.ZmapVariables.mag_zone;
          			YLine=obj.ZmapVariables.f;
          			
          			
          			if strcmp(FileType,'ascii_trio')
          				save(fullfile(pName,['XVector_',fName]),'XVector',saveConf);
          				save(fullfile(pName,['bvalSum_',fName]),'bvalSum',saveConf);
          				save(fullfile(pName,['bval_',fName]),'bval',saveConf);
          				save(fullfile(pName,['BLineX_',fName]),'XLine',saveConf);
          				save(fullfile(pName,['BLineY_',fName]),'YLine',saveConf);
          			else
          				save(fullfile(pName,fName),'XVector','bvalSum','bval','XLine','YLine',saveConf);
          			end
          		
          		
     			case 'McEstimate'
          			XVec=obj.ZmapVariables.dat(:,1);
         			YVec=obj.ZmapVariables.dat(:,7);
         			
         			if strcmp(FileType,'ascii_trio')
          				save(fullfile(pName,['XVec_',fName]),'XVec',saveConf);
          				save(fullfile(pName,['YVec_',fName]),'YVec',saveConf);
          				
          			else
          				save(fullfile(pName,fName),'XVec','YVec',saveConf);
          			end
          		
     			
       			case 'McTime'
       				TimeVector=obj.ZmapVariables.mResult(:,1);
         			Mc=obj.ZmapVariables.mMc;
          			stdMc=obj.ZmapVariables.mMcstd1;
          			        
          			if strcmp(FileType,'ascii_trio')
          				save(fullfile(pName,['TimeVec_',fName]),'TimeVector',saveConf);
          				save(fullfile(pName,['Mc_',fName]),'Mc',saveConf);
          				save(fullfile(pName,['stdMc_',fName]),'stdMc',saveConf);
          			else
          				save(fullfile(pName,fName),'TimeVector','Mc','stdMc',saveConf);
          			end	
          			
          				
          		case 'bdepth'
          			DepthVector=obj.ZmapVariables.BV3(:,1);
         			bvalDep=obj.ZmapVariables.BV(:,2);
          			stdbvalDep=obj.ZmapVariables.BV3(:,3);
          			        
          			if strcmp(FileType,'ascii_trio')
          				save(fullfile(pName,['DepthVec_',fName]),'DepthVector',saveConf);
          				save(fullfile(pName,['bvalDep_',fName]),'bvalDep',saveConf);
          				save(fullfile(pName,['stdbvalDep_',fName]),'stdbvalDep',saveConf);
          			else
          				save(fullfile(pName,fName),'DepthVector','bvalDep','stdbvalDep',saveConf);
          			end
          		
          		
          		case 'btime'
          			TimeVector=obj.ZmapVariables.mResult(:,1);
         			bval=obj.ZmapVariables.mB;
          			stdbval=obj.ZmapVariables.mBstd1;
          			        
          			if strcmp(FileType,'ascii_trio')
          				save(fullfile(pName,['TimeVec_',fName]),'TimeVector',saveConf);
          				save(fullfile(pName,['bval_',fName]),'bval',saveConf);
          				save(fullfile(pName,['stdbval_',fName]),'stdbval',saveConf);
          			else
          				save(fullfile(pName,fName),'TimeVector','bval','stdbval',saveConf);
          			end
          			
          		case 'bmag'
          			MagVector=obj.ZmapVariables.BV3(:,2);
         			bvalMag=obj.ZmapVariables.BV3(:,1);
          			stdbvalMag=obj.ZmapVariables.BV3(:,3);
          			        
          			if strcmp(FileType,'ascii_trio')
          				save(fullfile(pName,['MagVec_',fName]),'MagVector',saveConf);
          				save(fullfile(pName,['bvalMag_',fName]),'bvalMag',saveConf);
          				save(fullfile(pName,['stdbvalMag_',fName]),'stdbvalMag',saveConf);
          			else
          				save(fullfile(pName,fName),'MagVector','bvalMag','stdbvalMag',saveConf);
          			end
          		
					
			
		end
          	
		

          end
	
	
	
	
end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
	
	
	
	%THE FUNCTIONS UNDERNEATH ARE NOT ALWAYS DIRECTLY USEABLE, AS THEY OFTEN NEED DIRECT ACCESS
	%TO THE VARIABLE, SO JUST COPY IF NEEDED.
	function TheEvalString = UnpackStruct(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		TheEvalString=[];
		
		for i=1:numel(thefields)
			
			TheEvalString=[TheEvalString; thefields{i},'=' num2str(TheStruct.(thefields{i})),';'];
		end
		 %can be done easier directly in the function which should be calling this, it is more sort of 
		 %a sketch
		
		
	
	end
	
	
	function CellData = UnpackStruct2Cell(TheStruct)
		%greats a string which defines all fields in a structure as variable with their
		%field names as there name
		
		%get fields
		thefields=fieldnames(TheStruct);
		
		
		
		for i=1:numel(thefields)
			
			CellData{i,1}=thefields{i}
			CellData{i,2}=TheStruct.(thefields{i});
		end
		
		%assignin can be used to set the variable on the string.
	end
	
	function TheStruct = PackVariable(variablelist)
		%Packs all variables mentioned in the variablelist (cell array with strings (1) and variable 
		%(2)(optional)) into a structure with fieldnames defined by their name.
		
		if numel(variablelist(1,:))==2
			%two coulomn cell array, second coulomn for the variable itselectorf
		
			for i=1:numel(variablelist(:,1))
				TheStruct.(variablelist{i,1})=variablelist{i,2};
			
			end
		
		else
			%This may not work as the variables might not be in scope
			for i=1:numel(variablelist)
				TheStruct.(variablelist{i})=eval(variablelist{i});
			
			end
		end	
		
	end
	
	
	function TheStruct = PackAllVariable()
		%The opposite of UnPack, it takes every variable in the current Workspace an packs it 
		%into a structure
		
		varilist=who;
		
		for i=1:numel(varilist)
			TheStruct.(varilist{i})=eval(varilist{i});
		
		end
	
	
	end
	
	%---------------


	end
	
	

end
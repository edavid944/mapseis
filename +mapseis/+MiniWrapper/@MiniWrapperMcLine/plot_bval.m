function plot_bval(obj)
		%this is kind of a wrapper for view_bva
		
		%unpack
		%unpack the variables,
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		
		%to avoid doubled data menus
		WindowExist=false;
		
		
		switch obj.CalcType
			case 'Mcauto'
				 [existFlag,figNumber]=figflag('Frequency-magnitude distribution',1);
				 if existFlag
				 	% figure(bfig);
					bfig = figNumber;
					WindowExist=true;
				 else
					bfig=figure(...                  %build figure for plot
					    'Units','normalized','NumberTitle','off',...
					    'Name','Frequency-magnitude distribution',...
					    'MenuBar','none',...
					    'visible','off',...
					    'pos',[ 0.300  0.3 0.4 0.6]);
				
					ho = 'noho';
					makebut2
					matdraw
					
					
				 	optio = uimenu('Label','ZTools ');
					uimenu(optio,'Label','Estimate recurrence time/probability','callback',@(s,e) obj.universalCalc('plorem'));
					uimenu(optio,'Label','Manual fit of b-value','callback',@(s,e) obj.universalCalc('bfitnew(newcat)'));
					uimenu(optio,'Label','Plot time series','callback',@(s,e) obj.universalCalc('newcat = newt2; timeplot'));
					uimenu(optio,'Label','Do not show discrete curve','callback',@(s,e) obj.universalCalc('delete(pl1)'));
					uimenu(optio,'Label','Save values to file','callback',@(s,e) obj.universalCalc(calSave9));
					
					%for the moment set the menu to invisible, most parts do not work anyway.
					set(optio,'Visible','off');
					
				    end; % existflag
				
				    calSave9 =...
					[ 'welcome(''Save Data'',''  '');think;',...
					'[file1,path1] = uiputfile([ hodi fs ''out'' fs ''*.dat''], ''Filename ? '');',...
					's=[xt3'' bvalsum3'' ];',...
					'fid = fopen([path1 file1],''w'') ;',...
					'fprintf(fid,''%6.2f  %6.2f\n'',s'');',...
					'fclose(fid) ;',...
					'done';];
				    if ho(1:2) == 'ho'
					axes(cua)
					disp('hold on')
					hold on
				    else
					figure(bfig);delete(gca);delete(gca); delete(gca); delete(gca)
					rect = [0.22,  0.3, 0.65, 0.6];           % plot Freq-Mag curves
					axes('position',rect);
				    end; % ho
				
				    %%
				    % plot the cum. sum in each bin  %%
				    %%
				
				    pl =semilogy(xt3,bvalsum3,'sb');
				    set(pl,'LineWidth',[1.0],'MarkerSize',[6],...
					'MarkerFaceColor','w','MarkerEdgeColor','k');
				    hold on
				    pl1 =semilogy(xt3,bval2,'^b');
				    set(pl1,'LineWidth',[1.0],'MarkerSize',[4],...
					'MarkerFaceColor',[0.7 0.7 .7],'MarkerEdgeColor','k');
				
				    %%
				    % CALCULATE the diff in cum sum from the previous biin
				    %%
				
				
				    xlabel('Magnitude','FontWeight','bold','FontSize',fs12)
				    ylabel('Cumulative Number','FontWeight','bold','FontSize',fs12)
				    set(gca,'visible','on','FontSize',fs12,'FontWeight','normal',...
					'FontWeight','bold','LineWidth',[1.0],'TickDir','out','Ticklength',[0.02 0.02],...
					'Box','on','Tag','cufi','color','w')
				
				    cua = gca;
					
				     %%
				    % PLOTS an 'x' in the point of Mc
				    %%
				
				    te = semilogy(xt3(index_low),bvalsum3(index_low)*1.5,'vb');
				    set(te,'LineWidth',[1.0],'MarkerSize',7)
				
				    te = text(xt3(index_low)+0.2,bvalsum3(index_low)*1.5,'Mc');
				    set(te,'FontWeight','bold','FontSize',fs10,'Color','b')
				    
				    
				    figure(bfig)
				    
				    hold on
				    ttm= semilogy(mag_zone,f,'r');                         % plot linear fit to backg
				    set(ttm,'LineWidth',[1.])
				    
				    %pdf_calc;
				    set(gca,'XLim',[min(b(:,6))-0.5  max(b(:,6))+0.5])
				    set(gca,'YLim',[0.9 length(b(:,3)+30)*2.5]);
				    
				    if ho(1:2) == 'ho'
					set(pl,'LineWidth',[1.0],'MarkerSize',[8],...
					    'MarkerFaceColor','k','MarkerEdgeColor','k','Marker','^');
					%set(pl3,'LineWidth',[1.0],'MarkerSize',[6],...
					%'MarkerFaceColor','c','MarkerEdgeColor','m','Marker','s');
					%   txt1=text(.16, .06,['b-value (w LS, M  >= ', num2str(M1b(1)) '): ',tt1, ' +/- ', tt2 ', a-value = ' , num2str(aw) ]);
					set(txt1,'FontWeight','normal','FontSize',fs10,'Color','r')
				    else
					if bBst_button == 0
					    txt1=text(.16, .11,['b-value = ',tt1,' +/- ',tt2,',  a value = ',num2str(aw,3) ',  a value (annual) = ', num2str(a0,3)],'FontSize',fs10);
					    set(txt1,'FontWeight','normal')
					    set(gcf,'PaperPosition',[0.5 0.5 4.0 5.5])
					    text(.16, .14,sol_type,'FontSize',fs10 );
					    text(.16, .08,['Magnitude of Completeness = ',tmc],'FontSize',fs10);
					else
					    txt1=text(.16, .11,['b-value = ',num2str(round(100*fBValue)/100),' +/- ',num2str(round(100*fStd_B)/100),',  a value = ',num2str(aw,3) ',  a value (annual) = ', num2str(a0,3)],'FontSize',fs10);
					    set(txt1,'FontWeight','normal')
					    set(gcf,'PaperPosition',[0.5 0.5 4.0 5.5])
					    text(.16, .14,sol_type,'FontSize',fs10 );
					    text(.16, .08,['Magnitude of Completeness = ',tmc ' +/- ', num2str(round(100*fStd_Mc)/100)],'FontSize',fs10);
					end;
				    end; % ho
			
				    set(gcf,'visible','on','Color','w');
				    %welcome('  ','Done')
				    %done
				
				    if ho(1:2) == 'ho'
					% calculate the probability that the two distributins are differnt
					%l = newt2(:,6) >=  M1b(1);
					b2 = str2num(tt1); n2 = M1b(2);
					n = n1+n2;
					da = -2*n*log(n) + 2*n1*log(n1+n2*b1/b2) + 2*n2*log(n1*b2/b1+n2) -2;
					pr = exp(-da/2-2);
					disp(['Probability: ',  num2str(pr)]);
					txt1=text(.60, .85,['p=  ', num2str(pr,2)],'Units','normalized');
					set(txt1,'FontWeight','normal','FontSize',fs10)
					txt1=text(.60, .80,[ 'n1: ' num2str(n1) ', n2: '  num2str(n2) ', b1: ' num2str(b1)  ', b2: ' num2str(b2)]);
					set(txt1,'FontSize',[8],'Units','normalized')
				    end
				    
				    %set packlist
				    obj.PackList=[obj.PackList; {'cua';'bfig';'pl1';'calSave9'}];
				    
				
				    
			case 'McEstimate'
				fi = findobj('tag','mcfig'); 
				if isempty(fi) == 1
				   figure('pos',[300 300 600 300],...
				      'tag','mcfig');
				else
				   figure(fi); delete(gca);delete(gca);
				   WindowExist=true;
				end
				
				
				axes('pos',[0.15 0.2 0.7 0.65])
				plot(dat(:,1),dat(:,7),'k','LineWidth',[1.])
				hold on
				pl = plot(dat(:,1),dat(:,7),'b^')
				set(pl,'LineWidth',[1.0],'MarkerSize',[8],...
				   'MarkerFaceColor','y','MarkerEdgeColor','b');
				
				%errorbar(dat(:,1),dat(:,7),abs(dat(:,7) - dat(:,8)),abs(dat(:,7) - dat(:,9)))
				grid
				
				set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
				    'TickDir','out','LineWidth',[1.0],...
				    'Box','on')
				 xlabel('Magnitude')
				 ylabel('Residual in %')
				 title('Goodness of FMD fit to power law');
				 te = text(0.6,0.8,['Mc at 90% confidence: ' num2str(Mc90) ],...
				    'Units','normalized','FontWeight','bold'); 
				 te = text(0.6,0.9,['Mc at 95% confidence: ' num2str(Mc95) ],...
				    'Units','normalized','FontWeight','bold'); 

				obj.PackList=[obj.PackList; {'fi'}];

				
				
			
			case 'McTime'
				
				figNumber=findobj('tag','Mc time series');
				%[existFlag,figNumber]=figflag('b-value with depth',1);
				newTimeWindowFlag=isempty(figNumber);
				bdep= figNumber;
			
				
				
				
				if newTimeWindowFlag,
				    figure('Name','Mc time series','tag','Mc time series',...
				    	'visible','on');
				    
				else
					WindowExist=true;
					figure(bdep);
				end
				
				
				
				plot(mResult(:,1),mMc,'-','Linewidth',2,'Color',[0.2 0.2 0.2]);
				hold on;
				plot(mResult(:,1),mMc-mMcstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				plot(mResult(:,1),mMc+mMcstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				hold off;
				ylabel('Mc','Fontweight','bold','Fontsize',12)
				set(gca,'tag','smooth_btime_n1000')
				%set(gca,'XTickLabel',[])
				xlim([(min(mResult(:,1))) (max(mResult(:,1)))])
				ylim([floor(min(mMc(:,1))) ceil(max(mMc(:,1)))])
				xlabel('Time / [dec. year]','Fontweight','bold','Fontsize',12)% ylim([0.85 1.1])
				l1=legend('Mc','\delta Mc');
				%l1=legend('EMR','Std(EMR)','MaxM');
				set(l1,'Fontweight','bold')
				set(gca,'Fontweight','bold','Fontsize',10,'Linewidth',2,'Tickdir','out')
				

				
			case 'bdepth'
				% Find out of figure already exists
				%
				figNumber=findobj('Name','b-value with depth');
				%[existFlag,figNumber]=figflag('b-value with depth',1);
				newdepWindowFlag=isempty(figNumber);
				bdep= figNumber;
				
				% Set up the Cumulative Number window
				
				if newdepWindowFlag,
				    bdep = figure( ...
					'Name','b-value with depth',...
					'NumberTitle','off', ...
					'MenuBar','none', ...
					'NextPlot','add', ...
					'backingstore','on',...
					'Visible','on', ...
					'Position',[ 150 150 winx-50 winy-20]);
				    
				    makebut2
				    uicontrol('BackGroundColor','y','Units','normal',...
					'Position',[.0 .85 .08 .06],'String','Info ',...
					'callback','infoz(1)');
				    
				    matdraw
				    
				else
					WindowExist=true;
				end
				
				figure(bdep)
				delete(gca)
				delete(gca)
				delete(gca)
				delete(gca)
				hold off
				
				axis off
				hold on
				orient tall
				%rect = [ 0.15 0.65 0.7 0.25];
				rect = [ 0.25 0.15 0.5 0.75];
				axes('position',rect)
				ple = errorbar(BV3(:,2),BV3(:,1),BV3(:,3),BV3(:,3),'k')
				set(ple(1),'color',[0.5 0.5 0.5]);
				
				hold on
				pl = plot(BV(:,2),BV(:,1),'color',[0.5 0.5 0.5]);
				
				pl = plot(BV3(:,2),BV3(:,1),'sk')
				
				set(pl,'LineWidth',[1.0],'MarkerSize',[4],...
				    'MarkerFaceColor','w','MarkerEdgeColor','k','Marker','s');
				
				set(gca,'box','on',...
				    'DrawMode','fast','TickDir','out','FontWeight',...
				    'bold','FontSize',fs12,'Linewidth',[1.],'Ticklength',[ 0.02 0.02])
				
				bax = gca;
				strib = [name ', ni = ' num2str(ni), ', Mmin = ' num2str(min(newt2(:,6))) ];
				ylabel('b-value')
				xlabel('Depth [km]')
				title2(strib,'FontWeight','bold',...
				    'FontSize',fs12,...
				    'Color','k')
				
				xl = get(gca,'Xlim');
				view([90 90]),

				
				
			case 'btime'
				figNumber=findobj('tag','b-value time series');
				%[existFlag,figNumber]=figflag('b-value with depth',1);
				newTimeWindowFlag=isempty(figNumber);
				bdep= figNumber;
			
				if newTimeWindowFlag,
				    figure('Name','b-value time series','tag','b-value time series',...
				    		'visible','on');
				    
				else
					WindowExist=true;
					figure(bdep);
				end
				
				
				hp1=plot(mResult(:,1),mB,'-','Linewidth',2,'Color',[0.2 0.2 0.2]);
				hold on;
				hp2=plot(mResult(:,1),mB-mBstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				plot(mResult(:,1),mB+mBstd1,'-.','Linewidth',2,'Color',[0.5 0.5 0.5]);
				hold off;
				ylabel('b-value','Fontweight','bold','Fontsize',12)
				xlim([(min(mResult(:,1))) (max(mResult(:,1)))])
				ylim([floor(min(mB(:,1))) ceil(max(mB(:,1)))])
				xlabel('Time / [dec. year]','Fontweight','bold','Fontsize',12)
				l1=legend([hp1 hp2],'b-value','\delta b');
				set(l1,'Fontweight','bold')
				set(gca,'Fontweight','bold','Fontsize',10,'Linewidth',2,'Tickdir','out')
				
				
				
			case 'bmag'
				% Find out of figure already exists
				%
				%[existFlag,figNumber]=figflag('b-value with magnitude',1);
				figNumber=findobj('Name','b-value with magnitude');
				newdepWindowFlag=isempty(figNumber);
				bdep= figNumber;
				
				
				% Set up the Cumulative Number window
				
				if newdepWindowFlag,
				    bdep = figure( ...
					'Name','b-value with magnitude',...
					'NumberTitle','off', ...
					'MenuBar','none', ...
					'NextPlot','add', ...
					'backingstore','on',...
					'Visible','on', ...
					'Position',[ 150 150 600 500]);
				    
				    makebut2
				    
				    matdraw
				else
					WindowExist=true;
				end
				
				
				
				figure(bdep)
				delete(gca)
				delete(gca)
				delete(gca)
				delete(gca)
				hold off
				
				axis off
				hold on
				orient tall
				%rect = [ 0.15 0.65 0.7 0.25];
				rect = [ 0.15 0.15 0.7 0.7];
				axes('position',rect)
				ple = errorbar(BV3(:,2),BV3(:,1),BV3(:,3),BV3(:,3),'k')
				set(ple(1),'color',[0.5 0.5 0.5]);
				
				hold on
				
				pl = plot(BV3(:,2),BV3(:,1),'sk')
				
				set(pl,'LineWidth',[1.0],'MarkerSize',[4],...
				    'MarkerFaceColor','w','MarkerEdgeColor','k','Marker','s');
				
				set(gca,'box','on',...
				    'DrawMode','fast','TickDir','out','FontWeight',...
				    'bold','FontSize',fs12,'Linewidth',[1.],'Ticklength',[ 0.02 0.02])
				
				bax = gca;
				strib = [name ', ni = ' num2str(ni), ', Mmin = ' num2str(min(newt2(:,6))) ];
				ylabel('b-value')
				xlabel('Magnitude')
				title(strib,'FontWeight','bold',...
				    'FontSize',fs12,...
				    'Color','k')
				
				xl = get(gca,'Xlim');

		end
		
		
		if ~WindowExist
			%Add Data Menu
			options = uimenu('Label','- Data');
			uimenu('Parent',options,'Label','Calculate again ',...
				'Callback',@(s,e) obj.CalcAgain);
			uimenu('Parent',options,'Label','Write to datastore ',...
				'Callback',@(s,e) obj.saveCalc);
			uimenu('Parent',options,'Label','export Result (.mat)','Separator','on',... 
				'Callback',@(s,e) obj.exportResult('matlab'));
			uimenu('Parent',options,'Label','export Result (ascii)',... 
				'Callback',@(s,e) obj.exportResult('ascii'));
			uimenu('Parent',options,'Label','export Result (multiple files)',... 
				'Callback',@(s,e) obj.exportResult('ascii_trio'));
			uimenu('Parent',options,'Label','export Full Result',... 
				'Callback',@(s,e) obj.exportResult('full_result'));	
		end
		
		
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end

end
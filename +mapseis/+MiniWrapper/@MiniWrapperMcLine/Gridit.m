function Gridit(obj,initswitch)
		%disp(obj.ZmapVariables)
		%unpack the variables,
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		
		if initswitch
			%This has to be done just once, hence the switch;
			%update packlist
			obj.PackList=[obj.PackList; {'dx';'dy';'ni';'newgri';'xvect';'yvect';...
					'll';'gx';'gy'}];
			
			
			%get parameter from the datastore
			
			%build grid
			gridpars=obj.Datastore.getUserData('gridPars')
			
			if numel(gridpars.gridSep)<2
				dx=gridpars.gridSep;
				dy=gridpars.gridSep;
			else
				dx=gridpars.gridSep(1);
				dy=gridpars.gridSep(2);
			end	
			
			ra=deg2km(gridpars.rad);
			
			ni=100;
			Nmin = 50;
			
		end
		
		%extracted from CalcGridGUI, delete later what's not needed.
		regionFilter = obj.Filterlist.getByName('Region');
		regionParms = regionFilter.getRegion();
		
		if ~strcmp(regionParms{1},'all')
			theRegion = regionParms{2};
			bBox=theRegion.getBoundingBox();
                else
                	bBox(1)=min(obj.ZmapCatalog(:,1));
                	bBox(2)=max(obj.ZmapCatalog(:,1));
                	bBox(3)=min(obj.ZmapCatalog(:,2));
                	bBox(4)=max(obj.ZmapCatalog(:,2));
                end
                %xRange = (bBox(1):ptSep(1):bBox(2))';
                %yRange = (bBox(3):ptSep(2):bBox(4))';
		vX = [bBox(1); bBox(1); bBox(2); bBox(2)];
		vY = [bBox(4); bBox(3); bBox(3); bBox(4)];
		vX = [vX; vX(1)];
		vY = [vY; vY(1)]; 
		
		%from ex_selectgrid; (it is used in the fullarea mode, the data is filtered anyway)
		%==================
		%Could maybe be optimized, but leave it at the moment as it is
		
		% Create a rectangular grid
		vXVector = [bBox(1):dx:bBox(2)];
		vYVector = [bBox(3):dy:bBox(4)];
		mGrid = zeros((length(vXVector) * length(vYVector)), 2);
		nTotal = 0;
		for i = 1:length(vXVector)
		   for j = 1:length(vYVector)
		     nTotal = nTotal + 1;
		     mGrid(nTotal,:) = [vXVector(i) vYVector(j)];
		   end;
		end;
		  
		% Extract all gridpoints in chosen polygon
		XI=mGrid(:,1);
		YI=mGrid(:,2);
  
		m = length(vX)-1;      %  number of coordinates of polygon
		l = 1:length(XI);
		l = (l*0)';
		vUsedNodes = l;               %  Algorithm to select points inside a closed
		%  polygon based on Analytic Geometry    R.Z. 4/94
		for i = 1:m;
		  
			l= ((vY(i)-YI < 0) & (vY(i+1)-YI >= 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0) | ...
		  		((vY(i)-YI >= 0) & (vY(i+1)-YI < 0)) & ...
		  		(XI-vX(i)-(YI-vY(i))*(vX(i+1)-vX(i))/(vY(i+1)-vY(i)) < 0);
		    
		  	if i ~= 1 
		  		vUsedNodes(l) = 1 - vUsedNodes(l);
		  	else
		  		vUsedNodes = l; 
		  	end;         
		    
		end;         
		
		%grid points in polygon
		mGrid = mGrid(vUsedNodes,:);
		
		%======================
		
		%newgri, xvect, yvect, ll
		
		%set the values
		newgri=mGrid;
		xvect=vXVector;
		yvect=vYVector;
		ll=vUsedNodes;
		
		gx = xvect;
		gy = yvect;
		
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
	
	end
	
	
	
	function openCalcParamWindow(obj)
		%Here a window for the calculation parameters should be created
		%I suggest using the inputsdlg function 
		
		%and it will be used here.
		import mapseis.calc.Mc_calc.calc_Mc;
		
		
		%unpack again all the variables
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		%Needed Parameterlist
		%dim = [8 2];
		
		switch obj.CalcType
			case 'Mcauto'
				Title = 'Mc Input Parameter';
				Prompt={'Mc Method:', 'inpr1';...
					'Mc bootstraps:','bBst_button';...
					'Number of Bootstraps:','nBstSample';...
					'Mc Correction for Maxc:','fMccorr';
					'Magnitude Bin Size:','fBinning'};
				
				labelList2 =  {'1: Maximum curvature'; ...
					'2: Fixed Mc = minimum magnitude (Mmin)'; ...
					'3: Mc90 (90% probability)'; ...
					'4: Mc95 (95% probability)'; ...
					'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
					'6: EMR-method'; ...
					'7: Mc due b using Shi & Bolt uncertainty'; ...
					'8: Mc due b using bootstrap uncertainty'; ...
					'9: Mc due b Cao-criterion';...
					'10: MBass'}; 
				
				%Mc Method
				Formats(1,1).type='list';
				Formats(1,1).style='popupmenu';
				Formats(1,1).items=labelList2;
				Formats(1,2).type='none';
				Formats(1,1).size = [-1 0];
				Formats(1,2).limits = [0 99];
				
				%bootstrap toggle
				Formats(2,1).type='check';
				
				%bootstrap number
				Formats(2,2).type='edit';
				Formats(2,2).format='integer';
				Formats(2,2).limits = [0 9999];
				
				
				
				%Mc Correction
				Formats(3,1).type='edit';
				Formats(3,1).format='float';
				Formats(3,1).limits = [-99 99];
				Formats(3,1).size = [-1 0];
				Formats(3,2).limits = [0 1];
				Formats(3,2).type='none';
				
				%Mag Binning
				Formats(4,1).type='edit';
				Formats(4,1).format='float';
				Formats(4,1).limits = [-99 99];
				Formats(4,1).size = [-1 0];
				Formats(4,2).limits = [0 1];
				Formats(4,2).type='none';
				
		
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
		
				
				
				%default values
				defval =struct('inpr1',inpr1,...
					'bBst_button',bBst_button,...
					'nBstSample',nBstSample,...
					'fBinning',fBinning,...
					'fMccorr',fMccorr);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				
				%Now the new parameters should be existing format them if needed
				%and save them in the datastore if needed
				
				
				%unpack from the structure
				inpr1=NewParameter.inpr1;
				inb2=inpr1; %just in case
				bBst_button=NewParameter.bBst_button;
				nBstSample=NewParameter.nBstSample;		
				fMccorr=NewParameter.fMccorr;
				fBinning=NewParameter.fBinning;
				
				
			case 'McEstimate'
				%no config needed
				Canceled=0;
			case {'McTime','btime'}
				Title = 'Mc Input Parameter';
				Prompt={'Mc Method:', 'nMethod';...
					'Sample Window Size:','nSampleSize';...
					'Overlap:','nOverlap';...
					'Number of Bootstraps:','nBstSample';...
					'Binning:','fBinning';...
					'Mc Correction: ','fMccorr';...
					'Smooth Plot:', 'nWindowSize';...
					'Minimum Number:', 'nMinNumberevents'};
				
				labelList2 =  {'1: Maximum curvature'; ...
					'2: Fixed Mc = minimum magnitude (Mmin)'; ...
					'3: Mc90 (90% probability)'; ...
					'4: Mc95 (95% probability)'; ...
					'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
					'6: EMR-method'; ...
					'7: Mc due b using Shi & Bolt uncertainty'; ...
					'8: Mc due b using bootstrap uncertainty'; ...
					'9: Mc due b Cao-criterion';
					'10: MBass'}; 
				
				%Mc Method
				Formats(1,1).type='list';
				Formats(1,1).style='popupmenu';
				Formats(1,1).items=labelList2;
				Formats(1,2).type='none';
				Formats(1,1).size = [-1 0];
				Formats(1,2).limits = [0 99];
				
				%Sample Window
				Formats(2,1).type='edit';
				Formats(2,1).format='integer';
				Formats(2,1).limits = [0 9999];
				
				%Overlap
				Formats(2,2).type='edit';
				Formats(2,2).format='integer';
				Formats(2,2).limits = [0 9999];
				
				
				%bootstrap number
				Formats(3,1).type='edit';
				Formats(3,1).format='integer';
				Formats(3,1).limits = [0 9999];
				
				%Binning
				Formats(3,2).type='edit';
				Formats(3,2).format='float';
				Formats(3,2).limits = [0 99];
				
				%Mc Correction
				Formats(4,1).type='edit';
				Formats(4,1).format='float';
				Formats(4,1).limits = [-99 99];
				
				%Smooth plot
				Formats(4,2).type='edit';
				Formats(4,2).format='integer';
				Formats(4,2).limits = [0 9999];
				
				Formats(5,1).type='edit';
				Formats(5,1).format='integer';
				Formats(5,1).limits = [0 9999];
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
		

				%default values
				defval =struct(	'nMethod',nMethod,...
						'nSampleSize',nSampleSize,...
						'nOverlap',nOverlap,...
						'nBstSample',nBstSample,...
						'fMccorr',fMccorr,...
						'fBinning',fBinning,...
						'nWindowSize',nWindowSize,...
						'nMinNumberevents',nMinNumberevents);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				
				%Now the new parameters should be existing format them if needed
				%and save them in the datastore if needed
				
				
				%unpack from the structure
				nMethod=NewParameter.nMethod;
				nSampleSize=NewParameter.nSampleSize;
				nBstSample=NewParameter.nBstSample;
				nOverlap=NewParameter.nOverlap;
				fMccorr=NewParameter.fMccorr;	
				nMinNumberevents=NewParameter.nMinNumberevents;
				fBinning=NewParameter.fBinning;
				nWindowSize=NewParameter.nWindowSize;

			
				
			case 'bdepth'
				Title = 'b with depth input parameters';
				Prompt={'Number of events each window:', 'ni';...
					'Overlap factor:','ofac';...
					'Mc determination?','ButtonNr'};
				
				
				
				
				
				%bootstrap number
				Formats(1,1).type='edit';
				Formats(1,1).format='integer';
				Formats(1,1).limits = [0 9999];
				
				
				%Mc Correction
				Formats(2,1).type='edit';
				Formats(2,1).format='integer';
				Formats(2,1).limits = [0 9999];

				Formats(3,1).type='list';
				Formats(3,1).style='togglebutton';
				Formats(3,1).items={'Automatic','Fixed Mc=Mmin';}
				
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
				
				
				
				%default values
				switch ButtonName,
				 	case 'Automatic'
				 		ButtonNr=1;
				 	case 'Fixed Mc=Mmin'	
				 		ButtonNr=2;
				end		
				defval =struct(	'ni',ni,...
						'ofac',ofac,...
						'ButtonNr',ButtonNr);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				
				%Now the new parameters should be existing format them if needed
				%and save them in the datastore if needed
				
				
				%unpack from the structure
				ni=NewParameter.ni;
				ofac=NewParameter.ofac;
				
				if NewParameter.ButtonNr==1;
					ButtonName='Automatic';
				elseif NewParameter.ButtonNr==2;
					ButtonName='Fixed Mc=Mmin';
				end
				
			case 'bmag'
				%no parameters to set
				Canceled=0;
				
			
			
		end	
			
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
		
end

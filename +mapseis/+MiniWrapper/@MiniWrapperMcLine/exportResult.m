function exportResult(obj,FileType,Filename) 
	%saves the currently plotted result into a mat or ascii file
	
	if nargin<3
		Filename=[];
	end
	
	if isempty(obj.ZmapVariables)
		warning('nothing calculated')
	end	
  
	%get file name
	switch FileType
		case {'matlab','full_result'}
			%get filename
			if isempty(Filename)
				[fName,pName] = uiputfile('*.mat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;			
			end
			saveConf='-mat';
		case {'ascii','ascii_trio'}
			if isempty(Filename)
				[fName,pName] = uiputfile('*.dat','Enter save file name...');
			else
				[pathstr,name,ext] = fileparts(Filename)
				fName=[name,ext];
				pName=pathstr;
			end
			saveConf='-ascii';
	end
	
	if isempty(fName)
		%was probably canceled
		return
	end
	
	%get data
	%case with full result
	if strcmp(FileType,'full_result')
		ZmapVariables=obj.ZmapVariables;
		save(fullfile(pName,fName),'ZmapVariables',saveConf);
		return
		
	end
	
	switch obj.CalcType
		case 'Mcauto'
			XVector=obj.ZmapVariables.xt3;
			bvalSum=obj.ZmapVariables.bvalsum3;
			bval=obj.ZmapVariables.bval2;
			XLine=obj.ZmapVariables.mag_zone;
			YLine=obj.ZmapVariables.f;
			
			
			if strcmp(FileType,'ascii_trio')
				save(fullfile(pName,['XVector_',fName]),'XVector',saveConf);
				save(fullfile(pName,['bvalSum_',fName]),'bvalSum',saveConf);
				save(fullfile(pName,['bval_',fName]),'bval',saveConf);
				save(fullfile(pName,['BLineX_',fName]),'XLine',saveConf);
				save(fullfile(pName,['BLineY_',fName]),'YLine',saveConf);
			else
				save(fullfile(pName,fName),'XVector','bvalSum','bval','XLine','YLine',saveConf);
			end
		
		
		case 'McEstimate'
			XVec=obj.ZmapVariables.dat(:,1);
			YVec=obj.ZmapVariables.dat(:,7);
			
			if strcmp(FileType,'ascii_trio')
				save(fullfile(pName,['XVec_',fName]),'XVec',saveConf);
				save(fullfile(pName,['YVec_',fName]),'YVec',saveConf);
				
			else
				save(fullfile(pName,fName),'XVec','YVec',saveConf);
			end
		
		
		case 'McTime'
			TimeVector=obj.ZmapVariables.mResult(:,1);
			Mc=obj.ZmapVariables.mMc;
			stdMc=obj.ZmapVariables.mMcstd1;
				
			if strcmp(FileType,'ascii_trio')
				save(fullfile(pName,['TimeVec_',fName]),'TimeVector',saveConf);
				save(fullfile(pName,['Mc_',fName]),'Mc',saveConf);
				save(fullfile(pName,['stdMc_',fName]),'stdMc',saveConf);
			else
				save(fullfile(pName,fName),'TimeVector','Mc','stdMc',saveConf);
			end	
			
				
		case 'bdepth'
			DepthVector=obj.ZmapVariables.BV3(:,1);
			bvalDep=obj.ZmapVariables.BV(:,2);
			stdbvalDep=obj.ZmapVariables.BV3(:,3);
				
			if strcmp(FileType,'ascii_trio')
				save(fullfile(pName,['DepthVec_',fName]),'DepthVector',saveConf);
				save(fullfile(pName,['bvalDep_',fName]),'bvalDep',saveConf);
				save(fullfile(pName,['stdbvalDep_',fName]),'stdbvalDep',saveConf);
			else
				save(fullfile(pName,fName),'DepthVector','bvalDep','stdbvalDep',saveConf);
			end
		
		
		case 'btime'
			TimeVector=obj.ZmapVariables.mResult(:,1);
			bval=obj.ZmapVariables.mB;
			stdbval=obj.ZmapVariables.mBstd1;
				
			if strcmp(FileType,'ascii_trio')
				save(fullfile(pName,['TimeVec_',fName]),'TimeVector',saveConf);
				save(fullfile(pName,['bval_',fName]),'bval',saveConf);
				save(fullfile(pName,['stdbval_',fName]),'stdbval',saveConf);
			else
				save(fullfile(pName,fName),'TimeVector','bval','stdbval',saveConf);
			end
			
		case 'bmag'
			MagVector=obj.ZmapVariables.BV3(:,2);
			bvalMag=obj.ZmapVariables.BV3(:,1);
			stdbvalMag=obj.ZmapVariables.BV3(:,3);
				
			if strcmp(FileType,'ascii_trio')
				save(fullfile(pName,['MagVec_',fName]),'MagVector',saveConf);
				save(fullfile(pName,['bvalMag_',fName]),'bvalMag',saveConf);
				save(fullfile(pName,['stdbvalMag_',fName]),'stdbvalMag',saveConf);
			else
				save(fullfile(pName,fName),'MagVector','bvalMag','stdbvalMag',saveConf);
			end
		
				
		
	end
	
	

end

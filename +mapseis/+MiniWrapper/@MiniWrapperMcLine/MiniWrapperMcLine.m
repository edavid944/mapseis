classdef MiniWrapperMcLine < handle
		
	%This is a template to the so called MiniWrappers. The purpose of those
	%wrappers is to make conversion of old zmap calculation as easy as possible
	%(yeah this was said before, this will be easier, but also more limited).
	%All calculations done in this Wrapper are "on shots" the calculation can 
	%not be "redone" once the parameters are set.
	%Also there are selfcontaint and have no separation between GUI and calculation
	
	properties
		ListProxy
		Commander
		Datastore
		Keys
		Filterlist
		SendedEvents
		ZmapCatalog
		ZmapVariables
		CalcResult
		CalcParameter
		ErrorDialog
		ParallelMode
		PackList
		StartCalc
		CalcName
		CalcType
		savePos
	end

	events
		CalcDone

	end


	methods
	
		function obj = MiniWrapperMcLine(ListProxy,Commander,CalcType,Override)
			import mapseis.datastore.*;
			import mapseis.export.Export2Zmap;
			%This is a first application of the MiniWrapperTemplate
			%maybe later the Template will be used as SuperClass, but for 
			%this case the template is used directly
			
			%This models the function bdiff2 and some related function
			%it might be replaced later by the bigger McLineCalcWrapper
			
			%The override switch also to create a the object without any 
			%command started.
			if nargin<4
				Override=false;
			end
			%In this constructor everything is set, and the 
			obj.ListProxy=ListProxy;
			obj.Commander = Commander; 
			obj.ParallelMode=obj.Commander.ParallelMode;
			
			if ~Override
				%get the current datastore and filterlist and build zmap catalog
				obj.Datastore = obj.Commander.getCurrentDatastore;
				obj.Filterlist = obj.Commander.getCurrentFilterlist; 
				obj.Keys=obj.Commander.getMarkedKey;
				selected=obj.Filterlist.getSelected;
				obj.SendedEvents=selected;
				obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
				
				%set The calcType
				obj.CalcType=CalcType;
				
				switch obj.CalcType
					case 'Mcauto'
						obj.savePos='zmap-Mcauto-calc';
						basName='Mcauto-calc';
						
					case 'McEstimate'
						obj.savePos='zmap-McEstimate-calc';
						basName='McEstimate-calc';
					
					case 'McTime'
						obj.savePos='zmap-McTime-calc';
						basName='McTime-calc';
						
					case 'bdepth'
						obj.savePos='zmap-bdepth-calc';
						basName='bdepth-calc';
						
					case 'btime'
						obj.savePos='zmap-btime-calc';
						basName='btime-calc';
						
					case 'bmag'
						obj.savePos='zmap-bmag-calc';
						basName='bmag-calc';
				end
				
				
				%check if there are any saved calculations
				try
					PreviousCalc=obj.Datastore.getUserData(obj.savePos);
				catch
					PreviousCalc=[];
				end
				
				if ~isempty(PreviousCalc)
					 button = questdlg('There are existing Calculations in this catalog, do you want to load them?',...
								'Load Calculations?','Yes','No','No');
				else
					button = 'No';
				end
				
				
				if strcmp(button,'No');
				%The signal to start, this will be set to true if the parameter 
				%inputdlg is ended with "Ok"
				obj.StartCalc=false;
				
				%Init the needed zmap variables
				obj.InitZmapVar;
				
				%Init Grid if needed
				%obj.Gridit(true);
				
				%get CalcParameter
				obj.openCalcParamWindow;
				
				if obj.StartCalc
					obj.CalcName=basName;
					
					%Build the grid with new parameters if needed
					%obj.Gridit(false);
					
					%Calculation
					obj.doTheCalcThing;
					
					%Plotting
					obj.plot_bval;
				
				end
				
				else
					%Init the needed zmap variables (here done only because
					%of the paths);
					obj.InitZmapVar;
					
					obj.loadCalc(PreviousCalc);
					
					if obj.StartCalc
						obj.plot_bval;
					end
				end	
				
			end
		end



		%Methods in external files
		%-------------------------
		
		InitZmapVar(obj)
		
		Gridit(obj,initswitch)
        
		openCalcParamWindow(obj)
        
		doTheCalcThing(obj)

		plot_bval(obj)

		plot_modify(obj,bright,interswitch,circleGrid,overlay)

		adju(obj,asel, whichplot)

		universalCalc(obj,commandstring,newvariables)
		
		CalcAgain(obj)

		saveCalc(obj)

		loadCalc(obj,oldCalcs)

		exportResult(obj,FileType,Filename) 



	end
	
	%Additional static functions (not may be used here)
	%-------------------------------------------------
	methods (Static)
			TheDateString=decyear2string(decYR)
			
			decimalYear=string2decyear(instring)

			TheEvalString = UnpackStruct(TheStruct)

			CellData = UnpackStruct2Cell(TheStruct)

			TheStruct = PackVariable(variablelist)

			TheStruct = PackAllVariable()
		
	end

	
end

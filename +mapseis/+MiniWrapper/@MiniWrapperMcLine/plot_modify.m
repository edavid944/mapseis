function plot_modify(obj,bright,interswitch,circleGrid,overlay)
		%modifys the graphic
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		if ~isempty(bright)
			axes(hzma);
			brighten(bright);
		end
		
		if ~isempty(interswitch)
			obj.PackList=[obj.PackList; {'sha'}];
			switch interswitch
				case 'flat'
					axes(hzma);
					shading flat;
					sha='fl';
				case 'interp'
					axes(hzma);
					shading interp;
					sha='in';
			end		
		end
		
		if ~isempty(circleGrid)
			switch circleGrid
				case 'grid'
					hold on;
					plot(newgri(:,1),newgri(:,2),'+k');
				case 'circle'
					plotci2;
			
			end	
		end	
		
		if ~isempty(overlay)
			hold on;
			overlay;
			
		end
		
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		end
end
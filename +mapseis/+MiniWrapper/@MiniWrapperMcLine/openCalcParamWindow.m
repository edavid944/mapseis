function openCalcParamWindow(obj)
	%Here a window for the calculation parameters should be created
	%I suggest using the inputsdlg function 
	
	%and it will be used here.
	import mapseis.calc.Mc_calc.calc_Mc;
	
	
	%unpack again all the variables
	for i=1:numel(obj.PackList)
		%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
		tempoVal=obj.ZmapVariables.(obj.PackList{i});
		eval([obj.PackList{i},'=tempoVal;']);
	end
	
	%Needed Parameterlist
	%dim = [8 2];
	
	switch obj.CalcType
		case 'Mcauto'
			Title = 'Mc Input Parameter';
			Prompt={'Mc Method:', 'inpr1';...
				'Mc bootstraps:','bBst_button';...
				'Number of Bootstraps:','nBstSample';...
				'Mc Correction:','fMccorr';
				'Magnitude Bin Size:','fBinning'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';...
				'10: MBass'}; 
			
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 99];
			
			%bootstrap toggle
			Formats(2,1).type='check';
			
			%bootstrap number
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
						
			%Mc Correction
			Formats(3,1).type='edit';
			Formats(3,1).format='float';
			Formats(3,1).limits = [-99 99];
			Formats(3,1).size = [-1 0];
			Formats(3,2).limits = [0 1];
			Formats(3,2).type='none';
			
			%Mag Binning
			Formats(4,1).type='edit';
			Formats(4,1).format='float';
			Formats(4,1).limits = [-99 99];
			Formats(4,1).size = [-1 0];
			Formats(4,2).limits = [0 1];
			Formats(4,2).type='none';
			
	
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';			
			
			%default values
			defval =struct('inpr1',inpr1,...
				'bBst_button',bBst_button,...
				'nBstSample',nBstSample,...
				'fBinning',fBinning,...
				'fMccorr',fMccorr);
				
			%open the dialog window
			[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
			
			
			%Now the new parameters should be existing format them if needed
			%and save them in the datastore if needed
			
			
			%unpack from the structure
			inpr1=NewParameter.inpr1;
			inb2=inpr1; %just in case
			bBst_button=NewParameter.bBst_button;
			nBstSample=NewParameter.nBstSample;		
			fMccorr=NewParameter.fMccorr;
			fBinning=NewParameter.fBinning;
			
			
		case 'McEstimate'
			%no config needed
			Canceled=0;
		case {'McTime','btime'}
			Title = 'Mc Input Parameter';
			Prompt={'Mc Method:', 'nMethod';...
				'Sample Window Size:','nSampleSize';...
				'Overlap:','nOverlap';...
				'Number of Bootstraps:','nBstSample';...
				'Binning:','fBinning';...
				'Mc Correction: ','fMccorr';...
				'Smooth Plot:', 'nWindowSize';...
				'Minimum Number:', 'nMinNumberevents'};
			
			labelList2 =  {'1: Maximum curvature'; ...
				'2: Fixed Mc = minimum magnitude (Mmin)'; ...
				'3: Mc90 (90% probability)'; ...
				'4: Mc95 (95% probability)'; ...
				'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
				'6: EMR-method'; ...
				'7: Mc due b using Shi & Bolt uncertainty'; ...
				'8: Mc due b using bootstrap uncertainty'; ...
				'9: Mc due b Cao-criterion';
				'10: MBass'}; 
			
			%Mc Method
			Formats(1,1).type='list';
			Formats(1,1).style='popupmenu';
			Formats(1,1).items=labelList2;
			Formats(1,2).type='none';
			Formats(1,1).size = [-1 0];
			Formats(1,2).limits = [0 99];
			
			%Sample Window
			Formats(2,1).type='edit';
			Formats(2,1).format='integer';
			Formats(2,1).limits = [0 9999];
			
			%Overlap
			Formats(2,2).type='edit';
			Formats(2,2).format='integer';
			Formats(2,2).limits = [0 9999];
			
			
			%bootstrap number
			Formats(3,1).type='edit';
			Formats(3,1).format='integer';
			Formats(3,1).limits = [0 9999];
			
			%Binning
			Formats(3,2).type='edit';
			Formats(3,2).format='float';
			Formats(3,2).limits = [0 99];
			
			%Mc Correction
			Formats(4,1).type='edit';
			Formats(4,1).format='float';
			Formats(4,1).limits = [-99 99];
			
			%Smooth plot
			Formats(4,2).type='edit';
			Formats(4,2).format='integer';
			Formats(4,2).limits = [0 9999];
			
			Formats(5,1).type='edit';
			Formats(5,1).format='integer';
			Formats(5,1).limits = [0 9999];
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
	

			%default values
			defval =struct(	'nMethod',nMethod,...
					'nSampleSize',nSampleSize,...
					'nOverlap',nOverlap,...
					'nBstSample',nBstSample,...
					'fMccorr',fMccorr,...
					'fBinning',fBinning,...
					'nWindowSize',nWindowSize,...
					'nMinNumberevents',nMinNumberevents);
				
			%open the dialog window
			[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
			
			
			%Now the new parameters should be existing format them if needed
			%and save them in the datastore if needed
			
			
			%unpack from the structure
			nMethod=NewParameter.nMethod;
			nSampleSize=NewParameter.nSampleSize;
			nBstSample=NewParameter.nBstSample;
			nOverlap=NewParameter.nOverlap;
			fMccorr=NewParameter.fMccorr;	
			nMinNumberevents=NewParameter.nMinNumberevents;
			fBinning=NewParameter.fBinning;
			nWindowSize=NewParameter.nWindowSize;

			
		case 'bdepth'
			Title = 'b with depth input parameters';
			Prompt={'Number of events each window:', 'ni';...
				'Overlap factor:','ofac';...
				'Mc determination?','ButtonNr'};
			
			
			%bootstrap number
			Formats(1,1).type='edit';
			Formats(1,1).format='integer';
			Formats(1,1).limits = [0 9999];
			
			%Mc Correction
			Formats(2,1).type='edit';
			Formats(2,1).format='integer';
			Formats(2,1).limits = [0 9999];

			Formats(3,1).type='list';
			Formats(3,1).style='togglebutton';
			Formats(3,1).items={'Automatic','Fixed Mc=Mmin';}
			
			%%%% SETTING DIALOG OPTIONS
			Options.WindowStyle = 'modal';
			Options.Resize = 'on';
			Options.Interpreter = 'tex';
			Options.ApplyButton = 'off';
			
			
			%default values
			switch ButtonName,
			 	case 'Automatic'
			 		ButtonNr=1;
			 	case 'Fixed Mc=Mmin'	
			 		ButtonNr=2;
			end		
			defval =struct(	'ni',ni,...
					'ofac',ofac,...
					'ButtonNr',ButtonNr);
				
			%open the dialog window
			[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
						
			%Now the new parameters should be existing format them if needed
			%and save them in the datastore if needed
			
			%unpack from the structure
			ni=NewParameter.ni;
			ofac=NewParameter.ofac;
			
			if NewParameter.ButtonNr==1;
				ButtonName='Automatic';
			elseif NewParameter.ButtonNr==2;
				ButtonName='Fixed Mc=Mmin';
			end
			
		case 'bmag'
			%no parameters to set
			Canceled=0;
			
		
	end	
		
	if Canceled==1
		obj.StartCalc=false;
	else
		obj.StartCalc=true;
	end
	
	%Pack it again, Sam.
	for i=1:numel(obj.PackList)
		obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
	
	end
	
end
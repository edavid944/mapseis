function InitZmapVar(obj)
		%here all variable should be set used by a zmap calculation
		%They are saved in the structur ZmapVariables with there name 
		%as there fieldname.
		
		%warning off;
		
		%Set the packlist, the mentioned variables will be packed into the structure
		%They have to be defined in here or it will cause an error.
		obj.PackList={'fs';'hodo';'hoda';'p';'fs8';'fs10';'fs12';'fs14';'fs16';'ms6';'ms10';'ms12';...
				'ty';'ty1';'ty2';'ty3';'typele';'sel';'selector';'lth1';'lth15';'lth2';'wex';'wey';...
				'fipo';'r';'term';'welx';'wely';'winx';'winy';'rad';'ic';'ya0';'xa0';'iwl3';...
				'iwl2';'step';'ni';'name';'strib';'stri2';'ho';'ho2';'infstri';'maix';'maiy';...
				'tresh';'wi';'rotationangle';'fre';'c1';'c2';'c3';'cb1';'cb2';'cb3';'in';'ldx';...
				'tlap';'ca';'vi';'sha';'inb1';'inb2';'inda';'ra';'co';'par1';'minmag';'sys';...
				'cputype';'ve';'my_dir';'mess';'plt'};
		
		% set some of the paths from the zmap.m procedure, some may be deleted later
		r = ceil(rand(1,1)*21);
		fipo = get(0,'ScreenSize');holii = cd;
		fipo(4) = fipo(4)-150;
		term = get(0,'ScreenDepth');
		
		sys = computer;
		cputype = computer;


		ve = version;
		ve = str2num(ve(1:3));

		


		
		
		fs = filesep;
		hodi=[holii fs 'AddOneFiles' fs 'ZmapInBox'];
		hodo = [hodi fs 'out' fs];
		hoda = [hodi fs 'eq_data' fs];
		p = path;
		addpath([hodi fs 'myfiles'],[hodi fs 'src'],[hodi fs 'src' fs 'utils'],[hodi fs 'src' fs 'declus'],...
		[hodi fs 'src' fs 'fractal'], [hodi fs 'help'],[hodi fs 'dem'],[hodi fs 'zmapwww'],[hodi fs 'importfilters'],...
		[hodi fs  fs 'src' fs 'utils' fs 'eztool'],[hodi fs 'm_map'],[hodi fs 'src' fs 'pvals'],[hodi], [hodi fs 'src' fs 'synthetic'], ...
		[hodi fs 'src' fs 'movies'],[hodi fs 'src' fs 'danijel'],[hodi fs 'src' fs 'danijel' fs 'calc'],...
		[hodi fs 'src' fs 'danijel' fs 'ex'],[hodi fs 'src' fs 'danijel' fs 'gui'],...
		[hodi fs 'src' fs 'danijel' fs 'focal'],...
		[hodi fs 'src' fs 'danijel' fs 'plot'],[hodi fs 'src' fs 'danijel' fs 'probfore'],...
		[hodi fs 'src' fs 'jochen'], [hodi fs 'src' fs 'jochen' fs 'seisvar' fs 'calc'],...
		[hodi fs 'src' fs 'jochen' fs 'seisvar'], [hodi fs 'src' fs 'jochen' fs 'ex'],...
		[hodi fs 'src' fs 'thomas' fs 'slabanalysis'], [hodi fs 'src' fs 'thomas' fs 'seismicrates'],[hodi fs 'src' fs 'thomas' fs 'montereason'],...
		[hodi fs 'src' fs 'thomas' fs 'gui'],...
		[hodi fs 'src' fs 'jochen' fs 'plot'], [hodi fs 'src' fs 'jochen' fs 'stressinv'], [hodi fs 'src' fs 'jochen' fs 'auxfun'],...
		[hodi fs 'src' fs 'thomas'], ...
		[hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
		[hodi fs 'src' fs 'thomas' fs 'montereason'], ...
		[hodi fs 'src' fs 'thomas' fs 'etas'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
		[hodi fs 'src' fs 'thomas'], ...
		[hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
		[hodi fs 'src' fs 'thomas' fs 'montereason'], ...
		[hodi fs 'src' fs 'thomas' fs 'etas'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster'],...
		[hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
		[hodi fs 'src' fs 'juerg' fs 'misc'],...
		[hodi fs 'src' fs 'afterrate']);
		
		%variables from ini_zmap (some may be kicked later)
		
		% Set the font size
		%
		fs8 = 10;
		fs10 = 12;
		fs12 = 14;
		fs14 = 16;
		fs16 = 18;
		
		% Marker sizes
		ms6 = 3;
		ms10 = 10;
		ms12 = 12;
		
		% Marker type
		ty ='.';
		ty1 ='+';
		ty2 = 'o';
		ty3 ='x';
		typele = 'dep';
		sel  = 'ca';
		selector = 'ca'; %this was added because sel is a function, and there 
				%was a problem setting this again with eval
		
		
		% Line Thickness
		lth1 = 1.0;
		lth15 = 1.5;
		lth2 = 2.0;
		
		% set up Window size
		%
		% Welcome window
		wex = 80;
		wey = fipo(4)-380;
		welx = 340;
		wely = 300;
		
		% Map window
		%
		winx = 750;
		winy = 650;
		
		% Various setups
		%
		rad = 50.;
		ic = 0;
		ya0 = 0.;
		xa0 = 0.;
		iwl3 = 1.;
		iwl2 = 1.5;
		step = 3;
		ni = 100;
		
		name = [' '];
		strib = [' '];
		stri2 = [];
		ho ='noho';
		ho2 = 'noho';
		infstri = ' Please enter information about the | current dataset here';
		maix = [];
		maiy = [];
		
		
		% Initial Time setting
		
		% Tresh is the radius in km below which blocks 
		% in the zmap's will be plotted 
		% 
		tresh = 50;
		wi = 10 ;   % initial width of crossections
		rotationangle = 10; % initial rotation angle in cross-section window
		fre = 0;
		
		
		% set the background color (c1 = red, c2 = green, c3 = blue)
		% default: light gray 0.9 0.9 0.9 
		c1 = 0.9;
		c2 = 0.9;
		c3 = 0.9;
		
		% Set the Background color for the plot 
		% default \: light yellow 1 1 0.6
		cb1 = 1.0;
		cb2 = 1.0;
		cb3 = 1.0;	
		
		in = 'initf';
		
		% seislap default para,eters
		ldx = 100;
		tlap = 100;
		
		ca = 1;
		vi ='on';
		sha ='fl';
		inb1=1;inb2=1;
		inda = 1;
		ra = 5;
		
		co = 'w';
		par1 = 14;
		minmag = 8;
		
		
		%dummy figure
		figure;
		set(gcf,...  
			'Name','Message Window',...
			'NumberTitle','off', ...
			'MenuBar','none', ...
			'Visible','off', ...
			'Position',[ wex wey welx wely ],...
			'visible','on');

		mess=gcf;
		
		set(mess,'visible','off');
		%set the recursion slightly, to avoid error (specialy with the function ploop2.m
		%set(0,'RecursionLimit',750)
		%-----------------------------------------------------------------
		
		
		%I set plt to 'plo2' this topography is available in the ZmapInBox folder and should 
		%work, it can be changed if another type is wanted and available.
		plt='lo2';
		%use lo2 instead, seems like this is for loading, and it cannot be created first
		
		
		
		%Add the raw data some are needed in some function, coastline may added later
		main = [];
		mainfault = [];
		coastline = [];
		well = [];
		stat = [];
		faults = [];
		
		
		%try building the coastline
		try
			rawcoast=obj.Datastore.getUserData('Coast_PlotArgs');
			coastline(:,1)=rawcoast{1};
			coastline(:,2)=rawcoast{2};
		catch
			disp('no coastline available');
			coastline = [];
		end	
		
		
		obj.PackList=[obj.PackList; {'main';'mainfault';'coastline';'well';'stat';'faults'}];
		
		%variables from zmap.m
		my_dir = hodi;
		
		
		%variables from other functions needed here
		

		% Default value Mcauto
		nBstSample = 100;
		fMccorr = 0;
		fBinning = 0.1;
		bBst_button=0;
		inpr1=1;
		
		
		%Mc estimate		
		dat = []; 

		
		
		%McTime	& b with time 			
		nSampleSize = 500;
		nOverlap = 4;
		nMethod = 1;
		
		if any(strcmp(obj.CalcType,{'btime','McTime'}))
			nBstSample = 200;
		end
		
		nMinNumberevents = 50;
		%fBinning = 0.1;
		nWindowSize = 5;
		%fMcCorr = 0;
		selt= 'ca';
		
		%selt = 'in'; sPar = 'mc'; %mc
		%selt ='in'; sPar = 'b'; %b
		
		
		%b with depth
		BV = [];
		BV3 = [];
		mag = [];
		me = [];
		av2=[];
		fs12 = 10;
		Nmin = 50;
		ni = 150;
		ofac = 5;
		ButtonName='Automatic';
		
		%b with mag
		%BV = [];
		%BV3 = [];
		%mag = [];
		%me = [];
		%av2=[];
		%fs12 = 10;
		
		if strcmp(obj.CalcType,'bmag')
			Nmin = 20;
		end
		
		%update packlist for the additional variables
		obj.PackList=[obj.PackList; {'nBstSample';'fMccorr';'fBinning';'dat';'nSampleSize';'nOverlap';'nMethod';...
					'nMinNumberevents';'nWindowSize';'selt';'BV';'BV3';'mag';'me';'av2';'Nmin';'ni';...
					'bBst_button';'ofac';'ButtonName';'inpr1'}];
		
		%Pack da shit
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
		
		end
		
end
	

function CalcAgain(obj)
		%redo the calculation
		
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		obj.StartCalc=false;
		
		
		%get parameters
		obj.openCalcParamWindow;
		
		if obj.StartCalc
			
			figNr=findobj('Name','b-value-map');
			
			if ~isempty(figNr)
				close(figNr);
			end
		
			%refresh catalogs
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			%Build the grid with new parameters if needed
			%obj.Gridit(false);
			
			%Calculation
			obj.doTheCalcThing;
			
			%Plotting
			obj.plot_bval;
		end
	
	
end
function loadCalc(obj,oldCalcs)
		%asks if any of the old calcs should be loaded and loads it or build new one
		import mapseis.datastore.*;
		import mapseis.export.Export2Zmap;
		
		Title = 'Select Calculation';
		Prompt={'Which Calculation:', 'WhichOne';...
			'Overwrite FilterList?','OverWriteFilter'};
		
		AvailCalc=oldCalcs(:,1);			
	
	
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
		
		%Overwrite
		Formats(2,2).type='list';
		Formats(2,2).style='togglebutton';
		Formats(2,2).items={'Yes','No'};
		Formats(2,2).size = [-1 0];
		
		Formats(2,3).limits = [0 1];
		Formats(2,3).type='none';
		Formats(2,1).limits = [0 -1];
		Formats(2,1).type='none';
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';

		
		
		%default values
		defval =struct('WhichOne',1,...
				'OverWriteFilter',2);
			
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
		if Canceled==1
			obj.StartCalc=false;
		else
			obj.StartCalc=true;
		end
		
		if obj.StartCalc
			TheCalc=oldCalcs(NewParameter.WhichOne,:);
			
			%Overwrite Filter 
			if NewParameter.OverWriteFilter==1
				Keys=obj.Commander.getMarkedKey;
				FilterKey=Keys.ShownFilterID;
				
				obj.Commander.pushWithKey(TheCalc{3},FilterKey);
				obj.Commander.switcher('Both');
				
			else %Create new filter
				newdata.Filterlist=TheCalc{3};
				obj.Commander.pushIt(newdata);
				obj.Commander.switcher('Both');
			end
			
			%Now set the data
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.ZmapCatalog = Export2Zmap(obj.Datastore,selected);
			
			obj.ZmapVariables=TheCalc{2};
			obj.PackList=fields(obj.ZmapVariables);
			
			obj.CalcName=TheCalc{1};
		end
		
end
function doTheCalcThing(obj)
		%everything for the calculation itself goes in here
		
		
		import mapseis.calc.Mc_calc.*;
		
		%unpack again all the variables
		for i=1:numel(obj.PackList)
			%assignin('caller',obj.PackList{i},obj.ZmapVariables.(obj.PackList{i})); 
			tempoVal=obj.ZmapVariables.(obj.PackList{i});
			eval([obj.PackList{i},'=tempoVal;']);
		end
		
		a=obj.ZmapCatalog;
		newt2=a;
		%ra=rad;
		%now the code from bvalgrid
		%all used functions and procedures should be in the ZmapinBox folder and existing
		
		%========
		
		

		
		
		switch obj.CalcType
			case 'Mcauto'
			    maxmag = ceil(10*max(newt2(:,6)))/10;
			    mima = min(newt2(:,6));
			    if mima > 0 ; mima = 0 ; end;
			
			
			    %%
			    %
			    % bval contains the number of events in each bin
			    % bvalsum is the cum. sum in each bin
			    % bval2 is number events in each bin, in reverse order
			    % bvalsum3 is reverse order cum. sum.
			    % xt3 is the step in magnitude for the bins == .1
			    %
			    %%
			
			    [bval,xt2] = hist(newt2(:,6),(mima:fBinning:maxmag));
			    bvalsum = cumsum(bval); % N for M <=
			    bval2 = bval(length(bval):-1:1);
			    bvalsum3 = cumsum(bval(length(bval):-1:1));    % N for M >= (counted backwards)
			    xt3 = (maxmag:-fBinning:mima);
			
			    backg_ab = log10(bvalsum3);
			
			
			    %%
			    % Estimate the b value
			    %
			    % calculates max likelihood b value(bvml) && WLS(bvls)
			    %
			    %%
			
			    %% SET DEFAULTS TO BE ADDED INTERACTIVLY LATER
			    %Nmin = 10;
			
			    bvs=newt2;
			    b=newt2;
			
			    %% enough events??
			    if length(bvs(:,6)) >= Nmin
				% Added to obtain goodness-of-fit to powerlaw value
				%mcperc_ca3_org;
				[magco prf Mc90 Mc95]=mcperc_ca3(newt2);
				
				[fMc] = calc_Mc(bvs, inpr1, fBinning, fMccorr);
				l = bvs(:,6) >= fMc-(fBinning/2);
				disp(sum(l))
				disp(fMc)
				disp(fBinning/2)
				if length(bvs(l,:)) >= Nmin
				    [fMeanMag, fBValue, fStd_B, fAValue] =  calc_bmemag(bvs(l,:), fBinning);
				else
				    fMc = nan; fBValue = nan; fStd_B = nan; fAValue= nan;
				    disp('No b-value calculated')
				end;
				% Set standard deviation of a-value to nan;
				fStd_A= nan; fStd_Mc = nan;
			
				% Bootstrap uncertainties
				if bBst_button == 1
				    % Check Mc from original catalog
				    l = bvs(:,6) >= fMc-(fBinning/2);
				    if length(bvs(l,:)) >= Nmin
					[fMc, fStd_Mc, fBValue, fStd_B, fAValue, fStd_A, vMc, mBvalue] = calc_McBboot(bvs, fBinning, nBstSample, inpr1, Nmin, fMccorr);
				    else
					fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A= nan;
				    end;
				else
				    % Set standard deviation of a-value to nan;
				    fStd_A= nan; fStd_Mc = nan;
				end; % of bBst_button
			    else
				fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A = nan;
				fStdDevB = nan;
				fStdDevMc = nan;
			    end;% of if length(bvs) >= Nmin
			    %%
			    % calculate limits of line to plot for b value line
			    %%
			    % For ZMAP
			    magco = fMc;
			    index_low=find(xt3 < magco+.05 & xt3 > magco-.05);
			
			    mag_hi = xt3(1);
			    index_hi = 1;
			    mz = xt3 <= mag_hi & xt3 >= magco-.0001;
			    mag_zone=xt3(mz);
			
			    y = backg_ab(mz);
			
			   
			
			    %%
			    % Set to correct method, maximum like or least squares
			    %%
			    if bBst_button == 0
				sol_type = 'Maximum Likelihood Solution';
				bw=fBValue;%bvml;
				aw=fAValue;%avml;
				ew=fStd_B;%stanml;
				%     end
			    else
				sol_type = 'Maximum Likelihood Estimate, Uncertainties by bootstrapping';
				bw=fBValue;
				aw=fAValue;
				ew=fStd_B;
			    end; %bBst_button
			    %%
			    % create and draw a line corresponding to the b value
			    %%
			    
			    %p = [ -1*bw aw];
			    p = [ -1*bw aw];
			    %[p,S] = polyfit(mag_zone,y,1);
			    f = polyval(p,mag_zone);
			    f = 10.^f;
			    
			    std_backg = ew;      % standard deviation of fit
			
			    %%
			    % Error Bar Calculation -- call to pdf_calc.m
			    %%
			
			   
			
			
			    p=-p(1,1);
			    p=fix(100*p)/100;
			    tt1=num2str(bw,3);
			    tt2=num2str(std_backg,1);
			    %not used anymore
			    %tt4=num2str(bv,3);
			    %tt5=num2str(si,2);
			
			
			    tmc=num2str(magco,2);
			
			    rect=[0 0 1 1];
			    h2=axes('position',rect);
			    set(h2,'visible','off');
			
			    a0 = aw-log10(max(b(:,3))-min(b(:,3)));
			
			    %Change it for each calc
			    obj.PackList=[obj.PackList; {'maxmag';'mima';'bval';'xt2';'bvalsum';'bval2';'bvalsum3';...
					'xt3';'backg_ab';'l';'index_low';'mag_zone';'tt1';'tt2';'f';'b';'sol_type';...
					'tmc';'fBValue';'fStd_B';'aw';'a0';'newt2';'fStd_Mc'}];
			    
				
			case 'McEstimate'
			    	import mapseis.calc.synth_catalogs.*;
			    	import mapseis.calc.b_val_calc.*;
			    	
			    	%disp(['inb1: ',num2str(inb1)])
				[bv magco0 stan av me mer me2 pr] =  bvalca3(newt2,inb1);


				dat = []; 
				
				for i = (magco0-0.9):0.1:(magco0+1.5)
				    i
				   l = newt2(:,6) >= (i - 0.0499); 
				   nu = length(newt2(l,6));
				   %[bv magco stan av] =  bvalca3(newt2(l,:),2,2);
				  
				   [mw bv2 stan2 av] =  bmemag(newt2(l,:));
				   %disp(['mw: ',num2str(mw)])
				   %disp(['bv2: ',num2str(bv2)])
				   %disp(['stan2: ',num2str(stan2)])
				   %disp(['av: ',num2str(av)])
				   %synthb_aut
				   resCatalog = synthb_aut(newt2,bv2,i,l);
				   %res0 = res; 
				   res0=resCatalog;
				   xt2=[];
				  % bv = bv + stan ; synthb_aut; res1 = res; 
				  % bv = bv - 2*stan ; synthb_aut; res2 = res;
				
				   nc = 10.^(av - bv2*(i+0.05)) ;
				   nc1 = 10.^(av - (bv2-stan/2)*(i+0.05)) ;
				   nc2 = 10.^(av - (bv2+stan/2)*(i+0.05)) ;
				
				   dat = [ dat ; i nc nu nu/nc nu/nc1 nu/nc2 res0  ];
				   %display(['Completeness Mc: ' num2str(i) ';  rati = ' num2str(nu/nc)]); 
				
				end
				
				
				disp(min(dat(:,7)))
				j =  min(find(dat(:,7) < 10 )); 
				if isempty(j)
					Mc90 = nan; 
				else; 
				   Mc90 = dat(j,1); 
				end
				   
				j =  min(find(dat(:,7) < 5 )); 
				
				if isempty(j) 
					Mc95 = nan; 
				else; 
				   Mc95 = dat(j,1); 
				end
				
				display(['Completeness Mc at 90% confidence: ' num2str(Mc90) ]); 
				display(['Completeness Mc at 95% confidence: ' num2str(Mc95) ]); 
				
				
				obj.PackList=[obj.PackList; {'dat';'Mc90';'Mc95';'xt2';'bv';'magco0';'stan';...
					'av';'newt2'}];



				
			
			case 'McTime'
			
				if (obj.ParallelMode)
					matlabpool open;
			
				end
			
				% Set minimum number to number of moving window
				%nMinNumberevents = nSampleSize;
				% Calulate time series
				[mResult] = calc_McBwtime(newt2, nSampleSize, nOverlap, nMethod, nBstSample, nMinNumberevents, fBinning,fMccorr,obj.ParallelMode);
				    
				% Plot Mc time series
				%   if sPar == 'mc'
				
				mMc = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,2));
				mMc(1:nWindowSize,1)=mResult(1:nWindowSize,2);
				mMcstd1 = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,3));
				mMcstd1(1:nWindowSize,1)=mResult(1:nWindowSize,3);
					
				
				obj.PackList=[obj.PackList; {'mResult';'mMc';'mMcstd1';'newt2'}];
				
				
				
				
			case 'bdepth'
				
				[s,is] = sort(newt2(:,7));
				newt1 = newt2(is(:,1),:) ;
				watchon;
				allcount = 0.;
				
				
				itotal=numel(1:ni/ofac:length(newt1)-ni);
				
				if ~obj.ParallelMode
					wai = waitbar(0,' Please Wait ...  ');
					set(wai,'NumberTitle','off','Name','b-value with depth - percent done');;
					drawnow
					
					for t = 1:ni/ofac:length(newt1)-ni
					    allcount=allcount+1;
					    % calculate b-value based an weighted LS
					    b = newt1(t:t+ni,:);   
					    
					    switch ButtonName,
					    case 'Automatic'
						
					    	[magco prf Mc90 Mc95] = mcperc_ca3(b);  
						
						
						if isnan(Mc95) == 0 ; 
						    magco = Mc95; 
						elseif isnan(Mc90) == 0 ; 
						    magco = Mc90; 
						else 
						    [bv magco stan av me mer me2 pr] =  bvalca3(b,1,1); 
						end   
					    case 'Fixed Mc=Mmin'
						magco = min(newt1(:,6))
					    end
					    
					    l = b(:,6) >= magco-0.05; 
					    if length(b(l,:)) >= Nmin
						%[bv magco0 stan av me mer me2 pr] =  bvalca3(b(l,:),2,2); 
						[mea bv stan av] =  bmemag(b(l,:));
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV = [BV ; bv newt1(t,7) ; bv newt1(t+ni,7) ; inf inf];
					    BV3 = [BV3 ; bv newt1(t+round(ni/2),7) stan ];
					    %mag = [ mag ; av newt1(t+round(ni/2),7)];
					    
					    % calculate b-value based on maximum likelihood
					    %av2 = [ av2 ;   av  newt1(t+round(ni/2),7) stan bv];
					    
					    waitbar(allcount/itotal);
					end
					close(wai);
				
				else
					%Go Parallel
					NumberArray=1:ni/ofac:length(newt1)-ni;
					
					%if (obj.ParallelMode)
					%	matlabpool open;
					%end
					
					for i = 1:numel(NumberArray)
					    
					    t=NumberArray(i);
					    
					    % calculate b-value based an weighted LS
					    b = newt1(t:t+ni,:);   
					    
					    switch ButtonName,
						    case 'Automatic'
							mcperc_ca3;  
							if isnan(Mc95) == 0 ; 
							    magco = Mc95; 
							elseif isnan(Mc90) == 0 ; 
							    magco = Mc90; 
							else 
							    [bv magco stan av me mer me2 pr] =  bvalca3(b,1,1); 
							end   
						    case 'Fixed Mc=Mmin'
							magco = min(newt1(:,6))
					    end
					    
					    l = b(:,6) >= magco-0.05; 
					    if length(b(l,:)) >= Nmin
						%[bv magco0 stan av me mer me2 pr] =  bvalca3(b(l,:),2,2); 
						[mea bv stan av] =  bmemag(b(l,:));
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV(i,:) = [bv newt1(t,7) ; bv newt1(t+ni,7) ; inf inf];
					    BV3(i,:) = [bv newt1(t+round(ni/2),7) stan ];
					    %mag = [ mag ; av newt1(t+round(ni/2),7)];
					    
					    % calculate b-value based on maximum likelihood
					    %av2 = [ av2 ;   av  newt1(t+round(ni/2),7) stan bv];
					    
					    
					end
				
				
				end
				
				
				watchoff
				
				obj.PackList=[obj.PackList; {'newt2'}];
				
			
			
				
			case 'btime'
				% Set minimum number to number of moving window
				%nMinNumberevents = nSampleSize;
				% Calulate time series
				
				if (obj.ParallelMode)
					matlabpool open;
					
				end
				[mResult] = calc_McBwtime(newt2, nSampleSize, nOverlap, nMethod, nBstSample, nMinNumberevents, fBinning,fMccorr,obj.ParallelMode);
				
				mB = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,4));
				mB(1:nWindowSize,1)=mResult(1:nWindowSize,4);
				mBstd1 = filter(ones(1,nWindowSize)/nWindowSize,1,mResult(:,3));
				mBstd1(1:nWindowSize,1)=mResult(1:nWindowSize,3);
				
				obj.PackList=[obj.PackList; {'mResult';'mB';'mBstd1';'newt2'}];
			
			
				
				
			case 'bmag'
				
				[s,is] = sort(newt2(:,6));
				newt1 = newt2(is(:,1),:) ;
				watchon;
				itotal=numel(min(newt1(:,6)):0.1:max(newt1(:,6)));
				allcount=0;
				
				
				if ~obj.ParallelMode
					wai = waitbar(0,' Please Wait ...  ');
					set(wai,'NumberTitle','off','Name','b-value with Magnitude - percent done');;
					drawnow
					
					for t = min(newt1(:,6)):0.1:max(newt1(:,6));
					    allcount=allcount+1;
					    
					    % calculate b-value based an weighted LS
					    l = newt1(:,6) >= t -0.05;
					    b = newt1(l,:);   
					    
					    if length(b(:,1)) >= Nmin
						[bv magco stan av me mer me2 pr] =  bvalca3(b,2,2); 
						[mea bv stan2 av] =  bmemag(b);
					
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV3 = [BV3 ; bv t stan ];
					    
					    
					     waitbar(allcount/itotal);
					end
					
					close(wai);
				else
					%Go Parallel
					NumberArray=min(newt1(:,6)):0.1:max(newt1(:,6));
					
					%if (obj.ParallelMode)
					%	matlabpool open;
					%end
					
					for i = 1:numel(NumberArray)
					    t=NumberArray(i);
					    
					    % calculate b-value based an weighted LS
					    l = newt1(:,6) >= t -0.05;
					    b = newt1(l,:);   
					    
					    if length(b(:,1)) >= Nmin
						[bv magco stan av me mer me2 pr] =  bvalca3(b,2,2); 
						[mea bv stan2 av] =  bmemag(b);
					
					    else 
						bv = nan; bv2 = nan, magco = nan; av = nan; av2 = nan; 
					    end
					    BV3(i,:) = [bv t stan ];
					    
					    
					    
					end
					
				
				end
				
				watchoff
				
				
				obj.PackList=[obj.PackList; {'newt2'}];
			
			
				
		end
		
		
		
		%Change it for each calc
		%obj.PackList=[obj.PackList; {'normlap2';'mMc';'mStdMc';'mBvalue';'mStdB';'mAvalue';'mStdA';...
		%			'Prmap';'ro';'mStdDevB';'fStdDevB';'mStdDevMc';'mNumEq';'kll';...
		%			're3';'old';'t0b';'teb';'newt2'}];
		    
		%Pack it again, Sam.
		for i=1:numel(obj.PackList)
			obj.ZmapVariables.(obj.PackList{i})=eval(obj.PackList{i});
			
		end
		    
		if (obj.ParallelMode)
			try
				matlabpool close;
			end
		end

		

		
end
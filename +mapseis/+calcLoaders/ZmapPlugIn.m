function ObjectOut = ZmapPlugIn(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)

%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;


switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'This Plugin allows to exchange data with zmap in realtime.';
    	ObjectOut.displayname = 'Zmap in a Box' ;
    	ObjectOut.Category = 'Plugins'; 
    	ObjectOut.SubCategory = [];
    	ObjectOut.CalcType = 'Util';
    	ObjectOut.ResultGUIVersion = 'none';
    	ObjectOut.ParameterGUIVersion = 'none';
    
	case '-start'
	Plugin=ZmapInBoxWrapper(MainGUI.ListProxy,MainGUI.CommanderGUI,true);
	ObjectOut= 	struct(	'PlugIn',Plugin);
	
	%start zmap
	Plugin.StartZmap;
	
	%send catalog
	Plugin.SendToZmap;
	

end



end
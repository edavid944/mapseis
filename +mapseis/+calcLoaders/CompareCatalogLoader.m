function ObjectOut = CompareCatalogLoader(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too

%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;
import mapseis.MiniWrapper.*;
import mapseis.gui.*;

switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'The catalog comparison tool, similar to the old stand alone version';
    	ObjectOut.displayname = 'Compare Catalogs' ;
    	ObjectOut.Category = 'Calculation';
    	ObjectOut.SubCategory = [];
    	ObjectOut.CalcType = 'Diverse';
    	ObjectOut.ResultGUIVersion = '0.0';
    	ObjectOut.ParameterGUIVersion = '0.0';
    
	case '-start'
	ObjectOut.CalcWrapper=CompareGUI_Com('Compare Catalogs',MainGUI.CommanderGUI,MainGUI.ListProxy);
	ObjectOut.ResultGUI=[];
	ObjectOut.CalcParamGUI=[];
end



end
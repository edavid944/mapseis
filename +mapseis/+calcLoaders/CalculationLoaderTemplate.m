function ObjectOut = CalculationLoaderTemplate(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)

%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;
import mapseis.MiniWrapper.*;

switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'This is only a Template for building a Calculation Loader, DO NOT USE IT.';
    	ObjectOut.displayname = 'Calculation Template' ;
    	ObjectOut.Category = 'Template';
    	ObjectOut.SubCategory = [];
    	ObjectOut.CalcType = 'Template';
    	ObjectOut.ResultGUIVersion = '0.0';
    	ObjectOut.ParameterGUIVersion = '0.0';
    
	case '-start'
	

end



end
function ObjectOut = ScriptPlugIn(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)


%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;


switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'This Plugin allows to specify and run a script. The scripts only get the Commander as input, which should be enough';
    	ObjectOut.displayname = 'Run Script...' ;
    	ObjectOut.Category = 'Plugins';
    	ObjectOut.SubCategory = [];
    	ObjectOut.CalcType = 'Util';
    	ObjectOut.ResultGUIVersion = 'none';
    	ObjectOut.ParameterGUIVersion = 'none';
    
	case '-start'
	currDir=pwd;
	try
		cd('./MapSeis_Scripts');
	end
	
	[fName,pName] = uigetfile('*.m','Enter MapSeis Scriptname...');
	if isequal(fName,0) || isequal(pName,0)
               		disp('No filename selected. ');
               		return
        else
        
        	
        	cd(pName);
        	[pathStr,nameStr,extStr] = fileparts(fName);
        	ScriptHandle=str2func(nameStr);
        	cd(currDir);
		Result=ScriptHandle(MainGUI.CommanderGUI);
		ObjectOut= 	struct(	'Script',Result);

	end
	cd(currDir);
end



end
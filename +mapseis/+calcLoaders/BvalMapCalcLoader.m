function ObjectOut = BvalMapCalcLoader(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)


%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;
import mapseis.MiniWrapper.*;

switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'Version 1.5 of the b-val, Mc mapping tool';
    	ObjectOut.displayname = 'B-value, Mc mapping' ;
    	ObjectOut.Category = 'Calculation'; 
    	ObjectOut.SubCategory = 'B-value/Mc';
    	ObjectOut.CalcType = 'Map';
    	ObjectOut.ResultGUIVersion = 'miniwrapper';
    	ObjectOut.ParameterGUIVersion = 'miniwrapper';
    
	case '-start'
	
	ObjectOut.CalcWrapper=McBvalMappingWrapper(MainGUI.ListProxy,MainGUI.CommanderGUI,true,'2Dmap',[]);
	ObjectOut.ResultGUI=[];
	ObjectOut.CalcParamGUI=[];
end



end

function ObjectOut = RateCompareLoader(MainGUI,CallOption)
%The loader for the first calculation  done in Mapseis, the RateCompare Calculation

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)


%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;


switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'The first Calculation done for Mapseis a comparsion of the rates';
    	ObjectOut.displayname = 'Compare Rate' ;
    	ObjectOut.Category = 'Calculation'; 
    	ObjectOut.SubCategory = [];
    	ObjectOut.CalcType = 'Diverse';
    	ObjectOut.ResultGUIVersion = '1.0';
    	ObjectOut.ParameterGUIVersion = '1.0';
    
    
    
	case '-start'
	
	%Get Commander
	Commander=MainGUI.CommanderGUI;
	
	%get ListProxy
	ListProxy=MainGUI.ListProxy;
	
	%Determine Position of the window
	%(at the moment use screensize, but might be changed later)
	scnsize = get(0,'ScreenSize');
	WindowPos=[round(scnsize(3)*0.2),round(scnsize(4)*0.2)];
	ParamPos=[WindowPos(1)+1024,WindowPos(2)+768];
	
	Commander.AutoUpdate('off');
	
	%Create Calculation
	CalcWrapper=RateCompareWrapper(ListProxy,Commander,true);
	
	%create ResultGUI
	ResGUI=ResultGUILoader(CalcWrapper,'Compare Rate Result',WindowPos,ListProxy,[]);
	
	set(ResGUI.TheGUI,'DeleteFcn',@(s,e) Commander.AutoUpdate('on'));
	
	%create CalcParamGUI
	CalcParamGUI= CalcParamGUILoader(CalcWrapper,'Comparer Rate Parameter',ParamPos,ListProxy,[]);
	
	set(CalcParamGUI.TheGUI,'DeleteFcn',@(s,e) Commander.AutoUpdate('on'));
	
	%Return Objects
	ObjectOut= 	struct(	'CalcWrapper',CalcWrapper,...
				'ResultGUI',ResGUI,...
				'CalcParamGUI',CalcParamGUI);
				
				
end



end
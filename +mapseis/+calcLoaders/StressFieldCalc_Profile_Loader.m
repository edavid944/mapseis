function ObjectOut = StressFieldCalc_Profile_Loader(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)


%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;
import mapseis.MiniWrapper.*;

switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'First version of the stress inversion modul with traditional Michael Method and SATSI method. Depth Profile Calculation';
    	ObjectOut.displayname = 'Stress field inversion (Depth Profile)' ;
    	ObjectOut.Category = 'Calculation'; 
    	ObjectOut.SubCategory = 'Stress';
    	ObjectOut.CalcType = 'Profile';
    	ObjectOut.ResultGUIVersion = 'miniwrapper';
    	ObjectOut.ParameterGUIVersion = 'miniwrapper';
    
	case '-start'
	
	ObjectOut.CalcWrapper=StressFieldCalcWrapper(MainGUI.ListProxy,MainGUI.CommanderGUI,true,true,[]);
	ObjectOut.ResultGUI=[];
	ObjectOut.CalcParamGUI=[];
end



end
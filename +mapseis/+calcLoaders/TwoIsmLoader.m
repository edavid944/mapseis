function ObjectOut = TwoIsmLoader(MainGUI,CallOption)
%The loader for the first calculation  done in Mapseis, the RateCompare Calculation

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)

%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;

switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'Allows to compare two catalog and filterlist in a FMD plot';
    	ObjectOut.displayname = 'Dual FMD' ;
    	ObjectOut.Category = 'Calculation'; 
    	ObjectOut.SubCategory = [];
    	ObjectOut.CalcType = 'Diverse';
    	ObjectOut.ResultGUIVersion = '1.0';
    	ObjectOut.ParameterGUIVersion = '1.0';
    
    
    
	case '-start'
	
	%Get Commander
	Commander=MainGUI.CommanderGUI;
	
	%get ListProxy
	ListProxy=MainGUI.ListProxy;
	
	%Determine Position of the window
	%(at the moment use screensize, but might be changed later)
	%scnsize = get(0,'ScreenSize');
	%WindowPos=[round(scnsize(3)*0.2),round(scnsize(4)*0.2)];
	%ParamPos=[WindowPos(1)+1024,WindowPos(2)+768];
	
	%Create Calculation
	ObjectOut=TwoIsm_GUI('Dual FMD',Commander,ListProxy);
	
	
				
				
end



end
function ObjectOut = MiniMcAutoLoader(MainGUI,CallOption)
% This a Template for a CalculationLoader, this files are needed to automatically 
% Add the a calculation to the main GUI

%The CalcType can be 'Map','3D','Profile','Time','MultiDimensional', 'Util' or 'Diverse'
%some will might be used later (I might add some if needed too)

%% typical Folders for the wrappers 
import mapseis.util.gui.*;
import mapseis.LineCalcWrapper.*;
import mapseis.MapCalcWrapper.*;
import mapseis.SpecialWrapper.*;
import mapseis.MiniWrapper.*;

switch CallOption
	case '-description'
          %new option: returns a description of the Calculation and the menu names
    	ObjectOut.description = 'A first test of the mini wrapper, this function replicates the function of bvalgrid';
    	ObjectOut.displayname = 'Mc automatic' ;
    	ObjectOut.Category = 'ZmapCalculations'; 
    	ObjectOut.SubCategory = 'Mc and b-value estimation';
    	ObjectOut.CalcType = 'Diverse';
    	ObjectOut.ResultGUIVersion = 'miniwrapper';
    	ObjectOut.ParameterGUIVersion = 'miniwrapper';
    
	case '-start'
	
	ObjectOut=MiniWrapperMcLine(MainGUI.ListProxy,MainGUI.CommanderGUI,'Mcauto',false);

end



end
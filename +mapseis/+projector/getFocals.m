function [focalSelected,focalUnselected] = getFocals(dataStore,rowIndices)
% getFocals : Magnitude projector.   Extracts focal mechanism if available


% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell


import mapseis.projector.*;

if nargin<2
    rowIndices = true(dataStore.getRowCount,1);
end
try
	focalSelected = focalProj(dataStore.getFields({'focal_strike','focal_dip','focal_rake'},rowIndices));
	focalUnselected = focalProj(dataStore.getFields({'focal_strike','focal_dip','focal_rake'},~rowIndices));
catch
	%Focal mechanism do not always exist in a catalog
	focalSelected = ones(sum(rowIndices),3)*NaN;
	focalUnselected = ones(sum(~rowIndices),3)*NaN;
end	

end

function focalArray = focalProj(s)
% MagProj : Magnitude projector.  Extracts event magnitude as Nx1 array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell

if isempty(s)
    focalArray=[NaN NaN NaN];
else
    focalArray = [s.focal_strike s.focal_dip s.focal_rake];
end

end

function hypoArray = HypoProj(s)
% HypoProj : Location projector.  Extracts event lon and lat as Nx2 array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if isempty(s)
    hypoArray=zeros(0,3);
else
    hypoArray = [s.lon s.lat s.depth];
end

end
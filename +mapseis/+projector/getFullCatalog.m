function [CatSelect,CatUnselected] = getFullCatalog(dataStore,rowIndices,PaleRider)
	% just returns the whole catalog as structure, it is just a passthrought functio
	
	
	import mapseis.projector.*;
	
	if nargin<3
	    PaleRider=false;
	end
	
	if nargin<2
	    rowIndices = true(dataStore.getRowCount,1);
	    PaleRider=false;
	end
	
	[CatSelect,CatUnselected]=dataStore.getFields([],rowIndices);
	
	if dataStore.UseShift&~PaleRider
		[locSelected locUnselected]=getShiftedData(dataStore,rowIndices);
		CatSelect.lon=locSelected(:,1);
		CatSelect.lat=locSelected(:,2);
		CatUnselected.lon=locUnselected(:,1);
		CatUnselected.lat=locUnselected(:,2);
	end


end
function [timeSelected,timeUnselected] = getDecYear(dataStore,rowIndices)
% similar to getTimes but returns decimal years and uses the userdata buffer


% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 77 $    $Date: 2008-10-20 16:56:26 +0100 (Mon, 20 Oct 2008) $
% Author: Matt McDonnell $ Edited: David Eberhard

import mapseis.projector.*;

if nargin<2
    rowIndices = true(dataStore.getRowCount,1);
end

DecYear=dataStore.getUserData('DecYear');

timeSelected = DecYear(rowIndices);
timeUnselected = DecYear(~rowIndices);


end
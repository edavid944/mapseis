function [magSelected,magUnselected] = getMagnitudes(dataStore,rowIndices)
% getLocations : Magnitude projector.   Extracts event magnitude as Nx1
% array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell

import mapseis.projector.*;

if nargin<2
    rowIndices = true(dataStore.getRowCount,1);
end

magSelected = MagProj(dataStore.getFields({'mag'},rowIndices));
magUnselected = MagProj(dataStore.getFields({'mag'},~rowIndices));

end

function magArray = MagProj(s)
% MagProj : Magnitude projector.  Extracts event magnitude as Nx1 array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell

if isempty(s)
    magArray=[];
else
    magArray = [s.mag];
end

end
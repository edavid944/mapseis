function [MinuteSelected,MinuteUnselected] = getMinutes(dataStore,rowIndices)
% getLocations : Magnitude projector.   Extracts event magnitude as Nx1
% array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell
% Version 74$ Edited by: David Eberhard

import mapseis.projector.*;

if nargin<2
    rowIndices = true(dataStore.getRowCount,1);
end

%MinuteSelected = MinuteProj(dataStore.getFields({'dateNum'},rowIndices));
%MinuteUnselected = MinuteProj(dataStore.getFields({'dateNum'},~rowIndices));

minutes = dataStore.getUserData('Minute');
MinuteSelected = minutes(rowIndices);
MinuteUnselected = minutes(~rowIndices);
end

%Not needed anymore
function MinuteArray = MinuteProj(s)

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Stefan Wiemer
% Last-modified: Matt McDonnell

if isempty(s)
    MinuteArray=[];
else
    MinuteArray = str2num(datestr(s.dateNum,'MM')); %#ok<ST2NM>
end

end
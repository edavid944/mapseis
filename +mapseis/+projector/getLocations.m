function [locSelected,locUnselected] = getLocations(dataStore,rowIndices,PaleRider)
	% getLocations : Location projector.  Extracts event lon and lat as Nx2 array
	
	% Copyright 2007-2008 The MathWorks, Inc.
	% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
	% Author: Matt McDonnell
	
	import mapseis.projector.*;
	
	if nargin<3
	    PaleRider=false;
	end
	
	if nargin<2
	    rowIndices = true(dataStore.getRowCount,1);
	    PaleRider=false;
	end
	
	if isempty(rowIndices)
		
		rowIndices = true(dataStore.getRowCount,1);
	end


	if dataStore.UseShift&~PaleRider
		[locSelected locUnselected]=getShiftedData(dataStore,rowIndices);
	
	else
		locSelected = LocProj(dataStore.getFields({'lon','lat'},rowIndices));
		locUnselected = LocProj(dataStore.getFields({'lon','lat'},~rowIndices));
	end


end


function locArray = LocProj(s)
	% LocProj : Location projector.  Extracts event lon and lat as Nx2 array
	
	% Copyright 2007-2008 The MathWorks, Inc.
	% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
	% Author: Matt McDonnell
	
	if isempty(s)
	    locArray=zeros(0,2);
	else
	    locArray = [s.lon s.lat];
	end

end
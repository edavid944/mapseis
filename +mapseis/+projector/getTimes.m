function [timeSelected,timeUnselected] = getTimes(dataStore,rowIndices)
% getLocations : Magnitude projector.   Extracts event times as Nx1
% array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 76 $    $Date: 2008-10-20 16:56:26 +0100 (Mon, 20 Oct 2008) $
% Author: Matt McDonnell

import mapseis.projector.*;

if nargin<2
    rowIndices = true(dataStore.getRowCount,1);
end

timeSelected = TimeProj(dataStore.getFields({'dateNum'},rowIndices));
timeUnselected = TimeProj(dataStore.getFields({'dateNum'},~rowIndices));

end

function timeArray = TimeProj(s)

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 76 $    $Date: 2008-10-20 16:56:26 +0100 (Mon, 20 Oct 2008) $
% Author: Matt McDonnell

if isempty(s)
    timeArray = [];
else
    timeArray = [s.dateNum];
end

end
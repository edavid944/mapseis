function [locSelected,locUnSelected] = getSliceProjections(dataStore,pline,width,rowIndices)
% getSliceProjections : Extracts event lon and lat and calculates their projection 
%						on the profile line pline


% $Revision: 1 $    $Date: 2008-09-12$
% Author: David Eberhard

import mapseis.projector.*;
import mapseis.region.Slicer;

if nargin<4
    rowIndices = true(dataStore.getRowCount,1);
end


theSlice=Slicer(pline,width);

Selected = LocProj(dataStore.getFields({'lon','lat'},rowIndices));
UnSelected = LocProj(dataStore.getFields({'lon','lat'},~rowIndices));

locSelected = theSlice.LineProjection(Selected(:,1),Selected(:,2));
locUnSelected = theSlice.LineProjection(UnSelected(:,1),UnSelected(:,2));

end

function locArray = LocProj(s)
% LocProj : Location projector.  Extracts event lon and lat as Nx2 array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell

if isempty(s)
    locArray=zeros(0,2);
else
    locArray = [s.lon s.lat];
end

end
function [CatSelected,CatUnselected] = getShortCatalog(dataStore,rowIndices,PaleRider)
	% getShortCatalog : This projector builds a catalog similar in a way to the zmap 
	%		    type catalog but with the following coulumns:
	%		    [lon lat depth datennum mag]
	
	
	import mapseis.projector.*;
	
	if nargin<3
	    PaleRider=false;
	end
	
	if nargin<2
	    rowIndices = true(dataStore.getRowCount,1);
	    PaleRider=false;
	end
	
	if isempty(rowIndices)
		
		rowIndices = true(dataStore.getRowCount,1);
	end
	
	CatSelected = CatProj(dataStore.getFields({'lon','lat','depth','dateNum','mag'},rowIndices));
	CatUnselected = CatProj(dataStore.getFields({'lon','lat','depth','dateNum','mag'},~rowIndices));
	
	if dataStore.UseShift&~PaleRider
		[locSelected locUnselected]=getShiftedData(dataStore,rowIndices);
		CatSelected(:,1)=locSelected(:,1);
		CatSelected(:,2)=locSelected(:,2);
		CatUnselected(:,1)=locUnselected(:,1);
		CatUnselected(:,2)=locUnselected(:,2);
	
	
	end	


end

function locArray = CatProj(s)
	% CatProj : Just extracts the stuff from the structure
	
	
	
	if isempty(s)
	    locArray=zeros(0,5);
	else
	    locArray = [s.lon s.lat s.depth s.dateNum s.mag];
	end

end
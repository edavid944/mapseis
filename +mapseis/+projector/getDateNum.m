function [hourSelected,hourUnselected] = getDateNum(dataStore,rowIndices)
% getLocations : Magnitude projector.   Extracts event magnitude as Nx1
% array

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell
% Version 74$ Edited by: David Eberhard

import mapseis.projector.*;

if nargin<2
    rowIndices = true(dataStore.getRowCount,1);
end

hourSelected = dataStore.getFields({'dateNum'},rowIndices);
hourUnselected = dataStore.getFields({'dateNum'},~rowIndices);
hourSelected = hourSelected.dateNum;
hourUnselected = hourUnselected.dateNum;
%hours = dataStore.getUserData('Hour');
%hourSelected = hours(rowIndices);
%hourUnselected = hours(~rowIndices);

end


%Not needed anymore
function hourArray = HourProj(s)

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Stefan Wiemer
% Last-modified: Matt McDonnell

if isempty(s)
    hourArray=[];
else
    %hourArray = dataStore.getUserData('Hour');
    hourArray = str2num(datestr(s.dateNum,'HH')); %#ok<ST2NM>
end

end
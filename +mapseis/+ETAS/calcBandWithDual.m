function BandWith = calcBandWithDual(NrBuddys,ShortCat,RelmGrid,MagSearch,DepthSearch,AnySopMode,TheMask)
	%searches the k nearest neighbours for each grid cell in the catalog, and 
	%calculates the distance to them which will be used as bandwith for gaussian
	%smoothing kernels later.
	
	if nargin<7
		TheMask=[];
	end

	if isempty(TheMask)
		TheMask=true(size(RelmGrid(:,1)));
	end	
	
	%prepare grid
	SearchGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	SearchGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	
	Data2Search(:,1)=ShortCat(:,1);	
	Data2Search(:,2)=ShortCat(:,2);
	
	ne=3;
	
	if DepthSearch
		SearchGrid(:,ne)=(RelmGrid(:,5)+RelmGrid(:,6))/2;
		Data2Search(:,ne)=ShortCat(:,3);
		ne=4;
	end
	
	if MagSearch
		if ~AnySopMode
			SearchGrid(:,ne)=(RelmGrid(:,7)+RelmGrid(:,8))/2;
			Data2Search(:,ne)=ShortCat(:,5);
		else
			MagGrid=(RelmGrid(:,7)+RelmGrid(:,8))/2;
			Mag2Search=ShortCat(:,5);
			ToMagGrid=MagGrid(TheMask,:);
			
		end
	else
		AnySopMode=false;
		
	end
	
	%Kick out fields not in the mask
	SpatBand=zeros(size(RelmGrid(:,1)));
	MagBand=zeros(size(RelmGrid(:,1)));
	
	ToSearchGrid=SearchGrid(TheMask,:);
	
	if ~AnySopMode
		%Now search the nearest neighbours
		[n,d] = knnsearch(Data2Search,ToSearchGrid,'k',NrBuddys);
		SpatBand(TheMask)=max(d,[],2);
		BandWith=SpatBand;
	
	else
		if numel(NrBuddys)==1
			NrBuddys(2)=NrBuddys(1);
		end
	
		%spatial
		[n,d] = knnsearch(Data2Search,ToSearchGrid,'k',NrBuddys(1));
		SpatBand(TheMask)=max(d,[],2);
		BandWith{1}=SpatBand
		
		%magnitude
		[n,d] = knnsearch(Mag2Search,ToMagGrid,'k',NrBuddys(1));
		MagBand(TheMask)=max(d,[],2);
		BandWith{2}=MagBand
		
	end
	
end

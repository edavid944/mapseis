function SmoothOperator = smoothGaussBand_mk2(ValToSmooth,RelmGrid,minWidth,BandWith,bvalue)
	%Smooths a data vector with a gaussian smoothing kernel, with a varying 
	%Bandwith of the kernel
	
	%mk2 version, a corrected magnitude distribution
	%UNTESTED
	disp('----')
	disp(max(BandWith))
	disp(minWidth)
	disp('****')
	
	
	if ~nargin<5
		bavalue=1;
	end
	
	disp('smoothGaussBand_mk2 started')
	
	SmoothOperator=zeros(size(ValToSmooth));
	PosGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	PosGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	numMags=numel(unique(RelmGrid(:,7)));
	
	
	SpaceCelSel=(1:numMags:numel(PosGrid(:,1)));
	SpaceOnly=PosGrid(SpaceCelSel,:);
	
	%prepare Bandwidth
	TheBand=BandWith;
	TheBand(TheBand<minWidth)=minWidth;
	ReducedBand=TheBand(SpaceCelSel,:);
	
	
	SpaceOnlyNoise=RelmGrid(SpaceCelSel,:);
	ReducedVal=zeros(size(SpaceOnlyNoise(:,1)));
	%ReducedBand=zeros(size(SpaceOnlyNoise(:,1)));
	%reduce to spatial aspect
	for i=1:numel(SpaceOnlyNoise(:,1))
		InTheCell=RelmGrid(:,1)==SpaceOnlyNoise(i,1)&RelmGrid(:,3)==SpaceOnlyNoise(i,3);
		ReducedVal(i)=nansum(ValToSmooth(InTheCell));
	end
	
	
	%UniMags(mod(i-1,numMags)+1)
	
	%normalize to the area, NOT NEEDED NOW
	CellArea=abs(RelmGrid(1,2)-RelmGrid(1,1))*abs(RelmGrid(1,4)-RelmGrid(1,3));
	FullArea=numel(PosGrid(:,1))*CellArea;
	GauSum=0;


	

	CellWithData=find(ReducedVal~=0);
	SmoothOperatorSpace=zeros(size(ReducedVal));
	for i=CellWithData'
	
		%select to which mag slice it is going
		%ThisSlice=SpaceCelSel+mod(i-1,numMags);
		
		%calc distance to all other cells (euclidian for now)
		%DistMat=((SpaceOnly(:,1)-PosGrid(i,1)).^2+(SpaceOnly(:,2)-PosGrid(i,2)).^2).^(1/2);
			
		%calculate gaussian
		% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
		LonPart = 1/2*(erf((SpaceOnly(i,1)-SpaceOnlyNoise(:,1))./(ReducedBand(i)*sqrt(2))) - erf((SpaceOnly(i,1)-SpaceOnlyNoise(:,2))./(ReducedBand(i)*sqrt(2))));
		LatPart = 1/2*(erf((SpaceOnly(i,2)-SpaceOnlyNoise(:,3))./(ReducedBand(i)*sqrt(2))) - erf((SpaceOnly(i,2)-SpaceOnlyNoise(:,4))./(ReducedBand(i)*sqrt(2))));
		
		GaussVal=LonPart.*LatPart;
		%GaussVal=(1/(2*pi*TheBand(i)^2))*exp((-DistMat.^2)./(2*TheBand(i)^2));
			
		%add contribution
		SmoothOperatorSpace=SmoothOperatorSpace+(ReducedVal(i)*GaussVal);
		GauSum=GauSum+sum(GaussVal(:));
			
			
	end

	SmoothOperatorSpace=SmoothOperatorSpace/sum(SmoothOperatorSpace(:))*sum(ReducedVal(:));
	
	%now the magnitude distribution	(copied from correctMagDistro, could be optimized)
	%------------------------------
	TheMags = unique(RelmGrid(:,7));
	numMag=numel(TheMags);
	minMag = min(TheMags);
	MaxVal=1;
	TheCurve=MaxVal-(bvalue*(TheMags-minMag));
	
	%norm it so that the sum of the non log is 1
	MstCurve=10.^TheCurve/(sum(10.^TheCurve));	

	%build a template for the forecasts
	NewCast=RelmGrid;
	for mC=1:numMag
		MagSel=NewCast(:,7)==TheMags(mC);
		NewCast(MagSel,9)=MstCurve(mC);
	end
	
	NodeSel=NaN(size(NewCast(:,1)));
	for gC=1:numel(SpaceOnlyNoise(:,1))
		Sel=NewCast(:,1)==SpaceOnlyNoise(gC,1)&NewCast(:,3)==SpaceOnlyNoise(gC,3);
		NodeSel(Sel,1)=gC;		
	end
	
			
	%build a new forecast
	NewCast(:,9)=NewCast(:,9).*SmoothOperatorSpace(NodeSel);
	
	SmoothOperator=NewCast(:,9);
	%SmoothOperator=SmoothOperatorSpace
	
	disp('smoothGaussBand_mk2 ended')
	
	
	
end

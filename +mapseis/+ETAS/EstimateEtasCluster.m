function [OutStruct] = EstimateEtasCluster(Datastore,Filterlist,MinMainMag,...
				SpatRefPoint,TopLeftPoint,MinMag,ThetaStart,Options)
	%Similar approach like with EstimateSTEP, but for mETAS, the EtasEstimator
	%code (Schoenberg based) will be used on the single clusters instead of the 
	%full data set.
	
	
	%AutoSpace: Determine the Spatial Space itself according to the aftershock cluster
	%PassOnTheta: Theta will be passed to the next cluster as a starting point.
	
	import mapseis.ETAS.*;
	import mapseis.projector.*;
	
	%check if declustering data is available
	if isempty(Datastore.ClusterNR)
		error('No Declustering Data available')
	end
	
	if nargin<4
		Options=[];
	end
	
	if isempty(Options)
		%default options
		Options = struct('Range_mu',[[0, 10]],...
			  	 'Range_K',[[0 20]],...
			  	 'Range_b',[[0 2]],...
			  	 'Range_c',[[0 5]],...
			  	 'Range_p',[[0 5]],...
			  	 'Range_a',[[0 1]],...
			  	 'Range_d',[[0 5]],...
			  	 'Range_q',[[0 300]],...
			  	 'OnlySensible',true,...
			  	 'UseMaxTime',false,...
			  	 'MaxTime',50,...
			  	 'AutoSpace',false,...
			  	 'PassOnTheta',false);
		
	end
	
	
	
	%unpack the variables (needed Sensibility)
	mu = ThetaStart.mu;
	K = ThetaStart.K;
	c = ThetaStart.c;
	p = ThetaStart.p;
	a = ThetaStart.a;
	d = ThetaStart.d;
	q = ThetaStart.q;
	
	try
		b = ThetaStart.b; 
	catch
		b=1;
	end
	
	
	%get clusters
	ClusterNr=Datastore.ClusterNR;
	EventType=Datastore.EventType;
	
	
	%get available Earthquakes
	Filterlist.changeData(Datastore);
	Filterlist.updateNoEvent;
	selected=Filterlist.getSelected;
	Filterlist.PackIt;
	eventStruct=Datastore.getFields;
	
	%check if anything is init anyway
	if sum(selected)==0
		error('No Earthquake is in the selection of the filterlist')
	end
	
	%filter clusters
	FiltClustNr=ClusterNr(selected);
	FiltEventType=EventType(selected);
	
	
	%get suitable mainshock
	MagMainSelect=eventStruct.mag>=MinMainMag;
	isMain=EventType==2;
	isAftershock=EventType==3;
	MainCandidate=unique(ClusterNr(isMain&MagMainSelect&selected));
	
	%do still have something left?
	if isempty(MainCandidate)
		error('No Mainshock fit the selection criterias');
	end	
	
	%for sensible test
	Range_mu=Options.Range_mu;
	Range_K=Options.Range_K;
	Range_b=Options.Range_b;
	Range_c=Options.Range_c;
	Range_p=Options.Range_p;
	Range_a=Options.Range_a;
	Range_d=Options.Range_d;
	Range_q=Options.Range_q;
	
	
	
	%maximum time
	UseMaxTime = Options.UseMaxTime;
	MaxTime = Options.MaxTime;
	
	%AutoSpace&PassOn
	AutoSpace=Options.AutoSpace;
	PassOnTheta=Options.PassOnTheta;
	
	
	%prepare raw data
	mu_values=NaN(size(MainCandidate));
	K_values=NaN(size(MainCandidate));
	b_values=NaN(size(MainCandidate));
	c_values=NaN(size(MainCandidate));
	p_values=NaN(size(MainCandidate));
	a_values=NaN(size(MainCandidate));
	d_values=NaN(size(MainCandidate));
	q_values=NaN(size(MainCandidate));
	
	LogLikelihood=NaN(size(MainCandidate));
        RealLogLike=false(size(MainCandidate));
        ExitFlag=NaN(size(MainCandidate));
	
	NrAftershocks=NaN(size(MainCandidate));
	MainID=NaN(size(MainCandidate));
	MainTime=NaN(size(MainCandidate));
	MainMag=NaN(size(MainCandidate));
	isSensible=false(size(MainCandidate));
	
	%init theta
	ThisTheta=ThetaStart;
	
	%loop over all clusters 
	for i=1:numel(MainCandidate)
		
		%get mainshock
		MainID(i)=MainCandidate(i);
		MshockSel=(MainCandidate(i)==ClusterNr)&isMain;
		MainShock=getShortCatalog(Datastore,MshockSel);
		
		if isempty(MainShock)
			disp(['No suitable MainShock in ClusterID ',num2str(MainCandidate(i))]);
			continue;
		end
		
		
		MainTime(i)=MainShock(1,4);
		MainMag(i)=MainShock(1,5);
		
		%get aftershocks
		inTime=eventStruct.dateNum>=MainTime(i);
		AfShockSel=(MainCandidate(i)==ClusterNr)&selected&inTime&isAftershock;
		AShocks=getShortCatalog(Datastore,AfShockSel);
		NrAftershocks(i)=sum(AfShockSel);
		
		if isempty(AShocks)
			disp(['No suitable Aftershocks found for ClusterID ',num2str(MainCandidate(i))]);
			continue;
		end
	
		%combine catalog into full catalog
		FullClustCat=[MainShock;AShocks];
		
		%I do not exacalty the times should be selected, but for now use mainshock as 
		%start time and last aftershock as end time
		StartTime=MainTime(i);
		EndTime=AShocks(end,4);
		ThisTime=EndTime-StartTime;
		
		
		if UseMaxTime&(EndTime-StartTime)>MaxTime
			ThisTime=MaxTime;
		end
		
		
		
		if AutoSpace
			ThisSpatRefPoint(1)=min(FullClustCat(:,1));
			ThisSpatRefPoint(2)=min(FullClustCat(:,2));
			ThisTopLeftPoint(1)=max(FullClustCat(:,1));
			ThisTopLeftPoint(2)=max(FullClustCat(:,2));
		else
			ThisSpatRefPoint=SpatRefPoint;
			ThisTopLeftPoint=TopLeftPoint;
		
		end
				
		
		[EstValues, AddonData] = EtasEstimator(FullClustCat,ThisTime,ThisSpatRefPoint,ThisTopLeftPoint,MinMag,ThisTheta);
		
		
	
		
		mu_values(i)=EstValues.mu;
		K_values(i)=EstValues.K;
		b_values(i)=b;
		c_values(i)=EstValues.c;
		p_values(i)=EstValues.p;
		a_values(i)=EstValues.a;
		d_values(i)=EstValues.d;
		q_values(i)=EstValues.q;
		
		
		LogLikelihood(i)=AddonData.Est_fval;
		RealLogLike(i)=isreal(AddonData.Est_fval);
		ExitFlag(i)=AddonData.Est_exitflag;
		
		
		
		isSensible(i) = mu_values(i)>=Range_mu(1)&mu_values(i)<=Range_mu(2)&...
				K_values(i)>=Range_K(1)&K_values(i)<=Range_K(2)&...
				b_values(i)>=Range_b(1)&b_values(i)<=Range_b(2)&...
				c_values(i)>=Range_c(1)&c_values(i)<=Range_c(2)&...
				p_values(i)>=Range_p(1)&p_values(i)<=Range_p(2)&...
				a_values(i)>=Range_a(1)&a_values(i)<=Range_a(2)&...
				d_values(i)>=Range_d(1)&d_values(i)<=Range_d(2)&...
				q_values(i)>=Range_q(1)&q_values(i)<=Range_q(2);
				
		if PassOnTheta & isSensible(i) & RealLogLike(i)	
			ThisTheta=EstValues;
			ThisTheta.b=b;
		end
		
		
	end
	
	
	
	%calculate stacked parameters
	if Options.OnlySensible
		mean_mu=nanmean(mu_values(isSensible));
		mean_K=nanmean(K_values(isSensible));
		mean_b=nanmean(b_values(isSensible));
		mean_c=nanmean(c_values(isSensible));
		mean_p=nanmean(p_values(isSensible));
		mean_a=nanmean(a_values(isSensible));
		mean_d=nanmean(d_values(isSensible));
		mean_q=nanmean(q_values(isSensible));
		
		
		
	else
		mean_mu=nanmean(mu_values);
		mean_K=nanmean(K_values);
		mean_b=nanmean(b_values);
		mean_c=nanmean(c_values);
		mean_p=nanmean(p_values);
		mean_a=nanmean(a_values);
		mean_d=nanmean(d_values);
		mean_q=nanmean(q_values);
	
	end
	
	Theta.mu = mean_mu;
	Theta.K = mean_K;
	Theta.b = mean_b;
	Theta.c = mean_c;
	Theta.p = mean_p;
	Theta.a = mean_a;
	Theta.d = mean_d;
	Theta.q = mean_q;
	
	%pack it 
	OutStruct = struct(	'Theta',Theta,...
				'mu',mean_mu,...
				'K',mean_K,...
				'b',mean_b,...
				'c',mean_c,...
				'p',mean_p,...
				'a',mean_a,...
				'd',mean_d,...
				'q',mean_q,...
				'Single_mu_values',mu_values,...
				'Single_K_values',K_values,...
				'Single_b_values',b_values,...
				'Single_c_values',c_values,...
				'Single_p_values',p_values,...
				'Single_a_values',a_values,...
				'Single_d_values',d_values,...
				'Single_q_values',q_values,...
				'LogLikelihood',LogLikelihood,...
				'RealLogLike',RealLogLike,...
				'ExitFlag',ExitFlag,...
				'MainID',MainID,...
				'NrAftershocks',NrAftershocks,...
				'MainTime',MainTime,...
				'MainMag',MainMag,...
				'isSensible',isSensible);
	

	
	
	
	

end

function SmoothOperator = smoothGaussBand3D_mk2b(ValToSmooth,RelmGrid,minWidth,BandWith,SmoothMode)
	%Smooths a data vector with a gaussian smoothing kernel, with a varying 
	%Bandwith of the kernel
	
	%maybe more comments about it later.

	%with integration using the error function erf
	
	%cleaned up version of mk2 with support for 2D to 4D smoothing
	
	
	SmoothOperator=zeros(size(ValToSmooth));
	PosGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	PosGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	PosGrid(:,3)=((RelmGrid(:,6)+RelmGrid(:,5))/2); %Depths
	PosGrid(:,4)=((RelmGrid(:,8)+RelmGrid(:,7))/2);
	
	
	%prepare Bandwidths
	switch BandWith
		case '2D' 
			TheBand=BandWith;
			TheBand(TheBand<minWidth)=minWidth;
			
		case '3D_iso'
			TheBand=BandWith;
			TheBand(TheBand<minWidth)=minWidth;
			
		case '3D_aniso'
			if numel(minWidth)==1
				minWidth(2)=minWidth(1);
			end
			
			TheBandSpat=BandWith{1};
			TheBandSpat(TheBandSpat<minWidth(1))=minWidth(1);
			
			TheBandMag=BandWith{2};
			TheBandMag(TheBandMag<minWidth(2))=minWidth(2);
			
		case '4D_iso'
			TheBand=BandWith;
			TheBand(TheBand<minWidth)=minWidth;
		
		case '4D_aniso'
			if numel(minWidth)==1
				minWidth(2)=minWidth(1);
				minWidth(3)=minWidth(1);
			end
			
			TheBandSpat=BandWith{1};
			TheBandSpat(TheBandSpat<minWidth(1))=minWidth(1);
			
			TheBandMag=BandWith{2};
			TheBandMag(TheBandMag<minWidth(2))=minWidth(2);
			
			TheBandDepth=BandWith{3};
			TheBandDepth(TheBandDepth<minWidth(3))=minWidth(3);
		
				
	end	
	
	
	
	%UniMags(mod(i-1,numMags)+1)
	
	CellWithData=find(ValToSmooth~=0);
	disp(numel(CellWithData))
	
	%normalize to the area
	CellArea=abs(RelmGrid(1,2)-RelmGrid(1,1))*abs(RelmGrid(1,4)-RelmGrid(1,3));
	FullArea=numel(PosGrid(:,1))*CellArea;
	GauSum=0;
		
	
	switch Bandwidths
		case '2D' 
			for i=CellWithData'
				%NOT Fully finished	
				
				%calc distance to all other cells (euclidian for now)
				%DistMat=((PosGrid(:,1)-PosGrid(i,1)).^2+(PosGrid(:,2)-PosGrid(i,2)).^2+(PosGrid(:,3)-PosGrid(i,3)).^2).^(1/2);
					
				%calculate gaussian
				% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
				%GaussVal=(1/(2*pi*TheBand(i)^3))*exp((-DistMat.^2)./(2*TheBand(i)^2));
				
				LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBand(i)*sqrt(2))));
				LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBand(i)*sqrt(2))));
				
				
				GaussVal=LonPart.*LatPart;
				
				%add contribution
				SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
				GauSum=GauSum+sum(GaussVal(:));
					
					
			end
			
			
		case '3D_iso'
			for i=CellWithData'
		
				LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBand(i)*sqrt(2))));
				LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBand(i)*sqrt(2))));
				MagPart = 1/2*(erf((PosGrid(i,4)-RelmGrid(:,7))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,4)-RelmGrid(:,8))./(TheBand(i)*sqrt(2))));
				
				GaussVal=LonPart.*LatPart.*MagPart;
				
				%add contribution
				SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
				GauSum=GauSum+sum(GaussVal(:));
					
					
			end
			
			
		case '3D_aniso'
			for i=CellWithData'
				
				LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBandSpat(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBandSpat(i)*sqrt(2))));
				LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBandSpat(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBandSpat(i)*sqrt(2))));
				MagPart = 1/2*(erf((PosGrid(i,4)-RelmGrid(:,7))./(TheBandMag(i)*sqrt(2))) - erf((PosGrid(i,4)-RelmGrid(:,8))./(TheBandMag(i)*sqrt(2))));
				
				GaussVal=LonPart.*LatPart.*MagPart;
				
				%add contribution
				SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
				GauSum=GauSum+sum(GaussVal(:));
					
					
			end
			
			
		case '4D_iso'
			for i=CellWithData'
		
				LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBand(i)*sqrt(2))));
				LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBand(i)*sqrt(2))));
				DepthPart = 1/2*(erf((PosGrid(i,3)-RelmGrid(:,5))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,3)-RelmGrid(:,6))./(TheBand(i)*sqrt(2))));
				MagPart = 1/2*(erf((PosGrid(i,4)-RelmGrid(:,7))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,4)-RelmGrid(:,8))./(TheBand(i)*sqrt(2))));
				
				GaussVal=LonPart.*LatPart.*DepthPart.*MagPart;
				
				%add contribution
				SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
				GauSum=GauSum+sum(GaussVal(:));
					
					
			end
		
		case '4D_aniso'
			for i=CellWithData'
				
				LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBandSpat(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBandSpat(i)*sqrt(2))));
				LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBandSpat(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBandSpat(i)*sqrt(2))));
				DepthPart = 1/2*(erf((PosGrid(i,3)-RelmGrid(:,5))./(TheBandDepth(i)*sqrt(2))) - erf((PosGrid(i,3)-RelmGrid(:,6))./(TheBandDepth(i)*sqrt(2))));
				MagPart = 1/2*(erf((PosGrid(i,4)-RelmGrid(:,7))./(TheBandMag(i)*sqrt(2))) - erf((PosGrid(i,4)-RelmGrid(:,8))./(TheBandMag(i)*sqrt(2))));
				
				GaussVal=LonPart.*LatPart.*MagPart;
				
				%add contribution
				SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
				GauSum=GauSum+sum(GaussVal(:));
					
					
			end
			
				
	end	
	

	
	
	SmoothOperator=SmoothOperator/sum(SmoothOperator(:))*sum(ValToSmooth(:));
	
	
	

	
	
	
end

function ToMin = SumToOptimPar(WorkCat,TimeLength,SpatialExtend,ThetaStart)
	%based on the work of Schoenberg, BSSA, 2012
	%estimates the parameter for a ETAS model, the code is lously 
	%based on a C code by Rick Schoenberg (myss2.c)
	
	%This function has to be minized with fminsearch or similar
	
	%Parallel version, has to be verified that it work properly
	
	%hard-coded parts for now
	CurveMode=false;
	
	%unpack the variables
	mu = ThetaStart(1);
	%b = ThetaStart(2);
	K = ThetaStart(2);
	c = ThetaStart(3);
	p = ThetaStart(4);
	a = ThetaStart(5);
	d = ThetaStart(6);
	q = ThetaStart(7);
	
	
		
	%VARS not inputed yet: TimeLength, SpatialExtend
	%(they will probalby not be optimized directly)
	%SpatialExtend= maximal extend of the region

	
	%direct translation
	%sum1=0;
	%for 1=1:numel(WorkCat(:,1))
	%	 sum1 = sum1 + exp(a.*WorkCat(i,5));
	%end
	
	%faster
	sum1=sum(exp(a.*WorkCat(:,5)));
	
	%init "Error"
	ToMin = mu * TimeLength + K*sum1 - log(mu./(SpatialExtend(1).*SpatialExtend(2)));
	
	%stop mu to go to negative values and ToMin go to complexe numbers
	%if mu<0
	%	ToMin=999999999999;
	%	return
	%end
	
	
	%Constant used later
	Const1 = K * (p-1) * c^(p-1) * (q-1) * (d^(q-1))/pi; 	
	
	%main Loop
	lam1=ones(size(WorkCat(:,1)))*0.0000000001;
	parfor i=1:numel(WorkCat(:,1))

		%original 2nd loop (probably vectorizable)
		%sum2=0;
		%for j=1:(i-1)
		%	r2 = (WorkCat(i,1)-WorkCat(j,1))^2 + (WorkCat(i,2)-WorkCat(j,2))^2;
		%	sum2 = sum2 + (WorkCat(i,4)-WorkCat(j,4)+c)^(-p) * exp(a.*WorkCat(j,5)) * (r2+d)^(-q);
		%end
		
		%faster version
		r2 = (WorkCat(i,1)-WorkCat(1:i-1,1)).^2 + (WorkCat(i,2)-WorkCat(1:i-1,2)).^2;
		sum2 = sum((WorkCat(i,4)-WorkCat(1:i-1,4)+c).^(-p).*exp(a.*WorkCat(1:i-1,5)).*(r2+d).^(-q));
		
		lam1(i) = mu./(SpatialExtend(1).*SpatialExtend(2)) + Const1*sum2; 
		
		
		
		
	end
	
	
	if any(lam1 < 0.0000000001) 
		ToMin = 999999999999;
	else
		ToMin = ToMin - sum(log(lam1));
	end
	
	if isinf(ToMin)|isnan(ToMin)
		ToMin = 999999999999;
	end
	
	if (min(ThetaStart)<0.00000001) | (ThetaStart(4)-1<0.00000001)| (ThetaStart(7)-1<0.00000001)
		ToMin = 999999999999;
	end
	
	if ThetaStart(5)>=1
		ToMin = 999999999999;
	end
	
	%DELETE after TESTING
	%original code
	%  #include <R.h>
	%  #include <Rmath.h>
	%  
	%  //Remember *a is a pointer to a variable "a"
	%  void ss2 (double *x, double *y, double *t, double *hm, int *n,
		  %  double *x1, double *y1, double *T, double *theta, 
		  %  double *a2){
	    %  
	    %  int i,j;
	    %  double sum1, sum2, r2, const1, mu, K, c, p, a, d, q, lam1;
	    %  mu = theta[0]; K = theta[1]; c = theta[2];
	    %  p = theta[3]; a = theta[4]; d = theta[5]; q = theta[6];
	    %  sum1 = 0.0;
		%  //shouldn't it be pointers as well for theta?  
		%  //Nope, normally only the pointer of an array only points to 
		%  //the first element of an array  
	%  
		%  for(i = 0; i < *n; i++){
			%  sum1 += exp(a*hm[i]);
	    %  }
		%  //I guess that's a something like the total expect number of events
		%  // or so
	%  
		%  *a2 = mu * *T + K*sum1 - log(mu/(*x1 * *y1));
	    %  const1 = K * (p-1)* pow(c,(p-1)) * (q-1) * pow(d,(q-1)) / 3.141593;
	    %  //smoothing kernel?
		%  //a2 will be modified outside the function (throught the pointer). In Matlab this would look a bit like
		%  //a2 = mu * T + K*sum1 - log(mu./(x1.*y2)); or so I assume (a2 ,ay be a vector)
		%  //remember in c pow(x,y)=x^y, so const1 would be in Matlab
		%  //const1 = K * (p-1) * c^(p-1) * (q-1) * (d^(q-1)/Pi; 
	%  
	%  
		%  for(i = 1; i < *n; i++){
			%  sum2 = 0.0; //reset sum
			%  
			%  for(j=0; j < i; j++){
			%  r2 = (x[i]-x[j])*(x[i]-x[j]) + (y[i]-y[j])*(y[i]-y[j]);
			%  sum2 += pow(t[i]-t[j]+c,(-p)) * exp(a*hm[j])*pow(r2+d,(-q));
			%  }
			%  //loop over all location "before" (in the list) i 
			%  //r2 is just the euclidian distance in space between point i  and j
			%  //second part is in matlab: (hm == mag-minmag)
			%  //sum2 = (sum2+) (t(i)-t(j)+c)^(-p) * exp(a*hm(j)) * (r2+d)^(-q)
	 %  
			%  lam1 = mu / *x1 / *y1 + const1 * sum2;
			%  //I guess this can be a vector in matlab it would be
			%  // lam1 = mu./x1./y1 + const1 * sum2;
			%  //or lam1 = mu./(x1.*y1) + const1*sum2; 
	%  
			%  
			%  if(lam1 < 0.0000000001) *a2 = 999999999999;
			%  else {
			%  *a2 -= log(lam1);
				%  }
			%  //if lam1 is not super-tiny, the log of it will be subtracted from a2 
			%  //to make it clear, matlab it would be: a2 = (a2 -) log(lam1); 
			%  //a2 is used in an optim function (that's
			%  //why the if makes a lot of sense)
	%  
	    %  }
	%  }
	  %  
	
	
	
end

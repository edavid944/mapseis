function [Forecast AddonData] = EtasForecaster_mk2(ShortCat,currentTime,TimeLength,RelmGrid,ModConfig,ThetaStart)
	%prototype of an Etas forecast based on Schoenberg (second version
	FullClock=tic;
	
	%TODO: 	add magnitude correction (subtract Mc) to EtasGenerator
	%	use whole past seismicity for the generator
	%	integrate a gaussian smoothing (spacial and magnitude seperate)
	
	import mapseis.ETAS.*;
	
	%for experimentation of performance, will be deleted eventually	
	%LoopMode='Catalogs';
	LoopMode='Nodes';
	%LoopMode='EQ';
	%Smoothing=true;
	%SmoothDist=3;
	
	%This one uses a different approach for the generation of earthquake and 
	%gridding of the data, hopefully it improves the performances
	%for start it won't be much different in input parameters, but it may change
	%later a lot
	
	if isempty(ModConfig)
		%Default config
		ModConfig=struct(	'ReturnVal','Poisson',...
					'EstimatePara',true,...
					'EqGeneration','normal',...
					'BG_Distro',RelmGrid,...
					'GenTimeBefore',TimeLength,...
					'LearnStartTime',min(ShortCat(:,4)),...
					'MinMag',min(RelmGrid(:,7)),...
					'MinMainMag',min(RelmGrid(:,7)),...
					'GenEqList',[[]],...
					'Gen1stGen',true,...
					'Nr_Simulation',10000,...
					'Smoothing',true,...
					'SmoothMode','mean',...
					'SmoothParam',3,...
					'SmoothConvMat',{{}});
		
	end

	
	
	
	
	
	if isempty(currentTime)
		currentTime=0; %for some modes ok
	end	
	
	if isempty(ThetaStart)
		ThetaStart=struct(	'mu',5.250,...
					'b',1,...
					'K',0.200,...
					'c',0.025,...
					'p',1.500,...
					'a',0.700,...
					'd',0.025,...
					'q',1.500);
	end
	
	
	try
		b = ThetaStart.b;
	catch	
		disp('b value not set used 1')
		ThetaStart.b = 1;
	end
	
	%unpack smoothing parameters
	Smoothing=ModConfig.Smoothing;
	SmoothMode=ModConfig.SmoothMode;
	SmoothParam=ModConfig.SmoothParam;
	
	SmoothConvMat={};
	if isfield(ModConfig,'SmoothConvMat')
		disp('imported conversion matrix')
		SmoothConvMat=ModConfig.SmoothConvMat;
	end
	
	%set reference point (add 10% oversize, NOPE) 
	SpatRefPoint=[min(RelmGrid(:,1)),min(RelmGrid(:,3))];
	TopLeftPoint=[max(RelmGrid(:,2)),max(RelmGrid(:,4))];
	%SpatRefPoint=SpatRefPoint-abs(SpatRefPoint*0.05);
	%TopLeftPoint=TopLeftPoint+abs(TopLeftPoint*0.05);
	MinMag=ModConfig.MinMag;%min(RelmGrid(:,7));
	
	if isfield(ModConfig,'MinMainMag')
		
		MinMainMag=ModConfig.MinMainMag;
	else
		MinMainMag=MinMag;	
	end
	
	
	
	EstPar=ModConfig.EstimatePara;
	%Estimate ETAS parameter
	if EstPar
		EstClock=tic
		%[EstValues, EstAddonData] = EtasEstimator(ShortCat,TimeLength,SpatRefPoint,TopLeftPoint,MinMag,ThetaStart);
		[EstValues, EstAddonData] = EtasEstimator(ShortCat,ModConfig.GenTimeBefore,SpatRefPoint,TopLeftPoint,MinMag,ThetaStart);
		%[EstValues, EstAddonData] = PolyEtasEstimator(ShortCat,1000,SpatRefPoint,TopLeftPoint,MinMag,ThetaStart);
		EstTime=toc(EstClock);
	else
		EstValues=ThetaStart;
		EstAddonData=[];
		EstTime=NaN;
	end	
	
	%build generator data
	Theta=EstValues;
	Theta.b=ThetaStart.b;
	
	%Not needed anymore, but leave it for now (needed agaib :-)
	if strcmp(ModConfig.EqGeneration,'normalBG')
		ModConfig.BG_Distro(:,9)=ModConfig.BG_Distro(:,9)*Theta.mu;
		ModConfig.EqGeneration='normal';
		
	end
	
	%hard wire the mode
	MainshockStruct = struct(	'Mode','normal',...
					'Distro',ModConfig.BG_Distro,...
					'UseStartTime',true,...
					'MinMainMag',MinMainMag,...
					'TimeBefore',ModConfig.LearnStartTime,...
					'EqList',ModConfig.GenEqList,...
					'Generate1stGen',ModConfig.Gen1stGen);
	
	if isempty(MainshockStruct.TimeBefore)			
		MainshockStruct.TimeBefore=ModConfig.GenTimeBefore;
		MainshockStruct.UseStartTime=false;
	end
	
	%generate catalogs
	GenClock=tic;
	CatalogCollegion=[];
	
	%for i=1:ModConfig.Nr_Simulation
	%	OutStruct = EtasGenerator_mk1(Theta,SpatRefPoint,TopLeftPoint,TimeLength,currentTime,ShortCat,MainshockStruct,true,true);
	%	try %there is sometimes nothing generated
	%		CurCat=OutStruct.GenShortCat;
	%		CurCat(:,6)=ones(numel(CurCat(:,1)),1)*i; %mark the run
	%		CatalogCollegion=[CatalogCollegion;CurCat];
	%		disp(size(CatalogCollegion));
	%	end
				
		
	%end

	%paralizable version
	TheCats=cell(ModConfig.Nr_Simulation,1);
	OrgMarkers=cell(ModConfig.Nr_Simulation,1);
	
	%the big matrix with counts
	numSim=ModConfig.Nr_Simulation
	BigCount = sparse(numel(ModConfig.BG_Distro(:,1)),numSim);
	
	InStruct = EtasGenerator_init_mk4(Theta,SpatRefPoint,TopLeftPoint,TimeLength,currentTime,MinMag,ShortCat,MainshockStruct,true,true);
	
	
	
	
	
	parfor i=1:numSim
		%disp(i)
		OutStruct = EtasGenerator_runGrid_mk4(Theta,InStruct,MainshockStruct);
		
		if ~isempty(OutStruct) %there is sometimes nothing generated
			ThisCount=OutStruct.EqCount;
						
			%archive it
			BigCount(:,i)=ThisCount;
			
			%additional data
			OrgMarkers{i}=OutStruct.OriginMarker;
			BGNum(i)=OutStruct.Nr_BG;
			TrigNum(i)=OutStruct.Nr_Trig;
		else
			disp('something went wrong');
		end
				
		
	end
	%save('TEMP_BigCount.mat','BigCount')
	
	disp('finished generating');
	GenTime=toc(GenClock);
	
	
	%generate forecast
	%A bit messy here at the moment because it needs a loop of optimization
	
	BuildClock=tic;
	
	%
	%prepare arrays to hold the forecast
	DiscretForecast=cell(numel(RelmGrid(:,1)),1);
	meanVal=NaN(numel(RelmGrid(:,1)),1);
	VarVal=NaN(numel(RelmGrid(:,1)),1);
	ControlData={} %just to check that everything works fine will be deleted later
	
	
	%HERE
	
	%Estimating the rates should be relatively easy:
	meanVal=full(mean(BigCount,2));
	varVal=full(var(BigCount,0,2));
	
	%the discret distribution should only be calculated if needed, it can take a will
	if strcmp(ModConfig.ReturnVal,'Discret')
		%time example size(250000,10000):
		%	vector: 266s
		%	loop: 251s 
		%almost no difference, but I think the loop may have less memory issues
		%DiscretForecast={};
		for i=1:numel(BigCount(:,1))
			DiscretForecast{i,1}=full(BigCount(i,:));
		end
		
		%vector version
		%DiscretForecast=mat2cell(BigCount,ones(numel(RelmGrid(:,1))),numSim);
		
	
	end	
	

	%Finish the statistic
	PoissonRate=meanVal;
	NegBin_P=meanVal./varVal;
	NegBin_R=(NegBin_P.*varVal)./(1-NegBin_P);
	
	%stop clock
	BuildTime=toc(BuildClock);
	
	%Smoothing prototype(just a moving average)
	SmoothClock=tic;
	if Smoothing
		LonVec=(RelmGrid(:,1)+RelmGrid(:,2))/2;
		LatVec=(RelmGrid(:,3)+RelmGrid(:,4))/2;
		MagVec=(RelmGrid(:,7)+RelmGrid(:,8))/2;
		
		LonSpace=RelmGrid(1,2)-RelmGrid(1,1);	
		LatSpace=RelmGrid(1,4)-RelmGrid(1,3);
		MagSpace=RelmGrid(1,8)-RelmGrid(1,7);
		
		SmoothData=zeros(size(PoissonRate));
		
		
		switch SmoothMode
			case 'mean'
				parfor i=1:numel(LonVec)
					Buddies=(LonVec(i)-SmoothParam*LonSpace)<=LonVec&(LonVec(i)+SmoothParam*LonSpace)>LonVec&...
						(LatVec(i)-SmoothParam*LatSpace)<=LatVec&(LatVec(i)+SmoothParam*LatSpace)>LatVec&...
						(MagVec(i)-SmoothParam*MagSpace)<=MagVec&(MagVec(i)+SmoothParam*MagSpace)>MagVec;
					
					%as start just use a normally moving average without any weights
					SmoothData(i)=mean(PoissonRate(Buddies));
						
				end
			
			case 'gauss'
				
				%check if enough parameters are set
				if numel(SmoothParam)==4
					MagSmooth=true;
				elseif numel(SmoothParam)==2
					MagSmooth=false;
				elseif numel(SmoothParam)<=1
					disp('Used default smoothing parameters')
					SmoothParam=[5, 1, 5, 1];
					MagSmooth=true;
					
				end
				
			
			
				%reshape the data into 3D cube (works in this 2D+1D situation)
				%-----------------------------
				
				if isempty(SmoothConvMat)
					LonUni=unique(LonVec);
					LatUni=unique(LatVec);
					MagUni=unique(MagVec);
					PosVec=1:numel(LonVec);
					
					[XXX,YYY,ZZZ] = meshgrid(LonUni,LatUni,MagUni);
					disp('Mesh done')
				
				
					%to hold data
					CubeRate = zeros(size(XXX));
				
					PosInRay = ones(size(XXX));
					CubeSelector = false(size(XXX));
					
					%generate transformation matrixes
					for i=1:numel(PosVec)
						XPos=(LonVec(i)==XXX);
						YPos=(LatVec(i)==YYY);
						MPos=(MagVec(i)==ZZZ);
						
						CubeSelector(XPos&YPos&MPos)=true;
						PosInRay(XPos&YPos&MPos)=PosVec(i);
					end
					disp('Matrix done')
					SmoothConvMat={PosInRay,CubeSelector};
				else
					MagUni=unique(MagVec);
					PosInRay=SmoothConvMat{1};
					CubeSelector=SmoothConvMat{2};
					%to hold data
					CubeRate = zeros(size(CubeSelector));
				end
				
				%pack into cube
				CubeRate=PoissonRate(PosInRay);
				CubeRate(~CubeSelector)=0;
				
				%smooth space slice
				%------------------
				%build filter matrix
				hFilter = fspecial('gaussian',SmoothParam(1), SmoothParam(2));
				%TheVal= imfilter((TheVal), hFilter, 'replicate');
				
				for i=1:numel(MagUni)
					Slice=CubeRate(:,:,i);
					FiltSlice=filter2(hFilter,Slice);
					CubeRate(:,:,i)=FiltSlice;
				end
				disp('2D smoothing done')
				
				%smooth magbins
				if MagSmooth
					Filt1D = gausswin(SmoothParam(3),1/SmoothParam(4));
					Filt1D = Filt1D/sum(Filt1D);
					
					CubeRate = filter(Filt1D,1,CubeRate,[],3);
				
				end
				
				
				%back to vector
				%--------------
				CubeRate(~CubeSelector)=0; %to be sure
				
				RawVector=zeros(size(PoissonRate));
				RawVector(PosInRay(CubeSelector))=CubeRate(CubeSelector);
				
				SmoothData=RawVector;
				
				
			case 'ClassicETAS'
				%uses classic ETAS (Zhuang) like smoothing
				if numel(SmoothParam)<3
					disp('Used default smoothing parameters')
					SmoothParam=[0.1, 10, 0];
				end
				disp('use classic setup')
				%extract parameters
				minWidth=SmoothParam(1);
				NrBuddies=SmoothParam(2);
				UseData=logical(SmoothParam(3));
				
				UseData=true
				MagSearch=false;
				DepthSearch=false;
				AnySopMode=false;
				
				disp('getBandwith')
				%create bandwidth
				if UseData
					%BandWith = calcBandWith(NrBuddies,ShortCat,RelmGrid,MagSearch,DepthSearch);
					BandWith = calcBandWithDual(NrBuddies,ShortCat,RelmGrid,MagSearch,DepthSearch,AnySopMode);
				else
					%BandWith = calcBandWith(NrBuddies,CatalogCollegion,RelmGrid,MagSearch,DepthSearch);
					BandWith = calcBandWithDual(NrBuddies,CatalogCollegion,RelmGrid,MagSearch,DepthSearch,AnySopMode);
				end
				disp('smooth data')
				%Smooth data
				SmoothData = smoothGaussBand_mk2(PoissonRate,RelmGrid,minWidth,BandWith,Theta.b);
				
			case 'ETAS3D'
				%uses classic ETAS (Zhuang) like smoothing but magnitude as third dimension
				if numel(SmoothParam)<3
					disp('Used default smoothing parameters')
					SmoothParam=[0.1, 10, 0];
				end
				
				
				
				AnySopMode=true;
				
				%extract parameters
				minWidth(1)=SmoothParam(1);
				NrBuddies=SmoothParam(2);
				
				%other mode is not possible, due to the not present catalog
				UseData=true;
				
				if numel(SmoothParam)==4
					minWidth(2)=SmoothParam(2);
				end
				disp(NrBuddies)
				MagSearch=true;
				DepthSearch=false;
				
				%create bandwidth: if is not needed anymore, but at the moment left in just in case
				%should be removed in the next clean up
				Bandit=tic
				if UseData
					BandWith = calcBandWithDual(NrBuddies,ShortCat,RelmGrid,MagSearch,DepthSearch,AnySopMode);
				else
					BandWith = calcBandWithDual(NrBuddies,CatalogCollegion,RelmGrid,MagSearch,DepthSearch,AnySopMode);
				end
				disp(toc(Bandit));
				%Smooth data
				SmoothData = smoothGaussBand3D_mk2(PoissonRate,RelmGrid,minWidth,BandWith,AnySopMode);
				
		end	
			
		RawPoisson=PoissonRate;
		PoissonRate=SmoothData;
	else
		RawPoisson=[];
	
	end
	
	
	SmoothTime=toc(SmoothClock);
	
	
	%generate output
	%select output
	switch ModConfig.ReturnVal
		case 'Poisson'
			Forecast=PoissonRate;
			
		case 'NegBin'
			Forecast=[NegBin_P,NegBin_R];
			
		case 'Discret'
			Forecast=DiscretForecast;
	end	
	
	RawValues.meanVal=meanVal;
	RawValues.VarVal=VarVal;
	
	AddonData.PoissonOut=PoissonRate;
	AddonData.RawPoissonOut=RawPoisson;
	AddonData.NegBinOut=[NegBin_P,NegBin_R];
	AddonData.DiscretOut=DiscretForecast;
	
	AddonData.ControlData=ControlData;
	%AddonData.CatalogCollegion=CatalogCollegion;
	AddonData.Est_AddOn=EstAddonData;
	AddonData.ThetaUsed=Theta;
	AddonData.RawValues=RawValues;
	AddonData.NrSim=ModConfig.Nr_Simulation;
	
	%some other data from the mk3 generator 
	AddonData.Num_BG_Event=BGNum;
	AddonData.Num_Trig_Event=TrigNum;
	AddonData.PercTrigEvent=TrigNum./(BGNum+TrigNum);
	AddonData.sumPercTrigEvent=nansum(TrigNum)./(nansum(BGNum+TrigNum));

	%can be used to speed up the gaussian smoothing in later runs
	AddonData.SmoothConvMat=SmoothConvMat;  

	
	%Last Step add CPU time info
	CalcTimes.EstTime=EstTime;
	CalcTimes.GenTime=GenTime;
	CalcTimes.BuildTime=BuildTime;
	CalcTimes.SmoothTime=SmoothTime;
	FullTime=toc(FullClock);
	CalcTimes.FullTime=FullTime;
	AddonData.CalcTimes=CalcTimes;
		
	
	
end

function SmoothOperator = smoothGaussBand(ValToSmooth,RelmGrid,minWidth,BandWith)
	%Smooths a data vector with a gaussian smoothing kernel, with a varying 
	%Bandwith of the kernel
	
	%maybe more comments about it later.

	
	SmoothOperator=zeros(size(ValToSmooth));
	PosGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	PosGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	numMags=numel(unique(RelmGrid(:,7)));
	
	SpaceCelSel=(1:numMags:numel(PosGrid(:,1)));
	SpaceOnly=PosGrid(SpaceCelSel,:);
	
	%prepare Bandwidth
	TheBand=BandWith;
	TheBand(TheBand<minWidth)=minWidth;
	
	%UniMags(mod(i-1,numMags)+1)
	
	%normalize to the area
	CellArea=abs(RelmGrid(1,2)-RelmGrid(1,1))*abs(RelmGrid(1,4)-RelmGrid(1,3));
	FullArea=numel(PosGrid(:,1))*CellArea;
	GauSum=0;
	%NOT FULLY WORKING HAS TO BE SOMETHING MORE

	for i=1:numel(PosGrid(:,1))
	
		%select to which mag slice it is going
		ThisSlice=SpaceCelSel+mod(i-1,numMags);
		
		%calc distance to all other cells (euclidian for now)
		DistMat=((SpaceOnly(:,1)-PosGrid(i,1)).^2+(SpaceOnly(:,2)-PosGrid(i,2)).^2).^(1/2);
			
		%calculate gaussian
		% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
		GaussVal=(1/(2*pi*TheBand(i)^2))*exp((-DistMat.^2)./(2*TheBand(i)^2));
			
		%add contribution
		SmoothOperator(ThisSlice)=SmoothOperator(ThisSlice)+(ValToSmooth(i)*GaussVal);
		GauSum=GauSum+sum(GaussVal(:));
			
			
	end

	
	
	SmoothOperator=SmoothOperator/sum(SmoothOperator(:))*sum(ValToSmooth(:));
	
	
	

	
	
	
end

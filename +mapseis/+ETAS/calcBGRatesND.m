function BGRates = calcBGRatesND(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,AnySopMode)
	%creates a background rate for ETAS models, at the moment just a sketch
	%to see if it improves the model
	
	%Experiment of the sketch, a ND (3D for a start) version.
	%it works but it is a bit an "ignorant" approach in which it the possible
	%influence of small earthquakes on large earthquakes is neglected. But it
	%may be a good starting point
	
	
	import mapseis.ETAS.*;
	
	if isempty(BGProb)
		BGProb=ones(numel(ShortCat(:,1)),1);
	end
	
	if numel(BGProb)==1
		%constant probability 
		BGProb=ones(numel(ShortCat(:,1)),1)*BGProb;
	end

	if isempty(bval)
		bval=ones(numel(ShortCat(:,1)),1);
	end
	
	if numel(bval)==1
		%constant probability 
		bval=ones(numel(ShortCat(:,1)),1)*bval;
	end
	
	
	
	%generate needed variables
	
	PosGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	PosGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	PosGrid(:,3)=(RelmGrid(:,8)+RelmGrid(:,7))/2;
	
	
	%not needed in case no magnitudes is smoothed or magwise smoothing
	numMags=numel(unique(RelmGrid(:,7)));
	uniMags=[unique(RelmGrid(:,7)),unique(RelmGrid(:,8))];
	SpaceCelSel=(1:numMags:numel(PosGrid(:,1)));
	SpaceOnly=PosGrid(SpaceCelSel,:);
	BGRates=zeros(numel(PosGrid(:,1)),1);
	
	CellArea=abs(RelmGrid(1,2)-RelmGrid(1,1))*abs(RelmGrid(1,4)-RelmGrid(1,3))*abs(RelmGrid(1,8)-RelmGrid(1,7));
	FullArea=numel(PosGrid(:,1))*CellArea;
	
	MiniMag=min(RelmGrid(:,7));
	
	%create Bandwidth
	if ~AnySopMode
		ForKNN=[ShortCat(:,1:2),ShortCat(:,5)];
		[n,d] = knnsearch(ForKNN,ForKNN,'k',NrBuddys);
		TheBand = max(d,[],2);
		TheBand(TheBand<minWidth)=minWidth;
	
	else
		if numel(NrBuddys)==1
			NrBuddys(2)=NrBuddys(1);
		end
		
		if numel(minWidth)==1
			minWidth(2)=minWidth(1);
		end
		
		%Spacial bandwidth
		ForSpat=ShortCat(:,1:2);
		[n,d] = knnsearch(ForSpat,ForSpat,'k',NrBuddys(1));
		SpatBand = max(d,[],2);
		SpatBand(SpatBand<minWidth(1))=minWidth(1);

		%magnitude bandwidth
		ForMag=ShortCat(:,5);
		[n,d] = knnsearch(ForMag,ForMag,'k',NrBuddys(2));
		MagBand = max(d,[],2);
		MagBand(MagBand<minWidth(2))=minWidth(2);
		
		
	end
	
	
	%smooth seismicity
	if ~AnySopMode
		for i=1:numel(ShortCat(:,1))
			%calc distance to all other cells (euclidian for now)
			DistMat=((PosGrid(:,1)-ShortCat(i,1)).^2+(PosGrid(:,2)-ShortCat(i,2)).^2+(PosGrid(:,3)-ShortCat(i,5)).^2).^(1/2);
				
			%calculate gaussian
			% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
			GaussVal=(1/(sqrt(2*pi)*TheBand(i)^3))*exp((-DistMat.^2)./(2*TheBand(i)^2));
		
			%whereAmI=find(uniMags(:,1)<=ShortCat(i,5)&uniMags(:,2)>ShortCat(i,5))-1;
			%if ~isempty(whereAmI)
			%	ThisSlice=SpaceCelSel+whereAmI;
			%	BGRates(ThisSlice)=BGRates(ThisSlice)+(BGProb(i)/TimeLeng*GaussVal);
			%end
			
			BGRates=BGRates+(BGProb(i)*GaussVal);
		end
		
	else
		GVal=zeros(size(ShortCat(:,1)));
		GVKern=[];
		MaxVal=0;
		for i=1:numel(ShortCat(:,1))
			%calc distance to all other cells (euclidian for now)
			%DistMatSpatX=((PosGrid(:,1)-ShortCat(i,1)).^2).^(1/2);
			%DistMatSpatY=((PosGrid(:,2)-ShortCat(i,2)).^2).^(1/2);
			%DistMatMag=((PosGrid(:,3)-ShortCat(i,5)).^2).^(1/2);
			
			%DistMatSpat=distance(PosGrid(:,2),PosGrid(:,1),ShortCat(i,2),ShortCat(i,1));
			DistMatSpat=((PosGrid(:,1)-ShortCat(i,1)).^2+(PosGrid(:,2)-ShortCat(i,2)).^2).^(1/2);
			
			DistMatMag=((PosGrid(:,3)-ShortCat(i,5)).^2).^(1/2);
			
			
			%calculate gaussian
			% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
			GaussVal=(1/(2*pi*SpatBand(i)^2*MagBand(i)))*exp(-((DistMatSpat.^2)./(2*SpatBand(i)^2)+(DistMatMag.^2)./(2*MagBand(i)^2)));
			%GaussVal=GaussVal*CellArea;
			%just a test
			%if ShortCat(i,5)<MiniMag;
			%	Penalty=10.^(-(abs(PosGrid(:,3)-ShortCat(i,5)).*bval(i)));
			%	GaussVal=GaussVal.*Penalty;
				
			%end
			
			%whereAmI=find(uniMags(:,1)<=ShortCat(i,5)&uniMags(:,2)>ShortCat(i,5))-1;
			%if ~isempty(whereAmI)
			%	ThisSlice=SpaceCelSel+whereAmI;
			%	BGRates(ThisSlice)=BGRates(ThisSlice)+(BGProb(i)/TimeLeng*GaussVal);
			%end
			%disp(sum(GaussVal))
			GVal(i)=sum(GaussVal);
			
			BGRates=BGRates+(BGProb(i)*GaussVal);
			if GVal(i)>MaxVal
				GVKern=GaussVal;
				MaxVal=GVal(i);
			end
		end
		
	end
	[val ind]=max(GVal)
	%disp(val);
	%disp(ind);
	%disp(SpatBand(ind));
	%disp(MagBand(ind));
	%disp(max(SpatBand));
	%disp(max(MagBand));
	
	
	Temp=RelmGrid;
	Temp(:,9)=BGRates/(TimeLeng);%*FullArea);
	%BGRates=Temp;
	BGRates={Temp,GVal,SpatBand,MagBand,GVKern};
end

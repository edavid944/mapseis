function [Forecast AddonData] = EtasForecaster_test(ShortCat,currentTime,TimeLength,RelmGrid,ModConfig,ThetaStart)
	%prototype of an Etas forecast based on Schoenberg
	FullClock=tic;
	
	%TODO: 	add magnitude correction (subtract Mc) to EtasGenerator
	%	use whole past seismicity for the generator
	%	integrate a gaussian smoothing (spacial and magnitude seperate)
	
	import mapseis.ETAS.*;
	
	%for experimentation of performance, will be deleted eventually	
	%LoopMode='Catalogs';
	LoopMode='Nodes';
	%LoopMode='EQ';
	%Smoothing=true;
	%SmoothDist=3;


	
	if isempty(ModConfig)
		%Default config
		ModConfig=struct(	'ReturnVal','Poisson',...
					'EstimatePara',true,...
					'EqGeneration','normal',...
					'BG_Distro',RelmGrid,...
					'GenTimeBefore',TimeLength,...
					'LearnStartTime',min(ShortCat(:,4)),...
					'MinMag',min(RelmGrid(:,7)),...
					'MinMainMag',min(RelmGrid(:,7)),...
					'GenEqList',[[]],...
					'Gen1stGen',true,...
					'Nr_Simulation',10000,...
					'Smoothing',true,...
					'SmoothMode','mean',...
					'SmoothParam',3,...
					'SmoothConvMat',{{}});
		
	end

	if isempty(currentTime)
		currentTime=0; %for some modes ok
	end	
	
	if isempty(ThetaStart)
		ThetaStart=struct(	'mu',5.250,...
					'b',1,...
					'K',0.200,...
					'c',0.025,...
					'p',1.500,...
					'a',0.700,...
					'd',0.025,...
					'q',1.500);
	end
	
	
	try
		b = ThetaStart.b;
	catch	
		disp('b value not set used 1')
		ThetaStart.b = 1;
	end
	
	%unpack smoothing parameters
	Smoothing=ModConfig.Smoothing;
	SmoothMode=ModConfig.SmoothMode;
	SmoothParam=ModConfig.SmoothParam;
	
	SmoothConvMat={};
	if isfield(ModConfig,'SmoothConvMat')
		disp('imported conversion matrix')
		SmoothConvMat=ModConfig.SmoothConvMat;
	end
	
	%set reference point (add 10% oversize)
	SpatRefPoint=[min(RelmGrid(:,1)),min(RelmGrid(:,3))];
	TopLeftPoint=[max(RelmGrid(:,2)),max(RelmGrid(:,4))];
	SpatRefPoint=SpatRefPoint-abs(SpatRefPoint*0.05);
	TopLeftPoint=TopLeftPoint+abs(TopLeftPoint*0.05);
	MinMag=ModConfig.MinMag;%min(RelmGrid(:,7));
	
	if isfield(ModConfig,'MinMainMag')
		
		MinMainMag=ModConfig.MinMainMag;
	else
		MinMainMag=MinMag;	
	end
	
	
	
	EstPar=ModConfig.EstimatePara;
	%Estimate ETAS parameter
	if EstPar
		EstClock=tic
		%[EstValues, EstAddonData] = EtasEstimator(ShortCat,TimeLength,SpatRefPoint,TopLeftPoint,MinMag,ThetaStart);
		[EstValues, EstAddonData] = EtasEstimator(ShortCat,ModConfig.GenTimeBefore,SpatRefPoint,TopLeftPoint,MinMag,ThetaStart);
		EstTime=toc(EstClock);
	else
		EstValues=ThetaStart;
		EstAddonData=[];
		EstTime=NaN;
	end	
	
	%build generator data
	Theta=EstValues;
	Theta.b=ThetaStart.b;
	
	%Not needed anymore, but leave it for now (needed agaib :-)
	if strcmp(ModConfig.EqGeneration,'normalBG')
		ModConfig.BG_Distro(:,9)=ModConfig.BG_Distro(:,9)*Theta.mu;
		ModConfig.EqGeneration='normal';
		
	end
	
	
	MainshockStruct = struct(	'Mode',ModConfig.EqGeneration,...
					'Distro',ModConfig.BG_Distro,...
					'UseStartTime',true,...
					'MinMainMag',MinMainMag,...
					'TimeBefore',ModConfig.LearnStartTime,...
					'EqList',ModConfig.GenEqList,...
					'Generate1stGen',ModConfig.Gen1stGen);
	
	if isempty(MainshockStruct.TimeBefore)			
		MainshockStruct.TimeBefore=ModConfig.GenTimeBefore;
		MainshockStruct.UseStartTime=false;
	end
	
	%generate catalogs
	GenClock=tic;
	CatalogCollegion=[];
	
	%for i=1:ModConfig.Nr_Simulation
	%	OutStruct = EtasGenerator_mk1(Theta,SpatRefPoint,TopLeftPoint,TimeLength,currentTime,ShortCat,MainshockStruct,true,true);
	%	try %there is sometimes nothing generated
	%		CurCat=OutStruct.GenShortCat;
	%		CurCat(:,6)=ones(numel(CurCat(:,1)),1)*i; %mark the run
	%		CatalogCollegion=[CatalogCollegion;CurCat];
	%		disp(size(CatalogCollegion));
	%	end
				
		
	%end

	%paralizable version
	TheCats=cell(ModConfig.Nr_Simulation,1);
	OrgMarkers=cell(ModConfig.Nr_Simulation,1);
	
	if strcmp(MainshockStruct.Mode,'normalETAS')
		OutStruct = EtasGenerator_mk3(Theta,SpatRefPoint,TopLeftPoint,TimeLength,currentTime,MinMag,ShortCat,MainshockStruct,true,true);
		MainshockStruct.BGRates=OutStruct.BGRates;
		disp(OutStruct.BGRates)
	end
	
	numSim=ModConfig.Nr_Simulation
	

	parfor i=1:numSim
		OutStruct = EtasGenerator_mk3(Theta,SpatRefPoint,TopLeftPoint,TimeLength,currentTime,MinMag,ShortCat,MainshockStruct,true,true);
		if ~isempty(OutStruct) %there is sometimes nothing generated
			CurCat=OutStruct.GenShortCat;
			CurCat(:,6)=ones(numel(CurCat(:,1)),1)*i; %mark the run
			TheCats{i}=CurCat;
			
			%additional data
			OrgMarkers{i}=OutStruct.OriginMarker;
			BGNum(i)=OutStruct.Nr_BG;
			TrigNum(i)=OutStruct.Nr_Trig;
		else
			TheCats{i}=[];
			
			OrgMarkers{i}=[];
			BGNum(i)=NaN;
			TrigNum(i)=NaN;
		end
				
		
	end
	
	
	CatalogCollegion=cell2mat(TheCats);
	NotBG_Eq=logical(cell2mat(OrgMarkers));
	disp(size(CatalogCollegion));
	
	GenTime=toc(GenClock);
	
	
	%generate forecast
	%A bit messy here at the moment because it needs a loop of optimization
	
	BuildClock=tic;
	disp(max(CatalogCollegion))
	
	%Kick out everything which happens before the current time and after the forecast length
	InFocus=CatalogCollegion(:,4)>=currentTime&CatalogCollegion(:,4)<currentTime+TimeLength;
	InMag=CatalogCollegion(:,5)>=min(RelmGrid(:,7))&CatalogCollegion(:,5)<=max(RelmGrid(:,8));
	disp(max(CatalogCollegion(:,5)))
	CatalogCollegion=CatalogCollegion(InFocus&InMag,:);
	disp(sum(InMag&InFocus));
	
	%prepare arrays to hold the forecast
	DiscretForecast=cell(numel(RelmGrid(:,1)),1);
	meanVal=NaN(numel(RelmGrid(:,1)),1);
	VarVal=NaN(numel(RelmGrid(:,1)),1);
	ControlData={} %just to check that everything works fine will be deleted later
		
	%Resort grid
	[Val idx]=sort(CatalogCollegion(:,6));
	CatalogCollegion=CatalogCollegion(idx,:);
	disp(size(CatalogCollegion));
	
	%quite slow, are there ways to improve it?
	if strcmp(LoopMode,'Nodes')
	
		parfor i=1:numel(RelmGrid(:,1))
			
			InNode=(CatalogCollegion(:,1)>=RelmGrid(i,1)&CatalogCollegion(:,1)<RelmGrid(i,2))&...
				(CatalogCollegion(:,2)>=RelmGrid(i,3)&CatalogCollegion(:,2)<RelmGrid(i,4))&...
				(CatalogCollegion(:,5)>=RelmGrid(i,7)&CatalogCollegion(:,5)<RelmGrid(i,8));
			
			if sum(InNode>=1)	
				ThisCat=CatalogCollegion(InNode,:);
						
				%TryNrs=sort(ThisCat(:,6));
				TryNrs=ThisCat(:,6);
				
				%Find how many times the cell was hit by each catalog
				[UniNum,FirstOcc]=unique(TryNrs,'first');
				[Temp,LastOcc]=unique(TryNrs,'last');
				HowManyTime=LastOcc-FirstOcc+1;
				TheZeros=ModConfig.Nr_Simulation-numel(UniNum);
				
				%now check how common 0,1,2,... hits are 
				HowManyTime=sort(HowManyTime);
				OccVector=([zeros(TheZeros,1);HowManyTime]);
				[HitCount,FirstOcc]=unique(TryNrs,'first');
				[Temp,LastOcc]=unique(TryNrs,'last');
				MrCommon=LastOcc-FirstOcc+1;
				
				meanVal(i)=mean(OccVector);
				VarVal(i)=var(OccVector);
				DiscretForecast{i}=MrCommon/ModConfig.Nr_Simulation;
				ControlData{i}=[HitCount,MrCommon];
				disp(sum(InNode))
			else
				meanVal(i)=0;
				VarVal(i)=0;
				DiscretForecast{i}=1;
				ControlData{i}=[numSim,0];
				
			end
			
		end
		
		
		%A faster version is needed of the loop above, I guess I have to loop over
		%a different quantity than the grid nodes, because most of them are empty. 
		%Example: ~500000 nodes, only 14500 Earthquake, so at best 3% of all nodes are
		%needed to be passed.
		
		%so either I have to find a way to exlude a majority of grid nodes (preselect
		%grid nodes with a histogram e.g.) or better I loop over synthetic earthquakes
		%or more consistent over the number of synthetic catalogs
	
		
		
	elseif strcmp(LoopMode,'Catalogs')
		
		DiscretForecast=repmat({[1]},numel(RelmGrid(:,1)),1);
		IncPos=1/ModConfig.Nr_Simulation;
		ControlData={};
		for i=1:ModConfig.Nr_Simulation
			if ~isempty(TheCats{i})
				%double loop but it is a small one
				NrUsed=ones(numel(RelmGrid(:,1)),1);
				for j=1:numel(TheCats{i}(:,1))
					InNode=(TheCats{i}(j,1)>=RelmGrid(:,1)&TheCats{i}(j,1)<RelmGrid(:,2))&...
						(TheCats{i}(j,2)>=RelmGrid(:,3)&TheCats{i}(j,2)<RelmGrid(:,4))&...
						(TheCats{i}(j,5)>=RelmGrid(:,7)&TheCats{i}(j,5)<RelmGrid(:,8))&...
						(TheCats{i}(j,4)>=currentTime&TheCats{i}(j,4)<currentTime+TimeLength);
						if any(InNode)
							NrUsed(InNode)=NrUsed(InNode)+1;
							DiscretForecast{InNode}(NrUsed(InNode))=DiscretForecast{InNode}(NrUsed(InNode))+IncPos;
							DiscretForecast{InNode}(1)=DiscretForecast{InNode}(1)-IncPos;		
						end
				end
				
				
			end
		
		
		end
		
		meanVal=cellfun(@(x) mean(x),DiscretForecast);
		VarVal=cellfun(@(x) var(x),DiscretForecast);
		
		
	elseif strcmp(LoopMode,'EQ')
		CellUsage=cell(numel(RelmGrid(:,1)),1);
		DiscretForecast=repmat({[1]},numel(RelmGrid(:,1)),1);
		IncPos=1/ModConfig.Nr_Simulation;
		ControlData={};
		
		for i=1:numel(CatalogCollegion(:,1))
			InNode=(CatalogCollegion(i,1)>=RelmGrid(:,1)&CatalogCollegion(i,1)<RelmGrid(:,2))&...
				(CatalogCollegion(i,2)>=RelmGrid(:,3)&CatalogCollegion(i,2)<RelmGrid(:,4))&...
				(CatalogCollegion(i,5)>=RelmGrid(:,7)&CatalogCollegion(i,5)<RelmGrid(:,8))&...
				(CatalogCollegion(i,4)>=currentTime&CatalogCollegion(i,4)<currentTime+TimeLength);
			disp(sum(InNode))
			if any(InNode)
				HitBefore=CellUsage{InNode};
				disp('found one')
				if isempty(HitBefore)
					HitBefore=CatalogCollegion(i,6);
					DiscretForecast{InNode}(2)=DiscretForecast{InNode}(2)+IncPos;
					DiscretForecast{InNode}(1)=DiscretForecast{InNode}(1)-IncPos;	
					CellUsage{InNode}=HitBefore;
					
				else
					HowOften=HitBefore==CatalogCollegion(i,6);
					PosToWrite=sum(HowOften)+2;
					DiscretForecast{InNode}(PosToWrite)=DiscretForecast{InNode}(PosToWrite)+IncPos;
					DiscretForecast{InNode}(1)=DiscretForecast{InNode}(1)-IncPos;	
					HitBefore(end+1)=CatalogCollegion(i,6);
					CellUsage{InNode}=HitBefore;
				
				
				end
				
			end
		end
		
		meanVal=cellfun(@(x) mean(x),DiscretForecast);
		%NrEQ=cellfun(@(x) 0:numel(x)-1,DiscretForecast);
		VarVal=cellfun(@(x) var(x),DiscretForecast);
		
	
	end
	
	%Finish the statistic
	PoissonRate=meanVal;
	NegBin_P=meanVal./VarVal;
	NegBin_R=(NegBin_P.*VarVal)./(1-NegBin_P);
	
	%stop clock
	BuildTime=toc(BuildClock);
	
	%Smoothing prototype(just a moving average)
	SmoothClock=tic;
	if Smoothing
		LonVec=(RelmGrid(:,1)+RelmGrid(:,2))/2;
		LatVec=(RelmGrid(:,3)+RelmGrid(:,4))/2;
		MagVec=(RelmGrid(:,7)+RelmGrid(:,8))/2;
		
		LonSpace=RelmGrid(1,2)-RelmGrid(1,1);	
		LatSpace=RelmGrid(1,4)-RelmGrid(1,3);
		MagSpace=RelmGrid(1,8)-RelmGrid(1,7);
		
		SmoothData=zeros(size(PoissonRate));
		
		
		switch SmoothMode
			case 'mean'
				parfor i=1:numel(LonVec)
					Buddies=(LonVec(i)-SmoothParam*LonSpace)<=LonVec&(LonVec(i)+SmoothParam*LonSpace)>LonVec&...
						(LatVec(i)-SmoothParam*LatSpace)<=LatVec&(LatVec(i)+SmoothParam*LatSpace)>LatVec&...
						(MagVec(i)-SmoothParam*MagSpace)<=MagVec&(MagVec(i)+SmoothParam*MagSpace)>MagVec;
					
					%as start just use a normally moving average without any weights
					SmoothData(i)=mean(PoissonRate(Buddies));
						
				end
			
			case 'gauss'
				
				%check if enough parameters are set
				if numel(SmoothParam)==4
					MagSmooth=true;
				elseif numel(SmoothParam)==2
					MagSmooth=false;
				elseif numel(SmoothParam)<=1
					disp('Used default smoothing parameters')
					SmoothParam=[5, 1, 5, 1];
					MagSmooth=true;
					
				end
				
			
			
				%reshape the data into 3D cube (works in this 2D+1D situation)
				%-----------------------------
				
				if isempty(SmoothConvMat)
					LonUni=unique(LonVec);
					LatUni=unique(LatVec);
					MagUni=unique(MagVec);
					PosVec=1:numel(LonVec);
					
					[XXX,YYY,ZZZ] = meshgrid(LonUni,LatUni,MagUni);
					disp('Mesh done')
				
				
					%to hold data
					CubeRate = zeros(size(XXX));
				
					PosInRay = ones(size(XXX));
					CubeSelector = false(size(XXX));
					
					%generate transformation matrixes
					for i=1:numel(PosVec)
						XPos=(LonVec(i)==XXX);
						YPos=(LatVec(i)==YYY);
						MPos=(MagVec(i)==ZZZ);
						
						CubeSelector(XPos&YPos&MPos)=true;
						PosInRay(XPos&YPos&MPos)=PosVec(i);
					end
					disp('Matrix done')
					SmoothConvMat={PosInRay,CubeSelector};
				else
					MagUni=unique(MagVec);
					PosInRay=SmoothConvMat{1};
					CubeSelector=SmoothConvMat{2};
					%to hold data
					CubeRate = zeros(size(CubeSelector));
				end
				
				%pack into cube
				CubeRate=PoissonRate(PosInRay);
				CubeRate(~CubeSelector)=0;
				
				%smooth space slice
				%------------------
				%build filter matrix
				hFilter = fspecial('gaussian',SmoothParam(1), SmoothParam(2));
				%TheVal= imfilter((TheVal), hFilter, 'replicate');
				
				for i=1:numel(MagUni)
					Slice=CubeRate(:,:,i);
					FiltSlice=filter2(hFilter,Slice);
					CubeRate(:,:,i)=FiltSlice;
				end
				disp('2D smoothing done')
				
				%smooth magbins
				if MagSmooth
					Filt1D = gausswin(SmoothParam(3),1/SmoothParam(4));
					Filt1D = Filt1D/sum(Filt1D);
					
					CubeRate = filter(Filt1D,1,CubeRate,[],3);
				
				end
				
				
				%back to vector
				%--------------
				CubeRate(~CubeSelector)=0; %to be sure
				
				RawVector=zeros(size(PoissonRate));
				RawVector(PosInRay(CubeSelector))=CubeRate(CubeSelector);
				
				SmoothData=RawVector;
				
				
			case 'classicETAS'
				%uses classic ETAS (Zhuang) like smoothing
				if numel(SmoothParam)<3
					disp('Used default smoothing parameters')
					SmoothParam=[0.01, 10, 0];
				end
				
				%extract parameters
				minWidth=SmoothParam(1);
				NrBuddies=SmoothParam(2);
				UseData=logical(SmoothParam(3));
				
				MagSearch=false;
				DepthSearch=false;
				
				%create bandwidth
				if UseData
					BandWith = calcBandWith(NrBuddies,ShortCat,RelmGrid,MagSearch,DepthSearch);
				else
					BandWith = calcBandWith(NrBuddies,CatalogCollegion,RelmGrid,MagSearch,DepthSearch);
				end
				
				%Smooth data
				SmoothData = smoothGaussBand(PoissonRate,RelmGrid,minWidth,BandWith);
				
			case 'ETAS3D'
				%uses classic ETAS (Zhuang) like smoothing but magnitude as third dimension
				if numel(SmoothParam)<3
					disp('Used default smoothing parameters')
					SmoothParam=[0.1, 10, 0];
				end
				
				
				
				AnySopMode=true;
				
				%extract parameters
				minWidth(1)=SmoothParam(1);
				NrBuddies=SmoothParam(2);
				UseData=logical(SmoothParam(3));
				
				if numel(SmoothParam)==4
					minWidth(2)=SmoothParam(2);
				end
				disp(NrBuddies)
				MagSearch=true;
				DepthSearch=false;
				
				%create bandwidth
				Bandit=tic
				if UseData
					BandWith = calcBandWithDual(NrBuddies,ShortCat,RelmGrid,MagSearch,DepthSearch,AnySopMode);
				else
					BandWith = calcBandWithDual(NrBuddies,CatalogCollegion,RelmGrid,MagSearch,DepthSearch,AnySopMode);
				end
				disp(toc(Bandit));
				%Smooth data
				SmoothData = smoothGaussBand3D_mk2(PoissonRate,RelmGrid,minWidth,BandWith,AnySopMode);
				
		end	
			
		RawPoisson=PoissonRate;
		PoissonRate=SmoothData;
	else
		RawPoisson=[];
	
	end
	
	
	SmoothTime=toc(SmoothClock);
	
	
	%generate output
	%select output
	switch ModConfig.ReturnVal
		case 'Poisson'
			Forecast=PoissonRate;
			
		case 'NegBin'
			Forecast=[NegBin_P,NegBin_R];
			
		case 'Discret'
			Forecast=DiscretForecast;
	end	
	
	RawValues.meanVal=meanVal;
	RawValues.VarVal=VarVal;
	
	AddonData.PoissonOut=PoissonRate;
	AddonData.RawPoissonOut=RawPoisson;
	AddonData.NegBinOut=[NegBin_P,NegBin_R];
	AddonData.DiscretOut=DiscretForecast;
	
	AddonData.ControlData=ControlData;
	AddonData.CatalogCollegion=CatalogCollegion;
	AddonData.Est_AddOn=EstAddonData;
	AddonData.ThetaUsed=Theta;
	AddonData.RawValues=RawValues;
	AddonData.NrSim=ModConfig.Nr_Simulation;
	
	%some other data from the mk3 generator 
	AddonData.Num_BG_Event=BGNum;
	AddonData.Num_Trig_Event=TrigNum;
	AddonData.PercTrigEvent=TrigNum./(BGNum+TrigNum);
	AddonData.sumPercTrigEvent=nansum(TrigNum)./(nansum(BGNum+TrigNum));

	%can be used to speed up the gaussian smoothing in later runs
	AddonData.SmoothConvMat=SmoothConvMat;  

	
	%Last Step add CPU time info
	CalcTimes.EstTime=EstTime;
	CalcTimes.GenTime=GenTime;
	CalcTimes.BuildTime=BuildTime;
	CalcTimes.SmoothTime=SmoothTime;
	FullTime=toc(FullClock);
	CalcTimes.FullTime=FullTime;
	AddonData.CalcTimes=CalcTimes;
		
	
	
end

function BGRates = calcBGRates(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval)
	%creates a background rate for ETAS models, at the moment just a sketch
	%to see if it improves the model
	
	import mapseis.ETAS.*;
	
	if isempty(BGProb)
		BGProb=ones(numel(ShortCat(:,1)),1);
	end
	
	if numel(BGProb)==1
		%constant probability 
		BGProb=ones(numel(ShortCat(:,1)),1)*BGProb;
	end

	if isempty(bval)
		bval=ones(numel(ShortCat(:,1)),1);
	end
	
	if numel(bval)==1
		%constant probability 
		bval=ones(numel(ShortCat(:,1)),1)*bval;
	end
	
	
	
	%generate needed variables
	
	PosGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	PosGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	
	%not needed in case no magnitudes is smoothed or magwise smoothing
	numMags=numel(unique(RelmGrid(:,7)));
	uniMags=[unique(RelmGrid(:,7)),unique(RelmGrid(:,8))];
	SpaceCelSel=(1:numMags:numel(PosGrid(:,1)));
	SpaceOnly=PosGrid(SpaceCelSel,:);
	BGRates=zeros(numel(SpaceOnly(:,1)),1);
	
	CellArea=abs(RelmGrid(1,2)-RelmGrid(1,1))*abs(RelmGrid(1,4)-RelmGrid(1,3));
	FullArea=numel(PosGrid(:,1))*CellArea;

	%create Bandwidth
	[n,d] = knnsearch(ShortCat(:,1:2),ShortCat(:,1:2),'k',NrBuddys);
	TheBand = max(d,[],2);
	TheBand(TheBand<minWidth)=minWidth;
	
	
	%smooth seismicity
	for i=1:numel(ShortCat(:,1))
		%calc distance to all other cells (euclidian for now)
		DistMat=((SpaceOnly(:,1)-ShortCat(i,1)).^2+(SpaceOnly(:,2)-ShortCat(i,2)).^2).^(1/2);
			
		%calculate gaussian
		% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
		GaussVal=(1/(2*pi*TheBand(i)^2))*exp((-DistMat.^2)./(2*TheBand(i)^2));
	
		%whereAmI=find(uniMags(:,1)<=ShortCat(i,5)&uniMags(:,2)>ShortCat(i,5))-1;
		%if ~isempty(whereAmI)
		%	ThisSlice=SpaceCelSel+whereAmI;
		%	BGRates(ThisSlice)=BGRates(ThisSlice)+(BGProb(i)/TimeLeng*GaussVal);
		%end
		
		BGRates=BGRates+(BGProb(i)*GaussVal);
	end
	
	BGRates=[SpaceOnly,BGRates/(TimeLeng*FullArea)];
	

end

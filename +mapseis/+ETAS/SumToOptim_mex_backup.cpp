//mex file (C++ code) version of the SumToOptim
//ironically this means it has come from C and goes to C
//%based on the work of Schoenberg, BSSA, 2012
//%estimates the parameter for a ETAS model, the code is lously 
//%based on a C code by Rick Schoenberg (myss2.c)

//This function has to be minized with fminsearch or similar

//original call in matlab: 
//function ToMin = SumToOptimPar_b(WorkCat,TimeLength,SpatialExtend,ThetaStart,bval)


//#include <math.h>
#include <cmath>
#include <matrix.h>
#include <mex.h>


/* Definitions to keep compatibility with earlier versions of ML */
#ifndef MWSIZE_MAX
typedef int mwSize;
typedef int mwIndex;
typedef int mwSignedIndex;

#if (defined(_LP64) || defined(_WIN64)) && !defined(MX_COMPAT_32)
/* Currently 2^48 based on hardware limitations */
# define MWSIZE_MAX    281474976710655UL
# define MWINDEX_MAX   281474976710655UL
# define MWSINDEX_MAX  281474976710655L
# define MWSINDEX_MIN -281474976710655L
#else
# define MWSIZE_MAX    2147483647UL
# define MWINDEX_MAX   2147483647UL
# define MWSINDEX_MAX  2147483647L
# define MWSINDEX_MIN -2147483647L
#endif
#define MWSIZE_MIN    0UL
#define MWINDEX_MIN   0UL
#endif


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]  )
{
	//declare variables (most needed from start anyway)
	
	//input
	mxArray *cat_in, *theta_in, *timelen_in, *spatExt_in, *bval_in;
	const mwSize *dimcat;
	
	//output
	mxArray *likehood_out;
	
	//pointers to the IO
	double *eqcat, *theta, *timelen, *spatExt, *bval, *likehood;
	
	//variables
	long numEq;
	int i,j;
	const double pi=3.141593, inf = mxGetInf(),nan = mxGetNaN();
	//double inf,nan; //matlab constants
	double br, sum1, sum2, r2, const1, mu, b, K, c, p, a, d, q, lam1;
	
	
	
	 /* Check for proper number of input and output arguments */    
	 if (nrhs != 5) {
	 	 mexErrMsgIdAndTxt( "MATLAB:mxgetinf:invalidNumInputs",
	 	 	 "One input argument required.");
	 } 
	 
	 if(nlhs > 1){
	 	 mexErrMsgIdAndTxt( "MATLAB:mxgetinf:maxlhs",
	 	 	 "Too many output arguments.");
	 }
    
		
	
	
	
	
	//assign input data 
	cat_in = mxDuplicateArray(prhs[0]);
	timelen_in = mxDuplicateArray(prhs[1]);
	spatExt_in = mxDuplicateArray(prhs[2]);
	theta_in = mxDuplicateArray(prhs[3]);
	bval_in = mxDuplicateArray(prhs[4]);
	//I guess I could access directly the arrays, without duplicating them
	//but it is a bit saver like that and I guess the performance should be
	//still alright
	
	//determine size of the catalog
	dimcat = mxGetDimensions(prhs[0]);
	numEq = dimcat[0]; //I hope it is the right dimension
	
	//assign output 
	likehood_out = plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
	//exotic syntax
	
	//assign the pointers
	eqcat = mxGetPr(cat_in);
	theta = mxGetPr(theta_in);
	timelen = mxGetPr(timelen_in);
	spatExt = mxGetPr(spatExt_in);
	bval = mxGetPr(bval_in);
	likehood = mxGetPr(likehood_out);
	
	//unpack theta
	mu = theta[0];
	K = theta[1];
	c = theta[2];
	p = theta[3];
	a = theta[4];
	d = theta[5];
	q = theta[6];
	b = *bval;
	
	
	//Debug Output
	//mexPrintf("Num. Eq: %d\n",numEq);
	//mexPrintf("Num. Input: %d\n",nrhs);
	//mexPrintf("mu,K,c,p,a,d,q,b: %f, %f, %f, %f, %f, %f, %f, %f, \n",mu,K,c,p,a,d,q,b);
	
	
	//check if the parameters are still ok
	
	br = (K * b) / (b-a);
	if ((br>1) || (a >=b)){
		*likehood=999999999999;
		//mexPrintf("br to high: %f\n",br);
		//return to matlab
		mxDestroyArray(cat_in);
		mxDestroyArray(theta_in);
		mxDestroyArray(timelen_in);
		mxDestroyArray(spatExt_in);
		mxDestroyArray(bval_in);
		//mxDestroyArray(likehood_out);
		
		return;
	}
	
	if  (((p-1)<0.00000001) || ((q-1)<0.00000001)){	
		*likehood=999999999999;
		//mexPrintf("p or q to near to 1,p=%f, q=%f\n",p,q);
		//return to matlab
		mxDestroyArray(cat_in);
		mxDestroyArray(theta_in);
		mxDestroyArray(timelen_in);
		mxDestroyArray(spatExt_in);
		mxDestroyArray(bval_in);
		//mxDestroyArray(likehood_out);
		return;
	}
	
	bool btoolow = false;
	btoolow = (mu<0.00000001) || (K<0.00000001) || (c<0.00000001) || (p<0.00000001)
			|| (a<0.00000001) || (d<0.00000001) || (q<0.00000001);
			
	if  (btoolow){
		*likehood=999999999999;
		//mexPrintf("p or q to near to 1,p=%f, q=%f\n",p,q);
		//return to matlab
		mxDestroyArray(cat_in);
		mxDestroyArray(theta_in);
		mxDestroyArray(timelen_in);
		mxDestroyArray(spatExt_in);
		mxDestroyArray(bval_in);
		//mxDestroyArray(likehood_out);
		return;
	}
	
	
	if  (mu<0){	
		*likehood=999999999999;
		mexPrintf("mu is negative%f\n",mu);
		//return to matlab
		mxDestroyArray(cat_in);
		mxDestroyArray(theta_in);
		mxDestroyArray(timelen_in);
		mxDestroyArray(spatExt_in);
		mxDestroyArray(bval_in);
		//mxDestroyArray(likehood_out);
		return;
	}
	
	
	
	//determine productivity
	sum1 = 0.0;
	for (i=0;i<=numEq;i++)
	{
		sum1 += exp(a * eqcat[4*numEq+i]);
		
	}
	
	
	//init likelihood
	*likehood = mu * *timelen + K * sum1 - log(mu / (spatExt[0] * spatExt[1]));
	//mexPrintf("like: %f\n",sum1);
	
	
	//calc parts not changing during the loop
	const1 = K * (p-1) * pow(c,(p-1)) * (q-1) * pow(d,(q-1)) / pi;
	
	//mexPrintf("const1: %e\n",const1);
	
	for (i=1;i<numEq;i++)
	{
		//mexPrintf("Eq [%d]: %f, %f, %f, %f, %f \n",eqcat[i*numEq+0],eqcat[i*numEq+1],eqcat[i*numEq+2],eqcat[i*numEq+3],eqcat[i*numEq+4]);
		//mexPrintf("Eq [%d]: %f, %f, %f, %f, %f \n",i,eqcat[0*numEq+i],eqcat[1*numEq+i],eqcat[2*numEq+i],eqcat[3*numEq+i],eqcat[4*numEq+i]);
		//mexPrintf("\n");
		sum2=0.0;
		for (j=0;j < i;j++)
		{
			//r2 = pow((eqcat[i*numEq]-eqcat[j*numEq]),2.0) + pow((eqcat[i*numEq+1]-eqcat[j*numEq+1]),2.0);
			//sum2 =+ pow(eqcat[i*numEq+3]-eqcat[j*numEq+3]+c,(-p)) * exp(a*eqcat[j*numEq+4]) * pow((r2+d),(-q));
			
			r2 = (eqcat[0*numEq+i]-eqcat[0*numEq+j])*(eqcat[0*numEq+i]-eqcat[0*numEq+j]) 
				+ (eqcat[1*numEq+i]-eqcat[1*numEq+j])*(eqcat[1*numEq+i]-eqcat[1*numEq+j]);
			sum2 += pow(eqcat[3*numEq+i]-eqcat[3*numEq+j]+c,(-p)) * exp(a*eqcat[4*numEq+j]) * pow((r2+d),(-q));
			//mexPrintf("r2: %f\n",r2);
			//mexPrintf("sum2: %f\n",sum2);
		
		}
		//mexPrintf("sum2: %le\n",sum2);
		
		
		lam1 = mu / (spatExt[0] * spatExt[1]) + const1 * sum2; 
		//mexPrintf("lam1: %f\n",lam1);
		if (lam1 < 0.0000000001) {
			*likehood = 999999999999;
			//mexPrintf("lam1 to small: %f\n",lam1);
		}
		else{
			*likehood -= log(lam1);
		}
		
		
		
	}	
	
	//avoid infs and nans
	if ((*likehood==inf) || (*likehood==-(inf)) || (*likehood==nan)){
		//mexPrintf("likelihood was inf or nan: %f \n",*likehood);
		*likehood = 999999999999;
		
	}
	
	
	//clean up the variables in, so nothing wrong happens 
	//I
	mxDestroyArray(cat_in);
	mxDestroyArray(theta_in);
	mxDestroyArray(timelen_in);
	mxDestroyArray(spatExt_in);
	mxDestroyArray(bval_in);
	//mxDestroyArray(likehood_out);
	
	
}




	
	
	
	
	





	/* %DELETE after TESTING
	%original code
	%  #include <R.h>
	%  #include <Rmath.h>
	%  
	%  //Remember *a is a pointer to a variable "a"
	%  void ss2 (double *x, double *y, double *t, double *hm, int *n,
		  %  double *x1, double *y1, double *T, double *theta, 
		  %  double *a2){
	    %  
	    %  int i,j;
	    %  double sum1, sum2, r2, const1, mu, K, c, p, a, d, q, lam1;
	    %  mu = theta[0]; K = theta[1]; c = theta[2];
	    %  p = theta[3]; a = theta[4]; d = theta[5]; q = theta[6];
	    %  sum1 = 0.0;
		%  //shouldn't it be pointers as well for theta?  
		%  //Nope, normally only the pointer of an array only points to 
		%  //the first element of an array  
	%  
		%  for(i = 0; i < *n; i++){
			%  sum1 += exp(a*hm[i]);
	    %  }
		%  //I guess that's a something like the total expect number of events
		%  // or so
	%  
		%  *a2 = mu * *T + K*sum1 - log(mu/(*x1 * *y1));
	    %  const1 = K * (p-1)* pow(c,(p-1)) * (q-1) * pow(d,(q-1)) / 3.141593;
	    %  //smoothing kernel?
		%  //a2 will be modified outside the function (throught the pointer). In Matlab this would look a bit like
		%  //a2 = mu * T + K*sum1 - log(mu./(x1.*y2)); or so I assume (a2 ,ay be a vector)
		%  //remember in c pow(x,y)=x^y, so const1 would be in Matlab
		%  //const1 = K * (p-1) * c^(p-1) * (q-1) * (d^(q-1)/Pi; 
	%  
	%  
		%  for(i = 1; i < *n; i++){
			%  sum2 = 0.0; //reset sum
			%  
			%  for(j=0; j < i; j++){
			%  r2 = (x[i]-x[j])*(x[i]-x[j]) + (y[i]-y[j])*(y[i]-y[j]);
			%  sum2 += pow(t[i]-t[j]+c,(-p)) * exp(a*hm[j])*pow(r2+d,(-q));
			%  }
			%  //loop over all location "before" (in the list) i 
			%  //r2 is just the euclidian distance in space between point i  and j
			%  //second part is in matlab: (hm == mag-minmag)
			%  //sum2 = (sum2+) (t(i)-t(j)+c)^(-p) * exp(a*hm(j)) * (r2+d)^(-q)
	 %  
			%  lam1 = mu / *x1 / *y1 + const1 * sum2;
			%  //I guess this can be a vector in matlab it would be
			%  // lam1 = mu./x1./y1 + const1 * sum2;
			%  //or lam1 = mu./(x1.*y1) + const1*sum2; 
	%  
			%  
			%  if(lam1 < 0.0000000001) *a2 = 999999999999;
			%  else {
			%  *a2 -= log(lam1);
				%  }
			%  //if lam1 is not super-tiny, the log of it will be subtracted from a2 
			%  //to make it clear, matlab it would be: a2 = (a2 -) log(lam1); 
			%  //a2 is used in an optim function (that's
			%  //why the if makes a lot of sense)
	%  
	    %  }
	%  }
	  %  
 */

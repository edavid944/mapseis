function SmoothOperator = smoothGaussBand3D_mk2(ValToSmooth,RelmGrid,minWidth,BandWith,AnySopMode)
	%Smooths a data vector with a gaussian smoothing kernel, with a varying 
	%Bandwith of the kernel
	
	
	disp('----')
	disp(max(BandWith{1}))
	disp(max(BandWith{2}))
	disp(minWidth)
	disp('****')
	
	%maybe more comments about it later.

	%with integration using the error function erf
	
	SmoothOperator=zeros(size(ValToSmooth));
	PosGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	PosGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	PosGrid(:,3)=((RelmGrid(:,8)+RelmGrid(:,7))/2);
	
	if ~AnySopMode
		%prepare Bandwidth
		TheBand=BandWith;
		TheBand(TheBand<minWidth)=minWidth;
	else
		if numel(minWidth)==1
			minWidth(2)=minWidth(1);
		end
		
		TheBandSpat=BandWith{1};
		TheBandSpat(TheBandSpat<minWidth(1))=minWidth(1);
		
		TheBandMag=BandWith{2};
		TheBandMag(TheBandMag<minWidth(2))=minWidth(2);
		
	end
	
	
	%UniMags(mod(i-1,numMags)+1)
	
	CellWithData=find(ValToSmooth~=0);
	disp(numel(CellWithData))
	
	%normalize to the area
	CellArea=abs(RelmGrid(1,2)-RelmGrid(1,1))*abs(RelmGrid(1,4)-RelmGrid(1,3));
	FullArea=numel(PosGrid(:,1))*CellArea;
	GauSum=0;
	%NOT FULLY WORKING HAS TO BE SOMETHING MORE
	
	if ~AnySopMode
		for i=CellWithData'
		
	
			
			%calc distance to all other cells (euclidian for now)
			%DistMat=((PosGrid(:,1)-PosGrid(i,1)).^2+(PosGrid(:,2)-PosGrid(i,2)).^2+(PosGrid(:,3)-PosGrid(i,3)).^2).^(1/2);
				
			%calculate gaussian
			% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
			%GaussVal=(1/(2*pi*TheBand(i)^3))*exp((-DistMat.^2)./(2*TheBand(i)^2));
			
			LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBand(i)*sqrt(2))));
			LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBand(i)*sqrt(2))));
			MagPart = 1/2*(erf((PosGrid(i,3)-RelmGrid(:,7))./(TheBand(i)*sqrt(2))) - erf((PosGrid(i,3)-RelmGrid(:,8))./(TheBand(i)*sqrt(2))));
			
			GaussVal=LonPart.*LatPart.*MagPart;
			
			%add contribution
			SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
			GauSum=GauSum+sum(GaussVal(:));
				
				
		end

	else
		for i=CellWithData'
			%calc distance to all other cells (euclidian for now)
			%DistMatSpat=((PosGrid(:,1)-PosGrid(i,1)).^2+(PosGrid(:,2)-PosGrid(i,2)).^2).^(1/2);
			%DistMatSpat=distance(PosGrid(:,2),PosGrid(:,1),PosGrid(i,2),PosGrid(i,1));
			%DistMatMag=((PosGrid(:,3)-PosGrid(i,3)).^2).^(1/2);	
			
			%calculate gaussian
			% dgauss=1/2d0/PI/w(1)**2*exp(-r**2/2d0/w(1)**2) %from zhuangs ETAS
			%GaussVal=(1/(2*pi*TheBandSpat(i)^2*TheBandMag(i)))*exp(-((DistMatSpat.^2)./(2*TheBandSpat(i)^2)+(DistMatMag.^2)./(2*TheBandMag(i)^2)));
			
			LonPart = 1/2*(erf((PosGrid(i,1)-RelmGrid(:,1))./(TheBandSpat(i)*sqrt(2))) - erf((PosGrid(i,1)-RelmGrid(:,2))./(TheBandSpat(i)*sqrt(2))));
			LatPart = 1/2*(erf((PosGrid(i,2)-RelmGrid(:,3))./(TheBandSpat(i)*sqrt(2))) - erf((PosGrid(i,2)-RelmGrid(:,4))./(TheBandSpat(i)*sqrt(2))));
			MagPart = 1/2*(erf((PosGrid(i,3)-RelmGrid(:,7))./(TheBandMag(i)*sqrt(2))) - erf((PosGrid(i,3)-RelmGrid(:,8))./(TheBandMag(i)*sqrt(2))));
			
			GaussVal=LonPart.*LatPart.*MagPart;
			
			%add contribution
			SmoothOperator=SmoothOperator+(ValToSmooth(i)*GaussVal);
			GauSum=GauSum+sum(GaussVal(:));
				
				
		end
	end
	
	
	
	SmoothOperator=SmoothOperator/sum(SmoothOperator(:))*sum(ValToSmooth(:));
	
	
	

	
	
	
end

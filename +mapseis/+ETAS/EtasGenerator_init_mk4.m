function OutStruct = EtasGenerator_init_mk4(ThetaInput,SpatRefPoint,TopLeftPoint,TimeLength,currentTime,MinMag,ShortCat,MainshockStruct,SortOut,KeepAll)
	%splitted version of the EtasGenerator_mk3 with some additions
	%This part does the initialisation, which has to be done only once per 
	%forecast.	
	
	%only supports "normal" mode
	%		'normal'	us both trigger and distro together	
	%				from the past seismicity as mainshocks
	
	
	import mapseis.ETAS.*;
	
	%org. Comment from SimETAS23
	% Stefan {H}iemer
	% May 2012 UCLA
	% This program is for simulating ETAS data on
	% -- X1, Y1 --> space S = [0,X1] x [0,Y1] (km)
	% -- T      --> time: [0,T]
	% -- m0     --> lower magnitude cutoff m0
	% theta = (mu, b, K, c, p, a, d, q)
	
	% ETAS according to Schoenberg, BSSA, 2012:
	%# rho(x,y) = 1/(X1Y1), and ri = sqrt((x-xi)^2 + (y-yi)^2). 
	%# lambda(t,x,y) = mu rho(x,y) + K (p-1)c^(p-1) (q-1) d^(q-1) / pi
	%# SUM (ti-t+c)^-p exp{a(Mi-M0)}(ri^2 + d)^-q.
	%# For any theta, the integral of lambda over the space time region =
	%# mu T + K SUM exp{a(Mi-M0)}.
	
	%X1=XSpace;Y1=YSpace,T=TSpace


	
	%DE, 2012

	%SortOut 	= true ... outputs them not in chronological order
	%    		= false ... outputs them in chronological order
	%KeepAll 	= true ... you want to keep all points
	%    	 	= false ... you only want to keep all points in the windows TxS
	
	%MainshockStruct: a structure containing everything for the generation of 
	%the mainshock
	%it should contain following fields: 
	%	Distro:		Used in the distro mode, the distribution to sample the mainshocks
	%			from
	%	MinMainMag:	Used in the trigger and distro mode, only earthquakes above this magnitude will 
	%			be used a trigger
	%	TimeBefore:	Used by the trigger mode, only earthquakes between currentTime-TimeBefore
	%			and currentTime will be used as trigger (as to be days), it is also used
	%			for the other modes to set the origin time, normally this is just the same
	%			as the TimeLength (the forecast length)
	%	UseStartTime:	if true the TimeBefore will be interpreted as datenum and giving 
	%			start time of the learning period
	%	EqList:		Only for custom mode, a ShortCat with suitable eq.
	%	Generate1stGen: if true the first generation aftershocks will be generated
	%	BGRates
	%	

	
		
	if isempty(SortOut)
		SortOut = false;
	end
	
	if isempty(KeepAll)
		KeepAll = true;
	end

	OutStruct=[];
	BGRates=[];
	WorkCat=[];
	
	MinShock=MinMag;
	
	%unpack the variables
	mu = ThetaInput.mu;
	b = ThetaInput.b;
	K = ThetaInput.K;
	c = ThetaInput.c;
	p = ThetaInput.p;
	a = ThetaInput.a;
	d = ThetaInput.d;
	q = ThetaInput.q;
	
	%Check wether explosive or not, stop if it is
	br = K*b/(b-a);
	if br>1 | a >=b
		disp('Process is explosive');
		return
	else
		%disp('Process is not explosive');
	
	end
	

	%Now start with mainshock generation (uniform model in this case)
	%NrMainShocks=poissrnd(mu * TSpace);
	%I guess here it should be replaced with real earthquakes
	
	if isfield(MainshockStruct,'UseStartTime')
		UseStartTime=MainshockStruct.UseStartTime;
	else
		UseStartTime=false;
	end
	
	
	%generate region setting
	XSpace=TopLeftPoint(1)-SpatRefPoint(1);
	YSpace=TopLeftPoint(2)-SpatRefPoint(2);
	
	
	%generate/select Mainshocks
	

	%search suitable eq
	if ~UseStartTime
		minTime=currentTime-MainshockStruct.TimeBefore;
		BGTimeStart=MainshockStruct.TimeBefore; 
	else
		minTime=MainshockStruct.TimeBefore;
		BGTimeStart=currentTime-minTime;
	end
			
	maxTime=currentTime;
	originTime=minTime;
			
			
			
	selection=(ShortCat(:,1)>=SpatRefPoint(1) & ShortCat(:,1)<TopLeftPoint(1)) &...
		(ShortCat(:,2)>=SpatRefPoint(2) & ShortCat(:,2)<TopLeftPoint(2)) &...
		(ShortCat(:,4)>=minTime & ShortCat(:,4)<maxTime) &...
		(ShortCat(:,5)>=MainshockStruct.MinMainMag);
	FoundMainShocks=sum(selection);
			
	WorkCat=zeros(FoundMainShocks,7);
	%disp(['Nr. possible Mainshocks: ',num2str(FoundMainShocks)]);
		
	orgMainShock=ShortCat(selection,:);
			
	if ~isempty(orgMainShock)
		%norm them 
		orgMainShock(:,4)=orgMainShock(:,4)-minTime;
		orgMainShock(:,5)=orgMainShock(:,5);
				
		WorkCat(:,1)=orgMainShock(:,1);
		WorkCat(:,2)=orgMainShock(:,2);
		WorkCat(:,3)=orgMainShock(:,5);
		WorkCat(:,4)=orgMainShock(:,4);
		WorkCat(:,7)=1;
		
		
	end
	
	MinShock=MainshockStruct.MinMainMag;
	
	%The Time to stop in this case
	TimeToQuit=currentTime+TimeLength-originTime;
			
	
	
	%now pack for the generator
	OutStruct.FoundMainShocks = FoundMainShocks;
	OutStruct.TimeLength = TimeLength;
	OutStruct.BGTimeStart = BGTimeStart;
	OutStruct.WorkCat = WorkCat;
	OutStruct.MinShock = MinShock;
	OutStruct.TimeToQuit =TimeToQuit;
	OutStruct.XSpace = XSpace;
	OutStruct.YSpace = YSpace;
	OutStruct.TimeLength = TimeLength;
	OutStruct.SpatRefPoint = SpatRefPoint;
	OutStruct.MinMag = MinMag;
	OutStruct.originTime = originTime;
	OutStruct.SortOut = SortOut;
	OutStruct.KeepAll = KeepAll;
	
end

function [EstValues, AddonData] = PolyEtasEstimator(ShortCat,TimeLength,SpatRefPoint,TopLeftPoint,MinMag,ThetaStart)
	%based on the work of Schoenberg, BSSA, 2012
	%estimates the parameter for a ETAS model, the code is lously 
	%based on a R code by Rick Schoenberg (fitmyetas23toHectorby50.s)
	
	%Variables 
	%ShortCat:	Earthquake catalog in the format of MapSeis ShortCat
	%		([Lon Lat Depth Time(Datenum) Mag]), the catalog should
	%		filtered according to the test region.
	%TimeLength:	Length of the forecast in days
	%SpatRefPoint:	The spatial reference point in the test region, normally the
	%		bottom left corner (lon, lat)
	%TopLeftPoint:	The maximal possible coordinate in the region, normally this 
	%		is the top-right corner (lon,lat)
	%MinMag:	Minimum magnitude of the "forecast", this will be subtracted from
	%		the magnitudes to normalize them.s
	%ThetaStart:	Starting parameter for the estimation, it is structure with the fields
	%		mu,K,c,p,a,d,q. If left empty default parameters will be used.
	
	
	%More options will be added.
	
	%Does a step-wise estimation and takes TimeLength big chunks from the data and
	%estimates the starting from the minimum time
	
	
	
	%currently EXPERIMENTAL
	
	
	
	import mapseis.ETAS.*;
	
	%hard-coded parts for now
	CurveMode=false;
	minNumCalc=4;
	
	%to avoid nasty error
	fval=NaN;
	exitflag=NaN;
	output=NaN;
	
	
	if isempty(ThetaStart)
		ThetaStart=struct(	'mu',5.250,...
					'b',1,...
					'K',0.200,...
					'c',0.025,...
					'p',1.500,...
					'a',0.700,...
					'd',0.025,...
					'q',1.500);
		  
	end
	
	%unpack the variables
	mu = ThetaStart.mu;
	
	K = ThetaStart.K;
	c = ThetaStart.c;
	p = ThetaStart.p;
	a = ThetaStart.a;
	d = ThetaStart.d;
	q = ThetaStart.q;
	%maybe needed later to avoid explosive settings
	try
		b = ThetaStart.b; 
	catch
		b=1;
	end
	
		
	%original code (DELETE IT LATER)
	    %  ##### This is for fitting 
	    %  ##### lambda(t,x,y) = mu rho(x,y) + K (p-1)c^(p-1) (q-1) d^(q-1) / pi
	    %  ##### SUM (ti-t+c)^-p exp{a(Mi-M0)}(ri^2 + d)^-q,
	    %  ##### where rho(x,y) = 1/(X1Y1), and ri = sqrt((x-xi)^2 + (y-yi)^2),
	    %  ##### on a space S = [0,X1] x [0,Y1] (km), in time [0,T], and lower
	    %  ##### magnitude cutoff m0.
	    %  ##### The magnitude density is b exp(-b (m-m0)).    
	    %  ##### For any theta, the integral of lambda over the space time region =
	    %  ##### mu T + K SUM exp{a(Mi-M0)}.
	%  
	    %  ##### Fit b, for the magnitude frequency, separately.
	    %  ##### theta = (mu, K, c, p, a, d, q)
	%  
	%  x = read.table("scec2.txt") ## YYY/MM/DD HH mm SS.ss MAG	LAT LON
	%  y0 = x[,1]
	%  y1 = as.Date(y0,"%m/%d/%y")
	%  y2 = julian(y1,origin = as.Date("1999-10-16"))
	%  t = y2 + x[,2]/24 + x[,3]/60/24 + x[,4]/60/60/24
	%  ###DE: converts the dates into days from the first day (1999-10-16), seems to be similar to date num 
	%  ###The t has to be done that way because of the somewhat "weird" format of the input. it is just hour, 
	%  ###minutes and seconds into days.
	%  
	%  m = x[,5] # Magnitude
	%  y = x[,6] # Latitude
	%  x = x[,7] # Longitude
	%  
	%  ###DE: Plotting command not needed
	%  plot(x,y,pch=3,cex=.5,xlab="longitude",ylab="latitude")
	%  
	%  
	%  z1 = list(t=t, hm=m-3, lon=x+117, lat=y-34, n = length(t))
	%  X1 = 1
	%  Y1 = 1
	%  T = 434 ## in days
	%  ###DE:makes a list with normalized data: Time=days from start period, mag= mag above minmag and lon and lat
	%  ### normalize to the point (-117,34) which may be the bottom left corner, but I don't know
	%  
	%  
	
	%Determine how many steps can be done
	minTime=floor(min(ShortCat(:,4)));
	maxTime=ceil(max(ShortCat(:,4)));
	
	TotalTime=maxTime-minTime;
	
	numFullStep=floor(TotalTime/TimeLength);
	
	
	%Prepare data
	%------------
	%remove Eq with M<MinMag (to avoid "negative" magnitudes)
	AboveMag=ShortCat(:,5)>=MinMag;
	ShortCat=ShortCat(AboveMag,:);
	
	%WorkCatalog init
	WorkCat=zeros(size(ShortCat));
	
	%MagNorm
	WorkCat(:,5)=ShortCat(:,5)-MinMag;
	
	%SpaceNorm
	WorkCat(:,3)=ShortCat(:,3);
	
	%add time
	WorkCat(:,4)=ShortCat(:,4);
	
	if CurveMode
		%use distance instead of simple subtraction
		
	else
		WorkCat(:,2)=ShortCat(:,2)-SpatRefPoint(2);
		WorkCat(:,1)=ShortCat(:,1)-SpatRefPoint(1);
		SpaceExtend(1)=TopLeftPoint(1)-SpatRefPoint(1);
		SpaceExtend(2)=TopLeftPoint(2)-SpatRefPoint(2);
	end
	
	%kick out earthquake beyond the testing region
	inRegion=(WorkCat(:,1)>=0&WorkCat(:,1)<=SpaceExtend(1))&...
		(WorkCat(:,2)>=0&WorkCat(:,2)<=SpaceExtend(2));
	WorkCat=WorkCat(inRegion,:);
	
	%start coeff
	ThetaMan=[mu, K, c, p, a, d, q];
	
	
	%some options for the estimator
	EstOptions= struct(	'Display','iter',...
				'MaxIter',3000,...
				'MaxFunEvals',6000,...
				'TolX',1e-5,...
				'TolFun',1e-5);
	
	
	allThetas=nan(numFullStep+1,7);
	disp(size(allThetas));
	numEventsInTime=zeros(numFullStep+1,1);
	
	EstTimer=tic;
	%first the full steps
	%--------------------
	for i=1:numFullStep
		
		curWorkCat=WorkCat;
		
		%current Time
		curminTime=minTime%;+i*TimeLength;
		
		%TimeNorm
		curWorkCat(:,4)=curWorkCat(:,4)-curminTime;
	
		%cut all events not needed
		inTime=curWorkCat(:,4)>=0 & curWorkCat(:,4)<i*TimeLength;
		curWorkCat=curWorkCat(inTime,:);
		
		numEventsInTime(i)=sum(inTime);
		
		
		if numEventsInTime(i)<minNumCalc	
			%not worth to calculate
			continue;
		end
		
		%function handles to some candite function (in the end there should be
		%only one (like with the highlander))
		%generate the handle for the optimization function
		%FuncHandle=@(x) SumToOptim(curWorkCat,i*TimeLength,SpaceExtend,x);
		%FuncHandle=@(x) SumToOptimPar(curWorkCat,i*TimeLength,SpaceExtend,x);
		%FuncHandle=@(x) SumToOptimEXP(curWorkCat,i*TimeLength,SpaceExtend,x);
		%FuncHandle=@(x) SumToOptimPar_b(WorkCat,i*TimeLength,SpaceExtend,x,b);
		FuncHandle=@(x) SumToOptimEXP_b(WorkCat,i*TimeLength,SpaceExtend,x,b);
		
		%now search for that minimum (again different versions possible)
		[ThetaMan,fval,exitflag,output] = fminsearch(FuncHandle,ThetaMan,EstOptions)
		%[ThetaMan,fval,exitflag,output] = fminunc(FuncHandle,ThetaMan);
		%[ThetaMan,fval,exitflag,output] = patternsearch(FuncHandle,ThetaMan)
		%[ThetaMan,fval,exitflag,output] = ga(FuncHandle,numel(ThetaMan));
		
		%save the teta values and the number of earthquakes
		size(allThetas(:,i))
		size(ThetaMan)
		allThetas(i,:)=ThetaMan;
		
		
	end
	
	
	
	%the last unfinished step
	curminTime=minTime+numFullStep*TimeLength;
	
	
	curWorkCat=WorkCat;

	%current Time
	curminTime=minTime%+i*TimeLength;
		
	%TimeNorm
	curWorkCat(:,4)=curWorkCat(:,4)-curminTime;
		
	%cut all events not needed
	inTime=curWorkCat(:,4)>=0&curWorkCat(:,4)<maxTime-minTime;
	curWorkCat=curWorkCat(inTime,:);
		
	numEventsInTime(end)=sum(inTime);
	
	LastTimeLength=maxTime-curminTime;
		
	if numEventsInTime(end)>=minNumCalc	
		%function handles to some candite function (in the end there should be
		%only one (like with the highlander))
		%generate the handle for the optimization function
		%FuncHandle=@(x) SumToOptim(curWorkCat,LastTimeLength,SpaceExtend,x);
		FuncHandle=@(x) SumToOptimPar(curWorkCat,LastTimeLength,SpaceExtend,x);
		%FuncHandle=@(x) SumToOptimEXP(curWorkCat,LastTimeLength,SpaceExtend,x);
		%FuncHandle=@(x) SumToOptimPar_b(WorkCat,LastTimeLength,SpaceExtend,x,b);
		%FuncHandle=@(x) SumToOptimEXP_b(WorkCat,LastTimeLength,SpaceExtend,x,b);
		
		%now search for that minimum (again different versions possible)
		[ThetaMan,fval,exitflag,output] = fminsearch(FuncHandle,ThetaMan,EstOptions)
		%[ThetaMan,fval,exitflag,output] = fminunc(FuncHandle,ThetaMan);
		%[ThetaMan,fval,exitflag,output] = patternsearch(FuncHandle,ThetaMan)
		%[ThetaMan,fval,exitflag,output] = ga(FuncHandle,numel(ThetaMan));
		
		%save the teta values and the number of earthquakes
		allThetas(end,:)=ThetaMan;
	
		
	
	end
		
	%stopt the timer
	EstimationTime=toc(EstTimer);
	
	
	%compile output
	ThetaStruct=ThetaStart;
	ThetaStruct.mu=ThetaMan(1);
	ThetaStruct.K=ThetaMan(2);
	ThetaStruct.c=ThetaMan(3);
	ThetaStruct.p=ThetaMan(4);
	ThetaStruct.a=ThetaMan(5);
	ThetaStruct.d=ThetaMan(6);
	ThetaStruct.q=ThetaMan(7);
	
	EstValues=ThetaStruct;
	AddonData.ThetaStart=ThetaStart;
	AddonData.TimeLength=TimeLength;
	AddonData.AllThetas=allThetas;
	AddonData.NumEqInStep=numEventsInTime;
	AddonData.CalcTime=EstimationTime;
	AddonData.Est_fval=fval;
	AddonData.Est_exitflag=exitflag;
	AddonData.Est_Output=output;
	
	%quick fix:
	save('ThetaStruct.mat','ThetaStruct');
	
	%  system("R CMD SHLIB myss2.c")
		%  dyn.load("myss2.so")
		%  sum3 = function(theta1){
		    %  if(min(min(theta1),theta1[4]-1,theta1[7]-1) < 0.00000001) return(9e20)
		    %  a3 = .C("ss2", as.double(z$lon), as.double(z$lat), as.double(z$t), as.double(z$hm), as.integer(z$n),
			%  as.double(X1), as.double(Y1), as.double(T), as.double(theta1), a2=double(1), onelam=double(1))
		    %  if(p1 > 1){
			%  #cat("\n theta = ", signif(theta1,4))
			%  #cat("\n negloglike is ",a3$a2,". Int = ",theta1[1]*T + theta1[2]*sum(exp(theta[5]*z$hm)),".")
			%  #cat("\n sum of 1/lam is ",a3$onelam,".\n\n") 
		    %  }
		    %  a3$a2  ## or equivalently a[[10]]
		%  }
		%  #DE: Seems to be a function definition, but normally it is MyName <- function(Var){}
		%  #why not here? NEW: R now also accepts the = instead of the <- or ->, so everything is fine 
		%  #The external C code is used in this function
		%  #Remember: "The $ operator retrieves an element in a list, with no need to put its name between quotes, 
		%  #contrary to the [[ operator. The interest of the [[ operator is that is argument can be a variable."
		%  #This means a3$a2 is the returned value of sum3
	%  
		%  p1 = 2
		%  theta = c(5.250, 0.200, 0.025, 1.500, 0.700, 0.025, 1.500) #init values (DE)
		%  z = z1 # input catalog(DE)
		%  sum3(theta)
	%  
	%  theta0 = 2*c(0.0391745359, 0.4564154676, 0.0593339477, 1.3706619217, 1.1385348968,
	%  0.0001030552, 1.6692718270)
	%  ## I'm gonna go 20 days at a time.
	%  j1 = matrix(0,nrow=22,ncol=7)
	%  j2 = matrix(0,nrow=22,ncol=7)    
	%  for(i in 1:22){
	    %  j5 = 20*i
	    %  z = list(t=z1$t[z1$t<j5], hm=z1$hm[z1$t<j5], lon=z1$lon[z1$t<j5], lat=z1$lat[z1$t<j5], n=sum(z1$t<j5))
	    %  cat(i,z$n, b1$par[5], "\n")
	    %  T  = j5
	    %  b1 = optim(theta0,sum3) # ,hessian=T)
	    %  theta0 = b1$par
	    %  j1[i,] = b1$par
	    %  #b2 = b1$hess
	    %  #j2[i,] = sqrt(diag(solve(b2)))
	%  }
	%  
	%  j1[22,]
	%  
	%  theta1 = c(0.0391745359, 0.4564154676, 0.0593339477, 1.3706619217, 1.1385348968,
	%  0.0001030552, 1.6692718270)
	%  
	%  ###That's all? (together with the C code), this is very small.
	%  
	%  
	%  
	%  
	%  
	%  
	%  
	%  
	%  
	%  ###DE: Plotting, not needed for things I do
	%  m = 22
	%  plot(c(0,440),c(0.03,4),type="n",log="y",xlab="days",ylab="parameter estimate")
	%  lines((1:m)*20,j1[,1],col="yellow")
	%  lines((1:m)*20,j1[,2],col="green")
	%  lines((1:m)*20,j1[,3],col="red")
	%  lines((1:m)*20,j1[,4],col="black")
	%  lines((1:m)*20,j1[,5],col="orange")
	%  lines((1:m)*20,j1[,6]*10^3,col="brown")
	%  lines((1:m)*20,j1[,7],col="blue")
	%  # legend("topright",lty=c(1,1,1,1,1,1),col=c("blue","green","red","black","orange","brown","magenta"),
	%  #    c(expression(mu),"K","c","p","a","d","q"))
	%  
	%  m = 22
	%  plot(c(0,440),c(0.5,20),type="n",log="y",xlab="days",ylab="parameter estimate � final estimate")
	%  lines((1:m)*20,j1[,1]/j1[22,1],col="yellow")
	%  lines((1:m)*20,j1[,2]/j1[22,2],col="green")
	%  lines((1:m)*20,j1[,3]/j1[22,3],col="red")
	%  lines((1:m)*20,j1[,4]/j1[22,4],col="black")
	%  lines((1:m)*20,j1[,5]/j1[22,5],col="orange")
	%  lines((1:m)*20,j1[,6]/j1[22,6],col="brown")
	%  lines((1:m)*20,j1[,7]/j1[22,7],col="blue")
	%  #legend("topright",lty=c(1,1,1,1,1,1),col=c("blue","green","red","black","orange","brown","magenta"),
	%  #    c(expression(mu),"K","c","p","a","d","q"))
	%  
	%  ## you could also show 1.96 ses for each of them.
	%  plot(c(0,10*m),c(-200,200),type="n",xlab="time (months)",ylab="% error in parameter estimate")
	%  lines((1:m)*10,(j1[,1]/theta2[1] - 1)*100,col="blue")
	%  lines((1:m)*10,100*(j1[,1]/theta2[1] - 1)+100*1.96*j2[,1]/theta2[1],col="blue",lty=2)
	%  lines((1:m)*10,100*(j1[,1]/theta2[1] - 1)-100*1.96*j2[,1]/theta2[1],col="blue",lty=2)
	%  ###DE: end Plotting
	%  
	%  
	%  ###DE: as said here, comparision with normal method, it could be an option to integrate a switch allowing to use the 
	%  ###old method, but it is definitely not needed and has no priority
	%  
	%  #### For comparison, this below is for estimating the integral numerically. It's slow and crappy.
	%  ## install.packages("cubature")
	%  library(cubature)
	%  
		%  M0 = 0
		%  f = function(x){
		    %  ## calculate lambda
		    %  sum1 = 0
		    %  k = 1
		    %  ind1 = 0
		    %  if(z$t[1] >= x[3]) ind1 = 2
		    %  while(ind1 < 1){
			%  sum1 = sum1 + (x[3]-z$t[k]+c1)^(-p) * 
				   %  exp(a*(z$hm[k]-M0)) * ((z$lon[k]-x[1])^2 + (z$lat[k]-x[2])^2 + d)^(-q)
			%  k = k+1
			%  if(k > z$n) ind1 = 2
			%  else if(z$t[k] >= x[3]) ind1 = 2
		    %  }
		    %  b2 = mu/X1/Y1 + K*(p-1)*c1^(p-1) * (q-1) * d^(q-1) / pi * sum1
		%  # cat("\n",b2," ",x)
		%  b2
		%  }
		%  
		%  sum4 = function(theta1){
		    %  if(min(min(theta1),theta1[4]-1,theta1[7]-1) < 0.00000001) return(9e20)
		    %  mu <<- theta1[1]
		    %  K <<- theta1[2]
		    %  c1 <<- theta1[3]
		    %  p <<- theta1[4]
		    %  a <<- theta1[5]
		    %  d <<- theta1[6]
		    %  q <<- theta1[7]
		    %  z3 = adaptIntegrate(f, lowerLimit=c(0,0,0), upperLimit=c(X1,Y1,T1), maxEval=10000)
		    %  cat("\n Integral is ", z3$integral, ".  ", signif(theta1,4))
		    %  a3 = .C("ss2", as.double(z$lon), as.double(z$lat), as.double(z$t), as.double(z$hm), as.integer(z$n),
			%  as.double(X1), as.double(Y1), as.double(T1), as.double(theta1), a2=double(1))
		    %  a6 = theta1[1]*T1 + theta1[2]*sum(exp(theta[5]*z$hm))
		    %  a7 = z3$integral + a3$a2 - a6
		    %  cat("\n The loglike is ", -a7, "or my way it's ",-a3$a2,".\n")
		    %  a7
		%  }
	%  theta1 = c(0.0391745359, 0.4564154676, 0.0593339477, 1.3706619217, 1.1385348968,
	%  0.0001030552, 1.6692718270)
	%  b2 = optim(theta1, sum4, control=list(maxit=100))
	%  b2$par    ##### This is for fitting 
	    %  ##### lambda(t,x,y) = mu rho(x,y) + K (p-1)c^(p-1) (q-1) d^(q-1) / pi
	    %  ##### SUM (ti-t+c)^-p exp{a(Mi-M0)}(ri^2 + d)^-q,
	    %  ##### where rho(x,y) = 1/(X1Y1), and ri = sqrt((x-xi)^2 + (y-yi)^2),
	    %  ##### on a space S = [0,X1] x [0,Y1] (km), in time [0,T], and lower
	    %  ##### magnitude cutoff m0.
	    %  ##### The magnitude density is b exp(-b (m-m0)).    
	    %  ##### For any theta, the integral of lambda over the space time region =
	    %  ##### mu T + K SUM exp{a(Mi-M0)}.
	%  
	    %  ##### Fit b, for the magnitude frequency, separately.
	    %  ##### theta = (mu, K, c, p, a, d, q)
	%  
	%  x = read.table("scec2.txt") ## YYY/MM/DD HH mm SS.ss MAG	LAT LON
	%  y0 = x[,1]
	%  y1 = as.Date(y0,"%m/%d/%y")
	%  y2 = julian(y1,origin = as.Date("1999-10-16"))
	%  t = y2 + x[,2]/24 + x[,3]/60/24 + x[,4]/60/60/24
	%  ###DE: converts the dates into days from the first day (1999-10-16), seems to be similar to date num 
	%  ###The t has to be done that way because of the somewhat "weird" format of the input. it is just hour, 
	%  ###minutes and seconds into days.
	%  
	%  m = x[,5] # Magnitude
	%  y = x[,6] # Latitude
	%  x = x[,7] # Longitude
	%  
	%  ###DE: Plotting command not needed
	%  plot(x,y,pch=3,cex=.5,xlab="longitude",ylab="latitude")
	%  
	%  
	%  z1 = list(t=t, hm=m-3, lon=x+117, lat=y-34, n = length(t))
	%  X1 = 1
	%  Y1 = 1
	%  T = 434 ## in days
	%  ###DE:makes a list with normalized data: Time=days from start period, mag= mag above minmag and lon and lat
	%  ### normalize to the point (-117,34) which may be the top left corner, but I don't know
	%  
	%  
	%  system("R CMD SHLIB myss2.c")
		%  dyn.load("myss2.so")
		%  sum3 = function(theta1){
		    %  if(min(min(theta1),theta1[4]-1,theta1[7]-1) < 0.00000001) return(9e20)
		    %  a3 = .C("ss2", as.double(z$lon), as.double(z$lat), as.double(z$t), as.double(z$hm), as.integer(z$n),
			%  as.double(X1), as.double(Y1), as.double(T), as.double(theta1), a2=double(1), onelam=double(1))
		    %  if(p1 > 1){
			%  #cat("\n theta = ", signif(theta1,4))
			%  #cat("\n negloglike is ",a3$a2,". Int = ",theta1[1]*T + theta1[2]*sum(exp(theta[5]*z$hm)),".")
			%  #cat("\n sum of 1/lam is ",a3$onelam,".\n\n") 
		    %  }
		    %  a3$a2  ## or equivalently a[[10]]
		%  }
		%  #DE: Seems to be a function definition, but normally it is MyName <- function(Var){}
		%  #why not here? NEW: R now also accepts the = instead of the <- or ->, so everything is fine 
		%  #The external C code is used in this function
		%  #Remember: "The $ operator retrieves an element in a list, with no need to put its name between quotes, 
		%  #contrary to the [[ operator. The interest of the [[ operator is that is argument can be a variable."
		%  #This means a3$a2 is the returned value of sum3
	%  
		%  p1 = 2
		%  theta = c(5.250, 0.200, 0.025, 1.500, 0.700, 0.025, 1.500) #init values (DE)
		%  z = z1 # input catalog(DE)
		%  sum3(theta)
	%  
	%  theta0 = 2*c(0.0391745359, 0.4564154676, 0.0593339477, 1.3706619217, 1.1385348968,
	%  0.0001030552, 1.6692718270)
	%  ## I'm gonna go 20 days at a time.
	%  j1 = matrix(0,nrow=22,ncol=7)
	%  j2 = matrix(0,nrow=22,ncol=7)    
	%  for(i in 1:22){
	    %  j5 = 20*i
	    %  z = list(t=z1$t[z1$t<j5], hm=z1$hm[z1$t<j5], lon=z1$lon[z1$t<j5], lat=z1$lat[z1$t<j5], n=sum(z1$t<j5))
	    %  cat(i,z$n, b1$par[5], "\n")
	    %  T  = j5
	    %  b1 = optim(theta0,sum3) # ,hessian=T)
	    %  theta0 = b1$par
	    %  j1[i,] = b1$par
	    %  #b2 = b1$hess
	    %  #j2[i,] = sqrt(diag(solve(b2)))
	%  }
	%  
	%  j1[22,]
	%  
	%  theta1 = c(0.0391745359, 0.4564154676, 0.0593339477, 1.3706619217, 1.1385348968,
	%  0.0001030552, 1.6692718270)
	%  
	%  ###That's all? (together with the C code), this is very small.

	

end

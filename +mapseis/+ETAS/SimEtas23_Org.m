function OutStruct = SimEtas23_Org(ThetaInput,XSpace,YSpace,TSpace,SortOut,KeepAll)
	%Matlab version of the simetas23 r code done by Stefan Hiemer. It may serve as a starting point for a 
	%future implementation
	
	%org. Comment
	% Stefan {H}iemer
	% May 2012 UCLA
	% This program is for simulating ETAS data on
	% -- X1, Y1 --> space S = [0,X1] x [0,Y1] (km)
	% -- T      --> time: [0,T]
	% -- m0     --> lower magnitude cutoff m0
	% theta = (mu, b, K, c, p, a, d, q)
	
	% ETAS according to Schoenberg, BSSA, 2012:
	%# rho(x,y) = 1/(X1Y1), and ri = sqrt((x-xi)^2 + (y-yi)^2). 
	%# lambda(t,x,y) = mu rho(x,y) + K (p-1)c^(p-1) (q-1) d^(q-1) / pi
	%# SUM (ti-t+c)^-p exp{a(Mi-M0)}(ri^2 + d)^-q.
	%# For any theta, the integral of lambda over the space time region =
	%# mu T + K SUM exp{a(Mi-M0)}.
	
	%X1=XSpace;Y1=YSpace,T=TSpace


	
	%DE, 2012

	%SortOut 	= true ... outputs them not in chronological order
	%    		= false ... outputs them in chronological order
	%KeepAll 	= true ... you want to keep all points
	%    	 	= false ... you only want to keep all points in the windows TxS
	
	if isempty(SortOut)
		SortOut = false;
	end
	
	if isempty(KeepAll)
		KeepAll = true;
	end

	OutStruct=[];

	%unpack the variables
	mu = ThetaInput.mu;
	b = ThetaInput.b;
	K = ThetaInput.K;
	c = ThetaInput.c;
	p = ThetaInput.p;
	a = ThetaInput.a;
	d = ThetaInput.d;
	q = ThetaInput.q;
	
	%Check wether explosive or not, stop if it is
	br = K*b/(b-a);
	if br>1 | a >=b
		disp('Process is explosive');
		return
	else
		disp('Process is not explosive');
	
	end
	

	%Now start with mainshock generation (uniform model in this case)
	NrMainShocks=poissrnd(mu * TSpace);
	%I guess here it should be replaced with real earthquakes
	
	
	if NrMainShocks<0.5
		disp('No Mainshock generated');
		return
	end
	
	%init solution matrix [Lon Lat MagAboveM0 T ID AfterShocks];
	WorkCat=zeros(NrMainShocks,6);
	AfterShockNr=zeros(NrMainShocks,1);

	%generate the actuall mainshocks, uniform in space and time, but with exp. distro in mag 
	WorkCat(:,1)=rand(NrMainShocks,1)*XSpace;
	WorkCat(:,2)=rand(NrMainShocks,1)*YSpace;
	WorkCat(:,3)=random('exp',b,NrMainShocks,1); %exponential distribution beta*e^(-beta*m) % beta = b*log(10)
	WorkCat(:,4)=rand(NrMainShocks,1)*TSpace;
		
	

	%Generate 1st generation aftershocks
	WorkCat(:,6) = poissrnd(K*exp(a*WorkCat(:,3)));
	
	EndOfCat = NrMainShocks; %probably not needed and could be replaced by end
	ToNextSlice = sum(WorkCat(:,6));
	notZero=find(WorkCat(:,6));
	
	%I don't like the loop, but can't think of something better right now.
	SelArray=[];
	for i=1:numel(notZero)
		SelArray=[SelArray;repmat(notZero(i), WorkCat(notZero(i),6),1)];
	end	
	
	CurAftershocks = WorkCat(SelArray,:); %just copy from the mainshocks (will be changed later)
	
	GenerateShocks=true;
	Count=1;
	
	while GenerateShocks
		disp(Count);

		%calc magnitudes of the aftershocks
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,3)=random('exp',b, ToNextSlice,1);
		

		%calc time of aftershocks
		v = rand(ToNextSlice,1); %uniform distribution, number of samples corresponds to number of aftershocks (?)
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,4)=	CurAftershocks(:,4) + c*(1-v).^(1/(1-p)) - c; 		

		
		%calc locations
		v = rand(ToNextSlice,1); 
		dist1 = sqrt(d.*(1-v).^(1/(1-q))-d);
		thet1 = rand(ToNextSlice,1)*2*pi;
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,1)=	cos(thet1).*dist1 + CurAftershocks(:,1);		
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,2)=	sin(thet1).*dist1 + CurAftershocks(:,2);			
		

		%Add new id (generation id)
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,5)=	CurAftershocks(:,5)+1;

		
		%generate the next generation
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,6)=poissrnd(K*exp(a*WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,3)));
		
		ShortedCat=WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,:);
		notZero=find(ShortedCat(:,6));
	
		%I don't like the loop, but can't think of something better right now.
		SelArray=[];
		for i=1:numel(notZero)
			SelArray=[SelArray;repmat(notZero(i), ShortedCat(notZero(i),6),1)];
		end	
	
		CurAftershocks = ShortedCat(SelArray,:); %just copy from the mainshocks (will be changed later)
				
		Temp= EndOfCat + ToNextSlice;
		ToNextSlice = sum(WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,6));
		EndOfCat = Temp; %probably not needed and could be replaced by end
		
		if ToNextSlice<0.5
			GenerateShocks=false;
		end
		
		Count=Count+1;

	end
	

	%generate the output data
	if ~KeepAll
		KeepIt = (WorkCat(:,1)<XSpace & WorkCat(:,1)>0) & (WorkCat(:,2)<YSpace & WorkCat(:,2)>0) & ...
				(WorkCat(:,4)<TSpace);

	else
		KeepIt = true(numel(WorkCat(:,1)),1);

	end
	
	%Cut it
	CuteCat = WorkCat(KeepIt,:);

	%sort if needed;
	if SortOut
		[val idx] = sort(CuteCat(:,4)); 
		OutStruct = CuteCat(idx,:);

	else
		OutStruct = CuteCat;

	end
	

end
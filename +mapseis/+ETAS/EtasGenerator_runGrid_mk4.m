function OutStruct = EtasGenerator_runGrid_mk4(ThetaInput,InStruct,MainshockStruct)
	%splitted version of the EtasGenerator_mk3 with some additions
	%This part does the generate of the synthetic earthquakes 
	
	%A variant of the generator instead of earthquake catalogs it returns  
	%counted earthquakes per gridcell
	%This is the first version, so there might ways to save some time 
	
	%third version, added tracking for background and triggered event generated
	%earthquake. Column 7 in synthetic catalog: 0 = random/Bg event, 1=from "historic"
	%earthquake generate event	
	
	
	
	import mapseis.ETAS.*;
	
	%org. Comment from SimETAS23
	% Stefan {H}iemer
	% May 2012 UCLA
	% This program is for simulating ETAS data on
	% -- X1, Y1 --> space S = [0,X1] x [0,Y1] (km)
	% -- T      --> time: [0,T]
	% -- m0     --> lower magnitude cutoff m0
	% theta = (mu, b, K, c, p, a, d, q)
	
	% ETAS according to Schoenberg, BSSA, 2012:
	%# rho(x,y) = 1/(X1Y1), and ri = sqrt((x-xi)^2 + (y-yi)^2). 
	%# lambda(t,x,y) = mu rho(x,y) + K (p-1)c^(p-1) (q-1) d^(q-1) / pi
	%# SUM (ti-t+c)^-p exp{a(Mi-M0)}(ri^2 + d)^-q.
	%# For any theta, the integral of lambda over the space time region =
	%# mu T + K SUM exp{a(Mi-M0)}.
	
	%X1=XSpace;Y1=YSpace,T=TSpace


	
	%DE, 2012

	%SortOut 	= true ... outputs them not in chronological order
	%    		= false ... outputs them in chronological order
	%KeepAll 	= true ... you want to keep all points
	%    	 	= false ... you only want to keep all points in the windows TxS
	
	%MainshockStruct: a structure containing everything for the generation of 
	%the mainshock
	%it should contain following fields: 
	%	Mode:	Sets the used mode it can be 'random', 'distro',
	%		'trigger' or 'custom. 
	%		'random'	is the classic random uniform generation and 
	%				only makes sense for testing
	%		'distro'	allows to select a background rate model
	%				which will be used to sample a catalog from	
	%		'trigger'	uses the earthquake above a certain magnitude
	%		'normal'	us both trigger and distro together	
	%				from the past seismicity as mainshocks
	%		'custom'	allows to put in some custom mainshocks as 
	%				trigger
	%	
	%	Distro:	Used in the distro mode, the distribution to sample the mainshocks
	%		from
	%	MinMainMag:	Used in the trigger and distro mode, only earthquakes above this magnitude will 
	%			be used a trigger
	%	TimeBefore:	Used by the trigger mode, only earthquakes between currentTime-TimeBefore
	%			and currentTime will be used as trigger (as to be days), it is also used
	%			for the other modes to set the origin time, normally this is just the same
	%			as the TimeLength (the forecast length)
	%	UseStartTime:	if true the TimeBefore will be interpreted as datenum and giving 
	%			start time of the learning period
	%	EqList:		Only for custom mode, a ShortCat with suitable eq.
	%	Generate1stGen: if true the first generation aftershocks will be generated
	%	BGRates
	
	

	OutStruct=[];
	BGRates=[];
	
	
	%unpack the variables
	mu = ThetaInput.mu;
	b = ThetaInput.b;
	K = ThetaInput.K;
	c = ThetaInput.c;
	p = ThetaInput.p;
	a = ThetaInput.a;
	d = ThetaInput.d;
	q = ThetaInput.q;
	
	
	%unpack
	FoundMainShocks = InStruct.FoundMainShocks;
	TimeLength = InStruct.TimeLength;
	BGTimeStart = InStruct.BGTimeStart;
	WorkCat = InStruct.WorkCat;
	MinShock = InStruct.MinShock;
	TimeToQuit = InStruct.TimeToQuit;
	XSpace = InStruct.XSpace;
	YSpace = InStruct.YSpace;
	TimeLength = InStruct.TimeLength;
	SpatRefPoint = InStruct.SpatRefPoint;
	MinMag = InStruct.MinMag;
	originTime = InStruct.originTime;
	SortOut = InStruct.SortOut;
	KeepAll = InStruct.KeepAll;
	

	%generate BG events
	TheCat=poissrnd(MainshockStruct.Distro(:,9));
	NrMainShocks=sum(TheCat);
			
	if isempty(WorkCat)
		WorkCat=zeros(NrMainShocks,7);
	end
		
	EqInGrid=TheCat>0;
	NrEq=TheCat(EqInGrid);
	RelmPart=MainshockStruct.Distro(EqInGrid,1:8);
	TheGrid=MainshockStruct.Distro(:,1:8);
			
	%now generate some eq
	for i=1:numel(NrEq)
		for j=1:NrEq(1)
			WorkCat(end+1,1)=RelmPart(i,1)+rand(1,1)*(RelmPart(i,2)-RelmPart(i,1));
			WorkCat(end,2)=RelmPart(i,3)+rand(1,1)*(RelmPart(i,4)-RelmPart(i,3));
			WorkCat(end,3)=RelmPart(i,7)+rand(1,1)*(RelmPart(i,8)-RelmPart(i,7));
			WorkCat(end,4)=BGTimeStart+rand(1,1)*TimeLength;
			WorkCat(end,7)=0;
		end
	end
			
	%modify locations
	WorkCat(:,1)=WorkCat(:,1)-SpatRefPoint(1);
	WorkCat(:,2)=WorkCat(:,2)-SpatRefPoint(2);
	WorkCat(:,3)=WorkCat(:,3)-MinMag;
	
	NrMainShocks=numel(WorkCat(:,1));
	
	
	
	if NrMainShocks<0.5
		%disp('No Mainshock existing');
		return 
	end
	

	%Generate 1st generation aftershocks
	if MainshockStruct.Generate1stGen
		WorkCat(:,6) = poissrnd(K*exp(a*WorkCat(:,3)));
		notMain = WorkCat(:,3)<MinShock;
		WorkCat(notMain,6)=0;
		
	end
	
	EndOfCat = NrMainShocks; %probably not needed and could be replaced by end
	ToNextSlice = sum(WorkCat(:,6));
	notZero=find(WorkCat(:,6));
	
	%I don't like the loop, but can't think of something better right now.
	SelArray=[];
	for i=1:numel(notZero)
		SelArray=[SelArray;repmat(notZero(i), WorkCat(notZero(i),6),1)];
	end	
	
	CurAftershocks = WorkCat(SelArray,:); %just copy from the mainshocks (will be changed later)
	
	GenerateShocks=true;
	Count=1;
	
	%define the end
	ActiveShocks=WorkCat(:,4)<=TimeToQuit;
	%disp(sum(ActiveShocks));
	
	while GenerateShocks
		%disp(Count);

		%calc magnitudes of the aftershocks
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,3)=random('exp',b, ToNextSlice,1);%-MinMag;
		

		%calc time of aftershocks
		v = rand(ToNextSlice,1); %uniform distribution, number of samples corresponds to number of aftershocks (?)
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,4)=	CurAftershocks(:,4) + c*(1-v).^(1/(1-p)) - c; 		
		%added the missing brackets in (1/(1-p))
		
		%calc locations
		v = rand(ToNextSlice,1); 
		dist1 = sqrt(d.*(1-v).^(1/(1-q))-d);
		thet1 = rand(ToNextSlice,1)*2*pi;
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,1)=	cos(thet1).*dist1 + CurAftershocks(:,1);		
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,2)=	sin(thet1).*dist1 + CurAftershocks(:,2);			
		
		
		

		%Add new id (generation id)
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,5)=	CurAftershocks(:,5)+1;

		
		%generate the next generation
		WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,6)=poissrnd(K*exp(a*WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,3)));
		
		%"erase" all events which are to far away for the time interval
		ActiveShocks=WorkCat(:,4)<=TimeToQuit;
		Selector=zeros(size(WorkCat(:,1)));
		Selector(EndOfCat+1:EndOfCat+ToNextSlice)=true;
		WorkCat(~ActiveShocks&Selector,6)=0;
		
		%also kickout everything below MinShock and above 10 which is a bit unrealistic
		notMain = WorkCat(:,3)<MinShock&WorkCat(:,3)>10;
		WorkCat(notMain&Selector,6)=0;
		
		ShortedCat=WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,:);
		notZero=find(ShortedCat(:,6));
	
		%I don't like the loop, but can't think of something better right now.
		SelArray=[];
		for i=1:numel(notZero)
			SelArray=[SelArray;repmat(notZero(i), ShortedCat(notZero(i),6),1)];
		end	
	
		
		
		CurAftershocks = ShortedCat(SelArray,:); %just copy from the mainshocks (will be changed later)
		
		
				
		
		Temp= EndOfCat + ToNextSlice;
		ToNextSlice = sum(WorkCat(EndOfCat+1:EndOfCat+ToNextSlice,6));
		EndOfCat = Temp; %probably not needed and could be replaced by end
		
		
		
		
		if ToNextSlice<0.5
			GenerateShocks=false;
		end
		
		Count=Count+1;

	end
	

	%generate the output data
	
	KeepIt = (WorkCat(:,1)<XSpace & WorkCat(:,1)>0) & (WorkCat(:,2)<YSpace & WorkCat(:,2)>0) & ...
				(WorkCat(:,4)<TimeToQuit) & (WorkCat(:,4)>=(TimeToQuit-TimeLength));
	%Cut it
	CuteCat = WorkCat(KeepIt,:);

	
	%get catalog into region
	CuteCat(:,1)=CuteCat(:,1)+SpatRefPoint(1);
	CuteCat(:,2)=CuteCat(:,2)+SpatRefPoint(2);
	CuteCat(:,3)=CuteCat(:,3)+MinMag;
	CuteCat(:,4)=CuteCat(:,4)+originTime;
	
	%kick at least out what is impossible
	%Keeper=CuteCat(:,1)>=-180&CuteCat(:,1)<=180&CuteCat(:,2)>=-90&CuteCat(:,2)<=90;
	%CuteCat=CuteCat(Keeper,:);
	%not impossible with km data
	
	
	
	NewShortCat(:,1)=CuteCat(:,1);
	NewShortCat(:,2)=CuteCat(:,2);
	NewShortCat(:,3)=0;
	NewShortCat(:,4)=CuteCat(:,4);
	NewShortCat(:,5)=CuteCat(:,3);
	
	%now grid it all (eq mode, everything else does not make sense)
	
	EqCount=zeros(size(TheGrid(:,1)));
	
	
	
	for i=1:numel(NewShortCat(:,1))
		ThePos= NewShortCat(i,1)>=TheGrid(:,1)&NewShortCat(i,1)<TheGrid(:,2)&...
			NewShortCat(i,2)>=TheGrid(:,3)&NewShortCat(i,2)<TheGrid(:,4)&...
			NewShortCat(i,5)>=TheGrid(:,7)&NewShortCat(i,5)<TheGrid(:,8);
			
		if any(ThePos)
			EqCount(ThePos)=EqCount(ThePos)+1;
		end				
			
	end
	
	
	
	
	OriginMarker(:,1)=CuteCat(:,7);
	NrBG_Eq=sum(OriginMarker==0);	
	NrTrig_Eq=sum(OriginMarker==1);
	
	OutStruct=struct(	'EqCount',EqCount,...
				'BGRates',BGRates,...
				'OriginMarker',OriginMarker,...
				'Nr_BG',NrBG_Eq,...
				'Nr_Trig',NrTrig_Eq);
	

end

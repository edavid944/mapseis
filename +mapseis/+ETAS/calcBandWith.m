function BandWith = calcBandWith(NrBuddys,ShortCat,RelmGrid,MagSearch,DepthSearch)
	%searches the k nearest neighbours for each grid cell in the catalog, and 
	%calculates the distance to them which will be used as bandwith for gaussian
	%smoothing kernels later.
	
	%prepare grid
	SearchGrid(:,1)=(RelmGrid(:,1)+RelmGrid(:,2))/2;
	SearchGrid(:,2)=(RelmGrid(:,3)+RelmGrid(:,4))/2;
	
	Data2Search(:,1)=ShortCat(:,1);	
	Data2Search(:,2)=ShortCat(:,2);
	
	ne=3;
	
	if DepthSearch
		SearchGrid(:,ne)=(RelmGrid(:,5)+RelmGrid(:,6))/2;
		Data2Search(:,ne)=ShortCat(:,3);
		ne=4;
	end
	
	if MagSearch
		
		SearchGrid(:,ne)=(RelmGrid(:,7)+RelmGrid(:,8))/2;
		Data2Search(:,ne)=ShortCat(:,5);
				
		
	end
	
	%Now search the nearest neighbours
	[n,d] = knnsearch(Data2Search,SearchGrid,'k',NrBuddys);
	BandWith=max(d,[],2);

end

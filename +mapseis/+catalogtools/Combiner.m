function datastore = Combiner(folderpath)
	%This function scans the specified folder, and merges all the found
	%datastores together, it takes the Routing, FieldInfo and Comment from 
	%the first file
	
	import mapseis.util.importfilter.*;
	import mapseis.catalogtools.catalogmerger;
	import mapseis.datastore.*;
	
	%Scan the Folder
	%---------------
	
	%get all necessary reg expressionpatterns
	rp = regexpPatternCatalog();
	
	%where are I now
	curdir=pwd;
	
	cd(folderpath);
				
				
				
	if ispc
		[sta res] = dos(['dir /b *.mat']);
	else
		[sta res] = unix(['ls -1 *.mat']);
	end
				
	thelines = regexp(res,rp.line,'match');
	
	%get back to original dir
	cd(curdir);
	
	%get info from the first file
	[pathStr,filename,extStr] = fileparts(thelines{1});
	thedata=load([folderpath,filename]);
	
	try 
		currDatastore=thedata.dataStore;
	catch
		error('first file is no mapseis file')
	end	

	%get Routing and other stuff
	try
		RoutingTable=currDatastore.getRouting;
	catch
		RoutingTable=[];
	end
	
	
	try
		FieldInfo=currDatastore.getUserData('FieldInfo');
	catch
		FieldInfo=[];
	end

	
	try 	
		CatalogInfo=currDatastore.getUserData('CatalogInfo');
	catch
		CatalogInfo=[];
	end	
			
	
	eventStruct=currDatastore.getRawFields;
	
	
	%now loop over all lines and add them to the eventStruct
	for i=2:numel(thelines)
		[pathStr,filename,extStr] = fileparts(thelines{i});
		thedata=load([folderpath,filename]);
		try
			currDatastore=thedata.dataStore;
			newStruct=currDatastore.getRawFields;
			
			%merge
			eventStruct=catalogmerger(eventStruct,newStruct);
				
			%just to be save
			newStruct=[];
		end	
		
		
	end	
	
	
	%create datastore
	datastore=DataStore(eventStruct);
	
	%Reapply Routing and so on
	if ~isempty(RoutingTable)
		datastore.setRouting(RoutingTable);
	end	
	
	setDefaultUserData(datastore);
	datastore.bufferDate;
	datastore.SetPlotQuality;
	
	datastore.setUserData('filepath',folderpath);
	datastore.setUserData('Name', ['Merge ',folderpath]);
	
	if ~isempty(FieldInfo)
		datastore.setUserData('FieldInfo',FieldInfo);
	end
	
	if ~isempty(CatalogInfo)
		datastore.setUserData('CatalogInfo',CatalogInfo);
	end
	
end

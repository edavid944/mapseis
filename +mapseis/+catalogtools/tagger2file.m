function tagger2file(filename,totag,work)
%Sets the inputed events to 'selected' and adds the duplicates and comments if defined
%input: cellarray with: {org_Id, catalogname, duplicate_ID, duplicate_catalogname, Comment}
%the last three parameters can be empty.
%does the same as tagger but write the list to a file instead

	%open file
	try ls(filename);
		fid = fopen(filename,'a'); 
	catch	
		%write header
		fid = fopen(filename,'a'); 
		TheLine= ['selected?;org_ID;catname;ID_duplicate;catname_duplicate;comment;'];
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
	end

	
	
	%loop over all data and find the wanted event
	for i=1:numel(totag(:,1))

		switch work
			case 'set'
			TheLine= ['selected;',totag{i,1},';',totag{i,2},';',totag{i,3},';',totag{i,4},';',totag{i,5}];
				
			case 'unset'
			TheLine= ['notselected;',totag{i,1},';',totag{i,2},';',totag{i,3},';',totag{i,4},';',totag{i,5}];
		end
		
		%write it to the last line 
		%open file
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
	end
	fclose(fid); 
end
function HandCorrector(datastore,ToCorrect)
%This  programm changes the ids by the way specified in ToCorrect
%A line in ToCorrect consists of:
% org_ID, catname, new_group_id, new_appraisal, comment, CorrectionTyp
%CorrectionTyp can either be 'all' or 'single':
%'all' corrects all entries with the same groupID as the specified entry
%'single' only corrects the specified entry.


	try	
		group = datastore.getUserData('group');
		appraisal = datastore.getUserData('appraisal');
		comments = datastore.getUserData('comments');
		LastID = datastore.getUserData('LastID');
	catch
		disp('No Tags there')
	end
	
	eventStruct=datastore.getFields;
	
	PosVec=1:1:numel(eventStruct.org_ID);
	
	for i=1:numel(ToCorrect(:,1))
		
		%all group entries to correct
		if strcmp(ToCorrect{i,6},'all')
			SingleEntry=strcmp(eventStruct.org_ID,ToCorrect{i,1}) & strcmp(eventStruct.catname,ToCorrect{i,2});
			
			old_group=group{SingleEntry};
			AllEntries=strcmp(group,old_group);
			
			if ~isempty(ToCorrect{i,3})
				group(AllEntries)=ToCorrect(i,3);
			else
				group(AllEntries) = {num2str(LastID)};
				LastID=LastID+1;
			end

			
			appraisal(AllEntries)=ToCorrect(i,4);
			
			%different comments means different entries
			EntryPos=PosVec(AllEntries);
			for j=1:numel(group(AllEntries))
				comments(EntryPos(j))={[comments{EntryPos(j)},'/',ToCorrect{i,5}]};
			end
			
		
		%only the entry has to be corrected	
		elseif strcmp(ToCorrect{i,6},'single') 
			SingleEntry=strcmp(eventStruct.org_ID,ToCorrect{i,1}) & strcmp(eventStruct.catname,ToCorrect{i,2}); 
			
			if ~isempty(ToCorrect{i,3})
				group(SingleEntry) = ToCorrect(i,3);
			else
				group(SingleEntry) = {num2str(LastID)};
				LastID=LastID+1;
			end
				
			appraisal(SingleEntry)=ToCorrect(i,4);
			
			tempCom=comments(SingleEntry);
			if ~isempty(tempCom)
				comments(SingleEntry) = {[tempCom{1},'/',ToCorrect{i,5}]};
			else
				comments(SingleEntry) = {ToCorrect{i,5}};
			end	
		end
	
	end
	
	
	%write data to datastore
	datastore.setUserData('group',group);
	datastore.setUserData('appraisal',appraisal);
	datastore.setUserData('comments',comments);
	datastore.setUserData('LastID',LastID);

	
end
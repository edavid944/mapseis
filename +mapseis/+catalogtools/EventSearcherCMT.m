function [logicIndex notFound] = EventSearcherCMT(eventStruct,NameList)
	%simple programme searches all events which matches the entries in the
	%Namelist 
	
	%works at the moment only with the CMT ndk catalog may add a more
	%general version later 

	logicIndex=false(numel(eventStruct.CMT_event_name),1);
	notFound={};
	
	if ~iscell(NameList)
		NameList={NameList};
	end
	
	for i=1:numel(NameList)
		currentNameFounds=strcmp(NameList{i},eventStruct.CMT_event_name);
		
		if any(currentNameFounds)
			logicIndex=logicIndex|currentNameFounds;
		else
			notFound={notFound{:},NameList{i}};
		end
		
	
	end

end

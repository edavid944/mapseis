function res_struct = Calc_dmag_time(in_res_struct,catalog_A,catalog_B)
% This function adds the Magnitude difference over time to a pervious calulated "catalog_duplicates.m" 
% Result
% Input: 
% in_res_struct: Result from the catalog_duplicates
% catalog_A: Datastore from catalog A (may add support for simple data later)
% catalog_B: Datastore from catalog B
%
% Output:
% res_struct: the pervious results from in_res_struct and the new results
% res_struct.deltaMagTime: matrix with [time mean(deltaMag) var(deltaMag) length(deltaMag)]

import mapseis.projector.*;



%calculate the datenum for 0.1*year
minfactor = 365*0.1;

%A year is here 365 day, datenum does correct for leap year, but it will be difficult to add
%to the for loop


%get times and magnitudes identical
time_A = getDateNum(catalog_A,in_res_struct.identicalA);
time_B = getDateNum(catalog_B,in_res_struct.identicalB);

mags_A = getMagnitudes(catalog_A,in_res_struct.identicalA);
mags_B = getMagnitudes(catalog_B,in_res_struct.identicalB);

tmin = floor(min([time_A(:) ; time_B(:)])) ;   
tmax = ceil(max([time_A(:) ; time_B(:)])) ;
%try correct for the error when one Catalog has a much smaller timespace
%tmin = floor(max([min(time_A(:));min(time_B(:))]));
%tmax = ceil(min([max(time_A(:));max(time_B(:))]));
%tmax = max(time_B)

   dmt = [];
   % for t = tmin:2:tmax-3
   for t =tmin:minfactor:tmax
      l = time_B >= t & time_B < t+3*365 ;
      try
       	dm = mags_B(l) - mags_A(l);
      	%dmt = [ dmt ; t mean(dm) var(dm) length(dm) ];
	  catch	      
      	dm = NaN;
      end		
      dmt = [ dmt ; t mean(dm) var(dm) length(dm) ];
   	  
   end

%some fitting needed for the plot
%[p,s] = polyfit(mags_B,mags_A,1);
%f = polyval(p,(0:0.1:7));
%r = corrcoef(mags_A,mags_B); 

   
%writing output
res_struct = in_res_struct;
res_struct.deltaMagTime = dmt;
%res_struct.poly_p = p;
%res_struct.poly_s = s;
%res_struct.poly_f = f;	
%res_struct.corr_r = r;



end
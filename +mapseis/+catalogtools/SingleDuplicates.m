function logVec = SingleDuplicates(datastore)
	%this function checks every duplicate for it's primary. It is done by the 
	%"safe approach", checking every duplicate for a primary
	
	try	
		group = datastore.getUserData('group');
		appraisal = datastore.getUserData('appraisal');
		comments = datastore.getUserData('comments');
		LastID = datastore.getUserData('LastID');
	catch
		disp('No Tags there')
	end
	
	%get duplicates and build position vector
	dupli=strcmp(appraisal,'duplicate');
	logVec=dupli;
	PosVec=1:1:numel(appraisal);
	
	%groups and position 
	dupliGroups=group(dupli); 
	dupliPos=PosVec(dupli);
	
	for i=1:numel(dupliGroups)
		OptimusPrime = strcmp(group,dupliGroups{i}) & strcmp(appraisal,'primary');
		
		if numel(appraisal(OptimusPrime))>0
			logVec(dupliPos(i))=false;
		else	
			%just to be sure
			logVec(dupliPos(i))=true;
		end
	end

end
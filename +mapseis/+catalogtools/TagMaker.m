function Tags = TagMaker(dataA,CountryA,dataB,CountryB, CalcRes)
	%creates tags for the group tagger with the output of compare catalogs
	% Rules: if the identical event is in one of the country the network is situated at, select the one in country
	% 		 If outside both countries select the one which has a smaller distance to the home network.
	
	if ~strcmp(CountryA,'Switzerland') & ~strcmp(CountryB,'Switzerland')
	
	dataAcountry = dataA.in_country(CalcRes.id(:,2));
    dataBcountry = dataB.in_country(CalcRes.id(:,1));
	dataAID = dataA.org_ID(CalcRes.id(:,2));
    dataBID = dataB.org_ID(CalcRes.id(:,1));
	dataAcats = dataA.catname(CalcRes.id(:,2));
    dataBcats = dataB.catname(CalcRes.id(:,1));

	
	
	%find identical events in country A  in data A set them to primary
	inDatACA = strcmp(dataAcountry,CountryA);
	inDatBCA = strcmp(dataBcountry,CountryA);
	%the country has also to be identical
	equality=strcmp(dataAcountry,dataBcountry);
	
	
	if ~isempty(dataAID(inDatACA&equality))
	
		NewTag=[dataAID(inDatACA&equality), dataAcats(inDatACA&equality), dataBID(inDatBCA&equality), dataBcats(inDatBCA&equality),...
				 repmat({'primary'},numel(dataAID(inDatACA&equality)),1),repmat({['Found also in catalogs from ',CountryB]},numel(dataAID(inDatACA&equality)),1)]; 
		Tags=NewTag;	 

		%Now the opposite selection
		NewTag=[dataBID(inDatBCA&equality), dataBcats(inDatBCA&equality),dataAID(inDatACA&equality), dataAcats(inDatACA&equality),...
				 repmat({'duplicate'},numel(dataAID(inDatACA&equality)),1),...
				 repmat({['Found also in catalogs from ',CountryA,' thus not selected']},numel(dataAID(inDatACA&equality)),1)]; 
		Tags=[Tags;NewTag];
		
	end
	
	% find identical events in country B in data B and set to primary
	inDatACB = strcmp(dataAcountry,CountryB);
	inDatBCB = strcmp(dataBcountry,CountryB);
	
	if ~isempty(dataBID(inDatBCB&equality))
	
		NewTag=[dataBID(inDatBCB&equality), dataBcats(inDatBCB&equality), dataAID(inDatACB&equality), dataAcats(inDatACB&equality),...
				 repmat({'primary'},numel(dataBID(inDatBCB&equality)),1),repmat({['Found also in catalogs from ',CountryA]},numel(dataBID(inDatBCB&equality)),1)]; 
		Tags=[Tags;NewTag];	 

		%Now the opposite selection
		NewTag=[dataAID(inDatACB&equality), dataAcats(inDatACB&equality),dataBID(inDatBCB&equality), dataBcats(inDatBCB&equality),...
				 repmat({'duplicate'},numel(dataBID(inDatBCB&equality)),1),...
				 repmat({['Found also in catalogs from ',CountryB,' thus not selected']},numel(dataBID(inDatBCB&equality)),1)]; 
		Tags=[Tags;NewTag];
		
	end
	
	%The most difficult ones: not found in both countries	
	inDatA_NoCo= ~strcmp(dataAcountry,CountryA) & ~strcmp(dataAcountry,CountryB);
	inDatB_NoCo= ~strcmp(dataBcountry,CountryA) & ~strcmp(dataBcountry,CountryB);


	%get distances
	switch CountryA
		case 'Austria'
			dataADist = dataA.A_NEAR_DIST(CalcRes.id(:,2));
			dataAPriority = repmat(1,numel(dataADist),1);
		
		case 'France'
			dataADist = dataA.F_NEAR_DIST(CalcRes.id(:,2));
			dataAPriority = repmat(3,numel(dataADist),1);
		
		case 'Germany'
			dataADist = dataA.D_NEAR_DIST(CalcRes.id(:,2));
			dataAPriority = repmat(4,numel(dataADist),1);
			dataAPriority(dataA.year(CalcRes.id(:,2))<1975) = 2;
		
		case 'Italy'
			dataADist = dataA.I_NEAR_DIST(CalcRes.id(:,2));
			dataAPriority = repmat(2,numel(dataADist),1);
			dataAPriority(dataA.year(CalcRes.id(:,2))<1975) = 4;

		end	
	
	switch CountryB
		case 'Austria'
			dataBDist = dataB.A_NEAR_DIST(CalcRes.id(:,1));
			dataBPriority = repmat(1,numel(dataBDist),1);
		
		case 'France'
			dataBDist = dataB.F_NEAR_DIST(CalcRes.id(:,1));
			dataBPriority = repmat(3,numel(dataBDist),1);
			
		case 'Germany'
			dataBDist = dataB.D_NEAR_DIST(CalcRes.id(:,1));
			dataBPriority = repmat(4,numel(dataBDist),1);
			dataBPriority(dataB.year(CalcRes.id(:,1))<1975) = 2;

		case 'Italy'
			dataBDist = dataB.I_NEAR_DIST(CalcRes.id(:,1));
			dataBPriority = repmat(2,numel(dataBDist),1);
			dataBPriority(dataB.year(CalcRes.id(:,1))<1975) = 4;

		end	



	if ~isempty(inDatA_NoCo)
		

	
		%ADist = dataADist(inDatA_NoCo);
		%BDist = dataBDist(inDatB_NoCo);
		
		%NearerA = dataADist >= dataBDist & inDatA_NoCo;
		%NearerB = dataADist < dataBDist & inDatB_NoCo;
		
		NearerA = dataAPriority > dataBPriority & inDatA_NoCo;
		NearerB = dataAPriority < dataBPriority & inDatB_NoCo;

		
		
		%the one nearer in country A 
		NewTag=[dataAID(NearerA), dataAcats(NearerA), dataBID(NearerA), dataBcats(NearerA),...
				 repmat({'primary'},numel(dataAID(NearerA)),1),...
				 repmat({['Catalogs of ',CountryA, ' has a higher than the one of ',CountryB]},numel(dataAID(NearerA)),1)]; 
		Tags=[Tags;NewTag];	 

		%Now the opposite selection
		NewTag=[dataBID(NearerA), dataBcats(NearerA),dataAID(NearerA), dataAcats(NearerA),...
				 repmat({'duplicate'},numel(dataAID(NearerA)),1),...
				 repmat({['Catalogs of ',CountryA, ' has a higher than the one of ',CountryB]},numel(dataAID(NearerA)),1)]; 
		Tags=[Tags;NewTag];
	
		
		%Nearer to Country B	
		NewTag=[dataBID(NearerB), dataBcats(NearerB), dataAID(NearerB), dataAcats(NearerB),...
				 repmat({'primary'},numel(dataBID(NearerB)),1),...
				 repmat({['Catalogs of ',CountryB, ' has a higher than the one of ',CountryA]},numel(dataBID(NearerB)),1)]; 		
		Tags=[Tags;NewTag];	 

		%Now the opposite selection
		NewTag=[dataAID(NearerB), dataAcats(NearerB),dataBID(NearerB), dataBcats(NearerB),...
				 repmat({'duplicate'},numel(dataBID(NearerB)),1),...
				 repmat({['Catalogs of ',CountryB, ' has a higher than the one of ',CountryA]},numel(dataBID(NearerB)),1)]; 
		Tags=[Tags;NewTag];

	
	
	end
	
	%Cases were the two entry is are not in the same Country 
	if ~isempty(dataAID(equality))
	 	%NotEqA = abs(dataADist) >= abs(dataBDist) & ~equality;
		%NotEqB = abs(dataADist) < abs(dataBDist) & ~equality;
		
		NotEqA = dataAPriority > dataBPriority & ~equality;
		NotEqB = dataAPriority < dataBPriority & ~equality;
		
		%the one nearer in country A 
		NewTag=[dataAID(NotEqA), dataAcats(NotEqA), dataBID(NotEqA), dataBcats(NotEqA),...
				 repmat({'primary'},numel(dataAID(NotEqA)),1),...
				 repmat({['The duplicate is at the other side of the border, but ', ...
				 'Catalogs of ',CountryA, ' has a higher than the one of ',CountryB]},numel(dataAID(NotEqA)),1)]; 
		Tags=[Tags;NewTag];	 

		%Now the opposite selection
		NewTag=[dataBID(NotEqA), dataBcats(NotEqA),dataAID(NotEqA), dataAcats(NotEqA),...
				 repmat({'duplicate'},numel(dataAID(NotEqA)),1),...
				 repmat({['Catalog of ',CountryA,' have a higher priority, thus not selected (not equal in_country)']},numel(dataAID(NotEqA)),1)]; 
		Tags=[Tags;NewTag];
	
		
		%Nearer to Country B	
		NewTag=[dataBID(NotEqB), dataBcats(NotEqB), dataAID(NotEqB), dataAcats(NotEqB),...
				 repmat({'primary'},numel(dataBID(NotEqB)),1),...
				 repmat({['The duplicate is at the other side of the border, but ', ...
				 'Catalogs of ',CountryB, ' has a higher than the one of ',CountryA]},numel(dataBID(NotEqB)),1)]; 		
		Tags=[Tags;NewTag];	 

		%Now the opposite selection
		NewTag=[dataAID(NotEqB), dataAcats(NotEqB),dataBID(NotEqB), dataBcats(NotEqB),...
				 repmat({'duplicate'},numel(dataBID(NotEqB)),1),...
				 repmat({['Catalog of ',CountryB,' have a higher priority, thus not selected (not equal in_country)']},numel(dataBID(NotEqB)),1)]; 
		Tags=[Tags;NewTag];
	end
	
	
	
	else
		%with Switzerland
		dataAcountry = dataA.in_country(CalcRes.id(:,2));
	    dataBcountry = dataB.in_country(CalcRes.id(:,1));
		dataAID = dataA.org_ID(CalcRes.id(:,2));
	    dataBID = dataB.org_ID(CalcRes.id(:,1));
		dataAcats = dataA.catname(CalcRes.id(:,2));
	    dataBcats = dataB.catname(CalcRes.id(:,1));
		dataAlon = dataA.lon(CalcRes.id(:,2));
	    dataBlon = dataB.lon(CalcRes.id(:,1));
		dataAlat = dataA.lat(CalcRes.id(:,2));
	    dataBlat = dataB.lat(CalcRes.id(:,1));
		
		
		%find identical events in country A  in data A set them to primary
		if strcmp(CountryA,'Switzerland')
			inDatACA = strcmp(dataAcountry,CountryA) | strcmp(dataAcountry,'Liechtenstein');
			inDatBCA = strcmp(dataBcountry,CountryA) | strcmp(dataBcountry,'Liechtenstein');
		else 
			inDatACA = strcmp(dataAcountry,CountryA);
			inDatBCA = strcmp(dataBcountry,CountryA);
		end
		
		
		%the country has also to be identical
		equality=strcmp(dataAcountry,dataBcountry);
		
		
		if strcmp(CountryA,'Austria')	
		 	regSel=(dataAlon>10.5);
		elseif strcmp(CountryA,'Italy')
			regSel=(dataAlat<45.45);
		else
			regSel=logical(ones(numel(dataAlat),1))
		end
		
			if ~isempty(dataAID(inDatACA&equality&regSel))
			
				NewTag=[dataAID(inDatACA&equality&regSel), dataAcats(inDatACA&equality&regSel),...
				 dataBID(inDatBCA&equality&regSel), dataBcats(inDatBCA&equality&regSel),...
						 repmat({'primary'},numel(dataAID(inDatACA&equality&regSel)),1),...
						 repmat({['Found also in catalogs from ',CountryB]},numel(dataAID(inDatACA&equality&regSel)),1)]; 
				Tags=NewTag;	 
		
				%Now the opposite selection
				NewTag=[dataBID(inDatBCA&equality&regSel), dataBcats(inDatBCA&equality&regSel),...
						dataAID(inDatACA&equality&regSel), dataAcats(inDatACA&equality&regSel),...
						 repmat({'duplicate'},numel(dataAID(inDatACA&equality&regSel)),1),...
						 repmat({['Found also in catalogs from ',CountryA,' thus not selected']},numel(dataAID(inDatACA&equality&regSel)),1)]; 
				Tags=[Tags;NewTag];
				
				if (strcmp(CountryA,'Austria') | strcmp(CountryA,'Italy')) & ~all(regSel)
					%Those cases were special measurements are needed
					NewTag=[dataAID(inDatACA&equality&~regSel), dataAcats(inDatACA&equality&~regSel),...
					 dataBID(inDatBCA&equality&~regSel), dataBcats(inDatBCA&equality&~regSel),...
							 repmat({'duplicate'},numel(dataAID(inDatACA&equality&~regSel)),1),...
							 repmat({['Not Selected because of special rules']},numel(dataAID(inDatACA&equality&~regSel)),1)]; 
					Tags=NewTag;	 
			
					%Now the opposite selection
					NewTag=[dataBID(inDatBCA&equality&~regSel), dataBcats(inDatBCA&equality&~regSel),...
							dataAID(inDatACA&equality&~regSel), dataAcats(inDatACA&equality&~regSel),...
							 repmat({'primary'},numel(dataAID(inDatACA&equality&~regSel)),1),...
							 repmat({['Selected because of special rules']},numel(dataAID(inDatACA&equality&~regSel)),1)]; 
					Tags=[Tags;NewTag];

					
					
				
				end
			end
		
		
	

		
		% find identical events in country B in data B and set to primary
		if strcmp(CountryA,'Switzerland')
			inDatACB = strcmp(dataAcountry,CountryB);
			inDatBCB = strcmp(dataBcountry,CountryB);
		else
			inDatACB = strcmp(dataAcountry,CountryB)| strcmp(dataAcountry,'Liechtenstein');
			inDatBCB = strcmp(dataBcountry,CountryB)| strcmp(dataBcountry,'Liechtenstein');
		end
		
		if strcmp(CountryB,'Austria')	
		 	regSel=(dataBlon>10.5);
		elseif strcmp(CountryB,'Italy')
			regSel=(dataBlat<45.45);
		else
			regSel=logical(ones(numel(dataBlat),1))
		end
		
		
		if ~isempty(dataBID(inDatBCB&equality&regSel))
		
			NewTag=[dataBID(inDatBCB&equality&regSel), dataBcats(inDatBCB&equality&regSel), ...
					dataAID(inDatACB&equality&regSel), dataAcats(inDatACB&equality&regSel),...
					 repmat({'primary'},numel(dataBID(inDatBCB&equality&regSel)),1),...
					 repmat({['Found also in catalogs from ',CountryA]},numel(dataBID(inDatBCB&equality&regSel)),1)]; 
			Tags=[Tags;NewTag];	 
	
			%Now the opposite selection
			NewTag=[dataAID(inDatACB&equality&regSel), dataAcats(inDatACB&equality&regSel),...
					dataBID(inDatBCB&equality&regSel), dataBcats(inDatBCB&equality&regSel),...
					 repmat({'duplicate'},numel(dataBID(inDatBCB&equality&regSel)),1),...
					 repmat({['Found also in catalogs from ',CountryB,' thus not selected']},numel(dataBID(inDatBCB&equality&regSel)),1)]; 
			Tags=[Tags;NewTag];
			
			if (strcmp(CountryB,'Austria') | strcmp(CountryB,'Italy')) & ~all(regSel)
				NewTag=[dataBID(inDatBCB&equality&~regSel), dataBcats(inDatBCB&equality&~regSel), ...
						dataAID(inDatACB&equality&~regSel), dataAcats(inDatACB&equality&~regSel),...
						 repmat({'duplicate'},numel(dataBID(inDatBCB&equality&~regSel)),1),...
						 repmat({['Not Selected because of special rules']},numel(dataBID(inDatBCB&equality&~regSel)),1)]; 
				Tags=[Tags;NewTag];	 
		
				%Now the opposite selection
				NewTag=[dataAID(inDatACB&equality&~regSel), dataAcats(inDatACB&equality&~regSel),...
						dataBID(inDatBCB&equality&~regSel), dataBcats(inDatBCB&equality&~regSel),...
						 repmat({'primary'},numel(dataBID(inDatBCB&equality&~regSel)),1),...
						 repmat({['Selected because of special rules']},numel(dataBID(inDatBCB&equality&~regSel)),1)]; 
				Tags=[Tags;NewTag];

			
			
			
			end
		end
		
	%The most difficult ones: not found in both countries

		inDatA_NoCo= ~strcmp(dataAcountry,CountryA) & ~strcmp(dataAcountry,'Liechtenstein') & ~strcmp(dataAcountry,CountryB);
		inDatB_NoCo= ~strcmp(dataBcountry,CountryA) & ~strcmp(dataBcountry,'Liechtenstein') &~strcmp(dataBcountry,CountryB);




	if ~isempty(inDatA_NoCo)
		

	
		%ADist = dataADist(inDatA_NoCo);
		%BDist = dataBDist(inDatB_NoCo);
		
		%NearerA = dataADist >= dataBDist & inDatA_NoCo;
		%NearerB = dataADist < dataBDist & inDatB_NoCo;
		
		NearerA = inDatA_NoCo;
		NearerB = inDatB_NoCo;

		if strcmp(CountryA,'Switzerland')
		
			%the one nearer in country A 
			NewTag=[dataAID(NearerA), dataAcats(NearerA), dataBID(NearerA), dataBcats(NearerA),...
					 repmat({'primary'},numel(dataAID(NearerA)),1),...
					 repmat({['Catalogs of ',CountryA, ' has a higher than the one of ',CountryB]},numel(dataAID(NearerA)),1)]; 
			Tags=[Tags;NewTag];	 

			%Now the opposite selection
			NewTag=[dataBID(NearerA), dataBcats(NearerA),dataAID(NearerA), dataAcats(NearerA),...
					 repmat({'duplicate'},numel(dataAID(NearerA)),1),...
					 repmat({['Catalogs of ',CountryA, ' has a higher than the one of ',CountryB]},numel(dataAID(NearerA)),1)]; 
			Tags=[Tags;NewTag];
		
		elseif strcmp(CountryB,'Switzerland')
		
			%Nearer to Country B	
			NewTag=[dataBID(NearerB), dataBcats(NearerB), dataAID(NearerB), dataAcats(NearerB),...
					 repmat({'primary'},numel(dataBID(NearerB)),1),...
					 repmat({['Catalogs of ',CountryB, ' has a higher than the one of ',CountryA]},numel(dataBID(NearerB)),1)]; 		
			Tags=[Tags;NewTag];	 

			%Now the opposite selection
			NewTag=[dataAID(NearerB), dataAcats(NearerB),dataBID(NearerB), dataBcats(NearerB),...
					 repmat({'duplicate'},numel(dataBID(NearerB)),1),...
					 repmat({['Catalogs of ',CountryB, ' has a higher than the one of ',CountryA]},numel(dataBID(NearerB)),1)]; 
			Tags=[Tags;NewTag];

		end
	end
	%Cases were the two entry is are not in the same Country 
	if ~isempty(dataAID(equality))


	 	%NotEqA = abs(dataADist) >= abs(dataBDist) & ~equality;
		%NotEqB = abs(dataADist) < abs(dataBDist) & ~equality;
		
		NotEqA =  ~equality;
		NotEqB =  ~equality;
		
		if strcmp(CountryA,'Switzerland')

			%the one nearer in country A 
			NewTag=[dataAID(NotEqA), dataAcats(NotEqA), dataBID(NotEqA), dataBcats(NotEqA),...
					 repmat({'primary'},numel(dataAID(NotEqA)),1),...
					 repmat({['The duplicate is at the other side of the border, but ', ...
					 'Catalogs of ',CountryA, ' has a higher priority than the one of ',CountryB]},numel(dataAID(NotEqA)),1)]; 
			Tags=[Tags;NewTag];	 
	
			%Now the opposite selection
			NewTag=[dataBID(NotEqA), dataBcats(NotEqA),dataAID(NotEqA), dataAcats(NotEqA),...
					 repmat({'duplicate'},numel(dataAID(NotEqA)),1),...
					 repmat({['Catalog of ',CountryA,' have a higher priority, thus not selected (not equal in_country)']},numel(dataAID(NotEqA)),1)]; 
			Tags=[Tags;NewTag];
		
		elseif strcmp(CountryB,'Switzerland')
			
			%Nearer to Country B	
			NewTag=[dataBID(NotEqB), dataBcats(NotEqB), dataAID(NotEqB), dataAcats(NotEqB),...
					 repmat({'primary'},numel(dataBID(NotEqB)),1),...
					 repmat({['The duplicate is at the other side of the border, but ', ...
					 'Catalogs of ',CountryB, ' has a higher priority than the one of ',CountryA]},numel(dataBID(NotEqB)),1)]; 		
			Tags=[Tags;NewTag];	 
	
			%Now the opposite selection
			NewTag=[dataAID(NotEqB), dataAcats(NotEqB),dataBID(NotEqB), dataBcats(NotEqB),...
					 repmat({'duplicate'},numel(dataBID(NotEqB)),1),...
					 repmat({['Catalog of ',CountryB,' have a higher priority, thus not selected (not equal in_country)']},numel(dataBID(NotEqB)),1)]; 
			Tags=[Tags;NewTag];
		
		end
	
	
		
	
	
	end
end
function TagCorrector(datastore)
	% This function corrects the wrong group tags, when the wrong entry is primary and corrects 'notused' with
	% 'duplicate' if a primary exists.
	import mapseis.util.emptier;
	
	try	
		group = datastore.getUserData('group');
		appraisal = datastore.getUserData('appraisal');
		comments = datastore.getUserData('comments');
		LastID = datastore.getUserData('LastID');
	catch
		disp('No Tags there')
	end
		%datastore.setUserData('old_group',group);
		datastore.setUserData('old_appraisal',appraisal);
		%datastore.setUserData('old_comments',comments);

	eventStruct=datastore.getFields;
	
	%the unique group IDs
	group(emptier(group))={''};
	groupie= unique(group);
	temp=strcmp(groupie,'');
	groupie=groupie(~temp);
	
	%build catalog depencies
	Germany{1}='BGR01';
    Germany{3}='BGR68-07';
    France{3}='BRGM09';
    Italy{2}='CSI_delivered09';
    Italy{3}='INGV_BSI_ISIDe_09';
    Italy{1}='INGV_CSI10_delivered01';
    Italy{5}='INGV_CSI11web';
    France{1}='ISPN01';
    France{2}='LDG01';
   	Germany{2}='LED01';
    Germany{4}='LED09';
    Austria{2}='ZAMG09';
    Italy{4}='ingv_bolletino09';
    Switzerland{1}='sed_instr_09';
	Austria{1}='zamg01';
	
	
	
	%create a vector with priority of the event
	priorityVector=zeros(numel(eventStruct.org_ID),1);
	cats=eventStruct.catname;
	later75=eventStruct.year>=1975;
	
	%where are they?
	inFrance= strcmp(eventStruct.in_country,'France');
	inSwiss = strcmp(eventStruct.in_country,'Switzerland') | strcmp(eventStruct.in_country,'Liechtenstein');
	inGermany=strcmp(eventStruct.in_country,'Germany');
	inAustria= strcmp(eventStruct.in_country,'Austria');
	inItaly= strcmp(eventStruct.in_country,'Italy');
	
	%get all events located in one of the countries
	CataCountry=strcmp(eventStruct.in_country,'Switzerland') | strcmp(eventStruct.in_country,'Austria') | ...
				strcmp(eventStruct.in_country,'France') | strcmp(eventStruct.in_country,'Germany') | ...
				strcmp(eventStruct.in_country,'Italy') | strcmp(eventStruct.in_country,'Liechtenstein');
	
	%selection for the two special cases
	SpecItal = strcmp(eventStruct.in_country,'Italy') & eventStruct.lat>45.45;
	SpecAust = strcmp(eventStruct.in_country,'Austria') & eventStruct.lon<10.5;


	%switzerland
	prioritVector(strcmp(cats,Switzerland{1})&~inSwiss)=59;
	
	prioritVector(strcmp(cats,Switzerland{1})&inSwiss)=159;
	
	
	%France
	prioritVector(strcmp(cats,France{3})&~inFrance)=34;
	prioritVector(strcmp(cats,France{2})&~inFrance)=33;
	prioritVector(strcmp(cats,France{1})&~inFrance)=32;
	
	prioritVector(strcmp(cats,France{3})&inFrance)=134;
	prioritVector(strcmp(cats,France{2})&inFrance)=133;
	prioritVector(strcmp(cats,France{1})&inFrance)=132;

	
	%Austria
	prioritVector(strcmp(cats,Austria{2})&~inAustria)=14;
	prioritVector(strcmp(cats,Austria{1})&~inAustria)=12;
	
	prioritVector(strcmp(cats,Austria{2})&inAustria&~SpecAust)=114;
	prioritVector(strcmp(cats,Austria{1})&inAustria&~SpecAust)=112;

	prioritVector(strcmp(cats,Austria{2})&inAustria&SpecAust)=14;
	prioritVector(strcmp(cats,Austria{1})&inAustria&SpecAust)=12;

	
	%Italy
	prioritVector(strcmp(cats,Italy{5})&~later75&~inItaly)=44;
	prioritVector(strcmp(cats,Italy{5})&later75&~inItaly)=24;
	prioritVector(strcmp(cats,Italy{4})&~later75&~inItaly)=44;
	prioritVector(strcmp(cats,Italy{4})&later75&~inItaly)=24;
	prioritVector(strcmp(cats,Italy{3})&~later75&~inItaly)=44;
	prioritVector(strcmp(cats,Italy{3})&later75&~inItaly)=24;
	prioritVector(strcmp(cats,Italy{2})&~later75&~inItaly)=44;
	prioritVector(strcmp(cats,Italy{2})&later75&~inItaly)=24;
	prioritVector(strcmp(cats,Italy{1})&~later75&~inItaly)=42;
	prioritVector(strcmp(cats,Italy{1})&later75&~inItaly)=22;
	
	prioritVector(strcmp(cats,Italy{5})&~later75&inItaly&~SpecItal)=144;
	prioritVector(strcmp(cats,Italy{5})&later75&inItaly&~SpecItal)=124;
	prioritVector(strcmp(cats,Italy{4})&~later75&inItaly&~SpecItal)=144;
	prioritVector(strcmp(cats,Italy{4})&later75&inItaly&~SpecItal)=124;
	prioritVector(strcmp(cats,Italy{3})&~later75&inItaly&~SpecItal)=144;
	prioritVector(strcmp(cats,Italy{3})&later75&inItaly&~SpecItal)=124;
	prioritVector(strcmp(cats,Italy{2})&~later75&inItaly&~SpecItal)=144;
	prioritVector(strcmp(cats,Italy{2})&later75&inItaly&~SpecItal)=124;
	prioritVector(strcmp(cats,Italy{1})&~later75&inItaly&~SpecItal)=142;
	prioritVector(strcmp(cats,Italy{1})&later75&inItaly&~SpecItal)=122;
	
	prioritVector(strcmp(cats,Italy{5})&~later75&inItaly&SpecItal)=44;
	prioritVector(strcmp(cats,Italy{5})&later75&inItaly&SpecItal)=24;
	prioritVector(strcmp(cats,Italy{4})&~later75&~inItaly&SpecItal)=44;
	prioritVector(strcmp(cats,Italy{4})&later75&inItaly&SpecItal)=24;
	prioritVector(strcmp(cats,Italy{3})&~later75&inItaly&SpecItal)=44;
	prioritVector(strcmp(cats,Italy{3})&later75&inItaly&SpecItal)=24;
	prioritVector(strcmp(cats,Italy{2})&~later75&inItaly&SpecItal)=44;
	prioritVector(strcmp(cats,Italy{2})&later75&inItaly&SpecItal)=24;
	prioritVector(strcmp(cats,Italy{1})&~later75&inItaly&SpecItal)=42;
	prioritVector(strcmp(cats,Italy{1})&later75&inItaly&SpecItal)=22;


	%Germany
	prioritVector(strcmp(cats,Germany{4})&~later75&~inGermany)=24;
	prioritVector(strcmp(cats,Germany{4})&later75&~inGermany)=44;
	prioritVector(strcmp(cats,Germany{3})&~later75&~inGermany)=23;
	prioritVector(strcmp(cats,Germany{3})&later75&~inGermany)=43;
	prioritVector(strcmp(cats,Germany{2})&~later75&~inGermany)=21;
	prioritVector(strcmp(cats,Germany{2})&later75&~inGermany)=41;
	prioritVector(strcmp(cats,Germany{1})&~later75&~inGermany)=21;
	prioritVector(strcmp(cats,Germany{1})&later75&~inGermany)=41;

	prioritVector(strcmp(cats,Germany{4})&~later75&inGermany)=124;
	prioritVector(strcmp(cats,Germany{4})&later75&inGermany)=144;
	prioritVector(strcmp(cats,Germany{3})&~later75&inGermany)=123;
	prioritVector(strcmp(cats,Germany{3})&later75&inGermany)=143;
	prioritVector(strcmp(cats,Germany{2})&~later75&inGermany)=121;
	prioritVector(strcmp(cats,Germany{2})&later75&inGermany)=141;
	prioritVector(strcmp(cats,Germany{1})&~later75&inGermany)=121;
	prioritVector(strcmp(cats,Germany{1})&later75&inGermany)=141;

		
	%Vector Position Number
	PosVec=1:1:numel(eventStruct.org_ID);
	
	changes=0;
	notusedcorrected=0;	
	for i=1:numel(groupie)
	
		g_events=strcmp(group,groupie{i});
		org_id=eventStruct.org_ID(g_events);
		g_vecID=PosVec(g_events);
		%g_Prior=prioritVector(g_events);
		
		if numel(org_id)>2 | (numel(org_id)==2 & ~strcmp(org_id{1},org_id{2})) 
			
			%check for primary
			pri=strcmp(appraisal,'primary')&g_events;
			notUs=strcmp(appraisal,'notused')&g_events;
			if numel(appraisal(pri))>=1 		
				g_pose=PosVec(g_events);
				%get maximum entry of the priotitVector
				[val pos]=max(prioritVector(g_events));
				MaxPos=g_pose(pos);
				
				if ~any(strcmp(appraisal(MaxPos),'primary'))
					appraisal(MaxPos)={'primary'};
					
					changes=changes+1;
							
					%set the rest to duplicate
					logVec=logical(zeros(numel(g_pose),1));
					logVec(pos)=true;
					NotMaxPos=g_pose(~logVec);
					appraisal(NotMaxPos)={'duplicate'};
				
				end			
				
				%set all notused from entries with a primary to duplicate
				if numel(appraisal(notUs))>=1
					appraisal(notUs)={'duplicate'};
					notusedcorrected=notusedcorrected+1;
				end
			end			
					
		end


	end
	
	%write changes
	datastore.setUserData('appraisal',appraisal);
	
	disp(['Changed primaries: ',num2str(changes)]);
	disp(['Corrected "notused": ',num2str(notusedcorrected)]);
	
end	

function newdatastore = catalogsextractor(datastore,filter_field,selectionkey)
%The function creates a new datastore from an old one by selecting all event where
% the filter_field matches the selectionkey. CAUTION: This version only filters catalog
%fields but not the userdata, buffering and similar Stuff has to be redone on the new catalog
%input:
%datastore:old catalog
%filter_field: string with the fieldname which has to be matches
%selectionkey: string with which the fieldname has to be matched.
%output:
%newdatastore: a new unbuffered catalog containing the entries which match the selection criterion
%
%David Eberhard, 08.05.2009, version 1


import mapseis.datastore.*;

%get the data from the old datastore
eventStruct = datastore.getFields;

%get the fieldnames
fieldStruct = datastore.getFieldNames;

%create the filtervector
selected = strcmp(eventStruct.(filter_field),selectionkey); 


%filter all the fields
for i=1:numel(fieldStruct)
	NEWeventStruct.(fieldStruct{i})=eventStruct.(fieldStruct{i})(selected);

end

%create the new datastore
newdatastore = DataStore(NEWeventStruct);



end 
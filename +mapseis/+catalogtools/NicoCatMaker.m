function NicoCatMaker(datastoreA,datastoreB,filenames,id,filetyp,magnames,whichone)
	
	
	%get data
	dataA=datastoreA.getFields;
	dataB=datastoreB.getFields;
		
	%year 1
	rawA(:,1)=dataA.year(id(:,2));
	rawB(:,1)=dataB.year(id(:,1));
	
	%month 2
	rawA(:,2)=dataA.month(id(:,2));
	rawB(:,2)=dataB.month(id(:,1));
	 
	%day 3
	rawA(:,3)=dataA.day(id(:,2));
	rawB(:,3)=dataB.day(id(:,1));
	
	%hour 4
	rawA(:,4)=dataA.hr(id(:,2));
	rawB(:,4)=dataB.hr(id(:,1));
	
	%minutes 5
	rawA(:,5)=dataA.min(id(:,2));
	rawB(:,5)=dataB.min(id(:,1));
	
	%secondes 6
	rawA(:,6)=dataA.sec(id(:,2));
	rawB(:,6)=dataB.sec(id(:,1));		
	
	%lat 7
	rawA(:,7)=dataA.lat(id(:,2));
	rawB(:,7)=dataB.lat(id(:,1));
	
	%lon 8
	rawA(:,8)=dataA.lon(id(:,2));
	rawB(:,8)=dataB.lon(id(:,1));
	
	%depth 9
	rawA(:,9)=dataA.depth(id(:,2));
	rawB(:,9)=dataB.depth(id(:,1));
	
	%Magnitude 10
	rawA(:,10)=dataA.mag(id(:,2));
	rawB(:,10)=dataB.mag(id(:,1));	
	
	%Magnitude Type
	magtypeA=dataA.magtype(id(:,2));
	magtypeB=dataB.magtype(id(:,1));
	
	%just get the ones with ML magnitudes
	ml=(strcmp(magtypeA,'ML') | strcmp(magtypeA,'Ml') | strcmp(magtypeA,'MlMDN') | ...
						    strcmp(magtypeA,'MlREG') | strcmp(magtypeA,'')) & ...
						    (strcmp(magtypeB,'ML') | strcmp(magtypeB,'Ml') | strcmp(magtypeB,'MlMDN') | ...
						    strcmp(magtypeB,'MlREG') | strcmp(magtypeB,''));
	

	rawA=rawA(ml,:);
	rawB=rawB(ml,:);
	
	switch filetyp	
		
		case 'ASCII'
		
		%open file first catalog 
		fid1 = fopen(filenames{1},'w'); 
		
		if whichone==1
			TheLine1= ['yr;month;day;hr;min;sec;lat;lon;depth;',magnames{1},';',magnames{2}];
		else
			TheLine1= ['yr;month;day;hr;min;sec;lat;lon;depth;',magnames{1}];
		end
		
		fprintf(fid1,'%s',TheLine1); 
		fprintf(fid1,'\n');

		%open file first catalog 
		fid2 = fopen(filenames{2},'w');
		
		if whichone==2 
			TheLine2= ['yr;month;day;hr;min;sec;lat;lon;depth;',magnames{2},';',magnames{1}];
		else
			TheLine2= ['yr;month;day;hr;min;sec;lat;lon;depth;',magnames{2}];
		end
		
		fprintf(fid2,'%s',TheLine2); 
		fprintf(fid2,'\n');
	
	
		%loop over all data and find the wanted event
		for i=1:numel(magtypeA(ml))
			%write line first catalog
			if whichone==1
				TheLine1= [num2str(rawA(i,1)),';',num2str(rawA(i,2)),';',num2str(rawA(i,3)),';',...
							num2str(rawA(i,4)),';',num2str(rawA(i,5)),';',num2str(rawA(i,6)),';',...
							num2str(rawA(i,7)),';',num2str(rawA(i,7)),';',num2str(rawA(i,8)),';',...
							num2str(rawA(i,9)),';',num2str(rawA(i,10)),';',num2str(rawB(i,10))];
			else
				TheLine1= [num2str(rawA(i,1)),';',num2str(rawA(i,2)),';',num2str(rawA(i,3)),';',...
							num2str(rawA(i,4)),';',num2str(rawA(i,5)),';',num2str(rawA(i,6)),';',...
							num2str(rawA(i,7)),';',num2str(rawA(i,7)),';',num2str(rawA(i,8)),';',...
							num2str(rawA(i,9)),';',num2str(rawA(i,10))];
			end				
		
			if whichone==2
				TheLine2= [num2str(rawB(i,1)),';',num2str(rawB(i,2)),';',num2str(rawB(i,3)),';',...
							num2str(rawB(i,4)),';',num2str(rawB(i,5)),';',num2str(rawB(i,6)),';',...
							num2str(rawB(i,7)),';',num2str(rawB(i,7)),';',num2str(rawB(i,8)),';',...
							num2str(rawB(i,9)),';',num2str(rawB(i,10)),';',num2str(rawA(i,10))];
			else
				TheLine2= [num2str(rawB(i,1)),';',num2str(rawB(i,2)),';',num2str(rawB(i,3)),';',...
							num2str(rawB(i,4)),';',num2str(rawB(i,5)),';',num2str(rawB(i,6)),';',...
							num2str(rawB(i,7)),';',num2str(rawB(i,7)),';',num2str(rawB(i,8)),';',...
							num2str(rawB(i,9)),';',num2str(rawB(i,10))];
			end				

		
			%write it 
			fprintf(fid1,'%s',TheLine1); 
			fprintf(fid1,'\n');
			fprintf(fid2,'%s',TheLine2); 
			fprintf(fid2,'\n');
		end
	
		fclose(fid1); 
		fclose(fid2);

	case 'MATLAB'


		if whichone==1
			a=[rawA(:,8),rawA(:,7),rawA(:,1),rawA(:,2),rawA(:,3),rawA(:,10),rawA(:,9),rawA(:,4),rawA(:,5),rawA(:,6),rawB(:,10)];
		else
			a=[rawA(:,8),rawA(:,7),rawA(:,1),rawA(:,2),rawA(:,3),rawA(:,10),rawA(:,9),rawA(:,4),rawA(:,5),rawA(:,6)];
		end				
		save(filenames{1},'a'); 
		
		if whichone==2
			a=[rawB(:,8),rawB(:,7),rawB(:,1),rawB(:,2),rawB(:,3),rawB(:,10),rawB(:,9),rawB(:,4),rawB(:,5),rawB(:,6),rawA(:,10)];	
		else
			a=[rawB(:,8),rawB(:,7),rawB(:,1),rawB(:,2),rawB(:,3),rawB(:,10),rawB(:,9),rawB(:,4),rawB(:,5),rawB(:,6)];	
		end				
		save(filenames{2},'a');
	end

end
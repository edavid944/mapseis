function CatalogPrintOut(datastore,filename,logInd,seti)


%get data
	data=datastore.getFields;
	
		
	%year 1
	raw(:,1)=data.year(logInd);

	
	%month 2
	raw(:,2)=data.month(logInd);
	 
	%day 3
	raw(:,3)=data.day(logInd);

	
	%hour 4
	raw(:,4)=data.hr(logInd);

	
	%minutes 5
	raw(:,5)=data.min(logInd);

	
	%secondes 6
	raw(:,6)=data.sec(logInd);

	
	%lat 7
	raw(:,7)=data.lat(logInd);

	
	%lon 8
	raw(:,8)=data.lon(logInd);

	
	%depth 9
	raw(:,9)=data.depth(logInd);

	
	%Magnitude 10
	raw(:,10)=data.mag(logInd);

	
	

	
	switch seti	
		
		case 'PEGASOS'
			
			%Magnitude Type
			magtype=data.magtype(logInd);
	
			%IDs
			IDs=data.org_ID(logInd);
	
			%Catname
			catname=data.catname(logInd);
			
			%country
			in_country=data.in_country(logInd);
			
			%appraisal & group %comment
			appraisal=datastore.getUserData('appraisal');
			group=datastore.getUserData('group');
			comments=datastore.getUserData('comments');
			
			appr=appraisal(logInd);
			groupID=group(logInd);
			Comme=comments(logInd);
		
		%open file first catalog 
		fid = fopen(filename,'w'); 
		
		
			TheLine = ['yr;month;day;hr;min;sec;lat;lon;depth;magnitude;magtype;org_ID;catname;in_country;group_ID;Appraisal;comments'];
		
		
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');

			
	
		%loop over all data and find the wanted event
		for i=1:numel(magtype)
			%write line first catalog
			if ~isempty(appr{i})
				TheLine= [num2str(raw(i,1)),';',num2str(raw(i,2)),';',num2str(raw(i,3)),';',...
							num2str(raw(i,4)),';',num2str(raw(i,5)),';',num2str(raw(i,6)),';',...
							num2str(raw(i,7)),';',num2str(raw(i,8)),';',...
							num2str(raw(i,9)),';',num2str(raw(i,10)),';',magtype{i},';',...
							IDs{i},';',catname{i},';',in_country{i},';',groupID{i},';',...
							appr{i},';',Comme{i}];
			else
				TheLine= [num2str(raw(i,1)),';',num2str(raw(i,2)),';',num2str(raw(i,3)),';',...
							num2str(raw(i,4)),';',num2str(raw(i,5)),';',num2str(raw(i,6)),';',...
							num2str(raw(i,7)),';',num2str(raw(i,8)),';',...
							num2str(raw(i,9)),';',num2str(raw(i,10)),';',magtype{i},';',...
							IDs{i},';',catname{i},';',in_country{i},';','none',';',...
							'NotUsed',';',Comme{i}];
			end
			%write it 
			fprintf(fid,'%s',TheLine); 
			fprintf(fid,'\n');
	
		end
	
		fclose(fid); 
	
	case 'All-PEGASOS'
	
		fiels=dataStore.getFieldNames;		
		%appraisal & group %comment
		appraisal=datastore.getUserData('appraisal');
		group=datastore.getUserData('group');
		comments=datastore.getUserData('comments');
			
		appr=appraisal(logInd);
		groupID=group(logInd);
		Comme=comments(logInd);
		
		for i=1:numel(fiels)
			
		
		end

end
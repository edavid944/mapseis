function resStruct = catalog_duplicates(catalog_1, catalog_2, parame)
%The function takes two catalogs and search the identical events from the catalog and returns 
%three logical vectors or three matrices with the events unique in catalog A and B and the identical ones
%input
%-----
%catalog_1: first catalog
%catalog_2: second catalog 
%parame: Configuration of the calculation 
%%
%input incase of matrix catalogs: [lon lat dateNum Mag depth]
%
%output
%-------
%resStruct with the components
%uniqA: logical vector or matrix with all events unique in the first catalog
%indenticalA:logical vector or matrix with all events indentical in A
%indenticalB:logical vector or matrix with all events indentical in B
%uniqB: logical vector or matrix with all events unique in the second catalog 

import mapseis.projector.*;



%calculate the datenum for a minute
minfactor = datenum('00:01:00','HH:MM:SS') - datenum('00:00:00','HH:MM:SS');

%get and rework configuration data 
calcmode = parame.mode;

timediff = parame.timedifference*minfactor;
spacediff = km2deg(parame.spacedifference);
%spacediff = (parame.spacedifference);
magdiff = parame.maxmagdiff;

if isfield(parame,'Select_A')
	Select_A=parame.Select_A;
	disp('filter A')
else
	Select_A=[];
end

if isfield(parame,'Select_B')
	Select_B=parame.Select_B;
	disp('filter B')
else
	Select_B=[];
end



switch calcmode

	case 'catalog'
		%use the whole catalogs and return logical vectors		
		
		if isempty(Select_A);
			%get locations
			[lonlat_c1 temp]=getLocations(catalog_1);
			
			%get times
			[time_c1 temp]=getDateNum(catalog_1);
			
			%get magnitudes
			[mags_c1 temp] = getMagnitudes(catalog_1);
			
			
		else
			%get locations
			[lonlat_c1 temp]=getLocations(catalog_1,Select_A);
			
			%get times
			[time_c1 temp]=getDateNum(catalog_1,Select_A);
			
			%get magnitudes
			[mags_c1 temp]= getMagnitudes(catalog_1,Select_A);
			
			index_A=1:length(Select_A);
			index_A=index_A(Select_A);
			

		end
			
		if isempty(Select_B)	
			%get locations
			[lonlat_c2 temp]=getLocations(catalog_2);
				
			%get times
			[time_c2 temp]=getDateNum(catalog_2);
			
			%get magnitudes
			[mags_c2 temp] = getMagnitudes(catalog_2);
			
		else
			%get locations
			[lonlat_c2 temp]=getLocations(catalog_2,Select_B);
				
			%get times
			[time_c2 temp]=getDateNum(catalog_2,Select_B);
			
			%get magnitudes
			[mags_c2 temp] = getMagnitudes(catalog_2,Select_B);		
			
			index_B=1:length(Select_B);
			index_B=index_B(Select_B);
		end
		
		%the loop to calculate
			id = [];
			selvec=1:length(time_c2);
			
			for i=1:length(time_c2)
					%time
					disp(['timestep %' num2str(i/length(time_c2)*100)])
					dt = abs(time_c1(:) - time_c2(i));
      					
      				%distance	
      				distan = distance(lonlat_c1(:,2),lonlat_c1(:,1),lonlat_c2(i,2),lonlat_c2(i,1));	
      				%xa0 = lonlat_c2(i,1);
      				%ya0 = lonlat_c2(i,2);		      
			      	%distan = sqrt(((lonlat_c1(:,1)-xa0)*cos(pi/180*ya0)*111).^2 + ((lonlat_c1(:,2)-ya0)*111).^2);
			      	
			      	dmag = abs(mags_c1(:)-mags_c2(i));
			      	
			      	%find similar events
			      	%f = find((dt <= timediff) & (distan <= spacediff) & (dmag <= magdiff));
  			         f = selvec(((dt <= timediff) & (distan <= spacediff) & (dmag <= magdiff)));
  			        %sizer=size(f);
  			         	if length(f) >= 1	   
  			         		for n = 1:length(f)
  			         		    id = [ id ;  i f(1,n) ] ;     					
     						end
     					end

			
			end
			
			%complete the result
			if isempty(Select_B)
				resStruct.uniqB = logical(ones(length(time_c2),1));
				resStruct.uniqB(id(:,1),:) = logical(0);
   			else
   				resStruct.uniqB = Select_B;
   				uniB=logical(ones(length(time_c2),1));
   				uniB(id(:,1),:) = logical(0);
   				resStruct.uniqB(index_B)=uniB;
   			end
   			
   			if isempty(Select_A)
  				resStruct.uniqA = logical(ones(length(time_c1),1));
  				resStruct.uniqA(id(:,2),:) = logical(0);
  			else
  				resStruct.uniqA = Select_A;
   				uniA=logical(ones(length(time_c1),1));
   				uniA(id(:,2),:) = logical(0);
   				resStruct.uniqA(index_A)=uniA;
  			end
  			
%   			 size(id)
%   			 resStruct.uniqB =1:1:length(time_c2);
%  			 resStruct.uniqB(id(:,1),:) = [];
%   			 resStruct.uniqA = 1:1:length(time_c1);
%  			 resStruct.uniqA(id(:,2),:) = [];

   			 
   			%ij = jm(id(:,1),:);
   			%both needed because the catalogs are not necessary the same length
   			if isempty(Select_B)
   				resStruct.identicalB = ~(resStruct.uniqB);
   			else
   				resStruct.identicalB = ~(resStruct.uniqB)&Select_B ;
   			end
   			
   			if isempty(Select_A)
   				resStruct.identicalA = ~(resStruct.uniqA);
   			else
   				resStruct.identicalA = ~(resStruct.uniqA)&Select_A;
   			end
   			resStruct.id = id;
%   			temp=1:1:length(time_c2);
%   			resStruct.identicalB = temp(id(:,1),:);
%   			temp=1:1:length(time_c1);
%   			resStruct.identicalA = temp(id(:,2),:);
  
	
	
	
	case 'part'
	%use only the inputed events
			%get locations
		lonlat_c1=[catalog_1(:,1) catalog_1(:,2)];
		lonlat_c2=[catalog_2(:,1) catalog_2(:,2)];
		
		%get times
		time_c1=catalog_1(:,3);
		time_c2=catalog_2(:,3);
	
		%the loop to calculate
			id = [];
			for i=1:length(time_c2)
					%time
					dt = abs(time_c1(:) - time_c2(i));
      					
      				%distance	
      				distan = distance(lonlat_c1(:,1),lonlat_c1(:,2),lonlat_c2(i,1),lonlat_c2(i,2));			      
			      	
			      	%find similar events
			      	f = find(dt <= timediff & distan <= spacediff);
  			         	if length(f) == 1	   
  			         		    id = [ id ;  i f ] ; 
     					end

			
			end
			
			%complete the result
			resStruct.uniqB = catalog_2;
  			resStruct.uniqB(id(:,1),:) = logical(0);
   			resStruct.uniqA = catalog_1;
  			resStruct.uniqA(id(:,2),:) = logical(0);
   
   			%ij = jm(id(:,1),:);
   			%both needed because the catalogs are not necessary the same length
   			resStruct.identicalB = catalog_2(id(:,1),:);
   			resStruct.identicalA = catalog_1(id(:,2),:);

	
	
	
end	
	
	


end
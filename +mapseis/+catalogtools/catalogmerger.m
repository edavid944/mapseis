function mergeddata = catalogmerger(data_A, data_B)
	%This function merges all none-user data of the two inputed catalog and
	%returns one single catalog. 
	%Fields only existing in one catalog will have NaN in the other catalog
	
	import mapseis.datastore.*;
	
	if ~isstruct(data_A)
		%first check the existing fields in both catalogs
		fields_data_A = data_A.getFieldNames;
		fields_data_B = data_B.getFieldNames;
		
		%get the data from the catalogs
		StructData_A =  data_A.getFields;
		StructData_B =  data_B.getFields;
	else
		%structure so things are easier
		StructData_A =  data_A;
		StructData_B =  data_B;
		fields_data_A = fieldnames(data_A);
		fields_data_B = fieldnames(data_B);
	end
	
	%get length of both catalogs
	lengA = numel(StructData_A.dateNum);
	lengB = numel(StructData_B.dateNum);
	
	%get the needed fields 
	mergedfields = unique([fields_data_A;fields_data_B]);

	%merge the data to one big catalog
	%eventStruct = struct([]);
		
		for i = 1:numel(mergedfields) 
			
			%first the data_A
			if isfield(StructData_A,mergedfields{i})
					eventStruct.(mergedfields{i}) = StructData_A.(mergedfields{i});
			else
					if ~iscell(StructData_B.(mergedfields{i}))
						eventStruct.(mergedfields{i}) = nan(lengA,1);
					else
						eventStruct.(mergedfields{i}) = cell(lengA,1);
					end
			end
					
			%second the data_B
			if isfield(StructData_B,mergedfields{i})
					eventStruct.(mergedfields{i}) = ...
					[eventStruct.(mergedfields{i});	StructData_B.(mergedfields{i})];
			else		
					if ~iscell(StructData_A.(mergedfields{i}))
						eventStruct.(mergedfields{i}) = ...
						[eventStruct.(mergedfields{i});	nan(lengB,1)];
					else
						eventStruct.(mergedfields{i}) = ...
						[eventStruct.(mergedfields{i});	cell(lengB,1)];
					end
			end
		
			
		end
		
		%sort the new catalog
		[sortedDateNums,sortInds]=sort(eventStruct.dateNum);
		%fldNames = fieldnames(eventStruct);
		for i=1:numel(mergedfields)
		    if numel(eventStruct.(mergedfields{i})) == numel(sortInds)
    			   eventStruct.(mergedfields{i})= eventStruct.(mergedfields{i})(sortInds);
    		end
		end
		
		%create new catalog
		if ~isstruct(data_A)
			mergeddata=DataStore(eventStruct);
		else
			mergeddata=eventStruct;
		end	


end
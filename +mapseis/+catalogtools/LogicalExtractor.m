function eventStruct = LogicalExtractor(datastore,LogInd)
	%uses the logical index  LogInd to filter all fields and return it as a eventStruct
	
	 import mapseis.datastore.*;

	%get the data from the old datastore
	if ~isstruct(datastore)
		eventStruct = datastore.getFields;
		%get the fieldnames
		fieldStruct = datastore.getFieldNames;
	else
		eventStruct=datastore;
		fieldStruct=fieldnames(eventStruct);
	end	

	%filter all the fields
	for i=1:numel(fieldStruct)
		eventStruct.(fieldStruct{i})=eventStruct.(fieldStruct{i})(LogInd);

	end

end 
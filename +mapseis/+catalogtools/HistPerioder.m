function EventsInTime = HistPerioder(TheCatalog,BinSize,TheStep)
	%function counts the number of earthquakes in the specified periods
	%(BinSize in decyear)
	EventsInTime=[];
	
	%NrBins=floor((max(TheCatalog(:,3))-min(TheCatalog(:,3)))/BinSize);
	
	StartPoint=min(TheCatalog(:,3));
	CurPoint=StartPoint;
	
	i=1;
	while CurPoint<=max(TheCatalog(:,3))
		InBin=TheCatalog(:,3)>=CurPoint&TheCatalog(:,3)<CurPoint+BinSize;
		try
			EventsInTime(i)=sum(InBin);
		catch
			EventsInTime(i)=0;
		end	
		CurPoint=CurPoint+TheStep;
		i=i+1;
	end



end

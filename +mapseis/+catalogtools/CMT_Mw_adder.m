function datastore=CMT_Mw_adder(oldData)
	%simple code, it takes some old CMT catalog in the datastore format
	%and adds Mw to it (the new import filter adds it automatically)
	
	%get old data
	eventStruct=oldData.getRawFields;
	
	%add Mw (relation from the global CMT catalog homepage
	%TotalMoment=eventStruct.s1_value.*eventStruct.s1_value_mult+eventStruct.s2_value.*eventStruct.s2_value_mult+eventStruct.s3_value.*eventStruct.s3_value_mult;
	%TotalMoment=abs(eventStruct.s1_value_mult)+abs(eventStruct.s2_value_mult)+abs(eventStruct.s3_value_mult);
	TotalMoment=eventStruct.scalar_moment_mult;
	Mw=(2/3)*(log10(TotalMoment)-16.1);
	%Mw=round(Mw*10)/10;
	eventStruct.Mw=Mw;
	
	
	%edited texts
	CATALOG_FIELD_DESCRIPTION={'dateNum','The string containing the date and the time ';...
					'ref_catalog',['Hypocenter reference catalog (e.g., PDE for USGS location',...
						', ISC for ISC catalog, SWE for surface-wave location, [Ekstrom, BSSA, 2006])'];...
					'lat', 'Latitude of event in degrees ';...
					'lon','Longitude of event in degrees ';...
					'depth','Depth of event in km ';...
					'mb','Reported magnitudes, usually - mb - and MS ';...
					'MS','Reported magnitudes, usually mb and - MS - ';...
					'Mw','Calculated from the total Moment of the principal stress axis';...
					'geo_location','Geographical location (24 characters)';...
					'CMT_event_name', ['CMT event name. This string is a unique CMT-event identifier. Older',...
							'events have 8-character names, current ones have 14-character names.',...
							'See note (1) below for the naming conventions used.'];...
					'inv_data_used', ['Data used in the CMT inversion. Three data types may be used:',...
							'Long-period body waves (B), Intermediate-period surface waves (S),',...
							'and long-period mantle waves (M). For each data type, three values',...
							'are given: the number of stations used, the number of components',... 
							'used, and the shortest period used.'];...
					'source_Type', ['Type of source inverted for: "CMT: 0" - general moment tensor;',... 
							'"CMT: 1" - moment tensor with constraint of zero trace (standard);',... 
							'"CMT: 2" - double-couple source.'];...
					'Moment_rate_func', ['Type and duration of moment-rate function assumed in the inversion.',... 
							'"TRIHD" indicates a triangular moment-rate function, "BOXHD" indicates',...
							'a boxcar moment-rate function. The value given is half the duration',...
							'of the moment-rate function. This value is assumed in the inversion,',...
							'following a standard scaling relationship (see note (2) below),',...
							'and is not derived from the analysis.'];...
					'Centroid_parameters', ['The whole string see other parameters for singl values',...
								'Centroid parameters determined in the inversion. Centroid time, given',...
								'with respect to the reference time, centroid latitude, centroid',...
								'longitude, and centroid depth. The value of each variable is followed',...
								'by its estimated standard error. See note (3) below for cases in',...
								'which the hypocentral coordinates are held fixed.'];...
					'Centroid_time','Centroid time, given with respect to the reference time';...
					'Centroid_time_err','Error of the Centroid time';...
					'Centroid_lat','Centroid Latitude';...
					'Centroid_lat_err','Error of Centroid Latidude';...
					'Centroid_lon','Centroid Longitude';....
					'Centroid_lon_err','Error of Centroid Longitude';...
					'Centroid_depth','Centroid Depth';...
					'Centroid_depth_err','Error of the Centroid Depth';...
					'Centroid_depth_type', ['Type of depth. "FREE" indicates that the depth was a result of the',...
								'inversion; "FIX " that the depth was fixed and not inverted for;',...
								'"BDY " that the depth was fixed based on modeling of broad-band ',...
								'P waveforms.'];...
					'Centroid_timestap', ['Timestamp. This 16-character string identifies the type of analysis that',...
								'led to the given CMT results and, for recent events, the date and',... 
								'time of the analysis. This is useful to distinguish Quick CMTs ("Q-"),',... 
								'calculated within hours of an event, from Standard CMTs ("S-"), which ',...
								'are calculated later. The format for this string should not be ',...
								'considered fixed.'];...
					'Moment_value_exponent', ['The exponent for all following moment values. For example, if the ',...
								'exponent is given as 24, the moment values that follow, expressed in ',...
								'dyne-cm, should be multiplied by 10**24.'];...
					'Moment_Tensor_Elements', ['The whole string see other parameters for singl values ',...
								'The six moment-tensor elements: Mrr, Mtt, Mpp, Mrt, Mrp, Mtp, where r ',...
								'is up, t is south, and p is east. See Aki and Richards for conversions ',...
								'to other coordinate systems. The value of each moment-tensor ',...
								'element is followed by its estimated standard error. See note (4) ',...
								'below for cases in which some elements are constrained in the inversion.'];...
					'Mrr', 'Moment Tensor Element in up-up direction';...
					'Mrr_err', 'Error Mrr';...
					'Mtt', 'Moment Tensor Element in south-south direction';...
					'Mtt_err', 'Error Mtt';...
					'Mpp', 'Moment Tensor Element in east-east direction';...
					'Mpp_err', 'Error Mpp';...
					'Mrt', 'Moment Tensor Element in up-south direction';...
					'Mrt_err', 'Error Mrt';...
					'Mrp', 'Moment Tensor Element in up-east direction';...
					'Mrp_err', 'Error Mrp';...
					'Mtp', 'Moment Tensor Element in south-east direction';...
					'Mtp_err', 'Error Mrp';...
					'Version_Code',['Version code. This three-character string is used to track the version ',...
							'of the program that generates the "ndk" file.'];...
					'Moment_Tensor_Princ_String', ['The whole string see other parameters for singl values ',...
								'Moment tensor expressed in its principal-axis system: eigenvalue, ',... 
								'plunge, and azimuth of the three eigenvectors. The eigenvalue should be',...
								'multiplied by 10**(exponent) as given on line four.'];...
					's1_value', 'Eigenvalue of the first principal axis (not multiplied)';...
					's1_plunge', 'Plunge of the first principal axis';...			
					's1_azimuth', 'Azimuth of the first principal axis';...
					's1_value_mult', 'The eigenvalue of s1 multiplied by *10^Moment_value_exponent';...
					's2_value', 'Eigenvalue of the second principal axis (not multiplied)';...
					's2_plunge', 'Plunge of the second principal axis';...			
					's2_azimuth', 'Azimuth of the second principal axis';...
					's2_value_mult', 'The eigenvalue of s2 multiplied by *10^Moment_value_exponent';...
					's3_value', 'Eigenvalue of the third principal axis (not multiplied)';...
					's3_plunge', 'Plunge of the third principal axis';...			
					's3_azimuth', 'Azimuth of the third principal axis';...
					's3_value_mult', 'The eigenvalue of s3 multiplied by *10^Moment_value_exponent';...					
					'scalar_moment', 'Scalar moment, to be multiplied by 10**(exponent) as given on line four.';...
					'scalar_moment_mult', 'scalar_moment multiplied by *10^Moment_value_exponent';...
					'Nodal_Plane_String', ['The whole string see other parameters for single values ',...
							'Strike, dip, and rake for first nodal plane of the best-double-couple ',...
							'mechanism, repeated for the second nodal plane. The angles are defined ',...
							'as in Aki and Richards.'];...
					'first_plane_strike', 'Strike of the first Nodal Plane';...
					'first_plane_dip', 'Dip of the first Nodal Plane';...
					'first_plane_rake', 'Rake of the first Nodal Plane';...
					'second_plane_strike', 'Strike of the second Nodal Plane';...
					'second_plane_dip', 'Dip of the second Nodal Plane';...
					'second_plane_rake', 'Rake of the second Nodal Plane'};
	
					
	CATALOG_INFO_TEXT=[	   'Notes (additional information): ',...
					'(1) CMT event names follow two conventions. Older events use an 8-character ',... 
					'name with the structure XMMDDYYZ, where MMDDYY represents the date of ',...
					'the event, Z is a letter (A-Z followed by a-z) distinguishing different ',...
					'events on the same day, and X is a letter (B,M,Z,C,...) used to identify ',...
					'the types of data used in the inversion. Newer events use 14-character event ',...
					'names with the structure XYYYYMMDDhhmmZ, in which the time is given to greater ',...
					'precision, and the initial letter is limited to four possibilities: B - body ',...
					'waves only, S - surface waves only, M - mantle waves only, C - a combination ',...
					'of data types. ',...
					'(2) The source duration is generally estimated using an empirically determined ',...
					'relationship such that the duration increases as the cube root of the scalar ',...
					'moment. Specifically, we currently use a relationship where the half duration ',...
					'for an event with moment 10**24 is 1.05 seconds, and for an event with moment ',...
					'10**27 is 10.5 seconds. ',...
					'(3) For some small earthquakes for which the azimuthal distribution of stations ',...
					'with useful seismograms is poor, we constrain the epicenter of the event to ',...
					'the reference location. This is reflected in the catalog by standard ',...
					'errors of 0.0 for both the centroid latitude and the centroid longitude. ',...
					'(4) For some very shallow earthquakes, the CMT inversion does not well ',...
					'constrain the vertical-dip-slip components of the moment tensor (Mrt and Mrp), ',...
					'and we constrain these components to zero in the inversion. The standard',...
					'errors for Mrt and Mrp are set to zero in this case.'];
									

	%add new data to the catalog
	datastore=oldData;
	datastore.ReplaceInternalRawData(eventStruct);
	datastore.setUserData('FieldInfo',CATALOG_FIELD_DESCRIPTION);
	datastore.setUserData('CatalogInfo',CATALOG_INFO_TEXT);

end

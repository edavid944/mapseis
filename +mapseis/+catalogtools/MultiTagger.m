function MultiTagger(datastore,totag,prefix,work)
%Sets the inputed events to 'selected' and adds the duplicates and comments if defined
%input: cellarray with: {org_Id, catalogname, duplicate_ID, duplicate_catalogname, Comment}
%the last three parameters can be empty.
%the data is saved in 3 parts: selected, duplicate and comment

	%get data from datastore
	rawdata = datastore.getFields;
	
	%try if there already is a selection and get it 
	try	
		selected = datastore.getUserData([prefix,'_selected']);
		duplicates = datastore.getUserData([prefix,'_duplicates']);
		comments = datastore.getUserData([prefix,'_comments']);
	catch
		selected = logical(zeros(numel(rawdata.org_ID),1));
		duplicates =  cell(numel(rawdata.org_ID),2);
		comments =  cell(numel(rawdata.org_ID),1);
	end
	
	%loop over all data and find the wanted event
	for i=1:numel(totag(:,1))
		candidat=strcmp(totag{i,1},rawdata.org_ID) & strcmp(totag{i,2},rawdata.catname);
		
		%set duplicate and comment
		duplicates(candidat,1)=totag(i,3);
		duplicates(candidat,2)=totag(i,4);
		
		%set comment
		comments(candidat,1)=totag(i,5);
		
		%set or unset the selected
		switch work
			case 'set'
			selected(candidat,1)=true;	
			case 'unset'
			selected(candidat,1)=false;
		end

	
	end
	
	%write data to datastore
	datastore.setUserData([prefix,'_selected'],selected);
	datastore.setUserData([prefix,'_duplicates'],duplicates);
	datastore.setUserData([prefix,'_comments'],comments);
	
end
function grouptagger(datastore,totag, filename)
	% similar to the previous taggers but this one works with groups IDs and 'primary',
	% 'notused' and 'duplicate' instead of single links.
	% if filename is empty, the data will be written to the datastore 
	
	if isempty(filename)
		%get data from datastore
		rawdata = datastore.getFields;
		
		%try if there already is a selection and get it 
		try	
			group = datastore.getUserData('group');
			appraisal = datastore.getUserData('appraisal');
			comments = datastore.getUserData('comments');
			LastID = datastore.getUserData('LastID');
		catch
			group = cell(numel(rawdata.org_ID),1);
			appraisal =  cell(numel(rawdata.org_ID),1);
			comments =  cell(numel(rawdata.org_ID),1);
			LastID = 1;
			disp('User Data created')
		end
		
		%loop over all data and find the wanted event
		for i=1:numel(totag(:,1))
			%get the first entry
			primary = strcmp(totag{i,1},rawdata.org_ID) & strcmp(totag{i,2},rawdata.catname);
			try
				priID = group{primary};
			catch
				%not found, nothing to now
				continue
			end	
			%disp(totag{i,1});
			%simple rename to make the code more readable
			toSet=totag{i,5};
			
			%check if a second entry is specified.
			if ~isempty(totag{i,3})
				secondary = strcmp(totag{i,3},rawdata.org_ID) & strcmp(totag{i,4},rawdata.catname);
				try
					secID=group{secondary};
				catch
					secondary = {};
				end
			else
				secondary = {};
			end
			
			
			%now go throught the different cases
			
			%total new entry with no secondary specified
			if isempty(secondary) & isempty(priID)
				group(primary) = {num2str(LastID)};
				LastID=LastID+1;
				appraisal(primary) = {toSet};
				comments(primary) = {totag{i,6}};
			
			%No duplicate but previous group ID
			elseif isempty(secondary) & ~isempty(priID)
				
				if strcmp(toSet,'primary')
					%check if there are other with the groupID who are primary
					lastPrim=strcmp(group,priID) & strcmp(appraisal,'primary');
					if any(lastPrim)
						appraisal(lastPrim)={'duplicate'};
					end
					
					%check for notused, those become duplicates know
					lastNot=strcmp(group,priID) & strcmp(appraisal,'notused');
					if any(lastNot)
						appraisal(lastNot)={'duplicate'};
					end
					
					%set the parameters
					appraisal(primary)={toSet};
					
					if isempty(comments(primary))
						comments(primary) = {totag{i,6}};	
					else
						%needed in case more than one is found
						tempCom=comments(primary);
						comments(primary) = {[tempCom{1},'/',totag{i,6}]};
					end	
				else
					appraisal(primary) = {toSet};
					comments(primary) = {totag{i,6}};
				end	
			
			elseif ~isempty(secondary) & isempty(priID)
				if isempty(secID)
					%first one
					group(primary) = {num2str(LastID)};
					LastID=LastID+1;
					appraisal(primary) = {toSet};
					comments(primary) = {totag{i,6}};

					%second one
					tempID=group(primary); %needed for the case of duplicates
					group(secondary)={tempID{1}};
					appraisal(secondary)={'duplicate'};
					%comments are set only to the first event
				
				elseif ~isempty(secID) & strcmp(toSet,'primary')
					%check if there are other with the groupID who are primary
					lastPrim=strcmp(group,secID) & strcmp(appraisal,'primary');
					if any(lastPrim)
						appraisal(lastPrim)={'duplicate'};
					end
					
					%check for notused, those become duplicates know
					lastNot=strcmp(group,secID) & strcmp(appraisal,'notused');
					if any(lastNot)
						appraisal(lastNot)={'duplicate'}
					end
					
					group(primary)={secID};
					appraisal(primary) = {toSet};
					comments(primary) = {totag{i,6}};

					
					
				else
					group(primary)={secID};
					appraisal(primary) = {toSet};
					comments(primary) = {totag{i,6}};

					
				end		
			
			
			elseif ~isempty(secondary) & ~isempty(priID)
				if isempty(secID)
					%first one
					group(primary) = {num2str(LastID)};
					LastID=LastID+1;
					appraisal(primary) = {toSet};
					if isempty(comments(primary))
						comments(primary) = {totag{i,6}};	
					else
						%needed in case more than one is found
						tempCom=comments(primary);
						comments(primary) = {[tempCom{1},'/',totag{i,6}]};

					end	

					%second one
					
					group(secondary)={priID};
					appraisal(secondary)={'duplicate'};
					%comments are set only to the first event
				
				elseif ~isempty(secID) & strcmp(priID,secID)
					if strcmp(toSet,'primary')
						%check if there are other with the groupID who are primary
						lastPrim=strcmp(group,priID) & strcmp(appraisal,'primary');
						if any(lastPrim)
							appraisal(lastPrim)={'duplicate'};
						end
					
						%check for notused, those become duplicates know
						lastNot=strcmp(group,priID) & strcmp(appraisal,'notused');
						if any(lastNot)
							appraisal(lastNot)={'duplicate'};
						end
					
						group(primary)={priID};
						appraisal(primary) = {toSet};
						
						if isempty(comments(primary))
							comments(primary) = {totag{i,6}};	
						else
							%needed in case more than one is found
							tempCom=comments(primary);
							comments(primary) = {[tempCom{1},'/',totag{i,6}]};
						end	


					
					
					else
						group(primary)={priID};
						appraisal(primary) = {toSet};
						
						if isempty(comments(primary))
							comments(primary) = {totag{i,6}};	
						else
							%needed in case more than one is found
							tempCom=comments(primary);
							comments(primary) = {[tempCom{1},'/',totag{i,6}]};

						end	
					
					end
				
				elseif ~isempty(secID) & ~strcmp(priID,secID)	 
					%most difficult case
					if strcmp(toSet,'primary')
						%check if there are other with the groupID who are primary
						
						%first for the priID
						lastPrim=strcmp(group,priID) & strcmp(appraisal,'primary');
						if any(lastPrim)
							appraisal(lastPrim)={'duplicate'};
						end
					
						%check for notused, those become duplicates know
						lastNot=strcmp(group,priID) & strcmp(appraisal,'notused');
						if any(lastNot)
							appraisal(lastNot)={'duplicate'};
						end
						
						
						%now the secID
						lastPrim=strcmp(group,secID) & strcmp(appraisal,'primary');
						if any(lastPrim)
							appraisal(lastPrim)={'duplicate'};
							group(lastPrim)={priID};
						end
					
						%all others become duplicates
						lastNot=strcmp(group,secID) & ~strcmp(appraisal,'primary');

						if any(lastNot)
							appraisal(lastNot)={'duplicate'};
							group(lastNot)={priID};
						end
							
						group(primary)={priID};
						appraisal(primary) = {toSet};
						
						if isempty(comments(primary))
							comments(primary) = {totag{i,6}};	
						else
							%needed in case more than one is found
							tempCom=comments(primary);
							comments(primary) = {[tempCom{1},'/',totag{i,6}]};

						end	

					
					
					else
						%check if there are other with the groupID who are primary
						
						%first for the priID
						lastPrim=strcmp(group,priID) & strcmp(appraisal,'primary');
						if any(lastPrim)
							appraisal(lastPrim)={'duplicate'};
							group(lastPrim)={secID};
						end
					
						%check for notused, those become duplicates know
						lastNot=strcmp(group,priID) & ~strcmp(appraisal,'primary');
						if any(lastNot)
							appraisal(lastNot)={'duplicate'};
							group(lastNot)={secID};
						end
						
						
													
						group(primary)={secID};
						appraisal(primary) = {toSet};
						
						if isempty(comments(primary))
							comments(primary) = {totag{i,6}};	
						else
							%needed in case more than one is found
							tempCom=comments(primary);
							comments(primary) = {[tempCom{1},'/',totag{i,6}]};

						end
							
					end

			
				end
			
			end

			
	
		
		end
		
		%write data to datastore
		datastore.setUserData('group',group);
		datastore.setUserData('appraisal',appraisal);
		datastore.setUserData('comments',comments);
		datastore.setUserData('LastID',LastID);
	
	
	
	
	else		
		rawdata=datastore.getFields;
		
		try	
			group = datastore.getUserData('group');
			appraisal = datastore.getUserData('appraisal');
			comments = datastore.getUserData('comments');
			%LastID = str2num(datastore.getUserData('LastID'));
		catch
			disp('no groups are set in this catalog')
				
		end

		
		
		
		%open file
		fid = fopen(filename,'w'); 
		TheLine= ['org_id;catname;group;appraisal;comment'];
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');


	
	
	%loop over all data and find the wanted event
	for i=1:numel(rawdata.org_ID)
		if isempty(group{i})
			curapp='notused';
			curgroup='notset';
			curcomment='event was not set';
		else
			curapp=appraisal{i};
			curgroup = group{i};
			curcomment = comments{i};
		end
			
		TheLine= [rawdata.org_ID{i},';',rawdata.catname{i},';',curgroup,';',curapp,';',curcomment];

		
		%write it to the last line 
		%open file
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
	end
	fclose(fid); 

		
		
		
	end

end
function [k,c,p] = MaxLikeOmori(tStart,tEnd,ObsEvents,MainShock)
	%To modify
	
	import mapseis.step.*;
	import mapseis.util.*;
	
	%old call -> [k,c,p] = MaxLikeOmori(tStart,tEnd,ObsEvents,Gridxy,maepi,Mc,ModelRadius,RDefs)
	
	%use ShortCat and datenum
	TimeMS = MainShock(1,4);
	tDiff = (ObsEvents(:,4))-TimeMS;
	tStart = tStart - TimeMS;
	tEnd = tEnd - TimeMS;
	
	%try the original date
	%The26=datenum('1926-01-01 00:00:00','YYYY-MM-DD hh:mm:ss');
	%TimeMS = (MainShock(:,4)-The26)*60;
	%ConvObs = (ObsEvents(:,4)-The26)*60;
	%tDiff = (ConvObs);%/1440;
	%tStart = ((tStart-The26)*60);
	%tEnd = ((tEnd-The26)*60);
	
	%tDiff=tDiff-TimeMS;
	%tStart=tStart-TimeMS;
	%tEnd=tEnd-TimeMS;
	
	%TimeMS=decyear(datevec(TimeMS));
	%tDiff=decyear(datevec(tDiff));
	%tStart=decyear(datevec(tStart));
	%tEnd=decyear(datevec(tEnd));
	
	Options = optimset('Display','none','MaxFunEvals',800);
	
	[NumEvents,dummy] = size(ObsEvents);
	
	
	pStart = 1.3;
	cStart = 0.3;
	Acp = ((tEnd+cStart).^(1-pStart)-(tStart+cStart).^(1-pStart))/(1-pStart);
	kStart = NumEvents/Acp;
	StartVals = [pStart cStart kStart];
	
	%[fVals, LogLikelihood] = fmincon('CallbackOmori',StartVals,[],[],[],[],[0.01 0.01 .5],[5000 20 5],[],Options,tDiff,tStart,tEnd);
	
	FuncHandle=@(x) callback_omori_mle(x,tDiff,tStart,tEnd);
	
	
	%some options
	EstOptions= struct(	'Display','iter',...
				'MaxIter',1000);
	
	%now search that minimum
	EstTimer=tic;
	%[EndValues,fval,exitflag,output] = fminsearch(FuncHandle,StartVals,EstOptions)
	try
	[EndValues, LogLikelihood] = fmincon(FuncHandle,StartVals,[],[],[],[],[0.01 0.01 .5],[5000 20 5],[],Options);
	catch
	EndValues=[NaN NaN NaN];
	end	
	EstimationTime=toc(EstTimer);
	
	k = EndValues(3);
	c = EndValues(2);
	p = EndValues(1);

end


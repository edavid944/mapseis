function OutStruct = Add_RJ_a(GR_a_value,OrgStruct,b_value,M0)
	%uses RJafromGRa to add a Reasenberg-Jones based a-value based
	%the b-value(s) and the minimum Magnitude.
	
	import mapseis.step.*;
	
	if all(size(b_value)==1)
		b_value=ones(size(OrgStruct.Single_k_values))*b_value;
	end
	
	
	if all(size(GR_a_value)==1)
		GR_a_value=ones(size(OrgStruct.Single_k_values))*GR_a_value;
	end

	RJ_a_value=NaN(size(OrgStruct.Single_k_values));
	
	for i=1:numel(RJ_a_value)
		tStart=0
		tEnd=1
		c = OrgStruct.Single_c_values(i);
		p = OrgStruct.Single_p_values(i);
		
	
		RJ_a_value(i) = RJafromGRa(GR_a_value(i),tStart,tEnd,c,p,b_value(i),M0)


	end
	
	mean_a = nanmean(RJ_a_value(OrgStruct.isSensible));
	
	%pack again
	OutStruct=OrgStruct;
	OutStruct.Single_a_values=RJ_a_value;
	OutStruct.a=mean_a;
	
end

function[RJint] = RJintegral(tStart,tEnd,c,p)
	%to modify
		
	
	lenP = length(tStart);
	if length(p) == 1
	    p = repmat(p,lenP,1);
	    c = repmat(c,lenP,1);
	end
	
	for pLoop = 1:lenP
	    if p(pLoop) == 1
		RJint(pLoop) = log(tEnd(pLoop)+c(pLoop))-log(tStart(pLoop)+c(pLoop));
	    else
		RJintT = (tEnd(pLoop)+c(pLoop))^(1-p(pLoop));
		RJintS = (tStart(pLoop)+c(pLoop))^(1-p(pLoop));
		RJint(pLoop) = (RJintT-RJintS)/(1-p(pLoop));
	    end
	end

end

function [ForecastRates ]=calc_RJ(fMainMag,fMinMag,fMaxMag, fAvalue,fBvalue,fPvalue,fCvalue,fForecastTime,fForecastStart)
	% example: [sForecastRate]= calc_RJ(5.5,5, 7, -1.6, 1, 1.2, 0.05, 12, 0)
	%
	% -----------------------------------------------------------------------
	% Function to calculate rates of aftershocks and probabilities within time
	% fForecastStart - (ForecastStart+fForecastTime).
	%
	% Input:
	% fMainMag : main shock magnitude
	% fMinMag : minimum magnitude to forecast rates for
	% fMaxMag : maximum magnitude to forecast rates for
	% dmag    : Magnitude Step for integration
	% fAvalue : Generic a-value
	% fBvalue : Generic b-value
	% fPvalue : Generic p-value
	% fCvalue : Generic c-value
	% fForecastTime : Length of forecast window (days)
	% fForcastStart : Start of forcast window after the main shock (days)
	%
	% Output: fForecastRates    [ForecastRates lenStart' vProbRates mg]
	%                           Forecast Rate, Start Time, Probabilities, mainMag
	%
	%
	%
	%
	

	
	F = @(m,s)(10.^(fAvalue+fBvalue.*(fMainMag-m)).*(s+fCvalue).^(-fPvalue));
	% Rates in time bins
	ForecastRates = dblquad(F, fMinMag, fMaxMag, fForecastStart, fForecastStart+fForecastTime);
	
	
	%Probability that event from minMag to maxmag happens at within time
	%Forecaststart-Forecasttime
	vProbRates=1-exp(-ForecastRates);
	
	
	
	mg=ones(size(vProbRates));mg(:,:)=fMainMag;
	ForecastRates=[ForecastRates fForecastStart vProbRates  fMainMag];

end


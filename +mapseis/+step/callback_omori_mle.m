function [LogLikelihood] = callback_omori_mle(vValues,time_as,tStart,tEnd);
	% this is the function used in calc_omori_mle
	% Stefan Wiemer 2/2003
	
	%to modify
	
	p = vValues(1);
	c = vValues(2);
	k = vValues(3);
	l = time_as >= tStart & time_as < tEnd;
	NumEvents = length(time_as(l));
	time_as = time_as(l);
	
	if p ~= 1
	    Acp = ((tEnd+c).^(1-p)-(tStart+c).^(1-p))/(1-p);
	else
	    Acp = log(tEnd+c)-log(tStart+c);
	end
	
	LogLikelihood = NumEvents*log(k)-p*sum(log(time_as+c))-k*Acp;
	LogLikelihood = -LogLikelihood;


end

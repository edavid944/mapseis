function Results=BGStepper(EqMag,EqHash,BasicConfig,ReasJonConfig,TrafficSet,BGConfig,HazardConfig,PlotIt)
	%Just some experimental script for better understandig this type of models	

	
	import mapseis.step.*;
	import mapseis.plots.*;
	import mapseis.datastore.assocArray;
	
	if isempty(BasicConfig)
		BasicConfig =	struct(	'TimeStart',0,...
					'TimeStep',1/48,...
					'TimeEnd',365,...
					'LogTimeMode',false,...
					'RandomProb',10^-5,...
					'CastArea',100);
	end
	
	if isempty(ReasJonConfig)
		%Default settings
		ReasJonConfig =	struct(	'MiniMag',2,...
					'MaxiMag',7	,...
					'avalue',-1.69,...
					'bvalue',1,...
					'pvalue',0.91,...
					'cvalue',0.01,...
					'ForecastTime',0,...
					'ForecastStart',0);
					
	end
	
	if isempty(BGConfig)
		BGConfig =	struct(	'BGaval',2.91,...
					'BGbval',0.9,...
					'Scaler',1);
	end
	
	if isempty(HazardConfig)
		HazardConfig =	struct(	'EMSLevel',5,...
					'MaxMagHaz',5.15,...
					'MinMagHaz',6.95,...
					'HazCastTime',1,...
					'LowRateCut',0);
	end
	
	if isempty(TrafficSet)
		TrafficSet =	struct(	'GreenRate',0,...
					'YellowRate',0,...
					'RedRate',0);
	end
	
	
	%Unpack parameters
	TimeStart=BasicConfig.TimeStart;
	TimeStep=BasicConfig.TimeStep;
	TimeEnd=BasicConfig.TimeEnd;
	LogTimeMode=BasicConfig.LogTimeMode;
	RandomProb=BasicConfig.RandomProb;
	CastArea=BasicConfig.CastArea;
	
	MiniMag=ReasJonConfig.MiniMag;
	MaxiMag=ReasJonConfig.MaxiMag;
	avalue=ReasJonConfig.avalue;
	bvalue=ReasJonConfig.bvalue;
	pvalue=ReasJonConfig.pvalue;
	cvalue=ReasJonConfig.cvalue;
	ForecastTime=ReasJonConfig.ForecastTime;
	ForecastStart=ReasJonConfig.ForecastStart;

	BGaval=BGConfig.BGaval;
	BGbval=BGConfig.BGbval;
	Scaler=BGConfig.Scaler;
	
	LowRateCut=HazardConfig.LowRateCut;	
	GreenRate=TrafficSet.GreenRate;
	YellowRate=TrafficSet.YellowRate;
	RedRate=TrafficSet.RedRate;

	
	%Generate Hash and RawCatalog for EQ
	if isempty(EqHash)
		EqHash=assocArray();
		Count=0;
	else
		Keys=EqHash.getKeys;
		Count=max(num2str(Keys)); %Check if that works
	end	
	
	EqGrave=assocArray();
	EqCatalog=[];
	%Count=0;
	
	if ~isempty(EqMag)
		Count=Count+1;	
		EqCatalog(Count,:)=[TimeStart,EqMag];
		EqObj=struct(	'Mag',EqMag,...
				'Time',TimeStart,...
				'EndTime',[],...
				'RateHistory',[]);			
		EqHash.set(num2str(Count),EqObj);		
	end
	
	
	%Calc Rates BG
	BGArea=1; %Set area to 1 for now
	BGMagBin=(MiniMag+0.05:0.1:MaxiMag-0.05)';
	BGRates=(10.^((BGaval./365)-BGbval*BGMagBin))/(BGArea)*Scaler;
	BGRatesSum=sum(BGRates);
	
	
	%Current Rate
	CurRate=BGRates;
	CurTime=TimeStart;
	RatesTime=[];
	RateRatioTime=[];
	SumRateRatioTime=[];
	ProbEMS=[];
	RatioProbEMS=[];
	OverRatesTime=[];
	
	%TimeVector
	if LogTimeMode
		if TimeStart==0
			TimeVec=10.^(0:TimeStep:log10(TimeEnd))';
		else
			TimeVec=10.^(log10(TimeStart):TimeStep:log10(TimeEnd))';
		end	
	else
		TimeVec=(TimeStart:TimeStep:TimeEnd)';
	end
	
	
	 
	%Hazard for BG
	BGRateVec=[0,0,BGRates'];
	HazCo=HazardConfig;
	HazCo.HazCastTime=TimeStep;
	[scgx,scgy,BGPEMS,BGlogPEMS] = calcSwissHazGrid(BGRateVec,HazCo);
	
	
	
	%Simulate
	MagBinner=(MiniMag:0.1:MaxiMag)';
	HazCo=HazardConfig;
	for i=1:numel(TimeVec)
		disp(['Time Step: ', num2str(TimeVec(i)), ' of ', num2str(TimeEnd)]);
		%I wish matlab would have a real hash, a for-each commando would be great
		
		%generate Eq from Current Rates
		NewEq=poissrnd(CurRate./CastArea);
		notZero=find(NewEq~=0);
		%disp(notZero)
		for j=notZero' %The ' is important
			Count=Count+1;	
			EqCatalog(Count,:)=[TimeVec(i),BGMagBin(j)];
			EqObj=struct(	'Mag',BGMagBin(j),...
					'Time',TimeVec(i),...
					'EndTime',[],...
					'RateHistory',[]);		
			EqHash.set(num2str(Count),EqObj);
		end
		
		%Get the keys
		CurKeys=EqHash.getKeys;
		MagObjBJ=[];
		MagObj=[];
		disp(CurKeys)
		totKey=numel(CurKeys);
		
		if ~isempty(CurKeys)
			for j=1:numel(CurKeys)
				EqObj=EqHash.lookup(CurKeys{j});
				if EqObj.Time<TimeVec(i)
					for k=1:numel(MagBinner)-1
						BinRates(k,:)=calc_RJ(EqObj.Mag,MagBinner(k),MagBinner(k+1), avalue,bvalue,pvalue,...
							cvalue,TimeVec(i)-TimeStart,EqObj.Time);
					end
					%convert to daily rates
					BinRates(:,1)=BinRates(:,1)./TimeVec(i);
					BinRates(isnan(BinRates))=0;
					EqObj.RateHistory(i,:)=BinRates(:,1)';
					
					%only difference to BG is wanted for later on
					%MagObjBJ(j,:)=BinRates(:,1)'-BGRates';
					MagObjBJ(j,:)=BinRates(:,1)';
					MagObjBJ(j,MagObjBJ(j,:)<0)=0; 
					MagObj(j,:)=BinRates(:,1)';
					
					if sum(BinRates(:,1)')<=BGRatesSum
						%Eq is dead ;-), but had a last supper
						EqObj.EndTime=TimeVec(i);
						EqGrave.set(CurKeys{j},EqObj);
						EqHash.remove(CurKeys{j});
					else
						EqHash.set(CurKeys{j},EqObj);
					end
				else
					MagObjBJ(j,:)=zeros(size(BGRates'));
					MagObj(j,:)=ones(size(BGRates'));
				end
			end
		else
			%No rates so use 0 and 1
			MagObjBJ(1,:)=BGRates';
			MagObj(1,:)=ones(size(BGRates'));
		
		end
		%Now for each timestep update rates and keep track of the rates;
		%disp(size(sum(MagObjBJ,1)))
		%disp(size(BGRates))
		%CurRate=BGRates+sum(MagObjBJ,1)';
		if all(MagObjBJ==0)
			MagObjBJ(1,:)=BGRates';
			MagObj(1,:)=ones(size(BGRates'));
		end
		CurRate=sum(MagObjBJ,1)';
		RatesTime(:,i)=CurRate;
		OverRatesTime(i)=sum(CurRate);
		RateRatioTime(:,i)=sum(MagObj,1)'./BGRates;
		SumRateRatioTime(i)=sum(sum(MagObj,1)')./BGRatesSum;
		
		%Calc EMR
		RateVec=[0,0,CurRate'];
		HazCo.HazCastTime=TimeStep; %Leave it at the moment but needs change for log
		[scgx,scgy,PEMS,logPEMS] = calcSwissHazGrid(RateVec,HazCo);
		ProbEMS(i)=PEMS;
		RatioProbEMS(i)=PEMS/BGPEMS;

	end

	%Raise the dead ;-)
	EqHashComb=EqGrave;
	CurKeys=EqHash.getKeys;
	for j=1:numel(CurKeys)
		EqObj=EqHash.lookup(CurKeys{j});
		EqHashComb.set(CurKeys{j},EqObj);
	end

	
	
	%Pack it
	Results.TimeVec=TimeVec;
	Results.EqHashComb=EqHashComb;
	Results.EqHash=EqHash; %In case the calculation should be continued where it left
	Results.EqCatalog=EqCatalog;
	Results.RatesTime=RatesTime;
	Results.OverRatesTime=OverRatesTime;
	Results.RateRatioTime=RateRatioTime;
	Results.SumRateRatioTime=SumRateRatioTime;
	Results.ProbEMS=ProbEMS;
	%Results.ForecastRatesBG=ForecastRatesBG;
	%Results.CumRateBG=CumRateBG;
	%Results.CumMag=CumMag;
	Results.RatioProbEMS=RatioProbEMS;
	Results.BG_EMS=BGPEMS;
	Results.BGRates=BGRates;
	Results.BGRatesSum=BGRatesSum;
	
	%Results.PETime=PETime;
	%Results.LogPETime=LogPETime;
	Results.PETimeBG=repmat(BGPEMS,1,numel(TimeVec));
	Results.BGTime=repmat(BGRatesSum,1,numel(TimeVec));
	
	%Plot it 
	if PlotIt
		%Eq Time vs. Mag
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=stem(EqCatalog(:,1),EqCatalog(:,2));
		set(hand,'LineWidth',2);
		%hold on
		%hand2=plot(TimeVec(2:end),repmat(BGRatesSum,1,numel(ForecastRates(2:end,1))));
		%set(hand2,'LineWidth',2,'LineStyle','--','Color',[0.7 0.7 0.7]);
		%hold off
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Magnitude ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model: Simulated Earthquakes ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		%Rate vs. Time
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(1:end),OverRatesTime(1:end));
		set(hand,'LineWidth',3);
		hold on
		hand2=plot(TimeVec(1:end),Results.BGTime);
		set(hand2,'LineWidth',2,'LineStyle','--','Color',[0.7 0.7 0.7]);
		hold off
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Cum. Rate [1/d] ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Rate vs. Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		%RateRatio vs. Time
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(1:end),SumRateRatioTime(1:end));
		set(hand,'LineWidth',3,'Color','r');
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Rate/BG.Rate  ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Rate/BG.Rate vs. Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		%EMSprob vs. Time
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(1:end),ProbEMS(1:end));
		set(hand,'LineWidth',3,'Color','r');
		hold on
		hand2=plot(TimeVec(1:end),Results.PETimeBG);
		set(hand2,'LineWidth',2,'LineStyle','--','Color',[0.7 0.7 0.7]);
		hold off
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Cum.Rate/BG.Rate  ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Probabilty EMS vs. Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		%EMSprobRatio vs. Time
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(1:end),RatioProbEMS(1:end));
		set(hand,'LineWidth',3,'Color','r');
		xlab=xlabel('EMS  ');
		ylab=ylabel('Probability ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Probabilty EMS/BG.EMS vs. Time ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		%ylim([0 1]);
		%xlim([1 12]);
		
		
	end
	



end

function [RJa] = RJafromGRa(GRa,tStart,tEnd,c,p,b,M0)
	%to modify
	import mapseis.step.*;
	[RJint] = RJintegral(tStart,tEnd,c,-p);

	RJa = GRa - b*M0-log10(RJint);


end

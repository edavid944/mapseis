function Results=FirstStepBasel(EqMag,BasicConfig,ReasJonConfig,TrafficSet,BGConfig,HazardConfig,PlotIt)
	%Reasenberg-Jones Model for one earthquake in Basel 	

	
	import mapseis.step.*;
	import mapseis.plots.*;
	
	
	if isempty(BasicConfig)
		BasicConfig =	struct(	'TimeStart',0,...
					'TimeStep',1/48,...
					'TimeEnd',3650,...
					'LogTimeMode',true);
	end
	
	if isempty(ReasJonConfig)
		%Default settings
		ReasJonConfig =	struct(	'MiniMag',5.1,...
					'MaxiMag',7	,...
					'avalue',-1.69,...
					'bvalue',1,...
					'pvalue',0.91,...
					'cvalue',0.01,...
					'ForecastTime',0,...
					'ForecastStart',0);
					
	end
	
	if isempty(BGConfig)
		BGConfig =	struct(	'BGaval',2.91,...
					'BGbval',0.9);
	end
	
	if isempty(HazardConfig)
		HazardConfig =	struct(	'EMSLevel',5,...
					'MaxMagHaz',5.15,...
					'MinMagHaz',6.95,...
					'HazCastTime',1,...
					'LowRateCut',0);
	end
	
	if isempty(TrafficSet)
		TrafficSet =	struct(	'GreenRate',0,...
					'YellowRate',0,...
					'RedRate',0);
	end
	
	
	%Unpack parameters
	TimeStart=BasicConfig.TimeStart;
	TimeStep=BasicConfig.TimeStep;
	TimeEnd=BasicConfig.TimeEnd;
	LogTimeMode=BasicConfig.LogTimeMode;
	
	MiniMag=ReasJonConfig.MiniMag;
	MaxiMag=ReasJonConfig.MaxiMag;
	avalue=ReasJonConfig.avalue;
	bvalue=ReasJonConfig.bvalue;
	pvalue=ReasJonConfig.pvalue;
	cvalue=ReasJonConfig.cvalue;
	ForecastTime=ReasJonConfig.ForecastTime;
	ForecastStart=ReasJonConfig.ForecastStart;

	BGaval=BGConfig.BGaval;
	BGbval=BGConfig.BGbval;
	
	LowRateCut=HazardConfig.LowRateCut;	
	GreenRate=TrafficSet.GreenRate;
	YellowRate=TrafficSet.YellowRate;
	RedRate=TrafficSet.RedRate;

	
	
	%TimeVector
	if LogTimeMode
		if TimeStart==0
			TimeVec=10.^(0:TimeStep:log10(TimeEnd))';
		else
			TimeVec=10.^(log10(TimeStart):TimeStep:log10(TimeEnd))';
		end	
	else
		TimeVec=(TimeStart:TimeStep:TimeEnd)';
	end
	%Forecasted Rates
	for i=1:numel(TimeVec)
		ForecastRates(i,:)=calc_RJ(EqMag,MiniMag,MaxiMag, avalue,bvalue,pvalue,cvalue,TimeVec(i)-TimeStart,ForecastStart);
	
	end
		
	%convert to daily rates
	ForecastRates(:,1)=ForecastRates(:,1)./TimeVec;
	ForecastRates(isnan(ForecastRates))=0;
	%RawDiff=[0;ForecastRates(:,1)];
	%CumRate=diff(RawDiff);
	RawDiff=[ForecastRates(:,1)];
	CumRate=cumsum(RawDiff);
	
	
	%experiment
	MagBinner=(MiniMag:0.1:MaxiMag)';
	for i=1:numel(TimeVec)
		for j=1:numel(MagBinner)-1
			BinRates(j,:)=calc_RJ(EqMag,MagBinner(j),MagBinner(j+1), avalue,bvalue,pvalue,cvalue,TimeVec(i)-TimeStart,ForecastStart);
		end
		%convert to daily rates
		BinRates(:,1)=BinRates(:,1)./TimeVec(i);
		BinRates(isnan(BinRates))=0;
		RatesMag{i}=BinRates;
		MagTime(i,:)=BinRates(:,1)';
	end
	
	
	%Background Rate
	%Area
	BGArea=1; %Set it to 1 for now
	
	%Calc Rates BG
	BGMagBin=(MiniMag+0.05:0.1:MaxiMag-0.05)';
	BGRates=(10.^((BGaval./365)-BGbval*BGMagBin))/(BGArea);
	BGRatesSum=sum(BGRates);

	%Ratio above BGRates
	MagTimewBG=MagTime./repmat(BGRates',numel(TimeVec),1);
	ForecastRatesBG=ForecastRates;
	ForecastRatesBG(:,1)=ForecastRatesBG(:,1)./BGRatesSum;
	%ForecastRatesBG(ForecastRatesBG(:,1)<=0,1)=BGRatesSum;
	
	%RawDiffBG=[BGRatesSum(1);ForecastRatesBG(:,1)];
	%CumRateBG=diff(RawDiffBG);
	RawDiffBG=[ForecastRatesBG(:,1)];
	CumRateBG=cumsum(RawDiffBG);
	
	%RawMagDiff=[MagTime(1,:);MagTime];
	%CumMag=diff(RawMagDiff);
	RawMagDiff=[MagTime];
	CumMag=cumsum(RawMagDiff);
	
	%Hazard for the Forecast Hazard time 
	%BG
	BGRateVec=[0,0,BGRates'];
	
	%Forecast
	timste=find(TimeVec==HazardConfig.HazCastTime);
	if isempty(timste)
		timste=numel(TimeVec);
	end	
	
	RateVec=[0,0,MagTime(timste,:)];
	HazCo=HazardConfig;
	
	%should not go higher
	EMSVec=0:0.5:12;
	
	%BG
	for i=1:numel(EMSVec)
		HazCo.EMSLevel=EMSVec(i);
		[scgx,scgy,PEMS_BG,logPEMS_BG] = calcSwissHazGrid(BGRateVec,HazCo);
		
		if PEMS_BG<=LowRateCut
			break;
		else
			BG_PEMS(i)=PEMS_BG;
			BG_LogPEMS(i)=logPEMS_BG;
			BG_EMS(i)=EMSVec(i);
		end
	end
	
	%ForeCast
	for i=1:numel(EMSVec)
		HazCo.EMSLevel=EMSVec(i);
		[scgx,scgy,PEMS,logPEMS] = calcSwissHazGrid(RateVec,HazCo);
		
		if PEMS<=LowRateCut
			disp('beep')
			break;
		else
			ForPEMS(i)=PEMS;
			ForLogPEMS(i)=logPEMS;
			ForEMS(i)=EMSVec(i);
		end
	end
	
	%Now EMS with time
	HazCo=HazardConfig;
	HazCo.HazCastTime=TimeStep;
	for i=1:numel(TimeVec)
		RateVec=[0,0,MagTime(i,:)];
		[scgx,scgy,PEMS,logPEMS] = calcSwissHazGrid(RateVec,HazCo);
		PETime(i)=PEMS;
		LogPETime(i)=logPEMS;
		
	end
	
	%Now EMS with time BG
	HazCo=HazardConfig;
	HazCo.HazCastTime=TimeStep;
	
	RateVec=[0,0,BGRates'];
	[scgx,scgy,PEMS,logPEMS] = calcSwissHazGrid(RateVec,HazCo);
	PETimeBG=PEMS;
	LogPETimeBG=logPEMS;
		
	
	
	%[scgx,scgy,PEGS_BG,logPEGS_BG] = calcSwissHazGrid(BGRateVec,HazardConfig)
	
	%Pack it
	Results.TimeVec=TimeVec;
	Results.ForeRates=ForecastRates;
	Results.CumRate=CumRate;
	Results.RatesMag=RatesMag;
	Results.MagTime=MagTime;
	Results.BGRates=BGRates;
	Results.BGRatesSum=BGRatesSum;
	Results.MagTimewBG=MagTimewBG;
	Results.ForecastRatesBG=ForecastRatesBG;
	Results.CumRateBG=CumRateBG;
	Results.CumMag=CumMag;
	Results.BG_PEMS=BG_PEMS;
	Results.BG_LogPEMS=BG_LogPEMS;
	Results.BG_EMS=BG_EMS;
	Results.ForPEMS=ForPEMS;
	Results.ForLogPEMS=ForLogPEMS;
	Results.ForEMS=ForEMS;
	Results.PETime=PETime;
	Results.LogPETime=LogPETime;
	Results.PETimeBG=repmat(PETimeBG,1,numel(TimeVec));
	Results.LogPETimeBG=repmat(LogPETimeBG,1,numel(TimeVec));
	
	%Plot it 
	if PlotIt
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(2:end),ForecastRates(2:end,1));
		set(hand,'LineWidth',3);
		hold on
		hand2=plot(TimeVec(2:end),repmat(BGRatesSum,1,numel(ForecastRates(2:end,1))));
		set(hand2,'LineWidth',2,'LineStyle','--','Color',[0.7 0.7 0.7]);
		hold off
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Rate [1/d]');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Rate-Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(1:end),CumRate(1:end));
		set(hand,'LineWidth',3);
		hold on
		hand2=plot(TimeVec(1:end),repmat(BGRatesSum,1,numel(CumRate(1:end))));
		set(hand2,'LineWidth',2,'LineStyle','--','Color',[0.7 0.7 0.7]);
		hold off
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Cum. Rate [1/d] ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': cum. Rate-Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(2:end),ForecastRatesBG(2:end,1));
		set(hand,'LineWidth',3,'Color','r');
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Rate/BG.Rate  ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Rate/BG.Rate-Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(TimeVec(1:end),CumRateBG(1:end));
		set(hand,'LineWidth',3,'Color','r');
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Cum.Rate/BG.Rate  ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Cum.Rate/BG.Rate-Time  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand=plot(Results.ForEMS,Results.ForPEMS)
		set(hand,'LineWidth',3,'Color','r');
		hold on
		hand2=plot(Results.BG_EMS,Results.BG_PEMS);
		set(hand2,'LineWidth',3,'Color',[0.7 0.7 0.7]);
		hold off
		xlab=xlabel('EMS  ');
		ylab=ylabel('Probability ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold','YScale','log');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Probabilty EMS, day=',...
			num2str(HazardConfig.HazCastTime),'  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		ylim([0 1]);
		xlim([1 12]);
		
		figure
		set(gcf,'Pos',[680   656   720   567]);
		hand= plot(Results.TimeVec(2:end),Results.PETime(2:end));
		set(hand,'LineWidth',3,'Color','r');
		hold on
		hand2=plot(Results.TimeVec(2:end),Results.PETimeBG(2:end));
		set(hand2,'LineWidth',2,'LineStyle','--','Color',[0.7 0.7 0.7]);
		hold off
		xlab=xlabel('Time [d]  ');
		ylab=ylabel('Probability ');
		set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold','YScale','log');
		set([xlab ylab],'FontSize',16,'FontWeight','bold');
		tit=title(['R-J Model Mainshock= ',num2str(EqMag),': Probabilty EMS=',...
			num2str(HazardConfig.EMSLevel),'  ']);
		set(tit,'FontSize',18,'FontWeight','bold');
		%ylim([0 1]);
		%xlim([1 12]);
		
	end
	



end

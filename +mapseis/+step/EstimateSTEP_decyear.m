function [OutStruct] = EstimateSTEP_decyear(Datastore,Filterlist,MinMainMag,Options)
	%Uses a declustered Datastore and tries to determine the Reasenberg-Jones
	%parameters as used by the generic model of STEP.
	%The Filterlist should contain the Testregion and the learning Time intervall
	%MinMainMag defines what is considered to be a mainshock, only clusters with
	%a mainshock magnitude higher than MinMainMag will be analyzed.
	
	%decyear version
	
	import mapseis.step.*;
	import mapseis.projector.*;
	
	%check if declustering data is available
	if isempty(Datastore.ClusterNR)
		error('No Declustering Data available')
	end
	
	if nargin<4
		Options=[];
	end
	
	if isempty(Options)
		%default options
		Options = struct('RangeP',[[0.8, 2]],...
			  	 'RangeC',[[0 1]],...
			  	 'RangeK',[[-5 0]],...
			  	 'OnlySensible',true,...
			  	 'UseMaxTime',false,...
			  	 'MaxTime',50);
		
	end
	
	
	%get clusters
	ClusterNr=Datastore.ClusterNR;
	EventType=Datastore.EventType;
	
	
	%get available Earthquakes
	Filterlist.changeData(Datastore);
	Filterlist.updateNoEvent;
	selected=Filterlist.getSelected;
	Filterlist.PackIt;
	eventStruct=Datastore.getFields;
	DecYear=Datastore.getUserData('DecYear');
	
	%check if anything is init anyway
	if sum(selected)==0
		error('No Earthquake is in the selection of the filterlist')
	end
	
	%filter clusters
	FiltClustNr=ClusterNr(selected);
	FiltEventType=EventType(selected);
	
	
	%get suitable mainshock
	MagMainSelect=eventStruct.mag>=MinMainMag;
	isMain=EventType==2;
	isAftershock=EventType==3;
	MainCandidate=unique(ClusterNr(isMain&MagMainSelect&selected));
	
	%do still have something left?
	if isempty(MainCandidate)
		error('No Mainshock fit the selection criterias');
	end	
	
	%for sensible test
	RangeP=Options.RangeP;
	RangeC=Options.RangeC;
	RangeK=Options.RangeK;
	
	%maximum time
	UseMaxTime = Options.UseMaxTime;
	MaxTime = Options.MaxTime;
	
	%prepare raw data
	k_values=NaN(size(MainCandidate));
	p_values=NaN(size(MainCandidate));
	c_values=NaN(size(MainCandidate));
	NrAftershocks=NaN(size(MainCandidate));
	MainID=NaN(size(MainCandidate));
	MainTime=NaN(size(MainCandidate));
	MainMag=NaN(size(MainCandidate));
	isSensible=false(size(MainCandidate));
	
	
	%loop over all clusters 
	for i=1:numel(MainCandidate)
		
		%get mainshock
		MainID(i)=MainCandidate(i);
		MshockSel=(MainCandidate(i)==ClusterNr)&isMain;
		MainShock=getZMAPFormat(Datastore,MshockSel);
		MainDateNum=eventStruct.dateNum(MshockSel);
		
		if isempty(MainShock)
			disp(['No suitable MainShock in ClusterID ',num2str(MainCandidate(i))]);
			continue;
		end
		
		
		MainTime(i)=MainShock(1,3);
		MainMag(i)=MainShock(1,6);
		
		%get aftershocks
		inTime=DecYear>=MainTime(i);
		AfShockSel=(MainCandidate(i)==ClusterNr)&selected&inTime&isAftershock;
		AShocks=getZMAPFormat(Datastore,AfShockSel);
		ADateNum=eventStruct.dateNum(AfShockSel);
		NrAftershocks(i)=sum(AfShockSel);
		
		if isempty(AShocks)
			disp(['No suitable Aftershocks found for ClusterID ',num2str(MainCandidate(i))]);
			continue;
		end
	
		%I do not exacalty the times should be selected, but for now use mainshock as 
		%start time and last aftershock as end time
		StartTime=0;%MainTime(i);
		EndTime=ADateNum(end)-MainDateNum;
		
		if UseMaxTime&(EndTime-StartTime)>MaxTime
			EndTime=StartTime+MaxTime;
		end
		
		
		
		[k,c,p] = MaxLikeOmori_decyear(StartTime,EndTime,AShocks,MainShock)
		
		k_values(i)=k;
		p_values(i)=p;
		c_values(i)=c;
		
		isSensible(i) = k_values(i)>=RangeK(1)&k_values(i)<=RangeK(2)&...
				p_values(i)>=RangeP(1)&p_values(i)<=RangeP(2)&...
				c_values(i)>=RangeC(1)&c_values(i)<=RangeC(2);
		
		
		
	end
	
	
	
	%calculate stacked parameters
	if Options.OnlySensible
		mean_k=nanmean(k_values(isSensible));
		mean_p=nanmean(p_values(isSensible));
		mean_c=nanmean(c_values(isSensible));
	else
		mean_k=nanmean(k_values);
		mean_p=nanmean(p_values);
		mean_c=nanmean(c_values);
	
	end
	
	%pack it 
	OutStruct = struct(	'k',mean_k,...
				'p',mean_p,...
				'c',mean_c,...
				'Single_k_values',k_values,...
				'Single_p_values',p_values,...
				'Single_c_values',c_values,...
				'MainID',MainID,...
				'NrAftershocks',NrAftershocks,...
				'MainTime',MainTime,...
				'MainMag',MainMag,...
				'isSensible',isSensible);
	

	
	
	
	

end

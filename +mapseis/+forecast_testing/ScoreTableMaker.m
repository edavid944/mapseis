function OutStruct = ScoreTableMaker(TestObjects,TestName,TestSeriesName,...
			ModelName,PrintModName,ExpNumField,ObservedNum,FileName,FullMode,PercMod)
	
	%A script to make a table with the scores of different consistency tests for a 
	%multitude of different test results.

	%ExpNumField has to be a Single Trank test for the total expect number if left
	%empty the total expected number will not be written.
	
	%Full Mode will add also total tests passed to the table
	
	%use \usepackage[dvips]{color} and
	% \definecolor{Gray}{rgb}{0.7,0.7,0.7}
	% \definecolor{White}{rgb}{1,1,1}
	%for the colors in latex
	
	%latexMode currently always on
	
	LatexMode=true;
	
	%PercentGain is currently not used, but it may change later so it collect it anyway
	
	%build data first
	NameEntry={};
	TestEntry={};
	ModelEntry={};
	InfoGain=[];
	PercentGain=[];
	ObsEq=[];
	formatSpec = '%.2f';
	formatSpecInt = '%.0f';
	
	if isempty(PrintModName)
		PrintModName=ModelName;
	end
	
	
	if ~iscell(TestObjects)
		TestObjects={TestObjects};
		TestSeriesName={TestSeriesName};
	end
	
	if ~iscell(TestName)
		TestName={TestName};
	end
	
	
	%check what test have to be found
	Test2Extract={};
	TestField={};
	PrintTestName={}
	TestSym={}
	OneSide=[];
	TotalPass=[];
	TotalCast=[];
	PercPass=[];
	
	if ~isempty(ExpNumField)
		
		Test2Extract{end+1}=ExpNumField;
		TestField{end+1}='ExpVal';
		PrintTestName{end+1}='Exp. Nr.';
		TestSym{end+1}=' ';
		OneSide(end+1)=NaN;
		
	end
	
	
	for tC=1:numel(TestName)
	
		posPlots=TestObjects{1}.possiblePlots.(TestName{tC})(:,2);
		posRank=find(strcmp(posPlots,'LikeHist'));
		confStruct=TestObjects{1}.possiblePlots.(TestName{tC}){posRank(1),3};
		
		if confStruct.OneSide
			Test2Extract(end+1)=TestName(tC);
			TestField{end+1}=confStruct.ScoreField_low;
			PrintTestName{end+1}=confStruct.TestName;
			Sym=confStruct.TestSymbols_low{1};
			Sym=Sym(Sym~='$');
			Sym=Sym(Sym~='=');
			TestSym{end+1}=Sym;
			OneSide(end+1)=true;
		else
			Test2Extract(end+1)=TestName(tC);
			TestField{end+1}=confStruct.ScoreField_low;
			PrintTestName{end+1}=confStruct.TestName;
			Sym=confStruct.TestSymbols_low{1};
			Sym=Sym(Sym~='$');
			Sym=Sym(Sym~='=');
			TestSym{end+1}=Sym;
			OneSide(end+1)=false;
			
			Test2Extract(end+1)=TestName(tC);
			TestField{end+1}=confStruct.ScoreField_hi;
			PrintTestName{end+1}=confStruct.TestName;
			Sym=confStruct.TestSymbols_hi{1};
			Sym=Sym(Sym~='$');
			Sym=Sym(Sym~='=');
			TestSym{end+1}=Sym;
			OneSide(end+1)=false;
			
			
		end
	end
	
	
	%get values
	ScoreMat=[];
	PassMat=[];
	
	
	for bC=1:numel(TestObjects)
		
	
		for mC=1:numel(ModelName)
			ValList=NaN(1,numel(Test2Extract));
			PassList=false(1,numel(Test2Extract));
			TotPassList=NaN(1,numel(Test2Extract));
			TotCastList=NaN(1,numel(Test2Extract));
			PercList=NaN(1,numel(Test2Extract));
			
			for tC=1:numel(Test2Extract)
				if isfield(TestObjects{bC}.SummaryResults,Test2Extract{tC})
					if isfield(TestObjects{bC}.SummaryResults.(Test2Extract{tC}),ModelName{mC})
						ValList(tC)=TestObjects{bC}.SummaryResults.(Test2Extract{tC}).(ModelName{mC}).(TestField{tC});
						
						if ~isnan(OneSide(tC))
							PassList(tC)=TestObjects{bC}.SummaryResults.(Test2Extract{tC}).(ModelName{mC}).Passed
							TotPassList(tC)=TestObjects{bC}.SummaryResults.(Test2Extract{tC}).(ModelName{mC}).NumPassed;
							TotCastList(tC)=TestObjects{bC}.SummaryResults.(Test2Extract{tC}).(ModelName{mC}).TotalNumTest;
							
							PercList(tC)=TotPassList(tC)/TotCastList(tC)*100;
						else
							PassList(tC)=true;	
							
						end
						
					end
				end
			end
			
			if any(~isnan(ValList))
				NameEntry{end+1}=[TestSeriesName{bC},': ', PrintModName{mC}];
				TestEntry{end+1}=[TestSeriesName{bC},': '];
				ModelEntry{end+1}=[PrintModName{mC},' '];
				ScoreMat(end+1,:)=ValList;
				PassMat(end+1,:)=PassList;
				TotalPass(end+1,:)=TotPassList;
				TotalCast(end+1,:)=TotCastList;
				PercPass(end+1,:)=PercList;
				
			end
		end
	end
	%May have to change that structure again if I decide for another table format.
	
		
	%write the file
	
	%open file
	fid = fopen(FileName,'w'); 
	
	
	if LatexMode
		HeaderLine='       ';
		HeaderLineLatex1='\hline  Model';
		HeaderLineLatex2=' ';
		TabCode='c';
		for i=1:numel(PrintTestName);
			HeaderLine=[HeaderLine,'    ',PrintTestName{i}];
			HeaderLineLatex1=[HeaderLineLatex1,' & ',PrintTestName{i}];
			HeaderLineLatex2=[HeaderLineLatex2,' & ','$',TestSym{i},'$'];
			TabCode=[TabCode,'|c'];
		end
		
		HeaderLineLatex1 = [HeaderLineLatex1, '   \\'];	
		HeaderLineLatex2 = [HeaderLineLatex2, '   \\'];	
	
		TabWide=num2str(numel(PrintTestName)+1);
			
		
	
		
		%write header
		fprintf(fid,'%s','\begin{table}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{tabular}{|',TabCode,'|}');
		fprintf(fid,'\n');
		
		if isempty(ObservedNum)
			fprintf(fid,'%s','\multicolumn{',TabWide,'}{c}{Consistency Test Results} \\');
			fprintf(fid,'\n');
		else
			fprintf(fid,'%s','\multicolumn{',TabWide,'}{c}{Consistency Test Results, Observed Eq = ',num2str(ObservedNum),'} \\');
			fprintf(fid,'\n');
		end
		
		
		fprintf(fid,'%s',HeaderLineLatex1); 
		fprintf(fid,'\n');
		fprintf(fid,'%s',HeaderLineLatex2); 
		fprintf(fid,'\n');
		
	
	else
		%later
	
	
	end
	
	
		
	
	
	
	%write it 
	for lC=1:numel(NameEntry)
		%build line	
		if FullMode
			TheLine = ['\hline  ',TestEntry{lC}];
		else
			TheLine = ['\hline  ',NameEntry{lC}];
		end		
		
		
		for tC=1:numel(PrintTestName)
			if ~isnan(ScoreMat(lC,tC))
				if PassMat(lC,tC)
					TheLine = [TheLine,' & ',num2str(ScoreMat(lC,tC),formatSpec)];
				else
					TheLine = [TheLine,' & \fcolorbox{Gray}{Gray}{',num2str(ScoreMat(lC,tC),formatSpec),'}'];
				end
			else
				TheLine = [TheLine,' & ',' '];
			end
				
		end
				
		%end line
		TheLine = [TheLine,' \\'];
	
		%write line
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
		
		if FullMode
			if PercMod
				%print the additional lines
				TheLineX1 = ['   ',ModelEntry{lC}];
				TheLineX2 = ['    '];
				for tC=1:numel(PrintTestName)
					if ~isnan(TotalPass(lC,tC))
					
						TheLineX1 = [TheLineX1,' & \textbf{P:} ',num2str(PercPass(lC,tC),formatSpec),' $\%$ '];
						TheLineX2 = [TheLineX2,' & \textbf{T:} ',num2str(TotalCast(lC,tC),formatSpecInt)];
					else
						TheLineX1 = [TheLineX1,' & ',' '];
						TheLineX2 = [TheLineX2,' & ',' '];
					end
					
				end
				
				%end line
				TheLineX1 = [TheLineX1,' \\'];
				TheLineX2 = [TheLineX2,' \\'];
				%write line
				fprintf(fid,'%s',TheLineX1); 
				fprintf(fid,'\n');
				%fprintf(fid,'%s',TheLineX2); 
				%fprintf(fid,'\n');
				
				
			else
				%print the additional lines
				TheLineX1 = ['   ',ModelEntry{lC}];
				TheLineX2 = ['    '];
				for tC=1:numel(PrintTestName)
					if ~isnan(TotalPass(lC,tC))
					
						TheLineX1 = [TheLineX1,' & \textbf{P:} ',num2str(TotalPass(lC,tC),formatSpecInt)];
						TheLineX2 = [TheLineX2,' & \textbf{T:} ',num2str(TotalCast(lC,tC),formatSpecInt)];
					else
						TheLineX1 = [TheLineX1,' & ',' '];
						TheLineX2 = [TheLineX2,' & ',' '];
					end
					
				end
				
				%end line
				TheLineX1 = [TheLineX1,' \\'];
				TheLineX2 = [TheLineX2,' \\'];
				%write line
				fprintf(fid,'%s',TheLineX1); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',TheLineX2); 
				fprintf(fid,'\n');
			
			end
			
			
		end
	end
	
		
	%Finalize Table	
	fprintf(fid,'%s','\hline');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{tabular}');
	fprintf(fid,'\n');
	
	if FullMode
		if PercMod
			fprintf(fid,'%s',['\caption{The score of the different consistency tests.',...
					' The failed tests are marked in gray. \textbf{P} stands for ',...
					'Passed and gives the percentage of passed single tests}']);
		else
			fprintf(fid,'%s',['\caption{The score of the different consistency tests.',...
					' The failed tests are marked in gray. \textbf{P} stands for ',...
					'Passed and gives the number of passed single tests, and \textbf{T} ',...
					'stands for total and gives the total number of tests calculated.}']);
		end		
	else
		fprintf(fid,'%s',['\caption{The score of the different consistency tests.',...
				' The failed tests are marked in gray.}']);
	end
	
	fprintf(fid,'\n');
	fprintf(fid,'%s','\label{tab:ADD A LABEL}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{center}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{table}');
	fprintf(fid,'\n');
 
	
	%close file
	fclose(fid); 
	
	
	
	%build outstruct
	OutStruct = 	struct(	'NameEntry',{NameEntry},...
				'ScoreMat',[ScoreMat],...
				'PassMat',[PassMat],...
				'TotalPass',[TotalPass],...
				'TotalCast',[TotalCast],...
				'PrintTestName',{PrintTestName});
	
	
	
end

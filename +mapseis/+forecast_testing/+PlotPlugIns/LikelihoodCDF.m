classdef LikelihoodCDF < mapseis.forecast_testing.TestPlotObj
	%A classic CDF based likelihood plot
	
	properties
		%PlotName %Name of the plot, not used as title.
        	%Version %Has to be a STRING, used to describe the version of the code
        	%PlotInfo %The place for a description about the plot 
        	
        	%PlotSelect %can be used if the plot uses more than on axis per test (see signMatPlot as example)		
        	%PlotAxis	%Axis used for the plots
        	%PlotHandles	%Handles of the different plot parts (optional, but suggested)
        	
        	%TestReady
        	%CPUTimeData
        	
        	%PlotReady signalizes that the plot object is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it is just the time
        	%needed for plotting
        	
        	TestResults
        	TestConfig
        	UserConfig
        	LegendEntries
        	
        end
    

       
        methods 
        
        	function obj = LikelihoodCDF()
   		
        		obj.PlotName='Likelhood_CDF';
              		obj.Version='v1';
              		
              		obj.PlotInfo=['Classic CDF plot for likelihood based tests ',...
              				'like the L-test'];
              				
              		obj.PlotSelect = 1 
              		obj.PlotAxis = [];	
              		obj.PlotHandles	= [];
        	
              		obj.PlotReady = false;
              		obj.CPUTimeData	=[];	
              		
              		obj.TestResults=[];
              		obj.TestConfig=[];
              		
              		%default user config
              		DefaultUseConf=struct(	'CDFColor',[0.88 0.88 0.88],...
              					'orgColor','r',...
              					'meanColor','m',...
              					'medianColor','g',...
              					'ShowText',true,...
              					'plotOrg',true,...
              					'plotMean',false,...
              					'plotMedian',false,...
              					'PaperBoy',true);
              					
              		
              		obj.UserConfig=DefaultUseConf;
              		obj.LegendEntries={};	
              		
              		%COMMENT: currently median and mean are only partly supported as text
              		
        	end
        	
        	
        	function ConfigureTestConfig(obj,TestConfig)
			%Used to configurate the part of the configuration needed from the
			%Test, e.g. Test Name, fields with the values, etc.
			%For each result used a test config will be submitted, by the 
			%plotmaker, means TestConfig will be a cell array with structures
			%for each result (e.g. {conf1, conf2, conf3,....}
			
			if iscell(TestConfig)
				obj.TestConfig=TestConfig{1};
			else
				obj.TestConfig=TestConfig;
			end
			
        	end
        	
        	function ConfigureUserConfig(obj,UserConfig)
			%This is used to set the user configuration, which contain everything
			%not defined by the test, like colors, linewidth, etc. There is only
			%one UserConfig for all results used and some fields will always be 
			%submitted, no matter if the plot function is using it:
			%	PaperBoy:	if true the resulting graphic will be used
			%			in a print, hence lines should be a bit thicker
			%			and fonts a bit larger, but it is up to the plot
			%			function what it should do
			%Don't if there are more	
			obj.UserConfig=UserConfig;
			
        	end
        	
        	
        	function UserConfig = getUserConfig(obj)
        		%should return the current user configuration, mostly needed to get
        		%the default values
        		
        		UserConfig = obj.UserConfig;
        		
        	end	
        	
        	function FeatureList = getFeatures(obj)
			%This will be used by the TestSuite to determine what the model can 
			%be used for. The output should by structure with specific fieldnames
			%with mostly boolean values. 
			%Following keywords are available:
			%	MultiplePlot:		if true, the plot produces more than one
			%				plot for a single test, example sign. matrix
			%				plot function, which plots two plot "per test"
			%	No_Plots:		only needed if MultiplePlot is true, defines
			%				how many plots are produced
			%	Uncertain_mean:		Defines if the plot is able to plot Uncertainties
			%				with a mean value and variance (true if possible)
			%	Uncertain_median:	Defines if the plot is able to plot Uncertainties
			%				with a median Q16 and Q84 (true if possible)
			%	MultiTest:		True if more than one test per plot is allowed.
			%				If MultiTest is supported, TestConfig and Results will
			%				be submitted as cell array no matter if only one result 
			%				is used.
			%	SingleTest:		True if it is possible to plot only one test per
			%				plot
        	
			FeatureList = struct(	'MultiplePlot',false,...
						'No_Plots',1,...
						'Uncertain_mean',true,...
						'Uncertain_median',true,...
						'MultiTest',false,...
						'SingleTest',true);
        	
        	
        	
        	function PossibleParameters = getUserParameterSetting(obj)
			%The method should return a description of the parameters needed
			%in the UserConfig and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be used for ConfigureTest
			%Each entry in structure should be another structure with following
			%Often the Plots do not need any configuration in this case the 
			%PossibleParameters can return empty
			
			
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			the selected string.
			%			
			%			
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			%WILL WRITE THAT LATER
			
		end	
        	
		
        	function SetPlotAxis(obj,plotAxis)
        		%sets the axis used for plotting
        		
        		if isempty(plotAxis)
        			obj.PlotAxis=gca;
        		else
        			obj.PlotAxis=plotAxis;
        		end
        		
        		
        	end
        	
        	
        	
        	function AddTestResults(obj,Results)
			%Used to submit the results of the tests, often more than one test is supported
			%in this case Results should be a cell array of the same size as TestConfig
			%(e.g. {Result1,Result2,Result3,...}
			if iscell(Results)
				obj.TestResults=Results{1};
			else
				obj.TestResults=Results;
			end
			
        	end
        	
        	
        	function PlotResults(obj)
        		%Will be used to start the plotting. The Handles of the different plot parts
        		%(lines,points, etc.) can stored in PlotHandles
        		
        		import mapseis.plot.*;
        		
        		%handle list to be stored
        		handle=[];
        		legendentry={};
        		
        		
        		%Get All the data
        		%................
        		
        		%first get histogram data, original catalog likelihood and original score
        		%(all should be always present
        		HistData=obj.TestResults.(obj.TestConfig.LogSynthCatField);
        		LikeOrgCat=obj.TestResults.(obj.TestConfig.LogCatField);
        		
        		if obj.TestConfig.OneSide
        			QScore=obj.TestResults.(obj.TestConfig.ScoreField_low);
        			
        			%get test symbols
        			orgSym = obj.TestConfig.TestName.TestSymbols_low{1};
        			meanSym = obj.TestConfig.TestName.TestSymbols_low{2};
        			medSym = obj.TestConfig.TestName.TestSymbols_low{3};
        			
        		else
        			QScorelow=obj.TestResults.(obj.TestConfig.ScoreField_low);
        			QScorehi=obj.TestResults.(obj.TestConfig.ScoreField_hi);
        			
        			%get test symbols
        			orgSymlow = obj.TestConfig.TestName.TestSymbols_low{1};
        			meanSymlow = obj.TestConfig.TestName.TestSymbols_low{2};
        			medSymlow = obj.TestConfig.TestName.TestSymbols_low{3};
        			
        			orgSymhi = obj.TestConfig.TestName.TestSymbols_hi{1};
        			meanSymhi = obj.TestConfig.TestName.TestSymbols_hi{2};
        			medSymhi = obj.TestConfig.TestName.TestSymbols_hi{3};
        		end
        		
        		%generate title
        		if ~isstr(obj.TestConfig.ForecastTime)
        			TheTitle= [obj.TestConfig.TestName,': ',obj.TestConfig.ForecastName,', ',...
        					datestr(obj.TestConfig.ForecastTime)];
        		else
        			TheTitle= [obj.TestConfig.TestName,': ',obj.TestConfig.ForecastName,', ',...
        					obj.TestConfig.ForecastTime];
        		
        		end
        		
        		
        		%now the addtional fields in case of uncertianties
        		if obj.TestConfig.Uncertianties
        			       			
        			if obj.TestConfig.MeanAvailable
        				LikeOrgMean=obj.TestResults.(['mean_',obj.TestConfig.LogCatField]);
        				LikeOrgSTD=obj.TestResults.(['std_',obj.TestConfig.LogCatField]);
        				
        				if obj.TestConfig.OneSide
						QScoreMean=obj.TestResults.(['mean_',obj.TestConfig.ScoreField_low]);
						QScoreSTD=obj.TestResults.(['std_',obj.TestConfig.ScoreField_low]);
						
					else
						QScorelowMean=obj.TestResults.(['mean_',obj.TestConfig.ScoreField_low]);
						QScorelowSTD=obj.TestResults.(['std_',obj.TestConfig.ScoreField_low]);
						QScorehiMean=obj.TestResults.(['mean_',obj.TestConfig.ScoreField_hi]);
						QScorehiSTD=obj.TestResults.(['std_',obj.TestConfig.ScoreField_hi]);
						
					end
        		
        				
        			end
        			
        			
        			if obj.TestConfig.MedianAvailable
        				LikeOrgMedian=obj.TestResults.(['median_',obj.TestConfig.LogCatField]);
        				LikeOrgQuant=obj.TestResults.(['quant68_',obj.TestConfig.LogCatField]);
        				
        				if obj.TestConfig.OneSide
						QScoreMedian=obj.TestResults.(['median_',obj.TestConfig.ScoreField_low]);
						QScoreQuant=obj.TestResults.(['quant68_',obj.TestConfig.ScoreField_low]);
						
					else
						QScorelowMedian=obj.TestResults.(['median_',obj.TestConfig.ScoreField_low]);
						QScorelowQuant=obj.TestResults.(['quant68_',obj.TestConfig.ScoreField_low]);
						QScorehiMedian=obj.TestResults.(['median_',obj.TestConfig.ScoreField_hi]);
						QScorehiQuant=obj.TestResults.(['quant68_',obj.TestConfig.ScoreField_hi]);
						
					end
        			
        			end
        			
        			
        		end
        		
        		%define some plotting parameters
        		%...............................
        		if obj.UserConfig.PaperBoy
        			AxisWidth=3;
        			CDFWidth=5;
        			MainLineWidth=4;
        			ErrorLineWidth=2;
        			TitleFontSize=36;
        			AxisFontSize=16;
        			LabelFontSize=18;
        			WeightAxis='bold';
        			
        			TextSize=12;
        		else
        			AxisWidth=2;
        			CDFWidth=4;
        			MainLineWidth=3;
        			ErrorLineWidth=1;
        			TitleFontSize=28;
        			AxisFontSize=14;
        			LabelFontSize=16;
        			WeightAxis='demi';
        			
        			TextSize=11;
        			
        		end
        		
        		%Text to write, it is decide later if the text is actually wanted
        		TheText={};
        		if obj.TestConfig.OneSide
        			%one-side tests like L-test
				if obj.UserConfig.plotOrg
					TheText(end+1)={[orgSym,num2str(round(1000*QScore)/1000)]};
					
				end
				
				if obj.UserConfig.plotMean&obj.TestConfig.MeanAvailable
					TheText(end+1)={[meanSym,num2str(round(1000*QScoreMean)/1000),' +/- ',...
							num2str(round(1000*QScoreSTD)/1000)]};
				end
				
				if obj.UserConfig.plotMedian&obj.TestConfig.MedianAvailable
					TheText(end+1)={[medianSym,num2str(round(1000*QScoreMedian)/1000),...
							' Q16: ',num2str(round(1000*QScoreQuant(1))/1000),...
							' Q84: ',num2str(round(1000*QScoreQuant(2))/1000)]};
				end
        		
        		else
        			%two-sided test like the N-test, this plot may not be best option for 
        			%tests like that anyway
        			if obj.UserConfig.plotOrg
					TheText(end+1)={[orgSymlow,num2str(round(1000*QScorelow)/1000)]};
					TheText(end+1)={[orgSymhi,num2str(round(1000*QScorehi)/1000)]};
				end
				
				if obj.UserConfig.plotMean&obj.TestConfig.MeanAvailable
					TheText(end+1)={[meanSymlow,num2str(round(1000*QScorelowMean)/1000),' +/- ',...
							num2str(round(1000*QScorelowSTD)/1000)]};
					TheText(end+1)={[meanSymhi,num2str(round(1000*QScorehiMean)/1000),' +/- ',...
							num2str(round(1000*QScorehiSTD)/1000)]};		
				end
				
				if obj.UserConfig.plotMedian&obj.TestConfig.MedianAvailable
					TheText(end+1)={[medianSymlow,num2str(round(1000*QScorelowMedian)/1000),...
							' Q16: ',num2str(round(1000*QScorelowQuant(1))/1000),...
							' Q84: ',num2str(round(1000*QScorelowQuant(2))/1000)]};
					TheText(end+1)={[medianSymhi,num2str(round(1000*QScorehiMedian)/1000),...
							' Q16: ',num2str(round(1000*QScorehiQuant(1))/1000),...
							' Q84: ',num2str(round(1000*QScorehiQuant(2))/1000)]};		
				end
        			
        		
        		
        		end
        		
        		
        		
        		
        		%now go plotting
        		%...............
        		
        		%CDF is always needed so start here
               		handle(1) = plotCDF(HistData,obj.PlotAxis,obj.UserConfig.histColor);
        		set(handle(1),'LineWidth',CDFWidth,'Color',obj.UserConfig.histColor);
        		legendentry{1}='Likelihood model';
        		
			hold on 
			ylimer=ylim;
			xlimer=xlim;
			
			
			%original score
			if obj.TestConfig.OneSide
				handle(end+1)=plot(obj.PlotAxis,[xlimer(1) LikeOrgCat,LikeOrgCat],[QScore,QScore,0],'LineStyle','--',...
				'Color', obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
				legendentry{end+1}='Quantil org. Catalog';
			
			else
				handle(end+1)=plot(obj.PlotAxis,[xlimer(1) LikeOrgCat,LikeOrgCat],[QScorelow,QScorelow,0],'LineStyle','--',...
				'Color', obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
				legendentry{end+1}='Quantil org. Catalog';
				
				handle(end+1)=plot(obj.PlotAxis,[xlimer(1) LikeOrgCat,LikeOrgCat],[QScorehi,QScorehi,0],'LineStyle','--',...
				'Color', obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
				legendentry{end+1}='Quantil org. Catalog';
			end
			
		
			
			if obj.UserConfig.ShowText
				
				tpos=[0.65,0.25];
				
				%write the text
				leText=text(tpos,TheText,'Interpreter','Latex','Units','normalized');
				set(leText,'FontSize',TextSize);
			
			end
			
			
			%and the title
			theTit=title(TheTitle);
			set(theTit,'FontSize',TitleFontSize,'FontWeight','bold','Interpreter','none');
			
			
			
			%axis labels and some cosmetics
			%..............................
			xlab=xlabel(obj.TestConfig.XaxisLabel);
			ylab=ylabel(obj.TestConfig.YaxisLabel);
			set([xlab,ylab],'FontSize',LabelFontSize,'FontWeight','bold');
			set(obj.PlotAxis,'LineWidth',AxisWidth,'FontSize',AxisFontSize,'FontWeight',WeightAxis);
			
			hold off
			
			%store stuff in fields
			obj.LegendEntries=legendentry;
			obj.PlotHandles=handle;
        	
        	
        end

      
        
         %methods 
        	%useful methods 
        %	function PlotHandle = PlotIt(obj,plotAxis,Results,TestConfig,UserConfig)
        		%just and all in one config and plot shortcut, normally it will
        		%be enough to the following two steps to plot: 1.create object
        		%2. call PlotIt with all needed data, to plot a test result
        		
        		%this function will be used by the plotmaker, so it should be
        		%changed if something additional has to be done.
        		
        %		obj.ConfigureTestConfig(TestConfig);
        %		obj.ConfigureUserConfig(UserConfig);
        %		obj.AddTestResults(Results);
        %		obj.SetPlotAxis(plotAxis);
        %		obj.TestReady=true;
        		
        %		iPlot=tic;
        %		obj.PlotResults;
        %		obj.CPUTimeData=toc(iPlot);
        		
        		
        %		PlotHandle=obj.PlotHandles;
        		
        		
        %	end
        	
        %end
        
end	

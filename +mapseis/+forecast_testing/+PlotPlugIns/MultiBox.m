classdef MultiBox < mapseis.forecast_testing.TestPlotObj
	%A classic histogram based likelihood plot
	
	properties
		%PlotName %Name of the plot, not used as title.
        	%Version %Has to be a STRING, used to describe the version of the code
        	%PlotInfo %The place for a description about the plot 
        	
        	%PlotSelect %can be used if the plot uses more than on axis per test (see signMatPlot as example)		
        	%PlotAxis	%Axis used for the plots
        	%PlotHandles	%Handles of the different plot parts (optional, but suggested)
        	
        	%TestReady
        	%CPUTimeData
        	
        	%PlotReady signalizes that the plot object is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it is just the time
        	%needed for plotting
        	
        	TestResults
        	TestConfig
        	UserConfig
        	LegendEntries
        	
        end
    

       
        methods 
        
        	function obj = MultiBox()
   		
        		obj.PlotName='Likelhood_MultiBox';
              		obj.Version='v1';
              		
              		obj.PlotInfo=['Classic histogram plot for likelihood based tests ',...
              				'like the L-test. This version allows to plot multiple',...
              				'boxes representing the distribution of the histogram',...
              				'(for different models) into one plot'];
              				
              		obj.PlotSelect = 1 
              		obj.PlotAxis = [];	
              		obj.PlotHandles	= [];
        	
              		obj.PlotReady = false;
              		obj.CPUTimeData	=[];	
              		
              		obj.TestResults=[];
              		obj.TestConfig=[];
              		
              		%default user config
              		DefaultUseConf=struct(	'histColor',[0.88 0.88 0.88],...
              					'orgColor','r',...
              					'meanColor','m',...
              					'medianColor','g',...
              					'ShowText',true,...
              					'plotOrg',true,...
              					'plotMean',false,...
              					'plotMedian',false,...
              					'NtestMode',false,...
              					'PaperBoy',true);
              					
              		
              		obj.UserConfig=DefaultUseConf;
              		obj.LegendEntries={};		
        	end
        	
        	
        	function ConfigureTestConfig(obj,TestConfig)
			%Used to configurate the part of the configuration needed from the
			%Test, e.g. Test Name, fields with the values, etc.
			%For each result used a test config will be submitted, by the 
			%plotmaker, means TestConfig will be a cell array with structures
			%for each result (e.g. {conf1, conf2, conf3,....}
			
			obj.TestConfig={};
			
			if ~iscell(TestConfig)
				obj.TestConfig{1}=TestConfig;
			else
				obj.TestConfig=TestConfig;
			end
			
        	end
        	
        	function ConfigureUserConfig(obj,UserConfig)
			%This is used to set the user configuration, which contain everything
			%not defined by the test, like colors, linewidth, etc. There is only
			%one UserConfig for all results used and some fields will always be 
			%submitted, no matter if the plot function is using it:
			%	PaperBoy:	if true the resulting graphic will be used
			%			in a print, hence lines should be a bit thicker
			%			and fonts a bit larger, but it is up to the plot
			%			function what it should do
			%Don't if there are more	
			obj.UserConfig=UserConfig;
			
        	end
        	
        	
        	function UserConfig = getUserConfig(obj)
        		%should return the current user configuration, mostly needed to get
        		%the default values
        		
        		UserConfig = obj.UserConfig;
        		
        	end	
        	
        	function FeatureList = getFeatures(obj)
			%This will be used by the TestSuite to determine what the model can 
			%be used for. The output should by structure with specific fieldnames
			%with mostly boolean values. 
			%Following keywords are available:
			%	MultiplePlot:		if true, the plot produces more than one
			%				plot for a single test, example sign. matrix
			%				plot function, which plots two plot "per test"
			%	No_Plots:		only needed if MultiplePlot is true, defines
			%				how many plots are produced
			%	Uncertain_mean:		Defines if the plot is able to plot Uncertainties
			%				with a mean value and variance (true if possible)
			%	Uncertain_median:	Defines if the plot is able to plot Uncertainties
			%				with a median Q16 and Q84 (true if possible)
			%	MultiTest:		True if more than one test per plot is allowed.
			%				If MultiTest is supported, TestConfig and Results will
			%				be submitted as cell array no matter if only one result 
			%				is used.
			%	SingleTest:		True if it is possible to plot only one test per
			%				plot
        	
			FeatureList = struct(	'MultiplePlot',false,...
						'No_Plots',1,...
						'Uncertain_mean',true,...
						'Uncertain_median',true,...
						'MultiTest',true,...
						'SingleTest',true);
        	
        	
        	
        	function PossibleParameters = getUserParameterSetting(obj)
			%The method should return a description of the parameters needed
			%in the UserConfig and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be used for ConfigureTest
			%Each entry in structure should be another structure with following
			%Often the Plots do not need any configuration in this case the 
			%PossibleParameters can return empty
			
			
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			the selected string.
			%			
			%			
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			%WILL WRITE THAT LATER
			
		end	
        	
		
        	function SetPlotAxis(obj,plotAxis)
        		%sets the axis used for plotting
        		
        		if isempty(plotAxis)
        			obj.PlotAxis=gca;
        		else
        			obj.PlotAxis=plotAxis;
        		end
        		
        		
        	end
        	
        	
        	
        	function AddTestResults(obj,Results)
			%Used to submit the results of the tests, often more than one test is supported
			%in this case Results should be a cell array of the same size as TestConfig
			%(e.g. {Result1,Result2,Result3,...}
			
			obj.TestResults={};
			
			if ~iscell(Results)
				obj.TestResults{1}=Results;
			else
				obj.TestResults=Results;
			end
			
        	end
        	
        	
        	function PlotResults(obj)
        		%Will be used to start the plotting. The Handles of the different plot parts
        		%(lines,points, etc.) can stored in PlotHandles
        	
        		%FINISHED but UNTESTED
        		
        		%grey color used in this plot
        		histColor=obj.UserConfig.histColor;
        		redOne=[0.8 0.5 0.5];
        		greenOne=[0.5 0.8 0.5];
        		
        		
        		%high of the histograms, could be added to the UserConfig if needed.
        		MiniHistHigh=0.4; 
        		
        		
        		
        		%define some plotting parameters
        		%...............................
        		if obj.UserConfig.PaperBoy
        			AxisWidth=3;
        			MainLineWidth=4;
        			ErrorLineWidth=2;
        			TitleFontSize=36;
        			AxisFontSize=16;
        			LabelFontSize=18;
        			WeightAxis='bold';
        			
        			TextSize=12;
        		else
        			AxisWidth=2;
        			MainLineWidth=3;
        			ErrorLineWidth=1;
        			TitleFontSize=28;
        			AxisFontSize=14;
        			LabelFontSize=16;
        			WeightAxis='demi';
        			
        			TextSize=11;
        			
        		end
        		
        		
        	
        		
        		%reorder the data
        		
        		%init of data 
        		indexLabel=cell(numel(obj.TestResults),1);
        		HistData=cell(numel(obj.TestResults),1);
        		LikeOrgCat=NaN(numel(obj.TestResults),1);
        		SynthMin=NaN(numel(obj.TestResults),1);
        		SynthMax=NaN(numel(obj.TestResults),1);
        		
        		countHist=cell(numel(obj.TestResults),1);
        		bins=cell(numel(obj.TestResults),1);
        		PeakMaxes=NaN(numel(obj.TestResults),1);
        		PeakMines=NaN(numel(obj.TestResults),1);
        		
        		QScorelow=NaN(numel(obj.TestResults),1);
        		QScorehi=NaN(numel(obj.TestResults),1);
        		
        		LikeOrgMean=NaN(numel(obj.TestResults),1);
        		LikeOrgSTD=NaN(numel(obj.TestResults),1);
        		QScorelowMean=NaN(numel(obj.TestResults),1);
        		QScorelowSTD=NaN(numel(obj.TestResults),1);
        		QScorehiMean=NaN(numel(obj.TestResults),1);
        		QScorehiSTD=NaN(numel(obj.TestResults),1);
        		
        		LikeOrgMedian=NaN(numel(obj.TestResults),1);
        		LikeOrgQuantL=NaN(numel(obj.TestResults),1);
        		LikeOrgQuantH=NaN(numel(obj.TestResults),1);
        		QScorelowMedian=NaN(numel(obj.TestResults),1);
        		QScorelowQuantL=NaN(numel(obj.TestResults),1);
        		QScorelowQuantH=NaN(numel(obj.TestResults),1);
        		QScorehiMedian=NaN(numel(obj.TestResults),1);
        		QScorehiQuantL=NaN(numel(obj.TestResults),1);
        		QScorehiQuantH=NaN(numel(obj.TestResults),1);
        		
        		
        		
        		for i=1:numel(obj.TestResults)
								
				indexLabel{i} = [obj.TestConfig{i}.TestName,': ',obj.TestConfig{i}.ForecastName];
				
				HistData{i}=obj.TestResults{i}.(obj.TestConfig{i}.LogSynthCatField);
				LikeOrgCat(i)=obj.TestResults{i}.(obj.TestConfig{i}.LogCatField);
				SynthMin(i)=min(HistData{i});
				SynthMax(i)=max(HistData{i});
				
				
				
				if obj.TestConfig{i}.OneSide
					QScorelow(i)=obj.TestResults{i}.(obj.TestConfig{i}.ScoreField_low);					

				else
					QScorelow(i)=obj.TestResults{i}.(obj.TestConfig{i}.ScoreField_low);
					QScorehi(i)=obj.TestResults{i}.(obj.TestConfig{i}.ScoreField_hi);
				end
				
				
				if obj.TestConfig{i}.Uncertianties
					if obj.TestConfig{i}.MeanAvailable
						LikeOrgMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.LogCatField]);
						LikeOrgSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.LogCatField]);
					
						if obj.TestConfig{i}.OneSide
							QScorelowMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.ScoreField_low]);
							
						else
							QScorelowMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.ScoreField_low]);
							QScorehiMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.ScoreField_hi]);
							QScorehiSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.ScoreField_hi]);	
						
						end
					end
					
					
					if obj.TestConfig{i}.MedianAvailable
						LikeOrgMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.LogCatField]);
						LikeOrgQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.LogCatField])(1);
						LikeOrgQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.LogCatField])(2);
						
						if obj.TestConfig{i}.OneSide
							QScorelowMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(1);
							QScorelowQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(2);
							
						else
							QScorelowMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(1);
							QScorelowQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(2);
							QScorehiMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.ScoreField_hi]);
							QScorehiQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_hi])(1);
							QScorehiQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_hi])(2);
						end
						
					end	
					
				end
				
		
			end
        		
        		
		
			
			%additional needed values, some may not be needed in the 
			%new version, as the new version of the test includes more
			%fields, but I leave them for now to be consistent
			DistSynth=SynthMax-SynthMin;
					
			LQPoint=SynthMin+QScorelow.*DistSynth;
			LQMeanPoint=SynthMin+QScorelowMean.*DistSynth;
			LQSTDmin=SynthMin+(QScorelowMean-QScorelowSTD).*DistSynth;
			LQSTDmax=SynthMin+(QScorelowMean+QScorelowSTD).*DistSynth;
					
			HQPoint=SynthMin+QScorehi.*DistSynth;
			HQMeanPoint=SynthMin+QScorehiMean.*DistSynth;
			HQSTDmin=SynthMin+(QScorehiMean-QScorehiSTD).*DistSynth;
			HQSTDmax=SynthMin+(QScorehiMean+QScorehiSTD).*DistSynth;
			
			LQMedPoint=SynthMin+QScorelowMedian.*DistSynth;
			LQQmin=SynthMin+(QScorelowQuantL).*DistSynth;
			LQQmax=SynthMin+(QScorelowQuantH).*DistSynth;
						
			HQMedPoint=SynthMin+QScorehiMedian.*DistSynth;
			HQQmin=SynthMin+(QScorehiQuantL).*DistSynth;
			HQQmax=SynthMin+(QScorehiQuantH).*DistSynth;
			
			LikeSTDmin=LikeOrgMean-LikeOrgSTD;
			LikeSTDmax=LikeOrgMean+LikeOrgSTD;
			
			
			
			%handle list to be stored
        		handle=[];
        		legendentry={};
					
        		
        		for i=1:numel(obj.TestResults)
				%first plot the bar with the synthetic likelihoods
			
				
				SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
				SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
				
				handle(end+1)=fill(SynthBoxX,SynthBoxY,histColor);
				
				set(handle(end),'EdgeColor',histColor-0.25);
				legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
				hold on
				
				
				
				if obj.UserConfig.plotOrg
					if obj.TestConfig{i}.OneSide
						%low quantil
						OrgBarY=[i-0.2,i+0.2];
						OrgBarX=[LQPoint(i),LQPoint(i)];
						handle(end+1)=plot(OrgBarX,OrgBarY);
						set(handle(end),'LineStyle','-','Color',obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
						legendentry{end+1}=['org. Q-score ',indexLabel{i}];
							
						

					else
						if  obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[LikeOrgCat(i),LikeOrgCat(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
							legendentry{end+1}=['org. Number of Events ',indexLabel{i}];
						else
							%low quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[LQPoint(i),LQPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
							legendentry{end+1}=['org. low Q-score',indexLabel{i}];
							
							%hi quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[HQPoint(i),HQPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.orgColor,'LineWidth',MainLineWidth);
							legendentry{end+1}=['org. high Q-score',indexLabel{i}];
						end
						
					
					end
				end
				
				
				if obj.UserConfig.plotMean&obj.TestConfig{i}.MeanAvailable
					if obj.TestConfig{i}.OneSide
						ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
						ErrorBoxX=[LQSTDmin(i),LQSTDmin(i),LQSTDmax(i),LQSTDmax(i)];	
					
						LowErrorBarY=[i-0.2,i+0.2];
						LowErrorBarX=[LQSTDmin(i),LQSTDmin(i)];
							
						HiErrorBarY=[i-0.2,i+0.2];
						HiErrorBarX=[LQSTDmax(i),LQSTDmax(i)];
							
						MeanBarY=[i-0.2,i+0.2];
						MeanBarX=[LikeOrgMean(i),LikeOrgMean(i)];
						
						
						handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
						set(handle(end),'LineStyle','none');
						legendentry{end+1}=['Errorbox ',indexLabel{i}];
						
						
						handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
						set(handle(end),'LineStyle','--','Color', obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
						legendentry{end+1}=['lower error Q-score ',indexLabel{i}];
						handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
						set(handle(end),'LineStyle','--','Color', obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
						legendentry{end+1}=['upper error Q-score ',indexLabel{i}];
							
						handle(end+1)=plot(MeanBarX,MeanBarY);
						set(handle(end),'LineStyle','-','Color',obj.UserConfig.meanColor,'LineWidth',MeanLineWidth);
						legendentry{end+1}=['mean Q-score ',indexLabel{i}];
						
					else
						if  obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LikeSTDmin(i),LikeSTDmin(i),LikeSTDmax(i),LikeSTDmax(i)];	
						
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LikeSTDmin(i),LikeSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LikeSTDmax(i),LikeSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[NumbMean(i),NumbMean(i)];	
							
							handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['lower error Number ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['upper error Number ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.meanColor,'LineWidth',MeanLineWidth);
							legendentry{end+1}=['mean Number of Events ',indexLabel{i}];
						
						else
							%Low Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LQSTDmin(i),LQSTDmin(i),LQSTDmax(i),LQSTDmax(i)];	
						
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LQSTDmin(i),LQSTDmin(i)];
								
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LQSTDmax(i),LQSTDmax(i)];
								
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[LQMeanPoint(i),LQMeanPoint(i)];
							
							handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color', obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['lower error low Q-score ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color', obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['upper error low Q-score ',indexLabel{i}];
								
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.meanColor,'LineWidth',MeanLineWidth);
							legendentry{end+1}=['mean low Q-score ',indexLabel{i}];	
						
							
							%High Quantil 	
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[HQSTDmin(i),HQSTDmin(i),HQSTDmax(i),HQSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[HQSTDmin(i),HQSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[HQSTDmax(i),HQSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[HQMeanPoint(i),HQMeanPoint(i)];
							
							handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
								
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color', obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['lower error high Q-score ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color', obj.UserConfig.meanColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['upper error high Q-score ',indexLabel{i}];
								
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.meanColor,'LineWidth',MeanLineWidth);
							legendentry{end+1}=['mean high Q-score ',indexLabel{i}];	
						end
					end
					
				end
				
				
				if obj.UserConfig.plotMedian&obj.TestConfig{i}.MedianAvailable
					if obj.TestConfig{i}.OneSide
						ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
						ErrorBoxX=[LQQmin(i),LQQmin(i),LQQmax(i),LQQmax(i)];
					
						LowErrorBarY=[i-0.2,i+0.2];
						LowErrorBarX=[LQQmin(i),LQQmin(i)];
							
						HiErrorBarY=[i-0.2,i+0.2];
						HiErrorBarX=[LQQmax(i),LQQmax(i)];
							
						MedBarY=[i-0.2,i+0.2];
						MedBarX=[LQMedPoint(i),LQMedPoint(i)];
						
						handle(end+1)=fill(ErrorBoxX,ErrorBoxY,greenOne);
						set(handle(end),'LineStyle','none');
						legendentry{end+1}=['Quantilbox ',indexLabel{i}];
						
						handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
						set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
						legendentry{end+1}=['Q16 Q-score ',indexLabel{i}];
						handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
						set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
						legendentry{end+1}=['Q84 Q-score ',indexLabel{i}];
							
						handle(end+1)=plot(MedBarX,MedBarY);
						set(handle(end),'LineStyle','-','Color',obj.UserConfig.medianColor,'LineWidth',MedianLineWidth);
						legendentry{end+1}=['median Q-score ',indexLabel{i}];
					
					else
						if  obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LikeOrgQuantL(i),LikeOrgQuantL(i),LikeOrgQuantH(i),LikeOrgQuantH(i)];
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LikeOrgQuantL(i),LikeOrgQuantL(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LikeOrgQuantH(i),LikeOrgQuantH(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[LikeOrgMedian(i),LikeOrgMedian(i)];
							
							handle(end+1)=fill(ErrorBoxX,ErrorBoxY,greenOne);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['Q16 Number ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['Q84 Number ',indexLabel{i}];
								
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.medianColor,'LineWidth',MedianLineWidth);
							legendentry{end+1}=['median Number ',indexLabel{i}];	
						
							
						else
							%Low Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LQQmin(i),LQQmin(i),LQQmax(i),LQQmax(i)];
						
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LQQmin(i),LQQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LQQmax(i),LQQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[LQMedPoint(i),LQMedPoint(i)];
							
							handle(end+1)=fill(ErrorBoxX,ErrorBoxY,greenOne);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['Q16 low Q-score ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['Q84 low Q-score ',indexLabel{i}];
								
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.medianColor,'LineWidth',MedianLineWidth);
							legendentry{end+1}=['median low Q-score ',indexLabel{i}];	
						
						
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[HQQmin(i),HQQmin(i),HQQmax(i),HQQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[HQQmin(i),HQQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[HQQmax(i),HQQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[HQMedPoint(i),HQMedPoint(i)];
						
							handle(end+1)=fill(ErrorBoxX,ErrorBoxY,greenOne);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['Q16 high Q-score ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color',obj.UserConfig.medianColor,'LineWidth',ErrorLineWidth);
							legendentry{end+1}=['Q84 high Q-score ',indexLabel{i}];
								
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color',obj.UserConfig.medianColor,'LineWidth',MedianLineWidth);
							legendentry{end+1}=['median high Q-score ',indexLabel{i}];
							
						end
					end
				
				end
				
				
			end	
        		
			
			
			%generate title
			if ~isstr(obj.TestConfig.ForecastTime)
        			TheTitle= [obj.UserConfig.PlotTitle,':  ',datestr(obj.TestConfig.ForecastTime)];
			else
				TheTitle= [obj.UserConfig.PlotTitle,':  ',obj.TestConfig.ForecastTime];
			end
        		
			%and the title
			theTit=title(TheTitle);
			set(theTit,'FontSize',TitleFontSize,'FontWeight','bold','Interpreter','none');
			
			
			
			%axis labels and some cosmetics
			%..............................
			xlab=xlabel(obj.TestConfig.XaxisLabel);
			
				
			indexer=0:numel(obj.TestResults);
			indexLabel={'',indexLabel{:}};
			set(obj.PlotAxis,'YTick',indexer,'YTickLabel',indexLabel);
			
			set([xlab],'FontSize',LabelFontSize,'FontWeight','bold');
			set(obj.PlotAxis,'LineWidth',AxisWidth,'FontSize',AxisFontSize,'FontWeight',WeightAxis);
			
			hold off
			
			%store stuff in fields
			obj.LegendEntries=legendentry;
			obj.PlotHandles=handle;		
        				
        	
        	
        	
        	
        end

      
        
         %methods 
        	%useful methods 
        %	function PlotHandle = PlotIt(obj,plotAxis,Results,TestConfig,UserConfig)
        		%just and all in one config and plot shortcut, normally it will
        		%be enough to the following two steps to plot: 1.create object
        		%2. call PlotIt with all needed data, to plot a test result
        		
        		%this function will be used by the plotmaker, so it should be
        		%changed if something additional has to be done.
        		
        %		obj.ConfigureTestConfig(TestConfig);
        %		obj.ConfigureUserConfig(UserConfig);
        %		obj.AddTestResults(Results);
        %		obj.SetPlotAxis(plotAxis);
        %		obj.TestReady=true;
        		
        %		iPlot=tic;
        %		obj.PlotResults;
        %		obj.CPUTimeData=toc(iPlot);
        		
        		
        %		PlotHandle=obj.PlotHandles;
        		
        		
        %	end
        	
        %end
        
end	

classdef InfoRanking < mapseis.forecast_testing.TestPlotObj
	%A classic histogram based likelihood plot
	
	properties
		%PlotName %Name of the plot, not used as title.
        	%Version %Has to be a STRING, used to describe the version of the code
        	%PlotInfo %The place for a description about the plot 
        	
        	%PlotSelect %can be used if the plot uses more than on axis per test (see signMatPlot as example)		
        	%PlotAxis	%Axis used for the plots
        	%PlotHandles	%Handles of the different plot parts (optional, but suggested)
        	
        	%TestReady
        	%CPUTimeData
        	
        	%PlotReady signalizes that the plot object is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it is just the time
        	%needed for plotting
        	
        	TestResults
        	TestConfig
        	UserConfig
        	LegendEntries
        	
        end
    
        %UNFINISHED
       
        methods 
        
        	function obj = InfoRanking()
   		
        		obj.PlotName='Ranking';
              		obj.Version='v1';
              		
              		%Not the best description but it will do for now.
              		obj.PlotInfo=['This plots allows to plot information gain ',...
              				'to an uniform forecasts calculated in multipe Single-TRank ',...
              				'into a comparison plot.'];
              				
              		obj.PlotSelect = 1 
              		obj.PlotAxis = [];	
              		obj.PlotHandles	= [];
        	
              		obj.PlotReady = false;
              		obj.CPUTimeData	=[];	
              		
              		obj.TestResults=[];
              		obj.TestConfig=[];
              		
              		%default user config
              		DefaultUseConf=struct(	'greyColor',[[0.85 0.85 0.85]],...
              					'orgColor','r',...
              					'meanColor','m',...
              					'medianColor','g',...
              					'VerticalMode',false,...
              					'plotOrg',true,...
              					'plotMean',false,...
              					'plotMedian',false,...
              					'plotUniLine',false,...
              					'plotMaxGain',true,...
              					'WideBar',true,...
              					'BarTrail',true,...
              					'DisplayNames',{{}},...
              					'usePercent',false,...
              					'PlotTitle',[[]],...
              					'PaperBoy',true);
              		%have to write something about the parameters later but just to not forget:
              		%if SingleForecast is true the labels of the tests only show the testname and 
			%not the additional forecasts name.			
              		
              		obj.UserConfig=DefaultUseConf;
              		obj.LegendEntries={};		
        	end
        	
        	
        	function ConfigureTestConfig(obj,TestConfig)
			%Used to configurate the part of the configuration needed from the
			%Test, e.g. Test Name, fields with the values, etc.
			%For each result used a test config will be submitted, by the 
			%plotmaker, means TestConfig will be a cell array with structures
			%for each result (e.g. {conf1, conf2, conf3,....}
			
			obj.TestConfig={};
			
			if ~iscell(TestConfig)
				obj.TestConfig{1}=TestConfig;
			else
				obj.TestConfig=TestConfig;
			end
			
        	end
        	
        	
        	function ConfigureUserConfig(obj,UserConfig)
			%This is used to set the user configuration, which contain everything
			%not defined by the test, like colors, linewidth, etc. There is only
			%one UserConfig for all results used and some fields will always be 
			%submitted, no matter if the plot function is using it:
			%	PaperBoy:	if true the resulting graphic will be used
			%			in a print, hence lines should be a bit thicker
			%			and fonts a bit larger, but it is up to the plot
			%			function what it should do
			%Don't if there are more	
			obj.UserConfig=UserConfig;
			
        	end
        	
        	
        	function UserConfig = getUserConfig(obj)
        		%should return the current user configuration, mostly needed to get
        		%the default values
        		
        		UserConfig = obj.UserConfig;
        		
        	end	
        	
        	function FeatureList = getFeatures(obj)
			%This will be used by the TestSuite to determine what the model can 
			%be used for. The output should by structure with specific fieldnames
			%with mostly boolean values. 
			%Following keywords are available:
			%	MultiplePlot:		if true, the plot produces more than one
			%				plot for a single test, example sign. matrix
			%				plot function, which plots two plot "per test"
			%	No_Plots:		only needed if MultiplePlot is true, defines
			%				how many plots are produced
			%	Uncertain_mean:		Defines if the plot is able to plot Uncertainties
			%				with a mean value and variance (true if possible)
			%	Uncertain_median:	Defines if the plot is able to plot Uncertainties
			%				with a median Q16 and Q84 (true if possible)
			%	MultiTest:		True if more than one test per plot is allowed.
			%				If MultiTest is supported, TestConfig and Results will
			%				be submitted as cell array no matter if only one result 
			%				is used.
			%	SingleTest:		True if it is possible to plot only one test per
			%				plot
        	
			FeatureList = struct(	'MultiplePlot',false,...
						'No_Plots',1,...
						'Uncertain_mean',true,...
						'Uncertain_median',true,...
						'MultiTest',true,...
						'SingleTest',true);
        	
		end
        	
        	
        	function PossibleParameters = getUserParameterSetting(obj)
			%The method should return a description of the parameters needed
			%in the UserConfig and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be used for ConfigureTest
			%Each entry in structure should be another structure with following
			%Often the Plots do not need any configuration in this case the 
			%PossibleParameters can return empty
			
			
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			the selected string.
			%			
			%			
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			%WILL WRITE THAT LATER
			
		end	
        	
		
        	function SetPlotAxis(obj,plotAxis)
        		%sets the axis used for plotting
        		
        		if isempty(plotAxis)
        			obj.PlotAxis=gca;
        		else
        			obj.PlotAxis=plotAxis;
        		end
        		
        		
        	end
        	
        	
        	
        	function AddTestResults(obj,Results)
			%Used to submit the results of the tests, often more than one test is supported
			%in this case Results should be a cell array of the same size as TestConfig
			%(e.g. {Result1,Result2,Result3,...}
			
			obj.TestResults={};
			
			if ~iscell(Results)
				obj.TestResults{1}=Results;
			else
				obj.TestResults=Results;
			end
			
        	end
        	
        	
        	function PlotResults(obj)
        		%Will be used to start the plotting. The Handles of the different plot parts
        		%(lines,points, etc.) can stored in PlotHandles
        		
        		%FINISHED but UNTESTED
        		
        		import mapseis.util.*;
        		import mapseis.forecast_testing.util.*;
        		
        		%size(obj.UserConfig)
        		
			if  obj.UserConfig.PaperBoy
				AxisWidth=3;
        			MainLineWidth=4;
        			ErrorLineWidth=2;
        			TitleFontSize=22;
        			AxisFontSize=16;
        			LabelFontSize=18;
        			WeightAxis='bold';
        			WeightLab='bold';
        			TextSize=12;
				
				
			else
				AxisWidth=2;
        			MainLineWidth=3;
        			ErrorLineWidth=1;
        			TitleFontSize=16;
        			AxisFontSize=14;
        			LabelFontSize=16;
        			WeightAxis='demi';
        			WeightLab='demi';
        			
        			TextSize=11;
					
				
			end
		
			
			%fixed parameters (colors 
			redOne=[0.8 0.5 0.5];
			greenOne=[0.5 0.7 0.5];
	
			%grey color used in this plot
        		greyHound=obj.UserConfig.greyColor;
				
			if  obj.UserConfig.WideBar
				BarWider=0.9;
			else
				BarWider=0.25;
			end
        		
        		
			%create the data
			InfoGain=[];  %Ival=InfoGain
			PercGain=[];  %IGainPerc=PercGain
			InfoPerfect=[];  %MaxGain=InfoPerfect
			
			InfoGainMean=[]; 
			InfoGainSTD=[];
			PercGainMean=[]; 
			PercGainSTD=[]; 
			
			InfoGainMed=[]; %IvalMed=InfoGainMed
			InfoGainQ16=[]; %IvalQ16=InfoGainQ16
			InfoGainQ84=[]; %IvalQ84=InfoGainQ84
			
			PercGainMed=[]; 
			PercGainQ16=[]; 
			PercGainQ84=[]; 
			
			for rC=1:numel(obj.TestResults)
				TtestNow=obj.TestResults{rC};
				
				indexer(rC)=2*(rC);
				indexLabel(rC)=obj.UserConfig.DisplayNames(rC);
				InfoGain(rC)=TtestNow.(obj.TestConfig{rC}.InfoGain);
				PercGain(rC)=TtestNow.(obj.TestConfig{rC}.PercentGain);
				InfoPerfect(rC)=TtestNow.(obj.TestConfig{rC}.PerfectModel);
				
				if obj.TestConfig{rC}.Uncertianties
					if obj.TestConfig{rC}.MeanAvailable
						InfoGainMean(rC)=TtestNow.(['mean_',obj.TestConfig{rC}.InfoGain]);
						InfoGainSTD(rC)=TtestNow.(['std_',obj.TestConfig{rC}.InfoGain]);
						
						PercGainMean(rC)=TtestNow.(['mean_',obj.TestConfig{rC}.PercentGain]);
						PercGainSTD(rC)=TtestNow.(['std_',obj.TestConfig{rC}.PercentGain]);
					else
						InfoGainMean(rC)=NaN;
						InfoGainSTD(rC)=NaN;
						
						PercGainMean(rC)=NaN;
						PercGainSTD(rC)=NaN;
					end
					
					if obj.TestConfig{rC}.MedianAvailable
						InfoGainMed(rC)=TtestNow.(['median_',obj.TestConfig{rC}.InfoGain]);
						InfoGainQ16(rC)=TtestNow.(['quant68_',obj.TestConfig{rC}.InfoGain])(1);
						InfoGainQ84(rC)=TtestNow.(['quant68_',obj.TestConfig{rC}.InfoGain])(2);
						
						PercGainMed(rC)=TtestNow.(['median_',obj.TestConfig{rC}.PercentGain]);
						PercGainQ16(rC)=TtestNow.(['quant68_',obj.TestConfig{rC}.PercentGain])(1);
						PercGainQ84(rC)=TtestNow.(['quant68_',obj.TestConfig{rC}.PercentGain])(2);
						
					else
						InfoGainMed(rC)=NaN;
						InfoGainQ16(rC)=NaN;
						InfoGainQ84(rC)=NaN;
						
						PercGainMed(rC)=NaN;
						PercGainQ16(rC)=NaN;
						PercGainQ84(rC)=NaN;
						
					end
					
				
				
				else
					
					InfoGainMean(rC)=NaN;
					InfoGainSTD(rC)=NaN;
						
					PercGainMean(rC)=NaN;
					PercGainSTD(rC)=NaN;
						
					InfoGainMed(rC)=NaN;
					InfoGainQ16(rC)=NaN;
					InfoGainQ84(rC)=NaN;
					
					PercGainMed(rC)=NaN;
					PercGainQ16(rC)=NaN;
					PercGainQ84(rC)=NaN;
						
				end
			
			end	
				
			
				
				
			%Select the right parameters
			if obj.UserConfig.usePercent
				ModGain=PercGain;
				
				ModGainMean=PercGainMean;
				ModGainSTD=PercGainSTD;
				
				MaxGainLine=100;
				
				RawTitle1='Information Gain (in %): ';
				RawTitle2='Information Gain (in %) Reference: Uniform';
						
	
				ModGainMed=PercGainMed;
				ModeGainQ16=InfoGainQ16;
				ModGainQ84=PercGainQ84;
																		
			else	
				ModGain=InfoGain;
				
				ModGainMean=InfoGainMean;
				ModGainSTD=InfoGainSTD;
				
							
				MaxGainLine=InfoPerfect(1);
				
				
				RawTitle1='Information Gain: ';
				RawTitle2='Information Gain Reference: Uniform';
				
				ModGainMed=InfoGainMed;
				ModeGainQ16=InfoGainQ16;
				ModGainQ84=InfoGainQ84;
				
						
			end
				
				
				
				
				
				
				
					
			handle=[];
			legendentry={};
					
			%now plot it
			axes(obj.PlotAxis);
			for i=1:numel(indexer)
				if  obj.UserConfig.BarTrail
					%Dotted line from 0 to OrgVal
					TrailerX=[0, ModGain(i)];
					TrailerY=[indexer(i), indexer(i)];
					
					if obj.UserConfig.VerticalMode		
						handle(end+1)=plot(TrailerY,TrailerX);
					else
						handle(end+1)=plot(TrailerX,TrailerY);
					end
					
					set(handle(end),'LineStyle',':','Color','k','LineWidth',2);
					legendentry{end+1}=['TrailLine ',indexLabel{i}];
					hold on;
				end
					
					
				if  obj.UserConfig.plotMean&obj.TestConfig{1}.MeanAvailable
					MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
					MeanboxX=[ModGainMean(i)-ModGainSTD(i),ModGainMean(i)+ModGainSTD(i),...
						  ModGainMean(i)+ModGainSTD(i),ModGainMean(i)-ModGainSTD(i)];
							  
					hold on
						
					%first the boxes
					if obj.UserConfig.VerticalMode
						%plot vertical
						handle(end+1)=fill(MeanboxY,MeanboxX,greenOne);
						legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
									
													
						%now the lines
						handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[ModGainMean(i),ModGainMean(i)]);
						set(handle(end),'LineStyle','-','Color','g','LineWidth',MainLineWidth);
						legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
						
					else
						handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
						legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
									
													
						%now the lines
						handle(end+1)=plot([ModGainMean(i),ModGainMean(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
						set(handle(end),'LineStyle','-','Color','g','LineWidth',MainLineWidth);
						legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
					
					
					
					end
					
					
													
				end
				
				if obj.UserConfig.plotMedian&obj.TestConfig{1}.MedianAvailable
					%Difficult part because Q16 and Q84 are always fixed points, not differences
					%So check if it is right								  
					MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
					MedboxX=[ModeGainQ16(i),ModGainQ84(i),...
						  ModGainQ84(i),ModeGainQ16(i)];
					hold on
						
					if obj.UserConfig.VerticalMode	  
						handle(end+1)=fill(MedboxY,MedboxX,greenOne);
						legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
							
						
						%now the lines
						handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[ModGainMed(i),ModGainMed(i)]);
						set(handle(end),'LineStyle','-','Color','g','LineWidth',MainLineWidth);
						legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
																		
					else
						handle(end+1)=fill(MedboxX,MedboxY,greenOne);
						legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
							
						
						%now the lines
						handle(end+1)=plot([ModGainMed(i),ModGainMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
						set(handle(end),'LineStyle','-','Color','g','LineWidth',MainLineWidth);
						legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
					
					end
					
				end
				
				if obj.UserConfig.plotOrg
					if obj.UserConfig.VerticalMode
						handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[ModGain(i),ModGain(i)]);
						set(handle(end),'LineStyle','-','Color','g','LineWidth',MainLineWidth);
						legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
						hold on;
					
					else
					
						handle(end+1)=plot([ModGain(i),ModGain(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
						set(handle(end),'LineStyle','-','Color','g','LineWidth',MainLineWidth);
						legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
						hold on;
					end
					
				end		
			
				
				
				
				
				
			end
					
							
			if obj.UserConfig.VerticalMode		
				xlimer=xlim;
				xlimer(1)=xlimer(1)-0.5;
				xlimer(2)=xlimer(2)+0.5;
				xlim(xlimer);
			else
				ylimer=ylim;
				ylimer(1)=ylimer(1)-0.5;
				ylimer(2)=ylimer(2)+0.5;
				ylim(ylimer);
			end
			
			
			if obj.UserConfig.plotUniLine
				if obj.UserConfig.VerticalMode
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
				else
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
				end
			end		
					
			if obj.UserConfig.plotMaxGain
				if obj.UserConfig.VerticalMode
					handle(end+1)=plot([xlimer(1);xlimer(2)],[MaxGainLine;MaxGainLine])
					set(handle(end),'LineStyle','-','Color','b','LineWidth',2);
					legendentry{end+1}='Maximum Possible information gain';	
				
				else
					handle(end+1)=plot([MaxGainLine;MaxGainLine],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','-','Color','b','LineWidth',2);
					legendentry{end+1}='Maximum Possible information gain';	
				end
			end
					
					
			if ~isempty(obj.UserConfig.PlotTitle)
					TitStr=[RawTitle1,obj.UserConfig.PlotTitle];
			else
					TitStr=[RawTitle2];
			end
					
			if ~isstr(obj.TestConfig{1}.ForecastTime)
				TheTitle= [TitStr,':  ',datestr(obj.TestConfig{1}.ForecastTime)];
			else
				TheTitle= [TitStr,':  ',obj.TestConfig{1}.ForecastTime];
			end
				
			theTit=title(TheTitle);

			
			
			%axis labels and some cosmetics
			%..............................
			
			if obj.UserConfig.VerticalMode
				%vertical mode
				%format the plot
				if obj.UserConfig.usePercent
					ylab=ylabel('Information Gain I (%) ');
					if all(PercGain>=0)
						ylim([-1,101]);
					else
						ylimer=ylim;
						ylimer(1)=ylimer(1)-1;
						ylimer(2)=101;
						ylim(ylimer);
					end
					
				else
					ylab=ylabel('Information Gain I  ');
				end
				
				set(ylab,'FontSize',LabelFontSize,'FontWeight',WeightLab);
				set(obj.PlotAxis,'LineWidth',AxisWidth,'FontSize',AxisFontSize,'FontWeight',WeightAxis);
					
				%Sometimes the box was turned off, no clue why
				set(obj.PlotAxis,'XTick',indexer,'XTickLabel',indexLabel,'Box','on');
				set(theTit,'FontSize',TitleFontSize,'FontWeight','bold','Interpreter','none');
				
				try
					TheTicks=rotateticklabel(obj.PlotAxis,45);
					set(TheTicks,'FontSize',AxisFontSize,'FontWeight',WeightAxis);
				catch
					disp('could not rotate labels -rotateticklabel installed?')
					
				end
				
				%try set the axis position
				Pos=get(obj.PlotAxis,'Pos');
				Pos(2)=Pos(2)+0.2;
				Pos(4)=Pos(4)-0.2;
				set(obj.PlotAxis,'Pos',Pos);
				
			else
				%format the plot
				if obj.UserConfig.usePercent
					xlab=xlabel('Information Gain I (%) ');
					if all(PercGain>=0)
						xlim([-1,101]);
					else
						xlimer=xlim;
						xlimer(1)=xlimer(1)-1;
						xlimer(2)=101;
						xlim(xlimer);
					end
					
				else
					xlab=xlabel('Information Gain I  ');
				end
				
				set(xlab,'FontSize',LabelFontSize,'FontWeight',WeightLab);
				set(gca,'LineWidth',AxisWidth,'FontSize',AxisFontSize,'FontWeight',WeightAxis);
					
				%Sometimes the box was turned off, no clue why
				set(obj.PlotAxis,'YTick',indexer,'YTickLabel',indexLabel,'Box','on');
				set(theTit,'FontSize',TitleFontSize,'FontWeight','bold','Interpreter','none');
				
				
			
			
			end
		
			
			
			
			
			
			hold off
			
			%store stuff in fields
			obj.LegendEntries=legendentry;
			obj.PlotHandles=handle;		
			
			
			
			
			
			
			
		end	
			
			
			
			
			
			
			
			
			
        		
        		
        		
        				
        	
        	
        	
        	
        end

      
        
         %methods 
        	%useful methods 
        %	function PlotHandle = PlotIt(obj,plotAxis,Results,TestConfig,UserConfig)
        		%just and all in one config and plot shortcut, normally it will
        		%be enough to the following two steps to plot: 1.create object
        		%2. call PlotIt with all needed data, to plot a test result
        		
        		%this function will be used by the plotmaker, so it should be
        		%changed if something additional has to be done.
        		
        %		obj.ConfigureTestConfig(TestConfig);
        %		obj.ConfigureUserConfig(UserConfig);
        %		obj.AddTestResults(Results);
        %		obj.SetPlotAxis(plotAxis);
        %		obj.TestReady=true;
        		
        %		iPlot=tic;
        %		obj.PlotResults;
        %		obj.CPUTimeData=toc(iPlot);
        		
        		
        %		PlotHandle=obj.PlotHandles;
        		
        		
        %	end
        	
        %end
        
end	

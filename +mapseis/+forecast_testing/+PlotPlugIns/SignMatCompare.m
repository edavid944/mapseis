classdef SignMatCompare < mapseis.forecast_testing.TestPlotObj
	%A classic histogram based likelihood plot
	
	properties
		%PlotName %Name of the plot, not used as title.
        	%Version %Has to be a STRING, used to describe the version of the code
        	%PlotInfo %The place for a description about the plot 
        	
        	%PlotSelect %can be used if the plot uses more than on axis per test (see signMatPlot as example)		
        	%PlotAxis	%Axis used for the plots
        	%PlotHandles	%Handles of the different plot parts (optional, but suggested)
        	
        	%TestReady
        	%CPUTimeData
        	
        	%PlotReady signalizes that the plot object is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it is just the time
        	%needed for plotting
        	
        	TestResults
        	TestConfig
        	UserConfig
        	LegendEntries
        	
        end
    

       
        methods 
        
        	function obj = SignMatCompare()
   		
        		obj.PlotName='SignMatrix';
              		obj.Version='v1';
              		
              		obj.PlotInfo=['This object plots the p-values of the T-test ',...
              				'and W-test into a matrix. It is best combined with a ',...
              				'TR-test bar plot.'];
              				
              		obj.PlotSelect = 1 
              		obj.PlotAxis = [];	
              		obj.PlotHandles	= [];
        	
              		obj.PlotReady = false;
              		obj.CPUTimeData	=[];	
              		
              		obj.TestResults=[];
              		obj.TestConfig=[];
              		
              		%default user config
              		DefaultUseConf=struct(	   'Tsign',true,...
						   'Wsign',true,...
						   'ShowPNr',false,...
						   'PlotNames',{{}},...
						   'PlotTitle',[[]],...
						   'PaperBoy',true);
              		
              		%have to write something about the parameters later but just to not forget:
              		%if SingleForecast is true the labels of the tests only show the testname and 
			%not the additional forecasts name.			
              		
              		obj.UserConfig=DefaultUseConf;
              		obj.LegendEntries={};		
        	end
        	
        	
        	function ConfigureTestConfig(obj,TestConfig)
			%Used to configurate the part of the configuration needed from the
			%Test, e.g. Test Name, fields with the values, etc.
			%For each result used a test config will be submitted, by the 
			%plotmaker, means TestConfig will be a cell array with structures
			%for each result (e.g. {conf1, conf2, conf3,....}
			
			%As with the Result it should be three user config, each should contain a 
			%fieldname called ModelNames, where the single! forecast name (not
			%the name of the test-entries are stored as a cell array
			
			obj.TestConfig={};
			
			if ~iscell(TestConfig)
				obj.TestConfig{1}=TestConfig;
			else
				obj.TestConfig=TestConfig;
			end
			
        	end
        	
        	function ConfigureUserConfig(obj,UserConfig)
			%This is used to set the user configuration, which contain everything
			%not defined by the test, like colors, linewidth, etc. There is only
			%one UserConfig for all results used and some fields will always be 
			%submitted, no matter if the plot function is using it:
			%	PaperBoy:	if true the resulting graphic will be used
			%			in a print, hence lines should be a bit thicker
			%			and fonts a bit larger, but it is up to the plot
			%			function what it should do
			%Don't if there are more	
			obj.UserConfig=UserConfig;
			
        	end
        	
        	
        	function UserConfig = getUserConfig(obj)
        		%should return the current user configuration, mostly needed to get
        		%the default values
        		
        		UserConfig = obj.UserConfig;
        		
        	end	
        	
        	function FeatureList = getFeatures(obj)
			%This will be used by the TestSuite to determine what the model can 
			%be used for. The output should by structure with specific fieldnames
			%with mostly boolean values. 
			%Following keywords are available:
			%	MultiplePlot:		if true, the plot produces more than one
			%				plot for a single test, example sign. matrix
			%				plot function, which plots two plot "per test"
			%	No_Plots:		only needed if MultiplePlot is true, defines
			%				how many plots are produced
			%	Uncertain_mean:		Defines if the plot is able to plot Uncertainties
			%				with a mean value and variance (true if possible)
			%	Uncertain_median:	Defines if the plot is able to plot Uncertainties
			%				with a median Q16 and Q84 (true if possible)
			%	MultiTest:		True if more than one test per plot is allowed.
			%				If MultiTest is supported, TestConfig and Results will
			%				be submitted as cell array no matter if only one result 
			%				is used.
			%	SingleTest:		True if it is possible to plot only one test per
			%				plot
			
        	
			FeatureList = struct(	'MultiplePlot',false,...
						'No_Plots',1,...
						'Uncertain_mean',true,...
						'Uncertain_median',true,...
						'MultiTest',true,...
						'SingleTest',false);
        	
        	end
        	
        	function PossibleParameters = getUserParameterSetting(obj)
			%The method should return a description of the parameters needed
			%in the UserConfig and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be used for ConfigureTest
			%Each entry in structure should be another structure with following
			%Often the Plots do not need any configuration in this case the 
			%PossibleParameters can return empty
			
			
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			the selected string.
			%			
			%			
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			%WILL WRITE THAT LATER
			
		end	
        	
		
        	function SetPlotAxis(obj,plotAxis)
        		%sets the axis used for plotting
        		
        		if isempty(plotAxis)
        			obj.PlotAxis=gca;
        		else
        			obj.PlotAxis=plotAxis;
        		end
        		
        		
        	end
        	
        	
        	
        	function AddTestResults(obj,Results)
			%Used to submit the results of the tests, often more than one test is supported
			%in this case Results should be a cell array of the same size as TestConfig
			%(e.g. {Result1,Result2,Result3,...}
			
			%The Result should in this case consist of two structures
			%in the cell array,  the first one should be the substructure of the T-test and the
			%second one should be the substructure of the W-test
			
			
			obj.TestResults={};
			
			if ~iscell(Results)
				obj.TestResults{1}=Results;
			else
				obj.TestResults=Results;
			end
			
        	end
        	
        	
        	function PlotResults(obj)
        		%Will be used to start the plotting. The Handles of the different plot parts
        		%(lines,points, etc.) can stored in PlotHandles
        		
        		%FINISHED BUT UNTESTED
        		
        		
        		import mapseis.util.*;
        		import mapseis.forecast_testing.util.*;
        		
        		%hard wired colors and values for now.
        		WsuccCol=[0.6 0.9 0.6];
        		WfailCol=[0.9 0.6 0.6];
        		TsuccCol='g';
        		TfailCol='r';
        		
        		Spacer=2;
        		
        		
        		%define some plotting parameters
        		%...............................
        		if obj.UserConfig.PaperBoy
        			AxisWidth=3;
        			MainLineWidth=4;
        			ErrorLineWidth=2;
        			TitleFontSize=22;
        			AxisFontSize=16;
        			LabelFontSize=18;
        			WeightAxis='bold';
        			
        			TextSize=12;
        		else
        			AxisWidth=2;
        			MainLineWidth=3;
        			ErrorLineWidth=1;
        			TitleFontSize=16;
        			AxisFontSize=14;
        			LabelFontSize=16;
        			WeightAxis='demi';
        			
        			TextSize=11;
        			
        		end
        		%some of the above variable are probably not needed now
        		
        		
        		%get modelnames
        		try
        			ModelNames=obj.TestConfig{1}.ModelNames;
        		catch
        			ModelNames=obj.TestConfig{2}.ModelNames;
        		end
        		
        		%set display plot names
        		if isempty(obj.UserConfig.PlotNames)
        			PlotNames=obj.TestConfig{1}.ModelNames;
        		else
        			PlotNames=obj.UserConfig.PlotNames;
        		end
        		
        		%Length of the Line
        		LineLength=2*numel(ModelNames);
        		
        		%From SignMatPlotter
        		
        		
        		%Matrixes for the P values
			T_pVal=NaN(numel(ModelNames),numel(ModelNames));
			W_pVal=NaN(numel(ModelNames),numel(ModelNames));
			T_pStr=cell(numel(ModelNames),numel(ModelNames));
			W_pStr=cell(numel(ModelNames),numel(ModelNames));
			
			%MiddlePoints
			Xpos=NaN(numel(ModelNames),numel(ModelNames));
			Ypos=NaN(numel(ModelNames),numel(ModelNames));
			
			%Rectangles
			XRec=cell(numel(ModelNames),numel(ModelNames));
			YRex=cell(numel(ModelNames),numel(ModelNames));
			
			%markers for W-fields and T-fields
			inT=false(numel(ModelNames),numel(ModelNames));
			inW=false(numel(ModelNames),numel(ModelNames));
			inDiag=false(numel(ModelNames),numel(ModelNames));
		
			%Get the Values
			for i=1:numel(ModelNames)
				for j=1:numel(ModelNames)
					if i~=j
						Name=[ModelNames{i},'_',ModelNames{j}];
						NameW=[ModelNames{i},'_',ModelNames{j}];
					
						disp(Name)
						if obj.UserConfig.Tsign
							try
								TRes=obj.TestResults{1}.(Name);
							catch
								disp('reverse name Ttest');
								Name=[ModelNames{j},'_',ModelNames{i}];
								TRes=obj.TestResults{1}.(Name);
							end
					
							T_pVal(i,j)=TRes.(obj.TestConfig{1}.PvalField);
							WeSignT(i,j)=TRes.(obj.TestConfig{1}.SignResField);
							T_pStr{i,j}=num2str(T_pVal(i,j),2);
							
						else
							T_pVal(i,j)=NaN;
							WeSignT(i,j)=NaN;
							T_pStr{i,j}='';
							
						end	
							
						if obj.UserConfig.Wsign	
							try
								WRes=obj.TestResults{2}.(NameW);
							catch
								disp('reverse name Wtest');
								NameW=[ModelNames{j},'_',ModelNames{i}];
								WRes=obj.TestResults{2}.(NameW);
							end
						
							
							
							W_pVal(i,j)=WRes.(obj.TestConfig{2}.PvalField);
							WeSignW(i,j)=WRes.(obj.TestConfig{2}.SignResField);
							W_pStr{i,j}=num2str(W_pVal(i,j),2);
						else
							W_pVal(i,j)=NaN;
							WeSignW(i,j)=NaN;
							W_pStr{i,j}='';
							
						end
						
										
					
					else
						T_pStr{i,j}='n/a';
						W_pStr{i,j}='n/a';
						T_pVal(i,j)=1;
						W_pVal(i,j)=1;
						inDiag(i,j)=true;
					end
				
					%Middle points are needed in any case
					Xpos(i,j)=Spacer/2+(i-1)*Spacer;
					Ypos(i,j)=Spacer/2+(j-1)*Spacer;
					
					%same for rectangles
					RectX=[(i-1)*Spacer,(i)*Spacer,(i)*Spacer,(i-1)*Spacer,(i-1)*Spacer];
					RectY=[(j-1)*Spacer,(j-1)*Spacer,(j)*Spacer,(j)*Spacer,(j-1)*Spacer];
					XRec{i,j}=RectX;
					YRec{i,j}=RectY;
					
					%mark LR=A in the matrix
					if i>j
						inT(i,j)=true;
					end
					
					
					if i<j
						inW(i,j)=true;
					end
					
					
				end
			
			end			
				
		
			%now plot it
			axis(obj.PlotAxis);
			
			
			for i=1:numel(T_pVal)
				if inT(i)&obj.UserConfig.Tsign
					TheRect(i)=fill(XRec{i},YRec{i},'g','LineWidth',2);
					hold on;
					if obj.UserConfig.ShowPNr
						Texx(i)=text(Xpos(i),Ypos(i),T_pStr{i},'Units','data');
						set(Texx(i),'FontSize',18,'FontWeight','bold',...
						'HorizontalAlignment','center','VerticalAlignment','middle');
					end
					
					if WeSignT(i)
						set(TheRect(i),'FaceColor',TsuccCol);
					else
						set(TheRect(i),'FaceColor',TfailCol);
					end
					
				end
				
				if inW(i)&obj.UserConfig.Wsign
					TheRect(i)=fill(XRec{i},YRec{i},'g','LineWidth',2);
					hold on;
					if obj.UserConfig.ShowPNr
						Texx(i)=text(Xpos(i),Ypos(i),W_pStr{i},'Units','data');
						set(Texx(i),'FontSize',18,'FontWeight','bold',...
						'HorizontalAlignment','center','VerticalAlignment','middle');
					end
					
					if WeSignW(i)
						set(TheRect(i),'FaceColor',WsuccCol);
					else
						set(TheRect(i),'FaceColor',WfailCol);
					end
				end
			
				if inDiag(i)
					TheRect(i)=fill(XRec{i},YRec{i},[0.3 0.3 0.3],'LineWidth',2);
					hold on;
					%set(TheRect(i),'FaceColor','k');
				end
				
				
			end
			
			%axis labels and some cosmetics
			%..............................
			%Add some additional stuff
			set(obj.PlotAxis,'XTick',unique(Xpos),'XTickLabel',PlotNames,...
				'YTick',unique(Ypos),'YTickLabel',PlotNames,'FontSize',AxisFontSize,...
				'FontWeight','bold','LineWidth',AxisWidth,'Box','on');
			
			xlab=xlabel('T-Test');
			ylab=ylabel('W-Test');
			set([xlab,ylab],'FontSize',16,'FontWeight','bold');	
			
					
			%ylim([1 LineLength-1]);
		
			%Title      		
        		if ~isstr(obj.TestConfig{1}.ForecastTime)
				TheTitle= ['P-value matrix: ',obj.UserConfig.PlotTitle,',  ',datestr(obj.TestConfig{1}.ForecastTime)];
			else
				TheTitle= ['P-value matrix: ',obj.UserConfig.PlotTitle,',  ',obj.TestConfig{1}.ForecastTime];
			end
        		
			%and the title
			theTit=title(TheTitle);
			set(theTit,'FontSize',TitleFontSize,'FontWeight','bold','Interpreter','none');
			
			
			
			
			hold off
			
			%store stuff in fields
			obj.LegendEntries={};
			obj.PlotHandles=TheRect;		
        		
        		
        	end	
        		
        				
        	
        	
        	
        	
        end

      
        
         %methods 
        	%useful methods 
        %	function PlotHandle = PlotIt(obj,plotAxis,Results,TestConfig,UserConfig)
        		%just and all in one config and plot shortcut, normally it will
        		%be enough to the following two steps to plot: 1.create object
        		%2. call PlotIt with all needed data, to plot a test result
        		
        		%this function will be used by the plotmaker, so it should be
        		%changed if something additional has to be done.
        		
        %		obj.ConfigureTestConfig(TestConfig);
        %		obj.ConfigureUserConfig(UserConfig);
        %		obj.AddTestResults(Results);
        %		obj.SetPlotAxis(plotAxis);
        %		obj.TestReady=true;
        		
        %		iPlot=tic;
        %		obj.PlotResults;
        %		obj.CPUTimeData=toc(iPlot);
        		
        		
        %		PlotHandle=obj.PlotHandles;
        		
        		
        %	end
        	
        %end
        
end	

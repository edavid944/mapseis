classdef CombinedLike < mapseis.forecast_testing.TestPlotObj
	%A classic histogram based likelihood plot
	
	properties
		%PlotName %Name of the plot, not used as title.
        	%Version %Has to be a STRING, used to describe the version of the code
        	%PlotInfo %The place for a description about the plot 
        	
        	%PlotSelect %can be used if the plot uses more than on axis per test (see signMatPlot as example)		
        	%PlotAxis	%Axis used for the plots
        	%PlotHandles	%Handles of the different plot parts (optional, but suggested)
        	
        	%TestReady
        	%CPUTimeData
        	
        	%PlotReady signalizes that the plot object is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it is just the time
        	%needed for plotting
        	
        	TestResults
        	TestConfig
        	UserConfig
        	LegendEntries
        	
        end
    

       
        methods 
        
        	function obj = CombinedLike()
   		
        		obj.PlotName='CombinedLike';
              		obj.Version='v1';
              		
              		obj.PlotInfo=['This plots allows to plot multiple test results of likelihood-',...
              				'based tests into one bar-based plots. It is best used for combining',...
              				' the results of multiple likelihood based test for one forecast into ',...
              				'one single plot'];
              				
              		obj.PlotSelect = 1 
              		obj.PlotAxis = [];	
              		obj.PlotHandles	= [];
        	
              		obj.PlotReady = false;
              		obj.CPUTimeData	=[];	
              		
              		obj.TestResults=[];
              		obj.TestConfig=[];
              		
              		%default user config
              		DefaultUseConf=struct(	'histColor',[0.88 0.88 0.88],...
              					'orgColor','r',...
              					'meanColor','m',...
              					'medianColor','g',...
              					'plotOrg',true,...
              					'plotMean',false,...
              					'plotMedian',false,...
              					'ShowScoreDist',false,...
              					'NtestMode',false,...
              					'PlotTitle',[[]],...
              					'SingleForecast',true,...
              					'PaperBoy',true,...
              					'RoundIt',true);
              		%have to write something about the parameters later but just to not forget:
              		%if SingleForecast is true the labels of the tests only show the testname and 
			%not the additional forecasts name.			
              		
              		obj.UserConfig=DefaultUseConf;
              		obj.LegendEntries={};		
        	end
        	
        	
        	function ConfigureTestConfig(obj,TestConfig)
			%Used to configurate the part of the configuration needed from the
			%Test, e.g. Test Name, fields with the values, etc.
			%For each result used a test config will be submitted, by the 
			%plotmaker, means TestConfig will be a cell array with structures
			%for each result (e.g. {conf1, conf2, conf3,....}
			
			obj.TestConfig={};
			
			if ~iscell(TestConfig)
				obj.TestConfig{1}=TestConfig;
			else
				obj.TestConfig=TestConfig;
			end
			
        	end
        	
        	function ConfigureUserConfig(obj,UserConfig)
			%This is used to set the user configuration, which contain everything
			%not defined by the test, like colors, linewidth, etc. There is only
			%one UserConfig for all results used and some fields will always be 
			%submitted, no matter if the plot function is using it:
			%	PaperBoy:	if true the resulting graphic will be used
			%			in a print, hence lines should be a bit thicker
			%			and fonts a bit larger, but it is up to the plot
			%			function what it should do
			%Don't if there are more	
			obj.UserConfig=UserConfig;
			
        	end
        	
        	
        	function UserConfig = getUserConfig(obj)
        		%should return the current user configuration, mostly needed to get
        		%the default values
        		
        		UserConfig = obj.UserConfig;
        		
        	end	
        	
        	function FeatureList = getFeatures(obj)
			%This will be used by the TestSuite to determine what the model can 
			%be used for. The output should by structure with specific fieldnames
			%with mostly boolean values. 
			%Following keywords are available:
			%	MultiplePlot:		if true, the plot produces more than one
			%				plot for a single test, example sign. matrix
			%				plot function, which plots two plot "per test"
			%	No_Plots:		only needed if MultiplePlot is true, defines
			%				how many plots are produced
			%	Uncertain_mean:		Defines if the plot is able to plot Uncertainties
			%				with a mean value and variance (true if possible)
			%	Uncertain_median:	Defines if the plot is able to plot Uncertainties
			%				with a median Q16 and Q84 (true if possible)
			%	MultiTest:		True if more than one test per plot is allowed.
			%				If MultiTest is supported, TestConfig and Results will
			%				be submitted as cell array no matter if only one result 
			%				is used.
			%	SingleTest:		True if it is possible to plot only one test per
			%				plot
        	
			FeatureList = struct(	'MultiplePlot',false,...
						'No_Plots',1,...
						'Uncertain_mean',true,...
						'Uncertain_median',true,...
						'MultiTest',true,...
						'SingleTest',true);
        	end
        	
        	
        	function PossibleParameters = getUserParameterSetting(obj)
			%The method should return a description of the parameters needed
			%in the UserConfig and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be used for ConfigureTest
			%Each entry in structure should be another structure with following
			%Often the Plots do not need any configuration in this case the 
			%PossibleParameters can return empty
			
			
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			the selected string.
			%			
			%			
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			%WILL WRITE THAT LATER
			
		end	
        	
		
        	function SetPlotAxis(obj,plotAxis)
        		%sets the axis used for plotting
        		
        		if isempty(plotAxis)
        			obj.PlotAxis=gca;
        		else
        			obj.PlotAxis=plotAxis;
        		end
        		
        		
        	end
        	
        	
        	
        	function AddTestResults(obj,Results)
			%Used to submit the results of the tests, often more than one test is supported
			%in this case Results should be a cell array of the same size as TestConfig
			%(e.g. {Result1,Result2,Result3,...}
			
			obj.TestResults={};
			
			if ~iscell(Results)
				obj.TestResults{1}=Results;
			else
				obj.TestResults=Results;
			end
			
        	end
        	
        	
        	function PlotResults(obj)
        		%Will be used to start the plotting. The Handles of the different plot parts
        		%(lines,points, etc.) can stored in PlotHandles
        		
        		%NOT FINISHED
        		%I THINK I actually finished this, but it has to be TESTED
        		%to be sure.
        		
        		import mapseis.util.*;
        		import mapseis.forecast_testing.util.*;
        		
        		%hard wired colors and values for now.
        		FailCol='r';
        		SuccessCol='g';
        		MeanCol=[0.8 0.6 0.8];
        		MeanCol2=[0.8 0.2 0.8];
        		MedCol=[0.6 0.6 0.8];
        		MedCol2=[0.2 0.2 0.8];
        		
        		Transpar=0.5;
        		
        		TestSpacing=2;
      	  		HalfSpacer=TestSpacing/2;
      	  		LinCor=0.025;
        		
        		%grey color used in this plot
        		histColor=obj.UserConfig.histColor;
        		
        	
        		
        		
        		
        		
        		%define some plotting parameters
        		%...............................
        		if obj.UserConfig.PaperBoy
        			AxisWidth=3;
        			MainLineWidth=4;
        			ErrorLineWidth=2;
        			TitleFontSize=18;
        			AxisFontSize=16;
        			LabelFontSize=18;
        			WeightAxis='bold';
        			
        			TextSize=12;
        		else
        			AxisWidth=2;
        			MainLineWidth=3;
        			ErrorLineWidth=1;
        			TitleFontSize=16;
        			AxisFontSize=14;
        			TitleFontSize=16;
        			WeightAxis='demi';
        			
        			TextSize=11;
        			
        		end
        		
        		
        		
        		
        		
        		
        		%NewConsists snippets
        		
        		
        		%JUST Copy the data reformation part from minihist for the 
        		%moment and remove/add what is not needed or needed
        		%reorder the data
        		
        		%init of data 
        		indexLabel=cell(numel(obj.TestResults),1);
        		LikeOrgCat=NaN(numel(obj.TestResults),1);
        		
        		
        		
        		SynthMin=NaN(numel(obj.TestResults),1);
        		SynthMax=NaN(numel(obj.TestResults),1);
        		Quant=NaN(numel(obj.TestResults),1);
        		Passed=false(numel(obj.TestResults),1);
        		isTwoSide=false(numel(obj.TestResults),1);
        		QScorelow=NaN(numel(obj.TestResults),1);
        		QScorehi=NaN(numel(obj.TestResults),1);
        		
        		LikeOrgMean=NaN(numel(obj.TestResults),1);
        		LikeOrgSTD=NaN(numel(obj.TestResults),1);
        		QScorelowMean=NaN(numel(obj.TestResults),1);
        		QScorelowSTD=NaN(numel(obj.TestResults),1);
        		QScorehiMean=NaN(numel(obj.TestResults),1);
        		QScorehiSTD=NaN(numel(obj.TestResults),1);
        		
        		LikeOrgMedian=NaN(numel(obj.TestResults),1);
        		LikeOrgQuantL=NaN(numel(obj.TestResults),1);
        		LikeOrgQuantH=NaN(numel(obj.TestResults),1);
        		QScorelowMedian=NaN(numel(obj.TestResults),1);
        		QScorelowQuantL=NaN(numel(obj.TestResults),1);
        		QScorelowQuantH=NaN(numel(obj.TestResults),1);
        		QScorehiMedian=NaN(numel(obj.TestResults),1);
        		QScorehiQuantL=NaN(numel(obj.TestResults),1);
        		QScorehiQuantH=NaN(numel(obj.TestResults),1);
        		
        		LikeMonte=cell(numel(obj.TestResults),1);
        		QlowMonte=cell(numel(obj.TestResults),1);
        		QhiMonte=cell(numel(obj.TestResults),1);
        		
        		for i=1:numel(obj.TestResults)
				if obj.UserConfig.SingleForecast
					indexLabel{i} = [obj.TestConfig{i}.TestName];
				else				
					indexLabel{i} = [obj.TestConfig{i}.TestName,': ',obj.TestConfig{i}.ForecastName];
				end
				
				HistData=obj.TestResults{i}.(obj.TestConfig{i}.LogSynthCatField);
				LikeOrgCat(i)=obj.TestResults{i}.(obj.TestConfig{i}.LogCatField);
				SynthMin(i)=min(HistData);
				SynthMax(i)=max(HistData);
				
				
				Quant(i)=obj.TestResults{i}.SignVal;
				
				%Just calculated the rounded quantil passed anyway, does not take long
				%anyway.
				if obj.TestConfig{i}.OneSide
					isTwoSide(i)=false;
					QScorelow(i)=obj.TestResults{i}.(obj.TestConfig{i}.ScoreField_low);					
					RoundPass=(round(QScorelow(i)*200)/200)>=obj.TestResults{i}.SignVal;
				else
					isTwoSide(i)=true;
					QScorelow(i)=obj.TestResults{i}.(obj.TestConfig{i}.ScoreField_low);
					QScorehi(i)=obj.TestResults{i}.(obj.TestConfig{i}.ScoreField_hi);
					RoundPass=(round(QScorelow(i)*200)/200)>=(obj.TestResults{i}.SignVal/2)&...
						(round(QScorehi(i)*200)/200)>=(obj.TestResults{i}.SignVal/2);
					
				end			
				
				if obj.UserConfig.RoundIt
					%Passed has to be re-evaluated
					Passed(i)=RoundPass;
					
				else
					Passed(i)=obj.TestResults{i}.Passed;
				end
				
				if obj.TestConfig{i}.Uncertianties
					
					LikeMonte{i} = obj.TestResults{i}.(['single_',obj.TestConfig{i}.LogCatField]);
					
					if obj.TestConfig{i}.OneSide
						QlowMonte{i} = obj.TestResults{i}.(['single_',obj.TestConfig{i}.ScoreField_low]);
					else
						QlowMonte{i} = obj.TestResults{i}.(['single_',obj.TestConfig{i}.ScoreField_low]);
						QhiMonte{i} = obj.TestResults{i}.(['single_',obj.TestConfig{i}.ScoreField_hi]);
					end
					
					if obj.TestConfig{i}.MeanAvailable
						LikeOrgMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.LogCatField]);
						LikeOrgSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.LogCatField]);
				
						
						if obj.TestConfig{i}.OneSide
							QScorelowMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.ScoreField_low]);
							
						else
							QScorelowMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.ScoreField_low]);
							QScorehiMean(i)=obj.TestResults{i}.(['mean_',obj.TestConfig{i}.ScoreField_hi]);
							QScorehiSTD(i)=obj.TestResults{i}.(['std_',obj.TestConfig{i}.ScoreField_hi]);	
						
						end
					end
					
					
					if obj.TestConfig{i}.MedianAvailable
						LikeOrgMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.LogCatField]);
						LikeOrgQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.LogCatField])(1);
						LikeOrgQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.LogCatField])(2);
						
						if obj.TestConfig{i}.OneSide
							QScorelowMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(1);
							QScorelowQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(2);
							
						else
							QScorelowMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.ScoreField_low]);
							QScorelowQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(1);
							QScorelowQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_low])(2);
							QScorehiMedian(i)=obj.TestResults{i}.(['median_',obj.TestConfig{i}.ScoreField_hi]);
							QScorehiQuantL(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_hi])(1);
							QScorehiQuantH(i)=obj.TestResults{i}.(['quant68_',obj.TestConfig{i}.ScoreField_hi])(2);
						end
						
					end	
					
				end
				
		
			end
        		
        		       		
        								
			%for the N-test (do it for all, it does not need much time anyway)
			DistSynth=SynthMax-SynthMin;
			
			%the name ...Num is slightly misleading as it is only for the N-test
			%the number of earthquakes.
        		OrgNum = (LikeOrgCat-SynthMin)./DistSynth;
			MeanNum = (LikeOrgMean-SynthMin)/DistSynth;
			StdNum = (LikeOrgSTD-SynthMin)./DistSynth;
			MedNum = (LikeOrgMedian-SynthMin)./DistSynth;
			QuantNumL = (LikeOrgQuantL-SynthMin)./DistSynth;
			QuantNumH = (LikeOrgQuantH-SynthMin)./DistSynth;
			
			%could be done over cell function but it probably does not 
			%matter perfomancewise 
			DistNum=cell(size(LikeMonte));
			for i=1:numel(LikeMonte)
				if obj.TestConfig{i}.Uncertianties
					DistNum{i} = (LikeMonte{i}-SynthMin)./DistSynth;
				end	
			end
			
			
			
			
			%This is to avoid invisible lines
			OrgNum(OrgNum<=0.01)=0.01;
			OrgNum(OrgNum>=0.99)=0.99;
				
			QScorelow(QScorelow<=0.01)=0.01;
			QScorelow(QScorelow>=0.99)=0.99;
			
			QScorehi(QScorehi<=0.01)=0.01;
			QScorehi(QScorehi>=0.99)=0.99;
			
			%handle list to be stored
        		handle=[];
        		legendentry={};
			
        		
        		%build raw plot
        		disp(Passed)
        		plotyPos=InitConsPlot(obj.PlotAxis,numel(obj.TestResults),TestSpacing,Quant,isTwoSide,Passed);
        		hold on;
        		
        		for i=1:numel(obj.TestResults)
        		
        		
        			if obj.UserConfig.plotOrg
        				if obj.TestConfig{i}.OneSide
						OrgLineX=[QScorelow(i),QScorelow(i)];
						OrgLineY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
						
						OrgLine=plot(OrgLineX,OrgLineY,'k');
						
						
						%if Testdat.QScoreMonte(1)>=Quant
						if Passed(i)
							set(OrgLine,'LineWidth',5,'Color',SuccessCol);
						else
							set(OrgLine,'LineWidth',5,'Color',FailCol);
						end
				
							
						

					else
						if obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							OrgLineX=[OrgNum(i),OrgNum(i)];
							OrgLineY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							OrgLine=plot(OrgLineX,OrgLineY,'k');
							
							
							if Passed(i)
								set(OrgLine,'LineWidth',5,'Color',SuccessCol);
							else
								set(OrgLine,'LineWidth',5,'Color',FailCol);
							end
						else
						
							OrgLineLowX=[QScorelow(i),QScorelow(i)]
							OrgLineLowY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							OrgLineHighX=[QScoreHi(i),QScoreHi(i)]
							OrgLineHighY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							OrgLineL=plot(OrgLineLowX,OrgLineLowY,'k');
							OrgLineH=plot(OrgLineHighX,OrgLineHighY,'k');
							
							
							if Passed(i)
								set([OrgLineL,OrgLineH],'LineWidth',5,'Color',SuccessCol);
							else
								set([OrgLineL,OrgLineH],'LineWidth',5,'Color',FailCol);
							end
						
						
							
						end
						
					
					end
				end
        		
				
								
				if obj.UserConfig.plotMean&obj.TestConfig{i}.MeanAvailable
        				if obj.TestConfig{i}.OneSide
						MeanBoxX=[QScorelowMean(i)-QScorelowSTD(i),QScorelowMean(i)+QScorelowSTD(i),...
							QScorelowMean(i)+QScorelowSTD(i),QScorelowMean(i)-QScorelowSTD(i),...
							QScorelowMean(i)-QScorelowSTD(i)];
						MeanBoxY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
							(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
							(i-1)*TestSpacing+LinCor];
						MeanLineX=[QScorelowMean(i),QScorelowMean(i)];
						MeanLineY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
						%plot it
						Mnboxer=fill(MeanBoxX,MeanBoxY,MeanCol);
						set(Mnboxer,'FaceAlpha',Tranpa);
						MnLine=plot(MeanLineX,MeanLineY,'m');
						set(MnLine,'LineWidth',2);
							
						

					else
						if obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							MeanBoxX=[MeanNum(i)-StdNum(i),MeanNum(i)+StdNum(i),MeanNum(i)+StdNum(i),...
								MeanNum(i)-StdNum(i),MeanNum(i)-StdNum(i)];
							MeanBoxY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
								(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
								(i-1)*TestSpacing+LinCor];
							MeanLineX=[MeanNum(i),MeanNum(i)];
							MeanLineY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							%plot it
							Mnboxer=fill(MeanBoxX,MeanBoxY,MeanCol);
							set(Mnboxer,'FaceAlpha',Tranpa);
							MnLine=plot(MeanLineX,MeanLineY,'m');
							set(MnLine,'LineWidth',2);	
						
						
						else
							%Lower Quantil
							MeanBoxLowX=[QScorelowMean(i)-QScorelowSTD(i),QScorelowMean(i)+QScorelowSTD(i),...
								QScorelowMean(i)+QScorelowSTD(i),QScorelowMean(i)-QScorelowSTD(i),...
								QScorelowMean(i)-QScorelowSTD(i)];
							MeanBoxLowY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
								(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
								(i-1)*TestSpacing+LinCor];
							MeanLineLowX=[QScorelowMean(i),QScorelowMean(i)];
							MeanLineLowY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];	
							
							%Higher Quantil
							MeanBoxHighX=[QScorehiMean(i)-QScorehiSTD(i),QScorehiMean(i)+QScorehiSTD(i),...
								QScorehiMean(i)+QScorehiSTD(i),QScorehiMean(i)-QScorehiSTD(i),...
								QScorehiMean(i)-QScorehiSTD(i)];
							MeanBoxHighY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
								(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
								(i-1)*TestSpacing+LinCor];
							MeanLineHighX=[QScorehiMean(i),QScorehiMean(i)];
							MeanLineHighY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							
							%plot it
							MnboxerL=fill(MeanBoxLowX,MeanBoxLowY,MeanCol);
							set(MnboxerL,'FaceAlpha',Tranpa);
							MnLineL=plot(MeanLineLowX,MeanLineLowY,'p');
							set(MnLineL,'LineWidth',2);
							
							MnboxerH=fill(MeanBoxHighX,MeanBoxHighY,MeanCol);
							set(MnboxerH,'FaceAlpha',Tranpa);
							MnLineH=plot(MeanLineHighX,MeanLineHighY,'m');
							set(MnLineL,'LineWidth',2);	
						
						
						end
						
					
					end
				end
				
			
				if obj.UserConfig.plotMedian&obj.TestConfig{i}.MedianAvailable
        				if obj.TestConfig{i}.OneSide
						MedBoxX=[QScorelowQuantL(i),QScorelowQuantH(i),QScorelowQuantH(i),...
							QScorelowQuantL(i),QScorelowQuantL(i)];
						MedBoxY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
							(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
							(i-1)*TestSpacing+LinCor];
						MedLineX=[QScorelowMedian(i),QScorelowMedian(i)];
						MedLineY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
						%plot it
						Mdboxer=fill(MedBoxX,MedBoxY,MedCol);
						set(Mdboxer,'FaceAlpha',Tranpa);
						MdLine=plot(MedLineX,MedLineY,'b');
						set(MdLine,'LineWidth',2);
							
						

					else
						if obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							MedBoxX=[QuantNumL(i),QuantNumH(i),QuantNumH(i),QuantNumL(i),QuantNumL(i)];
							MedBoxY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
								(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
								(i-1)*TestSpacing+LinCor];
							MedLineX=[MedNum(i),MedNum(i)];
							MedLineY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							%plot it
							Mdboxer=fill(MedBoxX,MedBoxY,MedCol);
							set(Mdboxer,'FaceAlpha',Tranpa);
							MdLine=plot(MedLineX,MedLineY,'b');
							set(MdLine,'LineWidth',2);
							
						else
							%Lower Quantil
							MedBoxLowX=[QScorelowQuantL(i),QScorelowQuantH(i),QScorelowQuantH(i),...
								QScorelowQuantL(i),QScorelowQuantL(i)];
							MedBoxLowY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
								(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
								(i-1)*TestSpacing+LinCor];
							MedLineLowX=[QScorelowMedian(i),QScorelowMedian(i)];
							MedLineLowY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							%Higher Quantil
							MedBoxHighX=[QScorehiQuantL(i),QScorehiQuantH(i),QScorehiQuantH(i),...
									QScorehiQuantL(i),QScorehiQuantL(i)];
							MedBoxHighY=[(i-1)*TestSpacing+LinCor,(i-1)*TestSpacing+LinCor,...
								(i)*TestSpacing-LinCor,(i)*TestSpacing-LinCor,...
								(i-1)*TestSpacing+LinCor];
							MedLineHighX=[QScorehiMedian(i),QScorehiMedian(i)];
							MedLineHighY=[(i-1)*TestSpacing+LinCor,(i)*TestSpacing-LinCor];
							
							%plot it
							MdboxerL=fill(MedBoxLowX,MedBoxLowY,MedCol);
							set(MdboxerL,'FaceAlpha',Tranpa);
							MdLineL=plot(MedLineLowX,MedLineLowY,'b');
							set(MdLineL,'LineWidth',2);
							
							MdboxerH=fill(MedBoxHighX,MedBoxHighY,MedCol);
							set(MdboxerH,'FaceAlpha',Tranpa);
							MdLineH=plot(MedLineHighX,MedLineHighY,'b');
							set(MdLineH,'LineWidth',2);
						end
						
					
					end
				end
        		
				
				if obj.UserConfig.ShowScoreDist & obj.TestConfig{i}.Uncertianties
					if obj.TestConfig{i}.OneSide
						%Build a scale for the hist
						Scaler=linspace(0,1,50);
						[hi bi]=hist(QlowMonte{i},Scaler);
						
						notZero=hi~=0;
						bi=bi(notZero);
						hi=hi(notZero);
						
						%norm hi to TestSpacing-0.1
						normhi=((hi-min(hi))./(abs(max(hi)-min(hi)))).*(TestSpacing-0.1);
						
						myStair=stairs(bi,normhi+(i-1)*TestSpacing);
						
						set(myStair,'LineStyle','--','LineWidth',1,'Color','b');
						
					else
						if obj.UserConfig.NtestMode & obj.TestConfig{i}.NtestLike
							%Build a scale for the hist
							Scaler=((SynthMin:SynthMax)-SynthMin)./DistSynth(i);
							[hi bi]=hist(DistNum{i},Scaler);
							
							notZero=hi~=0;
							bi=bi(notZero);
							hi=hi(notZero);
							
							%norm hi to TestSpacing-0.1
							normhi=((hi-min(hi))./(abs(max(hi)-min(hi)))).*(TestSpacing-0.1);
							
							myStair=stairs(bi,normhi+(i-1)*TestSpacing);
							
							set(myStair,'LineStyle','--','LineWidth',1,'Color','b');
							
						else
							%Build a scale for the hist
							Scaler=linspace(0,1,50);
							[hiLow biLow]=hist(QlowMonte{i},Scaler);
							[hiHigh biHigh]=hist(QhiMonte{i},Scaler);
							
							notZeroLow=hiLow~=0;
							biLow=biLow(notZeroLow);
							hiLow=hiLow(notZeroLow);
							
							notZeroHigh=hiHigh~=0;
							biHigh=biHigh(notZeroHigh);
							hiHigh=hiHigh(notZeroHigh);
							
							%norm hi to TestSpacing-0.1
							normhiLow=((hiLow-min(hiLow))./(abs(max(hiLow)-min(hiLow)))).*(TestSpacing-0.1);
							normhiHigh=((hiHigh-min(hiLow))./(abs(max(hiHigh)-min(hiHigh)))).*(TestSpacing-0.1);
							
							myStairLow=stairs(biLow,normhiLow+(i-1)*TestSpacing);
							myStairHigh=stairs(biHigh,normhiHigh+(i-1)*TestSpacing);
							
							set([myStairHigh],'LineStyle','--','LineWidth',1,'Color','b');
							set([myStairLow],'LineStyle','--','LineWidth',1,'Color','g');
							
						end
					end
				
				end
        		
        		end
			
			
			%Title      		
        		if ~isstr(obj.TestConfig{1}.ForecastTime)
				TheTitle= [obj.UserConfig.PlotTitle,':  ',datestr(obj.TestConfig{1}.ForecastTime)];
			else
				TheTitle= [obj.UserConfig.PlotTitle,':  ',obj.TestConfig{1}.ForecastTime];
			end
        		
			
			%and the title
			theTit=title(obj.PlotAxis,TheTitle);
			set(theTit,'FontSize',TitleFontSize,'FontWeight','bold','Interpreter','none');
			
			
			
			%axis labels and some cosmetics
			%..............................
			
			xlab=xlabel('Score %   ');
			
			set(obj.PlotAxis,'YTick',plotyPos,'YTickLabel',indexLabel);
			
			set([xlab],'FontSize',LabelFontSize,'FontWeight','bold');
			set(obj.PlotAxis,'LineWidth',AxisWidth,'FontSize',AxisFontSize,'FontWeight',WeightAxis);
			
			hold off
			
			%store stuff in fields
			obj.LegendEntries=legendentry;
			obj.PlotHandles=handle;		
        		
        		
        	end		
        		
        				
        	
        	
        	
        	
        end

      
        
         %methods 
        	%useful methods 
        %	function PlotHandle = PlotIt(obj,plotAxis,Results,TestConfig,UserConfig)
        		%just and all in one config and plot shortcut, normally it will
        		%be enough to the following two steps to plot: 1.create object
        		%2. call PlotIt with all needed data, to plot a test result
        		
        		%this function will be used by the plotmaker, so it should be
        		%changed if something additional has to be done.
        		
        %		obj.ConfigureTestConfig(TestConfig);
        %		obj.ConfigureUserConfig(UserConfig);
        %		obj.AddTestResults(Results);
        %		obj.SetPlotAxis(plotAxis);
        %		obj.TestReady=true;
        		
        %		iPlot=tic;
        %		obj.PlotResults;
        %		obj.CPUTimeData=toc(iPlot);
        		
        		
        %		PlotHandle=obj.PlotHandles;
        		
        		
        %	end
        	
        %end
        
end	

function initResultStruct(obj)
	%builds and empty Result structur for storing the results
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	if ~all(obj.CurrentState)
		disp('Test Suite not fully configurated to calculate')
		return;
	end
	
	
	TestResults={};
	Times=obj.TimeList;
	
	for i=1:numel(Times)
		ResultStruct=struct(	'ForecastTime',Times(i),...
		'ForecastLength',obj.ForecastLength(i),...
		'Empty',true);
		TestResults{i}=ResultStruct;			
		
	end
	
	obj.TestResults=TestResults;


end
function addProject(obj,ProjectObj)
	%adds a forecast project to the TestSuite
	%currently only one Project object per TestSuiteObject 
	%is allowed, but a merger should be built and added.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.filter.*;
	
	%store project
	obj.ForecastProject=ProjectObj;
	
	%copy and update meta data
	obj.MetaData=obj.ForecastProject.MetaData;
	obj.MetaData.OriginalCatalog=true;
	
	%get the interal catalog and filterlist
	obj.Datastore=obj.ForecastProject.Datastore;
	       	
	%check if realtime
	obj.RealTimeTest=strcmp(obj.ForecastProject.PredictionIntervall,'realtime');
	%default for NormRealTime is in the moment false, at least from the object side.
	
	%get minimum magnitude and set filter to that value -0.3
	%Same for the depth but with +10
	RegConfig=obj.ForecastProject.buildRegionConfig;
	MinMag=RegConfig.MinMagnitude-0.3;
	MaxMag=RegConfig.MaxMagnitude+0.3;
	MinDepth=RegConfig.MinDepth-1;
	MaxDepth=RegConfig.MaxDepth+10;
	
	Range='in';
	
	obj.Filterlist=FilterListCloner(obj.ForecastProject.Filterlist);
	MagFilt=obj.Filterlist.getByName('Magnitude');
	MagFilt.setRange(Range,[MinMag MaxMag]);
	DepthFilt=obj.Filterlist.getByName('Depth');
	DepthFilt.setRange(Range,[MinDepth MaxDepth]);
	
	obj.CurrentState(1)=true;
	       		
	%generate model and time list:
	obj.generateModTime;
	
	
	obj.Status='config';
end

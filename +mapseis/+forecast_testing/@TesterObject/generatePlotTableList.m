function generatePlotTableList(obj)
	%generates a list with all possible plots and tables for
	%all tests. It is probably a structure with test names as
	%fields, each containing some cell arrays with name of plot
	%(or table) and some handle or keyword as well as possible
	%needed config
	%The list will later be needed by PlotMaker and TableMaker
	%as well as a possible PlotViewer.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	possiblePlots=struct;
	possibleTables=struct;
	
	
	for i=1:numel(obj.TestNames)
		%get test
		Name_of_test=obj.TestNames{i};
		TheTest=obj.TestPlugInList.(Name_of_test); 
		
		%first the plots
		possiblePlots.(Name_of_test)=TheTest.getPlotFunctions;
		
		
		%second the tables
		possibleTables.(Name_of_test)=TheTest.getTablesFunctions;
		
	end
	
	obj.possiblePlots=possiblePlots;
	obj.possibleTables=possibleTables;
	

end
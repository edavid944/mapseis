function setTestSelection(obj,WhichTest,ModTimeSelection)
	%used to select the parts which should be tested
	%instead of a logical matrix determine which parts
	%should be calculated, the keyword 'all' could be used
	%test everything
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	if ~obj.CurrentState(3)
		disp('no test added yet')
		return;
	end
	
	if isstr(ModTimeSelection)
		%only one case at the moment, but I have the 
		%feeling there will be more 
		switch ModTimeSelection
			case 'all'
				ModTimeSelection=obj.AvailableModTime;
			case 'none'
				ModTimeSelection=false(size(obj.AvailableModTime));
		end	
	
	
	
	else
		if ~all(size(obj.SelectedModTime)==size(ModTimeSelection))
			disp('Dimension of the input matrix not correct');
			return
		end
		
		
		ModTimeSelection=logical(ModTimeSelection);
	
	end
	
	
	regTestName=fieldnames(obj.TestPlugInList);
	if isstr(WhichTest)
		if strcmp(WhichTest,'all')
			for i=1:numel(regTestName)
				obj.SelectedModTime.(regTestName{i})=ModTimeSelection;
			end
		
		else
			obj.SelectedModTime.(WhichTest)=ModTimeSelection;
		end
	
	else
		for i=1:numel(WhichTest)
			obj.SelectedModTime.(WhichTest{i})=ModTimeSelection;
		end
	
	end
	
	
	obj.CurrentState(2)=true;
	
end

function startCalc(obj)
	%starts the calculation run

	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if ~all(obj.CurrentState)
		disp('Test Suite not fully configurated to calculate')
		return;
	end
	
	%to keep compabiltiy with old projects
	if isempty(obj.ParentParallelMode)
		obj.ParentParallelMode=false;
	end
	
	if isempty(obj.LoopMode)
		obj.LoopMode=false;
	end	
	
	
	
	if ~obj.LoopMode
		%set the recursion limit to a high enough number (matlab will stop
		%the calculation later if this is not done
		NrNeeded=numel(obj.WorkList(:,1))+500; 
		set(0,'RecursionLimit',NrNeeded);
	end
		
	%parallel mode is probably best in the case of monte carlo processing
	%but it can as well speed up the other calculations
	if obj.ParallelMode | obj.ParentParallelMode
		try
			matlabpool open
		end	
	end
	
	%built empty Data structur
	obj.initResultStruct;
	
	
	obj.Status='started';
	
	if ~obj.LoopMode
		%start calculation
		CurNr=obj.NextCalc;
		obj.NextCalc=obj.NextCalc+1;
		obj.contCalc(CurNr);
	
	else
		if ~obj.ParentParallelMode
			obj.LoopItSerial;
		else
			obj.ParallelMode=false;
			obj.LoopItParallel;
		end
	
	end

end
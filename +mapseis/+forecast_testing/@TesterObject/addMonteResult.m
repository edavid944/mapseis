function TestResult = addMonteResult(obj,IDX,oldTestResult,CurTestResults,ResDef)
	%Adds new results to a previous TestResult structure, it is just outsourced
	%from the main calculation, to make it more readable.
	
	%'normal' will not be considered, it should be replaced by buildMonteResult
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	TheFields=fieldnames(oldTestResult);
	TestResult=oldTestResult;
	
	  		
	for i=1:numel(TheFields)
	
		switch ResDef.(TheFields{i}){2}
		
			case 'normal'
				disp('there still is a "normal" field in the Result definition');
			
			case 'ignore'
				%nothing has to be done
			
			case 'stats'
				MonteFieldName=['single_',TheFields{i}];
				TestResult.(MonteFieldName)(IDX)=CurTestResults.(TheFields{i});
			
			case 'collect'
				MonteFieldName=['single_',TheFields{i}];
				TestResult.(MonteFieldName){IDX,1}=CurTestResults.(TheFields{i});
			
		end	
	
	end

end
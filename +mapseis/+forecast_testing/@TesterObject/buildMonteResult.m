function [TestResult corResDef] = buildMonteResult(obj,OrgTestResults,ResDef)
	%builds an initial TestResult structure with the results
	%from the original run and the suitable arrays for the the
	%monte-carlo results.
	%corResDef is the same as RefDef, but 'normal' is replaced according to the
	%data format
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	TheFields=fieldnames(OrgTestResults);
	TestResult=struct;
	
	NrMonti=obj.TestConfig.Nr_Monte_Catalogs;
	
	for i=1:numel(TheFields)
		
		switch ResDef.(TheFields{i}){2}
			case 'normal'
				switch ResDef.(TheFields{i}){1}
					case 'scalar'
						orgFieldName=['org_',TheFields{i}];
						MonteFieldName=['single_',TheFields{i}];
						
						TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
						TestResult.(MonteFieldName)=zeros(NrMonti,1);
										
						ResDef.(TheFields{i}){2}='stats';
						corResDef.(TheFields{i})=ResDef.(TheFields{i});
						
						
					case {'array','string','cell'}
						orgFieldName=['org_',TheFields{i}];
						MonteFieldName=['single_',TheFields{i}];
						
						TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
						TestResult.(MonteFieldName)=cell(NrMonti,1);
						
						ResDef.(TheFields{i}){2}='collect';
						corResDef.(TheFields{i})=ResDef.(TheFields{i});	
				end
	
	
			case 'ignore'
				TestResult.(TheFields{i})=OrgTestResults.(TheFields{i});
				corResDef.(TheFields{i})=ResDef.(TheFields{i});
			
			case 'stats'
				orgFieldName=['org_',TheFields{i}];
				MonteFieldName=['single_',TheFields{i}];
				
				TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
				TestResult.(MonteFieldName)=zeros(NrMonti,1);
				
				corResDef.(TheFields{i})=ResDef.(TheFields{i});
				
			case 'collect'
				orgFieldName=['org_',TheFields{i}];
				MonteFieldName=['single_',TheFields{i}];
				
				TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
				TestResult.(MonteFieldName)=cell(NrMonti,1);
				
				corResDef.(TheFields{i})=ResDef.(TheFields{i});
				
		end	
	
	end

end
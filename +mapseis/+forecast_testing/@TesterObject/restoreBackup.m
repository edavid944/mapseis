function restoreBackup(obj)
	%restores previous saved results. The Forecasts have to be
	%added seperately, before calling this function 
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	try
		load(obj.SavePath);
			
			obj.TestConfig=TestingBackup.TestConfig;
			obj.TestResults=TestingBackup.TestResults;
			obj.SingleTestConfig=TestingBackup.SingleTestConfig;
			obj.WorkList=TestingBackup.WorkList;
			obj.WorkRankArray=TestingBackup.WorkRankArray;
			obj.WorkDone=TestingBackup.WorkDone;
			obj.NextCalc=TestingBackup.NextCalc;
		
	catch
		disp('no save file exists');
	end
	
end

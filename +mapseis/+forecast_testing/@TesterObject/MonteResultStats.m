function TestResult = MonteResultStats(obj,oldTestResult,ResDef)
	%Does the wanted statistics for the uncertainty estimation, it is just outsourced
	%from the main calculation, to make it more readable.
	
	%'normal' will not be considered, it should be replaced by buildMonteResult
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	
	TheFields=fieldnames(oldTestResult);
	TestResult=oldTestResult;
	
	%at the moment using not the "nan" version of mean and std, may change if needed 
	
	%config values
	NrMonti=obj.TestConfig.Nr_Monte_Catalogs;
	doMean=obj.TestConfig.CalcMean;
	doMedian=obj.TestConfig.CalcMedian;
	
	for i=1:numel(TheFields)
		
		switch ResDef.(TheFields{i}){2}
			case 'normal'
				disp('there still is a "normal" field in the Result definition');
			
			case 'ignore'
				%nothing has to be done
			
			case 'stats'
				MonteFieldName=['single_',TheFields{i}];
				
				if doMean
					MeanFieldName=['mean_',TheFields{i}];
					stdFieldName=['std_',TheFields{i}];
					
					TestResult.(MeanFieldName)=mean(oldTestResult.(MonteFieldName));
					TestResult.(stdFieldName)=std(oldTestResult.(MonteFieldName));
				end
				
				
				if doMedian
					MedianFieldName=['median_',TheFields{i}];
					QuantFieldName=['quant68_',TheFields{i}];
					
					TheQuant=quantile(oldTestResult.(MonteFieldName),[0.16,0.5,0.84]);
					TestResult.(MedianFieldName)=TheQuant(2);
					TestResult.(QuantFieldName)=[TheQuant(1),TheQuant(3)];
				end
				
				
			case 'collect'
				%nothing to do 
			
		end	
		
	end

end
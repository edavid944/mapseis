function generateWorkList(obj)
	%builds the worklist with the parts which have to be done
	%can only be used after selection has been done and plugins
	%are added
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if ~all(obj.CurrentState(1:3))
		disp('Test Suite not fully configurated to generate Worklist')
		return;
	end
	
	%work it test-by-test
	TestNames=fieldnames(obj.TestPlugInList);
	obj.TestNames=TestNames; %to ensure that  it will always be the same
	
	
	AllModelID=1:numel(obj.ModelList(:,1));
	
	WorkList=[];
	WorkRankArray={};
	
	%what are the forecasts
	isPoisson = strcmp(obj.ModelList(:,3),'Poisson');
	isNegBin = strcmp(obj.ModelList(:,3),'NegBin');
	isCustom = strcmp(obj.ModelList(:,3),'Custom');
	
	DistroNr = zeros(size(isCustom));
	DistroNr(isPoisson) = 1;
	DistroNr(isNegBin) = 2;
	DistroNr(isCustom) = 3;
	
	for i=1:numel(TestNames)
		CurrentSelList = obj.SelectedModTime.(TestNames{i}) & obj.AvailableModTime;
	
		if any(CurrentSelList)
			CurTest=obj.TestPlugInList.(TestNames{i});
			CurFeatures=CurTest.getFeatures;
			isSingleTest=strcmp(CurFeatures.TestType,'single');
	
			%have to check criterium
			checkitout=(~(CurFeatures.supportPoisson&CurFeatures.supportNegBin&CurFeatures.supportCustom)&isSingleTest)|...
			(~(CurFeatures.supportPoisson&CurFeatures.supportNegBin&CurFeatures.supportCustom&CurFeatures.MixedDistro)&~isSingleTest);
		
	
	
	
			switch CurFeatures.TestType
				case 'single'
					%check which forecasts are compatible with test
					if checkitout
						if ~CurFeatures.supportPoisson	
							if any(isPoisson)
								inCompIDX=find(isPoisson);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
						
						if ~CurFeatures.supportNegBin	
							if any(isNegBin)
								inCompIDX=find(isNegBin);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
						
						if ~CurFeatures.supportCustom	
							if any(isCustom)
								inCompIDX=find(isCustom);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
						
					end
						
					if any(CurrentSelList)
						[TimeID ModelID] = find(CurrentSelList);
						%[ModelID TimeID] = find(CurrentSelList);
						
						for j=1:numel(TimeID)
							WorkList(end+1,:)=[i,TimeID(j),ModelID(j),-1];
						end
						
					end
	
	
				case 'comparison'
					%check which forecasts are compatible with test
					if checkitout
						%just a problem with some distros
						%kick out not supported Distros
						if ~CurFeatures.supportPoisson	
							if any(isPoisson)
								inCompIDX=find(isPoisson);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
					
						if ~CurFeatures.supportNegBin	
							if any(isNegBin)
								inCompIDX=find(isNegBin);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
					
						if ~CurFeatures.supportCustom	
							if any(isCustom)
								inCompIDX=find(isCustom);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
					
						if any(CurrentSelList)
							[TimeID ModelID] = find(CurrentSelList);
						
							if CurFeatures.MixedDistro
								%Mixed distros are supported
					
								if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
									%use the symmetry
									for j=1:numel(TimeID)
										NotMySelf=AllModelID(ModelID(j)~=AllModelID);
										ExistAsWell=CurrentSelList(TimeID,NotMySelf);
										
										
										if any(ExistAsWell)
											NotMySelf=NotMySelf(ExistAsWell(j,:));
				
											for k=NotMySelf
												alreadyExist=any(WorkList(:,1)==i&WorkList(:,2)==TimeID(j)&...
												WorkList(:,3)==AllModelID(k)&WorkList(:,4)==ModelID(j));
												if ~alreadyExist
													WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
												end
											end	
										end
									end
					
								else
									%no symmetry wished or supported
									for j=1:numel(TimeID)
										NotMySelf=AllModelID(ModelID(j)~=AllModelID);
										ExistAsWell=CurrentSelList(TimeID,NotMySelf);
									
										if any(ExistAsWell)
											NotMySelf=NotMySelf(ExistAsWell(j,:));
											
											for k=NotMySelf
												WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
											end	
										end	
									end
								end
					
							else
								%does not like mixed distros
								if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
									for j=1:numel(TimeID)
										NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
										ExistAsWell=CurrentSelList(TimeID,NotMySelf);
									
										if any(ExistAsWell)
											NotMySelf=NotMySelf(ExistAsWell(j,:));
								
											for k=NotMySelf
												alreadyExist=any(WorkList(:,1)==i&WorkList(:,2)==TimeID(j)&...
												WorkList(:,3)==AllModelID(k)&WorkList(:,4)==ModelID(j));
												if ~alreadyExist
													WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
												end
											end	
					
										end
					
									end
					
								else
									for j=1:numel(TimeID)
										NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
										ExistAsWell=CurrentSelList(TimeID,NotMySelf);
									
										if any(ExistAsWell)
											NotMySelf=NotMySelf(ExistAsWell(j,:));
											
											for k=NotMySelf
												WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
											end	
						
										end
									end
								end
					
							end
					
					
						end
					
					else
						%no problem, everything goes
						[TimeID ModelID] = find(CurrentSelList);
						
						if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
							%use symmetry of the test
							for j=1:numel(TimeID)
								NotMySelf=AllModelID(ModelID(j)~=AllModelID);
								ExistAsWell=CurrentSelList(TimeID,NotMySelf);
							
								if any(ExistAsWell)
									NotMySelf=NotMySelf(ExistAsWell(j,:));
								
									for k=NotMySelf
										if ~isempty(WorkList)
											alreadyExist=any(WorkList(:,1)==i&WorkList(:,2)==TimeID(j)&...
											WorkList(:,3)==AllModelID(k)&WorkList(:,4)==ModelID(j));
										else
											alreadyExist=false;
										end	
										
										if ~alreadyExist
											WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
										end
									end	
								end	
						
							end
						
						else
							%symmtry not wanted or allowed
							
							for j=1:numel(TimeID)
								NotMySelf=AllModelID(ModelID(j)~=AllModelID);
								ExistAsWell=CurrentSelList(TimeID,NotMySelf);
								
								if any(ExistAsWell)
									NotMySelf=NotMySelf(ExistAsWell);
									for k=NotMySelf
										WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
									end	
								end	
							end
						end
					end
	
	
				case 'ranking'
					%check which forecasts are compatible with test
					if checkitout
						if ~CurFeatures.supportPoisson	
							if any(isPoisson)
								inCompIDX=find(isPoisson);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
					
						if ~CurFeatures.supportNegBin	
							if any(isNegBin)
								inCompIDX=find(isNegBin);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
						
						if ~CurFeatures.supportCustom	
							if any(isCustom)
								inCompIDX=find(isCustom);
								CurrentSelList(:,inCompIDX)=false;
							end
						end
					end
				
				
					
					if any(CurrentSelList)
						[TimeID ModelID] = find(CurrentSelList);
						
						if CurFeatures.MixedDistro
							%mixing of distribution is allowed
							
							if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
								for j=1:numel(TimeID)
									potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
					
									NotMySelf=AllModelID(ModelID(j)~=AllModelID);
									ExistAsWell=CurrentSelList(TimeID,NotMySelf);
									
									if any(ExistAsWell)
										NotMySelf=NotMySelf(ExistAsWell);
										
										alreadyExist=false;
										CurList=[ModelID,NotMySelf];
										
										for k=potIdent
									
											SomethingDiff=setdiff(WorkRankArray(k),CurList);
											
											if ~isempty(SomethingDiff)
												alreadyExist=true;
												break;
											end
										end
				
										if ~alreadyExist
											WorkRankArray{end+1,1}=CurList;
											WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
										end
									end				
								end
				
							else
								for j=1:numel(TimeID)
									potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
									
									NotMySelf=AllModelID(ModelID(j)~=AllModelID);
									ExistAsWell=CurrentSelList(TimeID,NotMySelf);
								
									if any(ExistAsWell)
										NotMySelf=NotMySelf(ExistAsWell);
										
										alreadyExist=false;
										CurList=[ModelID,NotMySelf];
				
										for k=potIdent
											WorkRankArray{end+1,1}=CurList;
											WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
										end	
									end	
								end
							end
	
						else
							%no mixed testing possible
							if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
								for j=1:numel(TimeID)
									potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
								
									NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
									ExistAsWell=CurrentSelList(TimeID,NotMySelf);
									
									if any(ExistAsWell)
										NotMySelf=NotMySelf(ExistAsWell);
									
										alreadyExist=false;
										CurList=[ModelID,NotMySelf];
										
										for k=potIdent
											SomethingDiff=setdiff(WorkRankArray(k),CurList);
											
											if ~isempty(SomethingDiff)
												alreadyExist=true;
												break;
											end
										end
	
										if ~alreadyExist
											WorkRankArray{end+1,1}=CurList;
											WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
										end
	
									end
								end
								
							else
								for j=1:numel(TimeID)
									potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
									
									NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
									ExistAsWell=CurrentSelList(TimeID,NotMySelf);
									
									if any(ExistAsWell)
										NotMySelf=NotMySelf(ExistAsWell);
										
										alreadyExist=false;
										CurList=[ModelID,NotMySelf];
										
										for k=potIdent
											WorkRankArray{end+1,1}=CurList;
											WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
										end	
									end	
								end
							end	
	
						end
		
					end
	
				end
		
			end
	
	end
	
	obj.WorkRankArray=WorkRankArray;
	obj.WorkList=WorkList;
	obj.NextCalc=1;
	
	obj.CurrentState(4)=true;
	obj.Status='ready';
end

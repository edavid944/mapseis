function NormCast = NormForecast(obj,TheCast,TimeLength,ForecastType)
	%normalizes the forecast to the timelength (used for realtime mode)
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	%factor to norm
	normFact=TimeLength/obj.OriginalLength;
	
	switch ForecastType
		case 'Poisson'
			TheCast(:,9)=TheCast(:,9)*normFact;
		
		case 'NegBin'
			%to be tested
			
			ExpValue = ((1-TheCast(:,11)).*TheCast(:,9))./TheCast(:,11);
			VarValue = ((1-TheCast(:,11)).*TheCast(:,9))./(TheCast(:,11).^2);
			CorExp = ExpValue*normFact;
			newP = CorExp./VarValue;
			newR = (newP.*CorExp)./(1-newP);
			TheCast(:,9)=newR;
			TheCast(:,11)=newP;
		
		case 'Custom'
			error('Custom distribution currently not supported for normalization');
	end
		
	NormCast = TheCast;

end
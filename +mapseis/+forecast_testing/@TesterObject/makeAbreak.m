function makeAbreak(obj,ListNr)
	%just an experiment, done to kept memory in order.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if obj.SaveRes
		TestingBackup=struct(	'TestConfig',obj.TestConfig,...
								'TestResults',{obj.TestResults},...
								'SingleTestConfig',[obj.SingleTestConfig],...
								'WorkList',[obj.WorkList],...
								'WorkRankArray',{obj.WorkRankArray},...
								'WorkDone',{obj.WorkDone},...
								'NextCalc',obj.NextCalc);
		%just the most needed functions			
		
		save(obj.SavePath,'TestingBackup')
	end
	
	disp('Tiny coffee break...')	
	pause(obj.BreakTime);
	obj.BreakStep=obj.NumToBreak;
	disp('refreshed');
	
	obj.contCalc(ListNr);
	
	
end
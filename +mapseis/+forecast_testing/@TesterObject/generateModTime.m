function generateModTime(obj)
	%builds the list with all possible model and times
	%used for the selection	
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	import mapseis.filter.*;
	import mapseis.projector.*;
	
	%TEST: realtime mode added but has to be tested
	
	%get models
	[Forecasts,Names,Pos] = obj.ForecastProject.getAllForecasts;
	
	%get times
	TimeSteps = obj.ForecastProject.AvailableTimes;
	ForecastLength = ones(size(obj.ForecastProject.AvailableTimes))*obj.ForecastProject.ForecastLength;
	obj.OriginalLength = obj.ForecastProject.ForecastLength; %needed for norm the forecasts
	
	if obj.RealTimeTest&obj.NormRealTime
		ForecastLength = [diff(TimeSteps),1];
	end
	
	%check what is calculated
	%raw data
	AvailableTimes=false(numel(TimeSteps),numel(Pos));
	
	
	if obj.JumpTime
		PostProcessVec=false(size(AvailableTimes));
		
		TestFilter=FilterListCloner(obj.Filterlist);
		TestFilter.changeData(obj.Datastore);
		
		minTime=TimeSteps(1);
		maxTime=TimeSteps(end)+ForecastLength(end);
		MaxTimeInt=[minTime,maxTime];
		Range='in';
		TimeFilt=TestFilter.getByName('Time');
		TimeFilt.setRange(Range,MaxTimeInt);
		
		TestFilter.updateNoEvent;
		selected=TestFilter.getSelected;
		[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
		TestFilter.PackIt;
		
		%check with time periods contain any earthquakes 
		for i=1:numel(TimeSteps)
			%apply filter and catalog
			
			CurTime=TimeSteps(i);
			TimeInt=[CurTime,CurTime+ForecastLength(i)];
			selector=ShortCat(:,4)>=TimeInt(1)&ShortCat(:,4)<TimeInt(2);        			
							
			
			if any(selector)
				PostProcessVec(i,:)=true;
				disp(i)
			end
		end
	
	else
		PostProcessVec=true(size(AvailableTimes));
	end
	
	%have to check if it works like this
	ForecastType=cell(size(Forecasts));
	for i=1:numel(Forecasts)
		
		ForecastType{i}=Forecasts{i}.ForecastType.Type;
		
		[C,ia,ib] = intersect(TimeSteps,Forecasts{i}.TimeVector);
		
		if ~isempty(C)
			%AvailableTimes(i,ia)=true;
			AvailableTimes(ia,i)=true;
		end
		
	end
	
	obj.ModelList={Names{:};Forecasts{:};ForecastType{:}}';
	obj.TimeList=TimeSteps;
	obj.ForecastLength=ForecastLength;
	disp(size(AvailableTimes))
	disp(size(PostProcessVec))
	
	obj.AvailableModTime=AvailableTimes&PostProcessVec;
	
	
end
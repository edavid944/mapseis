function addTestPluging(obj,TestPlugin)
	%single plugin or cell array with plugins is possible	
	
	%IMPORTANT: The plugins are assumed to be configurated.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if iscell(TestPlugin)
		for i=1:numel(TestPlugin)
			%generate Name
			PlugName=[TestPlugin{i}.TestName,'_',TestPlugin{i}.Version,'_',TestPlugin{i}.Variant];
			
			%store
			obj.TestPlugInList.(PlugName)=TestPlugin{i};
		end
	
	else
		%generate Name
		PlugName=[TestPlugin.TestName,'_',TestPlugin.Version,'_',TestPlugin.Variant];
		
		%store
		obj.TestPlugInList.(PlugName)=TestPlugin;
		
	end
	obj.CurrentState(3)=true;   
    	        
end

function addAddonData(obj,Datastore,Filterlist)
	%allows to set a addon catalog and filterlist, which
	%will be used instead of the catalog and filterlist in
	%the Project object. Often needed when due error estimation
	%as this will need a "shaker" catalog.
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	
	%get the interal catalog and filterlist
	obj.Datastore=Datastore;
	obj.Filterlist=Filterlist;
	
	if isempty(Datastore)
		obj.Datastore=obj.ForecastProject.Datastore;
	end
	
	if isempty(Filterlist)
		obj.Filterlist=obj.ForecastProject.Filterlist;
	end
	
	if ~isempty(Datastore)&~isempty(Filterlist)
		obj.MetaData.OriginalCatalog=false;
	else
		obj.MetaData.OriginalCatalog=true;     			
	end

end
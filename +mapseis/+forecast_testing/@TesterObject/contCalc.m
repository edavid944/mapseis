function contCalc(obj,ListNr)
	
	%The actual calculation processing part 
		
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.filter.*;	
	import mapseis.projector.*;
	
 
	
	
	%get current forecast(s),timestep and test ID
	TestID = obj.WorkList(ListNr,1);
	TimeID = obj.WorkList(ListNr,2);
	ForeAID = obj.WorkList(ListNr,3);
	ForeBID = obj.WorkList(ListNr,4);
	
	%Use monte-carlo or not?
	Monti=obj.TestConfig.Uncertianties;
	
	%prepare test filter
	tenth=1/(24*36000); %0.1 second
	CurTime=obj.TimeList(TimeID);
	CurLength=obj.ForecastLength(TimeID);
	TimeInt=[CurTime+tenth,CurTime+CurLength];
	Range='in';
	%added a factor tenth to prevent possible double count of
	%eartquakes
	
	TestFilter=FilterListCloner(obj.Filterlist);
	TimeFilt=TestFilter.getByName('Time');
	TimeFilt.setRange(Range,TimeInt);
	
	%get test
	Name_of_test=obj.TestNames{TestID};
	
	disp(Name_of_test)
	disp(CurTime)
	
	TheTest = obj.TestPlugInList.(Name_of_test); 
	
	%set to parallel if needed
	TheTest.ParallelMode=obj.ParallelMode;
	
	
	%determine input format (future release)
	%CurFeatures=CurTest.getFeatures;
	%wholeObjectSend=strcmp(CurFeatures.InputFormat,'Fobj');
	
	EverythingFine=true;
	%get forecast(s)
	if ForeBID == -1
		%single test
		disp(ForeAID)
		ForecastA=obj.ModelList{ForeAID,2};
		NameA=obj.ModelList{ForeAID,1};
		TypeA=obj.ModelList{ForeAID,3};
		disp(ForecastA)
		try
			[TheCastA ForeTime ForeLength]= ForecastA.getCSEPForecast(CurTime);
		catch
			TheCastA=NaN;
			EverythingFine=false;
		end
		
		%normalize if needed
		if  EverythingFine&obj.RealTimeTest&obj.NormRealTime&(CurLength~=1) 	
			TheCastA = obj.NormForecast(TheCastA,CurLength,TypeA);
		end
	
		TheCasts={TheCastA,TypeA,NameA};
	
	
	elseif ForeAID == -1
		%ranking test
		TheList=obj.WorkRankArray{ForeBID};
		
		Forecasts={};
		TheCasts={};
		for i=1:numel(TheList)
			Forecasts(i)=obj.ModelList(i,2);
			NameA=obj.ModelList{i,1};
			TypeA=obj.ModelList{i,3};
			try
				[WantIt ForeTime ForeLength]= Forecasts{i}.getCSEPForecast(CurTime);
			catch
				WantIt=NaN;
				EverythingFine=false;
			end
		
			if  EverythingFine&obj.RealTimeTest&obj.NormRealTime&(CurLength~=1) 
				WantIt = obj.NormForecast(WantIt,CurLength,TypeA);
			end
	
	
			TheCasts(i,:)={WantIt,TypeA,NameA};
		end
	
	else
		%comparison test
		ForecastA=obj.ModelList{ForeAID,2};
		NameA=obj.ModelList{ForeAID,1};
		TypeA=obj.ModelList{ForeAID,3};
		
		ForecastB=obj.ModelList{ForeBID,2};
		NameB=obj.ModelList{ForeBID,1};
		TypeB=obj.ModelList{ForeBID,3};
		try
			[TheCastA ForeTime ForeLength]= ForecastA.getCSEPForecast(CurTime);
			[TheCastB ForeTime ForeLength]= ForecastB.getCSEPForecast(CurTime);
		catch
			TheCastA=NaN;
			TheCastB=NaN;
			EverythingFine=false;
		
		end
	
		if  EverythingFine&obj.RealTimeTest&obj.NormRealTime&(CurLength~=1) 	
			TheCastA = obj.NormForecast(TheCastA,CurLength,TypeA);
			TheCastB = obj.NormForecast(TheCastB,CurLength,TypeB);
		end
	
		TheCasts={TheCastA,TypeA,NameA;
		TheCastB,TypeB,NameB};
	end
	
	%calculate test score
	if EverythingFine
		if Monti
			%Uncertianties estimation
			
			%first result with original catal
			try
				obj.Datastore.ResetShaker;
			catch
				error('Catalog could not be modified, probably not a shaker-type catalog');
			end
	
			
			%apply filter and catalog
			TestFilter.changeData(obj.Datastore);
			TestFilter.updateNoEvent;
			selected=obj.Filterlist.getSelected;
			TestFilter.PackIt;
			
			if any(selected)
				[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
			else
				disp('no earthquake')
				ShortCat=[];	
			end	
	
			%calculate
			TheTest.resetMonteCarlo;
			ErrorCode = TheTest.calcTest(TheCasts,ShortCat)
			OrgTestResults = TheTest.getResults;
			
			%get result definition
			ResDef = TheTest.getResultDefinition;
			
			if isempty(ResDef)
				ResDef = obj.buildDefaultResDef(OrgTestResults);
			end	
		
			%build Result template
			[TestResult corResDef] = obj.buildMonteResult(OrgTestResults,ResDef);
			
			
			for i=1:obj.TestConfig.Nr_Monte_Catalogs
				
				obj.Datastore.ShakeCatalog;
				
				%apply filter and catalog
				TestFilter.changeData(obj.Datastore);
				TestFilter.updateNoEvent;
				selected=TestFilter.getSelected;
				TestFilter.PackIt;
				
				[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
				
				%calculate
				ErrorCode = TheTest.calcTest(TheCasts,ShortCat)
				CurTestResults = TheTest.getResults;			
				
				TestResult = obj.addMonteResult(i,TestResult,CurTestResults,corResDef);
			
			end
			
			
			%do the needed statistics (outsourced again
			TestResult = obj.MonteResultStats(TestResult,corResDef);
			
			
			%add test data and forecast date and name of forecast to the result
			TestResult.ForecastName=TheCasts(:,3);
			TestResult.LastTested=now;
			TestResult.ForecastTime=CurTime;
			TestResult.ForecastLength=CurLength;
			
			if iscell(TestResult.ForecastName)&numel(TestResult.ForecastName)>1
				Write4Name=TestResult.ForecastName{1};
			
				for i=2:numel(TestResult.ForecastName)
					Write4Name=[Write4Name,'_',TestResult.ForecastName{i}];
				end
			
			elseif iscell(TestResult.ForecastName)
				Write4Name=TestResult.ForecastName{1};
			
			else
				Write4Name=TestResult.ForecastName;
			
			end
			
			%Store Result
			obj.TestResults{TimeID}.Empty = false;
			obj.TestResults{TimeID}.(Name_of_test).(Write4Name) = TestResult;
				
	
		else
			%no uncertianties
			
			%apply filter and catalog
			TestFilter.changeData(obj.Datastore);
			TestFilter.updateNoEvent;
			selected=TestFilter.getSelected;
			TestFilter.PackIt;
			if any(selected)
				[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
				disp(numel(ShortCat(:,1)));
				disp(min(ShortCat(:,5)));
				disp(max(ShortCat(:,5)));
				%calculate
				TheTest.resetMonteCarlo;
				ErrorCode = TheTest.calcTest(TheCasts,ShortCat)
				TestResult = TheTest.getResults;
			else
				disp('no earthquake')
				TheTest.resetMonteCarlo;
				ErrorCode = TheTest.calcTest(TheCasts,[])
				TestResult = TheTest.getResults;
			end	
			
			%add test data and forecast date and name of forecast to the result
			TestResult.ForecastName=TheCasts(:,3);
			TestResult.LastTested=now;
			TestResult.ForecastTime=CurTime;
			TestResult.ForecastLength=CurLength;
			
			if iscell(TestResult.ForecastName)&numel(TestResult.ForecastName)>1
				Write4Name=TestResult.ForecastName{1};
			
				for i=2:numel(TestResult.ForecastName)
					Write4Name=[Write4Name,'_',TestResult.ForecastName{i}];
				end
			
			elseif iscell(TestResult.ForecastName)
				Write4Name=TestResult.ForecastName{1};
			
			else
				Write4Name=TestResult.ForecastName;
			
			end
			
			%Store Result
			obj.TestResults{TimeID}.Empty = false;
			obj.TestResults{TimeID}.(Name_of_test).(Write4Name) = TestResult;
			
	
		end
		
		
		%mark as work done (generate a short entry what has done)
		doneTestList=obj.WorkDone{1};
		doneTimeList=obj.WorkDone{2};
		doneCastList=obj.WorkDone{3};
		
		doneTestList{ListNr}=Name_of_test;
		doneTimeList(ListNr)=CurTime;
		doneCastList{ListNr}=TheCasts(:,3);
		
		obj.WorkDone={doneTestList,doneTimeList,doneCastList}; %CHECK IF CORRECT
		
		disp([num2str(obj.NextCalc), ' of ', num2str(numel(obj.WorkList(:,1)))]);
	
	else
	
		disp('Forecast was not existing');
		disp([num2str(obj.NextCalc), ' of ', num2str(numel(obj.WorkList(:,1)))]);
	end
	
	if ~obj.LoopMode
		
		%Go for the next calculation or finish
		if ListNr<numel(obj.WorkList(:,1))
			if obj.Relax
				if obj.BreakStep<=0;
					CurNr=obj.NextCalc;
					obj.NextCalc=obj.NextCalc+1;
					obj.makeAbreak(CurNr);
				else
					obj.BreakStep=obj.BreakStep-1;
					CurNr=obj.NextCalc;
					obj.NextCalc=obj.NextCalc+1;
					obj.contCalc(CurNr);
				end
		
			else
				CurNr=obj.NextCalc;
				obj.NextCalc=obj.NextCalc+1;
				obj.contCalc(CurNr);
			end
		else
			%The end my friend
			obj.finishCalc;
		
		end
	
	else
		%it is looped do nothing for now
	
	
	end
	
end
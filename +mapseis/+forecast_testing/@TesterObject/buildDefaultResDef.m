function ResDef = buildDefaultResDef(obj,resultDemo)
	%outsourced from contCalc, to make it more readable.
	%no result definition -> has to be built	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	ToIgnore={'TestName','TestVersion','SignVal'};
	ToCollect={'Passed'};
	
	TheFields=fieldnames(resultDemo);
	ResDef=struct;
	for i=1:numel(TheFields)
		if any(strcmp(ToIgnore,TheFields{i}))
			ResDef.(TheField{i})={'string','ignore'};
			
		elseif any(strcmp(ToCollect,TheFields{i}))
			ResDef.(TheField{i})={'scalar','collect'};
			
		else
			if iscell(resultDemo.(TheField{i}))
				ResDef.(TheField{i})={'cell','collect'};
			
			elseif isstr(resultDemo.(TheField{i}))
				ResDef.(TheField{i})={'string','collect'};
			
			else
				if any(size(resultDemo.(TheField{i}))>1)
					ResDef.(TheField{i})={'array','collect'};
				else
					ResDef.(TheField{i})={'scalar','stats'};
				end
			end
	
		end
	
	end
	
	
end
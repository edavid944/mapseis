function buildTestResultObj(obj,FullData)
	%builds the Test result object, this object is more like
	%a structure than an actual object, but it may change later
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	import mapseis.forecast_testing.*
	
	
	if nargin<2
		FullData=true;
	end
	
	if strcmp(obj.Status,'empty')
		disp('no Project - no Results, that simple');
		return;
	end
	
	if strcmp(obj.Status,'ready')
		disp('no Results yet, calculate first');
		return;
	end
	
	if strcmp(obj.Status,'started')
		disp('Not all tests have been finished, produce test results object anyway');
		%return;
	end
	
	
	if isempty(obj.possiblePlots)|isempty(obj.possibleTables)
		disp('build plot and table list');
		obj.generatePlotTableList;
	end
		
	%create TestInfo
	TestInfo={};
	for i=1:numel(obj.TestNames)
		%get test
		Name_of_test=obj.TestNames{i};
		TheTest=obj.TestPlugInList.(Name_of_test); 
		CurFeatures=TheTest.getFeatures;
		TestType=CurFeatures.TestType;
		SingleTestConfig=TheTest.getTestConfig;
		
		TestInfo(i,:)={Name_of_test,TestType,SingleTestConfig};
	
	end
	
	
	%Create a new Test Result object
	NewResults = TestResultObj([obj.ForecastProject.Name,'_TestResults']);
	
	
	
	%add all data to the object
	NewResults.MetaData = obj.MetaData;
	
	if FullData
		%get forecasts
		[Forecasts,Names,Pos] = obj.ForecastProject.getAllForecasts;
		NewResults.Forecasts = {Forecasts,Names};
		NewResults.FullData=true;
	end
	
	
	NewResults.ProjectID = obj.ForecastProject.ID;
	NewResults.Datastore = obj.Datastore;
	NewResults.Filterlist = obj.Filterlist;
	
	NewResults.TestConfig = obj.TestConfig
	NewResults.TestInfo = TestInfo;
	NewResults.CalcedResults = obj.WorkDone;
	NewResults.Results = obj.TestResults;
	
	NewResults.possibleTables = obj.possibleTables;
	NewResults.possiblePlots = obj.possiblePlots;
	
	%the last part of CalcedResults is double packed, instead of 
	%reworking the whole code just change it here
	faultyCell=NewResults.CalcedResults{3};
	NewResults.CalcedResults{3}=cellfun(@(x) x{:},faultyCell,'UniformOutput',false);
	
	
	obj.ResultObject=NewResults;
	
end
classdef TesterObject < handle
	%This the first version of the forecast project calculator
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	properties
		Name
		ID
		Version
		MetaData
		ForecastProject
		SingleTestConfig
		Datastore
		Filterlist
		
		ModelList
		TimeList
		ForecastLength
		OriginalLength
		AvailableModTime
		SelectedModTime
		
		TestPlugInList
		TestNames
		possiblePlots
		possibleTables
		TestResults
		ResultObject
		
		TestConfig
		
		WorkList
		WorkRankArray
		WorkDone
		NextCalc
		
		CurrentState
		Status
		BreakStep
		NumToBreak
		BreakTime
		Relax
		SaveRes
		SavePath
		JumpTime
		RealTimeTest
		NormRealTime
		ParallelMode
		
		LoopMode
		ParentParallelMode
		
	end
	
	
	
	methods
		function obj = TesterObject(Name)
			%first version of automatic testing suite
			%will be as "simple" as possible
			%i.e. now dynamic test list once added, is added, if something
			%new has to be added after calculation everything should be
			%recalculated
			
			%Only testing and test object building here
			
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
				%A bit gimmicky I admit, but could be useful later
				Name=RawChar(randi(15,1,8)+1);
			end
			
			obj.Name=Name; %may be not so important here, but who knows?
			obj.ID=RawChar(randi(15,1,8)+1);
			obj.Version='0.9';
			
			obj.MetaData=[];
			obj.ForecastProject=[];
			obj.SingleTestConfig=[];
			obj.Datastore=[];
			obj.Filterlist=[];
			
			obj.ModelList={};
			obj.TimeList=[];
			obj.ForecastLength=[];
			obj.OriginalLength=[];
			obj.AvailableModTime=[];
			obj.SelectedModTime=[];
			
			obj.TestPlugInList=[];
			obj.TestNames={};
			obj.possiblePlots=[];
			obj.possibleTables=[];
			obj.TestResults=[];
			obj.ResultObject=[];
			
			obj.WorkList=[];
			obj.WorkRankArray={};
			obj.WorkDone={{},[],{}};
			obj.CurrentState=false(4,1);
			obj.Status='empty';
			obj.NextCalc=[];
			
			%generate default test suite config
			defaultConfig = struct(	'SignVal',0.05,...
									'Uncertianties',false,...
									'Nr_Monte_Catalogs',1000,...
									'CalcMedian',true,...
									'CalcMean',true,...
									'UseSymmetry',true);
			%maybe more parameters later
			
			obj.TestConfig = defaultConfig;
			
			obj.BreakStep=250;
			obj.NumToBreak=250;
			obj.BreakTime=10;
			obj.Relax=true;
			obj.JumpTime=true;
			obj.SaveRes=true;
			obj.SavePath='./Temporary_Files/TestingBackup.mat';
			
			%Used only for "realtime projects", RealTimeTest puts the
			%TesterObject into the (pseudo-) realtime testing mod, and
			%of NormRealTime is true, the Forecasts will be normalized
			%to time and only the time intervall the forecasts is valid
			%will be used.
			obj.RealTimeTest=false;
			obj.NormRealTime=false;
			
			obj.ParallelMode=false;
			
			%if jump time is true, the object will only tests time periods
			%where there is an earthquake happening
			
			
			obj.LoopMode=true;
			obj.ParentParallelMode=false;
			
			%LoopMode: if set to true, Tester will use a for-loop or 
			%parfor-loop instead of a recursive function, which allows 
			%a bit more flexibility. For larger project this is often 
			%needed because of the piss-poor memory management of matlab
			%ParentParallelMode: if true the loop will be parallelized and
			%not the single tests, it will set ParallelMode automatically
			%false.
		
		
		end
	
		
		%functions in external files
		addProject(obj,ProjectObj)
		
		addAddonData(obj,Datastore,Filterlist)
		
		addTestPluging(obj,TestPlugin)
		
		generatePlotTableList(obj)
		
		ConfigureTesting(obj,ConfigStruct)
		
		ConfigStruct = getConfig(obj)
		
		generateModTime(obj)
		
		[ModelNames Times AvailableTimes] = getModTimeList(obj)
		
		setTestSelection(obj,WhichTest,ModTimeSelection)
		
		generateWorkList(obj)
		
		initResultStruct(obj)
		
		startCalc(obj)
		
		restartCalc(obj)
		
		contCalc(obj,ListNr)
		
		LoopItSerial(obj)
		
		LoopItParallel(obj)
		
		makeAbreak(obj,ListNr)
		
		restoreBackup(obj)
		
		finishCalc(obj)
		
		buildTestResultObj(obj,FullData)
		
		TestObj=getTestObject
		
		ResDef = buildDefaultResDef(obj,resultDemo)
		
		NormCast = NormForecast(obj,TheCast,TimeLength,ForecastType)
		
		[TestResult corResDef] = buildMonteResult(obj,OrgTestResults,ResDef)
		
		TestResult = addMonteResult(obj,IDX,oldTestResult,CurTestResults,ResDef)
		
		TestResult = MonteResultStats(obj,oldTestResult,ResDef)
		
	end
	
end
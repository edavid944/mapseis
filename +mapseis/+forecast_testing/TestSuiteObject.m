classdef TestSuiteObject < handle
	%This the first version of the forecast project calculator
	%THIS IS NOT RELEVANT ANYMORE
	
	properties
        	Name
        	Version
        	ID
        	ObjVersion
        	MetaData
        	ForecastProject
        	TestConfig
        	AddonCatalog
        	AddonFilterlist
        	
        	ModelList
        	TimeList
        	SelectedModTime
        	
        	TestPlugInList
        	TestResults
        	SelectedTables
        	SelectedPlots
        	OutputFolders
        	FileName
        	extGUI
        	
        	WorkToDo
        	WorkList
        	WorkDone
        end
        

    
        methods
        	function obj = TestSuiteObject(Name)
        		%first version of automatic testing suite
        		%will be as "simple" as possible
        		%i.e. no dynamic test list once added, is added, if something
        		%new has to be added after calculation everything should be
        		%recalculated
        	
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        		
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		obj.Name=Name; %may be not so important here, but who knows?
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.Version='0.9';
        		
        		obj.MetaData={};
			obj.ForecastProject=[];
			obj.TestConfig=[];
			obj.AddonCatalog=[];
			obj.AddonFilterlist=[];
			
			obj.ModelList={};
			obj.TimeList=[];
			obj.SelectedModTime=[];
			
			obj.TestPlugInList=[];
			obj.TestResults=[];
			obj.SelectedTables=[];
			obj.SelectedPlots=[];
			obj.OutputFolders={};
			obj.FileName=[];
			obj.extGUI=[];
			obj.WorkList={};
			obj.WorkToDo=[];
			obj.WorkDone=[];
        		
        	end
        	
        	
        	function addProject(obj,ProjectObj)
        		%adds a forecast project to the TestSuite
        		%currently only one Project object per TestSuiteObject 
        		%is allowed, but a merger should be built and added.
        		
        	end
        	
        	
        	function addAddonData(obj,Datastore,Filterlist)
        		%allows to set a addon catalog and filterlist, which
        		%will be used instead of the catalog and filterlist in
        		%the Project object. Often needed when due error estimation
        		%as this will need a "shaker" catalog.
        		
        		
        	end
        	
        	
        	function addTestPluging(obj,TestPlugin)
        	        %single plugin or cell array with plugins is possible	
        	
        	end
        	
        	
        	function ConfigureTesting(obj,ConfigStruct)
        	
        	end
        	
        	
        	function ConfigStruct = getConfig(obj)
        	
        	end
        	
        	
        	
        	
        	
        	function generateModTime(obj)
        		%builds the list with all possible model and times
        		%used for the selection	
        	
        	end
        	
        	
        	function generateWorkList(obj)
        		%builds the worklist with the parts which have to be done
        		%can only be used after selection has been done and plugins
        		%are added
        		
        	
        	end
        	
        	
        	
        	
        	function startTest(obj)
        	
        	end
        	
        	
        	function startPlot(obj)
        	
        	end
        	
        	
        	function startTable(obj)
        	
        	end
        	
        	
        	function startAll(obj)
        	
        	end
        	
        	
        	function startProcessing(obj,ProcessType)
        		%basically an internal function it starts one of the three
        		%processing steps: 
        		%'test': test result calculation, 
        		%'plot': plot tests result to file
        		%'table': writes tables to file
        		
        		%normally this is called over the "link" functions startTest,
        		%startPlot, startTable or startAll
        		
        		%The continuation uses currently different methods, the reason
        		%is to save a switch or if statement before every calculation
        		
        	end
        	
        	
        	function contTesting(obj,ListNr)
        	
        	end
        	
        	
        	function contPlotting(obj,ListNr)
        	
        	end
        	
        	
        	function contTabling(obj,ListNr)
        	
        	end
        	
        end

end	

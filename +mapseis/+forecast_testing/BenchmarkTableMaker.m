function OutStruct = BenchmarkTableMaker(CalcBench,BenchName,EnsBench,EnsName,ModelName,MaxMinDay,FileName,NoMin)
	%Simple scripts it takes writes Benchmark data statistic into a latex
	%formated table.
	%MaxMinDay: Data with a day with maximal number of earthquakes and ome with 
	%a minimum number of earthquake =[minDate,minNum;maxDate,maxNum]
	
	%NoMin will kick out the minimum time (only if no MaxMinDay is specified)
	

	%latexMode currently always on
	
	LatexMode=true;
	
	
	%build data first
	NameEntry={};
	PrintDays=false;
	TotalTimes=[];
	MeanTimes=[];
	STDTimes=[];
	minTimes=[];
	maxTimes=[];
	
	
	formatSpec = '%.2f';
	
	
	if ~isempty(MaxMinDay)
		PrintDays=true;
		maxDayTime=[];
		minDayTime=[];
	end
	
	
	
	if ~isempty(CalcBench);
		for bC=1:numel(CalcBench)
			%total time of calculation
			NameEntry{end+1}=[BenchName{bC},': ', 'Complete Calculation'];
			TotalTimes(end+1)=CalcBench{bC}.TotalTime;
			MeanTimes(end+1)=nanmean(CalcBench{bC}.CalcTimeStep);
			STDTimes(end+1)=nanstd(CalcBench{bC}.CalcTimeStep);
			minTimes(end+1)=nanmin(CalcBench{bC}.CalcTimeStep);
			maxTimes(end+1)=nanmax(CalcBench{bC}.CalcTimeStep);
			
			if PrintDays
				TheTime=CalcBench{bC}.TimeSteps;
				PosMax=find(TheTime==MaxMinDay(2,1));
				PosMin=find(TheTime==MaxMinDay(1,1));
			
				maxDayTime(end+1)=CalcBench{bC}.CalcTimeStep(PosMax);
				minDayTime(end+1)=CalcBench{bC}.CalcTimeStep(PosMin);
			end
			
			
			for mC=1:numel(ModelName)
				if any(strcmp(ModelName{mC},CalcBench{bC}.Models))
			
					NameEntry{end+1}=[BenchName{bC},': ', ModelName{mC}];
					TotalTimes(end+1)=CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTime;
					MeanTimes(end+1)=nanmean(CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTimePerStep);
					STDTimes(end+1)=nanstd(CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTimePerStep);
					minTimes(end+1)=nanmin(CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTimePerStep);
					maxTimes(end+1)=nanmax(CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTimePerStep);
					
					if PrintDays
										
						maxDayTime(end+1)=CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTimePerStep(PosMax);
						minDayTime(end+1)=CalcBench{bC}.ModelTimes.(ModelName{mC}).TotalTimePerStep(PosMin);
					end
				
				end
			end
		end
	end
	
	
	if ~isempty(EnsBench);
		for eC=1:numel(EnsBench)
			NameEntry{end+1}=EnsName{eC};
			TotalTimes(end+1)=EnsBench{eC}.TotalTime;
			MeanTimes(end+1)=nanmean(EnsBench{eC}.SingleStepTime);
			STDTimes(end+1)=nanstd(EnsBench{eC}.SingleStepTime);
			minTimes(end+1)=nanmin(EnsBench{eC}.SingleStepTime);
			maxTimes(end+1)=nanmax(EnsBench{eC}.SingleStepTime);
			
			if PrintDays
				TheTime=EnsBench{eC}.TimeSteps;
				PosMax=find(TheTime==MaxMinDay(2,1));
				PosMin=find(TheTime==MaxMinDay(1,1));
			
				maxDayTime(end+1)=EnsBench{eC}.SingleStepTime(PosMax);
				minDayTime(end+1)=EnsBench{eC}.SingleStepTime(PosMin);
			end
		end
	
	end
	
	
	
	%write the file
	
	%open file
	fid = fopen(FileName,'w'); 
	
	
	if LatexMode
		if PrintDays
			HeaderLineLatex1=['\hline  Benchmark Entry',' & ',...
						'Total Time',' & ',...
						'Mean Time',' & ',...
						'std. Time',' & ',...
						'min. Time',' & ',...
						'max. Time',' & ',...
						'max Eq. Time',' & ',...
						'min Eq. Time','   \\'];
			HeaderLineLatex2=[' ',' & ',...
						'(h)',' & ',...
						'(min)',' & ',...
						'(min)',' & ',...
						'(min)',' & ',...
						'(min)',' & ',...
						'(min)',' & ',...
						'(min)','   \\'];			
						
						
			TabCode='c|c|c|c|c|c|c|c';
			TabWide='8';
			
		else
		
			if NoMin
				HeaderLineLatex1=['\hline  Benchmark Entry',' & ',...
							'Total Time',' & ',...
							'Mean Time',' & ',...
							'std. Time',' & ',...
							'max. Time',' \\'];
				HeaderLineLatex2=[' ',' & ',...
							'(h)',' & ',...
							'(min)',' & ',...
							'(min)',' & ',...
							'(min)',' \\'];			
							
				TabCode='c|c|c|c|c';
				TabWide='5';
			
			else
				HeaderLineLatex1=['\hline  Benchmark Entry',' & ',...
						'Total Time',' & ',...
						'Mean Time',' & ',...
						'std. Time',' & ',...
						'min. Time',' & ',...
						'max. Time',' \\'];
				HeaderLineLatex2=[' ',' & ',...
							'(h)',' & ',...
							'(min)',' & ',...
							'(min)',' & ',...
							'(min)',' & ',...
							'(min)',' \\'];			
							
				TabCode='c|c|c|c|c|c';
				TabWide='6';
			
			end
		end
	
		
		%write header
		fprintf(fid,'%s','\begin{table}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{tabular}{|',TabCode,'|}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\multicolumn{',TabWide,'}{c}{Benchmark Calculation Times} \\');
		fprintf(fid,'\n');
		
		
		
		fprintf(fid,'%s',HeaderLineLatex1); 
		fprintf(fid,'\n');
		fprintf(fid,'%s',HeaderLineLatex2); 
		fprintf(fid,'\n');
		
	
	else
		%later
	
	
	end
	
	
		
	
	
	
	%write it 
	for lC=1:numel(NameEntry)
		%build line	
		TheLine = ['\hline  ',NameEntry{lC},'& ',...
				num2str(TotalTimes(lC)/3600,formatSpec), ' & ',...
				num2str(MeanTimes(lC)/60,formatSpec), ' & ',...
				num2str(STDTimes(lC)/60,formatSpec), ' & '];
		if ~NoMin		
			TheLine = [TheLine,num2str(minTimes(lC)/60,formatSpec), ' & '];
		end
		
		TheLine = [TheLine,num2str(maxTimes(lC)/60,formatSpec)];
				
		if PrintDays	
			TheLine = [TheLine, ' & ',num2str(minDayTime(lC)/60,formatSpec), ' & ',...
					num2str(maxDayTime(lC)/60,formatSpec)];
		end		
		
		%end line
		TheLine = [TheLine,' \\'];
	
		%write line
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
	end
	
		
	%Finalize Table	
	fprintf(fid,'%s','\hline');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{tabular}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\caption{The calculation time for different models and cases}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\label{tab:ADD A LABEL}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{center}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{table}');
	fprintf(fid,'\n');
 
	
	%close file
	fclose(fid); 
	
	
	
	%build outstruct
	OutStruct = 	struct(	'NameEntry',NameEntry,...
				'TotalTimes',TotalTimes,...
				'MeanTimes',MeanTimes,...
				'STDTimes',STDTimes,...
				'minTimes',minTimes,...
				'maxTimes',maxTimes);
				
	if PrintDays
		OutStruct.maxDayTime=maxDayTime;
		OutStruct.minDayTime=minDayTime;
	
	end
	
	
	
end

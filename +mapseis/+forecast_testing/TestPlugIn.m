classdef TestPlugIn < handle
	%a interface or abstract class  for a Forecast Testplugin. It is an more or 
	%less empty object and only specify what each plugin needs to provide 
	%for being supported by mapseis forecast testing suit.
	%To use it, the plugin has to be derived from it 
	%(e.g. LTestPlugin < mapseis.forecast_testing.TestPlugIn
	
	%DON'T PANIC
	%The descriptions are longer than the code which should be written
	
	properties
		TestName 
        	Version %Has to be a STRING (consist of CodeVersion and Variant)
        	CodeVersion %Version of the code
        	Variant %allows to use multiple varitions of the same model in a project
        	TestInfo %The place for a description about the test 
        	
        	CurrentTested %contains a cell array with the names of the currently
        		      %tested forecasts, just a security measurement, in case
        		      %something goes wrong
        	
        	TestReady
        	CPUTimeData
        	ParallelMode
        	%TestReady signalizes that the test is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it should be a 
        	%structure with some defined fields, if it is used. It can be empty
        	%in this case it will ignore the data
        	
        	%no '.' allowed for TestName, Version and variant
        	
        	%ParallelMode: will be set by the Tester object it signalizes that
        	%the test can use the parallel processingmode. It can be ignored 
        	%if no parallel mode is planed. 
        end
    

    
        methods (Abstract)
        	%obj = TestPlugIn()
              	
        	   	
        	       	
        	ConfigureTest(obj,ConfigData)
        	%used to configure the test 
        	%Default field for significance value is SignVal, this will always be
        	%sended by the Test Suite
        	
        	
        	
        	ConfigData = getTestConfig(obj)
        	%should return the current configuration, mostly needed to get
        	%the default values
        	
        	
        	
        	FeatureList = getFeatures(obj)
        	%This will be used by the TestSuite to determine what the model can 
        	%be used for. The output should by structure with specific fieldnames
        	%with mostly boolean values. 
        	%Following keywords are available:
        	%	supportPoisson:		true if a poisson distribution is
        	%				supported
        	%	supportNegBin:		true if negative binomila distribution
        	%				is supported
        	%	supportCustom:		true if the usage of discrete custom
        	%				distribution are allowed
        	%	MixedDistro:		Only needed for comparison and ranking type 
        	%				tests. If set to true, the test allows to 
        	%				use forecasts with unequal distribution type
        	%				(e.g. a Possion forecast vs. a NegBin one)
        	%	TestType:		Sets the type of the test and number of
        	%				forecast needed for the test. 'single' 
        	%				for consistency tests with only one model
        	%				'compare' for comparison tests which compare
        	%				two forecasts and 'ranking' for tests which 
        	%				rank all the model (currently no know test 
        	%				available), in this case every model in the 
        	%				testing suite will be sended.
        	%	InputFormat:		(future feature), allows to choose between 
        	%				Relmgrids ('relm') and forecastOBJ ('Fobj' as
        	%				input of the model. Not yet done, only 'relm'
        	%				allowed for now.
        	%	Symmetric:		Only needed for comparison tests, if true it 
        	%				means that test(A,B)==test(B,A), in this case
        	%				the Tester object can save half of the tests.
        	        	
        	
        	
        	PossibleParameters = getParameterSetting(obj)
        	%The method should return a description of the parameters needed
        	%for the test and should also include information about the way
        	%the gui for setting the parameters should be designed.
        	%The output has to be a structure with the field names being the 
        	%name of the parameters which should be used for ConfigureTest
        	%Each entry in structure should be another structure with following
        	%Often the Tests do not need any configuration or only the significance
        	%value, in this case the Possible Parameters can return empty
        	%Default field for significance value is SignVal
        	
        	%keywords:
        	%	Type: 		can be 	'check' for boolean,
        	%				'edit' for any value or small text
        	%				'list' for a selection of values specified
        	%					by Items
        	%				'text' for longer string boxes (e.g. Comments)
        	%
        	%	Format:		Defines the data type of value only applyable with
        	%			'edit'. It can be the following: 'string','float',
        	%			'integer','file','dir'	
        	%	ValLimit: 	The limit a value can have in a edit box, if left
        	%			empty it will set to [-9999 9999].
        	%	Name:		Sets the Name used for the value in the GUI, if 
        	%			left empty the variable name will be used instead
        	%	Items:		Only needed in case of 'list', there it has to be 
        	%			cell array with strings. The value returned will
        	%			the selected string.
        	%			
        	%			
        	%	DefaultValue:	Sets the value used as default value in the GUI.
        	%			If missing, 0 will be used as default or 1 for 
        	%			lists
        	%	Style:		Only applyable for the Type 'list', it sets the 
        	%			GUI element used to represent the list and it can
        	%			be:	
        	%				'listbox': a scrollable list for the selection
        	%				'popupmenu': a popup list 
        	%				'radiobutton': a block of checkerboxes
        	%				'togglebutton': a selection of buttons
        	%			If missing 'popupmenu' will be used. The last
        	%			two options are only suggest if not many elements
        	%			are in the list.
        	
        	
        	resetMonteCarlo(obj)
        	%this should reset the last configuration without the main configuration
        	%It will be called after each calculation with new models, but will not be 
        	%called between single calculation of Monte-Carlo-based uncertianty estimation
        	%the idea is that for instance in a likelihood based test with simulated
        	%catalogs, the simulated catalogs should be kept until this function is called.
        	%Often nothing has to be done in this function, but it has to be defined. 
        	
        	resetFull(obj)
        	%This should return the the calculation into the default configurations.
        	
        	
        	
        	ErrorCode = calcTest(obj,Models,ShortCat)
        	%calculate the test score, measure CPU time and return an errorcode
        	%0 for everything went fine, every other number for else.
        	%The input Models will be a cell array containing as much forecasts as specified over 
        	%TestType in the featurelist, the Type of the forecast ('Poisson','NegBin' or 'Custom')
        	%and the name of the forecasts (just use [] instead of the name if unwanted).
        	%example for Models: 	{RelmData,'Poisson','ETAS';
        	%			 RelmData,'NegBin','TripleS'};
        	
        	
        	
        	TestResults = getResults(obj)
        	%This should return the test results, the format should be a structure. In principle the 
        	%names of the field can be determine freely, but to use one of the existing plotting functions
        	%it is best to stick to a certain convention (not all fields needed, plot/test-type depended):
        	
        	%All tests
        	%	TestName:	Name of the test
        	%	TestVersion:	Version of the test, helps to compare them later
        	%	SignVal:	The significance value used, just to be sure, it is 
        	%			a passthru value
        	%	Passed:		set to true if forecast passes the tests
        	
        	%Likelihood based tests
        	%	LogLikeModel:	The Log Likelihood of the synthetic catalogs produced with the
        	%			forecast being true, means the log of the probablity that the 
        	%			synthetic catalog is product of the forecast. It should be an
        	%			array with loglikelihood of each synthetic catalog:
        	%	LogLikeCatalog:	Log Likelihood of the observed catalog, same as above but with 
        	%			the observed catalog instead of synthetic ones (only on value).
        	%	QScore:		The percentage of synthetic catalogs with a lower loglikelihood
        	%			than the catalog. Used for one-sided test
        	%	QScoreL:	Percentage of synthetic catalogs with lower likelihood than the
        	%			catalog (two-sided test)
        	%	QScoreH:	Percentage of synthetic catalogs with a higher likelihood than the
        	%			catalog (two-sided test)
        	%	numSimCat:	Number of synthetic catalogs used
        	
        	%comparison tests
        	%	TotalNum: 	Total number of earthquakes observed (used often for normalization)
        	%	ExpValA:	Number of earthquakes expected by forecast A (used often for normalization)
        	%	ExpValB:	Number of earthquakes expected by forecast B (used often for normalization)
        	%	Pval:		The probability that the Null Hypothesis is kept (mostly that A==B in performance)
        	%			Below SignVal (or Quantil) is a "success" models are different and above means
        	%			no difference.
        	%	A_better_B:	True if forecast A performs better than forecast B
        	%	Sign_better:	True if the result is significant
        	
        	%T-test like (plus the fields in comparison tests)
        	%	I_A:		Information of forecast A normalized (log-likelihood - ExpValA/TotalNum)	
        	%	I_B:		Information of forecast B normalized (log-likelihood - ExpValB/TotalNum)
        	%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
        	%	InAB:		mean Information gain of model A over B (mean(I_A-I_B)) 
        	%	deg_of_free:	degree of freedom of the t-test
        	%	Var_InAB:	Variance of the Information gain
        	%	Tval:		T value of the Information gain
        	%	Ttable:		T value in the table (depended on degree of freedom)
        	%	Conf_InAB:	Confidence intervall of mean Information Gain calculated from the P_val of
        	%			the T-test.
        	
        	
        	%W-test like (plus the fields in comparison tests)
        	%	W_plus: 	W+ value (see W-test)
        	%	W_minus:	W- value (see W-test)
        	%	W_overall:	total value of W (W+ + W-)
        	%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
        	
        	
        	ResDef = getResultDefinition(obj)
        	%This function should result the definition of the result and define how the different part should
        	%be used in the uncertianties estimation.
        	%It should return a structure with the same fields as the result, each field containing a cell array
        	%with two keyword, first one the data type of the result, second the way the field should be treated
        	%in a uncertianty estimation.
        	%The keywords for the data type are (the data type is currently not so important but it may change):
        	%	'scalar': for a single value (numeric or boolean does not matter)
        	%	'array':  more than one value either a vector or matrix of scalars
        	%	'string': a string value
        	%	'cell':	  a cell array
        	%
        	%The keywords for the uncertianity estimation
        	%	'normal': the result is treated according to its data type: statitistics are done 
        	%		  for 'scalars' the rest is packed into a cell array.
        	%	'stats':  The statistics are done like with a scalar, carefull, this may cause errors with 
        	%		  some data types (e.g. cells and strings)
        	%	'ignore': The value is not considered in the uncertianties and just the "original"
        	%		  value is stored.
        	%	'collect':The values are collected in a cell array but no statistics are done.
        	
        	%This function is OPTIONAL, but it has to be specified to at least return a empty value:
        	%
        	%function ResDef = getResultDefinition(obj)
        	%	ResDef=[];
        	%end
        	%
        	%In that case the TestObject will treat everything for the uncertianties as 'normal'
        	
        	
        	
        	
        			
        	
        	PossiblePlots = getPlotFunctions(obj)
        	%This has to return a list with plot functions with which this test results can be used.
        	%The format should be a cell array with Name, function handle, object or keyword and the config 
        	%for the plot. 
        	%example: PossiblePlots = 	{'Likelihood Histgram', 'LikeHist', PlotConfigStruct;
        	%				 'Special K test plot', @() SpecKPlot, PlotConfigStruct;
        	%				 'Summary Histogram', Plotter�bj,[]};				 
        	%Possible Keywords (no claim of completeness):
        	%For Likelihood based
        	%	'LikeHist': 	"Classic" plot with synth. catalog likelihood as histogram and the catalog
        	%			likelihood as a line
        	%	'LikeCDF':	Same as LikeHist but with a CDF instead of a histogram.
        	%	'MultiHist':	Same as LikeHist but for more than one test in one plot
        	%	'MultiBox':	Same as MultiHist but only a bar showing the bandwidth of likelihood
        	%			instead of the histograms
        	%	'CombinedLike':	Uses a normalize bar and lines to show the likelihood, also features automatic
        	%			markings of passed and not passed test. Can be used with more than one test in
        	%			the same plot
        	%	
        	%For comparison based
        	%	'SignMatrix':	Two plots, one with the ranking, the other one with a "matrix" showing the 
        	%			significance of two test (e.g. T-test and W-test)
        	%	'Ranking':	For tests like the single T-ranking test
        	%	'RatioCDF':	Like a LikeCDF plot, but for likelihood ratios 
        	%	'ClassicT':	Typical bar like plot for T-tests and similar ones
        	%	'ClassicW':	Typical vertical bar plot for W-tests and similar ones.
        	
        	%functions behind a function handle should have the following input:
        	%MyPlotFunction(plotAxis,TestResult,PlotConfig,UserConfig), with PlotConfig being the config specified 
        	%by getPlotFunctions and UserConfig being additional configurations which have to be set by the user
        	%For a Plot object, the interface TestPlotObj should be used
        	
        	
        	
        	PossibleTables = getTablesFunctions(obj)
        	%Same as for the plots for tables, which will be written into a text file 
        	%(in latex format or unformated text), the output format should be the same
        	%as for getPlotFunctions
        	%Keywords for the tables:
        	%	'LikeTable':	Normal likelihood table for one test and multiple forecasts
        	%	'BigTable':	Extended version of LikeTable, allowing to write more than
        	%			one test result as well as additional benchmark data into one big
        	%			table.
        	%	'MatrixTable':	Table typically used for comparison tests of multiple models, it is a
        	%			plot with models in row and column.
        	%
        	%functions behind a function handle should have the following input:
        	%MyTableFunction(Filename,TestResult,TableConfig,UserConfig), with TableConfig being the config 
        	%specified by getTableFunctions and UserConfig being additional configurations which have to be 
        	%set by the user.
        	%For a Table object, the interface TestTableObj should be used
        	
        	
        	
        	
        end

        
        methods 
        	%useful methods 
        	function ErrorList = getErrorList(obj)
        		PurList=[0;-1];
        		
        		Descript={	'no Error, everything fine';...
        				'Generic error'}
        		
        		ErrorList={PurList,Descript};		
        	end
        	
        	
        	function TestInfo = getTestInfo(obj)
        		%simple function for returning the TestInfo which should
        		%be a string. This is a generic function which will just 
        		%return the internal testinfo no matter if it is empty
        		%or not. A custom one can be build in case more options 
        		%are wanted or if the ModelInfo variable is not a string
        		
        		TestInfo=obj.TestInfo;
        		
        	end
        	
        	function CPUTimeData = getCalcTime(obj)
        		%simple function for returning the CPUTimeData which should
        		%be a structure with the following fields:
        			%InputDataTime: (optional) time used for calculating the input
        			%		files (e.g. catalog, config file) 
        			%		for each calculation not preparation
        			%CalcTime: Time needed for calculation of the test 
        			%	   score
        			%AddTimeInfo: Additional timer info (optional)
        			
        			
        		%This is a generic function which will just 
        		%return the internal CPUTimeData no matter if it is empty
        		%or not. Empty CPUTimeData will be ignored by the calculator
        		%so it is optional
        		
        		CPUTimeData=obj.CPUTimeData;
        	end
        		
        end	
end	

function [corrForeObj,OverallTime] = correctMagDistro(ForeObj,bvalue)
	%Build for the problems with to flat distribution of the matlab based
	%ETAS model, it will exchange the magnitude distribution with a 
	%pur GR-distribution. 
	%It is just a fix and later version should do something better
	
	
	%DE 2013
	
	import mapseis.forecast.*;
	import mapseis.forecast_testing.TestPlugIns.*;
	
        FullTimer=tic;
	

	%get timesteps (in this version it is assumbed that all forecasts
	%span the same region and the same time intervall
	TimeVector=ForeObj.TimeVector;
	TimeSpacing=ForeObj.TimeSpacing;
	
	%get grid from the first object
	TheGrid=ForeObj.getFullGrid;
	TripGrid=ForeObj.getGridNodes;
	ForeName=ForeObj.Name;
	%build a raw forecast object
	corrForeObj=ForecastObj([ForeName,'_cor']);
	corrForeObj.initForecast('Poisson',{});
	corrForeObj.addGrid(TheGrid,TripGrid);
	corrForeObj.RealTimeMode=ForeObj.RealTimeMode;
	

	%build mag distro template
	TheMags = unique(TheGrid(:,7));
	numMag=numel(TheMags);
	minMag = min(TheMags);
	MaxVal=1;
	TheCurve=MaxVal-(bvalue*(TheMags-minMag));
	
	
	%norm it so that the sum of the non log is 1
	MstCurve=10.^TheCurve/(sum(10.^TheCurve));	

	%lets use a S-test object for the sumation of the grid
	Stester = STestPlugin('');
	
	%to prepare the test
	[CSEPGrid ForeTime ForeLength]= ForeObj.getCSEPForecast(TimeVector(1));
	
	UsedBins=logical(CSEPGrid(:,10));
	Stester.UsedBins=UsedBins;
	SummedCast = Stester.buildSelector(CSEPGrid);
	
	%build a template for the forecasts
	TemplateCast=CSEPGrid;
	for mC=1:numMag
		MagSel=TemplateCast(:,7)==TheMags(mC);
		TemplateCast(MagSel,9)=MstCurve(mC);
	end
	
	NodeSel=NaN(size(TemplateCast(:,1)));
	for gC=1:numel(SummedCast(:,1))
		Sel=TemplateCast(:,1)==SummedCast(gC,1)&TemplateCast(:,3)==SummedCast(gC,3);
		NodeSel(Sel,1)=gC;
		
	end
	
	
	
	for tC=1:numel(TimeVector)
		disp(tC)
		
		hasData=false;	
		try
			[CSEPGrid ForeTime ForeLength]= ForeObj.getCSEPForecast(TimeVector(tC));
			ForecastNow{mC}=CSEPGrid;
			hasData=true;
					
		catch
			disp('Forecast not existing');
		end
		
		
		if ~hasData
			warning(['Time: ',TimeVector(tC),'has no forecast']);
			continue
		end
				
				
		%sum the magbins
		numEq=sum(CSEPGrid(UsedBins,9));
		NormCast = Stester.normForecast(CSEPGrid,numEq);
		
		%build a new forecast
		NewCast=TemplateCast;
		NewCast(:,9)=NewCast(:,9).*NormCast(NodeSel);
		
				
				
			
		%Store to forecast object
		corrForeObj.addForecast(NewCast(:,9),TimeVector(tC),ForeLength,UsedBins);
		
					
		
	end
	
	
	OverallTime=toc(FullTimer);

		
	
	
	





end

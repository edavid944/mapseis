function OutStruct = InfoTableMaker(TestObjects,TrTestName,TestSeriesName,ModelName,PrintModName,FileName)
	%A script to make a table with information gain compared to an uniform model for a 
	%multitude of different test results.

	%latexMode currently always on
	
	LatexMode=true;
	
	%PercentGain is currently not used, but it may change later so it collect it anyway
	
	%build data first
	NameEntry={};
	InfoGain=[];
	PercentGain=[];
	ObsEq=[];
	formatSpec = '%.2f';
	
	
	if isempty(PrintModName)
		PrintModName=ModelName;
	end
	
	if ~iscell(TestObjects)
		TestObjects={TestObjects};
		TestSeriesName={TestSeriesName};
	end
	
	if ~iscell(TrTestName)
		%there might be case when the test has a different name in differen forecast sets.
		%In this case TrTestName can also be a cell array. If it is not a cell array it is
		%will be converted into one, assuming it is always the same test.
		TrTestName=repmat({TrTestName},size(TestObjects));
		
	end
	
	
	for bC=1:numel(TestObjects)
		
	
		for mC=1:numel(ModelName)
			if isfield(TestObjects{bC}.SummaryResults.(TrTestName{bC}),ModelName{mC})
		
				NameEntry{end+1}=[TestSeriesName{bC},': ', PrintModName{mC}];
				InfoGain(end+1)=TestObjects{bC}.SummaryResults.(TrTestName{bC}).(ModelName{mC}).InUni;
				PercentGain(end+1)=TestObjects{bC}.SummaryResults.(TrTestName{bC}).(ModelName{mC}).PercentGain;
				ObsEq(end+1)=TestObjects{bC}.SummaryResults.(TrTestName{bC}).(ModelName{mC}).TotalNum;
			end
		end
	end
	
		
	
	
	%write the file
	
	%open file
	fid = fopen(FileName,'w'); 
	
	
	if LatexMode
		
		HeaderLineLatex1=['\hline  Model',' & ',...
					'Information Gain', ' \\'];
			
							
		TabCode='c|c';
		TabWide='2';
			
		
	
		
		%write header
		fprintf(fid,'%s','\begin{table}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{tabular}{|',TabCode,'|}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\multicolumn{',TabWide,'}{c}{Information Gain to a Uniform Forecast} \\');
		fprintf(fid,'\n');
		
		
		
		fprintf(fid,'%s',HeaderLineLatex1); 
		fprintf(fid,'\n');
		%fprintf(fid,'%s',HeaderLineLatex2); 
		%fprintf(fid,'\n');
		
	
	else
		%later
	
	
	end
	
	
		
	
	
	
	%write it 
	for lC=1:numel(NameEntry)
		%build line	
		TheLine = ['\hline  ',NameEntry{lC},'& ',...
				num2str(InfoGain(lC),formatSpec)];
		
		%end line
		TheLine = [TheLine,' \\'];
	
		%write line
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
	end
	
		
	%Finalize Table	
	fprintf(fid,'%s','\hline');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{tabular}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\caption{The average information gain per earthquake compared to an uniform forecasts over the whole time period.}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\label{tab:ADD A LABEL}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{center}');
	fprintf(fid,'\n');
	fprintf(fid,'%s','\end{table}');
	fprintf(fid,'\n');
 
	
	%close file
	fclose(fid); 
	
	
	
	%build outstruct
	OutStruct = 	struct(	'NameEntry',NameEntry,...
				'InfoGain',InfoGain,...
				'PercentGain',PercentGain,...
				'ObsEq',ObsEq);
	
	
	
end

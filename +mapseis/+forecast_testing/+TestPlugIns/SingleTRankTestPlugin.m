classdef SingleTRankTestPlugin < mapseis.forecast_testing.TestPlugIn
	%Based on the t-test calculates the information gain to an uniform forecasts	
	properties
		%TestName 
        	%Version %Has to be a STRING (consist of CodeVersion and Variant)
        	%CodeVersion %Version of the code
        	%Variant %allows to use multiple varitions of the same model in a project
        	%TestInfo %The place for a description about the test 
        	
        	%CurrentTested %contains a cell array with the names of the currently
        		      %tested forecasts, just a security measurement, in case
        		      %something goes wrong
        	
        	%TestReady
        	%CPUTimeData
        	
        	%TestReady signalizes that the test is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it should be a 
        	%structure with some defined fields, if it is used. It can be empty
        	%in this case it will ignore the data
        	
        	%no '.' allowed for TestName, Version and variant
        	
        	TestConfig
        	
        	
        	TestResults
        	UsedBins
        	
        end
    

        
        methods 
        	function obj = SingleTRankTestPlugin(Variant)
              		
        		obj.TestName='SingleTRtest';
              		obj.CodeVersion='v2';
        		obj.Variant=Variant;
              		obj.Version=[obj.CodeVersion,'_',Variant];
              		
              		obj.TestInfo=['A comparison test based on a student t-test, ',...
              				'poisson, negative binomial and discretized',...
              				' distributions are supported'];
              		
              		obj.CurrentTested={};
              		obj.TestReady=true;
              		obj.CPUTimeData=[];
              		
              		
              		%set some default config
              		DefaultConfig = struct(	'WelchT',false,...
              					'ForceBinMode',false,...
              					'SignVal',0.05,...
              					'CustomUniform',false,...
              					'UniformModel',[[]]);
              		
              		obj.TestConfig = DefaultConfig;		
              		
              		
        		
              		
              		obj.TestResults=[];
              		obj.UsedBins=[];
              		obj.ParallelMode=false;
        	end
        	
        	
        	
        	       	
        	function ConfigureTest(obj,ConfigData)
        		%used to configure the test 
        		%Only two fields currently: SignVal (default field)
        		%and numSimCat, the number of simulated catalogs
        		obj.TestConfig=ConfigData;
        		
        	end
        	
        	
        	function ConfigData = getTestConfig(obj)
			%returns configurations
			
			ConfigData = obj.TestConfig;
			
        	end
        	
        	
        	
        	function FeatureList = getFeatures(obj)
        		%List of the supported features like they are specified in the
        		%plugin interface for tests
        		
        		FeatureList = struct(	'supportPoisson',true,...
        					'supportNegBin',true,...
        					'supportCustom',true,...
        					'MixedDistro',true,...
        					'TestType','single',...
        					'InputFormat','relm',...
        					'Symmetric',true);
        	

        	        	
        	end
		        	
        	
        	function  PossibleParameters = getParameterSetting(obj)
			%Returns a description of the parameters needed for the test 
			%and description for the GUI
	
			%tk: has to be modified for the additional options
			
			%only one field needed, SignVal is default field and will be added automatically
			Checker = struct(	'Type', 'check',...
						'ValLimit',[[0 1]],...
						'Name', 'Use Welch t-test',...
						'DefaultValue',false);
						
			Forcer = struct(	'Type', 'check',...
						'ValLimit',[[0 1]],...
						'Name', 'Force Neg.Bin-like t-test',...
						'DefaultValue',false);
						
				

			PossibleParameters = struct(	'WelchT',Checker,...
							'ForceBinMode',Forcer);     				
        	
        	end
        	
        	
        	
        	function resetMonteCarlo(obj)
			%only thing it does is erasing the simulated catalogs. Needed for Monte-Carlo
			%based uncertianties estimation
		
			%nothing to do
        	end
        	
        	
        	function resetFull(obj)
        		%back to default config
        		
        		obj.CurrentTested={};
              		obj.TestReady=true;
              		obj.CPUTimeData=[];
              		
              		
              		%set some default config
              		DefaultConfig = struct(	'WelchT',false,...
              					'ForceBinMode',false,...
              					'SignVal',0.05,...
              					'CustomUniform',false,...
              					'UniformModel',[[]]);
              					
              		obj.TestConfig = DefaultConfig;		
        		
              		
              		obj.TestResults=[];
              		obj.UsedBins=[];
        	end
        	
        	
        	function ErrorCode = calcTest(obj,Models,ShortCat)
			%calculate the test score, measure CPU time and return an errorcode
			%0 for everything went fine, every other number for else.
			%The input Models will be a cell array containing as much forecasts as specified over 
			%TestType in the featurelist, the Type of the forecast ('Poisson','NegBin' or 'Custom')
			%and the name of the forecasts (just use [] instead of the name if unwanted).
			%example for Models: 	{RelmData,'Poisson','ETAS';
			%			 RelmData,'NegBin','TripleS'};
			
			ErrorCode = -1;
			
			
			%unpack the data
			ForecastName = Models{3};
			ForecastType = Models{2};
			TheCast = Models{1};
			        
			CastOK = checkForecast(obj,ForecastType,TheCast);
			
			if ~CastOK
				disp('Forecast is empty')
				obj.TestResults=[];
				return
			end
			
			
			%default results, in case catalog is empty or something similar
			[OrgExpPerCell OrgTotalExpCasts] = obj.calcTotalExpect(TheCast,ForecastType);
				
			TheRes = struct(	'TestName',obj.TestName,...
						'TestVersion',obj.CodeVersion,...
						'CustomUniformModel',obj.TestConfig.CustomUniform,...
						'TotalNum',0,...
						'numGain',0,...
						'ExpVal',OrgTotalExpCasts,...
						'Pval',1,...
						'SignVal',obj.TestConfig.SignVal,...
						'Passed',true,...
						'I_Uni',[[]],...
						'I_Perf',[[]],...
						'I_Model',[[]],...
						'RawDiffUni',[[]],...
						'RawDiffPerf',[[]],...
						'InUni',0,...
						'InPerf',0,...
						'deg_of_free_Uni',0,...
						'deg_of_free_Perf',0,...
						'Var_InUni',0,...
						'Var_InPerf',0,...
						'TvalUni',NaN,...
						'TvalPerf',NaN,...
						'TtableUni',NaN,...
						'TtablePerf',NaN,...
						'Conf_InUni',0,...
						'Conf_InPerf',0,...
						'PercentGain',0,...
						'PercentConf',0);
			
			
			
			
			
			if ~isempty(ShortCat)			
				%find cells with earthquakes
				[selected NrSelector TotalNum EqCount BinMode] = obj.generateSelector({TheCast},{ForecastType},ShortCat);
				
				if ~TotalNum==0
					
				
					
					if BinMode
						numGain=numel(NrSelector);
					else
						numGain=TotalNum;
					end
					
					
					%expected values for the model
					[OrgExpPerCell OrgTotalExpCasts] = obj.calcTotalExpect(TheCast,ForecastType);
					
					
					%extract grid
					if strcmp(ForecastType,'Custom')
						TheGrid=TheCast{1};
					else
						TheGrid=TheCast(:,1:10);
					end
					
					%generate uniform model if needed
					
					if obj.TestConfig.CustomUniform
						Unicast=obj.TestConfig.UniformModel;
						UniType='Poisson';
					else
						Unicast=TheGrid;
						Unicast(:,9)=ones(numel(TheGrid(:,9)),1);
						Unicast(~obj.UsedBins,9)=0;
						Unicast(:,9)=Unicast(:,9)/sum(Unicast(:,9))*OrgTotalExpCasts;
						UniType='Poisson'
					
					end
					
					%generate perfect model
					Perfcast = TheGrid;
					Perfcast(:,9)=zeros(numel(TheGrid(:,9)),1);
					
					for i=1:numel(NrSelector)
						Perfcast(NrSelector(i),9)=Perfcast(NrSelector(i),9)+1;
					end
					
					PerfType = 'Poisson';
					
					
					
					TheCasts = {Unicast,Perfcast,TheCast};
					ForecastTypes={UniType,PerfType,ForecastType};	    
					
					
					%calculate forecast related values (loop needed for later tests)
					ExpectCasts=[sum(Unicast(:,9)),sum(Perfcast(:,9)),OrgTotalExpCasts];
					ExpPerCell={Unicast(:,9),Perfcast(:,9),OrgExpPerCell};
					InfoGain_Forecast={};
					
					
					for i=1:numel(TheCasts)
						InfoGain_Forecast{i} = obj.calcInfoGain(TheCasts{i},ForecastTypes{i},NrSelector,EqCount,BinMode)
						InfoGain_Forecast{i} = InfoGain_Forecast{i} - (ExpectCasts(i)/TotalNum);
					end
					
					
					%now the statistic for the comparison t-test 
					%in the old version the gain was calculated the other way round with ModelB being the 
					%the actual forecast and not A, this means the results will have an oppsing sign.
					avg_InfoGain2Uni = mean(InfoGain_Forecast{3}-InfoGain_Forecast{1});
					avg_InfoGain2Perf = mean(InfoGain_Forecast{2}-InfoGain_Forecast{1});
					
					if ~obj.TestConfig.WelchT
						%normal paired t-test
						var_InfoGain2Uni = var(InfoGain_Forecast{3}-InfoGain_Forecast{1});
						var_InfoGain2Perf = var(InfoGain_Forecast{2}-InfoGain_Forecast{1});
						TvalueUni = avg_InfoGain2Uni/(sqrt(var_InfoGain2Uni)/sqrt(TotalNum)); 
						TvaluePerf = avg_InfoGain2Perf/(sqrt(var_InfoGain2Perf)/sqrt(TotalNum));
						
						%from table
						deg_of_freedom_Uni = numGain-1;
						deg_of_freedom_Perf = numGain-1;
						Tvalue_table_Uni = tinv(1-obj.TestConfig.SignVal,deg_of_freedom_Uni);
						Tvalue_table_Perf = tinv(1-obj.TestConfig.SignVal,deg_of_freedom_Perf);
						
						%confidence interval of InfoGain
						InfoGain_Conf_Uni = Tvalue_table_Uni * var_InfoGain2Uni.^(1/2) / sqrt(TotalNum);
						InfoGain_Conf_Perf = Tvalue_table_Perf * var_InfoGain2Perf.^(1/2) / sqrt(TotalNum);
						
						%and finally the p_value
						p_value_Uni = 1 - tcdf(abs(TvalueUni),deg_of_freedom_Uni);
						p_value_Perf = 1 - tcdf(abs(TvaluePerf),deg_of_freedom_Perf);
						
					else
						%Welch t-test
						avg_InfoUni = var(InfoGain_Forecast{1});
						avg_InfoPerf = var(InfoGain_Forecast{2});
						avg_InfoCand= var(InfoGain_Forecast{3});
						
						var_InfoGain2Uni  = sqrt(avg_InfoUni^2/TotalNum+avg_InfoCand^2/TotalNum);
						var_InfoGain2Perf  = sqrt(avg_InfoUni^2/TotalNum+avg_InfoPerf^2/TotalNum);
						
						
						%in this case avg_InfoGainA2B could also be used instead of the two means
						%but like this it would also work if numel(I_A)~=numel(I_B) which is the idea
						%behind the Welch t-test
						TvalueUni = (mean(InfoGain_Forecast{3})-mean(InfoGain_Forecast{1}))/var_InfoGain2Uni;
						TvaluePerf = (mean(InfoGain_Forecast{2})-mean(InfoGain_Forecast{1}))/var_InfoGain2Perf;
						
						%from table
						%special degree of freedom
						deg_of_freedom_Uni   =  ((avg_InfoCand^2/numGain+avg_InfoUni^2/numGain)^2)/...
									 ((avg_InfoCand^2/numGain)^2/(numGain-1)+...
									 (avg_InfoUni^2/numGain)^2/(numGain-1));
									 
						deg_of_freedom_Perf   =  ((avg_InfoPerf^2/numGain+avg_InfoUni^2/numGain)^2)/...
									 ((avg_InfoPerf^2/numGain)^2/(numGain-1)+...
									 (avg_InfoUni^2/numGain)^2/(numGain-1));
									 
						Tvalue_table_Uni = tinv(1-obj.TestConfig.SignVal,deg_of_freedom_Uni);
						Tvalue_table_Perf = tinv(1-obj.TestConfig.SignVal,deg_of_freedom_Perf);
						
						%confidence interval of InfoGain
						InfoGain_Conf_Uni = Tvalue_table_Uni * var_InfoGain2Uni;
						InfoGain_Conf_Perf = Tvalue_table_Perf * var_InfoGain2Perf;
						
						%and finally the p_value
						p_value_Uni = 1 - tcdf(abs(TvalueUni),deg_of_freedom_Uni);
						p_value_Perf = 1 - tcdf(abs(TvaluePerf),deg_of_freedom_Perf);
						
						
					end
					
					%calculate percentage of perfect gain	
					PercentGain=round(avg_InfoGain2Uni./avg_InfoGain2Perf*1000)./10;
					PercentConf=round(var_InfoGain2Uni./avg_InfoGain2Perf*10000)./100;
					
					%build the result
					
					
					TheRes = struct(	'TestName',obj.TestName,...
								'TestVersion',obj.CodeVersion,...
								'CustomUniformModel',obj.TestConfig.CustomUniform,...
								'TotalNum',TotalNum,...
								'numGain',numGain,...
								'ExpVal',ExpectCasts(3),...
								'Pval',p_value_Uni,...
								'SignVal',obj.TestConfig.SignVal,...
								'Passed',true,...
								'I_Uni',InfoGain_Forecast{1},...
								'I_Perf',InfoGain_Forecast{2},...
								'I_Model',InfoGain_Forecast{3},...
								'RawDiffUni',InfoGain_Forecast{3}-InfoGain_Forecast{1},...
								'RawDiffPerf',InfoGain_Forecast{2}-InfoGain_Forecast{1},...
								'InUni',avg_InfoGain2Uni,...
								'InPerf',avg_InfoGain2Perf,...
								'deg_of_free_Uni',deg_of_freedom_Uni,...
								'deg_of_free_Perf',deg_of_freedom_Perf,...
								'Var_InUni',var_InfoGain2Uni,...
								'Var_InPerf',var_InfoGain2Perf,...
								'TvalUni',TvalueUni,...
								'TvalPerf',TvaluePerf,...
								'TtableUni',Tvalue_table_Uni,...
								'TtablePerf',Tvalue_table_Perf,...
								'Conf_InUni',InfoGain_Conf_Uni,...
								'Conf_InPerf',InfoGain_Conf_Perf,...
								'PercentGain',PercentGain,...
								'PercentConf',PercentConf);
					
				end
				
			end
			
			
			obj.TestResults=TheRes
			
			ErrorCode = 0;
        	end
        	
        	
        	
        	function TestResults = getResults(obj)
			%This should return the test results, the format should be a structure. In principle the 
			%names of the field can be determine freely, but to use one of the existing plotting functions
			%it is best to stick to a certain convention (not all fields needed, plot/test-type depended):
			
			%All tests
			%	TestName:	Name of the test
			%	TestVersion:	Version of the test, helps to compare them later
			%	SignVal:	The significance value used, just to be sure, it is 
			%			a passthru value
			%	Passed:		set to true if forecast passes the tests
			
			%Likelihood based tests
			%	LogLikeModel:	The Log Likelihood of the synthetic catalogs produced with the
			%			forecast being true, means the log of the probablity that the 
			%			synthetic catalog is product of the forecast. It should be an
			%			array with loglikelihood of each synthetic catalog:
			%	LogLikeCatalog:	Log Likelihood of the observed catalog, same as above but with 
			%			the observed catalog instead of synthetic ones (only on value).
			%	QScore:		The percentage of synthetic catalogs with a lower loglikelihood
			%			than the catalog. Used for one-sided test
			%	QScoreL:	Percentage of synthetic catalogs with lower likelihood than the
			%			catalog (two-sided test)
			%	QScoreH:	Percentage of synthetic catalogs with a higher likelihood than the
			%			catalog (two-sided test)
			%	numSimCat:	Number of synthetic catalogs used
			
			%comparison tests
			%	TotalNum: 	Total number of earthquakes observed (used often for normalization)
			%	ExpValA:	Number of earthquakes expected by forecast A (used often for normalization)
			%	ExpValB:	Number of earthquakes expected by forecast B (used often for normalization)
			%	Pval:		The probability that the Null Hypothesis is kept (mostly that A==B in performance)
			%			Below SignVal (or Quantil) is a "success" models are different and above means
			%			no difference.
			%	A_better_B:	True if forecast A performs better than forecast B
			%	Sign_better:	True if the result is significant	
			
			%T-test like (plus the fields in comparison tests)
			%	I_A:		Information of forecast A normalized (log-likelihood - ExpValA/TotalNum)	
			%	I_B:		Information of forecast B normalized (log-likelihood - ExpValB/TotalNum)
			%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
			%	InAB:		mean Information gain of model A over B (mean(I_A-I_B)) 
			%	deg_og_free:	degree of freedom of the t-test
			%	Var_InAB:	Variance of the Information gain
			%	Tval:		T value of the Information gain
			%	Ttable:		T value in the table (depended on degree of freedom)
			%	Conf_InAB:	Confidence intervall of mean Information Gain calculated from the P_val of
			%			the T-test.
			
			
			%W-test like (plus the fields in comparison tests)
			%	W_plus: 	W+ value (see W-test)
			%	W_minus:	W- value (see W-test)
			%	W_overall:	total value of W (W+ + W-)
			%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
			
			
			TestResults = obj.TestResults;
			
        	end
        	
        	
        	
        	function ResDef = getResultDefinition(obj)
			%for the monte carlo approach, a definition of changing and not changing result parameters
			
						
									
						
			TheRes = struct(	'TestName',{{'string','ignore'}},...
						'TestVersion',{{'string','ignore'}},...
						'TotalNum',{{'scalar','stats'}},...
						'ExpVal',{{'scalar','stats'}},...
						'Pval',{{'scalar','stats'}},...
						'SignVal',{{'scalar','ignore'}},...
						'Passed',{{'scalar','ignore'}},...
						'I_Uni',{{'array','collect'}},...
						'I_Perf',{{'array','collect'}},...
						'I_Model',{{'array','collect'}},...
						'RawDiffUni',{{'array','collect'}},...
						'RawDiffPerf',{{'array','collect'}},...
						'InUni',{{'scalar','stats'}},...
						'InPerf',{{'scalar','stats'}},...
						'deg_of_free_Uni',{{'scalar','stats'}},...
						'deg_of_free_Perf',{{'scalar','stats'}},...
						'Var_InUni',{{'scalar','stats'}},...
						'Var_InPerf',{{'scalar','stats'}},...
						'TvalUni',{{'scalar','stats'}},...
						'TvalPerf',{{'scalar','stats'}},...
						'TtableUni',{{'scalar','stats'}},...
						'TtablePerf',{{'scalar','stats'}},...
						'Conf_InUni',{{'scalar','stats'}},...
						'Conf_InPerf',{{'scalar','stats'}},...
						'PercentGain',{{'scalar','stats'}},...
						'PercentConf',{{'scalar','stats'}});			
						
		end
        	
        	
        	
        			
        	
        	function PossiblePlots = getPlotFunctions(obj)
			%This has to return a list with plot functions with which this test results can be used.
			%The format should be a cell array with Name, function handle, object or keyword and the config 
			%for the plot. 
			%example: PossiblePlots = 	{'Likelihood Histgram', 'LikeHist', PlotConfigStruct;
			%				 'Special K test plot', @() SpecKPlot, PlotConfigStruct;
			%				 'Summary Histogram', Plotter�bj,[]};				 
			%Possible Keywords (no claim of completeness):
			%For Likelihood based
			%	'LikeHist': 	"Classic" plot with synth. catalog likelihood as histogram and the catalog
			%			likelihood as a line
			%	'LikeCDF':	Same as LikeHist but with a CDF instead of a histogram.
			%	'MultiHist':	Same as LikeHist but for more than one test in one plot
			%	'MultiBox':	Same as MultiHist but only a bar showing the bandwidth of likelihood
			%			instead of the histograms
			%	'CombinedLike':	Uses a normalize bar and lines to show the likelihood, also features automatic
			%			markings of passed and not passed test. Can be used with more than one test in
			%			the same plot
			%	
			%For comparison based
			%	'SignMatrix':	Two plots, one with the ranking, the other one with a "matrix" showing the 
			%			significance of two test (e.g. T-test and W-test)
			%	'Ranking':	For tests like the single T-ranking test
			%	'RatioCDF':	Like a LikeCDF plot, but for likelihood ratios 
			%	'ClassicT':	Typical bar like plot for T-tests and similar ones
			%	'ClassicW':	Typical vertical bar plot for W-tests and similar ones.
			
			%functions behind a function handle should have the following input:
			%MyPlotFunction(plotAxis,TestResult,PlotConfig,UserConfig), with PlotConfig being the config specified 
			%by getPlotFunctions and UserConfig being additional configurations which have to be set by the user
			%For a Plot object, the interface TestPlotObj should be used
			
			%Some fields have to be field in by the TesterObject, because it is unknown on this level
			%I have to do the config later, when I did the plots
			
			%NOT FINISHED
			ClassicTConfig = struct('TestName','TR-test',...
						'ModelNames',[[]],...
						'ForecastName',[[]],...
						'ForecastTime',[[]],...
						'SignVal',obj.TestConfig.SignVal,...
						'InfoGain','InUni',...
						'PerfectModel','InPerf',...
						'PercentGain','PercentGain',...
						'Uncertianties',false,...
						'MeanAvailable',false,...
						'MedianAvailable',false);
			
						%Field for Percent and Info Gain
						%as well as Perfect and Uniform are
						%needed and probably some other fields
						
						%FINISHED?? MAYBE
			
			
			
			
			PossiblePlots = {	'Classic T-test Bar plot', 'ClassicT', ClassicTConfig;...
						'Ranking Plot', 'Ranking', ClassicTConfig};
						
			
        	end
        	
        	
        	
        	function PossibleTables = getTablesFunctions(obj)
			%Same as for the plots for tables, which will be written into a text file 
			%(in latex format or unformated text), the output format should be the same
			%as for getPlotFunctions
			%Keywords for the tables:
			%	'LikeTable':	Normal likelihood table for one test and multiple forecasts
			%	'BigTable':	Extended version of LikeTable, allowing to write more than
			%			one test result as well as additional benchmark data into one big
			%			table.
			%	'MatrixTable':	Table typically used for comparison tests of multiple models, it is a
			%			plot with models in row and column.
			%
			%functions behind a function handle should have the following input:
			%MyTableFunction(Filename,TestResult,TableConfig,UserConfig), with TableConfig being the config 
			%specified by getTableFunctions and UserConfig being additional configurations which have to be 
			%set by the user.
			%For a Table object, the interface TestTableObj should be used
			
			%NOT FINISHED YET
			%I have to do the config later, when I did the tables
			LikeTableConfig = [];
			BigLikeConfig = [];
			
			
			
			PossibleTables = {	'Matrix Forecast-Forecast table', 'MatrixTable', LikeTableConfig};
						


        	end
        	
        	
        	
        	
        	
        	%custom functions
        	function CastOK = checkForecast(obj,ForecastType,TheCast)
        		import mapseis.util.emptier;
        		switch ForecastType
				case 'Poisson'
					CastOK = ~(all(isnan(TheCast(:,9)))|isempty(TheCast));
					
				case 'NegBin'
					CastOK = ~(all(isnan(TheCast(:,9)))|all(isnan(TheCast(:,11)))|isempty(TheCast));
					
				case 'Custom'
					Distro=TheCast{2};
					CastOK = ~(all(emptier(Distro)));
			end
        	
        	end
        	
        	
        	
        	function [selected NrSelector TotalNum EqCount BinMode] = generateSelector(obj,TheCasts,ForecastTypes,ShortCat)
			%finds the grid nodes with earthquakes in them.	
			
			%the grid has to have the same dimension for every forecast
			disp(ForecastTypes)
			switch ForecastTypes{1}
				case 'Poisson'
					SearchGrid=TheCasts{1};
				case 'NegBin'
					SearchGrid=TheCasts{1};
				case 'Custom'
					SearchGrid=TheCast{1}{1};
			end
			
			
			%only check bins which are used by all models (in this case both)
			UsedBins=true(numel(SearchGrid(:,1)),1);
			NegBinPresent=false;
			for i=1:numel(ForecastTypes)
				switch ForecastTypes{i}
					case 'Poisson'
						NowGrid=TheCasts{i};
					case 'NegBin'
						NowGrid=TheCasts{i};
						NegBinPresent=true;
					case 'Custom'
						NowGrid=TheCast{i}{1};
						NegBinPresent=true;
				end
				
				NowUsed = logical(NowGrid(:,10));
				
				UsedBins=UsedBins&NowUsed;
				
			end

			%store used
			obj.UsedBins=UsedBins;
							
			count=1;
			NrSelector=[];
			selected=false(numel(SearchGrid(:,1)),1);
			EqCount=[];
			
			%determine if search has to be done by earthquake or per bin.
			BinMode = obj.TestConfig.ForceBinMode | NegBinPresent;
			
			
			if BinMode     
				disp('Use BinMode')
				%EqCount=zeros(numel(SearchGrid(:,1)),1);
				for i=1:numel(SearchGrid(:,1))
					%no parfor steps are probably to small
					InLon=ShortCat(:,1)>=SearchGrid(i,1)&ShortCat(:,1)<SearchGrid(i,2);
					InLat=ShortCat(:,2)>=SearchGrid(i,3)&ShortCat(:,2)<SearchGrid(i,4);
					InDepth=ShortCat(:,3)>=SearchGrid(i,5)&ShortCat(:,3)<SearchGrid(i,6);
					InMag=ShortCat(:,5)>=SearchGrid(i,7)&ShortCat(:,5)<SearchGrid(i,8);
					
					
						
					if any(InLon&InLat&InDepth&InMag&obj.UsedBins(i))
						disp(count)
						NrSelector(count)=i;
						EqCount(count,1)=sum(InLon&InLat&InDepth&InMag);
						count=count+1;
						selected(i)=true;
						
					end
					
						
				end
			else
				for i=1:numel(ShortCat(:,1))
					%no parfor steps are probably to small
					%InLon=ShortCat(:,1)>=SearchGrid(i,1)&ShortCat(:,1)<SearchGrid(i,2);
					%InLat=ShortCat(:,2)>=SearchGrid(i,3)&ShortCat(:,2)<SearchGrid(i,4);
					%InDepth=ShortCat(:,3)>=SearchGrid(i,5)&ShortCat(:,3)<SearchGrid(i,6);
					%InMag=ShortCat(:,5)>=SearchGrid(i,7)&ShortCat(:,5)<SearchGrid(i,8);
					
					
					InLon=SearchGrid(:,1)<=ShortCat(i,1)&SearchGrid(:,2)>ShortCat(i,1);	
					InLat=SearchGrid(:,3)<=ShortCat(i,2)&SearchGrid(:,4)>ShortCat(i,2);	
					InDepth=SearchGrid(:,5)<=ShortCat(i,3)&SearchGrid(:,6)>ShortCat(i,3);	
					InMag=SearchGrid(:,7)<=ShortCat(i,5)&SearchGrid(:,8)>ShortCat(i,5);	
						
					if any(InLon&InLat&InDepth&InMag&obj.UsedBins)
						NrSelector(count)=find(InLon&InLat&InDepth&InMag&obj.UsedBins);
						EqCount(count,1)=sum(InLon&InLat&InDepth&InMag&obj.UsedBins);
						count=count+1;
						selected(InLon&InLat&InDepth&InMag&obj.UsedBins)=true;
					end
					
						
				end
	
			end
			
			TotalNum=count-1;
			
			
		end	
		
		
		
		
		function [ExpPerCell TotalExpect] = calcTotalExpect(obj,TheCast,ForecastType);
			%calculates the total value of expectance 
			
			
			switch ForecastType
				case 'Poisson'
					%most simple case, it is just the sum of the rates
					ExpPerCell = TheCast(:,9); 
					TotalExpect = sum(TheCast(obj.UsedBins,9));				
										
					
				case 'NegBin'
					%the mean of the neg. binomial distros 
					%assuming (TheCast(:,9)=r,TheCast(:,11)=p) 
					ExpPerCell = ((1-TheCast(:,11)).*TheCast(:,9))./TheCast(:,11);
					TotalExpect = sum(ExpPerCell(obj.UsedBins));
					
					
					
				case 'Custom'
					SearchGrid=TheCast{1};
					Distros=TheCast{2};
					
					%This case is a bit more difficult, there is more than one way to do this
					%one is to treat the problem cell-wise, for each cell calculate the expect
					%value (basically just a weighted average) and sum them up. The alternative
					%would be to calculate a total distribution and calculate the expected value
					%of that (more complicated and needs a lot more time)
					%I think for the moment I use the first method
													
					for i=1:numel(Distros)
						ThisDistro=Distros{i};
						NumEqRay = 0:(numel(ThisDistro)-1);
						ExpPerCell(i) = sum(NumEqRay.*ThisDistro);
						
											
					end
					
					TotalExpect = sum(ExpPerCell(obj.UsedBins));					
					
					
					
			end
			
		
		end
			
		
		
		function InfoGain_Forecast = calcInfoGain(obj,TheCast,ForecastType,NrSelector,EqCount,BinMode)
			%returns either the log of the rates (pur poisson testing) or the log-likelihood
			%(neg. binomial and BinMode)
			
			switch ForecastType
				case 'Poisson'
					if ~BinMode
						InfoGain_Forecast = log(TheCast(NrSelector,9)); 				
					else
					
						if ~isempty(NrSelector)
							InfoGain_Forecast = -TheCast(NrSelector,9)+EqCount.*log(TheCast(NrSelector,9))-log(factorial(EqCount));
							%InfoGain_Forecast = log(pdf('poiss',EqCount,TheCast(NrSelector,9)));
						else
							InfoGain_Forecast = [];
						end	
					end					
					
				case 'NegBin'
					if ~isempty(NrSelector)
						InfoGain_Forecast = log(pdf('nbin',EqCount,TheCast(NrSelector,9),TheCast(NrSelector,11)));
					else
						InfoGain_Forecast = [];
					end	
					
				case 'Custom'
					SearchGrid=TheCast{1};
					Distros=TheCast{2}(NrSelector);
					
					InfoGain_Forecast = log(cellfun(@(x,y) obj.pickProbBin(x,y),Distros,num2cell(EqCount)));				
					
					
					
			end
		end	
			
		
		
        	function val = pickProbBin(obj,Distro,NumEQ)
        		%short function, needed for picking the probability of a grid node
        		try
        			val = log(Distro(NumEQ+1));
        		catch
        			val = NaN;
        		end
        	end
		
		
        	
		
		
        end

   
end	

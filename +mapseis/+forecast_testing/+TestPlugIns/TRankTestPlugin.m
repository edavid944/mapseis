classdef TRankTestPlugin < mapseis.forecast_testing.TestPlugIn
	%Based on the t-test ranks the inputed models according to the information 
	%gain to an uniforn model
	
	properties
		%TestName 
        	%Version %Has to be a STRING (consist of CodeVersion and Variant)
        	%CodeVersion %Version of the code
        	%Variant %allows to use multiple varitions of the same model in a project
        	%TestInfo %The place for a description about the test 
        	
        	%CurrentTested %contains a cell array with the names of the currently
        		      %tested forecasts, just a security measurement, in case
        		      %something goes wrong
        	
        	%TestReady
        	%CPUTimeData
        	
        	%TestReady signalizes that the test is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it should be a 
        	%structure with some defined fields, if it is used. It can be empty
        	%in this case it will ignore the data
        	
        	%no '.' allowed for TestName, Version and variant
        	
        	TestConfig
        	
        	
        	TestResults
        	UsedBins
        	
        end
    

        
        methods 
        	function obj = TRankTestPlugin(Variant)
              		
        		obj.TestName='TRtest';
              		obj.CodeVersion='v1';
        		obj.Variant=Variant;
              		obj.Version=[obj.CodeVersion,'_',Variant];
              		
              		obj.TestInfo=['A comparison test based on a student t-test, ',...
              				'poisson, negative binomial and discretized',...
              				' distributions are supported'];
              		
              		obj.CurrentTested={};
              		obj.TestReady=true;
              		obj.CPUTimeData=[];
              		
              		
              		%set some default config
              		DefaultConfig = struct(	'WelchT',false,...
              					'ForceBinMode',false,...
              					'CalcSign',false,...
              					'SignVal',0.05);
              		obj.TestConfig = DefaultConfig;		
              		
        		
              		
              		obj.TestResults=[];
              		obj.UsedBins=[];
              		obj.ParallelMode=false;
        	end
        	
        	
        	
        	       	
        	function ConfigureTest(obj,ConfigData)
        		%used to configure the test 
        		%Only two fields currently: SignVal (default field)
        		%and numSimCat, the number of simulated catalogs
        		obj.TestConfig=ConfigData;
        		
        	end
        	
        	
        	function ConfigData = getTestConfig(obj)
			%returns configurations
			
			ConfigData = obj.TestConfig;
			
        	end
        	
        	
        	
        	function FeatureList = getFeatures(obj)
        		%List of the supported features like they are specified in the
        		%plugin interface for tests
        		
        		FeatureList = struct(	'supportPoisson',true,...
        					'supportNegBin',true,...
        					'supportCustom',true,...
        					'MixedDistro',true,...
        					'TestType','ranking',...
        					'InputFormat','relm',...
        					'Symmetric',true);
        	

        	        	
        	end
		        	
        	
        	function  PossibleParameters = getParameterSetting(obj)
			%Returns a description of the parameters needed for the test 
			%and description for the GUI
	
			%only one field needed, SignVal is default field and will be added automatically
			Checker = struct(	'Type', 'check',...
						'ValLimit',[[0 1]],...
						'Name', 'Use Welch t-test',...
						'DefaultValue',false);
						
				
	
				

			PossibleParameters = struct(	'WelchT',Checker);     			
        	
        	end
        	
        	
        	
        	function resetMonteCarlo(obj)
			%only thing it does is erasing the simulated catalogs. Needed for Monte-Carlo
			%based uncertianties estimation
		
			%nothing to do
        	end
        	
        	
        	function resetFull(obj)
        		%back to default config
        		
        		obj.CurrentTested={};
              		obj.TestReady=true;
              		obj.CPUTimeData=[];
              		
              		
              		%set some default config
              		DefaultConfig = struct(	'welchT',true,...
              					'SignVal',0.05);
              		obj.TestConfig = DefaultConfig;		
        		
              		
              		obj.TestResults=[];
              		obj.UsedBins=[];
        	end
        	
        	
        	function ErrorCode = calcTest(obj,Models,ShortCat)
			%calculate the test score, measure CPU time and return an errorcode
			%0 for everything went fine, every other number for else.
			%The input Models will be a cell array containing as much forecasts as specified over 
			%TestType in the featurelist, the Type of the forecast ('Poisson','NegBin' or 'Custom')
			%and the name of the forecasts (just use [] instead of the name if unwanted).
			%example for Models: 	{RelmData,'Poisson','ETAS';
			%			 RelmData,'NegBin','TripleS'};
			
			ErrorCode = -1;
			
			
			%unpack the data
			ForecastNameA = Models{1,3};
			ForecastTypeA = Models{1,2};
			TheCastA = Models{1,1};
			
			ForecastNameB = Models{2,3};
			ForecastTypeB = Models{2,2};
			TheCastB = Models{2,1};
			
			TheCasts={TheCastA,TheCastB};
			ForecastTypes={ForecastTypeY,ForecastTypeB};	            
						
			%find cells with earthquakes
			[selected NrSelector TotalNum] = obj.generateSelector(TheCasts,ForecastTypes,ShortCat);
			
			%calculate forecast related values (loop needed for later tests)
			ExpectCasts=[];
			ExpPerCell={};
			InfoGain_Forecast={};
			
			for i=1:numel(TheCasts)
				[ExpPerCell{i} TotalExpCasts(i)] = obj.calcTotalExpect(TheCasts{i},ForecastTypes{i});
				InfoGain_Forecast{i} = log(ExpPerCell{i}(NrSelector)) - (TotalExpCasts(i)/TotalNum);
			
				
			end
			
			
			%now the statistic for the comparison t-test (has to be modified for the later ranking based test
			avg_InfoGainA2B = mean(InfoGain_Forecast{1}-InfoGain_Forecast{2});
			
			if ~obj.TestConfig.WelchT
				%normal paired t-test
				var_InfoGainA2B = var(InfoGain_Forecast{1}-InfoGain_Forecast{2});
				Tvalue = avg_InfoGainA2B/(sqrt(var_InfoGainA2B)/sqrt(TotalNum)); 
				
				%from table
				deg_of_freedom = TotalNum-1;
				Tvalue_table = tinv(1-obj.TestConfig.SignVal,deg_of_freedom);
				
				%confidence interval of InfoGain
				InfoGain_Conf = Tvalue_table * var_InfoGainA2B.^(1/2) / sqrt(TotalNum);
				
				%and finally the p_value
				p_value = 1 - tcdf(abs(Tvalue),deg_of_freedom);
				
			else
				%Welch t-test
				avg_InfoA = var(InfoGain_Forecast{1};
				avg_InfoB = var(InfoGain_Forecast{2};
				var_InfoGainA2B  = sqrt(avg_InfoA^2/TotalNum+avg_InfoB^2/TotalNum);
				
				
				
				%in this case avg_InfoGainA2B could also be used instead of the two means
				%but like this it would also work if numel(I_A)~=numel(I_B) which is the idea
				%behind the Welch t-test
				Tvalue = (mean(InfoGain_Forecast{1})-mean(InfoGain_Forecast{2}))/var_InfoGainA2B;
				
				
				%from table
				%special degree of freedom
				deg_of_freedom   =  ((avg_InfoA^2/TotalNum+avg_InfoB^2/TotalNum)^2)/...
							 ((avg_InfoA^2/TotalNum)^2/(TotalNum-1)+...
							 (avg_InfoB^2/TotalNum)^2/(TotalNum-1));
							 
				Tvalue_table = tinv(1-obj.TestConfig.SignVal,deg_of_freedom);
				
				%confidence interval of InfoGain
				InfoGain_Conf = Tvalue_table * var_InfoGainA2B;
				
				%and finally the p_value
				p_value = 1 - tcdf(abs(Tvalue),deg_of_freedom);
				
				
			end
			
				
		
			
			%build the result
			A_better_B = avg_InfoGainA2B > 0;
			Sign_better=p_value<=obj.TestConfig.SignVal;
			
			TheRes = struct(	'TestName',obj.TestName,...
						'TestVersion',obj.CodeVersion,...
						'TotalNum',TotalNum,...
						'ExpValA',ExpectCasts(1),...
						'ExpValB',ExpectCasts(2),...
						'Pval',p_value,...
						'SignVal',obj.TestConfig.SignVal,...
						'A_better_B',A_better_B,...
						'Sign_better',Sign_better,...
						'I_A',InfoGain_Forecast{1},...
						'I_B',InfoGain_Forecast{B},...
						'RawDiff',,...
						'InAB',avg_InfoGainA2B,...
						'deg_of_free',deg_of_freedom,...
						'Var_InAB',var_InfoGainA2B,...
						'Tval',Tvalue,...
						'Ttable',Tvalue_table,...
						'Conf_InAB',InfoGain_Conf);
			obj.TestResults=TheRes
			
			ErrorCode = 0;
        	
        	end
        	
        	
        	
        	function TestResults = getResults(obj)
			%This should return the test results, the format should be a structure. In principle the 
			%names of the field can be determine freely, but to use one of the existing plotting functions
			%it is best to stick to a certain convention (not all fields needed, plot/test-type depended):
			
			%All tests
			%	TestName:	Name of the test
			%	TestVersion:	Version of the test, helps to compare them later
			%	SignVal:	The significance value used, just to be sure, it is 
			%			a passthru value
			%	Passed:		set to true if forecast passes the tests
			
			%Likelihood based tests
			%	LogLikeModel:	The Log Likelihood of the synthetic catalogs produced with the
			%			forecast being true, means the log of the probablity that the 
			%			synthetic catalog is product of the forecast. It should be an
			%			array with loglikelihood of each synthetic catalog:
			%	LogLikeCatalog:	Log Likelihood of the observed catalog, same as above but with 
			%			the observed catalog instead of synthetic ones (only on value).
			%	QScore:		The percentage of synthetic catalogs with a lower loglikelihood
			%			than the catalog. Used for one-sided test
			%	QScoreL:	Percentage of synthetic catalogs with lower likelihood than the
			%			catalog (two-sided test)
			%	QScoreH:	Percentage of synthetic catalogs with a higher likelihood than the
			%			catalog (two-sided test)
			%	numSimCat:	Number of synthetic catalogs used
			
			%comparison tests
			%	TotalNum: 	Total number of earthquakes observed (used often for normalization)
			%	ExpValA:	Number of earthquakes expected by forecast A (used often for normalization)
			%	ExpValB:	Number of earthquakes expected by forecast B (used often for normalization)
			%	Pval:		The probability that the Null Hypothesis is kept (mostly that A==B in performance)
			%			Below SignVal (or Quantil) is a "success" models are different and above means
			%			no difference.
			%	A_better_B:	True if forecast A performs better than forecast B
			%	Sign_better:	True if the result is significant	
			
			%T-test like (plus the fields in comparison tests)
			%	I_A:		Information of forecast A normalized (log-likelihood - ExpValA/TotalNum)	
			%	I_B:		Information of forecast B normalized (log-likelihood - ExpValB/TotalNum)
			%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
			%	InAB:		mean Information gain of model A over B (mean(I_A-I_B)) 
			%	deg_og_free:	degree of freedom of the t-test
			%	Var_InAB:	Variance of the Information gain
			%	Tval:		T value of the Information gain
			%	Ttable:		T value in the table (depended on degree of freedom)
			%	Conf_InAB:	Confidence intervall of mean Information Gain calculated from the P_val of
			%			the T-test.
			
			
			%W-test like (plus the fields in comparison tests)
			%	W_plus: 	W+ value (see W-test)
			%	W_minus:	W- value (see W-test)
			%	W_overall:	total value of W (W+ + W-)
			%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
			
			
			TestResults = obj.TestResults;
			
        	end
        	
        	
        	
        	function ResDef = getResultDefinition(obj)
			%for the monte carlo approach, a definition of changing and not changing result parameters
			
						
			ResDef = struct(	'TestName',{{'string','ignore'}},...
						'TestVersion',{{'string','ignore'}},...
						'TotalNum',{{'scalar','stats'}},...
						'ExpValA',{{'scalar','stats'}},...
						'ExpValB',{{'scalar','stats'}},...
						'Pval',{{'scalar','stats'}},...
						'SignVal',{{'scalar','ignore'}},...
						'A_better_B',{{'scalar','stats'}},...
						'Sign_better',{{'scalar','stats'}},...
						'I_A',{{'array','collect'}},...
						'I_B',{{'array','collect'}},...
						'RawDiff',{{'array','collect'}},...
						'InAB',{{'scalar','stats'}},...
						'deg_of_free',{{'scalar','stats'}},...
						'Var_InAB',{{'scalar','stats'}},...
						'Tval',{{'scalar','stats'}},...
						'Ttable',{{'scalar','stats'}},...
						'Conf_InAB',{{'scalar','stats'}});
		end
        	
        	
        	
        			
        	
        	function PossiblePlots = getPlotFunctions(obj)
			%This has to return a list with plot functions with which this test results can be used.
			%The format should be a cell array with Name, function handle, object or keyword and the config 
			%for the plot. 
			%example: PossiblePlots = 	{'Likelihood Histgram', 'LikeHist', PlotConfigStruct;
			%				 'Special K test plot', @() SpecKPlot, PlotConfigStruct;
			%				 'Summary Histogram', Plotter�bj,[]};				 
			%Possible Keywords (no claim of completeness):
			%For Likelihood based
			%	'LikeHist': 	"Classic" plot with synth. catalog likelihood as histogram and the catalog
			%			likelihood as a line
			%	'LikeCDF':	Same as LikeHist but with a CDF instead of a histogram.
			%	'MultiHist':	Same as LikeHist but for more than one test in one plot
			%	'MultiBox':	Same as MultiHist but only a bar showing the bandwidth of likelihood
			%			instead of the histograms
			%	'CombinedLike':	Uses a normalize bar and lines to show the likelihood, also features automatic
			%			markings of passed and not passed test. Can be used with more than one test in
			%			the same plot
			%	
			%For comparison based
			%	'SignMatrix':	Two plots, one with the ranking, the other one with a "matrix" showing the 
			%			significance of two test (e.g. T-test and W-test)
			%	'RatioCDF':	Like a LikeCDF plot, but for likelihood ratios 
			%	'ClassicT':	Typical bar like plot for T-tests and similar ones
			%	'ClassicW':	Typical vertical bar plot for W-tests and similar ones.
			
			%functions behind a function handle should have the following input:
			%MyPlotFunction(plotAxis,TestResult,PlotConfig,UserConfig), with PlotConfig being the config specified 
			%by getPlotFunctions and UserConfig being additional configurations which have to be set by the user
			%For a Plot object, the interface TestPlotObj should be used
			
			%Some fields have to be field in by the TesterObject, because it is unknown on this level
			%I have to do the config later, when I did the plots
			
			%NOT FINISHED
			ClassicTConfig = struct('TestName','L-test',...
						'TestSymbols_low',{{'$$\gamma=$$','$$\gamma_m=$$','$$\gamma_m=$$'}},...
						'TestSymbols_hi',{{}},...
						'ForecastName',[[]],...
						'ForecastTime',[[]],...
						'XaxisLabel','Joint log-likelihood   ',...
						'YaxisLabel','Number of occurence   ',...
						'OneSide',true,...
						'SignVal',obj.TestConfig.SignVal,...
						'ScoreField_low','QScore',...
						'ScoreField_hi',[],...
						'LogCatField','LogLikeCatalog',...
						'LogSynthCatField','LogLikeModel',...
						'Uncertianties',false,...
						'MeanAvailable',false,...
						'MedianAvailable',false);
			
	 	
			
			
			
			
			PossiblePlots = {	'Classic T-test Bar plot', 'ClassicT', ClassicTConfig;...
						'Ranking Plot with Sign. Matrix', 'SignMatrix', ClassicTConfig};
						
			
        	end
        	
        	
        	
        	function PossibleTables = getTablesFunctions(obj)
			%Same as for the plots for tables, which will be written into a text file 
			%(in latex format or unformated text), the output format should be the same
			%as for getPlotFunctions
			%Keywords for the tables:
			%	'LikeTable':	Normal likelihood table for one test and multiple forecasts
			%	'BigTable':	Extended version of LikeTable, allowing to write more than
			%			one test result as well as additional benchmark data into one big
			%			table.
			%	'MatrixTable':	Table typically used for comparison tests of multiple models, it is a
			%			plot with models in row and column.
			%
			%functions behind a function handle should have the following input:
			%MyTableFunction(Filename,TestResult,TableConfig,UserConfig), with TableConfig being the config 
			%specified by getTableFunctions and UserConfig being additional configurations which have to be 
			%set by the user.
			%For a Table object, the interface TestTableObj should be used
			
			%NOT FINISHED YET
			%I have to do the config later, when I did the tables
			LikeTableConfig = [];
			BigLikeConfig = [];
			
			
			
			PossibleTables = {	'Matrix Forecast-Forecast table', 'MatrixTable', LikeTableConfig};
						


        	end
        	
        	
        	
        	
        	
        	%custom functions
        	function [selected NrSelector TotalNum] = generateSelector(obj,TheCasts,ForecastTypes,ShortCat)
			%finds the grid nodes with earthquakes in them.	
			
			%the grid has to have the same dimension for every forecast
			disp(ForecastTypes)
			switch ForecastTypes{1}
				case 'Poisson'
					SearchGrid=TheCasts{1};
				case 'NegBin'
					SearchGrid=TheCasts{1};
				case 'Custom'
					SearchGrid=TheCast{1}{1};
			end
			
			
			%only check bins which are used by all models (in this case both)
			UsedBins=true(numel(SearchGrid(:,1)),1);
			for i=1:numel(ForecastTypes)
				switch ForecastTypes{i}
					case 'Poisson'
						NowGrid=TheCasts{i};
					case 'NegBin'
						NowGrid=TheCasts{i};
					case 'Custom'
						NowGrid=TheCast{i}{1};
				end
				
				NowUsed = logical(NowGrid(:,10));
				
				UsedBins=UsedBins&NowUsed;
				
			end

			%store used
			obj.UsedBins=UsedBins
							
			count=1;
			NrSelector=[];
			selected=false(numel(SearchGrid(:,1)),1);
			
			
				
			for i=1:numel(ShortCat(:,1))
				%no parfor steps are probably to small
				%InLon=ShortCat(:,1)>=SearchGrid(i,1)&ShortCat(:,1)<SearchGrid(i,2);
				%InLat=ShortCat(:,2)>=SearchGrid(i,3)&ShortCat(:,2)<SearchGrid(i,4);
				%InDepth=ShortCat(:,3)>=SearchGrid(i,5)&ShortCat(:,3)<SearchGrid(i,6);
				%InMag=ShortCat(:,5)>=SearchGrid(i,7)&ShortCat(:,5)<SearchGrid(i,8);
				
				
				InLon=SearchGrid(:,1)<=ShortCat(i,1)&SearchGrid(:,2)>ShortCat(i,1);	
				InLat=SearchGrid(:,3)<=ShortCat(i,2)&SearchGrid(:,4)>ShortCat(i,2);	
				InDepth=SearchGrid(:,5)<=ShortCat(i,3)&SearchGrid(:,6)>ShortCat(i,3);	
				InMag=SearchGrid(:,7)<=ShortCat(i,5)&SearchGrid(:,8)>ShortCat(i,5);	
					
				if any(InLon&InLat&InDepth&InMag&obj.UsedBins)
					NrSelector(count)=find(InLon&InLat&InDepth&InMag&obj.UsedBins);
					count=count+1;
					selected(InLon&InLat&InDepth&InMag&obj.UsedBins)=true;
				end
				
					
			end
	
			
			TotalNum=count-1;
			
			
		end	
		
		
		
		function [ExpPerCell TotalExpect] = calcTotalExpect(obj,TheCast,ForecastType);
			%calculates the total value of expectance
			
			switch ForecastType
				case 'Poisson'
					%most simple case, it is just the sum of the rates
					ExpPerCell = TheCast(:,9); 
					TotalExpect = sum(TheCast(obj.UsedBins,9));				
										
					
				case 'NegBin'
					%the mean of the neg. binomial distros 
					%assuming (TheCast(:,9)=r,TheCast(:,11)=p) 
					ExpPerCell = (TheCast(:,9).*TheCast(:,11))./(1-TheCast(:,11)).^2 
					TotalExpect = sum(ExpPerCell(obj.UsedBins));
					
					
				case 'Custom'
					SearchGrid=TheCast{1};
					Distros=TheCast{2};
					
					%This case is a bit more difficult, there is more than one way to do this
					%one is to treat the problem cell-wise, for each cell calculate the expect
					%value (basically just a weighted average) and sum them up. The alternative
					%would be to calculate a total distribution and calculate the expected value
					%of that (more complicated and needs a lot more time)
					%I think for the moment I use the first method
													
					for i=1:numel(Distros)
						ThisDistro=Distros{i};
						NumEqRay = 0:(numel(ThisDistro)-1);
						ExpPerCell(i) = sum(NumEqRay.*ThisDistro);
						
											
					end
					
					TotalExpect = sum(ExpPerCell(obj.UsedBins));					
					
					
					
			end
			
		
		end
        	
		
		
        end

   
end	

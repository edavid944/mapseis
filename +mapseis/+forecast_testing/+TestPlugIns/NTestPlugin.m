classdef NTestPlugin < mapseis.forecast_testing.TestPlugIn
	%N-test plugin for poisson neg.bin and custom distribution
	
	properties
		%TestName 
        	%Version %Has to be a STRING (consist of CodeVersion and Variant)
        	%CodeVersion %Version of the code
        	%Variant %allows to use multiple varitions of the same model in a project
        	%TestInfo %The place for a description about the test 
        	
        	%CurrentTested %contains a cell array with the names of the currently
        		      %tested forecasts, just a security measurement, in case
        		      %something goes wrong
        	
        	%TestReady
        	%CPUTimeData
        	
        	%TestReady signalizes that the test is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it should be a 
        	%structure with some defined fields, if it is used. It can be empty
        	%in this case it will ignore the data
        	
        	%no '.' allowed for TestName, Version and variant
        	
        	TestConfig
        	SimulatedCats
        	NumSimCat
        	TestResults
        	UsedBins
        	
        end
    

        
        methods 
        	function obj = NTestPlugin(Variant)
              		
        		obj.TestName='Ntest';
              		obj.CodeVersion='v1';
        		obj.Variant=Variant;
              		obj.Version=[obj.CodeVersion,'_',Variant];
              		
              		obj.TestInfo=['Classic CSEP (N)umber test, ',...
              				'poisson, negative binomial and discretized',...
              				' distributions are supported'];
              		
              		obj.CurrentTested={};
              		obj.TestReady=true;
              		obj.CPUTimeData=[];
              		
              		
              		%set some default config
              		DefaultConfig = struct(	'numSimCat',5000,...
              					'SignVal',0.05);
              		obj.TestConfig = DefaultConfig;		
        		
              		obj.SimulatedCats={};
              		obj.NumSimCat=[];
              		obj.TestResults=[];
              		obj.UsedBins=[];
              		obj.ParallelMode=false;
        	end
        	
        	
        	
        	       	
        	function ConfigureTest(obj,ConfigData)
        		%used to configure the test 
        		%Only two fields currently: SignVal (default field)
        		%and numSimCat, the number of simulated catalogs
        		obj.TestConfig=ConfigData;
        		
        	end
        	
        	
        	function ConfigData = getTestConfig(obj)
			%returns configurations
			
			ConfigData = obj.TestConfig;
			
        	end
        	
        	
        	
        	function FeatureList = getFeatures(obj)
        		%List of the supported features like they are specified in the
        		%plugin interface for tests
        		
        		FeatureList = struct(	'supportPoisson',true,...
        					'supportNegBin',true,...
        					'supportCustom',true,...
        					'MixedDistro',true,...
        					'TestType','single',...
        					'InputFormat','relm',...
        					'Symmetric',true);
        	

        	        	
        	end
		        	
        	
        	function  PossibleParameters = getParameterSetting(obj)
			%Returns a description of the parameters needed for the test 
			%and description for the GUI
	
			%only one field needed, SignVal is default field and will be added automatically
			EntrySim = struct(	'Type', 'edit',...
						'Format', 'float',...
						'ValLimit',[[1 999999]],...
						'Name', 'Num. synth. Catalogs',...
						'DefaultValue',5000);
						
				
	
				

			PossibleParameters = struct(	'numSimCat',EntrySim);     			
        	
        	end
        	
        	
        	
        	function resetMonteCarlo(obj)
			%only thing it does is erasing the simulated catalogs. Needed for Monte-Carlo
			%based uncertianties estimation
			obj.SimulatedCats={};
			obj.NumSimCat=[];
			obj.UsedBins=[];
        	end
        	
        	
        	function resetFull(obj)
        		%back to default config
        		
        		obj.CurrentTested={};
              		obj.TestReady=true;
              		obj.CPUTimeData=[];
              		
              		
              		%set some default config
              		DefaultConfig = struct(	'numSimCat',5000,...
              					'SignVal',0.05);
              		obj.TestConfig = DefaultConfig;		
        		
              		obj.SimulatedCats={};
              		obj.NumSimCat=[];
              		obj.TestResults=[];
              		obj.UsedBins=[];
        	end
        	
        	
        	function ErrorCode = calcTest(obj,Models,ShortCat)
			%calculate the test score, measure CPU time and return an errorcode
			%0 for everything went fine, every other number for else.
			%The input Models will be a cell array containing as much forecasts as specified over 
			%TestType in the featurelist, the Type of the forecast ('Poisson','NegBin' or 'Custom')
			%and the name of the forecasts (just use [] instead of the name if unwanted).
			%example for Models: 	{RelmData,'Poisson','ETAS';
			%			 RelmData,'NegBin','TripleS'};
			
			ErrorCode = -1;
			
			
			%unpack the data
			ForecastName = Models{3};
			ForecastType = Models{2};
			TheCast = Models{1};
			
			CastOK = checkForecast(obj,ForecastType,TheCast);
			
			if ~CastOK
				disp('Forecast is empty')
				obj.TestResults=[];
				return
			end
			
			
			
			%generate synthetic catalogs if not existing (calculate the number of eq in those as well)
			if isempty(obj.SimulatedCats)
				obj.generateSynthCat(TheCast,ForecastType);
			end
			
			if ~isempty(ShortCat)
				%find cells with earthquakes
				[selected EqCount] = obj.findNodes(TheCast,ForecastType,ShortCat);
				
				%calculate number of eq in catalog
				NumACat = nansum(EqCount);
			else
				NumACat = 0;
			end
			
			%build the result
			TheQuantLow=sum(obj.NumSimCat<=NumACat)/obj.TestConfig.numSimCat;
			TheQuantHi=sum(obj.NumSimCat>=NumACat)/obj.TestConfig.numSimCat;
			Passed=TheQuantLow>=obj.TestConfig.SignVal/2 & TheQuantHi>=obj.TestConfig.SignVal/2;
			
			TheRes = struct(	'TestName',obj.TestName,...
						'TestVersion',obj.CodeVersion,...
						'SignVal',obj.TestConfig.SignVal,...
						'Passed',Passed,...
						'LogLikeModel',obj.NumSimCat,...
						'LogLikeCatalog',NumACat,...
						'QScoreL',TheQuantLow,...
						'QScoreH',TheQuantHi,...
						'numSimCat',obj.TestConfig.numSimCat);
			obj.TestResults=TheRes
		
			ErrorCode = 0;
        	end
        	
        	
        	
        	function TestResults = getResults(obj)
			%This should return the test results, the format should be a structure. In principle the 
			%names of the field can be determine freely, but to use one of the existing plotting functions
			%it is best to stick to a certain convention (not all fields needed, plot/test-type depended):
			
			%All tests
			%	TestName:	Name of the test
			%	TestVersion:	Version of the test, helps to compare them later
			%	SignVal:	The significance value used, just to be sure, it is 
			%			a passthru value
			%	Passed:		set to true if forecast passes the tests
			
			%Likelihood based tests
			%	LogLikeModel:	The Log Likelihood of the synthetic catalogs produced with the
			%			forecast being true, means the log of the probablity that the 
			%			synthetic catalog is product of the forecast. It should be an
			%			array with loglikelihood of each synthetic catalog:
			%	LogLikeCatalog:	Log Likelihood of the observed catalog, same as above but with 
			%			the observed catalog instead of synthetic ones (only on value).
			%	QScore:		The percentage of synthetic catalogs with a lower loglikelihood
			%			than the catalog. Used for one-sided test
			%	QScoreL:	Percentage of synthetic catalogs with lower likelihood than the
			%			catalog (two-sided test)
			%	QScoreH:	Percentage of synthetic catalogs with a higher likelihood than the
			%			catalog (two-sided test)
			%	numSimCat:	Number of synthetic catalogs used
			
			%comparison tests
			%	TotalNum: 	Total number of earthquakes observed (used often for normalization)
			%	ExpValA:	Number of earthquakes expected by forecast A (used often for normalization)
			%	ExpValB:	Number of earthquakes expected by forecast B (used often for normalization)
			%	Pval:		The probability that the Null Hypothesis is kept (mostly that A==B in performance)
			%			Below SignVal (or Quantil) is a "success" models are different and above means
			%			no difference.
			%	A_better_B:	True if forecast A performs better than forecast B
			
			%T-test like (plus the fields in comparison tests)
			%	I_A:		Information of forecast A normalized (log-likelihood - ExpValA/TotalNum)	
			%	I_B:		Information of forecast B normalized (log-likelihood - ExpValB/TotalNum)
			%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
			%	InAB:		mean Information gain of model A over B (mean(I_A-I_B)) 
			%	deg_og_free:	degree of freedom of the t-test
			%	Var_InAB:	Variance of the Information gain
			%	Tval:		T value of the Information gain
			%	Ttable:		T value in the table (depended on degree of freedom)
			%	Conf_InAB:	Confidence intervall of mean Information Gain calculated from the P_val of
			%			the T-test.
			
			
			%W-test like (plus the fields in comparison tests)
			%	W_plus: 	W+ value (see W-test)
			%	W_minus:	W- value (see W-test)
			%	W_overall:	total value of W (W+ + W-)
			%	RawDiff:	difference of the log-likelihoods (needed for combining results later)
			
			
			TestResults = obj.TestResults;
			
        	end
        	
        	
        	
        	function ResDef = getResultDefinition(obj)
			%for the monte carlo approach, a definition of changing and not changing result parameters
			
			ResDef = struct(	'TestName',{{'string','ignore'}},...
						'TestVersion',{{'string','ignore'}},...
						'SignVal',{{'scalar','ignore'}},...
						'Passed',{{'scalar','stats'}},...
						'LogLikeModel',{{'array','ignore'}},...
						'LogLikeCatalog',{{'scalar','stats'}},...
						'QScoreL',{{'scalar','stats'}},...
						'QScoreH',{{'scalar','stats'}},...
						'numSimCat',{{'scalar','ignore'}});
			
		end
        	
        	
        	
        			
        	
        	function PossiblePlots = getPlotFunctions(obj)
			%This has to return a list with plot functions with which this test results can be used.
			%The format should be a cell array with Name, function handle, object or keyword and the config 
			%for the plot. 
			%example: PossiblePlots = 	{'Likelihood Histgram', 'LikeHist', PlotConfigStruct;
			%				 'Special K test plot', @() SpecKPlot, PlotConfigStruct;
			%				 'Summary Histogram', Plotter�bj,[]};				 
			%Possible Keywords (no claim of completeness):
			%For Likelihood based
			%	'LikeHist': 	"Classic" plot with synth. catalog likelihood as histogram and the catalog
			%			likelihood as a line
			%	'LikeCDF':	Same as LikeHist but with a CDF instead of a histogram.
			%	'MultiHist':	Same as LikeHist but for more than one test in one plot
			%	'MultiBox':	Same as MultiHist but only a bar showing the bandwidth of likelihood
			%			instead of the histograms
			%	'CombinedLike':	Uses a normalize bar and lines to show the likelihood, also features automatic
			%			markings of passed and not passed test. Can be used with more than one test in
			%			the same plot
			%	
			%For comparison based
			%	'SignMatrix':	Two plots, one with the ranking, the other one with a "matrix" showing the 
			%			significance of two test (e.g. T-test and W-test)
			%	'RatioCDF':	Like a LikeCDF plot, but for likelihood ratios 
			%	'ClassicT':	Typical bar like plot for T-tests and similar ones
			%	'ClassicW':	Typical vertical bar plot for W-tests and similar ones.
			
			%functions behind a function handle should have the following input:
			%MyPlotFunction(plotAxis,TestResult,PlotConfig,UserConfig), with PlotConfig being the config specified 
			%by getPlotFunctions and UserConfig being additional configurations which have to be set by the user
			%For a Plot object, the interface TestPlotObj should be used
			
			%Some fields have to be field in by the TesterObject, because it is unknown on this level
			%I have to do the config later, when I did the plots
			LikePlotConfig = struct('TestName','N-test',...
						'TestSymbols_low',{{'$$\delta_1=$$','$$\delta_1m=$$','$$\delta_1m=$$'}},...
						'TestSymbols_hi',{{'$$\delta_2=$$','$$\delta_2m=$$','$$\delta_2m=$$'}},...
						'ForecastName',[[]],...
						'ForecastTime',[[]],...
						'XaxisLabel','Number of earthquakes   ',...
						'YaxisLabel','Number of occurence   ',...
						'OneSide',false,...
						'NtestLike',true,...
						'SignVal',obj.TestConfig.SignVal,...
						'ScoreField_low','QScoreL',...
						'ScoreField_hi','QScoreH',...
						'LogCatField','LogLikeCatalog',...
						'LogSynthCatField','LogLikeModel',...
						'Uncertianties',false,...
						'MeanAvailable',false,...
						'MedianAvailable',false);
			
			LikePlotConfigCDF=LikePlotConfig;
			LikePlotConfigCDF.XaxisLabel='Joint log-likelihood   ';
			LikePlotConfigCDF.YaxisLabel='Empirical CDF   ';	
			
			MultiPlotConfig = LikePlotConfig;%MISSING
			CombPlotConf = LikePlotConfig;%MISSING
			
			
			PossiblePlots = {	'Likelihood Histogram', 'LikeHist', LikePlotConfig;...
						'Likelihood CDF', 'LikeCDF', LikePlotConfigCDF;...
						'Multiple Histograms', 'MultiHist', MultiPlotConfig;...
						'Multiple Box Plot', 'MultiBox', MultiPlotConfig;...
						'Combined Likelihood', 'CombinedLike', CombPlotConf};
						
			
        	end
        	
        	
        	
        	function PossibleTables = getTablesFunctions(obj)
			%Same as for the plots for tables, which will be written into a text file 
			%(in latex format or unformated text), the output format should be the same
			%as for getPlotFunctions
			%Keywords for the tables:
			%	'LikeTable':	Normal likelihood table for one test and multiple forecasts
			%	'BigTable':	Extended version of LikeTable, allowing to write more than
			%			one test result as well as additional benchmark data into one big
			%			table.
			%	'MatrixTable':	Table typically used for comparison tests of multiple models, it is a
			%			plot with models in row and column.
			%
			%functions behind a function handle should have the following input:
			%MyTableFunction(Filename,TestResult,TableConfig,UserConfig), with TableConfig being the config 
			%specified by getTableFunctions and UserConfig being additional configurations which have to be 
			%set by the user.
			%For a Table object, the interface TestTableObj should be used
			
			
			%I have to do the config later, when I did the tables
			LikeTableConfig = [];
			BigLikeConfig = [];
			
			
			
			PossibleTables = {	'Likelihood Table', 'LikeTable', LikeTableConfig;...
						'Combined Likelihood Table', 'BigTable', BigLikeConfig};
						


        	end
        	
        	
        	%custom functions
        	function CastOK = checkForecast(obj,ForecastType,TheCast)
        		import mapseis.util.emptier;
        		switch ForecastType
				case 'Poisson'
					CastOK = ~(all(isnan(TheCast(:,9)))|isempty(TheCast));
					
				case 'NegBin'
					CastOK = ~(all(isnan(TheCast(:,9)))|all(isnan(TheCast(:,11)))|isempty(TheCast));
					
				case 'Custom'
					Distro=TheCast{2};
					CastOK = ~(all(emptier(Distro)));
			end
        	
        	end
        	
        	
        	function generateSynthCat(obj,TheCast,ForecastType)
        		%generates synthetic catalogs and stores them in the property SimulatedCats.
        		%NOT FINISHED
        		
        		numSim = obj.TestConfig.numSimCat
        		synthCat= {};
        		NumSimCat=[];
        		if ~obj.ParallelMode
				switch ForecastType
					case 'Poisson'
						if numel(TheCast(1,:))<10
							TheCast(:,10)=true(numel(TheCast(:,1),1));
						end
						
						
						%decide which bins are used
						obj.UsedBins=logical(TheCast(:,10));
					
						for i=1:numSim
							TheCat=poissrnd(TheCast(:,9));
							
							%set unused cell to 0
							TheCat(~obj.UsedBins)=0;
							synthCat{i}=TheCat;
							
							NumSimCat(i)=nansum(TheCat);
							
						end
						
						
					case 'NegBin'
						%have to check if the order of the parameter is correct, from matlab help:
						%random('nbin',p,r);
						%r: number of successes	p: probability of success in a single trial
						
						if numel(TheCast(1,:))<10
							TheCast(:,10)=true(numel(TheCast(:,1),1));
						end
						
						%decide which bins are used
						obj.UsedBins=logical(TheCast(:,10));
						
						for i=1:numSim
							TheCat=random('nbin',TheCast(:,9),TheCast(:,11));
							synthCat{i}=TheCat;
							
							NumSimCat(i)=nansum(TheCat);
						end
					
					
					case 'Custom'
						%internal sampling routine of matlab is used, custom implementations (e.g. some
						%rejection sampling method) could be done if needed
						SearchGrid=TheCast{1};
						Distros=TheCast{2};
						
						if numel(SearchGrid(1,:))<10
							SearchGrid(:,10)=true(numel(SearchGrid(:,1),1));
						end
						
						%decide which bins are used
						obj.UsedBins=logical(SearchGrid(:,10));
						
						
						
						for i=1:numSim
							TheCat = cellfun(@(x) randsample(numel(x),1,true,x),Distros)-1;
							synthCat{i}=TheCat;
							
							NumSimCat(i)=nansum(TheCat);
						end
				end
				
				
				
        		else
        			switch ForecastType
					case 'Poisson'
						if numel(TheCast(1,:))<10
							TheCast(:,10)=true(numel(TheCast(:,1),1));
						end
						
						
						%decide which bins are used
						obj.UsedBins=logical(TheCast(:,10));
					
						parfor i=1:numSim
							TheCat=poissrnd(TheCast(:,9));
							
							%set unused cell to 0
							TheCat(~obj.UsedBins)=0;
							synthCat{i}=TheCat;
							
							NumSimCat(i)=nansum(TheCat);
							
						end
						
						
					case 'NegBin'
						%have to check if the order of the parameter is correct, from matlab help:
						%random('nbin',p,r);
						%r: number of successes	p: probability of success in a single trial
						
						if numel(TheCast(1,:))<10
							TheCast(:,10)=true(numel(TheCast(:,1),1));
						end
						
						%decide which bins are used
						obj.UsedBins=logical(TheCast(:,10));
						
						
						parfor i=1:numSim
							TheCat=random('nbin',TheCast(:,9),TheCast(:,11));
							synthCat{i}=TheCat;
							
							NumSimCat(i)=nansum(TheCat);
						end
					
					
					case 'Custom'
						%internal sampling routine of matlab is used, custom implementations (e.g. some
						%rejection sampling method) could be done if needed
						SearchGrid=TheCast{1};
						Distros=TheCast{2};
						
						if numel(SearchGrid(1,:))<10
							SearchGrid(:,10)=true(numel(SearchGrid(:,1),1));
						end
						
						%decide which bins are used
						obj.UsedBins=logical(SearchGrid(:,10));
						
						
						parfor i=1:numSim
							TheCat = cellfun(@(x) randsample(numel(x),1,true,x),Distros)-1;
							synthCat{i}=TheCat;
							
							NumSimCat(i)=nansum(TheCat);
						end
				end
        		
        		
        		
        		
        		end
        		
        	
			obj.SimulatedCats = synthCat;
        		obj.NumSimCat = NumSimCat;
			
        	end
        	
        	
        	function val = pickProbBin(obj,Distro,NumEQ)
        		%short function, needed for picking the probability of a grid node
        		try
        			val = log(Distro(NumEQ+1));
        		catch
        			val = NaN;
        		end
        	end
        	
        	
        	function [selected EqCount] = findNodes(obj,TheCast,ForecastType,ShortCat)
			%finds the grid nodes with earthquakes in them.	
			disp(ForecastType)
			switch ForecastType
				case 'Poisson'
					SearchGrid=TheCast;
				case 'NegBin'
					SearchGrid=TheCast;
				case 'Custom'
					SearchGrid=TheCast{1};
			end
			
			EqCount=zeros(numel(SearchGrid(:,1)),1);
			selected=false(numel(SearchGrid(:,1)),1);
			
			if ~obj.ParallelMode
				
				for i=1:numel(SearchGrid(:,1))
					%no parfor steps are probably to small
					InLon=ShortCat(:,1)>=SearchGrid(i,1)&ShortCat(:,1)<SearchGrid(i,2);
					InLat=ShortCat(:,2)>=SearchGrid(i,3)&ShortCat(:,2)<SearchGrid(i,4);
					InDepth=ShortCat(:,3)>=SearchGrid(i,5)&ShortCat(:,3)<SearchGrid(i,6);
					InMag=ShortCat(:,5)>=SearchGrid(i,7)&ShortCat(:,5)<SearchGrid(i,8);
					
					
					EqCount(i)=sum(InLon&InLat&InDepth&InMag&obj.UsedBins(i));
					selected(i)=true;
					
				end
	
			else
				
				
				parfor i=1:numel(SearchGrid(:,1))
					%no parfor steps are probably to small
					InLon=ShortCat(:,1)>=SearchGrid(i,1)&ShortCat(:,1)<SearchGrid(i,2);
					InLat=ShortCat(:,2)>=SearchGrid(i,3)&ShortCat(:,2)<SearchGrid(i,4);
					InDepth=ShortCat(:,3)>=SearchGrid(i,5)&ShortCat(:,3)<SearchGrid(i,6);
					InMag=ShortCat(:,5)>=SearchGrid(i,7)&ShortCat(:,5)<SearchGrid(i,8);
					
					
					EqCount(i)=sum(InLon&InLat&InDepth&InMag&obj.UsedBins(i));
					selected(i)=true;
					
				end
			
			end
			
		end	
        	
		
	
		
        end

   
end	

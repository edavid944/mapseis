classdef TesterObject < handle
	%This the first version of the forecast project calculator
	
	
	properties
        	Name
        	ID
        	Version
        	MetaData
        	ForecastProject
        	SingleTestConfig
        	Datastore
        	Filterlist
        		        	
        	ModelList
        	TimeList
        	ForecastLength
        	OriginalLength
        	AvailableModTime
        	SelectedModTime
        	
        	TestPlugInList
        	TestNames
        	possiblePlots
        	possibleTables
        	TestResults
        	ResultObject
        	
        	TestConfig
        	
        	WorkList
        	WorkRankArray
        	WorkDone
        	NextCalc
        	
        	CurrentState
        	Status
        	BreakStep
        	NumToBreak
        	BreakTime
        	Relax
        	SaveRes
        	SavePath
        	JumpTime
        	RealTimeTest
        	NormRealTime
        	ParallelMode
        	
        	LoopMode
        	ParentParallelMode
        	
        end
        

    
        methods
        	function obj = TesterObject(Name)
        		%first version of automatic testing suite
        		%will be as "simple" as possible
        		%i.e. now dynamic test list once added, is added, if something
        		%new has to be added after calculation everything should be
        		%recalculated
        		
        		%Only testing and test object building here
        		
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        		
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		obj.Name=Name; %may be not so important here, but who knows?
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.Version='0.9';
        		
        		obj.MetaData=[];
			obj.ForecastProject=[];
			obj.SingleTestConfig=[];
			obj.Datastore=[];
			obj.Filterlist=[];
			
			obj.ModelList={};
			obj.TimeList=[];
			obj.ForecastLength=[];
			obj.OriginalLength=[];
			obj.AvailableModTime=[];
			obj.SelectedModTime=[];
			
			obj.TestPlugInList=[];
			obj.TestNames={};
			obj.possiblePlots=[];
			obj.possibleTables=[];
			obj.TestResults=[];
			obj.ResultObject=[];
			
			obj.WorkList=[];
			obj.WorkRankArray={};
			obj.WorkDone={{},[],{}};
			obj.CurrentState=false(4,1);
        		obj.Status='empty';
        		obj.NextCalc=[];
        		
        		%generate default test suite config
        		defaultConfig = struct(	'SignVal',0.05,...
        					'Uncertianties',false,...
        					'Nr_Monte_Catalogs',1000,...
        					'CalcMedian',true,...
        					'CalcMean',true,...
        					'UseSymmetry',true);
        		%maybe more parameters later
        		
        		obj.TestConfig = defaultConfig;
        		
        		obj.BreakStep=250;
        		obj.NumToBreak=250;
        		obj.BreakTime=10;
        		obj.Relax=true;
        		obj.JumpTime=true;
        		obj.SaveRes=true;
        		obj.SavePath='./Temporary_Files/TestingBackup.mat';
        		
        		%Used only for "realtime projects", RealTimeTest puts the
        		%TesterObject into the (pseudo-) realtime testing mod, and
        		%of NormRealTime is true, the Forecasts will be normalized
        		%to time and only the time intervall the forecasts is valid
        		%will be used.
        		obj.RealTimeTest=false;
        		obj.NormRealTime=false;
        		
        		obj.ParallelMode=false;
        		
        		%if jump time is true, the object will only tests time periods
        		%where there is an earthquake happening
        		
        	
        		obj.LoopMode=true;
        		obj.ParentParallelMode=false;
        		
        		%LoopMode: if set to true, Tester will use a for-loop or 
        		%parfor-loop instead of a recursive function, which allows 
        		%a bit more flexibility. For larger project this is often 
        		%needed because of the piss-poor memory management of matlab
        		%ParentParallelMode: if true the loop will be parallelized and
        		%not the single tests, it will set ParallelMode automatically
        		%false.
        		
        		
        	end
        	
        	
        	function addProject(obj,ProjectObj)
        		%adds a forecast project to the TestSuite
        		%currently only one Project object per TestSuiteObject 
        		%is allowed, but a merger should be built and added.
        		import mapseis.filter.*;
        		
        		%store project
        		obj.ForecastProject=ProjectObj;
        		
        		%copy and update meta data
        		obj.MetaData=obj.ForecastProject.MetaData;
        		obj.MetaData.OriginalCatalog=true;
        		
        		%get the interal catalog and filterlist
        		obj.Datastore=obj.ForecastProject.Datastore;
        		        	
        		%check if realtime
        		obj.RealTimeTest=strcmp(obj.ForecastProject.PredictionIntervall,'realtime');
        		%default for NormRealTime is in the moment false, at least from the object side.
        		
        		%get minimum magnitude and set filter to that value -0.3
        		%Same for the depth but with +10
        		RegConfig=obj.ForecastProject.buildRegionConfig;
        		MinMag=RegConfig.MinMagnitude-0.3;
        		MaxMag=RegConfig.MaxMagnitude+0.3;
        		MinDepth=RegConfig.MinDepth-1;
        		MaxDepth=RegConfig.MaxDepth+10;
        		
        		Range='in';
        			
        		obj.Filterlist=FilterListCloner(obj.ForecastProject.Filterlist);
        		MagFilt=obj.Filterlist.getByName('Magnitude');
        		MagFilt.setRange(Range,[MinMag MaxMag]);
        		DepthFilt=obj.Filterlist.getByName('Depth');
        		DepthFilt.setRange(Range,[MinDepth MaxDepth]);
        		
        		obj.CurrentState(1)=true;
        		        		
        		%generate model and time list:
        		obj.generateModTime;
        		
        		
        		obj.Status='config';
        	end
        	
        	
        	function addAddonData(obj,Datastore,Filterlist)
        		%allows to set a addon catalog and filterlist, which
        		%will be used instead of the catalog and filterlist in
        		%the Project object. Often needed when due error estimation
        		%as this will need a "shaker" catalog.
        		
        		%get the interal catalog and filterlist
        		obj.Datastore=Datastore;
        		obj.Filterlist=Filterlist;
        		
        		if isempty(Datastore)
        			obj.Datastore=obj.ForecastProject.Datastore;
        		end
        		
        		if isempty(Filterlist)
        			obj.Filterlist=obj.ForecastProject.Filterlist;
        		end
        		
        		if ~isempty(Datastore)&~isempty(Filterlist)
        			obj.MetaData.OriginalCatalog=false;
        		else
        			obj.MetaData.OriginalCatalog=true;     			
        			
        		end
        		
        	end
        	
        	
        	function addTestPluging(obj,TestPlugin)
        	        %single plugin or cell array with plugins is possible	
        	        
        	        %IMPORTANT: The plugins are assumed to be configurated.
        	        
        	        if iscell(TestPlugin)
        	        	for i=1:numel(TestPlugin)
        	        		%generate Name
        	        		PlugName=[TestPlugin{i}.TestName,'_',TestPlugin{i}.Version,'_',TestPlugin{i}.Variant];
        	        		
        	        		%store
        	        		obj.TestPlugInList.(PlugName)=TestPlugin{i};
        	        	end
        	        
        	        else
        	        	%generate Name
        	        	PlugName=[TestPlugin.TestName,'_',TestPlugin.Version,'_',TestPlugin.Variant];
        	        	
        	        	%store
        	        	obj.TestPlugInList.(PlugName)=TestPlugin;
        	        
        	        end
 			obj.CurrentState(3)=true;       	        
        	end
        	
        	
        	function generatePlotTableList(obj)
        		%generates a list with all possible plots and tables for
        		%all tests. It is probably a structure with test names as
        		%fields, each containing some cell arrays with name of plot
        		%(or table) and some handle or keyword as well as possible
        		%needed config
        		%The list will later be needed by PlotMaker and TableMaker
        		%as well as a possible PlotViewer.
        		
        		possiblePlots=struct;
        		possibleTables=struct;
        		
        		
        		for i=1:numel(obj.TestNames)
        			%get test
        			Name_of_test=obj.TestNames{i};
        			TheTest=obj.TestPlugInList.(Name_of_test); 
        			
        			%first the plots
        			possiblePlots.(Name_of_test)=TheTest.getPlotFunctions;
        			
        			
        			%second the tables
        			possibleTables.(Name_of_test)=TheTest.getTablesFunctions;
        			
        		end
        		
        		obj.possiblePlots=possiblePlots;
        		obj.possibleTables=possibleTables;
        		
        		
        	end
        	
        	
        	
        	function ConfigureTesting(obj,ConfigStruct)
        		obj.TestConfig=ConfigStruct;	
        	end
        	
        	
        	
        	function ConfigStruct = getConfig(obj)
        		%returns the current configuration or the default
        		%ones if not specified
        		ConfigStruct=obj.TestConfig;
        		
        	end
        	
        	
        	
        	
        	
        	function generateModTime(obj)
        		%builds the list with all possible model and times
        		%used for the selection	
        		import mapseis.filter.*;
        		import mapseis.projector.*;
        		
        		%TEST: realtime mode added but has to be tested
        		
        		%get models
        		[Forecasts,Names,Pos] = obj.ForecastProject.getAllForecasts;
        		
        		%get times
        		TimeSteps = obj.ForecastProject.AvailableTimes;
        		ForecastLength = ones(size(obj.ForecastProject.AvailableTimes))*obj.ForecastProject.ForecastLength;
        		obj.OriginalLength = obj.ForecastProject.ForecastLength; %needed for norm the forecasts
        		
        		if obj.RealTimeTest&obj.NormRealTime
        			ForecastLength = [diff(TimeSteps),1];
        		end
        		
        		%check what is calculated
        		%raw data
        		AvailableTimes=false(numel(TimeSteps),numel(Pos));
        		
        		
        		if obj.JumpTime
        			PostProcessVec=false(size(AvailableTimes));
        			
        			TestFilter=FilterListCloner(obj.Filterlist);
        			TestFilter.changeData(obj.Datastore);
        			
        			minTime=TimeSteps(1);
        			maxTime=TimeSteps(end)+ForecastLength(end);
        			MaxTimeInt=[minTime,maxTime];
        			Range='in';
        			TimeFilt=TestFilter.getByName('Time');
        			TimeFilt.setRange(Range,MaxTimeInt);
        					
				TestFilter.updateNoEvent;
				selected=TestFilter.getSelected;
        			[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
				TestFilter.PackIt;
        			
        			%check with time periods contain any earthquakes 
        			for i=1:numel(TimeSteps)
        				%apply filter and catalog
        				
        				CurTime=TimeSteps(i);
        				TimeInt=[CurTime,CurTime+ForecastLength(i)];
        				selector=ShortCat(:,4)>=TimeInt(1)&ShortCat(:,4)<TimeInt(2);        			
        				      				
					
					if any(selector)
						PostProcessVec(i,:)=true;
						disp(i)
					end
					
        			end
        			
        			
        			
        		else
        			PostProcessVec=true(size(AvailableTimes));
        		end
        		
        		%have to check if it works like this
        		ForecastType=cell(size(Forecasts));
        		for i=1:numel(Forecasts)
        			
        			ForecastType{i}=Forecasts{i}.ForecastType.Type;
        			
        			[C,ia,ib] = intersect(TimeSteps,Forecasts{i}.TimeVector);
        			
        			if ~isempty(C)
        				%AvailableTimes(i,ia)=true;
        				AvailableTimes(ia,i)=true;
        			end
        		
        		end
        		
        		obj.ModelList={Names{:};Forecasts{:};ForecastType{:}}';
			obj.TimeList=TimeSteps;
			obj.ForecastLength=ForecastLength;
			disp(size(AvailableTimes))
			disp(size(PostProcessVec))
			
			obj.AvailableModTime=AvailableTimes&PostProcessVec;
        		
        		
        	end
        	
        	
        	function [ModelNames Times AvailableTimes] = getModTimeList(obj)
        		%returns the model and time list
        		ModelNames=obj.ModelList(:,1);
        		Times=obj.TimeList;
        		AvailableTimes=obj.AvailableModTime;
        		
        	end
        	
        	
        	
        	function setTestSelection(obj,WhichTest,ModTimeSelection)
        		%used to select the parts which should be tested
        		%instead of a logical matrix determine which parts
        		%should be calculated, the keyword 'all' could be used
        		%test everything
        		
        		if ~obj.CurrentState(3)
        			disp('no test added yet')
        			return;
        		end
        		
        		if isstr(ModTimeSelection)
        			
        			%only one case at the moment, but I have the 
        			%feeling there will be more 
        			switch ModTimeSelection
        				case 'all'
        					ModTimeSelection=obj.AvailableModTime;
        				case 'none'
        					ModTimeSelection=false(size(obj.AvailableModTime));
        			end	
        		
        		
        		
        		else
        			if ~all(size(obj.SelectedModTime)==size(ModTimeSelection))
        				disp('Dimension of the input matrix not correct');
        				return
        			end
        			
        			
        			ModTimeSelection=logical(ModTimeSelection);
        		
        		end
        		
        		
        		regTestName=fieldnames(obj.TestPlugInList);
        		if isstr(WhichTest)
				if strcmp(WhichTest,'all')
					for i=1:numel(regTestName)
						obj.SelectedModTime.(regTestName{i})=ModTimeSelection;
					end
					
				else
					obj.SelectedModTime.(WhichTest)=ModTimeSelection;
				end
        		
        		else
        			for i=1:numel(WhichTest)
					obj.SelectedModTime.(WhichTest{i})=ModTimeSelection;
				end
        		
        		end
        		
        		
        		obj.CurrentState(2)=true;
        		
        	end
        	
        	
        	
        	
        	function generateWorkList(obj)
        		%builds the worklist with the parts which have to be done
        		%can only be used after selection has been done and plugins
        		%are added
        		
        		if ~all(obj.CurrentState(1:3))
        			disp('Test Suite not fully configurated to generate Worklist')
        			return;
        		end
        		
        		
        		%work it test-by-test
        		TestNames=fieldnames(obj.TestPlugInList);
        		obj.TestNames=TestNames; %to ensure that  it will always be the same
        		
        		
        		AllModelID=1:numel(obj.ModelList(:,1));
        		
        		WorkList=[];
        		WorkRankArray={};
        		
        		%what are the forecasts
        		isPoisson = strcmp(obj.ModelList(:,3),'Poisson');
        		isNegBin = strcmp(obj.ModelList(:,3),'NegBin');
        		isCustom = strcmp(obj.ModelList(:,3),'Custom');
        		
        		DistroNr = zeros(size(isCustom));
        		DistroNr(isPoisson) = 1;
        		DistroNr(isNegBin) = 2;
        		DistroNr(isCustom) = 3;
        		
        		for i=1:numel(TestNames)
        			CurrentSelList = obj.SelectedModTime.(TestNames{i}) & obj.AvailableModTime;
        			
        			if any(CurrentSelList)
        				CurTest=obj.TestPlugInList.(TestNames{i});
        				CurFeatures=CurTest.getFeatures;
        				isSingleTest=strcmp(CurFeatures.TestType,'single');
        				
        				%have to check criterium
        				checkitout=(~(CurFeatures.supportPoisson&CurFeatures.supportNegBin&CurFeatures.supportCustom)&isSingleTest)|...
        					(~(CurFeatures.supportPoisson&CurFeatures.supportNegBin&CurFeatures.supportCustom&CurFeatures.MixedDistro)&~isSingleTest);
        				
        				
        				
        				
        				switch CurFeatures.TestType
        					case 'single'
        						%check which forecasts are compatible with test
							if checkitout
								if ~CurFeatures.supportPoisson	
									if any(isPoisson)
										inCompIDX=find(isPoisson);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if ~CurFeatures.supportNegBin	
									if any(isNegBin)
										inCompIDX=find(isNegBin);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if ~CurFeatures.supportCustom	
									if any(isCustom)
										inCompIDX=find(isCustom);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
							end
        					
							if any(CurrentSelList)
								[TimeID ModelID] = find(CurrentSelList);
								%[ModelID TimeID] = find(CurrentSelList);
								
								for j=1:numel(TimeID)
									WorkList(end+1,:)=[i,TimeID(j),ModelID(j),-1];
								end
							
							end
							
							
							
							
        					
        					case 'comparison'
        						
        						%check which forecasts are compatible with test
							if checkitout
								%just a problem with some distros
								%kick out not supported Distros
								if ~CurFeatures.supportPoisson	
									if any(isPoisson)
										inCompIDX=find(isPoisson);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if ~CurFeatures.supportNegBin	
									if any(isNegBin)
										inCompIDX=find(isNegBin);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if ~CurFeatures.supportCustom	
									if any(isCustom)
										inCompIDX=find(isCustom);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if any(CurrentSelList)
									[TimeID ModelID] = find(CurrentSelList);
								
									
									if CurFeatures.MixedDistro
										%Mixed distros are supported
										
										if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
											%use the symmetry
											for j=1:numel(TimeID)
												NotMySelf=AllModelID(ModelID(j)~=AllModelID);
												ExistAsWell=CurrentSelList(TimeID,NotMySelf);
												
												
												if any(ExistAsWell)
													NotMySelf=NotMySelf(ExistAsWell(j,:));
													
													for k=NotMySelf
														alreadyExist=any(WorkList(:,1)==i&WorkList(:,2)==TimeID(j)&...
																WorkList(:,3)==AllModelID(k)&WorkList(:,4)==ModelID(j));
														if ~alreadyExist
															WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
														end
													end	
												end
											end
									
										else
											%no symmetry wished or supported
											for j=1:numel(TimeID)
												NotMySelf=AllModelID(ModelID(j)~=AllModelID);
												ExistAsWell=CurrentSelList(TimeID,NotMySelf);
												
												if any(ExistAsWell)
													NotMySelf=NotMySelf(ExistAsWell(j,:));
												
													for k=NotMySelf
														
														WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
													end	
												end	
											end
										end
									
									else
										%does not like mixed distros
										if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
											for j=1:numel(TimeID)
											
												NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
												ExistAsWell=CurrentSelList(TimeID,NotMySelf);
												
												if any(ExistAsWell)
													NotMySelf=NotMySelf(ExistAsWell(j,:));
												
													for k=NotMySelf
														alreadyExist=any(WorkList(:,1)==i&WorkList(:,2)==TimeID(j)&...
																WorkList(:,3)==AllModelID(k)&WorkList(:,4)==ModelID(j));
														if ~alreadyExist
															WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
														end
													end	
													
												end
												
											end
									
										else
											for j=1:numel(TimeID)
												NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
												ExistAsWell=CurrentSelList(TimeID,NotMySelf);
												
												if any(ExistAsWell)
													NotMySelf=NotMySelf(ExistAsWell(j,:));
														
													for k=NotMySelf
														
														WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
													end	
												
												end
											end
										end
									
									
									
									end
										
								
								end
								
								
							
							
							else
								%no problem, everything goes
								[TimeID ModelID] = find(CurrentSelList);
								
								if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
									%use symmetry of the test
									for j=1:numel(TimeID)
										NotMySelf=AllModelID(ModelID(j)~=AllModelID);
										ExistAsWell=CurrentSelList(TimeID,NotMySelf);
										
																				if any(ExistAsWell)
											NotMySelf=NotMySelf(ExistAsWell(j,:));
											
											for k=NotMySelf
												if ~isempty(WorkList)
													alreadyExist=any(WorkList(:,1)==i&WorkList(:,2)==TimeID(j)&...
														WorkList(:,3)==AllModelID(k)&WorkList(:,4)==ModelID(j));
												else
													alreadyExist=false;
												end	
												if ~alreadyExist
													WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
												end
											end	
										end	
											
									end
							
								else
									%symmtry not wanted or allowed
									
									for j=1:numel(TimeID)
										NotMySelf=AllModelID(ModelID(j)~=AllModelID);
										ExistAsWell=CurrentSelList(TimeID,NotMySelf);
										
										if any(ExistAsWell)
											NotMySelf=NotMySelf(ExistAsWell);
											for k=NotMySelf
												
												WorkList(end+1,:)=[i,TimeID(j),ModelID(j),AllModelID(k)];
											end	
										end	
									end
								end
							
							end
        					
							
        					case 'ranking'
        						%check which forecasts are compatible with test
							if checkitout
								if ~CurFeatures.supportPoisson	
									if any(isPoisson)
										inCompIDX=find(isPoisson);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if ~CurFeatures.supportNegBin	
									if any(isNegBin)
										inCompIDX=find(isNegBin);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
								
								if ~CurFeatures.supportCustom	
									if any(isCustom)
										inCompIDX=find(isCustom);
										CurrentSelList(:,inCompIDX)=false;
									end
								end
							end
							
							
							
							if any(CurrentSelList)
								[TimeID ModelID] = find(CurrentSelList);
								
								
								if CurFeatures.MixedDistro
									%mixing of distribution is allowed
									
									if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
										for j=1:numel(TimeID)
											potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
											
											NotMySelf=AllModelID(ModelID(j)~=AllModelID);
											ExistAsWell=CurrentSelList(TimeID,NotMySelf);
											
											if any(ExistAsWell)
												NotMySelf=NotMySelf(ExistAsWell);
												
												alreadyExist=false;
												CurList=[ModelID,NotMySelf];
												
												for k=potIdent
												
													SomethingDiff=setdiff(WorkRankArray(k),CurList);
													
													if ~isempty(SomethingDiff)
														alreadyExist=true;
														break;
													end
												end
												
												if ~alreadyExist
													WorkRankArray{end+1,1}=CurList;
													WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
												end
												
											end
											
											
										end
								
									else
										for j=1:numel(TimeID)
											potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
											
											NotMySelf=AllModelID(ModelID(j)~=AllModelID);
											ExistAsWell=CurrentSelList(TimeID,NotMySelf);
											
											
											if any(ExistAsWell)
												NotMySelf=NotMySelf(ExistAsWell);
												
												alreadyExist=false;
												CurList=[ModelID,NotMySelf];
												
												for k=potIdent
												
													WorkRankArray{end+1,1}=CurList;
													WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
													
												end	
											end	
										end
									end
								
									
								else
									%no mixed testing possible
									if obj.TestConfig.UseSymmetry&CurFeatures.Symmetric
										for j=1:numel(TimeID)
											potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
											
											NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
											ExistAsWell=CurrentSelList(TimeID,NotMySelf);
											
											if any(ExistAsWell)
												NotMySelf=NotMySelf(ExistAsWell);
												
												alreadyExist=false;
												CurList=[ModelID,NotMySelf];
												
												for k=potIdent
												
													SomethingDiff=setdiff(WorkRankArray(k),CurList);
													
													if ~isempty(SomethingDiff)
														alreadyExist=true;
														break;
													end
												end
												
												if ~alreadyExist
													WorkRankArray{end+1,1}=CurList;
													WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
												end
												
											end
											
											
										end
								
									else
										for j=1:numel(TimeID)
											potIdent=find(WorkList(:,1)==i&WorkList(:,2)==TimeID(j));	
											
											NotMySelf=AllModelID((ModelID(j)~=AllModelID)&(DistroNr(j)==DistroNr));
											ExistAsWell=CurrentSelList(TimeID,NotMySelf);
											
											
											if any(ExistAsWell)
												NotMySelf=NotMySelf(ExistAsWell);
												
												alreadyExist=false;
												CurList=[ModelID,NotMySelf];
												
												for k=potIdent
												
													WorkRankArray{end+1,1}=CurList;
													WorkList(end+1,:)=[i,TimeID(j),-1,numel(WorkRankArray)];
													
												end	
											end	
										end
									end
									
									
									
								
								end
								
								
							
							
        					
							end
							
        				
        				end
        				
        					
        				
        				
        			
        			
        			end
        			
        		
        		end
        		
        		obj.WorkRankArray=WorkRankArray;
        		obj.WorkList=WorkList;
        		obj.NextCalc=1;
        		
        		obj.CurrentState(4)=true;
        		obj.Status='ready';
        	end
        	
        	
        	function initResultStruct(obj)
        		%builds and empty Result structur for storing the results
        		
        		if ~all(obj.CurrentState)
        			disp('Test Suite not fully configurated to calculate')
        			return;
        		end
        		
        		
        		TestResults={};
        		Times=obj.TimeList;
        		
        		for i=1:numel(Times)
        			ResultStruct=struct(	'ForecastTime',Times(i),...
        						'ForecastLength',obj.ForecastLength(i),...
        						'Empty',true);
        			TestResults{i}=ResultStruct;			
        						
        		end
        		
        		obj.TestResults=TestResults;
        		
        		
        	
        	end
        	
        	
        	function startCalc(obj)
        		if ~all(obj.CurrentState)
        			disp('Test Suite not fully configurated to calculate')
        			return;
        		end
        		
        		
        		%to keep compabiltiy with old projects
        		if isempty(obj.ParentParallelMode)
        			obj.ParentParallelMode=false;
        		end
        		
        		if isempty(obj.LoopMode)
        			obj.LoopMode=false;
        		end	
        		
        		
        	
        		if ~obj.LoopMode
				%set the recursion limit to a high enough number (matlab will stop
				%the calculation later if this is not done
				NrNeeded=numel(obj.WorkList(:,1))+500; 
				set(0,'RecursionLimit',NrNeeded);
        		end
        		
        		%parallel mode is probably best in the case of monte carlo processing
        		%but it can as well speed up the other calculations
        		if obj.ParallelMode | obj.ParentParallelMode
        	
        			try
        				matlabpool open
        			end	
        		end
        		   
        		%built empty Data structur
        		obj.initResultStruct;
        		
        		
        		obj.Status='started';
        		
        		if ~obj.LoopMode
				%start calculation
				CurNr=obj.NextCalc;
				obj.NextCalc=obj.NextCalc+1;
				obj.contCalc(CurNr);
        	
        		else
        			if ~obj.ParentParallelMode
        				obj.LoopItSerial;
        			else
        				obj.ParallelMode=false;
        				obj.LoopItParallel;
        			end
        		
        		end
        		
        	end
        	
        	
        	function restartCalc(obj)
        		%restarts from the last calcstep
        		
        		if isempty(obj.WorkList)
        			error('No calculation has been started before - use startCalc');
        		end	
        		
        		%to keep compabiltiy with old projects
        		if isempty(obj.ParentParallelMode)
        			obj.ParentParallelMode=false;
        		end
        		
        		if isempty(obj.LoopMode)
        			obj.LoopMode=false;
        		end	
        		
        		if ~obj.LoopMode
				%set the recursion limit to a high enough number (matlab will stop
				%the calculation later if this is not done
				NrNeeded=numel(obj.WorkList(:,1))+500; 
				set(0,'RecursionLimit',NrNeeded);
        		end
        		
        		%parallel mode is probably best in the case of monte carlo processing
        		%but it can as well speed up the other calculations
        		if obj.ParallelMode | obj.ParentParallelMode
        			try
        				matlabpool open
        			end	
        		end
        		
        		if ~obj.LoopMode
        			obj.contCalc(obj.NextCalc-1);
        		else
        			obj.NextCalc=obj.NextCalc-1;
        		
        			if ~obj.ParentParallelMode
        				obj.LoopItSerial;
        			else
        				obj.ParallelMode=false;
        				obj.LoopItParallel;
        			end
        		end
        			
        	end
        	
        	
        	function contCalc(obj,ListNr)
        		
        		import mapseis.filter.*;	
        		import mapseis.projector.*;
        		
        		%TEST: realtime mode is added but has to be tested, it is possible 
        		%that some parts could be missing. 
        		
        		
        		%get current forecast(s),timestep and test ID
        		TestID = obj.WorkList(ListNr,1);
        		TimeID = obj.WorkList(ListNr,2);
        		ForeAID = obj.WorkList(ListNr,3);
        		ForeBID = obj.WorkList(ListNr,4);
        		
        		%Use monte-carlo or not?
        		Monti=obj.TestConfig.Uncertianties;
        		
        		%prepare test filter
        		tenth=1/(24*36000); %0.1 second
        		CurTime=obj.TimeList(TimeID);
        		CurLength=obj.ForecastLength(TimeID);
        		TimeInt=[CurTime+tenth,CurTime+CurLength];
        		Range='in';
        		%added a factor tenth to prevent possible double count of
        		%eartquakes
        		
        		TestFilter=FilterListCloner(obj.Filterlist);
        		TimeFilt=TestFilter.getByName('Time');
        		TimeFilt.setRange(Range,TimeInt);

        		%get test
        		Name_of_test=obj.TestNames{TestID};
        		
        		disp(Name_of_test)
        		disp(CurTime)
        		
        		TheTest = obj.TestPlugInList.(Name_of_test); 
        		
        		%set to parallel if needed
        		TheTest.ParallelMode=obj.ParallelMode;
        		
        		
        		%determine input format (future release)
        		%CurFeatures=CurTest.getFeatures;
        		%wholeObjectSend=strcmp(CurFeatures.InputFormat,'Fobj');
        		
        		EverythingFine=true;
        		%get forecast(s)
        		if ForeBID == -1
        			%single test
        			disp(ForeAID)
        			ForecastA=obj.ModelList{ForeAID,2};
        			NameA=obj.ModelList{ForeAID,1};
        			TypeA=obj.ModelList{ForeAID,3};
        			disp(ForecastA)
        			try
        				[TheCastA ForeTime ForeLength]= ForecastA.getCSEPForecast(CurTime);
               			catch
        				TheCastA=NaN;
        				EverythingFine=false;
        			end
        			
        			%normalize if needed
        			if  EverythingFine&obj.RealTimeTest&obj.NormRealTime&(CurLength~=1) 	
        				TheCastA = obj.NormForecast(TheCastA,CurLength,TypeA);
        			end
        			
        			TheCasts={TheCastA,TypeA,NameA};
        			
        		
        		elseif ForeAID == -1
        			%ranking test
        			TheList=obj.WorkRankArray{ForeBID};
        			
        			Forecasts={};
        			TheCasts={};
        			for i=1:numel(TheList)
        				Forecasts(i)=obj.ModelList(i,2);
        				NameA=obj.ModelList{i,1};
        				TypeA=obj.ModelList{i,3};
        				try
        					[WantIt ForeTime ForeLength]= Forecasts{i}.getCSEPForecast(CurTime);
        				catch
        					WantIt=NaN;
        					EverythingFine=false;
        				end
        				
        				if  EverythingFine&obj.RealTimeTest&obj.NormRealTime&(CurLength~=1) 
        					WantIt = obj.NormForecast(WantIt,CurLength,TypeA);
        				end
        			
        				
        				TheCasts(i,:)={WantIt,TypeA,NameA};
        			end
        			
        		else
        			%comparison test
        			ForecastA=obj.ModelList{ForeAID,2};
        			NameA=obj.ModelList{ForeAID,1};
        			TypeA=obj.ModelList{ForeAID,3};
        			
        			ForecastB=obj.ModelList{ForeBID,2};
        			NameB=obj.ModelList{ForeBID,1};
        			TypeB=obj.ModelList{ForeBID,3};
        			try
        				[TheCastA ForeTime ForeLength]= ForecastA.getCSEPForecast(CurTime);
        				[TheCastB ForeTime ForeLength]= ForecastB.getCSEPForecast(CurTime);
        			catch
        				TheCastA=NaN;
        				TheCastB=NaN;
        				EverythingFine=false;
        				
        			end
        			
        			if  EverythingFine&obj.RealTimeTest&obj.NormRealTime&(CurLength~=1) 	
        				TheCastA = obj.NormForecast(TheCastA,CurLength,TypeA);
        				TheCastB = obj.NormForecast(TheCastB,CurLength,TypeB);
        			end
        			
        			TheCasts={TheCastA,TypeA,NameA;
        				  TheCastB,TypeB,NameB};
        		end
        		
        		%calculate test score
        		if EverythingFine
				if Monti
					%Uncertianties estimation
					
					%first result with original catal
					try
						obj.Datastore.ResetShaker;
					catch
						error('Catalog could not be modified, probably not a shaker-type catalog');
					end
					
					
					%apply filter and catalog
					TestFilter.changeData(obj.Datastore);
					TestFilter.updateNoEvent;
					selected=obj.Filterlist.getSelected;
					TestFilter.PackIt;
					
					if any(selected)
						[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
					else
						disp('no earthquake')
						ShortCat=[];	
					end	
						
					%calculate
					TheTest.resetMonteCarlo;
					ErrorCode = TheTest.calcTest(TheCasts,ShortCat)
					OrgTestResults = TheTest.getResults;
										
					%get result definition
					ResDef = TheTest.getResultDefinition;
						
					if isempty(ResDef)
						ResDef = obj.buildDefaultResDef(OrgTestResults);
					end	
						
					%build Result template
					[TestResult corResDef] = obj.buildMonteResult(OrgTestResults,ResDef);
						
		
					for i=1:obj.TestConfig.Nr_Monte_Catalogs
							
						obj.Datastore.ShakeCatalog;
						
						%apply filter and catalog
						TestFilter.changeData(obj.Datastore);
						TestFilter.updateNoEvent;
						selected=TestFilter.getSelected;
						TestFilter.PackIt;
							
						[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
							
						%calculate
						ErrorCode = TheTest.calcTest(TheCasts,ShortCat)
						CurTestResults = TheTest.getResults;			
						
						TestResult = obj.addMonteResult(i,TestResult,CurTestResults,corResDef);
							
					end
						
						
					%do the needed statistics (outsourced again
					TestResult = obj.MonteResultStats(TestResult,corResDef);
						
						
						
					%add test data and forecast date and name of forecast to the result
					TestResult.ForecastName=TheCasts(:,3);
					TestResult.LastTested=now;
					TestResult.ForecastTime=CurTime;
					TestResult.ForecastLength=CurLength;
						
					if iscell(TestResult.ForecastName)&numel(TestResult.ForecastName)>1
						Write4Name=TestResult.ForecastName{1};
						
						for i=2:numel(TestResult.ForecastName)
							Write4Name=[Write4Name,'_',TestResult.ForecastName{i}];
						end
							
					elseif iscell(TestResult.ForecastName)
						Write4Name=TestResult.ForecastName{1};
							
					else
						Write4Name=TestResult.ForecastName;
						
					end
						
					%Store Result
					obj.TestResults{TimeID}.Empty = false;
					obj.TestResults{TimeID}.(Name_of_test).(Write4Name) = TestResult;
					
					
					
					
					
				else
					%no uncertianties
					
					%apply filter and catalog
					TestFilter.changeData(obj.Datastore);
					TestFilter.updateNoEvent;
					selected=TestFilter.getSelected;
					TestFilter.PackIt;
					if any(selected)
						[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
						disp(numel(ShortCat(:,1)));
						disp(min(ShortCat(:,5)));
						disp(max(ShortCat(:,5)));
						%calculate
						TheTest.resetMonteCarlo;
						ErrorCode = TheTest.calcTest(TheCasts,ShortCat)
						TestResult = TheTest.getResults;
					else
						disp('no earthquake')
						TheTest.resetMonteCarlo;
						ErrorCode = TheTest.calcTest(TheCasts,[])
						TestResult = TheTest.getResults;
					end	
						
					%add test data and forecast date and name of forecast to the result
					TestResult.ForecastName=TheCasts(:,3);
					TestResult.LastTested=now;
					TestResult.ForecastTime=CurTime;
					TestResult.ForecastLength=CurLength;
						
					if iscell(TestResult.ForecastName)&numel(TestResult.ForecastName)>1
						Write4Name=TestResult.ForecastName{1};
					
						for i=2:numel(TestResult.ForecastName)
							Write4Name=[Write4Name,'_',TestResult.ForecastName{i}];
						end
							
					elseif iscell(TestResult.ForecastName)
						Write4Name=TestResult.ForecastName{1};
							
					else
						Write4Name=TestResult.ForecastName;
						
					end
						
					%Store Result
					obj.TestResults{TimeID}.Empty = false;
					obj.TestResults{TimeID}.(Name_of_test).(Write4Name) = TestResult;
					
					
				end
				
				
				%mark as work done (generate a short entry what has done)
				doneTestList=obj.WorkDone{1};
				doneTimeList=obj.WorkDone{2};
				doneCastList=obj.WorkDone{3};
				
				doneTestList{ListNr}=Name_of_test;
				doneTimeList(ListNr)=CurTime;
				doneCastList{ListNr}=TheCasts(:,3);
				
				obj.WorkDone={doneTestList,doneTimeList,doneCastList}; %CHECK IF CORRECT
				
				disp([num2str(obj.NextCalc), ' of ', num2str(numel(obj.WorkList(:,1)))]);
        		
			else
			
        			disp('Forecast was not existing');
				disp([num2str(obj.NextCalc), ' of ', num2str(numel(obj.WorkList(:,1)))]);
        		end
        		
        		if ~obj.LoopMode
        		
				%Go for the next calculation or finish
				if ListNr<numel(obj.WorkList(:,1))
					if obj.Relax
						if obj.BreakStep<=0;
							CurNr=obj.NextCalc;
							obj.NextCalc=obj.NextCalc+1;
							obj.makeAbreak(CurNr);
						else
							obj.BreakStep=obj.BreakStep-1;
							CurNr=obj.NextCalc;
							obj.NextCalc=obj.NextCalc+1;
							obj.contCalc(CurNr);
						end
					
					else
						CurNr=obj.NextCalc;
						obj.NextCalc=obj.NextCalc+1;
						obj.contCalc(CurNr);
					end
				else
					%The end my friend
					obj.finishCalc;
				
				end
        		
			else
        			%it is looped do nothing for now
        			
			
			end
        		
        	end
        	
        	
        	
        	function LoopItSerial(obj)
        		
        		TheEnd=numel(obj.WorkList(:,1));
        		
        		if obj.NextCalc<=TheEnd
        			for kk=obj.NextCalc:TheEnd
        				obj.contCalc(kk);
        				obj.NextCalc=obj.NextCalc+1;
        				
        				%save if needed
        				if obj.Relax
						if obj.BreakStep<=0;
							if obj.SaveRes
								TestingBackup=struct(	'TestConfig',obj.TestConfig,...
											'TestResults',{obj.TestResults},...
											'SingleTestConfig',[obj.SingleTestConfig],...
											'WorkList',[obj.WorkList],...
											'WorkRankArray',{obj.WorkRankArray},...
											'WorkDone',{obj.WorkDone},...
											'NextCalc',obj.NextCalc);
								%just the most needed functions			
											
								save(obj.SavePath,'TestingBackup');
								
								obj.BreakStep=obj.NumToBreak;
								disp('refreshed');
							end
						else
							obj.BreakStep=obj.BreakStep-1;
						end
					end
        			end
        			
        			%finish it
        			obj.finishCalc;
        			
        			
        		
        		end
        	
        	
        	end
        	
        	function LoopItParallel(obj)
        		%similar to LoopItSerial
        		%but this may change
        	
        		%currently not working, probably needs to be external ...
        		%... bloody matlab
        		obj.LoopItSerial;
        		
        		
        		%  TheEnd=numel(obj.WorkList(:,1));
        		%  
        		%  if obj.NextCalc<TheEnd
        			%  parfor kk=obj.NextCalc:TheEnd
        				%  obj.contCalc(kk);
        				%  obj.NextCalc=obj.NextCalc+1;
        				%  
        				%  %save if needed
        				%  if obj.Relax
						%  if obj.BreakStep<=0;
							%  if obj.SaveRes
								%  TestingBackup=struct(	'TestConfig',obj.TestConfig,...
											%  'TestResults',{obj.TestResults},...
											%  'SingleTestConfig',[obj.SingleTestConfig],...
											%  'WorkList',[obj.WorkList],...
											%  'WorkRankArray',{obj.WorkRankArray},...
											%  'WorkDone',{obj.WorkDone},...
											%  'NextCalc',obj.NextCalc);
								%  %just the most needed functions			
											%  
								%  save(obj.SavePath,'TestingBackup');
								%  
								%  obj.BreakStep=obj.NumToBreak;
								%  disp('refreshed');
							%  end
						%  else
							%  obj.BreakStep=obj.BreakStep-1;
						%  end
					%  end
        			%  end
        			%  
        			%  %finish it
        			%  obj.finishCalc;
        			%  
        			%  
        		%  
        		%  end
        	%  
        	
        	end
        	
        	
        	function makeAbreak(obj,ListNr)
        		%just an experiment, done to kept memory in order.
        		
        		if obj.SaveRes
        			TestingBackup=struct(	'TestConfig',obj.TestConfig,...
        						'TestResults',{obj.TestResults},...
        						'SingleTestConfig',[obj.SingleTestConfig],...
        						'WorkList',[obj.WorkList],...
        						'WorkRankArray',{obj.WorkRankArray},...
        						'WorkDone',{obj.WorkDone},...
        						'NextCalc',obj.NextCalc);
        			%just the most needed functions			
        						
        			save(obj.SavePath,'TestingBackup')
        		end
        	       	disp('Tiny coffee break...')	
        		pause(obj.BreakTime);
        		obj.BreakStep=obj.NumToBreak;
        		disp('refreshed');
        		
        		
        		
        		
        		obj.contCalc(ListNr);
        		
        		
        		
        	end
        	
        	function restoreBackup(obj)
        		%restores previous saved results. The Forecasts have to be
        		%added seperately, before calling this function 
        		
        		try
        			load(obj.SavePath);
        			
        			obj.TestConfig=TestingBackup.TestConfig;
        			obj.TestResults=TestingBackup.TestResults;
        			obj.SingleTestConfig=TestingBackup.SingleTestConfig;
        			obj.WorkList=TestingBackup.WorkList;
        			obj.WorkRankArray=TestingBackup.WorkRankArray;
        			obj.WorkDone=TestingBackup.WorkDone;
        			obj.NextCalc=TestingBackup.NextCalc;
        			
        		catch
        			disp('no save file exists');
        		end
        			
        	end
        	
        	
        	function finishCalc(obj)
        		%finish the calculation
        		
        		if obj.TestConfig.Uncertianties
        			%reset catalog if uncertianties were used.
        			try
        				obj.Datastore.ResetShaker;
        			catch
        				error('Catalog could not be modified, probably not a shaker-type catalog');
        			end
        		end
        		
        		
        	
        		obj.Status='finished';
        		
        		
        		
        	
        	end
        	
        	
        	
        	function buildTestResultObj(obj,FullData)
        		%builds the Test result object, this object is more like
        		%a structure than an actual object, but it may change later
        		
        		import mapseis.forecast_testing.*
        		
        		
        		if nargin<2
        			FullData=true;
        		end
        		
        		if strcmp(obj.Status,'empty')
        			disp('no Project - no Results, that simple');
        			return;
        		end
        		
        		if strcmp(obj.Status,'ready')
        			disp('no Results yet, calculate first');
        			return;
        		end
        		
        		if strcmp(obj.Status,'started')
        			disp('Not all tests have been finished, produce test results object anyway');
        			%return;
        		end
        		
        		
        		if isempty(obj.possiblePlots)|isempty(obj.possibleTables)
        			disp('build plot and table list');
        			obj.generatePlotTableList;
        		end
        		
        		
        		
        		%create TestInfo
        		TestInfo={};
        		for i=1:numel(obj.TestNames)
        			%get test
        			Name_of_test=obj.TestNames{i};
        			TheTest=obj.TestPlugInList.(Name_of_test); 
        			CurFeatures=TheTest.getFeatures;
        			TestType=CurFeatures.TestType;
        			SingleTestConfig=TheTest.getTestConfig;
        			
        			TestInfo(i,:)={Name_of_test,TestType,SingleTestConfig};
        			
        		end
        		
        		
        		%Create a new Test Result object
        		NewResults = TestResultObj([obj.ForecastProject.Name,'_TestResults']);
        		
        		
        		
        		%add all data to the object
        		NewResults.MetaData = obj.MetaData;
        		
        		if FullData
        			%get forecasts
        			[Forecasts,Names,Pos] = obj.ForecastProject.getAllForecasts;
        			NewResults.Forecasts = {Forecasts,Names};
        			NewResults.FullData=true;
        		end
        		
        			
        		NewResults.ProjectID = obj.ForecastProject.ID;
        		NewResults.Datastore = obj.Datastore;
        		NewResults.Filterlist = obj.Filterlist;
        	
        		NewResults.TestConfig = obj.TestConfig
        		NewResults.TestInfo = TestInfo;
        		NewResults.CalcedResults = obj.WorkDone;
        		NewResults.Results = obj.TestResults;
        	
        		NewResults.possibleTables = obj.possibleTables;
        		NewResults.possiblePlots = obj.possiblePlots;
        		
        		%the last part of CalcedResults is double packed, instead of 
        		%reworking the whole code just change it here
        		faultyCell=NewResults.CalcedResults{3};
        		NewResults.CalcedResults{3}=cellfun(@(x) x{:},faultyCell,'UniformOutput',false);
        		
        		
        		obj.ResultObject=NewResults;
        		
        		
        		
        		
        	end
        	
        	
        	
        	
        	
        	function TestObj=getTestObject(obj)
        		
        		if ~isempty(obj.ResultObject)
        			TestObj=obj.ResultObject;
        		
        		else
        			disp('try to create the Result object')
        			try
        				obj.buildTestResults;
        			catch
        				error('Result object does not exist and cannot be created')
        			end
        			
        			TestObj=obj.ResultObject;
        		
        			
        		end
        	
        	end
        	
        	
        	function ResDef = buildDefaultResDef(obj,resultDemo)
        		%outsourced from contCalc, to make it more readable.
        		%no result definition -> has to be built	
        		ToIgnore={'TestName','TestVersion','SignVal'};
        		ToCollect={'Passed'};
        			
        		TheFields=fieldnames(resultDemo);
        		ResDef=struct;
        		for i=1:numel(TheFields)
        			if any(strcmp(ToIgnore,TheFields{i}))
        				ResDef.(TheField{i})={'string','ignore'};
        					
        			elseif any(strcmp(ToCollect,TheFields{i}))
        				ResDef.(TheField{i})={'scalar','collect'};
        			
        			else
        				if iscell(resultDemo.(TheField{i}))
        					ResDef.(TheField{i})={'cell','collect'};
        						
        				elseif isstr(resultDemo.(TheField{i}))
        					ResDef.(TheField{i})={'string','collect'};
        					
        				else
        						
        						
        					if any(size(resultDemo.(TheField{i}))>1)
        						ResDef.(TheField{i})={'array','collect'};
        					else
        						ResDef.(TheField{i})={'scalar','stats'};
        					end
        				end
        				
        			end
        				
        		end
        				
        			
        	end
        	
        	
        	function NormCast = NormForecast(obj,TheCast,TimeLength,ForecastType)
        		%normalizes the forecast to the timelength (used for realtime mode)
        		
        		%factor to norm
        		normFact=TimeLength/obj.OriginalLength;
        		
        		switch ForecastType
        			case 'Poisson'
        				TheCast(:,9)=TheCast(:,9)*normFact;
        				
        			case 'NegBin'
        				%to be tested
        				
        				ExpValue = ((1-TheCast(:,11)).*TheCast(:,9))./TheCast(:,11);
        				VarValue = ((1-TheCast(:,11)).*TheCast(:,9))./(TheCast(:,11).^2);
        				CorExp = ExpValue*normFact;
        				newP = CorExp./VarValue;
					newR = (newP.*CorExp)./(1-newP);
        				TheCast(:,9)=newR;
					TheCast(:,11)=newP;
					
        			case 'Custom'
        				error('Custom distribution currently not supported for normalization');
        		end
        		
        		
        		
        		NormCast = TheCast;
        	
        	end
        	
        	
        	
        	function [TestResult corResDef] = buildMonteResult(obj,OrgTestResults,ResDef)
        		%builds an initial TestResult structure with the results
        		%from the original run and the suitable arrays for the the
        		%monte-carlo results.
        		%corResDef is the same as RefDef, but 'normal' is replaced according to the
        		%data format
        		
        		TheFields=fieldnames(OrgTestResults);
        		TestResult=struct;
        		
        		NrMonti=obj.TestConfig.Nr_Monte_Catalogs;
        		
        		for i=1:numel(TheFields)
        			
        			switch ResDef.(TheFields{i}){2}
        				case 'normal'
        					switch ResDef.(TheFields{i}){1}
        						case 'scalar'
        							orgFieldName=['org_',TheFields{i}];
								MonteFieldName=['single_',TheFields{i}];
								
								TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
								TestResult.(MonteFieldName)=zeros(NrMonti,1);
        						       						
        							ResDef.(TheFields{i}){2}='stats';
        							corResDef.(TheFields{i})=ResDef.(TheFields{i});
        				
        						
        						case {'array','string','cell'}
        							orgFieldName=['org_',TheFields{i}];
								MonteFieldName=['single_',TheFields{i}];
								
								TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
								TestResult.(MonteFieldName)=cell(NrMonti,1);
								
								ResDef.(TheFields{i}){2}='collect';
								corResDef.(TheFields{i})=ResDef.(TheFields{i});	
        						end
        				
        				
        				case 'ignore'
        					TestResult.(TheFields{i})=OrgTestResults.(TheFields{i});
        					corResDef.(TheFields{i})=ResDef.(TheFields{i});
        					
        				case 'stats'
        					orgFieldName=['org_',TheFields{i}];
        					MonteFieldName=['single_',TheFields{i}];
        					
        					TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
        					TestResult.(MonteFieldName)=zeros(NrMonti,1);
        					
        					corResDef.(TheFields{i})=ResDef.(TheFields{i});
        					
        				case 'collect'
        					orgFieldName=['org_',TheFields{i}];
        					MonteFieldName=['single_',TheFields{i}];
        					
        					TestResult.(orgFieldName)=OrgTestResults.(TheFields{i});
        					TestResult.(MonteFieldName)=cell(NrMonti,1);
        					
        					corResDef.(TheFields{i})=ResDef.(TheFields{i});
        					
        					
        			end	
        		
        		
        			
        		end
        		
        		
        	
        	
        	end
        	
        	
        	
        	
        	
        	
        	function TestResult = addMonteResult(obj,IDX,oldTestResult,CurTestResults,ResDef)
        		%Adds new results to a previous TestResult structure, it is just outsourced
        		%from the main calculation, to make it more readable.
        		
        		%'normal' will not be considered, it should be replaced by buildMonteResult
        		
        		TheFields=fieldnames(oldTestResult);
        		TestResult=oldTestResult;
        		
        		       		
        		for i=1:numel(TheFields)
        			
        			switch ResDef.(TheFields{i}){2}
        				
        				case 'normal'
        					disp('there still is a "normal" field in the Result definition');
        			
        				case 'ignore'
        					%nothing has to be done
        					
        				case 'stats'
        					MonteFieldName=['single_',TheFields{i}];
        					TestResult.(MonteFieldName)(IDX)=CurTestResults.(TheFields{i});
        				
        					
        				case 'collect'
        					MonteFieldName=['single_',TheFields{i}];
        					TestResult.(MonteFieldName){IDX,1}=CurTestResults.(TheFields{i});
        				
        					
        			end	
        		
        		
        			
        		end
        		
        		
        	
        	
        	end
        	
        	
        	
        	function TestResult = MonteResultStats(obj,oldTestResult,ResDef);
        		%Does the wanted statistics for the uncertainty estimation, it is just outsourced
        		%from the main calculation, to make it more readable.
        		
        		%'normal' will not be considered, it should be replaced by buildMonteResult
        		
        		TheFields=fieldnames(oldTestResult);
        		TestResult=oldTestResult;
        		
        		%at the moment using not the "nan" version of mean and std, may change if needed 
        		
        		%config values
        		NrMonti=obj.TestConfig.Nr_Monte_Catalogs;
        		doMean=obj.TestConfig.CalcMean;
        		doMedian=obj.TestConfig.CalcMedian;
        		
        		for i=1:numel(TheFields)
        			
        			switch ResDef.(TheFields{i}){2}
        				
        				case 'normal'
        					disp('there still is a "normal" field in the Result definition');
        			
        				case 'ignore'
        					%nothing has to be done
        					
        				case 'stats'
        					MonteFieldName=['single_',TheFields{i}];
        					
        					if doMean
        						MeanFieldName=['mean_',TheFields{i}];
        						stdFieldName=['std_',TheFields{i}];
        						
        						TestResult.(MeanFieldName)=mean(oldTestResult.(MonteFieldName));
        						TestResult.(stdFieldName)=std(oldTestResult.(MonteFieldName));
        					end
        					
        					
        					if doMedian
        						MedianFieldName=['median_',TheFields{i}];
        						QuantFieldName=['quant68_',TheFields{i}];
        						
        						TheQuant=quantile(oldTestResult.(MonteFieldName),[0.16,0.5,0.84]);
        						TestResult.(MedianFieldName)=TheQuant(2);
        						TestResult.(QuantFieldName)=[TheQuant(1),TheQuant(3)];
        					end
        					
        					
        				case 'collect'
        					%nothing to do 
        				
        					
        			end	
        		
        		
        			
        		end
        		
        		
        	
        	
        	end
        	
        end

end	

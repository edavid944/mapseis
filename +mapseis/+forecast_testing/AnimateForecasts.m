classdef AnimateForecasts < handle



	properties
        	Name
        	ID
        	Version
        	Forecasts
        	TimeVector
        	ForecastLength
        	RegionConfig
        	Datastore
        	Filterlist
        	
        	FixedTime
        	PlotMagnitude
        	TimeToPlot
        	AxisVectors
 		PlotWindow
 		singleWindow
 		plotEqs
 		plotBorder
 		plotCoast
 		PlotAxis
 		PlotLayout
 		VideoWindow
 		VideoFrames
 		LogMode
 		colorAxis
 		LastStep
 		continueCalc
 		AutoSave
 		SaveStep
 		TempFileName
 		
        end
        

    
        methods
        	function obj = AnimateForecasts(Name)
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        		
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		obj.Name=Name; %may be not so important here, but who knows?
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.Version='0.9';
        		
        		obj.Forecasts=[];
        		obj.TimeVector=[];
        		obj.RegionConfig=[];
        		obj.ForecastLength=[];
			obj.Datastore=[];
			obj.Filterlist=[];
        		
			
			obj.singleWindow=true;
			obj.plotEqs=true;
			obj.plotBorder=true;
			obj.plotCoast=true;
			obj.FixedTime=false;
			obj.PlotMagnitude=[];
			obj.TimeToPlot=[];
			obj.AxisVectors={};
			obj.PlotLayout=[];
			obj.VideoWindow=[];
			obj.VideoFrames=[];
			obj.LogMode=true;
        		obj.colorAxis=[];
        		obj.LastStep=[];
        		obj.continueCalc=false;
        		
        		obj.AutoSave = false;
        		obj.SaveStep = 100;
        		obj.TempFileName = './Temporary_Files/TempMovie.mat';
        		
        	end
        	
        	
        	
        	function addProject(obj,ForecastProject)
        		%adds a forecast project to the TestSuite
        		%currently only one Project object per TestSuiteObject 
        		%is allowed, but a merger should be built and added.
        		import mapseis.filter.*;
        		
        		        		
        	        		
        		%get the interal catalog and filterlist
        		obj.Datastore=ForecastProject.Datastore;
        		        		
        		%get minimum magnitude and set filter to that value -0.3
        		%Same for the depth but with +10
        		RegConfig=ForecastProject.buildRegionConfig;
        		MinMag=RegConfig.MinMagnitude;
        		MaxMag=RegConfig.MaxMagnitude;
        		MinDepth=RegConfig.MinDepth;
        		MaxDepth=RegConfig.MaxDepth;
        		
        		Range='in';
        			
        		obj.Filterlist=FilterListCloner(ForecastProject.Filterlist);
        		MagFilt=obj.Filterlist.getByName('Magnitude');
        		MagFilt.setRange(Range,[MinMag MaxMag]);
        		DepthFilt=obj.Filterlist.getByName('Depth');
        		DepthFilt.setRange(Range,[MinDepth MaxDepth]);
        		
        		        		        		
        		%generate model and time list:
        		%get models
        		[Forecasts,Names,Pos] = ForecastProject.getAllForecasts;
        		
        		%get times
        		TimeSteps = ForecastProject.AvailableTimes;
        		
        		
        		%store
        		obj.Forecasts={Forecasts,Names,Pos};
        		obj.TimeVector=TimeSteps;
        		obj.ForecastLength=ForecastProject.ForecastLength;
        		obj.RegionConfig=RegConfig;
        	end
        	
        	
        	function addAddonData(obj,Datastore,Filterlist)
        		%allows to set a addon catalog and filterlist, which
        		%will be used instead of the catalog and filterlist in
        		%the Project object. Often needed when due error estimation
        		%as this will need a "shaker" catalog.
        		
        		%get the interal catalog and filterlist
        		obj.Datastore=Datastore;
        		obj.Filterlist=Filterlist;
        		
        		if isempty(Datastore)
        			obj.Datastore=obj.ForecastProject.Datastore;
        		end
        		
        		if isempty(Filterlist)
        			obj.Filterlist=obj.ForecastProject.Filterlist;
        		end
        		
        		
        		
        	end
        	
        	
        	function initWindow(obj,PlotLayout)
        		%opens new window
        		
        		%PlotLayout is for the singleWindow style and should give
        		%the layout for subplots e.g. PlotLayout=[2,2] for a plot
        		%with for subplots arranged in a 2x2 way.
        		
        		%calculate monitor screensize
        		ScreenSize=get(0,'MonitorPositions');
        		PlotSize=round(0.8*ScreenSize);
        		if obj.singleWindow
				try 
					close(obj.PlotWindow)
				end
				
				obj.PlotWindow=figure;
				set(obj.PlotWindow,'Position',PlotSize);
				if ~isempty(obj.Forecasts)
					for i=1:numel(obj.Forecasts{1})
						obj.PlotAxis(i)=subplot(PlotLayout(1),PlotLayout(2),i);
					end
				else
					error('No Forecast set')
				end
				
				obj.PlotLayout=PlotLayout;
        		
        		else
        					
        			if ~isempty(obj.Forecasts)
					for i=1:numel(obj.Forecasts{1})
						try 
							close(obj.PlotWindow(i))
						end	
						
						obj.PlotWindow(i)=figure;
						set(obj.PlotWindow(i),'Position',PlotSize);
						obj.PlotAxis(i)=gca;
					end
				else
					error('No Forecast set')
				end
        		
        		
        		
        		end
        	end
        	
        	
        	function setPlotMode(obj,TimeMode,PlotPoint)
        		
        		obj.FixedTime=TimeMode;
        		
        		
        		if obj.FixedTime
				obj.TimeToPlot=PlotPoint;
				obj.PlotMagnitude=[];
			else		
				obj.PlotMagnitude=PlotPoint;
				obj.TimeToPlot=[];
        		end
        		
        	
        	end
        	
        	
        	
        	function generateVideo(obj)
        		%main function 
        		import mapseis.filter.*;
        		import mapseis.projector.*;
        		import mapseis.plot.*;
        		
        		if obj.continueCalc
        			startStep=obj.LastStep+1;
        			TheMovie=obj.VideoFrames;
        		else
        			startStep=1;
        			RawStruct(numel(obj.TimeVector),1) = struct('cdata',[],'colormap',[]);
        			if obj.singleWindow
        				TheMovie=RawStruct;
        			else
        				for j=1:numel(obj.Forecasts{1})
        					TheMovie{j}=RawStruct;
        				end
        			end
        		end
        		
        		if ~obj.FixedTime
        			
        			obj.Filterlist.changeData(obj.Datastore);
        			
        			minTime=obj.TimeVector(1);
        			maxTime=obj.TimeVector(end)+obj.ForecastLength;
        			MaxTimeInt=[minTime,maxTime];
        			Range='in';
        			TimeFilt=obj.Filterlist.getByName('Time');
        			TimeFilt.setRange(Range,MaxTimeInt);
        					
				obj.Filterlist.updateNoEvent;
				
				selected=obj.Filterlist.getSelected;
				rawSelect=false(size(selected));
				selNr=find(selected);
        			[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
				obj.Filterlist.PackIt;
				
				
				%get axis and other not changing data
				[SpaceGrid DepthGrid  MagGrid] = obj.Forecasts{1}{1}.getAxisMesh;
				obj.AxisVectors=SpaceGrid;
				
				MagTitle=['M=',num2str(obj.PlotMagnitude)];
				
				
				for i=startStep:numel(obj.TimeVector)
					%apply filter and catalog
						
					CurTime=obj.TimeVector(i);
					TimeInt=[CurTime,CurTime+obj.ForecastLength];
					selector=ShortCat(:,4)>=TimeInt(1)&ShortCat(:,4)<TimeInt(2);        			
					
					TimeTitle=datestr(obj.TimeVector(i),'dd.mm.yyyy');
					
					if obj.singleWindow
						for j=1:numel(obj.Forecasts{1})
								
							%get forecast slice
							try
								tic
								[TheSlice ForeTime ForeLength] = obj.Forecasts{1}{j}.get2DSlice(obj.TimeVector(i),obj.PlotMagnitude,[]);
								ImReady=true;
								disp(['time data import:',num2str(toc)]);
								
							catch
								warning('Forecast not found');
								ImReady=false;
								
							end
							
							if ImReady
								%set axis
								axes(obj.PlotAxis(j));
								
								%clear old plot
								h1=plot(mean(obj.AxisVectors{1}(:)),mean(obj.AxisVectors{2}(:)),'w');
								
								
								%plot map
								tic
								hand(j)=obj.plotForecast(obj.PlotAxis(j),TheSlice);
								hold on;
								disp(['time map plot:',num2str(toc)]);
								
								%colorbar
								myBar=colorbar;
								
								%axis limiter
								xlim([min(obj.AxisVectors{1}(:)),max(obj.AxisVectors{1}(:))]);
								ylim([min(obj.AxisVectors{2}(:)),max(obj.AxisVectors{2}(:))]);
								xlimiter=xlim;
								ylimiter=ylim;
								
								%coastline & border
								CoastConf= struct(	'PlotType','Coastline',...
											'Data',obj.Datastore,...
											'X_Axis_Label','Longitude ',...
											'Y_Axis_Label','Latitude ',...
											'LineStylePreset','Fatline',...
											'Colors','white');
								BorderConf= struct(	'PlotType','Border',...
											'Data',obj.Datastore,...
											'X_Axis_Label','Longitude ',...
											'Y_Axis_Label','Latitude ',...
											'LineStylePreset','dotted',...
											'Colors','black');
								if obj.plotCoast
									tic			
									[OverlayHandle(1) CoastEntry] = PlotCoastline(obj.PlotAxis(j),CoastConf);
									disp(['time coastline plot:',num2str(toc)]);
								end
								
								if obj.plotBorder
									tic
									[OverlayHandle(2) BorderEntry] = PlotBorder(obj.PlotAxis(j),BorderConf);
									disp(['time border plot:',num2str(toc)]);
								end
								
								%plot earthquake if wanted
								
								if any(selector)&obj.plotEqs
									tic
									NewSel=rawSelect;
									NewSel(selNr(selector))=true;
									EqConfig= struct(	'PlotType','Earthquakes',...
												'Data',obj.Datastore,...
												'PlotMode','old',...
												'PlotQuality','hi',...
												'SelectionSwitch','selected',...
												'SelectedEvents',NewSel,...
												'MarkedEQ','max',...
												'X_Axis_Label','Longitude ',...
												'Y_Axis_Label','Latitude ',...
												'MarkerSize','none',...
												'MarkerStylePreset','none',...
												'Colors','black',...
												'X_Axis_Limit',xlimiter,...
												'Y_Axis_Limit',ylimiter,...
												'C_Axis_Limit','auto',...
												'LegendText','Earthquakes');
												
									[EqHandle entry1] = PlotEarthquake(obj.PlotAxis(j),EqConfig);	
									disp(['time earthquaje plot:',num2str(toc)]);
								end
								
								
								%title and axis
								TheTitle=[TimeTitle,', ',MagTitle,': ',obj.Forecasts{2}{j},'   '];
								tit=title(TheTitle);
								set(tit,'FontSize',16,'FontWeight','bold','Interpreter','none');
								
								
								set(obj.PlotAxis(j),'YDir','normal');
								set(obj.PlotAxis(j),'LineWidth',2,'FontSize',10,'FontWeight','bold','Box','on');
								latlim = get(obj.PlotAxis(j),'Ylim');
								set(obj.PlotAxis(j),'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
								
								xlab=xlabel('Longitude  ');
								ylab=ylabel('Latitude  ');
								set([xlab,ylab],'FontSize',12,'FontWeight','bold');
								hold off;
								
								
							
							end
						
						end
						
						hold off;
						
						%save frame
						tic
						TheMovie(i)=getframe(obj.PlotWindow);
						disp(['time movie frame:',num2str(toc)]);
						%store movie in the object
						obj.VideoFrames=TheMovie;
						
						
						
					else
						for j=1:numel(obj.Forecasts{1})
								
							%get forecast slice
							try
								[TheSlice ForeTime ForeLength] = obj.Forecasts{1}{j}.get2DSlice(obj.TimeVector(i),obj.PlotMagnitude,[]);
								ImReady=true;
							catch
								warning('Forecast not found');
								ImReady=false;
								
							end
							
							if ImReady
								%set axis
								axes(obj.PlotAxis(j));
								
								%clear old plot
								h1=plot(mean(obj.AxisVectors{1}(:)),mean(obj.AxisVectors{2}(:)),'w');
								
								
								%plot map
								hand(j)=obj.plotForecast(obj.PlotAxis(j),TheSlice);
								hold on;
								
								%colorbar
								myBar=colorbar;
								
								%axis limiter
								xlim([min(obj.AxisVectors{1}(:)),max(obj.AxisVectors{1}(:))]);
								ylim([min(obj.AxisVectors{2}(:)),max(obj.AxisVectors{2}(:))]);
								xlimiter=xlim;
								ylimiter=ylim;
								
								%coastline & border
								CoastConf= struct(	'PlotType','Coastline',...
											'Data',obj.Datastore,...
											'X_Axis_Label','Longitude ',...
											'Y_Axis_Label','Latitude ',...
											'LineStylePreset','Fatline',...
											'Colors','white');
								BorderConf= struct(	'PlotType','Border',...
											'Data',obj.Datastore,...
											'X_Axis_Label','Longitude ',...
											'Y_Axis_Label','Latitude ',...
											'LineStylePreset','dotted',...
											'Colors','black');
											
								if obj.plotCoast
									tic			
									[OverlayHandle(1) CoastEntry] = PlotCoastline(obj.PlotAxis(j),CoastConf);
									disp(['time coastline plot:',num2str(toc)]);
								end
								
								if obj.plotBorder
									tic
									[OverlayHandle(2) BorderEntry] = PlotBorder(obj.PlotAxis(j),BorderConf);
									disp(['time border plot:',num2str(toc)]);
								end
								
								
								%plot earthquake if wanted
								if any(selector)&obj.plotEqs
									NewSel=rawSelect;
									NewSel(selNr(selector))=true;
									EqConfig= struct(	'PlotType','Earthquakes',...
												'Data',obj.Datastore,...
												'PlotMode','old',...
												'PlotQuality','hi',...
												'SelectionSwitch','selected',...
												'SelectedEvents',NewSel,...
												'MarkedEQ','max',...
												'X_Axis_Label','Longitude ',...
												'Y_Axis_Label','Latitude ',...
												'MarkerSize','none',...
												'MarkerStylePreset','none',...
												'Colors','black',...
												'X_Axis_Limit',xlimiter,...
												'Y_Axis_Limit',ylimiter,...
												'C_Axis_Limit','auto',...
												'LegendText','Earthquakes');
												
									[OverlayHandle(3) entry1] = PlotEarthquake(obj.PlotAxis(j),EqConfig);			
								end
								
								
								%title and axis
								TheTitle=[TimeTitle,', ',MagTitle,': ',obj.Forecasts{2}{j},'   '];
								tit=title(TheTitle);
								set(tit,'FontSize',36,'FontWeight','bold','Interpreter','none');
								
								
								set(obj.PlotAxis(j),'YDir','normal');
								set(obj.PlotAxis(j),'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
								latlim = get(obj.PlotAxis(j),'Ylim');
								set(obj.PlotAxis(j),'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
								
								xlab=xlabel('Longitude  ');
								ylab=ylabel('Latitude  ');
								set([xlab,ylab],'FontSize',18,'FontWeight','bold');
								
								hold off;
								
								%save frame
								TheMovie{j}(i)=getframe(obj.PlotWindow(j));
								%store movie in the object
								obj.VideoFrames=TheMovie;
							
							end
							
						end
						
						
					
					end
						
					obj.LastStep=i;
					if mod(i,obj.SaveStep)==0 & obj.AutoSave
						obj.SaveInterMovie;
					end
					
					
				end
				
				
				
				
				
			else
				%LATER
			
			
			
			
			end	
			
			
			
			%store movie in the object
			obj.VideoFrames=TheMovie;
			
			
			
        	
        	end
        	
        	
        	
        	function SaveStruct = exportInterCalc(obj)
        		SaveStruct = struct(	'MovieData',obj.VideoFrames,...
        					'LastStep',obj.LastStep);
        					
        					
        	
        	end
        	
        	
        	function importInterCalc(obj,SaveStruct)
        		
        		obj.VideoFrames = SaveStruct.MovieData;
        		obj.LastStep = SaveStruct.LastStep;
        		obj.continueCalc = true;
        		
        	end
        	
        	
        	function SaveInterMovie(obj)
        		 SaveStruct = obj.exportInterCalc;
        		 save(obj.TempFileName,'SaveStruct');
        	end
        	
        	
        	
        	
        	function hand=plotForecast(obj,plotAxis,TheSlice)
        		
        		if obj.LogMode
				%avoid infinity
				TheSlice(TheSlice==0)=realmin;
				TheSlice=log10(TheSlice);
			end	
        	
        		
        		overMin=min(TheSlice(:));
			overMax=max(TheSlice(:));
			
			
			
			axes(plotAxis);
						
			hand = image(obj.AxisVectors{1}(1,:),obj.AxisVectors{2}(:,1),TheSlice);
															
			%some additional plot settings
			set(hand,'CDataMapping','scaled');
			
			if ~isempty(obj.colorAxis)
				caxis(obj.colorAxis);
			end
			
        	end
        	
        	
        	
        	function playMovie(obj,Speed,WhichOne)
        		%replays the movie in the set speed
        		
        		if isempty(obj.VideoFrames)
        			error('No movie calculated');
        		end
        		
        		newFigure=figure;
        	
        		if iscell(obj.VideoFrames)
        			movie(obj.VideoFrames{WhichOne},Speed);
        		
        		else
        			movie(obj.VideoFrames,Speed);
        		
        		end
        		
        	
        	end
        	
     
        	

        end
        

end	

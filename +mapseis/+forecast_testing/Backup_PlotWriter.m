classdef PlotWriter < handle
	%This object plots the results (in the form of TestResObject) and saves
	%the plot automatically 

	
	properties
        	Name
        	Version
        	ID
        	ObjVersion
        	MetaData
        	TestResultObj
        	PlotterConfig
        	PlotFunctions
        	PlotFunctionName
        	
        	ModelList
        	TestList
        	TestPlotList
        	TimeList
        	AvailableData
        	SelectedData
        	EmptySelect
        	MasterTestList
        	
        		
        	TestPlugInList
        	OutputFolders
        	FileName
        	extGUI
        	PlotterConfig
        	
        	NextPlot
        	LoopMode
        	
        	WorkToDo
        	WorkList
        	WorkDone
        end
        

    
        methods
        	function obj = PlotWriter(Name)
        		%first version of automatic test plotter, not too many 
        		%features for now, only the one needed at the moment
        	
        		
        		%TODO: add the whole TR-test Ranking plot mode
        		%and also find a solution the SignMat plot.
        		%3 results (TR-, T- and W-test) with 2 plots vs
        		%only 2 results (T- and W-test) and only 1 plot
        		%leaving the ranking info gain test to TR ranking plot
        		
        		
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        		
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		obj.Name=Name; %may be not so important here, but who knows?
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.Version='0.9';
        		
        		obj.MetaData={};
			obj.TestResultObj=[];
			obj.PlotterConfig=[];
			obj.PlotFunctions=[];
			obj.PlotFunctionName={};
			
			obj.ModelList={};
			obj.TestList={};
			obj.TestPlotList={};
			obj.TimeList=[];
			obj.AvailableData=[];
			obj.SelectedData=[];
			obj.EmptySelect=[];
			obj.MasterTestList={};
        		
			
			
			obj.TestPlugInList=[];
			obj.OutputFolders={};
			obj.FileName=[];
			obj.extGUI=[];
			obj.WorkList={};
			obj.WorkToDo=[];
			obj.WorkDone={};
			
			obj.NextPlot=1;
			obj.LoopMode=true;
        		
			obj.PlotterConfig = struct(	'WindowSize',[[800 600]],...
							'PlotOnly',false,...
							'ExcludeEmptyDays',true,...
							'PlotOriginal',true,...
							'PlotMedian',false,...
							'PlotMean',false);
							
        	end
        	
        	
        	function addTestResult(obj,TestResultObj)
        		%adds a TestResult object 
        		
        		
        		obj.TestResultObj=TestResultObj;
        		
        		%determine available times, models, tests and plots
        		obj.TimeList=unique(TestResultObj.CalcedResults{2})';
        		obj.ModelList=TestResultObj.Forecasts{2};
        		obj.TestList=fieldnames(TestResultObj.possiblePlots);
        		obj.TestPlotList=TestResultObj.possiblePlots;
        		
        		%set folders
        		obj.generateFolderStruct;
        		
        		%scan functions
        		obj.scanPlotFunctions;
        		
        		%generate structure with availability
        		obj.generateModTime;
        	end
        	
        	
        	function generateTestTypeStruct(obj)
        		%generates a list with testtype and name of the test in
        		%structure
        			
        		
        		
        		for tC=1:numel(obj.TestList(:,1))
        				%check for none tests
        				IdSeq=obj.TestList{tC,1}(1:5);
        				if strcmp(IdSeq,'Ltest')
        					TestType='Ltest';
        					TestBasis='Likelihood';
        				elseif strcmp(IdSeq,'Ntest')
        					TestType='Ntest';
        					TestBasis='Likelihood';
        				elseif strcmp(IdSeq,'Mtest')
        					TestType='Mtest';
        					TestBasis='Likelihood';
        				elseif strcmp(IdSeq,'Stest')
        					TestType='Stest';
        					TestBasis='Likelihood';
        				elseif strcmp(IdSeq,'CLtes')
        					TestType='CLtest';
        					TestBasis='Likelihood';
        				elseif strcmp(IdSeq,'Ttest')
        					TestType='Ttest';
        					TestBasis='InfoGain';
        				elseif strcmp(IdSeq,'Wtest')
        					TestType='Wtest';
        					TestBasis='InfoGain';
        				elseif strcmp(IdSeq,'Singl')
        					TestType='TRanktest';
        					TestBasis='InfoGain';
        				else
        					TestType='Custom';
        					TestBasis='Unknown';
        				end
        				
        				obj.TestList{tC,2}=TestType;
        				obj.TestList{tC,3}=TestBasis;
        		end
        			
        	end
        	
        	
        	
        	function generateFolderStruct(obj)
        		%generates the structure with folders, seperated into a 
        		%function to avoid dublications
        		
        		
        		fs=filesep;
        		defaultMain=['.',fs,'MyTestPlots'];
        		if isempty(obj.OutputFolders)
        			obj.OutputFolders.MainFolder=defaultMain;
        		end
        		
        		if ~isfield(obj.OutputFolders,'MainFolder')
        			obj.OutputFolders.MainFolder=defaultMain;
        		end
        		
        		if ~isempty(obj.TestList)
        			MainFold = obj.OutputFolders.MainFolder;		
        			for tC=1:numel(obj.TestList(:,1))
        				%check for none tests
        				IdSeq=obj.TestList{tC,1}(1:5);
        				if strcmp(IdSeq,'Ltest')
        					TestFoldName='Ltest';
        				elseif strcmp(IdSeq,'Ntest')
        					TestFoldName='Ntest';
        				elseif strcmp(IdSeq,'Mtest')
        					TestFoldName='Mtest';
        				elseif strcmp(IdSeq,'Stest')
        					TestFoldName='Stest';
        				elseif strcmp(IdSeq,'CLtes')
        					TestFoldName='CLtest';
        				elseif strcmp(IdSeq,'Ttest')
        					TestFoldName='Ttest';
        				elseif strcmp(IdSeq,'Wtest')
        					TestFoldName='Wtest';
        				elseif strcmp(IdSeq,'Singl')
        					TestFoldName='TRanktest';
        				else
        					TestFoldName=['TestNr_',num2str(tC)];
        				end
        				
        				obj.OutputFolders.(obj.TestList{tC,1})=...
        					[MainFold,fs,TestFoldName];
        			end
        			
        			
        		
        		end
        		
        		%additional folders
        		obj.OutputFolders.CombCompareFolder=[MainFold,fs,'Comb_Comparison'];
        		obj.OutputFolders.CombLikeFolder=[MainFold,fs,'Comb_Likelihood'];
        		obj.OutputFolders.SummaryFolder=[MainFold,fs,'TestSummary'];
        		
        	end
        	
        	
        	
        	function setMainFolder(obj,MainFolder)
        		%sets main folder for saving the plots
        		
        		obj.OutputFolders.MainFolder=MainFolder;
        		obj.generateFolderStruct;
        		
		end        	
        	
		
        	
        	function ConfigurePlotting(obj,ConfigStruct)
        		
        		%field erase protection
        		defField=fieldnames(ConfigStruct);
        		for fC=1:numel(defField)
        			obj.PlotterConfig.(defField{fC})=ConfigStruct.(defField{fC});;
        		end
        			
        			
        	end
        	
        	
        	function ConfigStruct = getConfig(obj)
        		ConfigStruct=obj.PlotterConfig;
        	end
        	
        	
        	function scanPlotFunctions(obj)
        		%scans the available plot function to determine the input
        		%they need
        		import mapseis.forecast_testing.PlotPlugIns.*;
        		
        		PlotFunctions=[];
        		custoCount=1;
        		
        		for tC=1:numel(obj.TestList(:,1))
        			availPlots=obj.TestPlotList.(obj.TestList{tC,1}))(:,2);
        			
        			for fC=1:numel(availPlots)
        				if isstr(PlotFunctions,availPlots{fC})
        					SregName=availPlots{fC};
        				else
        					RawString=func2str(availPlots{fC});	
        					%kick out all '(',')','@'
        					selCrit = RawString=='(' | RawString==')' |...
        						RawString=='@';
        					RawString=RawString(~selCrit);
        					
        					%replace '.' by '_'
        					TheDot=RawString=='.';
        					RawString(TheDot)='_';
        					SregName=RawString;
        				end
        				
        				
        				
        				if ~isfield(PlotFunctions,SregName)
        					switch SregName	
        						case 'LikeHist'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							PlotFunctions.(SregName).PlotObject= LikelihoodHist();
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='single';
        							
        						case 'LikeCDF'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							PlotFunctions.(SregName).FuncHandle= LikelihoodCDF();
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='single';
        							
        						case 'MultiHist'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							PlotFunctions.(SregName).PlotObject=MultiHist();
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='testwise';
        						
        						
        						case 'MultiBox'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							PlotFunctions.(SregName).PlotObject=MultiBox();
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='testwise';	
        						
        						
        						case 'CombinedLike'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							PlotFunctions.(SregName).PlotObject=CombinedLike();
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='modelwise';	
        							
        							
        						case 'SignMatrix'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							%PlotFunctions.(SregName).PlotObject=CombinedLike;
        							PlotFunctions.(SregName).No_Plots=2;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='TWcombined';				
        		
        						case 'Ranking'
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							%PlotFunctions.(SregName).PlotObject=CombinedLike;
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='comparison';	
        						
        						case 'RatioCDF'
        							%MISSING
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							%PlotFunctions.(SregName).PlotObject=CombinedLike;
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='comparison';	
        							
        						case 'ClassicT'
        							%MISSING
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							%PlotFunctions.(SregName).PlotObject=CombinedLike;
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='comparison';	
        							
        						case 'ClassicW'
        							%MISSING
        							PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
        							%PlotFunctions.(SregName).PlotObject=CombinedLike;
        							PlotFunctions.(SregName).No_Plots=1;
        							PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
        							PlotFunctions.(SregName).PlotType='comparison';		
        						
        						otherwise
        							%unknown, means it is a function handle
        							%UNFINISHED
        							TheObject=SregName();
        							PlotName=['Plot_'num2str(custoCount)];
        							custoCount=custoCount+1;
        							PlotFunctions.(PlotName).PlotObject=TheObject;
        							
        					
        					
        				else
        				
        					PlotFunctions.(SregName).SupportedTests(end+1)=obj.TestList(tC,1);
        				
        				
        				end
        					
        			end
        		end
        		
        		obj.PlotFunctions=PlotFunctions;
        		obj.PlotFunctionName=fieldnames(PlotFunctions);
        		
        	end
        	
        	
        	
        	function generateModTime(obj)
        		%builds the list with all possible model and times
        		%used for the selection	
        		
        		%a structure is needed to bring order into the chaos of 
        		%test results. Suggestion:
        		%	1. Plot function
        		%	2. Available Tests for the plot function
        		%	3. Available Models (-Combinations) for this test
        		%	4. Available Timesteps for this model.
        		
        		%Plots which combine multiple results will be treated 
        		%similar to the single-test-model-time plot, the first
        		%one will just produce one plot with the input and the 
        		%second will plot multiple plots.
        		
        		%Summary can also be selected instead/additional to time
        		%steps.
        		
        		%one complication is still there, plots like the combined
        		%likelihood plot are best for multiple tests of the SAME
        		%model, but not only, so I guess they have to be treated a bit special.
        		%But how exactly and how should I design the communication
        		%between test and this object to assure, that future tests
        		%with similar properties are treated the same?
        		
        		%possible ways to deal with the problem:
        		%1. fixed approach, as soon as a plot of this kind is used
        		%	the data is reordered and automatically divided into
        		%	model->tests instead of the normal tests->models
        		%2. Change the structure order for this tests: 
        		%	plot->model->test->time 
        		
        		%I think the first one is probably the better option, because
        		%less confusing and error prone. It would also be possible to
        		%add a switch later, for changing the plot order.
        		
        		%similar things have to be done for the T-test and W-test
        		
        		%As for the communication between the object, in case of the 
        		%include plots (currently the only available ones), it is 
        		%know already (a switch statement is probalby the savest thing),
        		%for the custom function, the object has to be created and the
        		%feature list has to be used (I may have to update the 
        		%specification of this list)
        		%I should do the scanning in a separate funcion
        		
        		%build a template for plot selection as well.
        		
        		availStruct=[];
        		selectionStruct=[];
        		summaryStruct=[];
        		summarySelStruct=[];
        		
        		%get possible plots
        		ThePlots=obj.PlotFunctionName;
        		MasterTestList={};
        		MasterModelList={};
        		
        		%all possible times
        		TimeList=obj.TimeList;
        		RawTimeSelVec=true(size(TimeList));
        		
        		%for each plots get the possible tests
        		for pC=1:numel(ThePlots)
        			TestList=obj.PlotFunctions.(ThePlots{pc}).SupportedTests;
        			
        			for tC=1:numel(TestList)
        				%get available Models
        				
        				%CHECK IF WORKING SEEMS FISHY->Nope it is correct it was just
        				%an old version of the object
        				InThisTest=strcmp(obj.TestResultObj.CalcedResults{1},TestList{tC});
        				uniMod=unique(obj.TestResultObj.CalcedResults{3}(InThisTest));
        				
        				if ~any(strcmp(MasterTestList,TestList{tC}))
        					MasterTestList{end+1}=TestList{tC};
        				end
        				
        				for mC=1:numel(uniMod)
        					inMod=strcmp(ResObj.CalcedResults{3},uniMod{mC});
        					
        					if ~any(strcmp(MasterModelList,uniMod{mC}))
							MasterModelList{end+1}=uniMod{mC};
						end
        					
        					%determine available times
        					ThisTimes=obj.TestResultObj.CalcedResults{2}(inMod&InThisTest)';
        					[timeVal,Pos] = setdiff(TimeList,ThisTimes);
        					availInThis=RawTimeSelVec;
        					if ~isempty(Pos)
        						availInThis(Pos)=false;
        					end
        					
        					availStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=availInThis;
        					selectionStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=false(size(TimeList));
        					
        					
        					if ~isempty(obj.TestResultObj.SummaryResults)
        						%summarized tests are available determine if this model and test has one
        						if isfield(obj.TestResultObj.SummaryResults,TestList{tC})	
        							if isfield(obj.TestResultObj.SummaryResults.(TestList{tC}),uniMod{mC})
        								summaryStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=true;
        								summarySelStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=false;
        							end
        						
        						end
        					end
        		
        					
        				end
        				
        			
        			end
        			
        		
        		end
        		
        		
        		
        		
        		obj.AvailableData.Time=availStruct;
        		obj.AvailableData.Summary=summaryStruct;
        		obj.SelectedData.Time=selectionStruct;
        		obj.SelectedData.Summary=summarySelStruct;
        		
        		%save an empty selection, to make live easier later.
        		obj.EmptySelect=obj.SelectedData;
        		
        		%to avoid unnecessary memory usage later:
        		obj.MasterTestList=MasterTestList;
        		%obj.MasterModelList=MasterModelList;
        	end
        	
        	
        	function setPlot(obj,Tests,Models,Plots,Times)
        		%allows to set the plots which should be plotted
        		
        		%each of parameters can be a cell array (or normal vector in case of
        		%Times, a single value or the keyword 'all' or 'none'
        		%In case of Times, also the keyword 'summary' is allowed.
        			
        		%FINISHED (but untested)
        		
        		%prepare lists with needed models and times;
        		SummarySwitch=[];
        		
        		
        		if iscell(Plots)
        			PlotList=Plots;
        			
        		else
        			if strcmp(Plots,'none')
        				%That's a full reset, delete everything
        				obj.SelectedData=obj.EmptySelect;
        				return;
        				
        			elseif strcmp(Plots,'all')
        				PlotList=fieldnames(obj.PlotFunctions);
        			else
        				PlotList={Plots};
        			end	
        		
        		end
        		
        		if iscell(Tests)
        			TestList=Tests;
        			
        		else	
        			if strcmp(Tests,'none')
        				TestList=-1;
        				%TestList=repmat({'none'},size(PlotList));
        				
        			elseif strcmp(Tests,'all')
        				TestList=fieldnames(obj.PlotFunctions);
        			else
        				TestList={Tests};
        			end	
        		end
        	
        		if iscell(Models)
        			ModelList=Models;
        		
        		else
        			if strcmp(Models,'none')
        				ModelList=-1;
        				%ModelList=repmat({'none'},size(TestList));
        			
        			elseif strcmp(Models,'all')
        				ModelList=obj.ModelList;
        			else
        				ModelList={Models};
        			end	
        		end
        	
        	
        		
        		if isnumeric(Times)
        			TimeSel=true(size(obj.TimeList));
        			[timeVal,notWanted] = setdiff(obj.TimeList,Times);
        			TimeSel(notWanted)=false;
        			
        			
        		elseif islogical(Times)
        			TimeSel=Times;
        		else
        			if strcmp(Models,'none')
        				TimeSel=false(size(obj.TimeList));
        				SummarySwitch=false;
        				
        			elseif strcmp(Tests,'all')
        				TimeSel=true(size(obj.TimeList));
        				%SummarySwitch=true;
        				
        			elseif strcmp(Tests,'summary')
        				TimeSel=[];
        				SummarySwitch=true;
        			
        			end
        		end
        		
        		
        		for pC=1:numel(PlotList)
        			if TestList==-1
        				obj.SelectedData.Time.(PlotList{pC})=obj.EmptySelect.Time.(PlotList{pC});
        				obj.SelectedData.Summary.(PlotList{pC})=obj.EmptySelect.Summary.(PlotList{pC});
        			else
        			
        				for tC=1:numel(TestList)
        					if ModelList==-1
        						obj.SelectedData.Time.(PlotList{pC}).(TestList{tC})=obj.EmptySelect.Time.(PlotList{pC}).(TestList{tC});
        						obj.SelectedData.Summary.(PlotList{pC}).(TestList{tC})=obj.EmptySelect.Summary.(PlotList{pC}).(TestList{tC});
        					else
        						for mC=1:numel(ModelList)
        							obj.SelectedData.Time.(PlotList{pC}).(TestList{tC}).(ModelList{mC})=TimeSel;
        							
        							if ~isempty(SummarySwitch)
        								obj.SelectedData.Time.(PlotList{pC}).(TestList{tC}).(ModelList{mC})=SummarySwitch;
        							       							
        							end
        						
        						end
        			
        					end
        				end	
        			end
        		
        		end
        		
        		
        	end
        	
        	
        	
        	
        	
        	function generateWorkList(obj)
        		%builds the worklist with the parts which have to be done
        		%can only be used after selection has been done and plugins
        		%are added
        		
        		%now the more complicated things, it also has to be taken into
        		%account, that some plots need multiple test results, sometime
        		%from different time-periods, different tests or different models.
        		%I guess the information should somehow be added here,... but it 
        		%would be possible to also reconstruct it during the calculation.
        		
        		%so what struct?
        		%definitely it has to be a list, either one cell array or multiple
        		%data based "vectors"
        		%Needed is for each entry:
        		%	-the plot function: only one allowed
        		%	-the test type: can be more than one
        		%	-the involved models: again, can be more than one
        		%	-the timestep(s): for many plots only one is needed
        		%			  but multiple timesteps should be 
        		%			  allowed.
        		%because of the number of "ingridients changing a cell array
        		%has to be used at least for some of the part
        		
        		
        		%it is FINISHED but has to be TESTED, I expect there to be some errors
        		%at least, given that it is all quite complex and may not the most
        		%elegant solution.
        		
        		WorkList={};
        		
        		%get possible plots
        		ThePlots=obj.PlotFunctionName;
        		        		
        		%all possible times
        		TimeList=obj.TimeList;
        		
        		%all possible models	
        		ModelList=obj.ModelList;
        		
        		%all possible tests	
        		MasterTestList=obj.MasterTestList;
        		
        		
        		%for each plots get the possible tests
        		for pC=1:numel(ThePlots)
        			ThisEntry={};
        			PlotProps=obj.PlotFunctions.(ThePlots{pc});
        			
        			
        			%check if something is active
        		
        			activeTestTime=false(size(MasterTestList));
        			activeTestSum=false(size(MasterTestList));
        			ModelListTime=[];
        			ModelListSum=[];
        			for tC=1:numel(MasterTestList)
        				RawModelList=fieldnames(obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}));
        				TimeTestActive=false;
					SumTestActive=false;					
        				
        				for mC=1:numel(RawModelList)
        					try
        						selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC})&
        							obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC});
        					catch
        						selectionTime=false;
        					end
        					
        					try
        						selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC})&
        							obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC});
        					catch
        						selectionSum=false;
        					end
        					
        					if any(selectionTime)
        						TimeTestActive=true;
        						ModNr=find(strcmp(ModelList,RawModelList{mC});
        						if ~isempty(ModNr)
        							if ~any(ModNr==ModelListTime)
        								ModelListTime(end+1)=ModNr;
        							end
        						end
        					end
        					
        					if any(selectionSum)
        						TimeTestActive=true;
        						ModNr=find(strcmp(ModelList,RawModelList{mC});
        						if ~isempty(ModNr)
								if ~any(ModNr==ModelListSum)
									ModelListSum(end+1)=ModNr;
								end
        						end
        					end
        					
        					
        				end
        				
        				activeTestTime(tC)=TimeTestActive;
        				activeTestSum(tC)=SumTestActive;
        			end
        			
        			
        			if ~any(activeTestTime|activeTestSum)
        				%nothing wanted here
        				continue;
        			
        			end
        			
        			
        			%the try-catch blocks in the section underneath are needed
        			%because some models may not exist in some tests (e.g. comparison
        			%tests)
        			
        			switch PlotProps.PlotType
        			
        				case 'single'
        					%simple case one test, one model, and one timestep
        					for tC=1:numel(MasterTestList)
        						if activeTestTime(tC)
        							for mC=1:numel(ModelListTime)
        								ModelName=ModelList{ModelListTime(mC)};
        								try
        									selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								catch
        									selectionTime=false;
        								end
        								
        								TheTimes=find(selectionTime);
        								
        								for ttC=1:numel(TheTimes)
        									WorkList(end+1,1)={pC};
        									WorkList(end,2)={tC};
        									WorkList(end,3)={mC};
        									WorkList(end,4)={TheTimes(ttC)};	
        								
        								end
        							
        							end
        						
        						end
        						
        						if activeTestSum(tC)
        							for mC=1:numel(ModelListSum)
        								ModelName=ModelList{ModelListTime(mC)};
        								try
        									selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								catch
        									selectionSum=false;
        								end
        								
        								if selectionSum
        									WorkList(end+1,1)={pC};
        									WorkList(end,2)={tC};
        									WorkList(end,3)={mC};
        									WorkList(end,4)={0};
        								end
        							end
        						
        						end
        					
        					end
        					
        	
        					
        					
        				case 'testwise'
        					%NOT PERFECT, the case where we do not want to include a test in some times steps
        					%and for only one model, is not covered. In this case it would have to be done in
        					%two steps. I may add direct support for that later, but it has no priority.
        					
        					%multiple test results (only likelihood will be used)
        					LikeliTest=strcmp(obj.TestList(:,3),'Likelihood');
        					%get all selected likelihood based tests
        					TimeTest=find(activeTestTime&LikeliTest);
        					SumTest=find(activeTestSum&LikeliTest);
        					
        					if ~isempty(TimeTest)
							for mC=1:numel(ModelListTime)
								ModelName=ModelList{ModelListTime(mC)};
								ProtoTest=MasterTestList{TimeTest(1)};
								try
									selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(ProtoTest).(ModelName)&
        									obj.SelectedData.Time.(ThePlots{pc}).(ProtoTest).(ModelName);
        							catch
        								selectionTime=false;
        							end
        							
        							TheTimes=find(selectionTime);
        							
        							for ttC=1:numel(TheTimes)
        								WorkList(end+1,1)={pC};
        								WorkList(end,2)={TimeTest};
        								WorkList(end,3)={mC};
        								WorkList(end,4)={TheTimes(ttC)};	
        							end
        							
        									
							
							end
						end
						
						
						if ~isempty(SumTest)
							for mC=1:numel(ModelListTime)
								ModelName=ModelList{ModelListTime(mC)};
								ProtoTest=MasterTestList{SumTest(1)};
								try
									selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(ProtoTest).(ModelName)&
        									obj.SelectedData.Summary.(ThePlots{pc}).(ProtoTest).(ModelName);
        							catch
        								selectionSum=false;
        							
        							end
        							
        							if selectionSum
        								WorkList(end+1,1)={pC};
        								WorkList(end,2)={SumTest};
        								WorkList(end,3)={mC};
        								WorkList(end,4)={0};	
        							end
        							
        									
							
							end
						end
						
						
						
							
        					
        				case 'modelwise'
        					%multiple models will be sent
        					
						
						
						for tC=1:numel(MasterTestList)
        						if activeTestTime(tC)
        							%get the active models
        							
        							ThisTestMod=[];
        							for mC=1:numel(ModelListTime)
        								ModelName=ModelList{ModelListTime(mC)};
        								try
        									selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								catch
        									selectionTime=false;
        								end
        										
        								if any(selectionTime)
        									if ~any(ThisTestMod==ModelListTime(mC))
        										ThisTestMod(end+1)=ModelListTime(mC);
        									end
        									
        								end
        							end
        								
        							if ~isempty(ThisTestMod)
        								ModelName=ModelList{ThisTestMod(1)};
        								selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								TheTimes=find(selectionTime);
        								
        								for ttC=1:numel(TheTimes)
        									WorkList(end+1,1)={pC};
        									WorkList(end,2)={tC};
        									WorkList(end,3)={ThisTestMod};
        									WorkList(end,4)={TheTimes(ttC)};	
        								
        								end
        							
        							
        							end
        								
        							
        							
        						
        						end
        					
        						
        						
        						if activeTestSum(tC)
        							%get the active models
        							
        							ThisTestMod=[];
        							for mC=1:numel(ModelListTime)
        								ModelName=ModelList{ModelListTime(mC)};
        								try
        									selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								catch
        									selectionSum=false;
        								end
        								
        								if any(selectionSum)
        									if ~any(ThisTestMod==ModelListTime(mC))
        										ThisTestMod(end+1)=ModelListTime(mC);
        									end
        									
        								end
        							end
        								
        							if ~isempty(ThisTestMod)
        								ModelName=ModelList{ThisTestMod(1)};
        								selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								
        								if selectionSum
										WorkList(end+1,1)={pC};
										WorkList(end,2)={tC};
										WorkList(end,3)={ThisTestMod};
										WorkList(end,4)={0};	
        								end
        								        							
        							
        							end
        								
        							
        							
        						
        						end
        						
        						
        					
        					end
        					
        					
        				case 'TWcombined'
        					%T and W-test needed 
        					%Both tests have to be selected
        					TtestID=strcmp(obj.TestList(:,2),'Ttest');
        					WtestID=strcmp(obj.TestList(:,2),'Wtest');
        					
        					if sum(TtestID|WtestID)<2
        						%one is missing so continue to to the next plot
        						continue;
        					end
        					
        					TheTests=find(TtestID|WtestID);
        					
        					if all(activeTestTime(TheTests))
        						%get model(s), remember although two models
        						%are compare only one model entry per plot
        						%is used.
        						for mC=1:numel(ModelListTime)
        							ModelName=ModelList{ModelListTime(mC)};
        							try
        								selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName)&
        									obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName);
        							catch
        								selectionTime=false;
        							end
        								
        							TheTimes=find(selectionTime);
        								
        							for ttC=1:numel(TheTimes)
        								WorkList(end+1,1)={pC};
        								WorkList(end,2)={TheTests};
        								WorkList(end,3)={mC};
        								WorkList(end,4)={TheTimes(ttC)};	
        								
        							end
        							
        						end
        					
        					
        					end
        					
        					
        					
        					if all(activeTestSum(TheTests))
        						for mC=1:numel(ModelListSum)
        							ModelName=ModelList{ModelListTime(mC)};
        							try
        								selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName)&
        									obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName);
        							catch
        								selectionSum=false;
        							end
        								
        							if selectionSum
        								WorkList(end+1,1)={pC};
        								WorkList(end,2)={TheTests};
        								WorkList(end,3)={mC};
        								WorkList(end,4)={0};
        							end
        						end
        						
        					end
        					
        				
        					
        					
        				case 'comparison'
        					%two models but they are already predefined
        					%currently the same as for 'single' just the model entries
        					%are different
        					
        					for tC=1:numel(MasterTestList)
        						if activeTestTime(tC)
        							for mC=1:numel(ModelListTime)
        								ModelName=ModelList{ModelListTime(mC)};
        								try
        									selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								catch
        									selectionTime=false;
        								end
        								
        								TheTimes=find(selectionTime);
        								
        								for ttC=1:numel(TheTimes)
        									WorkList(end+1,1)={pC};
        									WorkList(end,2)={tC};
        									WorkList(end,3)={mC};
        									WorkList(end,4)={TheTimes(ttC)};	
        								
        								end
        							
        							end
        						
        						end
        						
        						if activeTestSum(tC)
        							for mC=1:numel(ModelListSum)
        								ModelName=ModelList{ModelListTime(mC)};
        								try
        									selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
        										obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
        								catch
        									selectionSum=false;
        								end
        								
        								if selectionSum
        									WorkList(end+1,1)={pC};
        									WorkList(end,2)={tC};
        									WorkList(end,3)={mC};
        									WorkList(end,4)={0};
        								end
        							end
        						
        						end
        					
        					end
        					
        					
        			end
        			
        		
        		end
        		
        		
        		obj.WorkList=WorkList;
        		
        	
        	end
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	function startPlot(obj)
        		%starts the whole plotting routine
        		
        		%build folder structure
        		obj.initFolders
        		
        		
        		
        		if ~obj.LoopMode
				%set the recursion limit to a high enough number (matlab will stop
				%the calculation later if this is not done
				NrNeeded=numel(obj.WorkList(:,1))+500; 
				set(0,'RecursionLimit',NrNeeded);
        		end
        	
        		
        		
        		if ~obj.LoopMode
				%start calculation
				CurNr=obj.NextPlot;
				obj.NextPlot=obj.NextPlot+1;
				obj.contPlotting(CurNr);
        	
        		else
        			
        			obj.LoopItSerial;
        			        
        		end
        		
        		
        	
        	end
        	
        	
        	
        
        	
        	function contPlotting(obj,ListNr)
        	
        	
        	
        	end
        	
        	
        	
        	
        	
        	
        	function LoopItSerial(obj)
        		
        		TheEnd=numel(obj.WorkList(:,1));
        		
        		if obj.NextPlot<TheEnd
        			for kk=obj.NextCalc:TheEnd
        				obj.contPlotting(kk);
        				obj.NextPlot=obj.NextPlot+1;		
        			end
        			
        				
        			%finish it
        			obj.finishCalc;
        			
        			
        		
        		end
        	
        	
        	end
        	
        	
        	
        	
        	function initFolders(obj)
        		%generates the folders as specified by the folder structure.
        		
        		
        		fs=filesep;
        		
        		
        		OutStru=obj.OutputFolders;
        		
        		%built main folder.
        		CheckExist=exist(OutStru.MainFolder);
        		if CheckExist==0
				if isunix
					Ucommand=['mkdir -p ',OutStru.MainFolder];
					[stat, res] = unix(Ucommand);
						
					if stat==0
						disp(['Main Folder folder generated']);
					else
						warning(['Could not generate Main folder']);
							
					end
				else
					Dcommand=['mkdir -p ',OutStru.MainFolder];
					[stat, res] = dos(Dcommand);
						
					if stat==0
						disp(['Main Folder folder generated']);
					else
						warning(['Could not generate Main folder']);
							
					end
				end
        		end
			
			
			RawFolderList=fieldnames(OutStru);
			MainPos=strcmp(RawFolderList,'MainFolder');
			FolderList=RawFolderList(~MainPos);
			
			
			for fC=1:numel(FolderList)
				CheckExist=exist(OutStru.(FolderList{fC}));
				if CheckExist==0
					if isunix
						Ucommand=['mkdir -p ',OutStru.(FolderList{fC})];
						[stat, res] = unix(Ucommand);
							
						if stat==0
							disp([FolderList{fC}, ' folder generated']);
						else
							warning(['Could not generate ',FolderList{fC}, ' folder']);
								
						end
					else
						Dcommand=['mkdir -p ',OutStru.(FolderList{fC})];
						[stat, res] = dos(Dcommand);
							
						if stat==0
							disp([FolderList{fC}, ' folder generated']);
						else
							warning(['Could not generate ',FolderList{fC}, ' folder']);
								
						end
					end
				end
			
			
			
			
			end
        		
        	end
        	
        
        end

end	

function plotyPos=InitConsPlot(plotAxis,numTests,TestSpacing,Quantil,OneOrTwo,Passed)
	%generates a raw plot for plotting CSEP consistency tests
	%OneOrTwo is a logical vector with defines which column is using a 
	%Twosided test (set to true) and which one is a onesided test (false)
	%Passed is a logical array and colors all tests slides which are set to false
	%into red
	
	%Update: Quantil can now be a vector, allowing different significance niveau 
	%for different tests
	
	if nargin<6
		Passed=true(size(OneOrTwo));
	end	
	
	boxx=[];
	BBox=[];
	redCol=[1 0.5 0.5];
	greenCol=[0.6 0.6 0.6];
	redBG=[1 0.8 0.8];
	
	if all(numel(Quantil)==1);
		Quantil=ones(size(OneOrTwo))*Quantil;
	end
	
	for i=1:numTests	
		if OneOrTwo(i)
			%Two-Sided
			xBox1=[0,Quantil(i)/2,Quantil(i)/2,0,0];
			yBox1=[(i-1)*TestSpacing,(i-1)*TestSpacing,...
				(i)*TestSpacing,(i)*TestSpacing,(i-1)*TestSpacing]
			
			xBox2=[1-Quantil(i)/2,1,1,1-Quantil(i)/2,1-Quantil(i)/2];
			yBox2=[(i-1)*TestSpacing,(i-1)*TestSpacing,...
				(i)*TestSpacing,(i)*TestSpacing,(i-1)*TestSpacing]
			
			backBoxX=[Quantil(i)/2,1-Quantil(i)/2,1-Quantil(i)/2,Quantil(i)/2,Quantil(i)/2];	
			backBoxY=[(i-1)*TestSpacing,(i-1)*TestSpacing,...
				(i)*TestSpacing,(i)*TestSpacing,(i-1)*TestSpacing]
			
			BBox(i)=fill(backBoxX,backBoxY,greenCol);
			hold on;
			boxx(end+1)=fill(xBox1,yBox1,redCol);
			boxx(end+1)=fill(xBox2,yBox2,redCol);	
		else
			%Two-Sided
			xBox1=[0,Quantil(i),Quantil(i),0,0];
			yBox1=[(i-1)*TestSpacing,(i-1)*TestSpacing,...
				(i)*TestSpacing,(i)*TestSpacing,(i-1)*TestSpacing]
			backBoxX=[Quantil(i),1,1,Quantil(i),Quantil(i)];	
			backBoxY=[(i-1)*TestSpacing,(i-1)*TestSpacing,...
				(i)*TestSpacing,(i)*TestSpacing,(i-1)*TestSpacing]
			
			BBox(i)=fill(backBoxX,backBoxY,greenCol);
			hold on;			
			boxx(end+1)=fill(xBox1,yBox1,redCol);
				
		end
		
		
		
	end
	
	for i=1:numTests	
		hand(i)=plot([0 1],[TestSpacing TestSpacing]*i,'k')
			plotyPos(i)=TestSpacing/2+(i-1)*TestSpacing;
	end
	
	set(BBox(Passed),'FaceAlpha',0.1,'LineStyle','none','FaceColor',greenCol);
	set(BBox(~Passed),'FaceAlpha',0.5,'LineStyle','none','FaceColor',redBG);
	set(boxx,'FaceAlpha',1);
	set(hand,'LineWidth',3);
	set(plotAxis,'LineWidth',3,'YTick',[],'XTick',[0 0.25 0.5 0.75 1])
	xlim(plotAxis,[0,1]);
	ylim(plotAxis,[0 numTests*TestSpacing]);
	hold off;
end

classdef TestPlotObj < handle
	%a interface or abstract class  for a Forecast Test plot object. It is 
	%an more or less empty object and only specify what each plugin needs to
	%provide for being supported by mapseis forecast testing suit.
	%To use it, the plugin has to be derived from it 
	%(e.g. MyPlotObj < mapseis.forecast_testing.TestPlotObj
	
	%DON'T PANIC
	%The descriptions are longer than the code which should be written
	
	properties
		PlotName %Name of the plot, not used as title.
        	Version %Has to be a STRING, used to describe the version of the code
        	PlotInfo %The place for a description about the plot 
        	
        	PlotSelect %can be used if the plot uses more than on axis per test (see signMatPlot as example)		
        	PlotAxis	%Axis used for the plots
        	PlotHandles	%Handles of the different plot parts (optional, but suggested)
        	
        	PlotReady
        	CPUTimeData
        	
        	%PlotReady signalizes that the plot object is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it is just the time
        	%needed for plotting
        	
        end
    

        %TODO: write a description about what is wanted from the functions
        methods (Abstract)
        	%obj = TestPlotObj()
   		
        	ConfigureTestConfig(obj,TestConfig)
        	%Used to configurate the part of the configuration needed from the
        	%Test, e.g. Test Name, fields with the values, etc.
        	%For each result used a test config will be submitted, by the 
        	%plotmaker, means TestConfig will be a cell array with structures
        	%for each result (e.g. {conf1, conf2, conf3,....}
        	
        	
        	ConfigureUserConfig(obj,UserConfig)
        	%This is used to set the user configuration, which contain everything
        	%not defined by the test, like colors, linewidth, etc. There is only
        	%one UserConfig for all results used and some fields will always be 
        	%submitted, no matter if the plot function is using it:
        	%	Paperboy:	if true the resulting graphic will be used
        	%			in a print, hence lines should be a bit thicker
        	%			and fonts a bit larger, but it is up to the plot
        	%			function what it should do
        	%Don't if there are more	
        	
        	
        	UserConfig = getUserConfig(obj)
        	%should return the current user configuration, mostly needed to get
        	%the default values
        	
        	
        	FeatureList = getFeatures(obj)
        	%This will be used by the TestSuite to determine what the model can 
        	%be used for. The output should by structure with specific fieldnames
        	%with mostly boolean values. 
        	%Following keywords are available:
        	%	MultiplePlot:		if true, the plot produces more than one
        	%				plot for a single test, example sign. matrix
        	%				plot function, which plots two plot "per test"
        	%	No_Plots:		only needed if MultiplePlot is true, defines
        	%				how many plots are produced
        	%	Uncertain_mean:		Defines if the plot is able to plot Uncertainties
        	%				with a mean value and variance (true if possible)
        	%	Uncertain_median:	Defines if the plot is able to plot Uncertainties
        	%				with a median Q16 and Q84 (true if possible)
        	%	MultiTest:		True if more than one test per plot is allowed.
        	%				If MultiTest is supported, TestConfig and Results will
        	%				be submitted as cell array no matter if only one result 
        	%				is used.
        	%	SingleTest:		True if it is possible to plot only one test per
        	%				plot
        	
        	
        	PossibleParameters = getUserParameterSetting(obj)
        	%The method should return a description of the parameters needed
        	%in the UserConfig and should also include information about the way
        	%the gui for setting the parameters should be designed.
        	%The output has to be a structure with the field names being the 
        	%name of the parameters which should be used for ConfigureTest
        	%Each entry in structure should be another structure with following
        	%Often the Plots do not need any configuration in this case the 
        	%PossibleParameters can return empty
        	
        	
        	%keywords:
        	%	Type: 		can be 	'check' for boolean,
        	%				'edit' for any value or small text
        	%				'list' for a selection of values specified
        	%					by Items
        	%				'text' for longer string boxes (e.g. Comments)
        	%
        	%	Format:		Defines the data type of value only applyable with
        	%			'edit'. It can be the following: 'string','float',
        	%			'integer','file','dir'	
        	%	ValLimit: 	The limit a value can have in a edit box, if left
        	%			empty it will set to [-9999 9999].
        	%	Name:		Sets the Name used for the value in the GUI, if 
        	%			left empty the variable name will be used instead
        	%	Items:		Only needed in case of 'list', there it has to be 
        	%			cell array with strings. The value returned will
        	%			the selected string.
        	%			
        	%			
        	%	DefaultValue:	Sets the value used as default value in the GUI.
        	%			If missing, 0 will be used as default or 1 for 
        	%			lists
        	%	Style:		Only applyable for the Type 'list', it sets the 
        	%			GUI element used to represent the list and it can
        	%			be:	
        	%				'listbox': a scrollable list for the selection
        	%				'popupmenu': a popup list 
        	%				'radiobutton': a block of checkerboxes
        	%				'togglebutton': a selection of buttons
        	%			If missing 'popupmenu' will be used. The last
        	%			two options are only suggest if not many elements
        	%			are in the list.
        	
        	
        	
        	SetPlotAxis(obj,plotAxis)
        	%sets the axis used for plotting
        	
        	
        	AddTestResults(obj,Results)
        	%Used to submit the results of the tests, often more than one test is supported
        	%in this case Results should be a cell array of the same size as TestConfig
        	%(e.g. {Result1,Result2,Result3,...}
        	
        	
        	PlotResults(obj)
        	%Will be used to start the plotting. The Handles of the different plot parts
        	%(lines,points, etc.) can stored in PlotHandles
        	
        	
        	
        end

      
        
         methods 
        	%useful methods 
        	function PlotHandle = PlotIt(obj,plotAxis,Results,TestConfig,UserConfig)
        		%just and all in one config and plot shortcut, normally it will
        		%be enough to the following two steps to plot: 1.create object
        		%2. call PlotIt with all needed data, to plot a test result
        		
        		%this function will be used by the plotmaker, so it should be
        		%changed if something additional has to be done.
        		
        		obj.ConfigureTestConfig(TestConfig);
        		obj.ConfigureUserConfig(UserConfig);
        		obj.AddTestResults(Results);
        		obj.SetPlotAxis(plotAxis);
        		obj.PlotReady=true;
        		
        		iPlot=tic;
        		obj.PlotResults;
        		obj.CPUTimeData=toc(iPlot);
        		
        		
        		PlotHandle=obj.PlotHandles;
        		
        		
        	end
        	
        end
        
end	

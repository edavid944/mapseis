function plotSummaryScore(TestResObj,TestNames,Models,TheTitle,PlotModName)
	%plots the summary of the likelihood tests into a combined likelhood tests 
	%It is a quick and dirty version and will be replaced by the already started
	%plotwriter later.
		
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.forecast_testing.PlotPlugIns.*;
	import mapseis.util.*;
	

	%I think this is it already... FINISHED
	

	
	%check if Summary exists
	if isempty(TestResObj.SummaryResults)
		error('No Summary in this Result object.')
	end
	
	%no get it 
	SummaryRes=TestResObj.SummaryResults;
	
	%get the possible plots as well (needed for config)
	possiblePlots=TestResObj.possiblePlots;
	
	
	
	%generate user config for the plot
        DefaultUseConf=struct(	'histColor',[0.88 0.88 0.88],...
        			'orgColor','r',...
        			'meanColor','m',...
        			'medianColor','g',...
        			'plotOrg',true,...
        			'plotMean',false,...
        			'plotMedian',false,...
        			'ShowScoreDist',true,...
        			'NtestMode',true,...
        			'PlotTitle',[[]],...
        			'SingleForecast',true,...
        			'PaperBoy',true,...
        			'RoundIt',false);
	
	
	
	%get the configs for the different tests
	TheConfig=cell(size(TestNames));
	for cC=1:numel(TestNames)
		PosInConf=strcmp(possiblePlots.(TestNames{cC})(:,2),'CombinedLike');
		TheConfig{cC}=possiblePlots.(TestNames{cC}){PosInConf,3};
		TheConfig{cC}.ForecastTime='Summary';
		
	end
	
	
	%build plot object
	ThePlotter=CombinedLike();
	
	for mC=1:numel(Models)
		%build UserConfig from the template
		UserConfig=DefaultUseConf;
		UserConfig.PlotTitle= [TheTitle,', ',PlotModName{mC}];
		
		%open new window
		fig=figure;
		plotAxis=gca;
		
		
		TestConfig=TheConfig;
		Results=cell(size(TestNames));
		%update config and get data
		for cC=1:numel(TestNames)
			TestConfig{cC}.ForecastName=PlotModName{mC};	
			Results{cC}=SummaryRes.(TestNames{cC}).(Models{mC});
			
		end
	
	
		PlotHandle = ThePlotter.PlotIt(plotAxis,Results,TestConfig,UserConfig);
		
		
	
	
	end
	
	
        			
        
        			
	
	
	
	
	


end

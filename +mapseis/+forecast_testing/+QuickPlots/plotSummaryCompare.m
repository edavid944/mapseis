function plotSummaryCompare(TestResObj,TestNames,Models,TheTitle,PlotModName)
	

	%FINISHED BUT UNTESTED
	%plots the summary of the comparison tests into two different plots:
	%a TR-ranking plot and a sign. matrix plot
		
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.forecast_testing.PlotPlugIns.*;
	import mapseis.util.*;
	

	%check if Summary exists
	if isempty(TestResObj.SummaryResults)
		error('No Summary in this Result object.')
	end
	
	%know get it 
	SummaryRes=TestResObj.SummaryResults;
	
	%get the possible plots as well (needed for config)
	possiblePlots=TestResObj.possiblePlots;
	
	
	
	
	
	%generate user config for the plot
	ConfRanking = struct(	'greyColor',[[0.85 0.85 0.85]],...
              			'orgColor','r',...
              			'meanColor','m',...
              			'medianColor','g',...
              			'plotOrg',true,...
              			'plotMean',false,...
              			'plotMedian',false,...
              			'plotUniLine',true,...
              			'plotMaxGain',true,...
              			'WideBar',true,...
              			'BarTrail',true,...
              			'DisplayNames',{PlotModName},...
              			'usePercent',false,...
              			'PlotTitle',TheTitle,...
              			'PaperBoy',true);
	
	
	
	
	ConfSignMat = struct(	'Tsign',true,...
				'Wsign',true,...
				'ShowPNr',true,...
				'PlotNames',{PlotModName},...
				'PlotTitle',TheTitle,...
				'PaperBoy',true);
	
	
	
	%get the configs for the different tests (Ranking)
	TheConfigRank=cell(size(Models));
	RankResults=cell(size(Models));
	for cC=1:numel(Models)
		PosInConf=strcmp(possiblePlots.(TestNames{1})(:,2),'Ranking');
		TheConfigRank{cC}=possiblePlots.(TestNames{1}){PosInConf,3};
		TheConfigRank{cC}.ModelNames=Models;
		TheConfigRank{cC}.ForecastName=Models{cC};
		TheConfigRank{cC}.ForecastTime='Summary';
		
		%get data
		RankResults{cC}=SummaryRes.(TestNames{1}).(Models{cC});
		
	end
	
	%get config for Ttest and Wtest
	PosInConf=strcmp(possiblePlots.(TestNames{2})(:,2),'SignMatrix');
	TheConfigSign{1}=possiblePlots.(TestNames{2}){PosInConf,3};
	TheConfigSign{1}.ModelNames=Models;
	TheConfigSign{1}.ForecastName=Models;
	TheConfigSign{1}.ForecastTime='Summary';
	
	PosInConf=strcmp(possiblePlots.(TestNames{3})(:,2),'SignMatrix');
	TheConfigSign{2}=possiblePlots.(TestNames{3}){PosInConf,3};
	TheConfigSign{2}.ModelNames=Models;
	TheConfigSign{2}.ForecastName=Models;
	TheConfigSign{2}.ForecastTime='Summary';
	
	%get data
	SignResults{1}=SummaryRes.(TestNames{2});
	SignResults{2}=SummaryRes.(TestNames{3});
	
	
	%build theRank plot object
	TheRankPlotter=InfoRanking();
	
	%build the SignMatrix plot object
	TheSignPlotter=SignMatCompare();
	
	%open new window and plot
	fig=figure;
	plotAxis=gca;
	PlotHandle{1} = TheRankPlotter.PlotIt(plotAxis,RankResults,TheConfigRank,ConfRanking);
	
	fig=figure;
	plotAxis=gca;
	ConfRanking.usePercent=true;
	PlotHandle{2} = TheRankPlotter.PlotIt(plotAxis,RankResults,TheConfigRank,ConfRanking);
	
	
	%open new window and plot
	fig=figure;
	plotAxis=gca;
	PlotHandle{3} = TheSignPlotter.PlotIt(plotAxis,SignResults,TheConfigSign,ConfSignMat);
	
	
	
        			
        
        			
	
	
	
	
	


end

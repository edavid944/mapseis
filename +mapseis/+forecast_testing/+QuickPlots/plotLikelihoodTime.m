function plotLikelihoodTime(TestResObj,TestNames,BasicFieldCat,BasicFieldMod,Models,TheTitle,PlotModName,CumNumMode)
	%a limited tool which allows to plot score over time for selected tests
	%only meant as a temporary file and quick overview
	
	%EqTimeData: should be a cell array with {1} num. Eq  and {2} TimeStep
	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	if ~iscell(TestNames)
		TestNames={TestNames};
	end
	
	if ~iscell(BasicFieldCat)
		BasicFieldCat={BasicFieldCat};
	end
	
	if ~iscell(BasicFieldMod)
		BasicFieldMod={BasicFieldMod};
	end
	
	if ~iscell(Models)
		Models={Models};
	end
	
	if ~iscell(TheTitle)
		TheTitle={TheTitle};
	end
	

	if isempty(PlotModName)
		PlotModName=Models;
	end	
		
	
	
	
	numModels=numel(Models);
	
	
	%needed later
	wheelColor={'b','g','k','c','m'}
	%wheelColor{mod(existPlots,6)+1}
	
	for tC=1:numel(TestNames)
		
		%init graphic	
		fig=figure;
		numPlots=numModels;
	
		for mC=1:numModels
			
			%get model data
			[TestRes, TimeVec] = TestResExtractor(TestResObj,TestNames{tC},Models{mC},BasicFieldMod{tC});
			YearDec = DateNum2DecYear(TimeVec);
			
			%get cat data
			[CatRes, TimeVec] = TestResExtractor(TestResObj,TestNames{tC},Models{mC},BasicFieldCat{tC});
			
			%calculate accepatble model likelihood
			if strcmp(TestResObj.Results{1}.(TestNames{tC}).(Models{mC}).TestName,'Ntest');
				TopSign=TestResObj.Results{1}.(TestNames{tC}).(Models{mC}).SignVal/2;
				LowSign=TestResObj.Results{1}.(TestNames{tC}).(Models{mC}).SignVal/2;
			else
				TopSign=0;
				LowSign=TestResObj.Results{1}.(TestNames{tC}).(Models{mC}).SignVal;			
			end
			
			for cC=1:numel(TestRes)
				minVal=min(TestRes{cC});
				maxVal=max(TestRes{cC});
				LowModLike(cC)=minVal*(1+LowSign);
				HighModLike(cC)=maxVal*(1-TopSign);
				
			end
			
			
			%plot
			subplot(numPlots,1,mC);
			
			if CumNumMode
				LowModLike(isnan(LowModLike))=0;	
				HighModLike(isnan(HighModLike))=0;	
				CatRes(isnan(CatRes))=0;	
								
				curSTD(1)=plot(YearDec,cumsum(LowModLike),wheelColor{mod(mC,5)+1});
				hold on;
				curSTD(2)=plot(YearDec,cumsum(HighModLike),wheelColor{mod(mC,5)+1});
				eqHand=plot(YearDec,cumsum(CatRes),'r');
				
			
				yla=ylabel('cum. Log-Likelihood');
				xla=xlabel('');
			
			
			else	
								
				curSTD(1)=plot(YearDec,LowModLike,wheelColor{mod(mC,5)+1});
				hold on;
				curSTD(2)=plot(YearDec,HighModLike,wheelColor{mod(mC,5)+1});
				eqHand=plot(YearDec,CatRes,'r');
				
			
				yla=ylabel('Log-Likelihood');
				xla=xlabel('');
			end
			
			set(gca,'LineWidth',3,'Box','on','FontSize',16);
			set([xla,yla],'FontSize',16,'FontWeight','bold');
			set([eqHand],'LineWidth',2);
			set(curSTD,'LineWidth',1.5,'LineStyle','--');
			legend([curSTD(1),eqHand],{PlotModName{mC},'Observed EQ'});
			
		end	
		
		%last label
		xla=xlabel('Time');
		set([xla],'FontSize',16,'FontWeight','bold');
		
		%set title
		subplot(numPlots,1,1);
		tit1=title(TheTitle{tC});
		set(tit1,'FontSize',24,'FontWeight','bold');
	
	end
	
	
	
	
	


end

function plotSummaryCompare_Vertical(TestResObj,TestNames,Models,TheTitle,PlotModName)
	
	%Similar to plotSummaryCompare but it only plots the infogain, allows
	%"unrelated" forecasts and plots everything vertical instead of horizontal
		
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.forecast_testing.PlotPlugIns.*;
	import mapseis.util.*;
	

	if ~iscell(TestResObj)
		TestResObj={TestResObj};
	end
	
	%check if Summary exists
	if isempty(TestResObj{1}.SummaryResults)
		error('No Summary in this Result object.')
	end
	
	%know get it 
	for rC=1:numel(TestResObj)
		SummaryRes{rC}=TestResObj{rC}.SummaryResults;
		possiblePlots{rC}=TestResObj{rC}.possiblePlots;
	end
	
	%get the possible plots as well (needed for config)
	
	
	
	
	
	
	%generate user config for the plot
	ConfRanking = struct(	'greyColor',[[0.85 0.85 0.85]],...
              			'orgColor','r',...
              			'meanColor','m',...
              			'medianColor','g',...
              			'VerticalMode',true,...
              			'plotOrg',true,...
              			'plotMean',false,...
              			'plotMedian',false,...
              			'plotUniLine',true,...
              			'plotMaxGain',true,...
              			'WideBar',true,...
              			'BarTrail',true,...
              			'DisplayNames',{PlotModName},...
              			'usePercent',false,...
              			'PlotTitle',TheTitle,...
              			'PaperBoy',true);
	
	
	
	
	
	%get the configs for the different tests (Ranking)
	%TheConfigRank=cell(size(Models));
	%RankResults=cell(size(Models));
	RankResults={};
	TheConfigRank={};
	FullModelList={};
	FullPlotModel={};
	InfGain=[];
	PercGain=[];
	
	count=1;
	for rC=1:numel(TestResObj)
		for cC=1:numel(Models{rC})
			PosInConf=strcmp(possiblePlots{rC}.(TestNames)(:,2),'Ranking');
			TheConfigRank{count}=possiblePlots{rC}.(TestNames){PosInConf,3};
			TheConfigRank{count}.ModelNames=Models{rC};
			TheConfigRank{count}.ForecastName=Models{rC}{cC};
			TheConfigRank{count}.ForecastTime='Summary';
			
			%get data
			RankResults{count}=SummaryRes{rC}.(TestNames).(Models{rC}{cC});
			
			%the whole name and value thing
			FullModelList{count}=Models{rC}{cC};
			FullPlotModel{count}=PlotModName{rC}{cC};
			
			try	
				InfGain(count)=RankResults{count}.InUni;
				PercGain(count)=RankResults{count}.PercentGain;
			catch
				InfGain(count)=NaN;
				PercGain(count)=NaN;
			end
			
			count=count+1;
			
		end
	end
	
	
	%build theRank plot object
	TheRankPlotter=InfoRanking();
	
	%sort for max gain
	if ~isnan(InfGain);
		[temp idxGain]=sort(InfGain);
		[temp idxPerc]=sort(PercGain);
	else
		idxGain=(1:numel(InfGain))';
		idxPerc=(1:numel(PercGain))';
	end
	
	ConfRanking.DisplayNames=FullPlotModel(idxGain);
	%open new window and plot
	fig=figure;
	plotAxis=gca;
	PlotHandle{1} = TheRankPlotter.PlotIt(plotAxis,RankResults(idxGain),TheConfigRank(idxGain),ConfRanking);
	
	fig=figure;
	plotAxis=gca;
	ConfRanking.DisplayNames=FullPlotModel(idxPerc);
	ConfRanking.usePercent=true;
	PlotHandle{2} = TheRankPlotter.PlotIt(plotAxis,RankResults(idxPerc),TheConfigRank(idxPerc),ConfRanking);
	
	
	%open new window and plot
	%fig=figure;
	%plotAxis=gca;
	%PlotHandle{3} = TheSignPlotter.PlotIt(plotAxis,SignResults,TheConfigSign,ConfSignMat);
	
	
	
        			
        
        			
	
	
	
	
	


end

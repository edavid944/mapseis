function [TestRes, TimeVec] = TestResExtractor(TestResObj,TestName,ModelName,TheField)
    %just a short script to extract a single field from a test with
    %multiple times into on array. Should not be needed later, as there
    %should be better options available later.
    
    TestRes=[];
    TimeVec=[];
    
    TheRes = TestResObj.Results;
    NumTimeStep = numel(TheRes);

    %get type of the field
    try
        TestVal = TheRes{1}.(TestName).(ModelName).(TheField); 
        
               
    catch err
        %try searching other timesteps
        TestVal=[];
        for i=1:NumTimeStep
            try
                TestVal = TheRes{i}.(TestName).(ModelName).(TheField);
                break
            catch
                 %disp(i)
            end    
        end
        
        if isempty(TestVal)
            error('Test, Model or Field not existing');
        end
    end
    
    Temp=whos('TestVal');
	FieldType=Temp.class;
    
    switch FieldType
		case {'double','logical','integer','numeric'}
                        if all(size(TestVal)==1)
                            
                            TestRes = NaN(size(TheRes));
                            TimeVec = NaN(size(TheRes));
                            
                            for i=1:NumTimeStep
                                CurRes = TheRes{i};
                                try
                                    TimeVec(i) = CurRes.ForecastTime;
                                    TestRes(i) = CurRes.(TestName).(ModelName).(TheField);
                                   
                                catch noErr
                                    disp('Some result not found');
                                end    
                            end 
                        
                        else
                             
                            TestRes = cell(size(TheRes));
                             TimeVec = NaN(size(TheRes));
                             
                            for i=1:NumTimeStep
                                CurRes = TheRes{i};
                                try
                                    TimeVec(i) = CurRes.ForecastTime;
                                    TestRes{i} = CurRes.(TestName).(ModelName).(TheField);
                                    
                                catch noErr
                                    disp('Some result not found');
                                end        
                            end 
                        
                            
                        end
					
		case {'string','char','cell','struct','(unassigned)'}
                        TestRes = cell(size(TheRes));
                        TimeVec = NaN(size(TheRes));
                        
                        for i=1:NumTimeStep
                            CurRes = TheRes{i};
                            try
                                TestRes{i} = CurRes.(TestName).(ModelName).(TheField);
                                TimeVec(i) = CurRes.ForecastTime;
                            catch noErr
                                disp('Some result not found');
                            end        
                        end 
					
										
					
		otherwise	
			disp('Type not supported in Compare')
			return;
				
    end
    
    
    

end
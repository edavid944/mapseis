function plotEqDayBars(TestResObj,TestName,TestField,TrTestField,Models,TheTitle,PlotModName,ColMod)
	%Plots a value for days with earthquakes as a bar plot, ideal for information gain
	%the graphics may have to be post processed.
	
	
	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	
	if ~iscell(Models)
		Models={Models};
	end
	


	if isempty(PlotModName)
		PlotModName=Models;
	end	
		
	
		numModels=numel(Models);
	
	
	%needed later
	axPos=[0.1300,0.2840,0.7750,0.6410];
	HiPos=[100,100,1135,700];
		
	%get year data
	[TotalNum, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{1},'TotalNum');
	YearDec = DateNum2DecYear(TimeVec);	
	
	%days with Eqs
	DaysIn=TotalNum~=0&~isnan(TotalNum);
	
	
	%process data
	for mC=1:numel(Models)
		[TheData, TimeVec] = TestResExtractor(TestResObj,TestName,Models{mC},TestField);
		PlotMat(mC,:)=TheData(DaysIn)';
	
	end
	
	minAx=0;
	maxAx=sum(DaysIn);
	bonusDist=0.01*(maxAx-minAx);
	xlimiter=[minAx,maxAx+1];
	
	
	%plot it
	fig=figure;
	set(fig,'Pos',HiPos);
	
	handle = bar(PlotMat','hist');
	
	hold on
	%plot zero line
	zlinhand=plot([minAx,maxAx+1],[0,0],'k');
	set(zlinhand,'LineStyle','--','LineWidth',1);
	hold off
	
	set(handle,'EdgeColor','w');
	if ~isempty(ColMod)
		for mC=1:numel(Models)
			set(handle(mC),'FaceColor',ColMod{mC});		
		end
	end
	
	xlim(xlimiter);
	
	%format axis
	set(gca,'LineWidth',5,'Box','on','FontSize',24);
	
	%get the time into the axis
	set(gca,'Xtick',1:maxAx,'XTickLabel',datestr(TimeVec(DaysIn)));
	set(gca,'Pos',axPos);
	%rotate
	texhan=rotateticklabel(gca,45);
	set(texhan,'FontSize',24);
	
	leghand=legend(PlotModName);
	

	tit1=title(TheTitle);
	set(tit1,'FontSize',24,'FontWeight','bold');
	
	
		
	hold off
	
end
	


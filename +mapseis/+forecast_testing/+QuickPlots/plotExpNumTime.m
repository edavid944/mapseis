function plotExpNumTime(TestResObj,TrTestField,Models,TheTitle,PlotModName,SinglePlot,CumNumMode,plotError)
	%a limited tool which allows to plot expected number over time different models 
	%together with the observed number
	%only meant as a temporary file and quick overview
	
	
	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	
	if ~iscell(Models)
		Models={Models};
	end
	


	if isempty(PlotModName)
		PlotModName=Models;
	end	
		
	ErrorType=1;
	if plotError==2
		ErrorType=2;
		plotError=true;
		
	end
	
	
	
	numModels=numel(Models);
	
	
	%needed later
	wheelColor={'b','g','k','c','m'}
	%wheelColor{mod(existPlots,6)+1}
	
	
	%get year data
	[TotalNum, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{1},'TotalNum');
	YearDec = DateNum2DecYear(TimeVec);	
	%init graphic	
	fig=figure;
	
	if ~SinglePlot
		numPlots=numModels+1;
	
		%plot Eq
		subplot(numPlots,1,1);
		if CumNumMode
			TotalNum(isnan(TotalNum))=0;	
			eqHand=plot(YearDec,cumsum(TotalNum),'r');
			yla=ylabel('Cum. Num. Eq');
		else
			eqHand=plot(YearDec,TotalNum,'r');
			yla=ylabel('Num. Eq');
		end
		
		
		xla=xlabel('');
		set(gca,'LineWidth',3,'Box','on','FontSize',16);
		set([xla,yla],'FontSize',16,'FontWeight','bold');
		set(eqHand,'LineWidth',2);
		legend('Observed EQ');
		
	else
		if CumNumMode
			TotalNum(isnan(TotalNum))=0;
			eqHand=plot(YearDec,cumsum(TotalNum),'r');
			yla=ylabel('Cum. Num. Eq / Exp. Num');
		else
			eqHand=plot(YearDec,TotalNum,'r');
			yla=ylabel('Num. Eq / Exp. Num');
		end
		
		
		xla=xlabel('Time');
		set(gca,'LineWidth',3,'Box','on','FontSize',16);
		set([xla,yla],'FontSize',16,'FontWeight','bold');
		set(eqHand,'LineWidth',2);
		legendHand=eqHand;
		legendText={'Observed EQ'};
		hold on
	
	end	

	tit1=title(TheTitle);
	set(tit1,'FontSize',24,'FontWeight','bold');
	
	
	
	for mC=1:numModels
		%get data
		[TestRes, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{mC},'ExpVal');
		YearDec = DateNum2DecYear(TimeVec);
		
		if ErrorType==1
			STDMinus=TestRes-(TestRes);
			STDPlus=TestRes+(TestRes);
			%STDMinus=poissinv(0.025,TestRes);
			%STDPlus=poissinv(0.975,TestRes);
			
		elseif ErrorType==2
			%negative binomial variance
			%UNFINISHED

		end		

		
		if ~SinglePlot 
			%plot
			subplot(numPlots,1,1+mC);
			if CumNumMode
				TestRes(isnan(TestRes))=0;	
				STDMinus(isnan(STDMinus))=0;
				STDPlus(isnan(STDPlus))=0;
				
				curHand=plot(YearDec,cumsum(TestRes),wheelColor{mod(mC,5)+1});	
				hold on
				yla=ylabel(' cum. Exp Value');
				xla=xlabel('');
				
				if plotError
					curSTD(1)=plot(YearDec,cumsum(STDMinus),wheelColor{mod(mC,5)+1});
					curSTD(2)=plot(YearDec,cumsum(STDPlus),wheelColor{mod(mC,5)+1});
					
					set(curSTD,'LineWidth',1.5,'LineStyle','--');
					legend(curHand,PlotModName{mC});
				else
					legend(curHand,PlotModName{mC});
				end
				
				
			else
				curHand=plot(YearDec,TestRes,wheelColor{mod(mC,5)+1});
				hold on
				yla=ylabel('Exp Value');
				xla=xlabel('');
				
				if plotError
					curSTD(1)=plot(YearDec,STDMinus,wheelColor{mod(mC,5)+1});
					curSTD(2)=plot(YearDec,STDPlus,wheelColor{mod(mC,5)+1});
					set(curSTD,'LineWidth',1.5,'LineStyle','--');
					
					legend(curHand,PlotModName{mC});
				else
					legend(curHand,PlotModName{mC});
				end
			
			end
		
			set(gca,'LineWidth',3,'Box','on','FontSize',16);
			set([xla,yla],'FontSize',16,'FontWeight','bold');
			set([curHand],'LineWidth',2);
			hold off
		else
			if CumNumMode
				TestRes(isnan(TestRes))=0;	
				STDMinus(isnan(STDMinus))=0;
				STDPlus(isnan(STDPlus))=0;
				
				curHand=plot(YearDec,cumsum(TestRes),wheelColor{mod(mC,5)+1});	
				
				
				if plotError
					curSTD(1)=plot(YearDec,cumsum(STDMinus),wheelColor{mod(mC,5)+1});
					curSTD(2)=plot(YearDec,cumsum(STDPlus),wheelColor{mod(mC,5)+1});
					set(curSTD,'LineWidth',1.5,'LineStyle','--');
					legendHand(end+1)=curHand;
					legendText{end+1}=PlotModName{mC};
					
				else
					legendHand(end+1)=curHand;
					legendText{end+1}=PlotModName{mC};
				
				end
				
				
			else
				curHand=plot(YearDec,TestRes,wheelColor{mod(mC,5)+1});	
				
				
				if plotError
					curSTD(1)=plot(YearDec,STDMinus,wheelColor{mod(mC,5)+1});
					curSTD(2)=plot(YearDec,STDPlus,wheelColor{mod(mC,5)+1});
					set(curSTD,'LineWidth',1.5,'LineStyle','--');
					legendHand(end+1)=curHand;
					legendText{end+1}=PlotModName{mC};
				else
					legendHand(end+1)=curHand;
					legendText{end+1}=PlotModName{mC};
				end
			
			end
		
			set([curHand],'LineWidth',2);
		
		end
	end	
		
	
	
	if ~SinglePlot 
		%last label
		xla=xlabel('Time');
		set([xla],'FontSize',16,'FontWeight','bold');
	else
		set(gca,'LineWidth',3,'Box','on','FontSize',16);
		legend(legendHand,legendText);
		

	end	
		
	hold off
	
end
	


function plotSummaryTtest(TestResObj,TTestName,Models,PlotModName,ExModel,PlotLayout)
	%plots the result of a Comparison T-test into a figure with differnent 
	%subplots
	

	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	
	FigPos=[200 200 1024 1024];
	
	%makes life easier
	SummaryRes=TestResObj.SummaryResults;
	
	%build refernce list
	InIt=~strcmp(Models,ExModel);
	RefModels=Models(InIt);
	RefModelLabel=PlotModName(InIt);
	
	
	%get the data & plot	
	fig=figure
	set(fig,'Pos',FigPos);
	for rC=1:numel(RefModels)
		plotAxis=subplot(PlotLayout(1),PlotLayout(2),rC);
		
		
		%collect data
		count=1;
		SingleModName={};
		InfoGain=[];
		InfoConf=[];
		
		for mC=1:numel(Models)
			if ~strcmp(RefModels{rC},Models{mC})
				NormName=[RefModels{rC},'_',Models{mC}];
				RevName=[Models{mC},'_',RefModels{rC}];
				
				try
					TRes=SummaryRes.(TTestName).(NormName);
					MFactor=1;
				catch
					disp('reverse name Ttest');
					TRes=SummaryRes.(TTestName).(RevName);
					MFactor=-1;
				end
				
				SingleModName(count)=PlotModName(mC);
				InfoGain(count)=MFactor*TRes.InAB;
				InfoConf(count)=TRes.Conf_InAB;
				count=count+1;
			end
			
	
		end
		
		%plot
		handles = plotSingleTtest(plotAxis,InfoGain,InfoConf,SingleModName,RefModelLabel{rC});
	end
	
	
	
	
	
	
	
	
end
	


function plotScoreTime(TestResObj,TestNames,BasicField,Models,EqTimeData,TheTitle,PlotModName)
	%a limited tool which allows to plot score over time for selected tests
	%only meant as a temporary file and quick overview
	
	%EqTimeData: should be a cell array with {1} num. Eq  and {2} TimeStep
	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	if ~iscell(TestNames)
		TestNames={TestNames};
	end
	
	if ~iscell(BasicField)
		BasicField={BasicField};
	end
	
	if ~iscell(Models)
		Models={Models};
	end
	
	if ~iscell(TheTitle)
		TheTitle={TheTitle};
	end
	
	if nargin<7
		PlotModName=[];
	end

	if isempty(PlotModName)
		PlotModName=Models;
	end	
		
	
	
	
	numModels=numel(Models);
	
	
	%get YearData
	TotalNum=EqTimeData{1};
	YearDecCat=EqTimeData{2};
	
	%needed later
	wheelColor={'b','g','k','c','m','r'}
	%wheelColor{mod(existPlots,6)+1}
	
	for tC=1:numel(TestNames)
		
		%init graphic	
		fig=figure;
		numPlots=numModels+1;
	
		%plot Eq
		subplot(numPlots,1,1);
		eqHand=plot(YearDecCat,TotalNum,'r');
		yla=ylabel('Num. Eq');
		xla=xlabel('');
		set(gca,'LineWidth',3,'Box','on','FontSize',16);
		set([xla,yla],'FontSize',16,'FontWeight','bold');
		set(eqHand,'LineWidth',2);
		legend('Observed EQ');
		
		tit1=title(TheTitle{tC});
		set(tit1,'FontSize',24,'FontWeight','bold');
		
		for mC=1:numModels
			%get data
			[TestRes, TimeVec] = TestResExtractor(TestResObj,TestNames{tC},Models{mC},BasicField{tC});
			YearDec = DateNum2DecYear(TimeVec);
			%plot
			subplot(numPlots,1,1+mC);
			curHand=plot(YearDec,TestRes,wheelColor{mod(mC,6)+1});	
			ylim([0 1]);
			yla=ylabel('% Score');
			xla=xlabel('');
			set(gca,'LineWidth',3,'Box','on','FontSize',16);
			set([xla,yla],'FontSize',16,'FontWeight','bold');
			set([curHand],'LineWidth',2);
			legend(PlotModName{mC});
			
		end	
		
		%last label
		xla=xlabel('Time');
		set([xla],'FontSize',16,'FontWeight','bold');
		
		
	
	
	end
	
	
	
	
	


end

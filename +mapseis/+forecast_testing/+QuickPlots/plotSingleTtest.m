function handles = plotSingleTtest(plotAxis,InfoGain,InfoConf,PlotModName,RefModelName)
	%plots the result of a single T-test into a given subplot (plotAxis) is meant as temporary solution
	

	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	
	%Some parameters
	halfWidth=0.5;
	Spacer=2;
	
	ZLineCol='k';
	ZLineStyle='--';
	ZLineWidth=1;
	
	BarCol=[0.7 0.7 0.7];
	BarStyle='-';
	BarWidth=2;
	
	ValCol='r';
	ValStyle='-';
	ValWidth=3;
	
	ConfCol='b';
	ConfStyle='-';
	ConfWidth=5;
	
	TitFontSize=16;
	TitFontWeight='bold';
	
	AxisFontSize=14;
	AxisFontWeight='bold';
	AxisLineWidth=4;
	
	LabelFontSize=14;
	LabelFontWeight='bold';
	
	%build y-vector
	Ypos=1:Spacer:2*numel(InfoGain);
	
	
	
	
	%plot it
	%-------
	%zero Line first
	handles{1}=plot([0 0],[0 Ypos(end)+1])
	set(handles{1},'Color',ZLineCol,'LineStyle',ZLineStyle,'LineWidth',ZLineWidth);
	hold on
	
	%now the single bars
	for pC=1:numel(InfoGain)	
		TempHand=[];
		
		%conf. interval
		ConfIval=[InfoGain(pC)-InfoConf(pC),InfoGain(pC)+InfoConf(pC)];
		
		%MidLine Y-Range
		MidY=[Ypos(pC)-(halfWidth/2),Ypos(pC)+(halfWidth/2)];
		
		%ConfLine Y-Range
		ConY=[Ypos(pC)-(halfWidth),Ypos(pC)+(halfWidth)];
		
		%bar
		TempHand(1)=plot(ConfIval,[Ypos(pC),Ypos(pC)]);
		set(TempHand(1),'Color',BarCol,'LineStyle',BarStyle,'LineWidth',BarWidth);
		
		%mid
		TempHand(2)=plot([InfoGain(pC),InfoGain(pC)],MidY);
		set(TempHand(2),'Color',ValCol,'LineStyle',ValStyle,'LineWidth',ValWidth);
		
		%conf
		TempHand(3)=plot([ConfIval(1),ConfIval(1)],ConY);
		set(TempHand(3),'Color',ConfCol,'LineStyle',ConfStyle,'LineWidth',ConfWidth);
		TempHand(4)=plot([ConfIval(2),ConfIval(2)],ConY);
		set(TempHand(4),'Color',ConfCol,'LineStyle',ConfStyle,'LineWidth',ConfWidth);
		
		%store handles for re-use
		handles{pC+1}=TempHand;
	end
	
	
	%make title
	theTit=title(['Reference: ',RefModelName]);
	set(theTit,'FontSize',TitFontSize,'FontWeight',TitFontWeight);
	handles{end+1}=theTit;
	
	%set y-ticks
	set(plotAxis,'YTick',Ypos,'YTickLabel',PlotModName);
	
	
	%xlabel
	xlab=xlabel('Information Gain');
	set(xlab,'FontSize',LabelFontSize,'FontWeight',LabelFontWeight);
	handles{end+1}=xlab;
	
	%format the axis
	set(plotAxis,'LineWidth',AxisLineWidth,'Box','on','FontSize',AxisFontSize,'FontWeight',AxisFontWeight);
	

	
		
	hold off
	
end
	


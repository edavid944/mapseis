function plotInfoGainTime(TestResObj,TrTestField,Models,TheTitle,PlotModName,SinglePlot,CorrValue,CumNumMode)
	%a limited tool which allows to plot Information Gain over time different models 
	%together with the observed number
	%only meant as a temporary file and quick overview
	
	
	
	
	import mapseis.forecast_testing.QuickPlots.*;
	import mapseis.util.*;
	
	
	if ~iscell(Models)
		Models={Models};
	end
	


	if isempty(PlotModName)
		PlotModName=Models;
	end	
		
	
	EqNumPlot=false;
	
	
	numModels=numel(Models);
	
	
	%needed later
	wheelColor={'b','g','k','c','m'}
	%wheelColor{mod(existPlots,6)+1}
	
	
	%get year data
	[TotalNum, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{1},'TotalNum');
	YearDec = DateNum2DecYear(TimeVec);	
	%init graphic	
	fig=figure;
	
	if ~SinglePlot
		numPlots=numModels+1;
	
		%plot Eq
		subplot(numPlots,1,1);
		if CumNumMode
			TotalNum(isnan(TotalNum))=0;	
			eqHand=plot(YearDec,cumsum(TotalNum),'r');
			yla=ylabel('Cum. Num. Eq');
		else
			eqHand=plot(YearDec,TotalNum,'r');
			yla=ylabel('Num. Eq');
		end
		
		
		xla=xlabel('');
		set(gca,'LineWidth',3,'Box','on','FontSize',16);
		set([xla,yla],'FontSize',16,'FontWeight','bold');
		set(eqHand,'LineWidth',2);
		legend('Observed EQ');
		
	else	if EqNumPlot
			if CumNumMode
				TotalNum(isnan(TotalNum))=0;
				eqHand=plot(YearDec,cumsum(TotalNum),'r');
				yla=ylabel('Cum. Num. Eq / cum. Info. Gain');
			else
				eqHand=plot(YearDec,TotalNum,'r');
				yla=ylabel('Num. Eq / Info. Gain');
			end
			
			
			xla=xlabel('Time');
			set(gca,'LineWidth',3,'Box','on','FontSize',16);
			set([xla,yla],'FontSize',16,'FontWeight','bold');
			set(eqHand,'LineWidth',2);
			legendHand=eqHand;
			legendText={'Observed EQ'};
			hold on
			
		else
			if CumNumMode
				%TotalNum(isnan(TotalNum))=0;
				%eqHand=plot(YearDec,cumsum(TotalNum),'r');
				yla=ylabel('Cum. Info. Gain');
			else
				%eqHand=plot(YearDec,TotalNum,'r');
				yla=ylabel('Info. Gain');
			end
			
			
			xla=xlabel('Time');
			set(gca,'LineWidth',3,'Box','on','FontSize',16);
			set([xla,yla],'FontSize',16,'FontWeight','bold');
			%set(eqHand,'LineWidth',2);
			legendHand=[];
			legendText={};
			hold on
		end
	
	end	

	tit1=title(TheTitle);
	set(tit1,'FontSize',24,'FontWeight','bold');
	
	
	
	for mC=1:numModels
		%get data
		[TestRes, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{mC},'InUni');
		YearDec = DateNum2DecYear(TimeVec);
		
		if ~SinglePlot 
			%plot
			subplot(numPlots,1,1+mC);
			if CumNumMode
				TestRes(isnan(TestRes))=0;	
				if CorrValue
					[TotalNum, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{mC},'TotalNum');
					TotalNum(isnan(TotalNum))=0;
					ForPlot=cumsum(TestRes.*TotalNum)./cumsum(TotalNum);
					ForPlot(cumsum(TotalNum)==0)=0;
				else
					ForPlot=cumsum(TestRes);
				end
				curHand=plot(YearDec,ForPlot,wheelColor{mod(mC,5)+1});	
				hold on
				yla=ylabel(' cum. Info. Gain');
				xla=xlabel('');
				
				legend(curHand,PlotModName{mC});
				
				
				
			else
				curHand=plot(YearDec,TestRes,wheelColor{mod(mC,5)+1});
				hold on
				yla=ylabel('Info. Gain');
				xla=xlabel('');
				
				
				legend(curHand,PlotModName{mC});
				
			
			end
		
			set(gca,'LineWidth',3,'Box','on','FontSize',16);
			set([xla,yla],'FontSize',16,'FontWeight','bold');
			set([curHand],'LineWidth',2);
			hold off
		else
			if CumNumMode
				TestRes(isnan(TestRes))=0;	
				if CorrValue
					[TotalNum, TimeVec] = TestResExtractor(TestResObj,TrTestField,Models{mC},'TotalNum');
					TotalNum(isnan(TotalNum))=0;
					ForPlot=cumsum(TestRes.*TotalNum)./cumsum(TotalNum);
					ForPlot(cumsum(TotalNum)==0)=0;
				else
					ForPlot=cumsum(TestRes);
				end
				curHand=plot(YearDec,ForPlot,wheelColor{mod(mC,5)+1});	
				
				
				
				legendHand(end+1)=curHand;
				legendText{end+1}=PlotModName{mC};
				
				
				
				
			else
				curHand=plot(YearDec,TestRes,wheelColor{mod(mC,5)+1});	
				
				
				legendHand(end+1)=curHand;
				legendText{end+1}=PlotModName{mC};
				
			
			end
		
			set([curHand],'LineWidth',2);
		
		end
	end	
		
	
	
	if ~SinglePlot 
		%last label
		xla=xlabel('Time');
		set([xla],'FontSize',16,'FontWeight','bold');
	else
		set(gca,'LineWidth',3,'Box','on','FontSize',16);
		legend(legendHand,legendText);
		

	end	
		
	hold off
	
end
	


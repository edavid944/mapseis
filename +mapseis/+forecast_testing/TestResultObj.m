classdef TestResultObj < handle
	%This the first version of the forecast project calculator
	
	
	properties
        	Name
        	ID
        	ObjVersion
        	MetaData
        	
        	Forecasts
        	ProjectID
        	Datastore
        	Filterlist
        	
        	TestConfig
        	TestInfo
        	CalcedResults
        	Results
        	SummaryResults
        	
        	possibleTables
        	possiblePlots
        	
        	FullData
        end
        

    
        methods
        	function obj = TestResultObj(Name)
        		%currently only a constructor function is needed
        		%if needed I will add some functions later if needed. 
        		%TO ADD: getter function, to get single test results 
        		%a bit faster
        		
        		%added 10.9.2013: a property space for the summary results
        		%it can be used for storing results calculated with timestep
        		%based results.
        		
        		
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		
        		%Meta data and progress 
        		obj.Name=Name;
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.ObjVersion='0.9';
        		
        		obj.MetaData=[];
        		obj.Forecasts={};
        		obj.ProjectID=[];
        		obj.Datastore=[];
        		obj.Filterlist=[];
        	
        		obj.TestConfig=[];
        		obj.CalcedResults=[];
        		obj.Results=[];
        		obj.SummaryResults=[];
        	
        		obj.possibleTables=[];
        		obj.possiblePlots=[];
        		obj.FullData=false;
        		
        		
        	end
        	
        	
        	function TestNames = getTestNames(obj)
        		%returns the names of the tests
        		TestNames = TestInfo(:,1);
        	end
        	
        	
        	function TheTimes = getForecastTimes(obj)
        		%returns a list with the available times
        		TheTimes = obj.CalcedResults{2};
        	end
        	
        	
        	function TestResult = getResult(obj,ForecastTime,TestName,ForecastName)
        		%searches the correct test results for a certain time, test and forecast
        		%in case of comparison and ranking test, all model names have to be specied
        		%cell arrays can be used to get more than one result at the time, in case of
        		%the comparison and ranking test, ForecastName has to be a cell array of cell
        		%array.
        		
        		%most of the stuff here is only needed because of the symmetric option in the
        		%comparison test, 
        		
        		
        		
        		TestResult={};
        		
        		if iscell(TestName)
        			
        			foundAll=true;
        			
        			for i=1:numel(TestName)
        				
        				
        				if ~iscell(ForecastTime)
        					CurTime=ForecastTime(i);
        				else
        					CurTime=ForecastTime{i};
        				end
        				
        				CurTestName=TestName{i};
        				CurForeName=ForecastName{i}
        				
        				[CurResult FoundIt] = obj.getSingleTest(CurTime,CurTestName,CurForeName);
        				
        				TestResult={TestResult,CurResult};
        				
        				if ~FoundIt
        					foundAll=false;
        				end	
			
        			end
        			
        		else
        			%passthrough
        			[TestResult FoundIt] = obj.getSingleTest(ForecastTime,TestName,ForecastName);
        			
        		
        		end
        		
        		if ~foundAll
        			warning('Not all test results have been found');
        		end
        		
        		
        	
        	end
        	
        	
        	function [TestResult FoundIt] = getSingleTest(obj,ForecastTime,TestName,ForecastName)
        		%Only for none-cell input, does not have to be called from the outside
        		
        		%needed for the job...
        		TheTimes=obj.CalcedResults{2};
        		TheTest=obj.CalcedResults{1};
        		CastName=obj.CalcedResults{3};
        		
        		symetric=obj.TestConfig.UseSymmetry;
        		
        		FoundIt=false;
			
			inTime=ForecastTime==TheTimes;
			inTest=strcmp(TestName,TheTest);
			
			if any(inTime&inTest)
				TestPos=strcmp(obj.TestInfo(:,1),TestName);
				TestType=obj.TestInfo(TestPos,2);
				Candidats=find(inTime&inTest);
				
				if symmetric
					SameFore = cellfun(@(x) all(strcmp(ForecastName,x)),CastName(Candidats),'UniformOutput',true); 
				
				else
					switch TestType
						case 'single'
							SameFore = cellfun(@(x) all(strcmp(ForecastName,x)),CastName(Candidats),'UniformOutput',true); 
						case 'comparison'
							SameFore = cellfun(@(x) isempty(setdiff(ForecastName,x)),CastName(Candidats),'UniformOutput',true); 	
						case 'ranking'
							SameFore = cellfun(@(x) isempty(setdiff(ForecastName,x)),CastName(Candidats),'UniformOutput',true);
							
							
					end
					
				end
			
				if any(SameFore)
					FoundIt=true;
					ToCheckOut=Candidats(SameFore);
					
					%should not happen if everything has been done correctly in the result structure.
					if sum(SameFore)>1
						disp('more than one test found')
						
						for j=1:sum(SameFore)
							Names=CastName(ToCheckOut(j));
							if iscell(Names)&numel(Names)
								Write4Name=Names{1};
								
								for k=2:numel(Names)
									Write4Name=[Write4Name,'_',Names{k}];
								end
								
							elseif iscell(Names)
								Write4Name=Names{1};
								
							else
								Write4Name=Names;
							
							end
						
							TestResult{end+1}=obj.TestResults{find(inTime)}.(TestName).(Write4Name);
							
							
							
						end
					else
						Names=CastName(ToCheckOut(j));
							
						if iscell(Names)&numel(Names)
							Write4Name=Names{1};
							
							for k=2:numel(Names)
								Write4Name=[Write4Name,'_',Names{k}];
							end
								
						elseif iscell(Names)
							Write4Name=Names{1};
								
						else
							Write4Name=Names;
							
						end
						
						TestResult=obj.TestResults{find(inTime)}.(TestName).(Write4Name);
						
					end
					
				end
			
				
			end
			
			
			if ~FoundIt
				warning('Test Result not found');
			end
        	
        	
        	end
        	
        	
        end

end	

function scanPlotFunctions(obj)
	%scans the available plot function to determine the input
	%they need
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	import mapseis.forecast_testing.PlotPlugIns.*;
	
	PlotFunctions=[];
	custoCount=1;
	
	for tC=1:numel(obj.TestList(:,1))
		availPlots=obj.TestPlotList.(obj.TestList{tC,1}))(:,2);
	
		for fC=1:numel(availPlots)
			if isstr(PlotFunctions,availPlots{fC})
				SregName=availPlots{fC};
			else
				RawString=func2str(availPlots{fC});	
				%kick out all '(',')','@'
				selCrit = RawString=='(' | RawString==')' |...
				RawString=='@';
				RawString=RawString(~selCrit);
				
				%replace '.' by '_'
				TheDot=RawString=='.';
				RawString(TheDot)='_';
				SregName=RawString;
			end
			
			
			
			if ~isfield(PlotFunctions,SregName)
				switch SregName	
					case 'LikeHist'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						PlotFunctions.(SregName).PlotObject= LikelihoodHist();
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='single';
					
					case 'LikeCDF'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						PlotFunctions.(SregName).FuncHandle= LikelihoodCDF();
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='single';
					
					case 'MultiHist'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						PlotFunctions.(SregName).PlotObject=MultiHist();
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='testwise';
						
					
					case 'MultiBox'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						PlotFunctions.(SregName).PlotObject=MultiBox();
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='testwise';	
						
					
					case 'CombinedLike'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						PlotFunctions.(SregName).PlotObject=CombinedLike();
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='modelwise';	
						
					
					case 'SignMatrix'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						%PlotFunctions.(SregName).PlotObject=CombinedLike;
						PlotFunctions.(SregName).No_Plots=2;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='TWcombined';				
					
					case 'Ranking'
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						%PlotFunctions.(SregName).PlotObject=CombinedLike;
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='comparison';	
					
					case 'RatioCDF'
						%MISSING
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						%PlotFunctions.(SregName).PlotObject=CombinedLike;
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='comparison';	
					
					case 'ClassicT'
						%MISSING
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						%PlotFunctions.(SregName).PlotObject=CombinedLike;
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='comparison';	
						
					case 'ClassicW'
						%MISSING
						PlotFunctions.(SregName).Name=obj.TestPlotList.(obj.TestList{tC,1})){fC,1};
						%PlotFunctions.(SregName).PlotObject=CombinedLike;
						PlotFunctions.(SregName).No_Plots=1;
						PlotFunctions.(SregName).SupportedTests=obj.TestList(tC,1);
						PlotFunctions.(SregName).PlotType='comparison';		
					
					otherwise
						%unknown, means it is a function handle
						%UNFINISHED
						TheObject=SregName();
						PlotName=['Plot_'num2str(custoCount)];
						custoCount=custoCount+1;
						PlotFunctions.(PlotName).PlotObject=TheObject;
						
				end
			
			else
			
				PlotFunctions.(SregName).SupportedTests(end+1)=obj.TestList(tC,1);
			
			end
		
		end
	end
	
	obj.PlotFunctions=PlotFunctions;
	obj.PlotFunctionName=fieldnames(PlotFunctions);
	
end
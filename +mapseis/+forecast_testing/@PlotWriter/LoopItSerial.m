function LoopItSerial(obj)
	%serial loop function
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	TheEnd=numel(obj.WorkList(:,1));
	
	if obj.NextPlot<TheEnd
		for kk=obj.NextCalc:TheEnd
			obj.contPlotting(kk);
			obj.NextPlot=obj.NextPlot+1;		
		end
		
		%finish it
		obj.finishCalc;
				
	end
	
end
classdef PlotWriter < handle
	%This object plots the results (in the form of TestResObject) and saves
	%the plot automatically 
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	%UNFINISHED CLASS
	
	
	properties
		Name
		Version
		ID
		ObjVersion
		MetaData
		TestResultObj
		PlotterConfig
		PlotFunctions
		PlotFunctionName
		
		ModelList
		TestList
		TestPlotList
		TimeList
		AvailableData
		SelectedData
		EmptySelect
		MasterTestList
		
		
		TestPlugInList
		OutputFolders
		FileName
		extGUI
		PlotterConfig
		
		NextPlot
		LoopMode
		
		WorkToDo
		WorkList
		WorkDone
	end
	
	
	
	methods
		function obj = PlotWriter(Name)
			%first version of automatic test plotter, not too many 
			%features for now, only the one needed at the moment
			
			
			%TODO: add the whole TR-test Ranking plot mode
			%and also find a solution the SignMat plot.
			%3 results (TR-, T- and W-test) with 2 plots vs
			%only 2 results (T- and W-test) and only 1 plot
			%leaving the ranking info gain test to TR ranking plot
			
			
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
				%A bit gimmicky I admit, but could be useful later
				Name=RawChar(randi(15,1,8)+1);
			end
			
			obj.Name=Name; %may be not so important here, but who knows?
			obj.ID=RawChar(randi(15,1,8)+1);
			obj.Version='0.9';
			
			obj.MetaData={};
			obj.TestResultObj=[];
			obj.PlotterConfig=[];
			obj.PlotFunctions=[];
			obj.PlotFunctionName={};
			
			obj.ModelList={};
			obj.TestList={};
			obj.TestPlotList={};
			obj.TimeList=[];
			obj.AvailableData=[];
			obj.SelectedData=[];
			obj.EmptySelect=[];
			obj.MasterTestList={};
			
			
			
			obj.TestPlugInList=[];
			obj.OutputFolders={};
			obj.FileName=[];
			obj.extGUI=[];
			obj.WorkList={};
			obj.WorkToDo=[];
			obj.WorkDone={};
			
			obj.NextPlot=1;
			obj.LoopMode=true;
			
			obj.PlotterConfig = struct(	'WindowSize',[[800 600]],...
										'PlotOnly',false,...
										'ExcludeEmptyDays',true,...
										'PlotOriginal',true,...
										'PlotMedian',false,...
										'PlotMean',false);
									
		end
		
		%methods in external files
		addTestResult(obj,TestResultObj)
	
		generateTestTypeStruct(obj)
		
		generateFolderStruct(obj)
		
		setMainFolder(obj,MainFolder)
		
		ConfigurePlotting(obj,ConfigStruct)	
	
		ConfigStruct = getConfig(obj)
	
		scanPlotFunctions(obj)
		
		generateModTime(obj)
		
		setPlot(obj,Tests,Models,Plots,Times)
		
		generateWorkList(obj)
		
		startPlot(obj)
		
		contPlotting(obj,ListNr) %UNFINISHED
		
		LoopItSerial(obj)
		
		initFolders(obj)
	
	end
	
end

function generateModTime(obj)
	%builds the list with all possible model and times
	%used for the selection	
	
	%a structure is needed to bring order into the chaos of 
	%test results. Suggestion:
	%	1. Plot function
	%	2. Available Tests for the plot function
	%	3. Available Models (-Combinations) for this test
	%	4. Available Timesteps for this model.
	
	%Plots which combine multiple results will be treated 
	%similar to the single-test-model-time plot, the first
	%one will just produce one plot with the input and the 
	%second will plot multiple plots.
	
	%Summary can also be selected instead/additional to time
	%steps.
	
	%one complication is still there, plots like the combined
	%likelihood plot are best for multiple tests of the SAME
	%model, but not only, so I guess they have to be treated a bit special.
	%But how exactly and how should I design the communication
	%between test and this object to assure, that future tests
	%with similar properties are treated the same?
	
	%possible ways to deal with the problem:
	%1. fixed approach, as soon as a plot of this kind is used
	%	the data is reordered and automatically divided into
	%	model->tests instead of the normal tests->models
	%2. Change the structure order for this tests: 
	%	plot->model->test->time 
	
	%I think the first one is probably the better option, because
	%less confusing and error prone. It would also be possible to
	%add a switch later, for changing the plot order.
	
	%similar things have to be done for the T-test and W-test
	
	%As for the communication between the object, in case of the 
	%include plots (currently the only available ones), it is 
	%know already (a switch statement is probalby the savest thing),
	%for the custom function, the object has to be created and the
	%feature list has to be used (I may have to update the 
	%specification of this list)
	%I should do the scanning in a separate funcion
	
	%build a template for plot selection as well.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	availStruct=[];
	selectionStruct=[];
	summaryStruct=[];
	summarySelStruct=[];
	
	%get possible plots
	ThePlots=obj.PlotFunctionName;
	MasterTestList={};
	MasterModelList={};
	
	%all possible times
	TimeList=obj.TimeList;
	RawTimeSelVec=true(size(TimeList));
	
	%for each plots get the possible tests
	for pC=1:numel(ThePlots)
		TestList=obj.PlotFunctions.(ThePlots{pc}).SupportedTests;
		
		for tC=1:numel(TestList)
			%get available Models
	
			%CHECK IF WORKING SEEMS FISHY->Nope it is correct it was just
			%an old version of the object
			InThisTest=strcmp(obj.TestResultObj.CalcedResults{1},TestList{tC});
			uniMod=unique(obj.TestResultObj.CalcedResults{3}(InThisTest));
			
			if ~any(strcmp(MasterTestList,TestList{tC}))
				MasterTestList{end+1}=TestList{tC};
			end
	
			for mC=1:numel(uniMod)
				inMod=strcmp(ResObj.CalcedResults{3},uniMod{mC});
				
				if ~any(strcmp(MasterModelList,uniMod{mC}))
					MasterModelList{end+1}=uniMod{mC};
				end
	
				%determine available times
				ThisTimes=obj.TestResultObj.CalcedResults{2}(inMod&InThisTest)';
				[timeVal,Pos] = setdiff(TimeList,ThisTimes);
				availInThis=RawTimeSelVec;
				if ~isempty(Pos)
					availInThis(Pos)=false;
				end
	
				availStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=availInThis;
				selectionStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=false(size(TimeList));
				
	
				if ~isempty(obj.TestResultObj.SummaryResults)
					%summarized tests are available determine if this model and test has one
					if isfield(obj.TestResultObj.SummaryResults,TestList{tC})	
						if isfield(obj.TestResultObj.SummaryResults.(TestList{tC}),uniMod{mC})
							summaryStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=true;
							summarySelStruct.(ThePlots{pC}).(TestList{tC}).(uniMod{mC})=false;
						end
					end
				end
	
			end
	
		end
	
	end
	
	obj.AvailableData.Time=availStruct;
	obj.AvailableData.Summary=summaryStruct;
	obj.SelectedData.Time=selectionStruct;
	obj.SelectedData.Summary=summarySelStruct;
	
	%save an empty selection, to make live easier later.
	obj.EmptySelect=obj.SelectedData;
	
	%to avoid unnecessary memory usage later:
	obj.MasterTestList=MasterTestList;
	%obj.MasterModelList=MasterModelList;
	
end
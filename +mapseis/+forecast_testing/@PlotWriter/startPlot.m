function startPlot(obj)
	%starts the whole plotting routine
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	%build folder structure
	obj.initFolders
	
	if ~obj.LoopMode
		%set the recursion limit to a high enough number (matlab will stop
		%the calculation later if this is not done
		NrNeeded=numel(obj.WorkList(:,1))+500; 
		set(0,'RecursionLimit',NrNeeded);
	end
	
	
	if ~obj.LoopMode
		%start calculation
		CurNr=obj.NextPlot;
		obj.NextPlot=obj.NextPlot+1;
		obj.contPlotting(CurNr);
	
	else
		obj.LoopItSerial;
	     
	end
	
end
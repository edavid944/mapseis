function generateFolderStruct(obj)
	%generates the structure with folders, seperated into a 
	%function to avoid dublications
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	fs=filesep;
	defaultMain=['.',fs,'MyTestPlots'];
	if isempty(obj.OutputFolders)
		obj.OutputFolders.MainFolder=defaultMain;
	end
	
	if ~isfield(obj.OutputFolders,'MainFolder')
		obj.OutputFolders.MainFolder=defaultMain;
	end
	
	if ~isempty(obj.TestList)
		MainFold = obj.OutputFolders.MainFolder;		
		for tC=1:numel(obj.TestList(:,1))
			%check for none tests
			IdSeq=obj.TestList{tC,1}(1:5);
			if strcmp(IdSeq,'Ltest')
				TestFoldName='Ltest';
			elseif strcmp(IdSeq,'Ntest')
				TestFoldName='Ntest';
			elseif strcmp(IdSeq,'Mtest')
				TestFoldName='Mtest';
			elseif strcmp(IdSeq,'Stest')
				TestFoldName='Stest';
			elseif strcmp(IdSeq,'CLtes')
				TestFoldName='CLtest';
			elseif strcmp(IdSeq,'Ttest')
				TestFoldName='Ttest';
			elseif strcmp(IdSeq,'Wtest')
				TestFoldName='Wtest';
			elseif strcmp(IdSeq,'Singl')
				TestFoldName='TRanktest';
			else
				TestFoldName=['TestNr_',num2str(tC)];
			end
			
			obj.OutputFolders.(obj.TestList{tC,1})=...
			[MainFold,fs,TestFoldName];
		end
	
	end
	
	%additional folders
	obj.OutputFolders.CombCompareFolder=[MainFold,fs,'Comb_Comparison'];
	obj.OutputFolders.CombLikeFolder=[MainFold,fs,'Comb_Likelihood'];
	obj.OutputFolders.SummaryFolder=[MainFold,fs,'TestSummary'];

end
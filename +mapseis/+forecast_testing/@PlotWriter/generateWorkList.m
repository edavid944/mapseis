function generateWorkList(obj)
	%builds the worklist with the parts which have to be done
	%can only be used after selection has been done and plugins
	%are added
	
	%now the more complicated things, it also has to be taken into
	%account, that some plots need multiple test results, sometime
	%from different time-periods, different tests or different models.
	%I guess the information should somehow be added here,... but it 
	%would be possible to also reconstruct it during the calculation.
	
	%so what struct?
	%definitely it has to be a list, either one cell array or multiple
	%data based "vectors"
	%Needed is for each entry:
	%	-the plot function: only one allowed
	%	-the test type: can be more than one
	%	-the involved models: again, can be more than one
	%	-the timestep(s): for many plots only one is needed
	%			  but multiple timesteps should be 
	%			  allowed.
	%because of the number of "ingridients changing a cell array
	%has to be used at least for some of the part
	
	
	%it is FINISHED but has to be TESTED, I expect there to be some errors
	%at least, given that it is all quite complex and may not the most
	%elegant solution.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	WorkList={};
	
	%get possible plots
	ThePlots=obj.PlotFunctionName;
	   		
	%all possible times
	TimeList=obj.TimeList;
	
	%all possible models	
	ModelList=obj.ModelList;
	
	%all possible tests	
	MasterTestList=obj.MasterTestList;
	
	
	%for each plots get the possible tests
	for pC=1:numel(ThePlots)
		ThisEntry={};
		PlotProps=obj.PlotFunctions.(ThePlots{pc});
		
		
		%check if something is active
		
		activeTestTime=false(size(MasterTestList));
		activeTestSum=false(size(MasterTestList));
		ModelListTime=[];
		ModelListSum=[];
		
		for tC=1:numel(MasterTestList)
			RawModelList=fieldnames(obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}));
			TimeTestActive=false;
			SumTestActive=false;					
	
			for mC=1:numel(RawModelList)
				try
					selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC})&
					obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC});
				catch
					selectionTime=false;
				end
	
				try
					selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC})&
					obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(RawModelList{mC});
				catch
					selectionSum=false;
				end
	
				if any(selectionTime)
					TimeTestActive=true;
					ModNr=find(strcmp(ModelList,RawModelList{mC});
					if ~isempty(ModNr)
						if ~any(ModNr==ModelListTime)
							ModelListTime(end+1)=ModNr;
						end
					end
				end
	
				if any(selectionSum)
					TimeTestActive=true;
					ModNr=find(strcmp(ModelList,RawModelList{mC});
					if ~isempty(ModNr)
						if ~any(ModNr==ModelListSum)
							ModelListSum(end+1)=ModNr;
						end
					end
				end
	
			end
		
			activeTestTime(tC)=TimeTestActive;
			activeTestSum(tC)=SumTestActive;
		end
	
		if ~any(activeTestTime|activeTestSum)
			%nothing wanted here
			continue;
		
		end
	
		%the try-catch blocks in the section underneath are needed
		%because some models may not exist in some tests (e.g. comparison
		%tests)
		
	switch PlotProps.PlotType
		case 'single'
			%simple case one test, one model, and one timestep
			for tC=1:numel(MasterTestList)
				if activeTestTime(tC)
					for mC=1:numel(ModelListTime)
						ModelName=ModelList{ModelListTime(mC)};
						try
							selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
							obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						catch
							selectionTime=false;
						end
			
						TheTimes=find(selectionTime);
						
						for ttC=1:numel(TheTimes)
							WorkList(end+1,1)={pC};
							WorkList(end,2)={tC};
							WorkList(end,3)={mC};
							WorkList(end,4)={TheTimes(ttC)};	
						end
					end
				end
			
				if activeTestSum(tC)
					for mC=1:numel(ModelListSum)
						ModelName=ModelList{ModelListTime(mC)};
						try
							selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
							obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						catch
							selectionSum=false;
						end
				
						if selectionSum
							WorkList(end+1,1)={pC};
							WorkList(end,2)={tC};
							WorkList(end,3)={mC};
							WorkList(end,4)={0};
						end
					end
			
				end
			end
		
		case 'testwise'
			%NOT PERFECT, the case where we do not want to include a test in some times steps
			%and for only one model, is not covered. In this case it would have to be done in
			%two steps. I may add direct support for that later, but it has no priority.
			
			%multiple test results (only likelihood will be used)
			LikeliTest=strcmp(obj.TestList(:,3),'Likelihood');
			%get all selected likelihood based tests
			TimeTest=find(activeTestTime&LikeliTest);
			SumTest=find(activeTestSum&LikeliTest);
			
			if ~isempty(TimeTest)
				for mC=1:numel(ModelListTime)
					ModelName=ModelList{ModelListTime(mC)};
					ProtoTest=MasterTestList{TimeTest(1)};
					try
						selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(ProtoTest).(ModelName)&
								obj.SelectedData.Time.(ThePlots{pc}).(ProtoTest).(ModelName);
					catch
						selectionTime=false;
					end
					
					TheTimes=find(selectionTime);
					
					for ttC=1:numel(TheTimes)
						WorkList(end+1,1)={pC};
						WorkList(end,2)={TimeTest};
						WorkList(end,3)={mC};
						WorkList(end,4)={TheTimes(ttC)};	
					end
				
						
				
				end
			end
			
			
			if ~isempty(SumTest)
				for mC=1:numel(ModelListTime)
					ModelName=ModelList{ModelListTime(mC)};
					ProtoTest=MasterTestList{SumTest(1)};
					try
						selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(ProtoTest).(ModelName)&
						obj.SelectedData.Summary.(ThePlots{pc}).(ProtoTest).(ModelName);
					catch
						selectionSum=false;
					
					end
					
					if selectionSum
						WorkList(end+1,1)={pC};
						WorkList(end,2)={SumTest};
						WorkList(end,3)={mC};
						WorkList(end,4)={0};	
					end
					
				end
			end

		case 'modelwise'
			%multiple models will be sent

			for tC=1:numel(MasterTestList)
				if activeTestTime(tC)
					%get the active models
					
					ThisTestMod=[];
					for mC=1:numel(ModelListTime)
						ModelName=ModelList{ModelListTime(mC)};
						try
							selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
							obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						catch
							selectionTime=false;
						end
						
						if any(selectionTime)
							if ~any(ThisTestMod==ModelListTime(mC))
								ThisTestMod(end+1)=ModelListTime(mC);
							end
						end
					end
			
					if ~isempty(ThisTestMod)
						ModelName=ModelList{ThisTestMod(1)};
						selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
						obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						TheTimes=find(selectionTime);
						
						for ttC=1:numel(TheTimes)
							WorkList(end+1,1)={pC};
							WorkList(end,2)={tC};
							WorkList(end,3)={ThisTestMod};
							WorkList(end,4)={TheTimes(ttC)};	
						end			
					end
				end
			
				if activeTestSum(tC)
					%get the active models
					
					ThisTestMod=[];
					for mC=1:numel(ModelListTime)
						ModelName=ModelList{ModelListTime(mC)};
						try
							selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
							obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						catch
							selectionSum=false;
						end
					
						if any(selectionSum)
							if ~any(ThisTestMod==ModelListTime(mC))
								ThisTestMod(end+1)=ModelListTime(mC);
							end
						end
					end
			
					if ~isempty(ThisTestMod)
						ModelName=ModelList{ThisTestMod(1)};
						selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
						obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						
						if selectionSum
							WorkList(end+1,1)={pC};
							WorkList(end,2)={tC};
							WorkList(end,3)={ThisTestMod};
							WorkList(end,4)={0};	
						end
					
					end
			
				end

			end
			
			
		case 'TWcombined'
			%T and W-test needed 
			%Both tests have to be selected
			TtestID=strcmp(obj.TestList(:,2),'Ttest');
			WtestID=strcmp(obj.TestList(:,2),'Wtest');
			
			if sum(TtestID|WtestID)<2
				%one is missing so continue to to the next plot
				continue;
			end
			
			TheTests=find(TtestID|WtestID);
			
			if all(activeTestTime(TheTests))
				%get model(s), remember although two models
				%are compare only one model entry per plot
				%is used.
				for mC=1:numel(ModelListTime)
					ModelName=ModelList{ModelListTime(mC)};
					try
						selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName)&
						obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName);
					catch
						selectionTime=false;
					end
			
					TheTimes=find(selectionTime);
					
					for ttC=1:numel(TheTimes)
						WorkList(end+1,1)={pC};
						WorkList(end,2)={TheTests};
						WorkList(end,3)={mC};
						WorkList(end,4)={TheTimes(ttC)};	
					
					end
				end
			end
						
			if all(activeTestSum(TheTests))
				for mC=1:numel(ModelListSum)
					ModelName=ModelList{ModelListTime(mC)};
					try
						selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName)&
						obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{TheTests(1)}).(ModelName);
					catch
						selectionSum=false;
					end
					
					if selectionSum
						WorkList(end+1,1)={pC};
						WorkList(end,2)={TheTests};
						WorkList(end,3)={mC};
						WorkList(end,4)={0};
					end
				end
				
			end			
			
		case 'comparison'
			%two models but they are already predefined
			%currently the same as for 'single' just the model entries
			%are different
			
			for tC=1:numel(MasterTestList)
				if activeTestTime(tC)
					for mC=1:numel(ModelListTime)
						ModelName=ModelList{ModelListTime(mC)};
						try
							selectionTime=obj.AvailableData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
							obj.SelectedData.Time.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						catch
							selectionTime=false;
						end
						
						TheTimes=find(selectionTime);
						
						for ttC=1:numel(TheTimes)
							WorkList(end+1,1)={pC};
							WorkList(end,2)={tC};
							WorkList(end,3)={mC};
							WorkList(end,4)={TheTimes(ttC)};	
							
						end
					
					end
				
				end
			
				if activeTestSum(tC)
					for mC=1:numel(ModelListSum)
						ModelName=ModelList{ModelListTime(mC)};
						try
							selectionSum=obj.AvailableData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName)&
							obj.SelectedData.Summary.(ThePlots{pc}).(MasterTestList{tC}).(ModelName);
						catch
							selectionSum=false;
						end
				
						if selectionSum
							WorkList(end+1,1)={pC};
							WorkList(end,2)={tC};
							WorkList(end,3)={mC};
							WorkList(end,4)={0};
						end
					end
			
				end
			
			end
			
			
		end
			
			
	end
	
	obj.WorkList=WorkList;
	
end
function initFolders(obj)
	%generates the folders as specified by the folder structure.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	fs=filesep;
	OutStru=obj.OutputFolders;
	
	%built main folder.
	CheckExist=exist(OutStru.MainFolder);
	if CheckExist==0
		if isunix
			Ucommand=['mkdir -p ',OutStru.MainFolder];
			[stat, res] = unix(Ucommand);
			
			if stat==0
				disp(['Main Folder folder generated']);
			else
				warning(['Could not generate Main folder']);
			end
		
		else
			Dcommand=['mkdir -p ',OutStru.MainFolder];
			[stat, res] = dos(Dcommand);
			
			if stat==0
				disp(['Main Folder folder generated']);
			else
				warning(['Could not generate Main folder']);
			end
		end
	end
	
	RawFolderList=fieldnames(OutStru);
	MainPos=strcmp(RawFolderList,'MainFolder');
	FolderList=RawFolderList(~MainPos);
	
	for fC=1:numel(FolderList)
		CheckExist=exist(OutStru.(FolderList{fC}));
		if CheckExist==0
			if isunix
				Ucommand=['mkdir -p ',OutStru.(FolderList{fC})];
				[stat, res] = unix(Ucommand);
				
				if stat==0
					disp([FolderList{fC}, ' folder generated']);
				else
					warning(['Could not generate ',FolderList{fC}, ' folder']);
				end
				
			else
				Dcommand=['mkdir -p ',OutStru.(FolderList{fC})];
				[stat, res] = dos(Dcommand);
				
				if stat==0
					disp([FolderList{fC}, ' folder generated']);
				else
					warning(['Could not generate ',FolderList{fC}, ' folder']);
				end
			end
		end
	
	end
	
end
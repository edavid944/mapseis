function setPlot(obj,Tests,Models,Plots,Times)
	%allows to set the plots which should be plotted
	
	%each of parameters can be a cell array (or normal vector in case of
	%Times, a single value or the keyword 'all' or 'none'
	%In case of Times, also the keyword 'summary' is allowed.
	
	%FINISHED (but untested)
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	%prepare lists with needed models and times;
	SummarySwitch=[];
	
	
	if iscell(Plots)
		PlotList=Plots;
	else
		if strcmp(Plots,'none')
			%That's a full reset, delete everything
			obj.SelectedData=obj.EmptySelect;
			return;
			
		elseif strcmp(Plots,'all')
			PlotList=fieldnames(obj.PlotFunctions);
		else
			PlotList={Plots};
		end	
	
	end
	
	if iscell(Tests)
		TestList=Tests;
	
	else	
		if strcmp(Tests,'none')
			TestList=-1;
			%TestList=repmat({'none'},size(PlotList));
			
		elseif strcmp(Tests,'all')
			TestList=fieldnames(obj.PlotFunctions);
		else
			TestList={Tests};
		end	
	end
	
	if iscell(Models)
		ModelList=Models;
	
	else
		if strcmp(Models,'none')
			ModelList=-1;
			%ModelList=repmat({'none'},size(TestList));
			
		elseif strcmp(Models,'all')
			ModelList=obj.ModelList;
		else
			ModelList={Models};
		end	
	end
	
	if isnumeric(Times)
		TimeSel=true(size(obj.TimeList));
		[timeVal,notWanted] = setdiff(obj.TimeList,Times);
		TimeSel(notWanted)=false;
		
	elseif islogical(Times)
		TimeSel=Times;
	else
		if strcmp(Models,'none')
			TimeSel=false(size(obj.TimeList));
			SummarySwitch=false;
			
		elseif strcmp(Tests,'all')
			TimeSel=true(size(obj.TimeList));
			%SummarySwitch=true;
			
		elseif strcmp(Tests,'summary')
			TimeSel=[];
			SummarySwitch=true;
			
		end
	end
	
	
	for pC=1:numel(PlotList)
		if TestList==-1
			obj.SelectedData.Time.(PlotList{pC})=obj.EmptySelect.Time.(PlotList{pC});
			obj.SelectedData.Summary.(PlotList{pC})=obj.EmptySelect.Summary.(PlotList{pC});
		else
		
			for tC=1:numel(TestList)
				if ModelList==-1
					obj.SelectedData.Time.(PlotList{pC}).(TestList{tC})=obj.EmptySelect.Time.(PlotList{pC}).(TestList{tC});
					obj.SelectedData.Summary.(PlotList{pC}).(TestList{tC})=obj.EmptySelect.Summary.(PlotList{pC}).(TestList{tC});
				else
					for mC=1:numel(ModelList)
						obj.SelectedData.Time.(PlotList{pC}).(TestList{tC}).(ModelList{mC})=TimeSel;
				
						if ~isempty(SummarySwitch)
							obj.SelectedData.Time.(PlotList{pC}).(TestList{tC}).(ModelList{mC})=SummarySwitch;
				  							
						end
	
					end
	
				end
			end	
		end
	
	end
	
end
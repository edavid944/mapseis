function generateTestTypeStruct(obj)
	%generates a list with testtype and name of the test in
	%structure
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	for tC=1:numel(obj.TestList(:,1))
		%check for none tests
		IdSeq=obj.TestList{tC,1}(1:5);
		if strcmp(IdSeq,'Ltest')
			TestType='Ltest';
			TestBasis='Likelihood';
		elseif strcmp(IdSeq,'Ntest')
			TestType='Ntest';
			TestBasis='Likelihood';
		elseif strcmp(IdSeq,'Mtest')
			TestType='Mtest';
			TestBasis='Likelihood';
		elseif strcmp(IdSeq,'Stest')
			TestType='Stest';
			TestBasis='Likelihood';
		elseif strcmp(IdSeq,'CLtes')
			TestType='CLtest';
			TestBasis='Likelihood';
		elseif strcmp(IdSeq,'Ttest')
			TestType='Ttest';
			TestBasis='InfoGain';
		elseif strcmp(IdSeq,'Wtest')
			TestType='Wtest';
			TestBasis='InfoGain';
		elseif strcmp(IdSeq,'Singl')
			TestType='TRanktest';
			TestBasis='InfoGain';
		else
			TestType='Custom';
			TestBasis='Unknown';
		end
		
		obj.TestList{tC,2}=TestType;
		obj.TestList{tC,3}=TestBasis;
	end
	
end
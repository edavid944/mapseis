function TestSummarizer(TestResObj,TestTypes)
	%Adds summary results to a test object
	%TestTypes has to be a cell row vector containing the type of the tests
	%it will be added to the TestInfo in the TestResObject and is needed
	%for the summary, it can be left empty or missing if this info has been 
	%already added. Maybe later it will be filled automatically.
	%The entries can be 'Likelihood', 'TRtest' ,'Ttest', 'Wtest', 'Etest' and 'None', if set to 
	%none the test will be ignored for the summary.
	
	
	%this version is currently only for none-error estimated variants
	
	%DE 2013
	
	if nargin<2
		TestTypes=[];
	end
	
	if isempty(TestTypes)
		try
			TestTypes=TestResObj.TestInfo(:,4);
		catch
			error('Test Object missing the Test Type - please specify in the input')
		end
			
	else
		TestResObj.TestInfo(:,4)=TestTypes;
		
	end
	
	%get names of the tests
	TestNames=TestResObj.TestInfo(:,1);	
	
	
	
	
	for tC=1:numel(TestNames)
		
		%get available models
		TheModName=fieldnames(TestResObj.Results{1}.(TestNames{tC}));
		disp(TestNames{tC})
	
		switch TestTypes{tC}
			case 'Likelihood'
			
				for mC=1:numel(TheModName)
					%use first result as a template
					CombRes=TestResObj.Results{1}.(TestNames{tC}).(TheModName{mC});
					
					%init needed values
					PassedTest=0;
					NaNCount=0;
					LogLikeModel=CombRes.LogLikeModel;
					LogLikeCatalog=CombRes.LogLikeCatalog;
					TotalTests=numel(TestResObj.Results);
					
					disp(size(LogLikeModel));
					
					for sC=2:numel(TestResObj.Results)
						%disp(sC)
						try
							ThisResult=TestResObj.Results{sC}.(TestNames{tC}).(TheModName{mC});
						catch
							ThisResult.LogLikeCatalog=NaN;
						end
						
						if ~isnan(ThisResult.LogLikeCatalog)
							%disp(size(ThisResult.LogLikeModel));
							try
								LogLikeModel=LogLikeModel+ThisResult.LogLikeModel;
							catch
								disp('had to shift')
								LogLikeModel=LogLikeModel+ThisResult.LogLikeModel';
							end
							%in some tests there are the dimensions reverse (code bug)
							
							LogLikeCatalog=LogLikeCatalog+ThisResult.LogLikeCatalog;
							if ThisResult.Passed
								PassedTest=PassedTest+1;	
													
							end
						else
							NaNCount=NaNCount+1;
							TotalTests=TotalTests-1;
						end
					end
					
					CombRes.LogLikeModel=LogLikeModel;
					CombRes.LogLikeCatalog=LogLikeCatalog;
					
					if strcmp(CombRes.TestName,'Ntest');
						CombRes.QScoreL=sum(LogLikeModel<=LogLikeCatalog)/CombRes.numSimCat;
						CombRes.QScoreH=sum(LogLikeModel>=LogLikeCatalog)/CombRes.numSimCat;
						CombRes.Passed=CombRes.QScoreL>=CombRes.SignVal/2 & CombRes.QScoreH>=CombRes.SignVal/2;
						
					else
						CombRes.QScore=sum(LogLikeModel<=LogLikeCatalog)/CombRes.numSimCat;
						CombRes.Passed=CombRes.QScore>=CombRes.SignVal;
						
					end
					
					CombRes.NumPassed=PassedTest;
					CombRes.TotalNumTest=TotalTests;
					CombRes.NaNCount=NaNCount;
					
					%save into struct
					ResFields=fieldnames(CombRes);
					for fC=1:numel(ResFields)
						TestResObj.SummaryResults.(TestNames{tC}).(TheModName{mC}).(ResFields{fC})= CombRes.(ResFields{fC});
					
					end
					%I use the loop here to avoid accidental overwritting of previous not related summaries
					
				end
			
				
				
				
				
				
				
				
			
				
				
			case 'TRtest'
			
				for mC=1:numel(TheModName)
					%use first result as a template
					CombRes=TestResObj.Results{1}.(TestNames{tC}).(TheModName{mC});
					
					%init needed values
					PassedTest=0;
					NaNCount=0;
					
					EqNum=CombRes.TotalNum;
					if isfield(CombRes,'numGain')
						numGain=CombRes.numGain;
					else
						numGain=EqNum;
						CombRes.numGain=numGain;
						
					end
					%later test should have the field numGain, but some do not
					%this is problematic for bin mode tests, but using TotalNum
					%is better then nothing (none was done without it anyway)
					
					ExpVal=CombRes.ExpVal;
					I_Uni=CombRes.I_Uni+(CombRes.ExpVal/CombRes.TotalNum);
					I_Perf=CombRes.I_Perf;
					I_Model=CombRes.I_Model+(CombRes.ExpVal/CombRes.TotalNum);
					
					UseWelch=TestResObj.TestInfo{tC,3}.WelchT;
					
					
					TotalTests=numel(TestResObj.Results);
					
					for sC=2:numel(TestResObj.Results)
						try
							ThisResult=TestResObj.Results{sC}.(TestNames{tC}).(TheModName{mC});
						catch
							NaNCount=NaNCount+1;
							continue;	
						end
						
						EqNum=EqNum+ThisResult.TotalNum;
						if ~isnan(ThisResult.ExpVal)
							ExpVal=ExpVal+ThisResult.ExpVal;
						end
						
						ThisResUni=ThisResult.I_Uni+(ThisResult.ExpVal/ThisResult.TotalNum);
						ThisResModel=ThisResult.I_Model+(ThisResult.ExpVal/ThisResult.TotalNum);
						I_Uni=[I_Uni;ThisResUni];
						I_Perf=[I_Perf;ThisResult.I_Perf];
						I_Model=[I_Model;ThisResModel];
						
						if isfield(ThisResult,'numGain')
							numGain=numGain+ThisResult.numGain;
						else
							numGain=EqNum;
							
						end
						
												
						if ThisResult.Passed
							PassedTest=PassedTest+1;	
												
						end
					end
					
					%exclude zeros from the Gains (some version of the test set this in the 
					%empty case)
					I_Uni=I_Uni(I_Uni~=0);
					I_Perf=I_Perf(I_Perf~=0);
					I_Model=I_Model(I_Model~=0);
					
					%correct for number
					I_Uni=I_Uni-(ExpVal/EqNum);
					I_Model=I_Model-(ExpVal/EqNum);
					
					%recalculate everything needed
					RawDiffUni=I_Model-I_Uni;
					RawDiffPerf=I_Perf-I_Uni;
					InUni=mean(RawDiffUni);
					InPerf=mean(RawDiffPerf);
					
					if ~UseWelch
						Var_InUni=var(RawDiffUni);
						Var_InPerf=var(RawDiffPerf);
						
						TvalUni=Var_InUni/(sqrt(Var_InUni)/sqrt(EqNum));
						TvalPerf=Var_InPerf/(sqrt(Var_InPerf)/sqrt(EqNum));
						
						deg_of_free_Uni=numGain-1;
						deg_of_free_Perf=numGain-1;
						
						TtableUni = tinv(1-CombRes.SignVal,deg_of_free_Uni);
						TtablePerf = tinv(1-CombRes.SignVal,deg_of_free_Perf);
						Conf_InUni = TtableUni * Var_InUni.^(1/2) /sqrt(EqNum);
						Conf_InPerf = TtablePerf * Var_InPerf.^(1/2) /sqrt(EqNum);
						Pval= 1 - tcdf(abs(TvalUni),deg_of_free_Uni);
						
					else	
						avg_InfoUni = var(I_Uni);
						avg_InfoPerf = var(I_Perf);
						avg_InfoCand= var(I_Model);
					
						Var_InUni=sqrt(avg_InfoUni^2/TotalNum+avg_InfoCand^2/TotalNum);
						Var_InPerf=sqrt(avg_InfoUni^2/TotalNum+avg_InfoPerf^2/TotalNum);
						
						TvalUni=(mean(I_Model)-mean(InUni))/Var_InUni;
						TvalPerf=(mean(I_Perf)-mean(InUni))/Var_InPerf;
						
						deg_of_free_Uni=((avg_InfoCand^2/numGain+avg_InfoUni^2/numGain)^2)/...
								 ((avg_InfoCand^2/numGain)^2/(numGain-1)+...
								 (avg_InfoUni^2/numGain)^2/(numGain-1));
								 
						deg_of_free_Perf= ((avg_InfoPerf^2/numGain+avg_InfoUni^2/numGain)^2)/...
								 ((avg_InfoPerf^2/numGain)^2/(numGain-1)+...
								 (avg_InfoUni^2/numGain)^2/(numGain-1));
						
						TtableUni = tinv(1-CombRes.SignVal,deg_of_free_Uni);
						TtablePerf = tinv(1-CombRes.SignVal,deg_of_free_Perf);
						
						Conf_InUni = Tvalue_table_Uni * Var_InUni;
						Conf_InPerf = Tvalue_table_Perf * Var_InPerf;
						Pval= 1 - tcdf(abs(TvalUni),deg_of_free_Uni);
					
					
					end
					
					PercentGain=round(InUni./InPerf*1000)./10;
					PercentConf=round(Var_InUni./InPerf*10000)./100;
					
					CombRes.TotalNum=EqNum;
					CombRes.numGain=numGain;
					CombRes.ExpVal=ExpVal;
					CombRes.Pval=Pval;
					CombRes.I_Uni=I_Uni;
					CombRes.I_Perf=I_Perf;
					CombRes.I_Model=I_Model;
					CombRes.RawDiffUni=RawDiffUni;
					CombRes.RawDiffPerf=RawDiffPerf;
					CombRes.InUni=InUni;
					CombRes.InPerf=InPerf;
					CombRes.deg_of_free_Uni=deg_of_free_Uni;
					CombRes.deg_of_free_Perf=deg_of_free_Perf;
					CombRes.Var_InUni=Var_InUni;
					CombRes.Var_InPerf=Var_InPerf;
					CombRes.TvalUni=TvalUni;
					CombRes.TvalPerf=TvalPerf;
					CombRes.TtableUni=TtableUni;
					CombRes.TtablePerf=TtablePerf;
					CombRes.Conf_InUni=Conf_InUni;
					CombRes.Conf_InPerf=Conf_InPerf;
					CombRes.PercentGain=PercentGain;
					CombRes.PercentConf=PercentConf;
					
					CombRes.NumPassed=PassedTest;
					CombRes.TotalNumTest=TotalTests;
					CombRes.NaNCount=NaNCount;
					
					%save into struct
					ResFields=fieldnames(CombRes);
					for fC=1:numel(ResFields)
						TestResObj.SummaryResults.(TestNames{tC}).(TheModName{mC}).(ResFields{fC})= CombRes.(ResFields{fC});
					
					end
					%I use the loop here to avoid accidental overwritting of previous not related summaries
					
					
				end	
			
				
				
				
				
				
				
				
				
				
				
				
			case 'Ttest'
				for mC=1:numel(TheModName)
					%use first result as a template
					CombRes=TestResObj.Results{1}.(TestNames{tC}).(TheModName{mC});
					
					%init needed values
					A_better_B_count=0;
					Sign_better_count=0;
					NaNCount=0;
					
					EqNum=CombRes.TotalNum;
					if isfield(CombRes,'numGain')
						numGain=CombRes.numGain;
					else
						numGain=EqNum;
						CombRes.numGain=numGain;
						
					end
					%later test should have the field numGain, but some do not
					%this is problematic for bin mode tests, but using TotalNum
					%is better then nothing (none was done without it anyway)
					
					ExpValA=CombRes.ExpValA;
					ExpValB=CombRes.ExpValB;
					I_A=CombRes.I_A+(ExpValA/CombRes.TotalNum);
					I_B=CombRes.I_B+(ExpValB/CombRes.TotalNum);
										
					UseWelch=TestResObj.TestInfo{tC,3}.WelchT;
					
					
					TotalTests=numel(TestResObj.Results);
					
					for sC=2:numel(TestResObj.Results)
						try
							ThisResult=TestResObj.Results{sC}.(TestNames{tC}).(TheModName{mC});
						catch
							NaNCount=NaNCount+1;
							continue
						end
							
						EqNum=EqNum+ThisResult.TotalNum;
						if ~isnan(ThisResult.ExpValA)
							ExpValA=ExpValA+ThisResult.ExpValA;
						end
						if ~isnan(ThisResult.ExpValB)
							ExpValB=ExpValB+ThisResult.ExpValB;
						end
						
						ThisResA=ThisResult.I_A+(ThisResult.ExpValA/ThisResult.TotalNum);
						ThisResB=ThisResult.I_B+(ThisResult.ExpValB/ThisResult.TotalNum);
						I_A=[I_A;ThisResA];
						I_B=[I_B;ThisResB];
											
						if isfield(ThisResult,'numGain')
							numGain=numGain+ThisResult.numGain;
						else
							numGain=EqNum;
							
						end
						
						
						if ThisResult.A_better_B
							A_better_B_count=A_better_B_count+1;	
												
						end
					
						
						if ThisResult.Sign_better
							Sign_better_count=Sign_better_count+1;	
												
						end
						
					end
					
					%correct the I_A and I_B
					I_A=I_A-(ExpValA/EqNum);
					I_B=I_B-(ExpValB/EqNum);
					
					%calculate addition values
					RawDiff=I_A-I_B;
					InAB=mean(RawDiff);
					
					if ~UseWelch
						Var_InAB=var(RawDiff);
						Tval=InAB/(sqrt(Var_InAB)/sqrt(EqNum)); 
						deg_of_free=numGain-1;
						
						Ttable=tinv(1-CombRes.SignVal,deg_of_free);
						Conf_InAB = Ttable * Var_InAB.^(1/2) / sqrt(EqNum);
						Pval = 1 - tcdf(abs(Tval),deg_of_free);
					
					else
						avg_InfoA = var(I_A);
						avg_InfoB = var(I_B);
						Var_InAB=sqrt(avg_InfoA^2/TotalNum+avg_InfoB^2/EqNum);
						Tval=(mean(I_A)-mean(I_B))/var_InfoGainA2B;
						deg_of_free=((avg_InfoA^2/numGain+avg_InfoB^2/numGain)^2)/...
								 ((avg_InfoA^2/numGain)^2/(numGain-1)+...
								 (avg_InfoB^2/numGain)^2/(numGain-1));
						
						Ttable=tinv(1-CombRes.SignVal,deg_of_free);
						Conf_InAB = Ttable * Var_InAB;
						Pval = 1 - tcdf(abs(Tval),deg_of_free);
					end
					
					A_better_B = InAB > 0;
					Sign_better=Pval<=CombRes.SignVal;
					
					
					%repack
					CombRes.TotalNum=EqNum;
					CombRes.numGain=numGain;
					CombRes.ExpValA=ExpValA;
					CombRes.ExpValB=ExpValB;
					CombRes.Pval=Pval;
					CombRes.A_better_B=A_better_B;
					CombRes.Sign_better=Sign_better;
					CombRes.I_A=I_A;
					CombRes.I_B=I_B;
					CombRes.RawDiff=RawDiff;
					CombRes.InAB=InAB;
					CombRes.deg_of_free=deg_of_free;
					CombRes.Var_InAB=Var_InAB;
					CombRes.Tval=Tval;
					CombRes.Ttable=Ttable;
					CombRes.Conf_InAB=Conf_InAB;
					
					CombRes.A_better_B_count=A_better_B_count;
					CombRes.Sign_better_count=Sign_better_count;
					CombRes.TotalNumTest=TotalTests;
					CombRes.NaNCount=NaNCount;
					
					%save into struct
					ResFields=fieldnames(CombRes);
					for fC=1:numel(ResFields)
						TestResObj.SummaryResults.(TestNames{tC}).(TheModName{mC}).(ResFields{fC})= CombRes.(ResFields{fC});
					
					end
					%I use the loop here to avoid accidental overwritting of previous not related summaries
					
					
				end
				
				

				
				
			
			case 'Wtest'
				for mC=1:numel(TheModName)
					%use first result as a template
					CombRes=TestResObj.Results{1}.(TestNames{tC}).(TheModName{mC});
					
					%init needed values
					A_better_B_count=0;
					Sign_better_count=0;
					NaNCount=0;
					
					EqNum=CombRes.TotalNum;
					
					ExpValA=CombRes.ExpValA;
					ExpValB=CombRes.ExpValB;
					I_A=CombRes.I_A+(ExpValA/CombRes.TotalNum);
					I_B=CombRes.I_B+(ExpValB/CombRes.TotalNum);
										
					
					MatlabMethod=TestResObj.TestInfo{tC,3}.MatlabMethod;					
								
					
					TotalTests=numel(TestResObj.Results);
					
					for sC=2:numel(TestResObj.Results)
						try
							ThisResult=TestResObj.Results{sC}.(TestNames{tC}).(TheModName{mC});
						catch
							NaNCount=NaNCount+1;
							continue
						end
						EqNum=EqNum+ThisResult.TotalNum;
						if ~isnan(ThisResult.ExpValA)
							ExpValA=ExpValA+ThisResult.ExpValA;
						end
						if ~isnan(ThisResult.ExpValB)
							ExpValB=ExpValB+ThisResult.ExpValB;
						end
					
						
						ThisResA=ThisResult.I_A+(ThisResult.ExpValA/ThisResult.TotalNum);
						ThisResB=ThisResult.I_B+(ThisResult.ExpValB/ThisResult.TotalNum);
						I_A=[I_A;ThisResA];
						I_B=[I_B;ThisResB];
											
											
						
						if ThisResult.A_better_B
							A_better_B_count=A_better_B_count+1;	
												
						end
					
						
						if ThisResult.Sign_better
							Sign_better_count=Sign_better_count+1;	
												
						end
						
					end
					
					%correct the I_A and I_B
					I_A=I_A-(ExpValA/EqNum);
					I_B=I_B-(ExpValB/EqNum);
					
					%calculate addition values
					RawDiff=I_A-I_B;
					InAB=mean(RawDiff);
					
					if ~MatlabMethod
						MedNoZero=RawDiff(RawDiff~=0);
						NegVal=MedNoZero<0;
						TheRanks=tiedrank(abs(MedNoZero));	
						
						W_plus=sum(TheRanks(~NegVal));
						W_minus=sum(TheRanks(NegVal));
						W_overall=W_plus+W_minus;
						try
							[Pval,h,stats] = signrank(RawDiff,0,'alpha',CombRes.SignVal);
						catch
							warning('could not determine p_value')
							if EqNum==0
								Pval=1;
							else
								Pval=NaN;
							end
						end	
					
					else
						
						[Pval,h,stats] = signrank(RawDiff,0,'alpha',CombRes.SignVal);
					
						W_plus=NaN;
						W_minus=NaN;
						W_overall=NaN;
					end
					
					A_better_B = InAB > 0;
					Sign_better=Pval<=CombRes.SignVal;
					
					
					%repack
					CombRes.TotalNum=EqNum;
					CombRes.numGain=numGain;
					CombRes.ExpValA=ExpValA;
					CombRes.ExpValB=ExpValB;
					CombRes.Pval=Pval;
					CombRes.A_better_B=A_better_B;
					CombRes.Sign_better=Sign_better;
					CombRes.I_A=I_A;
					CombRes.I_B=I_B;
					CombRes.RawDiff=RawDiff;
					CombRes.InAB=InAB;
					CombRes.W_plus=W_plus;
					CombRes.W_minus=W_minus;
					CombRes.W_overall=W_overall;
					
					CombRes.A_better_B_count=A_better_B_count;
					CombRes.Sign_better_count=Sign_better_count;
					CombRes.TotalNumTest=TotalTests;
					CombRes.NaNCount=NaNCount;
					
					%save into struct
					ResFields=fieldnames(CombRes);
					for fC=1:numel(ResFields)
						TestResObj.SummaryResults.(TestNames{tC}).(TheModName{mC}).(ResFields{fC})= CombRes.(ResFields{fC});
					
					end
					%I use the loop here to avoid accidental overwritting of previous not related summaries
					
					
				end
			
			case 'Etest'
				for mC=1:numel(TheModName)
					%use first result as a template
					CombRes=TestResObj.Results{1}.(TestNames{tC}).(TheModName{mC});
					
					%init needed values
					A_better_B_count=0;
					Sign_better_count=0;
					NaNCount=0;
					
					EqNum=CombRes.TotalNum;
					if isfield(CombRes,'numGain')
						numGain=CombRes.numGain;
					else
						numGain=EqNum;
						CombRes.numGain=numGain;
						
					end
					%later test should have the field numGain, but some do not
					%this is problematic for bin mode tests, but using TotalNum
					%is better then nothing (none was done without it anyway)
					
					ExpValA=CombRes.ExpValA;
					ExpValB=CombRes.ExpValB;
					I_A=CombRes.I_A+(ExpValA/CombRes.TotalNum);
					I_B=CombRes.I_B+(ExpValB/CombRes.TotalNum);
				
					
					TotalTests=numel(TestResObj.Results);
					
					for sC=2:numel(TestResObj.Results)
						try
							ThisResult=TestResObj.Results{sC}.(TestNames{tC}).(TheModName{mC});
						catch
							NaNCount=NaNCount+1;
							continue
						end
							
						EqNum=EqNum+ThisResult.TotalNum;
						ExpValA=ExpValA+ThisResult.ExpValA;
						ExpValB=ExpValB+ThisResult.ExpValB;
					
						ThisResA=ThisResult.I_A+(ThisResult.ExpValA/ThisResult.TotalNum);
						ThisResB=ThisResult.I_B+(ThisResult.ExpValB/ThisResult.TotalNum);
						I_A=[I_A;ThisResA];
						I_B=[I_B;ThisResB];
											
						if isfield(ThisResult,'numGain')
							numGain=numGain+ThisResult.numGain;
						else
							numGain=EqNum;
							
						end
						
						
						if ThisResult.A_better_B
							A_better_B_count=A_better_B_count+1;	
												
						end
					
						
						if ThisResult.Sign_better
							Sign_better_count=Sign_better_count+1;	
												
						end
						
					end
					
					%correct the I_A and I_B
					I_A=I_A-(ExpValA/EqNum);
					I_B=I_B-(ExpValB/EqNum);
					
					%calculate addition values
					RawDiff=I_A-I_B;
					InAB=mean(RawDiff);
					
					%code data: ModelA=0, ModelB=1
					ToCorrX=[zeros(size(I_A));ones(size(I_B))];
					ToCorrY=[I_A;I_B];
						
					%determine the correlation
					[r_value,p_value]=corrcoef(ToCorrX,ToCorrY);
									
					A_better_B = InAB > 0;
					Sign_better=p_value(1,2)<=CombRes.SignVal;
					
					
					%repack
					CombRes.TotalNum=EqNum;
					CombRes.numGain=numGain;
					CombRes.ExpValA=ExpValA;
					CombRes.ExpValB=ExpValB;
					CombRes.Pval_Correlation=p_value(1,2);
					CombRes.Rval=r_value(1,2);
					CombRes.A_better_B=A_better_B;
					CombRes.Sign_better=Sign_better;
					CombRes.I_A=I_A;
					CombRes.I_B=I_B;
					CombRes.RawDiff=RawDiff;
					CombRes.InAB=InAB;
									
					CombRes.A_better_B_count=A_better_B_count;
					CombRes.Sign_better_count=Sign_better_count;
					CombRes.TotalNumTest=TotalTests;
					CombRes.NaNCount=NaNCount;
					
					%save into struct
					ResFields=fieldnames(CombRes);
					for fC=1:numel(ResFields)
						TestResObj.SummaryResults.(TestNames{tC}).(TheModName{mC}).(ResFields{fC})= CombRes.(ResFields{fC});
					
					end
					%I use the loop here to avoid accidental overwritting of previous not related summaries
					
					
				end
			
			
			
				
				
			case 'None'
				%Nothing to do
		end
		
		
		
	end
	



end

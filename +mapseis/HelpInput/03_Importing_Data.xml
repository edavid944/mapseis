<?xml version="1.0" encoding="utf-8"?>
<doc name="Importing Data" briefdescription="Importing data from external data sources">
  <p>
    The data import functions use regular expressions (regexps) to parse
    the input data file.  These regexps are defined by match patterns and
    can return the match result in a number of ways.  In the import code
    defined in the application the input data is first broken up into a
    cell array of rows of data by matching the data up to the end of line
    character.  Each line is then matched against the pattern defined by
    the file format and the result inserted into a struct with named
    fields.  This produces a struct array which is converted to a single
    element struct with vector fields as the final stage of the processing.
  </p>
  <section name="Example" staticref="regexpExample">
    <p>
	Consider the case of importing data from the 
	<a href="http://www.data.scec.org/catalog_search/date_mag_loc.php">
	Southern California Earthquake Data Center</a> catalog.  A variety
	of export formats are provided, the default gives tab-delimited text
	of the form:
	<pre>
	#YYY/MM/DD HH:mm:SS.ss ET MAG  M    LAT     LON     DEPTH Q  EVID NPH NGRM
	2008/02/04 01:22:54.66 le 1.70 l    34.040 -117.252  15.5 A 14345884 53  606
	2008/02/04 02:34:17.44 le 1.41 l    33.921 -117.918   5.7 A 14345888 37  392
	2008/02/04 06:53:18.85 le 1.07 l    33.489 -116.478  12.9 A 14345892 30  259
	2008/02/04 08:57:12.03 le 1.51 l    34.802 -116.293   7.4 A 14345904 25  309
	2008/02/04 11:10:12.88 le 1.06 l    33.777 -116.908  16.4 A 14345912 25    0

	# Number of events: 5
	</pre>
    </p>
    <p>
	The goal of the import filter is to convert this table of text into
	a MATLAB structure with the columns present as column vectors in the
	struct, i.e.
	<pre>
	  data.mag = [1.70; 1.41; 1.07; 1.51; 1.06];
	  data.lat = [34.040; 33.921; 33.489; 34.802; 33.777];
	  etc
	</pre>
	The date and time fields are converted into a datenum and stored as
	a single value.  Textual fields, for example the event type (ET)
	column, are stored in cell arrays.
    </p>
    <p>
	The construction of the regular expressions for the file import
	starts with defining what expressions are present in each line of
	data.  
    </p>
    <p>
	For this example a row of data can be described as "A date field,
	tab, a time field, tab, an event type, tab, a magnitude floting
	point number, tab, ...".  The expression for a the rows in a
	particular data source should be stored as a file in the ms_Import
	directory off the package root directory, see
	ms_importSCECCatRead.m for this example.
    </p>
    <p>
      The next step is to define the regular expressions for each field.
	These patterns should be stored in the regular expression catalog,
	which is in the file regexpPatternCatalog.m in the private
	directory under the ms_Import directory.  This file is used to
	store 'building block' regular expressions that may be used by
	multiple import filters, such as the following:
	<pre>
	% Date in YYYY/MM/DD format
	patStruct.datePat = '\d\d\d\d/\d\d/\d\d';

	% Time in HH:mm:SS.ss
	patStruct.timePat = '\d\d:\d\d:\d\d\.\d\d';

	% Real number
	patStruct.realPat = '[+|-]?\d+((\.\d+)?)';
	
	...
	</pre>
	The syntax for defining regular expressions can be found in the
	MATLAB documentation for <a
	href="http://www.mathworks.com/access/helpdesk/help/techdoc/matlab_prog/f0-42649.html">
	Regular Expressions</a>.
    </p>
    <p>
	Once the regular expressions are constructed the data is processed
	by reading the text file into a character array and breaking up the character array
	into a set of cell arrays corresponding to the data rows.  The data
	rows are then matched individually against the row pattern.  This
	is done in readTable.m in the private directory under the
	ms_Import directory, as shown below:
	<pre>
	...
	lines = regexp(inStr,rowSepPat,'match');
	% Return the matches as a struct array, with field names as in the
	% rowPat  pattern
	matches = regexp(lines,rowPat,'names');
	...
	</pre>
	The 'matches' variable in the row above is a struct array.  This
	represents storage of the event data in an element-by-element
	manner.  The final step in the import filter converts this into a
	plane organization that is more suitable for use as a data store
	for this application.  See the section 'Organizing Data in
	Structure Arrays' in the <a
	href="http://www.mathworks.com/access/helpdesk/help/techdoc/matlab_prog/f2-88951.html#f2-41956">
	MATLAB Structures documentation</a> for a more detailed
	explanation.
    </p>
  </section>
  <section name="Writing new filters" staticref="regexpNewFilters">
    <p>
	New import filters should be constructed from existing regexp
	patterns in the regexp catalog as far is reasonably possible.
	Source-dependent regular expressions should be added in the import
	filter m-file in the ms_Import directory, unless it is likely that
	multiple sources will use the same expressions in which case they
	can be added to the regular expression catalog in the
	ms_Import/private.
    </p>
  </section>
</doc>

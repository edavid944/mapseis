%% Script that builds the documentation for DocTools

%  Copyright 2006-2008 The MathWorks, Inc.
%  $Revision: 59 $    $Date: 2008-10-13 11:31:44 +0100 (Mon, 13 Oct 2008) $

% Get the version information

ver = '0.5';
verDate = '2008/02/04';
verStr = [ver, ' (', verDate, ')'];

% Work out where to put it
srcDir = fileparts( mfilename('fullpath') );
dstDir = fullfile( fileparts(srcDir), 'ms_Help' );

% Now build the doc
docBuild( 'MapSeis', verStr, srcDir, dstDir );

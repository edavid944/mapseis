classdef SliceViewer_mk3 < handle
	%This is a prototype for a viewer with allows to view a Volume Slice by
	%Slice. It is currently in the forecast package, but maybe not staying
	%there. Also it is just test class, and will likely be changed quite
	%a lot.
	%It is also an experiment how well the approach with plotting everything
	%and showing only the wanted part works, especially with large forecasts
	
	%Second version, does not plot all times at once to keep plotting times
	%lower for large data sets
	
	%third version, reworked GUI and support for coastlines etc
	

	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	properties
        	Name
        	Version
        	Forecast
        	Datastore
        	Filterlist
        	WorkMode
        	MagList
        	DepthList
        	TimeList
        	AxisVectors
        	TitleRawParts
        	PlotHandles
        	WindowHandle
        	TitleHandle
        	OverlayHandle
        	plotAxis
        	MagSlider
        	MagText
        	DepthSlider
        	DepthText
        	CoarseTimeSelect
        	TimeInterval
        	TimeSlider
        	TimeText
        	PlotMenu
        	PlotOption
        	LogMode
		AllReady
		CurrentPlotRange
		CurrentTimePeriod
		TimeSliceSize  
		HandPos
		CustomColor
		
		
        end
    

    
        methods
        	function obj=SliceViewer_mk3(Name)
        		%constructor: only inits the most important fields
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(16,8,1))
        		end
        	
        		
        		obj.Name=Name;
        		obj.Version='0.7';
        		obj.Forecast=[];
        		obj.Datastore=[];
        		obj.Filterlist=[];
        		obj.WorkMode=struct(	'Space',false,...
        					'Depth',false,...
        					'Mag',false,...
        					'Time',false);
        		
        		obj.MagList=[];
        		obj.DepthList=[];
        		obj.TimeList=[];
        		obj.AxisVectors={};
        		obj.TitleRawParts={};
        		obj.PlotHandles=[];
        		obj.WindowHandle=[];
        		obj.TitleHandle=[];
        		obj.OverlayHandle=[];
        		obj.plotAxis=[];
        		obj.MagSlider=[];
        		obj.MagText=[];
        		obj.DepthSlider=[];
        		obj.DepthText=[];
        		obj.CoarseTimeSelect=[];
        		obj.TimeInterval={};
        		obj.TimeSlider=[];
        		obj.TimeText=[];
        		obj.PlotMenu=[];
        		obj.PlotOption=[];
        		obj.LogMode=false;
        		obj.AllReady=false;
        		obj.CurrentPlotRange=[];
        		obj.CurrentTimePeriod=[];
        		obj.TimeSliceSize=10;
        		obj.HandPos=[1,1,1];
        		obj.CustomColor=jet(128);
        		
        		%use custom color if existing
        		try
        			fs=filesep;
        			load(['.',fs,'AddOneFiles',fs,'CustomColor',fs,'LightJetColor.mat']);
        			obj.CustomColor=LightJetCol;
        		end
        		
        		
        	end
        	
        	
        	function TypicalStart(obj,Forecast,Datastore,Filterlist)
        		import mapseis.filter.*;
        		%Filterlist will be  cloned to freely edit it
        		
        		if nargin<3
				obj.AddForecast(Forecast);
				obj.Datastore=[];
				obj.Filterlist=[];
				obj.CreateViewer;
				obj.InitPlots;
				
        		else
        			obj.AddForecast(Forecast);
				obj.Datastore=Datastore;
				obj.Filterlist=FilterListCloner(Filterlist);
				obj.CreateViewer;
				obj.InitPlots;
				
        		end
        	       		
        	end
        	
        	
        	function AddForecast(obj,Forecast)
        		%just for adding a forecast
        		
        		if ~isempty(obj.WindowHandle)
        			%close window if existing
        			%(Will come later)
        		
        		end
        		
        		%save it
        		obj.Forecast=Forecast;
        		obj.WorkMode=obj.Forecast.Dimension;
        		
        		[SpaceGrid DepthGrid  MagGrid] = obj.Forecast.getAxisMesh;
        		
        		%init needed data
        		obj.MagList=obj.Forecast.MagBins;
        		obj.DepthList=obj.Forecast.DepthBins;
        		obj.TimeList=obj.Forecast.TimeVector;
        		obj.AxisVectors=SpaceGrid;
        		obj.TitleRawParts{1}=obj.Forecast.Name;
        		obj.TitleRawParts{2}=', Magnitude: ';
        		obj.TitleRawParts{3}=' ';
        		obj.TitleRawParts{4}=', Depth: ';
        		obj.TitleRawParts{5}=' ';
        		obj.TitleRawParts{6}=', Time: ';
        		obj.TitleRawParts{7}=' ';
        		
        		%clear old stuff (if it exists)
        		obj.PlotHandles=[];
        		obj.WindowHandle=[];
        		obj.TitleHandle=[];
        		obj.plotAxis=[];
        		obj.MagSlider=[];
        		obj.MagText=[];
        		obj.DepthSlider=[];
        		obj.DepthText=[];
        		
        		
        	end
        	
        	
        	function CreateViewer(obj)
        		import mapseis.util.*;	
        	
        		%maybe check if there is an old window existing (later)
        		
        		%check if a forecast is loaded
        		if isempty(obj.Forecast)
        			error('No Forecast loaded, cannot create Viewer');
        		end	
        		
        		
        		%create a window 
        		obj.WindowHandle=figure;
        		
        		%set default size
        		set(obj.WindowHandle,'Name','Sliceviewer','Units','normalized',...
        			'Position',[0.2 0.35 0.6 0.6]);
        		
        		%create axis
        		axis;
        		obj.plotAxis=gca;
        		obj.ApplyAxisSetting;
        		
        		
        		%create the needed sliders
        		if obj.WorkMode.Mag
        			obj.MagSlider=uicontrol('Style', 'slider',...
        						'Min',1,'Max',numel(obj.MagList),'Value',1,...
        						'Units','normalized',...
        						'Position', [0.15,0.025 0.6 0.1],...
        						'Callback', @(s,e) obj.ShowPlotSlider());  
        			
        			LabText1=uicontrol(	'Style', 'text',...
        					  	'String','Magnitude: ',... 
        					  	'Units','normalized',...
        					  	'Position', [0.15 0.045 0.6 0.05],...
        					  	'FontSize',18,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8])			
        			
        			obj.MagText=uicontrol(	'Style', 'text',...
        					  	'String',num2str(obj.MagList(1)),... 
        					  	'Units','normalized',...
        					  	'Position', [0.5 0.05 0.05 0.05],...
        					  	'FontSize',24,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8],...
        					  	'ForegroundColor','r')
        			
        		end
        		
        		if obj.WorkMode.Depth
        			obj.DepthSlider=uicontrol('Style', 'slider',...
        						'Min',1,'Max',numel(obj.DepthList),'Value',1,...
        						'Units','normalized',...
        						'Position', [0.8,0.275 0.1 0.6],...
        						'Callback', @(s,e) obj.ShowPlotSlider());   
        						
        			LabText2=uicontrol(	'Style', 'text',...
        					  	'String','Depth: ',... 
        					  	'Units','normalized',...
        					  	'Position', [0.795 0.18 0.2 0.05],...
        					  	'FontSize',18,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8])			
        			
        			obj.DepthText=uicontrol('Style', 'text',...
        					  	'String',num2str(obj.DepthList(1)),... 
        					  	'Units','normalized',...
        					  	'Position',[0.795 0.14 0.2 0.05],...
        					  	'FontSize',24,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8],...
        					  	'ForegroundColor','r')			
        						
        		end
        		
        		
        		if obj.WorkMode.Time
        			
        			%generate Timeinterval list
        			TIntVal=1:obj.TimeSliceSize:numel(obj.TimeList);
        			if TIntVal(end)<numel(obj.TimeList)
        				TIntVal(end+1)=numel(obj.TimeList);
        			end
        			
        			for i=1:numel(TIntVal)-1
        				%RangeString=[num2str(decyear(datevec(obj.TimeList(TIntVal(i))))),' to ',...
        				%		num2str(decyear(datevec(obj.TimeList(TIntVal(i+1)))))];
        				RangeString=[datestr(obj.TimeList(TIntVal(i))),' to ',...
        						datestr(obj.TimeList(TIntVal(i+1)))];		
        				obj.TimeInterval{i,1}=RangeString;
        				obj.TimeInterval{i,2}=[TIntVal(i),TIntVal(i+1)];
        				obj.TimeInterval{i,3}=[obj.TimeList(TIntVal(i)),obj.TimeList(TIntVal(i+1))];
        			end
        			
        			
        		
        			
        			%create additional GUI
        			obj.CoarseTimeSelect=uicontrol('Style', 'popupmenu',...
        						'String',obj.TimeInterval(:,1),...
        						'Value',1,...
        						'Units','normalized',...
        						'Position', [0.15,0.2 0.6 0.1],...%%%POSITION NEEDED
        						'Tag','Importfilterselect',...
        						'Callback', @(s,e) obj.ShowPlotSlider());   	
        		
        			obj.TimeSlider=uicontrol('Style', 'slider',...
        						'Min',1,'Max',obj.TimeSliceSize+1,'Value',1,...
        						'Units','normalized',...
        						'Position', [0.15,0.125 0.6 0.1],...
        						'Callback', @(s,e) obj.ShowPlotSlider());   
        						
        			LabText3=uicontrol(	'Style', 'text',...
        					  	'String','Time: ',... 
        					  	'Units','normalized',...
        					  	'Position', [0.15 0.145 0.6 0.05],...
        					  	'FontSize',18,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8])			
        			
        			obj.TimeText=uicontrol('Style', 'text',...
        					  	'String',datestr(obj.TimeList(1)),... 
        					  	'Units','normalized',...
        					  	'Position', [0.5 0.15 0.1 0.05],...
        					  	'FontSize',24,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8],...
        					  	'ForegroundColor','b')			
        			%'String',num2str(decyear(datevec(obj.TimeList(1)))),... 			
        		end
			
			obj.BuildMenu;        		
        		
        	end
        	
        	
        	function BuildMenu(obj)
        		%builds the menu for the overlay
        		obj.PlotMenu = uimenu( obj.WindowHandle,'Label','- Plot Options');
        		
        		uimenu(obj.PlotMenu,'Label','plot Coastlines','Separator','on',... 
				'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
			uimenu(obj.PlotMenu,'Label','plot Country Borders',...
				'Callback',@(s,e) obj.TooglePlotOptions('Border'));
			uimenu(obj.PlotMenu,'Label','plot Earthquake Locations',...
				'Callback',@(s,e) obj.TooglePlotOptions('EQ'));	
		%	uimenu(obj.PlotMenu,'Label','apply Mag/Depth Filter',...
		%		'Callback',@(s,e) obj.TooglePlotOptions('FiltCut'));		
        		
			uimenu(obj.PlotMenu,'Label','Zmap style','Separator','on',... 
				'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotMenu,'Label','Medium Quality Plot',...
				'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotMenu,'Label','High Quality Plot',...
				'Callback',@(s,e) obj.setqual('hi'));
        		
			%set the checks right
			obj.TooglePlotOptions('-chk');
			obj.setqual('chk');
        	end
        	
        	
        	function TooglePlotOptions(obj,WhichOne)
			%This functions sets the toogles in this modul
			%WhichOne can be the following strings:
			%	'Border':	Borderlines
			%	'Coast'	:	Coastlines
			%	'EQ'	:	Earthquake locations
			%	'-chk'	:	This will set all checks in the menu
			%			to the in toggles set state.
			
			CoastMenu = findobj(obj.PlotMenu,'Label','plot Coastlines');
			BorderMenu = findobj(obj.PlotMenu,'Label','plot Country Borders');
			EqMenu = findobj(obj.PlotMenu,'Label','plot Earthquake Locations');
			%SliceMenu = findobj(obj.PlotMenu,'Label','apply Mag/Depth Filter');
			
			if isempty(obj.PlotOption)
        			%default options
        			obj.PlotOption=struct(	'CoastLine',true,...
        						'Borders',true,...
        						'EQ',true,...
        						'EQ_Quality','hi',...
        						'SliceFilter',false);
        		end
			
			switch WhichOne
				case '-chk'
					if obj.PlotOption.CoastLine
						set(CoastMenu, 'Checked', 'on');
					else
						set(CoastMenu, 'Checked', 'off');
					end
					
					if obj.PlotOption.Borders
						set(BorderMenu, 'Checked', 'on');
					else
						set(BorderMenu, 'Checked', 'off');
					end
					
					if obj.PlotOption.EQ
						set(EqMenu, 'Checked', 'on');
					else
						set(EqMenu, 'Checked', 'off');
					end
					
					
					%if obj.PlotOption.SliceFilter
					%	set(SliceMenu, 'Checked', 'on');
					%else
					%	set(SliceMenu, 'Checked', 'off');
					%end
					
					
				case 'Border'
					obj.PlotOption.Borders=~obj.PlotOption.Borders;
					
					if obj.BorderToogle
						set(CoastMenu, 'Checked', 'on');
					else
						set(CoastMenu, 'Checked', 'off');
					end
					
					obj.PlotOverlay;
				
				case 'Coast'
					obj.PlotOption.CoastLine=~obj.PlotOption.CoastLine;
					
					if obj.CoastToogle
						set(BorderMenu, 'Checked', 'on');
					else
						set(BorderMenu, 'Checked', 'off');
					end
					
					obj.PlotOverlay;
				
				case 'EQ'
					obj.PlotOption.EQ=~obj.PlotOption.EQ;
					
					if obj.EQToogle
						set(EqMenu, 'Checked', 'on');
					else
						set(EqMenu, 'Checked', 'off');
					end
					
					obj.PlotOverlay;
				
					
			%	case 'FiltCut'
			%		obj.PlotOption.SliceFilter=~obj.PlotOption.SliceFilter;
			%		
			%		if obj.EQToogle
			%			set(SliceMenu, 'Checked', 'on');
			%		else
			%			set(SliceMenu, 'Checked', 'off');
			%		end
					
			%		obj.PlotOverlay;
						
			end
		
		
		end
        	
        	
		function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
			Menu2Check(3) = findobj(obj.PlotMenu,'Label','Zmap style');
			Menu2Check(2) = findobj(obj.PlotMenu,'Label','Medium Quality Plot');
			Menu2Check(1) = findobj(obj.PlotMenu,'Label','High Quality Plot');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
 				case 'low'
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotOption.EQ_Quality='low';
 					obj.PlotOverlay;
                		
				case 'med'
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotOption.EQ_Quality='med';
 					obj.PlotOverlay;
					
				case 'hi'
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotOption.EQ_Quality='hi';
 					obj.PlotOverlay;
					
				case 'chk'
					StoredQuality = obj.PlotOption.EQ_Quality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
		end
		
        	
        	function ApplyAxisSetting(obj)
        		%default optical stuff
        		set(obj.plotAxis,'LineWidth',3,'FontSize',18,'FontWeight','bold','Box','on');
        		%positioning the axis
        		set(obj.plotAxis,'Units','normalized','Position', [0.1 0.35 0.7 0.55]);
        		
        		%set labels
        		xlab=xlabel('Longitude  ');
        		ylab=ylabel('Latitude  ');
        		set([xlab,ylab],'FontSize',18,'FontWeight','bold');
        		
        		%set generic title
        		obj.TitleHandle=title([obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
        					obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},...
        					obj.TitleRawParts{7},'   ']);
        		set(obj.TitleHandle,'FontSize',24,'FontWeight','bold');
        	end
        	
        	
        	
        	function InitPlots(obj,TimeInt)
        		%plot everything
        		%the plot routine may change later to incoporate
        		%more settings and also to allow coastlines, earthquakes
        		%and so one.
        		
        		%now allows to plot only a range of time slices
        		if nargin<2
        			TimeInt=1;
        		end
        		
        		%"delete" old plots
        		axes(obj.plotAxis);
        		h1=plot(mean(obj.AxisVectors{1}(:)),mean(obj.AxisVectors{2}(:)),'w');
        		set(h1,'Visible','off');
        		
        		overMin=[];
        		overMax=[];
        		disp(TimeInt)
        		
        		%get the times to plot %HERE
 
        		if obj.WorkMode.Time
        			%set to current time range
        			obj.CurrentTimePeriod=TimeInt;
        			obj.CurrentPlotRange=obj.TimeInterval{TimeInt,2};
        			
        		else
        			obj.CurrentPlotRange=[1 1];
        		end       			
        		
        		
        		MaxPlot=obj.CurrentPlotRange(2)-obj.CurrentPlotRange(1);
        		
			if MaxPlot==0
				MaxPlot=1;
			end	
        		
        		%empty handles
        		obj.PlotHandles={};
        		
        		hold on;
        		for i=1:numel(obj.MagList)
        			for j=1:numel(obj.DepthList)
        				for k=1:MaxPlot
												
						%T-M-D coordinates
						TransTime=obj.CurrentPlotRange(1)+(k-1);
						
						CastTime=obj.TimeList(TransTime);
						Magnitude=obj.MagList(i);
						Depth=obj.DepthList(j);
						
						%get data
						[ForeSlice ForeTime ForeLength] = obj.Forecast.get2DSlice(CastTime,Magnitude,Depth);
						
						if obj.LogMode
							%avoid infinity
							ForeSlice(ForeSlice==0)=realmin;
							ForeSlice=log10(ForeSlice);
						end
						
						%go for min and max (for unified colorscale)
						overMin=min([overMin,min(ForeSlice(:))]);
						overMax=max([overMax,max(ForeSlice(:))]);
						
						%plot it
						axes(obj.plotAxis);
						obj.PlotHandles{i,j,k}=image(obj.AxisVectors{1}(1,:),obj.AxisVectors{2}(:,1),ForeSlice);
															
						%some additional plot settings
						set(obj.PlotHandles{i,j,k},'CDataMapping','scaled');
						
						%hide it	
						set(obj.PlotHandles{i,j,k},'Visible','off')
					end
        			end
        		end
        		
        		%set coloraxis
        		%caxis([overMin overMax]);
        		
        		myBar=colorbar;
        		
        		%set to the define colorbar
        		colormap(obj.CustomColor);
        		
        		%here could be the additional stuff needed to be plotted
        		
        		hold off;
        		
        		%set axis limit right
        		xlim([min(obj.AxisVectors{1}(:)),max(obj.AxisVectors{1}(:))]);
        		ylim([min(obj.AxisVectors{2}(:)),max(obj.AxisVectors{2}(:))]);
        		
        		%aspect ratio&axis direction
        		set(obj.plotAxis,'YDir','normal');
        		latlim = get(obj.plotAxis,'Ylim');
        		set(obj.plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
        		
        		%reapply axis setting, some version just override them
        		obj.ApplyAxisSetting;
        		
        		obj.AllReady=true;
        		
        		%update position (not ideal but it should work all the time
        		%TimeRange=obj.CurrentPlotRange(1):obj.CurrentPlotRange(2);
        		%TimerPos=find(TimeRange==CurTime);
        		%obj.HandPos(3)=TimerPos(1);
        		
        		if isempty(obj.HandPos)
        			obj.HandPos=[1 1 1];
        		end
        		
        		obj.PlotOverlay;
        		
        		%set first image visible
        		set(obj.PlotHandles{obj.HandPos(1),obj.HandPos(2),obj.HandPos(3)},'Visible','on');
        	end
        	
        	
        	function PlotOverlay(obj)
        		%plots the coastlines and eq if selected
        		
        		%NOT FULLY FINISHED: I have to find a way to clever plot
        		%the eqs time&mag&depth (or at least time) dependent.
        		%there are just a lot of handles that's the problem
        		
        		import mapseis.plot.*;
        	
        		xlimiter=xlim;
        		ylimiter=ylim;
        		
        		%check what is available
        		if isempty(obj.Datastore)
        			disp('No Datastore - No CoastLine');
        			return
        		end
        		
        		       		
        		%check what is wanted
        		if isempty(obj.PlotOption)
        			%default options
        			obj.PlotOption=struct(	'CoastLine',true,...
        						'Borders',true,...
        						'EQ',false,...
        						'EQ_Quality','hi',...
        						'SliceFilter',false);
        		end
        		
        		CoastToggle=obj.PlotOption.CoastLine;
        		BorderToggle=obj.PlotOption.Borders;
        		EQToggle=obj.PlotOption.EQ;
        		EQ_Quali=obj.PlotOption.EQ_Quality;
        		%SliceFiltToggle=obj.PlotOption.SliceFilter;
        		
        		
        		%Eq not allowed now, I have to find a solution for
        		%the slider-Eq interaction
        		EQToggle=false;
        		
        		if (~CoastToggle&~BorderToggle&~EQToggle)
        			%nothing has to be done
        			return
        		end
        		
        		if ~isempty(obj.Filterlist)
        			
        		%	TimeFilt=obj.Filterlist.getByName('Time');
        		%	TimeFilt.setRange(Range,TimeInt);			
        			selected=[];
        		
        		else
        			selected=[];
        		
        		end
        		
        		
        		%TODO: Maybe a color switch black or white for the Coast 
        		%and Border is an idea
        		
        		%prepare data
        		CoastConf= struct(	'PlotType','Coastline',...
						'Data',obj.Datastore,...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'LineStylePreset','Fatline',...
						'Colors','white');
				
			BorderConf= struct(	'PlotType','Border',...
						'Data',obj.Datastore,...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'LineStylePreset','dotted',...
						'Colors','black');
						
						
			EqConfig= struct(	'PlotType','Earthquakes',...
						'Data',obj.Datastore,...
						'PlotMode','old',...
						'PlotQuality',EQ_Quali,...
						'SelectionSwitch','selected',...
						'SelectedEvents',selected,...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'C_Axis_Limit','auto',...
						'LegendText','Earthquakes');
				
					
					
						
						
        		
        		%plot
        		
        		%erase old plots
        		if ~isempty(obj.OverlayHandle)
        			for i=1:numel(obj.OverlayHandle)
        				try
        					delete(obj.OverlayHandle(i));
        				end	
        			end
        			obj.OverlayHandle=[];
        		end
        		
        		
        		hold on
        		if CoastToggle
        			[obj.OverlayHandle(1) CoastEntry] = PlotCoastline(obj.plotAxis,CoastConf);
        		end
        		
        		
        		if BorderToggle
        			[obj.OverlayHandle(2) BorderEntry] = PlotBorder(obj.plotAxis,BorderConf);
        		end
        		
        		if EQToggle
        			[obj.OverlayHandle(3) entry1] = PlotEarthquake(plotAxis,EqConfig);
        		end
        		
        		hold off
        		
     
        		
        		
        		
        		
        	end
        	
        	
        	
        	
        	
        	
        	
        	
        	function ShowPlotSlider(obj)
        		%shows the by the sliders selected plot
        		%I maybe add an addition function for a other gui element
        		
        		import mapseis.util.*;	
        		
        		%check if it is save to do the work
        		if obj.AllReady
        			%Default position needed if only one is variable
        			MagPos=1;
        			DepthPos=1;
        			TimePos=1;
        			
        			if obj.WorkMode.Mag
        				%get slider position
        				MagPos=round(get(obj.MagSlider,'Value'));
        				obj.HandPos(1)=MagPos;
        				
					%set MagText
					set(obj.MagText,'String',num2str(obj.MagList(MagPos)));
					
					%change Title part
					obj.TitleRawParts{3}=num2str(obj.MagList(MagPos));
					
					%set back the rounded value
					set(obj.MagSlider,'Value',MagPos);
					        				
        			end 
        			
        			
        			if obj.WorkMode.Depth
        				%get slider position
        				DepthPos=round(get(obj.DepthSlider,'Value'));
        				obj.HandPos(2)=DepthPos;
        				
					%set MagText
					set(obj.DepthText,'String',num2str(obj.MagList(DepthPos)));
					
					%change Title part
					obj.TitleRawParts{5}=num2str(obj.MagList(DepthPos));
					
					%set back the rounded value
					set(obj.DepthSlider,'Value',DepthPos);
        			end
        			
        			if obj.WorkMode.Time
        				%get slider position
        				TimeSlidePos=round(get(obj.TimeSlider,'Value'));
        				
        				%get time period
        				TimePeriod=get(obj.CoarseTimeSelect,'Value');
        				
        				%calculate current selected time
        				TimePos=(TimePeriod-1)*obj.TimeSliceSize+TimeSlidePos-1;
        				if TimePos>numel(obj.TimeList)
        					TimePos=numel(obj.TimeList);
        				end
        				
        				
        				
					%set TimeText
					%set(obj.TimeText,'String',num2str(decyear(datevec(obj.TimeList(TimePos)))));
					set(obj.TimeText,'String',datestr(obj.TimeList(TimePos)));
					
					%change Title part
					%obj.TitleRawParts{7}=num2str(decyear(datevec(obj.TimeList(TimePos))));
					obj.TitleRawParts{7}=datestr(obj.TimeList(TimePos));
					
					%set back the rounded value
					set(obj.TimeSlider,'Value',TimeSlidePos);
					        
					
					%UNFINISHED
					obj.HandPos(3)=TimeSlidePos;
					
					disp(TimePeriod)
					disp(obj.CurrentTimePeriod)
					%check if still in range, if not re-init plot
					if TimePeriod~=obj.CurrentTimePeriod
						obj.InitPlots(TimePeriod);
						
					end
																		
        			end 
        			
        			
        			%change title
        			try
        				set(obj.TitleHandle,'String',[obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
        					obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},obj.TitleRawParts{7},'   ']);
        			catch
        				%Title somehow deleted ->recreate
        				obj.TitleHandle=title([obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
        					obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},obj.TitleRawParts{7},'   ']);
        				set(obj.TitleHandle,'FontSize',24,'FontWeight','bold');
        			end
        			
        			
        			%set all plots to hidden
        			obj.HideAllPlots;
        			
        			%set the selected plot to visible
        			set(obj.PlotHandles{obj.HandPos(1),obj.HandPos(2),obj.HandPos(3)},'Visible','on');
        			
        			
        			
        			
        		end
        	
        	end
        	
        	
        	function HideAllPlots(obj)
        		%Hides every plot
        		if ~isempty(obj.PlotHandles)
        			for i=1:numel(obj.PlotHandles)
        				set(obj.PlotHandles{i},'Visible','off');
        			end
        		end
        	end
        	
        	
        	
        	
        	
        	
        end



end	

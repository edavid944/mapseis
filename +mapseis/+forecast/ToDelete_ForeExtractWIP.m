	%just a prototype from the forecast format, to make a rework a bit easier.
	%It will be added to the object when finished
	
	
	function [ForeSlice ForeTime ForeLength] = get2DSlice(obj,CastTime,Magnitude,Depth)
        		%returns a slice of the forecast as 2D matrix.
        		%Magnitude and/or Depth can be left empty if the grid has only one 
        		%bin for one of them.
        		%For Magnitude and Depth also the keyword 'all' can be used, in this 
        		%case a summation of all grid cells in that dimension will be returned.
        		
        		%search if the forecast can be found
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVec-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
			%condition for 3D (either Mag or Depth but not both)
			Cond3d=(obj.Dimension.Depth | obj.Dimension.Mag) & ~(obj.Dimension.Depth & obj.Dimension.Mag);
			%condition to stop and return an error
			BreakCond=(obj.Dimension.Depth&isempty(Depth))|(obj.Dimension.Mag&isempty(Magnitude)); 
			
			if strcmp(obj.ForecastType.Type,'None')
				error('No forecast set')
			end
			
			%Maybe possible to simplify this a bit
			
				
			RawGrid=obj.ForecastData{FindInTime};
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);
			
			%use the masking if needed
			if ~isempty(obj.DataMask)
				if obj.EqualMask
					TheMask=obj.DataMask;
					
				else
					TheMask=obj.DataMask{FindInTime};
				end
				
				RawGrid = MatrixCorrector(RawGrid,~TheMask,'set');
			end
			
			
			if ~obj.Dimension.Depth&~obj.Dimension.Mag
				%only 2D data, the easiest case

				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice RawVolume] = BuildRawGrid(false);
				ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
				
			elseif Cond3d
				%3D case
				if BreakCond
					error('Depth or Mag input variable not defined')
				end
			
				if obj.Dimension.Depth
					if strcmp(Depth,'all')
						%sum up
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(false);
						RawSlice=ForeSlice;
						
						%first step
						ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
						
						%additonal ones
						for i=2:numel(obj.DepthBins)
							NewSlice=RawSlice;
							NewSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs+(SliPos-1))
							ForeSlice=SumFullDistro(ForeSlice,NewSlice);
							
						end
					else
						%single field
						SliPos=find(obj.DepthBins==Depth);
						
						if isempty(SliPos)
							error('Depth not found');
						end
						
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(false);
						ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs+(SliPos-1));
						%PLEASE TEST IT
														
					end
					
					
				elseif obj.Dimension.Mag
					if strcmp(Magnitude,'all')
						%sum up
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(false);
						RawSlice=ForeSlice;
						
						%first step
						ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
						
						%additonal ones
						for i=2:numel(obj.MagBins)
						
							NewSlice=RawSlice;
							NewSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs+(SliPos-1))
							ForeSlice=SumFullDistro(ForeSlice,NewSlice);
						end
					else
						%single field
						SliPos=find(obj.MagBins==Magnitude);
						
						if isempty(SliPos)
							error('Magnitude not found');
						end
						
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(false);
						ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs+(SliPos-1));
						%PLEASE TEST IT
														
					end
				end
				
			else
				%full 4D (most difficult case with two jumps
				%ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
				[ForeSlice RawVolume] = BuildRawGrid(true);
				
				if BreakCond
					error('Depth or Mag input variable not defined')
				end
				
				
				if ~strcmp(Depth,'all')&~strcmp(Magnitude,'all')
					%no summation needed
					MagPos=find(obj.MagBins==Magnitude);
					
					if isempty(MagPos)
						error('Magnitude not found');
					end
					
					DepthPos=find(obj.DepthBins==Depth);
					
					if isempty(DepthPos)
						error('Depth not found');
					end
					
					NewSelID=obj.SelectedIDs{1}{DepthPos}+(MagPos-1)
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
					
				elseif strcmp(Depth,'all')&~strcmp(Magnitude,'all')
					%sumamation of the depth needed
					MagPos=find(obj.MagBins==Magnitude);
					
					if isempty(MagPos)
						error('Magnitude not found');
					end
					
					%Dummy slice
					RawSlice=ForeSlice;
					
					%first element to prevent zeros
					NewSelID=obj.SelectedIDs{1}{1}+(MagPos-1)
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
					
					%now the rest
					for i=2:numel(obj.DepthBins)
						NewSelID=obj.SelectedIDs{1}{i}+(MagPos-1)
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						%again the annoying substep
						NewSlice=RawSlice;
						NewSlice(obj.SelectedNodes{1}{i})=RawGrid(NewSelID);
						ForeSlice=SumFullDistro(ForeSlice,NewSlice);
					end
					
				
				elseif ~strcmp(Depth,'all')&strcmp(Magnitude,'all')
					%summation of the magnitude
					DepthPos=find(obj.DepthBins==Depth);
					
					if isempty(DepthPos)
						error('Depth not found');
					end
					
					%Dummy slice
					RawSlice=ForeSlice;
					
					%first element to prevent zeros
					NewSelID=obj.SelectedIDs{1}{DepthPos};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
					
					%now the rest
					for i=2:numel(obj.MagBins)
						NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1);
						
						%again the annoying substep
						NewSlice=RawSlice;
						NewSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
						ForeSlice=SumFullDistro(ForeSlice,NewSlice);
					end
					
					
				else
					%summation of magnitude and depth
					
					%first do a prototype slice
					NewSelID=obj.SelectedIDs{1}{1};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
					MrNan = MatrixCorrector(ForeSlice,[],'get');
					%MrNan=isnan(ForeSlice);
					
					%make a new Sum slice with zeros
					ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
					RawSlice=ForeSlice;
					
					%now sum them all
					for d=1:numel(obj.DepthBins)
						for m=1:numel(obj.MagBins)
							NewSelID=obj.SelectedIDs{1}{d}+(m-1);
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							NewSlice=RawSlice;
							%again the annoying substep
							NewSlice(obj.SelectedNodes{1}{d})=RawGrid(NewSelID);
							ForeSlice=SumFullDistro(ForeSlice,NewSlice);	
							
						end
					end
					
					%Kill all zeros which should not be existing
					%ForeSlice(MrNan)=NaN;
					ForeSlice=MatrixCorrector(ForeSlice,MrNaN,'set');
					
				end
				
				
				
				
			
			end
					
			
        			
        			
        	
        		
        		
        		
        	end
        	
        	
        	
        	function [ForeVolume ForeTime ForeLength] = get3DVolume(obj,CastTime,Magnitude,Depth)
        		%same as get2DSlice, but for Volumes, at least 3D dimensions (without time) 
        		%are needed this function to work
        		
        		%Caution some of the stuff is experimental and it is not sure if it works in every
        		%case. For instace could the depth summation fail in  cases the region is not
        		%cylindrical (which isn't allowed right now anyway)
        		
        		if ~obj.Dimension.Space&(~obj.Dimension.Depth|~obj.Dimension.Mag)
        			%missing a necessary dimension
        			error('Not enough dimension available'); 
        		end
        		
        		
        		%search if the forecast can be found
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVec-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
        		%just in case
        		if strcmp(obj.ForecastType.Type,'None')
				error('No forecast set')
			end
        		
        		
        
			RawGrid=obj.ForecastData{FindInTime};
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);
			
			
			%use the masking if needed
			if ~isempty(obj.DataMask)
				if obj.EqualMask
					TheMask=obj.DataMask;
					
				else
					TheMask=obj.DataMask{FindInTime};
				end
				
				RawGrid = MatrixCorrector(RawGrid,~TheMask,'set');
			end
			
			
			
			try
				%ForeVolume=NaN(size(obj.SelectedNodes{2}));
				%ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
				[ForeSlice ForeVolume] = BuildRawGrid(true)
			catch
				%probalby 2D
				%ForeVolume=[];
				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice ForeVolume] = BuildRawGrid(false)
			end
			
			
			if obj.Dimension.Depth&obj.Dimension.Mag
				%full 4D case
				BreakCond = 	(isempty(Depth)&isempty(Magnitude))|...
						(~isempty(Depth)&~isempty(Magnitude));
					
				%possible errors
				if BreakCond
					error('Wrong Depth and/or Magnitude parameter')
				end
				
				if strcmp(Depth,'all')&strcmp(Magnitude,'all') 
					error('only on parameter can be set to "all"');
				end
				
				
				%different cases (summation over one dimension or a specific slide)
				if strcmp(Depth,'all')
					ForeVolume=[];
					
					%again to prevent unnecessary zeros
					Tempslice=ForeSlice;
					NewSelID=obj.SelectedIDs{1}{1};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					
					Tempslice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
					%MrNan=isnan(Tempslice);
					MrNan = MatrixCorrector(Tempslice,[],'get');
					%ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
					[ForeSlice RawVolume] = BuildRawGrid(true,true);
					
					%all depth have to be summed and the new "depth" is the magnitude
					for i=1:numel(obj.MagBins)
						
						DepthSlice=ForeSlice;
						
						
						%first step (again the slighty annoying extra step has to be done)
						NewSelID=obj.SelectedIDs{1}{1}+(i-1);
						DepthSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
						
						for j=2:numel(obj.DepthBins)
							NewSlice=ForeSlice;
							NewSelID=obj.SelectedIDs{1}{j}+(i-1);
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							NewSlice(obj.SelectedNodes{1}{j})=RawGrid(NewSelID);
							DepthSlice=SumFullDistro(DepthSlice,NewSlice);
						
						end
						
						%reapply nan
						%DepthSlice(MrNan)=NaN;
						DepthSlice=MatrixCorrector(DepthSlice,MrNan,'set');
						%save in in 3D matrix
						ForeVolume(:,:,i)=DepthSlice;
						
						
				
					end
					
					
					
					
				elseif strcmp(Magnitude,'all') 
					%One of the real 3D cases
					
					%It has to be done a bit more complicated than I originally did think
					%because using a matrix with logical matrix results in a matrix, expect
					%you write an other matrix into it (sounds wierd I know)
					RawVol=ForeVolume;
					
					%first step
					NewSelID=obj.SelectedIDs{2};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
					
					for i=2:numel(obj.MagBins)
						NewSlide=RawVol;
						NewSelID=obj.SelectedIDs{2}+(i-1);
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						NewSlide(obj.SelectedNodes{2})=RawGrid(NewSelID);
						
						%now sum up the single volumes without any selector
						ForeVolume=SumFullDistro(ForeVolume,NewSlide);
						
					end
					
					
				elseif ~isempty(Depth)
					%Similar to the all case but with a specified depth
					ForeVolume=[];
					DepthPos=find(obj.DepthBins==Depth);
					
					if isempty(DepthPos)
						error('Depth not found');
					end
											
					%get the magnitude slices
					for i=1:numel(obj.MagBins)
						
						DepthSlice=ForeSlice;
						
						NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1)
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						DepthSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
						
						%save in in 3D matrix
						ForeVolume(:,:,i)=DepthSlice;
						
					
					end
					
					
				elseif ~isempty(Magnitude)
					MagPos=find(obj.MagBins==Magnitude);
					
					if isempty(MagPos)
						error('Magnitude not found');
					end
					
					NewSelID=obj.SelectedIDs{2}+(MagPos-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
				end
				
				
				
			elseif obj.Dimension.Depth&~obj.Dimension.Mag
				%"real" 3D case with depth,in this case no Depth and Magnitude
				%input is "allowed" (means it will be ignored)
				if ~isempty(Depth)&~isempty(Magnitude)
					disp('Depth and Magnitude input ignored');
					warning('Depth and Magnitude input ignored');
				end
				
				NewSelID=obj.SelectedIDs{2};
				
				%only take Ids which are not nan
				NotNaN=~isnan(NewSelID);
				NewSelID=NewSelID(NotNaN);
				
				ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
				
				
				
			elseif ~obj.Dimension.Depth&obj.Dimension.Mag
				%Magnitude as 3rd dimension,in this case no Depth and Magnitude
				%input is "allowed" (means it will be ignored)
				if ~isempty(Depth)&~isempty(Magnitude)
					disp('Depth and Magnitude input ignored');
					warning('Depth and Magnitude input ignored');
				end
				
				ForeVolume=[];
				
					
											
				%get the magnitude slices
				for i=1:numel(obj.MagBins)
					
					DepthSlice=ForeSlice;
					disp(i)
					NewSelID=obj.SelectedIDs+(i-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					DepthSlice(obj.SelectedNodes)=RawGrid(NewSelID);
					
					%save in in 3D matrix
					ForeVolume(:,:,i)=DepthSlice;

				end
				
				
			end
	
        		
        		
        	end
        	
        	
        	
        	function [OverallDistro ExpVal] = setFullDistro(obj,ForecastData)
        		%produces the CombindeDistro (or sum of all rates for 
        		%Poisson) together with the expected value. 
        	
        		import mapseis.util.stat.CombineDiscretDistro;
        		
        		OverallDistro=[]; 
        		ExpVal=[];
        		
        		switch obj.ForecastType.Type
        			case 'Poisson'
        				OverallDistro=nansum(ForecastData);
        				ExpVal=OverallDistro;
        				
        			case 'Custom'

        				OverallDistro=1;
        				
        				for i=1:numel(ForecastData)
        					OverallDistro=CombineDiscretDistro(OverallDistro,ForecastData{i},true);
        				end
        				
        				ExpVal=sum((0:1:numel(OverallDistro))'.*OverallDistro;
        	
        			case 'NegBin'
        				%currently missing
        		end	
        		
        		
        		
        	end
        	
        	
        	function [OverallDistro ExpVal] = getTotalRate(obj,CastTime)
        		%returns the overall rate or distribution of the forecast
        		%simple for Poisson, but not for a custom distribution
        		
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVec-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
			
			
			if strcmp(obj.ForecastType.Type,'None')
				error('No forecast set')
			end
			
			OverallDistro = obj.OverallDistro{FindInTime};
			ExpVal =  obj.ExpectedValue(FindInTime);
			
			
			
		end	
        	
        	
        	
        	%NEW FUNCTIONS WHICH SHOULD ALSO BE COPIED
        	
        	function [RawSlice RawVolume] = BuildRawGrid(VolumeMode,ZeroMode)
        		%The function just builds raw grid data depended on the 
        		%forecast type (NaNs for poisson, empty cells for Custom)
        		%VolumeMode selects the caller, true for get3DVolume 
        		%and get2DSlice in full 3D mode and false for "real 2D"
        		%get2DSlice
        		
        		if nargin<2
        			ZeroMode=false;
        		end
        		
        		
        		switch obj.ForecastType.Type
        		
        			case 'Poisson'
        				if VolumeMode
        					if ~ZeroMode
        						RawSlice = NaN(size(obj.SelectedNodes{1}{1}));
        						RawVolume = NaN(size(obj.SelectedNodes{2}));
        					
        					else
        						RawSlice = zeros(size(obj.SelectedNodes{1}{1}));
        						RawVolume = zeros(size(obj.SelectedNodes{2}));
        					end
        					
        				else
        					if ~ZeroMode
        						RawSlice = NaN(size(obj.SelectedNodes));
        						RawVolume = [];
        						
        					else
        						RawSlice = zeros(size(obj.SelectedNodes));
        						RawVolume = [];
        					
        					end
        				end
        					
        				
        			case 'Custom'
        				if VolumeMode
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        					else
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        						
        					end
        					
        				else
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        						
        					else
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        					
        					end
        				end
        				
        				
        			case 'NegBin'
        				%Missing currently, guess it will be cell
        				if VolumeMode
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        					else
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        						
        					end
        					
        				else
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        						
        					else
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        					
        					end
        				end
        		end	
        		
        	
        	
        	end
        	
        	
        	
        	function SumCast = SumFullDistro(DistA,DistB)
        		%Sum all distribution of a slice/volume with 
        		%another slice/volume. The dimension of DistA and DistB
        		%has to be the same
        		
        		import mapseis.util.stat.CombineDiscretDistro;
        		
        		switch obj.ForecastType.Type
        			case 'Poisson'
        				%super simple stuff
        				SumCast=DistA.+DistB;
        				
        			case 'Custom'
        				%The difficult and time consuming part
        				SumCast=cell(size(DistA));
        				
        				for i=1:numel(DistA)
        					SumCast{i}=CombineDiscretDistro(DistA{i},DistB{i},true);
        				end
        			
        				
        			case 'NegBin'
        				%currently not supported
        			
        		end	
        	
        	end
        	
        	
        	
        	
        	function ResMat = MatrixCorrector(inMat,LogicArray,WorkMode)
        		%This function is meant to replace the isnan, set to
        		%nan block in the functions to make it compatible with 
        		%the custom distributions
        		%LogicArray can be empty for 'get'
        		
        		import mapseis.util.emptier;
        		
        		ResMat=[];
        		
        		
        		if strcmp(WorkMode,'get')
        			switch obj.ForecastType.Type
					case 'Poisson'
						ResMat=isnan(inMat);
						
					case 'Custom'
						ResMat=emptier(inMat);				
						
					case 'NegBin'
						%currently not supported
        			
				end	
        			
        		elseif strcmp(WorkMode,'set')
        			switch obj.ForecastType.Type
					case 'Poisson'
						ResMat=inMat;
						ResMat(LogicArray)=NaN;
						
					case 'Custom'
						ResMat=inMat;
						ResMat(LogicArray)=cell(sum(sum(inMat)),1);				
						
					case 'NegBin'
						%currently not supported
        			
				end	
        		
        		else
        			disp('Please select correct workmode');
        		
        		end	
        		
        	
        	end
        	

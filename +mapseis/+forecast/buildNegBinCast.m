function [NegBinForecast] = buildNegBinCast(PoisFore,PastVariance)
	%Converts all saved poisson forecasts of a forecasts object into
	%negative binomial forecasts using the PastVariance.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.forecast.*;
	
	
	
	%get timesteps 
	TimeVector=PoisFore.TimeVector;

	if numel(PastVariance)==1
		PastVariance = ones(size(TimeVector))*PastVariance;
	end
	
	
	%get grid from the first object
	TheGrid=PoisFore.getFullGrid;
	TripGrid=PoisFore.getGridNodes;
	
	%build a raw forecast object
	NegBinForecast=ForecastObj([PoisFore.Name,'_NegBin']);
	NegBinForecast.initForecast('NegBin',{});
	NegBinForecast.addGrid(TheGrid,TripGrid);
	NegBinForecast.RealTimeMode=PoisFore.RealTimeMode;
	
	for tC=1:numel(TimeVector)
		disp(tC)
		try
			[CSEPGrid ForeTime ForeLength]= PoisFore.getCSEPForecast(TimeVector(tC));
			
				
		catch
			warning(['Forecast for Time: ',TimeVector(tC),' not existing']);
			continue;
		end
	
		%estimate negative binomial
		NegBinCast = estimateNegBinForecasts(CSEPGrid,PastVariance(tC));
		
		%Store to forecast object
		NegBinForecast.addForecast(NegBinCast(:,[9,11]),ForeTime, ForeLength,NegBinCast(:,10));
		
	end
end

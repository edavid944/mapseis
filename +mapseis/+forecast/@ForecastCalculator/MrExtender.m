function MrExtender(obj,LearnFilt)
	%extends the polygon in the filter by the amount defined in
	%PolyExtend

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.region.*;
	
	%the region filter
	regionFilter=LearnFilt.getByName('Region');
	filterRegion = getRegion(regionFilter);
	pRegion = filterRegion{2};
	RegRange = filterRegion{1};
	
	if ~strcmp(RegRange,'all');
		RegPoly=pRegion.getBoundary;
		
		%determine "middle point" (just the mean)
		MidPoly(1)=mean(RegPoly(1:end-1,1));
		MidPoly(2)=mean(RegPoly(1:end-1,2));
		
		%"direction" of subtraction vector
		dirVec(:,1)=sign(abs(RegPoly(:,1))-abs(MidPoly(1)));
		dirVec(:,2)=sign(abs(RegPoly(:,2))-abs(MidPoly(2)));
		
		NewPoly(:,1)=sign(RegPoly(:,1)).*(abs(RegPoly(:,1))+dirVec(:,1).*obj.PolyExtend(1));
		NewPoly(:,2)=sign(RegPoly(:,2)).*(abs(RegPoly(:,2))+dirVec(:,2).*obj.PolyExtend(2));
		
		newRegType=RegRange;
		newRegion = Region(NewPoly);
		regionFilter.setRegion(newRegType,newRegion);
		
	
	else
		disp('No Polygon in filter - does not have to be extended');
	end

end
function setCalcSteps(obj,CalcStepStruct)
	%adds calcstep structure



	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	TheNames=fieldnames(CalcStepStruct);
	
	for i=1:numel(TheNames)
		if any(strcmp(TheNames{i},obj.ModelNameList))
			obj.WorkToDo.(TheNames{i})=CalcStepStruct.(TheNames{i});
		end	
	end
	
	
	%kill parts which are not allowed to be calculated
	for i=1:numel(obj.ModelNameList)	    		       			
		if isfield(obj.ModelNameList{i},obj.ForecastProject.CalcProgress.StopProgress)
			obj.WorkToDo.(obj.ModelNameList{i})(~obj.ForecastProject.CalcProgress.StopProgress.(obj.ModelNameList{i}))=NaN;
			
			%removed timesteps as well
			obj.WorkToDo.(obj.ModelNameList{i})(obj.ForecastProject.CalcProgress.ModelProgress.(obj.ModelNameList{i})==2)=NaN;
			obj.WorkToDo.(obj.ModelNameList{i})(obj.ForecastProject.CalcProgress.ModelProgress.(obj.ModelNameList{i})==-1)=NaN;
		end
	
	end
	
	%generate new CalcList
	obj.generateCalcSteps;
	
end
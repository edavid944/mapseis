function generateTimeStruct(obj)
	%generates a template for storing the calculation times


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.*;
	
	NrNodes=obj.ForecastProject.ForecastGrid.getNrNodes;
	
	zeroVec=zeros(size(obj.TimeList));
	SingMod=struct( 'TotalTimePerStep',zeroVec,...
					'ConfigTimePerStep',zeroVec,...
					'CalcTimePerStep',zeroVec,...
					'ImportTimePerStep',zeroVec,...
					'PrepareTime',0,...
					'TotalTime',0,...
					'ConfigTime',0,...
					'CalcTime',0,...
					'ImportTime',0,...
					'AddTimeInfo',{{}});
	
	ModTime=struct;		
	for i=1:numel(obj.ModelNameList)
		ModTime.(obj.ModelNameList{i})=SingMod;
	end
	
	TimeStruct=struct(	'System',getSystemInfo,...
						'NumberOfNodes',NrNodes,...
						'Models',{obj.ModelNameList},...
						'TimeSteps',obj.TimeList,...
						'ModelTimes',ModTime,...
						'CalcTimeStep',zeroVec,...
						'FileSize',zeroVec,...
						'TotalTime',0);
	
	obj.BenchmarkData=TimeStruct;
	
	
end
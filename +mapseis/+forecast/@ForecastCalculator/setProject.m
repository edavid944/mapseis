function setProject(obj,ForecastProject)
	%set the project and additional data

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.filter.*;
	
	obj.ForecastProject=ForecastProject;
	
	try %a try for now, my first files did not have this field
		obj.UseTimer=obj.ForecastProject.ProjectConfig.Benchmark;
	end
	
	%check if project is complete
	obj.ForecastProject.checkState;
	
	%correct Progress if not started or ready (started is new here)
	if ~any(strcmp(obj.ForecastProject.ProjectStatus,{'ready','started'}))
		%not entirely sure about the order of the commands
		obj.ForecastProject.correctProgress;
		obj.ForecastProject.cleanForecasts;
		obj.ForecastProject.checkState;
		
	end
	
	%There is a bug somewhere which erases old forecasts when it shouldn't
	%have to check where this is going wrong, I guess it is somewhere in the 
	%correctProgress, cleanForecasts routine but it is not certain.
	
	
	if any(strcmp(obj.ForecastProject.ProjectStatus,{'empty','changed','config'}))
		%something seems to be missing
		warning('Forecast Project is not complete')
		obj.StopToggle=true;
		obj.CalcStatus='uncomplete';
		return
	end
	
	
	if strcmp(obj.ForecastProject.DataMode,'Source')
		%update catalog
		obj.ForecastProject.updateCatalog;
	
	end
	
	
	%The project seems to be calculateable ->get parameters
	obj.SavePath=obj.ForecastProject.StoreProject;
	obj.CalcPath=obj.ForecastProject.CalcPath;
	obj.ArchivePath=obj.ForecastProject.ArchivePath;
	
	%just some shortcuts
	obj.ArchiveForecasts=obj.ForecastProject.ProjectConfig.ArchiveForecasts;
	obj.StoreLogs=obj.ForecastProject.ProjectConfig.StoreLogs;
	obj.ArchiveLogs=obj.ForecastProject.ProjectConfig.ArchiveLogs;
	obj.SaveConfig=obj.ForecastProject.ProjectConfig.SaveConfig;
	
	
	%some of the values are meant for diagnostic and are not used directly 
	%by the calculator
	genericSpace=struct(	'CalculatorID','origin_v1',...
							'CalcName',obj.Name,...
							'CalcID',obj.ID,...
							'InternalCatSel',{{}},...
							'DefectModels',[[]],...
							'NextCalc',obj.NextCalc,...   
							'CalcProgress',obj.CalcProgress,...
							'CalcStatus',obj.CalcStatus,...
							'ErrorCode',0,...
							'LastCalcTime',now,...
							'Finished',true);
	
	%check if something is already done in the past
	contCalcer=false;			
	if ~isempty(obj.ForecastProject.CalculatorSpace)
		disp('import past progress');
		
		%a calculator was active before
		if strcmp(obj.ForecastProject.CalculatorSpace.CalculatorID,'origin_v1')
			obj.CalculatorSpace=obj.ForecastProject.CalculatorSpace;
		
			contCalcer=true;
		else
			%use the generic one
			obj.CalculatorSpace=genericSpace;
		end
		
	else
		%use the generic one
		obj.CalculatorSpace=genericSpace;
	
	end
	
	
	%generate NameList
	for i=1:numel(obj.ForecastProject.ModelPlugIns(:,1))
		ModelNames{i}=[obj.ForecastProject.ModelPlugIns{i,1},'_v',obj.ForecastProject.ModelPlugIns{i,2}];
	end
	
	obj.ModelNameList=ModelNames;	
	
	%generate the WorkToDo structure
	for i=1:numel(obj.ModelNameList)
		obj.WorkToDo.(obj.ModelNameList{i})=obj.ForecastProject.CalcProgress.ModelProgress.(obj.ModelNameList{i});
	
		if isfield(obj.ModelNameList{i},obj.ForecastProject.CalcProgress.StopProgress)
			obj.WorkToDo.(obj.ModelNameList{i})(~obj.ForecastProject.CalcProgress.StopProgress.(obj.ModelNameList{i}))=NaN;
		end
		
	end
	
	%get times
	obj.TimeList=obj.ForecastProject.CalcProgress.Times;
	if isempty(obj.TimeList)
		obj.TimeList=obj.ForecastProject.AvailableTimes;
	end
	
	%update internal FilterList storage, for the available times
	if isempty(obj.CalculatorSpace.InternalCatSel)
		obj.CalculatorSpace.InternalCatSel=cell(size(obj.TimeList));
	
	elseif numel(obj.CalculatorSpace.InternalCatSel)~=numel(obj.TimeList)
		obj.CalculatorSpace.InternalCatSel=cell(size(obj.TimeList));
	
	end
	
	
	%generate calcsteps 
	obj.generateCalcSteps;
	
	%set to correct step
	obj.NextCalc=obj.CalculatorSpace.NextCalc;
	obj.CalcProgress=obj.CalculatorSpace.CalcProgress;
	
	
	%set the correct calculator status
	switch obj.ForecastProject.ProjectStatus
		case {'ready','recalc','started'}
			obj.CalcStatus='ready';
			obj.StopToggle=false;
			%ready for action
			obj.ErrorCode=0;
			
		case 'finished'
			obj.CalcStatus='finished';
			obj.StopToggle=false;
			%recalc is of course possible
			obj.ErrorCode=0;
			
		case 'cont'
			obj.CalcStatus='cont-finished';
			obj.StopToggle=false;
			%recalc is of course possible	
			obj.ErrorCode=0;
			
		case {'empty','config','changed'}
			%This one should never happen, but to be sure
			obj.CalcStatus='uncomplete';
			obj.StopToggle=true;
			obj.ErrorCode=-1;
	end
	
	
	
	
	
	
	%create benchmarking structure if UseTimer is turned on
	if obj.UseTimer
		if contCalcer
			if ~isempty(obj.ForecastProject.BenchmarkData)
				obj.BenchmarkData=obj.ForecastProject.BenchmarkData;
			else
				obj.generateTimeStruct;
			end
		else
	
			obj.generateTimeStruct;
		end
	end
	
	DoExtend=obj.ForecastProject.getParameter('AutoExtend');
	if ~isempty(DoExtend)
		obj.ExtendRegionPoly=DoExtend;
		obj.PolyExtend=obj.ForecastProject.getParameter('RegionExtend');
	end
	
	
	if obj.ExtendRegionPoly
		RawLearner=FilterListCloner(obj.ForecastProject.LearningFilter);
		obj.MrExtender(RawLearner);
		obj.LearningFilter=RawLearner;
		
	else
		obj.LearningFilter=FilterListCloner(obj.ForecastProject.LearningFilter);
	end

end
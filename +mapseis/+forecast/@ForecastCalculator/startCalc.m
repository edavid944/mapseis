function startCalc(obj,forced)
	%this will start the calculation, by calling the prepare of every model
	%and doing the necessary stuff
	  		
	%NOTE: I'm not entirely sure if this function is finished, there might be some
	%functionallity which will be added here
	   		

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	import mapseis.filter.*;
	
	
	if ~any(strcmp(obj.CalcStatus,{'ready','finished','cont-ready','cont-finished'}))	
		error('Calculator not ready')
	end
	
	if any(strcmp(obj.CalcStatus,{'finished','cont-finished'}))
		warning('Calculator is finished, please select parts to be re-calculated');
		return
	end
	
	%Check error code (short version)
	if ~obj.checkError
		error('Calculator notified an error');
	end
	
	%set the recursion limit to a high enough number (matlab will stop
	%the calculation later if this is not done
	NrNeeded=numel(obj.WorkList(:,1))+500; 
	set(0,'RecursionLimit',NrNeeded);
	
	   		
	%use the initial learning period as input for prepare
	[TimeInt,Range] = getLearningTime(obj,0);
	
	Datastore=obj.ForecastProject.Datastore;
	%LearningFilter=obj.ForecastProject.LearningFilter;
	LearningFilter=obj.LearningFilter;
	
	%open Question: clone Filterlist or not (how easy is it anyway?)
	%->Clone
	FirstLearningFilter=FilterListCloner(LearningFilter);
	TimeFilt=FirstLearningFilter.getByName('Time');
	TimeFilt.setRange(Range,TimeInt);
	
	%get data, not needed the plugin has to do this
	%but I leave it here maybe needed later or elsewhere
	%FirstLearningFilter.changeData(Datastore);
	%FirstLearningFilter.updateNoEvent;
	%Selection=FirstLearningFilter.getSelected;
	
	UsedModelID=unique(obj.WorkList(:,2));
	
	for i=1:numel(UsedModelID)
		%get model plugin, ID could be sended directly, but
		%it saver like this (avoids mixups)
		PrepTic=tic;
		ModelName=obj.ModelNameList{UsedModelID(i)};
		[curModel,Pos] = obj.ForecastProject.getModel(ModelName);
		
		%reset model
		curModel.ResetFullCalc;
		
		%prepare model
		curModel.setInputData(Datastore,FirstLearningFilter,obj.TimeList(1));
		curModel.prepareCalc;
		PrepTime=toc(PrepTic);
		
		if obj.UseTimer
			obj.AddBenchTimes(PrepTime,i,'prepare');
		end
	end
	
	obj.ForecastProject.setBenchmark(obj.BenchmarkData);
	
	if ~obj.LoopMode		
		%start calculation
		CurNr=obj.NextCalc;
		obj.NextCalc=obj.NextCalc+1;
		obj.contCalc(CurNr);
		
	else
		if ~obj.ParentParallelMode
			obj.LoopItSerial;
		else
			obj.ParallelMode=false;
			obj.LoopItParallel;
		end
	
	end
	
end
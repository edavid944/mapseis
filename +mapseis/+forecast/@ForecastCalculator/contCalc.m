function contCalc(obj,ListNr)
	%the actual calculation, this can be used as a recursive function
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.filter.*;
	
	%check if stopped or fatal error is present
	if ~checkError(obj)|obj.StopToggle
		warning(['calculation stopped at ID ', num2str(ListNr)]);
		obj.pauseCalc
		return
	end
	
	CalcerTic=tic;
	
	%get which model and time
	TimeID = obj.WorkList(ListNr,1);
	ModelID = obj.WorkList(ListNr,2);
	
	if any(obj.CalculatorSpace.DefectModels==ModelID)
		warning('Model has been automatically stopped');
		
		%continue if any left
		if ListNr<=obj.NextCalc
			CurNr=obj.NextCalc;
			obj.NextCalc=obj.NextCalc+1;
			obj.contCalc(CurNr);
		else
			%The end my friend
			obj.finishCalc;
		end
		
		return
	
	end
	
	%check if catalog is existing, if not build and save
	if isempty(obj.CalculatorSpace.InternalCatSel{TimeID})
		%build new filterlist
		Datastore=obj.ForecastProject.Datastore;
		%LearningFilter=obj.ForecastProject.LearningFilter;
		LearningFilter=obj.LearningFilter;
		
		[TimeInt,Range] = getLearningTime(obj,TimeID);
		
		TheLearner=FilterListCloner(LearningFilter);
		TimeFilt=TheLearner.getByName('Time');
		TimeFilt.setRange(Range,TimeInt);
		
		if obj.BufferCatalogs
			%store it internally
			obj.CalculatorSpace.InternalCatSel{TimeID}=TheLearner;
		end
	
		%store it in the project
		ErrorCode = obj.ForecastProject.setUsedCatalog(obj.TimeList(TimeID),TheLearner);
		
	else
		%get old one
		Datastore=obj.ForecastProject.Datastore;
		TheLearner=obj.CalculatorSpace.InternalCatSel{TimeID};
	end
	
	
	%get model
	ModelName=obj.ModelNameList{ModelID};
	[curModel,Pos] = obj.ForecastProject.getModel(ModelName);
	
	%reset model
	curModel.ResetLastCalc;
	
	%send data
	curModel.setInputData(Datastore,TheLearner,obj.TimeList(TimeID));
	
	%calc
	try
		ErrorCode = curModel.calcForecast;
	catch
		%to be sure that it continue with the rest, even if it completely fails
		disp('Calculation failed')
		ErrorCode = -1
	end	
	
	%check error
	IsSave = obj.checkError(ErrorCode,ListNr);
	
	if ~IsSave
		warning(['calculation stopped at ID ', num2str(ListNr),' ,with error code ',num2str(ErrorCode)]);
		obj.StopToggle=true;
		obj.pauseCalc
		return
	end
	
	
	%everything should be fine now, get the results
	[ForeCast,DataMask] = curModel.getForecast(false); %no grid, keep the size low
	
	%generate benchmark data if needed
	if obj.UseTimer
		CPUTimeData = curModel.getCalcTime;
		
		obj.AddBenchTimes(CPUTimeData,ListNr,'calc');
		obj.ForecastProject.setBenchmark(obj.BenchmarkData);
				
	end
	
	
	
	%additional parts of the results
	if obj.ArchiveForecasts
		FilePath = curModel.getForecastFile;
		ForPath = obj.archiveFile(FilePath);
	else
		ForPath=[];
	end
	
	disp('archived');
	
	if obj.StoreLogs
		TheLogs = curModel.getCalcLog;
		
	else
		TheLogs=[];
	end
	
	disp('stored');
	
	if obj.ArchiveLogs
		FilePath = curModel.getCalcLogFile;
		LogPath = obj.archiveFile(FilePath);
	else
		LogPath=[];
	end
	
	disp('archived logs');
	
	if obj.SaveConfig
		ModConfig = curModel.getModelConfig;
	else
		ModConfig=[];
	end
	
	disp('saved config');
	
	%store in the project
	ForecastData={ForeCast,ForPath};
	LogData={TheLogs,LogPath};
	ConfigData=ModConfig;
	ErrorCode = obj.ForecastProject.setForecastResults(obj.TimeList(TimeID),ModelName,ForecastData,DataMask,LogData,ConfigData);
	
	disp('project saved');
	
	if ErrorCode~=0
		obj.ErrorCode=99;
		warning(['calculation stopped at ID ', num2str(ListNr),' ,because of an I/O problem Code: ',num2str(ErrorCode)]);
		IsSave = obj.checkError;
		obj.StopToggle=true;
		obj.pauseCalc
		return
	end
	       		
	%store project
	obj.storeCurrentResults;
	
	
	%register as done calculation
	obj.WorkDone(ListNr)=true;
	
	%update progress
	obj.CalcProgress=ceil(ListNr/numel(obj.WorkList(:,1))*1000)/10;
	disp(['Calculation progress ',num2str(obj.CalcProgress),'%']);        		
	%notify the CalcUpdate
	notify(obj,'CalcUpdate')
	
	obj.PastCalcTime=toc(CalcerTic);
	
	if ~obj.LoopMode
		%continue if any left
		if ListNr<numel(obj.WorkList(:,1))
			CurNr=obj.NextCalc;
			obj.NextCalc=obj.NextCalc+1;
			obj.contCalc(CurNr);
		else
			%The end my friend
			obj.finishCalc;
		
		end
		
	else
		%it is looped do nothing for now
	
	end
	
	
	
end
function storeCurrentResults(obj)
	%brings the ForecastProject "up-to-date" (add stuff to 
	%continue later) and stores it as file in the predefined
	%path
	


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	if isempty(obj.SaveEveryStep)
		obj.SaveEveryStep=true;
	end
	
	%update Calculator space
	obj.CalculatorSpace.CalcStatus='started';
	obj.CalculatorSpace.CalcName=obj.Name;   
	obj.CalculatorSpace.CalcID=obj.ID;   
	obj.CalculatorSpace.NextCalc=obj.NextCalc;
	obj.CalculatorSpace.CalcProgress=obj.CalcProgress
	obj.CalculatorSpace.LastCalcTime=now;
	obj.CalculatorSpace.Finished=false;
	obj.CalculatorSpace.ErrorCode=obj.ErrorCode;
	
	%update file
	obj.ForecastProject.UpdateCalcSpace(obj.CalculatorSpace);
	
	
	if ~obj.SaveEveryStep
		obj.SaveCount=obj.SaveCount-1;
		
		if obj.SaveCount<=0
			%save file
			ForecastProject=obj.ForecastProject;
			save(obj.SavePath,'ForecastProject');
			obj.SaveCount=obj.SaveOften;
			disp('saved Forecasts');
			
		else
			disp(['Will save in ',num2str(obj.SaveCount), 'steps']);
		end
	
	else
		%save file
		ForecastProject=obj.ForecastProject;
		save(obj.SavePath,'ForecastProject');
	
	end
	
end
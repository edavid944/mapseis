function restartCalc(obj)
	%restarts from the last calcstep
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	if isempty(obj.WorkList)
		error('No calculation has been started before - use startCalc');
	end	
	
	
	NrNeeded=numel(obj.WorkList(:,1))+500; 
	set(0,'RecursionLimit',NrNeeded);
	
	if ~obj.LoopMode
		if obj.NextCalc>1
			obj.contCalc(obj.NextCalc-1);
		else
			obj.contCalc(obj.NextCalc);
		end
		
	else
		if obj.NextCalc>1
			obj.NextCalc=obj.NextCalc-1;
		end
		
		
		if ~obj.ParentParallelMode
			obj.LoopItSerial;
		else
			obj.ParallelMode=false;
			obj.LoopItParallel;
		end
	
	end

end
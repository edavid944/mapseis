function newPath = archiveFile(obj,FilePath,ForeTime)
	%This function will move the original file to the 
	%archive folder and rename it by adding a datenum-based
	%string to the end of it.
	%The names of the files produced by different model
	%should be different, or else they will be overwritten
	%A possible way could be to add a version string to the 
	%file name, like this no file would be overwritten even
	%with the same model used with different config
	


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	newPath=[];
	
	if nargin<3
		ForeTime=[];
	end
	
	if isempty(ForeTime)
		ForeTime=now;
	end
	
	fs=filesep;
	
	%digest input
	[pathstr, name, ext] = fileparts(FilePath);
	
	%generate random 3-digit hex number, like this there will
	%be 4096, this gives a lot of possible forecasts a day
	RawChar='0123456789ABCDEF'; %Hex
	HexAddon=RawChar(randi(15,1,3)+1);
	
	%get some of the digits in the datenum out
	yr200=floor(ForeTime/(365*200))*(365*200);
	TimeStr=num2str(round((ForeTime-yr200)*100));
	
	%generate new name
	newName = [name,'_',TimeStr,'_',HexAddon];
	
	CoolPlace=[obj.ArchivePath,fs,newName,ext];
	
	%try moving it 
	CheckExist=exist(FilePath)
	
	if CheckExist==0
		warning('File not existing');
		newPath=[];
	else
		if isunix
			%the command
			Ucommand=['mv -f ',FilePath,' ',CoolPlace];
			[stat, res] = unix(Ucommand);
	
			if stat==0
				newPath=CoolPlace;
			else
				newPath=[];
				warning('Could not move file');
	
			end
	
		else
			%the command
			Dcommand=['move /Y ',FilePath,' ',CoolPlace];
			[stat, res] = dos(Dcommand);
			
			if stat==0
				newPath=CoolPlace;
			else
				newPath=[];
				warning('Could not move file');
			
			end		
		end
	end
	
end
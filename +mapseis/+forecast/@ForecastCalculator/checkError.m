function IsSave = checkError(obj,ErrorCode,ListNr)
	%Checks if the error specified allows to continue and
	%registers the error as well as set the global error
	%if needed
	%Using this without ErrorCode and ListNr (or with an
	%empty one) will cause the program to only the check
	%the global error code
	
	%List of common errors:
	%PlugIn:
	%		0: no Error, everything fine
	%		1: Model not or not correctly configurated
	%		2: No catalog data defined
	%		3: external function did not work
	%		-1: generic not specified error
	%Other error can be specified by the plugin
	%
	%Calculator:
	%		-1: No project specified 
	%		0: no Error, everything fine
	%		1: some Models had errors, reasonable save to continue
	%		2: one or more failed, but where isolated
	%		3: over 50% overall failures
	%		4: too many models failed
	%		99: I/O problem with the ForecastProject object
	
	%NOTE: the error handling is quite complexe but it allows to 
	%save a lot of time in case something goes wrong, and the 
	%most times is probably spent in the models themself anyway.


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if nargin<2
		ErrorCode=[];
		ListNr=[];
	end
	
	if isempty(ErrorCode)|isempty(ListNr)
		%check the global error
		IsSave=obj.ErrorCode==0|obj.ErrorCode==1|obj.ErrorCode==2|obj.ErrorCode==3;
		if obj.ErrorCode~=0
			notify(obj,'ErrorOccured');
		end
		return
	end
	
	%first write the code into the table
	obj.ErrorInCalc(ListNr)=ErrorCode; 
	
	%now check if the error itself is save
	SingleSave=ErrorCode==0;
	
	%now check the general error
	GeneralSave=obj.ErrorCode==0|obj.ErrorCode==1|obj.ErrorCode==2|obj.ErrorCode==3;
	
	if ~GeneralSave
		IsSave=false;
		notify(obj,'ErrorOccured')
		
	elseif ~SingleSave&obj.ErrorCode==0
		%probably the first Error
		IsSave=true;
		obj.ErrorCode=1;
		notify(obj,'ErrorOccured');
		
	elseif ~SingleSave&(obj.ErrorCode==1|obj.ErrorCode==2|obj.ErrorCode==3)
		%it is getting critical
		%check if it is still only 50% or is becoming more 
		%and more (it has to over 50% without the stopped models
		
		CopyError=obj.ErrorInCalc;
		if ~isempty(obj.CalculatorSpace.DefectModels)
			%models have been stopped, exclude those
			for i=1:numel(obj.CalculatorSpace.DefectModels)
				Places=obj.WorkList(:,2)==obj.CalculatorSpace.DefectModels(i);
				CopyError(Places)=nan;
			end
		
		end
	
		%How many are faulty
		DidNotWork=sum(CopyError~=0)-sum(isnan(CopyError));
		Existing=sum(~isnan(CopyError));
	
	
	
		%at least 10% of the work has to be done to issue
		%a total stop
		if ListNr>=0.1*numel(obj.WorkList(:,1))
			if DidNotWork>=Existing
				%it is unsave now
				IsSave=false;
				obj.ErrorCode=4;
			elseif DidNotWork>=0.5*Existing 
				IsSave=true;
				obj.ErrorCode=3;
			else
				IsSave=true;
	
				if ~isempty(obj.CalculatorSpace.DefectModels)
					obj.ErrorCode=2;
				else
					obj.ErrorCode=1;
				end
			end
	
		else
			if DidNotWork>=Existing
				%it is seems unsave but lets see later
				IsSave=true;
				obj.ErrorCode=3;
			elseif DidNotWork>=0.5*Existing 
				IsSave=true;
				obj.ErrorCode=3;
			else
				IsSave=true;
	
				if ~isempty(obj.CalculatorSpace.DefectModels)
					obj.ErrorCode=2;
				else
					obj.ErrorCode=1;
				end
			end
	
		end
	
		%check also if a model continues to fail 
		leftOverModel=unique(obj.WorkList(~isnan(CopyError),2));
		for i=1:numel(leftOverModel)
			%also here at least 10% of total Nr for the 
			%specific models have to be tried before a 
			%model is considered to fail.
			ErrorsOfMod=CopyError(obj.WorkList(:,2)==leftOverModel(i));
			
			Failed=sum(leftOverModel~=0)-sum(isnan(leftOverModel));
			%calced=sum(leftOverModel==0)-sum(isnan(leftOverModel));
			totalCalced=sum(~isnan(leftOverModel));
			
			if totalCalced>=0.1*numel(ErrorsOfMod)
				if Failed>=totalCalced
					%Exterminate
					obj.CalculatorSpace.DefectModels(end+1)=i;
				
					if obj.ErrorCode~=3
						obj.ErrorCode=2;
					end
				end
			end
	
		end
	
		notify(obj,'ErrorOccured')
	
	else
		IsSave=true;
	
	end
	
end
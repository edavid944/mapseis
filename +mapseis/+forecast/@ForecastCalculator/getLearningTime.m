function [TimeInt Range] = getLearningTime(obj,TimeID)
	%returns the start and end of the learning time for a certain
	%forecast
	%OutPut: [startLearn, endLearn] and the range as keyword


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	%get the key values for this
	LearningPeriod=obj.ForecastProject.LearningPeriod;
	LearningUpdate=obj.ForecastProject.LearningUpdate;
	LearningLength=obj.ForecastProject.LearningLength;
	LearningLag=obj.ForecastProject.LearningLag;
	
	if TimeID==0
		%initial learning period
		TimeInt = LearningPeriod;
		
		if all(isinf(TimeInt))
			Range='all';
		elseif 	isinf(TimeInt(1))
			Range='before';
		elseif 	isinf(TimeInt(2))
			Range='after';
		else
			Range='in';
		end
		
		return
	end
	
	
	DoUpdate=strcmp(LearningUpdate,'update');
	FullLength=isstr(LearningLength);
	
	%now filtering applied, it is just lowest possible time
	eventStruct=obj.ForecastProject.Datastore.getFields;
	
	mostAcient=LearningPeriod(1);
	curCastStart=obj.TimeList(TimeID);
	
	if DoUpdate&FullLength
		TimeInt = [mostAcient curCastStart-LearningLag];
	elseif DoUpdate
		TimeInt = [curCastStart-LearningLength curCastStart]-LearningLag;
	else
		%fixed time
		TimeInt = LearningPeriod;
	end
	
	if all(isinf(TimeInt))
		Range='all';
	elseif 	isinf(TimeInt(1))
		Range='before';
	elseif 	isinf(TimeInt(2))
		Range='after';
	else
		Range='in';
	end
	
end
classdef ForecastCalculator < handle
	%This the first version of the forecast project calculator
	


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	properties
		Name
		Version
		ID
		ObjVersion
		MetaData
		CalcStatus
		
		ModelNameList
		TimeList
		WorkToDo
		WorkList
		WorkDone
		ErrorInCalc
		  	
		CalculatorSpace
		
		NextCalc
		StopToggle
		
		ForecastProject 
		CalcProgress
		ErrorCode
		SavePath
		CalcPath
		ArchivePath
		ArchiveForecasts
		StoreLogs
		ArchiveLogs
		SaveConfig
		BenchmarkData
		UseTimer
		BufferCatalogs
		PastCalcTime
		LearningFilter
		ExtendRegionPoly
		PolyExtend
		SaveEveryStep
		SaveCount
		SaveOften
		LoopMode
		ParentParallelMode
	end
	
	events
		CalcUpdate
		ErrorOccured
	end
	
	
	methods
		function obj = ForecastCalculator(Name)
			%constructor, sets default values and creates the object
			
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
				%A bit gimmicky I admit, but could be useful later
				Name=RawChar(randi(15,1,8)+1);
			end
			
			obj.Name=Name; %may be not so important here, but who knows?
			obj.ID=RawChar(randi(15,1,8)+1);
			obj.Version='0.9';
			
			%init data
			obj.CalcStatus='empty';
			obj.ModelNameList={};
			obj.TimeList=[];
			obj.WorkToDo=[];
			obj.ForecastProject=[];       	
			obj.CalcProgress=0;
			obj.ErrorCode=-1;
			   		
			obj.WorkList=[];
			obj.WorkDone=[];
			obj.ErrorInCalc=[];
			
			obj.CalculatorSpace=[];
			
			obj.NextCalc=0;
			obj.StopToggle=false;
			
			obj.SavePath=[];
			obj.CalcPath=[];
			obj.ArchivePath=[];
			obj.BenchmarkData=[];
			obj.UseTimer=false;
			obj.ArchiveForecasts=false;
			obj.StoreLogs=false;
			obj.ArchiveLogs=false;
			obj.SaveConfig=false;
			obj.BufferCatalogs=false;
			obj.PastCalcTime=[];
			
			obj.ExtendRegionPoly=false;
			obj.PolyExtend=[0.2,0.2];
			obj.LearningFilter=[];
			
			obj.SaveEveryStep=true;
			obj.SaveCount=10;
			obj.SaveOften=10;
			
			obj.LoopMode=true;
			obj.ParentParallelMode=false;
		
		end


		%as usual the methods declaration
		externStop(obj, Shouter)

		setProject(obj,ForecastProject)		

		generateCalcSteps(obj)

		generateTimeStruct(obj)

		setCalcSteps(obj,CalcStepStruct)

		CalcStepStruct = getCalcSteps(obj)

		MrExtender(obj,LearnFilt)

		startCalc(obj,forced)

		restartCalc(obj)

		contCalc(obj,ListNr)

		LoopItSerial(obj)

		LoopItParallel(obj)

		AddBenchTimes(obj,CPUTimeData,ListNr,Mode)

		stopCalc(obj)

		IsSave = checkError(obj,ErrorCode,ListNr)

		newPath = archiveFile(obj,FilePath,ForeTime)
	
		finishCalc(obj)

		pauseCalc(obj)

		storeCurrentResults(obj)

		[TimeInt Range] = getLearningTime(obj,TimeID)

		ResetDefectModels(obj)		


	end
end
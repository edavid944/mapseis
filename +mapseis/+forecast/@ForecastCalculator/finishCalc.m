function finishCalc(obj)
	%everything correctly finished, so clean up, set the 
	%correct state of the project and store it one last time
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	%last thing to benchmark
	if obj.UseTimer
		obj.AddBenchTimes([],numel(obj.WorkList(:,1)),'finish');
		obj.ForecastProject.setBenchmark(obj.BenchmarkData);
	end
	
	%first reset all calculations
	UsedModelID=unique(obj.WorkList(:,2));
	
	for i=1:numel(UsedModelID)
		%get model plugin, ID could be sended directly, but
		%it saver like this (avoids mixups)
		ModelName=obj.ModelNameList{UsedModelID(i)};
		[curModel,Pos] = obj.ForecastProject.getModel(ModelName);
		
		%reset model
		curModel.ResetFullCalc;
	end
	
	%build new CalcSpace with only the needed info
	genericSpace=struct(	'CalculatorID','origin_v1',...
							'CalcName',obj.Name,...
							'CalcID',obj.ID,...
							'InternalCatSel',{{}},...
							'DefectModels',obj.CalculatorSpace.DefectModels,...
							'NextCalc',obj.NextCalc,...   
							'CalcProgress',obj.CalcProgress,...
							'CalcStatus','finished',...
							'ErrorCode',obj.ErrorCode,...
							'LastCalcTime',obj.CalculatorSpace.LastCalcTime,...
							'Finished',true,...
							'FinishedCalc',now);
	
	%now kick out everything not needed in the project
	obj.ForecastProject.UpdateCalcSpace(genericSpace);
	
	%Save it one last time
	ForecastProject=obj.ForecastProject;
	save(obj.SavePath,'ForecastProject');
	disp('saved the last time')
	
	%notify everybody
	obj.ErrorCode=0; %everything is fine
	disp('Calculation was finished normally')
	notify(obj,'CalcUpdate');

end
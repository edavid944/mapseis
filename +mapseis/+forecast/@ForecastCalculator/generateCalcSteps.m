function generateCalcSteps(obj)
	%generates the list of jobs which should be done
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	obj.WorkList=[];
	
	for j=1:numel(obj.TimeList)
		for i=1:numel(obj.ModelNameList)
			if obj.WorkToDo.(obj.ModelNameList{i})(j)==0
				obj.WorkList(end+1,:)=[j,i];
			end
		end
	end	
	
	%needed also
	obj.WorkDone=false(size(obj.WorkList(:,1)));
	obj.ErrorInCalc=nan(size(obj.WorkList(:,1)));
	
	
	if ~isempty(obj.WorkList)
		obj.NextCalc=1;
		
		if strcmp(obj.CalcStatus,'finished')
			obj.CalcStatus='ready';
			obj.ErrorCode=0;
		end	
		
		if strcmp(obj.CalcStatus,'cont-finished')
			obj.CalcStatus='cont-ready';
			obj.ErrorCode=0;
		end
	
	end
	
	%set toggle if it wasn't before
	if ~any(strcmp(obj.ForecastProject.ProjectStatus,{'empty','changed','config'}))
		obj.StopToggle=false;
		obj.ErrorCode=0;
	end
	
end
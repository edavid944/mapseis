function AddBenchTimes(obj,CPUTimeData,ListNr,Mode)
	%"outsourced" from the contCalc, to make it more readable
	%it gets all the times in the calculation and writes them 
	%to the correct place


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	switch Mode
		case 'calc'
			
			%Times
			if ~isempty(CPUTimeData)
				TimeIn=CPUTimeData.InputDataTime;
				TimeCalcNow=CPUTimeData.CalcTime;
				TimeImp=CPUTimeData.ImportTime;
			end
			
			if ~isempty(obj.PastCalcTime)
				TimeCalcPast=obj.PastCalcTime(1); %better estimate of time
				PastExist=true;
			else
				TimeCalcPast=0;
				PastExist=false;
			end
			
			%check for addon
			if isfield(CPUTimeData,'AddTimeInfo')
				AddTimeInfo=CPUTimeData.AddTimeInfo;
			else
				AddTimeInfo=[];
			end
			
			
			%store
			TimeStruct=obj.BenchmarkData;
			TimeIDnow = obj.WorkList(ListNr,1);
			ModelIDnow = obj.WorkList(ListNr,2);
			ModNameNow = obj.ModelNameList{ModelIDnow}
			
			
			%first the past (if there is any)
			if (ListNr-1)>0&PastExist
				TimeIDpast = obj.WorkList(ListNr-1,1);
				ModelIDpast = obj.WorkList(ListNr-1,2);
				ModNamePast =obj.ModelNameList{ModelIDpast}
				
				
				%update total time per step
				TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast)=...
								TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast)+...
								TimeCalcPast+TimeStruct.ModelTimes.(ModNamePast).ConfigTimePerStep(TimeIDpast)+...
								TimeStruct.ModelTimes.(ModNamePast).ImportTimePerStep(TimeIDpast);
				
				%update CalcTime
				TimeStruct.ModelTimes.(ModNamePast).CalcTimePerStep(TimeIDpast)=TimeCalcPast; %better time
			
				%update TotalTime
				TimeStruct.ModelTimes.(ModNamePast).TotalTime=...
													TimeStruct.ModelTimes.(ModNamePast).TotalTime+...
													TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast);
			
				%update CalcTime
				TimeStruct.ModelTimes.(ModNamePast).CalcTime=TimeStruct.ModelTimes.(ModNamePast).CalcTime+TimeCalcPast;
				
				%update time per step (all models)
				TimeStruct.CalcTimeStep(TimeIDpast)=TimeStruct.CalcTimeStep(TimeIDpast)+...
								TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast);
				
				%get filesize of last file
				s=dir(obj.SavePath);
				CurSize=s.bytes;
				
				if ~isempty(TimeStruct.FileSize)
					if TimeStruct.FileSize(TimeIDpast)<CurSize;
						TimeStruct.FileSize(TimeIDpast)=CurSize;
					end
				else
					TimeStruct.FileSize(TimeIDpast)=CurSize;
				end
			
				TimeStruct.TotalTime=TimeStruct.TotalTime+...
							TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast);
			
			end
			
			if ~isempty(CPUTimeData)
				%so much for the past, now the "now"
				TimeStruct.ModelTimes.(ModNameNow).ConfigTimePerStep(TimeIDnow)=TimeIn;
				TimeStruct.ModelTimes.(ModNameNow).ImportTimePerStep(TimeIDnow)=TimeImp;              
				TimeStruct.ModelTimes.(ModNameNow).CalcTimePerStep(TimeIDnow)=TimeCalcNow; %first estimate
				
				%AddonTimes if existing
				if ~isempty(AddTimeInfo)
					TimeStruct.ModelTimes.(ModNameNow).AddTimeInfo{TimeIDnow}=AddTimeInfo;
				end
			end
			
			obj.BenchmarkData=TimeStruct;
			
	
		case 'prepare'
			%ListNR should here be the model nr and CPUTimeData the time for preparation
			
			TimeStruct=obj.BenchmarkData;
			ModNameNow =obj.ModelNameList{ListNr}
			TimeStruct.ModelTimes.(ModNameNow).PrepareTime=CPUTimeData;
			
			obj.BenchmarkData=TimeStruct;
	
	
		case 'finish'
			%for the last step just updates the past calc step and file size
			
			%to be sure
			if ListNr==numel(obj.WorkList(:,1))
				TimeStruct=obj.BenchmarkData;
				TimeIDnow = obj.WorkList(ListNr,1);
				ModelIDnow = obj.WorkList(ListNr,2);
				ModNameNow = obj.ModelNameList{ModelIDnow}
				TimeCalcPast=obj.PastCalcTime(1);
				
				TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow)=...
								TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow)+...
								TimeCalcPast+TimeStruct.ModelTimes.(ModNameNow).ConfigTimePerStep(TimeIDnow)+...
								TimeStruct.ModelTimes.(ModNameNow).ImportTimePerStep(TimeIDnow);
				
				%update CalcTime
				TimeStruct.ModelTimes.(ModNameNow).CalcTimePerStep(TimeIDnow)=TimeCalcPast; %better time
				
				%update TotalTime
				TimeStruct.ModelTimes.(ModNameNow).TotalTime=...
							TimeStruct.ModelTimes.(ModNameNow).TotalTime+... 
							TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow);
				
				%update CalcTime
				TimeStruct.ModelTimes.(ModNameNow).CalcTime=TimeStruct.ModelTimes.(ModNameNow).CalcTime+TimeCalcPast;
				
				%update time per step (all models)
				TimeStruct.CalcTimeStep(TimeIDnow)=TimeStruct.CalcTimeStep(TimeIDnow)+…
									TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow);
				
				%get filesize of last file
				s=dir(obj.SavePath);
				CurSize=s.bytes;
				
				if ~isempty(TimeStruct.FileSize)
					if TimeStruct.FileSize(TimeIDnow)<CurSize;
						TimeStruct.FileSize(TimeIDnow)=CurSize;
					end
				else
					TimeStruct.FileSize(TimeIDnow)=CurSize;
				end
				
				TimeStruct.TotalTime=TimeStruct.TotalTime+TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow);
				
				
				obj.BenchmarkData=TimeStruct;
			
			end
	
	end
	
end
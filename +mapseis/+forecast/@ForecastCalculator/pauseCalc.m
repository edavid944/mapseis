function pauseCalc(obj)
	%similar to the finishCalc, but does not clean up, it is 
	%supposed to be used in case where there is a fatal error
	%or in case where the calculation was stopped.
	
	%no error notification and errorcode here because it may 
	%not an error but just a pause, also the error handling
	%should have been done elsewhere
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	%update Calculator space
	obj.CalculatorSpace.CalcStatus='started';
	obj.CalculatorSpace.CalcName=obj.Name;   
	obj.CalculatorSpace.CalcID=obj.ID;   
	obj.CalculatorSpace.NextCalc=obj.NextCalc;   
	obj.CalculatorSpace.CalcProgress=obj.CalcProgress
	obj.CalculatorSpace.LastCalcTime=now;
	obj.CalculatorSpace.Finished=false;
	obj.CalculatorSpace.ErrorCode=obj.ErrorCode;
	
	%update file
	obj.ForecastProject.UpdateCalcSpace(obj.CalculatorSpace);
	
	
	ForecastProject=obj.ForecastProject;
	save(obj.SavePath,'ForecastProject');
	
	%notify everybody
	disp('Calculation was has been stopped')
	notify(obj,'CalcUpdate');
	
end
function plotEnsembleWeights(plotAxis,Weights,ModelNames,TheTitle,TimeVector)
	%plots the previous calculated Weights
	
	%DE 2013
	
	EdgeCol=[0.3 0.3 0.3];
	
	%new face colors
	Fblue=[0.2,0.15,1];
	Fgreen=[0.3,0.9,0.4];
	Fred=[1,0.2,0.2];
	Fcyan=[0.3,0.9,0.9];
	Fmagneta=[0.9,0.4,0.9];
	
	%needed later
	wheelColor={'b','g','r','c','m'}
	FaceColor={Fblue,Fgreen,Fred,Fcyan,Fmagneta}
	%wheelColor{mod(existPlots,6)+1}
	
	TheYLab='Weight';
	TheXLab='Time ';
	
	if nargin<5
		TimeVector=[];
	end

	
	if isempty(TimeVector)
		TimeVector=(1:numel(Weights))';
		TheXLab='Element';
	end
	
	
	%convert Weights to mat array
	WArray=cell2mat(Weights');
	
	%determine which one is on average the most "used" model
	MeanWeight=mean(WArray);
	[Temp,WeightIDX]=sort(MeanWeight,'descend');
	
	
	%first the BG Box
	BgBox(:,1)=[TimeVector(1),TimeVector(end),TimeVector(end),TimeVector(1),TimeVector(1)]';
	BgBox(:,2)=[0,0,1,1,0]';
	
	%Template poly for the later ones
	TemplerPol(:,1)=[TimeVector',TimeVector(end),TimeVector(1),TimeVector(1)]';
	
	%go for plot
	axis(plotAxis);
	Hands(1)=fill(BgBox(:,1),BgBox(:,2),FaceColor{mod(1,5)+1});
	set(Hands(1),'EdgeColor',EdgeCol,'FaceAlpha',0.7);
	hold on;
	
	for pC=2:numel(WeightIDX)
		%build polygon
		ThisPol=TemplerPol;
		ThisPol(:,2)=[WArray(:,WeightIDX(pC))',0,0,WArray(1,WeightIDX(pC))]';
		
		%plot it
		Hands(pC)=fill(ThisPol(:,1),ThisPol(:,2),FaceColor{mod(pC,5)+1});
		set(Hands(pC),'EdgeColor',EdgeCol,'FaceAlpha',0.7);
	end
	
	%this time only lines
	for pC=numel(WeightIDX):-1:2
		%build polygon
		%ThisPol=TemplerPol;
		%ThisPol(:,2)=[WArray(:,WeightIDX(pC))',0,0,WArray(1,WeightIDX(pC))]';
		
		%plot it
		HH=plot(TimeVector,WArray(:,WeightIDX(pC)));
		set(HH,'Color','k','LineWidth',2);
		%set(Hands(pC),'EdgeColor',EdgeCol,'FaceAlpha',0.7);
	end
	
	
	yla=ylabel(TheYLab);
	xla=xlabel(TheXLab);
	xlim([TimeVector(1),TimeVector(end)]);
	set(plotAxis,'LineWidth',3,'Box','on','FontSize',16);
	set([xla,yla],'FontSize',16,'FontWeight','bold');
	%set(Hands,'LineStyle',':');
	legend(Hands,ModelNames(WeightIDX));
	hold off
		
	tit1=title(TheTitle);
	set(tit1,'FontSize',24,'FontWeight','bold');
	
	
	
	
	
	
	
end

function plotEnsembleWeightsLine(plotAxis,Weights,ModelNames,TheTitle,TimeVector)
	%plots the previous calculated Weights similar to plotEnsembleWeights but
	%as line only
	
	%DE 2013
	
	EdgeCol=[0.3 0.3 0.3];
	
	%new face colors
	Fblue=[0.2,0.15,1];
	Fgreen=[0.3,0.9,0.4];
	Fred=[1,0.2,0.2];
	Fcyan=[0.3,0.9,0.9];
	Fmagneta=[0.9,0.4,0.9];
	
	%needed later
	wheelColor={'b','g','r','c','m'}
	FaceColor={Fblue,Fgreen,Fred,Fcyan,Fmagneta}
	%wheelColor{mod(existPlots,6)+1}
	
	
	TotalNum=[];
	
	TheYLab='Weight';
	TheXLab='Time ';
	
	if nargin<5
		TimeVector=[];
	end
	
	if iscell(TimeVector)
		TotalNum=TimeVector{1};
		TimeVector=TimeVector{2};
		
	end
	
	if isempty(TimeVector)
		
		TimeVector=(1:numel(Weights))';
		TheXLab='Element';
		
	end
	
	
	%convert Weights to mat array
	WArray=cell2mat(Weights');
	
	%determine which one is on average the most "used" model
	MeanWeight=mean(WArray);
	[Temp,WeightIDX]=sort(MeanWeight,'descend');
	
	
	%go for plot
	axis(plotAxis);
	hold on;
	
	for pC=1:numel(WeightIDX)
		
		%plot it
		Hands(pC)=plot(TimeVector,WArray(:,WeightIDX(pC)));
		set(Hands(pC),'Color',FaceColor{mod(pC,5)+1},'LineWidth',3);
	end
	
	
	if ~isempty(TotalNum)
		yline=[0 1]';
		
		Eqhere=find(TotalNum~=0);
		
		for i=1:numel(Eqhere)
			xline=[TimeVector(Eqhere(i)),TimeVector(Eqhere(i))]';
			hh=plot(xline,yline);
			set(hh,'Color','k','LineWidth',0.5,'LineStyle',':');
		end
	
		
	end
	
	
	
	yla=ylabel(TheYLab);
	xla=xlabel(TheXLab);
	xlim([TimeVector(1),TimeVector(end)]);
	set(plotAxis,'LineWidth',3,'Box','on','FontSize',16);
	set([xla,yla],'FontSize',16,'FontWeight','bold');
	%set(Hands,'LineStyle',':');
	legend(Hands,ModelNames(WeightIDX));
	hold off
		
	tit1=title(TheTitle);
	set(tit1,'FontSize',24,'FontWeight','bold');
	
	
	
	
	
	
	
end

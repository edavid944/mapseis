function [CorWeight CorMatrix] = calcCorrWeight(Models,ModelType)
	%Calculates weight based on the correlation between the different model
	%like proposed by Marrzocchi et al 2012.
	
	%DE 2013
	
	%Models has to be a Cell array containing the models (either only the 
	%parameter or a full Relm based grid), ModelType is also cell array
	%the same size as Models and should contain strings ('Poisson','NegBin'
	%or 'Custom') describing the type of the forecasts.
	%For Custom and negative binomial forecasts the expected value will be 
	%used for determine the correlation
	
	%Unpack and transform data
	TheRates=cell(size(Models));
	for i = 1:numel(TheRates)
		switch ModelType{i};
			case 'Poisson'
				if any(size(Models{i})==1)
					TheRates{i}=Models{i};
				else
					TheRates{i}=Models{i}(:,9);
				end
				
			case 'NegBin'
				if any(size(Models{i})==2)
					TheRates{i}=(Models{i}(:,1).*Models{i}(:,2))./...
						(1-Models{i}(:,2)).^2;
				else
					TheRates{i}=(Models{i}(:,9).*Models{i}(:,11))./...
						(1-Models{i}(:,11)).^2;
				end
			
			case 'Custom'
				if any(size(Models{i})==2)&any(size(Models{i})==1)
					Distros=Models{i}{2};
				else
					Distros=Models{i};
				end
				
				MyRate=zeros(size(Distros));
				for k=1:numel(Distros)
					ThisDistro=Distros{k};
					NumEqRay = 0:(numel(ThisDistro)-1);
					MyRate(k) = sum(NumEqRay.*ThisDistro);
										
				end
				
				TheRates{i} = MyRate;
				
		end
		
	
	end
	
	%calculate means (so it only has to be once)
	TheMeans=zeros(size(TheRates));
	for i=1:numel(TheRates)
		TheMeans(i)=mean(TheRates{i});
	end
	%probably could be vectorized, but I doubt it matters here.
	%disp(TheMeans)
	
	%build correlation Raw matrix
	RawCorMatrix=zeros(numel(TheRates),numel(TheRates));
	for i=1:numel(TheRates)
		for k=i:numel(TheRates)
			RateA=TheRates{i};
			RateB=TheRates{k};
			MeanA=TheMeans(i);
			MeanB=TheMeans(k);
			
			CorElement=sum((RateA-MeanA).*(RateB-MeanB))./...
				((sum((RateA-MeanA).^2).*sum((RateB-MeanB).^2)).^0.5);
			
			RawCorMatrix(i,k)=CorElement;
			RawCorMatrix(k,i)=CorElement;	
		end
	
	end
	
	%disp(RawCorMatrix)
	
	%determine the eigenvectors and values
	[TheEigVec,TheEigVal] = eig(RawCorMatrix);
	
	%disp(TheEigVec)
	
	%cap eigenvectors at 1
	TheEigVal(TheEigVal>1)=1;
	
	%build corrected matrix
	CorMatrix = TheEigVec * TheEigVal * TheEigVec';
	%This works with the transponsed matrix, because the correlation matrix
	%is a real symmetrix matrix, if this would be not the case the inverse
	%instead of the transponsed has to be used (i.e. A = Q*E*Q^-1)
	
	%build Correlation Weights
	CorWeight=diag(CorMatrix)./sum(diag(CorMatrix));
	



end

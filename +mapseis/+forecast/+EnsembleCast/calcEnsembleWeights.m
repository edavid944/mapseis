function [CombineWeights S,BestModel,LikeAll] = calcEnsembleWeights(Models,ModelType,ShortCat,CorWeights,WeightMode,L0const,pastLike)
	%Calculates weight based on likelihood and the correlation weights
	%like proposed by Marrzocchi et al 2012.
	
	%DE 2013
	
	%Models has to be a Cell array containing the models (either only the 
	%parameter or a full Relm based grid), ModelType is also cell array
	%the same size as Models and should contain strings ('Poisson','NegBin'
	%or 'Custom') describing the type of the forecasts.
	
	import mapseis.forecast.EnsembleCast.pickProbBin;
	
	
	if nargin<6
		L0const=0;
		pastLike=[];
		
	end
	
	
	if nargin<7
		pastLike=[];
		
	end
	
	
		
	
	%assign the earthquakes to the grid
	%get the grid first
	switch ModelType{1}
		case {'Poisson','NegBin'}
			SearchGrid=Models{1};
		case 'Custom'
			SearchGrid=Models{1}{1};
		
	end
	
	EqCount=zeros(numel(SearchGrid(:,1)),1);
	selected=false(numel(SearchGrid(:,1)),1);
	
	for i=1:numel(SearchGrid(:,1))
		%no parfor, steps are probably to small
		InLon=ShortCat(:,1)>=SearchGrid(i,1)&ShortCat(:,1)<SearchGrid(i,2);
		InLat=ShortCat(:,2)>=SearchGrid(i,3)&ShortCat(:,2)<SearchGrid(i,4);
		InDepth=ShortCat(:,3)>=SearchGrid(i,5)&ShortCat(:,3)<SearchGrid(i,6);
		InMag=ShortCat(:,5)>=SearchGrid(i,7)&ShortCat(:,5)<SearchGrid(i,8);
						
						
		EqCount(i)=sum(InLon&InLat&InDepth&InMag);
		selected(i)=true;
						
	end	
	
	
	SparamRaw=NaN(size(Models));
	for i=1:numel(Models)
		LikeACat=0;
		switch ModelType{i}
			case 'Poisson'
				TheCast=Models{i};
				%disp([num2str(i),': ',num2str(sum(TheCast(:,9)))])
				LikeIt=-TheCast(:,9)+EqCount.*log(TheCast(:,9))-log(factorial(EqCount));					
				LikeACat=nansum(LikeIt(~isinf(LikeIt)));
					
					
			case 'NegBin'
				TheCast=Models{i};
				LikeIt=log(pdf('nbin',EqCount,TheCast(:,9),TheCast(:,11)));
				LikeACat=nansum(LikeIt(~isinf(LikeIt)));
					
					
			case 'Custom'
				TheCast=Models{i};
				SearchGrid=TheCast{1};
				Distros=TheCast{2};
					
				LikeIt=log(cellfun(@(x,y) pickProbBin(x,y),Distros,num2cell(EqCount)));
				LikeACat=nansum(LikeIt(~isinf(LikeIt)));
					
					
		end
		
		%Save Likelihood
		LikeAll(i,1)=LikeACat;
		
	end
	
	if isempty(pastLike)
		pastLike=zeros(size(LikeAll));
	end	
	LikeAll=LikeAll+pastLike;
	
	%disp(LikeAll)
	
	%find best model=
	[LikeBest maxId]=max(LikeAll);
	%disp(maxId)
	%calculate likelihood weight
	switch WeightMode
		case 'BMA'
			%disp('used BMA')
			S = exp(LikeAll);
		case 'SMA'
			%disp('used SMA')
			S = 1./abs(LikeAll);
		case 'gSMA'
			%disp('used gSMA')
			%S = 1./abs(LikeAll-L0const);
			S = 1./abs(L0const+(LikeBest-LikeAll));
	end
	
	%calculate overall weights
	CombineWeights = (CorWeights.*S)./sum(CorWeights.*S);
	%disp(S/sum(S))
	BestModel=maxId;
	
end

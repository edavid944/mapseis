function [TheEnsemble,Weights,BenchMarkTimes] = buildEnsembleCast(ForeObjs,Datastore,Filterlist,EnsConfig)
	%Builds a forecast object containing a weighted average ensemble forecasts
	%of the input Forecasts (forecast objects). This is based on the work of
	%Marrzocchi et al 2012.
	
	
	%DE 2013
	
	%FINISHED BUT UNTESTED
	
	import mapseis.forecast.*;
	import mapseis.forecast.EnsembleCast.*;
	import mapseis.filter.*;	
        import mapseis.projector.*;
	
        FullTimer=tic;
        
	%EnsConfig should be structure containing the following:
	DefaultConfig = struct(	'WeightMode','SMA',...
				'L0const',1,...
				'EstimationMode','dynamic',...
				'WeightTime',[[]],...
				'PlanB','KeepLast',...
				'AvoidEmpty',false);
				
	%WeightMode: is the weigthing schema used it can be 'BMA', 'SMA' or 
	%'gSMA' and they revere to the ones described in the mentioned paper.
	%L0Const: is needed only for 'gSMA'
	%EstimationMode: defines how often the weights are estimated, if
	%set to 'dynamic' the parameters will be updated for every timestep,
	%'fixed' will estimate the parameter only for the specified WeightTime
	%and use an average value of thos weights for every timestep. More modes
	%may will be added later.
	%WeightTime: used for the fixed mode it sets the time intervall in which
	%the parameters should be estimated, it has to be an 2 value vector with
	%Wtime(1) equals the start time and Wtime(2) equals the end time and both
	%values should be in the datenum format.
	%PlanB: This describes what to do in case now parameter could be 
	%determined for a timestep or in case no earthquake is present. 'KeepLast'
	%will just use the last weigths, 'CorOnly' only the correlation weights
	%are used.
	%AvoidEmpty: if set to true, the weights will not be determined for 
	%timesteps containing no earthquake, 
	
				
	if nargin<4
		EnsConfig=DefaultConfig;
	end
	
	
	%get timesteps (in this version it is assumbed that all forecasts
	%span the same region and the same time intervall
	TimeVector=ForeObjs{1}.TimeVector;
	STEPTimes=NaN(size(TimeVector));
	
	%determine the types of the forecasts (needed later)
	ModType=cell(size(ForeObjs));
	for mC=1:numel(ForeObjs)
		ModType{mC}=ForeObjs{mC}.ForecastType.Type;			
	end	
	
	%get grid from the first object
	TheGrid=ForeObjs{1}.getFullGrid;
	TripGrid=ForeObjs{1}.getGridNodes;
	
	%build a raw forecast object
	TheEnsemble=ForecastObj('EnsembleForecast');
	TheEnsemble.initForecast('Poisson',{});
	TheEnsemble.addGrid(TheGrid,TripGrid);
	TheEnsemble.RealTimeMode=ForeObjs{1}.RealTimeMode;
	
	%NOTE: it is currently not possible to built anything else than a poisson
	%forecast, simply because there is no standard method to combine negative
	%binomial forecasts or even discretized distributions.
	
	
	
	%NOTE 2: the fixed part my need some fixes.
	
	switch EnsConfig.EstimationMode
		case 'fixed'
			%estimate weightsWeightTime
			inTime=	TimeVector>=DefaultConfig.WeightTime(1) &...
				TimeVector<=DefaultConfig.WeightTime(2);	
			
			EstTimes=TimeVector(inTime);
			
			
			RawEstWeights=NaN(numel(EstTimes),numel(ForeObjs));
			for tC=1:numel(EstTimes)
				hasData=false(size(ForeObjs));
				ForecastNow=cell(size(ForeObjs));
				
				for mC=1:numel(ForeObjs)
					try
						[CSEPGrid ForeTime ForeLength]= ForeObjs{mC}.getCSEPForecast(EstTimes(tC));
						ForecastNow{mC}=CSEPGrid;
						hasData(mC)=true;
						
					catch
						disp('Forecast not existing');
					end
					
					
				end
				
					
				%check if any forecast is existing at all
				if ~any(hasData)
					warning(['Time: ',EstTimes(tC),'has no forecast']);
					continue
				end
				
				%prepare earthquake data
				TimeInt=[ForeTime,ForeTime+ForeLength];
				Range='in';
        			
				TestFilter=FilterListCloner(Filterlist);
				TimeFilt=TestFilter.getByName('Time');
				TimeFilt.setRange(Range,TimeInt);

				TestFilter.changeData(obj.Datastore);
				TestFilter.updateNoEvent;
				selected=TestFilter.getSelected;
				TestFilter.PackIt;
							
				[ShortCat temp]=getShortCatalog(Datastore,selected);
				%to avoid memory problems
				clear('TestFilter');	
				
				
				if ~sum(hasData)==1
					Models=ForecastNow(hasData);
					ModelType=ModType(hasData);
					
					[CorWeight CorMatrix] = calcCorrWeight(Models,ModelType);
					[CombineWeights S] = calcEnsembleWeights(Models,ModelType,ShortCat,...
							CorWeight,DefaultConfig.WeightMode,DefaultConfig.L0const);
					TheWeights=NaN(size(ForeObjs));
					TheWeights(hasData)=CombineWeights;
					RawEstWeights(tC,:)=TheWeights';
					%CHECK if dimensions agree
				
				else
					TempVec=NaN(size(ForeObjs));
					TempVec(hasData)=1;
					RawEstWeights(tC,:)=TempVec';
					%CHECK if dimensions agree
				end
				
				
				
			end
			
			%Calculate the mean from the calculated Weights
			Weights=nanmean(RawEstWeights);
			%renorm them 
			Weights=Weights/sum(Weights);
			
			
			%calculate the forecasts
			for tC=1:numel(TimeVector)
				
				TimIt=tic;
				
				%get data from the forecasts
				hasData=false(size(ForeObjs));
				ForecastNow=cell(size(ForeObjs));
				for mC=1:numel(ForeObjs)
					try
						[CSEPGrid ForeTime ForeLength]= ForeObjs{mC}.getCSEPForecast(TimeVector(tC));
						ForecastNow{mC}=CSEPGrid;
						hasData(mC)=true;
						
					catch
						disp('Forecast not existing');
					end
						
					
					
				end
				
				%check if any forecast is existing at all
				if ~any(hasData)
					warning(['Time: ',TimeVector(tC),'has no forecast']);
					continue
				end
				
				
				usableWeights=Weights(hasData);
				%combine forecast
				RateVec=zeros(size(TheGrid(:,1)));
				for mC=1:sum(hasData)
					switch ModelType{mC}
						case 'Poisson'
							RateVec=RateVec+Models{mC}*usableWeighs(mC);
							
						case 'NegBin'
							ExpVal=(Models{mC}(:,9).*Models{mC}(:,11))./...
								(1-Models{mC}(:,11)).^2;
							RateVec=RateVec+ExpVal*usableWeighs(mC);
							
						case 'Custom'
							Distros=Models{i}{2};
							ExpVal=zeros(size(Distros));
							for k=1:numel(Distros)
								ThisDistro=Distros{k};
								NumEqRay = 0:(numel(ThisDistro)-1);
								ExpVal(k) = sum(NumEqRay.*ThisDistro);				
							end
								
							RateVec=RateVec+ExpVal*usableWeighs(mC);
							
							
					end
				end
				
				ThisEns=TheGrid;
				ThisEns(:,9)=RateVec;
				
				%Store to forecast object
				TheEnsemble.addForecast(ThisEns(:,9),ForeTime,ForeLength,ThisEns(:,10));
				
				STEPTimes(tC)=toc(TimIt);
				
			end	
			
			
			
		
		case 'dynamic'
			KeepLast=strcmp(EnsConfig.PlanB,'KeepLast');	
			NoEmpty=EnsConfig.AvoidEmpty;
			TheWeights=NaN(size(ForeObjs));
			
			LastWeights=[];
			for tC=1:numel(TimeVector)
				disp(tC)
				TimIt=tic;
				
				%get data from the forecasts
				hasData=false(size(ForeObjs));
				ForecastNow=cell(size(ForeObjs));
				for mC=1:numel(ForeObjs)
					try
						[CSEPGrid ForeTime ForeLength]= ForeObjs{mC}.getCSEPForecast(TimeVector(tC));
						ForecastNow{mC}=CSEPGrid;
						hasData(mC)=true;
						
					catch
						disp('Forecast not existing');
					end
						
					
					
				end
				
				%check if any forecast is existing at all
				if ~any(hasData)
					warning(['Time: ',TimeVector(tC),'has no forecast']);
					continue
				end
				
				%prepare earthquake data
				TimeInt=[ForeTime,ForeTime+ForeLength];
				Range='in';
        			
				TestFilter=FilterListCloner(Filterlist);
				TimeFilt=TestFilter.getByName('Time');
				TimeFilt.setRange(Range,TimeInt);

				TestFilter.changeData(Datastore);
				TestFilter.updateNoEvent;
				selected=TestFilter.getSelected;
				TestFilter.PackIt;
							
				[ShortCat temp]=getShortCatalog(Datastore,selected);
				%to avoid memory problems
				clear('TestFilter');	
			
				%check if parameters should be estimated
				if sum(hasData)~=1
						EstParCrit=~isempty(ShortCat)|...
						~NoEmpty|isempty(TheWeights)|...
						any(isnan(TheWeights(hasData)));
					if EstParCrit
						Models=ForecastNow(hasData);
						ModelType=ModType(hasData);
						%weights have to be estimated
						[CorWeight CorMatrix] = calcCorrWeight(Models,ModelType);
						%disp(CorWeight)
						%disp(size(CorWeight))
						%disp(size(LastWeights))
						
						if ~isempty(LastWeights)
							CorWeight=CorWeight.*LastWeights';
						end
						
						
						
						[CombineWeights S] = calcEnsembleWeights(Models,ModelType,ShortCat,...
								CorWeight,EnsConfig.WeightMode,EnsConfig.L0const);
						%disp(CombineWeights)
						TheWeights=NaN(size(ForeObjs));
						TheWeights(hasData)=CombineWeights;
								
						
					else
						if KeepLast
							Models=ForecastNow(hasData);
							ModelType=ModType(hasData);
						else
							Models=ForecastNow(hasData);
							ModelType=ModType(hasData);
							[CorWeight CorMatrix] = calcCorrWeight(Models,ModelType);
							TheWeights=NaN(size(ForeObjs));
							TheWeights(hasData)=CombineWeights;
							
						end
					end
				
					%save Weights
					Weights{tC}=TheWeights;
					useableWeights=TheWeights(hasData);
					LastWeights=useableWeights;
					
					%combine forecast
					RateVec=zeros(size(TheGrid(:,1)));
					for mC=1:sum(hasData)
						switch ModelType{mC}
							case 'Poisson'
								RateVec=RateVec+Models{mC}(:,9)*useableWeights(mC);
								
							case 'NegBin'
								ExpVal=(Models{mC}(:,9).*Models{mC}(:,11))./...
									(1-Models{mC}(:,11)).^2;
								RateVec=RateVec+ExpVal*usableWeighs(mC);
								
							case 'Custom'
								Distros=Models{i}{2};
								ExpVal=zeros(size(Distros));
								for k=1:numel(Distros)
									ThisDistro=Distros{k};
									NumEqRay = 0:(numel(ThisDistro)-1);
									ExpVal(k) = sum(NumEqRay.*ThisDistro);				
								end
								
								RateVec=RateVec+ExpVal*usableWeighs(mC);
							
							
						end
					end
					
					ThisEns=TheGrid;
					ThisEns(:,9)=RateVec;
				else
					ThisEns=ForecastNow{hasData};
					TempVec=NaN(size(ForeObjs));
					TempVec(hasData)=1;
					Weights{tC}=TempVec;
				end
				
				
				%Store to forecast object
				TheEnsemble.addForecast(ThisEns(:,9),ForeTime,ForeLength,ThisEns(:,10));
				STEPTimes(tC)=toc(TimIt);
			end
			
		
	end
	
	
	OverallTime=toc(FullTimer);
	BenchMarkTimes.TotalTime=OverallTime;
	BenchMarkTimes.SingleStepTime=STEPTimes;
	BenchMarkTimes.TimeSteps=TimeVector;
	
	
	
	
	
	
	





end

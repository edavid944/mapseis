function val = pickProbBin(Distro,NumEQ)
       	%short function, needed for picking the probability of a grid node
       	%It is the same as in Ltest and similar is used, this is just an external
       	%version, needed for the  ensemble weights calculation.
       	
       	
       	try
       		val = log(Distro(NumEQ+1));
       	catch
      		val = NaN;
      	end
end

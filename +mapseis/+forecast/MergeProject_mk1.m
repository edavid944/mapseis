function MergeProject_mk1(ProjectA,ProjectB)
	%This function will merge two forecastsproject by adding everything from 
	%ProjectB into ProjectA. In this version it will only work if ProjectA
	%and ProjectB have the same Region and Timesteps. It is also suggested to 
	%change the savepath of the project by hand as will be the same as for
	%ProjectA.
	
	%It would be possible to copy everything into a new object, but in case
	%of large projects, this would double the used memory.
	%Also remember the object are copied by handle, means content from ProjectB
	%is, if it is an object) still connected to B, if it changed there it will
	%also be changed in the Merged project.

	%At the moment best apply it on finished projects
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.forecast.*;

	%first check if the two project are similar:
	RegA=ProjectA.buildRegionConfig;
	RegB=ProjectB.buildRegionConfig;
	isSimilar=true;
	
	%time first (only checking AvailableTimes
	isSimilar = isSimilar & all(ProjectA.AvailableTimes==ProjectB.AvailableTimes);

	%now region
	isSimilar = isSimilar & all(RegA.RegionPoly==RegB.RegionPoly);
	isSimilar = isSimilar & (RegA.LonSpacing==RegB.LonSpacing);
	isSimilar = isSimilar & (RegA.LatSpacing==RegB.LatSpacing);
	isSimilar = isSimilar & all(RegA.MagVector==RegB.MagVector);
	isSimilar = isSimilar & all(RegA.DepthVector==RegB.DepthVector);
	
	if ~isSimilar
		error('ProjectA and ProjectB do not match')
	end
	
	
	%Now let the fun begin....
	
	%check state of the projects first
	StateA=ProjectA.ProjectStatus;
	StateB=ProjectB.ProjectStatus;
	
	%Model from B to A
	ModofA=ProjectA.ModelPlugIns;
	ModConfofA=ProjectA.ModelPlugIns;
	ModofB=ProjectB.ModelPlugIns;
	ModConfofB=ProjectB.ModelPlugIns;
	
	MergedMods=ModofA;
	MergedMods(end+1:end+numel(ModofB(:,1)),:)=ModofB;
	MergedModConfs=ModConfofA;
	MergedModConfs(end+1:end+numel(ModConfofB(:,1)),:)=ModConfofB;
	
	%forecasts
	ForeA=ProjectA.Forecasts;
	ForeB=ProjectB.Forecasts;
	MergedFore=ForeA;
	MergedFore(end+1:end+numel(ForeB(:,1)),:)=ForeB;
	
	%logs
	LogsA=ProjectA.OutputLogs;
	LogsB=ProjectB.OutputLogs;
	MergedLogs=LogsA;
	MergedLogs(end+1:end+numel(LogsB(:,1)),:)=LogsB;
	
	%UsedConf
	ConfsA=ProjectA.UsedConfigs;
	ConfsB=ProjectB.UsedConfigs;
	MergedConfs=ConfsA;
	MergedConfs(end+1:end+numel(ConfsB(:,1)),:)=ConfsB;
	
	
	%merge CalcProgress
	ProgressA=ProjectA.CalcProgress;
	ProgressB=ProjectB.CalcProgress;
	MergedProgress=ProgressA;
	ModNameB=fieldnames(ProgressB.ModelProgress);
	
	for i=1:numel(ModNameB)
		MergedProgress.ModelProgress.(ModNameB{i}) = ProgressB.ModelProgress.(ModNameB{i});
	end
	
	
	
	%merge Benchmark
	BenchA=ProjectA.BenchmarkData;
	BenchB=ProjectB.BenchmarkData;
	
	if ~isempty(BenchA)&~isempty(BenchB)
		MergedBench=BenchA;
		MergedBench.Models=fieldnames(MergedProgress.ModelProgress);
		for i=1:numel(ModNameB)
			MergedBench.ModelTimes.(ModNameB{i}) = BenchB.ModelTimes.(ModNameB{i});
		end
		
		%it probably would have taken longe if everything was calculated together
		MergedBench.CalcTimeStep = MergedBench.CalcTimeStep + BenchB.CalcTimeStep;
		MergedBench.TotalTime = sum(MergedBench.CalcTimeStep); 
		
		%filesize is a bit a problem, at the moment they just summed up, but this
		%is not totally realistic.
		MergedBench.FileSize = MergedBench.FileSize + BenchB.FileSize;
		ProjectA.setParameter('BenchmarkData',MergedBench);
	end
	
	
	%write everything into ProjectA
	ProjectA.setParameter('CalcProgress',MergedProgress);
	ProjectA.setParameter('ModelPlugIns',MergedMods);
	ProjectA.setParameter('ModelConfig',MergedModConfs);
	
	ProjectA.setParameter('Forecasts',MergedFore);
	ProjectA.setParameter('OutputLogs',MergedLogs);
	ProjectA.setParameter('UsedConfigs',MergedConfs);
	
	
	
	
	if ~(strcmp(StateA,'finished')&strcmp(StateB,'finished'))
		%not both where finished, at the moment only issue a warning
		warning('A project was not finished before merging');
	end
	
	ProjectA.setParameter('ProjectStatus','finished');
	
end

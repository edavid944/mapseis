classdef ForecastProject < handle
	%This the first version of the forecast project object. The object is 
	%more less a data structure with a some needed functionallity. The 
	%actuall work will be done by the ForecastCalculater and the set up of 
	%this object will be done by the ForeProjectBuilder
	
	
	properties(SetAccess = private)
        	Name
        	ID
        	ObjVersion
        	MetaData
        	ProjectStatus
        	CalcProgress
        	
        	
        	DataMode
        	Datastore
        	Filterlist
        	LearningFilter
        	
        	ModelPlugIns
        	ModelConfig
        	InputCatalogs
        	Forecasts
        	MArchive
        	OutputLogs
        	UsedConfigs
        	
        	TestPolygon
        	BoundBox
        	Depths
        	Magnitudes
        	Spacings
        	ForecastGrid
        	AutoExtend
        	RegionExtend
        	ForecastLength
        	PredictionIntervall
        	UpdateMag
        	StartTime
        	EndTime
        	CurrentEndTime
        	AvailableTimes
        	
        	LearningPeriod
        	LearningUpdate
        	LearningLength
        	LearningLag
        	
        	ArchivePath
        	CalcPath
        	StoreProject
        	ProjectConfig
        	
        	LastParameters
        	
        	CalculatorSpace
        	BenchmarkData
        	AdditionalData
        	LogObject
        	
        end
    

    
        methods
        	function obj=ForecastProject(Name)
        		%object constructor and a lot of explantation about
        		%the different parameters
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		
        		%Meta data and progress 
        		obj.Name=Name;
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.ObjVersion='0.9';
			obj.MetaData= struct(	'author',[[]],...
						'version',[[]],...
						'created',[[]],...
						'updated',[[]],...
						'comment',[[]]);
			obj.ProjectStatus = 'empty';
			obj.CalcProgress = struct(	'ModelProgress',[[]],...
							'StopProgress',[[]],...
							'Times',[[]]);
							
			%Name: Name of the project no surprise
			
			%ID: an unique ID which can be used for identifiaction
			
			%ObjVersion: is just the version of the object in used 
			%internally
			
			%MetaData: is a structure containing parameters like 
			%author, version ,creation date, last update and comment
			
			%ProjectStatus: is a single Keyword which gives the status
			%of the project, it can be:
			%	'empty': empty object nothing in it
			%	'config': configuration started but not parts are
			%		  missing
			%	'ready': everything is configurated and the project
			%		 is ready for calculation
			%	'started': calculation has started but is not 
			%		   is not finished
			%	'cont':	the project is finished calculation but 
			%		is for the moment but has to be updated
			%		when new data is available (only 
			%		continues mode)
			%	'finished': everything is calculated and the 
			%		    project is ready for testing
			%	'changed': project was 'finished' or 'started'
			%		   was edited afterwards (e.g. new model
			%		   added), the project has to be re-checked
			%		   and calculation has to be restarted.
			%	'recalc': the project was changed and is ready to 
			%		  recalculated
			
			%CalcProgress: This structure contains everything about the
			%progress of the calculation it is a structure with multiple
			%fields not all defined yet. It will consist out of two main
			%fields: 'ModelProgress' and 'StopProgess'. Both will contain
			%themself a structure consisting out of every model as fieldname
			%together with version number. The in the field 'ModelProgress' 
			%the progress of the calculation will be stored for each time
			%step: 0 means not calculated, 1 means calculated, and 2 means
			%calculated but model or timestep has been removed from the 
			%project (can be reactivated) and -1 means the model was never 
			%calculated and has be removed. In the second field each step is
			%a boolean value which is true if the model is activated or 
			%false if the model is deactivated and should not be calculated
			%for this timestep. The field 'ModelProgress' is normally auto-
			%matically managed by the projectbuilder and the function which 
			%does this is integrated in this object. The field 'StopProgress'
			%is a manually managed field and its purpose is to allow the 
			%user to stop calculation for certain step, for instance to stop
			%model which has an updated version from being calculated without
			%deleting the whole model.
			%The field time is similar to the available times but is expanded to
			%the range of progress vector (for those cases the time range was
			%shortened after calculation)
			%New ModelProgress code -2: A calculation was attended but failed
			%UNFINISHED: -2 may not be fully integrated right now, I have to 
			%check where a change is needed
			
			
			%Catalog data
			obj.DataMode='Datastore';
			obj.Datastore=[];
			obj.Filterlist=[];
			obj.LearningFilter=[];
			%DataMode: sets the format of Datastore, if this is 
			%'Datastore', the Datastore property should be a object
			%of the type datastore (typical for mapseis). If the 
			%keyword is 'Source' obj.Datastore should be a source-
			%object (interface/abstract will be provided) which 
			%generates a Datastore catalog from a online (or offline)
			%source or provides datastore similar access.
			
			%Datastore: as described before
			
			%Filterlist: the filterlist contains the selection of the 
			%earthquakes in the catalog as well as the test region 
			%polygon (see filterlist for more details). It is of course
			%also possible to produce a dummy object, which provides
			%only the needed parts with the same commands as a normal 
			%filterlist
			
			%LearningFilter: this is optional, if a filterlist is added
			%the learning period will use a different "region" than the
			%forecast target. The builder will often create a learning
			%filter itself if needed (very often the case)
			%IMPORTANT: It is the LearningFilter which actually creates
			%the input catalog for the models. The Filterlist property
			%will be used mostly for testing. 
			
			
			
			%The models and the "storage area" for input and output 
			obj.ModelPlugIns={};
			obj.ModelConfig={};
			obj.InputCatalogs={};
			obj.Forecasts={};
			obj.MArchive={};
			obj.OutputLogs={};
			obj.UsedConfigs={};
			%ModelPlugIns: this is a cell array with one column 
			%containing the names second one containing the versions 
			%of the model and the third one containing the model plugin 
			%objects (they have to be configurated at least partially). 
			%See the ForecastPlugIn interface/abstract for more 
			%information
			
			%ModelConfig: contains the basic configuration of the 
			%different models in form of a cell array, each cell 
			%containing a structure with the parameters as needed by
			%the model in the first column and in the second column the
			%ConfigID is stored.
			
			%InputCatalogs: here the actually used catalogs for the 
			%forecast calculation are stored (format may vary from
			%datastore to ShortCat to boolean selection vector) 
			
			%Forecasts: the resulting forecast from a calculation are
			%stored here. It is a cell array with two column, first 
			%one storing the ForecastObj for each model and the 
			%second one contains a cell array with paths to forecast
			%files in case this option is activated.
			
			%MArchive: Forecast from pervious models can be archived
			%there and in a future version a restore may also be
			%possible. (Only fully removed models can be archived
			%there, not single timesteps)
			
			%OutputLogs: similar to obj.Forecasts but for the output
			%logs incase they are existing and should be saved.
			
			%UsedConfigs: stores the actual configuration used for each
			%model. can be switchen on and off. I guess I will also use
			%the ForecastLog object for this purpose, so again similar to 
			%obj.Forecasts;
			
				
			%Testregion configuration
			obj.TestPolygon=[];
			obj.BoundBox=[]
			obj.Depths=[];
			obj.Magnitudes=[];
			obj.Spacings=[];
			obj.ForecastGrid=[];
			obj.AutoExtend=false;
			obj.RegionExtend=[0.2,0.2];
			obj.ForecastLength=[];
			obj.PredictionIntervall=[];
			obj.UpdateMag=[];
			obj.StartTime=[];
			obj.EndTime=[];
			obj.CurrentEndTime=[];
			obj.AvailableTimes=[];
				
			%TestPolygon: the test region as polygon (double vector
			%first column lon second lat). It will be automatically 
			%generated from the Filterlist
			
			%BoundBox: boundary box around the TestPolygon, will 
			%automatically be generated from the Filterlist
			
			%Depths: an array with all the depth bins used (middle
			%point). In most cases this only contains min and max
			%depth as spatial 3D models are not very common.
			
			%Magnitudes: an array with the magnitude bins (middle
			%of the bins). In case of a model without magnitude bins
			%this will only contain min and max of the magnitude
			
			%Spacings: contains the bin size/spacing of the different
			%dimension. It is a row vector with following values,
			%[lon_space, lat_space, mag_bin, depth_bin]. The spacing
			%for a not used axis will be set to NaN. In case of only
			%one spacing for lon/lat, the spacing will automatically
			%set for both.
			
			%ForecastGrid: This is were the generated grid is 
			%stored in. It has the format of a empty forecast object
			
			%AutoExtend: If set to true the Calculator will extend
			%the polygon used for selection of the learning period
			%earthquake catalog by the amount defined in RegionExtend.
			%NOTE: this will not infuence the grid in anyway.
			
			%RegionExtend: used in the AutoExtend mode. Defines the 
			%amount the polygon is enlarged [lonExtend latExtend]
			
			%ForecastLength: is the length of the forecasts in days
			%years will be converted into 365days and vice versa
			
			%PredicationIntervall: determines how often a forecast
			%will be updated. Many times this will be the same value
			%as the ForecastLength, but it may be interesting to lower
			%it. It is also possible to use to string 'realtime' as 
			%value, in this case the forecasts will be updated every
			%time a earthquake with a magnitude above UpdateMag happens
			
			%UpdateMag: only used for realtime mode, it set how large
			%an earthquake has to be to trigger a update of the forecasts
			
			%StartTime: Simple, the time after which forecasts are 
			%produced, the format is the matlab typical datenum.
			
			%EndTime: the time for the last update (careful, the last
			%forecast will last from EndTime to EndTime+ForecastLength).
			%The format is as usual datenum. It is also possible, to use
			%the string 'open' as value, in this case the forecast will 
			%go one until now data is available, practical in case where
			%the catalog is updated on a regular basis.
			
			%CurrentEndTime: Don't know if I need this property yet, but
			%I know if don't have it and need it I will regret it. It is
			%meant for the 'open' case where the endtime is varying, it does
			%not have to be set directly. 
			
			%AvailableTimes: is an vector containing all the starting
			%times of the currently possible forecasts. Meant as 
			%guideline for checking which forecasts are already calc-
			%ulated.
			
			%Setting for update of the learning data
			obj.LearningPeriod=[];
			obj.LearningUpdate='update';
			obj.LearningLength='full';
			obj.LearningLag=0;
			
			%LearningPeriod: the starttime and endtime of the initial
			%learning period ([start,end]) in the datenum format. 
			%This period will be used for the preparetion of the models,
			%often the end time of the learning period is the start
			%time of the forecast or equal to start time -lag. The 
			%start time of the learning period is also used as earliest
			%possible time of any learning period, no forecast will have
			%learning period starting earlier than that value. The value
			%can be set to infinite if needed.
			
			%LearningUpdate: determines how the learning period is 
			%updated. Default is 'update', in this case the period
			%will automatically updated. If it is set to 'fixed' the
			%period will be same for the whole time.
			
			%LearningLength: sets the length of the learning period
			%in days, default is here 'full', in this case all data
			%available is used and the period gets longer with time.
			%If a number is used instead the period is kept the same
			%length and past events are filtered out. This may 
			%practical in some cases. Full length with fixed time
			%period is not possible.
			
			%LearningLag: can be used to introduce a "lag" to learning
			%period, it is only applyable in the 'update' mode. The 
			%endtime of the learning period will be determined like 
			%this: endtime=currentForecastTime-LearningLag. May be 
			%interesting to toy around with it. Default value is 0
			

			%Some Paths and Project configurations
			obj.ArchivePath='./Forecast_Results';
			obj.CalcPath='./Temporary_Files';
			obj.StoreProject='./Forecast_Results/MyForecasts.mat';
			obj.ProjectConfig=struct(	'ArchiveForecasts',false,...
							'StoreLogs',true,...
							'ArchiveLogs',false,...
							'SaveConfig',true,...
							'ArchiveDelete',false,...
							'ParallelProcessing',false,...
							'CatalogStore','ShortCat',...
							'Benchmark',true,...
							'SaveAddOnData',true);
			
			%ArchivePath: path where the forecast output files are
			%copied into, in case the the forecasts file are archived
			%There will be a subfolder for each model. It may be wise
			%to not directly use the default path, as it is a relativ
			%path.
			
			%CalcPath: Path for the temporary files used by and for the
			%models. The folder will be emptied on each calculation, 
			%so nothing should be stored there only. As with ArchivePath
			%it may be wise to set the path the first time, because of 
			%default path being relativ.
				
			%StoreProject: Filename and path where this object should be
			%saved after every calculation step. This can be different 
			%from the filename the uncalculated object has been saved in
			%e.g. could a not calculated object be saved under name
			%'Forecast.mat' but the calculated version will be stored under
			%'Forecast_calc.mat')
			
			%ProjectConfig: is structure containing the configuration 
			%the project itself. It consist out of the following fields
				%ArchiveForecasts, set to true if forecast files
				%		   should be archive
				%StoreLogs, set to true if Logs should be store
				%	    internally (opposed to not store at all)
				%ArchiveLogs, set to true if the logs files should be
				%	      archived as well.
				%SaveConfig, set to true if the actual configuration
				%	     for each forecast should be saved.
				%ArchiveDelete, will allow to archive delete models and 
				%		restore them later, but is currently not
				%		fully available (cannot be restored now)
				%ParallelProcessing, sets the parallel processing on and
				%		     off if available.
				%CatalogStore,  sets the format of the input catalogs
				%		stored. It can be
						%Filterlist: for Filterlists
						%ShortCat: for ShortCatalog
						%Boolean: for selection vector
						%Datastore: for datastore format
						%Short_and_Bool: ShortCatalog and
						%		Boolean in a cell
						%Data_and_Bool: Datastore and boolean
						%None: Nothing will be stored, saves a lot of space
				%Benchmark, if set true, the calculation times will be logged
				%SaveAddOnData, if set to true the AddonData generated by some
				%		models will be saved.
				
						
			obj.LastParameters=[];
			%This property contains all configuration of the last 
			%calculation. It is needed to determine wether the parameters
			%changed and if a recalc is needed. 
			
			
			obj.CalculatorSpace=[];
			obj.BenchmarkData=[];
			obj.AdditionalData=[];
			obj.LogObject=[];
			
			%The calculator space is only used by the calculator. It allows 
			%to temporary save parameters needed by the calculator and not 
			%already stored in elsewhere in the project object.
			
			%BenchmarkData will be filled by the calculator (if benchmarking is
			%turned on) and will log how much time is needed for each step, model
			%and so on.
			
			%AdditionalData is meant for additional data produced by the forecasts
			%it is a structure with the model names as fieldnames, how the data in 
			%there is managed is up to calculator and calculation plugin
			
			%The LogObject will be added later, and it will allow to store
			%all warnings, errors and notes into a log file as well as display
			%them on the screen instead of only showing them once. This LogObject
			%will mostly be used by the Calculator and the tester, but every other
			%object of the framework can use it, rule of thumb is that the part of 
			%framework which normally would create the message with warning, error
			%or disp should use the object, not the one sending the error code.
			
			
			
				
        	end
        	
        	function setParameter(obj,Parameter,Value);
        		%universal set routine
        		%both input can be cell array to add several parameters 
        		%at once
        		
        		if ~iscell(Parameter)
        			obj.(Parameter)=Value;
        		else
        			for i=1:numel(Parameter);
        				obj.(Parameter{i})=Value{i};
        			end
        		end
        		
        		
        		switch obj.ProjectStatus
        			case 'empty'
        				obj.ProjectStatus='config';
        			case {'ready','started','finished','cont'}
        				obj.ProjectStatus='changed';
        				
        				
        			case 'changed'
        				obj.checkState;
        		end
        	
        		
        		
        		
        	end
        	
        	
        	function Value = getParameter(obj,Parameter)
        		%more for completeness, currently it would be possible
        		%to access the values without this method, but it may
        		%change.
        		if ~iscell(Parameter)
        			Value=obj.(Parameter);
        		else
        			for i=1:numel(Parameter);
        				Value{i}=obj.(Parameter{i});
        			end
        		end
        	
        	end
        	
        	
        	function extBack=backupParameters(obj,externalBack,noTime)
        		%writes needed parts into LastParameters, should be 
        		%done after a completed calculation
        		%check if everthing is completed
        		if nargin<2
        			externalBack=false;
        			noTime=false;
        		end
        		
        		
        		extBack=[];
        		
        		SaveList={'DataMode','Datastore','Filterlist',...
        			'LearningFilter','TestPolygon',...
        			'Depths','Magnitudes','Spacings','ForecastGrid','ForecastLength',...
        			'PredictionIntervall','UpdateMag','StartTime','EndTime',...
        			'LearningPeriod','LearningUpdate','StoreProject',...
        			'LearningLength','LearningLag','ArchivePath','CalcPath','ProjectConfig'};
        			
        		%For checking parameter change,  without change of start and end time
        		SaveList2={'DataMode','Datastore','Filterlist',...
        			'LearningFilter','TestPolygon',...
        			'Depths','Magnitudes','Spacings','ForecastGrid','ForecastLength',...
        			'PredictionIntervall','UpdateMag',...
        			'LearningUpdate','StoreProject',...
        			'LearningLength','LearningLag','ArchivePath','CalcPath','ProjectConfig'};
        			
        		if externalBack
        			if noTime
					for i=1:numel(SaveList2)
						extBack.(SaveList2{i})=obj.(SaveList2{i});
					end
				else
					for i=1:numel(SaveList)
						extBack.(SaveList{i})=obj.(SaveList{i});
					end
				end
				
        		else
				for i=1:numel(SaveList)
					obj.LastParameters.(SaveList{i})=obj.(SaveList{i});
				end
        		end
        	
        	
        	end
        	
        	
        	
        	function checkState(obj)
        		%checks if all the parameters are set 
        		import mapseis.util.*;
        		
        		%check if everthing is completed
        		EmptyList={'CalcProgress','DataMode','Datastore','Filterlist',...
        			'LearningFilter','ModelPlugIns','ModelConfig','TestPolygon',...
        			'Depths','Magnitudes','Spacings','ForecastGrid','ForecastLength',...
        			'PredictionIntervall','UpdateMag','StartTime','EndTime',...
        			'AvailableTimes','LearningPeriod','LearningUpdate','StoreProject',...
        			'LearningLength','LearningLag','ArchivePath','CalcPath'}
        		
        		Complete=true;
        		TotalEmpty=true;
        		for i=1:numel(EmptyList)
        			Complete=Complete&~isempty(EmptyList{i});
        			TotalEmpty=TotalEmpty&isempty(EmptyList{i});
        		end
        		
        		if TotalEmpty
        			obj.ProjectStatus='empty';
        			return;
        		end
        		
        		
        		if Complete
        			%check if the structures are also correct
        			%(MetaData is in a way option and will not be 
        			%enforced)
        			StillComplete=~isempty(obj.ProjectConfig.ArchiveForecasts)&...
						~isempty(obj.ProjectConfig.StoreLogs)&...
						~isempty(obj.ProjectConfig.ArchiveLogs)&...
						~isempty(obj.ProjectConfig.SaveConfig)&...
						~isempty(obj.ProjectConfig.CatalogStore);
				
				%maybe later a check for sensible values would also be an option
				Complete=Complete&StillComplete;
				
				
				
				%set the state right 
				if Complete
					switch obj.ProjectStatus
						case 'changed'
							obj.ProjectStatus='recalc';
							%It is suggested to update  
							%the progress variable with
							%correctProgress
						case {'config','empty'}
							obj.ProjectStatus='ready';	
						
					end	
				
				else
					switch obj.ProjectStatus
						case {'recalc','finished','cont'}
							obj.ProjectStatus='changed';
							%It is suggested to update  
							%the progress variable with
							%correctProgress
						case 'ready'
							obj.ProjectStatus='config';	
						
					end	
				end
						
        		end
        		
        	end
        	
        	
        	
        	function correctProgress(obj)
        		%corrects the progress variable if models are changed, added or remove
        		%and/or if the test region has changed. It determines what should be
        		%recalculated.
        		
        		%I have to be carefull with updating the models, because it will change the
        		%ConfigID, so only changes which "change" a value, no "bulk" modifications 
        		
        		%The whole method is quite complicated, as there are many different cases
        		%it has to be tested if everything always work in every possible case, and 
        		%may modified. But it is also possible to avoid any "false tags" by just never
        		%edit a calculated project.
        		
        		%Hopefully it works, I guess I have to build kind of a test to see if it works
        		%TODO
        		
        		%Carefull, this method is only meant for correcting progress structur, it is not
        		%possible to correct for "deformed" forecast storage due to uncomplete removal of 
        		%a model. At best always use function addModel, replaceModel and removeModel, for
        		%managing the model list. If used correctly the correctProgress method may never
        		%has to be called.
        		
        		import mapseis.util.*
        		
			if strcmp(obj.ProjectStatus,'changed')| strcmp(obj.ProjectStatus,'recalc');
				
				
				newParam=obj.backupParameters(true,true);
				if ~isempty(obj.LastParameters)
					NoChange=StructCompare(newParam,obj.LastParameters,true);
				else
					NoChange=false;
				end
				
				if NoChange
					
					OneMoreTime=numel(obj.AvailableTimes)-numel(obj.LastParameters.AvailableTimes);
				
					if OneMoreTime==0
						%It is assumed that the times are updated before the check
						NoTimeChange=all(obj.AvailableTimes==obj.LastParameters.AvailableTimes);
						HaveToWarn=NoTimeChange&(obj.StartTime~=obj.LastParameters.StartTime)&...
							all(obj.EndTime~=obj.LastParameters.EndTime);
						
						if HaveToWarn
							warning('Start and End time did change, but not the AvailableTimes')
						end
					else
						NoTimeChange=false;
					end
					
					if NoTimeChange
						TimeFixable=true;
						
					else
						%is fixable if learning period length did not change
						%and if the time intervals overlap
						PastLearn=obj.LastParameters.LearningPeriod(2)-obj.LastParameters.LearningPeriod(1);
						NowLearn=obj.LearningPeriod(2)-obj.LearningPeriod(1);
						
						minPast=min(obj.LastParameters.AvailableTimes(:));
						maxPast=max(obj.LastParameters.AvailableTimes(:));
						
						DoOverlap=any(minPast==obj.AvailableTimes)|any(maxPast==obj.AvailableTimes);
						
						TimeFixable=DoOverlap&(PastLearn==NowLearn);
					end
					
				else
					%Hopeless anyway everything has to be recalculated
					NoTimeChange=false;
					TimeFixable=false;
					
				end
				
				if isstr(obj.PredictionIntervall)
					%currently not fixable
					TimeFixable=false;
				end
				
				
				TheNames={};
				NrTimes=numel(obj.AvailableTimes);
				
				if TimeFixable
					%do some of the work outside the loop
					minPast=min(obj.LastParameters.AvailableTimes(:));
					maxPast=max(obj.LastParameters.AvailableTimes(:));
					minNow=min(obj.AvailableTimes(:));
					maxNow=max(obj.AvailableTimes(:));
					
					SuperMin=min([minPast,minNow]);
					SuperMax=max([maxPast,maxNow]);
					SuperTime=SuperMin:obj.PredictionIntervall:SuperMax;
					obj.CalcProgress.Times=SuperTime;
					
					RawProgress=zeros(size(SuperTime));
					RawCatStore=cell(size(SuperTime));
					
					%goal find pos of old in new
					if minPast<minNow
						%losing the past
						PosMin=-find(SuperTime==minNow);
					elseif minPast==minNow
						%extension
						PosMin=1;
					else
						%back in time
						PosMin=find(SuperTime==minPast);
					end
					
					if maxPast<maxNow
						%no future
						PosMax=-find(SuperTime==maxNow);
					elseif maxPast==maxNow
						%extension in past?
						PosMax=numel(SuperTime);
					else
						%back to the future
						PosMax=find(SuperTime==maxPast);
					end
					
					
					
					
					
					if isempty(PosMax)|isempty(PosMin)
						%oops, seems like it cannot be corrected
						TimeFixable=false;
					end
					
					
					
				end
				
				CatFixed=false;
				
				%check which model has to be recalculated			
				for i=1:numel(obj.ModelPlugIns(:,1))
					curModel=obj.ModelPlugIns{i,3};
					SameModel=strcmp(obj.ModelPlugIns{i,1},curModel.PluginName)&...
						strcmp(obj.ModelPlugIns{i,2},curModel.Version);
					
					SameConfig=strcmp(curModel.ConfigID,obj.ModelConfig{i,2});	
					%carefull both Same* statement assume that the model changed and
					%the additional fields where not updated
					
					%important new model (means in a new slot) may have an 
					%empty Name in the model struct, which lead to a false in SameModel
					%-> which is fine, but could lead to an error when the old name is
					%generated
					
					ModNamNew=[curModel.PluginName,'_v',curModel.Version];
					noReduce=true;
					
					
					if ~(SameModel&SameConfig)&~isempty(obj.ModelPlugIns{i,1})
						%there is an old name registered -> maybe not new
						ModNamOld=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
						noReduce=~isfield(obj.CalcProgress.ModelProgress,ModNamOld);
						
						
					elseif  ~(SameModel&SameConfig)&isempty(obj.ModelPlugIns{i,1})
						noReduce=true;
					
					
					
					end
					
					
					if ~(SameModel&SameConfig)
						
						if ~noReduce
							if ~isempty(obj.Forecasts{i})
								if obj.ProjectConfig.ArchiveDelete
									try
										CalcToArch=obj.CalcProgress.ModelProgress.(ModNamOld);
										CalcToArch(CalcToArch==0)=-1;
										CalcToArch(CalcToArch==1)=2;
									catch
										CalcToArch=[];
									end
									
									
									if ~isempty(obj.UsedConfigs)
										UseConf=obj.UsedConfigs{i};
									else
										UseConf=[];
									end
									
									if ~isempty(obj.OutputLogs)
										TheLogs=obj.OutputLogs{i};
									else
										TheLogs=[];
									end
									
									RIP={ModNamOld,CalcToArch,obj.Forecasts{i},UseConf,TheLogs};
									
									%Rest in peace
									obj.MArchive{end+1}=RIP;
									
									
								end
								
																%
								%selVec=true(size(obj.Forecasts));
								%selVec(i)=false;
								
								%add empty elements, it is replaced not deleted
								widthCell=numel(obj.Forecasts(i,:));
								obj.Forecasts(i,:)=cell(1,widthCell);
								
								
									
								if ~isempty(obj.UsedConfigs)
									widthCell=numel(obj.UsedConfigs(i,:));
									obj.UsedConfigs(i,:)=cell(1,widthCell);
								end
									
								if ~isempty(obj.OutputLogs)
									widthCell=numel(obj.OutputLogs(i,:));
									obj.OutputLogs(i,:)=cell(1,widthCell);
								end
								
							
							end
							
							%now remove traces in the progress struct
							try
								obj.CalcProgress.ModelProgress=rmfield(obj.CalcProgress.ModelProgress,ModNamOld);
							end
								
						
						end
					
						%add new entry
						obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
						
						
						
						%if it is at the end of the cell arrays (truely a new model) cells have to be added;
						if i>numel(obj.Forecasts(:,1))
							widthCell=numel(obj.Forecasts(i,:));
							obj.Forecasts(i,:)=cell(1,widthCell);
								
																
							if ~isempty(obj.UsedConfigs)
								widthCell=numel(obj.UsedConfigs(i,:));
								obj.UsedConfigs(i,:)=cell(1,widthCell);
							end
									
							if ~isempty(obj.OutputLogs)
								widthCell=numel(obj.OutputLogs(i,:));
								obj.OutputLogs(i,:)=cell(1,widthCell);
							end
						end
						
						%A new model needs correct name and version string
						if isempty(obj.ModelPlugIns{i,1});
							obj.ModelPlugIns{i,1}=obj.ModelPlugIns{i,3}.PluginName;
							obj.ModelPlugIns{i,2}=obj.ModelPlugIns{i,3}.Version;
						end
						
						%never configurated model
						if isempty(obj.ModelConfig{i,2});
							obj.ModelConfig{i,2}=obj.ModelPlugIns{i,3}.ConfigID;
							obj.ModelConfig{i,1}=obj.ModelPlugIns{i,3}.getModelConfig;
						end
						
						
					else
						
					
						
						if ~NoChange
							%everything has to be done new, but no cells have to be deleted
							obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
							if ~isempty(obj.InputCatalogs)&~CatFixed
								obj.InputCatalogs=cell(size(obj.AvailableTimes));
							end
						
						else
							
							if ~TimeFixable
								obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
								if ~isempty(obj.InputCatalogs)&~CatFixed
									obj.InputCatalogs=cell(size(obj.AvailableTimes));
								end
								
							else
								curProgress=RawProgress;
								if ~NoTimeChange
									%here it gets messy
									if PosMin<0
										Kickout=obj.CalcProgress.ModelProgress.(ModNamNew)(1:-PosMin);
										Kickout(Kickout==0)=-1;	
										Kickout(Kickout==1)=2;
										curProgress(1:-PosMin)=Kickout;
										PosOldMin=-PosMin;
										
										%only in the first step, transfer old catalogs if existing
										if ~isempty(obj.InputCatalogs)&~CatFixed
											TransCat=obj.InputCatalogs(1:-PosMin);
											RawCatStore(1:-PosMin)=TransCat;
										end
										
									else
										PosOldMin=1;
									end
								
									if PosMax<0
										Kickout=obj.CalcProgress.ModelProgress.(ModNamNew)(-PosMax:end);
										Kickout(Kickout==0)=-1;	
										Kickout(Kickout==1)=2;
										curProgress(-PosMax:end)=Kickout;
										PosOldMax=-PosMax;
										
										if ~isempty(obj.InputCatalogs)&~CatFixed
											TransCat=obj.InputCatalogs(-PosMax:end);
											RawCatStore(-PosMax:end)=TransCat;
										end
										
									else	
										PosOldMax=numel(obj.CalcProgress.ModelProgress.(ModNamNew));
									end
									
								end
								
								%now middle part
								MidPart=obj.CalcProgress.ModelProgress.(ModNamNew)(PosOldMin:PosOldMax);
								curProgress(abs(PosMin):abs(PosMax))=MidPart;
								
								if ~isempty(obj.InputCatalogs)&~CatFixed
									TransCat=obj.InputCatalogs(PosOldMin:PosOldMax);
									RawCatStore(abs(PosMin):abs(PosMax))=TransCat;
									obj.InputCatalogs=RawCatStore;
								end
								
								%store
								obj.CalcProgress.ModelProgress.(ModNamNew)=curProgress;
								
								
							end
						
						end
						
					
					
					
					end %if end
					
				
					
				end %for end
        	
			elseif strcmp(obj.ProjectStatus,'ready')|strcmp(obj.ProjectStatus,'config')
				%a new project needs to add some empty progress bars
				for i=1:numel(obj.ModelPlugIns(:,1))
					curModel=obj.ModelPlugIns{i,3};
					
					
					ModNamNew=[curModel.PluginName,'_v',curModel.Version];
					
					if i>numel(obj.Forecasts(:,1))
							
						widthCell=numel(obj.Forecasts(i,:));
						obj.Forecasts(i,:)=cell(1,widthCell);
								
																
						if ~isempty(obj.UsedConfigs)
							widthCell=numel(obj.UsedConfigs(i,:));
							obj.UsedConfigs(i,:)=cell(1,widthCell);
						end
									
						if ~isempty(obj.OutputLogs)
							widthCell=numel(obj.OutputLogs(i,:));
							obj.OutputLogs(i,:)=cell(1,widthCell);
						end
					end
					
					if isempty(obj.ModelPlugIns{i,1});
						obj.ModelPlugIns{i,1}=obj.ModelPlugIns{i,3}.PluginName;
						obj.ModelPlugIns{i,2}=obj.ModelPlugIns{i,3}.Version;
					end
						
					%never configurated model
					if isempty(obj.ModelConfig{i,2});
						obj.ModelConfig{i,2}=obj.ModelPlugIns{i,3}.ConfigID;
						obj.ModelConfig{i,1}=obj.ModelPlugIns{i,3}.getModelConfig;
					end
					
					obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
				end
				
				
				%Times are probably as well missing
				if isempty(obj.CalcProgress.Times)
        				obj.CalcProgress.Times=obj.AvailableTimes;
        			end	
        			
        			
			end %if end
        	
		end %function end
        	
        	
        	function autoSetupTime(obj)
        		%can be called when PredictionIntervall, UpdateMag, StartTime, EndTime are set
        		%and it will add or update AvailableTimes. If the mode is 'open', a Datastore 
        		%catalog has to available or CurrentEndTime has to be set manually before.
        		
        		%check if everthing there what is needed
        		Ready=~isempty(obj.PredictionIntervall)&~isempty(obj.StartTime)&...
        			~isempty(obj.EndTime);
        		
        		if ~Ready
        			error('Not enough data to produce time steps')
        		end	
        		
        		%toggles
        		OpenMind=false;
        		ForReal=false;
        		
        		if strcmp(obj.EndTime,'open')
        			Ready=~isempty(obj.Datastore)|~isempty(obj.CurrentEndTime);
        			OpenMind=true;
        		end
        		
        		if strcmp(obj.PredictionIntervall,'realtime')
        			Ready=Ready&~isempty(obj.Datastore)&~isempty(obj.UpdateMag);
        			ForReal=true;
        		end
        			
        		
        		%check once again
        		if ~Ready
        			error('Not enough data to produce time steps')
        		end
        		
        		
        		%now cue the music...
        		
        		%take learning filter as first priority, but it won't matter
        		%in most cases, as time is always ignored.
        		if isempty(obj.LearningFilter)
        			usedFilter=obj.Filterlist;
        		else
        			usedFilter=obj.LearningFilter;
        		end
        		
        		if (OpenMind|ForReal)&~isempty(obj.Datastore)
        			%get logical vector
        			usedFilter.changeData(obj.Datastore);
        			usedFilter.updateNoEvent;
        			selVec=usedFilter.excludeFilter('Time');
        		
        		end
        		
        		
        		if OpenMind
        			%get the current maximum end time
        			%(obj.CurrentEndTime has the higher priority than the
        			%maximum time of datastore)
        			if isempty(obj.CurrentEndTime)
        				eventStruct=obj.Datastore.getFields(selVec);
        				obj.CurrentEndTime=max(eventStruct.dateNum);
        			end
        			
        			EndTimeToUse=obj.CurrentEndTime;
        		
        		else
        			EndTimeToUse=obj.EndTime;
        			       			
        		end
        		
        		if ForReal
        			%realtime mode 
        			eventStruct=obj.Datastore.getFields([],selVec);
        			inTime=eventStruct.dateNum>=obj.StartTime&eventStruct.dateNum<EndTimeToUse;
        			inMag=eventStruct.mag>=obj.UpdateMag;
        			RawTimes=eventStruct.dateNum(inTime&inMag);
        			RawTimes=[obj.StartTime;RawTimes;obj.EndTime];
        			TimeList=[];
        			
        			for i=1:numel(RawTimes)-1; 
        				TimeList=[TimeList,RawTimes(i):1:RawTimes(i+1)];
        			end
        			
        			obj.AvailableTimes=TimeList;
        		else
        			obj.AvailableTimes=obj.StartTime:obj.PredictionIntervall:EndTimeToUse
        		
        		end
        		
        		%generate Catalog slots
        		obj.InputCatalogs=cell(size(obj.AvailableTimes));
        		
        		
        	end
        	
        	
        	
        	function autoSetupRegion(obj,MagRange,DepthRange,GridPrecision)
        		%can be called when Filterlist, Spacings are set. It will produce everything needed 
        		%for the testregion grid, mags etc. In case there is no Magnitude and Depths set in the 
        		%Filterlist, Datastore is needed.
        		
        		import mapseis.forecast.*;
        		import mapseis.region.*;
        		import mapseis.projector.*;
        		
        		if nargin<4
        			GridPrecision=false;
        		elseif nargin<3
        			GridPrecision=false;
        			DepthRange=[];
        		elseif nargin<2
        			GridPrecision=false;
        			DepthRange=[];
        			MagRange=[];
        		end
        			
        		%check if everthing needed is set
        		Ready=~isempty(obj.Filterlist)&~isempty(obj.Spacings)&...
        			(~isempty(obj.Datastore)|(~isempty(MagRange)&~isempty(DepthRange)))|...
        			(~isempty(obj.Depths)&~isempty(obj.Magnitudes));
        		        		      			
        		if ~Ready
        			error('Not enough data to produce regional grid')
        		end		
        		
        		NewGrid = ForecastObj('RawGrid');
        		
        		%check the filterlist
        		regionFilter = obj.Filterlist.getByName('Region');
        		filterRegion = getRegion(regionFilter);
        		pRegion = filterRegion{2};
        		RegRange = filterRegion{1};
		
				
        		if strcmp(RegRange,'all');
        			if isempty(obj.Datastore)
        				error('Not enough data to produce regional grid')
        			end
        			obj.Filterlist.changeData(obj.Datastore);
        			obj.Filterlist.updateNoEvent;
        			selected=obj.Filterlist.getSelected;
        			obj.Filterlist.PackIt
				
        			[locations temp]=getLocations(obj.Datastore,selected);
				lon=locations(:,1);
				lat=locations(:,2);
				FiltInput=[min(lon)-0.001, max(lon)+0.001];
				DataInput=[min(lat)-0.001, max(lat)+0.001];
				RegPoly=[FiltInput(1),DataInput(1);...
					 FiltInput(2),DataInput(1);...
					 FiltInput(2),DataInput(2);...
					 FiltInput(1),DataInput(2);...
					 FiltInput(1),DataInput(1)];
				TheBox=[FiltInput(1) FiltInput(2) DataInput(1) DataInput(2)];
			
			else
				FiltInput=obj.Filterlist;
				DataInput=obj.Datastore;
				RegPoly=pRegion.getBoundary;
				TheBox=pRegion.getBoundingBox;
        		end
        		
        		obj.TestPolygon=RegPoly;
        		
        		%round bounding box according to precision
        		if GridPrecision|GridPrecision~=0
        			TheBox=round(TheBox*(1/GridPrecision))*GridPrecision;
        			TheBox=round(TheBox*(1/GridPrecision))*GridPrecision;
        		end
        		
        		obj.BoundBox=TheBox;
        		
        		
        		%now which datasource shall it be? 
        		%highest priority: direct input
        		if ~(isempty(MagRange)&isempty(DepthRange))
        			if ~isnan(obj.Spacings(3))
        				MagInput=[MagRange(1) MagRange(2) obj.Spacings(3)];
        			else	
        				MagInput=[MagRange(1) MagRange(2)];
        			end	
        			
        			if ~isnan(obj.Spacings(4))
        				DepthInput=[DepthRange(1) DepthRange(2) obj.Spacings(4)];
        			else	
        				DepthInput=[DepthRange(1) DepthRange(2)];
        			end	
        			
        			
        		elseif ~(isempty(obj.Depths)&isempty(obj.Magnitudes))
        			%second priority: existing MagRange and DepthRange
        			%At the moment only regular Mags and depths are supported
        			MinMag=min(obj.Magnitudes);
        			MaxMag=max(obj.Magnitudes);
        			MinDepth=min(obj.Depths);
        			MaxDepth=max(obj.Depths);
        			
        			if ~isnan(obj.Spacings(3))
        				MagInput=[MinMag MaxMag obj.Spacings(3)];
        			else	
        				MagInput=[MinMag MaxMag];
        			end	
        			
        			if ~isnan(obj.Spacings(4))
        				DepthInput=[MinDepth MaxDepth obj.Spacings(4)];
        			else	
        				DepthInput=[MinDepth MaxDepth];
        			end	
        			
        			
        		else
        			%last: use the datastore
        			obj.Filterlist.changeData(obj.Datastore);
        			obj.Filterlist.updateNoEvent;
        			selected=obj.Filterlist.getSelected;
        			obj.Filterlist.PackIt
        			eventStruct=obj.Datastore.getFields({},selected);
        			
        			MinMag=min(eventStruct.mag);
        			MaxMag=max(eventStruct.mag);
        			MinDepth=min(eventStruct.depth);
        			MaxDepth=max(eventStruct.depth);
        			
        			if ~isnan(obj.Spacings(3))
        				MagInput=[MinMag MaxMag obj.Spacings(3)];
        			else	
        				MagInput=[MinMag MaxMag];
        			end	
        			
        			if ~isnan(obj.Spacings(4))
        				DepthInput=[MinDepth MaxDepth obj.Spacings(4)];
        			else	
        				DepthInput=[MinDepth MaxDepth];
        			end	
        			
        			
        			
        		end
        		
        		%Generate the grid in the object
        		NewGrid.createGrid(FiltInput,DataInput,obj.Spacings(1:2),MagInput,DepthInput,[],GridPrecision);
        		%I go now for the rounded version, but may change or make it switchable later if needed.
        		
        		%save grid and get the data from the grid back
        		obj.ForecastGrid=NewGrid;
        		obj.Depths=obj.ForecastGrid.getDepths;
        		obj.Magnitudes=obj.ForecastGrid.getMagnitudes;
        		
        		%check if one of the is empty
        		if isempty(obj.Depths)&~isempty(DepthRange)&isnan(obj.Spacings(4))
        			obj.Depths=DepthRange;
        		end
        		
        		if isempty(obj.Magnitudes)&~isempty(MagRange)&isnan(obj.Spacings(3))
        			obj.Magnitudes=MagRange;
        		end
        		
        		if isempty(obj.LearningFilter)
        			warning('LearningFilter has been set automatically')
        			obj.LearningFilter=obj.Filterlist
        		end
        		
        		
        		
        		
        	end
        	
        	
        	function RegionConfig = buildRegionConfig(obj)
        		%builds a structur which can be used to configure a model plugin
        		%(of course only works if the test region is defined)
        		
        		%NEEDED fields
        		%RegionPoly:		Polygon surrounding the region
			%RegionBoundingBox:	Box around the Polygon
			%LonSpacing:		Longitude spacing or spacial spacing
			%			in case only on spacing is allowed
			%LatSpacing:		Latitude spacing, if two spacings 
			%			are allowed
			%GridPrecision:		The value to which the grid was rounded to.
			%MinMagnitude:		Minimum magnitude which should be 
			%			forecasted
			%MaxMagnitude:		The maximal magnitude which should be
			%			forecasted
			%MagnitudeBin:		Size of the Magnitude Bin often this 
			%			will be 0.1, but can be different. This
			%			will be empty if no magnitude binning is
			%			used
			%MinDepth:		Minimal depth range which should be 
			%			forecasted, in most cases this will be 0
			%MaxDepth:		Maximal depth to forecast
			%DepthBin:		In most cases this will empty as 3D 
			%			regions are not common, but in case a
			%			3D model is used this gives depth bin 
			%			size
			%TimeInterval:		Length of the forcast in days or year 
			%			(can be specified in getFeatures)
			%RelmGrid:		A raw grid similar to the one used in 
			%			Relm Forecasts based on the values 
			%			specified. 
			%			(Relmformat: [LonL LonH LatL LatH ...
			%			DepthL DepthH MagL MagH (ID) Mask]
			%Nodes			%Just the spacial nodes of the grid as 
			%			for instance by TripleS, in contrary to
			%			the RELM grid this descriptes the centre
			%			of the grid cell
			%MagVector:		Consisting of the Magnitudes which have
			%			to be forecasted (the points are the in 
			%			middle of the magnitude bin)
			%DepthVector:		Consisting of the Depths which have to 
			%			forecasted (only 3D) (the points are the  
			%			in middle of the depth bin)
        		
			if isempty(obj.ForecastGrid)
				RegionConfig=[];
				error('No grid defined');			
			end
			
			
			%Magnitudes
			MagBin=obj.Spacings(3)
			if isnan(MagBin)
				MagShift=0;
			else
				MagShift=MagBin/2;
			end
			
			MinMag=obj.Magnitudes(1)-MagShift
			MaxMag=obj.Magnitudes(end)+MagShift
			
			
			%Depths
			DepthBin=obj.Spacings(4)
			if isnan(DepthBin)
				DepthShift=0;
			else
				DepthShift=DepthBin/2;
			end
			
			MinDepth=obj.Depths(1)-DepthShift
			MaxDepth=obj.Depths(end)+DepthShift
			
			
			
			RegionConfig = struct(	'RegionPoly',obj.TestPolygon,...
						'RegionBoundingBox',obj.BoundBox,...
						'LonSpacing',obj.Spacings(1),...
						'LatSpacing',obj.Spacings(2),...
						'GridPrecision',obj.ForecastGrid.GridPrecision,...
						'MinMagnitude',MinMag,...
						'MaxMagnitude',MaxMag,...
						'MagnitudeBin',MagBin,...
						'MinDepth',MinDepth,...
						'MaxDepth',MaxDepth,...
						'DepthBin',DepthBin,...
						'TimeInterval',obj.ForecastLength,...
						'RelmGrid',obj.ForecastGrid.getFullGrid,...
						'Nodes',obj.ForecastGrid.getGridNodes,...
						'MagVector',obj.Magnitudes,...
						'DepthVector',obj.Depths);
						
						
						
			
        	end
        	
        	
        	function addModel(obj,ForecastModel)
        		%adds a new model to the stack
        		if ~isempty(obj.ModelPlugIns)
        			nextEntry=numel(obj.ModelPlugIns(:,1))+1;
        		else
        			nextEntry=1;
        			%No matter if there was something in the other
        			%cell arrays, it should be deleted if the Pluginlist
        			%was empty
        			obj.ModelPlugIns=cell(1,3);
        			obj.ModelConfig=cell(1,2);
        			obj.Forecasts=cell(1,2);
        			obj.UsedConfigs=cell(1,2);
        			obj.OutputLogs=cell(1,2);
        			
        			
        		end
        		
        		disp(nextEntry)
        		if iscell(ForecastModel)
        			for i=1:numel(ForecastModel)
        				obj.ModelPlugIns{nextEntry,3}=ForecastModel{i};
        				obj.ModelConfig(nextEntry,:)=cell(1,2);
        				obj.Forecasts(nextEntry,:)=cell(1,2);
        				obj.UsedConfigs(nextEntry,:)=cell(1,2);
        				obj.OutputLogs(nextEntry,:)=cell(1,2);
        				
        				nextEntry=nextEntry+1;
        			end
        		else
        			obj.ModelPlugIns{nextEntry,3}=ForecastModel;
        			obj.ModelConfig(nextEntry,:)=cell(1,2);
        			obj.Forecasts(nextEntry,:)=cell(1,2);
        			obj.UsedConfigs(nextEntry,:)=cell(1,2);
        			obj.OutputLogs(nextEntry,:)=cell(1,2);
        		
        		end
        		
        		%correct the Progress list -> this will also add some of 
        		%the missing parts
        		obj.correctProgress;
        		
        		
        	end
        	
        	
        	function [ForecastModel,Pos] = getModel(obj,ModelID)
        		%I think the name says it all	
        		%ModelID can either be the full name as registered in
        		%the structure or just the index in the list
        		if ~isempty(obj.ModelPlugIns)
        			%generate name list
        			for i=1:numel(obj.ModelPlugIns(:,1))
        				ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        			end
        		
        		
        			if iscell(ModelID)
        				ForecastModel={};
        				Pos={};
        				for i=numel(ModelID)
        					if isstr(ModelID{i})
        						IDX=find(strcmp(ModelID{i},ModelNames));
        					else
        						%has to be numeric
        						IDX=ModelID{i};
        					end
        					
        					ForecastModel{i}=obj.ModelPlugIns{IDX(1),3};
        					Pos{i}=IDX(1);
        				end
        			else
        				ForecastModel=[];
        				Pos=[];
        				if isstr(ModelID)
        					IDX=find(strcmp(ModelID,ModelNames));
        				else
        					%has to be numeric
        					IDX=ModelID;
        				end
        					
        				ForecastModel=obj.ModelPlugIns{IDX(1),3};
        				Pos=IDX(1);
        				        			
        				
        			end
        		
        		else
        			error('No model stored')
        		
        		end
        		
        		
        		
        		
        	end
        	
        	
        	
        	function [ForecastModels,Names,Pos] = getAllModels(obj)
        		%same as getModel but returns all models at once
        		
        		%it could be done more directely, but this may be better if there
        		%are some changes in the model storage, and should not matter to much
        		%anyway.
        		
        		%generate name list
        		for i=1:numel(obj.ModelPlugIns(:,1))
        			Names{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        		end
        		
        		%get models with getModel
        		[ForecastModels,Pos] = obj.getModel(Names);
        		
        		
        	end
        	
        	
        	
        	
        	
        	
        	function replaceModel(obj,ModelID,ForecastModel)
        		%replaces a model with a new one
        		if ~isempty(obj.ModelPlugIns)
        			%generate name list
        			for i=1:numel(obj.ModelPlugIns(:,1))
        				ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        			end
        		
        		
        			if iscell(ModelID)
        				
        				for i=numel(ModelID)
        					if isstr(ModelID{i})
        						IDX=find(strcmp(ModelID{i},ModelNames));
        					else
        						%has to be numeric
        						IDX=ModelID{i};
        					end
        					
        					if ~isempty(IDX)
        						obj.ModelPlugIns{IDX(1),3}=ForecastModel{i};
        					
        					else
        						warning('No model in storage, added instead of replaced')
        						nextEntry=numel(obj.ModelPlugIns(:,1))+1;
        						obj.ModelPlugIns{nextEntry,3}=ForecastModel{i};
        						obj.ModelConfig(nextEntry,:)=cell(1,2);
        						obj.Forecasts(nextEntry,:)=cell(1,2);
        						obj.UsedConfigs(nextEntry,:)=cell(1,2);
        						obj.OutputLogs(nextEntry,:)=cell(1,2);
        					end
        					
        					
        				end
        				
        			else
        				
        				if isstr(ModelID)
        					IDX=find(strcmp(ModelID,ModelNames));
        				else
        					%has to be numeric
        					IDX=ModelID;
        				end
        					
        				if ~isempty(IDX)
        					obj.ModelPlugIns{IDX(1),3}=ForecastModel;
        					
        				else
        					warning('No model in storage, added instead of replaced')
        					nextEntry=numel(obj.ModelPlugIns(:,1))+1;
        					obj.ModelPlugIns{nextEntry,3}=ForecastModel;
        					obj.ModelConfig(nextEntry,:)=cell(1,2);
        					obj.Forecasts(nextEntry,:)=cell(1,2);
        					obj.UsedConfigs(nextEntry,:)=cell(1,2);
        					obj.OutputLogs(nextEntry,:)=cell(1,2);
        				end
        					
        				
        				        			
        				
        			end
        			
        			%correct the Progress list -> this will also add some of 
        			%the missing parts
        			obj.correctProgress;
        			
        		
        		else
        			warning('No model in storage, added instead of replaced')
        			obj.addModel(ForecastModel);
        		
        		end	
        		
        		
        	end
        	
        	
        	function removeModel(obj,ModelID)
        		%it removes a model and everything surrounding it
        		if ~isempty(obj.ModelPlugIns)
        			%generate name list
        			for i=1:numel(obj.ModelPlugIns(:,1))
        				ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        			end
        		
        			RawSelRay=false(size(obj.ModelPlugIns(:,1)));
        			
        			if iscell(ModelID)
        				for i=numel(ModelID)
        					if isstr(ModelID{i})
        						IDX=find(strcmp(ModelID{i},ModelNames));
        					else
        						%has to be numeric
        						IDX=ModelID{i};
        					end
        					
        					if ~isempty(IDX)
        						Pos=RawSelRay;
        						Pos(IDX(1))=true;
        						ModNamOld=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        						
        						if obj.ProjectConfig.ArchiveDelete
								%Archive the old forecasts if existing
								try
									CalcToArch=obj.CalcProgress.ModelProgress.(ModNamOld);
									CalcToArch(CalcToArch==0)=-1;
									CalcToArch(CalcToArch==1)=2;
								catch
									CalcToArch=[];
								end
								
								
								if ~isempty(obj.UsedConfigs)
									UseConf=obj.UsedConfigs{i};
								else
									UseConf=[];
								end
									
								if ~isempty(obj.OutputLogs)
									TheLogs=obj.OutputLogs{i};
								else
									TheLogs=[];
								end
									
								RIP={ModNamOld,CalcToArch,obj.Forecasts{i},UseConf,TheLogs};
									
								%Rest in peace
								obj.MArchive{end+1}=RIP;
									
									
							end
							
							%Now Remove
							obj.ModelPlugIns=obj.ModelPlugIns(~Pos,:);
							obj.ModelConfig=obj.ModelConfig(~Pos,:);
							obj.Forecasts=obj.Forecasts(~Pos,:);
							obj.UsedConfigs=obj.UsedConfigs(~Pos,:);
							obj.OutputLogs=obj.OutputLogs(~Pos,:);
							
        						try
								obj.CalcProgress.ModelProgress=rmfield(obj.CalcProgress.ModelProgress,ModNamOld);
							end
        						
        					else
        						warning('Model not found')
        						
        					end
        					
        				
        				end
        				
        			else
        				
        				if isstr(ModelID)
        					IDX=find(strcmp(ModelID,ModelNames));
        				else
        					%has to be numeric
        					IDX=ModelID;
        				end
        					
        				if ~isempty(IDX)
        					Pos=RawSelRay;
        					Pos(IDX(1))=true;
        					ModNamOld=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        						
        					if obj.ProjectConfig.ArchiveDelete
							%Archive the old forecasts if existing
							try
								CalcToArch=obj.CalcProgress.ModelProgress.(ModNamOld);
								CalcToArch(CalcToArch==0)=-1;
								CalcToArch(CalcToArch==1)=2;
							catch
								CalcToArch=[];
							end
								
								
							if ~isempty(obj.UsedConfigs)
								UseConf=obj.UsedConfigs{i};
							else
								UseConf=[];
							end
									
							if ~isempty(obj.OutputLogs)
								TheLogs=obj.OutputLogs{i};
							else
								TheLogs=[];
							end
									
							RIP={ModNamOld,CalcToArch,obj.Forecasts{i},UseConf,TheLogs};
									
							%Rest in peace
							obj.MArchive{end+1}=RIP;
									
						end			
													
						%Now Remove
						obj.ModelPlugIns=obj.ModelPlugIns(~Pos,:);
						obj.ModelConfig=obj.ModelConfig(~Pos,:);
						obj.Forecasts=obj.Forecasts(~Pos,:);
						obj.UsedConfigs=obj.UsedConfigs(~Pos,:);
						obj.OutputLogs=obj.OutputLogs(~Pos,:);
							
        					try
							obj.CalcProgress.ModelProgress=rmfield(obj.CalcProgress.ModelProgress,ModNamOld);
						end
        						
        				else
        					warning('Model not found')
        						
        				end
        					
        				
        				        			
        				
        			end
        			
        			%correct the Progress list -> this will also add some of 
        			%the missing parts
        			%->Not needed, should have been taken care of
        			%obj.correctProgress;
        			
        		
        		else
        			error('No model in storage, nothing deleted')
        			
        		
        		end	
        		
        		
        		
        		
        	end
        	
        	
        	
        	
        	
        	
        	
        	function [Forecast,Pos] = getForecast(obj,ModelID)
        		%I think the name says it all	
        		%ModelID can either be the full name as registered in
        		%the structure or just the index in the list
        		if ~isempty(obj.ModelPlugIns)
        			%generate name list
        			for i=1:numel(obj.ModelPlugIns(:,1))
        				ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        			end
        		
        		
        			if iscell(ModelID)
        				
        				Forecast={};
        				Pos={};
        				for i=1:numel(ModelID)
        					if isstr(ModelID{i})
        						IDX=find(strcmp(ModelID{i},ModelNames));
        					else
        						%has to be numeric
        						IDX=ModelID{i};
        					end
        					
        					Forecast{i}=obj.Forecasts{IDX(1),1};
        					Pos{i}=IDX(1);
        				end
        			else
        				Forecast=[];
        				Pos=[];
        				if isstr(ModelID)
        					IDX=find(strcmp(ModelID,ModelNames));
        				else
        					%has to be numeric
        					IDX=ModelID;
        				end
        					
        				Forecast=obj.Forecasts{IDX(1),1};
        				Pos=IDX(1);
        				        			
        				
        			end
        		
        		else
        			error('No forecast/model stored')
        		
        		end
        		
        		
        		
        		
        	end
        	
        	
        	
        	function [Forecasts,Names,Pos] = getAllForecasts(obj)
        		%same as getForecast but returns all forecasts at once
        		
        		%it could be done more directely, but this may be better if there
        		%are some changes in the model storage, and should not matter to much
        		%anyway.
        		
        		%generate name list
        		for i=1:numel(obj.ModelPlugIns(:,1))
        			Names{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        		end
        		
        		%get models with getForecasts
        		[Forecasts,Pos] = obj.getForecast(Names);
        		
        		
        	end
        	
        	
        	
        	
        	
        	function cleanForecasts(obj,FullClean)
        		%removes every forecast not up-to-date or removed from the
        		%list from the forecast objects and all related list and objects
        		%it also adds empty forecast objects to new models in case it has
        		%not been done before
        		
        		%If FullClean true, also forecast which are not anymore in the timerange
        		%will be removed (set to 2 in the ProgressStructur) 
        		import mapseis.forecast.*;
        		
        		if nargin<2
        			FullClean=false;
        			
        		end
        		
        		if ~isempty(obj.ModelPlugIns)
        			%generate name list
        			for i=1:numel(obj.ModelPlugIns(:,1))
        				CurModel=obj.ModelPlugIns{i,3}
        				if isempty(obj.ModelPlugIns{i,1})
        					warning('Model not correctly added, please run "correctProgress"')
        					continue;
        				end
        				
        				ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        				
        				if isempty(obj.Forecasts{i,1})
        					%no forecast -> create one 
        					NewForecast=obj.ForecastGrid.cloneWars;
        					NewForecast.setName(ModelNames{i});
        					
        					ModelFeature=CurModel.getFeatures;
        					
        					if ~strcmp(ModelFeature.ForecastType,'Selectable')
        						NewForecast.initForecast(ModelFeature.ForecastType,[]);
        						
        					else      					
        						disp('variable ForecastType, has to be set later');
        					end
        					
        					ForecastID=NewForecast.getID;
        					obj.Forecasts{i,1}=NewForecast;
        					
        					%Log object
        					NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
        					NewLogObj.InfoType='logs';
        					obj.OutputLogs{i,1}=NewLogObj;
        					
        					%UsedConfig Object
        					NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
        					NewLogObj.InfoType='config';
        					obj.UsedConfigs{i,1}=NewLogObj;
        					
        					if strcmp(obj.PredictionIntervall,'realtime')	
							obj.Forecasts{i,1}.setRealTimeMode(true);
							obj.OutputLogs{i,1}.setRealTimeMode(true);
							obj.UsedConfigs{i,1}.setRealTimeMode(true);
						end
								
        					
        				else
        					if ~isfield(obj.CalcProgress.ModelProgress,ModelNames{i});
        						warning('Model not correctly added, please run "correctProgress"')
        						continue;
        					end
        				
        					ProgressVec=obj.CalcProgress.ModelProgress.(ModelNames{i});
        					
        					if obj.Forecasts{i,1}.EmptyGrid;
        						%it is empty -> check if Logs and Configs are
        						%existing
        						ForecastID=obj.Forecasts{i,1}.getID;
        						if isempty(obj.OutputLogs{i,1})
        							NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
        							NewLogObj.InfoType='logs';
        							obj.OutputLogs{i,1}=NewLogObj;
        						end
        						
        						if isempty(obj.UsedConfigs{i,1})
        							NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
        							NewLogObj.InfoType='config';
        							obj.UsedConfigs{i,1}=NewLogObj;
        						end
        						
        						
        						if strcmp(obj.PredictionIntervall,'realtime')	
								obj.OutputLogs{i,1}.setRealTimeMode(true);
								obj.UsedConfigs{i,1}.setRealTimeMode(true);
							end
        						disp('Empty Forecast nothing to add/delete')
        						continue;
        						
        						
        						
        						
        					else
        						if all(ProgressVec==0)
        							%nothing should be calculated or all can be deleted
        							%->reset forecasts
        							NewForecast=obj.ForecastGrid.cloneWars;
        							NewForecast.setName(ModelNames{i});
        							
								NewForecast.initForecast(obj.Forecasts{i,1}.ForecastType.Type,[]);
								ForecastID=NewForecast.getID;
								obj.Forecasts{i,1}=NewForecast;
								
								%NOTE: the addon objects are build wether they are used or not
								%like this a on/off switching will be easier. But in case
								%it causes performance problems I will check before creating
								
								%Log object
								NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
								NewLogObj.InfoType='logs';
								obj.OutputLogs{i,1}=NewLogObj;
								
								%UsedConfig Object
								NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
								NewLogObj.InfoType='config';
								obj.UsedConfigs{i,1}=NewLogObj;
								
								%The rest as well, there might be something in it
								obj.Forecasts{i,2}=[];
        							obj.OutputLogs{i,2}=[];
        							%obj.UsedConfigs{i,2}=[];
								
        							if strcmp(obj.PredictionIntervall,'realtime')	
									obj.Forecasts{i,1}.setRealTimeMode(true);
									obj.OutputLogs{i,1}.setRealTimeMode(true);
									obj.UsedConfigs{i,1}.setRealTimeMode(true);
								end
        							
        							
        						elseif all(ProgressVec==1)
        							disp('Nothing to do')
        							
        							
        						else
        							if isempty(obj.CalcProgress.Times)
        								MyTime=obj.AvailableTimes;
        							else
        								MyTime=obj.CalcProgress.Times;
        							end
        							
        							RawLogicSelect=false(size(ProgressVec));
        							
        							for j=1:numel(ProgressVec)
        								if ProgressVec(j)==0|(ProgressVec(j)==2&FullClean)
        									%Forecast
        									try 
        										obj.Forecasts{i,1}.removeForecast(MyTime(j));
        									end
        									
        									%Logs
        									try 
        										obj.OutputLogs{i,1}.removeLog(MyTime(j));
        									end
        									
        									%Configs
        									try 
        										obj.UsedConfigs{i,1}.removeLog(MyTime(j));
        									end
        									
        									
        									SelVec=RawLogicSelect;
        									SelVec(j)=true;
        										
        									
        									%remove arrays with filepaths in case there are existing
        									if ~isempty(obj.Forecasts{i,2})
        										obj.Forecasts{i,2}=obj.Forecasts{i,2}(~SelVec);
        									end
        									
        									
        									if ~isempty(obj.OutputLogs{i,2})
        										obj.OutputLogs{i,2}=obj.OutputLogs{i,2}(~SelVec);
        									end
        									
        									
        									%if ~isempty(obj.UsedConfigs{i,2})
        									%	obj.UsedConfigs{i,2}=obj.UsedConfigs{i,2}(~SelVec);
        									%end
        									
        									%in case it was 2
        									ProgressVec(j)=0;
        									
        								end
        							end
        						end
        					
        					
        					end
        				end
        				
        				
        				
        			end
        			
        		
        			
        			
        			
        		else
        			warning('No model set yet')
        			
        		end
        		
        	end
        	
        	
        	
        	function ErrorCode = setAdditionalData(obj,ModelID,AddonData)
        		%this function will store the additionalData in a appropriate
        		%structure, but it will not be managed in time by the project 
        		%as with results and logs.
        		import mapseis.forecast.*;
        		
        		%just for start, in case something goes wrong
        		ErrorCode=-1
        		
        		%find correct model, this is best done with the name to avoid mix-ups
        		%due to different order of the models
        		
        		%generate name list (sooner or later I have to add this as property)
        		for i=1:numel(obj.ModelPlugIns(:,1))
        			ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        		end
        		
        		if isstr(ModelID)
        			IDX=find(strcmp(ModelID,ModelNames));
        			MName=ModelID;
        		else
        			%has to be numeric, I keep it in but strongly suggest
        			%to use names instead of ID 
        			IDX=ModelID;
        			MName=ModelNames{IDX};
        		end
        			
        		if isempty(IDX)
        			%model not found -> Code 1
        			ErrorCode=1;
        			return
        		end
        		
        		obj.AdditionalData.(MName)=AddonData;
        		
        		        		       		
        	end
        	
        	
        	
        	function setBenchmark(obj,BenchmarkData)
        		%Just a shortcut to store benchmark data
        		import mapseis.forecast.*;
        		        	
        		obj.BenchmarkData=BenchmarkData;
        		        		        		       		
        	end
        	
        	
        	
        	function ErrorCode = setForecastResults(obj,TimeStep,ModelID,ForecastData,DataMask,LogData,ConfigData)
        		%this function will store the handed in Forecast and additional data to the
        		%right place in the project. 
        		%ErrorCode is a Error variable, it is 0 if everything worked and something else
        		%if not.
        		import mapseis.forecast.*;
        		
        		%just for start, in case something goes wrong
        		ErrorCode=-1
        		
        		%find correct model, this is best done with the name to avoid mix-ups
        		%due to different order of the models
        		
        		%generate name list (sooner or later I have to add this as property)
        		for i=1:numel(obj.ModelPlugIns(:,1))
        			ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
        		end
        		
        		if isstr(ModelID)
        			IDX=find(strcmp(ModelID,ModelNames));
        		else
        			%has to be numeric, I keep it in but strongly suggest
        			%to use names instead of ID 
        			IDX=ModelID;
        		end
        			
        		if isempty(IDX)
        			%model not found -> Code 1
        			ErrorCode=1;
        			return
        		end
        		
        		%To be sure check if timestep exist
        		if ~any(obj.AvailableTimes==TimeStep)
        			ErrorCode=2;
        			return
        		end
        	        		
        		TimeID=find(obj.AvailableTimes==TimeStep);
        		
        		%check if a forecast object is existing if not create one.
        		if isempty(obj.Forecasts{IDX,1})
        			%no forecast -> create one 
        			CurModel=obj.ModelPlugIns{IDX,3}
        			NewForecast=obj.ForecastGrid.cloneWars;
        			NewForecast.setName(ModelNames{IDX});
        					
        			ModelFeature=CurModel.getFeatures;
        				
        			if ~strcmp(ModelFeature.ForecastType,'Selectable')
        				NewForecast.initForecast(ModelFeature.ForecastType,[]);
        					
        			else      					
        				%disp('variable ForecastType, has to be set later')
        				ModelPram=CurModel.getModelConfig
        				NewForecast.initForecast(ModelPram.ForecastType,[])
        			end
        			
        			
        			
        			ForecastID=NewForecast.getID;
        			obj.Forecasts{IDX,1}=NewForecast;
        			
        			
        			%NOTE: the addon objects are build wether they are used or not
        			%like this a on/off switching will be easier. But in case
        			%it causes performance problems I will check before creating
        			
        			%Log object
        			NewLogObj=ForecastLogObj(ModelNames{IDX},ForecastID);
        			NewLogObj.InfoType='logs';
        			obj.OutputLogs{IDX,1}=NewLogObj;
        			
        			%UsedConfig Object
        			NewLogObj=ForecastLogObj(ModelNames{IDX},ForecastID);
        			NewLogObj.InfoType='config';
        			obj.UsedConfigs{IDX,1}=NewLogObj;
        			
        			if strcmp(obj.PredictionIntervall,'realtime')	
        				obj.Forecasts{IDX,1}.setRealTimeMode(true);
        				obj.OutputLogs{IDX,1}.setRealTimeMode(true);
        				obj.UsedConfigs{IDX,1}.setRealTimeMode(true);
        			end
        			
        		end
        		
        		%to be sure, check if the forecast object has initialized
        		if ~obj.Forecasts{IDX,1}.ForecastReady
        			CurModel=obj.ModelPlugIns{IDX,3}
        			       					
        			ModelFeature=CurModel.getFeatures;
        				
        			if ~strcmp(ModelFeature.ForecastType,'Selectable')
        				obj.Forecasts{IDX,1}.initForecast(ModelFeature.ForecastType,[]);
        					
        			else      					
        				%disp('variable ForecastType, has to be set later')
        				ModelPram=CurModel.getModelConfig
        				obj.Forecasts{IDX,1}.initForecast(ModelPram.ForecastType,[])
        			end
        			
        			if strcmp(obj.PredictionIntervall,'realtime')	
        				obj.Forecasts{IDX,1}.setRealTimeMode(true);
        			end
        		end
        		
        		
        		
        		
        		
        		%Now everything should be ready to go, finally
        		try
        			obj.Forecasts{IDX,1}.addForecast(ForecastData{1},TimeStep,obj.ForecastLength,DataMask);
        		catch
       				obj.Forecasts{IDX,1}.replaceForecast(ForecastData{1},TimeStep,obj.ForecastLength,DataMask);
      				warning('old forecast has been overwritten')
      			end	
        
      			
        		if obj.ProjectConfig.ArchiveForecasts
      				obj.Forecasts{IDX,2}{TimeID,1}=ForecastData{2};
        		end
        		
        		
        		if obj.ProjectConfig.StoreLogs
        			try
        				obj.OutputLogs{IDX,1}.addLog(LogData{1},TimeStep,obj.ForecastLength);
        			catch
       					obj.OutputLogs{IDX,1}.replaceLog(LogData{1},TimeStep,obj.ForecastLength);
       					warning('old forecast log has been overwritten')
       				end	
      				
        		end
        		
        		
        		if obj.ProjectConfig.ArchiveLogs
      				obj.OutputLogs{IDX,2}{TimeID,1}=LogData{2};
        		end
        		
        		
        		if obj.ProjectConfig.SaveConfig
        			try
        				obj.UsedConfigs{IDX,1}.addLog(ConfigData,TimeStep,obj.ForecastLength);
        			catch
       					obj.UsedConfigs{IDX,1}.replaceLog(ConfigData,TimeStep,obj.ForecastLength);
       					warning('old forecast log has been overwritten')
        		     	end		
        		end
        		
        		%update the progress 
        		if ~isempty(obj.CalcProgress.Times)
        			TheTimes=obj.CalcProgress.Times;
        		else
        			TheTimes=obj.AvailableTimes;
        		end
        		
        		TheName=ModelNames{IDX};
        		TimePos=TimeStep==TheTimes;
        		if ~isfield(TheName,obj.CalcProgress.ModelProgress);
        			%you never know
        			obj.CalcProgress.ModelProgress.(TheName)=zeros(size(TheTimes));
        		end
        		
        		if ~isempty(ForecastData{1})
        			obj.CalcProgress.ModelProgress.(TheName)(TimePos)=1;
        		else
        			%probably an error in calc ->-2
        			obj.CalcProgress.ModelProgress.(TheName)(TimePos)=-2;
        		end
        		
        		%if it got up to here it terminated like it should
        		ErrorCode=0;
        		
        		
        	end
        	
        	
        	
        	
        	function ErrorCode = setUsedCatalog(obj,TimeStep,Filterlist)
        		%it will get a filterlist and the timeStep from the calculator 
        		%and do everything itself, set the right format and find
        		%the right place.
        		
        		import mapseis.forecast.*;
        		import mapseis.region.*;
        		import mapseis.datastore.*;
        		import mapseis.projector.*;
        		
        		ErrorCode=-1;
        		
        		%To be sure check if timestep exist
        		if ~any(obj.AvailableTimes==TimeStep)
        			ErrorCode=2;
        			return
        		end
        		
        		TimeID=find(obj.AvailableTimes==TimeStep);
        		
        		
        		switch obj.ProjectConfig.CatalogStore
        			case 'Filterlist'
        				obj.InputCatalogs{TimeID}=Filterlist;
        				
        			case 'ShortCat'
        				Filterlist.changeData(obj.Datastore);
        				Filterlist.updateNoEvent;
        				selected=Filterlist.getSelected;
        				Filterlist.PackIt;
        				
        				[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
        				
        				obj.InputCatalogs{TimeID}=ShortCat;
        				
        			
        			case 'Boolean'
        				Filterlist.changeData(obj.Datastore);
        				Filterlist.updateNoEvent;
        				selected=Filterlist.getSelected;
        				Filterlist.PackIt;
        				
        				obj.InputCatalogs{TimeID}=selected;
        				
        			case 'Datastore'
        				Filterlist.changeData(obj.Datastore);
        				Filterlist.updateNoEvent;
        				selected=Filterlist.getSelected;
        				Filterlist.PackIt;
        				
        				%not the best solution but will do for now, 
        				%would suggest not using it anyway
        				eventStruct=obj.Datastore.getFields([],selected);
        				CutDatastore=DataStore(eventStruct);
        				
        				obj.InputCatalogs{TimeID}=CutDatastore;
        				
        			case 'Short_and_Bool'
        				Filterlist.changeData(obj.Datastore);
        				Filterlist.updateNoEvent;
        				selected=Filterlist.getSelected;
        				Filterlist.PackIt;
        				
        				[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
        				
        				obj.InputCatalogs{TimeID}={ShortCat,selected};
        			
        			
        			case 'Data_and_Bool'
        				Filterlist.changeData(obj.Datastore);
        				Filterlist.updateNoEvent;
        				selected=Filterlist.getSelected;
        				Filterlist.PackIt;
        				
        				%not the best solution but will do for now, 
        				%would suggest not using it anyway
        				eventStruct=obj.Datastore.getFields([],selected);
        				CutDatastore=DataStore(eventStruct);
        				
        				obj.InputCatalogs{TimeID}={CutDatastore,selected};
        			
        			case 'None'
        				%Do nothing
        				
        			
        		end	
        			
        		%Another job well done
        		ErrorCode=0;
				
        		
        	end
        	
        	
        	
        	function updateCatalog(obj)
        		%only available in the DataMode 'Source', it will update
        		%the internal datastore source
        	
        		if strcmp(obj.DataMode,'Source')&~isempty(obj.Datastore)
        			obj.Datastore.selfUpdate;
        		end
        		
        	
        	end
        	
        	
        	        	
        	function UpdateCalcSpace(obj,NewCalcSpace)
        		%just a simple shortcut to avoid long typing
        		obj.CalculatorSpace=NewCalcSpace
        		
        		if ~isempty(NewCalcSpace)&~NewCalcSpace.Finished
        			obj.ProjectStatus='started';
        		elseif 	~isempty(NewCalcSpace)&NewCalcSpace.Finished
        			obj.ProjectStatus='finished';
        		
        		end
        	
        	end
        	
        	
        end

        
        
        

        
        
end


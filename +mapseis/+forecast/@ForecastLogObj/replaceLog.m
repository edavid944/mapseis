function replaceLog(obj,ForecastLog,CastTime,CastLength)
	%works similar to the addLog but replaces an old log
	%if present, the date have to match and old data to do this
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	if isempty(obj.TimeVector)
		%nothing present, just add
		disp('No Forecast present, added as new forecast log')
		obj.TimeVector=CastTime;
		obj.TimeSpacing=CastLength;
		obj.LogData{end+1}=ForecastLog;
	
	else
		if CastTime>obj.TimeVector(end)
			if CastTime<obj.TimeVector(end)+obj.TimeSpacing(end)&~obj.RealTimeMode
				error('New forecast overlaps old on')
			end
			
			obj.TimeVector(end+1)=CastTime;
			obj.TimeSpacing(end+1)=CastLength;
			obj.LogData{end+1}=ForecastLog;
			
			
			disp('Forecast is newer than every old one, and was added as new forecast');
			
		else
			MrSmall=find(obj.TimeVector==CastTime);
			
			if isempty(MrSmall)
				error('No Forecast to replace found');
			end
			
			if numel(MrSmall)>1
				%this should never happen
				error('More than one suitable forecast found')
			end
			
			oldPos=MrSmall;
			
			if isempty(CastLength)
				%use old spacing
				CastLength=obj.TimeSpacing(oldPos);
			end	
			
			%check if now overlap is present needs three conditions (if it is on start or end)
			if numel(obj.TimeVector)==1&oldPos==1 
				Condition=false;
			elseif oldPos==1
				Condition = (CastTime+CastLength)>obj.TimeVector(oldPos+1);
			elseif oldPos==numel(obj.TimeVector)
				Condition = ((obj.TimeVector(oldPos-1)+obj.TimeSpacing(oldPos-1))>CastTime);
			else
				Condition = ((obj.TimeVector(oldPos-1)+obj.TimeSpacing(oldPos-1))>CastTime)|...
				(CastTime+CastLength)>obj.TimeVector(oldPos+1);
			end
			
			if Condition&~obj.RealTimeMode
				error('There is a overlap present with an old entry')
			end	
			
			obj.TimeVector(oldPos)=CastTime;
			obj.TimeSpacing(oldPos)=CastLength;
			obj.LogData{oldPos}=ForecastLog;
			
			MaskPos=oldPos;
		
		end
	
	end

end
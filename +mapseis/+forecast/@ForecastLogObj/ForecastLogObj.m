classdef ForecastLogObj < handle
	%A storage container for logs produced by forecast models (either external
	%ones or internal ones). This object is similar to the forecast object, but 
	%much simpler
	%Actully this object can be used for almost any additional forecast data
	%InfoType can be used freely to add information about the content of the
	%object (e.g. 'logs', 'configs',...)
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	properties
		Name
		InfoType
		ID
		ForecastID
		Version
		TimeVector
		TimeSpacing
		RealTimeMode
		LogData
		TheBag
	
	end
	
	
	
	methods
		function obj=ForecastLogObj(Name,ForecastID)
			%constructor: only inits the most important fields
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
				%A bit gimmicky I admit, but could be useful later
				Name=RawChar(randi(15,1,8)+1)
			end
			
			obj.Name=Name;
			obj.InfoType='generic';
			obj.ID=RawChar(randi(15,1,8)+1)
			obj.Version='0.9';
			
			%init data
			obj.ForecastID=ForecastID;
			obj.TimeVector=[];
			obj.TimeSpacing=[];
			obj.RealTimeMode=false;
			obj.LogData={};
			
			%internal variable
			obj.TheBag={};
		
		end	

		%functions in external files
		%---------------------------

		setRealTimeMode(obj, ModeToogle)

		addLog(obj,ForecastLog,CastTime,CastLength)
		
		replaceLog(obj,ForecastLog,CastTime,CastLength)

		removeLog(obj,CastTime)

		ForecastLog = getLog(obj,CastTime)

		Name = getName(obj)

		ID = getID(obj)

		setName(obj,Name)

		idmatch = CheckIDs(obj,ForecastObj)

		PortableStruct =  exportFullData(obj)

		importFullData(obj,PortableStruct)


	end

end

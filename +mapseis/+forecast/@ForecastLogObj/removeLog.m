function removeLog(obj,CastTime)
	%removes a forecast from the list if found.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	if isempty(obj.TimeVector)
		error('No forecast logs saved')
	end
	
	oldPos=find(obj.TimeVector==CastTime);
	
	if isempty(oldPos)
		error('No Forecast Log with this date found');
	end
	
	if numel(oldPos)>1
		%this should never happen
		error('More than one suitable forecast log found')
	end
	
	%special case: forecast empty after removal
	if numel(obj.TimeVector)==1
		disp('Last forecast deleted')
		obj.TimeVector=[];
		obj.TimeSpacing=[];
		obj.LogData={};
		
		
	
	else
		NewTimeVec=[obj.TimeVector(1:oldPos-1),obj.TimeVector(oldPos+1:end)];
		NewSpacing=[obj.TimeSpacing(1:oldPos-1),obj.TimeSpacing(oldPos+1:end)];
		obj.TimeVector=NewTimeVec;
		obj.TimeSpacing=NewSpacing;
		NewCaster={obj.LogData{1:oldPos-1},obj.LogData{oldPos+1:end}};
		obj.LogData=NewCaster;
	
	end

end	
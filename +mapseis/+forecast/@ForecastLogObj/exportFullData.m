function PortableStruct =  exportFullData(obj)
	%meant to export everything for later use in a new object
	%Only needed fields are currently exported, all which can
	%be reproduced will not be exported. This may change if it
	%turns out that the creation of this data is to computing
	%intensive for the common cases.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	PortableStruct = struct('Name',obj.Name,...
							'ID',obj.ID,...
							'InfoType',obj.InfoType,...
							'ForecastID',obj.ForecastID,...
							'Version',obj.Version,...
							'TimeVector',obj.TimeVector,...
							'TimeSpacing',obj.TimeSpacing,...
							'RealTimeMode',obj.RealTimeMode,...
							'LogData',obj.LogData);
	
end
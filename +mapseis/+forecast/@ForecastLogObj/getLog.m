function ForecastLog = getLog(obj,CastTime)
	%Returns the log for a certain time, if the time is
	%not existing the nearest lower time is used instead
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	if isempty(obj.TimeVector)
		error('No log saved yet');
	end
	
	
	
	FindInTime=obj.TimeVector==CastTime;
	
	if ~any(FindInTime)
		%get the next lower time
		ModTimeVec=obj.TimeVec-CastTime;
		SmallerZero=find(ModTimeVec<0);
		
		if isempty(SmallerZero)
			error('No forecast log before this time available');
		end
		
		FindInTime=SmallerZero(end);
	
	end
	
	ForecastLog=obj.LogData{FindInTime};
	
end
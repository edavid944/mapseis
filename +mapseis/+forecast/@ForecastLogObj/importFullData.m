function importFullData(obj,PortableStruct)
	%import a previous exported data set
	%restores a forecast from a data dump.
	%In future versions this may also updates a data dump
	%to the most current version.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	%Set everything back in original state as done by the 
	%constructor
	obj.Version='0.9';
	
	%init data
	obj.TimeVector=[];
	obj.TimeSpacing=[];
	obj.RealTimeMode=false;
	obj.LogData={};
	
	%internal variable
	obj.TheBag={};
	
	%add the rest of the data directly (could be looped, but
	%I prefere it like this, more control)
	obj.Name=PortableStruct.Name;
	obj.InfoType=PortableStruct.InfoType;
	obj.ID=PortableStruct.ID;
	obj.ForecastID=PortableStruct.ForecastID;
	obj.TimeVector=PortableStruct.TimeVector;
	obj.TimeSpacing=PortableStruct.TimeSpacing;
	obj.RealTimeMode=PortableStruct.RealTimeMode;
	obj.LogData=PortableStruct.LogData;

end
function addLog(obj,ForecastLog,CastTime,CastLength)
	%adds a Forecast log to the Object
	%CastTime should give the start time of the forecast and
	%CastLength how long the forecast period is, eg. CastTime=2008
	%CastLength=1 for a 1 year forecast for the whole of 2008. The 
	%format of time does not really matter as long as it is conisted
	%and numerical.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	

	oldPos=[];
	%write times and change to time dependent if necessary
	if ~isempty(obj.TimeVector)
		if CastTime>obj.TimeVector(end)
			if CastTime<obj.TimeVector(end)+obj.TimeSpacing(end)&~obj.RealTimeMode
				error('New forecast log overlaps old on')
			end
			
			obj.TimeVector(end+1)=CastTime;
			obj.TimeSpacing(end+1)=CastLength;
			obj.Dimension.Time=true;
		
		else
			%find position
			MrSmall=find(obj.TimeVector<CastTime);
		
			if isempty(MrSmall)
			error('No suitable position found');
			end
		
			oldPos=MrSmall(end);
			
			%check if now overlap is present
			Condition = ((obj.TimeVector(oldPos)+obj.TimeSpacing(oldPos))>CastTime)|...
			(CastTime+CastLength)>obj.TimeVector(oldPos+1);
			if Condition&~obj.RealTimeMode|(obj.TimeVector(oldPos)==CastTime)
				error('There is a overlap present with an old entry, use replaceLog if and old entry should be replaced')
			end	
		
			NewTimeVec=[obj.TimeVector(1:oldPos),CastTime,obj.TimeVector(oldPos+1:end)];
			NewSpacing=[obj.TimeSpacing(1:oldPos),CastLength,obj.TimeSpacing(oldPos+1:end)];
			obj.TimeVector=NewTimeVec;
			obj.TimeSpacing=NewSpacing;
			obj.Dimension.Time=true;
		end
	
	else
		obj.TimeVector=CastTime;
		obj.TimeSpacing=CastLength;
	end
	
	%write forecast
	if isempty(oldPos)
		obj.LogData{end+1}=ForecastLog;
		
	else
		NewCaster={obj.LogData{1:oldPos},ForecastLog,obj.LogData{oldPos+1:end}};
		obj.LogData=NewCaster;
	
	end
	
	

end
classdef SliceViewer_mk2 < handle
	%This is a prototype for a viewer with allows to view a Volume Slice by
	%Slice. It is currently in the forecast package, but maybe not staying
	%there. Also it is just test class, and will likely be changed quite
	%a lot.
	%It is also an experiment how well the approach with plotting everything
	%and showing only the wanted part works, especially with large forecasts
	
	%Second version, does not plot all times at once to keep plotting times
	%lower for large data sets
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	properties
		Name
		Version
		Forecast
		WorkMode
		MagList
		DepthList
		TimeList
		AxisVectors
		TitleRawParts
		PlotHandles
		WindowHandle
		TitleHandle
		plotAxis
		MagSlider
		MagText
		DepthSlider
		DepthText
		TimeSlider
		TimeText
		LogMode
		AllReady
		CurrentPlotRange
		TimeSliceSize  
		HandPos
	end
	
	
	
	methods
		function obj=SliceViewer_mk2(Name)
			%constructor: only inits the most important fields
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
				%A bit gimmicky I admit, but could be useful later
				Name=RawChar(randi(16,8,1))
			end
			
			obj.Name=Name;
			obj.Version='0.5';
			obj.Forecast=[];
			obj.WorkMode=struct(	'Space',false,...
									'Depth',false,...
									'Mag',false,...
									'Time',false);
			
			obj.MagList=[];
			obj.DepthList=[];
			obj.TimeList=[];
			obj.AxisVectors={};
			obj.TitleRawParts={};
			obj.PlotHandles=[];
			obj.WindowHandle=[];
			obj.TitleHandle=[];
			obj.plotAxis=[];
			obj.MagSlider=[];
			obj.MagText=[];
			obj.DepthSlider=[];
			obj.DepthText=[];
			obj.TimeSlider=[];
			obj.TimeText=[];
			obj.LogMode=false;
			obj.AllReady=false;
			obj.CurrentPlotRange=[];
			obj.TimeSliceSize=5;
			obj.HandPos=[1,1,1];

		end
	
	
		%Methods parked in external files
		%--------------------------------
	
		TypicalStart(obj,Forecast)

		AddForecast(obj,Forecast)
	
		CreateViewer(obj)

		ApplyAxisSetting(obj)

		InitPlots(obj,CurTime)

		ShowPlotSlider(obj)

		HideAllPlots(obj)
	
	
	end
	
end
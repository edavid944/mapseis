function ApplyAxisSetting(obj)
	%Formats the Axis of the GUI
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	%default optical stuff
	set(obj.plotAxis,'LineWidth',3,'FontSize',18,'FontWeight','bold','Box','on');
	%positioning the axis
	set(obj.plotAxis,'Units','normalized','Position', [0.1 0.35 0.7 0.55]);
	
	%set labels
	xlab=xlabel('Longitude  ');
	ylab=ylabel('Latitude  ');
	set([xlab,ylab],'FontSize',18,'FontWeight','bold');
	
	%set generic title
	obj.TitleHandle=title([obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
	obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},...
	obj.TitleRawParts{7},'   ']);
	set(obj.TitleHandle,'FontSize',24,'FontWeight','bold');
	
end
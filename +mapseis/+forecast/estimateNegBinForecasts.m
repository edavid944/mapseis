function [NegBinCast] = estimateNegBinForecasts(PoissonCast,PastVar)
	%estimates parameters for a negative binomial forecast based on the
	%variance of the past seismicity and a poisson based forecast.
	
	%Input
	%PoissonCast:	Relmtype poisson based forecast (rates being row 9) 
	%PastVar:	The variance of the past seismicity either a vector
	%		or a single value
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	%get Poisson rates and raw grid
	PoisRates=PoissonCast(:,9);
	PurGrid=PoissonCast(:,1:8);
	Selector=PoissonCast(:,10);
	
	%prepare past variance if needed)
	if numel(PastVar)==1
		PastVar=ones(size(PoisRates))*PastVar;
	end
	
	%estimate parameters
	
	PVal =PoisRates./PastVar;
	RVal = (PVal.*PoisRates)./(1-PVal);
	
	%assemble output
	NegBinCast=[PurGrid,RVal,Selector,PVal];
	
	

end



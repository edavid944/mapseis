%The old poisson only routines used the forecast object, a backup just in case


function [ForeSlice ForeTime ForeLength] = get2DSlice(obj,CastTime,Magnitude,Depth)
        		%returns a slice of the forecast as 2D matrix.
        		%Magnitude and/or Depth can be left empty if the grid has only one 
        		%bin for one of them.
        		%For Magnitude and Depth also the keyword 'all' can be used, in this 
        		%case a summation of all grid cells in that dimension will be returned.
        		
        		%search if the forecast can be found
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVec-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
			%condition for 3D (either Mag or Depth but not both)
			Cond3d=(obj.Dimension.Depth | obj.Dimension.Mag) & ~(obj.Dimension.Depth & obj.Dimension.Mag);
			%condition to stop and return an error
			BreakCond=(obj.Dimension.Depth&isempty(Depth))|(obj.Dimension.Mag&isempty(Magnitude)); 
			
			%Maybe possible to simplify this a bit
			switch obj.ForecastType.Type
				case 'Poisson'
					RawGrid=obj.ForecastData{FindInTime};
					ForeTime=obj.TimeVector(FindInTime);
					ForeLength=obj.TimeSpacing(FindInTime);
					
					
					if ~obj.Dimension.Depth&~obj.Dimension.Mag
						%only 2D data, the easiest case

						ForeSlice=NaN(size(obj.SelectedNodes));
						ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
						
					elseif Cond3d
						%3D case
						if BreakCond
							error('Depth or Mag input variable not defined')
						end
					
						if obj.Dimension.Depth
							if strcmp(Depth,'all')
								%sum up
								ForeSlice=NaN(size(obj.SelectedNodes));
								
								%first step
								ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
								
								%additonal ones
								for i=2:numel(obj.DepthBins)
									ForeSlice(obj.SelectedNodes)=ForeSlice(obj.SelectedNodes)+RawGrid(obj.SelectedIDs+(SliPos-1));	
								end
							else
								%single field
								SliPos=find(obj.DepthBins==Depth);
								
								if isempty(SliPos)
									error('Depth not found');
								end
								
								ForeSlice=NaN(size(obj.SelectedNodes));
								ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs+(SliPos-1));
								%PLEASE TEST IT
																
							end
							
							
						elseif obj.Dimension.Mag
							if strcmp(Magnitude,'all')
								%sum up
								ForeSlice=NaN(size(obj.SelectedNodes));
								
								%first step
								ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
								
								%additonal ones
								for i=2:numel(obj.MagBins)
									ForeSlice(obj.SelectedNodes)=ForeSlice(obj.SelectedNodes)+RawGrid(obj.SelectedIDs+(SliPos-1));	
								end
							else
								%single field
								SliPos=find(obj.MagBins==Magnitude);
								
								if isempty(SliPos)
									error('Magnitude not found');
								end
								
								ForeSlice=NaN(size(obj.SelectedNodes));
								ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs+(SliPos-1));
								%PLEASE TEST IT
																
							end
						end
						
					else
						%full 4D (most difficult case with two jumps
						ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
						
						if BreakCond
							error('Depth or Mag input variable not defined')
						end
						
						
						if ~strcmp(Depth,'all')&~strcmp(Magnitude,'all')
							%no summation needed
							MagPos=find(obj.MagBins==Magnitude);
							
							if isempty(MagPos)
								error('Magnitude not found');
							end
							
							DepthPos=find(obj.DepthBins==Depth);
							
							if isempty(DepthPos)
								error('Depth not found');
							end
							
							NewSelID=obj.SelectedIDs{1}{DepthPos}+(MagPos-1)
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
							
						elseif strcmp(Depth,'all')&~strcmp(Magnitude,'all')
							%sumamation of the depth needed
							MagPos=find(obj.MagBins==Magnitude);
							
							if isempty(MagPos)
								error('Magnitude not found');
							end
							
							%Dummy slice
							RawSlice=ForeSlice;
							
							%first element to prevent zeros
							NewSelID=obj.SelectedIDs{1}{1}+(MagPos-1)
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
							
							%now the rest
							for i=2:numel(obj.DepthBins)
								NewSelID=obj.SelectedIDs{1}{i}+(MagPos-1)
								
								%only take Ids which are not nan
								NotNaN=~isnan(NewSelID);
								NewSelID=NewSelID(NotNaN);
								
								%again the annoying substep
								NewSlice=RawSlice;
								NewSlice(obj.SelectedNodes{1}{i})=RawGrid(NewSelID);
								ForeSlice=ForeSlice+NewSlice;
							end
							
						
						elseif ~strcmp(Depth,'all')&strcmp(Magnitude,'all')
							%summation of the magnitude
							DepthPos=find(obj.DepthBins==Depth);
							
							if isempty(DepthPos)
								error('Depth not found');
							end
							
							%Dummy slice
							RawSlice=ForeSlice;
							
							%first element to prevent zeros
							NewSelID=obj.SelectedIDs{1}{DepthPos};
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
							
							%now the rest
							for i=2:numel(obj.MagBins)
								NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1);
								
								%again the annoying substep
								NewSlice=RawSlice;
								NewSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
								ForeSlice=ForeSlice+NewSlice;
							end
							
							
						else
							%summation of magnitude and depth
							
							%first do a prototype slice
							NewSelID=obj.SelectedIDs{1}{1};
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
							MrNan=isnan(ForeSlice);
							
							%make a new Sum slice with zeros
							ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
							RawSlice=ForeSlice;
							
							%now sum them all
							for d=1:numel(obj.DepthBins)
								for m=1:numel(obj.MagBins)
									NewSelID=obj.SelectedIDs{1}{d}+(m-1);
									
									%only take Ids which are not nan
									NotNaN=~isnan(NewSelID);
									NewSelID=NewSelID(NotNaN);
									
									NewSlice=RawSlice;
									%again the annoying substep
									NewSlice(obj.SelectedNodes{1}{d})=RawGrid(NewSelID);
									ForeSlice=ForeSlice+NewSlice;	
									
								end
							end
							
							%Kill all zeros which should not be existing
							ForeSlice(MrNan)=NaN;
							
						end
						
						
						
						
					
					end
					
				case 'None'
					error('No forecast set')
			end	
        			
        			
        	
        		
        		
        		
        	end
        	
        	
        	function [ForeVolume ForeTime ForeLength] = get3DVolume(obj,CastTime,Magnitude,Depth)
        		%same as get2DSlice, but for Volumes, at least 3D dimensions (without time) 
        		%are needed this function to work
        		
        		%Caution some of the stuff is experimental and it is not sure if it works in every
        		%case. For instace could the depth summation fail in  cases the region is not
        		%cylindrical (which isn't allowed right now anyway)
        		
        		if ~obj.Dimension.Space&(~obj.Dimension.Depth|~obj.Dimension.Mag)
        			%missing a necessary dimension
        			error('Not enough dimension available'); 
        		end
        		
        		
        		%search if the forecast can be found
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVec-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
        		
        		switch obj.ForecastType.Type
        			case 'Poisson'
        				RawGrid=obj.ForecastData{FindInTime};
					ForeTime=obj.TimeVector(FindInTime);
					ForeLength=obj.TimeSpacing(FindInTime);
					try
						ForeVolume=NaN(size(obj.SelectedNodes{2}));
						ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
					catch
						%probalby 2D
						ForeVolume=[];
						ForeSlice=NaN(size(obj.SelectedNodes));
					end
					
					
					if obj.Dimension.Depth&obj.Dimension.Mag
						%full 4D case
						BreakCond = 	(isempty(Depth)&isempty(Magnitude))|...
								(~isempty(Depth)&~isempty(Magnitude));
							
						%possible errors
						if BreakCond
							error('Wrong Depth and/or Magnitude parameter')
						end
						
						if strcmp(Depth,'all')&strcmp(Magnitude,'all') 
							error('only on parameter can be set to "all"');
						end
						
						
						%different cases (summation over one dimension or a specific slide)
						if strcmp(Depth,'all')
							ForeVolume=[];
							
							%again to prevent unnecessary zeros
							Tempslice=ForeSlice;
							NewSelID=obj.SelectedIDs{1}{1};
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							Tempslice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
							MrNan=isnan(Tempslice);
							ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
							
							%all depth have to be summed and the new "depth" is the magnitude
							for i=1:numel(obj.MagBins)
								
								DepthSlice=ForeSlice;
								
								
								%first step (again the slighty annoying extra step has to be done)
								NewSelID=obj.SelectedIDs{1}{1}+(i-1);
								DepthSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
								
								for j=2:numel(obj.DepthBins)
									NewSlice=ForeSlice;
									NewSelID=obj.SelectedIDs{1}{j}+(i-1);
									
									%only take Ids which are not nan
									NotNaN=~isnan(NewSelID);
									NewSelID=NewSelID(NotNaN);
									
									NewSlice(obj.SelectedNodes{1}{j})=RawGrid(NewSelID);
									DepthSlice=DepthSlice+NewSlice;
								
								end
								
								%reapply nan
								DepthSlice(MrNan)=NaN;
								
								%save in in 3D matrix
								ForeVolume(:,:,i)=DepthSlice;
								
								
						
							end
							
							
							
							
						elseif strcmp(Magnitude,'all') 
							%One of the real 3D cases
							
							%It has to be done a bit more complicated than I originally did think
							%because using a matrix with logical matrix results in a matrix, expect
							%you write an other matrix into it (sounds wierd I know)
							RawVol=ForeVolume;
							
							%first step
							NewSelID=obj.SelectedIDs{2};
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
							
							for i=2:numel(obj.MagBins)
								NewSlide=RawVol;
								NewSelID=obj.SelectedIDs{2}+(i-1);
								
								%only take Ids which are not nan
								NotNaN=~isnan(NewSelID);
								NewSelID=NewSelID(NotNaN);
								
								NewSlide(obj.SelectedNodes{2})=RawGrid(NewSelID);
								
								%now sum up the single volumes without any selector
								ForeVolume=ForeVolume+NewSlide;
								
							end
							
							
						elseif ~isempty(Depth)
							%Similar to the all case but with a specified depth
							ForeVolume=[];
							DepthPos=find(obj.DepthBins==Depth);
							
							if isempty(DepthPos)
								error('Depth not found');
							end
													
							%get the magnitude slices
							for i=1:numel(obj.MagBins)
								
								DepthSlice=ForeSlice;
								
								NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1)
								
								%only take Ids which are not nan
								NotNaN=~isnan(NewSelID);
								NewSelID=NewSelID(NotNaN);
								
								DepthSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
								
								%save in in 3D matrix
								ForeVolume(:,:,i)=DepthSlice;
								
							
							end
							
							
						elseif ~isempty(Magnitude)
							MagPos=find(obj.MagBins==Magnitude);
							
							if isempty(MagPos)
								error('Magnitude not found');
							end
							
							NewSelID=obj.SelectedIDs{2}+(MagPos-1);
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
						end
						
						
						
					elseif obj.Dimension.Depth&~obj.Dimension.Mag
						%"real" 3D case with depth,in this case no Depth and Magnitude
						%input is "allowed" (means it will be ignored)
						if ~isempty(Depth)&~isempty(Magnitude)
							disp('Depth and Magnitude input ignored');
							warning('Depth and Magnitude input ignored');
						end
						
						NewSelID=obj.SelectedIDs{2};
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
						
						
						
					elseif ~obj.Dimension.Depth&obj.Dimension.Mag
						%Magnitude as 3rd dimension,in this case no Depth and Magnitude
						%input is "allowed" (means it will be ignored)
						if ~isempty(Depth)&~isempty(Magnitude)
							disp('Depth and Magnitude input ignored');
							warning('Depth and Magnitude input ignored');
						end
						
						ForeVolume=[];
						
							
													
						%get the magnitude slices
						for i=1:numel(obj.MagBins)
							
							DepthSlice=ForeSlice;
							disp(i)
							NewSelID=obj.SelectedIDs+(i-1);
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							DepthSlice(obj.SelectedNodes)=RawGrid(NewSelID);
							
							%save in in 3D matrix
							ForeVolume(:,:,i)=DepthSlice;

						end
						
						
					end
					
				case 'None'
					error('No forecast set')
			end	
        		
        		
        		
        	end

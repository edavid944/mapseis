classdef SliceViewer_mk2 < handle
	%This is a prototype for a viewer with allows to view a Volume Slice by
	%Slice. It is currently in the forecast package, but maybe not staying
	%there. Also it is just test class, and will likely be changed quite
	%a lot.
	%It is also an experiment how well the approach with plotting everything
	%and showing only the wanted part works, especially with large forecasts
	
	%Second version, does not plot all times at once to keep plotting times
	%lower for large data sets
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	properties
        	Name
        	Version
        	Forecast
        	WorkMode
        	MagList
        	DepthList
        	TimeList
        	AxisVectors
        	TitleRawParts
        	PlotHandles
        	WindowHandle
        	TitleHandle
        	plotAxis
        	MagSlider
        	MagText
        	DepthSlider
        	DepthText
        	TimeSlider
        	TimeText
        	LogMode
		AllReady
		CurrentPlotRange
		TimeSliceSize  
		HandPos
        end
    

    
        methods
        	function obj=SliceViewer_mk2(Name)
        		%constructor: only inits the most important fields
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(16,8,1))
        		end
        	
        		
        		obj.Name=Name;
        		obj.Version='0.5';
        		obj.Forecast=[];
        		obj.WorkMode=struct(	'Space',false,...
        					'Depth',false,...
        					'Mag',false,...
        					'Time',false);
        		
        		obj.MagList=[];
        		obj.DepthList=[];
        		obj.TimeList=[];
        		obj.AxisVectors={};
        		obj.TitleRawParts={};
        		obj.PlotHandles=[];
        		obj.WindowHandle=[];
        		obj.TitleHandle=[];
        		obj.plotAxis=[];
        		obj.MagSlider=[];
        		obj.MagText=[];
        		obj.DepthSlider=[];
        		obj.DepthText=[];
        		obj.TimeSlider=[];
        		obj.TimeText=[];
        		obj.LogMode=false;
        		obj.AllReady=false;
        		obj.CurrentPlotRange=[];
        		obj.TimeSliceSize=5;
        		obj.HandPos=[1,1,1];
        	end
        	
        	
        	function TypicalStart(obj,Forecast)
        		
        		obj.AddForecast(Forecast);
        		obj.CreateViewer;
        		obj.InitPlots;
        	       		
        	end
        	
        	
        	function AddForecast(obj,Forecast)
        		%just for adding a forecast
        		
        		if ~isempty(obj.WindowHandle)
        			%close window if existing
        			%(Will come later)
        		
        		end
        		
        		%save it
        		obj.Forecast=Forecast;
        		obj.WorkMode=obj.Forecast.Dimension;
        		
        		[SpaceGrid DepthGrid  MagGrid] = obj.Forecast.getAxisMesh;
        		
        		%init needed data
        		obj.MagList=obj.Forecast.MagBins;
        		obj.DepthList=obj.Forecast.DepthBins;
        		obj.TimeList=obj.Forecast.TimeVector;
        		obj.AxisVectors=SpaceGrid;
        		obj.TitleRawParts{1}=obj.Forecast.Name;
        		obj.TitleRawParts{2}=', Magnitude: ';
        		obj.TitleRawParts{3}=' ';
        		obj.TitleRawParts{4}=', Depth: ';
        		obj.TitleRawParts{5}=' ';
        		obj.TitleRawParts{6}=', Time: ';
        		obj.TitleRawParts{7}=' ';
        		
        		%clear old stuff (if it exists)
        		obj.PlotHandles=[];
        		obj.WindowHandle=[];
        		obj.TitleHandle=[];
        		obj.plotAxis=[];
        		obj.MagSlider=[];
        		obj.MagText=[];
        		obj.DepthSlider=[];
        		obj.DepthText=[];
        		
        		
        	end
        	
        	
        	function CreateViewer(obj)
        		import mapseis.util.*;	
        	
        		%maybe check if there is an old window existing (later)
        		
        		%check if a forecast is loaded
        		if isempty(obj.Forecast)
        			error('No Forecast loaded, cannot create Viewer');
        		end	
        		
        		
        		%create a window 
        		obj.WindowHandle=figure;
        		
        		%set default size
        		set(obj.WindowHandle,'Name','Sliceviewer','Units','normalized',...
        			'Position',[0.2 0.2 0.6 0.6]);
        		
        		%create axis
        		axis;
        		obj.plotAxis=gca;
        		obj.ApplyAxisSetting;
        		
        		
        		%create the needed sliders
        		if obj.WorkMode.Mag
        			obj.MagSlider=uicontrol('Style', 'slider',...
        						'Min',1,'Max',numel(obj.MagList),'Value',1,...
        						'Units','normalized',...
        						'Position', [0.15,0.05 0.6 0.1],...
        						'Callback', @(s,e) obj.ShowPlotSlider());  
        			
        			LabText1=uicontrol(	'Style', 'text',...
        					  	'String','Magnitude: ',... 
        					  	'Units','normalized',...
        					  	'Position', [0.15 0.07 0.6 0.05],...
        					  	'FontSize',18,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8])			
        			
        			obj.MagText=uicontrol(	'Style', 'text',...
        					  	'String',num2str(obj.MagList(1)),... 
        					  	'Units','normalized',...
        					  	'Position', [0.5 0.075 0.05 0.05],...
        					  	'FontSize',24,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8],...
        					  	'ForegroundColor','r')
        			
        		end
        		
        		if obj.WorkMode.Depth
        			obj.DepthSlider=uicontrol('Style', 'slider',...
        						'Min',1,'Max',numel(obj.DepthList),'Value',1,...
        						'Units','normalized',...
        						'Position', [0.8,0.275 0.1 0.6],...
        						'Callback', @(s,e) obj.ShowPlotSlider());   
        						
        			LabText2=uicontrol(	'Style', 'text',...
        					  	'String','Depth: ',... 
        					  	'Units','normalized',...
        					  	'Position', [0.795 0.18 0.2 0.05],...
        					  	'FontSize',18,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8])			
        			
        			obj.DepthText=uicontrol('Style', 'text',...
        					  	'String',num2str(obj.DepthList(1)),... 
        					  	'Units','normalized',...
        					  	'Position',[0.795 0.14 0.2 0.05],...
        					  	'FontSize',24,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8],...
        					  	'ForegroundColor','r')			
        						
        		end
        		
        		
        		if obj.WorkMode.Time
        			obj.TimeSlider=uicontrol('Style', 'slider',...
        						'Min',1,'Max',numel(obj.TimeList),'Value',1,...
        						'Units','normalized',...
        						'Position', [0.15,0.15 0.6 0.1],...
        						'Callback', @(s,e) obj.ShowPlotSlider());   
        						
        			LabText3=uicontrol(	'Style', 'text',...
        					  	'String','Time: ',... 
        					  	'Units','normalized',...
        					  	'Position', [0.15 0.17 0.6 0.05],...
        					  	'FontSize',18,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8])			
        			
        			obj.TimeText=uicontrol('Style', 'text',...
        					  	'String',num2str(decyear(datevec(obj.TimeList(1)))),... 
        					  	'Units','normalized',...
        					  	'Position', [0.5 0.175 0.1 0.05],...
        					  	'FontSize',24,'FontWeight','bold',...
        					  	'BackgroundColor',[0.8 0.8 0.8],...
        					  	'ForegroundColor','b')			
        						
        		end
        		
        		
        	end
        	
        	function ApplyAxisSetting(obj)
        		%default optical stuff
        		set(obj.plotAxis,'LineWidth',3,'FontSize',18,'FontWeight','bold','Box','on');
        		%positioning the axis
        		set(obj.plotAxis,'Units','normalized','Position', [0.1 0.35 0.7 0.55]);
        		
        		%set labels
        		xlab=xlabel('Longitude  ');
        		ylab=ylabel('Latitude  ');
        		set([xlab,ylab],'FontSize',18,'FontWeight','bold');
        		
        		%set generic title
        		obj.TitleHandle=title([obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
        					obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},...
        					obj.TitleRawParts{7},'   ']);
        		set(obj.TitleHandle,'FontSize',24,'FontWeight','bold');
        	end
        	
        	
        	function InitPlots(obj,CurTime)
        		%plot everything
        		%the plot routine may change later to incoporate
        		%more settings and also to allow coastlines, earthquakes
        		%and so one.
        		
        		%now allows to plot only a range of time slices
        		if nargin<2
        			CurTime=1;
        		end
        		
        		%"delete" old plots
        		axes(obj.plotAxis);
        		h1=plot(mean(obj.AxisVectors{1}(:)),mean(obj.AxisVectors{2}(:)),'w');
        		set(h1,'Visible','off');
        		
        		overMin=[];
        		overMax=[];
        		disp(CurTime)
        		
        		if ~(numel(obj.TimeList)==1)
        			if numel(obj.TimeList)<=2*obj.TimeSliceSize+1
        				obj.CurrentPlotRange=[1,numel(obj.TimeList)];		
        			else
        				TempPlotRange=[CurTime-obj.TimeSliceSize,CurTime+obj.TimeSliceSize];
        				disp(TempPlotRange)
        				if TempPlotRange(1)<=0
        					TempPlotRange(2)=1-TempPlotRange(1);
        					TempPlotRange(1)=1;
        					
        					if TempPlotRange(2)>numel(obj.TimeList)
        						TempPlotRange(2)=numel(obj.TimeList);
        					end
        					
        				elseif TempPlotRange(2)>numel(obj.TimeList)
        					TempPlotRange(1)=numel(obj.TimeList)-TempPlotRange(2);
        					TempPlotRange(2)=numel(obj.TimeList);
        					
        					if TempPlotRange(1)<1
        						TempPlotRange(1)=1;
        					end
        			      				
        				end
        				
        				obj.CurrentPlotRange=TempPlotRange;
        				
        			end
        			
        		else
        			obj.CurrentPlotRange=[1,1]
        		end
        		
        		
        		MaxPlot=obj.CurrentPlotRange(2)-obj.CurrentPlotRange(1);
        		if MaxPlot==0
        			MaxPlot=1;
        		end	
        		
        		%empty handles
        		obj.PlotHandles={};
        		
        		hold on;
        		for i=1:numel(obj.MagList)
        			for j=1:numel(obj.DepthList)
        				for k=1:MaxPlot
												
						%T-M-D coordinates
						TransTime=obj.CurrentPlotRange(1)+(k-1);
						
						CastTime=obj.TimeList(TransTime);
						Magnitude=obj.MagList(i);
						Depth=obj.DepthList(j);
						
						%get data
						[ForeSlice ForeTime ForeLength] = obj.Forecast.get2DSlice(CastTime,Magnitude,Depth);
						
						if obj.LogMode
							%avoid infinity
							ForeSlice(ForeSlice==0)=realmin;
							ForeSlice=log10(ForeSlice);
						end
						
						%go for min and max (for unified colorscale)
						overMin=min([overMin,min(ForeSlice(:))]);
						overMax=max([overMax,max(ForeSlice(:))]);
						
						%plot it
						axes(obj.plotAxis);
						obj.PlotHandles{i,j,k}=image(obj.AxisVectors{1}(1,:),obj.AxisVectors{2}(:,1),ForeSlice);
															
						%some additional plot settings
						set(obj.PlotHandles{i,j,k},'CDataMapping','scaled');
						
						%hide it	
						set(obj.PlotHandles{i,j,k},'Visible','off')
					end
        			end
        		end
        		
        		%set coloraxis
        		%caxis([overMin overMax]);
        		
        		myBar=colorbar;
        		
        		%here could be the additional stuff needed to be plotted
        		
        		hold off;
        		
        		%set axis limit right
        		xlim([min(obj.AxisVectors{1}(:)),max(obj.AxisVectors{1}(:))]);
        		ylim([min(obj.AxisVectors{2}(:)),max(obj.AxisVectors{2}(:))]);
        		
        		%aspect ratio&axis direction
        		set(obj.plotAxis,'YDir','normal');
        		latlim = get(obj.plotAxis,'Ylim');
        		set(obj.plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
        		
        		%reapply axis setting, some version just override them
        		obj.ApplyAxisSetting;
        		
        		obj.AllReady=true;
        		
        		%update position (not ideal but it should work all the time
        		TimeRange=obj.CurrentPlotRange(1):obj.CurrentPlotRange(2);
        		TimerPos=find(TimeRange==CurTime);
        		obj.HandPos(3)=TimerPos(1);
        		
        		%set first image visible
        		set(obj.PlotHandles{obj.HandPos(1),obj.HandPos(2),obj.HandPos(3)},'Visible','on');
        	end
        	
        	
        	function ShowPlotSlider(obj)
        		%shows the by the sliders selected plot
        		%I maybe add an addition function for a other gui element
        		
        		import mapseis.util.*;	
        		
        		%check if it is save to do the work
        		if obj.AllReady
        			%Default position needed if only one is variable
        			MagPos=1;
        			DepthPos=1;
        			TimePos=1;
        			
        			if obj.WorkMode.Mag
        				%get slider position
        				MagPos=round(get(obj.MagSlider,'Value'));
        				obj.HandPos(1)=MagPos;
        				
					%set MagText
					set(obj.MagText,'String',num2str(obj.MagList(MagPos)));
					
					%change Title part
					obj.TitleRawParts{3}=num2str(obj.MagList(MagPos));
					
					%set back the rounded value
					set(obj.MagSlider,'Value',MagPos);
					        				
        			end 
        			
        			
        			if obj.WorkMode.Depth
        				%get slider position
        				DepthPos=round(get(obj.DepthSlider,'Value'));
        				obj.HandPos(2)=DepthPos;
        				
					%set MagText
					set(obj.DepthText,'String',num2str(obj.MagList(DepthPos)));
					
					%change Title part
					obj.TitleRawParts{5}=num2str(obj.MagList(DepthPos));
					
					%set back the rounded value
					set(obj.DepthSlider,'Value',DepthPos);
        			end
        			
        			if obj.WorkMode.Time
        				%get slider position
        				TimePos=round(get(obj.TimeSlider,'Value'));
        				
					%set TimeText
					set(obj.TimeText,'String',num2str(decyear(datevec(obj.TimeList(TimePos)))));
					
					%change Title part
					obj.TitleRawParts{7}=num2str(decyear(datevec(obj.TimeList(TimePos))));
					
					%set back the rounded value
					set(obj.TimeSlider,'Value',TimePos);
					        
					%check if still in range, if not re-init plot
					if ~(TimePos>=obj.CurrentPlotRange(1)&TimePos<=obj.CurrentPlotRange(2))
						obj.InitPlots(TimePos);
						
					else
						TimeRange=obj.CurrentPlotRange(1):obj.CurrentPlotRange(2);
						Pos=find(TimeRange==TimePos);
						obj.HandPos(3)=Pos(1);
					end
        			end 
        			
        			%change title
        			try
        				set(obj.TitleHandle,'String',[obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
        					obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},obj.TitleRawParts{7},'   ']);
        			catch
        				%Title somehow deleted ->recreate
        				obj.TitleHandle=title([obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
        					obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},obj.TitleRawParts{7},'   ']);
        				set(obj.TitleHandle,'FontSize',24,'FontWeight','bold');
        			end
        			
        			
        			%set all plots to hidden
        			obj.HideAllPlots;
        			
        			%set the selected plot to visible
        			set(obj.PlotHandles{obj.HandPos(1),obj.HandPos(2),obj.HandPos(3)},'Visible','on');
        			
        			
        			
        			
        		end
        	
        	end
        	
        	
        	function HideAllPlots(obj)
        		%Hides every plot
        		if ~isempty(obj.PlotHandles)
        			for i=1:numel(obj.PlotHandles)
        				set(obj.PlotHandles{i},'Visible','off');
        			end
        		end
        	end
        	
        	
        	
        	
        	
        	
        end



end	

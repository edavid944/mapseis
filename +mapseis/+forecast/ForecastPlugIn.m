classdef ForecastPlugIn < handle
	%a interface or abstract class  for a Forecast plugin. It is an more or 
	%less empty object and only specify what each plugin needs to provide 
	%for being supported by mapseis forecast suit.
	%To use it, the plugin has to be derived from it 
	%(e.g. TripleSPlugin < mapseis.forecast.ForecastPlugIn
	
	%DON'T PANIC
	%The descriptions are longer than the code which should be written
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	properties
		PluginName
        	Version %Has to be a STRING (consist of CodeVersion and Variant)
        	CodeVersion %Version of the code
        	Variant %allows to use multiple varitions of the same model in a project
        	ConfigID %Has to be re-set everytime the model is configurated
        	ModelInfo %The place for a description about the model 
        	Datastore
        	Filterlist
        	ModelReady 
        	CPUTimeData
        	
        	%ModelReady signalizes that the model is configurated and ready 
        	%for calculation, the use is optional, just set it to true all
        	%the time if the feature is not supported
        	
        	%CPUTimeData is used for benchmarking purpose, it should be a 
        	%structure with some defined fields, if it is used. It can be empty
        	%in this case the calculator will ignore the data
        	
        end
    

        %TODO: write a description about what is wanted from the functions
        methods (Abstract)
        	%obj = ForecastPlugIn()
              	
        	   	
        	       	
        	ConfigureModel(obj,ConfigData)
        	%This has method should allow to set the configurations of the model
        	%based on the "template" specified in getParameterSetting. Only
        	%direct model parameters are included here, no parameter of the 
        	%test region should be included, those will be set by Config-
        	%ureTestRegion.
        	
        	ConfigureFramework(obj,FrameConfig)
        	%Similar to ConfigureModel, but consist only of framework based
        	%variable which are defined by the framework. It consist at the
        	%moment out of two fields:
        	%TemporaryFolder: this is path of the temporary folder which may
        	%		  be used to temporary store the files needed for
        	%		  a forecast. 
        	%ModelConfigFile: In a way a optional field, but it has to be
        	%		  present even if it is ignored and left empty.
        	%		  In this field a path to a config file of the 
        	%		  model can be stored. The idea is not to store
        	%		  configuration for the forecast in this file but
        	%		  things actually needed to start, like paths to 
        	%		  the executable files or system dependent config-
		%		  urations which have to be set only during 
		%		  installation of the model.
        	
        	
        	ConfigureTestRegion(obj,RegionConfig)
        	%This  method should allow to set the TestRegion config and grid
        	%The variable RegionConfig will be structure with following 
        	%entries:
        	
        	%RegionPoly:		Polygon surrounding the region
        	%RegionBoundingBox:	Box around the Polygon
        	%LonSpacing:		Longitude spacing or spacial spacing
        	%			in case only on spacing is allowed
        	%LatSpacing:		Latitude spacing, if two spacings 
        	%			are allowed
        	%GridPrecision:		The value to which the grid was rounded to.
        	%MinMagnitude:		Minimum magnitude which should be 
        	%			forecasted
        	%MaxMagnitude:		The maximal magnitude which should be
        	%			forecasted
        	%MagnitudeBin:		Size of the Magnitude Bin often this 
        	%			will be 0.1, but can be different. This
        	%			will be empty if no magnitude binning is
        	%			used
        	%MinDepth:		Minimal depth range which should be 
        	%			forecasted, in most cases this will be 0
        	%MaxDepth:		Maximal depth to forecast
        	%DepthBin:		In most cases this will empty as 3D 
        	%			regions are not common, but in case a
        	%			3D model is used this gives depth bin 
        	%			size
        	%TimeInterval:		Length of the forcast in days or year 
        	%			(can be specified in getFeatures)
        	%RelmGrid:		A raw grid similar to the one used in 
        	%			Relm Forecasts based on the values 
        	%			specified. 
        	%			(Relmformat: [LonL LonH LatL LatH ...
        	%			DepthL DepthH MagL MagH (ID) Mask]
        	%Nodes			%Just the spacial nodes of the grid as 
        	%			for instance by TripleS, in contrary to
        	%			the RELM grid this descriptes the centre
        	%			of the grid cell
        	%MagVector:		Consisting of the Magnitudes which have
        	%			to be forecasted (the points are the in 
        	%			middle of the magnitude bin)
        	%DepthVector:		Consisting of the Depths which have to 
        	%			forecasted (only 3D) (the points are the  
        	%			in middle of the depth bin)
        	
        	
        	ConfigData = getModelConfig(obj)
        	%This method should return the configuration of the model the same 
        	%way it was entered before by ConfigureModel as a structure with
        	%the same field. Often this will be just a copy of the internal
        	%config structur, but it could be that the parameters are stored 
        	%different in the class. Also it is possible and may even desirable
        	%to add additional fields for none user configurated parameters 
        	%to the output structure, as this function will also be called
        	%to store the model configuration for reproducability.
        	
        	
        	FrameConfig = getFrameConfig(obj)
        	%Should return the two mentioned fields. If nothing is returned
        	%or empty fields the internal default of the framework will be
        	%used. This will work as well but is not user friendly. 
        	
        	
        	
        	RegionConfig = getTestRegionConfig(obj)
        	%Similar to the getModelConfig method this should return the 
        	%configuration of the testregion. This method will mostly be
        	%used for archiving purpose and thus fields missing in the list
        	%mentioned in ConfigureTestRegion may not be a problem, as well 
        	%as it is possible to add additional fields. But it is suggested
        	%to design the output of this method in a way that it would be
        	%possible to recreate the test region setting from it.
        	
        	
        	
        	PossibleParameters = getParameterSetting(obj)
        	%The method should return a description of the parameters needed
        	%for the model and should also include information about the way
        	%the gui for setting the parameters should be designed.
        	%The output has to be a structure with the field names being the 
        	%name of the parameters which should be inputed by ConfigureModel
        	%Each entry in structure should be another structure with following
        	%keywords:
        	%	Type: 		can be 	'check' for boolean,
        	%				'edit' for any value or small text
        	%				'list' for a selection of values specified
        	%					by Items
        	%				'text' for longer string boxes (e.g. Comments)
        	%
        	%	Format:		Defines the data type of value only applyable with
        	%			'edit'. It can be the following: 'string','float',
        	%			'integer','file','dir'	
        	%	ValLimit: 	The limit a value can have in a edit box, if left
        	%			empty it will set to [-9999 9999].
        	%	Name:		Sets the Name used for the value in the GUI, if 
        	%			left empty the variable name will be used instead
        	%	Items:		Only needed in case of 'list', there it has to be 
        	%			cell array with strings. The value returned will
        	%			the selected string.
        	%			There are also two keywords which can used (without
        	%			cell array, just a single keyword), one is 'datastore'
        	%			the other one is 'filterlist' and the two allow to 
        	%			select an additional datastore/filterlist from the 
        	%			currently in the commander loaded objects. This object
        	%			will be static and do not change with every calculation
        	%			
        	%	UserDataSelect: This can be used instead of 'Items' in the 'list'
        	%			entry type. It it allows to select previous, in
        	%			the Datastore catalog stored, calculations like 
        	%			for instance b-value, stress calculations and so 
        	%			on. The GUI will in this case automatically generate
        	%			the Items list, only thing needed is the field name 
        	%			under which the data is stored in the datastore 
        	%			userdata area (e.g. 'new-bval-calc' for b-value maps)
        	%			the command Datastore.getUserDataKeys can be used
        	%			to get the currently saved variables. The result
        	%			sended back will in this case be the name of the 
        	%			calculation (store in in the first column of the 
        	%			result cell array). 
        	%	DefaultValue:	Sets the value used as default value in the GUI.
        	%			If missing, 0 will be used as default or 1 for 
        	%			lists
        	%	Style:		Only applyable for the Type 'list', it sets the 
        	%			GUI element used to represent the list and it can
        	%			be:	
        	%				'listbox': a scrollable list for the selection
        	%				'popupmenu': a popup list 
        	%				'radiobutton': a block of checkerboxes
        	%				'togglebutton': a selection of buttons
        	%			If missing 'popupmenu' will be used. The last
        	%			two options are only suggest if not many elements
        	%			are in the list.
        	
        	
        	
        	
        	setInputData(obj,Datastore,Filterlist,StartDate)
        	%This method is used by the project calculator to send the input
        	%catalog data to the model. Sended will be a Datastore catalog 
        	%object and a Filterlist object or boolean vector giving the 
        	%selection of the earthquakes (islogical can be used to separate
        	%between the two cases).
        	%Everything what has to be done to bring the input catalog into 
        	%the right format has to be done here. Like conversion into a 
        	%file format readable by the model.
        	%StartDate is the start of the forecast in the datenum format, this
        	%allows a cleaner start time, the length of the forecast should be
        	%know before throught the region config
        	
        	
        	UsedData = getInputData(obj,OnlyFilter)
        	%This method will be used by the project calculator for archiving
		%the used data. The returned UsedData can have different format 
		%but suggested is zmap or shortcat based catalog or a datastore 
		%catalog (cutted!). If OnlyFilter is set to true, the UsedData 
		%variable should be a boolean vector giving the selected earthquakes
        	
        	
        	FeatureList = getFeatures(obj)
        	%This is used by the project builder to determine which parameters
        	%are available for the model. It influences mostly the region 
        	%setting, the way data is exchanged and some additional menus
        	%The Output should be a structure with specified fieldnames with 
        	%mostly boolean values.
        	%Following keywords are available:
        	%	MagDist:	Should be normally true, means forecasts
        	%			for different magnitude bins are available
        	%			-> Magnitude bins
        	%	DepthDist:	If set to true the model allows forecasts
        	%			for different depth bins
        	%			-> 3D model
        	%	RectGrid:	If true the model should allow different
        	%			grid spacing for Longitude and Latitude
        	%	TimeYear:	If false the project builder will use
        	%			days for time interval value and if true
        	%			years will be used.
        	%	Parallel:	If true the this GUI allows parallization
        	%			and the toggle will be sended by builder
        	%	HostMode:	Only for parallel mode, if set to true
        	%			builder is allows to use this object in
        	%			a parfor loop or similar. If false the
        	%			model deals itself the parallization
        	%	NoFiles:	If set to true the model does not produce
        	%			any files and thus they cannot be archived
        	%			The project builder will in this case not 
        	%			allow the option to archive files 
        	%	LogData:	If true Logdata is available not only in 
        	%			file form but also directly imported as
        	%			string or structure or whatever.
        	%	ForecastType:	This is not boolean value but string
        	%			It describes what distribution is 
        	%			returning. It can be 'Possion', 'NegBin'
        	%			'Custom' and 'Selectable'.
        	%			'Selectable' means that the model supports
        	%			more than on distribution. 
        	
        	%The project builder does also have a standard feature set in 
        	%case some parts of the feature list are not defined.
        	%The default is:
        	%	MagDist=true;
        	%	DepthHist=false;
        	%	RectGrid=false;
        	%	TimeYear=false;
        	%	Parallel=true;
        	%	HostMode=false;
        	%	NoFiles=false;
        	%	LogData=true;
        	%	ForecastType='Poisson';
        	%	AddonData=false;
        	%Maybe more later
        	
        	%IMPORTANT: if ForecastType is 'Selectable', a field with
        	%the name "ForecastType" has to be added to the Model Configuration
        	%describing the actual ForecastType, also a descripting for the 
        	%GUI has to be added allowing to change the parameter
        	
        	%TODO: May have to add stuff to this list
        	
        	
        	prepareCalc(obj)
        	%This method is called the project builder, and should do everything
        	%what is needed to run the forecast later. Often this method is 
        	%totally unnecessary as everything can be done be before or after 
        	%call of this method. But in some cases it might be a good idea 
        	%to separate some cpu heavy preparation from the configuration.
        	%If not needed the function can defined as a empty function.
        	%IMPORTANT: No catalog preparation should be done here, as the
        	%catalog data is sended by the project calculator. This function
        	%is the last function before a actual calculation run (called in
        	%the startCalc method of the calculator.
        	
        	
        	ErrorCode = calcForecast(obj)
        	%Names says it. If this method is called the calculation of the 
        	%forecast and according the configuration and catalog data should
        	%started and the result should be saved in this object as well as
        	%the path to the result file in case any is existing.
        	%The ErrorCode can be used to signalize errors, it is just an 
        	%integer. 0 means everthing worked fine, 1 means model is not
        	%configurated properly, 2 means catalog data is missing, 3 means 
        	%something with a external function did not work out and -1
        	%means a generic not specified error. Every other value can 
        	%used to specify special model specific errors. It is of course
        	%sensible to explain the error code somewhere.
        	%The error can be specified in the function getErrorList(obj), but 
        	%it is as often optional, a generic function is integrated anyway.
        	%Currently the function is not directly used, but can change very
        	%soon
        	
        	[ForeCast, DataMask] = getForecast(obj,WithGrid)
        	%This should return the calculated forecast. The Format which should
        	%be returned is depended on the ForecastType and on the input 
        	%parameter WithGrid. For 'Poisson' and 'NegBin' the output should be
        	%vector (Poisson) and a "twin" vector (NegBin) and if WithGrid is
		%set to true a full Relm Grid forecast should be outputed. In the
		%'Custom' mode the output will always be a cell, without WithGrid
		%the output should be a cell vector containing the single grid cells
		%discrete distribution, else it should be a cell with two elements
		%first one containing the Relmgrid, second one containing the 
		%forecast like mentioned before.
		%The DataMask is the last column in a typical Relm based forecast
		%and marks if a cell has been used/defined (1) or not (0). It can 
		%be left empty, in this case it is assumed that all nodes are used.
		%In case of a full WithGrid output it DataMask will be ignored and
		%the grid will be used instead
         	
        	
        	FilePath = getForecastFile(obj)
		%This method will only be called if NoFiles is false and if the 
		%option to archive the forecast files is switched on the project
		%object. 
		%It should return the path to the forecast file as a string. The 
		%project calculater will then copy the file to a specific folder
		%save the link to this file. If there is more then one 
        	%result file existing a Cell array with multiple paths (strings)
        	%can be used instead of a normal single path


		LogData = getCalcLog(obj)
		%This will be called if saving logs is activated and LogData is 
		%set to true in the FeatureList. The method should return a log 
		%of the calculation in a matlab common format and it should NOT
		%be a file link, because the log file will not be copied.
		%the easiest way to create the LogData is to just read the file 
		%into a string and return the string.
		
		
		LogFile = getCalcLogFile(obj)
        	%The project calculator will only use this method if NoFiles is
        	%set to false in the FeatureList and if the option to store log
        	%files is activated in the project. This method should return a
        	%file path(s) to the log file(s), the project builder will copy 
        	%the files and store the new path. If there is more then one 
        	%logfiles existing a Cell array with multiple paths (strings)
        	%can be used instead of a normal single path
		
        	
        	AddonData = getAddonData(obj)
		%This will be called if saving AddonData is activated and AddonData is 
		%set to true in the FeatureList. The method can return anything over 
		%this function, but the model has also to keep track itself in case
		%it is timevariing data
        	
        	
        	
        	ResetLastCalc(obj)
        	%This method should revert the plugin to the state before the 
        	%calculation of the forecast, so that new data can be sent and
        	%a new forecast can be calculated. This may involve things like 
        	%empty the catalog storage and and deleting some input and output
        	%files. But it might as well be just an empty function if no clean
        	%up is needed. 
        	%IMPORTANT: this method should revert the plugin to the state AFTER
        	%prepareCalc was called.
        	
        	
        	ResetFullCalc(obj)
        	%Similar to ResetLastCalc but this one should revert the plugin to
        	%state BEFORE prepareCalc. The function can be equal to ResetLastCalc
        	%but it should not cause an error because double execution of a command
        	%(e.g. delete of a file)
        	
        	
        	
        	
        	
        end
        
        
        methods
        	%some useful methods which can be used for the implementation of
        	%a forecast plugin
        	
        	function [RelmGrid MetaData] = importXMLGrid(obj,FileName)
        		%imports a CSEP xml formated grid
        		import mapseis.forecast.importCSEPGrid;
        		
        		fid = fopen(FileName);
        		inStr = fscanf(fid,'%c');
        		fclose(fid);
        		
        		GridStruct = importCSEPGrid(inStr,true,'-string');
        		
        		RelmGrid=GridStruct.TheGrid;
        		TagsMeta={'modelName','version','author','issueDate','forecastStartDate',...
        				'forecastEndDate','defaultMagBinDimension'}
        		
        		for i=1:numel(TagsMeta)
        			MetaData.(TagsMeta{i})=GridStruct.(TagsMeta{i});
        		
        		end
        		
        		
        		
        	end
        	
        	function setConfigID(obj)
        		RawChar='0123456789ABCDEF'; %Hex
        		obj.ConfigID=RawChar(randi(15,1,12)+1)
        		
        	end
        	
        	
        	function ErrorList = getErrorList(obj)
        		PurList=[0;1;2;3;-1];
        		isSave=[true;false;false;false;true];
        		Descript={	'no Error, everything fine';...
        				'Model not or not correctly configurated';...
        				'No catalog data defined';...
        				'External function did not work';...
        				'Generic error'}
        		
        		ErrorList={PurList,isSave,Descript};		
        	end
        	
        	
        	function ModelInfo = getModelInfo(obj)
        		%simple function for returning the ModelInfo which should
        		%be a string. This is a generic function which will just 
        		%return the internal modelinfo no matter if it is empty
        		%or not. A custom one can be build in case more options 
        		%are wanted or if the ModelInfo variable is not a string
        		
        		ModelInfo=obj.ModelInfo;
        		
        	end
        	
        	
        	
        	
        	function CPUTimeData = getCalcTime(obj)
        		%simple function for returning the CPUTimeData which should
        		%be a structure with the following fields:
        			%InputDataTime: time used for calculating the input
        			%		files (e.g. catalog, config file) 
        			%		for each calculation not preparation
        			%CalcTime: Time spent in the external or the actual 
        			%	   calculation
        			%ImportTime: Time needed for importing the data
        			%PrepareTime: (Optional) can be added to measure the
        			%	      time needed for the calculation preparation
        			%	      the calculator will not need it as it can
        			%	      measure itself.
        			%AddTimeInfo: Additional timer info (optional)
        			
        			
        		%This is a generic function which will just 
        		%return the internal CPUTimeData no matter if it is empty
        		%or not. Empty CPUTimeData will be ignored by the calculator
        		%so it is optional
        		
        		CPUTimeData=obj.CPUTimeData;
        	end
        	
        end
        
        
        
        
end        

function Magnitudes = getMagnitudes(obj)
	%simple getter function, as the fields are the moment
	%public it would not be really needed, but it may needed
	%later, either because the fields will not be public or 
	%because some addition functionality is needed.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if obj.Dimension.Mag
		Magnitudes = obj.MagBins;
	else
		Magnitudes = [];
	end

end
function TheGrid = getFullGrid(obj)
	%returns as empty RELM type grid with column 9 being the
	%id and as a bonus column 10 being the mask. The mask is 
	%bonus as it could be varying anyway, maybe ignore in
	%most cases
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if ~obj.Ready
		error('No Grid defined');
	end
	
	if ~isempty(obj.DataMask)
		if obj.EqualMask
			TheMask=obj.DataMask;
		else
			%use the first
			TheMask=obj.DataMask{1};
		end

	else
		TheMask=ones(size(obj.TheGrid(:,1)));	
	
	end
		
	TheGrid=[obj.TheGrid,obj.NodeIDs,TheMask];

end
function NewObj = cloneWars(obj)
	%generates a clone of the existing object, equal in every
	%property but with a different ID
	%Name
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.forecast.*;
	
	NewObj= ForecastObj(obj.Name);
	Props2Copy = {'Version','EmptyGrid','Ready','TheGrid','NodeIDs',...
				'GridNodes','AxisVectors','GridSpacings','GridLimits','GridPrecision',...
				'MagBins','DepthBins','RegionPoly','BoundBox','TimeVector','TimeSpacing',...
				'RealTimeMode','Dimension','SelectedNodes','SelectedIDs',...
				'JumpValue','ForecastData','ExpectedValue','OverallDistro',...
				'ForecastType','DataMask','EqualMask','GenericMask','TheBag',...
				'ForecastReady'};
	
	for i=1:numel(Props2Copy)
		NewObj.(Props2Copy{i})=obj.(Props2Copy{i});
	end
	
end
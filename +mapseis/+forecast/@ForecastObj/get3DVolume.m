function [ForeVolume ForeTime ForeLength] = get3DVolume(obj,CastTime,Magnitude,Depth)
	%same as get2DSlice, but for Volumes, at least 3D dimensions (without time) 
	%are needed this function to work
	
	%Caution some of the stuff is experimental and it is not sure if it works in every
	%case. For instace could the depth summation fail in  cases the region is not
	%cylindrical (which isn't allowed right now anyway)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if ~obj.Dimension.Space&(~obj.Dimension.Depth|~obj.Dimension.Mag)
		%missing a necessary dimension
		error('Not enough dimension available'); 
	end
	
	
	%search if the forecast can be found
	if obj.EmptyGrid
		error('No forecast set');
	end
	
	if obj.Dimension.Time
		%time depended
		FindInTime=obj.TimeVector==CastTime;
		
		if ~any(FindInTime)
			%get the next lower time
			ModTimeVec=obj.TimeVector-CastTime;
			SmallerZero=find(ModTimeVec<0);
			
			if isempty(SmallerZero)
				error('No forecast before this time available');
			end
			
			FindInTime=SmallerZero(end);
		
		end
	
	else
		FindInTime=1;
	end
	
	%just in case
	if strcmp(obj.ForecastType.Type,'None')
		error('No forecast set')
	end

	RawGrid=obj.ForecastData{FindInTime};
	ForeTime=obj.TimeVector(FindInTime);
	ForeLength=obj.TimeSpacing(FindInTime);
		
	%use the masking if needed
	if ~isempty(obj.DataMask)
		if obj.EqualMask
			TheMask=obj.DataMask;
		
		else
			TheMask=obj.DataMask{FindInTime};
		end
		
		RawGrid = MatrixCorrector(obj,RawGrid,~TheMask,'set');
	end
	
	try
		[ForeSlice ForeVolume] = BuildRawGrid(obj,true)
	catch
		%probalby 2D
		[ForeSlice ForeVolume] = BuildRawGrid(obj,false)
	end
	
	if obj.Dimension.Depth&obj.Dimension.Mag
		%full 4D case
		BreakCond = 	(isempty(Depth)&isempty(Magnitude))|...
						(~isempty(Depth)&~isempty(Magnitude));
		
		%possible errors
		if BreakCond
			error('Wrong Depth and/or Magnitude parameter')
		end
		
		if strcmp(Depth,'all')&strcmp(Magnitude,'all') 
			error('only on parameter can be set to "all"');
		end
	
	
		%different cases (summation over one dimension or a specific slide)
		if strcmp(Depth,'all')
			ForeVolume=[];
			
			%again to prevent unnecessary zeros
			Tempslice=ForeSlice;
			NewSelID=obj.SelectedIDs{1}{1};
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			
			Tempslice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
			%MrNan=isnan(Tempslice);
			MrNan = MatrixCorrector(obj,Tempslice,[],'get');
			%ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
			[ForeSlice RawVolume] = BuildRawGrid(obj,true,true);
			
			%all depth have to be summed and the new "depth" is the magnitude
			for i=1:numel(obj.MagBins)
				DepthSlice=ForeSlice;

				%first step (again the slighty annoying extra step has to be done)
				NewSelID=obj.SelectedIDs{1}{1}+(i-1);
				DepthSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
		
				for j=2:numel(obj.DepthBins)
					NewSlice=ForeSlice;
					NewSelID=obj.SelectedIDs{1}{j}+(i-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					NewSlice(obj.SelectedNodes{1}{j})=RawGrid(NewSelID);
					DepthSlice=SumFullDistro(obj,DepthSlice,NewSlice);
				
				end
	
				%reapply nan
				%DepthSlice(MrNan)=NaN;
				DepthSlice=MatrixCorrector(DepthSlice,MrNan,'set');
				
				%save in in 3D matrix
				ForeVolume(:,:,i)=DepthSlice;

			end

		elseif strcmp(Magnitude,'all') 
			%One of the real 3D cases
			
			%It has to be done a bit more complicated than I originally did think
			%because using a matrix with logical matrix results in a matrix, expect
			%you write an other matrix into it (sounds wierd I know)
			RawVol=ForeVolume;
			
			%first step
			NewSelID=obj.SelectedIDs{2};
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
			
			for i=2:numel(obj.MagBins)
				NewSlide=RawVol;
				NewSelID=obj.SelectedIDs{2}+(i-1);
				
				%only take Ids which are not nan
				NotNaN=~isnan(NewSelID);
				NewSelID=NewSelID(NotNaN);
				
				NewSlide(obj.SelectedNodes{2})=RawGrid(NewSelID);
				
				%now sum up the single volumes without any selector
				ForeVolume=SumFullDistro(obj,ForeVolume,NewSlide);
				
			end
	
	
		elseif ~isempty(Depth)
			%Similar to the all case but with a specified depth
			ForeVolume=[];
			DepthPos=find(obj.DepthBins==Depth);
			
			if isempty(DepthPos)
				error('Depth not found');
			end
			
			%get the magnitude slices
			for i=1:numel(obj.MagBins)
				DepthSlice=ForeSlice;
				
				NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1)
				
				%only take Ids which are not nan
				NotNaN=~isnan(NewSelID);
				NewSelID=NewSelID(NotNaN);
				
				DepthSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
				
				%save in in 3D matrix
				ForeVolume(:,:,i)=DepthSlice;

			end
		
		elseif ~isempty(Magnitude)
			MagPos=find(obj.MagBins==Magnitude);
			
			if isempty(MagPos)
				error('Magnitude not found');
			end
			
			NewSelID=obj.SelectedIDs{2}+(MagPos-1);
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
		end

	elseif obj.Dimension.Depth&~obj.Dimension.Mag
		%"real" 3D case with depth,in this case no Depth and Magnitude
		%input is "allowed" (means it will be ignored)
		if ~isempty(Depth)&~isempty(Magnitude)
			disp('Depth and Magnitude input ignored');
			warning('Depth and Magnitude input ignored');
		end
		
		NewSelID=obj.SelectedIDs{2};
		
		%only take Ids which are not nan
		NotNaN=~isnan(NewSelID);
		NewSelID=NewSelID(NotNaN);
		
		ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
			
	elseif ~obj.Dimension.Depth&obj.Dimension.Mag
		%Magnitude as 3rd dimension,in this case no Depth and Magnitude
		%input is "allowed" (means it will be ignored)
		if ~isempty(Depth)&~isempty(Magnitude)
		disp('Depth and Magnitude input ignored');
		warning('Depth and Magnitude input ignored');
		end
		
		ForeVolume=[];

		%get the magnitude slices
		for i=1:numel(obj.MagBins)
			DepthSlice=ForeSlice;
			disp(i)
			NewSelID=obj.SelectedIDs+(i-1);
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			DepthSlice(obj.SelectedNodes)=RawGrid(NewSelID);
			
			%save in in 3D matrix
			ForeVolume(:,:,i)=DepthSlice;
		
		end
	
	end
	
end
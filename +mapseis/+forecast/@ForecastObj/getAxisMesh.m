function [SpaceGrid DepthGrid  MagGrid] = getAxisMesh(obj)
	%similar to getAxisVectors, but it generate a meshed grid
	%for the axis, which is needed by some plot functions
	%All outputs are cell arrays with the needed grids in them
	%SpaceGrid: {Lon2D Lat2D}
	%DepthGrid: {Lon3D Lat3D Depth}
	%MagGrid: {Lon3D Lat3D Mag}
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%defaults
	SpaceGrid  = {};
	DepthGrid = {};
	MagGrid = {};
	
	%check what is available
	if obj.Dimension.Space
		LonVec=obj.AxisVectors{1}+obj.GridSpacings(1)/2;
		LatVec=obj.AxisVectors{2}+obj.GridSpacings(2)/2;
		[SpaceGrid{1} SpaceGrid{2}] = meshgrid(LonVec,LatVec);			
	end
	
	if obj.Dimension.Depth&obj.Dimension.Space
		DepthVec=obj.DepthBins;
		[DepthGrid{1} DepthGrid{2} DepthGrid{3}] = meshgrid(LonVec,LatVec,DepthVec);	
	end
	
	if obj.Dimension.Mag&obj.Dimension.Space
		MagVec=obj.MagBins;
		[MagGrid{1} MagGrid{2} MagGrid{3}] = meshgrid(LonVec,LatVec,MagVec);
	end

end
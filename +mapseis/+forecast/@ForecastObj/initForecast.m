function initForecast(obj,ForecastType,ForecastMethod)
	%inits a forecast to a certain format, has to be run once
	%before adding the first forecast.
	%At the moment only 'Poisson' and 'Custom' is allowed
	
	%Custom format info:
	%for the Custom format the format is a cell vector with 
	%vectors of probability. Each element i in the vector gives
	%the probabilty that i-1 earthquakes happen in the grid cell
	
	%e.g.
	%eq:	   0	1	2	3	4	 
	%
	%	{ [0.5 	0.25 	0.25];
	%	  [0.2 	0.2 	0.2 	0.15 	0.15];
	%	   ...
	%	   ...
	%	  [0.9 	0.1]}	 
	
	%The sum of all probabilties in each vector should sum to
	%one, if this is not the case a warning will be displayed
	%(It should be still possible to continue with the calculation) 
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if nargin<2
		ForecastMethod={};
	end
	
	obj.ForecastType.Type=ForecastType;
	obj.ForecastType.Method=ForecastMethod;
	
	obj.ForecastReady=true;
	
	
end
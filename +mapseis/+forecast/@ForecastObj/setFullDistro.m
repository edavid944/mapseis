function [OverallDistro ExpVal] = setFullDistro(obj,ForecastData)
	%produces the CombindeDistro (or sum of all rates for 
	%Poisson) together with the expected value. 


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	import mapseis.util.stat.CombineDiscretDistro;
	
	OverallDistro=[]; 
	ExpVal=[];
	
	switch obj.ForecastType.Type
		case 'Poisson'
			OverallDistro=nansum(ForecastData);
			ExpVal=OverallDistro;
		
		case 'Custom'
			OverallDistro=1;
			
			for i=1:numel(ForecastData)
				OverallDistro=CombineDiscretDistro(OverallDistro,ForecastData{i},true);
			end
			
			ExpVal=sum((0:1:numel(OverallDistro)-1)'.*OverallDistro);
			
		case 'NegBin'
			ExpVal=nansum(((1-ForecastData(:,2)).*ForecastData(:,1))./ForecastData(:,2));
			OverallDistro=ExpVal;
	end	
		
end
function setRealTimeMode(obj, ModeToogle)
	%sets the realtime mode (allowing overlaping forecast)
	%it gives a warning if already two forecasts are in the 
	%object.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if numel(obj.TimeVector)>=2
		warning('There are already two forecast in this object');
	end
	
	obj.RealTimeMode=ModeToogle;
	
end

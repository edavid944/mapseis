function [OverallDistro ExpVal] = getTotalRate(obj,CastTime)
	%returns the overall rate or distribution of the forecast
	%simple for Poisson, but not for a custom distribution
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if obj.EmptyGrid
		error('No forecast set');
	end
	
	if obj.Dimension.Time
		%time depended
		FindInTime=obj.TimeVector==CastTime;
		
			if ~any(FindInTime)
				%get the next lower time
				ModTimeVec=obj.TimeVector-CastTime;
				SmallerZero=find(ModTimeVec<0);
				
				if isempty(SmallerZero)
					error('No forecast before this time available');
				end
				
				FindInTime=SmallerZero(end);
		
		end
	
	else
		FindInTime=1;
	
	end
	
	if strcmp(obj.ForecastType.Type,'None')
		error('No forecast set')
	end
	
	OverallDistro = obj.OverallDistro{FindInTime};
	ExpVal =  obj.ExpectedValue(FindInTime);

end	
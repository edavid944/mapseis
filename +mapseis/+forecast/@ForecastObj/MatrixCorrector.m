function ResMat = MatrixCorrector(obj,inMat,LogicArray,WorkMode)
	%This function is meant to replace the isnan, set to
	%nan block in the functions to make it compatible with 
	%the custom distributions
	%LogicArray can be empty for 'get'
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.emptier;
	
	ResMat=[];
	
	if strcmp(WorkMode,'get')
		switch obj.ForecastType.Type
			case 'Poisson'
				ResMat=isnan(inMat);
			
			case 'Custom'
				ResMat=emptier(inMat);				
			
			case 'NegBin'
				%currently not supported
			
		end	
	
	elseif strcmp(WorkMode,'set')
		switch obj.ForecastType.Type
			case 'Poisson'
				ResMat=inMat;
				ResMat(LogicArray)=NaN;
				
			case 'Custom'
				ResMat=inMat;
				ResMat(LogicArray)=cell(sum(sum(LogicArray)),1);				
				
			case 'NegBin'
				%currently not supported
			
		end	
		
	else
		disp('Please select correct workmode');
	
	end	

end
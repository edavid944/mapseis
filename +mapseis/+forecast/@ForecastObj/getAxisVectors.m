function [LonVec LatVec DepthVec MagVec] = getAxisVectors(obj)
	%returns the vectors for all possible axis available
	%all coordinates give the middle of the cell not the 
	%corners
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%defaults
	LonVec = [];
	LatVec = [];
	DepthVec = [];
	MagVec = [];

	%check what is available
	if obj.Dimension.Space
		LonVec=obj.AxisVectors{1}+obj.GridSpacings(1)/2;
		LatVec=obj.AxisVectors{2}+obj.GridSpacings(2)/2;
	end
	
	if obj.Dimension.Depth
		DepthVec=obj.DepthBins;
	end
	
	if obj.Dimension.Mag
		MagVec=obj.MagBins;
	end

end
function removeForecast(obj,CastTime)
	%removes a forecast from the list if found.


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if isempty(obj.TimeVector)
		error('No forecasts saved')
	end
	
	oldPos=find(obj.TimeVector==CastTime);
	
	if isempty(oldPos)
		error('No Forecast with this date found');
	end
	
	if numel(oldPos)>1
		%this should never happen
		error('More than one suitable forecast found')
	end
	
	%special case: forecast empty after removal
	if numel(obj.TimeVector)==1
		disp('Last forecast deleted')
		obj.TimeVector=[];
		obj.TimeSpacing=[];
		obj.ForecastData={};
		obj.EmptyGrid=true;
		obj.DataMask=[];
		obj.EqualMask=true;
		obj.GenericMask=true;
		obj.OverallDistro={};
		obj.ExpectedValue=[];
		
	else
		NewTimeVec=[obj.TimeVector(1:oldPos-1),obj.TimeVector(oldPos+1:end)];
		NewSpacing=[obj.TimeSpacing(1:oldPos-1),obj.TimeSpacing(oldPos+1:end)];
		obj.TimeVector=NewTimeVec;
		obj.TimeSpacing=NewSpacing;
		NewCaster={obj.ForecastData{1:oldPos-1},obj.ForecastData{oldPos+1:end}};
		obj.ForecastData=NewCaster;
		
		if numel(obj.TimeVector)>=2
			obj.Dimension.Time=true;
		else
			obj.Dimension.Time=false;
		end
		
		if ~obj.EqualMask
			NewMaskVec={obj.DataMask{1:oldPos-1},obj.DataMask{oldPos+1:end}};
			obj.DataMask=NewMaskVec;
		end
	
		NewOverallDistro={obj.OverallDistro{1:oldPos-1},obj.OverallDistro{oldPos+1:end}};
		obj.DataMask=NewOverallDistro;
		NewExpVec=[obj.ExpectedValue(1:oldPos-1),obj.ExpectedValue(oldPos+1:end)];
		obj.DataMask=NewExpVec;
		
	end
	
	%check what is present:
	if ~isempty(obj.ForecastData)
		obj.EmptyGrid=false;
	else
		obj.EmptyGrid=true;
	end

end	
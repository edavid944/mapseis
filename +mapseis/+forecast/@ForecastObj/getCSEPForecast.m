function [CSEPGrid ForeTime ForeLength]= getCSEPForecast(obj,CastTime)
	%Returns a CSEP grid for a certain time, if the time is
	%not existing the nearest lower time is used instead
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	if obj.EmptyGrid
		error('No forecast set');
	end
	
	if obj.Dimension.Time
		%time depended
		FindInTime=obj.TimeVector==CastTime;
		
		if ~any(FindInTime)
			%get the next lower time
			ModTimeVec=obj.TimeVector-CastTime;
			SmallerZero=find(ModTimeVec<0);
		
			if isempty(SmallerZero)
				error('No forecast before this time available');
			end
	
			FindInTime=SmallerZero(end);
		end
	
	else
		FindInTime=1;
	end
	
	
	
	if obj.EqualMask
		TheMask=obj.DataMask;
	else
		TheMask=obj.DataMask{FindInTime};
	end
	
	
	switch obj.ForecastType.Type
		case 'Poisson'
			CSEPGrid=[obj.TheGrid(:,1:8),obj.ForecastData{FindInTime},TheMask];
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);
		
		case 'NegBin'
			CSEPGrid=[obj.TheGrid(:,1:8),obj.ForecastData{FindInTime}(:,1),TheMask,...
			obj.ForecastData{FindInTime}(:,2)];
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);

		case 'Custom'
			if isempty(obj.NodeIDs)
				obj.NodeIDs=(1:numel(obj.TheGrid(:,1)))';
			end	
			CSEPGrid={[obj.TheGrid,obj.NodeIDs,TheMask],obj.ForecastData{FindInTime}};
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);	
			
		case 'None'
			error('No forecast set')
	end	
	
end
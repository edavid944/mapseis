function addGrid(obj,TheGrid,TripleGrid)
	%Allows to add a Grid to the object, this can also be a previous used Grid
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if nargin<3
		TripleGrid=[];
	end
	
	if isempty(TripleGrid)
		%generate a TripleGrid
		ElementSelector=find(TheGrid(1,5)==TheGrid(:,5)&...
		TheGrid(1,7)==TheGrid(:,7));
		
		TripleGrid(:,1)=(TheGrid(ElementSelector,1)+TheGrid(ElementSelector,2))/2;
		TripleGrid(:,2)=(TheGrid(ElementSelector,3)+TheGrid(ElementSelector,4))/2;
	end
	
	%Check if somebody left something behind ;-)
	%maybe seems funny to put TripleGrid not through here, but
	%TripleGrid is a Result from a previous calc and not a input 
	%parameter (a bit picky maybe, yes);
	
	%The rounding of the calculated parameters is needed, and according
	%to the same rounding the grid generator does, if it is not done
	%the values will jitter
	if ~isempty(obj.TheBag)
		GridSpacing=obj.TheBag{1};
		MagSpace=obj.TheBag{2};
		DepthSpace=obj.TheBag{3};
		
		%empty the bag
		obj.TheBag={};

	else
		%Stuff missing, so it has to be generated
		GridSpacing(1)=TheGrid(1,2)-TheGrid(1,1);
		GridSpacing(2)=TheGrid(1,4)-TheGrid(1,3);
		
		%rounding is needed, to prevent jitter 
		GridSpacing(1)=round(GridSpacing(1)*(1/obj.GridPrecision))*obj.GridPrecision;
		GridSpacing(2)=round(GridSpacing(2)*(1/obj.GridPrecision))*obj.GridPrecision;
		
		
			if all(TheGrid(1,5)==TheGrid(:,5))&all(TheGrid(1,6)==TheGrid(:,6))
				DepthSpace(1)=(min(TheGrid(:,6))+min(TheGrid(:,5)))/2;
				DepthSpace(2)=(max(TheGrid(:,6))+max(TheGrid(:,5)))/2;
				DepthSpace=round(DepthSpace*10^4)/10^4;
			
			else
				DepthSpace(1)=(min(TheGrid(:,6))+min(TheGrid(:,5)))/2;
				DepthSpace(2)=(max(TheGrid(:,6))+max(TheGrid(:,5)))/2;
				DepthSpace(3)=(TheGrid(1,6)-TheGrid(1,5));
				DepthSpace=round(DepthSpace*10^4)/10^4;
			end
			
			if all(TheGrid(1,7)==TheGrid(:,7))&all(TheGrid(1,8)==TheGrid(:,8))
				MagSpace(1)=(min(TheGrid(:,8))+min(TheGrid(:,7)))/2;
				MagSpace(2)=(max(TheGrid(:,8))+max(TheGrid(:,7)))/2;
				MagSpace=round(MagSpace*10^3)/10^3;

			else
				MagSpace(1)=(min(TheGrid(:,8))+min(TheGrid(:,7)))/2;
				MagSpace(2)=(max(TheGrid(:,8))+max(TheGrid(:,7)))/2;
				MagSpace(3)=(TheGrid(1,8)-TheGrid(1,7));
				MagSpace=round(MagSpace*10^3)/10^3;
			end
	end
	
	
	%add IDs to the TheGrid and add to the object
	TheGrid=TheGrid(:,1:8);
	TheGrid(:,9)=(1:numel(TheGrid(:,1)))';
	obj.TheGrid=TheGrid;
	
	%generate a generic mask (everything used)
	obj.DataMask=true(size(TheGrid(:,8)));
	obj.EqualMask=true;
	obj.GenericMask=true;
	
	%Add Spacings
	obj.GridSpacings=GridSpacing;
	
	%Decide which Dimensions have to be set on and add Spacing if needed.
	obj.Dimension.Space=true;
	
	if numel(DepthSpace)==3
		obj.Dimension.Depth=true;
		obj.GridSpacings(3)=DepthSpace(3);
	else
		obj.Dimension.Depth=false;
	end
	
	if numel(MagSpace)==3
		obj.Dimension.Mag=true;
		obj.GridSpacings(4)=MagSpace(3);
	else
		obj.Dimension.Mag=false;
	end
	
	%Add the Ranges (min and max including grid spacing
	minLon=min(obj.TheGrid(:,1));
	maxLon=max(obj.TheGrid(:,2));
	minLat=min(obj.TheGrid(:,3));
	maxLat=max(obj.TheGrid(:,4));
	minDepth=min(obj.TheGrid(:,5));
	maxDepth=max(obj.TheGrid(:,6));
	minMag=min(obj.TheGrid(:,7));
	maxMag=max(obj.TheGrid(:,8));
	
	obj.GridLimits=[	minLon,minLat,minDepth,minMag;
	maxLon,maxLat,maxDepth,maxMag];
	
	
	%add available values (always the middle point of the node)
	obj.GridNodes=TripleGrid;
	obj.DepthBins=(unique(obj.TheGrid(:,5))+unique(obj.TheGrid(:,6)))/2;
	obj.MagBins=(unique(obj.TheGrid(:,7))+unique(obj.TheGrid(:,8)))/2;
	
	%round the bins
	obj.DepthBins=round(obj.DepthBins*10^4)/10^4;
	obj.MagBins=round(obj.MagBins*10^3)/10^3;
	
	%build vectors
	obj.AxisVectors{1}=unique(obj.GridNodes(:,1));
	obj.AxisVectors{2}=unique(obj.GridNodes(:,2));
	
	%The vectors for Depth and Magnitude are at the moment unnecessary
	%as the grid does not support free 3D or 4D shapes. If this needed
	%late it can be added easiely
	
	%build selection matrix (needed to build fast maps)
	if obj.Dimension.Depth
		%3D mode with depth
		%find how many bins are between the spacial cells
		Temp=find(obj.TheGrid(1,7)==obj.TheGrid(:,7));
		SpaceBetween3D=Temp(2)-Temp(1);
		
		Temp=find(obj.TheGrid(1,5)==obj.TheGrid(:,5)&...
		obj.TheGrid(1,7)==obj.TheGrid(:,7));
		SpaceBetween2D=Temp(2)-Temp(1);
		
		LowMag=obj.TheGrid(1,7);
		AltDepthVec=obj.TheGrid(1:SpaceBetween3D:SpaceBetween2D,5);
		
		%For 2D Slices
		[XX,YY] = meshgrid(obj.AxisVectors{1},obj.AxisVectors{2});
		RawSelector2D = {};
		RawNrSel2D = {};
		TempSel2D = false(size(XX));
		TempNrSel2D = NaN(size(XX));	
		for k=1:numel(AltDepthVec);
			RawSelector2D{k} = false(size(XX));
			RawNrSel2D{k} = NaN(size(XX));	
		end
		
		%For 3D Volumes
		[XXX,YYY,ZZZ] = meshgrid(obj.AxisVectors{1},obj.AxisVectors{2},AltDepthVec);
		
		RawSelector3D = false(size(XXX));
		RawNrSel3D = NaN(size(XXX));
		
		
		for i=1:length(obj.GridNodes);
			%Full 3D Slices
			for j=1:numel(AltDepthVec)
				Found3D=(XXX==obj.GridNodes(i,1))&(YYY==obj.GridNodes(i,2)&(ZZZ==AltDepthVec(j)));
				RawSelector3D(Found3D)=true;
				RawNrSel3D(Found3D)=1+(i-1)*SpaceBetween2D+(j-1)*SpaceBetween3D;
			end	
		
			%2D Slices
			Found2D=(XX==obj.GridNodes(i,1))&(YY==obj.GridNodes(i,2));
			TempSel2D(Found2D)=true;
			TempNrSel2D(Found2D)=1+(i-1)*SpaceBetween2D;
		end
		
		%generate 2D slices
		for j=1:numel(AltDepthVec)
			RawSelector2D{j}=TempSel2D;
			RawNrSel2D{j}=TempNrSel2D+(j-1)*SpaceBetween3D;
		end
		
		%Now pack it all into the objects fields
		obj.SelectedNodes={RawSelector2D,RawSelector3D};
		obj.SelectedIDs={RawNrSel2D,RawNrSel3D};
		obj.JumpValue=[SpaceBetween2D,SpaceBetween3D];
		
		
	else
		%2D mode
		
		%find how many bins are between the spacial cells
		Temp=find(obj.TheGrid(1,5)==obj.TheGrid(:,5)&...
		obj.TheGrid(1,7)==obj.TheGrid(:,7));
		SpaceBetween=Temp(2)-Temp(1);
		
		[XX,YY] = meshgrid(obj.AxisVectors{1},obj.AxisVectors{2});
		RawSelector = false(size(XX));
		RawNrSel = NaN(size(XX));

		for i=1:length(obj.GridNodes);
			RawSelector((XX==obj.GridNodes(i,1))&(YY==obj.GridNodes(i,2)))=true;
			RawNrSel((XX==obj.GridNodes(i,1))&(YY==obj.GridNodes(i,2)))=1+(i-1)*SpaceBetween;
		end
		
		obj.SelectedNodes=RawSelector;
		obj.SelectedIDs=RawNrSel;
		obj.JumpValue=SpaceBetween;

	end
	
	%Now we're ready
	obj.Ready=true;

end
function [ForeSlice ForeTime ForeLength] = get2DSlice(obj,CastTime,Magnitude,Depth)
	%returns a slice of the forecast as 2D matrix.
	%Magnitude and/or Depth can be left empty if the grid has only one 
	%bin for one of them.
	%For Magnitude and Depth also the keyword 'all' can be used, in this 
	%case a summation of all grid cells in that dimension will be returned.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%search if the forecast can be found
	if obj.EmptyGrid
		error('No forecast set');
	end
	
	if obj.Dimension.Time
		%time depended
		FindInTime=obj.TimeVector==CastTime;
		
		if ~any(FindInTime)
			%get the next lower time
			ModTimeVec=obj.TimeVector-CastTime;
			SmallerZero=find(ModTimeVec<0);
			
			if isempty(SmallerZero)
				error('No forecast before this time available');
			end

			FindInTime=SmallerZero(end);
		
		end
	
	else
		FindInTime=1;
	
	end
	
	%condition for 3D (either Mag or Depth but not both)
	Cond3d=(obj.Dimension.Depth | obj.Dimension.Mag) & ~(obj.Dimension.Depth & obj.Dimension.Mag);
	%condition to stop and return an error
	BreakCond=(obj.Dimension.Depth&isempty(Depth))|(obj.Dimension.Mag&isempty(Magnitude)); 
	
	if strcmp(obj.ForecastType.Type,'None')
		error('No forecast set')
	end
	
	%Maybe possible to simplify this a bit

	RawGrid=obj.ForecastData{FindInTime};
	ForeTime=obj.TimeVector(FindInTime);
	ForeLength=obj.TimeSpacing(FindInTime);
	
	%use the masking if needed
	if ~isempty(obj.DataMask)
		if obj.EqualMask
			TheMask=obj.DataMask;
			
		else
			TheMask=obj.DataMask{FindInTime};
		end
	
		RawGrid = MatrixCorrector(obj,RawGrid,~TheMask,'set');
	end
	
	
	if ~obj.Dimension.Depth&~obj.Dimension.Mag
		%only 2D data, the easiest case
		
		%ForeSlice=NaN(size(obj.SelectedNodes));
		[ForeSlice RawVolume] = BuildRawGrid(obj,false);
		ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
		
	elseif Cond3d
		%3D case
		if BreakCond
			error('Depth or Mag input variable not defined')
		end
	
		NewSelID=obj.SelectedIDs;
		
		%only take Ids which are not nan
		NotNaN=~isnan(NewSelID);
		NewSelID=NewSelID(NotNaN);
		%It should work like this, but 
		%maybe it could lead to some problems
		%if the parts containing nan are not
		%removed in the result (will leave it
		%for now)
		
		
		if obj.Dimension.Depth
			if strcmp(Depth,'all')
				%sum up
				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice RawVolume] = BuildRawGrid(obj,false);
				RawSlice=ForeSlice;
								
				%first step
				ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID);
					
				%additonal ones
				for i=2:numel(obj.DepthBins)
					NewSlice=RawSlice;
					NewSlice(obj.SelectedNodes)=RawGrid(NewSelID+(i-1))
					ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
					
				end

			else
				%single field
				SliPos=find(obj.DepthBins==Depth);
	
				if isempty(SliPos)
					error('Depth not found');
				end
		
				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice RawVolume] = BuildRawGrid(obj,false);
				ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID+(SliPos-1));			
			
			end	
	
		elseif obj.Dimension.Mag
			if strcmp(Magnitude,'all')
				%sum up
				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice RawVolume] = BuildRawGrid(obj,false);
				RawSlice=ForeSlice;
				
				%first step
				ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID);
				
				%additonal ones
				for i=2:numel(obj.MagBins)
					NewSlice=RawSlice;
					NewSlice(obj.SelectedNodes)=RawGrid(NewSelID+(i-1));
					ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
				end
	
			else
				%single field
				SliPos=find(obj.MagBins==Magnitude);
				
				if isempty(SliPos)
					error('Magnitude not found');
				end
				
				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice RawVolume] = BuildRawGrid(obj,false);
				ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID+(SliPos-1));
		
			end
		end
	
	else
		%full 4D (most difficult case with two jumps
		%ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
		[ForeSlice RawVolume] = BuildRawGrid(obj,true);
		
		if BreakCond
			error('Depth or Mag input variable not defined')
		end
		
		
		if ~strcmp(Depth,'all')&~strcmp(Magnitude,'all')
			%no summation needed
			MagPos=find(obj.MagBins==Magnitude);
			
			if isempty(MagPos)
				error('Magnitude not found');
			end
			
			DepthPos=find(obj.DepthBins==Depth);
			
			if isempty(DepthPos)
				error('Depth not found');
			end
			
			NewSelID=obj.SelectedIDs{1}{DepthPos}+(MagPos-1);
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
		
		elseif strcmp(Depth,'all')&~strcmp(Magnitude,'all')
			%sumamation of the depth needed
			MagPos=find(obj.MagBins==Magnitude);
			
			if isempty(MagPos)
				error('Magnitude not found');
			end
			
			%Dummy slice
			RawSlice=ForeSlice;
			
			%first element to prevent zeros
			NewSelID=obj.SelectedIDs{1}{1}+(MagPos-1);
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
			
			%now the rest
			for i=2:numel(obj.DepthBins)
				NewSelID=obj.SelectedIDs{1}{i}+(MagPos-1);
				
				%only take Ids which are not nan
				NotNaN=~isnan(NewSelID);
				NewSelID=NewSelID(NotNaN);
				
				%again the annoying substep
				NewSlice=RawSlice;
				NewSlice(obj.SelectedNodes{1}{i})=RawGrid(NewSelID);
				ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
			end
			
		elseif ~strcmp(Depth,'all')&strcmp(Magnitude,'all')
			%summation of the magnitude
			DepthPos=find(obj.DepthBins==Depth);
			
			if isempty(DepthPos)
				error('Depth not found');
			end
			
			%Dummy slice
			RawSlice=ForeSlice;
			
			%first element to prevent zeros
			NewSelID=obj.SelectedIDs{1}{DepthPos};
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
			
			%now the rest
			for i=2:numel(obj.MagBins)
				NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1);
				
				%again the annoying substep
				NewSlice=RawSlice;
				NewSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
				ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
			end
	
		else
			%summation of magnitude and depth
			%first do a prototype slice
			NewSelID=obj.SelectedIDs{1}{1};
			
			%only take Ids which are not nan
			NotNaN=~isnan(NewSelID);
			NewSelID=NewSelID(NotNaN);
			
			ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
			MrNan = MatrixCorrector(obj,ForeSlice,[],'get');
			%MrNan=isnan(ForeSlice);
			
			%make a new Sum slice with zeros
			ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
			RawSlice=ForeSlice;
			
			%now sum them all
			for d=1:numel(obj.DepthBins)
				for m=1:numel(obj.MagBins)
					NewSelID=obj.SelectedIDs{1}{d}+(m-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					NewSlice=RawSlice;
					%again the annoying substep
					NewSlice(obj.SelectedNodes{1}{d})=RawGrid(NewSelID);
					ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);	
				
				end
			end
	
			%Kill all zeros which should not be existing
			%ForeSlice(MrNan)=NaN;
			ForeSlice=MatrixCorrector(obj,ForeSlice,MrNaN,'set');
	
		end
	
	end
		
end
function importCSEPForecast(obj,RelmGrid,CastTime,CastLength,ForecastType)
	%allows to import a CSEP or RELM forecast directly. Caution:
	%The previous grid will be overwritten.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if nargin<5
		ForecastType='Poisson'
	end	
	
	if ~obj.EmptyGrid|obj.Ready
		disp('Grid and forecasts will be overwritten');
		warning('Grid and forecasts will be overwritten');
	end
	
	if isempty(CastTime)|isempty(CastLength)
		error('No Time specified')
	end
	
	
	%reset the dimension in case it is needed
	obj.Dimension=struct(	'Space',false,...
							'Depth',false,...
							'Mag',false,...
							'Time',false);
	
	%Split of the Poissonian forecast
	switch ForecastType
		case 'Poisson'
			ForecastData=RelmGrid(:,9);
			TheGrid=RelmGrid(:,1:8);
			
			ForeLength=numel(RelmGrid(:,9));
			MaskThere=numel(RelmGrid(1,:))==10;
			
			if MaskThere
				TheMask=RelmGrid(:,10);
			end
		
		case 'Custom'
			ForecastData=RelmGrid{2};
			TheGrid=RelmGrid{1}(:,1:8);
			obj.NodeIDs=RelmGrid{1}(:,9)
			
			ForeLength=numel(RelmGrid{1}(:,9));
			MaskThere=numel(RelmGrid{1}(1,:))==10;
			
			if MaskThere
				TheMask=RelmGrid{1}(:,10);
			end
			
		case 'NegBin'
			ForecastData=RelmGrid(:,9,11);
			TheGrid=RelmGrid(:,1:8);
			
			ForeLength=numel(RelmGrid(:,9));
			MaskThere=numel(RelmGrid(1,:))==11;
			
			if MaskThere
				TheMask=RelmGrid(:,10);
			end
		
	end	
	
	%EmptyBag
	obj.TheBag={};
	
	%pass it to the function which does the actuall work
	obj.addGrid(TheGrid);
	
	%init forecast
	obj.initForecast(ForecastType,{});
	
	%add forecast (to be sure, reset everything)
	obj.TimeVector=CastTime;
	obj.TimeSpacing=CastLength;
	obj.ForecastData={};
	obj.ForecastData{1}=ForecastData;
	obj.EmptyGrid=false;
	
	%set DataMask
	if MaskThere
		obj.DataMask=logical(TheMask);
		obj.GenericMask=false;
	else
		obj.DataMask=true(ForeLength,1);
		obj.GenericMask=false;
	end
	
	obj.EqualMask=true;
	
	%Write total distro and expval
	[OverallDistro ExpVal] = setFullDistro(obj,ForecastData);
	obj.OverallDistro={OverallDistro};
	obj.ExpectedValue=ExpVal;
	
	
end

classdef ForecastObj < handle
	%ForecastObj v1 serves as storage for CSEP and Relm style forecasts.
	%It supports currently up to 5D (lon,lat,depth,mag,time) and can be used
	%either as real forecast or as a empty grid.
	%This version does only support a single grid common to all timeslices	
	
	%Comment RealTimeMode: This allows to use overlapping forecasts, it is 
	%not tested currently but I guess it should work and I hope I got all
	%places where change was needed.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	properties
		Name
		ID
		Version
		EmptyGrid
		Ready
		ForecastReady
		TheGrid
		NodeIDs
		GridNodes
		AxisVectors
		GridSpacings
		GridLimits
		GridPrecision
		MagBins
		DepthBins
		RegionPoly
		BoundBox
		TimeVector
		TimeSpacing
		RealTimeMode
		Dimension
		SelectedNodes
		SelectedIDs
		JumpValue
		ForecastData
		DisForecastData
		ExpectedValue
		OverallDistro
		ForecastType
		DataMask
		EqualMask
		GenericMask
		TheBag
	
	end
	
	
	
	methods
		function obj=ForecastObj(Name)
			%constructor: only inits the most important fields
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
				%A bit gimmicky I admit, but could be useful later
				Name=RawChar(randi(15,1,8)+1)
			end
			
			obj.Name=Name;
			obj.ID=RawChar(randi(15,1,8)+1);
			obj.Version='0.9';
			
			%init data
			obj.EmptyGrid=true;
			obj.Ready=false; %Grid is set
			obj.ForecastReady=false; %forecast is initialized
			obj.TheGrid=[];
			obj.GridNodes=[];
			obj.AxisVectors={};
			obj.GridSpacings=[];
			obj.GridLimits=[];
			obj.GridPrecision=10^(-5);
			obj.MagBins=[];
			obj.DepthBins=[];
			obj.RegionPoly=[];
			obj.BoundBox=[];
			obj.TimeVector=[];
			obj.TimeSpacing=[];
			obj.RealTimeMode=false;
			obj.Dimension=struct(	'Space',false,...
									'Depth',false,...
									'Mag',false,...
									'Time',false);
			obj.SelectedNodes=[];
			obj.SelectedIDs=[];
			obj.JumpValue=[];
			obj.ForecastData={};
			obj.DisForecastData={}; %Needed for neg. bin.
			obj.ExpectedValue=[];
			obj.OverallDistro={};
			
			obj.NodeIDs=[];
			%Node IDs are meant for the Custom mode, but mostly not needed even there, they're more
			%a security measurement to avoid mix up of nodes in the file 
			
			%ForecastType is needed to output the a complete forecast
			%at the moment 'None' for empty forecast and 'Poisson' and 'Custom' is
			%know and specified. The field 'Method' is needed for future
			%expantions where it will be a handle to a converter or similar.
			%Note: ForecastType will also determine which format the 
			%ForecastData field has.In case of 'Poisson' this is simply a 
			%vector. In a possible 'NegBin' (Negative Binomial) this may be
			%a matrix of the dimension [NrBin,2
			obj.ForecastType=struct('Type','None',...
									'Method',{{}});
			
			%Defines if a cell is used in the forecast (true) or not (false)			
			obj.DataMask=[];			
			
			%if set to true, the DataMask is the same for every forecast,
			%which makes DataMask a vector, if set to false a DataMask for
			%every timestep is used making DataMask a cell array of vectors
			obj.EqualMask= true;			
			obj.GenericMask=true;
			
			%internal variable
			obj.TheBag={};
			
		end

		%The external file methods
		%-------------------------
		
		createGrid(obj,Filterlist,Datastore,GridSpacing,MagSpace,DepthSpace,KeyPoint,GridPrec)
		
		setRealTimeMode(obj, ModeToogle)

		addGrid(obj,TheGrid,TripleGrid)

		initForecast(obj,ForecastType,ForecastMethod)

		addForecast(obj,ForecastData,CastTime,CastLength,DataMask)
		
		replaceForecast(obj,ForecastData,CastTime,CastLength,DataMask)

		removeForecast(obj,CastTime)

		importCSEPForecast(obj,RelmGrid,CastTime,CastLength,ForecastType)

		[CSEPGrid ForeTime ForeLength]= getCSEPForecast(obj,CastTime)

		NrNodes=getNrNodes(obj)
		
		[ForeSlice ForeTime ForeLength] = get2DSlice(obj,CastTime,Magnitude,Depth)

		[ForeVolume ForeTime ForeLength] = get3DVolume(obj,CastTime,Magnitude,Depth)

		Name = getName(obj)

		ID = getID(obj)

		setName(obj,Name)

		PortableStruct =  exportFullData(obj)

		importFullData(obj,PortableStruct)
		
		Depths = getDepths(obj)	

		Magnitudes = getMagnitudes(obj)

		TheGrid = getFullGrid(obj)
	
		GridNode = getGridNodes(obj)

		[LonVec LatVec DepthVec MagVec] = getAxisVectors(obj)

		[SpaceGrid DepthGrid  MagGrid] = getAxisMesh(obj)

		[OverallDistro ExpVal] = setFullDistro(obj,ForecastData)

		[OverallDistro ExpVal] = getTotalRate(obj,CastTime)

		RawSlice RawVolume] = BuildRawGrid(obj,VolumeMode,ZeroMode)

		SumCast = SumFullDistro(obj,DistA,DistB)

		ResMat = MatrixCorrector(obj,inMat,LogicArray,WorkMode)

		NewObj = cloneWars(obj)

	end




end	
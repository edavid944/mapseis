function importFullData(obj,PortableStruct)
	%import a previous exported data set
	%restores a forecast from a data dump.
	%In future versions this may also updates a data dump
	%to the most current version.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	%Set everything back in original state as done by the 
	%constructor
	obj.Version='0.9';
	%init data
	obj.EmptyGrid=true;
	obj.Ready=false;
	obj.TheGrid=[];
	obj.GridNodes=[];
	obj.AxisVectors={};
	obj.GridSpacings=[];
	obj.GridPrecision=10^(-5);
	obj.RealTimeMode=false;
	obj.GridLimits=[];
	obj.MagBins=[];
	obj.DepthBins=[];
	obj.RegionPoly=[];
	obj.BoundBox=[];
	obj.TimeVector=[];
	obj.TimeSpacing=[];
	obj.Dimension=struct(	'Space',false,...
							'Depth',false,...
							'Mag',false,...
							'Time',false);
	obj.SelectedNodes=[];
	obj.SelectedIDs=[];
	obj.JumpValue=[];
	obj.ForecastData={};
	
	obj.ForecastType=struct('Type','None',...
							'Method',{{}});
	obj.TheBag={};
	obj.ForecastReady=false;
	
	%Add the grid
	obj.GridPrecision=PortableStruct.GridPrecision;
	obj.addGrid(PortableStruct.TheGrid);
	
	%add the rest of the data directly (could be looped, but
	%I prefere it like this, more control)
	obj.Name=PortableStruct.Name;
	obj.ID=PortableStruct.ID;
	obj.RegionPoly=PortableStruct.RegionPoly;
	obj.BoundBox=PortableStruct.BoundBox;
	obj.TimeVector=PortableStruct.TimeVector;
	obj.TimeSpacing=PortableStruct.TimeSpacing;
	obj.RealTimeMode=PortableStruct.RealTimeMode;
	obj.Dimension=PortableStruct.Dimension;
	obj.ForecastData=PortableStruct.ForecastData;
	obj.ForecastType=PortableStruct.ForecastType;
	obj.ExpectedValue=PortableStruct.ExpectedValue;
	obj.OverallDistro=PortableStruct.OverallDistro;
	obj.DataMask=PortableStruct.DataMask;
	obj.EqualMask=PortableStruct.EqualMask;
	obj.GenericMask=PortableStruct.GenericMask;
	obj.ForecastReady=PortableStruct.ForecastReady;
	
	%check what is present:
	if ~isempty(obj.ForecastData)
		obj.EmptyGrid=false;
	else
		obj.EmptyGrid=true;
	end
     		
end
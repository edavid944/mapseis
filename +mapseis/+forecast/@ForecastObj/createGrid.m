function createGrid(obj,Filterlist,Datastore,GridSpacing,MagSpace,DepthSpace,KeyPoint,GridPrec)
	%a passthrought to the RelmGrid creator
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.forecast.*;
	import mapseis.projector.*;
	
	if ~isempty(GridPrec)
		if ~islogical(GridPrec)
			%new precision
			obj.GridPrecision=GridPrec;
		end
	end
	
	%generate the almost finished grid
	[TheGrid TripleGrid isInIt] = generateGrid(Filterlist,Datastore,GridSpacing,MagSpace,DepthSpace,KeyPoint,obj.GridPrecision);
	
	%Pack the Bag, makes life a bit easier, in the other function
	obj.TheBag={GridSpacing,MagSpace,DepthSpace};
	
	%pass it to the function which does the actuall work
	obj.addGrid(TheGrid,TripleGrid);
	
	
	%get Grid polygon and add it
	if ~isnumeric(Filterlist)
		regionFilter = Filterlist.getByName('Region');
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange=filterRegion{1};
		
		if strcmp(RegRange,'all');
			Filterlist.changeData(Datastore);
			Filterlist.updateNoEvent;
			selected=obj.Filterlist.getSelected;
			Filterlist.PackIt
			
			[locations temp]=getLocations(Datastore,selected);
			lon=locations(:,1);
			lat=locations(:,2);
			bounded=[min(lon) max(lon) min(lat) max(lat)];
			TheBox=bounded;
		
		else
			bounded=pRegion.getBoundary;
			TheBox=pRegion.getBoundingBox;
		
		end
	else
		bounded=[Filterlist(1) Filterlist(2) Datastore(1) Datastore(2)];
		TheBox=bounded;

	end
	
	obj.RegionPoly=bounded;
	obj.BoundBox=TheBox;
	
end
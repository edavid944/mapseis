function GridNode = getGridNodes(obj)
	%Returns a TripleS type grid with only the spacial grid
	%nodes and centres of the cells instead of corners.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if ~obj.Ready
		error('No Grid defined');
	end
	
	TheGrid = getFullGrid(obj);
	
	%generate a GridNode
	ElementSelector=find(TheGrid(1,5)==TheGrid(:,5)&...
						TheGrid(1,7)==TheGrid(:,7));     	
		
	GridNode(:,1)=(TheGrid(ElementSelector,1)+TheGrid(ElementSelector,2))/2;
	GridNode(:,2)=(TheGrid(ElementSelector,3)+TheGrid(ElementSelector,4))/2;
	
end
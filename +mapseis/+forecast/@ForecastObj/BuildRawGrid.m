function [RawSlice RawVolume] = BuildRawGrid(obj,VolumeMode,ZeroMode)
	%The function just builds raw grid data depended on the 
	%forecast type (NaNs for poisson, empty cells for Custom)
	%VolumeMode selects the caller, true for get3DVolume 
	%and get2DSlice in full 3D mode and false for "real 2D"
	%get2DSlice
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if nargin<3
		ZeroMode=false;
	end
	
	
	switch obj.ForecastType.Type
		case 'Poisson'
			if VolumeMode
				if ~ZeroMode
					RawSlice = NaN(size(obj.SelectedNodes{1}{1}));
					RawVolume = NaN(size(obj.SelectedNodes{2}));
					
				else
					RawSlice = zeros(size(obj.SelectedNodes{1}{1}));
					RawVolume = zeros(size(obj.SelectedNodes{2}));
				end
				
			else
				if ~ZeroMode
					RawSlice = NaN(size(obj.SelectedNodes));
					RawVolume = [];
				
				else
					RawSlice = zeros(size(obj.SelectedNodes));
					RawVolume = [];
				
				end
			end
				
		case 'Custom'
			if VolumeMode
				if ~ZeroMode
					RawSlice = cell(size(obj.SelectedNodes{1}{1}));
					RawVolume = cell(size(obj.SelectedNodes{2}));
				else
					RawSlice = cell(size(obj.SelectedNodes{1}{1}));
					RawVolume = cell(size(obj.SelectedNodes{2}));	
				end
				
			else
				if ~ZeroMode
					RawSlice = cell(size(obj.SelectedNodes));
					RawVolume = [];
					
				else
					RawSlice = cell(size(obj.SelectedNodes));
					RawVolume = [];
					
				end
			end
			
			
		case 'NegBin'
			%Missing currently, guess it will be cell
			if VolumeMode
				if ~ZeroMode
					RawSlice = cell(size(obj.SelectedNodes{1}{1}));
					RawVolume = cell(size(obj.SelectedNodes{2}));
				else
					RawSlice = cell(size(obj.SelectedNodes{1}{1}));
					RawVolume = cell(size(obj.SelectedNodes{2}));
					
				end
				
			else
				if ~ZeroMode
					RawSlice = cell(size(obj.SelectedNodes));
					RawVolume = [];
				
				else
					RawSlice = cell(size(obj.SelectedNodes));
					RawVolume = [];
				
				end
			end
	end	
	
end
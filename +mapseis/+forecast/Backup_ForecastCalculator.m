classdef ForecastCalculator < handle
	%This the first version of the forecast project calculator
	
	
	properties
        	Name
        	Version
        	ID
        	ObjVersion
        	MetaData
        	CalcStatus
        	
        	ModelNameList
        	TimeList
        	WorkToDo
        	WorkList
        	WorkDone
        	ErrorInCalc
        	        	
        	CalculatorSpace
        	
        	NextCalc
        	StopToggle
        	
 		ForecastProject 
 		CalcProgress
        	ErrorCode
        	SavePath
               	CalcPath
        	ArchivePath
        	ArchiveForecasts
        	StoreLogs
        	ArchiveLogs
        	SaveConfig
        	BenchmarkData
        	UseTimer
        	BufferCatalogs
        	PastCalcTime
        	
        	LearningFilter
        	ExtendRegionPoly
        	PolyExtend
        	
        	SaveEveryStep
        	SaveCount
        	SaveOften
        	
        	LoopMode
        	ParentParallelMode
        end
        
        events
        	CalcUpdate
        	ErrorOccured
        end

    
        methods
        	function obj = ForecastCalculator(Name)
        		%constructor, sets default values and creates the object
        		
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        		
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1);
        		end
        		
        		obj.Name=Name; %may be not so important here, but who knows?
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.Version='0.9';
        		
        		%init data
        		obj.CalcStatus='empty';
        		obj.ModelNameList={};
        		obj.TimeList=[];
        		obj.WorkToDo=[];
        		obj.ForecastProject=[];       	
        		obj.CalcProgress=0;
        		obj.ErrorCode=-1;
        		        		
        		obj.WorkList=[];
			obj.WorkDone=[];
			obj.ErrorInCalc=[];
			
			obj.CalculatorSpace=[];
			
			obj.NextCalc=0;
			obj.StopToggle=false;
			
			obj.SavePath=[];
        		obj.CalcPath=[];
        		obj.ArchivePath=[];
        		obj.BenchmarkData=[];
        		obj.UseTimer=false;
			obj.ArchiveForecasts=false;
        		obj.StoreLogs=false;
        		obj.ArchiveLogs=false;
        		obj.SaveConfig=false;
			obj.BufferCatalogs=false;
			obj.PastCalcTime=[];
			
			obj.ExtendRegionPoly=false;
			obj.PolyExtend=[0.2,0.2];
			obj.LearningFilter=[];
			
			obj.SaveEveryStep=true;
			obj.SaveCount=10;
			obj.SaveOften=10;
			
			obj.LoopMode=true;
			obj.ParentParallelMode=false;
			
        	end
        	
        	
        	
		function externStop(obj, Shouter)
		    % This allows to let the stopCalc listen to an external stop 
		    %notification
		    
		    addlistener(Shouter,'Stop',@(s,e) obj.stopCalc());
		
		end
		
		
        	function setProject(obj,ForecastProject)
        		%set the project and additional data
        		import mapseis.filter.*;
        		
        		obj.ForecastProject=ForecastProject;
        		
        		try %a try for now, my first files did not have this field
        			obj.UseTimer=obj.ForecastProject.ProjectConfig.Benchmark;
        		end
        		
        		%check if project is complete
        		obj.ForecastProject.checkState;
        		
        		%correct Progress if not started or ready (started is new here)
        		if ~any(strcmp(obj.ForecastProject.ProjectStatus,{'ready','started'}))
        			%not entirely sure about the order of the commands
        			obj.ForecastProject.correctProgress;
        			obj.ForecastProject.cleanForecasts;
        			obj.ForecastProject.checkState;
        			
        		end
        		
        		%There is a bug somewhere which erases old forecasts when it shouldn't
        		%have to check where this is going wrong, I guess it is somewhere in the 
        		%correctProgress, cleanForecasts routine but it is not certain.
        		
        		
        		if any(strcmp(obj.ForecastProject.ProjectStatus,{'empty','changed','config'}))
        			%something seems to be missing
        			warning('Forecast Project is not complete')
        			obj.StopToggle=true;
        			obj.CalcStatus='uncomplete';
        			return
        		end
        		
        		
        		if strcmp(obj.ForecastProject.DataMode,'Source')
        			%update catalog
        			obj.ForecastProject.updateCatalog;
        			
        		end
        		
        		
        		%The project seems to be calculateable ->get parameters
        		obj.SavePath=obj.ForecastProject.StoreProject;
        		obj.CalcPath=obj.ForecastProject.CalcPath;
        		obj.ArchivePath=obj.ForecastProject.ArchivePath;
        		
        		%just some shortcuts
        		obj.ArchiveForecasts=obj.ForecastProject.ProjectConfig.ArchiveForecasts;
        		obj.StoreLogs=obj.ForecastProject.ProjectConfig.StoreLogs;
        		obj.ArchiveLogs=obj.ForecastProject.ProjectConfig.ArchiveLogs;
        		obj.SaveConfig=obj.ForecastProject.ProjectConfig.SaveConfig;
        		
        		
        		%some of the values are meant for diagnostic and are not used directly 
        		%by the calculator
        		genericSpace=struct(	'CalculatorID','origin_v1',...
        					'CalcName',obj.Name,...
        					'CalcID',obj.ID,...
        					'InternalCatSel',{{}},...
        					'DefectModels',[[]],...
        					'NextCalc',obj.NextCalc,...   
        					'CalcProgress',obj.CalcProgress,...
        					'CalcStatus',obj.CalcStatus,...
        					'ErrorCode',0,...
        					'LastCalcTime',now,...
        					'Finished',true);
        		
        		contCalcer=false;			
        		if ~isempty(obj.ForecastProject.CalculatorSpace)
        			disp('import past progress');
        			%a calculator was active before
        			if strcmp(obj.ForecastProject.CalculatorSpace.CalculatorID,'origin_v1')
        				obj.CalculatorSpace=obj.ForecastProject.CalculatorSpace;
        				
        				contCalcer=true;
        			else
        				%use the generic one
        				obj.CalculatorSpace=genericSpace;
        				
        				
        			end
        		else
        			%use the generic one
        			obj.CalculatorSpace=genericSpace;
        			
        		end
        		
        		
        		
        		%generate NameList
        		for i=1:numel(obj.ForecastProject.ModelPlugIns(:,1))
        			ModelNames{i}=[obj.ForecastProject.ModelPlugIns{i,1},'_v',obj.ForecastProject.ModelPlugIns{i,2}];
        		end
        		
        		obj.ModelNameList=ModelNames;	
        		
        		%generate the WorkToDo structure
        		for i=1:numel(obj.ModelNameList)
        		
        			obj.WorkToDo.(obj.ModelNameList{i})=obj.ForecastProject.CalcProgress.ModelProgress.(obj.ModelNameList{i});
        			
        			if isfield(obj.ModelNameList{i},obj.ForecastProject.CalcProgress.StopProgress)
        				obj.WorkToDo.(obj.ModelNameList{i})(~obj.ForecastProject.CalcProgress.StopProgress.(obj.ModelNameList{i}))=NaN;
        			end
        		
        		end
        		
        		%get times
        		obj.TimeList=obj.ForecastProject.CalcProgress.Times;
        		if isempty(obj.TimeList)
        			obj.TimeList=obj.ForecastProject.AvailableTimes;
        		end
        		
        		%update internal FilterList storage, for the available times
        		if isempty(obj.CalculatorSpace.InternalCatSel)
        			obj.CalculatorSpace.InternalCatSel=cell(size(obj.TimeList));
        			
        		elseif numel(obj.CalculatorSpace.InternalCatSel)~=numel(obj.TimeList)
        			obj.CalculatorSpace.InternalCatSel=cell(size(obj.TimeList));
        		
        		end
        		
        		
        		%generate calcsteps 
        		obj.generateCalcSteps;
        		
        		%set to correct step
        		obj.NextCalc=obj.CalculatorSpace.NextCalc;
        		obj.CalcProgress=obj.CalculatorSpace.CalcProgress;
        		
        		
        		%set the correct calculator status
        		switch obj.ForecastProject.ProjectStatus
        			case {'ready','recalc','started'}
        				obj.CalcStatus='ready';
        				obj.StopToggle=false;
        				%ready for action
        				obj.ErrorCode=0;
        				
        			case 'finished'
        				obj.CalcStatus='finished';
        				obj.StopToggle=false;
        				%recalc is of course possible
        				obj.ErrorCode=0;
        				
        			case 'cont'
        				obj.CalcStatus='cont-finished';
        				obj.StopToggle=false;
        				%recalc is of course possible	
        				obj.ErrorCode=0;
        				
        			case {'empty','config','changed'}
        				%This one should never happen, but to be sure
        				obj.CalcStatus='uncomplete';
        				obj.StopToggle=true;
        				obj.ErrorCode=-1;
        		end
        		
        		
        		
        		
        		
        		
        		%create benchmarking structure if UseTimer is turned on
        		if obj.UseTimer
        			if contCalcer
        				if ~isempty(obj.ForecastProject.BenchmarkData)
        					obj.BenchmarkData=obj.ForecastProject.BenchmarkData;
        				else
        					obj.generateTimeStruct;
        				end
        			else
        			
        				obj.generateTimeStruct;
        			end
        		end
        		
        		DoExtend=obj.ForecastProject.getParameter('AutoExtend');
        		if ~isempty(DoExtend)
        			obj.ExtendRegionPoly=DoExtend;
        			obj.PolyExtend=obj.ForecastProject.getParameter('RegionExtend');
        		end
        		
        		
        		if obj.ExtendRegionPoly
        			RawLearner=FilterListCloner(obj.ForecastProject.LearningFilter);
        			obj.MrExtender(RawLearner);
        			obj.LearningFilter=RawLearner;
        		
        		else
        			obj.LearningFilter=FilterListCloner(obj.ForecastProject.LearningFilter);
        		end
        	end
        	
        	
        	
        	function generateCalcSteps(obj)
        		%generates the list of jobs which should be done
        		
        		obj.WorkList=[];
        		
        		for j=1:numel(obj.TimeList)
        			for i=1:numel(obj.ModelNameList)
        				if obj.WorkToDo.(obj.ModelNameList{i})(j)==0
        					obj.WorkList(end+1,:)=[j,i];
        				end
        			end
        		end	
        		
        		%needed also
        		obj.WorkDone=false(size(obj.WorkList(:,1)));
        		obj.ErrorInCalc=nan(size(obj.WorkList(:,1)));
        		
        		
        		if ~isempty(obj.WorkList)
        			obj.NextCalc=1;
        			if strcmp(obj.CalcStatus,'finished')
        				obj.CalcStatus='ready';
        				obj.ErrorCode=0;
        			end	
        			
        			if strcmp(obj.CalcStatus,'cont-finished')
        				obj.CalcStatus='cont-ready';
        				obj.ErrorCode=0;
        			end	
        		end
        		
        		%set toggle if it wasn't before
        		if ~any(strcmp(obj.ForecastProject.ProjectStatus,{'empty','changed','config'}))
        			obj.StopToggle=false;
        			obj.ErrorCode=0;
        		end
        		
        		
        	end
        	
        	
        	function generateTimeStruct(obj)
        		%generates a template for storing the calculation times
        		import mapseis.util.*;
        		
        		NrNodes=obj.ForecastProject.ForecastGrid.getNrNodes;
        		
        		zeroVec=zeros(size(obj.TimeList));
        		SingMod=struct( 'TotalTimePerStep',zeroVec,...
        				'ConfigTimePerStep',zeroVec,...
        				'CalcTimePerStep',zeroVec,...
        				'ImportTimePerStep',zeroVec,...
        				'PrepareTime',0,...
        				'TotalTime',0,...
        				'ConfigTime',0,...
        				'CalcTime',0,...
        				'ImportTime',0,...
        				'AddTimeInfo',{{}});
        		
        		ModTime=struct;		
        		for i=1:numel(obj.ModelNameList)
        			ModTime.(obj.ModelNameList{i})=SingMod;
        		end
        		
        		TimeStruct=struct(	'System',getSystemInfo,...
        					'NumberOfNodes',NrNodes,...
        					'Models',{obj.ModelNameList},...
        					'TimeSteps',obj.TimeList,...
        					'ModelTimes',ModTime,...
        					'CalcTimeStep',zeroVec,...
        					'FileSize',zeroVec,...
        					'TotalTime',0);
        					
        		obj.BenchmarkData=TimeStruct;
        		
        		
        	end
        	
        	
        	
        	function setCalcSteps(obj,CalcStepStruct)
        		%adds calcstep structure
        		TheNames=fieldnames(CalcStepStruct);
        		
        		for i=1:numel(TheNames)
        			if any(strcmp(TheNames{i},obj.ModelNameList))
        				obj.WorkToDo.(TheNames{i})=CalcStepStruct.(TheNames{i});
        			end
        			
        		end
        		
        		
        		%kill parts which are not allowed to be calculated
        		
        		for i=1:numel(obj.ModelNameList)
        		        		       			
        			if isfield(obj.ModelNameList{i},obj.ForecastProject.CalcProgress.StopProgress)
        				obj.WorkToDo.(obj.ModelNameList{i})(~obj.ForecastProject.CalcProgress.StopProgress.(obj.ModelNameList{i}))=NaN;
        				
        				%removed timesteps as well
        				obj.WorkToDo.(obj.ModelNameList{i})(obj.ForecastProject.CalcProgress.ModelProgress.(obj.ModelNameList{i})==2)=NaN;
        				obj.WorkToDo.(obj.ModelNameList{i})(obj.ForecastProject.CalcProgress.ModelProgress.(obj.ModelNameList{i})==-1)=NaN;
        			end
        		
        		end
	        
        		%generate new CalcList
        		obj.generateCalcSteps;
        		
        	end
        	
        	
        	
        	
        	
        	function CalcStepStruct = getCalcSteps(obj)
        		CalcStepStruct = obj.WorkToDo;
        	
        	end
        	
        	
        	
        	function MrExtender(obj,LearnFilt)
        		%extends the polygon in the filter by the amount defined in
        		%PolyExtend
        		import mapseis.region.*;
        		
        		%the region filter
        		regionFilter=LearnFilt.getByName('Region');
        		filterRegion = getRegion(regionFilter);
        		pRegion = filterRegion{2};
        		RegRange = filterRegion{1};
        		
        		if ~strcmp(RegRange,'all');
				RegPoly=pRegion.getBoundary;
				
				%determine "middle point" (just the mean)
				MidPoly(1)=mean(RegPoly(1:end-1,1));
				MidPoly(2)=mean(RegPoly(1:end-1,2));
				
				%"direction" of subtraction vector
				dirVec(:,1)=sign(abs(RegPoly(:,1))-abs(MidPoly(1)));
				dirVec(:,2)=sign(abs(RegPoly(:,2))-abs(MidPoly(2)));
				
				NewPoly(:,1)=sign(RegPoly(:,1)).*(abs(RegPoly(:,1))+dirVec(:,1).*obj.PolyExtend(1));
				NewPoly(:,2)=sign(RegPoly(:,2)).*(abs(RegPoly(:,2))+dirVec(:,2).*obj.PolyExtend(2));
				
				newRegType=RegRange;
				newRegion = Region(NewPoly);
				regionFilter.setRegion(newRegType,newRegion);
							
        		
        		else
        			disp('No Polygon in filter - does not have to be extended');
        		end
        	end
        	
        	
        	
        	function startCalc(obj,forced)
        		%this will start the calculation, by calling the prepare of every model
        		%and doing the necessary stuff
        		       		
        		%NOTE: I'm not entirely sure if this function is finished, there might be some
        		%functionallity which will be added here
        		        		
        		import mapseis.filter.*;
        		
        		
        		if ~any(strcmp(obj.CalcStatus,{'ready','finished','cont-ready','cont-finished'}))	
        			error('Calculator not ready')
        		end
        	
        		if any(strcmp(obj.CalcStatus,{'finished','cont-finished'}))
        			warning('Calculator is finished, please select parts to be re-calculated');
        			return
        		end
        		
        		%Check error code (short version)
        		if ~obj.checkError
        			error('Calculator notified an error');
        		end
        		
        		%set the recursion limit to a high enough number (matlab will stop
        		%the calculation later if this is not done
        		NrNeeded=numel(obj.WorkList(:,1))+500; 
        		set(0,'RecursionLimit',NrNeeded);
        		
        		        		
        		%use the initial learning period as input for prepare
        		[TimeInt,Range] = getLearningTime(obj,0);
        		
        		Datastore=obj.ForecastProject.Datastore;
        		%LearningFilter=obj.ForecastProject.LearningFilter;
        		LearningFilter=obj.LearningFilter;
        		
        		%open Question: clone Filterlist or not (how easy is it anyway?)
        		%->Clone
        		FirstLearningFilter=FilterListCloner(LearningFilter);
        		TimeFilt=FirstLearningFilter.getByName('Time');
        		TimeFilt.setRange(Range,TimeInt);
        		
        		%get data, not needed the plugin has to do this
        		%but I leave it here maybe needed later or elsewhere
        		%FirstLearningFilter.changeData(Datastore);
        		%FirstLearningFilter.updateNoEvent;
        		%Selection=FirstLearningFilter.getSelected;
        		
        		UsedModelID=unique(obj.WorkList(:,2));
        		
        		for i=1:numel(UsedModelID)
        			%get model plugin, ID could be sended directly, but
        			%it saver like this (avoids mixups)
        			PrepTic=tic;
        			ModelName=obj.ModelNameList{UsedModelID(i)};
        			[curModel,Pos] = obj.ForecastProject.getModel(ModelName);
        			
        			%reset model
        			curModel.ResetFullCalc;
        			
        			%prepare model
        			curModel.setInputData(Datastore,FirstLearningFilter,obj.TimeList(1));
        			curModel.prepareCalc;
        			PrepTime=toc(PrepTic);
        			
        			if obj.UseTimer
        				obj.AddBenchTimes(PrepTime,i,'prepare');
        			end
        		end
        		
        		obj.ForecastProject.setBenchmark(obj.BenchmarkData);
        		
        		if ~obj.LoopMode
        		
        			%start calculation
        			CurNr=obj.NextCalc;
        			obj.NextCalc=obj.NextCalc+1;
        			obj.contCalc(CurNr);
        			
        		else
        			if ~obj.ParentParallelMode
        				obj.LoopItSerial;
        			else
        				obj.ParallelMode=false;
        				obj.LoopItParallel;
        			end
        		
        		end
        		
        	end
        	
        	
        	function restartCalc(obj)
        		%restarts from the last calcstep
        		
        		
        		if isempty(obj.WorkList)
        			error('No calculation has been started before - use startCalc');
        		end	
        	
        		
        		NrNeeded=numel(obj.WorkList(:,1))+500; 
        		set(0,'RecursionLimit',NrNeeded);
        		
        		
        		if ~obj.LoopMode
        			if obj.NextCalc>1
        				obj.contCalc(obj.NextCalc-1);
				else
					obj.contCalc(obj.NextCalc);
				end
				
        		else
        			if obj.NextCalc>1
        				obj.NextCalc=obj.NextCalc-1;
				end
        			
        		
        			if ~obj.ParentParallelMode
        				obj.LoopItSerial;
        			else
        				obj.ParallelMode=false;
        				obj.LoopItParallel;
        			end
        		end
        	end
        	
        	
        	
        	function contCalc(obj,ListNr)
        		%the actual calculation, this a recursive function
        		        		
        		import mapseis.filter.*;
        		
        		
        		
        		%check if stopped or fatal error is present
        		if ~checkError(obj)|obj.StopToggle
        			warning(['calculation stopped at ID ', num2str(ListNr)]);
        			obj.pauseCalc
        			return
        		end
        		
        		CalcerTic=tic;
        		
        		%get which model and time
        		TimeID = obj.WorkList(ListNr,1);
        		ModelID = obj.WorkList(ListNr,2);
        		
        		if any(obj.CalculatorSpace.DefectModels==ModelID)
        			warning('Model has been automatically stopped');
        			
        			%continue if any left
				if ListNr<=obj.NextCalc
					CurNr=obj.NextCalc;
					obj.NextCalc=obj.NextCalc+1;
					obj.contCalc(CurNr);
				else
					%The end my friend
					obj.finishCalc;
				
				end
				
				return
        		end
        		
        		%check if catalog is existing, if not build and save
        		if isempty(obj.CalculatorSpace.InternalCatSel{TimeID})
        			%build new filterlist
        			Datastore=obj.ForecastProject.Datastore;
        			%LearningFilter=obj.ForecastProject.LearningFilter;
        			LearningFilter=obj.LearningFilter;

        			[TimeInt,Range] = getLearningTime(obj,TimeID);
        			
        			TheLearner=FilterListCloner(LearningFilter);
        			TimeFilt=TheLearner.getByName('Time');
        			TimeFilt.setRange(Range,TimeInt);
        			
        			if obj.BufferCatalogs
        				%store it internally
        				obj.CalculatorSpace.InternalCatSel{TimeID}=TheLearner;
        			end
        			
        			%store it in the project
        			ErrorCode = obj.ForecastProject.setUsedCatalog(obj.TimeList(TimeID),TheLearner);
        			
        		else
        			%get old one
        			Datastore=obj.ForecastProject.Datastore;
        			TheLearner=obj.CalculatorSpace.InternalCatSel{TimeID};
        		end
        		
        		
        		%get model
        		ModelName=obj.ModelNameList{ModelID};
        		[curModel,Pos] = obj.ForecastProject.getModel(ModelName);
        		
        		%reset model
        		curModel.ResetLastCalc;
        		
        		%send data
        		curModel.setInputData(Datastore,TheLearner,obj.TimeList(TimeID));
        		        		
        		%calc
        		try
        			ErrorCode = curModel.calcForecast;
        		catch
        			%to be sure that it continue with the rest, even if it completely fails
        			disp('Calculation failed')
        			ErrorCode = -1
        		end	
        		
        		
        		%check error
        		IsSave = obj.checkError(ErrorCode,ListNr);
        		
        		if ~IsSave
        			warning(['calculation stopped at ID ', num2str(ListNr),' ,with error code ',num2str(ErrorCode)]);
        			obj.StopToggle=true;
        			obj.pauseCalc
        			return
        		end
			        		
        		
        		        		
        		%everything should be fine now, get the results
        		[ForeCast,DataMask] = curModel.getForecast(false); %no grid, keep the size low
        		
        		%generate benchmark data if needed
        		if obj.UseTimer
        			CPUTimeData = curModel.getCalcTime;
        			
        			obj.AddBenchTimes(CPUTimeData,ListNr,'calc');
        			obj.ForecastProject.setBenchmark(obj.BenchmarkData);
        			        			
        		end
        		
        		
        		
        		%additional parts of the results
        		if obj.ArchiveForecasts
        			FilePath = curModel.getForecastFile;
        			ForPath = obj.archiveFile(FilePath);
        		else
        			ForPath=[];
        		end
        		
        		disp('archived');
        		
        		if obj.StoreLogs
        			TheLogs = curModel.getCalcLog;
        			
        		else
        			TheLogs=[];
        		end
        		
        		disp('stored');
        		
        		if obj.ArchiveLogs
        			FilePath = curModel.getCalcLogFile;
        			LogPath = obj.archiveFile(FilePath);
        		else
        			LogPath=[];
        		end
        		
        		disp('archived logs');
        		
        		if obj.SaveConfig
        			ModConfig = curModel.getModelConfig;
        		else
        			ModConfig=[];
        		end
        		
        		disp('saved config');
        		
        		%store in the project
        		ForecastData={ForeCast,ForPath};
        		LogData={TheLogs,LogPath};
        		ConfigData=ModConfig;
        		ErrorCode = obj.ForecastProject.setForecastResults(obj.TimeList(TimeID),ModelName,ForecastData,DataMask,LogData,ConfigData);
        
        		disp('project saved');
        		
        		if ErrorCode~=0
        			obj.ErrorCode=99;
        			warning(['calculation stopped at ID ', num2str(ListNr),' ,because of an I/O problem Code: ',num2str(ErrorCode)]);
        			IsSave = obj.checkError;
        			obj.StopToggle=true;
        			obj.pauseCalc
        			return
        		end
        		       		        		
        		%store project
        		obj.storeCurrentResults;
        		
        		
        		%register as done calculation
        		obj.WorkDone(ListNr)=true;
        		
        		%update progress
        		obj.CalcProgress=ceil(ListNr/numel(obj.WorkList(:,1))*1000)/10;
        		disp(['Calculation progress ',num2str(obj.CalcProgress),'%']);        		
        		%notify the CalcUpdate
        		notify(obj,'CalcUpdate')
        		
        		obj.PastCalcTime=toc(CalcerTic);
        		
        		if ~obj.LoopMode
        		
				%continue if any left
				if ListNr<numel(obj.WorkList(:,1))
					CurNr=obj.NextCalc;
					obj.NextCalc=obj.NextCalc+1;
					obj.contCalc(CurNr);
				else
					%The end my friend
					obj.finishCalc;
				
				end
        		
        		else
        			%it is looped do nothing for now
        		
        		end
        		
        	
        	
        	end
        	
        	
        	function LoopItSerial(obj)
        		%the starter for the loop mode
        		
        		TheEnd=numel(obj.WorkList(:,1));
        		
        		if obj.NextCalc<=TheEnd
        			for kk=obj.NextCalc:TheEnd
        				obj.contCalc(kk);
        				obj.NextCalc=obj.NextCalc+1;
        		
        			end
        			
        			
        			%finish it
        			obj.finishCalc;
        			
        		end
        	
        	
        	end
        	
        	
        	function LoopItParallel(obj)
        		%currently not available, because it is not sure if it works
        		disp('Not yet implemented')
        	
        			
        	end
        	
        	
        	
        	function AddBenchTimes(obj,CPUTimeData,ListNr,Mode)
        		%"outsourced" from the contCalc, to make it more readable
        		%it gets all the times in the calculation and writes them 
        		%to the correct place
        		
        		switch Mode
        			case 'calc'
        		
					%Times
					if ~isempty(CPUTimeData)
						TimeIn=CPUTimeData.InputDataTime;
						TimeCalcNow=CPUTimeData.CalcTime;
						TimeImp=CPUTimeData.ImportTime;
					end
					
					if ~isempty(obj.PastCalcTime)
						TimeCalcPast=obj.PastCalcTime(1); %better estimate of time
						PastExist=true;
					else
						TimeCalcPast=0;
						PastExist=false;
					end
					
					%check for addon
					if isfield(CPUTimeData,'AddTimeInfo')
						AddTimeInfo=CPUTimeData.AddTimeInfo;
					else
						AddTimeInfo=[];
					end
					
					
					%store
					TimeStruct=obj.BenchmarkData;
					TimeIDnow = obj.WorkList(ListNr,1);
					ModelIDnow = obj.WorkList(ListNr,2);
					ModNameNow = obj.ModelNameList{ModelIDnow}
					
					
					%first the past (if there is any)
					if (ListNr-1)>0&PastExist
						TimeIDpast = obj.WorkList(ListNr-1,1);
						ModelIDpast = obj.WorkList(ListNr-1,2);
						ModNamePast =obj.ModelNameList{ModelIDpast}
						
						
						%update total time per step
						TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast)=TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast)+...
							TimeCalcPast+TimeStruct.ModelTimes.(ModNamePast).ConfigTimePerStep(TimeIDpast)+...
							TimeStruct.ModelTimes.(ModNamePast).ImportTimePerStep(TimeIDpast);
						
						%update CalcTime
						TimeStruct.ModelTimes.(ModNamePast).CalcTimePerStep(TimeIDpast)=TimeCalcPast; %better time
							
						%update TotalTime
						TimeStruct.ModelTimes.(ModNamePast).TotalTime=TimeStruct.ModelTimes.(ModNamePast).TotalTime+TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast);
						
						%update CalcTime
						TimeStruct.ModelTimes.(ModNamePast).CalcTime=TimeStruct.ModelTimes.(ModNamePast).CalcTime+TimeCalcPast;
						
						%update time per step (all models)
						TimeStruct.CalcTimeStep(TimeIDpast)=TimeStruct.CalcTimeStep(TimeIDpast)+TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast);
						
						%get filesize of last file
						s=dir(obj.SavePath);
						CurSize=s.bytes;
						
						if ~isempty(TimeStruct.FileSize)
							if TimeStruct.FileSize(TimeIDpast)<CurSize;
								TimeStruct.FileSize(TimeIDpast)=CurSize;
							end
						else
							TimeStruct.FileSize(TimeIDpast)=CurSize;
						end
						
						TimeStruct.TotalTime=TimeStruct.TotalTime+TimeStruct.ModelTimes.(ModNamePast).TotalTimePerStep(TimeIDpast);
						
						
					end
					
					if ~isempty(CPUTimeData)
						%so much for the past, now the "now"
						TimeStruct.ModelTimes.(ModNameNow).ConfigTimePerStep(TimeIDnow)=TimeIn;
						TimeStruct.ModelTimes.(ModNameNow).ImportTimePerStep(TimeIDnow)=TimeImp;              
						TimeStruct.ModelTimes.(ModNameNow).CalcTimePerStep(TimeIDnow)=TimeCalcNow; %first estimate
						
						%AddonTimes if existing
						if ~isempty(AddTimeInfo)
							TimeStruct.ModelTimes.(ModNameNow).AddTimeInfo{TimeIDnow}=AddTimeInfo;
						end
					end
					
					obj.BenchmarkData=TimeStruct;
					
					
				case 'prepare'
					%ListNR should here be the model nr and CPUTimeData the time for preparation
										
					TimeStruct=obj.BenchmarkData;
					ModNameNow =obj.ModelNameList{ListNr}
					TimeStruct.ModelTimes.(ModNameNow).PrepareTime=CPUTimeData;
					
					obj.BenchmarkData=TimeStruct;
					
					
				case 'finish'
					%for the last step just updates the past calc step and file size
					
					%to be sure
					if ListNr==numel(obj.WorkList(:,1))
						TimeStruct=obj.BenchmarkData;
						TimeIDnow = obj.WorkList(ListNr,1);
						ModelIDnow = obj.WorkList(ListNr,2);
						ModNameNow = obj.ModelNameList{ModelIDnow}
						TimeCalcPast=obj.PastCalcTime(1);
						
						TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow)=TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow)+...
							TimeCalcPast+TimeStruct.ModelTimes.(ModNameNow).ConfigTimePerStep(TimeIDnow)+...
							TimeStruct.ModelTimes.(ModNameNow).ImportTimePerStep(TimeIDnow);
						
						%update CalcTime
						TimeStruct.ModelTimes.(ModNameNow).CalcTimePerStep(TimeIDnow)=TimeCalcPast; %better time
							
						%update TotalTime
						TimeStruct.ModelTimes.(ModNameNow).TotalTime=TimeStruct.ModelTimes.(ModNameNow).TotalTime+TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow);
						
						%update CalcTime
						TimeStruct.ModelTimes.(ModNameNow).CalcTime=TimeStruct.ModelTimes.(ModNameNow).CalcTime+TimeCalcPast;
						
						%update time per step (all models)
						TimeStruct.CalcTimeStep(TimeIDnow)=TimeStruct.CalcTimeStep(TimeIDnow)+TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow);
						
						%get filesize of last file
						s=dir(obj.SavePath);
						CurSize=s.bytes;
						
						if ~isempty(TimeStruct.FileSize)
							if TimeStruct.FileSize(TimeIDnow)<CurSize;
								TimeStruct.FileSize(TimeIDnow)=CurSize;
							end
						else
							TimeStruct.FileSize(TimeIDnow)=CurSize;
						end
						
						TimeStruct.TotalTime=TimeStruct.TotalTime+TimeStruct.ModelTimes.(ModNameNow).TotalTimePerStep(TimeIDnow);
						
						
						obj.BenchmarkData=TimeStruct;
						
					end
					
					
				
				
			end
			
			
        		
        		
        		
        	end
        	
        	
        
        	
        	
        	function stopCalc(obj)
        		%stop function used for stop calculation in progress
        		%over notification and so on, I don't now how well it 
        		%works.
        		disp(['Calculation has be stopped at ',num2str(obj.CalcProgress),'%']); 
        		obj.StopToggle=true;
        	end
        	
        	
        	
        	function IsSave = checkError(obj,ErrorCode,ListNr)
        		%Checks if the error specified allows to continue and
        		%registers the error as well as set the global error
        		%if needed
        		%Using this without ErrorCode and ListNr (or with an
        		%empty one) will cause the program to only the check
        		%the global error code
        		
        		%List of common errors:
        		%PlugIn:
        		%		0: no Error, everything fine
        		%		1: Model not or not correctly configurated
        		%		2: No catalog data defined
        		%		3: external function did not work
        		%		-1: generic not specified error
        		%Other error can be specified by the plugin
        		%
        		%Calculator:
        		%		-1: No project specified 
        		%		0: no Error, everything fine
        		%		1: some Models had errors, reasonable save to continue
        		%		2: one or more failed, but where isolated
        		%		3: over 50% overall failures
        		%		4: too many models failed
        		%		99: I/O problem with the ForecastProject object
        		
        		%NOTE: the error handling is quite complexe but it allows to 
        		%save a lot of time in case something goes wrong, and the 
        		%most times is probably spent in the models themself anyway.
        		
        		if nargin<2
        			ErrorCode=[];
        			ListNr=[];
        		end
        		
        		if isempty(ErrorCode)|isempty(ListNr)
        			%check the global error
        			IsSave=obj.ErrorCode==0|obj.ErrorCode==1|obj.ErrorCode==2|obj.ErrorCode==3;
        			if obj.ErrorCode~=0
        				notify(obj,'ErrorOccured');
        			end
        			
        			return
        			
        		end
        		
			%first write the code into the table
			obj.ErrorInCalc(ListNr)=ErrorCode; 
			
			%now check if the error itself is save
			SingleSave=ErrorCode==0;
			
			%now check the general error
			GeneralSave=obj.ErrorCode==0|obj.ErrorCode==1|obj.ErrorCode==2|obj.ErrorCode==3;
			
			if ~GeneralSave
				IsSave=false;
				notify(obj,'ErrorOccured')
				
			elseif ~SingleSave&obj.ErrorCode==0
				%probably the first Error
				IsSave=true;
				obj.ErrorCode=1;
				notify(obj,'ErrorOccured');
				
			elseif ~SingleSave&(obj.ErrorCode==1|obj.ErrorCode==2|obj.ErrorCode==3)
				%it is getting critical
				%check if it is still only 50% or is becoming more 
				%and more (it has to over 50% without the stopped models
				
				CopyError=obj.ErrorInCalc;
				if ~isempty(obj.CalculatorSpace.DefectModels)
					%models have been stopped, exclude those
					for i=1:numel(obj.CalculatorSpace.DefectModels)
						Places=obj.WorkList(:,2)==obj.CalculatorSpace.DefectModels(i);
						CopyError(Places)=nan;
					end
						
				end
				
				%How many are faulty
				DidNotWork=sum(CopyError~=0)-sum(isnan(CopyError));
				Existing=sum(~isnan(CopyError));
				
				
				
				%at least 10% of the work has to be done to issue
				%a total stop
				if ListNr>=0.1*numel(obj.WorkList(:,1))
					if DidNotWork>=Existing
						%it is unsave now
						IsSave=false;
						obj.ErrorCode=4;
					elseif DidNotWork>=0.5*Existing 
						IsSave=true;
						obj.ErrorCode=3;
					else
						IsSave=true;
						
						if ~isempty(obj.CalculatorSpace.DefectModels)
							obj.ErrorCode=2;
						else
							obj.ErrorCode=1;
						end
					end
					
					
					
					
				else
					if DidNotWork>=Existing
						%it is seems unsave but lets see later
						IsSave=true;
						obj.ErrorCode=3;
					elseif DidNotWork>=0.5*Existing 
						IsSave=true;
						obj.ErrorCode=3;
					else
						IsSave=true;
						
						if ~isempty(obj.CalculatorSpace.DefectModels)
							obj.ErrorCode=2;
						else
							obj.ErrorCode=1;
						end
					end
				
				end
				
				%check also if a model continues to fail 
				leftOverModel=unique(obj.WorkList(~isnan(CopyError),2));
				for i=1:numel(leftOverModel)
					%also here at least 10% of total Nr for the 
					%specific models have to be tried before a 
					%model is considered to fail.
					ErrorsOfMod=CopyError(obj.WorkList(:,2)==leftOverModel(i));
					
					Failed=sum(leftOverModel~=0)-sum(isnan(leftOverModel));
					%calced=sum(leftOverModel==0)-sum(isnan(leftOverModel));
					totalCalced=sum(~isnan(leftOverModel));
					
					if totalCalced>=0.1*numel(ErrorsOfMod)
						if Failed>=totalCalced
							%Exterminate
							obj.CalculatorSpace.DefectModels(end+1)=i;
							
							if obj.ErrorCode~=3
								obj.ErrorCode=2;
							end
						end
					end
					
				end
				
				notify(obj,'ErrorOccured')
					
				
				
				
						
			else
				IsSave=true;
			end
			
		
			
        		
        		
        	end
        	
        	
        	function newPath = archiveFile(obj,FilePath,ForeTime)
        		%This function will move the original file to the 
        		%archive folder and rename it by adding a datenum-based
        		%string to the end of it.
        		%The names of the files produced by different model
        		%should be different, or else they will be overwritten
        		%A possible way could be to add a version string to the 
        		%file name, like this no file would be overwritten even
        		%with the same model used with different config
        		
        		newPath=[];
        		
        		if nargin<3
        			ForeTime=[];
        		end
        		
        		if isempty(ForeTime)
        			ForeTime=now;
        		end
        		
        		fs=filesep;
        		
        		%digest input
        		[pathstr, name, ext] = fileparts(FilePath);
        		
        		%generate random 3-digit hex number, like this there will
        		%be 4096, this gives a lot of possible forecasts a day
        		RawChar='0123456789ABCDEF'; %Hex
        		HexAddon=RawChar(randi(15,1,3)+1);
        		
        		%get some of the digits in the datenum out
        		yr200=floor(ForeTime/(365*200))*(365*200);
        		TimeStr=num2str(round((ForeTime-yr200)*100));
        		
        		%generate new name
        		newName = [name,'_',TimeStr,'_',HexAddon];
        		
        		CoolPlace=[obj.ArchivePath,fs,newName,ext];
        		
        		
        		%try moving it 
        		CheckExist=exist(FilePath)
        		
        		if CheckExist==0
        			warning('File not existing');
        			newPath=[];
        		else
        			if isunix
        				%the command
        				Ucommand=['mv -f ',FilePath,' ',CoolPlace];
        				[stat, res] = unix(Ucommand);
        				
        				if stat==0
        					newPath=CoolPlace;
        				else
        					newPath=[];
        					warning('Could not move file');
        				
        				end
        			
        			else
        				%the command
        				Dcommand=['move /Y ',FilePath,' ',CoolPlace];
        				[stat, res] = dos(Dcommand);
        				
        				if stat==0
        					newPath=CoolPlace;
        				else
        					newPath=[];
        					warning('Could not move file');
        				
        				end		
        			
        			end
        		
        		end
        		
        		
        		
        	end
        	
        	
        	
        	
        	
        	function finishCalc(obj)
        		%everything correctly finished, so clean up, set the 
        		%correct state of the project and store it one last time
        		
        		
        		%last thing to benchmark
        		if obj.UseTimer
        			obj.AddBenchTimes([],numel(obj.WorkList(:,1)),'finish');
        			obj.ForecastProject.setBenchmark(obj.BenchmarkData);
        		end
        		
        		%first reset all calculations
        		UsedModelID=unique(obj.WorkList(:,2));
        		
        		for i=1:numel(UsedModelID)
        			%get model plugin, ID could be sended directly, but
        			%it saver like this (avoids mixups)
        			ModelName=obj.ModelNameList{UsedModelID(i)};
        			[curModel,Pos] = obj.ForecastProject.getModel(ModelName);
        			
       	 			%reset model
        			curModel.ResetFullCalc;
               		end
               		
               		%build new CalcSpace with only the needed info
               		genericSpace=struct(	'CalculatorID','origin_v1',...
               					'CalcName',obj.Name,...
        					'CalcID',obj.ID,...
        					'InternalCatSel',{{}},...
        					'DefectModels',obj.CalculatorSpace.DefectModels,...
        					'NextCalc',obj.NextCalc,...   
        					'CalcProgress',obj.CalcProgress,...
        					'CalcStatus','finished',...
        					'ErrorCode',obj.ErrorCode,...
        					'LastCalcTime',obj.CalculatorSpace.LastCalcTime,...
        					'Finished',true,...
        					'FinishedCalc',now);
               		
               		%now kick out everything not needed in the project
               		obj.ForecastProject.UpdateCalcSpace(genericSpace);
               		
               		%Save it one last time
               		ForecastProject=obj.ForecastProject;
               		save(obj.SavePath,'ForecastProject');
               		disp('saved the last time')
               		
               		%notify everybody
               		obj.ErrorCode=0; %everything is fine
               		disp('Calculation was finished normally')
               		notify(obj,'CalcUpdate');
               		
        	
        	
        	end
        	
        	
        	function pauseCalc(obj)
        		%similar to the finishCalc, but does not clean up, it is 
        		%supposed to be used in case where there is a fatal error
        		%or in case where the calculation was stopped.
        		
        		%no error notification and errorcode here because it may 
        		%not an error but just a pause, also the error handling
        		%should have been done elsewhere
        		
        		%update Calculator space
        		obj.CalculatorSpace.CalcStatus='started';
        		obj.CalculatorSpace.CalcName=obj.Name;   
        		obj.CalculatorSpace.CalcID=obj.ID;   
        		obj.CalculatorSpace.NextCalc=obj.NextCalc;   
        		obj.CalculatorSpace.CalcProgress=obj.CalcProgress
        		obj.CalculatorSpace.LastCalcTime=now;
        		obj.CalculatorSpace.Finished=false;
                	obj.CalculatorSpace.ErrorCode=obj.ErrorCode;
                		
        		%update file
        		obj.ForecastProject.UpdateCalcSpace(obj.CalculatorSpace);
        		
        		
        		ForecastProject=obj.ForecastProject;
               		save(obj.SavePath,'ForecastProject');
               		
               		%notify everybody
               		disp('Calculation was has been stopped')
               		notify(obj,'CalcUpdate');
        		
        	end
        	
        	
        	
        	function storeCurrentResults(obj)
        		%brings the ForecastProject "up-to-date" (add stuff to 
        		%continue later) and stores it as file in the predefined
        		%path
        		
        		if isempty(obj.SaveEveryStep)
        			obj.SaveEveryStep=true;
        		end
        		
        		%update Calculator space
        		obj.CalculatorSpace.CalcStatus='started';
        		obj.CalculatorSpace.CalcName=obj.Name;   
        		obj.CalculatorSpace.CalcID=obj.ID;   
        		obj.CalculatorSpace.NextCalc=obj.NextCalc;
        		obj.CalculatorSpace.CalcProgress=obj.CalcProgress
        		obj.CalculatorSpace.LastCalcTime=now;
        		obj.CalculatorSpace.Finished=false;
                	obj.CalculatorSpace.ErrorCode=obj.ErrorCode;
                		
        		%update file
        		obj.ForecastProject.UpdateCalcSpace(obj.CalculatorSpace);
        		
        		
        		if ~obj.SaveEveryStep
        			obj.SaveCount=obj.SaveCount-1;
        			
        			if obj.SaveCount<=0
        				%save file
					ForecastProject=obj.ForecastProject;
					save(obj.SavePath,'ForecastProject');
					obj.SaveCount=obj.SaveOften;
					disp('saved Forecasts');
				
				else
					disp(['Will save in ',num2str(obj.SaveCount), 'steps']);
        			end
        		
        		else
        			%save file
        			ForecastProject=obj.ForecastProject;
        			save(obj.SavePath,'ForecastProject');
        		
        		end
        		
        		
        		
        		
        	end
        	
        	
        	function [TimeInt Range] = getLearningTime(obj,TimeID)
        		%returns the start and end of the learning time for a certain
        		%forecast
        		%OutPut: [startLearn, endLearn] and the range as keyword
        		
        		%get the key values for this
        		LearningPeriod=obj.ForecastProject.LearningPeriod;
        		LearningUpdate=obj.ForecastProject.LearningUpdate;
        		LearningLength=obj.ForecastProject.LearningLength;
        		LearningLag=obj.ForecastProject.LearningLag;
        		
        		if TimeID==0
        			%initial learning period
        			TimeInt = LearningPeriod;
        			
        			if all(isinf(TimeInt))
        				Range='all';
        			elseif 	isinf(TimeInt(1))
        				Range='before';
        			elseif 	isinf(TimeInt(2))
        				Range='after';
        			else
        				Range='in';
        			end
        			
        			return
        		end
        		
        		
        		DoUpdate=strcmp(LearningUpdate,'update');
        		FullLength=isstr(LearningLength);
        		
        		%now filtering applied, it is just lowest possible time
        		eventStruct=obj.ForecastProject.Datastore.getFields;
        		
        		mostAcient=LearningPeriod(1);
        		curCastStart=obj.TimeList(TimeID);
        		
        		if DoUpdate&FullLength
        			TimeInt = [mostAcient curCastStart-LearningLag];
        		elseif DoUpdate
        			TimeInt = [curCastStart-LearningLength curCastStart]-LearningLag;
        		else
        			%fixed time
        			TimeInt = LearningPeriod;
        		end
        			
        		if all(isinf(TimeInt))
        			Range='all';
        		elseif 	isinf(TimeInt(1))
        			Range='before';
        		elseif 	isinf(TimeInt(2))
        			Range='after';
        		else
        			Range='in';
        		end
        			
        		
        	end
        	
        	
        	function ResetDefectModels(obj)
        		%empties the defect models space.
        		obj.CalculatorSpace.DefectModels=[];
        		
        	end
        	
        
        end
        
        
end        

function [ForecastModel,Pos] = getModel(obj,ModelID)
	%I think the name says it all	
	%ModelID can either be the full name as registered in
	%the structure or just the index in the list
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if ~isempty(obj.ModelPlugIns)
		%generate name list
		for i=1:numel(obj.ModelPlugIns(:,1))
			ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
		end
		
		
		if iscell(ModelID)
			ForecastModel={};
			Pos={};
			for i=numel(ModelID)
				if isstr(ModelID{i})
					IDX=find(strcmp(ModelID{i},ModelNames));
				else
					%has to be numeric
					IDX=ModelID{i};
				end
			
				ForecastModel{i}=obj.ModelPlugIns{IDX(1),3};
				Pos{i}=IDX(1);
			end
		
		else
			ForecastModel=[];
			Pos=[];
			
			if isstr(ModelID)
				IDX=find(strcmp(ModelID,ModelNames));
			else
				%has to be numeric
				IDX=ModelID;
			end
			
			ForecastModel=obj.ModelPlugIns{IDX(1),3};
			Pos=IDX(1);

		end
	
	else
		error('No model stored')
		
	end
	
end
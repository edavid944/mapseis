function setParameter(obj,Parameter,Value);
	%universal set routine
	%both input can be cell array to add several parameters 
	%at once
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if ~iscell(Parameter)
		obj.(Parameter)=Value;
	else
		for i=1:numel(Parameter);
			obj.(Parameter{i})=Value{i};
		end
	end
	
	
	switch obj.ProjectStatus
		case 'empty'
			obj.ProjectStatus='config';
		case {'ready','started','finished','cont'}
			obj.ProjectStatus='changed';
		case 'changed'
			obj.checkState;
	end

end
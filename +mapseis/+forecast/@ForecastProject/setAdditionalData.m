function ErrorCode = setAdditionalData(obj,ModelID,AddonData)
	%this function will store the additionalData in a appropriate
	%structure, but it will not be managed in time by the project 
	%as with results and logs.


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.forecast.*;
	
	%just for start, in case something goes wrong
	ErrorCode=-1
	
	%find correct model, this is best done with the name to avoid mix-ups
	%due to different order of the models
	
	%generate name list (sooner or later I have to add this as property)
	for i=1:numel(obj.ModelPlugIns(:,1))
		ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
	end
	
	if isstr(ModelID)
		IDX=find(strcmp(ModelID,ModelNames));
		MName=ModelID;
	else
		%has to be numeric, I keep it in but strongly suggest
		%to use names instead of ID 
		IDX=ModelID;
		MName=ModelNames{IDX};
	end
	
	if isempty(IDX)
		%model not found -> Code 1
		ErrorCode=1;
		return
	end
	
	obj.AdditionalData.(MName)=AddonData;
		      		       		
end
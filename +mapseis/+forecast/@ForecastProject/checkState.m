function checkState(obj)
	%checks if all the parameters are set 


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.*;
	
	%check if everthing is completed
	EmptyList={'CalcProgress','DataMode','Datastore','Filterlist',...
	'LearningFilter','ModelPlugIns','ModelConfig','TestPolygon',...
	'Depths','Magnitudes','Spacings','ForecastGrid','ForecastLength',...
	'PredictionIntervall','UpdateMag','StartTime','EndTime',...
	'AvailableTimes','LearningPeriod','LearningUpdate','StoreProject',...
	'LearningLength','LearningLag','ArchivePath','CalcPath'}
	
	Complete=true;
	TotalEmpty=true;
	for i=1:numel(EmptyList)
		Complete=Complete&~isempty(EmptyList{i});
		TotalEmpty=TotalEmpty&isempty(EmptyList{i});
	end
	
	if TotalEmpty
	obj.ProjectStatus='empty';
	return;
	end
	
	
	if Complete
		%check if the structures are also correct
		%(MetaData is in a way option and will not be 
		%enforced)
		StillComplete=	~isempty(obj.ProjectConfig.ArchiveForecasts)&...
						~isempty(obj.ProjectConfig.StoreLogs)&...
						~isempty(obj.ProjectConfig.ArchiveLogs)&...
						~isempty(obj.ProjectConfig.SaveConfig)&...
						~isempty(obj.ProjectConfig.CatalogStore);
		
		%maybe later a check for sensible values would also be an option
		Complete=Complete&StillComplete;
		
		
		
		%set the state right 
		if Complete
			switch obj.ProjectStatus
				case 'changed'
					obj.ProjectStatus='recalc';
					%It is suggested to update  
					%the progress variable with
					%correctProgress
				case {'config','empty'}
					obj.ProjectStatus='ready';	
				
			end	
		
		else
			switch obj.ProjectStatus
				case {'recalc','finished','cont'}
					obj.ProjectStatus='changed';
					%It is suggested to update  
					%the progress variable with
					%correctProgress
				case 'ready'
					obj.ProjectStatus='config';	
			
			end	
		end
		
	end

end
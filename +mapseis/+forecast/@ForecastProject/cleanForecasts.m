function cleanForecasts(obj,FullClean)
	%removes every forecast not up-to-date or removed from the
	%list from the forecast objects and all related list and objects
	%it also adds empty forecast objects to new models in case it has
	%not been done before
	
	%If FullClean true, also forecast which are not anymore in the timerange
	%will be removed (set to 2 in the ProgressStructur) 


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.forecast.*;
	
	if nargin<2
		FullClean=false;
	end
	
	if ~isempty(obj.ModelPlugIns)
		%generate name list
		for i=1:numel(obj.ModelPlugIns(:,1))
			CurModel=obj.ModelPlugIns{i,3}
			
			if isempty(obj.ModelPlugIns{i,1})
				warning('Model not correctly added, please run "correctProgress"')
				continue;
			end
		
			ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
			
			if isempty(obj.Forecasts{i,1})
				%no forecast -> create one 
				NewForecast=obj.ForecastGrid.cloneWars;
				NewForecast.setName(ModelNames{i});
				
				ModelFeature=CurModel.getFeatures;
			
				if ~strcmp(ModelFeature.ForecastType,'Selectable')
					NewForecast.initForecast(ModelFeature.ForecastType,[]);
				
				else      					
					disp('variable ForecastType, has to be set later');
				end
			
				ForecastID=NewForecast.getID;
				obj.Forecasts{i,1}=NewForecast;
			
				%Log object
				NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
				NewLogObj.InfoType='logs';
				obj.OutputLogs{i,1}=NewLogObj;
				
				%UsedConfig Object
				NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
				NewLogObj.InfoType='config';
				obj.UsedConfigs{i,1}=NewLogObj;
				
				if strcmp(obj.PredictionIntervall,'realtime')	
					obj.Forecasts{i,1}.setRealTimeMode(true);
					obj.OutputLogs{i,1}.setRealTimeMode(true);
					obj.UsedConfigs{i,1}.setRealTimeMode(true);
				end
	
			else
				if ~isfield(obj.CalcProgress.ModelProgress,ModelNames{i});
					warning('Model not correctly added, please run "correctProgress"')
					continue;
				end
		
				ProgressVec=obj.CalcProgress.ModelProgress.(ModelNames{i});
				
				if obj.Forecasts{i,1}.EmptyGrid;
					%it is empty -> check if Logs and Configs are
					%existing
					ForecastID=obj.Forecasts{i,1}.getID;
					if isempty(obj.OutputLogs{i,1})
						NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
						NewLogObj.InfoType='logs';
						obj.OutputLogs{i,1}=NewLogObj;
					end
					
					if isempty(obj.UsedConfigs{i,1})
						NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
						NewLogObj.InfoType='config';
						obj.UsedConfigs{i,1}=NewLogObj;
					end
			
					
					if strcmp(obj.PredictionIntervall,'realtime')	
						obj.OutputLogs{i,1}.setRealTimeMode(true);
						obj.UsedConfigs{i,1}.setRealTimeMode(true);
					end
					
					disp('Empty Forecast nothing to add/delete')
					continue;
		
				else
					if all(ProgressVec==0)
						%nothing should be calculated or all can be deleted
						%->reset forecasts
						NewForecast=obj.ForecastGrid.cloneWars;
						NewForecast.setName(ModelNames{i});
						
						NewForecast.initForecast(obj.Forecasts{i,1}.ForecastType.Type,[]);
						ForecastID=NewForecast.getID;
						obj.Forecasts{i,1}=NewForecast;
						
						%NOTE: the addon objects are build wether they are used or not
						%like this a on/off switching will be easier. But in case
						%it causes performance problems I will check before creating
						
						%Log object
						NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
						NewLogObj.InfoType='logs';
						obj.OutputLogs{i,1}=NewLogObj;
						
						%UsedConfig Object
						NewLogObj=ForecastLogObj(ModelNames{i},ForecastID);
						NewLogObj.InfoType='config';
						obj.UsedConfigs{i,1}=NewLogObj;
						
						%The rest as well, there might be something in it
						obj.Forecasts{i,2}=[];
						obj.OutputLogs{i,2}=[];
						%obj.UsedConfigs{i,2}=[];
						
						if strcmp(obj.PredictionIntervall,'realtime')	
							obj.Forecasts{i,1}.setRealTimeMode(true);
							obj.OutputLogs{i,1}.setRealTimeMode(true);
							obj.UsedConfigs{i,1}.setRealTimeMode(true);
						end
		
					elseif all(ProgressVec==1)
						disp('Nothing to do')
		
					else
						if isempty(obj.CalcProgress.Times)
							MyTime=obj.AvailableTimes;
						else
							MyTime=obj.CalcProgress.Times;
						end
		
						RawLogicSelect=false(size(ProgressVec));
						
						for j=1:numel(ProgressVec)
							if ProgressVec(j)==0|(ProgressVec(j)==2&FullClean)
								%Forecast
								try 
									obj.Forecasts{i,1}.removeForecast(MyTime(j));
								end
								
								%Logs
								try 
									obj.OutputLogs{i,1}.removeLog(MyTime(j));
								end
		
								%Configs
								try 
									obj.UsedConfigs{i,1}.removeLog(MyTime(j));
								end
								
								SelVec=RawLogicSelect;
								SelVec(j)=true;
		
								%remove arrays with filepaths in case there are existing
								if ~isempty(obj.Forecasts{i,2})
									obj.Forecasts{i,2}=obj.Forecasts{i,2}(~SelVec);
								end						
		
								if ~isempty(obj.OutputLogs{i,2})
									obj.OutputLogs{i,2}=obj.OutputLogs{i,2}(~SelVec);
								end
		
	
								%in case it was 2
								ProgressVec(j)=0;
		
							end
						end %for end
					end
		
				end
			end
	
		end %for end
	
	else
		warning('No model set yet')
	
	end
        		
end %function end
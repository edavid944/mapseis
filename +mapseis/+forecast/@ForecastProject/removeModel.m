function removeModel(obj,ModelID)
	%it removes a model and everything surrounding it
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if ~isempty(obj.ModelPlugIns)
		%generate name list
		for i=1:numel(obj.ModelPlugIns(:,1))
			ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
		end
		
		RawSelRay=false(size(obj.ModelPlugIns(:,1)));
		
		if iscell(ModelID)
			for i=numel(ModelID)
				if isstr(ModelID{i})
					IDX=find(strcmp(ModelID{i},ModelNames));
				else
					%has to be numeric
					IDX=ModelID{i};
				end
		
				if ~isempty(IDX)
					Pos=RawSelRay;
					Pos(IDX(1))=true;
					ModNamOld=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
					
					if obj.ProjectConfig.ArchiveDelete
						%Archive the old forecasts if existing
						try
							CalcToArch=obj.CalcProgress.ModelProgress.(ModNamOld);
							CalcToArch(CalcToArch==0)=-1;
							CalcToArch(CalcToArch==1)=2;
						catch
							CalcToArch=[];
						end
					
						if ~isempty(obj.UsedConfigs)
							UseConf=obj.UsedConfigs{i};
						else
							UseConf=[];
						end
						
						if ~isempty(obj.OutputLogs)
							TheLogs=obj.OutputLogs{i};
						else
							TheLogs=[];
						end
			
						RIP={ModNamOld,CalcToArch,obj.Forecasts{i},UseConf,TheLogs};
						
						%Rest in peace
						obj.MArchive{end+1}=RIP;
								
					end
			
					%Now Remove
					obj.ModelPlugIns=obj.ModelPlugIns(~Pos,:);
					obj.ModelConfig=obj.ModelConfig(~Pos,:);
					obj.Forecasts=obj.Forecasts(~Pos,:);
					obj.UsedConfigs=obj.UsedConfigs(~Pos,:);
					obj.OutputLogs=obj.OutputLogs(~Pos,:);
					
					try
						obj.CalcProgress.ModelProgress=rmfield(obj.CalcProgress.ModelProgress,ModNamOld);
					end
		
				else
					warning('Model not found')
				end
	
			end
		
		else	
			if isstr(ModelID)
				IDX=find(strcmp(ModelID,ModelNames));
			else
				%has to be numeric
				IDX=ModelID;
			end
		
			if ~isempty(IDX)
				Pos=RawSelRay;
				Pos(IDX(1))=true;
				ModNamOld=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
				
				if obj.ProjectConfig.ArchiveDelete
					%Archive the old forecasts if existing
					try
						CalcToArch=obj.CalcProgress.ModelProgress.(ModNamOld);
						CalcToArch(CalcToArch==0)=-1;
						CalcToArch(CalcToArch==1)=2;
					catch
						CalcToArch=[];
					end
		
		
					if ~isempty(obj.UsedConfigs)
					UseConf=obj.UsedConfigs{i};
					else
					UseConf=[];
					end
					
					if ~isempty(obj.OutputLogs)
					TheLogs=obj.OutputLogs{i};
					else
					TheLogs=[];
					end
					
					RIP={ModNamOld,CalcToArch,obj.Forecasts{i},UseConf,TheLogs};
					
					%Rest in peace
					obj.MArchive{end+1}=RIP;
					
				end			
		
				%Now Remove
				obj.ModelPlugIns=obj.ModelPlugIns(~Pos,:);
				obj.ModelConfig=obj.ModelConfig(~Pos,:);
				obj.Forecasts=obj.Forecasts(~Pos,:);
				obj.UsedConfigs=obj.UsedConfigs(~Pos,:);
				obj.OutputLogs=obj.OutputLogs(~Pos,:);
				
				try
				obj.CalcProgress.ModelProgress=rmfield(obj.CalcProgress.ModelProgress,ModNamOld);
				end
				
			else
				warning('Model not found')
		
			end
	
		end
		
		%correct the Progress list -> this will also add some of 
		%the missing parts
		%->Not needed, should have been taken care of
		%obj.correctProgress;
		
		
	else
		error('No model in storage, nothing deleted')
		
	end	

end
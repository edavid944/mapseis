function [ForecastModels,Names,Pos] = getAllModels(obj)
	%same as getModel but returns all models at once
	
	%it could be done more directely, but this may be better if there
	%are some changes in the model storage, and should not matter to much
	%anyway.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	%generate name list
	for i=1:numel(obj.ModelPlugIns(:,1))
		Names{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
	end
	
	%get models with getModel
	[ForecastModels,Pos] = obj.getModel(Names);
		
end
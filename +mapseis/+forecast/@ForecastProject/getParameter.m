function Value = getParameter(obj,Parameter)
	%more for completeness, currently it would be possible
	%to access the values without this method, but it may
	%change.


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if ~iscell(Parameter)
		Value=obj.(Parameter);
	else
		for i=1:numel(Parameter);
			Value{i}=obj.(Parameter{i});
		end
	end

end
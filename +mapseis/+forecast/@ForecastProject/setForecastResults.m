function ErrorCode = setForecastResults(obj,TimeStep,ModelID,ForecastData,DataMask,LogData,ConfigData)
	%this function will store the handed in Forecast and additional data to the
	%right place in the project. 
	%ErrorCode is a Error variable, it is 0 if everything worked and something else
	%if not.


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.forecast.*;
	
	%just for start, in case something goes wrong
	ErrorCode=-1
	
	%find correct model, this is best done with the name to avoid mix-ups
	%due to different order of the models
	
	%generate name list (sooner or later I have to add this as property)
	for i=1:numel(obj.ModelPlugIns(:,1))
		ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
	end
	
	if isstr(ModelID)
		IDX=find(strcmp(ModelID,ModelNames));
	else
		%has to be numeric, I keep it in but strongly suggest
		%to use names instead of ID 
		IDX=ModelID;
	end
	
	if isempty(IDX)
		%model not found -> Code 1
		ErrorCode=1;
		return
	end
	
	%To be sure check if timestep exist
	if ~any(obj.AvailableTimes==TimeStep)
		ErrorCode=2;
		return
	end
	
	TimeID=find(obj.AvailableTimes==TimeStep);
	
	%check if a forecast object is existing if not create one.
	if isempty(obj.Forecasts{IDX,1})
		%no forecast -> create one 
		CurModel=obj.ModelPlugIns{IDX,3}
		NewForecast=obj.ForecastGrid.cloneWars;
		NewForecast.setName(ModelNames{IDX});
		
		ModelFeature=CurModel.getFeatures;
		
		if ~strcmp(ModelFeature.ForecastType,'Selectable')
			NewForecast.initForecast(ModelFeature.ForecastType,[]);
		
		else      					
			%disp('variable ForecastType, has to be set later')
			ModelPram=CurModel.getModelConfig
			NewForecast.initForecast(ModelPram.ForecastType,[])
		end
		
	
		
		ForecastID=NewForecast.getID;
		obj.Forecasts{IDX,1}=NewForecast;
		
		
		%NOTE: the addon objects are build wether they are used or not
		%like this a on/off switching will be easier. But in case
		%it causes performance problems I will check before creating
		
		%Log object
		NewLogObj=ForecastLogObj(ModelNames{IDX},ForecastID);
		NewLogObj.InfoType='logs';
		obj.OutputLogs{IDX,1}=NewLogObj;
		
		%UsedConfig Object
		NewLogObj=ForecastLogObj(ModelNames{IDX},ForecastID);
		NewLogObj.InfoType='config';
		obj.UsedConfigs{IDX,1}=NewLogObj;
		
		if strcmp(obj.PredictionIntervall,'realtime')	
			obj.Forecasts{IDX,1}.setRealTimeMode(true);
			obj.OutputLogs{IDX,1}.setRealTimeMode(true);
			obj.UsedConfigs{IDX,1}.setRealTimeMode(true);
		end
		
	end
	
	%to be sure, check if the forecast object has initialized
	if ~obj.Forecasts{IDX,1}.ForecastReady
		CurModel=obj.ModelPlugIns{IDX,3}
						
		ModelFeature=CurModel.getFeatures;
		
		if ~strcmp(ModelFeature.ForecastType,'Selectable')
			obj.Forecasts{IDX,1}.initForecast(ModelFeature.ForecastType,[]);
		
		else      					
			%disp('variable ForecastType, has to be set later')
			ModelPram=CurModel.getModelConfig
			obj.Forecasts{IDX,1}.initForecast(ModelPram.ForecastType,[])
		end
	
		if strcmp(obj.PredictionIntervall,'realtime')	
			obj.Forecasts{IDX,1}.setRealTimeMode(true);
		end
	end


	%Now everything should be ready to go, finally
	try
		obj.Forecasts{IDX,1}.addForecast(ForecastData{1},TimeStep,obj.ForecastLength,DataMask);
	catch
		obj.Forecasts{IDX,1}.replaceForecast(ForecastData{1},TimeStep,obj.ForecastLength,DataMask);
		warning('old forecast has been overwritten')
	end	
	
	
	if obj.ProjectConfig.ArchiveForecasts
		obj.Forecasts{IDX,2}{TimeID,1}=ForecastData{2};
	end
	
	
	if obj.ProjectConfig.StoreLogs
		try
			obj.OutputLogs{IDX,1}.addLog(LogData{1},TimeStep,obj.ForecastLength);
		catch
			obj.OutputLogs{IDX,1}.replaceLog(LogData{1},TimeStep,obj.ForecastLength);
			warning('old forecast log has been overwritten')
		end	
	
	end
	
	
	if obj.ProjectConfig.ArchiveLogs
		obj.OutputLogs{IDX,2}{TimeID,1}=LogData{2};
	end
	
	
	if obj.ProjectConfig.SaveConfig
		try
			obj.UsedConfigs{IDX,1}.addLog(ConfigData,TimeStep,obj.ForecastLength);
		catch
			obj.UsedConfigs{IDX,1}.replaceLog(ConfigData,TimeStep,obj.ForecastLength);
			warning('old forecast log has been overwritten')
		end		
	end
	
	%update the progress 
	if ~isempty(obj.CalcProgress.Times)
		TheTimes=obj.CalcProgress.Times;
	else
		TheTimes=obj.AvailableTimes;
	end
	
	TheName=ModelNames{IDX};
	TimePos=TimeStep==TheTimes;
	if ~isfield(TheName,obj.CalcProgress.ModelProgress);
		%you never know
		obj.CalcProgress.ModelProgress.(TheName)=zeros(size(TheTimes));
	end
	
	if ~isempty(ForecastData{1})
		obj.CalcProgress.ModelProgress.(TheName)(TimePos)=1;
	else
		%probably an error in calc ->-2
		obj.CalcProgress.ModelProgress.(TheName)(TimePos)=-2;
	end
	
	%if it got up to here it terminated like it should
	ErrorCode=0;

end
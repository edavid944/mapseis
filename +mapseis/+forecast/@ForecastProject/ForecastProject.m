classdef ForecastProject < handle
	%This the first version of the forecast project object. The object is 
	%more less a data structure with a some needed functionallity. The 
	%actuall work will be done by the ForecastCalculater and the set up of 
	%this object will be done by the ForeProjectBuilder

	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	properties(SetAccess = private)
		Name
		ID
		ObjVersion
		MetaData
		ProjectStatus
		CalcProgress
		
		
		DataMode
		Datastore
		Filterlist
		LearningFilter
		
		ModelPlugIns
		ModelConfig
		InputCatalogs
		Forecasts
		MArchive
		OutputLogs
		UsedConfigs
		
		TestPolygon
		BoundBox
		Depths
		Magnitudes
		Spacings
		ForecastGrid
		AutoExtend
		RegionExtend
		ForecastLength
		PredictionIntervall
		UpdateMag
		StartTime
		EndTime
		CurrentEndTime
		AvailableTimes
		
		LearningPeriod
		LearningUpdate
		LearningLength
		LearningLag
		
		ArchivePath
		CalcPath
		StoreProject
		ProjectConfig
		
		LastParameters
		
		CalculatorSpace
		BenchmarkData
		AdditionalData
		LogObject
	
	end
   

    
methods
		function obj=ForecastProject(Name)
			%object constructor and a lot of explantation about
			%the different parameters
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Name=[];
			end
			
			if isempty(Name)
			%A bit gimmicky I admit, but could be useful later
			Name=RawChar(randi(15,1,8)+1);
			end
			
			
			%Meta data and progress 
			obj.Name=Name;
			obj.ID=RawChar(randi(15,1,8)+1);
			obj.ObjVersion='0.9';
			obj.MetaData= struct(	'author',[[]],...
									'version',[[]],...
									'created',[[]],...
									'updated',[[]],...
									'comment',[[]]);
			obj.ProjectStatus = 'empty';
			obj.CalcProgress = struct(	'ModelProgress',[[]],...
										'StopProgress',[[]],...
										'Times',[[]]);
			
			%Name: Name of the project no surprise
			
			%ID: an unique ID which can be used for identifiaction
			
			%ObjVersion: is just the version of the object in used 
			%internally
			
			%MetaData: is a structure containing parameters like 
			%author, version ,creation date, last update and comment
			
			%ProjectStatus: is a single Keyword which gives the status
			%of the project, it can be:
			%	'empty': empty object nothing in it
			%	'config': configuration started but not parts are
			%		  missing
			%	'ready': everything is configurated and the project
			%		 is ready for calculation
			%	'started': calculation has started but is not 
			%		   is not finished
			%	'cont':	the project is finished calculation but 
			%		is for the moment but has to be updated
			%		when new data is available (only 
			%		continues mode)
			%	'finished': everything is calculated and the 
			%		    project is ready for testing
			%	'changed': project was 'finished' or 'started'
			%		   was edited afterwards (e.g. new model
			%		   added), the project has to be re-checked
			%		   and calculation has to be restarted.
			%	'recalc': the project was changed and is ready to 
			%		  recalculated
			
			%CalcProgress: This structure contains everything about the
			%progress of the calculation it is a structure with multiple
			%fields not all defined yet. It will consist out of two main
			%fields: 'ModelProgress' and 'StopProgess'. Both will contain
			%themself a structure consisting out of every model as fieldname
			%together with version number. The in the field 'ModelProgress' 
			%the progress of the calculation will be stored for each time
			%step: 0 means not calculated, 1 means calculated, and 2 means
			%calculated but model or timestep has been removed from the 
			%project (can be reactivated) and -1 means the model was never 
			%calculated and has be removed. In the second field each step is
			%a boolean value which is true if the model is activated or 
			%false if the model is deactivated and should not be calculated
			%for this timestep. The field 'ModelProgress' is normally auto-
			%matically managed by the projectbuilder and the function which 
			%does this is integrated in this object. The field 'StopProgress'
			%is a manually managed field and its purpose is to allow the 
			%user to stop calculation for certain step, for instance to stop
			%model which has an updated version from being calculated without
			%deleting the whole model.
			%The field time is similar to the available times but is expanded to
			%the range of progress vector (for those cases the time range was
			%shortened after calculation)
			%New ModelProgress code -2: A calculation was attended but failed
			%UNFINISHED: -2 may not be fully integrated right now, I have to 
			%check where a change is needed
			
			
			%Catalog data
			obj.DataMode='Datastore';
			obj.Datastore=[];
			obj.Filterlist=[];
			obj.LearningFilter=[];
			%DataMode: sets the format of Datastore, if this is 
			%'Datastore', the Datastore property should be a object
			%of the type datastore (typical for mapseis). If the 
			%keyword is 'Source' obj.Datastore should be a source-
			%object (interface/abstract will be provided) which 
			%generates a Datastore catalog from a online (or offline)
			%source or provides datastore similar access.
			
			%Datastore: as described before
			
			%Filterlist: the filterlist contains the selection of the 
			%earthquakes in the catalog as well as the test region 
			%polygon (see filterlist for more details). It is of course
			%also possible to produce a dummy object, which provides
			%only the needed parts with the same commands as a normal 
			%filterlist
			
			%LearningFilter: this is optional, if a filterlist is added
			%the learning period will use a different "region" than the
			%forecast target. The builder will often create a learning
			%filter itself if needed (very often the case)
			%IMPORTANT: It is the LearningFilter which actually creates
			%the input catalog for the models. The Filterlist property
			%will be used mostly for testing. 
			
			
			
			%The models and the "storage area" for input and output 
			obj.ModelPlugIns={};
			obj.ModelConfig={};
			obj.InputCatalogs={};
			obj.Forecasts={};
			obj.MArchive={};
			obj.OutputLogs={};
			obj.UsedConfigs={};
			%ModelPlugIns: this is a cell array with one column 
			%containing the names second one containing the versions 
			%of the model and the third one containing the model plugin 
			%objects (they have to be configurated at least partially). 
			%See the ForecastPlugIn interface/abstract for more 
			%information
			
			%ModelConfig: contains the basic configuration of the 
			%different models in form of a cell array, each cell 
			%containing a structure with the parameters as needed by
			%the model in the first column and in the second column the
			%ConfigID is stored.
			
			%InputCatalogs: here the actually used catalogs for the 
			%forecast calculation are stored (format may vary from
			%datastore to ShortCat to boolean selection vector) 
			
			%Forecasts: the resulting forecast from a calculation are
			%stored here. It is a cell array with two column, first 
			%one storing the ForecastObj for each model and the 
			%second one contains a cell array with paths to forecast
			%files in case this option is activated.
			
			%MArchive: Forecast from pervious models can be archived
			%there and in a future version a restore may also be
			%possible. (Only fully removed models can be archived
			%there, not single timesteps)
			
			%OutputLogs: similar to obj.Forecasts but for the output
			%logs incase they are existing and should be saved.
			
			%UsedConfigs: stores the actual configuration used for each
			%model. can be switchen on and off. I guess I will also use
			%the ForecastLog object for this purpose, so again similar to 
			%obj.Forecasts;
			
			
			%Testregion configuration
			obj.TestPolygon=[];
			obj.BoundBox=[]
			obj.Depths=[];
			obj.Magnitudes=[];
			obj.Spacings=[];
			obj.ForecastGrid=[];
			obj.AutoExtend=false;
			obj.RegionExtend=[0.2,0.2];
			obj.ForecastLength=[];
			obj.PredictionIntervall=[];
			obj.UpdateMag=[];
			obj.StartTime=[];
			obj.EndTime=[];
			obj.CurrentEndTime=[];
			obj.AvailableTimes=[];
			
			%TestPolygon: the test region as polygon (double vector
			%first column lon second lat). It will be automatically 
			%generated from the Filterlist
			
			%BoundBox: boundary box around the TestPolygon, will 
			%automatically be generated from the Filterlist
			
			%Depths: an array with all the depth bins used (middle
			%point). In most cases this only contains min and max
			%depth as spatial 3D models are not very common.
			
			%Magnitudes: an array with the magnitude bins (middle
			%of the bins). In case of a model without magnitude bins
			%this will only contain min and max of the magnitude
			
			%Spacings: contains the bin size/spacing of the different
			%dimension. It is a row vector with following values,
			%[lon_space, lat_space, mag_bin, depth_bin]. The spacing
			%for a not used axis will be set to NaN. In case of only
			%one spacing for lon/lat, the spacing will automatically
			%set for both.
			
			%ForecastGrid: This is were the generated grid is 
			%stored in. It has the format of a empty forecast object
			
			%AutoExtend: If set to true the Calculator will extend
			%the polygon used for selection of the learning period
			%earthquake catalog by the amount defined in RegionExtend.
			%NOTE: this will not infuence the grid in anyway.
			
			%RegionExtend: used in the AutoExtend mode. Defines the 
			%amount the polygon is enlarged [lonExtend latExtend]
			
			%ForecastLength: is the length of the forecasts in days
			%years will be converted into 365days and vice versa
			
			%PredicationIntervall: determines how often a forecast
			%will be updated. Many times this will be the same value
			%as the ForecastLength, but it may be interesting to lower
			%it. It is also possible to use to string 'realtime' as 
			%value, in this case the forecasts will be updated every
			%time a earthquake with a magnitude above UpdateMag happens
			
			%UpdateMag: only used for realtime mode, it set how large
			%an earthquake has to be to trigger a update of the forecasts
			
			%StartTime: Simple, the time after which forecasts are 
			%produced, the format is the matlab typical datenum.
			
			%EndTime: the time for the last update (careful, the last
			%forecast will last from EndTime to EndTime+ForecastLength).
			%The format is as usual datenum. It is also possible, to use
			%the string 'open' as value, in this case the forecast will 
			%go one until now data is available, practical in case where
			%the catalog is updated on a regular basis.
			
			%CurrentEndTime: Don't know if I need this property yet, but
			%I know if don't have it and need it I will regret it. It is
			%meant for the 'open' case where the endtime is varying, it does
			%not have to be set directly. 
			
			%AvailableTimes: is an vector containing all the starting
			%times of the currently possible forecasts. Meant as 
			%guideline for checking which forecasts are already calc-
			%ulated.
			
			%Setting for update of the learning data
			obj.LearningPeriod=[];
			obj.LearningUpdate='update';
			obj.LearningLength='full';
			obj.LearningLag=0;
			
			%LearningPeriod: the starttime and endtime of the initial
			%learning period ([start,end]) in the datenum format. 
			%This period will be used for the preparetion of the models,
			%often the end time of the learning period is the start
			%time of the forecast or equal to start time -lag. The 
			%start time of the learning period is also used as earliest
			%possible time of any learning period, no forecast will have
			%learning period starting earlier than that value. The value
			%can be set to infinite if needed.
			
			%LearningUpdate: determines how the learning period is 
			%updated. Default is 'update', in this case the period
			%will automatically updated. If it is set to 'fixed' the
			%period will be same for the whole time.
			
			%LearningLength: sets the length of the learning period
			%in days, default is here 'full', in this case all data
			%available is used and the period gets longer with time.
			%If a number is used instead the period is kept the same
			%length and past events are filtered out. This may 
			%practical in some cases. Full length with fixed time
			%period is not possible.
			
			%LearningLag: can be used to introduce a "lag" to learning
			%period, it is only applyable in the 'update' mode. The 
			%endtime of the learning period will be determined like 
			%this: endtime=currentForecastTime-LearningLag. May be 
			%interesting to toy around with it. Default value is 0
			
			
			%Some Paths and Project configurations
			obj.ArchivePath='./Forecast_Results';
			obj.CalcPath='./Temporary_Files';
			obj.StoreProject='./Forecast_Results/MyForecasts.mat';
			obj.ProjectConfig=struct(	'ArchiveForecasts',false,...
										'StoreLogs',true,...
										'ArchiveLogs',false,...
										'SaveConfig',true,...
										'ArchiveDelete',false,...
										'ParallelProcessing',false,...
										'CatalogStore','ShortCat',...
										'Benchmark',true,...
										'SaveAddOnData',true);
			
			%ArchivePath: path where the forecast output files are
			%copied into, in case the the forecasts file are archived
			%There will be a subfolder for each model. It may be wise
			%to not directly use the default path, as it is a relativ
			%path.
			
			%CalcPath: Path for the temporary files used by and for the
			%models. The folder will be emptied on each calculation, 
			%so nothing should be stored there only. As with ArchivePath
			%it may be wise to set the path the first time, because of 
			%default path being relativ.
			
			%StoreProject: Filename and path where this object should be
			%saved after every calculation step. This can be different 
			%from the filename the uncalculated object has been saved in
			%e.g. could a not calculated object be saved under name
			%'Forecast.mat' but the calculated version will be stored under
			%'Forecast_calc.mat')
			
			%ProjectConfig: is structure containing the configuration 
			%the project itself. It consist out of the following fields
			%ArchiveForecasts, set to true if forecast files
			%		   should be archive
			%StoreLogs, set to true if Logs should be store
			%	    internally (opposed to not store at all)
			%ArchiveLogs, set to true if the logs files should be
			%	      archived as well.
			%SaveConfig, set to true if the actual configuration
			%	     for each forecast should be saved.
			%ArchiveDelete, will allow to archive delete models and 
			%		restore them later, but is currently not
			%		fully available (cannot be restored now)
			%ParallelProcessing, sets the parallel processing on and
			%		     off if available.
			%CatalogStore,  sets the format of the input catalogs
			%		stored. It can be
			%Filterlist: for Filterlists
			%ShortCat: for ShortCatalog
			%Boolean: for selection vector
			%Datastore: for datastore format
			%Short_and_Bool: ShortCatalog and
			%		Boolean in a cell
			%Data_and_Bool: Datastore and boolean
			%None: Nothing will be stored, saves a lot of space
			%Benchmark, if set true, the calculation times will be logged
			%SaveAddOnData, if set to true the AddonData generated by some
			%		models will be saved.
			
			
			obj.LastParameters=[];
			%This property contains all configuration of the last 
			%calculation. It is needed to determine wether the parameters
			%changed and if a recalc is needed. 
			
			
			obj.CalculatorSpace=[];
			obj.BenchmarkData=[];
			obj.AdditionalData=[];
			obj.LogObject=[];
			
			%The calculator space is only used by the calculator. It allows 
			%to temporary save parameters needed by the calculator and not 
			%already stored in elsewhere in the project object.
			
			%BenchmarkData will be filled by the calculator (if benchmarking is
			%turned on) and will log how much time is needed for each step, model
			%and so on.
			
			%AdditionalData is meant for additional data produced by the forecasts
			%it is a structure with the model names as fieldnames, how the data in 
			%there is managed is up to calculator and calculation plugin
			
			%The LogObject will be added later, and it will allow to store
			%all warnings, errors and notes into a log file as well as display
			%them on the screen instead of only showing them once. This LogObject
			%will mostly be used by the Calculator and the tester, but every other
			%object of the framework can use it, rule of thumb is that the part of 
			%framework which normally would create the message with warning, error
			%or disp should use the object, not the one sending the error code.
		
		
		end

		
		%External file functions
		setParameter(obj,Parameter,Value)

		Value = getParameter(obj,Parameter)

		extBack=backupParameters(obj,externalBack,noTime)
		
		checkState(obj)
		
		correctProgress(obj)

		autoSetupTime(obj)

		autoSetupRegion(obj,MagRange,DepthRange,GridPrecision)

		RegionConfig = buildRegionConfig(obj)

		addModel(obj,ForecastModel)

		[ForecastModel,Pos] = getModel(obj,ModelID)

		[ForecastModels,Names,Pos] = getAllModels(obj)

		replaceModel(obj,ModelID,ForecastModel)

		removeModel(obj,ModelID)

		[Forecast,Pos] = getForecast(obj,ModelID)

		[Forecasts,Names,Pos] = getAllForecasts(obj)

		cleanForecasts(obj,FullClean)

		ErrorCode = setAdditionalData(obj,ModelID,AddonData)

		setBenchmark(obj,BenchmarkData)

		ErrorCode = setForecastResults(obj,TimeStep,ModelID,ForecastData,DataMask,LogData,ConfigData)

		ErrorCode = setUsedCatalog(obj,TimeStep,Filterlist)

		updateCatalog(obj)

		UpdateCalcSpace(obj,NewCalcSpace)

	end


end
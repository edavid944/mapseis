function RegionConfig = buildRegionConfig(obj)
	%builds a structur which can be used to configure a model plugin
	%(of course only works if the test region is defined)
	
	%NEEDED fields
	%RegionPoly:		Polygon surrounding the region
	%RegionBoundingBox:	Box around the Polygon
	%LonSpacing:		Longitude spacing or spacial spacing
	%			in case only on spacing is allowed
	%LatSpacing:		Latitude spacing, if two spacings 
	%			are allowed
	%GridPrecision:		The value to which the grid was rounded to.
	%MinMagnitude:		Minimum magnitude which should be 
	%			forecasted
	%MaxMagnitude:		The maximal magnitude which should be
	%			forecasted
	%MagnitudeBin:		Size of the Magnitude Bin often this 
	%			will be 0.1, but can be different. This
	%			will be empty if no magnitude binning is
	%			used
	%MinDepth:		Minimal depth range which should be 
	%			forecasted, in most cases this will be 0
	%MaxDepth:		Maximal depth to forecast
	%DepthBin:		In most cases this will empty as 3D 
	%			regions are not common, but in case a
	%			3D model is used this gives depth bin 
	%			size
	%TimeInterval:		Length of the forcast in days or year 
	%			(can be specified in getFeatures)
	%RelmGrid:		A raw grid similar to the one used in 
	%			Relm Forecasts based on the values 
	%			specified. 
	%			(Relmformat: [LonL LonH LatL LatH ...
	%			DepthL DepthH MagL MagH (ID) Mask]
	%Nodes			%Just the spacial nodes of the grid as 
	%			for instance by TripleS, in contrary to
	%			the RELM grid this descriptes the centre
	%			of the grid cell
	%MagVector:		Consisting of the Magnitudes which have
	%			to be forecasted (the points are the in 
	%			middle of the magnitude bin)
	%DepthVector:		Consisting of the Depths which have to 
	%			forecasted (only 3D) (the points are the  
	%			in middle of the depth bin)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if isempty(obj.ForecastGrid)
		RegionConfig=[];
		error('No grid defined');			
	end
	
	
	%Magnitudes
	MagBin=obj.Spacings(3)
	if isnan(MagBin)
		MagShift=0;
	else
		MagShift=MagBin/2;
	end
	
	MinMag=obj.Magnitudes(1)-MagShift
	MaxMag=obj.Magnitudes(end)+MagShift
	
	
	%Depths
	DepthBin=obj.Spacings(4)
	if isnan(DepthBin)
		DepthShift=0;
	else
		DepthShift=DepthBin/2;
	end
	
	MinDepth=obj.Depths(1)-DepthShift
	MaxDepth=obj.Depths(end)+DepthShift


	RegionConfig = struct(	'RegionPoly',obj.TestPolygon,...
							'RegionBoundingBox',obj.BoundBox,...
							'LonSpacing',obj.Spacings(1),...
							'LatSpacing',obj.Spacings(2),...
							'GridPrecision',obj.ForecastGrid.GridPrecision,...
							'MinMagnitude',MinMag,...
							'MaxMagnitude',MaxMag,...
							'MagnitudeBin',MagBin,...
							'MinDepth',MinDepth,...
							'MaxDepth',MaxDepth,...
							'DepthBin',DepthBin,...
							'TimeInterval',obj.ForecastLength,...
							'RelmGrid',obj.ForecastGrid.getFullGrid,...
							'Nodes',obj.ForecastGrid.getGridNodes,...
							'MagVector',obj.Magnitudes,...
							'DepthVector',obj.Depths);


end
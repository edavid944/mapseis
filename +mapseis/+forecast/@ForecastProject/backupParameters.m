function extBack=backupParameters(obj,externalBack,noTime)
	%writes needed parts into LastParameters, should be 
	%done after a completed calculation
	%check if everthing is completed


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	if nargin<2
		externalBack=false;
		noTime=false;
	end
	
	extBack=[];
	
	SaveList={	'DataMode','Datastore','Filterlist',...
				'LearningFilter','TestPolygon',...
				'Depths','Magnitudes','Spacings','ForecastGrid','ForecastLength',...
				'PredictionIntervall','UpdateMag','StartTime','EndTime',...
				'LearningPeriod','LearningUpdate','StoreProject',...
				'LearningLength','LearningLag','ArchivePath','CalcPath','ProjectConfig'};
	
	%For checking parameter change,  without change of start and end time
	SaveList2={	'DataMode','Datastore','Filterlist',...
				'LearningFilter','TestPolygon',...
				'Depths','Magnitudes','Spacings','ForecastGrid','ForecastLength',...
				'PredictionIntervall','UpdateMag',...
				'LearningUpdate','StoreProject',...
				'LearningLength','LearningLag','ArchivePath','CalcPath','ProjectConfig'};
	
	if externalBack
		if noTime
			for i=1:numel(SaveList2)
				extBack.(SaveList2{i})=obj.(SaveList2{i});
			end
		else
			for i=1:numel(SaveList)
				extBack.(SaveList{i})=obj.(SaveList{i});
			end
		end
	
	else
		for i=1:numel(SaveList)
			obj.LastParameters.(SaveList{i})=obj.(SaveList{i});
		end
	
	end
	
end
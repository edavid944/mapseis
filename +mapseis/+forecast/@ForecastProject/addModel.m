function addModel(obj,ForecastModel)
	%adds a new model to the stack


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if ~isempty(obj.ModelPlugIns)
		nextEntry=numel(obj.ModelPlugIns(:,1))+1;
	else
		nextEntry=1;
		%No matter if there was something in the other
		%cell arrays, it should be deleted if the Pluginlist
		%was empty
		obj.ModelPlugIns=cell(1,3);
		obj.ModelConfig=cell(1,2);
		obj.Forecasts=cell(1,2);
		obj.UsedConfigs=cell(1,2);
		obj.OutputLogs=cell(1,2);
	
	end
	
	disp(nextEntry)
	
	if iscell(ForecastModel)
		for i=1:numel(ForecastModel)
			obj.ModelPlugIns{nextEntry,3}=ForecastModel{i};
			obj.ModelConfig(nextEntry,:)=cell(1,2);
			obj.Forecasts(nextEntry,:)=cell(1,2);
			obj.UsedConfigs(nextEntry,:)=cell(1,2);
			obj.OutputLogs(nextEntry,:)=cell(1,2);
			
			nextEntry=nextEntry+1;
		end
	else
		obj.ModelPlugIns{nextEntry,3}=ForecastModel;
		obj.ModelConfig(nextEntry,:)=cell(1,2);
		obj.Forecasts(nextEntry,:)=cell(1,2);
		obj.UsedConfigs(nextEntry,:)=cell(1,2);
		obj.OutputLogs(nextEntry,:)=cell(1,2);
		
	end
	
	%correct the Progress list -> this will also add some of 
	%the missing parts
	obj.correctProgress;
	

end
function autoSetupTime(obj)
	%can be called when PredictionIntervall, UpdateMag, StartTime, EndTime are set
	%and it will add or update AvailableTimes. If the mode is 'open', a Datastore 
	%catalog has to available or CurrentEndTime has to be set manually before.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%check if everthing there what is needed
	Ready=~isempty(obj.PredictionIntervall)&~isempty(obj.StartTime)&...
	
	~isempty(obj.EndTime);
	if ~Ready
		error('Not enough data to produce time steps')
	end	
		
	%toggles
	OpenMind=false;
	ForReal=false;

	if strcmp(obj.EndTime,'open')
		Ready=~isempty(obj.Datastore)|~isempty(obj.CurrentEndTime);
		OpenMind=true;
	end

	if strcmp(obj.PredictionIntervall,'realtime')
		Ready=Ready&~isempty(obj.Datastore)&~isempty(obj.UpdateMag);
		ForReal=true;
	end

	%check once again
	if ~Ready
		error('Not enough data to produce time steps')
	end


	%now cue the music...
	
	%take learning filter as first priority, but it won't matter
	%in most cases, as time is always ignored.
	if isempty(obj.LearningFilter)
		usedFilter=obj.Filterlist;
	else
		usedFilter=obj.LearningFilter;
	end
	
	if (OpenMind|ForReal)&~isempty(obj.Datastore)
		%get logical vector
		usedFilter.changeData(obj.Datastore);
		usedFilter.updateNoEvent;
		selVec=usedFilter.excludeFilter('Time');
	end


	if OpenMind
		%get the current maximum end time
		%(obj.CurrentEndTime has the higher priority than the
		%maximum time of datastore)
		if isempty(obj.CurrentEndTime)
			eventStruct=obj.Datastore.getFields(selVec);
			obj.CurrentEndTime=max(eventStruct.dateNum);
		end
		
		EndTimeToUse=obj.CurrentEndTime;
		
	else
		EndTimeToUse=obj.EndTime;
		   			
	end

	if ForReal
		%realtime mode 
		eventStruct=obj.Datastore.getFields([],selVec);
		inTime=eventStruct.dateNum>=obj.StartTime&eventStruct.dateNum<EndTimeToUse;
		inMag=eventStruct.mag>=obj.UpdateMag;
		RawTimes=eventStruct.dateNum(inTime&inMag);
		RawTimes=[obj.StartTime;RawTimes;obj.EndTime];
		TimeList=[];
	
		for i=1:numel(RawTimes)-1; 
			TimeList=[TimeList,RawTimes(i):1:RawTimes(i+1)];
		end
		
		obj.AvailableTimes=TimeList;
	
	else
		obj.AvailableTimes=obj.StartTime:obj.PredictionIntervall:EndTimeToUse
	
	end

	%generate Catalog slots
	obj.InputCatalogs=cell(size(obj.AvailableTimes));
	

end
function correctProgress(obj)
	%corrects the progress variable if models are changed, added or remove
	%and/or if the test region has changed. It determines what should be
	%recalculated.
	
	%I have to be carefull with updating the models, because it will change the
	%ConfigID, so only changes which "change" a value, no "bulk" modifications 
	
	%The whole method is quite complicated, as there are many different cases
	%it has to be tested if everything always work in every possible case, and 
	%may modified. But it is also possible to avoid any "false tags" by just never
	%edit a calculated project.
	
	%Hopefully it works, I guess I have to build kind of a test to see if it works
	%TODO
	
	%Carefull, this method is only meant for correcting progress structur, it is not
	%possible to correct for "deformed" forecast storage due to uncomplete removal of 
	%a model. At best always use function addModel, replaceModel and removeModel, for
	%managing the model list. If used correctly the correctProgress method may never
	%has to be called.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.util.*
	
	if strcmp(obj.ProjectStatus,'changed')| strcmp(obj.ProjectStatus,'recalc');

		newParam=obj.backupParameters(true,true);

		if ~isempty(obj.LastParameters)
			NoChange=StructCompare(newParam,obj.LastParameters,true);
		else
			NoChange=false;
		end
		
		if NoChange
		
			OneMoreTime=numel(obj.AvailableTimes)-numel(obj.LastParameters.AvailableTimes);
		
			if OneMoreTime==0
				%It is assumed that the times are updated before the check
				NoTimeChange=all(obj.AvailableTimes==obj.LastParameters.AvailableTimes);
				HaveToWarn=NoTimeChange&(obj.StartTime~=obj.LastParameters.StartTime)&...
				all(obj.EndTime~=obj.LastParameters.EndTime);
				
				if HaveToWarn
					warning('Start and End time did change, but not the AvailableTimes')
				end
			
			else
				NoTimeChange=false;
			end
		
			if NoTimeChange
				TimeFixable=true;
			
			else
				%is fixable if learning period length did not change
				%and if the time intervals overlap
				PastLearn=obj.LastParameters.LearningPeriod(2)-obj.LastParameters.LearningPeriod(1);
				NowLearn=obj.LearningPeriod(2)-obj.LearningPeriod(1);
				
				minPast=min(obj.LastParameters.AvailableTimes(:));
				maxPast=max(obj.LastParameters.AvailableTimes(:));
				
				DoOverlap=any(minPast==obj.AvailableTimes)|any(maxPast==obj.AvailableTimes);
				
				TimeFixable=DoOverlap&(PastLearn==NowLearn);
			end
	
		else
			%Hopeless anyway everything has to be recalculated
			NoTimeChange=false;
			TimeFixable=false;
		
		end
	
		if isstr(obj.PredictionIntervall)
			%currently not fixable
			TimeFixable=false;
		end
		
		
		TheNames={};
		NrTimes=numel(obj.AvailableTimes);
	
		if TimeFixable
			%do some of the work outside the loop
			minPast=min(obj.LastParameters.AvailableTimes(:));
			maxPast=max(obj.LastParameters.AvailableTimes(:));
			minNow=min(obj.AvailableTimes(:));
			maxNow=max(obj.AvailableTimes(:));
			
			SuperMin=min([minPast,minNow]);
			SuperMax=max([maxPast,maxNow]);
			SuperTime=SuperMin:obj.PredictionIntervall:SuperMax;
			obj.CalcProgress.Times=SuperTime;
			
			RawProgress=zeros(size(SuperTime));
			RawCatStore=cell(size(SuperTime));
			
			%goal find pos of old in new
			if minPast<minNow
				%losing the past
				PosMin=-find(SuperTime==minNow);
			elseif minPast==minNow
				%extension
				PosMin=1;
			else
				%back in time
				PosMin=find(SuperTime==minPast);
			end
			
			if maxPast<maxNow
				%no future
				PosMax=-find(SuperTime==maxNow);
			elseif maxPast==maxNow
				%extension in past?
				PosMax=numel(SuperTime);
			else
				%back to the future
				PosMax=find(SuperTime==maxPast);
			end
	
			if isempty(PosMax)|isempty(PosMin)
				%oops, seems like it cannot be corrected
				TimeFixable=false;
			end
		
		end
		
		CatFixed=false;
	
		%check which model has to be recalculated			
		for i=1:numel(obj.ModelPlugIns(:,1))
			curModel=obj.ModelPlugIns{i,3};
			SameModel=strcmp(obj.ModelPlugIns{i,1},curModel.PluginName)&...
			strcmp(obj.ModelPlugIns{i,2},curModel.Version);
			
			SameConfig=strcmp(curModel.ConfigID,obj.ModelConfig{i,2});	
			%carefull both Same* statement assume that the model changed and
			%the additional fields where not updated
			
			%important new model (means in a new slot) may have an 
			%empty Name in the model struct, which lead to a false in SameModel
			%-> which is fine, but could lead to an error when the old name is
			%generated
			
			ModNamNew=[curModel.PluginName,'_v',curModel.Version];
			noReduce=true;
			
			
			if ~(SameModel&SameConfig)&~isempty(obj.ModelPlugIns{i,1})
				%there is an old name registered -> maybe not new
				ModNamOld=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
				noReduce=~isfield(obj.CalcProgress.ModelProgress,ModNamOld);
			
			elseif  ~(SameModel&SameConfig)&isempty(obj.ModelPlugIns{i,1})
				noReduce=true;
			end
			
			if ~(SameModel&SameConfig)
				if ~noReduce
					if ~isempty(obj.Forecasts{i})
						if obj.ProjectConfig.ArchiveDelete
							try
								CalcToArch=obj.CalcProgress.ModelProgress.(ModNamOld);
								CalcToArch(CalcToArch==0)=-1;
								CalcToArch(CalcToArch==1)=2;
							catch
								CalcToArch=[];
							end
				
							if ~isempty(obj.UsedConfigs)
								UseConf=obj.UsedConfigs{i};
							else
								UseConf=[];
							end
							
							if ~isempty(obj.OutputLogs)
								TheLogs=obj.OutputLogs{i};
							else
								TheLogs=[];
							end
			
							RIP={ModNamOld,CalcToArch,obj.Forecasts{i},UseConf,TheLogs};
							
							%Rest in peace
							obj.MArchive{end+1}=RIP;

						end
			
						%add empty elements, it is replaced not deleted
						widthCell=numel(obj.Forecasts(i,:));
						obj.Forecasts(i,:)=cell(1,widthCell);
						
						if ~isempty(obj.UsedConfigs)
							widthCell=numel(obj.UsedConfigs(i,:));
							obj.UsedConfigs(i,:)=cell(1,widthCell);
						end
						
						if ~isempty(obj.OutputLogs)
							widthCell=numel(obj.OutputLogs(i,:));
							obj.OutputLogs(i,:)=cell(1,widthCell);
						end
					
					end
			
					%now remove traces in the progress struct
					try
						obj.CalcProgress.ModelProgress=rmfield(obj.CalcProgress.ModelProgress,ModNamOld);
					end
			
				end
			
				%add new entry
				obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
			
				%if it is at the end of the cell arrays (truely a new model) cells have to be added;
				if i>numel(obj.Forecasts(:,1))
					widthCell=numel(obj.Forecasts(i,:));
					obj.Forecasts(i,:)=cell(1,widthCell);
								
					if ~isempty(obj.UsedConfigs)
						widthCell=numel(obj.UsedConfigs(i,:));
						obj.UsedConfigs(i,:)=cell(1,widthCell);
					end
			
					if ~isempty(obj.OutputLogs)
						widthCell=numel(obj.OutputLogs(i,:));
						obj.OutputLogs(i,:)=cell(1,widthCell);
					end
				end
			
				%A new model needs correct name and version string
				if isempty(obj.ModelPlugIns{i,1});
					obj.ModelPlugIns{i,1}=obj.ModelPlugIns{i,3}.PluginName;
					obj.ModelPlugIns{i,2}=obj.ModelPlugIns{i,3}.Version;
				end
				
				%never configurated model
				if isempty(obj.ModelConfig{i,2});
					obj.ModelConfig{i,2}=obj.ModelPlugIns{i,3}.ConfigID;
					obj.ModelConfig{i,1}=obj.ModelPlugIns{i,3}.getModelConfig;
				end
			
			
			else
				if ~NoChange
					%everything has to be done new, but no cells have to be deleted
					obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
					if ~isempty(obj.InputCatalogs)&~CatFixed
						obj.InputCatalogs=cell(size(obj.AvailableTimes));
					end
			
				else
			
					if ~TimeFixable
						obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
						if ~isempty(obj.InputCatalogs)&~CatFixed
							obj.InputCatalogs=cell(size(obj.AvailableTimes));
						end
			
					else
						curProgress=RawProgress;
						if ~NoTimeChange
							%here it gets messy
							if PosMin<0
								Kickout=obj.CalcProgress.ModelProgress.(ModNamNew)(1:-PosMin);
								Kickout(Kickout==0)=-1;	
								Kickout(Kickout==1)=2;
								curProgress(1:-PosMin)=Kickout;
								PosOldMin=-PosMin;
								
								%only in the first step, transfer old catalogs if existing
								if ~isempty(obj.InputCatalogs)&~CatFixed
									TransCat=obj.InputCatalogs(1:-PosMin);
									RawCatStore(1:-PosMin)=TransCat;
								end
				
							else
								PosOldMin=1;
							end
			
							if PosMax<0
								Kickout=obj.CalcProgress.ModelProgress.(ModNamNew)(-PosMax:end);
								Kickout(Kickout==0)=-1;	
								Kickout(Kickout==1)=2;
								curProgress(-PosMax:end)=Kickout;
								PosOldMax=-PosMax;
								
								if ~isempty(obj.InputCatalogs)&~CatFixed
									TransCat=obj.InputCatalogs(-PosMax:end);
									RawCatStore(-PosMax:end)=TransCat;
								end
				
							else	
								PosOldMax=numel(obj.CalcProgress.ModelProgress.(ModNamNew));
							end
			
						end
			
						%now middle part
						MidPart=obj.CalcProgress.ModelProgress.(ModNamNew)(PosOldMin:PosOldMax);
						curProgress(abs(PosMin):abs(PosMax))=MidPart;
						
						if ~isempty(obj.InputCatalogs)&~CatFixed
							TransCat=obj.InputCatalogs(PosOldMin:PosOldMax);
							RawCatStore(abs(PosMin):abs(PosMax))=TransCat;
							obj.InputCatalogs=RawCatStore;
						end
						
						%store
						obj.CalcProgress.ModelProgress.(ModNamNew)=curProgress;
					
					end
			
				end
			
			end %if end
			
		end %for end
	

	elseif strcmp(obj.ProjectStatus,'ready')|strcmp(obj.ProjectStatus,'config')
		%a new project needs to add some empty progress bars
		for i=1:numel(obj.ModelPlugIns(:,1))
			curModel=obj.ModelPlugIns{i,3};
			
			ModNamNew=[curModel.PluginName,'_v',curModel.Version];
		
			if i>numel(obj.Forecasts(:,1))
				widthCell=numel(obj.Forecasts(i,:));
				obj.Forecasts(i,:)=cell(1,widthCell);
					
				if ~isempty(obj.UsedConfigs)
					widthCell=numel(obj.UsedConfigs(i,:));
					obj.UsedConfigs(i,:)=cell(1,widthCell);
				end
				
				if ~isempty(obj.OutputLogs)
					widthCell=numel(obj.OutputLogs(i,:));
					obj.OutputLogs(i,:)=cell(1,widthCell);
				end
			end
		
			if isempty(obj.ModelPlugIns{i,1});
				obj.ModelPlugIns{i,1}=obj.ModelPlugIns{i,3}.PluginName;
				obj.ModelPlugIns{i,2}=obj.ModelPlugIns{i,3}.Version;
			end
		
			%never configurated model
			if isempty(obj.ModelConfig{i,2});
				obj.ModelConfig{i,2}=obj.ModelPlugIns{i,3}.ConfigID;
				obj.ModelConfig{i,1}=obj.ModelPlugIns{i,3}.getModelConfig;
			end
		
			obj.CalcProgress.ModelProgress.(ModNamNew)=zeros(size(obj.AvailableTimes));
		end
		
		
		%Times are probably as well missing
		if isempty(obj.CalcProgress.Times)
			obj.CalcProgress.Times=obj.AvailableTimes;
		end	
		
		
	end %if end

end %function end
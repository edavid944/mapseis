function ErrorCode = setUsedCatalog(obj,TimeStep,Filterlist)
	%it will get a filterlist and the timeStep from the calculator 
	%and do everything itself, set the right format and find
	%the right place.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.forecast.*;
	import mapseis.region.*;
	import mapseis.datastore.*;
	import mapseis.projector.*;
	
	ErrorCode=-1;
	
	%To be sure check if timestep exist
	if ~any(obj.AvailableTimes==TimeStep)
		ErrorCode=2;
		return
	end
	
	TimeID=find(obj.AvailableTimes==TimeStep);
	
	
	switch obj.ProjectConfig.CatalogStore
		case 'Filterlist'
			obj.InputCatalogs{TimeID}=Filterlist;
			
		case 'ShortCat'
			Filterlist.changeData(obj.Datastore);
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			Filterlist.PackIt;
			
			[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
			
			obj.InputCatalogs{TimeID}=ShortCat;
			
			
		case 'Boolean'
			Filterlist.changeData(obj.Datastore);
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			Filterlist.PackIt;
			
			obj.InputCatalogs{TimeID}=selected;
			
		case 'Datastore'
			Filterlist.changeData(obj.Datastore);
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			Filterlist.PackIt;
			
			%not the best solution but will do for now, 
			%would suggest not using it anyway
			eventStruct=obj.Datastore.getFields([],selected);
			CutDatastore=DataStore(eventStruct);
			
			obj.InputCatalogs{TimeID}=CutDatastore;
			
		case 'Short_and_Bool'
			Filterlist.changeData(obj.Datastore);
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			Filterlist.PackIt;
			
			[ShortCat temp]=getShortCatalog(obj.Datastore,selected);
			
			obj.InputCatalogs{TimeID}={ShortCat,selected};
			
			
		case 'Data_and_Bool'
			Filterlist.changeData(obj.Datastore);
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			Filterlist.PackIt;
			
			%not the best solution but will do for now, 
			%would suggest not using it anyway
			eventStruct=obj.Datastore.getFields([],selected);
			CutDatastore=DataStore(eventStruct);
			
			obj.InputCatalogs{TimeID}={CutDatastore,selected};
			
		case 'None'
			%Do nothing
		
	end	
	
	%Another job well done
	ErrorCode=0;
	
	
end
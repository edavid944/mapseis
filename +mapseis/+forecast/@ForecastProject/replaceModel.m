function replaceModel(obj,ModelID,ForecastModel)
	%replaces a model with a new one


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if ~isempty(obj.ModelPlugIns)
		%generate name list
		for i=1:numel(obj.ModelPlugIns(:,1))
			ModelNames{i}=[obj.ModelPlugIns{i,1},'_v',obj.ModelPlugIns{i,2}];
		end
	
	
		if iscell(ModelID)
		
			for i=numel(ModelID)
				if isstr(ModelID{i})
					IDX=find(strcmp(ModelID{i},ModelNames));
				else
					%has to be numeric
					IDX=ModelID{i};
				end
				
				if ~isempty(IDX)
					obj.ModelPlugIns{IDX(1),3}=ForecastModel{i};
				else
					warning('No model in storage, added instead of replaced')
					nextEntry=numel(obj.ModelPlugIns(:,1))+1;
					obj.ModelPlugIns{nextEntry,3}=ForecastModel{i};
					obj.ModelConfig(nextEntry,:)=cell(1,2);
					obj.Forecasts(nextEntry,:)=cell(1,2);
					obj.UsedConfigs(nextEntry,:)=cell(1,2);
					obj.OutputLogs(nextEntry,:)=cell(1,2);
				end
	
	
			end
	
		else
	
			if isstr(ModelID)
				IDX=find(strcmp(ModelID,ModelNames));
			else
				%has to be numeric
				IDX=ModelID;
			end
			
			if ~isempty(IDX)
				obj.ModelPlugIns{IDX(1),3}=ForecastModel;
			
			else
				warning('No model in storage, added instead of replaced')
				nextEntry=numel(obj.ModelPlugIns(:,1))+1;
				obj.ModelPlugIns{nextEntry,3}=ForecastModel;
				obj.ModelConfig(nextEntry,:)=cell(1,2);
				obj.Forecasts(nextEntry,:)=cell(1,2);
				obj.UsedConfigs(nextEntry,:)=cell(1,2);
				obj.OutputLogs(nextEntry,:)=cell(1,2);
			end	
	
		end
	
	%correct the Progress list -> this will also add some of 
	%the missing parts
	obj.correctProgress;
	
	
	else
		warning('No model in storage, added instead of replaced')
		obj.addModel(ForecastModel);
	
	end	
	
end
function autoSetupRegion(obj,MagRange,DepthRange,GridPrecision)
	%can be called when Filterlist, Spacings are set. It will produce everything needed 
	%for the testregion grid, mags etc. In case there is no Magnitude and Depths set in the 
	%Filterlist, Datastore is needed.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.forecast.*;
	import mapseis.region.*;
	import mapseis.projector.*;
	
	if nargin<4
		GridPrecision=false;
	elseif nargin<3
		GridPrecision=false;
		DepthRange=[];
	elseif nargin<2
		GridPrecision=false;
		DepthRange=[];
		MagRange=[];
	end
	
	%check if everthing needed is set
	Ready=	~isempty(obj.Filterlist)&~isempty(obj.Spacings)&...
			(~isempty(obj.Datastore)|(~isempty(MagRange)&~isempty(DepthRange)))|...
			(~isempty(obj.Depths)&~isempty(obj.Magnitudes));
	      		      			
	if ~Ready
		error('Not enough data to produce regional grid')
	end		
	
	NewGrid = ForecastObj('RawGrid');
	
	%check the filterlist
	regionFilter = obj.Filterlist.getByName('Region');
	filterRegion = getRegion(regionFilter);
	pRegion = filterRegion{2};
	RegRange = filterRegion{1};
	
	
	if strcmp(RegRange,'all');
		if isempty(obj.Datastore)
			error('Not enough data to produce regional grid')
		end
		obj.Filterlist.changeData(obj.Datastore);
		obj.Filterlist.updateNoEvent;
		selected=obj.Filterlist.getSelected;
		obj.Filterlist.PackIt
		
		[locations temp]=getLocations(obj.Datastore,selected);
		lon=locations(:,1);
		lat=locations(:,2);
		FiltInput=[min(lon)-0.001, max(lon)+0.001];
		DataInput=[min(lat)-0.001, max(lat)+0.001];
		RegPoly=[FiltInput(1),DataInput(1);...
		FiltInput(2),DataInput(1);...
		FiltInput(2),DataInput(2);...
		FiltInput(1),DataInput(2);...
		FiltInput(1),DataInput(1)];
		TheBox=[FiltInput(1) FiltInput(2) DataInput(1) DataInput(2)];
		
	else
		FiltInput=obj.Filterlist;
		DataInput=obj.Datastore;
		RegPoly=pRegion.getBoundary;
		TheBox=pRegion.getBoundingBox;
	end
	
	obj.TestPolygon=RegPoly;
	
	%round bounding box according to precision
	if GridPrecision|GridPrecision~=0
		TheBox=round(TheBox*(1/GridPrecision))*GridPrecision;
		TheBox=round(TheBox*(1/GridPrecision))*GridPrecision;
	end
	
	obj.BoundBox=TheBox;
	
	
	%now which datasource shall it be? 
	%highest priority: direct input
	if ~(isempty(MagRange)&isempty(DepthRange))
		if ~isnan(obj.Spacings(3))
			MagInput=[MagRange(1) MagRange(2) obj.Spacings(3)];
		else	
			MagInput=[MagRange(1) MagRange(2)];
		end	
	
		if ~isnan(obj.Spacings(4))
			DepthInput=[DepthRange(1) DepthRange(2) obj.Spacings(4)];
		else	
			DepthInput=[DepthRange(1) DepthRange(2)];
		end	
	
	
	elseif ~(isempty(obj.Depths)&isempty(obj.Magnitudes))
		%second priority: existing MagRange and DepthRange
		%At the moment only regular Mags and depths are supported
		MinMag=min(obj.Magnitudes);
		MaxMag=max(obj.Magnitudes);
		MinDepth=min(obj.Depths);
		MaxDepth=max(obj.Depths);
		
		if ~isnan(obj.Spacings(3))
			MagInput=[MinMag MaxMag obj.Spacings(3)];
		else	
			MagInput=[MinMag MaxMag];
		end	
		
		if ~isnan(obj.Spacings(4))
			DepthInput=[MinDepth MaxDepth obj.Spacings(4)];
		else	
			DepthInput=[MinDepth MaxDepth];
		end	
		
	
	else
		%last: use the datastore
		obj.Filterlist.changeData(obj.Datastore);
		obj.Filterlist.updateNoEvent;
		selected=obj.Filterlist.getSelected;
		obj.Filterlist.PackIt
		eventStruct=obj.Datastore.getFields({},selected);
		
		MinMag=min(eventStruct.mag);
		MaxMag=max(eventStruct.mag);
		MinDepth=min(eventStruct.depth);
		MaxDepth=max(eventStruct.depth);
		
		if ~isnan(obj.Spacings(3))
			MagInput=[MinMag MaxMag obj.Spacings(3)];
		else	
			MagInput=[MinMag MaxMag];
		end	
		
		if ~isnan(obj.Spacings(4))
			DepthInput=[MinDepth MaxDepth obj.Spacings(4)];
		else	
			DepthInput=[MinDepth MaxDepth];
		end	
	
	end
	
	%Generate the grid in the object
	NewGrid.createGrid(FiltInput,DataInput,obj.Spacings(1:2),MagInput,DepthInput,[],GridPrecision);
	%I go now for the rounded version, but may change or make it switchable later if needed.
	
	%save grid and get the data from the grid back
	obj.ForecastGrid=NewGrid;
	obj.Depths=obj.ForecastGrid.getDepths;
	obj.Magnitudes=obj.ForecastGrid.getMagnitudes;
	
	%check if one of the is empty
	if isempty(obj.Depths)&~isempty(DepthRange)&isnan(obj.Spacings(4))
		obj.Depths=DepthRange;
	end
	
	if isempty(obj.Magnitudes)&~isempty(MagRange)&isnan(obj.Spacings(3))
		obj.Magnitudes=MagRange;
	end
	
	if isempty(obj.LearningFilter)
		warning('LearningFilter has been set automatically')
		obj.LearningFilter=obj.Filterlist
	end
	

end
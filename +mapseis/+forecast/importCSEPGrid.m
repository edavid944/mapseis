function GridStruct = importCSEPGrid(inStr,FastMode,varargin)
	%import filter for xml based CSEP Grid Forecast
	%It returns a structure which contains the grid and the extracable 
	%metadata
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	import mapseis.util.importfilter.*;

	
	
	if 3==nargin
	    if strcmp(varargin{1},'-file')
		fid = fopen(inStr);
		inStr = fscanf(fid,'%c'); % Read the entire text file into an array
		fclose(fid);
	    elseif strcmp(varargin{1},'-string')
		%nothing to do		    
	    end
	     
	end

	
	rp = regexpPatternCatalog();
		
	% Import the whitespace character 
	ws = rp.ws;
	
	% Define function to add name to pattern for named pattern matching
	nm = rp.name;
	
	
	%first find all cell blocks. It is possible to extract the cells
	%with the command regexp, but it seemed to be to slow.
	TheLines=regexp(inStr,rp.line,'match');
	
	startCell=[];
	endCell=[];
	for i=1:numel(TheLines)
		FoundStart = strfind(TheLines{i}, '<cell');
		FoundEnd = strfind(TheLines{i}, '</cell>');
		
		if ~isempty(FoundStart)
			startCell=[startCell;i];
		end
		
		if ~isempty(FoundEnd)
			endCell=[endCell;i];
		end
		
	end
	
	if numel(startCell)~=numel(endCell)
		error('The xml files seems to be not properly formated')
	end
	
	HeaderString=[];
	%first go throught the "header of the file
	for i=1:startCell(1)
		HeaderString=[HeaderString,TheLines{i}];
	end
	
	
%	<?xml version='1.0' encoding='UTF-8'?>
%	<CSEPForecast xmlns='http://www.scec.org/xml-ns/csep/forecast/0.1'>
%	<forecastData publicID='smi:org.scec/csep/forecast/1'>
%	<modelName>Etas</modelName>
%	<version>  J1.1  </version>
%	<author>Jiancang Zhuang</author> 
%	<issueDate>  Tue Sep 11 14:20:46 2012  </issueDate>
%	<forecastStartDate>  2011-03-09  </forecastStartDate> 
%	<forecastEndDate>  2011-03-10  </forecastEndDate> 
%	<defaultCellDimension latRange='0.1' lonRange='0.1'/>
%	<defaultMagBinDimension>0.1</defaultMagBinDimension>
%	<lastMagBinOpen>1</lastMagBinOpen>
%	<depthLayer max='100.0' min='0.0'>

	%Try to get all meta data fields
	TagsMeta={'modelName','version','author','issueDate','forecastStartDate',...
	'forecastEndDate','defaultMagBinDimension'}
	
	%Kick out all ' and < and > and /
	HeaderString(HeaderString=='''')=' ';
	HeaderString(HeaderString=='<')=' ';
	HeaderString(HeaderString=='>')=' ';
	HeaderString(HeaderString=='/')=' ';
	
	for i=1:numel(TagsMeta)
		MetaFound=strfind(HeaderString, TagsMeta{i});
		if ~isempty(MetaFound)	
			if numel(MetaFound)==3
				Pos1=MetaFound(2)+numel(TagsMeta{i})+1;
				Pos2=(MetaFound(3)-3);
				GridStruct.(TagsMeta{i})=...
				HeaderString(Pos1:Pos2);
			else
			
				Pos1=MetaFound(1)+numel(TagsMeta{i})+1;
				Pos2=(MetaFound(2)-3);
				GridStruct.(TagsMeta{i})=...
				HeaderString(Pos1:Pos2);
			end
		end
	
	end
	
	GridStruct.defaultMagBinDimension=str2num(GridStruct.defaultMagBinDimension);
	MagBin=GridStruct.defaultMagBinDimension;
	
	%DepthLayer
	DepthFound=strfind(HeaderString, 'depthLayer');
	
	if isempty(DepthFound)
		error('No Depth information set')
	end
	
	
	%maybe not the most elegant way but it should work and is 
	%reasonable fast
	%Depth
	EndDepth=DepthFound(1)+100; %should not be longer
	EndDepth=min([EndDepth,numel(HeaderString)]);
	DepthString=HeaderString(DepthFound(1):EndDepth);
	
	maxFound=strfind(DepthString, 'max=');
	minFound=strfind(DepthString, 'min=');
	emptyFound=strfind(DepthString, ' ');
	
	
	if isempty(maxFound)|isempty(minFound)
		error('Depth not correct')
	end
	
	minPos1=minFound(1)+4;
	temp=find(emptyFound>minFound+4);
	if ~isempty(temp)	
		minPos2=emptyFound(temp(1));
		
	else
		minPos2=numel(DepthString);
	end
	
	minDepth=str2num(DepthString(minPos1:minPos2));
	
	maxPos1=maxFound(1)+4;
	temp=find(emptyFound>maxFound+4);
	if ~isempty(temp)	
		maxPos2=emptyFound(temp(1));
		
	else
		maxPos2=numel(DepthString);
	end
	
	maxDepth=str2num(DepthString(maxPos1:maxPos2));
	
	
	
	
	%Spacing Lon/Lat
	SpaceFound=strfind(HeaderString, 'defaultCellDimension');
	
	if isempty(SpaceFound)
		error('No Spacing set')
	end
	
	EndSpace=SpaceFound(1)+100; %should not be longer
	EndSpace=min([EndSpace,numel(HeaderString)]);
	SpaceString=HeaderString(SpaceFound(1):EndSpace);
	
	lonFound=strfind(SpaceString, 'lonRange=');
	latFound=strfind(SpaceString, 'latRange=');
	emptyFound=strfind(SpaceString, ' ');
	
	if isempty(lonFound)|isempty(latFound)
		error('Spacing not correct')
	end
	
	lonPos1=lonFound(1)+9;
	temp=find(emptyFound>lonPos1);
	if ~isempty(temp)
		lonPos2=emptyFound(temp(1));
	else	
		lonPos2=numel(SpaceString);
	end
	
	LonSpace=str2num(SpaceString(lonPos1:lonPos2));
	
	
	latPos1=latFound(1)+9;
	temp=find(emptyFound>latPos1);
	
	if ~isempty(temp)
		latPos2=emptyFound(temp(1));
	else	
		latPos2=numel(SpaceString);
	end
	
	LatSpace=str2num(SpaceString(latPos1:latPos2));

	
	
	
	
	
	%now go through ever cell block and extract the grid info
	curLonPos=nan;
	curLatPos=nan;
	curMag=nan;
	curRate=nan;
	
	% <cell lat='24.15' lon='122.45'>
	% <bin m='4'>8.43357e-05</bin>
        % <bin m='4.1'>7.00097e-05</bin>
        % <bin m='4.2'>5.81172e-05</bin>
        
        TheGrid=[];
        
	
        %Can be quite slow (a lot of lines, maybe there is a faster way)
	if ~FastMode
		for i=startCell(1):numel(TheLines)
			CurLine=TheLines{i};
			disp(i);
			
			%Kick out all ' and < and > and /
			CurLine(CurLine=='''')=' ';
			CurLine(CurLine=='<')=' ';
			CurLine(CurLine=='>')=' ';
			CurLine(CurLine=='/')=' ';	
			
			emptyFound=strfind(CurLine, ' ');
			LonFound=strfind(CurLine, 'lon=');
			LatFound=strfind(CurLine, 'lat=');
			
			if ~isempty(LonFound)&~isempty(LatFound)
				%new position
				lonPos1=LonFound(1)+4;
				temp=find(emptyFound>lonPos1);
				lonPos2=emptyFound(temp(1));
				curLonPos=str2num(CurLine(lonPos1:lonPos2));
				
				latPos1=LatFound(1)+4;
				temp=find(emptyFound>latPos1);
				latPos2=emptyFound(temp(1));
				curLatPos=str2num(CurLine(latPos1:latPos2));
				
				
			end
			
			
			%find current Magnitude
			MagFound=strfind(CurLine, 'm=');
			
			if ~isempty(MagFound)
				MagPos1=MagFound(1)+2;
				temp=find(emptyFound>MagPos1);
				MagPos2=emptyFound(temp(1));
				curMag=str2num(CurLine(MagPos1:MagPos2));
				
				
				%"find" currentRate
				
				RatePos1=MagPos2+2;
				temp=find(emptyFound>RatePos1);
				RatePos2=emptyFound(temp(1));
				curRate=str2num(CurLine(RatePos1:RatePos2));
			end
			
			
			%generate line
			GriLine=[curLonPos,curLonPos+LonSpace,curLatPos,curLatPos+LatSpace,...
				minDepth,maxDepth,curMag,curMag+MagBin,curRate,1];
			
			TheGrid=[TheGrid;GriLine];
			
			
		end
	else
		%An attempt to improve the speed of the previous routine by
		%using the knowledge about the lines (first line always the 
		%position, following lines the magnitude bins) and by going 
		%blockwise
		%Seems work quite fine, it takes ~180s for 1e6 gridpoints (50mb)
		BlockSize=endCell(1)-startCell(1);
		MiniGrid={};
		for i=1:numel(startCell)
			TheGrid=[];
			%Lon and Lat
			CurLine=TheLines{startCell(i)};
			
			%Kick out all ' and < and > and /
			CurLine(CurLine=='''')=' ';
			CurLine(CurLine=='<')=' ';
			CurLine(CurLine=='>')=' ';
			CurLine(CurLine=='/')=' ';	
			
			emptyFound=strfind(CurLine, ' ');
			LonFound=strfind(CurLine, 'lon=');
			LatFound=strfind(CurLine, 'lat=');
			
			lonPos1=LonFound(1)+4;
			temp=find(emptyFound>lonPos1);
			lonPos2=emptyFound(temp(1));
			curLonPos=str2num(CurLine(lonPos1:lonPos2));
				
			latPos1=LatFound(1)+4;
			temp=find(emptyFound>latPos1);
			latPos2=emptyFound(temp(1));
			curLatPos=str2num(CurLine(latPos1:latPos2));
			disp([num2str(i) ' of ' num2str(numel(startCell))])
			%It maybe possible to improve that part
			for j=1:BlockSize
				CurLine=TheLines{startCell(i)+j};
				%Kick out all ' and < and > and /
				CurLine(CurLine=='''')=' ';
				CurLine(CurLine=='<')=' ';
				CurLine(CurLine=='>')=' ';
				CurLine(CurLine=='/')=' ';	
			
				MagFound=strfind(CurLine, 'm=');
				emptyFound=strfind(CurLine, ' ');
				
				if ~isempty(MagFound)
					MagPos1=MagFound(1)+2;
					temp=find(emptyFound>MagPos1);
					MagPos2=emptyFound(temp(1));
					curMag=str2num(CurLine(MagPos1:MagPos2));
					
					
					%"find" currentRate
					
					RatePos1=MagPos2+2;
					temp=find(emptyFound>RatePos1);
					RatePos2=emptyFound(temp(1));
					curRate=str2num(CurLine(RatePos1:RatePos2));
				
				
				
					%generate line
					GriLine=[curLonPos,curLonPos+LonSpace,curLatPos,curLatPos+LatSpace,...
						minDepth,maxDepth,curMag,curMag+MagBin,curRate,1];
					
					TheGrid=[TheGrid;GriLine];
				end
				
			end
			MiniGrid{i}=TheGrid;
			%disp(size(TheGrid))
			
		end
		TheGrid=[];
		TheGrid=cell2mat(MiniGrid');
		%for i=1:numel(MiniGrid)
		%	TheGrid=[TheGrid;MiniGrid{i}];
		%	disp(i)
		%end
	
	end
	
	%finish structure
	GridStruct.Depths=[minDepth,maxDepth];
	GridStruct.Spacing=[LonSpace,LatSpace];
	GridStruct.TheGrid=TheGrid;
	
	
end

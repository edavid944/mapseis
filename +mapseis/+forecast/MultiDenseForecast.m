function [Forecast AddData] = MultiDenseForecast(Datastore,Filterlist,TheGrid,WorkMode,AddConfig)
	%simple forecast similar in a way to TripleS model but with more "kernel" options
	%and a depth support.
	%I don't know how many of this modes really make sense, it is just some experimental 
	%function to test out some methods and produce some data.
	%The function is meant to be called from another function which should do the 
	%normalization, and the time managment as well as the creation of a ForecastObj.	
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	import mapseis.projector.*;
	
	
	if isempty(AddConfig)
		%add some default values
		
		%radius used for the density (in km)
		AddConfig.DenseRadius=50;
		
		%used in the PureDistance mode for 1/(R^DistExp)
		AddConfig.DistExp=1;
		AddConfig.MagJump=0;
		
		%used in the TripleS mode as the smoothing distance (km)
		AddConfig.SmoothDist=50;
	end
	
	
	if isnumeric(Datastore)
		%Zmap style catalog
		
		if ~isempty(Filterlist)
			if islogical(Filterlist)
				TheCat=Datastore(Filterlist,:);
			else
				%probably filterlist -> ignore
				TheCat=Datastore;
			end
		else
			TheCat=Datastore
		end
		
	else
		%A Datastore catalog
		
		if ~isempty(Filterlist)
			if islogical(Filterlist)
				TheCat=getZMAPFormat(Datastore,Filterlist);
			else
				%probably filterlist
				Filterlist.changeData(Datastore);
				Filterlist.update;
				selected=Filterlist.getSelected;
				
				TheCat=getZMAPFormat(Datastore,selected);
				
			end
		
		else
			TheCat=getZMAPFormat(Datastore);
		
		end
		
	end
	
	
	%Determine how many dimension the grid has
	DepthSwitch=~(all(TheGrid(1,5)==TheGrid(:,5))&all(TheGrid(1,6)==TheGrid(:,6)));
	MagSwitch=~(all(TheGrid(1,7)==TheGrid(:,7))&all(TheGrid(1,8)==TheGrid(:,8)));
	
	
	%it would be possible to calculate all distances at once, but it especially
	%for large 3D grids,this would use way to much memory
	
	%decide which mode
	switch WorkMode
		case 'PureDense'
			%uses the density 
			
			
		case 'PureDistance'	
			%more like TripleS, calculate the distance of each earthquake 
			%to each grid cell and sum up the reverse of them (1/R^c)
			
			%calc Mid cell points
			MidPoints=[(TheGrid(:,1)+TheGrid(:,2))./2,(TheGrid(:,3)+TheGrid(:,4))./2,...
					(TheGrid(:,5)+TheGrid(:,6))./2];
			Forecast=zeros(numel(TheGrid(:,1)),1);
			CountEq=zeros(numel(TheGrid(:,1)),1);
			DiEq=AddConfig.DistExp;
			MagJump=AddConfig.MagJump;
			
			if DepthSwitch
				disp('Used Depths')
				%calc distances (3D)
				for i=1:numel(TheGrid(:,1))
					%get middle of the cell
					CurPos=MidPoints(i,:);
					InMag=(TheCat(:,6)>=TheGrid(i,7)-MagJump)&(TheCat(:,6)<TheGrid(i,8)+MagJump);
					
					if sum(InMag)~=0
						TheDist=sqrt( (TheCat(InMag,7)-CurPos(3)).^2 + ...
							(deg2km(distance(TheCat(InMag,2), TheCat(InMag,1),...
							repmat(CurPos(2),numel( TheCat(InMag,2)),1),...
							repmat(CurPos(1),numel(TheCat(InMag,1)),1)))).^2 );
						Forecast(i)=sum(1./(TheDist.^DiEq));
						CountEq(i)=sum(InMag);
						
					end
							
					
				end
			else
			
			end
			
			AddData.CountEq=CountEq;
			
			
		case 'TripleS'
			%A try on the TripleS
			
			%calc Mid cell points
			MidPoints=[(TheGrid(:,1)+TheGrid(:,2))./2,(TheGrid(:,3)+TheGrid(:,4))./2,...
					(TheGrid(:,5)+TheGrid(:,6))./2];
			Forecast=zeros(numel(TheGrid(:,1)),1);
			CountEq=zeros(numel(TheGrid(:,1)),1);
			DiEq=AddConfig.DistExp;
			
			if DepthSwitch
				disp('Used Depths')
				%calc distances (3D)
				for i=1:numel(TheGrid(:,1))
					%get middle of the cell
					CurPos=MidPoints(i,:);
					InMag=(TheCat(:,6)>=TheGrid(i,7))&(TheCat(:,6)<TheGrid(i,8));
					
					if sum(InMag)~=0
						TheDist=sqrt( (TheCat(InMag,7)-CurPos(3)).^2 + ...
							(deg2km(distance(TheCat(InMag,2), TheCat(InMag,1),...
							repmat(CurPos(2),numel( TheCat(InMag,2)),1),...
							repmat(CurPos(1),numel(TheCat(InMag,1)),1)))).^2 );
						Forecast(i)=sum(1./(TheDist.^DiEq));
						CountEq(i)=sum(InMag);
						
					end
							
					
				end
			else
			
			end
			
			AddData.CountEq=CountEq;
	
	
	
	
	end
	
	
	
	
	

end

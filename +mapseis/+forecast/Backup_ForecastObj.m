classdef ForecastObj < handle
	%ForecastObj v1 serves as storage for CSEP and Relm style forecasts.
	%It supports currently up to 5D (lon,lat,depth,mag,time) and can be used
	%either as real forecast or as a empty grid.
	%This version does only support a single grid common to all timeslices	
	
	%Comment RealTimeMode: This allows to use overlapping forecasts, it is 
	%not tested currently but I guess it should work and I hope I got all
	%places where change was needed.
	
	properties
        	Name
        	ID
        	Version
        	EmptyGrid
        	Ready
        	ForecastReady
		TheGrid
		NodeIDs
		GridNodes
		AxisVectors
        	GridSpacings
        	GridLimits
        	GridPrecision
        	MagBins
        	DepthBins
        	RegionPoly
        	BoundBox
        	TimeVector
        	TimeSpacing
        	RealTimeMode
        	Dimension
        	SelectedNodes
        	SelectedIDs
        	JumpValue
        	ForecastData
        	DisForecastData
        	ExpectedValue
        	OverallDistro
        	ForecastType
        	DataMask
        	EqualMask
        	GenericMask
        	TheBag
        	
        	    	
        end
    

    
        methods
        	function obj=ForecastObj(Name)
        		%constructor: only inits the most important fields
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1)
        		end
        		
        		obj.Name=Name;
        		obj.ID=RawChar(randi(15,1,8)+1);
        		obj.Version='0.9';
        		%init data
        		obj.EmptyGrid=true;
        		obj.Ready=false; %Grid is set
        		obj.ForecastReady=false; %forecast is initialized
        		obj.TheGrid=[];
        		obj.GridNodes=[];
        		obj.AxisVectors={};
        		obj.GridSpacings=[];
        		obj.GridLimits=[];
        		obj.GridPrecision=10^(-5);
        		obj.MagBins=[];
        		obj.DepthBins=[];
        		obj.RegionPoly=[];
        		obj.BoundBox=[];
        		obj.TimeVector=[];
        		obj.TimeSpacing=[];
        		obj.RealTimeMode=false;
        		obj.Dimension=struct(	'Space',false,...
        					'Depth',false,...
        					'Mag',false,...
        					'Time',false);
        		obj.SelectedNodes=[];
        		obj.SelectedIDs=[];
        		obj.JumpValue=[];
        		obj.ForecastData={};
        		obj.DisForecastData={}; %Needed for neg. bin.
        		obj.ExpectedValue=[];
        		obj.OverallDistro={};
        		
        		obj.NodeIDs=[];
        		%Node IDs are meant for the Custom mode, but mostly not needed even there, they're more
        		%a security measurement to avoid mix up of nodes in the file 
        		
        		%ForecastType is needed to output the a complete forecast
        		%at the moment 'None' for empty forecast and 'Poisson' and 'Custom' is
        		%know and specified. The field 'Method' is needed for future
        		%expantions where it will be a handle to a converter or similar.
        		%Note: ForecastType will also determine which format the 
        		%ForecastData field has.In case of 'Poisson' this is simply a 
        		%vector. In a possible 'NegBin' (Negative Binomial) this may be
        		%a matrix of the dimension [NrBin,2
        		obj.ForecastType=struct('Type','None',...
        					'Method',{{}});
        					
        		%Defines if a cell is used in the forecast (true) or not (false)			
        		obj.DataMask=[];			
        		
        		%if set to true, the DataMask is the same for every forecast,
        		%which makes DataMask a vector, if set to false a DataMask for
        		%every timestep is used making DataMask a cell array of vectors
        		obj.EqualMask= true;			
        		obj.GenericMask=true;
        		
        		%internal variable
        		obj.TheBag={};
        					
        					
        	end	
        	
        	
        	function createGrid(obj,Filterlist,Datastore,GridSpacing,MagSpace,DepthSpace,KeyPoint,GridPrec)
        		%a passthrought to the RelmGrid creator
        		import mapseis.forecast.*;
        		import mapseis.projector.*;
        		
        		if ~isempty(GridPrec)
        			if ~islogical(GridPrec)
        				%new precision
        				obj.GridPrecision=GridPrec;
        			end
        		end
        		
        		%generate the almost finished grid
        		[TheGrid TripleGrid isInIt] = generateGrid(Filterlist,Datastore,GridSpacing,MagSpace,DepthSpace,KeyPoint,obj.GridPrecision);
        		
        		%Pack the Bag, makes life a bit easier, in the other function
        		obj.TheBag={GridSpacing,MagSpace,DepthSpace};
        		
        		%pass it to the function which does the actuall work
        		obj.addGrid(TheGrid,TripleGrid);
        		
        		
        		%get Grid polygon and add it
        		if ~isnumeric(Filterlist)
        			regionFilter = Filterlist.getByName('Region');
        			filterRegion = getRegion(regionFilter);
        			pRegion = filterRegion{2};
        			RegRange=filterRegion{1};
        			
				if strcmp(RegRange,'all');
					Filterlist.changeData(Datastore);
					Filterlist.updateNoEvent;
					selected=obj.Filterlist.getSelected;
					Filterlist.PackIt

					[locations temp]=getLocations(Datastore,selected);
					lon=locations(:,1);
					lat=locations(:,2);
					bounded=[min(lon) max(lon) min(lat) max(lat)];
					TheBox=bounded;
				else
					bounded=pRegion.getBoundary;
					TheBox=pRegion.getBoundingBox;
				end
				
        		else
        			bounded=[Filterlist(1) Filterlist(2) Datastore(1) Datastore(2)];
        			TheBox=bounded;
        		end
        		
        		obj.RegionPoly=bounded;
        		obj.BoundBox=TheBox;
        		
        	end
        	
        	
        	function setRealTimeMode(obj, ModeToogle)
        		%sets the realtime mode (allowing overlaping forecast)
        		%it gives a warning if already two forecasts are in the 
        		%object.
        		
        		if numel(obj.TimeVector)>=2
        			warning('There are already two forecast in this object');
        		end
        		
        		obj.RealTimeMode=ModeToogle;
        		
        	end
        	
        	
        	function addGrid(obj,TheGrid,TripleGrid)
        		%Allows to add a Grid to the object, this can also be a previous used Grid
        		
        		if nargin<3
        			TripleGrid=[];
        		end
        		
        		if isempty(TripleGrid)
        			%generate a TripleGrid
        			ElementSelector=find(TheGrid(1,5)==TheGrid(:,5)&...
        				TheGrid(1,7)==TheGrid(:,7));
        		
        			TripleGrid(:,1)=(TheGrid(ElementSelector,1)+TheGrid(ElementSelector,2))/2;
        			TripleGrid(:,2)=(TheGrid(ElementSelector,3)+TheGrid(ElementSelector,4))/2;
        		end
        		
        		%Check if somebody left something behind ;-)
        		%maybe seems funny to put TripleGrid not through here, but
        		%TripleGrid is a Result from a previous calc and not a input 
        		%parameter (a bit picky maybe, yes);
        		
        		%The rounding of the calculated parameters is needed, and according
        		%to the same rounding the grid generator does, if it is not done
        		%the values will jitter
        		if ~isempty(obj.TheBag)
        			GridSpacing=obj.TheBag{1};
        			MagSpace=obj.TheBag{2};
        			DepthSpace=obj.TheBag{3};
        			
        			%empty the bag
        			obj.TheBag={};
        		else
        			%Stuff missing, so it has to be generated
        			GridSpacing(1)=TheGrid(1,2)-TheGrid(1,1);
        			GridSpacing(2)=TheGrid(1,4)-TheGrid(1,3);
        			
        			%rounding is needed, to prevent jitter 
        			GridSpacing(1)=round(GridSpacing(1)*(1/obj.GridPrecision))*obj.GridPrecision;
        			GridSpacing(2)=round(GridSpacing(2)*(1/obj.GridPrecision))*obj.GridPrecision;
        			
        			
        			if all(TheGrid(1,5)==TheGrid(:,5))&all(TheGrid(1,6)==TheGrid(:,6))
        				DepthSpace(1)=(min(TheGrid(:,6))+min(TheGrid(:,5)))/2;
        				DepthSpace(2)=(max(TheGrid(:,6))+max(TheGrid(:,5)))/2;
        				DepthSpace=round(DepthSpace*10^4)/10^4;
        				
        				
        			else
        				DepthSpace(1)=(min(TheGrid(:,6))+min(TheGrid(:,5)))/2;
        				DepthSpace(2)=(max(TheGrid(:,6))+max(TheGrid(:,5)))/2;
        				DepthSpace(3)=(TheGrid(1,6)-TheGrid(1,5));
        				DepthSpace=round(DepthSpace*10^4)/10^4;
        			end
        		
        			if all(TheGrid(1,7)==TheGrid(:,7))&all(TheGrid(1,8)==TheGrid(:,8))
        				MagSpace(1)=(min(TheGrid(:,8))+min(TheGrid(:,7)))/2;
        				MagSpace(2)=(max(TheGrid(:,8))+max(TheGrid(:,7)))/2;
        				MagSpace=round(MagSpace*10^3)/10^3;
        			else
        				MagSpace(1)=(min(TheGrid(:,8))+min(TheGrid(:,7)))/2;
        				MagSpace(2)=(max(TheGrid(:,8))+max(TheGrid(:,7)))/2;
        				MagSpace(3)=(TheGrid(1,8)-TheGrid(1,7));
        				MagSpace=round(MagSpace*10^3)/10^3;
        			end
        		end
        		
        		
        		%add IDs to the TheGrid and add to the object
        		TheGrid=TheGrid(:,1:8);
        		TheGrid(:,9)=(1:numel(TheGrid(:,1)))';
        		obj.TheGrid=TheGrid;
        		
        		%generate a generic mask (everything used)
        		obj.DataMask=true(size(TheGrid(:,8)));
        		obj.EqualMask=true;
        		obj.GenericMask=true;
        		
        		%Add Spacings
        		obj.GridSpacings=GridSpacing;
        		
        		%Decide which Dimensions have to be set on and add Spacing if needed.
        		obj.Dimension.Space=true;
        		
        		if numel(DepthSpace)==3
        			obj.Dimension.Depth=true;
        			obj.GridSpacings(3)=DepthSpace(3);
        		else
        			obj.Dimension.Depth=false;
        		end
        		
        		
        		if numel(MagSpace)==3
        			obj.Dimension.Mag=true;
        			obj.GridSpacings(4)=MagSpace(3);
        		else
        			obj.Dimension.Mag=false;
        		end
        		

        		%Add the Ranges (min and max including grid spacing
        		minLon=min(obj.TheGrid(:,1));
        		maxLon=max(obj.TheGrid(:,2));
        		minLat=min(obj.TheGrid(:,3));
        		maxLat=max(obj.TheGrid(:,4));
        		minDepth=min(obj.TheGrid(:,5));
        		maxDepth=max(obj.TheGrid(:,6));
        		minMag=min(obj.TheGrid(:,7));
        		maxMag=max(obj.TheGrid(:,8));
        		
        		obj.GridLimits=[	minLon,minLat,minDepth,minMag;
        					maxLon,maxLat,maxDepth,maxMag];
        		
        		
        		%add available values (always the middle point of the node)
        		obj.GridNodes=TripleGrid;
        		obj.DepthBins=(unique(obj.TheGrid(:,5))+unique(obj.TheGrid(:,6)))/2;
        		obj.MagBins=(unique(obj.TheGrid(:,7))+unique(obj.TheGrid(:,8)))/2;
        		
        		%round the bins
        		obj.DepthBins=round(obj.DepthBins*10^4)/10^4;
        		obj.MagBins=round(obj.MagBins*10^3)/10^3;
        		
        		%build vectors
        		obj.AxisVectors{1}=unique(obj.GridNodes(:,1));
        		obj.AxisVectors{2}=unique(obj.GridNodes(:,2));
        		
        		
        		
        		%The vectors for Depth and Magnitude are at the moment unnecessary
        		%as the grid does not support free 3D or 4D shapes. If this needed
        		%late it can be added easiely
        		
        		%build selection matrix (needed to build fast maps)
        		if obj.Dimension.Depth
        			%3D mode with depth
        			%find how many bins are between the spacial cells
        			Temp=find(obj.TheGrid(1,7)==obj.TheGrid(:,7));
        			SpaceBetween3D=Temp(2)-Temp(1);
        			
        			Temp=find(obj.TheGrid(1,5)==obj.TheGrid(:,5)&...
        					obj.TheGrid(1,7)==obj.TheGrid(:,7));
        			SpaceBetween2D=Temp(2)-Temp(1);
        			
        			LowMag=obj.TheGrid(1,7);
        			AltDepthVec=obj.TheGrid(1:SpaceBetween3D:SpaceBetween2D,5);
        			
        			%For 2D Slices
        			[XX,YY] = meshgrid(obj.AxisVectors{1},obj.AxisVectors{2});
        			RawSelector2D = {};
        			RawNrSel2D = {};
        			TempSel2D = false(size(XX));
        			TempNrSel2D = NaN(size(XX));	
        			for k=1:numel(AltDepthVec);
        					RawSelector2D{k} = false(size(XX));
        					RawNrSel2D{k} = NaN(size(XX));	
        			end
        			
        			%For 3D Volumes
        			[XXX,YYY,ZZZ] = meshgrid(obj.AxisVectors{1},obj.AxisVectors{2},AltDepthVec);
        			
        			RawSelector3D = false(size(XXX));
        			RawNrSel3D = NaN(size(XXX));
        			
        			
        			for i=1:length(obj.GridNodes);
        				%Full 3D Slices
        				for j=1:numel(AltDepthVec)
        					Found3D=(XXX==obj.GridNodes(i,1))&(YYY==obj.GridNodes(i,2)&(ZZZ==AltDepthVec(j)));
        					RawSelector3D(Found3D)=true;
        					RawNrSel3D(Found3D)=1+(i-1)*SpaceBetween2D+(j-1)*SpaceBetween3D;
        				end	
        				
        				%2D Slices
        				Found2D=(XX==obj.GridNodes(i,1))&(YY==obj.GridNodes(i,2));
        				TempSel2D(Found2D)=true;
        				TempNrSel2D(Found2D)=1+(i-1)*SpaceBetween2D;
        			end
        			
        			%generate 2D slices
        			for j=1:numel(AltDepthVec)
        				RawSelector2D{j}=TempSel2D;
        				RawNrSel2D{j}=TempNrSel2D+(j-1)*SpaceBetween3D;
        			end
        			
        			
        			%Now pack it all into the objects fields
        			obj.SelectedNodes={RawSelector2D,RawSelector3D};
        			obj.SelectedIDs={RawNrSel2D,RawNrSel3D};
        			obj.JumpValue=[SpaceBetween2D,SpaceBetween3D];
        			
        			
        		else
        			%2D mode
        			
        			%find how many bins are between the spacial cells
        			Temp=find(obj.TheGrid(1,5)==obj.TheGrid(:,5)&...
        					obj.TheGrid(1,7)==obj.TheGrid(:,7));
        			SpaceBetween=Temp(2)-Temp(1);
        			
        			[XX,YY] = meshgrid(obj.AxisVectors{1},obj.AxisVectors{2});
        			RawSelector = false(size(XX));
        			RawNrSel = NaN(size(XX));
        			for i=1:length(obj.GridNodes);
        				RawSelector((XX==obj.GridNodes(i,1))&(YY==obj.GridNodes(i,2)))=true;
        				RawNrSel((XX==obj.GridNodes(i,1))&(YY==obj.GridNodes(i,2)))=1+(i-1)*SpaceBetween;
        			end
        			
        			obj.SelectedNodes=RawSelector;
        			obj.SelectedIDs=RawNrSel;
        			obj.JumpValue=SpaceBetween;
        		end
        		
        		
        		%Now we're ready
        		obj.Ready=true;
        		
        	end
        	
        	function initForecast(obj,ForecastType,ForecastMethod)
        		%inits a forecast to a certain format, has to be run once
        		%before adding the first forecast.
        		%At the moment only 'Poisson' and 'Custom' is allowed
        		
        		%Custom format info:
        		%for the Custom format the format is a cell vector with 
        		%vectors of probability. Each element i in the vector gives
        		%the probabilty that i-1 earthquakes happen in the grid cell
        		
        		%e.g.
        		%eq:	   0	1	2	3	4	 
        		%
        		%	{ [0.5 	0.25 	0.25];
        		%	  [0.2 	0.2 	0.2 	0.15 	0.15];
        		%	   ...
        		%	   ...
        		%	  [0.9 	0.1]}	 
        		
        		%The sum of all probabilties in each vector should sum to
        		%one, if this is not the case a warning will be displayed
        		%(It should be still possible to continue with the calculation) 
        		
        		if nargin<2
        			ForecastMethod={};
        		end
        		
        		obj.ForecastType.Type=ForecastType;
        		obj.ForecastType.Method=ForecastMethod;
        		
        		obj.ForecastReady=true;
        		
        		
        	end
        	
        	
        	function addForecast(obj,ForecastData,CastTime,CastLength,DataMask)
        		%adds a Forecast to the Object, before the first forecast
        		%InitForecast has to be used and before also a Grid has to 
        		%be defined
        		%CastTime should give the start time of the forecast and
        		%CastLength how long the forecast period is, eg. CastTime=2008
        		%CastLength=1 for a 1 year forecast for the whole of 2008. The 
        		%format of time does not really matter as long as it is conisted
        		%and numerical.

        		
        		if nargin<5
        			DataMask=[];
        		end
        		
        		
        		if ~obj.Ready
        			error('No Grid defined');
        		end
        		      		
        		
        		if strcmp(obj.ForecastType.Type,'None')
        			error('No Forecast initiated, please run initForecast first')
        		end
        			
        		
        		oldPos=[];
        		%write times and change to time dependent if necessary
        		if ~isempty(obj.TimeVector)
        			if CastTime>obj.TimeVector(end)
        				if (CastTime<obj.TimeVector(end)+obj.TimeSpacing(end))&~obj.RealTimeMode
        					error('New forecast overlaps old on')
        				end
        				
        				obj.TimeVector(end+1)=CastTime;
        				obj.TimeSpacing(end+1)=CastLength;
        				obj.Dimension.Time=true;
        			else
        				%find position
        				MrSmall=find(obj.TimeVector<CastTime);
        				
        				if isempty(MrSmall)
        					error('No suitable position found');
        				end
        				
        				oldPos=MrSmall(end);
        				
        				%check if now overlap is present
        				Condition = ((obj.TimeVector(oldPos)+obj.TimeSpacing(oldPos))>CastTime)|...
        						(CastTime+CastLength)>obj.TimeVector(oldPos+1);
        				if (Condition&~obj.RealTimeMode)|obj.TimeVector(oldPos)==CastTime
        					error('There is a overlap present with an old entry, use replaceForecast if and old entry should be replaced')
        				end	
        					
        				NewTimeVec=[obj.TimeVector(1:oldPos),CastTime,obj.TimeVector(oldPos+1:end)];
        				NewSpacing=[obj.TimeSpacing(1:oldPos),CastLength,obj.TimeSpacing(oldPos+1:end)];
        				obj.TimeVector=NewTimeVec;
        				obj.TimeSpacing=NewSpacing;
        				obj.Dimension.Time=true;
        			end
        			
        		else
        			obj.TimeVector=CastTime;
        			obj.TimeSpacing=CastLength;
        		end
        		
        		%write forecast
        		if length(ForecastData)==length(obj.TheGrid)
        			
        			if isempty(oldPos)
					obj.ForecastData{end+1}=ForecastData;
					obj.EmptyGrid=false;
					MaskPos=numel(obj.ForecastData);
				else
					NewCaster={obj.ForecastData{1:oldPos},ForecastData,obj.ForecastData{oldPos+1:end}};
					obj.ForecastData=NewCaster;
					obj.EmptyGrid=false;
					MaskPos=oldPos;
				end
				
				
				if ~isempty(DataMask)
					if obj.EqualMask
						%single mask existing, check 
						if all(obj.DataMask==DataMask)
							%still a equal mask
							obj.DataMask=DataMask;
							obj.GenericMask=false;
							
						elseif GenericMask
							%it is a generic mask and 
							%can be overwritten
							obj.DataMask=DataMask;
							obj.GenericMask=false;
						else
							%more difficult part
							OldMask=obj.DataMask;
							obj.DataMask=repmat({OldMask},size(obj.ForecastData));
							obj.DataMask{MaskPos}=DataMask;
							
							obj.EqualMask=false;
							obj.GenericMask=false;
							
							
						end
					else	
						%multiple mask already used, just fill in
						obj.DataMask{MaskPos}=DataMask;
							
					end
				end
				
				%Write total distro and expval
				[OverallDistro, ExpVal] = setFullDistro(obj,ForecastData);
				obj.OverallDistro{MaskPos}=OverallDistro;
				obj.ExpectedValue(MaskPos)=ExpVal;
				
        		else
        			error('size of forecast data does not match the grid');
        		end
        		
        		%check what is present:
        		if ~isempty(obj.ForecastData)
        			obj.EmptyGrid=false;
        		else
        			obj.EmptyGrid=true;
        		end
        		
        		
        	end
        	
        	
        	function replaceForecast(obj,ForecastData,CastTime,CastLength,DataMask)
        		%works similar to the addForecast but replaces an old forecast
        		%if present, the date have to match and old data to do this
        		
        		if length(ForecastData)~=length(obj.TheGrid)
        			error('size of forecast data does not match the grid');	
        		end
        		
        		if nargin<5
        			DataMask=[];
        		end
        		
        		if isempty(obj.TimeVector)
        			%nothing present, just add
        			disp('No Forecast present, added as new forecast')
        			obj.TimeVector=CastTime;
        			obj.TimeSpacing=CastLength;
        			obj.ForecastData{end+1}=ForecastData;
				obj.EmptyGrid=false;
        		else
        			if CastTime>obj.TimeVector(end)
        				
        				if (CastTime<obj.TimeVector(end)+obj.TimeSpacing(end))&~obj.RealTimeMode
        					error('New forecast overlaps old on')
        				end
        				
        				obj.TimeVector(end+1)=CastTime;
        				obj.TimeSpacing(end+1)=CastLength;
        				obj.Dimension.Time=true;
        				obj.ForecastData{end+1}=ForecastData;
					obj.EmptyGrid=false;
					
					MaskPos=numel(obj.ForecastData);
					
					disp('Forecast is newer than every old one, and was added as new forecast');
        				
        			else
        				MrSmall=find(obj.TimeVector==CastTime);
        				
        				if isempty(MrSmall)
        					error('No Forecast to replace found');
        				end
        				
        				if numel(MrSmall)>1
        					%this should never happen
        					error('More than one suitable forecast found')
        				end
        				
        				oldPos=MrSmall;
        				
        				if isempty(CastLength)
        					%use old spacing
        					CastLength=obj.TimeSpacing(oldPos);
        				end	
        				
        				%check if now overlap is present needs three conditions (if it is on start or end)
        				if numel(obj.TimeVector)==1&oldPos==1 
        					Condition=false;
        				elseif oldPos==1
        					Condition = (CastTime+CastLength)>obj.TimeVector(oldPos+1);
        				elseif oldPos==numel(obj.TimeVector)
        					Condition = ((obj.TimeVector(oldPos-1)+obj.TimeSpacing(oldPos-1))>CastTime);
        				else
        					Condition = ((obj.TimeVector(oldPos-1)+obj.TimeSpacing(oldPos-1))>CastTime)|...
        							(CastTime+CastLength)>obj.TimeVector(oldPos+1);
        				end
        				
        				if Condition&~obj.RealTimeMode
        					error('There is a overlap present with an old entry')
        				end	
        					
        				obj.TimeVector(oldPos)=CastTime;
        				obj.TimeSpacing(oldPos)=CastLength;
        				obj.ForecastData{oldPos}=ForecastData;
        				
        				MaskPos=oldPos;

        				
        			end
        			
        			
        			if ~isempty(DataMask)
					if obj.EqualMask
						%single mask existing, check 
						if all(obj.DataMask==DataMask)
							%still a equal mask
							obj.DataMask=DataMask;
							obj.GenericMask=false;
							
						elseif GenericMask
							%it is a generic mask and 
							%can be overwritten
							obj.DataMask=DataMask;
							obj.GenericMask=false;
						else
							%more difficult part
							OldMask=obj.DataMask;
							obj.DataMask=repmat({OldMask},size(obj.ForecastData));
							obj.DataMask{MaskPos}=DataMask;
							
							obj.EqualMask=false;
							obj.GenericMask=false;
							
							
						end
					else	
						%multiple mask already used, just fill in
						obj.DataMask{MaskPos}=DataMask;
							
					end
				end
        			
				%Write total distro and expval
				[OverallDistro ExpVal] = setFullDistro(obj,ForecastData);
				obj.OverallDistro{MaskPos}=OverallDistro;
				obj.ExpectedValue(MaskPos)=ExpVal;
				
        		
        		end
        		
        		if numel(obj.TimeVector)>=2
        			obj.Dimension.Time=true;
        		else
        			obj.Dimension.Time=false;
        		end
        		
        		
        		%just to be sure
        		%check what is present:
        		if ~isempty(obj.ForecastData)
        			obj.EmptyGrid=false;
        		else
        			obj.EmptyGrid=true;
        		end
        		
        	end
        	
        	
        	function removeForecast(obj,CastTime)
        		%removes a forecast from the list if found.
        		if isempty(obj.TimeVector)
        			error('No forecasts saved')
        		end
        		
        		oldPos=find(obj.TimeVector==CastTime);
        		
        		if isempty(oldPos)
        			error('No Forecast with this date found');
        		end
        		
        		if numel(oldPos)>1
        			%this should never happen
        			error('More than one suitable forecast found')
        		end
        		
        		%special case: forecast empty after removal
        		if numel(obj.TimeVector)==1
        			disp('Last forecast deleted')
        			obj.TimeVector=[];
        			obj.TimeSpacing=[];
        			obj.ForecastData={};
				obj.EmptyGrid=true;
				obj.DataMask=[];
				obj.EqualMask=true;
				obj.GenericMask=true;
				obj.OverallDistro={};
				obj.ExpectedValue=[];
				
				
        		else
        			NewTimeVec=[obj.TimeVector(1:oldPos-1),obj.TimeVector(oldPos+1:end)];
        			NewSpacing=[obj.TimeSpacing(1:oldPos-1),obj.TimeSpacing(oldPos+1:end)];
        			obj.TimeVector=NewTimeVec;
        			obj.TimeSpacing=NewSpacing;
        			NewCaster={obj.ForecastData{1:oldPos-1},obj.ForecastData{oldPos+1:end}};
				obj.ForecastData=NewCaster;
        			
        			if numel(obj.TimeVector)>=2
        				obj.Dimension.Time=true;
        			else
        				obj.Dimension.Time=false;
        			end
				
        			if ~obj.EqualMask
        				NewMaskVec={obj.DataMask{1:oldPos-1},obj.DataMask{oldPos+1:end}};
        				obj.DataMask=NewMaskVec;
        			end
        			
        			NewOverallDistro={obj.OverallDistro{1:oldPos-1},obj.OverallDistro{oldPos+1:end}};
        			obj.DataMask=NewOverallDistro;
        			NewExpVec=[obj.ExpectedValue(1:oldPos-1),obj.ExpectedValue(oldPos+1:end)];
        			obj.DataMask=NewExpVec;
        			
        		end
        		
        		%check what is present:
        		if ~isempty(obj.ForecastData)
        			obj.EmptyGrid=false;
        		else
        			obj.EmptyGrid=true;
        		end
        		
        		
        	end	
        	
        	
        	
        	
        	
        	function importCSEPForecast(obj,RelmGrid,CastTime,CastLength,ForecastType)
        		%allows to import a CSEP or RELM forecast directly. Caution:
        		%The previous grid will be overwritten.
        		
        		if nargin<5
        			ForecastType='Poisson'
        		end	
        			
        		
        		if ~obj.EmptyGrid|obj.Ready
        			disp('Grid and forecasts will be overwritten');
        			warning('Grid and forecasts will be overwritten');
        		end
        		
        		if isempty(CastTime)|isempty(CastLength)
        			error('No Time specified')
        		end
        		
        		
        		%reset the dimension in case it is needed
        		obj.Dimension=struct(	'Space',false,...
        					'Depth',false,...
        					'Mag',false,...
        					'Time',false);
        		
        		%Split of the Poissonian forecast
        		switch ForecastType
        			case 'Poisson'
        				ForecastData=RelmGrid(:,9);
        				TheGrid=RelmGrid(:,1:8);
        				
        				ForeLength=numel(RelmGrid(:,9));
        				MaskThere=numel(RelmGrid(1,:))==10;
        				
        				if MaskThere
        					TheMask=RelmGrid(:,10);
        				end
        				
        			case 'Custom'
        				ForecastData=RelmGrid{2};
        				TheGrid=RelmGrid{1}(:,1:8);
        				obj.NodeIDs=RelmGrid{1}(:,9)
        				
        				ForeLength=numel(RelmGrid{1}(:,9));
        				MaskThere=numel(RelmGrid{1}(1,:))==10;
        				
        				if MaskThere
        					TheMask=RelmGrid{1}(:,10);
        				end
        				
        			case 'NegBin'
        				ForecastData=RelmGrid(:,9,11);
        				TheGrid=RelmGrid(:,1:8);
        				
        				ForeLength=numel(RelmGrid(:,9));
        				MaskThere=numel(RelmGrid(1,:))==11;
        				
        				if MaskThere
        					TheMask=RelmGrid(:,10);
        				end
        				
        		end	
        		
        		%EmptyBag
        		obj.TheBag={};
        		
        		%pass it to the function which does the actuall work
        		obj.addGrid(TheGrid);
        		
        		%init forecast
        		obj.initForecast(ForecastType,{});
        		
        		%add forecast (to be sure, reset everything)
        		obj.TimeVector=CastTime;
        		obj.TimeSpacing=CastLength;
        		obj.ForecastData={};
        		obj.ForecastData{1}=ForecastData;
        		obj.EmptyGrid=false;
        		
        		%set DataMask
        		if MaskThere
        			obj.DataMask=logical(TheMask);
        			obj.GenericMask=false;
        		else
        			obj.DataMask=true(ForeLength,1);
        			obj.GenericMask=false;
        		end
        		
        		obj.EqualMask=true;
        		
        		%Write total distro and expval
			[OverallDistro ExpVal] = setFullDistro(obj,ForecastData);
			obj.OverallDistro={OverallDistro};
			obj.ExpectedValue=ExpVal;
        		
        		
        	end
        	
        	
        	function [CSEPGrid ForeTime ForeLength]= getCSEPForecast(obj,CastTime)
        		%Returns a CSEP grid for a certain time, if the time is
        		%not existing the nearest lower time is used instead
        		
        		
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVector-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        			
        		else
        			FindInTime=1;
        		end
        		
        		
        		
        		if obj.EqualMask
        			TheMask=obj.DataMask;
        			
        		else
        			TheMask=obj.DataMask{FindInTime};
        		end
        		
        		
        		switch obj.ForecastType.Type
        			case 'Poisson'
					CSEPGrid=[obj.TheGrid(:,1:8),obj.ForecastData{FindInTime},TheMask];
					ForeTime=obj.TimeVector(FindInTime);
					ForeLength=obj.TimeSpacing(FindInTime);
				
				case 'NegBin'
					CSEPGrid=[obj.TheGrid(:,1:8),obj.ForecastData{FindInTime}(:,1),TheMask,...
							obj.ForecastData{FindInTime}(:,2)];
					ForeTime=obj.TimeVector(FindInTime);
					ForeLength=obj.TimeSpacing(FindInTime);
					
					
				case 'Custom'
					if isempty(obj.NodeIDs)
						obj.NodeIDs=(1:numel(obj.TheGrid(:,1)))';
					end	
					CSEPGrid={[obj.TheGrid,obj.NodeIDs,TheMask],obj.ForecastData{FindInTime}};
					ForeTime=obj.TimeVector(FindInTime);
					ForeLength=obj.TimeSpacing(FindInTime);	
				
				
				case 'None'
					error('No forecast set')
			end	
        			
        			

        	
        	end
        	
        	
        	function NrNodes=getNrNodes(obj)
        		%name says it, it returns the number of nodes in the grid
        		
        		if ~isempty(obj.TheGrid)
        			NrNodes=numel(obj.TheGrid(:,1));
        		else
        			NrNodes=0;
        		end
        		
        	end
        	
        	
        	function [ForeSlice ForeTime ForeLength] = get2DSlice(obj,CastTime,Magnitude,Depth)
        		%returns a slice of the forecast as 2D matrix.
        		%Magnitude and/or Depth can be left empty if the grid has only one 
        		%bin for one of them.
        		%For Magnitude and Depth also the keyword 'all' can be used, in this 
        		%case a summation of all grid cells in that dimension will be returned.
        		
        		%search if the forecast can be found
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVector-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
			%condition for 3D (either Mag or Depth but not both)
			Cond3d=(obj.Dimension.Depth | obj.Dimension.Mag) & ~(obj.Dimension.Depth & obj.Dimension.Mag);
			%condition to stop and return an error
			BreakCond=(obj.Dimension.Depth&isempty(Depth))|(obj.Dimension.Mag&isempty(Magnitude)); 
			
			if strcmp(obj.ForecastType.Type,'None')
				error('No forecast set')
			end
			
			%Maybe possible to simplify this a bit
			
				
			RawGrid=obj.ForecastData{FindInTime};
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);
			
			%use the masking if needed
			if ~isempty(obj.DataMask)
				if obj.EqualMask
					TheMask=obj.DataMask;
					
				else
					TheMask=obj.DataMask{FindInTime};
				end
				
				RawGrid = MatrixCorrector(obj,RawGrid,~TheMask,'set');
			end
			
			
			if ~obj.Dimension.Depth&~obj.Dimension.Mag
				%only 2D data, the easiest case

				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice RawVolume] = BuildRawGrid(obj,false);
				ForeSlice(obj.SelectedNodes)=RawGrid(obj.SelectedIDs);
				
			elseif Cond3d
				%3D case
				if BreakCond
					error('Depth or Mag input variable not defined')
				end
				
				NewSelID=obj.SelectedIDs;
					
				%only take Ids which are not nan
				NotNaN=~isnan(NewSelID);
				NewSelID=NewSelID(NotNaN);
				%It should work like this, but 
				%maybe it could lead to some problems
				%if the parts containing nan are not
				%removed in the result (will leave it
				%for now)
				
				
				if obj.Dimension.Depth
					if strcmp(Depth,'all')
						%sum up
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(obj,false);
						RawSlice=ForeSlice;
																				
						
						%first step
						ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID);
						
						
						
						%additonal ones
						for i=2:numel(obj.DepthBins)
							NewSlice=RawSlice;
							NewSlice(obj.SelectedNodes)=RawGrid(NewSelID+(i-1))
							ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
							
						end
					else
						%single field
						SliPos=find(obj.DepthBins==Depth);
						
						
						
						
						if isempty(SliPos)
							error('Depth not found');
						end
						
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(obj,false);
						ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID+(SliPos-1));
						%PLEASE TEST IT
														
					end
					
					
				elseif obj.Dimension.Mag
					if strcmp(Magnitude,'all')
						%sum up
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(obj,false);
						RawSlice=ForeSlice;
						
						%first step
						ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID);
						
						%additonal ones
						for i=2:numel(obj.MagBins)
						
							NewSlice=RawSlice;
							NewSlice(obj.SelectedNodes)=RawGrid(NewSelID+(i-1));
							ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
						end
					else
						%single field
						SliPos=find(obj.MagBins==Magnitude);
						
						if isempty(SliPos)
							error('Magnitude not found');
						end
						
						%ForeSlice=NaN(size(obj.SelectedNodes));
						[ForeSlice RawVolume] = BuildRawGrid(obj,false);
						ForeSlice(obj.SelectedNodes)=RawGrid(NewSelID+(SliPos-1));
						%PLEASE TEST IT
														
					end
				end
				
			else
				%full 4D (most difficult case with two jumps
				%ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
				[ForeSlice RawVolume] = BuildRawGrid(obj,true);
				
				if BreakCond
					error('Depth or Mag input variable not defined')
				end
				
				
				if ~strcmp(Depth,'all')&~strcmp(Magnitude,'all')
					%no summation needed
					MagPos=find(obj.MagBins==Magnitude);
					
					if isempty(MagPos)
						error('Magnitude not found');
					end
					
					DepthPos=find(obj.DepthBins==Depth);
					
					if isempty(DepthPos)
						error('Depth not found');
					end
					
					NewSelID=obj.SelectedIDs{1}{DepthPos}+(MagPos-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
					
				elseif strcmp(Depth,'all')&~strcmp(Magnitude,'all')
					%sumamation of the depth needed
					MagPos=find(obj.MagBins==Magnitude);
					
					if isempty(MagPos)
						error('Magnitude not found');
					end
					
					%Dummy slice
					RawSlice=ForeSlice;
					
					%first element to prevent zeros
					NewSelID=obj.SelectedIDs{1}{1}+(MagPos-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
					
					%now the rest
					for i=2:numel(obj.DepthBins)
						NewSelID=obj.SelectedIDs{1}{i}+(MagPos-1);
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						%again the annoying substep
						NewSlice=RawSlice;
						NewSlice(obj.SelectedNodes{1}{i})=RawGrid(NewSelID);
						ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
					end
					
				
				elseif ~strcmp(Depth,'all')&strcmp(Magnitude,'all')
					%summation of the magnitude
					DepthPos=find(obj.DepthBins==Depth);
					
					if isempty(DepthPos)
						error('Depth not found');
					end
					
					%Dummy slice
					RawSlice=ForeSlice;
					
					%first element to prevent zeros
					NewSelID=obj.SelectedIDs{1}{DepthPos};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
					
					%now the rest
					for i=2:numel(obj.MagBins)
						NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1);
						
						%again the annoying substep
						NewSlice=RawSlice;
						NewSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
						ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);
					end
					
					
				else
					%summation of magnitude and depth
					
					%first do a prototype slice
					NewSelID=obj.SelectedIDs{1}{1};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
					MrNan = MatrixCorrector(obj,ForeSlice,[],'get');
					%MrNan=isnan(ForeSlice);
					
					%make a new Sum slice with zeros
					ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
					RawSlice=ForeSlice;
					
					%now sum them all
					for d=1:numel(obj.DepthBins)
						for m=1:numel(obj.MagBins)
							NewSelID=obj.SelectedIDs{1}{d}+(m-1);
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							NewSlice=RawSlice;
							%again the annoying substep
							NewSlice(obj.SelectedNodes{1}{d})=RawGrid(NewSelID);
							ForeSlice=SumFullDistro(obj,ForeSlice,NewSlice);	
							
						end
					end
					
					%Kill all zeros which should not be existing
					%ForeSlice(MrNan)=NaN;
					ForeSlice=MatrixCorrector(obj,ForeSlice,MrNaN,'set');
					
				end
				
				
				
				
			
			end
					
			
        			
        			
        	
        		
        		
        		
        	end
        	
        	
        	
        	function [ForeVolume ForeTime ForeLength] = get3DVolume(obj,CastTime,Magnitude,Depth)
        		%same as get2DSlice, but for Volumes, at least 3D dimensions (without time) 
        		%are needed this function to work
        		
        		%Caution some of the stuff is experimental and it is not sure if it works in every
        		%case. For instace could the depth summation fail in  cases the region is not
        		%cylindrical (which isn't allowed right now anyway)
        		
        		if ~obj.Dimension.Space&(~obj.Dimension.Depth|~obj.Dimension.Mag)
        			%missing a necessary dimension
        			error('Not enough dimension available'); 
        		end
        		
        		
        		%search if the forecast can be found
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVector-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
        		%just in case
        		if strcmp(obj.ForecastType.Type,'None')
				error('No forecast set')
			end
        		
        		
        
			RawGrid=obj.ForecastData{FindInTime};
			ForeTime=obj.TimeVector(FindInTime);
			ForeLength=obj.TimeSpacing(FindInTime);
			
			
			%use the masking if needed
			if ~isempty(obj.DataMask)
				if obj.EqualMask
					TheMask=obj.DataMask;
					
				else
					TheMask=obj.DataMask{FindInTime};
				end
				
				RawGrid = MatrixCorrector(obj,RawGrid,~TheMask,'set');
			end
			
			
			
			try
				%ForeVolume=NaN(size(obj.SelectedNodes{2}));
				%ForeSlice=NaN(size(obj.SelectedNodes{1}{1}));
				[ForeSlice ForeVolume] = BuildRawGrid(obj,true)
			catch
				%probalby 2D
				%ForeVolume=[];
				%ForeSlice=NaN(size(obj.SelectedNodes));
				[ForeSlice ForeVolume] = BuildRawGrid(obj,false)
			end
			
			
			if obj.Dimension.Depth&obj.Dimension.Mag
				%full 4D case
				BreakCond = 	(isempty(Depth)&isempty(Magnitude))|...
						(~isempty(Depth)&~isempty(Magnitude));
					
				%possible errors
				if BreakCond
					error('Wrong Depth and/or Magnitude parameter')
				end
				
				if strcmp(Depth,'all')&strcmp(Magnitude,'all') 
					error('only on parameter can be set to "all"');
				end
				
				
				%different cases (summation over one dimension or a specific slide)
				if strcmp(Depth,'all')
					ForeVolume=[];
					
					%again to prevent unnecessary zeros
					Tempslice=ForeSlice;
					NewSelID=obj.SelectedIDs{1}{1};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					
					Tempslice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
					%MrNan=isnan(Tempslice);
					MrNan = MatrixCorrector(obj,Tempslice,[],'get');
					%ForeSlice=zeros(size(obj.SelectedNodes{1}{1}));
					[ForeSlice RawVolume] = BuildRawGrid(obj,true,true);
					
					%all depth have to be summed and the new "depth" is the magnitude
					for i=1:numel(obj.MagBins)
						
						DepthSlice=ForeSlice;
						
						
						%first step (again the slighty annoying extra step has to be done)
						NewSelID=obj.SelectedIDs{1}{1}+(i-1);
						DepthSlice(obj.SelectedNodes{1}{1})=RawGrid(NewSelID);
						
						for j=2:numel(obj.DepthBins)
							NewSlice=ForeSlice;
							NewSelID=obj.SelectedIDs{1}{j}+(i-1);
							
							%only take Ids which are not nan
							NotNaN=~isnan(NewSelID);
							NewSelID=NewSelID(NotNaN);
							
							NewSlice(obj.SelectedNodes{1}{j})=RawGrid(NewSelID);
							DepthSlice=SumFullDistro(obj,DepthSlice,NewSlice);
						
						end
						
						%reapply nan
						%DepthSlice(MrNan)=NaN;
						DepthSlice=MatrixCorrector(DepthSlice,MrNan,'set');
						%save in in 3D matrix
						ForeVolume(:,:,i)=DepthSlice;
						
						
				
					end
					
					
					
					
				elseif strcmp(Magnitude,'all') 
					%One of the real 3D cases
					
					%It has to be done a bit more complicated than I originally did think
					%because using a matrix with logical matrix results in a matrix, expect
					%you write an other matrix into it (sounds wierd I know)
					RawVol=ForeVolume;
					
					%first step
					NewSelID=obj.SelectedIDs{2};
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
					
					for i=2:numel(obj.MagBins)
						NewSlide=RawVol;
						NewSelID=obj.SelectedIDs{2}+(i-1);
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						NewSlide(obj.SelectedNodes{2})=RawGrid(NewSelID);
						
						%now sum up the single volumes without any selector
						ForeVolume=SumFullDistro(obj,ForeVolume,NewSlide);
						
					end
					
					
				elseif ~isempty(Depth)
					%Similar to the all case but with a specified depth
					ForeVolume=[];
					DepthPos=find(obj.DepthBins==Depth);
					
					if isempty(DepthPos)
						error('Depth not found');
					end
											
					%get the magnitude slices
					for i=1:numel(obj.MagBins)
						
						DepthSlice=ForeSlice;
						
						NewSelID=obj.SelectedIDs{1}{DepthPos}+(i-1)
						
						%only take Ids which are not nan
						NotNaN=~isnan(NewSelID);
						NewSelID=NewSelID(NotNaN);
						
						DepthSlice(obj.SelectedNodes{1}{DepthPos})=RawGrid(NewSelID);
						
						%save in in 3D matrix
						ForeVolume(:,:,i)=DepthSlice;
						
					
					end
					
					
				elseif ~isempty(Magnitude)
					MagPos=find(obj.MagBins==Magnitude);
					
					if isempty(MagPos)
						error('Magnitude not found');
					end
					
					NewSelID=obj.SelectedIDs{2}+(MagPos-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
				end
				
				
				
			elseif obj.Dimension.Depth&~obj.Dimension.Mag
				%"real" 3D case with depth,in this case no Depth and Magnitude
				%input is "allowed" (means it will be ignored)
				if ~isempty(Depth)&~isempty(Magnitude)
					disp('Depth and Magnitude input ignored');
					warning('Depth and Magnitude input ignored');
				end
				
				NewSelID=obj.SelectedIDs{2};
				
				%only take Ids which are not nan
				NotNaN=~isnan(NewSelID);
				NewSelID=NewSelID(NotNaN);
				
				ForeVolume(obj.SelectedNodes{2})=RawGrid(NewSelID);
				
				
				
			elseif ~obj.Dimension.Depth&obj.Dimension.Mag
				%Magnitude as 3rd dimension,in this case no Depth and Magnitude
				%input is "allowed" (means it will be ignored)
				if ~isempty(Depth)&~isempty(Magnitude)
					disp('Depth and Magnitude input ignored');
					warning('Depth and Magnitude input ignored');
				end
				
				ForeVolume=[];
				
					
											
				%get the magnitude slices
				for i=1:numel(obj.MagBins)
					
					DepthSlice=ForeSlice;
					disp(i)
					NewSelID=obj.SelectedIDs+(i-1);
					
					%only take Ids which are not nan
					NotNaN=~isnan(NewSelID);
					NewSelID=NewSelID(NotNaN);
					
					DepthSlice(obj.SelectedNodes)=RawGrid(NewSelID);
					
					%save in in 3D matrix
					ForeVolume(:,:,i)=DepthSlice;

				end
				
				
			end
	
        		
        		
        	end
        	
        	
        	
        	
        	function Name = getName(obj)
        		%getter function for name
        		Name = obj.Name;
        		
        		%just a good opportunity to check if the name was 
        		%correctly set
        		if isempty(Name)
        			warning('No Name was defined')
        		end
        			
        		
        	end
        	
        	
        	function ID = getID(obj)
        		%getter function for ID
        		ID = obj.ID;
        		
        		%just a good opportunity to check if the name was 
        		%correctly set
        		if isempty(ID)
        			warning('No ID was defined')
        		end
        			
        		
        	end
        	
        	
        	
        	function setName(obj,Name)
        		%setter function for name
        		
        		if isempty(Name)
        			error('No Name defined')
        		end
        		
        		if ~ischar(Name)
        			error('Name has to be a string')
        		end
        		
        		obj.Name = Name;
        		
        	
        			
        		
        	end
        	
        	
        	function PortableStruct =  exportFullData(obj)
        		%meant to export everything for later use in a new object
        		%Only needed fields are currently exported, all which can
        		%be reproduced will not be exported. This may change if it
        		%turns out that the creation of this data is to computing
        		%intensive for the common cases.
        		
        		PortableStruct = struct('Name',obj.Name,...
        					'ID',obj.ID,...
        					'Version',obj.Version,...
        					'TheGrid',obj.TheGrid,...
        					'GridPrecision',obj.GridPrecision,...
        					'RegionPoly',obj.RegionPoly,...
        					'BoundBox',obj.BoundBox,...
        					'TimeVector',obj.TimeVector,...
        					'TimeSpacing',obj.TimeSpacing,...
        					'RealTimeMode',obj.RealTimeMode,...
        					'Dimension',obj.Dimension,...
        					'ForecastData',obj.ForecastData,...
        					'ForecastType',obj.ForecastType,...
        					'ExpectedValue',obj.ExpectedValue,...
        					'OverallDistro',obj.OverallDistro,...
        					'DataMask',obj.DataMask,...
        					'EqualMask',obj.EqualMask,...
        					'GenericMask',obj.GenericMask,...
        					'ForecastReady',obj.ForecastReady);
        		
        		
        	end
        	
        	
        	function importFullData(obj,PortableStruct)
        		%import a previous exported data set
        		%restores a forecast from a data dump.
        		%In future versions this may also updates a data dump
        		%to the most current version.
        		
        		%Set everything back in original state as done by the 
        		%constructor
        		obj.Version='0.9';
        		%init data
        		obj.EmptyGrid=true;
        		obj.Ready=false;
        		obj.TheGrid=[];
        		obj.GridNodes=[];
        		obj.AxisVectors={};
        		obj.GridSpacings=[];
        		obj.GridPrecision=10^(-5);
        		obj.RealTimeMode=false;
        		obj.GridLimits=[];
        		obj.MagBins=[];
        		obj.DepthBins=[];
        		obj.RegionPoly=[];
        		obj.BoundBox=[];
        		obj.TimeVector=[];
        		obj.TimeSpacing=[];
        		obj.Dimension=struct(	'Space',false,...
        					'Depth',false,...
        					'Mag',false,...
        					'Time',false);
        		obj.SelectedNodes=[];
        		obj.SelectedIDs=[];
        		obj.JumpValue=[];
        		obj.ForecastData={};
        		
        		obj.ForecastType=struct('Type','None',...
        					'Method',{{}});
        		obj.TheBag={};
        		obj.ForecastReady=false;
        		
        		%Add the grid
        		obj.GridPrecision=PortableStruct.GridPrecision;
        		obj.addGrid(PortableStruct.TheGrid);
        		
        		%add the rest of the data directly (could be looped, but
        		%I prefere it like this, more control)
        		obj.Name=PortableStruct.Name;
        		obj.ID=PortableStruct.ID;
        		obj.RegionPoly=PortableStruct.RegionPoly;
        		obj.BoundBox=PortableStruct.BoundBox;
        		obj.TimeVector=PortableStruct.TimeVector;
        		obj.TimeSpacing=PortableStruct.TimeSpacing;
        		obj.RealTimeMode=PortableStruct.RealTimeMode;
        		obj.Dimension=PortableStruct.Dimension;
        		obj.ForecastData=PortableStruct.ForecastData;
        		obj.ForecastType=PortableStruct.ForecastType;
        		obj.ExpectedValue=PortableStruct.ExpectedValue;
        		obj.OverallDistro=PortableStruct.OverallDistro;
        		obj.DataMask=PortableStruct.DataMask;
        		obj.EqualMask=PortableStruct.EqualMask;
        		obj.GenericMask=PortableStruct.GenericMask;
        		obj.ForecastReady=PortableStruct.ForecastReady;
        		
        		%check what is present:
        		if ~isempty(obj.ForecastData)
        			obj.EmptyGrid=false;
        		else
        			obj.EmptyGrid=true;
        		end
        		
        	
        	end
        	
        	
        	function Depths = getDepths(obj)
        		%simple getter function, as the fields are the moment
        		%public it would not be really needed, but it may needed
        		%later, either because the fields will not be public or 
        		%because some addition functionality is needed.
        		if obj.Dimension.Depth
        			Depths = obj.DepthBins;
        		else
        			Depths = [];
        		end
        		
        		
        	end
        	
        	
        	function Magnitudes = getMagnitudes(obj)
        		%simple getter function, as the fields are the moment
        		%public it would not be really needed, but it may needed
        		%later, either because the fields will not be public or 
        		%because some addition functionality is needed.
        		
        		if obj.Dimension.Mag
        			Magnitudes = obj.MagBins;
        		else
        			Magnitudes = [];
        		end
        	end
        	
        	
        	function TheGrid = getFullGrid(obj)
        		%returns as empty RELM type grid with column 9 being the
        		%id and as a bonus column 10 being the mask. The mask is 
        		%bonus as it could be varying anyway, maybe ignore in
        		%most cases
        		
        		if ~obj.Ready
        			error('No Grid defined');
        		end
        		
        		if ~isempty(obj.DataMask)
        			if obj.EqualMask
        				TheMask=obj.DataMask;
        			
        			else
        				%use the first
        				TheMask=obj.DataMask{1};
        			end
        		else
        			TheMask=ones(size(obj.TheGrid(:,1)));	
        		
        		
        		end
        		
        		
        		TheGrid=[obj.TheGrid,obj.NodeIDs,TheMask];
        	end
        	
        	
        	
        	function GridNode = getGridNodes(obj)
        		%Returns a TripleS type grid with only the spacial grid
        		%nodes and centres of the cells instead of corners.
        		
        		if ~obj.Ready
        			error('No Grid defined');
        		end
        		
        		TheGrid = getFullGrid(obj);
        		
        		%generate a GridNode
        		ElementSelector=find(TheGrid(1,5)==TheGrid(:,5)&...
        					TheGrid(1,7)==TheGrid(:,7));     	
        			
        		GridNode(:,1)=(TheGrid(ElementSelector,1)+TheGrid(ElementSelector,2))/2;
        		GridNode(:,2)=(TheGrid(ElementSelector,3)+TheGrid(ElementSelector,4))/2;
        		
        	
        	end
        	
        	
        	
        	function [LonVec LatVec DepthVec MagVec] = getAxisVectors(obj)
        		%returns the vectors for all possible axis available
        		%all coordinates give the middle of the cell not the 
        		%corners
        		
        		%defaults
        		LonVec = [];
        		LatVec = [];
        		DepthVec = [];
        		MagVec = [];

        		
        		%check what is available
        		if obj.Dimension.Space
        			LonVec=obj.AxisVectors{1}+obj.GridSpacings(1)/2;
        			LatVec=obj.AxisVectors{2}+obj.GridSpacings(2)/2;
        		end
        		
        		
        		if obj.Dimension.Depth
        			DepthVec=obj.DepthBins;
        		end

      		
	   		if obj.Dimension.Mag
        			MagVec=obj.MagBins;
        		end
        		
        
        		
        		
        	end
        	
        	
        	function [SpaceGrid DepthGrid  MagGrid] = getAxisMesh(obj)
        		%similar to getAxisVectors, but it generate a meshed grid
        		%for the axis, which is needed by some plot functions
        		%All outputs are cell arrays with the needed grids in them
        		%SpaceGrid: {Lon2D Lat2D}
        		%DepthGrid: {Lon3D Lat3D Depth}
        		%MagGrid: {Lon3D Lat3D Mag}
        		
        		%defaults
        		SpaceGrid  = {};
        		DepthGrid = {};
        		MagGrid = {};

        		
        		%check what is available
        		if obj.Dimension.Space
        			LonVec=obj.AxisVectors{1}+obj.GridSpacings(1)/2;
        			LatVec=obj.AxisVectors{2}+obj.GridSpacings(2)/2;
        			[SpaceGrid{1} SpaceGrid{2}] = meshgrid(LonVec,LatVec);			
        		end
        		
        		
        		if obj.Dimension.Depth&obj.Dimension.Space
        			DepthVec=obj.DepthBins;
        			[DepthGrid{1} DepthGrid{2} DepthGrid{3}] = meshgrid(LonVec,LatVec,DepthVec);	
        		end

      		
	   		if obj.Dimension.Mag&obj.Dimension.Space
        			MagVec=obj.MagBins;
        			[MagGrid{1} MagGrid{2} MagGrid{3}] = meshgrid(LonVec,LatVec,MagVec);
        		end
        		
        
        		
        		
        	end
        	
        	
        	
        	function [OverallDistro ExpVal] = setFullDistro(obj,ForecastData)
        		%produces the CombindeDistro (or sum of all rates for 
        		%Poisson) together with the expected value. 
        	
        		import mapseis.util.stat.CombineDiscretDistro;
        		
        		OverallDistro=[]; 
        		ExpVal=[];
        		
        		switch obj.ForecastType.Type
        			case 'Poisson'
        				OverallDistro=nansum(ForecastData);
        				ExpVal=OverallDistro;
        				
        			case 'Custom'

        				OverallDistro=1;
        				
        				for i=1:numel(ForecastData)
        					OverallDistro=CombineDiscretDistro(OverallDistro,ForecastData{i},true);
        				end
        				
        				ExpVal=sum((0:1:numel(OverallDistro)-1)'.*OverallDistro);
        	
        			case 'NegBin'
        				ExpVal=nansum(((1-ForecastData(:,2)).*ForecastData(:,1))./ForecastData(:,2));
        				OverallDistro=ExpVal;
        		end	
        		
        		
        		
        	end
        	
        	
        	function [OverallDistro ExpVal] = getTotalRate(obj,CastTime)
        		%returns the overall rate or distribution of the forecast
        		%simple for Poisson, but not for a custom distribution
        		
        		if obj.EmptyGrid
        			error('No forecast set');
        		end
        		
        		if obj.Dimension.Time
        			%time depended
        			FindInTime=obj.TimeVector==CastTime;
        			
        			if ~any(FindInTime)
        				%get the next lower time
        				ModTimeVec=obj.TimeVector-CastTime;
        				SmallerZero=find(ModTimeVec<0);
        				
        				if isempty(SmallerZero)
        					error('No forecast before this time available');
        				end
        				
        				
        				FindInTime=SmallerZero(end);
        				
        			end
        		else
        		
        			FindInTime=1;
        		end
        		
			
			
			if strcmp(obj.ForecastType.Type,'None')
				error('No forecast set')
			end
			
			OverallDistro = obj.OverallDistro{FindInTime};
			ExpVal =  obj.ExpectedValue(FindInTime);
			
			
			
		end	
        	
        	
        	
        	%Parts for the Slice and Volume methods
        	
        	function [RawSlice RawVolume] = BuildRawGrid(obj,VolumeMode,ZeroMode)
        		%The function just builds raw grid data depended on the 
        		%forecast type (NaNs for poisson, empty cells for Custom)
        		%VolumeMode selects the caller, true for get3DVolume 
        		%and get2DSlice in full 3D mode and false for "real 2D"
        		%get2DSlice
        		
        		if nargin<3
        			ZeroMode=false;
        		end
        		
        		
        		switch obj.ForecastType.Type
        		
        			case 'Poisson'
        				if VolumeMode
        					if ~ZeroMode
        						RawSlice = NaN(size(obj.SelectedNodes{1}{1}));
        						RawVolume = NaN(size(obj.SelectedNodes{2}));
        					
        					else
        						RawSlice = zeros(size(obj.SelectedNodes{1}{1}));
        						RawVolume = zeros(size(obj.SelectedNodes{2}));
        					end
        					
        				else
        					if ~ZeroMode
        						RawSlice = NaN(size(obj.SelectedNodes));
        						RawVolume = [];
        						
        					else
        						RawSlice = zeros(size(obj.SelectedNodes));
        						RawVolume = [];
        					
        					end
        				end
        					
        				
        			case 'Custom'
        				if VolumeMode
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        					else
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        						
        					end
        					
        				else
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        						
        					else
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        					
        					end
        				end
        				
        				
        			case 'NegBin'
        				%Missing currently, guess it will be cell
        				if VolumeMode
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        					else
        						RawSlice = cell(size(obj.SelectedNodes{1}{1}));
        						RawVolume = cell(size(obj.SelectedNodes{2}));
        						
        					end
        					
        				else
        					if ~ZeroMode
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        						
        					else
        						RawSlice = cell(size(obj.SelectedNodes));
        						RawVolume = [];
        					
        					end
        				end
        		end	
        		
        	
        	
        	end
        	
        	
        	
        	function SumCast = SumFullDistro(obj,DistA,DistB)
        		%Sum all distribution of a slice/volume with 
        		%another slice/volume. The dimension of DistA and DistB
        		%has to be the same
        		
        		import mapseis.util.stat.CombineDiscretDistro;
        		
        		switch obj.ForecastType.Type
        			case 'Poisson'
        				%super simple stuff
        				SumCast=DistA+DistB;
        				
        			case 'Custom'
        				%The difficult and time consuming part
        				SumCast=cell(size(DistA));
        				
        				for i=1:numel(DistA)
        					SumCast{i}=CombineDiscretDistro(DistA{i},DistB{i},true);
        				end
        			
        				
        			case 'NegBin'
        				%currently not supported
        			
        		end	
        	
        	end
        	
        	
        	
        	
        	function ResMat = MatrixCorrector(obj,inMat,LogicArray,WorkMode)
        		%This function is meant to replace the isnan, set to
        		%nan block in the functions to make it compatible with 
        		%the custom distributions
        		%LogicArray can be empty for 'get'
        		
        		import mapseis.util.emptier;
        		
        		ResMat=[];
        		
        		
        		if strcmp(WorkMode,'get')
        			switch obj.ForecastType.Type
					case 'Poisson'
						ResMat=isnan(inMat);
						
					case 'Custom'
						ResMat=emptier(inMat);				
						
					case 'NegBin'
						%currently not supported
        			
				end	
        			
        		elseif strcmp(WorkMode,'set')
        			switch obj.ForecastType.Type
					case 'Poisson'
						ResMat=inMat;
						ResMat(LogicArray)=NaN;
						
					case 'Custom'
						ResMat=inMat;
						ResMat(LogicArray)=cell(sum(sum(LogicArray)),1);				
						
					case 'NegBin'
						%currently not supported
        			
				end	
        		
        		else
        			disp('Please select correct workmode');
        		
        		end	
        		
        	
        	end
        	
        	function NewObj = cloneWars(obj)
        		%generates a clone of the existing object, equal in every
        		%property but with a different ID
        		%Name
        		
        		import mapseis.forecast.*;
        		
        		NewObj= ForecastObj(obj.Name);
        		Props2Copy = {'Version','EmptyGrid','Ready','TheGrid','NodeIDs',...
				'GridNodes','AxisVectors','GridSpacings','GridLimits','GridPrecision',...
				'MagBins','DepthBins','RegionPoly','BoundBox','TimeVector','TimeSpacing',...
				'RealTimeMode','Dimension','SelectedNodes','SelectedIDs',...
				'JumpValue','ForecastData','ExpectedValue','OverallDistro',...
				'ForecastType','DataMask','EqualMask','GenericMask','TheBag',...
				'ForecastReady'};
			
			for i=1:numel(Props2Copy)
				NewObj.(Props2Copy{i})=obj.(Props2Copy{i});
			end
				
        		
        	end
        	
        end  
end

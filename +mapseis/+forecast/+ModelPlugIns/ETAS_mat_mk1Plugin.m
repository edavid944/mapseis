classdef ETAS_mat_mk1Plugin < mapseis.forecast.ForecastPlugIn
	%based on the plugin interface, 
	%First version of a plugin for a Schoenberg based ETAS model build 
	%entirely in matlab
	
	%FINISHED BUT UNTESTED
	
	properties
		%Defined in Plugin superclass
		%  PluginName
        	%  Version %Has to be a STRING (consist of CodeVersion and Variant)
        	%  CodeVersion %Version of the code
        	%  Variant %allows to use multiple varitions of the same model in a project
        	%  ConfigID %Has to be re-set everytime the model is configurated
        	%  ModelInfo %The place for a description about the model 
        	%  
        	%  Datastore
        	%  Filterlist
        	%  ModelReady 
        	
        	LogData
        	AddonData
        	ForecastPeriod
        	LearningPeriod
        	BG_Scaler
        	
        	
        	ModelConfStruct
        	RegionConfStruct        	
        	ModRegReady
        	ForecastResult
        	BG_model
        	ModConfig
        	AddonToSend
        	CalcTimeScratch
        	GeoMode
        	ForGeo
        	ParaEstimated
        	DevMod
        end
    

       
        methods 
        	function obj = ETAS_mat_mk1Plugin(Variant)
              		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Variant=[];
        		end
        		
        		if isempty(Variant)
        			Variant=RawChar(randi(15,1,3)+1)
        		end
        		
        		obj.Variant=Variant;
        		
        		obj.PluginName='ETAS_matlab';
        		obj.CodeVersion='proto_v1';
        		obj.Version=[obj.CodeVersion,'_',obj.Variant];
        		obj.ModelInfo=[	'This is the first version of a plugin for ',...
        				'ETAS '];
        				
        		%Init config key
        		obj.setConfigID;
        	
        		obj.Datastore=[];
        		obj.Filterlist=[];
        		
        		obj.LogData=[];
        		obj.AddonData=[];
        		obj.BG_Scaler=1;
        		
        		obj.ModelConfStruct=[];
        		obj.RegionConfStruct=[];   
        		
        		obj.CPUTimeData=[];
        		
        		obj.ForecastPeriod=[];
        		obj.LearningPeriod=[];
       
        		
        		obj.ModRegReady=[false true false];
        		obj.ModelReady=false;
        		obj.ForecastResult=[];
        		obj.BG_model=[];
        		obj.ModConfig=[];
        		obj.AddonToSend=[];
        		obj.CalcTimeScratch=[];
        		
        		%converts the data into km (slower but more accurate)
        		%the results will be in lon and lat as usual
        		obj.GeoMode=false;
        		obj.ForGeo=[];
        		obj.ParaEstimated=false;
        		
        		%developer mode (will add all output to addon)
        		obj.DevMod=false;
        	end
        	   	
        	       	
        	function ConfigureModel(obj,ConfigData)
        		%allows to configurate the model
        	
        		obj.ModelConfStruct=ConfigData;
        		
        			      			
        	        		
        		%Not much parameters needed here, the models sets everything
        		%itself, but it needs an additional learning and testing period
        		%so parameters needed
        		%StartRetroLearning: start add. learning period string of following 
        		%		     format: yyyy-mm-dd hh:mm:ss	
        		%EndRetroLearning: End of the add. learning period
        		%StartRetroTesting: Start of the additional testing period
        		%EndRetroTesting: End of the additional testing period
        		%FixedTime: defines if the additional periods are fixed in
        		%	    time (true) or not (false). If false all times
        		%	    will be shifted in a similar way the forecast
        		%	    time was shifted.
        		%All times have to be given in the mentioned format, and all
        		%times should be before the forecast time (of course)
        		
        		obj.setConfigID;
        		
        		
			%Check if all complete
        		obj.ModRegReady(1)=true;
        		obj.ModelReady=all(obj.ModRegReady);
        		obj.ParaEstimated=false;
        	end	
        	
        	
        	function ConfigureFramework(obj,FrameConfig)
			%Similar to ConfigureModel, but consist only of framework based
			%variable which are defined by the framework. It consist at the
			%moment out of two fields:
			%TemporaryFolder: this is path of the temporary folder which may
			%		  be used to temporary store the files needed for
			%		  a forecast. 
			%ModelConfigFile: In a way a optional field, but it has to be
			%		  present even if it is ignored and left empty.
			%		  In this field a path to a config file of the 
			%		  model can be stored. The idea is not to store
			%		  configuration for the forecast in this file but
			%		  things actually needed to start, like paths to 
			%		  the executable files or system dependent config-
			%		  urations which have to be set only during 
			%		  installation of the model.
			import mapseis.util.PathMaker;
			
			%Nothing has to be done, it is matlab only
			obj.ModRegReady(2)=true;
        		obj.ModelReady=all(obj.ModRegReady);
			
        		
        	end
        	
        	
        	function ConfigureTestRegion(obj,RegionConfig)
			%This  method should allow to set the TestRegion config and grid
			%The variable RegionConfig will be structure with following 
			%entries:
			
			%RegionPoly:		Polygon surrounding the region
			%RegionBoundingBox:	Box around the Polygon
			%LonSpacing:		Longitude spacing or spacial spacing
			%			in case only on spacing is allowed
			%LatSpacing:		Latitude spacing, if two spacings 
			%			are allowed
			%GridPrecision:		The value to which the grid was rounded to.
			%MinMagnitude:		Minimum magnitude which should be 
			%			forecasted
			%MaxMagnitude:		The maximal magnitude which should be
			%			forecasted
			%MagnitudeBin:		Size of the Magnitude Bin often this 
			%			will be 0.1, but can be different. This
			%			will be empty if no magnitude binning is
			%			used
			%MinDepth:		Minimal depth range which should be 
			%			forecasted, in most cases this will be 0
			%MaxDepth:		Maximal depth to forecast
			%DepthBin:		In most cases this will empty as 3D 
			%			regions are not common, but in case a
			%			3D model is used this gives depth bin 
			%			size
			%TimeInterval:		Length of the forcast in days or year 
			%			(can be specified in getFeatures)
			%RelmGrid:		A raw grid similar to the one used in 
			%			Relm Forecasts based on the values 
			%			specified. 
			%			(Relmformat: [LonL LonH LatL LatH ...
			%			DepthL DepthH MagL MagH (ID) Mask]
			%Nodes			%Just the spacial nodes of the grid as 
			%			for instance by TripleS, in contrary to
			%			the RELM grid this descriptes the centre
			%			of the grid cell
			%MagVector:		Consisting of the Magnitudes which have
			%			to be forecasted (the points are the in 
			%			middle of the magnitude bin)
			%DepthVector:		Consisting of the Depths which have to 
			%			forecasted (only 3D) (the points are the  
			%			in middle of the depth bin)
			
			%Simple Only some are really needed, but everything stored
			%anyway
			obj.RegionConfStruct=RegionConfig;
			
			obj.setConfigID;
			
			%Check if all complete
        		obj.ModRegReady(3)=true;
        		obj.ModelReady=all(obj.ModRegReady);
			obj.ParaEstimated=false;	
	       	end
        	
        	
        	function ConfigData = getModelConfig(obj)
        		%Either gives out the inputed structure or returns some default values
        		
        		if ~isempty(obj.ModelConfStruct)
        			OutConf = obj.ModelConfStruct;
        			       			
        			ConfigData = OutConf;
        			
        		else
        			%default values : there will be some changes here later
        			ConfigData = struct(	'mu', 5.250,...
        						'b', 1,...
        						'K',0.200,...
        						'c',0.025,...
        						'p',1.500,...
        						'a',0.700,...
        						'd',0.025,...
        						'q',1.500,...
        						'MinMag',0,...
        						'MinMainMag',3,...
        						'Est_Param',true,...
        						'ResEst_Param',true,...
        						'ext_BGmodel',false,...
        						'ext_BG_Path','none',...
        						'int_BG_mode','AnySmooth',...
        						'BG_SmoothParam',[[0.1, 10, 0]],...
        						'BG_Prob',0.6,...        		
        						'Nr_Simulation',10000,...
        						'Res_SmoothMode','ETAS3D',...
        						'Res_SmoothParam',[[0.1, 10, 0]],...
        						'ParallelMode',false);
        						%Parallel mode will be kick out later, it 
        						%will be taken care of in the Calculator
        						
        		end
        		
        	end
        	
        	
        	function FrameConfig = getFrameConfig(obj)
			%Should return the two mentioned fields. If nothing is returned
			%or empty fields the internal default of the framework will be
			%used. This will work as well but is not user friendly. 
			
			FrameConfig=[];
			
			
		end	
			
		
		
        	
        	function RegionConfig = getTestRegionConfig(obj)
			%It returns the internal region config structure
			RegionConfig = obj.RegionConfStruct;
		
        	end
        	
        	
        	function PossibleParameters = getParameterSetting(obj)
			%The method should return a description of the parameters needed
			%for the model and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be inputed by ConfigureModel
			%Each entry in structure should be another structure with following
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			be the an integer giving the position of the 
			%			selected string in the cell array. 
			%	UserDataSelect: This can be used instead of 'Items' in the 'list'
			%			entry type. It it allows to select previous, in
			%			the Datastore catalog stored, calculations like 
			%			for instance b-value, stress calculations and so 
			%			on. The GUI will in this case automatically generate
			%			the Items list, only thing needed is the field name 
			%			under which the data is stored in the datastore 
			%			userdata area (e.g. 'new-bval-calc' for b-value maps)
			%			the command Datastore.getUserDataKeys can be used
			%			to get the currently saved variables. The result
			%			sended back will in this case be the name of the 
			%			calculation (store in in the first column of the 
			%			result cell array). 
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			%UNFINISHED
			
			TimeEntry = struct(	'Type', 'edit',...
						'Format', 'string',...
						'ValLimit',[[1 1]],...
						'Name', 'Generic Time Entry',...
						'DefaultValue','yyyy-mm-dd hh:mm:ss');
			StartRetroLearn=TimeEntry;
			StartRetroLearn.Name='Start retro. learning period';

			StopRetroLearn=TimeEntry;
			StopRetroLearn.Name='End retro. learning period';
			
			StartRetroTest=TimeEntry;
			StartRetroTest.Name='Start retro. testing period';
			
			StopRetroTest=TimeEntry;
			StopRetroTest.Name='End retro. testing period';
			
			Fixer =	struct(	'Type', 'check',...
					'Name', 'Used Fixed Learning Time',...
					'DefaultValue',false);
					
			PossibleParameters = struct(	'StartRetroLearning', StartRetroLearn,...
        						'EndRetroLearning' , StopRetroLearn,...
        			      			'StartRetroTesting', StartRetroTest,...
        			      			'EndRetroTesting', StopRetroTest,...
        			      			'FixedTime',Fixer);
			
        	
        	end
        	
        	
        	function setInputData(obj,Datastore,Filterlist,StartDate)
			%This method is used by the project calculator to send the input
			%catalog data to the model. Sended will be a Datastore catalog 
			%object and a Filterlist object or boolean vector giving the 
			%selection of the earthquakes (islogical can be used to separate
			%between the two cases).
			%Everything what has to be done to bring the input catalog into 
			%the right format has to be done here. Like conversion into a 
			%file format readable by the model.
			
			import mapseis.projector.getDateNum;
			
			%check if it is the first run:
			InBench=tic;
			
			
			obj.Datastore = Datastore;
			obj.Filterlist = Filterlist;
			obj.ForecastPeriod = [StartDate, StartDate+obj.RegionConfStruct.TimeInterval]
			        			
             	
						
			
			
			
			FilterExist=false;
        		if isempty(obj.Filterlist)
				selected=true(obj.Datastore.getRowCount,1);	
			else
					
				if ~islogical(obj.Filterlist)
					obj.Filterlist.changeData(obj.Datastore);
					obj.Filterlist.updateNoEvent;
					selected=obj.Filterlist.getSelected;
					obj.Filterlist.PackIt;
					FilterExist=true;
				else
					selected=obj.Filterlist;
				end
			end
			
			if FilterExist
				TimeFilt=obj.Filterlist.getByName('Time');
				WhatPer = TimeFilt.getRange;
				
				TimePer=WhatPer{2};
				disp(WhatPer)
				
				if isinf(TimePer(1))
					%use start of catalog
					TimePer(1)=min(getDateNum(obj.Datastore,selected));
					
				end
				
				if isinf(TimePer(2))
					%use end of catalog
					TimePer(2)=max(getDateNum(obj.Datastore,selected));
				end
        		else
        			TimePer(1)=min(getDateNum(obj.Datastore,selected));
        			TimePer(2)=max(getDateNum(obj.Datastore,selected));
        		end
        		
        		obj.LearningPeriod=TimePer;
        		
        		%to be sure
        		obj.ModelReady=all(obj.ModRegReady);
        		obj.CalcTimeScratch(2)=toc(InBench);
        		
        	end
        	
        	
        	function UsedData = getInputData(obj,OnlyFilter)
			%This method will be used by the project calculator for archiving
			%the used data. The returned UsedData can have different format 
			%but suggested is zmap or shortcat based catalog or a datastore 
			%catalog (cutted!). If OnlyFilter is set to true, the UsedData 
			%variable should be a boolean vector giving the selected earthquakes
			
			%I go for the ShortCat this time, but it could easily be changed
			%at the moment the  function is more of diagnostic nature anyway
			import mapseis.projector.*;
			
			
			if isempty(obj.Datastore)|isempty(obj.Filterlist)
				UsedData=[];
			else
				if OnlyFilter
					if ~islogical(obj.Filterlist)
						obj.Filterlist.changeData(obj.Datastore);
						obj.Filterlist.updateNoEvent;
						UsedData=obj.Filterlist.getSelected;
						obj.Filterlist.PackIt;
					else
						UsedData=obj.Filterlist;
					end
				
				else
					if ~islogical(obj.Filterlist)
						obj.Filterlist.changeData(obj.Datastore);
						obj.Filterlist.updateNoEvent;
						selected=obj.Filterlist.getSelected;
						obj.Filterlist.PackIt;
					else
						selected=obj.Filterlist;
					end
        				
        				[UsedData temp]=getShortCatalog(obj.Datastore,selected);
				
				end
			end
        	end
        	
        	
        	
        	function FeatureList = getFeatures(obj)
			%This is used by the project builder to determine which parameters
			%are available for the model. It influences mostly the region 
			%setting, the way data is exchanged and some additional menus
			%The Output should be a structure with specified fieldnames with 
			%mostly boolean values.
			%Following keywords are available:
			%	MagDist:	Should be normally true, means forecasts
			%			for different magnitude bins are available
			%			-> Magnitude bins
			%	DepthDist:	If set to true the model allows forecasts
			%			for different depth bins
			%			-> 3D model
			%	RectGrid:	If true the model should allow different
			%			grid spacing for Longitude and Latitude
			%	TimeYear:	If false the project builder will use
			%			days for time interval value and if true
			%			years will be used.
			%	Parallel:	If true the this GUI allows parallization
			%			and the toggle will be sended by builder
			%	HostMode:	Only for parallel mode, if set to true
			%			builder is allows to use this object in
			%			a parfor loop or similar. If false the
			%			model deals itself the parallization
			%	NoFiles:	If set to true the model does not produce
			%			any files and thus they cannot be archived
			%			The project builder will in this case not 
			%			allow the option to archive files 
			%	LogData:	If true Logdata is available not only in 
			%			file form but also directly imported as
			%			string or structure or whatever.
			%	ForecastType:	This is not boolean value but string
			%			It describes what distribution is 
			%			returning. It can be 'Possion', 'NegBin'
			%			'Custom' and 'Selectable'.
			%			'Selectable' means that the model supports
			%			more than on distribution. 
			
			
			%The project builder does also have a standard feature set in 
			%case some parts of the feature list are not defined.
			%The default is:
			%	MagDist=true;
			%	DepthHist=false;
			%	RectGrid=false;
			%	TimeYear=false;
			%	Parallel=true;
			%	HostMode=false;
			%	NoFiles=false;
			%	LogData=true;
			%	ForecastType='Poisson';
			%Maybe more later
			
			%IMPORTANT: if ForecastType is 'Selectable', a field with
			%the name "ForecastType" has to be added to the Model Configuration
			%describing the actual ForecastType, also a descripting for the 
			%GUI has to be added allowing to change the parameter
			
			%TODO: May have to add stuff to this list
			
			%not so sure about the parallel processing, but I say no for the 
			%moment
			
			FeatureList = struct(	'MagDist',true,...
						'DepthDist',false,...
						'RectGrid',false,...
						'TimeYear',false,...
						'Parallel',true,...
						'HostMode',false,...
						'NoFiles',true,...
						'LogData',false,...
						'AddonData',true,...
						'ForecastType','Poisson');
						
						
        	end
			
			
        	
        	
        	
        	function prepareCalc(obj)
			%This method is called the project builder, and should do everything
			%what is needed to run the forecast later. Often this method is 
			%totally unnecessary as everything can be done be before or after 
			%call of this method. But in some cases it might be a good idea 
			%to separate some cpu heavy preparation from the configuration.
			%If not needed the function can defined as a empty function.
			%IMPORTANT: No catalog preparation should be done here, as the
			%catalog data is sended by the project calculator. This function
			%is the last function before a actual calculation run (called in
			%the startCalc method of the calculator.
			
			import mapseis.ETAS.*;
			import mapseis.projector.*;
			
			%THIS WILL disappear with the stable version
			if  isempty(obj.DevMod)
				obj.DevMod=false;
			end
			
			
			bencher=tic;
			%switch on parallel if needed
			if obj.ModelConfStruct.ParallelMode
				try
					matlabpool open;
				catch
					disp('Parallel Processing could not be switched on')
				end	
			end
			
			
			%check if geoMode is active and the structure for it exists
        		if obj.GeoMode
        			if isempty(obj.ForGeo)
        				obj.generateGeoData;
        			end
			
        		end
			
			%decide which BG is needed
			if obj.ModelConfStruct.ext_BGmodel
				obj.BG_model=load(obj.ModelConfStruct.ext_BG_Path);
				GenMode='normal';
				
				if obj.GeoMode
					obj.convertBG2km(obj.BG_model);
				end
				
			else	
							
				%generate catalog
				if ~islogical(obj.Filterlist)
					obj.Filterlist.changeData(obj.Datastore);
					obj.Filterlist.updateNoEvent;
					selected=obj.Filterlist.getSelected;
					obj.Filterlist.PackIt;
				else
					selected=obj.Filterlist;
				end
				
				ShortCat = getShortCatalog(obj.Datastore,selected,false);
				
				%kick out everything below minMag
				lowMag=ShortCat(:,5)<obj.ModelConfStruct.MinMag;
				ShortCat=ShortCat(~lowMag,:);
				
				BGProb=obj.ModelConfStruct.BG_Prob;
				TimeLeng=obj.LearningPeriod(2)-obj.LearningPeriod(1);
				RelmGrid=obj.RegionConfStruct.RelmGrid;
				NrBuddys=obj.ModelConfStruct.BG_SmoothParam(2);
				minWidth=obj.ModelConfStruct.BG_SmoothParam(1);
				bval=obj.ModelConfStruct.b;
				
				if obj.GeoMode
					%convert
					ShortCat=obj.convert2km(ShortCat);
					degRelm=RelmGrid;
					RelmGrid=obj.ForGeo.convGrid;
					minWidth(2)=minWidth(1);
					minWidth(1)=deg2km(minWidth(1));
				end
				
				
				disp('generate Background model')
				GenMode='normal';
				switch obj.ModelConfStruct.int_BG_mode
					case 'AnySmooth_norm'
						BGRates = calcBGRatesND_mk2b(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,true);
						GenMode='normalBG';
					case 'AnySmooth'
						BGRates = calcBGRatesND_mk2(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,true);
					case 'Smooth3D'
						BGRates = calcBGRatesND_mk2(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,false);
					case 'Smooth'
						BGRates = calcBGRates(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval);
				end	
					
				%multiply the rates with the forecast length
				BGRates(:,9)=BGRates(:,9)*obj.RegionConfStruct.TimeInterval;
				
				%obj.BG_model=RelmGrid;
				if obj.GeoMode
					obj.ForGeo.convBG=BGRates;
					degRelm(:,9)=BGRates(:,9);
					BGRates=degRelm;
					
				end
				
				obj.BG_model=BGRates;
				
			end	
			
			%obj.BG_model(:,9)=obj.BG_model(:,9)*obj.BG_Scaler;
			
			%decide which smoothing (may add some more options to it later)
			switch obj.ModelConfStruct.Res_SmoothMode
				case 'None'
					SmoothSwitch=false;
					SmoothPar=[];
					SmoothMode='none';
					
				case 'ETAS3D'
					SmoothSwitch=true;
					SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
					SmoothMode='ETAS3D';
				
				case 'ClassicETAS'
					SmoothSwitch=true;
					SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
					SmoothMode='ClassicETAS';
				
				case 'Gauss'					
					SmoothSwitch=true;
					SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
					SmoothMode='gauss';
					
				case 'Mean'
					SmoothSwitch=true;
					SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
					SmoothMode='mean';
				
			end
			
			if obj.GeoMode
				BGMod=obj.ForGeo.convBG;
				
				
				
				if numel(SmoothPar)==3&strcmp(SmoothMode,'ETAS3D')
					SmoothPar=[deg2km(SmoothPar(1)),SmoothPar(2),SmoothPar(3),SmoothPar(1)];
				else
					try
						SmoothPar(1)=deg2km(SmoothPar(1));
					end 	
				
				end
				
				
			else
				BGMod=obj.BG_model;
			end
			
			%generate basic config structure 
			obj.ModConfig=	struct(	'ReturnVal','Poisson',...
						'EstimatePara',obj.ModelConfStruct.Est_Param,...
						'EqGeneration',GenMode,...
						'BG_Distro',BGMod,...
						'GenTimeBefore',TimeLeng,...
						'LearnStartTime',obj.LearningPeriod(1),...
						'MinMag',obj.ModelConfStruct.MinMag,...
						'MinMainMag',obj.ModelConfStruct.MinMainMag,...
						'GenEqList',[[]],...
						'Gen1stGen',true,...
						'Nr_Simulation',obj.ModelConfStruct.Nr_Simulation,...
						'Smoothing',SmoothSwitch,...
						'SmoothMode',SmoothMode,...
						'SmoothParam',SmoothPar,...
						'SmoothConvMat',{{}});

			
						
			%generate basic structure for AddonData
			obj.AddonToSend= struct('NrBGHist',{{}},...
						'NrTrigHist',{{}},...
						'PercTrigHist',{{}},...
						'sumPercTrigHist',[[]]);			
						
			disp('Model Prepared')			
						
			obj.CalcTimeScratch(1)=toc(bencher);
        	end
        	
        	
        	
        	function ErrorCode = calcForecast(obj)
			%Names says it. If this method is called the calculation of the 
			%forecast and according the configuration and catalog data should
			%started and the result should be saved in this object as well as
			%the path to the result file in case any is existing.
			%The ErrorCode can be used to signalize errors, it is just an 
			%integer. 0 means everthing worked fine, 1 means model is not
			%configurated properly, 2 means catalog data is missing, 3 means 
			%something with a external function did not work out and -1
			%means a generic not specified error. Every other value can 
			%used to specify special model specific errors. It is of course
			%sensible to explain the error code somewhere.
			%The error can be specified in the function getErrorList(obj), but 
			%it is as often optional, a generic function is integrated anyway.
			%Currently the function is not directly used, but can change very
			%soon
			
			import mapseis.ETAS.*;
			import mapseis.projector.*;
			
			ErrorCode=-1;
			
			%check if everything is set
			if ~obj.ModelReady
				%here it really should be an error, because
				%that can be changed easily 
				error('Model is not configurated')
			end
			
			%update Config
			%-------------
			
			%generate catalog
			if ~islogical(obj.Filterlist)
				obj.Filterlist.changeData(obj.Datastore);
				obj.Filterlist.updateNoEvent;
				selected=obj.Filterlist.getSelected;
				obj.Filterlist.PackIt;
			else
				selected=obj.Filterlist;
			end
				
			ShortCat = getShortCatalog(obj.Datastore,selected,false);
			
			currentTime=obj.ForecastPeriod(1);
			TimeLength=obj.RegionConfStruct.TimeInterval;
			RelmGrid=obj.RegionConfStruct.RelmGrid;
			degRelm=RelmGrid;
			
			if obj.GeoMode
				%convert
				ShortCat=obj.convert2km(ShortCat);
				RelmGrid=obj.ForGeo.convGrid;
			end
			
			ThetaStart=struct(	'mu',obj.ModelConfStruct.mu,...
						'b',obj.ModelConfStruct.b,...
						'K',obj.ModelConfStruct.K,...
						'c',obj.ModelConfStruct.c,...
						'p',obj.ModelConfStruct.p,...
						'a',obj.ModelConfStruct.a,...
						'd',obj.ModelConfStruct.d,...
						'q',obj.ModelConfStruct.q);
			obj.ParaEstimated
			obj.ModConfig.EstimatePara = (obj.ModelConfStruct.Est_Param&~obj.ParaEstimated)|obj.ModelConfStruct.ResEst_Param;	
						
			
			
			%run calculation
			disp('start Calc step')
			%[Forecast AddonData] = EtasForecaster_test2(ShortCat,currentTime,TimeLength,RelmGrid,obj.ModConfig,ThetaStart);
			[Forecast AddonData] = EtasForecaster_mk2(ShortCat,currentTime,TimeLength,RelmGrid,obj.ModConfig,ThetaStart);
			
			%write estimated parameters into Config
			obj.ModelConfStruct.mu=AddonData.ThetaUsed.mu;
			obj.ModelConfStruct.b=AddonData.ThetaUsed.b;
			obj.ModelConfStruct.K=AddonData.ThetaUsed.K;
			obj.ModelConfStruct.c=AddonData.ThetaUsed.c;
			obj.ModelConfStruct.p=AddonData.ThetaUsed.p;
			obj.ModelConfStruct.a=AddonData.ThetaUsed.a;
			obj.ModelConfStruct.d=AddonData.ThetaUsed.d;
			obj.ModelConfStruct.q=AddonData.ThetaUsed.q;
						
			%store results
			obj.ForecastResult=degRelm;
			obj.ForecastResult(:,9)=Forecast;
			obj.LogData=AddonData.CalcTimes;
			obj.AddonData=AddonData;
			
			
			
			obj.ModConfig.SmoothConvMat=AddonData.SmoothConvMat;
			
			obj.generateAddonData;
			obj.generateBenchmark;
			obj.ParaEstimated=true;
			
			%finished
			ErrorCode=0;
			disp('Finished Calc Step')
			
        	end
        	
        	
        	function [ForeCast, DataMask] = getForecast(obj,WithGrid)
			%This should return the calculated forecast. The Format which should
			%be returned is depended on the ForecastType and on the input 
			%parameter WithGrid. For 'Poisson' and 'NegBin' the output should be
			%vector (Poisson) and a "twin" vector (NegBin) and if WithGrid is
			%set to true a full Relm Grid forecast should be outputed. In the
			%'Custom' mode the output will always be a cell, without WithGrid
			%the output should be a cell vector containing the single grid cells
			%discrete distribution, else it should be a cell with two elements
			%first one containing the Relmgrid, second one containing the 
			%forecast like mentioned before.
			%The DataMask is the last column in a typical Relm based forecast
			%and marks if a cell has been used/defined (1) or not (0). It can 
			%be left empty, in this case it is assumed that all nodes are used.
			%In case of a full WithGrid output it DataMask will be ignored and
			%the grid will be used instead
			
			%check if calculated
			if isempty(obj.ForecastResult)
        			error('Nothing Calculated');
        		end
        		
        		RelmGrid=obj.ForecastResult;
        		
			%decide what has to be sended
			if WithGrid
				ForeCast=RelmGrid;
				DataMask=RelmGrid(:,10);
			else
				ForeCast=RelmGrid(:,9);
				DataMask=RelmGrid(:,10);
			end
			
			
		end
         	
         	
        	
        	function FilePath = getForecastFile(obj)
			%This method will only be called if NoFiles is false and if the 
			%option to archive the forecast files is switched on the project
			%object. 
			%It should return the path to the forecast file as a string. The 
			%project calculater will then copy the file to a specific folder
			%save the link to this file. If there is more then one 
			%result file existing a Cell array with multiple paths (strings)
			%can be used instead of a normal single path
			
        		
        		FilePath=[];
        		warning('Model does not produce output files');
        		
        		
        		
        		
			
		end
		
		

		function LogData = getCalcLog(obj)
			%This will be called if saving logs is activated and LogData is 
			%set to true in the FeatureList. The method should return a log 
			%of the calculation in a matlab common format and it should NOT
			%be a file link, because the log file will not be copied.
			%the easiest way to create the LogData is to just read the file 
			%into a string and return the string.
			
			if obj.DevMod
				LogData = {obj.AddonData,obj.LogData};
			else
				LogData = obj.LogData;
			end
			
			
		end
		
		
		function LogFile = getCalcLogFile(obj)
			%The project calculator will only use this method if NoFiles is
			%set to false in the FeatureList and if the option to store log
			%files is activated in the project. This method should return a
			%file path(s) to the log file(s), the project builder will copy 
			%the files and store the new path. If there is more then one 
			%logfiles existing a Cell array with multiple paths (strings)
			%can be used instead of a normal single path
			
			%QUESTION: does tripleS have any log data?
			%--> No log files
			
        		LogFile=[];
        		warning('Model does not produce output files');
        		
			
			
			
		end
        	
		
		function AddonData = getAddonData(obj)
			%This will be called if saving AddonData is activated and AddonData is 
			%set to true in the FeatureList. The method can return anything over 
			%this function, but the model has also to keep track itself in case
			%it is timevariing data
			
			AddonData=obj.AddonToSend;
			
			
		end
		
		
        	function ResetLastCalc(obj)
			%This method should revert the plugin to the state before the 
			%calculation of the forecast, so that new data can be sent and
			%a new forecast can be calculated. This may involve things like 
			%empty the catalog storage and and deleting some input and output
			%files. But it might as well be just an empty function if no clean
			%up is needed. 
			%IMPORTANT: this method should revert the plugin to the state AFTER
			%prepareCalc was called.
			
			%means removing data and files, but not reset the OrgStartTime
			
			%reset some of the properties
			obj.ForecastPeriod=[];
        		obj.LearningPeriod=[];
        		obj.ForecastResult=[];
			obj.Datastore=[];
        		obj.Filterlist=[];
			obj.LogData=[];
			obj.AddonToSend=[];
        	end
        	
        	
        	
        	function ResetFullCalc(obj)
			%Similar to ResetLastCalc but this one should revert the plugin to
			%state BEFORE prepareCalc. The function can be equal to ResetLastCalc
			%but it should not cause an error because double execution of a command
			%(e.g. delete of a file)
			
			%reset everything apart config, so first call ResetLastCalc
			
			obj.ResetLastCalc;
			obj.BG_model=[];
			obj.ModConfig=[];
			
			%normally written for every calc step, but because
			%it is meant for diagnostic, it should be delete after
			%a calc run and not after every calculation.
			obj.AddonData=[];
			obj.CalcTimeScratch=[];
			
			if obj.ModelConfStruct.ParallelMode
				try
					matlabpool close;
				catch
					disp('Parallel Processing was not running')
				end	
			end
			
			obj.ParaEstimated=false;
		
			
			
		end
		
		
		%CUSTOM FUNCTIONS
		function generateAddonData(obj)
			%generate new AddonData
			if isempty(obj.AddonToSend)
				obj.AddonToSend.NrBGHist={};
				obj.AddonToSend.NrTrigHist={};
				obj.AddonToSend.PercTrigHist={};
				obj.AddonToSend.sumPercTrigHist=[];
				
				obj.AddonToSend.NrBGHist{end+1,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.Num_BG_Event,25);
				obj.AddonToSend.NrBGHist{end,2}=[n;xout];
				
				obj.AddonToSend.NrTrigHist{end+1,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.Num_Trig_Event,25);
				obj.AddonToSend.NrTrigHist{end,2}=[n;xout];
				
				obj.AddonToSend.PercTrigHist{end+1,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.PercTrigEvent,25);
				obj.AddonToSend.PercTrigHist{end,2}=[n;xout];
				
				obj.AddonToSend.sumPercTrigHist(end+1,1)=obj.ForecastPeriod(1);
				obj.AddonToSend.sumPercTrigHist(end,2)=obj.AddonData.sumPercTrigEvent;
				return
				
			end
			
			
			if obj.AddonToSend.NrBGHist{end,1}~=obj.ForecastPeriod(1)
				obj.AddonToSend.NrBGHist{end+1,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.Num_BG_Event,25);
				obj.AddonToSend.NrBGHist{end,2}=[n;xout];
				
			else
				obj.AddonToSend.NrBGHist{end,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.Num_BG_Event,25);
				obj.AddonToSend.NrBGHist{end,2}=[n;xout];
			end
			
			
			
			if obj.AddonToSend.NrTrigHist{end,1}~=obj.ForecastPeriod(1)
				obj.AddonToSend.NrTrigHist{end+1,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.Num_Trig_Event,25);
				obj.AddonToSend.NrTrigHist{end,2}=[n;xout];
				
			else
				obj.AddonToSend.NrTrigHist{end,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.Num_Trig_Event,25);
				obj.AddonToSend.NrTrigHist{end,2}=[n;xout];
			end
			
			
			
			if obj.AddonToSend.PercTrigHist{end,1}~=obj.ForecastPeriod(1)
				obj.AddonToSend.PercTrigHist{end+1,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.PercTrigEvent,25);
				obj.AddonToSend.PercTrigHist{end,2}=[n;xout];
				
			else
				obj.AddonToSend.PercTrigHist{end,1}=obj.ForecastPeriod(1);
				[n,xout] = hist(obj.AddonData.PercTrigEvent,25);
				obj.AddonToSend.PercTrigHist{end,2}=[n;xout];
			end
			
			
			
			
			if obj.AddonToSend.sumPercTrigHist(end,1)~=obj.ForecastPeriod(1)
				obj.AddonToSend.sumPercTrigHist(end+1,1)=obj.ForecastPeriod(1);
				obj.AddonToSend.sumPercTrigHist(end,2)=obj.AddonData.sumPercTrigEvent;
				
			else
				obj.AddonToSend.sumPercTrigHist(end,1)=obj.ForecastPeriod(1);
				obj.AddonToSend.sumPercTrigHist(end,2)=obj.AddonData.sumPercTrigEvent;
				
			end
						
		end
		
		
		
		function generateBenchmark(obj)
			%this generates the calculation times "needed" by the calculator
			%plus some additional times
			obj.CPUTimeData = struct(	'InputDataTime',obj.CalcTimeScratch(2),...
							'CalcTime',obj.AddonData.CalcTimes.FullTime,...
							'ImportTime',0,...
							'PrepareTime',obj.CalcTimeScratch(1),...
							'AddTimeInfo',obj.AddonData.CalcTimes);
			
			
		end
		
		function generateGeoData(obj)
			%generate the reference points and converts the grid
			
			import mapseis.ETAS.*;
			import mapseis.projector.*;
			
			%build catalog (for getting reference point)
			if ~islogical(obj.Filterlist)
				obj.Filterlist.changeData(obj.Datastore);
				obj.Filterlist.updateNoEvent;
				selected=obj.Filterlist.getSelected;
				obj.Filterlist.PackIt;
			else
				selected=obj.Filterlist;
			end
				
			ShortCat = getShortCatalog(obj.Datastore,selected,false);
			orgRelmGrid = obj.RegionConfStruct.RelmGrid;
			leng=size(orgRelmGrid(:,1));
			
			%find reverence: (assuming the first catalog is relevant
			minCatLon=min(ShortCat(:,1));
			minCatLat=min(ShortCat(:,2));
			minGridLon=min(orgRelmGrid(:,1));
			minGridLat=min(orgRelmGrid(:,3));
			
			
			RefPoint=[sign(minGridLon)*(abs(floor(min([minCatLon;minGridLon])-5))),...
				sign(minGridLat)*(abs(floor(min([minCatLat;minGridLat])-5)))];
			
			RelmGrid(:,1)=sign(orgRelmGrid(:,1)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,1)));	
			RelmGrid(:,2)=sign(orgRelmGrid(:,2)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,2)));	
			RelmGrid(:,3)=sign(orgRelmGrid(:,3)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,3),RefPoint(1).*ones(leng)));	
			RelmGrid(:,4)=sign(orgRelmGrid(:,4)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,4),RefPoint(1).*ones(leng)));
			RelmGrid(:,5:10)=orgRelmGrid(:,5:10);
			
			obj.ForGeo=struct(	'RefPoint',RefPoint,...
						'convGrid',RelmGrid);
		end
		
		function convertBG2km(obj,RelmGrid)
			%converts a BG grid with the reference point and saves it
			%into the ForGeo structure
			%at the moment the RefPoint will not be shifted 
			RefPoint=obj.ForGeo.RefPoint;
			orgRelmGrid=RelmGrid;
			leng=size(orgRelmGrid(:,1));
			
			RelmGrid(:,1)=sign(orgRelmGrid(:,1)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,1)));	
			RelmGrid(:,2)=sign(orgRelmGrid(:,2)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,2)));	
			RelmGrid(:,3)=sign(orgRelmGrid(:,3)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,3),RefPoint(1).*ones(leng)));	
			RelmGrid(:,4)=sign(orgRelmGrid(:,4)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,4),RefPoint(1).*ones(leng)));
			RelmGrid(:,5:10)=orgRelmGrid(:,5:10);
			
			obj.ForGeo.convBG=RelmGrid;
		end
		
		
		function copyCat=convert2km(obj,ShortCat)
			%converts a catalog in to distance-km from reference point
			
			
			RefPoint=obj.ForGeo.RefPoint;
			copyCat=ShortCat;
			leng=size(ShortCat(:,1));
			
			copyCat(:,1)=sign(ShortCat(:,1)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),ShortCat(:,1)));	
			copyCat(:,2)=sign(ShortCat(:,2)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),ShortCat(:,2),RefPoint(1).*ones(leng)));
			
			
		end
		
		function TripTime = convertTime(obj,inTime)
        		%converts a datenum into a date strings used by the 
        		%TripleS model and vice versa. It would not need to be 
        		%internal method the TripleS plugin, but it is not needed 
        		%outside, and like this it is easier to call inside the object.
        		%inTime: datenum format e.g 7.3516e+05
        		%TripTime: time string of the format: '2012/10/16 16:30:58'
        		
        		%actually it is only on line, but still handy
        		if isnumeric(inTime)
        			TripTime = datestr(inTime,'yyyy/mm/dd HH:MM:SS');
        		else
        			TripTime = datenum(inTime,'yyyy/mm/dd HH:MM:SS');
        		end
        		
		end
        	
        end
        
        
       
        
        
end        

classdef TripleSPlugin < mapseis.forecast.ForecastPlugIn
	%based on the plugin interface, the first plugin for the TripleS java-base
	%forecast model
	
	%FINISHED BUT UNTESTED
	
	properties
		%Defined in Plugin superclass
		%  PluginName
        	%  Version %Has to be a STRING (consist of CodeVersion and Variant)
        	%  CodeVersion %Version of the code
        	%  Variant %allows to use multiple varitions of the same model in a project
        	%  ConfigID %Has to be re-set everytime the model is configurated
        	%  ModelInfo %The place for a description about the model 
        	%  
        	%  Datastore
        	%  Filterlist
        	%  ModelReady 
        	
        	LogData
        	
        	ForecastPeriod
        	LearningPeriod
        	RetroSmoothPeriod
		RetroTargetPeriod
        	
        	
        	ModelConfStruct
        	RegionConfStruct        	
        	ModRegReady
        	OrgStartTime
        	
        	       	
        	TempFolder
        	ConfigFile
        	ExternalPath
        	ExtModel
        	CatalogPath
        	GridPath
        	AVFSPath
        	ResultPath
        	JavaPath
        	
        	PrepareFiles
        	CalcFiles
        end
    

       
        methods 
        	function obj = TripleSPlugin(Variant)
              		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Variant=[];
        		end
        		
        		if isempty(Variant)
        			Variant=RawChar(randi(15,1,3)+1)
        		end
        		
        		obj.Variant=Variant;
        		
        		obj.PluginName='TripleS_java';
        		obj.CodeVersion='v1';
        		obj.Version=[obj.CodeVersion,'_',obj.Variant];
        		obj.ModelInfo=[	'This is the first version of a plugin for ',...
        				'TripleS java based model by Jeremy Zechar'];
        				
        		%Init config key
        		obj.setConfigID;
        	
        		obj.Datastore=[];
        		obj.Filterlist=[];
        		
        		obj.LogData=[];
        		
        		obj.ModelConfStruct=[];
        		obj.RegionConfStruct=[];   
        		
        		obj.ForecastPeriod=[];
        		obj.LearningPeriod=[];
        		obj.RetroSmoothPeriod=[];
			obj.RetroTargetPeriod=[];
        		
        		obj.ModRegReady=[false false false];
        		obj.ModelReady=false;
        		obj.OrgStartTime=[];
        		
        		obj.CPUTimeData=[];
        		
        		%paths	
        		obj.TempFolder=[];
        		obj.ConfigFile=['.',fs,'Forecast_Models',fs,'TripleS',fs,'FrameConfig.conf'];
        		obj.ExternalPath=[];
        		obj.ExtModel=[];
        		obj.CatalogPath=[];
        		obj.GridPath=[];
        		obj.AVFSPath=[];
        		obj.ResultPath=[];
        		
        		%new feature instead of just use java allow to set a path to a custom version of it
        		obj.JavaPath = 'java';
        		
        		%makes deleting the files a bit faster
        		obj.PrepareFiles=[];
        		obj.CalcFiles=[];
        		
        	end
        	   	
        	       	
        	function ConfigureModel(obj,ConfigData)
        		%allows to configurate the model
        		
        		%convert datestrings
        		ConfigData.StartRetroLearning = obj.convertTime(ConfigData.StartRetroLearning);
        		ConfigData.EndRetroLearning = obj.convertTime(ConfigData.EndRetroLearning);
        		ConfigData.StartRetroTesting = obj.convertTime(ConfigData.StartRetroTesting);
        		ConfigData.EndRetroTesting = obj.convertTime(ConfigData.EndRetroTesting);
        		
        		obj.ModelConfStruct=ConfigData;
        		
        			      			
        	        		
        		%Not much parameters needed here, the models sets everything
        		%itself, but it needs an additional learning and testing period
        		%so parameters needed
        		%StartRetroLearning: start add. learning period string of following 
        		%		     format: yyyy-mm-dd hh:mm:ss	
        		%EndRetroLearning: End of the add. learning period
        		%StartRetroTesting: Start of the additional testing period
        		%EndRetroTesting: End of the additional testing period
        		%FixedTime: defines if the additional periods are fixed in
        		%	    time (true) or not (false). If false all times
        		%	    will be shifted in a similar way the forecast
        		%	    time was shifted.
        		%All times have to be given in the mentioned format, and all
        		%times should be before the forecast time (of course)
        		
        		obj.setConfigID;
        		
        		
			%Check if all complete
        		obj.ModRegReady(1)=true;
        		obj.ModelReady=all(obj.ModRegReady);
        		
        	end	
        	
        	
        	function ConfigureFramework(obj,FrameConfig)
			%Similar to ConfigureModel, but consist only of framework based
			%variable which are defined by the framework. It consist at the
			%moment out of two fields:
			%TemporaryFolder: this is path of the temporary folder which may
			%		  be used to temporary store the files needed for
			%		  a forecast. 
			%ModelConfigFile: In a way a optional field, but it has to be
			%		  present even if it is ignored and left empty.
			%		  In this field a path to a config file of the 
			%		  model can be stored. The idea is not to store
			%		  configuration for the forecast in this file but
			%		  things actually needed to start, like paths to 
			%		  the executable files or system dependent config-
			%		  urations which have to be set only during 
			%		  installation of the model.
			
			import mapseis.util.PathMaker;
			
			%added the PathMaker to be sure that they are absolute paths
			obj.TempFolder = PathMaker(FrameConfig.TemporaryFolder);
			obj.ConfigFile = PathMaker(FrameConfig.ModelConfigFile);
			
			%Check if all complete
        		obj.ModRegReady(2)=true;
        		obj.ModelReady=all(obj.ModRegReady);
			
			%get the additional folders from the file
			obj.initFolders;
			
			%COMMENT: not ConfigID change here, because everything changed here
			%should not change the results, only the possible folder of the 
			%result files
			
			
        		
        	end
        	
        	
        	function ConfigureTestRegion(obj,RegionConfig)
			%This  method should allow to set the TestRegion config and grid
			%The variable RegionConfig will be structure with following 
			%entries:
			
			%RegionPoly:		Polygon surrounding the region
			%RegionBoundingBox:	Box around the Polygon
			%LonSpacing:		Longitude spacing or spacial spacing
			%			in case only on spacing is allowed
			%LatSpacing:		Latitude spacing, if two spacings 
			%			are allowed
			%GridPrecision:		The value to which the grid was rounded to.
			%MinMagnitude:		Minimum magnitude which should be 
			%			forecasted
			%MaxMagnitude:		The maximal magnitude which should be
			%			forecasted
			%MagnitudeBin:		Size of the Magnitude Bin often this 
			%			will be 0.1, but can be different. This
			%			will be empty if no magnitude binning is
			%			used
			%MinDepth:		Minimal depth range which should be 
			%			forecasted, in most cases this will be 0
			%MaxDepth:		Maximal depth to forecast
			%DepthBin:		In most cases this will empty as 3D 
			%			regions are not common, but in case a
			%			3D model is used this gives depth bin 
			%			size
			%TimeInterval:		Length of the forcast in days or year 
			%			(can be specified in getFeatures)
			%RelmGrid:		A raw grid similar to the one used in 
			%			Relm Forecasts based on the values 
			%			specified. 
			%			(Relmformat: [LonL LonH LatL LatH ...
			%			DepthL DepthH MagL MagH (ID) Mask]
			%Nodes			%Just the spacial nodes of the grid as 
			%			for instance by TripleS, in contrary to
			%			the RELM grid this descriptes the centre
			%			of the grid cell
			%MagVector:		Consisting of the Magnitudes which have
			%			to be forecasted (the points are the in 
			%			middle of the magnitude bin)
			%DepthVector:		Consisting of the Depths which have to 
			%			forecasted (only 3D) (the points are the  
			%			in middle of the depth bin)
			
			%Simple Only some are really needed, but everything stored
			%anyway
			obj.RegionConfStruct=RegionConfig;
			
			obj.setConfigID;
			
			%Check if all complete
        		obj.ModRegReady(3)=true;
        		obj.ModelReady=all(obj.ModRegReady);
				
	       	end
        	
        	
        	function ConfigData = getModelConfig(obj)
        		%Either gives out the inputed structure or returns some default values
        		
        		if ~isempty(obj.ModelConfStruct)
        			OutConf = obj.ModelConfStruct;
        			OutConf.StartRetroLearning = obj.convertTime(OutConf.StartRetroLearning);
        			OutConf.EndRetroLearning = obj.convertTime(OutConf.EndRetroLearning);
        			OutConf.StartRetroTesting = obj.convertTime(OutConf.StartRetroTesting);
        			OutConf.EndRetroTesting = obj.convertTime(OutConf.EndRetroTesting);
        			ConfigData = OutConf;
        			
        		else
        			%default values
        			ConfigData = struct(	'StartRetroLearning', 'yyyy/mm/dd hh:mm:ss',...
        						'EndRetroLearning' , 'yyyy/mm/dd hh:mm:ss',...
        			      			'StartRetroTesting', 'yyyy/mm/dd hh:mm:ss',...
        			      			'EndRetroTesting', 'yyyy/mm/dd hh:mm:ss',...
        			      			'FixedTime',false);
        		end
        		
        	end
        	
        	
        	function FrameConfig = getFrameConfig(obj)
			%Should return the two mentioned fields. If nothing is returned
			%or empty fields the internal default of the framework will be
			%used. This will work as well but is not user friendly. 
			
			FrameConfig.TemporaryFolder = obj.TempFolder;
			FrameConfig.ModelConfigFile = obj.ConfigFile; 
			
		end	
			
		
		
        	
        	function RegionConfig = getTestRegionConfig(obj)
			%It returns the internal region config structure
			RegionConfig = obj.RegionConfStruct;
		
        	end
        	
        	
        	function PossibleParameters = getParameterSetting(obj)
			%The method should return a description of the parameters needed
			%for the model and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be inputed by ConfigureModel
			%Each entry in structure should be another structure with following
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			be the an integer giving the position of the 
			%			selected string in the cell array. 
			%	UserDataSelect: This can be used instead of 'Items' in the 'list'
			%			entry type. It it allows to select previous, in
			%			the Datastore catalog stored, calculations like 
			%			for instance b-value, stress calculations and so 
			%			on. The GUI will in this case automatically generate
			%			the Items list, only thing needed is the field name 
			%			under which the data is stored in the datastore 
			%			userdata area (e.g. 'new-bval-calc' for b-value maps)
			%			the command Datastore.getUserDataKeys can be used
			%			to get the currently saved variables. The result
			%			sended back will in this case be the name of the 
			%			calculation (store in in the first column of the 
			%			result cell array). 
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			TimeEntry = struct(	'Type', 'edit',...
						'Format', 'string',...
						'ValLimit',[[1 1]],...
						'Name', 'Generic Time Entry',...
						'DefaultValue','yyyy-mm-dd hh:mm:ss');
			StartRetroLearn=TimeEntry;
			StartRetroLearn.Name='Start retro. learning period';

			StopRetroLearn=TimeEntry;
			StopRetroLearn.Name='End retro. learning period';
			
			StartRetroTest=TimeEntry;
			StartRetroTest.Name='Start retro. testing period';
			
			StopRetroTest=TimeEntry;
			StopRetroTest.Name='End retro. testing period';
			
			Fixer =	struct(	'Type', 'check',...
					'Name', 'Used Fixed Learning Time',...
					'DefaultValue',false);
					
			PossibleParameters = struct(	'StartRetroLearning', StartRetroLearn,...
        						'EndRetroLearning' , StopRetroLearn,...
        			      			'StartRetroTesting', StartRetroTest,...
        			      			'EndRetroTesting', StopRetroTest,...
        			      			'FixedTime',Fixer);
			
        	
        	end
        	
        	
        	function setInputData(obj,Datastore,Filterlist,StartDate)
			%This method is used by the project calculator to send the input
			%catalog data to the model. Sended will be a Datastore catalog 
			%object and a Filterlist object or boolean vector giving the 
			%selection of the earthquakes (islogical can be used to separate
			%between the two cases).
			%Everything what has to be done to bring the input catalog into 
			%the right format has to be done here. Like conversion into a 
			%file format readable by the model.
			
			import mapseis.projector.getDateNum;
			
			%check if it is the first run:
			NoShift=false;
			if isempty(obj.OrgStartTime)
				obj.OrgStartTime=StartDate;
				NoShift=true;
			end
			
			InputTimer=tic;
			
			obj.Datastore = Datastore;
			obj.Filterlist = Filterlist;
			obj.ForecastPeriod = [StartDate, StartDate+obj.RegionConfStruct.TimeInterval]
		
        			
             		obj.RetroSmoothPeriod = [obj.ModelConfStruct.StartRetroLearning,...
							obj.ModelConfStruct.EndRetroLearning];
			obj.RetroTargetPeriod = [obj.ModelConfStruct.StartRetroTesting,...
							obj.ModelConfStruct.EndRetroTesting];	
						
			if ~(NoShift|obj.ModelConfStruct.FixedTime)
				TimeShift = obj.OrgStartTime-StartDate;
				obj.RetroSmoothPeriod = obj.RetroSmoothPeriod+TimeShift;
				obj.RetroTargetPeriod = obj.RetroTargetPeriod+TimeShift;
			end
			
			
			FilterExist=false;
        		if isempty(obj.Filterlist)
				selected=true(obj.Datastore.getRowCount,1);	
			else
					
				if ~islogical(obj.Filterlist)
					obj.Filterlist.changeData(obj.Datastore);
					obj.Filterlist.updateNoEvent;
					selected=obj.Filterlist.getSelected;
					obj.Filterlist.PackIt;
					FilterExist=true;
				else
					selected=obj.Filterlist;
				end
			end
			
			if FilterExist
				TimeFilt=obj.Filterlist.getByName('Time');
				WhatPer = TimeFilt.getRange;
				
				TimePer=WhatPer{2};
				
						
				if isinf(TimePer(1))
					%use start of catalog
					TimePer(1)=min(getDateNum(obj.Datastore,selected));
					
				end
				
				if isinf(TimePer(2))
					%use end of catalog
					TimePer(2)=max(getDateNum(obj.Datastore,selected));
				end
        		else
        			TimePer(1)=min(getDateNum(obj.Datastore,selected));
        			TimePer(2)=max(getDateNum(obj.Datastore,selected));
        		end
        		
        		obj.LearningPeriod=TimePer;
        		
        		%to be sure
        		obj.ModelReady=all(obj.ModRegReady);
        		
        		%now build the files needed for the calculation
        		obj.generateFiles('config');
        		obj.generateFiles('catalog');
			obj.CPUTimeData.InputDataTime=toc(InputTimer);
        		
        	end
        	
        	
        	function UsedData = getInputData(obj,OnlyFilter)
			%This method will be used by the project calculator for archiving
			%the used data. The returned UsedData can have different format 
			%but suggested is zmap or shortcat based catalog or a datastore 
			%catalog (cutted!). If OnlyFilter is set to true, the UsedData 
			%variable should be a boolean vector giving the selected earthquakes
			
			%I go for the ShortCat this time, but it could easily be changed
			%at the moment the  function is more of diagnostic nature anyway
			import mapseis.projector.*;
			
			
			if isempty(obj.Datastore)|isempty(obj.Filterlist)
				UsedData=[];
			else
				if OnlyFilter
					if ~islogical(obj.Filterlist)
						obj.Filterlist.changeData(obj.Datastore);
						obj.Filterlist.updateNoEvent;
						UsedData=obj.Filterlist.getSelected;
						obj.Filterlist.PackIt;
					else
						UsedData=obj.Filterlist;
					end
				
				else
					if ~islogical(obj.Filterlist)
						obj.Filterlist.changeData(obj.Datastore);
						obj.Filterlist.updateNoEvent;
						selected=obj.Filterlist.getSelected;
						obj.Filterlist.PackIt;
					else
						selected=obj.Filterlist;
					end
        				
        				[UsedData temp]=getShortCatalog(obj.Datastore,selected);
				
				end
			end
        	end
        	
        	
        	
        	function FeatureList = getFeatures(obj)
			%This is used by the project builder to determine which parameters
			%are available for the model. It influences mostly the region 
			%setting, the way data is exchanged and some additional menus
			%The Output should be a structure with specified fieldnames with 
			%mostly boolean values.
			%Following keywords are available:
			%	MagDist:	Should be normally true, means forecasts
			%			for different magnitude bins are available
			%			-> Magnitude bins
			%	DepthDist:	If set to true the model allows forecasts
			%			for different depth bins
			%			-> 3D model
			%	RectGrid:	If true the model should allow different
			%			grid spacing for Longitude and Latitude
			%	TimeYear:	If false the project builder will use
			%			days for time interval value and if true
			%			years will be used.
			%	Parallel:	If true the this GUI allows parallization
			%			and the toggle will be sended by builder
			%	HostMode:	Only for parallel mode, if set to true
			%			builder is allows to use this object in
			%			a parfor loop or similar. If false the
			%			model deals itself the parallization
			%	NoFiles:	If set to true the model does not produce
			%			any files and thus they cannot be archived
			%			The project builder will in this case not 
			%			allow the option to archive files 
			%	LogData:	If true Logdata is available not only in 
			%			file form but also directly imported as
			%			string or structure or whatever.
			%	ForecastType:	This is not boolean value but string
			%			It describes what distribution is 
			%			returning. It can be 'Possion', 'NegBin'
			%			'Custom' and 'Selectable'.
			%			'Selectable' means that the model supports
			%			more than on distribution. 
			
			
			%The project builder does also have a standard feature set in 
			%case some parts of the feature list are not defined.
			%The default is:
			%	MagDist=true;
			%	DepthHist=false;
			%	RectGrid=false;
			%	TimeYear=false;
			%	Parallel=true;
			%	HostMode=false;
			%	NoFiles=false;
			%	LogData=true;
			%	ForecastType='Poisson';
			%Maybe more later
			
			%IMPORTANT: if ForecastType is 'Selectable', a field with
			%the name "ForecastType" has to be added to the Model Configuration
			%describing the actual ForecastType, also a descripting for the 
			%GUI has to be added allowing to change the parameter
			
			%TODO: May have to add stuff to this list
			
			%not so sure about the parallel processing, but I say no for the 
			%moment
			
			FeatureList = struct(	'MagDist',true,...
						'DepthDist',false,...
						'RectGrid',false,...
						'TimeYear',false,...
						'Parallel',false,...
						'HostMode',false,...
						'NoFiles',false,...
						'LogData',false,...
						'AddonData',false,...
						'ForecastType','Poisson');
						
						
        	end
			
			
        	
        	
        	
        	function prepareCalc(obj)
			%This method is called the project builder, and should do everything
			%what is needed to run the forecast later. Often this method is 
			%totally unnecessary as everything can be done be before or after 
			%call of this method. But in some cases it might be a good idea 
			%to separate some cpu heavy preparation from the configuration.
			%If not needed the function can defined as a empty function.
			%IMPORTANT: No catalog preparation should be done here, as the
			%catalog data is sended by the project calculator. This function
			%is the last function before a actual calculation run (called in
			%the startCalc method of the calculator.
			
			%set the First time to empty -> to get the shift right 
			%if non-fixed add. time intervals are selected
			PrepTimer=tic;
			obj.OrgStartTime=[];
			
			%generate not changing input files
			obj.generateFiles('grid');
			obj.CPUTimeData.PrepareTime=toc(PrepTimer);
        	end
        	
        	
        	
        	function ErrorCode = calcForecast(obj)
			%Names says it. If this method is called the calculation of the 
			%forecast and according the configuration and catalog data should
			%started and the result should be saved in this object as well as
			%the path to the result file in case any is existing.
			%The ErrorCode can be used to signalize errors, it is just an 
			%integer. 0 means everthing worked fine, 1 means model is not
			%configurated properly, 2 means catalog data is missing, 3 means 
			%something with a external function did not work out and -1
			%means a generic not specified error. Every other value can 
			%used to specify special model specific errors. It is of course
			%sensible to explain the error code somewhere.
			%The error can be specified in the function getErrorList(obj), but 
			%it is as often optional, a generic function is integrated anyway.
			%Currently the function is not directly used, but can change very
			%soon
			
			ErrorCode=-1;
			
			%check if everything is set
			if ~obj.ModelReady
				%here it really should be an error, because
				%that can be changed easily 
				error('Model is not configurated')
			end
			
						
			%check if files are existing
			Checker=true
			
			%grid
			CheckExist=exist([obj.GridPath,filesep,'triples.forecast.nodes.dat']);
        		Checker=Checker&CheckExist~=0
        		
        		%config
        		CheckExist=exist([obj.GridPath,filesep,'TripleS_MainConfig.prop']);
        		Checker=Checker&CheckExist~=0
        		
        		%catalog
        		CheckExist=exist([obj.CatalogPath,filesep,'TripleS_Input_Catalog.cat']);
        		Checker=Checker&CheckExist~=0
			
        		if ~Checker
        			warning('File not found. Model not prepared/ no data sended');
        			return
        		end
        		
        		     		        		        		
			%call the java external
			%example command
			%java -jar ./TripleS.jar ./configfiles/italy.3month.properties
			
			fs=filesep;
			
			CalcTimer=tic;
			if isunix
				UCommand=[obj.JavaPath,' -jar ', obj.ExtModel,' ',obj.GridPath,fs,'TripleS_MainConfig.prop'];
				[stat, res] = unix(UCommand);
				obj.LogData=res;
			else
				DCommand=[obj.JavaPath,' -jar ', obj.ExtModel,' ',obj.GridPath,fs,'TripleS_MainConfig.prop'];
				%The dos/windows command is so far not tested, as I have no
				%windows system available at the moment
				
				[stat, res] = dos(DCommand);
				obj.LogData=res;
				
			end
			obj.CPUTimeData.CalcTime=toc(CalcTimer);
			
			
			
			%check if no error was present
			if stat~=0
				ErrorCode=3;
				warning('There was an error while executing the java external');
				return
			end
			
			%finished
			ErrorCode=0;
			
			
        	end
        	
        	
        	function [ForeCast, DataMask] = getForecast(obj,WithGrid)
			%This should return the calculated forecast. The Format which should
			%be returned is depended on the ForecastType and on the input 
			%parameter WithGrid. For 'Poisson' and 'NegBin' the output should be
			%vector (Poisson) and a "twin" vector (NegBin) and if WithGrid is
			%set to true a full Relm Grid forecast should be outputed. In the
			%'Custom' mode the output will always be a cell, without WithGrid
			%the output should be a cell vector containing the single grid cells
			%discrete distribution, else it should be a cell with two elements
			%first one containing the Relmgrid, second one containing the 
			%forecast like mentioned before.
			%The DataMask is the last column in a typical Relm based forecast
			%and marks if a cell has been used/defined (1) or not (0). It can 
			%be left empty, in this case it is assumed that all nodes are used.
			%In case of a full WithGrid output it DataMask will be ignored and
			%the grid will be used instead
			
			%check if file is existing
			CheckExist=exist([obj.ResultPath,filesep,'TripleS_Forecast_Result.xml']);
        		
        		if CheckExist==0
        			error('Output file not found');
        		end
        		ImportTimer=tic;
        		
        		%use xml importer
			[RelmGrid MetaData] = obj.importXMLGrid([obj.ResultPath,filesep,'TripleS_Forecast_Result.xml']);
        		
			%decide what has to be sended
			if WithGrid
				ForeCast=RelmGrid;
				DataMask=RelmGrid(:,10);
			else
				ForeCast=RelmGrid(:,9);
				DataMask=RelmGrid(:,10);
			end
			
			obj.CPUTimeData.ImportTime=toc(ImportTimer);
			
		end
         	
         	
        	
        	function FilePath = getForecastFile(obj)
			%This method will only be called if NoFiles is false and if the 
			%option to archive the forecast files is switched on the project
			%object. 
			%It should return the path to the forecast file as a string. The 
			%project calculater will then copy the file to a specific folder
			%save the link to this file. If there is more then one 
			%result file existing a Cell array with multiple paths (strings)
			%can be used instead of a normal single path
			CheckExist=exist([obj.ResultPath,filesep,'TripleS_Forecast_Result.xml']);
        		
        		if CheckExist==0
        			error('Output file not found');
        		end
        		
        		FilePath=[obj.ResultPath,filesep,'TripleS_Forecast_Result.xml'];
        		
			
		end
		
		

		function LogData = getCalcLog(obj)
			%This will be called if saving logs is activated and LogData is 
			%set to true in the FeatureList. The method should return a log 
			%of the calculation in a matlab common format and it should NOT
			%be a file link, because the log file will not be copied.
			%the easiest way to create the LogData is to just read the file 
			%into a string and return the string.
			
			%QUESTION: does tripleS have any log data?
			%Nope, but the console output can be delivered
			LogData=obj.LogData;
			
			
			
		end
		
		
		function LogFile = getCalcLogFile(obj)
			%The project calculator will only use this method if NoFiles is
			%set to false in the FeatureList and if the option to store log
			%files is activated in the project. This method should return a
			%file path(s) to the log file(s), the project builder will copy 
			%the files and store the new path. If there is more then one 
			%logfiles existing a Cell array with multiple paths (strings)
			%can be used instead of a normal single path
			
			%QUESTION: does tripleS have any log data?
			%--> No log files
			LogFile=[];
			
			
			
		end
        	
		
        	function ResetLastCalc(obj)
			%This method should revert the plugin to the state before the 
			%calculation of the forecast, so that new data can be sent and
			%a new forecast can be calculated. This may involve things like 
			%empty the catalog storage and and deleting some input and output
			%files. But it might as well be just an empty function if no clean
			%up is needed. 
			%IMPORTANT: this method should revert the plugin to the state AFTER
			%prepareCalc was called.
			
			%means removing data and files, but not reset the OrgStartTime
			
			%delete files from list
			for i=1:numel(obj.CalcFiles)
				try
					if isunix
						UCommand=['rm ', obj.CalcFiles{i}];
						[stat, res] = unix(Ucommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					else
						DCommand=['del ', obj.CalcFiles{i}];
						[stat, res] = dos(Dcommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					end
					
				catch
					warning('File did not exist')
				end
			end
			
			obj.CalcFiles={};
			
			%reset some of the properties
			obj.ForecastPeriod=[];
        		obj.LearningPeriod=[];
        		obj.RetroSmoothPeriod=[];
			obj.RetroTargetPeriod=[];
			obj.Datastore=[];
        		obj.Filterlist=[];
			obj.LogData=[];
        	end
        	
        	
        	function AddonData = getAddonData(obj)
			%This will be called if saving AddonData is activated and AddonData is 
			%set to true in the FeatureList. The method can return anything over 
			%this function, but the model has also to keep track itself in case
			%it is timevariing data
		
		
			AddonData=[];
        		warning('Model does not have additional data');
		
		end
		
        	
        	
        	function ResetFullCalc(obj)
			%Similar to ResetLastCalc but this one should revert the plugin to
			%state BEFORE prepareCalc. The function can be equal to ResetLastCalc
			%but it should not cause an error because double execution of a command
			%(e.g. delete of a file)
			
			%reset everything apart config, so first call ResetLastCalc
			
			obj.ResetLastCalc;
			
			
			
			%delete files
			for i=1:numel(obj.PrepareFiles)
			
				try
					if isunix
						UCommand=['rm ', obj.PrepareFiles{i}];
						[stat, res] = unix(Ucommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					else
						DCommand=['del ', obj.PrepareFiles{i}];
						[stat, res] = dos(Dcommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					end
				catch
					warning('File did not exist')
				end
			end
			
			obj.PrepareFiles={};
			
			%reset some of the properties
			%reset startTime
			obj.CPUTimeData=[];
			obj.OrgStartTime=[];
		end
		
		
		%CUSTOM FUNCTIONS
		
		function generateFiles(obj,FileType)
			%generates the needed input files
			import mapseis.exportfilter.*;
			import mapseis.util.PropertiesIO;
			import mapseis.util.PathMaker; 
			
			switch FileType
				case 'catalog'
					%output should be Zmap catalog
					if isempty(obj.Datastore)
						warning('No Datastore available, could not generate catalog file')
						return
					end
					
					if isempty(obj.Filterlist)
						selected=true(obj.Datastore.getRowCount,1);	
					else
					
						if ~islogical(obj.Filterlist)
							obj.Filterlist.changeData(obj.Datastore);
							obj.Filterlist.updateNoEvent;
							selected=obj.Filterlist.getSelected;
							obj.Filterlist.PackIt;
						else
							selected=obj.Filterlist;
						end
					end
					
					FileName=[obj.CatalogPath,filesep,'TripleS_Input_Catalog.cat'];
					
					try
						exportZMAPCatalogASCII(obj.Datastore,selected,FileName,'-file');
					catch
						error('could not write catalog file')
					end
					
					%add file to the list to delete
					obj.CalcFiles{end+1} = FileName;
					
					
					
				case 'config'
					%config file	
					%Unfinished
					if ~obj.ModelReady
						error('Model not fully configurated')
					end	
					
					fs=filesep;
														
					TheProps=PropertiesIO;
					
					ModConfigFileName=[obj.GridPath,fs,'TripleS_MainConfig.prop'];
					TheProps.setFile(ModConfigFileName);
					
					%The Paths first
					InCatPath = [obj.CatalogPath,fs,'TripleS_Input_Catalog.cat'];
					GridPath = [obj.GridPath,fs,'triples.forecast.nodes.dat'];
					OutResPath = [obj.ResultPath,fs,'TripleS_Forecast_Result.xml'];
					ColCatPath = [obj.CatalogPath,fs,'TripleS_Collection_Catalog.cat'];
					RetroSmoothCat = [obj.CatalogPath,fs,'TripleS_RetroSmooth_Catalog.cat'];
					RetroTargetCat = [obj.CatalogPath,fs,'TripleS_RetroTarget_Catalog.cat'];
					ProSmoothCat = [obj.CatalogPath,fs,'TripleS_ProSmooth_Catalog.cat'];
					AFVSPath = [obj.AVFSPath,fs];
					
					TheProps.setProperty('inputCatalogPath',InCatPath);
					TheProps.setProperty('forecastNodesPath',GridPath);
					TheProps.setProperty('rateForecastMLPath',OutResPath);
					TheProps.setProperty('collectionRegionCatalogPath',ColCatPath);
					TheProps.setProperty('retrospectiveSmoothingCatalogPath',RetroSmoothCat);
					TheProps.setProperty('retrospectiveTargetCatalogPath',RetroTargetCat);
					TheProps.setProperty('prospectiveSmoothingCatalogPath',ProSmoothCat);
					TheProps.setProperty('afvPath',AFVSPath);
					
					
					
					%add files to the list (at least the none AFVS files)
					obj.CalcFiles{end+1} = InCatPath;
					obj.CalcFiles{end+1} = GridPath;
					obj.CalcFiles{end+1} = OutResPath;
					obj.CalcFiles{end+1} = ColCatPath;
					obj.CalcFiles{end+1} = RetroSmoothCat;
					obj.CalcFiles{end+1} = RetroTargetCat;
					obj.CalcFiles{end+1} = ProSmoothCat;
										
					%Times
					ForeStart = obj.convertTime(obj.ForecastPeriod(1));
					ForeEnd = obj.convertTime(obj.ForecastPeriod(2));
					proSmoothStart = obj.convertTime(obj.LearningPeriod(1));
					proSmoothEnd = obj.convertTime(obj.LearningPeriod(2));
					
					retroSmoothStart = obj.convertTime(obj.RetroSmoothPeriod(1));
					retroSmoothEnd = obj.convertTime(obj.RetroSmoothPeriod(2));
					retroTargetStart = obj.convertTime(obj.RetroTargetPeriod(1));
					retroTargetEnd = obj.convertTime(obj.RetroTargetPeriod(2));
				
					TheProps.setProperty('forecastStartDate',ForeStart);
					TheProps.setProperty('forecastEndDate',ForeEnd);
					TheProps.setProperty('prospectiveSmoothingPeriodStart',proSmoothStart);
					TheProps.setProperty('prospectiveSmoothingPeriodEnd',proSmoothEnd);
					
					TheProps.setProperty('retrospectiveSmoothingPeriodStart',retroSmoothStart);
					TheProps.setProperty('retrospectiveSmoothingPeriodEnd',retroSmoothEnd);
					TheProps.setProperty('retrospectiveTargetPeriodStart',retroTargetStart);
					TheProps.setProperty('retrospectiveTargetPeriodEnd',retroTargetEnd);
					
					
					%The spatial grid
					minLat=num2str(obj.RegionConfStruct.RegionBoundingBox(3));
					maxLat=num2str(obj.RegionConfStruct.RegionBoundingBox(4));
					minLon=num2str(obj.RegionConfStruct.RegionBoundingBox(1));
					maxLon=num2str(obj.RegionConfStruct.RegionBoundingBox(2));
					boxSize=num2str(obj.RegionConfStruct.LonSpacing);
					
					TheProps.setProperty('minLat',minLat);
					TheProps.setProperty('maxLat',maxLat);
					TheProps.setProperty('minLon',minLon);
					TheProps.setProperty('maxLon',maxLon);
					TheProps.setProperty('boxSize',boxSize);
					
					%Magnitudes and Depth
					minMag=num2str(obj.RegionConfStruct.MinMagnitude);
					maxMag=num2str(obj.RegionConfStruct.MaxMagnitude);
					MagBin=num2str(obj.RegionConfStruct.MagnitudeBin);
					minDepth=num2str(obj.RegionConfStruct.MinDepth);
					maxDepth=num2str(obj.RegionConfStruct.MaxDepth);
					
					TheProps.setProperty('minTargetMagnitude',minMag);
					TheProps.setProperty('maxTargetMagnitude',maxMag);
					TheProps.setProperty('magnitudeBinSize',MagBin);
					TheProps.setProperty('minDepth',minDepth);
					TheProps.setProperty('maxDepth',maxDepth);
					
					%It is complete, lets write it 
					TheProps.writeFile;
					
					
				case 'grid'
					%The grid file 
					if isempty(obj.RegionConfStruct)
						warning('No Grid defined, could not generate file')
						return
					end
					
					TheGrid=obj.RegionConfStruct.Nodes;
					
					FileName=[obj.GridPath,filesep,'triples.forecast.nodes.dat'];
					
					try
						dlmwrite(FileName, TheGrid, 'delimiter', '\t','precision', 6);
					catch
						error('could not write grid file')
					end
					
					%add file to prepare delete list
					obj.PrepareFiles{end+1}=FileName;
			end
			
		end
        	
		
		
		function initFolders(obj)
			
			import mapseis.util.PropertiesIO;
			import mapseis.util.PathMaker; 
			
			
			%reads the paths from the FrameConfig files
			%and generates temporary folders if needed.
			
			%The files/paths should use the following keywords followed by a
			% '=' sign. (e.g. TripleS.MainFolder=./mapseis/Forecast_Model 
			%1:ParentFolder TripleS: 'TripleS.MainFolder' 
			%  (normally in mapseis/Forecast_Model folder)
			%2: Path to executable: TripleS.Java (TripleS.jar)
			%3: Path of the catalog folder TripleS.CatFolder
			%4: Path of the folder for grids and configs TripleS.ConfigFolder
        		%5: Path of the folder for the AVFS files TripleS.AVFS
        		%6: Path of the Result Folder TripleS.Results
        		
        		%3 to 6 are best left empty, like this the default folders in the
        		%temporary folders will be used.
        		
        		%Pathmaker is used to generate absolute paths if possible.
        		
        		fs=filesep;
        		
        		TheProps=PropertiesIO(obj.ConfigFile);
        		
        		%MainFolder and executable path can of course not be created
        		obj.ExternalPath=PathMaker(TheProps.getProperty('TripleS.MainFolder'));
        		obj.ExtModel=PathMaker(TheProps.getProperty('TripleS.Java'));
        		
        		        		        		
        		%The rest is optional
        		CatPath=TheProps.getProperty('TripleS.CatFolder');
        		if isempty(CatPath)
        			CatPath=[obj.TempFolder,fs,'TripleS',fs,'catalogs'];
        		end
        		
        		ConfigPath=TheProps.getProperty('TripleS.ConfigFolder');
        		if isempty(ConfigPath)
        			ConfigPath=[obj.TempFolder,fs,'TripleS',fs,'configfiles'];
        		end
        		
        		AFVSPath=TheProps.getProperty('TripleS.AVFS');
        		if isempty(AFVSPath)
        			AFVSPath=[obj.TempFolder,fs,'TripleS',fs,'afvs'];
        		end
        		
        		ResPath=TheProps.getProperty('TripleS.Results');
        		if isempty(ResPath)
        			ResPath=[obj.TempFolder,fs,'TripleS',fs,'results'];
        		end
        		
        		%generate folders if not existing
        		ToGen={CatPath,ConfigPath,AFVSPath,ResPath};
        		ToName={'Catalog','Config','AFVS','Result'};
        		
        		for i=1:numel(ToGen)
				CheckExist=exist(ToGen{i});
				
				if CheckExist==0
					if isunix
						Ucommand=['mkdir -p ',ToGen{i}];
						[stat, res] = unix(Ucommand);
						
						if stat==0
							disp([ToName{i}, ' folder generated']);
						else
							warning(['Could not generate ', ToName{i}, ' folder']);
						
						end
					else
						Dcommand=['mkdir -p ',ToGen{i}];
						[stat, res] = dos(Dcommand);
						
						if stat==0
							disp([ToName{i}, ' folder generated']);
						else
							warning(['Could not generate ', ToName{i}, ' folder']);
						
						end
					end
					
				end
        		end
        		
        		
        		
        		
        		obj.CatalogPath=PathMaker(CatPath);
        		obj.GridPath=PathMaker(ConfigPath);
        		obj.AVFSPath=PathMaker(AFVSPath);
        		obj.ResultPath=PathMaker(ResPath);
        		
			
			
		end
        	
		
		function TripTime = convertTime(obj,inTime)
        		%converts a datenum into a date strings used by the 
        		%TripleS model and vice versa. It would not need to be 
        		%internal method the TripleS plugin, but it is not needed 
        		%outside, and like this it is easier to call inside the object.
        		%inTime: datenum format e.g 7.3516e+05
        		%TripTime: time string of the format: '2012/10/16 16:30:58'
        		
        		%actually it is only on line, but still handy
        		if isnumeric(inTime)
        			TripTime = datestr(inTime,'yyyy/mm/dd HH:MM:SS');
        		else
        			TripTime = datenum(inTime,'yyyy/mm/dd HH:MM:SS');
        		end
        		
		end
        	
        end
        
        
       
        
        
end        

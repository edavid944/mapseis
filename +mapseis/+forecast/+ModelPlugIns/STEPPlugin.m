classdef STEPPlugin < mapseis.forecast.ForecastPlugIn
	%based on the plugin interface, the first plugin for the STEP java-base
	%forecast model
	
	%UNFINISHED
	
	properties
		%Defined in Plugin superclass
		%  PluginName
        	%  Version %Has to be a STRING (consist of CodeVersion and Variant)
        	%  CodeVersion %Version of the code
        	%  Variant %allows to use multiple varitions of the same model in a project
        	%  ConfigID %Has to be re-set everytime the model is configurated
        	%  ModelInfo %The place for a description about the model 
        	%  
        	%  Datastore
        	%  Filterlist
        	%  ModelReady 
        	
        	LogData
        	
        	ForecastPeriod
        	LearningPeriod
        	
        	
        	
        	ModelConfStruct
        	RegionConfStruct        	
        	ModRegReady
        	OrgStartTime
        	
        	       	
        	TempFolder
        	ConfigFile
        	TripConfig
        	ExternalPath
        	ExtModel
        	DataPath
        	StepConfigPath
        	LogPath
        	ResultPath
        	BGrates
        	SpecialGrid
        	AddGridParam
        	JavaPath
        	
        	PrepareFiles
        	CalcFiles
        	ShiftVector
        end
    

       
        methods 
        	function obj = STEPPlugin(Variant)
              		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Variant=[];
        		end
        		
        		if isempty(Variant)
        			Variant=RawChar(randi(15,1,3)+1)
        		end
        		
        		obj.Variant=Variant;
        		
        		obj.PluginName='STEP_java';
        		obj.CodeVersion='v1';
        		obj.Version=[obj.CodeVersion,'_',obj.Variant];
        		obj.ModelInfo=[	'This is the first version of a plugin for ',...
        				'STEP java based model by Matt Gerstenberger'];
        				
        		%Init config key
        		obj.setConfigID;
        	
        		obj.Datastore=[];
        		obj.Filterlist=[];
        		
        		obj.LogData=[];
        		
        		obj.ModelConfStruct=[];
        		obj.RegionConfStruct=[];   
        		
        		obj.ForecastPeriod=[];
        		obj.LearningPeriod=[];
        		        		
        		obj.ModRegReady=[false false false];
        		obj.ModelReady=false;
        		
        		obj.CPUTimeData=[];
        		
        		%paths	
        		fs=filesep;
        		obj.TempFolder=[];
        		obj.ConfigFile=['.',fs,'Forecast_Models',fs,'STEP_Java',fs,'FrameConfig.conf']
        		obj.ExternalPath=[];
        		obj.ExtModel=[];
        		obj.DataPath=[];
        		obj.StepConfigPath=[];
        		obj.LogPath=[];
        		obj.ResultPath=[];
        		obj.TripConfig=[];
        		obj.BGrates=[];
        		obj.SpecialGrid=[];
        		obj.AddGridParam;
        		%STEP is not really flexible concering the grid
        		%that's why a slightly oversized rectangular grid is needed
        		
        		%makes deleting the files a bit faster
        		obj.PrepareFiles={};
        		obj.CalcFiles={};
        		obj.ShiftVector=[];
        		%OK, but check the position of the frameconfig file and 
        		%add the additional files if needed
        		
        		
        		%new feature instead of just use java allow to set a path to a custom version of it
        		obj.JavaPath='java';
        	end
        	   	
        	       	
        	function ConfigureModel(obj,ConfigData)
        		%allows to configurate the model
        		
        		%convert datestrings
        		import mapseis.util.PathMaker;
        		
        		%TripleS parameters
        		%ConfigData.StartRetroLearning = obj.convertTime(ConfigData.StartRetroLearning);
        		%ConfigData.EndRetroLearning = obj.convertTime(ConfigData.EndRetroLearning);
        		%ConfigData.StartRetroTesting = obj.convertTime(ConfigData.StartRetroTesting);
        		%ConfigData.EndRetroTesting = obj.convertTime(ConfigData.EndRetroTesting);
        		
        		obj.ModelConfStruct=ConfigData;
        		obj.ModelConfStruct.background_rate_file=PathMaker(obj.ModelConfStruct.background_rate_file);
        			      			
        	        %Parameters needed here:
        	        %new_generic_model: 0 for old generic model (default) 1 for the new one
        	        %background_rate_file: 	location of the background rates file, it may be an option
        	        %			to add an option for automatically use TripleS for the background
        	        %			later, but not now. 
        	        %min_mag_main: minimum allowed magnitude for a mainshock
        	        %days_from_qdm:	no idea what this is, but it is per default 7
        	        %min_aftershock_radium: default is 10, I guess it is the minium radius a aftershock region 
        	        %			should have
        	        %grid_bg_anchor: anchor of the background grid (?) default is 0.05
        	        %grid_cutoff: cutoff distance (of the grid spacing) in calculation of the forecast (0.5 default)
        	        %fixed_forecast_param: true if the parameters for the forecast should be fixed
        	        %a_value: says it (default US: -2.18) (has to be negative)
        	        %b_value: says it (default US: 0.84)
        		%p_value: says it (default US: 1.05)
        		%c_value: says it (default US: 0.05)
        		%Lat_Shift: Step does not like high latitude regions, this can be used to shift a region down to
			%	    more "accepted" latitudes. Before the calculation every latitude is shifted by -(Lat_Shift)
			%	    and after calculation is shifted back with +Lat_Shift
        		
        		%parameters about which I'm not sure: 
        		%different forecast.start.time and event.start.time: does the later one have to be configuratable?
        		%grid.precision: I think it is alright to have it fixed, but is there a need to allow it in the config
        		%save.models: is it only technical or does it matter?
        		
        		
        		
        		obj.setConfigID;
        		
        		
			%Check if all complete
        		obj.ModRegReady(1)=true;
        		obj.ModelReady=all(obj.ModRegReady);
        		
        	end	
        	
        	
        	function ConfigureFramework(obj,FrameConfig)
        	
			%Similar to ConfigureModel, but consist only of framework based
			%variable which are defined by the framework. It consist at the
			%moment out of two fields:
			%TemporaryFolder: this is path of the temporary folder which may
			%		  be used to temporary store the files needed for
			%		  a forecast. 
			%ModelConfigFile: In a way a optional field, but it has to be
			%		  present even if it is ignored and left empty.
			%		  In this field a path to a config file of the 
			%		  model can be stored. The idea is not to store
			%		  configuration for the forecast in this file but
			%		  things actually needed to start, like paths to 
			%		  the executable files or system dependent config-
			%		  urations which have to be set only during 
			%		  installation of the model.
			
			import mapseis.util.PathMaker;
			
			%added the PathMaker to be sure that they are absolute paths
			obj.TempFolder = PathMaker(FrameConfig.TemporaryFolder);
			obj.ConfigFile = PathMaker(FrameConfig.ModelConfigFile);
			
			%Check if all complete
        		obj.ModRegReady(2)=true;
        		obj.ModelReady=all(obj.ModRegReady);
			
			%get the additional folders from the file
			obj.initFolders;
			
			%COMMENT: not ConfigID change here, because everything changed here
			%should not change the results, only the possible folder of the 
			%result files
			
			
        		
        	end
        	
        	
        	function ConfigureTestRegion(obj,RegionConfig)
			%This  method should allow to set the TestRegion config and grid
			%The variable RegionConfig will be structure with following 
			%entries:
			
			%RegionPoly:		Polygon surrounding the region
			%RegionBoundingBox:	Box around the Polygon
			%LonSpacing:		Longitude spacing or spacial spacing
			%			in case only on spacing is allowed
			%LatSpacing:		Latitude spacing, if two spacings 
			%			are allowed
			%GridPrecision:		The value to which the grid was rounded to.
			%MinMagnitude:		Minimum magnitude which should be 
			%			forecasted
			%MaxMagnitude:		The maximal magnitude which should be
			%			forecasted
			%MagnitudeBin:		Size of the Magnitude Bin often this 
			%			will be 0.1, but can be different. This
			%			will be empty if no magnitude binning is
			%			used
			%MinDepth:		Minimal depth range which should be 
			%			forecasted, in most cases this will be 0
			%MaxDepth:		Maximal depth to forecast
			%DepthBin:		In most cases this will empty as 3D 
			%			regions are not common, but in case a
			%			3D model is used this gives depth bin 
			%			size
			%TimeInterval:		Length of the forcast in days or year 
			%			(can be specified in getFeatures)
			%RelmGrid:		A raw grid similar to the one used in 
			%			Relm Forecasts based on the values 
			%			specified. 
			%			(Relmformat: [LonL LonH LatL LatH ...
			%			DepthL DepthH MagL MagH (ID) Mask]
			%Nodes			%Just the spacial nodes of the grid as 
			%			for instance by TripleS, in contrary to
			%			the RELM grid this descriptes the centre
			%			of the grid cell
			%MagVector:		Consisting of the Magnitudes which have
			%			to be forecasted (the points are the in 
			%			middle of the magnitude bin)
			%DepthVector:		Consisting of the Depths which have to 
			%			forecasted (only 3D) (the points are the  
			%			in middle of the depth bin)
			
			%Simple Only some are really needed, but everything stored
			%anyway
			obj.RegionConfStruct=RegionConfig;
			
			obj.setConfigID;
			
			%Check if all complete
        		obj.ModRegReady(3)=true;
        		obj.ModelReady=all(obj.ModRegReady);
				
	       	end
        	
        	
        	function ConfigData = getModelConfig(obj)
        		%Either gives out the inputed structure or returns some default values
        		%It is not needed to convert the TripleS times forward and backward, the
        		%TripleS plugin does this.
        		
        		if ~isempty(obj.ModelConfStruct)
        			OutConf = obj.ModelConfStruct;
        			%OutConf.StartRetroLearning = obj.convertTime(OutConf.StartRetroLearning);
        			%OutConf.EndRetroLearning = obj.convertTime(OutConf.EndRetroLearning);
        			%OutConf.StartRetroTesting = obj.convertTime(OutConf.StartRetroTesting);
        			%OutConf.EndRetroTesting = obj.convertTime(OutConf.EndRetroTesting);
        			ConfigData = OutConf;
        			        			
        		else
        			%default values
        			ConfigData=struct(	'new_generic_model',false,...
        						'background_rate_file',[[pwd,filesep,'anyfile.txt']],...
        						'min_mag_main',3,...
        						'days_from_qdm',7,...
        						'min_aftershock_radium',10,...
        						'grid_bg_anchor',0.05,...
        						'grid_cutoff',0.5,...
        						'fixed_forecast_param',false,...
        						'a_value',-2.18,...
        						'b_value',0.84,...
        						'p_value',1.05,...
        						'c_value',0.05,...
        						'Lat_Shift',0,...
        						'UseTripleS_BG',true,...
        						'StartRetroLearning', 'yyyy/mm/dd hh:mm:ss',...
        						'EndRetroLearning' , 'yyyy/mm/dd hh:mm:ss',...
        			      			'StartRetroTesting', 'yyyy/mm/dd hh:mm:ss',...
        			      			'EndRetroTesting', 'yyyy/mm/dd hh:mm:ss',...
        			      			'FixedTime',false);
        		
        			      			
				%Parameters needed here:
				%new_generic_model: 0 for old generic model (default) 1 for the new one
				%background_rate_file: 	location of the background rates file, it may be an option
				%			to add an option for automatically use TripleS for the background
				%			later, but not now. 
				%min_mag_main: minimum allowed magnitude for a mainshock
				%days_from_qdm:	no idea what this is, but it is per default 7
				%min_aftershock_radium: default is 10, I guess it is the minium radius a aftershock region 
				%			should have
				%grid_bg_anchor: anchor of the background grid (?) default is 0.05
				%grid_cutoff: cutoff distance (of the grid spacing) in calculation of the forecast (0.5 default)
				%fixed_forecast_param: true if the parameters for the forecast should be fixed
				%a_value: says it (default US: -2.18) (has to be negative)
				%b_value: says it (default US: 0.84)
				%p_value: says it (default US: 1.05)
				%c_value: says it (default US: 0.05)
				%Lat_Shift: Step does not like high latitude regions, this can be used to shift a region down to
				%	    more "accepted" latitudes. Before the calculation every latitude is shifted by -(Lat_Shift)
				%	    and after calculation is shifted back with +Lat_Shift
				%UseTripleS_BG: if true a TripleS model will be used for the background rates instead of an external
				%		file
				%Rest of the parameter is for the TripleS model if used.
				
				%COMMENT: If a Background file is used, the grid should be on each side 2 cells bigger than the bounding
				%box of the polygon and same spacing and so on.
        		end
        		
        	end
        	
        	
        	function FrameConfig = getFrameConfig(obj)
			%Should return the two mentioned fields. If nothing is returned
			%or empty fields the internal default of the framework will be
			%used. This will work as well but is not user friendly. 
			
			FrameConfig.TemporaryFolder = obj.TempFolder;
			FrameConfig.ModelConfigFile = obj.ConfigFile; 
			
		end	
			
		
		
        	
        	function RegionConfig = getTestRegionConfig(obj)
			%It returns the internal region config structure
			RegionConfig = obj.RegionConfStruct;
		
        	end
        	
        	
        	function PossibleParameters = getParameterSetting(obj)
			%The method should return a description of the parameters needed
			%for the model and should also include information about the way
			%the gui for setting the parameters should be designed.
			%The output has to be a structure with the field names being the 
			%name of the parameters which should be inputed by ConfigureModel
			%Each entry in structure should be another structure with following
			%keywords:
			%	Type: 		can be 	'check' for boolean,
			%				'edit' for any value or small text
			%				'list' for a selection of values specified
			%					by Items
			%				'text' for longer string boxes (e.g. Comments)
			%
			%	Format:		Defines the data type of value only applyable with
			%			'edit'. It can be the following: 'string','float',
			%			'integer','file','dir'	
			%	ValLimit: 	The limit a value can have in a edit box, if left
			%			empty it will set to [-9999 9999].
			%	Name:		Sets the Name used for the value in the GUI, if 
			%			left empty the variable name will be used instead
			%	Items:		Only needed in case of 'list', there it has to be 
			%			cell array with strings. The value returned will
			%			be the an integer giving the position of the 
			%			selected string in the cell array. 
			%	UserDataSelect: This can be used instead of 'Items' in the 'list'
			%			entry type. It it allows to select previous, in
			%			the Datastore catalog stored, calculations like 
			%			for instance b-value, stress calculations and so 
			%			on. The GUI will in this case automatically generate
			%			the Items list, only thing needed is the field name 
			%			under which the data is stored in the datastore 
			%			userdata area (e.g. 'new-bval-calc' for b-value maps)
			%			the command Datastore.getUserDataKeys can be used
			%			to get the currently saved variables. The result
			%			sended back will in this case be the name of the 
			%			calculation (store in in the first column of the 
			%			result cell array). 
			%	DefaultValue:	Sets the value used as default value in the GUI.
			%			If missing, 0 will be used as default or 1 for 
			%			lists
			%	Style:		Only applyable for the Type 'list', it sets the 
			%			GUI element used to represent the list and it can
			%			be:	
			%				'listbox': a scrollable list for the selection
			%				'popupmenu': a popup list 
			%				'radiobutton': a block of checkerboxes
			%				'togglebutton': a selection of buttons
			%			If missing 'popupmenu' will be used. The last
			%			two options are only suggest if not many elements
			%			are in the list.
			
			genericEntry = struct(	'Type', 'edit',...
						'Format', 'float',...
						'ValLimit',[[-9999 9999]],...
						'Name', 'Generic Float Entry',...
						'DefaultValue',0);
						
						
			
			TimeEntry = struct(	'Type', 'edit',...
						'Format', 'string',...
						'ValLimit',[[1 1]],...
						'Name', 'Generic Time Entry',...
						'DefaultValue','yyyy-mm-dd hh:mm:ss');			
						
			Fixer =	struct(	'Type', 'check',...
					'Name', 'LogSel',...
					'DefaultValue',false);			
						
			genMod=Fixer;
			genMod.Name='Use new generic model';

			bgFile=genericEntry;
			bgFile.Name='Background rate file';
			bgFile.Format='file';
			bgFile.DefaultValue=[[pwd,filesep,'anyfile.txt']];
			
			minMagMain=genericEntry;
			minMagMain.Name='min. magnitude mainshock';
			minMagMain.DefaultValue=3;
			
			DayQDM=genericEntry;
			DayQDM.Name='Days from QDM';
			DayQDM.DefaultValue=7;
			
			afterRad=genericEntry;
			afterRad.Name='min. aftershock radium';
			afterRad.DefaultValue=10;
			
			gridAnch=genericEntry;
			gridAnch.Name='BG Grid anchor point';
			gridAnch.DefaultValue=0.05;
			
			gridCut=genericEntry;
			gridCut.Name='Grid calculation cutoff';
			gridCut.DefaultValue=0.5;
			
			fixParam=Fixer;
			fixParam.Name='Fix forecast parameters';
			
			aval=genericEntry;
			aval.Name='a-value';
			aval.DefaultValue=-2.18;
			
			bval=genericEntry;
			bval.Name='b-value';
			bval.DefaultValue=0.84;
			
			pval=genericEntry;
			pval.Name='p-value';
			pval.DefaultValue=1.05;
			
			cval=genericEntry;
			cval.Name='c-value';
			cval.DefaultValue=0.05;
			
			Lat_Shift=genericEntry;
			Lat_Shift.Name='Latitude Shift';
			Lat_Shift.DefaultValue=0;
			
			swTripS=Fixer;
			swTripS.Name='Use TripleS BG'; 
			
			StartRetroLearn=TimeEntry;
			StartRetroLearn.Name='Start retro. learning period';

			StopRetroLearn=TimeEntry;
			StopRetroLearn.Name='End retro. learning period';
			
			StartRetroTest=TimeEntry;
			StartRetroTest.Name='Start retro. testing period';
			
			StopRetroTest=TimeEntry;
			StopRetroTest.Name='End retro. testing period';
			
			FixTripPar=Fixer;
			FixTripPar.Name='Used Fixed Learning Time';
			
			PossibleParameters =struct(	'new_generic_model',genMod,...
        						'background_rate_file',bgFile,...
        						'min_mag_main',minMagMain,...
        						'days_from_qdm',DayQDM,...
        						'min_aftershock_radium',afterRad,...
        						'grid_bg_anchor',gridAnch,...
        						'grid_cutoff',gridCut,...
        						'fixed_forecast_param',fixParam,...
        						'a_value',aval,...
        						'b_value',bval,...
        						'p_value',pval,...
							'c_value',cval,...
							'Lat_Shift',Lat_Shift,...
							'UseTripleS_BG',swTripS,...
							'StartRetroLearning', StartRetroLearn,...
							'EndRetroLearning' , StopRetroLearn,...
							'StartRetroTesting', StartRetroTest,...
							'EndRetroTesting', StopRetroTest,...
							'FixedTime',FixTripPar);
			%FINISHED
        	
        	end
        	
        	
        	function setInputData(obj,Datastore,Filterlist,StartDate)
			%This method is used by the project calculator to send the input
			%catalog data to the model. Sended will be a Datastore catalog 
			%object and a Filterlist object or boolean vector giving the 
			%selection of the earthquakes (islogical can be used to separate
			%between the two cases).
			%Everything what has to be done to bring the input catalog into 
			%the right format has to be done here. Like conversion into a 
			%file format readable by the model.
			
			import mapseis.projector.getDateNum;
			
			InputTimer=tic;
			
			obj.Datastore = Datastore;
			obj.Filterlist = Filterlist;
			obj.ForecastPeriod = [StartDate, StartDate+obj.RegionConfStruct.TimeInterval]
			     			
             		%LatShift came in a slightly later version check if is field
			if ~isfield('Lat_Shift',obj.ModelConfStruct)
				obj.ModelConfStruct.Lat_Shift=0;
			end		
			%(May be deleted in a later release)		
			
			FilterExist=false;
        		if isempty(obj.Filterlist)
				selected=true(obj.Datastore.getRowCount,1);	
			else
					
				if ~islogical(obj.Filterlist)
					obj.Filterlist.changeData(obj.Datastore);
					obj.Filterlist.updateNoEvent;
					selected=obj.Filterlist.getSelected;
					obj.Filterlist.PackIt;
					FilterExist=true;
				else
					selected=obj.Filterlist;
				end
			end
			
			if FilterExist
				TimeFilt=obj.Filterlist.getByName('Time');
				WhatPer = TimeFilt.getRange;
				
				TimePer=WhatPer{2};
				disp(WhatPer)
				
				if isinf(TimePer(1))
					%use start of catalog
					TimePer(1)=min(getDateNum(obj.Datastore,selected));
					
				end
				
				if isinf(TimePer(2))
					%use end of catalog
					TimePer(2)=max(getDateNum(obj.Datastore,selected));
				end
        		else
        			TimePer(1)=min(getDateNum(obj.Datastore,selected));
        			TimePer(2)=max(getDateNum(obj.Datastore,selected));
        		end
        		
        		obj.LearningPeriod=TimePer;
        		
        		%to be sure
        		obj.ModelReady=all(obj.ModRegReady);
        		
        		%now build the files needed for the calculation
        		obj.generateFiles('csep_config');
        		obj.generateFiles('catalog');
        		
        		obj.CPUTimeData.InputDataTime=toc(InputTimer);
			
			
        	end
        	
        	
        	function UsedData = getInputData(obj,OnlyFilter)
			%This method will be used by the project calculator for archiving
			%the used data. The returned UsedData can have different format 
			%but suggested is zmap or shortcat based catalog or a datastore 
			%catalog (cutted!). If OnlyFilter is set to true, the UsedData 
			%variable should be a boolean vector giving the selected earthquakes
			
			%I go for the ShortCat this time, but it could easily be changed
			%at the moment the  function is more of diagnostic nature anyway
			import mapseis.projector.*;
			
			
			
			
			if isempty(obj.Datastore)|isempty(obj.Filterlist)
				UsedData=[];
			else
				if OnlyFilter
					if ~islogical(obj.Filterlist)
						obj.Filterlist.changeData(obj.Datastore);
						obj.Filterlist.updateNoEvent;
						UsedData=obj.Filterlist.getSelected;
						obj.Filterlist.PackIt;
					else
						UsedData=obj.Filterlist;
					end
				
				else
					if ~islogical(obj.Filterlist)
						obj.Filterlist.changeData(obj.Datastore);
						obj.Filterlist.updateNoEvent;
						selected=obj.Filterlist.getSelected;
						obj.Filterlist.PackIt;
					else
						selected=obj.Filterlist;
					end
        				
        				[UsedData temp]=getShortCatalog(obj.Datastore,selected);
				
				end
			end
        	end
        	
        	
        	
        	function FeatureList = getFeatures(obj)
			%This is used by the project builder to determine which parameters
			%are available for the model. It influences mostly the region 
			%setting, the way data is exchanged and some additional menus
			%The Output should be a structure with specified fieldnames with 
			%mostly boolean values.
			%Following keywords are available:
			%	MagDist:	Should be normally true, means forecasts
			%			for different magnitude bins are available
			%			-> Magnitude bins
			%	DepthDist:	If set to true the model allows forecasts
			%			for different depth bins
			%			-> 3D model
			%	RectGrid:	If true the model should allow different
			%			grid spacing for Longitude and Latitude
			%	TimeYear:	If false the project builder will use
			%			days for time interval value and if true
			%			years will be used.
			%	Parallel:	If true the this GUI allows parallization
			%			and the toggle will be sended by builder
			%	HostMode:	Only for parallel mode, if set to true
			%			builder is allows to use this object in
			%			a parfor loop or similar. If false the
			%			model deals itself the parallization
			%	NoFiles:	If set to true the model does not produce
			%			any files and thus they cannot be archived
			%			The project builder will in this case not 
			%			allow the option to archive files 
			%	LogData:	If true Logdata is available not only in 
			%			file form but also directly imported as
			%			string or structure or whatever.
			%	ForecastType:	This is not boolean value but string
			%			It describes what distribution is 
			%			returning. It can be 'Possion', 'NegBin'
			%			'Custom' and 'Selectable'.
			%			'Selectable' means that the model supports
			%			more than on distribution. 
			
			
			%The project builder does also have a standard feature set in 
			%case some parts of the feature list are not defined.
			%The default is:
			%	MagDist=true;
			%	DepthHist=false;
			%	RectGrid=false;
			%	TimeYear=false;
			%	Parallel=true;
			%	HostMode=false;
			%	NoFiles=false;
			%	LogData=true;
			%	ForecastType='Poisson';
			%Maybe more later
			
			%IMPORTANT: if ForecastType is 'Selectable', a field with
			%the name "ForecastType" has to be added to the Model Configuration
			%describing the actual ForecastType, also a descripting for the 
			%GUI has to be added allowing to change the parameter
			
			%TODO: May have to add stuff to this list
			
			%not so sure about the parallel processing, but I say no for the 
			%moment
			
			FeatureList = struct(	'MagDist',true,...
						'DepthDist',false,...
						'RectGrid',false,...
						'TimeYear',false,...
						'Parallel',false,...
						'HostMode',false,...
						'NoFiles',false,...
						'LogData',false,...
						'AddonData',false,...
						'ForecastType','Poisson');
						
						
        	end
			
			
        	
        	
        	
        	function prepareCalc(obj)
			%This method is called the project builder, and should do everything
			%what is needed to run the forecast later. Often this method is 
			%totally unnecessary as everything can be done be before or after 
			%call of this method. But in some cases it might be a good idea 
			%to separate some cpu heavy preparation from the configuration.
			%If not needed the function can defined as a empty function.
			%IMPORTANT: No catalog preparation should be done here, as the
			%catalog data is sended by the project calculator. This function
			%is the last function before a actual calculation run (called in
			%the startCalc method of the calculator.
			
			import mapseis.forecast.ModelPlugIns.TripleSPlugin_mk2;
			import mapseis.forecast.generateGrid;
			import mapseis.util.PathMaker; 
			
			if isempty(obj.Datastore)|isempty(obj.Filterlist)
				warning('Input data is needed preparing the model')
				return;
			end
			
			fs=filesep;
			
			PrepTimer=tic;
									
			%IMPORTANT: the background rate grid and the actuall calculation
			%grid have to fit grid-node-wise (grid-anchor can be used to shift bg
			% grid if needed), so that for every grid node in the calculation a 
			%bg rate is available. The BG grid can however bigger than the calculation
			%grid
			
			%generate grid based on boundary box
			%...................................
			
			BoundBox=obj.RegionConfStruct.RegionBoundingBox;
			AngleShare=obj.RegionConfStruct.LonSpacing*2;	
			
			%remember: BoundBox=[minLon maxLon minLat maxLat]
			obj.AddGridParam=BoundBox+[-AngleShare,AngleShare,-AngleShare,AngleShare];
			
			
			%generate grid selection vector
			if obj.ModelConfStruct.UseTripleS_BG
				%generate a totally new grid
				LonRange=obj.AddGridParam(1:2);
				LatRange=obj.AddGridParam(3:4);
				Spacer=[obj.RegionConfStruct.LonSpacing,obj.RegionConfStruct.LonSpacing];
				
				MagRange=[obj.RegionConfStruct.MinMagnitude,obj.RegionConfStruct.MaxMagnitude];
				if ~isnan(obj.RegionConfStruct.MagnitudeBin)
					MagRange(3)=obj.RegionConfStruct.MagnitudeBin;
				end
				
				DepthRange=[obj.RegionConfStruct.MinDepth,obj.RegionConfStruct.MaxDepth];
				if ~isnan(obj.RegionConfStruct.DepthBin)
					DepthRange(3)=obj.RegionConfStruct.DepthBin;
				end
				
				GridPrec=obj.RegionConfStruct.GridPrecision;
				if isempty(GridPrec)
					GridPrec=10^(-5);
				end
				
				[TheGrid TripleGrid isInIt] = generateGrid(LonRange,LatRange,Spacer,MagRange,DepthRange,[],GridPrec);
											
				obj.SpecialGrid{1} = TripleGrid;
				obj.SpecialGrid{2} = TheGrid;
				
				
			else
				%load an adopt this grid
				CheckExist=exist(obj.ModelConfStruct.background_rate_file);
        		
				if CheckExist==0
					error('Backgroudn rate file not found');
					return
				end
        		
				FilePath=obj.ModelConfStruct.background_rate_file;
				TheBGData=load(FilePath);
				CorrectedData=TheBGData;
				%This has to be checked if it is shifted into the right direction later
				CorrectedData(:,2:3)=CorrectedData(:,2:3)-obj.ModelConfStruct.grid_bg_anchor;
				
				%check if the grid is suitable.
				BigEnough=any(CorrectedData(:,2)<=obj.AddGridParam(1))&...
					  any(CorrectedData(:,2)>=obj.AddGridParam(2))&...
					  any(CorrectedData(:,3)<=obj.AddGridParam(3))&...
					  any(CorrectedData(:,3)>=obj.AddGridParam(4));
				isNode=any(CorrectedData(:,2)==obj.AddGridParam(1)&...
					 CorrectedData(:,3)==obj.AddGridParam(3));
				
				%seperate error for better error tracing
				if ~BigEnough
					warning('Background rate grid to small');
					return;
				end
				
				if ~isNode
					warning('Background rate grid does not fit');
					return;
				end
					  
				obj.SpecialGrid{1}=CorrectedData(:,2:3);
				
				%save new corrected grid file 
				NewFileName=[obj.DataPath,filesep,'BackGroundRates.dat'];
				save(NewFileName,'CorrectedData','-ASCII');
				obj.PrepareFiles{end+1}=NewFileName;
				
				
			end
				
			
			
			%if the TripleS model is used as BG, use the model now
			if obj.ModelConfStruct.UseTripleS_BG
				%use TripleS
				TripMod=TripleSPlugin_mk2('BGMod');
				
				%config the BG model
				%...................
				
				%model is easy, just send the same data, it contains
				%the fields needed
				obj.ModelConfStruct.ForecastType='Poisson';
				TripMod.ConfigureModel(obj.ModelConfStruct);
				
				%make a custom tempfolder for TripleS
				newTempFold=[obj.TempFolder,filesep,'StepBG'];
						
				
				%build framework parameters
				TripFrame=struct( 'TemporaryFolder', newTempFold,...
						  'ModelConfigFile', obj.TripConfig);
				
				TripMod.ConfigureFramework(TripFrame);
				
				%Special RegConfig for TripleS with the rectangular grid
				TripReg=obj.RegionConfStruct;
				TripPoly=[	obj.AddGridParam(1),obj.AddGridParam(3);...
						obj.AddGridParam(2),obj.AddGridParam(3);...
						obj.AddGridParam(2),obj.AddGridParam(4);...
						obj.AddGridParam(1),obj.AddGridParam(4);...
						obj.AddGridParam(1),obj.AddGridParam(3)];
				
				TripReg.RegionPoly=TripPoly;
				TripReg.RegionBoundingBox=obj.AddGridParam;
				TripReg.Nodes=obj.SpecialGrid{1};
				TripReg.RelmGrid=obj.SpecialGrid{2};
				
				
				
				TripMod.ConfigureTestRegion(TripReg);
				
				%send data
				TripMod.setInputData(obj.Datastore,obj.Filterlist,obj.ForecastPeriod(1));
				
				%prepare TripleS
				TripMod.prepareCalc;
				
				%calc TripleS
				ErrorCodeSSS=TripMod.calcForecast;
				
				if ErrorCodeSSS~=0
					warning(['Could not calculate BG rate: TripleS error ',num2str(ErrorCodeSSS) ]);
					return
				end
				
				
				%get Result
				[ForeCast, DataMask] = TripMod.getForecast(false);
				obj.BGrates=ForeCast;
				
				%generate BGfile
				obj.generateBgRates;
				
			end
			
			%Done
			
			%generate not changing input files
			obj.generateFiles('config');
			
			obj.CPUTimeData.PrepareTime=toc(PrepTimer);
			
        	end
        	
        	
        	
        	function ErrorCode = calcForecast(obj)
			%Names says it. If this method is called the calculation of the 
			%forecast and according the configuration and catalog data should
			%started and the result should be saved in this object as well as
			%the path to the result file in case any is existing.
			%The ErrorCode can be used to signalize errors, it is just an 
			%integer. 0 means everthing worked fine, 1 means model is not
			%configurated properly, 2 means catalog data is missing, 3 means 
			%something with a external function did not work out and -1
			%means a generic not specified error. Every other value can 
			%used to specify special model specific errors. It is of course
			%sensible to explain the error code somewhere.
			%The error can be specified in the function getErrorList(obj), but 
			%it is as often optional, a generic function is integrated anyway.
			%Currently the function is not directly used, but can change very
			%soon
			
			%HERE
			
			ErrorCode=-1;
			
			%check if everything is set
			if ~obj.ModelReady
				%here it really should be an error, because
				%that can be changed easily 
				error('Model is not configurated')
			end
			
						
			%check if files are existing
			Checker=true
			fs=filesep;
			
			        		
        		%config
        		CheckExist=exist([obj.ExternalPath,fs,'config',fs,'defaults.properties']);
        		Checker=Checker&CheckExist~=0
        		
        		%csep file
        		CheckExist=exist([obj.StepConfigPath,fs,'CSEP_params.txt']);
        		Checker=Checker&CheckExist~=0
        		
        		%catalog
        		CheckExist=exist([obj.DataPath,fs,'InputCatalog.dat']);
        		Checker=Checker&CheckExist~=0
			
        		if ~Checker
        			warning('File not found. Model not prepared/ no data sended');
        			return
        		end
        		
        		
        		
			%call the java external
			%example command
			%java -jar ./lib/step-aftershock.jar 0
			
			%NEEDED for MOUNTAIN LION:
			%export JAVA_HOME=`/usr/libexec/java_home -d64 -v 1.6*`
			%in the command, this will set java to version 1.6 the newest
			%version (currently) 1.7 does not work with step
			
			
			currentFold=pwd;
			cd(obj.ExternalPath);
			%cd('..');
			
			CalcTimer=tic;
			
			if ismac
				ExCommand='export JAVA_HOME=`/usr/libexec/java_home -d64 -v 1.6*`';
				%MCommand=['java -Xmx256m -jar ', obj.ExtModel,' 0'];
				%MCommand=['java -Xmx1024m -jar ', obj.ExtModel,' 0'];
				MCommand=[obj.JavaPath,' -Xmx2048m -jar ', obj.ExtModel,' 0'];
				
				if strcmp(obj.JavaPath,'java');
					FullCommand=[ExCommand,';',MCommand];
				else
					FullCommand=MCommand;
				end
				
				[stat, res] = unix(FullCommand,'-echo');
				obj.LogData=res;
				
			elseif isunix	
				UCommand=[obj.JavaPath,' -Xmx2048m -jar ', obj.ExtModel,' 0'];
				[stat, res] = unix(UCommand,'-echo');
				obj.LogData=res;
			
			
			else
				DCommand=[obj.JavaPath,' -Xmx2048m -jar ', obj.ExtModel,' 0'];
				%The dos/windows command is so far not tested, as I have no
				%windows system available at the moment
				
				[stat, res] = dos(DCommand,'-echo');
				obj.LogData=res;
				
			end
			obj.CPUTimeData.CalcTime=toc(CalcTimer);
			
			cd(currentFold);
			
			
			
			%check if no error was present
			if stat~=0
				ErrorCode=3;
				warning('There was an error while executing the java external');
				return
			end
			
			%finished
			ErrorCode=0;
			
			
        	end
        	
        	
        	function [ForeCast, DataMask] = getForecast(obj,WithGrid)
			%This should return the calculated forecast. The Format which should
			%be returned is depended on the ForecastType and on the input 
			%parameter WithGrid. For 'Poisson' and 'NegBin' the output should be
			%vector (Poisson) and a "twin" vector (NegBin) and if WithGrid is
			%set to true a full Relm Grid forecast should be outputed. In the
			%'Custom' mode the output will always be a cell, without WithGrid
			%the output should be a cell vector containing the single grid cells
			%discrete distribution, else it should be a cell with two elements
			%first one containing the Relmgrid, second one containing the 
			%forecast like mentioned before.
			%The DataMask is the last column in a typical Relm based forecast
			%and marks if a cell has been used/defined (1) or not (0). It can 
			%be left empty, in this case it is assumed that all nodes are used.
			%In case of a full WithGrid output it DataMask will be ignored and
			%the grid will be used instead
			
			
			fs=filesep;
			
			%check if file is existing
			CheckExist=exist([obj.ResultPath,fs,'STEP_Forecast.txt']);
        		
        		if CheckExist==0
        			error('Output file not found');
        		end
        		
        		ImportTimer=tic;
        		
        		%load str and correct for , instead of .
        		%(maybe a java language packet problem, but I don't know)
        		FileName=[obj.ResultPath,fs,'STEP_Forecast.txt'];
        		fid = fopen(FileName);
        			inStr = fscanf(fid,'%c'); % Read the entire text file into an array
        		fclose(fid);
        		
        		inStr(inStr==',')='.';
        		
        		%write back
        		fid = fopen(FileName,'w');
        		fprintf(fid,'%s',inStr); 
        		fclose(fid);
        		
        		        		
        		%Load the corrected data
			DataPack=load(FileName);
        		        
			if obj.ModelConfStruct.Lat_Shift~=0
				%shift the Grid
				DataPack(:,3:4)=DataPack(:,3:4)+obj.ModelConfStruct.Lat_Shift;
							
			end
			
			
			%Now check what is in and only use that
			ThePos=[(DataPack(:,1)+DataPack(:,2))/2 , (DataPack(:,3)+DataPack(:,4))/2];
			ThePoly=obj.RegionConfStruct.RegionPoly;
			Selector=inpolygon(ThePos(:,1),ThePos(:,2),ThePoly(:,1),ThePoly(:,2));	
			
			%hopefully it works (means the number of cells is still the same)
			RelmGrid=DataPack(Selector,:);
			
			if isempty(obj.ShiftVector)
				%A Shift vector is needed
				obj.BuildShiftVector(RelmGrid);
			end
			
			NewRelmGrid=RelmGrid(obj.ShiftVector,:);
			
			%decide what has to be sended
			if WithGrid
				ForeCast=NewRelmGrid;
				DataMask=NewRelmGrid(:,10);
			else
				ForeCast=NewRelmGrid(:,9);
				DataMask=NewRelmGrid(:,10);
			end
			
			obj.CPUTimeData.ImportTime=toc(ImportTimer);
			
		end
         	
         	
        	
        	function FilePath = getForecastFile(obj)
			%This method will only be called if NoFiles is false and if the 
			%option to archive the forecast files is switched on the project
			%object. 
			%It should return the path to the forecast file as a string. The 
			%project calculater will then copy the file to a specific folder
			%save the link to this file. If there is more then one 
			%result file existing a Cell array with multiple paths (strings)
			%can be used instead of a normal single path
			CheckExist=exist([obj.ResultPath,filesep,'TripleS_Forecast_Result.xml']);
        		
        		if CheckExist==0
        			error('Output file not found');
        		end
        		
        		FilePath=[obj.ResultPath,fs,'STEP_Forecast.txt'];
        		
			
		end
		
		

		function LogData = getCalcLog(obj)
			%This will be called if saving logs is activated and LogData is 
			%set to true in the FeatureList. The method should return a log 
			%of the calculation in a matlab common format and it should NOT
			%be a file link, because the log file will not be copied.
			%the easiest way to create the LogData is to just read the file 
			%into a string and return the string.
			
			%QUESTION: does tripleS have any log data?
			%Nope, but the console output can be delivered
			LogData=obj.LogData;
			
			
			
		end
		
		
		function LogFile = getCalcLogFile(obj)
			%The project calculator will only use this method if NoFiles is
			%set to false in the FeatureList and if the option to store log
			%files is activated in the project. This method should return a
			%file path(s) to the log file(s), the project builder will copy 
			%the files and store the new path. If there is more then one 
			%logfiles existing a Cell array with multiple paths (strings)
			%can be used instead of a normal single path
			
			%QUESTION: does tripleS have any log data?
			%--> No log files
			LogFile=[];
			
			
			
		end
        	
		
		function AddonData = getAddonData(obj)
			%This will be called if saving AddonData is activated and AddonData is 
			%set to true in the FeatureList. The method can return anything over 
			%this function, but the model has also to keep track itself in case
			%it is timevariing data
		
		
			AddonData=[];
        		warning('Model does not have additional data');
		
		end
		
		
        	function ResetLastCalc(obj)
			%This method should revert the plugin to the state before the 
			%calculation of the forecast, so that new data can be sent and
			%a new forecast can be calculated. This may involve things like 
			%empty the catalog storage and and deleting some input and output
			%files. But it might as well be just an empty function if no clean
			%up is needed. 
			%IMPORTANT: this method should revert the plugin to the state AFTER
			%prepareCalc was called.
			
			%means removing data and files, but not reset the OrgStartTime
			
			%delete files from list
			for i=1:numel(obj.CalcFiles)
				try
					if isunix
						UCommand=['rm ', obj.CalcFiles{i}];
						[stat, res] = unix(Ucommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					else
						DCommand=['del ', obj.CalcFiles{i}];
						[stat, res] = dos(Dcommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					end
					
				catch
					warning('File did not exist')
				end
			end
			
			obj.CalcFiles={};
			
			%reset some of the properties
			obj.ForecastPeriod=[];
        		obj.LearningPeriod=[];
        		obj.Datastore=[];
        		obj.Filterlist=[];
			obj.LogData=[];
        	end
        	
        	
        	
        	function ResetFullCalc(obj)
			%Similar to ResetLastCalc but this one should revert the plugin to
			%state BEFORE prepareCalc. The function can be equal to ResetLastCalc
			%but it should not cause an error because double execution of a command
			%(e.g. delete of a file)
			
			%reset everything apart config, so first call ResetLastCalc
			
			obj.ResetLastCalc;
			
			
			
			%delete files
			for i=1:numel(obj.PrepareFiles)
			
				try
					if isunix
						UCommand=['rm ', obj.PrepareFiles{i}];
						[stat, res] = unix(Ucommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					else
						DCommand=['del ', obj.PrepareFiles{i}];
						[stat, res] = dos(Dcommand);
						
						if stat~=0
							warning('Could not delete file');
						end
					end
				catch
					warning('File did not exist')
				end
			end
			
			obj.PrepareFiles={};
			
			%reset some of the properties
			%reset startTime
			obj.OrgStartTime=[];
			obj.ShiftVector=[];
			obj.CPUTimeData=[];
			
		end
		
		
		%CUSTOM FUNCTIONS
		
		function generateFiles(obj,FileType)
			%generates the needed input files
			import mapseis.exportfilter.*;
			import mapseis.util.PropertiesIO;
			import mapseis.util.PathMaker; 
			
			switch FileType
				case 'catalog'
					%DONE
					%output should be Zmap catalog
					if isempty(obj.Datastore)
						warning('No Datastore available, could not generate catalog file')
						return
					end
					
					if isempty(obj.Filterlist)
						selected=true(obj.Datastore.getRowCount,1);	
					else
					
						if ~islogical(obj.Filterlist)
							obj.Filterlist.changeData(obj.Datastore);
							obj.Filterlist.updateNoEvent;
							selected=obj.Filterlist.getSelected;
							obj.Filterlist.PackIt;
						else
							selected=obj.Filterlist;
						end
					end
					
					FileName=[obj.DataPath,filesep,'InputCatalog.dat'];
					
					try
						exportCSEPCatalog(obj.Datastore,selected,FileName,'-file');
						
						%Not the most elegant version but it works 
						if obj.ModelConfStruct.Lat_Shift~=0
							%shift the data
							CatMan=load(FileName);
							CatMan(:,2)=CatMan(:,2)-obj.ModelConfStruct.Lat_Shift;
							save(FileName,'CatMan','-ASCII');
						
						end
					
						
					catch
						error('could not write catalog file')
					end
					
					%add file to the list to delete
					obj.CalcFiles{end+1} = FileName;
					
					
				case 'csep_config'
					%DONE
					%always call this before a calc step
					
					fs=filesep;
					%csep param file
					CSEPConfName=[obj.StepConfigPath,fs,'CSEP_params.txt']
					
					%Parameters for the file
					learningStartCSEP=obj.convertTimeCSEP(obj.LearningPeriod(1));
					forecastStartCSEP=obj.convertTimeCSEP(obj.ForecastPeriod(1));
					learningPeriod=num2str(obj.RegionConfStruct.TimeInterval);
					CatFile=[obj.DataPath,filesep,'InputCatalog.dat'];
					OutputFile=[obj.ResultPath,fs,'STEP_Forecast.txt'];
					BGFile=[obj.DataPath,filesep,'BackGroundRates.dat'];
					
					%example file
					%1  1 1992 0 0 0
					%1  8 1992 0 0 0
					%1
					%data/csep/example_CSEP_cat.txt
					%output/csep/STEP_daily.txt
					%data/csep/CSEP_CA_Background

					%write file
					try
						fid = fopen(CSEPConfName,'w');
					catch
						warning('File could not be opened')
						return
					end
					
					%learning Start
					fprintf(fid,'%s',learningStartCSEP); 
					fprintf(fid,'\n');
					
					%forecast Start
					fprintf(fid,'%s',forecastStartCSEP); 
					fprintf(fid,'\n');
					
					%forecast Length
					fprintf(fid,'%s',learningPeriod); 
					fprintf(fid,'\n');
					
					%Catalog File
					fprintf(fid,'%s',CatFile); 
					fprintf(fid,'\n');
					
					%Output File
					fprintf(fid,'%s',OutputFile); 
					fprintf(fid,'\n');
					
					%BG file
					fprintf(fid,'%s',BGFile); 
					fprintf(fid,'\n');
					
					fclose(fid);
					
					obj.CalcFiles{end+1} = OutputFile;
					obj.CalcFiles{end+1} = CSEPConfName;
					
				case 'config'
					%DONE
					%config file	
					%only needed to call once in preparation
					
					%LATER: New PARAMTER
					%bg.min.depth 
					%bg.max.depth 
					
					disp('write main config file')
					
					%Unfinished
					if ~obj.ModelReady
						error('Model not fully configurated')
					end	
					
					%The fixed parameters
					save_models=num2str(1);
					model_format=num2str(1);
					param_file_option=num2str(0);
					UndefinedFile='nope';
					StepObj='STEP_AftershockObj';
					
					start_current=num2str(0);
					
					quakesource=num2str(0);
					quakeurl='nope';
					default_quaketime='{1}{1}{5,6,7,8}';
					default_readback=num2str(1);
					
					if isempty(obj.RegionConfStruct.GridPrecision)
						grid_prec=num2str(10^(-5));
					else
						grid_prec=num2str(obj.RegionConfStruct.GridPrecision);
					end
					
					if isnan(obj.RegionConfStruct.MagnitudeBin)
						MagBin=num2str(obj.RegionConfStruct.MaxMagnitude-obj.RegionConfStruct.MinMagnitude,'%1.2f');
						CorrMag=(obj.RegionConfStruct.MaxMagnitude-obj.RegionConfStruct.MinMagnitude)/2;
					else
						MagBin=num2str(obj.RegionConfStruct.MagnitudeBin,'%1.2f');
						CorrMag=obj.RegionConfStruct.MagnitudeBin/2;
					end
						
					%the won't be used and are overwritten by csep param file
					eventTimeConf='0'
					startTimeConf='0'
					
					
					fs=filesep;
														
					TheProps=PropertiesIO;
					
					%Step is not very flexible concering name and position of the main
					%config file, it has to be in the file path ./config/default.properties
					
					ModConfigFileName=[obj.ExternalPath,fs,'config',fs,'defaults.properties'];
					TheProps.setFile(ModConfigFileName);
					
					%csep param file
					CSEPConfName=[obj.StepConfigPath,fs,'CSEP_params.txt']
					
						
					%The Paths first
					DataPath = obj.DataPath;
					OutputPath =  obj.ResultPath;
					
					%Latitude Stuff
					if obj.ModelConfStruct.Lat_Shift~=0
						%shift the Grid
						LatMin=obj.AddGridParam(3)-obj.ModelConfStruct.Lat_Shift;
						LatMax=obj.AddGridParam(4)-obj.ModelConfStruct.Lat_Shift;
					
					else
						LatMin=obj.AddGridParam(3);
						LatMax=obj.AddGridParam(4);
						
					end
					
					TheProps.setProperty('generic.model.type',num2str(obj.ModelConfStruct.new_generic_model));
					TheProps.setProperty('save.models',save_models);
					TheProps.setProperty('model.format',model_format);
					TheProps.setProperty('params.file.option',param_file_option);
					
					TheProps.setProperty('model.params.file',CSEPConfName);
					TheProps.setProperty('data.dir',DataPath);
					TheProps.setProperty('input.file.cube',UndefinedFile);
					TheProps.setProperty('input.file.bg.haz',UndefinedFile);
					TheProps.setProperty('input.file.bg.rates',UndefinedFile);
					
					TheProps.setProperty('output.dir',OutputPath);
					TheProps.setProperty('output.file.step.prob',UndefinedFile);
					TheProps.setProperty('output.file.time.dep.rates',UndefinedFile);
					TheProps.setProperty('output.file.step.rates',UndefinedFile);
					TheProps.setProperty('output.file.haz.curv.prob',UndefinedFile);
					TheProps.setProperty('output.file.step.aftershock.obj',StepObj);
					
					TheProps.setProperty('min.mag.main',num2str(obj.ModelConfStruct.min_mag_main,'%1.2f'));
					TheProps.setProperty('min.mag.forcast',num2str(obj.RegionConfStruct.MinMagnitude+CorrMag,'%1.2f'));
					TheProps.setProperty('max.mag.forcast',num2str(obj.RegionConfStruct.MaxMagnitude-CorrMag,'%1.2f'));
					TheProps.setProperty('delta.mag.forcast',MagBin);
					TheProps.setProperty('forecast.len.days',num2str(obj.RegionConfStruct.TimeInterval));
					
					TheProps.setProperty('start.forecast.current',start_current);
					TheProps.setProperty('forecast.start.time',startTimeConf);
					TheProps.setProperty('event.start.time',eventTimeConf);
					TheProps.setProperty('days.from.qdm',num2str(obj.ModelConfStruct.days_from_qdm));
					TheProps.setProperty('min.aftershock.radium',num2str(obj.ModelConfStruct.min_aftershock_radium));
					
					TheProps.setProperty('bg.min.lat',num2str(LatMin));
					TheProps.setProperty('bg.max.lat',num2str(LatMax));
					TheProps.setProperty('bg.min.lon',num2str(obj.AddGridParam(1)));
					TheProps.setProperty('bg.max.lon',num2str(obj.AddGridParam(2)));
					TheProps.setProperty('bg.min.depth ',num2str(obj.RegionConfStruct.MinDepth));
					TheProps.setProperty('bg.max.depth ',num2str(obj.RegionConfStruct.MaxDepth));
										
					TheProps.setProperty('grid.spacing',num2str(obj.RegionConfStruct.LonSpacing));
					TheProps.setProperty('grid.precision',grid_prec);
					TheProps.setProperty('grid.anchor',num2str(0));
					TheProps.setProperty('grid.cutoff',num2str(obj.ModelConfStruct.grid_cutoff));
					TheProps.setProperty('forecast.param.fixed',num2str(obj.ModelConfStruct.fixed_forecast_param));
					
					TheProps.setProperty('a.value',num2str(obj.ModelConfStruct.a_value));
					TheProps.setProperty('b.value',num2str(obj.ModelConfStruct.b_value));
					TheProps.setProperty('p.value',num2str(obj.ModelConfStruct.p_value));
					TheProps.setProperty('c.value',num2str(obj.ModelConfStruct.c_value));
										
					TheProps.setProperty('quake.datasource',quakesource);
					TheProps.setProperty('geonet.quake.url',quakeurl);
					TheProps.setProperty('default.test.times',default_quaketime);
					TheProps.setProperty('default.test.read.back',default_readback);
					
					%add files to the list (at least the none AFVS files)
					obj.PrepareFiles{end+1} = [obj.ResultPath,fs,'STEP_AftershockObj'];
					obj.PrepareFiles{end+1} = ModConfigFileName;
														
					%It is complete, lets write it 
					TheProps.writeFile;
					
					
				case 'grid'
					%Not needed for STEP, it generates it one grid, which
					%will lead to some complicated import functions I suppose
					
				
					%The grid file 
					
					
					if isempty(obj.RegionConfStruct)
						warning('No Grid defined, could not generate file')
						return
					end
					
					TheGrid=obj.RegionConfStruct.Nodes;
					
					FileName=[obj.GridPath,filesep,'triples.forecast.nodes.dat'];
					
					try
						dlmwrite(FileName, TheGrid, 'delimiter', '\t','precision', 6);
					catch
						error('could not write grid file')
					end
					
					%add file to prepare delete list
					obj.PrepareFiles{end+1}=FileName;
			end
			
		end
        	
		
		
		function initFolders(obj)
			
			import mapseis.util.PropertiesIO;
			import mapseis.util.PathMaker; 
			
			%CORRECT COMMENT
			%reads the paths from the FrameConfig files
			%and generates temporary folders if needed.
			
			%The files/paths should use the following keywords followed by a
			% '=' sign. (e.g. TripleS.MainFolder=./mapseis/Forecast_Model 
			%1:ParentFolder TripleS: 'TripleS.MainFolder' 
			%  (normally in mapseis/Forecast_Model folder)
			%2: Path to executable: TripleS.Java (TripleS.jar)
			%3: Path of the catalog folder TripleS.CatFolder
			%4: Path of the folder for grids and configs TripleS.ConfigFolder
        		%5: Path of the folder for the AVFS files TripleS.AVFS
        		%6: Path of the Result Folder TripleS.Results
        		
        		%3 to 6 are best left empty, like this the default folders in the
        		%temporary folders will be used.
        		
        		%Pathmaker is used to generate absolute paths if possible.
        		
        		fs=filesep;
        		
        		TheProps=PropertiesIO(obj.ConfigFile);
        		
        		%MainFolder and executable path can of course not be created
        		obj.ExternalPath=PathMaker(TheProps.getProperty('Step.MainFolder'));
        		obj.ExtModel=PathMaker(TheProps.getProperty('Step.Java'));
        		obj.TripConfig=PathMaker(TheProps.getProperty('TripleS.Config'));
        		        		        		
        		%the rest is optional.
        		CatPath=TheProps.getProperty('Step.DataFolder');
        		if isempty(CatPath)
        			CatPath=[obj.TempFolder,fs,'Step',fs,'data'];
        		end
        		
        		ConfigPath=TheProps.getProperty('Step.ConfigFolder');
        		if isempty(ConfigPath)
        			ConfigPath=[obj.TempFolder,fs,'Step',fs,'config'];
        		end
        		
        		LogPath=TheProps.getProperty('Step.LogFolder');
        		if isempty(LogPath)
        			ResPath=[obj.TempFolder,fs,'Step',fs,'log'];
        		end
        		        		
        		ResPath=TheProps.getProperty('Step.OutputFolder');
        		if isempty(ResPath)
        			ResPath=[obj.TempFolder,fs,'Step',fs,'output'];
        		end
        		
        		%generate folders if not existing
        		ToGen={CatPath,ConfigPath,LogPath,ResPath};
        		ToName={'Data','Config','Log','Output'};
        		
        		for i=1:numel(ToGen)
				CheckExist=exist(ToGen{i});
				
				if CheckExist==0
					if isunix
						Ucommand=['mkdir -p ',ToGen{i}];
						[stat, res] = unix(Ucommand);
						
						if stat==0
							disp([ToName{i}, ' folder generated']);
						else
							warning(['Could not generate ', ToName{i}, ' folder']);
						
						end
					else
						Dcommand=['mkdir -p ',ToGen{i}];
						[stat, res] = dos(Dcommand);
						
						if stat==0
							disp([ToName{i}, ' folder generated']);
						else
							warning(['Could not generate ', ToName{i}, ' folder']);
						
						end
					end
					
				end
        		end
        		
        		
        		
        		
        		obj.DataPath=PathMaker(CatPath);
        		obj.StepConfigPath=PathMaker(ConfigPath);
        		obj.LogPath=PathMaker(LogPath);
        		obj.ResultPath=PathMaker(ResPath);
        		
			
			
		end
        	
		
		function generateBgRates(obj)
			%This method generates the BG rates file out of a TripleS forecast
			TheRates=obj.BGrates;
			TripGrid=obj.SpecialGrid{1};
			FullGrid=obj.SpecialGrid{2};
			MagBins=obj.RegionConfStruct.MagVector;
			
			%empty grid
			BGGrid=zeros(numel(TripGrid(:,1)),3+numel(MagBins));
						
			%RatePart=reshape(TheRates,numel(TripGrid(:,1)),numel(MagBins)); %->was wrong
			RatePart=reshape(TheRates,numel(MagBins),numel(TripGrid(:,1)))';
			%file in data
			BGGrid(:,2:3)=TripGrid;
			BGGrid(:,4:end)=RatePart;
			
			%Latitude Stuff
			if obj.ModelConfStruct.Lat_Shift~=0
				%shift the Grid
				BGGrid(:,3)=BGGrid(:,3)-obj.ModelConfStruct.Lat_Shift;
				
			
			end
			
			%save (dlmwrite is not necessarily needed)
			NewFileName=[obj.DataPath,filesep,'BackGroundRates.dat'];
			save(NewFileName,'BGGrid','-ASCII');
			obj.PrepareFiles{end+1}=NewFileName;
			
		end
		
		
		function BuildShiftVector(obj,RelmGrid)
			%builds a vector for sorting the result grid into the 
			%correct order
			%There maybe a faster way to do this, but this one is the
			%savest, it compares grids cells and does not assume 
			%something
			
			ShiftVector=[];
			
			%normally grid precision may be to coarse
			GridPrecision=10^(-10);
			
			orgGrid=obj.RegionConfStruct.RelmGrid;
			
			%Round Grid nodes, needed, because there is some jitter in the
			%position around the order of 10^-13
			orgGrid(:,1:4)=round(orgGrid(:,1:4)*1/GridPrecision)*GridPrecision;
			RelmGrid(:,1:4)=round(RelmGrid(:,1:4)*1/GridPrecision)*GridPrecision;
			
			%for i=1:numel(RelmGrid(:,1));
			%	ShiftVector(i)=find(RelmGrid(i,1)==orgGrid(:,1)&...
			%			RelmGrid(i,3)==orgGrid(:,3)&...
			%			RelmGrid(i,5)==orgGrid(:,5)&...
			%			RelmGrid(i,7)==orgGrid(:,7));
				
			%end
			
			for i=1:numel(orgGrid(:,1));
				ShiftVector(i)=find(RelmGrid(:,1)==orgGrid(i,1)&...
						RelmGrid(:,3)==orgGrid(i,3)&...
						RelmGrid(:,5)==orgGrid(i,5)&...
						RelmGrid(:,7)==orgGrid(i,7));
				
			end
			
			
			obj.ShiftVector=ShiftVector;
		
		end
		
		
		function TripTime = convertTimeCSEP(obj,inTime)
        		%converts a datenum into a date strings used by the 
        		%TripleS model and vice versa. It would not need to be 
        		%internal method the TripleS plugin, but it is not needed 
        		%outside, and like this it is easier to call inside the object.
        		%inTime: datenum format e.g 7.3516e+05
        		%TripTime: time string of the format: '2012/10/16 16:30:58'
        		
        		%actually it is only on line, but still handy
        		if isnumeric(inTime)
        			TripTime = datestr(inTime,'dd mm yyyy HH MM SS');
        		else
        			TripTime = datenum(inTime,'dd mm yyyy HH MM SS');
        		end
        		
		end
        	
		
		function TripTime = convertTime(obj,inTime)
        		%converts a datenum into a date strings used by the 
        		%TripleS model and vice versa. It would not need to be 
        		%internal method the TripleS plugin, but it is not needed 
        		%outside, and like this it is easier to call inside the object.
        		%inTime: datenum format e.g 7.3516e+05
        		%TripTime: time string of the format: '2012/10/16 16:30:58'
        		
        		%actually it is only on line, but still handy
        		if isnumeric(inTime)
        			TripTime = datestr(inTime,'yyyy-mm-ddTHH:MM:SS');
        		else
        			TripTime = datenum(inTime,'yyyy-mm-ddTHH:MM:SS');
        		end
        		
		end
		
		
		
		function TripTime = convertTimeSSS(obj,inTime)
        		%converts a datenum into a date strings used by the 
        		%TripleS model and vice versa. It would not need to be 
        		%internal method the TripleS plugin, but it is not needed 
        		%outside, and like this it is easier to call inside the object.
        		%inTime: datenum format e.g 7.3516e+05
        		%TripTime: time string of the format: '2012/10/16 16:30:58'
        		
        		%actually it is only on line, but still handy
        		if isnumeric(inTime)
        			TripTime = datestr(inTime,'yyyy/mm/dd HH:MM:SS');
        		else
        			TripTime = datenum(inTime,'yyyy/mm/dd HH:MM:SS');
        		end
        		
		end
		
        end
        
        
       
        
        
end        

function PossibleParameters = getParameterSetting(obj)
	%The method should return a description of the parameters needed
	%for the model and should also include information about the way
	%the gui for setting the parameters should be designed.
	%The output has to be a structure with the field names being the 
	%name of the parameters which should be inputed by ConfigureModel
	%Each entry in structure should be another structure with following
	%keywords:
	%	Type: 		can be 	'check' for boolean,
	%				'edit' for any value or small text
	%				'list' for a selection of values specified
	%					by Items
	%				'text' for longer string boxes (e.g. Comments)
	%
	%	Format:		Defines the data type of value only applyable with
	%			'edit'. It can be the following: 'string','float',
	%			'integer','file','dir'	
	%	ValLimit: 	The limit a value can have in a edit box, if left
	%			empty it will set to [-9999 9999].
	%	Name:		Sets the Name used for the value in the GUI, if 
	%			left empty the variable name will be used instead
	%	Items:		Only needed in case of 'list', there it has to be 
	%			cell array with strings. The value returned will
	%			be the an integer giving the position of the 
	%			selected string in the cell array. 
	%	UserDataSelect: This can be used instead of 'Items' in the 'list'
	%			entry type. It it allows to select previous, in
	%			the Datastore catalog stored, calculations like 
	%			for instance b-value, stress calculations and so 
	%			on. The GUI will in this case automatically generate
	%			the Items list, only thing needed is the field name 
	%			under which the data is stored in the datastore 
	%			userdata area (e.g. 'new-bval-calc' for b-value maps)
	%			the command Datastore.getUserDataKeys can be used
	%			to get the currently saved variables. The result
	%			sended back will in this case be the name of the 
	%			calculation (store in in the first column of the 
	%			result cell array). 
	%	DefaultValue:	Sets the value used as default value in the GUI.
	%			If missing, 0 will be used as default or 1 for 
	%			lists
	%	Style:		Only applyable for the Type 'list', it sets the 
	%			GUI element used to represent the list and it can
	%			be:	
	%				'listbox': a scrollable list for the selection
	%				'popupmenu': a popup list 
	%				'radiobutton': a block of checkerboxes
	%				'togglebutton': a selection of buttons
	%			If missing 'popupmenu' will be used. The last
	%			two options are only suggest if not many elements
	%			are in the list.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard	


	
	TimeEntry = struct(	'Type', 'edit',...
						'Format', 'string',...
						'ValLimit',[[1 1]],...
						'Name', 'Generic Time Entry',...
						'DefaultValue','yyyy-mm-dd hh:mm:ss');
	StartRetroLearn=TimeEntry;
	StartRetroLearn.Name='Start retro. learning period';
	
	StopRetroLearn=TimeEntry;
	StopRetroLearn.Name='End retro. learning period';
	
	StartRetroTest=TimeEntry;
	StartRetroTest.Name='Start retro. testing period';
	
	StopRetroTest=TimeEntry;
	StopRetroTest.Name='End retro. testing period';
	
	Fixer =	struct(	'Type', 'check',...
	'Name', 'Used Fixed Learning Time',...
	'DefaultValue',false);
	
	PossibleParameters = struct(	'StartRetroLearning', StartRetroLearn,...
									'EndRetroLearning' , StopRetroLearn,...
									'StartRetroTesting', StartRetroTest,...
									'EndRetroTesting', StopRetroTest,...
									'FixedTime',Fixer);
	
	
end
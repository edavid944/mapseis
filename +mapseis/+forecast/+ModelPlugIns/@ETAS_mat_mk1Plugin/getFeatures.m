function FeatureList = getFeatures(obj)
	%This is used by the project builder to determine which parameters
	%are available for the model. It influences mostly the region 
	%setting, the way data is exchanged and some additional menus
	%The Output should be a structure with specified fieldnames with 
	%mostly boolean values.
	%Following keywords are available:
	%	MagDist:	Should be normally true, means forecasts
	%			for different magnitude bins are available
	%			-> Magnitude bins
	%	DepthDist:	If set to true the model allows forecasts
	%			for different depth bins
	%			-> 3D model
	%	RectGrid:	If true the model should allow different
	%			grid spacing for Longitude and Latitude
	%	TimeYear:	If false the project builder will use
	%			days for time interval value and if true
	%			years will be used.
	%	Parallel:	If true the this GUI allows parallization
	%			and the toggle will be sended by builder
	%	HostMode:	Only for parallel mode, if set to true
	%			builder is allows to use this object in
	%			a parfor loop or similar. If false the
	%			model deals itself the parallization
	%	NoFiles:	If set to true the model does not produce
	%			any files and thus they cannot be archived
	%			The project builder will in this case not 
	%			allow the option to archive files 
	%	LogData:	If true Logdata is available not only in 
	%			file form but also directly imported as
	%			string or structure or whatever.
	%	ForecastType:	This is not boolean value but string
	%			It describes what distribution is 
	%			returning. It can be 'Possion', 'NegBin'
	%			'Custom' and 'Selectable'.
	%			'Selectable' means that the model supports
	%			more than on distribution. 
	
	
	%The project builder does also have a standard feature set in 
	%case some parts of the feature list are not defined.
	%The default is:
	%	MagDist=true;
	%	DepthHist=false;
	%	RectGrid=false;
	%	TimeYear=false;
	%	Parallel=true;
	%	HostMode=false;
	%	NoFiles=false;
	%	LogData=true;
	%	ForecastType='Poisson';
	%Maybe more later
	
	%IMPORTANT: if ForecastType is 'Selectable', a field with
	%the name "ForecastType" has to be added to the Model Configuration
	%describing the actual ForecastType, also a descripting for the 
	%GUI has to be added allowing to change the parameter
	
	%TODO: May have to add stuff to this list
	
	%not so sure about the parallel processing, but I say no for the 
	%moment
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	
	FeatureList = struct(	'MagDist',true,...
							'DepthDist',false,...
							'RectGrid',false,...
							'TimeYear',false,...
							'Parallel',true,...
							'HostMode',false,...
							'NoFiles',true,...
							'LogData',false,...
							'AddonData',true,...
							'ForecastType','Poisson');
	

end
function convertBG2km(obj,RelmGrid)
	%converts a BG grid with the reference point and saves it
	%into the ForGeo structure
	%at the moment the RefPoint will not be shifted
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	 
	RefPoint=obj.ForGeo.RefPoint;
	orgRelmGrid=RelmGrid;
	leng=size(orgRelmGrid(:,1));
	
	RelmGrid(:,1)=sign(orgRelmGrid(:,1)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,1)));	
	RelmGrid(:,2)=sign(orgRelmGrid(:,2)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,2)));	
	RelmGrid(:,3)=sign(orgRelmGrid(:,3)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,3),RefPoint(1).*ones(leng)));	
	RelmGrid(:,4)=sign(orgRelmGrid(:,4)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,4),RefPoint(1).*ones(leng)));
	RelmGrid(:,5:10)=orgRelmGrid(:,5:10);
	
	obj.ForGeo.convBG=RelmGrid;
	
end
function TripTime = convertTime(obj,inTime)
	%converts a datenum into a date strings used by the 
	%TripleS model and vice versa. It would not need to be 
	%internal method the TripleS plugin, but it is not needed 
	%outside, and like this it is easier to call inside the object.
	%inTime: datenum format e.g 7.3516e+05
	%TripTime: time string of the format: '2012/10/16 16:30:58'
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	%actually it is only on line, but still handy
	if isnumeric(inTime)
		TripTime = datestr(inTime,'yyyy/mm/dd HH:MM:SS');
	else
		TripTime = datenum(inTime,'yyyy/mm/dd HH:MM:SS');
	end

end
function ConfigureTestRegion(obj,RegionConfig)
	%This  method should allow to set the TestRegion config and grid
	%The variable RegionConfig will be structure with following 
	%entries:
	
	%RegionPoly:		Polygon surrounding the region
	%RegionBoundingBox:	Box around the Polygon
	%LonSpacing:		Longitude spacing or spacial spacing
	%			in case only on spacing is allowed
	%LatSpacing:		Latitude spacing, if two spacings 
	%			are allowed
	%GridPrecision:		The value to which the grid was rounded to.
	%MinMagnitude:		Minimum magnitude which should be 
	%			forecasted
	%MaxMagnitude:		The maximal magnitude which should be
	%			forecasted
	%MagnitudeBin:		Size of the Magnitude Bin often this 
	%			will be 0.1, but can be different. This
	%			will be empty if no magnitude binning is
	%			used
	%MinDepth:		Minimal depth range which should be 
	%			forecasted, in most cases this will be 0
	%MaxDepth:		Maximal depth to forecast
	%DepthBin:		In most cases this will empty as 3D 
	%			regions are not common, but in case a
	%			3D model is used this gives depth bin 
	%			size
	%TimeInterval:		Length of the forcast in days or year 
	%			(can be specified in getFeatures)
	%RelmGrid:		A raw grid similar to the one used in 
	%			Relm Forecasts based on the values 
	%			specified. 
	%			(Relmformat: [LonL LonH LatL LatH ...
	%			DepthL DepthH MagL MagH (ID) Mask]
	%Nodes			%Just the spacial nodes of the grid as 
	%			for instance by TripleS, in contrary to
	%			the RELM grid this descriptes the centre
	%			of the grid cell
	%MagVector:		Consisting of the Magnitudes which have
	%			to be forecasted (the points are the in 
	%			middle of the magnitude bin)
	%DepthVector:		Consisting of the Depths which have to 
	%			forecasted (only 3D) (the points are the  
	%			in middle of the depth bin)
	
	%Simple Only some are really needed, but everything stored
	%anyway
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	obj.RegionConfStruct=RegionConfig;
	
	obj.setConfigID;
	
	%Check if all complete
	obj.ModRegReady(3)=true;
	obj.ModelReady=all(obj.ModRegReady);
	obj.ParaEstimated=false;	
end
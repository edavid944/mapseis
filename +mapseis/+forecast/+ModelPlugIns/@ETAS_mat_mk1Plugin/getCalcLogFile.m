function LogFile = getCalcLogFile(obj)
	%The project calculator will only use this method if NoFiles is
	%set to false in the FeatureList and if the option to store log
	%files is activated in the project. This method should return a
	%file path(s) to the log file(s), the project builder will copy 
	%the files and store the new path. If there is more then one 
	%logfiles existing a Cell array with multiple paths (strings)
	%can be used instead of a normal single path
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	LogFile=[];
	warning('Model does not produce output files');
	
	
end
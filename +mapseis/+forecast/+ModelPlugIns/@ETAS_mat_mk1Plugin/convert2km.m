function copyCat=convert2km(obj,ShortCat)
	%converts a catalog in to distance-km from reference point
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	
	RefPoint=obj.ForGeo.RefPoint;
	copyCat=ShortCat;
	leng=size(ShortCat(:,1));
	
	copyCat(:,1)=sign(ShortCat(:,1)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),ShortCat(:,1)));	
	copyCat(:,2)=sign(ShortCat(:,2)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),ShortCat(:,2),RefPoint(1).*ones(leng)));
	
end
function [ForeCast, DataMask] = getForecast(obj,WithGrid)
	%This should return the calculated forecast. The Format which should
	%be returned is depended on the ForecastType and on the input 
	%parameter WithGrid. For 'Poisson' and 'NegBin' the output should be
	%vector (Poisson) and a "twin" vector (NegBin) and if WithGrid is
	%set to true a full Relm Grid forecast should be outputed. In the
	%'Custom' mode the output will always be a cell, without WithGrid
	%the output should be a cell vector containing the single grid cells
	%discrete distribution, else it should be a cell with two elements
	%first one containing the Relmgrid, second one containing the 
	%forecast like mentioned before.
	%The DataMask is the last column in a typical Relm based forecast
	%and marks if a cell has been used/defined (1) or not (0). It can 
	%be left empty, in this case it is assumed that all nodes are used.
	%In case of a full WithGrid output it DataMask will be ignored and
	%the grid will be used instead
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	%check if calculated
	if isempty(obj.ForecastResult)
		error('Nothing Calculated');
	end
	
	RelmGrid=obj.ForecastResult;
	
	%decide what has to be sended
	if WithGrid
		ForeCast=RelmGrid;
		DataMask=RelmGrid(:,10);
	else
		ForeCast=RelmGrid(:,9);
		DataMask=RelmGrid(:,10);
	end
	
	
end
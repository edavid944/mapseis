function UsedData = getInputData(obj,OnlyFilter)
	%This method will be used by the project calculator for archiving
	%the used data. The returned UsedData can have different format 
	%but suggested is zmap or shortcat based catalog or a datastore 
	%catalog (cutted!). If OnlyFilter is set to true, the UsedData 
	%variable should be a boolean vector giving the selected earthquakes
	
	%I go for the ShortCat this time, but it could easily be changed
	%at the moment the  function is more of diagnostic nature anyway
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.projector.*;
	
	
	if isempty(obj.Datastore)|isempty(obj.Filterlist)
		UsedData=[];
	else
		if OnlyFilter
			if ~islogical(obj.Filterlist)
				obj.Filterlist.changeData(obj.Datastore);
				obj.Filterlist.updateNoEvent;
				UsedData=obj.Filterlist.getSelected;
				obj.Filterlist.PackIt;
			else
				UsedData=obj.Filterlist;
			end
		
		else
			if ~islogical(obj.Filterlist)
				obj.Filterlist.changeData(obj.Datastore);
				obj.Filterlist.updateNoEvent;
				selected=obj.Filterlist.getSelected;
				obj.Filterlist.PackIt;
			else
				selected=obj.Filterlist;
			end
	
			[UsedData temp]=getShortCatalog(obj.Datastore,selected);
	
		end
	end
end
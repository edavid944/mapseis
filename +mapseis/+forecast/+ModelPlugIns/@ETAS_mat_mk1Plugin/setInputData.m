function setInputData(obj,Datastore,Filterlist,StartDate)
	%This method is used by the project calculator to send the input
	%catalog data to the model. Sended will be a Datastore catalog 
	%object and a Filterlist object or boolean vector giving the 
	%selection of the earthquakes (islogical can be used to separate
	%between the two cases).
	%Everything what has to be done to bring the input catalog into 
	%the right format has to be done here. Like conversion into a 
	%file format readable by the model.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
		
	import mapseis.projector.getDateNum;
	
	%check if it is the first run:
	InBench=tic;
	
	
	obj.Datastore = Datastore;
	obj.Filterlist = Filterlist;
	obj.ForecastPeriod = [StartDate, StartDate+obj.RegionConfStruct.TimeInterval]
	
	FilterExist=false;
	if isempty(obj.Filterlist)
		selected=true(obj.Datastore.getRowCount,1);	
	
	else
		if ~islogical(obj.Filterlist)
			obj.Filterlist.changeData(obj.Datastore);
			obj.Filterlist.updateNoEvent;
			selected=obj.Filterlist.getSelected;
			obj.Filterlist.PackIt;
			FilterExist=true;
		else
			selected=obj.Filterlist;
		end
		
	end
	
	if FilterExist
		TimeFilt=obj.Filterlist.getByName('Time');
		WhatPer = TimeFilt.getRange;
		
		TimePer=WhatPer{2};
		disp(WhatPer)
		
		if isinf(TimePer(1))
			%use start of catalog
			TimePer(1)=min(getDateNum(obj.Datastore,selected));
		end
		
		if isinf(TimePer(2))
			%use end of catalog
			TimePer(2)=max(getDateNum(obj.Datastore,selected));
		end
	else
		TimePer(1)=min(getDateNum(obj.Datastore,selected));
		TimePer(2)=max(getDateNum(obj.Datastore,selected));
	end
	
	obj.LearningPeriod=TimePer;
	
	%to be sure
	obj.ModelReady=all(obj.ModRegReady);
	obj.CalcTimeScratch(2)=toc(InBench);
	
end
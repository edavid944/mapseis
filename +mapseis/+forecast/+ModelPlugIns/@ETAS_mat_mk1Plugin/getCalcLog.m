function LogData = getCalcLog(obj)
	%This will be called if saving logs is activated and LogData is 
	%set to true in the FeatureList. The method should return a log 
	%of the calculation in a matlab common format and it should NOT
	%be a file link, because the log file will not be copied.
	%the easiest way to create the LogData is to just read the file 
	%into a string and return the string.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	if obj.DevMod
		LogData = {obj.AddonData,obj.LogData};
	else
		LogData = obj.LogData;
	end
	

end
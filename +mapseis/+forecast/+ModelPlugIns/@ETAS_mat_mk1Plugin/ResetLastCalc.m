function ResetLastCalc(obj)
	%This method should revert the plugin to the state before the 
	%calculation of the forecast, so that new data can be sent and
	%a new forecast can be calculated. This may involve things like 
	%empty the catalog storage and and deleting some input and output
	%files. But it might as well be just an empty function if no clean
	%up is needed. 
	%IMPORTANT: this method should revert the plugin to the state AFTER
	%prepareCalc was called.
	
	%means removing data and files, but not reset the OrgStartTime
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	%reset some of the properties
	obj.ForecastPeriod=[];
	obj.LearningPeriod=[];
	obj.ForecastResult=[];
	obj.Datastore=[];
	obj.Filterlist=[];
	obj.LogData=[];
	obj.AddonToSend=[];
	
end
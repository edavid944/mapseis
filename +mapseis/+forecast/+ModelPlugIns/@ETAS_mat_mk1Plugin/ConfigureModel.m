function ConfigureModel(obj,ConfigData)
	%allows to configurate the model
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	obj.ModelConfStruct=ConfigData;
	
	     			
	     		
	%Not much parameters needed here, the models sets everything
	%itself, but it needs an additional learning and testing period
	%so parameters needed
	%StartRetroLearning: start add. learning period string of following 
	%		     format: yyyy-mm-dd hh:mm:ss	
	%EndRetroLearning: End of the add. learning period
	%StartRetroTesting: Start of the additional testing period
	%EndRetroTesting: End of the additional testing period
	%FixedTime: defines if the additional periods are fixed in
	%	    time (true) or not (false). If false all times
	%	    will be shifted in a similar way the forecast
	%	    time was shifted.
	%All times have to be given in the mentioned format, and all
	%times should be before the forecast time (of course)
	
	obj.setConfigID;
	
	
	%Check if all complete
	obj.ModRegReady(1)=true;
	obj.ModelReady=all(obj.ModRegReady);
	obj.ParaEstimated=false;
	
end	

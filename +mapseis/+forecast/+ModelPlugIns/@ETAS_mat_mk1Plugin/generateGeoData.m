function generateGeoData(obj)
	%generate the reference points and converts the grid
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	import mapseis.ETAS.*;
	import mapseis.projector.*;
	
	%build catalog (for getting reference point)
	if ~islogical(obj.Filterlist)
		obj.Filterlist.changeData(obj.Datastore);
		obj.Filterlist.updateNoEvent;
		selected=obj.Filterlist.getSelected;
		obj.Filterlist.PackIt;
	else
		selected=obj.Filterlist;
	end
	
	ShortCat = getShortCatalog(obj.Datastore,selected,false);
	orgRelmGrid = obj.RegionConfStruct.RelmGrid;
	leng=size(orgRelmGrid(:,1));
	
	%find reverence: (assuming the first catalog is relevant
	minCatLon=min(ShortCat(:,1));
	minCatLat=min(ShortCat(:,2));
	minGridLon=min(orgRelmGrid(:,1));
	minGridLat=min(orgRelmGrid(:,3));
	
	
	RefPoint=[sign(minGridLon)*(abs(floor(min([minCatLon;minGridLon])-5))),...
	sign(minGridLat)*(abs(floor(min([minCatLat;minGridLat])-5)))];
	
	RelmGrid(:,1)=sign(orgRelmGrid(:,1)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,1)));	
	RelmGrid(:,2)=sign(orgRelmGrid(:,2)-RefPoint(1)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),RefPoint(2).*ones(leng),orgRelmGrid(:,2)));	
	RelmGrid(:,3)=sign(orgRelmGrid(:,3)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,3),RefPoint(1).*ones(leng)));	
	RelmGrid(:,4)=sign(orgRelmGrid(:,4)-RefPoint(2)).*deg2km(distance(RefPoint(2).*ones(leng),RefPoint(1).*ones(leng),orgRelmGrid(:,4),RefPoint(1).*ones(leng)));
	RelmGrid(:,5:10)=orgRelmGrid(:,5:10);
	
	obj.ForGeo=struct(	'RefPoint',RefPoint,...
						'convGrid',RelmGrid);

end
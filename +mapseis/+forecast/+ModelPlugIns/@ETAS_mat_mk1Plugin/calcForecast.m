function ErrorCode = calcForecast(obj)
	%Names says it. If this method is called the calculation of the 
	%forecast and according the configuration and catalog data should
	%started and the result should be saved in this object as well as
	%the path to the result file in case any is existing.
	%The ErrorCode can be used to signalize errors, it is just an 
	%integer. 0 means everthing worked fine, 1 means model is not
	%configurated properly, 2 means catalog data is missing, 3 means 
	%something with a external function did not work out and -1
	%means a generic not specified error. Every other value can 
	%used to specify special model specific errors. It is of course
	%sensible to explain the error code somewhere.
	%The error can be specified in the function getErrorList(obj), but 
	%it is as often optional, a generic function is integrated anyway.
	%Currently the function is not directly used, but can change very
	%soon
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	import mapseis.ETAS.*;
	import mapseis.projector.*;
	
	ErrorCode=-1;
	
	%check if everything is set
	if ~obj.ModelReady
		%here it really should be an error, because
		%that can be changed easily 
		error('Model is not configurated')
	end
	
	%update Config
	%-------------
	
	%generate catalog
	if ~islogical(obj.Filterlist)
		obj.Filterlist.changeData(obj.Datastore);
		obj.Filterlist.updateNoEvent;
		selected=obj.Filterlist.getSelected;
		obj.Filterlist.PackIt;
	else
		selected=obj.Filterlist;
	end
	
	ShortCat = getShortCatalog(obj.Datastore,selected,false);
	
	currentTime=obj.ForecastPeriod(1);
	TimeLength=obj.RegionConfStruct.TimeInterval;
	RelmGrid=obj.RegionConfStruct.RelmGrid;
	degRelm=RelmGrid;
	
	if obj.GeoMode
		%convert
		ShortCat=obj.convert2km(ShortCat);
		RelmGrid=obj.ForGeo.convGrid;
	end
	
	ThetaStart=struct(	'mu',obj.ModelConfStruct.mu,...
						'b',obj.ModelConfStruct.b,...
						'K',obj.ModelConfStruct.K,...
						'c',obj.ModelConfStruct.c,...
						'p',obj.ModelConfStruct.p,...
						'a',obj.ModelConfStruct.a,...
						'd',obj.ModelConfStruct.d,...
						'q',obj.ModelConfStruct.q);
	obj.ParaEstimated
	obj.ModConfig.EstimatePara = (obj.ModelConfStruct.Est_Param&~obj.ParaEstimated)|obj.ModelConfStruct.ResEst_Param;	
	
	
	%run calculation
	disp('start Calc step')
	%[Forecast AddonData] = EtasForecaster_test2(ShortCat,currentTime,TimeLength,RelmGrid,obj.ModConfig,ThetaStart);
	[Forecast AddonData] = EtasForecaster_mk2(ShortCat,currentTime,TimeLength,RelmGrid,obj.ModConfig,ThetaStart);
	
	%write estimated parameters into Config
	obj.ModelConfStruct.mu=AddonData.ThetaUsed.mu;
	obj.ModelConfStruct.b=AddonData.ThetaUsed.b;
	obj.ModelConfStruct.K=AddonData.ThetaUsed.K;
	obj.ModelConfStruct.c=AddonData.ThetaUsed.c;
	obj.ModelConfStruct.p=AddonData.ThetaUsed.p;
	obj.ModelConfStruct.a=AddonData.ThetaUsed.a;
	obj.ModelConfStruct.d=AddonData.ThetaUsed.d;
	obj.ModelConfStruct.q=AddonData.ThetaUsed.q;
	
	%store results
	obj.ForecastResult=degRelm;
	obj.ForecastResult(:,9)=Forecast;
	obj.LogData=AddonData.CalcTimes;
	obj.AddonData=AddonData;
	
	
	obj.ModConfig.SmoothConvMat=AddonData.SmoothConvMat;
	
	obj.generateAddonData;
	obj.generateBenchmark;
	obj.ParaEstimated=true;
	
	%finished
	ErrorCode=0;
	disp('Finished Calc Step')
	
end
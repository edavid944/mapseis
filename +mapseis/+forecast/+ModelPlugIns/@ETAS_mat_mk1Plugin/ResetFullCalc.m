function ResetFullCalc(obj)
	%Similar to ResetLastCalc but this one should revert the plugin to
	%state BEFORE prepareCalc. The function can be equal to ResetLastCalc
	%but it should not cause an error because double execution of a command
	%(e.g. delete of a file)
	
	%reset everything apart config, so first call ResetLastCalc
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	obj.ResetLastCalc;
	obj.BG_model=[];
	obj.ModConfig=[];
	
	%normally written for every calc step, but because
	%it is meant for diagnostic, it should be delete after
	%a calc run and not after every calculation.
	obj.AddonData=[];
	obj.CalcTimeScratch=[];
	
	if obj.ModelConfStruct.ParallelMode
		try
			matlabpool close;
		catch
			disp('Parallel Processing was not running')
		end	
	end
	
	obj.ParaEstimated=false;

end
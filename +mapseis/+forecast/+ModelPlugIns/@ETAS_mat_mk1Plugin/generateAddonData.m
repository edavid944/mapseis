function generateAddonData(obj)
	%generate new AddonData
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if isempty(obj.AddonToSend)
		obj.AddonToSend.NrBGHist={};
		obj.AddonToSend.NrTrigHist={};
		obj.AddonToSend.PercTrigHist={};
		obj.AddonToSend.sumPercTrigHist=[];
		
		obj.AddonToSend.NrBGHist{end+1,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.Num_BG_Event,25);
		obj.AddonToSend.NrBGHist{end,2}=[n;xout];
		
		obj.AddonToSend.NrTrigHist{end+1,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.Num_Trig_Event,25);
		obj.AddonToSend.NrTrigHist{end,2}=[n;xout];
		
		obj.AddonToSend.PercTrigHist{end+1,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.PercTrigEvent,25);
		obj.AddonToSend.PercTrigHist{end,2}=[n;xout];
		
		obj.AddonToSend.sumPercTrigHist(end+1,1)=obj.ForecastPeriod(1);
		obj.AddonToSend.sumPercTrigHist(end,2)=obj.AddonData.sumPercTrigEvent;
		return
	
	end
	
	
	if obj.AddonToSend.NrBGHist{end,1}~=obj.ForecastPeriod(1)
		obj.AddonToSend.NrBGHist{end+1,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.Num_BG_Event,25);
		obj.AddonToSend.NrBGHist{end,2}=[n;xout];
		
	else
		obj.AddonToSend.NrBGHist{end,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.Num_BG_Event,25);
		obj.AddonToSend.NrBGHist{end,2}=[n;xout];
	end
	
	
	
	if obj.AddonToSend.NrTrigHist{end,1}~=obj.ForecastPeriod(1)
		obj.AddonToSend.NrTrigHist{end+1,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.Num_Trig_Event,25);
		obj.AddonToSend.NrTrigHist{end,2}=[n;xout];
	
	else
		obj.AddonToSend.NrTrigHist{end,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.Num_Trig_Event,25);
		obj.AddonToSend.NrTrigHist{end,2}=[n;xout];
	end
	
	
	
	if obj.AddonToSend.PercTrigHist{end,1}~=obj.ForecastPeriod(1)
		obj.AddonToSend.PercTrigHist{end+1,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.PercTrigEvent,25);
		obj.AddonToSend.PercTrigHist{end,2}=[n;xout];
		
	else
		obj.AddonToSend.PercTrigHist{end,1}=obj.ForecastPeriod(1);
		[n,xout] = hist(obj.AddonData.PercTrigEvent,25);
		obj.AddonToSend.PercTrigHist{end,2}=[n;xout];
	end
	
	
	
	
	if obj.AddonToSend.sumPercTrigHist(end,1)~=obj.ForecastPeriod(1)
		obj.AddonToSend.sumPercTrigHist(end+1,1)=obj.ForecastPeriod(1);
		obj.AddonToSend.sumPercTrigHist(end,2)=obj.AddonData.sumPercTrigEvent;
		
	else
		obj.AddonToSend.sumPercTrigHist(end,1)=obj.ForecastPeriod(1);
		obj.AddonToSend.sumPercTrigHist(end,2)=obj.AddonData.sumPercTrigEvent;
		
	end

end
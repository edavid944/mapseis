function prepareCalc(obj)
	%This method is called the project builder, and should do everything
	%what is needed to run the forecast later. Often this method is 
	%totally unnecessary as everything can be done be before or after 
	%call of this method. But in some cases it might be a good idea 
	%to separate some cpu heavy preparation from the configuration.
	%If not needed the function can defined as a empty function.
	%IMPORTANT: No catalog preparation should be done here, as the
	%catalog data is sended by the project calculator. This function
	%is the last function before a actual calculation run (called in
	%the startCalc method of the calculator.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	import mapseis.ETAS.*;
	import mapseis.projector.*;
	
	%THIS WILL disappear with the stable version
	if  isempty(obj.DevMod)
		obj.DevMod=false;
	end
	
	
	bencher=tic;
	%switch on parallel if needed
	if obj.ModelConfStruct.ParallelMode
		try
			matlabpool open;
		catch
			disp('Parallel Processing could not be switched on')
		end	
	end
	
	
	%check if geoMode is active and the structure for it exists
	if obj.GeoMode
		if isempty(obj.ForGeo)
			obj.generateGeoData;
		end
	end
	
	%decide which BG is needed
	if obj.ModelConfStruct.ext_BGmodel
		obj.BG_model=load(obj.ModelConfStruct.ext_BG_Path);
		GenMode='normal';
		if obj.GeoMode
			obj.convertBG2km(obj.BG_model);
		end

	else	
		%generate catalog
		if ~islogical(obj.Filterlist)
			obj.Filterlist.changeData(obj.Datastore);
			obj.Filterlist.updateNoEvent;
			selected=obj.Filterlist.getSelected;
			obj.Filterlist.PackIt;
		else
			selected=obj.Filterlist;
		end
		
		ShortCat = getShortCatalog(obj.Datastore,selected,false);
		
		%kick out everything below minMag
		lowMag=ShortCat(:,5)<obj.ModelConfStruct.MinMag;
		ShortCat=ShortCat(~lowMag,:);
		
		BGProb=obj.ModelConfStruct.BG_Prob;
		TimeLeng=obj.LearningPeriod(2)-obj.LearningPeriod(1);
		RelmGrid=obj.RegionConfStruct.RelmGrid;
		NrBuddys=obj.ModelConfStruct.BG_SmoothParam(2);
		minWidth=obj.ModelConfStruct.BG_SmoothParam(1);
		bval=obj.ModelConfStruct.b;
		
		if obj.GeoMode
			%convert
			ShortCat=obj.convert2km(ShortCat);
			degRelm=RelmGrid;
			RelmGrid=obj.ForGeo.convGrid;
			minWidth(2)=minWidth(1);
			minWidth(1)=deg2km(minWidth(1));
		end
	
	
		disp('generate Background model')
		GenMode='normal';
		switch obj.ModelConfStruct.int_BG_mode
			case 'AnySmooth_norm'
				BGRates = calcBGRatesND_mk2b(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,true);
				GenMode='normalBG';
			case 'AnySmooth'
				BGRates = calcBGRatesND_mk2(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,true);
			case 'Smooth3D'
				BGRates = calcBGRatesND_mk2(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval,false);
			case 'Smooth'
				BGRates = calcBGRates(ShortCat,BGProb,TimeLeng,RelmGrid,NrBuddys,minWidth,bval);
		end	
	
		%multiply the rates with the forecast length
		BGRates(:,9)=BGRates(:,9)*obj.RegionConfStruct.TimeInterval;
		
		%obj.BG_model=RelmGrid;
		if obj.GeoMode
			obj.ForGeo.convBG=BGRates;
			degRelm(:,9)=BGRates(:,9);
			BGRates=degRelm;
		end
	
		obj.BG_model=BGRates;
	
	end	
	
	%obj.BG_model(:,9)=obj.BG_model(:,9)*obj.BG_Scaler;
	
	%decide which smoothing (may add some more options to it later)
	switch obj.ModelConfStruct.Res_SmoothMode
		case 'None'
			SmoothSwitch=false;
			SmoothPar=[];
			SmoothMode='none';
		
		case 'ETAS3D'
			SmoothSwitch=true;
			SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
			SmoothMode='ETAS3D';
		
		case 'ClassicETAS'
			SmoothSwitch=true;
			SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
			SmoothMode='ClassicETAS';
		
		case 'Gauss'					
			SmoothSwitch=true;
			SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
			SmoothMode='gauss';
		
		case 'Mean'
			SmoothSwitch=true;
			SmoothPar=obj.ModelConfStruct.Res_SmoothParam;
			SmoothMode='mean';
	
	end
	
	if obj.GeoMode
		BGMod=obj.ForGeo.convBG;
		
		
		
		if numel(SmoothPar)==3&strcmp(SmoothMode,'ETAS3D')
			SmoothPar=[deg2km(SmoothPar(1)),SmoothPar(2),SmoothPar(3),SmoothPar(1)];
		else
			try
				SmoothPar(1)=deg2km(SmoothPar(1));
			end 	
		end
	
	else
		BGMod=obj.BG_model;
	end
	
	%generate basic config structure 
	obj.ModConfig=	struct(	'ReturnVal','Poisson',...
							'EstimatePara',obj.ModelConfStruct.Est_Param,...
							'EqGeneration',GenMode,...
							'BG_Distro',BGMod,...
							'GenTimeBefore',TimeLeng,...
							'LearnStartTime',obj.LearningPeriod(1),...
							'MinMag',obj.ModelConfStruct.MinMag,...
							'MinMainMag',obj.ModelConfStruct.MinMainMag,...
							'GenEqList',[[]],...
							'Gen1stGen',true,...
							'Nr_Simulation',obj.ModelConfStruct.Nr_Simulation,...
							'Smoothing',SmoothSwitch,...
							'SmoothMode',SmoothMode,...
							'SmoothParam',SmoothPar,...
							'SmoothConvMat',{{}});
	
	
	%generate basic structure for AddonData
	obj.AddonToSend= struct('NrBGHist',{{}},...
							'NrTrigHist',{{}},...
							'PercTrigHist',{{}},...
							'sumPercTrigHist',[[]]);			
	
	disp('Model Prepared')			
	
	obj.CalcTimeScratch(1)=toc(bencher);
	
end
function FilePath = getForecastFile(obj)
	%This method will only be called if NoFiles is false and if the 
	%option to archive the forecast files is switched on the project
	%object. 
	%It should return the path to the forecast file as a string. The 
	%project calculater will then copy the file to a specific folder
	%save the link to this file. If there is more then one 
	%result file existing a Cell array with multiple paths (strings)
	%can be used instead of a normal single path
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	FilePath=[];
	warning('Model does not produce output files');

end
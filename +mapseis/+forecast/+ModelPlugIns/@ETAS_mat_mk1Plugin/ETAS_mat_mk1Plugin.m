classdef ETAS_mat_mk1Plugin < mapseis.forecast.ForecastPlugIn
	%based on the plugin interface, 
	%First version of a plugin for a Schoenberg based ETAS model build 
	%entirely in matlab
	
	%first version
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	
	properties
		%Defined in Plugin superclass
		%  PluginName
		%  Version %Has to be a STRING (consist of CodeVersion and Variant)
		%  CodeVersion %Version of the code
		%  Variant %allows to use multiple varitions of the same model in a project
		%  ConfigID %Has to be re-set everytime the model is configurated
		%  ModelInfo %The place for a description about the model 
		%  
		%  Datastore
		%  Filterlist
		%  ModelReady 
		
		LogData
		AddonData
		ForecastPeriod
		LearningPeriod
		BG_Scaler
		
		
		ModelConfStruct
		RegionConfStruct        	
		ModRegReady
		ForecastResult
		BG_model
		ModConfig
		AddonToSend
		CalcTimeScratch
		GeoMode
		ForGeo
		ParaEstimated
		DevMod
	end
	
	
	
	methods 
		function obj = ETAS_mat_mk1Plugin(Variant)
			RawChar='0123456789ABCDEF'; %Hex
			if nargin<1
				Variant=[];
			end
			
			if isempty(Variant)
				Variant=RawChar(randi(15,1,3)+1)
			end
			
			obj.Variant=Variant;
			
			obj.PluginName='ETAS_matlab';
			obj.CodeVersion='proto_v1';
			obj.Version=[obj.CodeVersion,'_',obj.Variant];
			obj.ModelInfo=[	'This is the first version of a plugin for ',...
							'ETAS '];
			
			%Init config key
			obj.setConfigID;
			
			obj.Datastore=[];
			obj.Filterlist=[];
			
			obj.LogData=[];
			obj.AddonData=[];
			obj.BG_Scaler=1;
			
			obj.ModelConfStruct=[];
			obj.RegionConfStruct=[];   
			
			obj.CPUTimeData=[];
			
			obj.ForecastPeriod=[];
			obj.LearningPeriod=[];
			
			
			obj.ModRegReady=[false true false];
			obj.ModelReady=false;
			obj.ForecastResult=[];
			obj.BG_model=[];
			obj.ModConfig=[];
			obj.AddonToSend=[];
			obj.CalcTimeScratch=[];
			
			%converts the data into km (slower but more accurate)
			%the results will be in lon and lat as usual
			obj.GeoMode=false;
			obj.ForGeo=[];
			obj.ParaEstimated=false;
			
			%developer mode (will add all output to addon)
			obj.DevMod=false;
		end
		
		
		%external file methods
		
		ConfigureModel(obj,ConfigData)
		
		ConfigureFramework(obj,FrameConfig)
		
		ConfigureTestRegion(obj,RegionConfig)
		
		ConfigData = getModelConfig(obj)
		
		FrameConfig = getFrameConfig(obj)

		RegionConfig = getTestRegionConfig(obj)
		
		PossibleParameters = getParameterSetting(obj)

		UsedData = getInputData(obj,OnlyFilter)
		
		FeatureList = getFeatures(obj)
		
		prepareCalc(obj)
		
		ErrorCode = calcForecast(obj)
		
		[ForeCast, DataMask] = getForecast(obj,WithGrid)
		
		FilePath = getForecastFile(obj)
		
		LogData = getCalcLog(obj)
		
		LogFile = getCalcLogFile(obj)
		
		AddonData = getAddonData(obj)
		
		ResetLastCalc(obj)
		
		ResetFullCalc(obj)
		
		
		
		%external custom methods
		
		generateAddonData(obj)
		
		generateBenchmark(obj)
		
		generateGeoData(obj)
		
		convertBG2km(obj,RelmGrid)
		
		copyCat=convert2km(obj,ShortCat)
		
		TripTime = convertTime(obj,inTime)
		
		
	end
	
end	
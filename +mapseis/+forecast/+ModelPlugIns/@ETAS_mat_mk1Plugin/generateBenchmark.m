function generateBenchmark(obj)
	%this generates the calculation times "needed" by the calculator
	%plus some additional times
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	% You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	obj.CPUTimeData = struct(	'InputDataTime',obj.CalcTimeScratch(2),...
								'CalcTime',obj.AddonData.CalcTimes.FullTime,...
								'ImportTime',0,...
								'PrepareTime',obj.CalcTimeScratch(1),...
								'AddTimeInfo',obj.AddonData.CalcTimes);
	

end
function TypicalStart(obj,Forecast,Datastore,Filterlist)
	%Adds a forecasts and opens the GUI
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.filter.*;

	%Filterlist will be  cloned to freely edit it	
	if nargin<3
		obj.AddForecast(Forecast);
		obj.Datastore=[];
		obj.Filterlist=[];
		obj.CreateViewer;
		obj.InitPlots;
	
	else
		obj.AddForecast(Forecast);
		obj.Datastore=Datastore;
		obj.Filterlist=FilterListCloner(Filterlist);
		obj.CreateViewer;
		obj.InitPlots;
	
	end
	   		
end
function AddForecast(obj,Forecast)
	%just for adding a forecast
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	if ~isempty(obj.WindowHandle)
		%close window if existing
		%(Will come later)
	
	end
	
	%save it
	obj.Forecast=Forecast;
	obj.WorkMode=obj.Forecast.Dimension;
	
	[SpaceGrid DepthGrid  MagGrid] = obj.Forecast.getAxisMesh;
	
	%init needed data
	obj.MagList=obj.Forecast.MagBins;
	obj.DepthList=obj.Forecast.DepthBins;
	obj.TimeList=obj.Forecast.TimeVector;
	obj.AxisVectors=SpaceGrid;
	obj.TitleRawParts{1}=obj.Forecast.Name;
	obj.TitleRawParts{2}=', Magnitude: ';
	obj.TitleRawParts{3}=' ';
	obj.TitleRawParts{4}=', Depth: ';
	obj.TitleRawParts{5}=' ';
	obj.TitleRawParts{6}=', Time: ';
	obj.TitleRawParts{7}=' ';
	
	%clear old stuff (if it exists)
	obj.PlotHandles=[];
	obj.WindowHandle=[];
	obj.TitleHandle=[];
	obj.plotAxis=[];
	obj.MagSlider=[];
	obj.MagText=[];
	obj.DepthSlider=[];
	obj.DepthText=[];
	
end
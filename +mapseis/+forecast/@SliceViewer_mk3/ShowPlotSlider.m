function ShowPlotSlider(obj)
	%shows the by the sliders selected plot
	%I maybe add an addition function for a other gui element
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	import mapseis.util.*;	
	
	%check if it is save to do the work
	if obj.AllReady
		%Default position needed if only one is variable
		MagPos=1;
		DepthPos=1;
		TimePos=1;
		
		if obj.WorkMode.Mag
			%get slider position
			MagPos=round(get(obj.MagSlider,'Value'));
			obj.HandPos(1)=MagPos;
			
			%set MagText
			set(obj.MagText,'String',num2str(obj.MagList(MagPos)));
			
			%change Title part
			obj.TitleRawParts{3}=num2str(obj.MagList(MagPos));
			
			%set back the rounded value
			set(obj.MagSlider,'Value',MagPos);
			
		end 
	
	
		if obj.WorkMode.Depth
			%get slider position
			DepthPos=round(get(obj.DepthSlider,'Value'));
			obj.HandPos(2)=DepthPos;
			
			%set MagText
			set(obj.DepthText,'String',num2str(obj.MagList(DepthPos)));
			
			%change Title part
			obj.TitleRawParts{5}=num2str(obj.MagList(DepthPos));
			
			%set back the rounded value
			set(obj.DepthSlider,'Value',DepthPos);
		end
	
		if obj.WorkMode.Time
			%get slider position
			TimeSlidePos=round(get(obj.TimeSlider,'Value'));
			
			%get time period
			TimePeriod=get(obj.CoarseTimeSelect,'Value');
			
			%calculate current selected time
			TimePos=(TimePeriod-1)*obj.TimeSliceSize+TimeSlidePos-1;
			if TimePos>numel(obj.TimeList)
				TimePos=numel(obj.TimeList);
			end
		
			%set TimeText
			%set(obj.TimeText,'String',num2str(decyear(datevec(obj.TimeList(TimePos)))));
			set(obj.TimeText,'String',datestr(obj.TimeList(TimePos)));
			
			%change Title part
			%obj.TitleRawParts{7}=num2str(decyear(datevec(obj.TimeList(TimePos))));
			obj.TitleRawParts{7}=datestr(obj.TimeList(TimePos));
			
			%set back the rounded value
			set(obj.TimeSlider,'Value',TimeSlidePos);
			
			
			%UNFINISHED
			obj.HandPos(3)=TimeSlidePos;
			
			disp(TimePeriod)
			disp(obj.CurrentTimePeriod)
	
			%check if still in range, if not re-init plot
			if TimePeriod~=obj.CurrentTimePeriod
				obj.InitPlots(TimePeriod);
			end
			
		end 
	
		%change title
		try
			set(obj.TitleHandle,'String',[obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
			obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},obj.TitleRawParts{7},'   ']);
		catch
			%Title somehow deleted ->recreate
			obj.TitleHandle=title([obj.TitleRawParts{1},obj.TitleRawParts{2},obj.TitleRawParts{3},...
			obj.TitleRawParts{4},obj.TitleRawParts{5},obj.TitleRawParts{6},obj.TitleRawParts{7},'   ']);
			set(obj.TitleHandle,'FontSize',24,'FontWeight','bold');
		end
		
		%set all plots to hidden
		obj.HideAllPlots;
		
		%set the selected plot to visible
		set(obj.PlotHandles{obj.HandPos(1),obj.HandPos(2),obj.HandPos(3)},'Visible','on');
	
	end
	
end
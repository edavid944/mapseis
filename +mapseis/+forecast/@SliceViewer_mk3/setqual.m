function setqual(obj,qual)
	%sets the quality of the plot 
	%chk just gets the userdata parameter and sets the 'checked' to the menu
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	
	%dataStore = obj.DataStore;
	Menu2Check(3) = findobj(obj.PlotMenu,'Label','Zmap style');
	Menu2Check(2) = findobj(obj.PlotMenu,'Label','Medium Quality Plot');
	Menu2Check(1) = findobj(obj.PlotMenu,'Label','High Quality Plot');
	
	%uncheck all menupoints
	set(Menu2Check(1), 'Checked', 'off');
	set(Menu2Check(2), 'Checked', 'off');
	set(Menu2Check(3), 'Checked', 'off');
	
	switch qual
		case 'low'
			set(Menu2Check(3), 'Checked', 'on');
			obj.PlotOption.EQ_Quality='low';
			obj.PlotOverlay;
		   		
		case 'med'
			set(Menu2Check(2), 'Checked', 'on');
			obj.PlotOption.EQ_Quality='med';
			obj.PlotOverlay;
		
		case 'hi'
			set(Menu2Check(1), 'Checked', 'on');
			obj.PlotOption.EQ_Quality='hi';
			obj.PlotOverlay;
		
		case 'chk'
			StoredQuality = obj.PlotOption.EQ_Quality;
			switch StoredQuality
				case 'low'
					sel=3;
				case 'med'
					sel=2;
				case 'hi'
					sel=1;
			end		
		set(Menu2Check(sel), 'Checked', 'on');
	
	end

end
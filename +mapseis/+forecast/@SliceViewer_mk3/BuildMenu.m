function BuildMenu(obj)
	%builds the menu for the overlay
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	obj.PlotMenu = uimenu( obj.WindowHandle,'Label','- Plot Options');
	
	uimenu(obj.PlotMenu,'Label','plot Coastlines','Separator','on',... 
				'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
	uimenu(obj.PlotMenu,'Label','plot Country Borders',...
				'Callback',@(s,e) obj.TooglePlotOptions('Border'));
	uimenu(obj.PlotMenu,'Label','plot Earthquake Locations',...
				'Callback',@(s,e) obj.TooglePlotOptions('EQ'));	
	%uimenu(obj.PlotMenu,'Label','apply Mag/Depth Filter',...
	%			'Callback',@(s,e) obj.TooglePlotOptions('FiltCut'));		
	
	uimenu(obj.PlotMenu,'Label','Zmap style','Separator','on',... 
	'Callback',@(s,e) obj.setqual('low'));
	uimenu(obj.PlotMenu,'Label','Medium Quality Plot',...
	'Callback',@(s,e) obj.setqual('med'));
	uimenu(obj.PlotMenu,'Label','High Quality Plot',...
	'Callback',@(s,e) obj.setqual('hi'));
	
	%set the checks right
	obj.TooglePlotOptions('-chk');
	obj.setqual('chk');
	
end
function TooglePlotOptions(obj,WhichOne)
	%This functions sets the toogles in this modul
	%WhichOne can be the following strings:
	%	'Border':	Borderlines
	%	'Coast'	:	Coastlines
	%	'EQ'	:	Earthquake locations
	%	'-chk'	:	This will set all checks in the menu
	%			to the in toggles set state.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	CoastMenu = findobj(obj.PlotMenu,'Label','plot Coastlines');
	BorderMenu = findobj(obj.PlotMenu,'Label','plot Country Borders');
	EqMenu = findobj(obj.PlotMenu,'Label','plot Earthquake Locations');
	%SliceMenu = findobj(obj.PlotMenu,'Label','apply Mag/Depth Filter');
	
	if isempty(obj.PlotOption)
		%default options
		obj.PlotOption=struct(	'CoastLine',true,...
								'Borders',true,...
								'EQ',true,...
								'EQ_Quality','hi',...
								'SliceFilter',false);
	end
	
	switch WhichOne
		case '-chk'
			if obj.PlotOption.CoastLine
				set(CoastMenu, 'Checked', 'on');
			else
				set(CoastMenu, 'Checked', 'off');
			end
			
			if obj.PlotOption.Borders
				set(BorderMenu, 'Checked', 'on');
			else
				set(BorderMenu, 'Checked', 'off');
			end
			
			if obj.PlotOption.EQ
				set(EqMenu, 'Checked', 'on');
			else
				set(EqMenu, 'Checked', 'off');
			end
			
			
			%if obj.PlotOption.SliceFilter
			%	set(SliceMenu, 'Checked', 'on');
			%else
			%	set(SliceMenu, 'Checked', 'off');
			%end
			
			
		case 'Border'
			obj.PlotOption.Borders=~obj.PlotOption.Borders;
			
			if obj.BorderToogle
				set(CoastMenu, 'Checked', 'on');
			else
				set(CoastMenu, 'Checked', 'off');
			end
			
			obj.PlotOverlay;
			
		case 'Coast'
			obj.PlotOption.CoastLine=~obj.PlotOption.CoastLine;
			
			if obj.CoastToogle
				set(BorderMenu, 'Checked', 'on');
			else
				set(BorderMenu, 'Checked', 'off');
			end
			
			obj.PlotOverlay;
			
		case 'EQ'
			obj.PlotOption.EQ=~obj.PlotOption.EQ;
			
			if obj.EQToogle
				set(EqMenu, 'Checked', 'on');
			else
				set(EqMenu, 'Checked', 'off');
			end
			
			obj.PlotOverlay;
			
			
			%case 'FiltCut'
			%	obj.PlotOption.SliceFilter=~obj.PlotOption.SliceFilter;
			%		
			%	if obj.EQToogle
			%		set(SliceMenu, 'Checked', 'on');
			%	else
			%		set(SliceMenu, 'Checked', 'off');
			%	end
			
			%	obj.PlotOverlay;
	
	end
	
end
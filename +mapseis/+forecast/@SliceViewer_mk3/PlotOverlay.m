function PlotOverlay(obj)
	%plots the coastlines and eq if selected
	
	%NOT FULLY FINISHED: I have to find a way to clever plot
	%the eqs time&mag&depth (or at least time) dependent.
	%there are just a lot of handles that's the problem
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.plot.*;
	
	xlimiter=xlim;
	ylimiter=ylim;
	
	%check what is available
	if isempty(obj.Datastore)
		disp('No Datastore - No CoastLine');
		return
	end
	
	
	%check what is wanted
	if isempty(obj.PlotOption)
		%default options
		obj.PlotOption=struct(	'CoastLine',true,...
								'Borders',true,...
								'EQ',false,...
								'EQ_Quality','hi',...
								'SliceFilter',false);
	end
	
	CoastToggle=obj.PlotOption.CoastLine;
	BorderToggle=obj.PlotOption.Borders;
	EQToggle=obj.PlotOption.EQ;
	EQ_Quali=obj.PlotOption.EQ_Quality;
	%SliceFiltToggle=obj.PlotOption.SliceFilter;
	
	
	%Eq not allowed now, I have to find a solution for
	%the slider-Eq interaction
	EQToggle=false;
	
	if (~CoastToggle&~BorderToggle&~EQToggle)
		%nothing has to be done
		return
	end
	
	if ~isempty(obj.Filterlist)		
		%	TimeFilt=obj.Filterlist.getByName('Time');
		%	TimeFilt.setRange(Range,TimeInt);			
		selected=[];
	
	else
		selected=[];
	
	end
	
	%TODO: Maybe a color switch black or white for the Coast 
	%and Border is an idea
	
	%prepare data
	CoastConf= struct(	'PlotType','Coastline',...
						'Data',obj.Datastore,...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'LineStylePreset','Fatline',...
						'Colors','white');
	
	BorderConf= struct(	'PlotType','Border',...
						'Data',obj.Datastore,...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'LineStylePreset','dotted',...
						'Colors','black');
	
	
	EqConfig= struct(	'PlotType','Earthquakes',...
						'Data',obj.Datastore,...
						'PlotMode','old',...
						'PlotQuality',EQ_Quali,...
						'SelectionSwitch','selected',...
						'SelectedEvents',selected,...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit',xlimiter,...
						'Y_Axis_Limit',ylimiter,...
						'C_Axis_Limit','auto',...
						'LegendText','Earthquakes');
	
	
	%plot
	
	%erase old plots
	if ~isempty(obj.OverlayHandle)
		for i=1:numel(obj.OverlayHandle)
			try
				delete(obj.OverlayHandle(i));
			end	
		end
		obj.OverlayHandle=[];
	end
	
	
	hold on
	if CoastToggle
		[obj.OverlayHandle(1) CoastEntry] = PlotCoastline(obj.plotAxis,CoastConf);
	end
	
	
	if BorderToggle
		[obj.OverlayHandle(2) BorderEntry] = PlotBorder(obj.plotAxis,BorderConf);
	end
	
	if EQToggle
		[obj.OverlayHandle(3) entry1] = PlotEarthquake(plotAxis,EqConfig);
	end
	
	hold off
	
end
function InitPlots(obj)
	%plot everything
	%the plot routine may change later to incoporate
	%more settings and also to allow coastlines, earthquakes
	%and so one.
	
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	%"delete" old plots
	axes(obj.plotAxis);
	h1=plot(mean(obj.AxisVectors{1}(:)),mean(obj.AxisVectors{2}(:)),'w');
	set(h1,'Visible','off');
	
	overMin=[];
	overMax=[];
	
	hold on;
	for i=1:numel(obj.MagList)
		for j=1:numel(obj.DepthList)
			for k=1:numel(obj.TimeList)
				%at moment only first forecast support
				
				%T-M-D coordinates
				CastTime=obj.TimeList(k);
				Magnitude=obj.MagList(i);
				Depth=obj.DepthList(j);
				
				%get data
				[ForeSlice ForeTime ForeLength] = obj.Forecast.get2DSlice(CastTime,Magnitude,Depth);
				
				if obj.LogMode
					%avoid infinity
					ForeSlice(ForeSlice==0)=realmin;
					ForeSlice=log10(ForeSlice);
				end
				
				%go for min and max (for unified colorscale)
				overMin=min([overMin,min(ForeSlice(:))]);
				overMax=max([overMax,max(ForeSlice(:))]);
				
				%plot it
				axes(obj.plotAxis);
				obj.PlotHandles{i,j,k}=image(obj.AxisVectors{1}(1,:),obj.AxisVectors{2}(:,1),ForeSlice);
				
				%some additional plot settings
				set(obj.PlotHandles{i,j,k},'CDataMapping','scaled');
				
				%hide it	
				set(obj.PlotHandles{i,j,k},'Visible','off')
			end
		end
	end
	
	%set coloraxis
	%caxis([overMin overMax]);
	
	myBar=colorbar;
	
	%here could be the additional stuff needed to be plotted
	
	hold off;
	
	%set axis limit right
	xlim([min(obj.AxisVectors{1}(:)),max(obj.AxisVectors{1}(:))]);
	ylim([min(obj.AxisVectors{2}(:)),max(obj.AxisVectors{2}(:))]);
	
	%aspect ratio&axis direction
	set(obj.plotAxis,'YDir','normal');
	latlim = get(obj.plotAxis,'Ylim');
	set(obj.plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
	
	%reapply axis setting, some version just override them
	obj.ApplyAxisSetting;
	
	obj.AllReady=true;
	
	
	%set first image visible
	set(obj.PlotHandles{1,1},'Visible','on');

end
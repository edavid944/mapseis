classdef ForecastLogObj < handle
	%A storage container for logs produced by forecast models (either external
	%ones or internal ones). This object is similar to the forecast object, but 
	%much simpler
	%Actully this object can be used for almost any additional forecast data
	%InfoType can be used freely to add information about the content of the
	%object (e.g. 'logs', 'configs',...)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	properties
        	Name
        	InfoType
        	ID
        	ForecastID
        	Version
        	TimeVector
        	TimeSpacing
        	RealTimeMode
        	LogData
        	TheBag
        	    	
        end
    

    
        methods
        	function obj=ForecastLogObj(Name,ForecastID)
        		%constructor: only inits the most important fields
        		RawChar='0123456789ABCDEF'; %Hex
        		if nargin<1
        			Name=[];
        		end
        
        		if isempty(Name)
        			%A bit gimmicky I admit, but could be useful later
        			Name=RawChar(randi(15,1,8)+1)
        		end
        		
        		obj.Name=Name;
        		obj.InfoType='generic';
        		obj.ID=RawChar(randi(15,1,8)+1)
        		obj.Version='0.9';
        		
        		%init data
        		obj.ForecastID=ForecastID;
        		obj.TimeVector=[];
        		obj.TimeSpacing=[];
        		obj.RealTimeMode=false;
        		obj.LogData={};
        		
        		%internal variable
        		obj.TheBag={};
        					
        					
        	end	
        	
        	
        	function setRealTimeMode(obj, ModeToogle)
        		%sets the realtime mode (allowing overlaping forecast)
        		%it gives a warning if already two forecasts are in the 
        		%object.
        		
        		if numel(obj.TimeVector)>=2
        			warning('There are already two forecast in this object');
        		end
        		
        		obj.RealTimeMode=ModeToogle;
        		
        	end
        	        	      	
        	
        	function addLog(obj,ForecastLog,CastTime,CastLength)
        		%adds a Forecast log to the Object
        		%CastTime should give the start time of the forecast and
        		%CastLength how long the forecast period is, eg. CastTime=2008
        		%CastLength=1 for a 1 year forecast for the whole of 2008. The 
        		%format of time does not really matter as long as it is conisted
        		%and numerical.

        		
        		oldPos=[];
        		%write times and change to time dependent if necessary
        		if ~isempty(obj.TimeVector)
        			if CastTime>obj.TimeVector(end)
        				if CastTime<obj.TimeVector(end)+obj.TimeSpacing(end)&~obj.RealTimeMode
        					error('New forecast log overlaps old on')
        				end
        				
        				obj.TimeVector(end+1)=CastTime;
        				obj.TimeSpacing(end+1)=CastLength;
        				obj.Dimension.Time=true;
        			else
        				%find position
        				MrSmall=find(obj.TimeVector<CastTime);
        				
        				if isempty(MrSmall)
        					error('No suitable position found');
        				end
        				
        				oldPos=MrSmall(end);
        				
        				%check if now overlap is present
        				Condition = ((obj.TimeVector(oldPos)+obj.TimeSpacing(oldPos))>CastTime)|...
        						(CastTime+CastLength)>obj.TimeVector(oldPos+1);
        				if Condition&~obj.RealTimeMode|(obj.TimeVector(oldPos)==CastTime)
        					error('There is a overlap present with an old entry, use replaceLog if and old entry should be replaced')
        				end	
        					
        				NewTimeVec=[obj.TimeVector(1:oldPos),CastTime,obj.TimeVector(oldPos+1:end)];
        				NewSpacing=[obj.TimeSpacing(1:oldPos),CastLength,obj.TimeSpacing(oldPos+1:end)];
        				obj.TimeVector=NewTimeVec;
        				obj.TimeSpacing=NewSpacing;
        				obj.Dimension.Time=true;
        			end
        			
        		else
        			obj.TimeVector=CastTime;
        			obj.TimeSpacing=CastLength;
        		end
        		
        		%write forecast
        		
			
			if isempty(oldPos)
				obj.LogData{end+1}=ForecastLog;
			
			else
				NewCaster={obj.LogData{1:oldPos},ForecastLog,obj.LogData{oldPos+1:end}};
				obj.LogData=NewCaster;
			
			end
			

        		
        	end
        	
        	
        	function replaceLog(obj,ForecastLog,CastTime,CastLength)
        		%works similar to the addLog but replaces an old log
        		%if present, the date have to match and old data to do this
        		
        		
        		if isempty(obj.TimeVector)
        			%nothing present, just add
        			disp('No Forecast present, added as new forecast log')
        			obj.TimeVector=CastTime;
        			obj.TimeSpacing=CastLength;
        			obj.LogData{end+1}=ForecastLog;
				
        		else
        			if CastTime>obj.TimeVector(end)
        				
        				if CastTime<obj.TimeVector(end)+obj.TimeSpacing(end)&~obj.RealTimeMode
        					error('New forecast overlaps old on')
        				end
        				
        				obj.TimeVector(end+1)=CastTime;
        				obj.TimeSpacing(end+1)=CastLength;
        				obj.LogData{end+1}=ForecastLog;
					
									
					disp('Forecast is newer than every old one, and was added as new forecast');
        				
        			else
        				MrSmall=find(obj.TimeVector==CastTime);
        				
        				if isempty(MrSmall)
        					error('No Forecast to replace found');
        				end
        				
        				if numel(MrSmall)>1
        					%this should never happen
        					error('More than one suitable forecast found')
        				end
        				
        				oldPos=MrSmall;
        				
        				if isempty(CastLength)
        					%use old spacing
        					CastLength=obj.TimeSpacing(oldPos);
        				end	
        				
        				%check if now overlap is present needs three conditions (if it is on start or end)
        				if numel(obj.TimeVector)==1&oldPos==1 
        					Condition=false;
        				elseif oldPos==1
        					Condition = (CastTime+CastLength)>obj.TimeVector(oldPos+1);
        				elseif oldPos==numel(obj.TimeVector)
        					Condition = ((obj.TimeVector(oldPos-1)+obj.TimeSpacing(oldPos-1))>CastTime);
        				else
        					Condition = ((obj.TimeVector(oldPos-1)+obj.TimeSpacing(oldPos-1))>CastTime)|...
        							(CastTime+CastLength)>obj.TimeVector(oldPos+1);
        				end
        				
        				if Condition&~obj.RealTimeMode
        					error('There is a overlap present with an old entry')
        				end	
        					
        				obj.TimeVector(oldPos)=CastTime;
        				obj.TimeSpacing(oldPos)=CastLength;
        				obj.LogData{oldPos}=ForecastLog;
        				
        				MaskPos=oldPos;

        				
        			end
        			
        			
        					
        		
        		end
        	
        	end
        	
        	
        	function removeLog(obj,CastTime)
        		%removes a forecast from the list if found.
        		if isempty(obj.TimeVector)
        			error('No forecast logs saved')
        		end
        		
        		oldPos=find(obj.TimeVector==CastTime);
        		
        		if isempty(oldPos)
        			error('No Forecast Log with this date found');
        		end
        		
        		if numel(oldPos)>1
        			%this should never happen
        			error('More than one suitable forecast log found')
        		end
        		
        		%special case: forecast empty after removal
        		if numel(obj.TimeVector)==1
        			disp('Last forecast deleted')
        			obj.TimeVector=[];
        			obj.TimeSpacing=[];
        			obj.LogData={};
				
				
				
        		else
        			NewTimeVec=[obj.TimeVector(1:oldPos-1),obj.TimeVector(oldPos+1:end)];
        			NewSpacing=[obj.TimeSpacing(1:oldPos-1),obj.TimeSpacing(oldPos+1:end)];
        			obj.TimeVector=NewTimeVec;
        			obj.TimeSpacing=NewSpacing;
        			NewCaster={obj.LogData{1:oldPos-1},obj.LogData{oldPos+1:end}};
				obj.LogData=NewCaster;
        			
				
        		       			
        		end
        		
        		
        		
        		
        	end	
        	
        	
        	
        	
        	
        	
        	
        	
        	function ForecastLog = getLog(obj,CastTime)
        		%Returns the log for a certain time, if the time is
        		%not existing the nearest lower time is used instead
        		
        		
        		if isempty(obj.TimeVector)
        			error('No log saved yet');
        		end
        		
        		
        		
        		FindInTime=obj.TimeVector==CastTime;
        			
        		if ~any(FindInTime)
        			%get the next lower time
        			ModTimeVec=obj.TimeVec-CastTime;
        			SmallerZero=find(ModTimeVec<0);
        				
        			if isempty(SmallerZero)
        				error('No forecast log before this time available');
        			end
        				
        				
        			FindInTime=SmallerZero(end);
        				
        		end
        			
        		
        		
        		
        		ForecastLog=obj.LogData{FindInTime};
        			
        			

        	
        	end
        	
        	
        	
        	
        	function Name = getName(obj)
        		%getter function for name
        		Name = obj.Name;
        		
        		%just a good opportunity to check if the name was 
        		%correctly set
        		if isempty(Name)
        			warning('No Name was defined')
        		end
        			
        		
        	end
        	
        	
        	function ID = getID(obj)
        		%getter function for ID
        		ID = obj.ID;
        		
        		%just a good opportunity to check if the name was 
        		%correctly set
        		if isempty(ID)
        			warning('No ID was defined')
        		end
        			
        		
        	end
        	
        	
        	
        	function setName(obj,Name)
        		%setter function for name
        		
        		if isempty(Name)
        			error('No Name defined')
        		end
        		
        		if ~ischar(Name)
        			error('Name has to be a string')
        		end
        		
        		obj.Name = Name;
        		
        	
        			
        		
        	end
        	
        	function idmatch = CheckIDs(obj,ForecastObj)
        		%checks if the ID of the forecast matches this one
        		
        		idmatch = strcmp(obj.ForecastID,ForecastObj.ID);
        		
        	end
        	
        	
        	function PortableStruct =  exportFullData(obj)
        		%meant to export everything for later use in a new object
        		%Only needed fields are currently exported, all which can
        		%be reproduced will not be exported. This may change if it
        		%turns out that the creation of this data is to computing
        		%intensive for the common cases.
        		
        		PortableStruct = struct('Name',obj.Name,...
        					'ID',obj.ID,...
        					'InfoType',obj.InfoType,...
        					'ForecastID',obj.ForecastID,...
        					'Version',obj.Version,...
        					'TimeVector',obj.TimeVector,...
        					'TimeSpacing',obj.TimeSpacing,...
        					'RealTimeMode',obj.RealTimeMode,...
        					'LogData',obj.LogData);
        		
        		
        	end
        	
        	
        	function importFullData(obj,PortableStruct)
        		%import a previous exported data set
        		%restores a forecast from a data dump.
        		%In future versions this may also updates a data dump
        		%to the most current version.
        		
        		%Set everything back in original state as done by the 
        		%constructor
        		obj.Version='0.9';
        		%init data
        	  	obj.TimeVector=[];
        		obj.TimeSpacing=[];
        		obj.RealTimeMode=false;
        		obj.LogData={};
        		
        		%internal variable
        		obj.TheBag={};
        		
        		
        		
        		%add the rest of the data directly (could be looped, but
        		%I prefere it like this, more control)
        		obj.Name=PortableStruct.Name;
        		obj.InfoType=PortableStruct.InfoType;
        		obj.ID=PortableStruct.ID;
        		obj.ForecastID=PortableStruct.ForecastID;
        		obj.TimeVector=PortableStruct.TimeVector;
        		obj.TimeSpacing=PortableStruct.TimeSpacing;
        		obj.RealTimeMode=PortableStruct.RealTimeMode;
        		obj.LogData=PortableStruct.LogData;
        		
        		
        	
        	end
        
        	
        
        	
        	
        end  
end

function [EqPerPeriod,TimeStep,MeanEq,VarEq] = calcEqPerTime(Datastore,Filterlist,StepSize)
	%Calculates the number of earthquakes per time intervall, and takes the 
	%average and variance over all available time intervalls. The result can
	%be used to estimate the negative binomial parameters from a poisson 
	%based forecast
	%StepSize is the length of the time intervall to be observed in days
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%check for filterlist
	if ~isempty(Filterlist)
		if ~islogical(Filterlist)
			Filterlist.changeData(Datastore);
			Filterlist.updateNoEvent;
			Selected=Filterlist.getSelected;
			Filterlist.PackIt;
		else
			Selected=Filterlist;
			
		end
		
	else
		Selected = true(Datastore.getRowCount,1);
	
	
	end
	

	%get Data
	eventStruct = Datastore.getFields([],Selected);
	
	%get lowest/highest time (round to the lowest/highest full day)
	LowDate=floor(min(eventStruct.dateNum));
	HiDate=ceil(max(eventStruct.dateNum));
	
	%produce Time intervalls
	RawTime=(LowDate:StepSize:HiDate)';
	
	%calculate number of eq per time period
	EqPerPeriod=NaN(numel(RawTime)-1,1);
	for i=1:numel(RawTime)-1
		EqPerPeriod(i)=sum(eventStruct.dateNum>=RawTime(i) & ...
				eventStruct.dateNum<RawTime(i+1));
	
	
	end
	
	TimeStep=RawTime(1:end-1);
	MeanEq=mean(EqPerPeriod);
	VarEq=var(EqPerPeriod);
	
	
	

end

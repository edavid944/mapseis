function updateForecastProject(ThePro)
	%simple function it brings the project up to date, by adding config
	%fields which are not set, just meant for old projects, more a sketch 
	%and script-like function
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
		
	ProConfNew={	'Benchmark',true;
			'SaveAddOnData',true};
			
			
	oldProjectConfig=ThePro.getParameter('ProjectConfig');
	
	%update structure
	for i=1:numel(ProConfNew(:,1))
		if isfield(oldProjectConfig,ProConfNew{i,1})
			if isempty(oldProjectConfig.(ProConfNew{i,1}))
				oldProjectConfig.(ProConfNew{i,1})=ProConfNew{i,2};
			end
		else
			oldProjectConfig.(ProConfNew{i,1})=ProConfNew{i,2};
		end
	end
	
	%store in project
	ThePro.setParameter('ProjectConfig',oldProjectConfig);
	
	
	%other "free roaming" properties
	NewProp={	'AutoExtend',false;
			'RegionExtend',[0.2,0.2]};
	
	for i=1:numel(NewProp(:,1))
		OldVal=	ThePro.getParameter(NewProp{i,1});
		
		if isempty(OldVal)
			ThePro.setParameter(NewProp{i,1},NewProp{i,2});
		end
	
	end
	
	
	
end

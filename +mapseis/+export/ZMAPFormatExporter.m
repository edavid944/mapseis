function dSelected=ZMAPFormatExporter(dataStore,rowIndices)
% getZMAPFormat : Extracts events in Nx9 array corresponding to ZMAP format

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 86 $    $Date: 2008-11-26 11:11:59 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

import mapseis.projector.*;

if nargin<2 || ischar(rowIndices)
    % If rowIndicies are unspecified then get all rows or
    % rowIndices = 'all'
    rowIndices = true(dataStore.getRowCount(),1);
end

selectedCount = nnz(rowIndices);
% ZMAP format matrices have columns
% [Lon Lat Year Month Day Mag Depth Hour Min]
zmapColCount = 9;

% Matrix to store selected events in ZMAP format
dSelected = zeros(selectedCount,zmapColCount);

dSelected(:,1:2) = getLocations(dataStore,rowIndices);
dSelected(:,6) = getMagnitudes(dataStore,rowIndices);
dSelected(:,7) = getDepths(dataStore,rowIndices);

% Get the time and date elements
raweventTimes = getTimes(dataStore,rowIndices);


%use decyear instead of year if possible
try 
	rawdecyear=dataStore.getUserData('DecYear');

catch
	rawdecyear = str2num(datestr(raweventTimes,'yyyy'));
	dataStore.setUserData('DecYear',rawdecyear);
	disp('Buffered Decyear')
end
zmapcata(:,3) = rawdecyear(rowIndices);


% Months
try 
	rawmonth=dataStore.getUserData('Month');

catch
	rawmonth = str2num(datestr(raweventTimes,'mm'));
	dataStore.setUserData('Month',rawmonth);
	disp('Buffered Month')
end            
zmapcata(:,4) = rawmonth(rowIndices);


% Days
try 
	rawday=dataStore.getUserData('Day');

catch
	rawday = str2num(datestr(raweventTimes,'dd'));
	dataStore.setUserData('Day',rawday);
	disp('Buffered Day')
end
zmapcata(:,5) = rawday(rowIndices);


% Hours
try
	rawhour=dataStore.getUserData('Hour');
	
catch
	rawhour = str2num(datestr(raweventTimes,'HH'));
	dataStore.setUserData('Hour',rawhour);
	disp('Buffered Hour')
end	
zmapcata(:,8) = rawhour(rowIndices);	


% Minutes
try
	rawminute=dataStore.getUserData('Minute');
	
catch
	rawminute=str2num(datestr(raweventTimes,'MM'));
	dataStore.setUserData('Minute',rawminute);
	disp('Buffered Minute')
end	
zmapcata(:,9) = rawminute(rowIndices);	


try
	rawsecond=dataStore.getUserData('Second');
		
catch
	rawsecond=str2num(datestr(raweventTimes,'SS'));
	dataStore.setUserData('Second',rawsecond);
	disp('Buffered Second')
end	
zmapcata(:,10) = rawsecond(rowIndices);	



% Years
%dSelected(:,3) = str2num(datestr(eventTimes,'yyyy'));
% Months
%dSelected(:,4) = str2num(datestr(eventTimes,'mm'));
% Days
%dSelected(:,5) = str2num(datestr(eventTimes,'dd'));
% Hours
%dSelected(:,8) = str2num(datestr(eventTimes,'HH'));
% Minutes
%dSelected(:,9) = str2num(datestr(eventTimes,'MM'));
%seconds
%dSelected(:,10) = str2num(datestr(eventTimes,'SS'));

%zmap does not like nan values
%replace nan with certain values

dSelected(isnan(dSelected(:,1)),1)=0;
dSelected(isnan(dSelected(:,2)),2)=0;
dSelected(isnan(dSelected(:,3)),3)=0;
dSelected(isnan(dSelected(:,4)),4)=1;
dSelected(isnan(dSelected(:,5)),5)=1;
dSelected(isnan(dSelected(:,6)),6)=0;
dSelected(isnan(dSelected(:,7)),7)=-99;
dSelected(isnan(dSelected(:,8)),8)=0;
dSelected(isnan(dSelected(:,9)),9)=0;
dSelected(isnan(dSelected(:,10)),10)=0;

end
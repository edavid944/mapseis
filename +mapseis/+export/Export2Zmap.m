function zmapcata = Export2Zmap(datastore,rowIndices,FocalSwitch)
%Works similar to the ZmapFormatExporter but adds focal mechanism if available
%or NaNs instead, later. An additional function is used, in case some modifiactions are 
%needed


%Added automatic buffer 

import mapseis.projector.*;

if nargin<2 || ischar(rowIndices)
    % If rowIndicies are unspecified then get all rows or
    % rowIndices = 'all'
    rowIndices = true(datastore.getRowCount(),1);
    FocalSwitch=false;
end

if nargin<3 
    % If rowIndicies are unspecified then get all rows or
    % rowIndices = 'all'
    FocalSwitch=false;
end

if isempty(FocalSwitch)
	FocalSwitch=false;
end

if isempty(rowIndices)
	rowIndices = true(datastore.getRowCount(),1);
end	

%build count array
countarray=1:datastore.getRowCount;

selectedCount = nnz(rowIndices);
% ZMAP format matrices have columns
% [Lon Lat Year Month Day Mag Depth Hour Min]
zmapColCount = 9;

% Matrix to store selected events in ZMAP format
zmapcata = zeros(selectedCount,zmapColCount);

zmapcata(:,1:2) = getLocations(datastore,rowIndices);
zmapcata(:,6) = getMagnitudes(datastore,rowIndices);
zmapcata(:,7) = getDepths(datastore,rowIndices);

% Get the time and date elements
eventTimes = getTimes(datastore,rowIndices);
raweventTimes = getTimes(datastore);

%use decyear instead of year if possible
try 
	rawdecyear=datastore.getUserData('DecYear');

catch
	rawdecyear = str2num(datestr(raweventTimes,'yyyy'));
	datastore.setUserData('DecYear',rawdecyear);
	disp('Buffered Decyear')
end
zmapcata(:,3) = rawdecyear(rowIndices);


% Months
try 
	rawmonth=datastore.getUserData('Month');

catch
	rawmonth = str2num(datestr(raweventTimes,'mm'));
	datastore.setUserData('Month',rawmonth);
	disp('Buffered Month')
end            
zmapcata(:,4) = rawmonth(rowIndices);


% Days
try 
	rawday=datastore.getUserData('Day');

catch
	rawday = str2num(datestr(raweventTimes,'dd'));
	datastore.setUserData('Day',rawday);
	disp('Buffered Day')
end
zmapcata(:,5) = rawday(rowIndices);


% Hours
try
	rawhour=datastore.getUserData('Hour');
	
catch
	rawhour = str2num(datestr(raweventTimes,'HH'));
	datastore.setUserData('Hour',rawhour);
	disp('Buffered Hour')
end	
zmapcata(:,8) = rawhour(rowIndices);	


% Minutes
try
	rawminute=datastore.getUserData('Minute');
	
catch
	rawminute=str2num(datestr(raweventTimes,'MM'));
	datastore.setUserData('Minute',rawminute);
	disp('Buffered Minute')
end	
zmapcata(:,9) = rawminute(rowIndices);	


if ~FocalSwitch
	%seconds
	try
		rawsecond=datastore.getUserData('Second');
		
	catch
		rawsecond=str2num(datestr(raweventTimes,'SS'));
		datastore.setUserData('Second',rawsecond);
		disp('Buffered Second')
	end	
	zmapcata(:,10) = rawsecond(rowIndices);	
	zmapcata(:,11:12) = ones(selectedCount,2)*NaN;
	zmapcata(isnan(zmapcata(:,10)),10)=0;

else
	%get focal mechanisms
	zmapcata(:,10:12) = getFocals(datastore,rowIndices);
end


zmapcata(isnan(zmapcata(:,1)),1)=0;
zmapcata(isnan(zmapcata(:,2)),2)=0;
zmapcata(isnan(zmapcata(:,3)),3)=0;
zmapcata(isnan(zmapcata(:,4)),4)=1;
zmapcata(isnan(zmapcata(:,5)),5)=1;
zmapcata(isnan(zmapcata(:,6)),6)=0;
zmapcata(isnan(zmapcata(:,7)),7)=-99;
zmapcata(isnan(zmapcata(:,8)),8)=0;
zmapcata(isnan(zmapcata(:,9)),9)=0;




zmapcata(:,13) = countarray(rowIndices);

end 
function CalcRes =  Calc_Master_Mc_bval(zmapCatalog,CalcConfig)
	%This reproduces part of the functionallity of the zmap function bdiff2 
	%and bdiff. The function is meant as a "Master" function and calls smaller
	%functions which do the actuall calculations and can be later on use standalone
	%It calculates a, b and Mc values and calculates the data for the cum. Num
	%plot and the eq per mag plot
	
	%Needed Input
	%	zmapCatalog: zmap formated catalog (projector can be used)
	%	CalcConfig: a Structur with the following calculation parameters:
	%		Mc_method:
	%		Use_bootstrap:
	%		Nr_bootstraps:
	%		Mc_correction:
	%		NoCurve:
	
	%Additional optional Parameter might be added in a later version
	%Regional_bval:  used for sign. bval calc.
	%Calc_sign_bval: if set to true sign bvals are calculated.
	
	
	%Given output:
	%	b_value:
	%	b_value_bootstrap:
	%	a_value:
	%	a_value_annual:
	%	Mc:
	%	Mc_bootstrap:
	%and if NoCurve=false also this vectors
	%	cum_eq_mag:
	%	eq_mag:
	%	mag_bins:
	%	b_val_line:
	%	Mc_point: 	same as Mc but with a y axis value to plot the marker at 
	%		  	right position.
	
	import mapseis.calc.Mc_calc.*;
	import mapseis.calc.b_val_calc.*;
	import mapseis.calc.signcalc.*;
	
	
	%make things easier and shorter, write structur variables in single variables
	Mc_method=CalcConfig.Mc_method;
	Use_bootstrap=CalcConfig.Use_bootstrap;
	Nr_bootstraps=CalcConfig.Nr_bootstraps;
	Mc_correction=CalcConfig.Mc_correction;
	NoCurve=CalcConfig.NoCurve;
	
	%set up all Output values and needed internal values
	CalcRes.b_value=[];
	CalcRes.b_value_bootstrap=[];
	CalcRes.a_value=[];
	CalcRes.a_value_annual=[];
	CalcRes.Mc=[];
	CalcRes.Mc_bootstrap=[];
	CalcRes.cum_eq_mag=[];
	CalcRes.eq_mag=[];
	CalcRes.mag_bins=[];
	CalcRes.b_val_line=[];
	CalcRes.Mc_point=[];
	
	
	CalcRes.sign_bvalue=[];
	CalcRes.sign_WinningModel=[];
	CalcRes.sign_likelihood1=[];
	CalcRes.sign_likelihood2=[];
	CalcRes.sign_akaike1=[];
	CalcRes.sign_akaike2=[];
	
	%new optional parameters
	try
		minimumEvents=CalcConfig.MinNumber;
	catch
		minimumEvents=10;
	end
	
	try
		binsize=CalcConfig.Mc_Binning;
	catch
		binsize=0.1;
	end
	
	try
		reg_bval=CalcConfig.Regional_bval;
	catch
		reg_bval=1;
	end
	
	try
		calc_sign=CalcConfig.Calc_sign_bval;
	catch
		calc_sign=false;
	end
	
	
	
	% enough events??
	if length(zmapCatalog(:,6)) >= minimumEvents
	
		% Added to obtain goodness-of-fit to powerlaw value
		%mcperc_ca3; %where are the values of this actually used????
		%was not found where in the org. code -> commented out
		
		[Mc] = calc_Mc(zmapCatalog, Mc_method, binsize, Mc_correction);
		l = zmapCatalog(:,6) >= Mc-(binsize/2);
		
		if length(zmapCatalog(l,:)) >= minimumEvents
			[MeanMag, b_value, Std_b_val, a_value] =  calc_bmemag(zmapCatalog(l,:), binsize);
		else
			Mc = nan; b_value = nan; Std_b_val = nan; a_value= nan;
		end;
		
		% Set standard deviation of a-value to nan;
		Std_a_val= nan; Std_Mc = nan;
		
		
		% Bootstrap uncertainties if needed
		if Use_bootstrap
			% Check Mc from original catalog
			l = zmapCatalog(:,6) >= Mc-(binsize/2);
		
			if length(zmapCatalog(l,:)) >= minimumEvents
				[Mc, Std_Mc, b_value, Std_b_val, a_value, Std_a_val, Mc_values, mBvalue] = ...
				calc_McBboot(zmapCatalog, binsize, Nr_bootstraps, Mc_method, minimumEvents, Mc_correction);
			
			else
				Mc = nan; Std_Mc = nan; b_value = nan; Std_b_val = nan; a_value= nan; Std_a_val= nan;
			
			end
		
		else
			% Set standard deviation of a-value to nan;
			Std_a_val = nan; Std_Mc = nan;
		end; % of bBst_button
		
		%go prf
		try
			[magco prf Mc90 Mc95]=mcperc_ca3(zmapCatalog);
		catch
			prf=NaN;
		end
		
		%calc sign_bval
		if calc_sign
			if ~isnan(b_value)|~isempty(b_value)
				sign_struct=winningmodel(zmapCatalog, reg_bval, b_value);
				
			else
				sign_struct.nWinningModel=NaN;
				sign_struct.bpreferred=NaN;
				sign_struct.likelihood1=NaN;
				sign_struct.likelihood2=NaN;
				sign_struct.nevents=NaN;
				sign_struct.akaike1=NaN;
				sign_struct.akaike2=NaN;
			
			end
		else
			sign_struct.nWinningModel=NaN;
			sign_struct.bpreferred=NaN;
			sign_struct.likelihood1=NaN;
			sign_struct.likelihood2=NaN;
			sign_struct.nevents=NaN;
			sign_struct.akaike1=NaN;
			sign_struct.akaike2=NaN;
		end		
		
	else
		Mc = nan; Std_Mc = nan; b_value = nan; Std_b_val = nan; a_value= nan; Std_a_val = nan;prf=NaN;
		%Std_Dev_b_val = nan;
		%Std_Dev_Mc = nan;
		
		sign_struct.nWinningModel=NaN;
		sign_struct.bpreferred=NaN;
		sign_struct.likelihood1=NaN;
		sign_struct.likelihood2=NaN;
		sign_struct.nevents=NaN;
		sign_struct.akaike1=NaN;
		sign_struct.akaike2=NaN;
		
	end
	%----------
	
	%anual a value
	annual_a = a_value-log10(max(zmapCatalog(:,3))-min(zmapCatalog(:,3)));
	if isempty(annual_a)
		annual_a=NaN;
	end	
	
	%pack the values
	CalcRes.b_value= b_value;
	CalcRes.b_value_bootstrap=Std_b_val;
	CalcRes.a_value=a_value;
	CalcRes.a_value_annual=annual_a;
	CalcRes.Mc=Mc;
	CalcRes.Mc_bootstrap=Std_Mc;
	CalcRes.PowerLawFit=prf;
	
	CalcRes.sign_bvalue=sign_struct.bpreferred;
	CalcRes.sign_WinningModel=sign_struct.nWinningModel;
	CalcRes.sign_likelihood1=sign_struct.likelihood1;
	CalcRes.sign_likelihood2=sign_struct.likelihood2;
	CalcRes.sign_akaike1=sign_struct.akaike1;
	CalcRes.sign_akaike2=sign_struct.akaike2;
	
	
	%curves are wanted so calculate them 
	if ~CalcConfig.NoCurve
	
		%needed for the bins
		maxMag = ceil(10*max(zmapCatalog(:,6)))/10;
		minMag = min(zmapCatalog(:,6));
		
		%eq per magnitude bin and cumsum of it
		[eq_per_mag,magbins] = hist(zmapCatalog(:,6),(minMag:0.1:maxMag));
		sum_eq_mag = cumsum(eq_per_mag); % N for M <=
		rev_eq_mag = eq_per_mag(length(eq_per_mag):-1:1);
		revsum_eq_mag = cumsum(eq_per_mag(length(eq_per_mag):-1:1));    % N for M >= (counted backwards)
		rev_magbins = (maxMag:-0.1:minMag);
		log_revsum_eq = log10(revsum_eq_mag);
		
		%%
		% create and draw a line corresponding to the b value
		mag_linpar = rev_magbins <= rev_magbins(1) & rev_magbins >= Mc-.0001;
		mag_zone=rev_magbins(mag_linpar);
		b_polydata = [ -1*b_value a_value];
		bval_line = polyval(b_polydata,mag_zone);
		log_bval_line = 10.^bval_line;
		
		%find position for the Mc point
		index_low=find(rev_magbins < Mc+.05 & rev_magbins > Mc-.05);
		Mc_point= [rev_magbins(index_low) revsum_eq_mag(index_low)*1.5];
		%add 0.2 to the x-coordinate for the text position
		
		
		%pack the curve data into the structur
		CalcRes.cum_eq_mag = revsum_eq_mag;
		CalcRes.eq_mag = rev_eq_mag;
		CalcRes.mag_bins = rev_magbins;
		CalcRes.b_val_line=[mag_zone', log_bval_line'];
		CalcRes.Mc_point= Mc_point;
	
	end
	%----
	
	
	
	

	
	
	
	
end

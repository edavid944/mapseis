function [xRange,zRange,calcRes]=Profile2DGridder(datastore,calcObj,TheParameters)
% CalcGridProfile : Calculate event properties averaged around grid points on a depth profile


% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

%modified for the new CalcWrapper type by David Eberhard 2010

%update to v1.2 similar to norm2DGridder

%	-Allow to select the selection mode
%			'Number': for fixed Number of events
%			'Radius': for fixed selection radius
%			'Both': for fixed Number without selecting events
%				further away than the specified radius
%	-SelectionNumber: Number of events selected
%	-DistanceSend: 	This allows to send the calculated distances from the
%			from the current grid point to the calculation
%			The calculation has to have a suitable parameter input 
%			for this.


%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation
ptSep = TheParameters.Gridspacing;
rad = TheParameters.SelectionRadius;
pline = TheParameters.ProfileLine;
width = TheParameters.ProfileWidth;
selected = TheParameters.SelectedEvents;
distMode=TheParameters.ParallelCalc;


if isfield(TheParameters,'SelectionNumber')
	Sel_Nr=TheParameters.SelectionNumber;
else
	Sel_Nr=100;
end

if isfield(TheParameters,'SelectionMode')
	Sel_Mode=TheParameters.SelectionMode;
else
	Sel_Mode='Radius';
end

if isfield(TheParameters,'DistanceSend')
	SendDist=TheParameters.DistanceSend;
else
	SendDist=false;
end	


% If only one value of point separation is defined then use same separation
% in both longitude and latitude
if numel(ptSep)<2
    ptSep(end+1) = ptSep(1);
end

%calculate the depth bBox
bBox = getLineDepthBbox(datastore,pline,width,selected);

% Ranges for grid
xRange = (bBox(1,1):ptSep(1):bBox(2,1))'; %on the lin
zRange = (bBox(1,2):ptSep(2):bBox(2,2))'; %Depth

% Element counts of rows and columns
rowCount = numel(xRange);
colCount = numel(zRange);

% Make a cell array to hold the calculation result
calcRes = cell(rowCount,colCount);

%% Extract the event data needed for the calculation

% Define 2D matrices of ranges
[XX,ZZ] = meshgrid(xRange,zRange);
elementCount = numel(XX);
% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
else
    nDoParallelCalc();
end
% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        calcRes = calcRes';
        matlabpool open
        parfor (elementIndex = 1:elementCount)
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), ZZ(elementIndex)];
            % Define the filter function about the grid point
            filterFun = ProfileFilterV2(thisGridPoint,rad,pline,width,selected,Sel_Nr,Sel_Mode,'RadDist');
            % Use the filter function to define the logical index vector
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected()&selected;
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
           
        end
        matlabpool close
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        calcRes = calcRes';
        for elementIndex = 1:elementCount
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), ZZ(elementIndex)];
            % Define the filter function about the grid point
            filterFun = ProfileFilterV2(thisGridPoint,rad,pline,width,selected,Sel_Nr,Sel_Mode,'RadDist');
            % Use the filter function to define the logical index vector
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected()&selected;
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
                      
            if rem(elementIndex,50) == 0
                waitbar(elementIndex./elementCount,h);
            end
        end
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
        % Close the waitbar
        close(h)
    end
    
    function glbox = getLineDepthBbox(datastore,pline,width,rowIndices)
				import mapseis.projector.*
				
				%get data from datastore
				
				[locSelected,locUnSelected] = getSliceProjections(datastore,pline,width,rowIndices);
				[depSelected, depUnSelected] = getDepths(datastore,rowIndices);
				
				glbox = [min(locSelected), min(depSelected);...
						 max(locSelected), max(depSelected)	];	
				
	end
	
	
				
end

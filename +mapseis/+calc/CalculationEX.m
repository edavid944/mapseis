classdef CalculationEX < handle
    %CalculationEX : Similar to Calculation, but with more features. It is a handle class
    %	Which allows to change parameters of the calculation 
    
	% Version 1, David Eberhard, 03 Jun 2009
	     
    
    properties (SetAccess = private)
        InputProjector
        CalcFun
    end
    
    properties
        ExtraArguments
        Name
    end
    
    methods
        function obj = CalculationEX(varargin)
            import mapseis.projector.*;
            
            % Parse the input arguments
            p = inputParser;
            funHandleTest = @(x) isa(x,'function_handle');
            cellorstruct = @(x) (isstruct(x) | iscell(x));
            p.addRequired('calcFun',funHandleTest);
            p.addOptional('inputProj',@getZMAPFormat,funHandleTest);
            p.addOptional('extraArgs',{},cellorstruct);
            p.addParamValue('Name','',@ischar);
            p.parse(varargin{:});
            
            % Extract the results of parsing
            obj.InputProjector = p.Results.inputProj;
            obj.CalcFun = p.Results.calcFun;
            obj.ExtraArguments = p.Results.extraArgs;
            obj.Name = p.Results.Name;
            %obj.Datastore = p.Results.DataStore;
        end
        
        function calcRes = calculate(obj,Datastore,logicalIndices)
            % Perform the calculation
            if nargin<3 & ~isempty(Datastore)
                logicalIndices = true(Datastore.getRowCount(),1);
            end
            if ~isempty(Datastore)
            	% Extract the data of interest
            	inputValues = obj.InputProjector(Datastore,logicalIndices);
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(inputValues,obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(inputValues,obj.ExtraArguments);	
            	end
            
            else
            	
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(obj.ExtraArguments);	
            	end
            

            end	
        end    
        
        
        
        function calcRes = calculate_PD(obj,Datastore,logicalIndices,PointDist)
            % Perform the calculation, but allows to send PointDistance to the calculation
            % Needed in case where the Resolution in a map is calculated or similar
            if nargin<3 & ~isempty(Datastore)
                logicalIndices = true(Datastore.getRowCount(),1);
            end
            if ~isempty(Datastore)
            	% Extract the data of interest
            	inputValues = obj.InputProjector(Datastore,logicalIndices);
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(inputValues,PointDist(logicalIndices),obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(inputValues,PointDist(logicalIndices),obj.ExtraArguments);	
            	end
            
            else
            	
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(PointDist(logicalIndices),obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(PointDist(logicalIndices),obj.ExtraArguments);	
            	end
            

            end	
        end    
        
        
        
        
        function calcRes = calculate_LogicSend(obj,Datastore,logicalIndices)
            % Perform the calculation, but allows to send Logical indices to the calculation
            % Needed in case where a map of a subset is calculated or similar
            
            if nargin<3 & ~isempty(Datastore)
                logicalIndices = true(Datastore.getRowCount(),1);
            end
            
            if ~isempty(Datastore)
            	% Extract the data of interest
            	inputValues = obj.InputProjector(Datastore,logicalIndices);
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(inputValues,logicalIndices,obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(inputValues,logicalIndices,obj.ExtraArguments);	
            	end
            
            else
            	
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(logicalIndices,obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(logicalIndices,obj.ExtraArguments);	
            	end
            

            end	
        end    
        
        
        function calcRes = calculate_LogicDistance(obj,Datastore,logicalIndices,PointDist)
            % Perform the calculation, but allows to send PointDistance and logical indices to the calculation
            % Needed in case where the Resolution in a map is calculated or similar
            if nargin<3 & ~isempty(Datastore)
                logicalIndices = true(Datastore.getRowCount(),1);
            end
            if ~isempty(Datastore)
            	% Extract the data of interest
            	inputValues = obj.InputProjector(Datastore,logicalIndices);
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(inputValues,PointDist(logicalIndices),logicalIndices,obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(inputValues,PointDist(logicalIndices),logicalIndices,obj.ExtraArguments);	
            	end
            
            else
            	
            	% Actually do the calculation
            	if iscell(obj.ExtraArguments)
            		calcRes = obj.CalcFun(PointDist(logicalIndices),logicalIndices,obj.ExtraArguments{:});
            	else
            		calcRes = obj.CalcFun(PointDist(logicalIndices),logicalIndices,obj.ExtraArguments);	
            	end
            

            end	
        end    
        
        
        function ChangeValues(obj,Extravalues)
        	%allows changes to the ExtraArguments, if a cell is used, the whole the cell is replaced
        	
        	if iscell(Extravalues) & iscell(obj.ExtraArguments)
        		obj.ExtraArguments = Extravalues;
        	
        	elseif isstruct(Extravalues) & isstruct(obj.ExtraArguments)
        		
        		%get all fields of Extravalues
        		mayfield = fields(Extravalues);
        			
        		for i=1:numel(mayfield)
        			%fields not in ExtraArguments will not be added
        			if isfield(obj.ExtraArguments,mayfield{i})
        				obj.ExtraArguments.(mayfield{i}) = Extravalues.(mayfield{i});
        			end	
        		end
        	else 
        		disp('Error, Type of ExtraArguments and input does not match');	
        	end	 
        end

       			 	 	
        		                            
    end
    
end


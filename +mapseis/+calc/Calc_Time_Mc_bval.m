function CalcRes =  Calc_Time_Mc_bval(datastore,selected,CalcConfig)
	%This function works as Gate to various Mc-bval calculations.
	%It calls Calc_Master_bval and calculates a single values and does
	%optionally calculate also a map with the Earthquake density and b-values
	
	import mapseis.calc.*;
	import mapseis.projector.*;
	import mapseis.util.importfilter.cellArrayToPlane;
	
	%first do the normal calculation (pass-throught)
	CalcRes =  Calc_Master_Mc_bval(getZMAPFormat(zmapCatalog,selected),CalcConfig);
	
	
	%check if density and/or b-value map is wanted 
	if CalcConfig.MappingMode&CalcConfig.bvalMapping
	
		%Needs to configuered from the outside.
		MapConfig=CalcConfig.CalcParameter;	
		DenseConfig.CalcFunction=@mapseis.calc.Calc_Master_Mc_bval;
		DenseConfig.FunctionConfig=MapConfig;
		DenseConfig.ProfileSwitch=MapConfig.ProfileSwitch;
		
		CalcObject = mapseis.calc.CalculationEX(...
		 		@mapseis.calc.DensityAdder,@mapseis.projector.getZMAPFormat,...
		   		DenseConfig,'Name','BvalCalc');		
		
		
		MapNeed=true;
		
	
	elseif CalcConfig.MappingMode&~CalcConfig.bvalMapping
		%Needs to configuered from the outside.
		MapConfig=CalcConfig.CalcParameter;	
		DenseConfig.CalcFunction=[];
		DenseConfig.FunctionConfig=MapConfig;
		DenseConfig.ProfileSwitch=MapConfig.ProfileSwitch;
		
		CalcObject = mapseis.calc.CalculationEX(...
		 		@mapseis.calc.DensityAdder,@mapseis.projector.getZMAPFormat,...
		   		DenseConfig,'Name','BvalCalc');		
	
	
	
		MapNeed=true;
		
	else
		MapNeed=false;
	
		
	end
	
	if MapNeed
		if CalcConfig.ProfileSwitch
			GridConfig=CalcConfig.GridConfig;
			GridConfig.SelectedEvents=selected;
			[xRange,yRange,MapRes]=Profile2DGridder(Datastore,CalcObject,GriConfig);
			
		else
			%The use of the Datastore without projector might be memory consuming, so it might be changed
			%if needed later.
			GridConfig=CalcConfig.GridConfig;
			GridConfig.SelectedEvents=selected;
			[xRange,yRange,MapRes]=norm2DGridder(Datastore,CalcObject,GridConfig)
		end
	
		RawRes=cellArrayToPlane(MapRes);
				
		    		
		fields=fieldnames(RawRes);
		for i=1:numel(fields)
			MapCalcRes.(fields{i})=cell2mat(RawRes.(fields{i}));					
		end
				
		[xx yy]=meshgrid(xRange,yRange);
		le=numel(xx);
		xvec=reshape(xx,le,1);
		yvec=reshape(yy,le,1);
		MapCalcRes.X=xRange;
		MapCalcRes.Y=yRange;
		MapCalcRes.Xmeshed=xvec;
		MapCalcRes.Ymeshed=yvec;
		MapCalcRes.SelectedEq=selected;
		
		CalcRes.MappingResult=MapCalcRes;
	
	
	end
	
	
	
	
	
	
	
	

	
end

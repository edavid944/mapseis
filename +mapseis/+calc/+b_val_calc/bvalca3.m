function [b_val_edit, magco, std_b_edit, a_value ,me, mer , me2, pr] =  bvalca3(zmapCatalog,old_bval);
	%Arrrghhhh
	%global  backcat fs12 fs10 n les teb t0b no1 bo1 xt3 bvalsum3
	
	
	%disp('This is /src/bvalca3.m');
	
	
	%This function was a bloody mess!!!!!!!!
	%And still is dirty, it should be replaced later by a more general function
	%For a lot of the variables the function they fullfile is unknown.
	
	%inb2 not used here, why is it in the parameters entry then?
	
	import mapseis.calc.stat_func.wls;
	
	
	if nargin<2
		%bo1 seems to be the value bv (b_value) set in some calculations outside
		 %it often seems to be the b_value of the last calculation.
		 %As this function is incomplete and will probably replace late with a more
		 %general function the bo1 value will be set to the already calculated value bv
		 %if not specified
		 bo1=[];
	else
		bo1=old_bval;
	end	 
	
	binsize = 0.1; %ex dm1
	pr = nan; me2 = nan; mer = nan; a_value = nan; me = nan; std_b_edit = nan; magco = nan; b_val_edit = nan; 
	maxmag = max(zmapCatalog(:,6));
	minmag = min(zmapCatalog(:,6)); %ex mima
	%replacement for t2b and t0b
	last_time=max(zmapCatalog(:,3));
	first_time=min(zmapCatalog(:,3));
	number_events=numel(zmapCatalog(:,1)); % replaces no1
	%It is not entirely clear if no1 should always be set to the number of events
	
	if minmag > 0 
		minmag = 0 ; 
	end
	
	try % if an error occures, set values to nan 
	   
		% number of mag units
		Nr_Mag_Unit = round(maxmag*10)+1;
		
		%init the arrays
		eq_per_mag = zeros(1,Nr_Mag_Unit); %ex bval
		sum_eq_mag = zeros(1,Nr_Mag_Unit);	 %ex bvalsum	
		revsum_eq_mag = zeros(1,Nr_Mag_Unit); %ex bvalsum3	
		
		%magbins was xt2
		%rev_magbins was xt3
		
		[eq_per_mag,magbins] = hist(zmapCatalog(:,6),(minmag:binsize:maxmag));
		sum_eq_mag = cumsum(eq_per_mag);                        % N for M <=
		revsum_eq_mag = cumsum(eq_per_mag(length(eq_per_mag):-1:1));    % N for M >= (counted backwards)
		rev_magbins = (maxmag:-binsize:minmag);
		
		log_revsum_eq = log10(revsum_eq_mag); %ex backg_ab
		dif_log_revsum = [ 0 diff(revsum_eq_mag) ]; %ex diffb
		
		%maxmimum difference (curvature)
		countmaxDif = find(dif_log_revsum == max(dif_log_revsum)); %ex i
		countmaxDif = max(countmaxDif);
		
		%maximum number of earthquake per bin
		countmaxEq = find(eq_per_mag == max(eq_per_mag)); %ex i2
		
		magco = max(magbins(countmaxEq));
		
		% if no automatic etimate of Mcomp %not allowed at the moment
		%if inb1 == 2
		% 	 i = length(rev_magbins)-10*min(zmapCatalog(:,6));
		% 	 if i > length(rev_magbins);
		% 	 	i = length(rev_magbins)-1 ; 
		% 	 end
		%end
		
		%why????, erease for the moment
		%found out, they want to use the biggest magnitude which is in 
		%reverse magnitude bins the first element
		countmaxEq = 1;                 
		
		par2 = 0.1 * max(revsum_eq_mag);
		par3 = 0.12 * max(revsum_eq_mag);
		M1b = [];
		M1b = [rev_magbins(countmaxDif) revsum_eq_mag(countmaxDif)];
		 
		M2b = [];
		M2b =  [rev_magbins(countmaxEq) revsum_eq_mag(countmaxEq)];
		
		%select all bigger than the max curvature and smaller then the maximum magnitude
		selection = rev_magbins >= M1b(1) & rev_magbins <= M2b(1); %ex ll
		
		selected_bins = rev_magbins(selection); %ex x
		selected_numbers = log_revsum_eq(selection); %ex y
		
		%[p,s] = polyfit2(x,y,1);                   % fit a line to background
		%weighed least square for coeff estimation
		[a_value b_value std_b] = wls(selected_bins',selected_numbers');
		%aw= a_value, bw=b_value, ew=std_b
		
		%build a line from the estimated coeffs and "reverse" the logarithmen
		%line seems not be used here -> comment it out
		polab = [ b_value a_value]; %ex p
		%pol_Line = polyval(polab,selected_bins); %ex f
		%pol_Line = 10.^pol_Line;
		
		rt = (last_time - first_time)/(10.^(polyval(polab,6.0)));
		r = corrcoef(selected_bins,selected_numbers);
		r = r(1,2);
		%std_backg = std(y - polyval(p,x));      % standard deviation of fit
		
		
		magselect = zmapCatalog(:,6) >= M1b(1) & zmapCatalog(:,6) <= M2b(1); %ex l
		les = (mean(zmapCatalog(magselect,6)) - M1b(1))/binsize;
		
		%aval=p(1,2); %av
		polab=-polab(1,1);
		b_val_edit=fix(100*polab)/100; %ex bv
		std_b_edit=fix(100*std_b)/100;
		
		 % calculate probability
		 if isempty(bo1)
		 	bo1=b_val_edit;
		 end
		 
		 b2 = polab; 
		 n2 =  M1b(2);
		 n = number_events+n2;
		 da = -2*n*log(n) + 2*number_events*log(number_events+n2*bo1/b2) ...
		 		+ 2*n2*log(number_events*b2/bo1+n2) -2;
		 pr = (1  -  exp(-da/2-2))*100;
		 % if bo1 > b2 ; pr = -pr; end
	 
	catch
		 disp('Error while evaluating bvalca3 - set to nan');
		 b_val_edit= nan;  magco= nan;  std_b_edit = nan ;  a_value = nan; me = nan;  mer = nan ; me2 = nan ; pr= nan ;
		   
		   
	end  % try 
	

end   


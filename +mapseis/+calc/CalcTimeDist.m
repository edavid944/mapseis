function timeDist=CalcTimeDist(evTimes)
	% CalcTimeDist : cumulative distribution function of event times
	% Returns the cumulative number of events as a function of dateNum
	
	% Copyright 2007-2008 The MathWorks, Inc.
	% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
	% Author: Matt McDonnell
	
	% Sort the events in time order (may not be necessary if events are
	% extracted in sorted order from database)
	
	%Not needed, it is not returned anyway
	%evTimes = sort(evTimes);
	
	% Total number of events
	numEvents = numel(evTimes);
	
	timeDist = 1:numEvents;
	
	% timeDist = timeseries(1:numEvents,evTimes,'name','Event Time Distribution');
	% timeDist.DataInfo.Interpolation=tsdata.interpolation('zoh');
	% timeDist.DataInfo.Units='Total number of events';
end
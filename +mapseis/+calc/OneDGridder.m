function [varRange,calcRes]=OneDGridder(datastore,calcObj,TheParameters,PlayField,UserData)
%Uses a function for all bins along a certain variable (set with PlayField)

%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation

%Unpack the parameters
intervall=TheParameters.Intervall	
selected=TheParameters.SelectedEvents;
distMode=TheParameters.ParallelCalc;
BinMode=TheParameters.BinningType;


try
	binsize=TheParameters.Binsize;
catch
	binsize=[];
end

try
	BinNumber=TheParameters.Binnumber;
catch
	BinNumber=[];
end

try
	Overlap=TheParameters.Overlap;
catch
	Overlap=0;
end

try
	MinNumber=TheParameters.MinNumber;
catch
	MinNumber=10;
end

%for calculate_LogicSend
try
	LogicSend=TheParameters.LogicSend;
catch
	LogicSend=false;
end



% get Parameter to select from datastore
if ~UserData
	SelectionParameter=datastore.getFields(PlayField);
else
	SelectionParameter=datastore.getUserData(PlayField);
end

%set intervall if necessary
if isempty(intervall)
	intervall(1)=min(SelectionParameter)
	intervall(2)=max(SelectionParameter);
end

%build the Range array 
switch BinMode
	case 'fixrange'
		% Range for grid
		varRange = (intervall(1):binsize-Overlap*binsize:intervall(2))';

		% Element counts 
		Count = numel(varRange);

	case 'fixnumber'
		%get everything in the whole intervall
		inIntervall=SelectionParameter>=intervall(1) & SelectionParameter<=intervall(2);
		
		%build new SelectionArray with every entry not in the intervall set to nan
		newParam=SelectionParameter;
		newParam(~inIntervall)=NaN;
		
		%now sort this array 
		[sortVal sortedSelect] = sort(newParam);
		sortedSelect=sortedSelect(~isnan(sortVal));
		
		
		varRange=(1:BinNumber/Overlap:length(sortedSelect)-BinNumber)';
		Count=numel(varRange);
		
		%needed to transform the sort into a logical selection
		zeroArray=zeros(size(SelectionParameter));
end	

% Make a cell array to hold the calculation result
calcRes = cell(Count);



% Do the calculation
if ~distMode
    nDoSerialCalc();
    disp('Calculate Serial')
else
    nDoParallelCalc();
    disp('Calculate Parallel')

end


    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
       calcRes = calcRes';
        
       switch BinMode
        	case 'fixrange'
			matlabpool open;
			parfor (elementIndex = 1:Count)
			    %get elements in the currentbin
			    inBins= SelectParameter>=(varRange(elementIndex)-binsize/2) & ...
					SelectParameter<(varRange(elementIndex)+binsize/2);
			    logicalIndices = inBins & selected;
			    
			    % Perform the calculation
			    if sum(logicalIndices)<MinNumber
					calcRes{elementIndex} = NaN;
					calcRes{elementIndex}.NumberData=sum(logicalIndices);	
			    else	
			    		calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
			    		calcRes{elementIndex}.NumberData=sum(logicalIndices);
			    end
			    %pause(0.2);
			end
			matlabpool close;
			
        	case 'fixnumber'
        		matlabpool open;
			parfor (elementIndex = 1:Count)
				%get the events in the bin
				inBins = zeroArray;
				inBins(sortedSelect(varRange(elementIndex):varRange(elementIndex)+BinNumber)=true;
				logicalIndices = inBins & selected;
				
				if sum(logicalIndices)<MinNumber
					calcRes{elementIndex} = NaN;
					calcRes{elementIndex}.NumberData=sum(logicalIndices);	
				else	
					calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
					calcRes{elementIndex}.NumberData=sum(logicalIndices);
				end	
			end	
			matlabpool close;
	end
	
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        calcRes = calcRes';
        switch BinMode
        	case 'fixrange'
			for elementIndex = 1:elementCount
			    %get elements in the currentbin
			    inBins= SelectParameter>=(varRange(elementIndex)-binsize/2) & ...
					SelectParameter<(varRange(elementIndex)+binsize/2);
			    logicalIndices = inBins & selected;
			    
			    if sum(logicalIndices)<MinNumber
			    		calcRes{elementIndex} = NaN;
					calcRes{elementIndex}.NumberData=sum(logicalIndices);	
			    else			
			    		% Perform the calculation
			    		if LogicSend
			    			calcRes{elementIndex} = calcObj.calculate_LogicSend(datastore,logicalIndices);
			    		
			    		else
			    			calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
			    		end
			    		calcRes{elementIndex}.NumberData=sum(logicalIndices);
			    end
			    
			    if rem(elementIndex,50) == 0
				waitbar(elementIndex./Count,h);
			    end
			end
			
		case 'fixnumber'
			for elementIndex = 1:elementCount
			    %get the events in the bin
			    inBins = zeroArray;
			    inBins(sortedSelect(varRange(elementIndex):varRange(elementIndex)+BinNumber)=true;
			    logicalIndices = inBins & selected;
			    
			    if sum(logicalIndices)<MinNumber
			    		calcRes{elementIndex} = NaN;
					calcRes{elementIndex}.NumberData=sum(logicalIndices);	
			    else			
			    		% Perform the calculation
			    		if LogicSend
			    			calcRes{elementIndex} = calcObj.calculate_LogicSend(datastore,logicalIndices);
			    		
			    		else
			    			calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
			    		end
			    		calcRes{elementIndex}.NumberData=sum(logicalIndices);
			    end
			    
			    if rem(elementIndex,50) == 0
				waitbar(elementIndex./Count,h);
			    end
			end
		
	end	
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
        % Close the waitbar
        close(h)
    end

end

function CalcRes = Calc_bval_nofit(zmapcata, CalcConfig)
	% This function is a translation of the old bvanofit from zmap
	% Original Header Commentary:
		%---------------------------------------------------------------------
		%   Calculates Freq-Mag functions (b-value) for two time-segments
		%   finds best fit to the foreground for a modified background
		%   assuming a change in time of the following types:
		%   Mnew = Mold + d     , i.e. Simple magnitude shift
		%   Mnew = c*Mold + d   , i.e. Mag stretch plus shift 
		%   Nnew = fac*Nold     , i.e. Rate change (N = number of events)
		%                                      R. Zuniga IGF-UNAM/GI-UAF  6/94
		%--------------------------------------------------------------------- 
	
	% Changes to the original code:
	% - It is now a function
	% - No plotting and GUI stuff in here
	% - only one Intervall is calculated in this Code, two objects using the same code are 
	%   needed to calculate the traditional two rate comparison. Due to this setting comparing
	%	more than two intervalls is possible.
	% - Calculation of combined statistical values have to been done outside of this code
	% - All Configurations and Results are written into a structur.
	
	%min & max magnitudes
	maxmag = max(zmapcata(:,6));
	minmag = min(zmapcata(:,6));
	
	%min & max time
	firsttime = min(zmapcata(:,3));
	lasttime = max(zmapcata(:,3));
	tdiff = round(firsttime - lasttime);
	
	% number of mag units
	nmagu = ceil((maxmag*10)+1);
	
	%init the needed arrays 
	bval = zeros(1,nmagu);
	bvalsum_norm = zeros(1,nmagu);
	bvalsum_reverse = zeros(1,nmagu);
	log_bvalsum_norm = [];
	log_bvalsum_reverse = [];
	selected_catalog = [];
     
	%get time_intervall
	time1 = CalcConfig.time1;
	time2 = CalcConfig.time2;
	
	%switch the times if necessary
	if time1>time2
		temp=time1;
		time1=time2;
		time2=temp;
	end
	
	time_int = time2 - time1;              
	
	%timestep for the bins
	tstep = CalcConfig.timestep;

	%select the events in the time intervall
	logInd = zmapcata(:,3) > time1 & zmapcata(:,3) < time2 ;
	selected_catalog =  zmapcata(logInd,:);
	
	%bvalues
	[bval,bval_bins] = hist(selected_catalog(:,6),(minmag:0.1:maxmag));
	bval = bval /time_int;                      % normalization
	
	%cumnum bvalues
	bvalsum_norm = cumsum(bval);                        % N for M <=
	bvalsum_reverse = cumsum(bval(length(bval):-1:1));    % N for M >= (counted backwards)
	bval_bins_reverse = (maxmag:-0.1:minmag);
	
	%logarithmen of cumnums	
	log_bvalsum_norm = log10(bvalsum_norm);
	log_bvalsum_reverse = log10(bvalsum_reverse);
	
	%cumulative number
	[cumux cumux_bins] = hist(zmapcata(logInd,3),time1:tstep/365:time2);			
	 meanCumux = mean(cumux);
	 varCumux = cov(cumux);

	%TO BE DONE IN THE WRAPPER? or in a addition call element?
	%zscore = (mean1 - mean2)/(sqrt(var1/length(cumux)+var2/length(cumux2)));
	%change in percent
	%R1 = length(backg(:,1))/(t2p(1)-t1p(1))
	%R2 = length(foreg(:,1))/(t4p(1)-t3p(1));
	%change = -((R1-R2)/R1)*100
	 
	 
	%now pack the results into a Result structur
	CalcRes.selected_catalog = selected_catalog;
	CalcRes.bvalue = bval;
	CalcRes.bvalue_sum_norm = bvalsum_norm;
	CalcRes.bvalue_sum_reverse = bvalsum_reverse;
	CalcRes.logbvalue_sum_norm = log_bvalsum_norm;
	CalcRes.logbvalue_sum_reverse = log_bvalsum_reverse;
 	CalcRes.bval_bins=bval_bins;
 	CalcRes.bval_bins_reverse=bval_bins_reverse;
 	CalcRes.CumulativeNumber = cumux;
 	CalcRes.CumNum_bins = cumux_bins;
 	CalcRes.MeanCumNum = meanCumux;
 	CalcRes.VarianceCumNum = varCumux;
 	
 

	





end
function [result]=CalcStressInv_node(focals);
	% function [result]=CalcStressInv_node(params,mCatalog)
	% --------------------------------------------------------
	% Function to calculate stress inversion at a specific node
	%
	% Incoming variables:
	% mCatalog     : current earthquake catalog
	% params       : See gui_CalcStressInv for parameters
	% 
	% Outgoing variable:
	% result.
	%
	% Author: J. Woessner
	% j.woessner@sed.ethz.ch
	% last update: 16.02.2005
	
	% Init variable path has to be changed 
	% modified for Mapseis 21.7.2010 DE
	
	%params delete, not needed in the whole function
	
	
	%----------------
	%Some part in the conventional stress calculation algorithmen might need change,
	%as the 'load' on a ascii filecommand will cause error if one line contains a error message, 
	%which can happen -> needs a special inputfilter, which does the input line by line.
	%----------------
	
	
	
	import mapseis.calc.StressInv.*;
	import mapseis.special_import.import_slickoput;
	
	fs=filesep;
	result=[];
	sZmapPath = ['.',fs,'AddOneFiles',fs,'StressInversion',fs];
	
	
	%get the computer type
	cputype = computer;
	% Array of focal mechanisms: dip direction, dip, rake
	%mFPS = [mCatalog(:,10:12)];
	mFPS=focals;
	[nRow,nCol]=size(mFPS);
	if nRow >= 999
	    mFPS = mFPS(1:998,:);
	end;
	dCount=numel(mFPS(:,1));
	% Do inversion using A. Michael code
	% Create file for inversion
	sPath = ['.',fs,'Temporary_Files'];
	
	%generate name (needed for parallel processing
	rawName=['data2_',num2str(round(rand(1)*100))];
	
	fid = fopen([sPath,fs,rawName],'w');
	str = ['Inversion data'];str = str';
	fprintf(fid,'%s  \n',str');
	fprintf(fid,'%7.3f  %7.3f  %7.3f\n',mFPS');
	fclose(fid);
	% slick calculates the best solution for the stress tensor according to
	% Michael(1987): creates data2.oput
	%sPath = pwd;
	
	%unix([sZmapPath 'external/slick ' sPath '/data2 ']);
	
	%added support for different architectures
	if strcmp(cputype,'GLNX86') == 1    
		unix([sZmapPath 'slick_linux ' sPath fs rawName ' ']);
	elseif strcmp(cputype,'MAC') == 1
		unix([sZmapPath 'slick_macppc ' sPath fs rawName ' ']);    
	elseif strcmp(cputype,'MACI') == 1
		unix([sZmapPath 'slick_maci ' sPath fs rawName ' ']);
	elseif strcmp(cputype,'MACI64') == 1
		unix([sZmapPath 'slick_maci ' sPath fs rawName ' ']); 
		%maci64 version seems  not to work, have to check the compiled routine
	elseif strcmp(cputype,'PCWIN') == 1
		dos([sZmapPath 'slick.exe ' sPath fs rawName ' ']);
	elseif strcmp(cputype,'PCWIN64') == 1
		dos([sZmapPath 'slick.exe ' sPath fs rawName ' ']);
		%untested, I don't know if the 32bit version works
	end

	% Get data from data2.oput
	sFilename = [sPath,fs,rawName,'.oput'];
	% Calculate avareage angle between tangential traction predicted by best
	% stress tensor and the slip direction
	[result.fBeta, result.fStdBeta, result.fTauFit, result.fAvgTau, result.fStdTau] = import_slickoput(sFilename);
	
	% Delete existing from earlier runs data2.slboot
	sData2 = [sPath,fs,rawName,'.slboot'];
	delete(sData2);
	
	% Stress tensor inversion
	%unix([sZmapPath 'external/slfast ' sPath '/data2 ']);
	
	%added support for different architectures
	if strcmp(cputype,'GLNX86') == 1    
		unix([sZmapPath 'slfast_linux ' sPath fs rawName ' ']);
	elseif strcmp(cputype,'MAC') == 1
		unix([sZmapPath 'slfast_macppc ' sPath fs rawName ' ']);    
	elseif strcmp(cputype,'MACI') == 1
		unix([sZmapPath 'slfast_maci ' sPath fs rawName ' ']);
	elseif strcmp(cputype,'MACI64') == 1
		unix([sZmapPath 'slfast_maci ' sPath fs rawName ' ']);
		%maci64 seems  not to work, have to check the compiled routine
	elseif strcmp(cputype,'PCWIN') == 1
		dos([sZmapPath 'slfast.exe ' sPath fs rawName ' ']);
	elseif strcmp(cputype,'PCWIN64') == 1
		dos([sZmapPath 'slfast.exe ' sPath fs rawName ' ']);
		%untested, I don't know if the 32bit version works
	end
	
	
	
	
	
	sGetFile = [sPath,fs,rawName,'.slboot'];
	try
		data2=load(sGetFile);
		% Description of data2
		% Line 1: Variance S11 S12 S13 S22 S23 S33 => Variance and components of
		% stress tensor (S = sigma)
		% Line 2: Phi S1t S1p S2t S2p S3t S3p
		% Phi is relative size S2/S1, t=trend, p=plunge (other description)
		result.fVariance = data2(1,1);
		result.fS11 = data2(1,2);
		result.fS12 = data2(1,3);
		result.fS13 = data2(1,4);
		result.fS22 = data2(1,5);
		result.fS23 = data2(1,6);
		result.fS33 = data2(1,7);
		result.fPhi = data2(2,1);
		result.fS1Trend = data2(2,2);
		result.fS1Plunge = data2(2,3);
		result.fS2Trend = data2(2,4);
		result.fS2Plunge = data2(2,5);
		result.fS3Trend = data2(2,6);
		result.fS3Plunge = data2(2,7);
		result.dCount = dCount;
		% Number of events
		[nY, nX] = size(focals(:,1));
		result.nNumEvents = nY;
		
		% Compute diversity
		[fRms] = calc_FMdiversity(focals(:,1),focals(:,2),focals(:,3));
		result.fRms = fRms;
		
		% Compute style of faulting
		[fAphi] = calc_FaultStyle(result);
		result.fAphi = fAphi;
		
	catch
		result.fVariance = nan;
		result.fS11 = nan;
		result.fS12 = nan;
		result.fS13 = nan;
		result.fS22 = nan;
		result.fS23 = nan;
		result.fS33 = nan;
		result.fPhi = nan;
		result.fS1Trend = nan;
		result.fS1Plunge = nan;
		result.fS2Trend = nan;
		result.fS2Plunge = nan;
		result.fS3Trend = nan;
		result.fS3Plunge = nan;
		result.nNumEvents = nan;
		result.fRms = nan;
		result.fAphi = nan;
		result.dCount = nan;
		disp('cell set to nan');
	end	

end
classdef Calculation
    %CALCULATION : calculation object to act on earthquake data
    %   Defines the projector to convert the datastore into data that can
    %   be processed by the calculation function, the calculation function
    %   itself and any extra arguments
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
    % Author: Matt McDonnell        
    
    properties (SetAccess = private)
        InputProjector
        CalcFun
    end
    
    properties 
        ExtraArguments
        Name
    end
    
    methods
        function obj = Calculation(varargin)
            import mapseis.projector.*;
            
            % Parse the input arguments
            p = inputParser;
            funHandleTest = @(x) isa(x,'function_handle');
            p.addRequired('calcFun',funHandleTest);
            p.addOptional('inputProj',@getZMAPFormat,funHandleTest);
            p.addOptional('extraArgs',{},@iscell);
            p.addParamValue('Name','',@ischar);
            p.parse(varargin{:});
            
            % Extract the results of parsing
            obj.InputProjector = p.Results.inputProj;
            obj.CalcFun = p.Results.calcFun;
            obj.ExtraArguments = p.Results.extraArgs;
            obj.Name = p.Results.Name;
        end
        
        function calcRes = calculate(obj,dataStore,logicalIndices)
            % Perform the calculation
            if nargin<3
                logicalIndices = true(dataStore.getRowCount(),1);
            end
            % Extract the data of interest
            inputValues = obj.InputProjector(dataStore,logicalIndices);
            % Actually do the calculation
            calcRes = obj.CalcFun(inputValues,obj.ExtraArguments{:});
        end                                
    end
    
end


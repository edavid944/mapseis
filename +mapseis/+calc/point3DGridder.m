function [xRange,yRange,zRange,calcRes]=point3DGridder(datastore,calcObj,TheParameters)
% CalcGrid : Calculate event properties averaged around grid points
%bBox,ptSep,rad,selectedEvents,calcObj,varargin)
% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

%modified for the new CalcWrapper type by David Eberhard 2010

%v1.2 changes
%	-Allow to select the selection mode
%			'Number': for fixed Number of events
%			'Radius': for fixed selection radius
%			'Both': for fixed Number without selecting events
%				further away than the specified radius
%	-SelectionNumber: Number of events selected
%	-DistanceSend: 	This allows to send the calculated distances from the
%			from the current grid point to the calculation
%			The calculation has to have a suitable parameter input 
%			for this.


%3D point version of the norm2DGridder/regular3DGridder, it allows use
%simple points in space as grid nodes, this can for instance be earthquakes.
%the input is a bit different, ptSep is not needed anymore, also not bBox
%needed is either a array with ThePoints(:,1) for lon, ThePoints(:,2) for lat and
%ThePoints(:,3) for depth or it can be set to 'selffeed' to use the earthquakes
%themself as pointgrid.






%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation

%Unpack the parameters



rad=TheParameters.SelectionRadius;

selected=TheParameters.SelectedEvents;
distMode=TheParameters.ParallelCalc;


if isfield(TheParameters,'SelectionNumber')
	Sel_Nr=TheParameters.SelectionNumber;
else
	Sel_Nr=100;
end

if isfield(TheParameters,'SelectionMode')
	Sel_Mode=TheParameters.SelectionMode;
else
	Sel_Mode='Both';
end

if isfield(TheParameters,'DistanceSend')
	SendDist=TheParameters.DistanceSend;
else
	SendDist=false;
end	


if isstr(TheParameters.ThePoints)&strcmp(TheParameters.ThePoints,'selffeed');
	[selectedLonLat,unselectedLonLat] = getLocations(datastore,selected);			
	[selectedDepth,unselectedDepth] = getDepths(datastore,selected);

	ThePoints=[selectedLonLat(:,1),selectedLonLat(:,2),selectedDepth];
	xRange=ThePoints(:,1);
	yRange=ThePoints(:,2);
	zRange=ThePoints(:,3);
	

else
	ThePoints=TheParameters.ThePoints;
	xRange=ThePoints(:,1);
	yRange=ThePoints(:,2);
	zRange=ThePoints(:,3);
end



% If only one value of point separation is defined then use same separation
% in both longitude and latitude
% if no dz is defined set it to one.


% Element counts of rows and columns

colCount = numel(yRange);

% Make a cell array to hold the calculation result
calcRes = cell(3,colCount);

% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
    disp('Calculate Serial')
else
    nDoParallelCalc();
    disp('Calculate Parallel')

end




% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        
        matlabpool open;
        parfor (elementIndex = 1:colCount)
            % Get the current coordinate
            thisGridPoint = [xRange(elementIndex), yRange(elementIndex), zRange(elementIndex)];
            % Define the filter function about the grid point
            filterFun = RadNumberFilter3D(thisGridPoint,rad,Sel_Nr,Sel_Mode,'RadDist3D');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected() & selected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            %pause(0.2);
        end
        matlabpool close;
        % Make the matrix have the correct orientation again.
        
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        
        for elementIndex = 1:colCount
            % Get the current coordinate
            thisGridPoint = [xRange(elementIndex), yRange(elementIndex), zRange(elementIndex)];
            % Define the filter function about the grid point
            filterFun = RadNumberFilter3D(thisGridPoint,rad,Sel_Nr,Sel_Mode,'RadDist3D');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected() & selected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            if rem(elementIndex,50) == 0
                waitbar(elementIndex./elementCount,h);
            end
        end
        % Make the matrix have the correct orientation again.
        % Close the waitbar
        close(h)
    end

end

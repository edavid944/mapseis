function CalcRes =  DensityAdder(CataData,Distances,CalcConfig)
	%This function calls the functionhandle inputed with the inputed data and config
	%and uses Distances to calculate density and resolution (radius) to the result. 
	
	TheFunction=CalcConfig.CalcFunction;
	FunctionConfig=CalcConfig.FunctionConfig;
	
	try
		ProfileSwitch=CalcConfig.ProfileSwitch;
	catch
		ProfileSwitch=false;
	end	
	
	
	%Call function:
	%disp(size(CataData))
	if ~isempty(TheFunction)
		CalcRes=TheFunction(CataData,FunctionConfig);
	end
	
	%add radius/resolution
	if ~ProfileSwitch
		CalcRes.Resolution=deg2km(max(Distances));
	else
		CalcRes.Resolution=max(Distances);
	end
	
	%add eq density
	if ~ProfileSwitch
		CalcRes.EqDensity=log10(numel(Distances)./(max(Distances).^2*pi)); 
	else
		CalcRes.EqDensity=log10(numel(Distances)./(km2deg(max(Distances)).^2*pi));
	end
	
	%number of eq selected
	CalcRes.NumEq=numel(Distances);
	
	if isempty(CalcRes.Resolution)
		CalcRes.Resolution=NaN;
	end
	
	if isempty(CalcRes.EqDensity)
		CalcRes.EqDensity=NaN;
	end
	
	if isempty(CalcRes.NumEq)
		CalcRes.NumEq=NaN;
	end
	
end

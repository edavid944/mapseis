function [xRange,yRange,zRange,calcRes]=regular3DGridder(datastore,calcObj,TheParameters)
% CalcGrid : Calculate event properties averaged around grid points
%bBox,ptSep,rad,selectedEvents,calcObj,varargin)
% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

%modified for the new CalcWrapper type by David Eberhard 2010

%v1.2 changes
%	-Allow to select the selection mode
%			'Number': for fixed Number of events
%			'Radius': for fixed selection radius
%			'Both': for fixed Number without selecting events
%				further away than the specified radius
%	-SelectionNumber: Number of events selected
%	-DistanceSend: 	This allows to send the calculated distances from the
%			from the current grid point to the calculation
%			The calculation has to have a suitable parameter input 
%			for this.


%3D version of the norm2DGridder
%similar but in 3 dimensions.
%At the moment only "normal" regionfilters are allowed (only does exist anyway)

%some parameters more are needed here:
%vertical_Min_Max: minimum depth and maximum depth used in the grid
%Gridspacing: needs a third parameter: dz



%DELETE LATER (Dev. comment):
%New parts needed:
%New Spherical Filter -> 3D distance will be a bit tricky
%Flip of the matrix ('): how to do it? Verification the gridder works is also needed.
%


%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation

%Unpack the parameters
bBox=TheParameters.boundaryBox;
ptSep=TheParameters.Gridspacing;
rad=TheParameters.SelectionRadius;
selected=TheParameters.SelectedEvents;
distMode=TheParameters.ParallelCalc;
vertMinMax=TheParameters.vertical_Min_Max;

if isfield(TheParameters,'SelectionNumber')
	Sel_Nr=TheParameters.SelectionNumber;
else
	Sel_Nr=100;
end

if isfield(TheParameters,'SelectionMode')
	Sel_Mode=TheParameters.SelectionMode;
else
	Sel_Mode='Radius';
end

if isfield(TheParameters,'DistanceSend')
	SendDist=TheParameters.DistanceSend;
else
	SendDist=false;
end	

% If only one value of point separation is defined then use same separation
% in both longitude and latitude
% if no dz is defined set it to one.

if numel(ptSep)<2
    ptSep(end+1) = ptSep(1);
    ptSep(3) = 1;
end

if numel(ptSep)<3
    ptSep(3) = 1;

end


% Ranges for grid
xRange = (bBox(1):ptSep(1):bBox(2))';
yRange = (bBox(3):ptSep(2):bBox(4))';
zRange = (vertMinMax(1):ptSep(3):vertMinMax(2))';


% Element counts of rows and columns
rowCount = numel(xRange);
colCount = numel(yRange);
pageCount = numel(zRange);

% Make a cell array to hold the calculation result
%calcRes = cell(rowCount,colCount,pageCount);

%% Extract the event data needed for the calculation

% Define 2D matrices of ranges
[XX,YY,ZZ] = meshgrid(xRange,yRange,zRange);
calcRes = cell(size(XX));
elementCount = numel(XX);
% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
    disp('Calculate Serial')
else
    nDoParallelCalc();
    disp('Calculate Parallel')

end




% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        %calcRes = calcRes';
        %calcRes=permute(calcRes,[2 3 1]);
        %calcRes=permute(calcRes,[2 1 3]);
        matlabpool open;
        parfor (elementIndex = 1:elementCount)
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), YY(elementIndex), ZZ(elementIndex)];
            % Define the filter function about the grid point
            filterFun = RadNumberFilter3D(thisGridPoint,rad,Sel_Nr,Sel_Mode,'RadDist3D');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected() & selected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            %pause(0.2);
        end
        matlabpool close;
        % Make the matrix have the correct orientation again.
        %calcRes = calcRes';
        %calcRes=permute(calcRes,[2 1 3]);
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        %calcRes = calcRes';
        %calcRes=permute(calcRes,[2 3 1]);
        %calcRes=permute(calcRes,[2 1 3]);
         
        for elementIndex = 1:elementCount
            % Get the current coordinate
             thisGridPoint = [XX(elementIndex), YY(elementIndex), ZZ(elementIndex)];
            % Define the filter function about the grid point
            filterFun = RadNumberFilter3D(thisGridPoint,rad,Sel_Nr,Sel_Mode,'RadDist3D');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected() & selected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            if rem(elementIndex,50) == 0
                waitbar(elementIndex./elementCount,h);
            end
        end
        % Make the matrix have the correct orientation again.
        %calcRes = calcRes';
        %calcRes=permute(calcRes,[2 1 3]);
        % Close the waitbar
        close(h)
    end

end

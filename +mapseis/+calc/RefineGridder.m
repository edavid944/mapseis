function [xRange,yRange,calcRes]=RefineGridder(datastore,calcObj,TheParameters)
%Special version of the 2D gridder which will solve a calculation on a special 
%refined grid with following coloumn: [x y dx dy selected rad] in the '2D' case
%and [x y z dx dy dz selected rad] in '3D' and [x y z t dx dy dz dt selected rad]
%in the '4D' mode, at the moment only the 2D mode is available.
%


%	-Radius_coeff: 	Allows to further modify the inputed radius by multiplication 
%			with this coeff.
%	-SendDist: 	This allows to send the calculated distances from the
%			from the current grid point to the calculation
%			The calculation has to have a suitable parameter input 
%			for this.
%	-minNumber:


%PROTOTYP

%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation

%Unpack the parameters
RefGrid=TheParameters.RefGrid;
Radius_coeff=TheParameters.Radius_coeff;
selected=TheParameters.SelectedEvents;
Sel_Nr=TheParameters.minNumber;
distMode=TheParameters.ParallelCalc;

try
	SendDist=TheParameters.SendDist;
catch
	SendDist=false;
end





% Ranges for grid
xRange = RefGrid(:,1);
yRange = RefGrid(:,2);

% Element counts of rows and colum



%% Extract the event data needed for the calculation

% Define 2D matrices of ranges
%[XX,YY] = meshgrid(xRange,yRange);
elementCount =  numel(xRange);

% Make a cell array to hold the calculation result
calcRes = cell(elementCount,1);

% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
    disp('Calculate Serial')
else
    nDoParallelCalc();
    disp('Calculate Parallel')

end
% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
     
        matlabpool open;
        parfor (elementIndex = 1:elementCount)
            % Get the current coordinate
            thisGridPoint = [RefGrid(elementIndex,1), RefGrid(elementIndex,2)];
            thisRad=RefGrid(elementIndex,6)*Radius_coeff;
            
            % Define the filter function about the grid point
            filterFun = RadNumberFilter(thisGridPoint,thisRad,Sel_Nr,'Radius','RadDist');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            %pause(0.2);
        end
        matlabpool close;
        % Make the matrix have the correct orientation again.
        %calcRes = calcRes';
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        %calcRes = calcRes';
        for elementIndex = 1:elementCount
            % Get the current coordinate
             thisGridPoint = [RefGrid(elementIndex,1), RefGrid(elementIndex,2)];
            thisRad=RefGrid(elementIndex,6)*Radius_coeff;
            % Define the filter function about the grid point
            filterFun = RadNumberFilter(thisGridPoint,thisRad,Sel_Nr,'Radius','RadDist');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            if rem(elementIndex,50) == 0
           	try
                	waitbar(elementIndex./elementCount,h);
                end	
            end
        end
        % Make the matrix have the correct orientation again.
        %calcRes = calcRes';
        % Close the waitbar
        close(h)
    end

end

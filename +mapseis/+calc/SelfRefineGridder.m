function [xRange,yRange,RefGrid,calcRes]=SelfRefineGridder(datastore,calcObj,thestate,TheParameters)
	%This function is kind of a hybrid between SelfRefineGrid and RefineGridder. It creates a 
	%grid and calculates it at the same time. This has the advanced that, other cell splitting
	%criterions than just the earthquake number can be used. And also it is possible to 
	%fill missing calculations on a gridcell size level with results from larger cells.
	
	
	
	
	
	%org SelfRefineGrid Comment
	%This function will build a variable grid based on the data density
	%at the moment it is a prototype function
	
	%SetupParameter
	%StartGrid: 	Defines the initalgrid used with a classical [x y dx dy NodeUsed] array
	%		x,y give the middle point of the nodes and dx,dy determine the gridspacing
	%		NodeUsed can be 0 for not used 1 for used and 2 for "has childrens" it will be 
	%		set by the algorithmen so it can be any value for init but it is suggest to use
	%		1 in case it will be used in some future version of the code.
	%Radius_coeff: 	The Selection radius will be determine by 
	%		squrt(dx^2+dy^2)*Radius_coeff
	%maxNumber:	The minimum number of dots per node allowed lower nodes
	%		will not be splitted anymore
	%minNumber:	Nodes with less events than minNumber will not be used.
	%minSpacing:	This [min_dx min_dy] will limit the minimal gridsize, to avoid to small cells
	%		if now limit is wanted, just set to [-inf -inf]
	%parallelmode: 	should turn the parallel processing mode, but does
	%KeepTop:	if this is set to true, the grid will at least contain the inputgrid.
	%StrictMode:	Similar to KeepTop, but in this case the larger cell will always be kept if not 
	%		fully replaced by the underlying cells (on dim smaller)
	
	
	
	
	%org RefineGridder Comment
	%Special version of the 2D gridder which will solve a calculation on a special 
	%refined grid with following coloumn: [x y dx dy selected rad] in the '2D' case
	%and [x y z dx dy dz selected rad] in '3D' and [x y z t dx dy dz dt selected rad]
	%in the '4D' mode, at the moment only the 2D mode is available.
	%
	
	
	%	-Radius_coeff: 	Allows to further modify the inputed radius by multiplication 
	%			with this coeff.
	%	-SendDist: 	This allows to send the calculated distances from the
	%			from the current grid point to the calculation
	%			The calculation has to have a suitable parameter input 
	%			for this.
	%	-minNumber:
	
	
	
	
	
	
	%PROTOTYP
	
	%% Import the necessary utility functions and objects
	import mapseis.datastore.*;
	import mapseis.filter.*;
	import mapseis.projector.*;
	
	%% Initialise the calculation
	
	%Unpack the parameters
	RefGrid=TheParameters.RefGrid;
	Radius_coeff=TheParameters.Radius_coeff;
	selected=TheParameters.SelectedEvents;
	Sel_Nr=TheParameters.minNumber;
	distMode=TheParameters.ParallelCalc;
	
	try
		SendDist=TheParameters.SendDist;
	catch
		SendDist=false;
	end
	
	
	
	
	
	% Ranges for grid
	xRange = RefGrid(:,1);
	yRange = RefGrid(:,2);
	
	% Element counts of rows and colum
	
	
	
	%% Extract the event data needed for the calculation
	
	% Define 2D matrices of ranges
	%[XX,YY] = meshgrid(xRange,yRange);
	elementCount =  numel(xRange);
	
	% Make a cell array to hold the calculation result
	calcRes = cell(elementCount,1);
	
	% Make the pointer look 'busy'
	set(gcf,'Pointer','watch');
	drawnow;
	% Do the calculation
	
	if ~distMode
	    nDoSerialCalc();
	    disp('Calculate Serial')
	else
	    nDoParallelCalc();
	    disp('Calculate Parallel')
	
	end
	% Make the pointer look normal again
	set(gcf,'Pointer','arrow');
	
	    function nDoParallelCalc()
		% Perform the calculation using a parfor loop
		% Transpose the result matrix since we are accessing elements using
		% linear indexing and hence are going down the columns of the
		% matrix.
		
		import mapseis.filter.*;
		
	     
		matlabpool open;
		parfor (elementIndex = 1:elementCount)
		    % Get the current coordinate
		    thisGridPoint = [RefGrid(elementIndex,1), RefGrid(elementIndex,2)];
		    thisRad=RefGrid(elementIndex,6)*Radius_coeff;
		    
		    % Define the filter function about the grid point
		    filterFun = RadNumberFilter(thisGridPoint,thisRad,Sel_Nr,'Radius','RadDist');
		    % Use the filter function to define the logical index vector
		    filterFun.execute(datastore);
		    logicalIndices = filterFun.getSelected() & selected;
		    
		    % Perform the calculation
		    if SendDist
			calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
						filterFun.CalculatedDistances);
		    else
			calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
		    end
		    
		    %pause(0.2);
		end
		matlabpool close;
		% Make the matrix have the correct orientation again.
		%calcRes = calcRes';
	    end
	
	    function nDoSerialCalc()
		% Perform the calculation using a for loop, also display a waitbar
		% Transpose the result matrix since we are accessing elements using
		% linear indexing and hence are going down the columns of the
		% matrix.
		
		import mapseis.filter.*;
		
		h = waitbar(0,'Please wait...');
		%calcRes = calcRes';
		for elementIndex = 1:elementCount
		    % Get the current coordinate
		     thisGridPoint = [RefGrid(elementIndex,1), RefGrid(elementIndex,2)];
		    thisRad=RefGrid(elementIndex,6)*Radius_coeff;
		    % Define the filter function about the grid point
		    filterFun = RadNumberFilter(thisGridPoint,thisRad,Sel_Nr,'Radius','RadDist');
		    % Use the filter function to define the logical index vector
		    filterFun.execute(datastore);
		    logicalIndices = filterFun.getSelected() & selected;
		    
		    % Perform the calculation
		    if SendDist
			calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
						filterFun.CalculatedDistances);
		    else
			calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
		    end
		    
		    if rem(elementIndex,50) == 0
			try
				waitbar(elementIndex./elementCount,h);
			end	
		    end
		end
		% Make the matrix have the correct orientation again.
		%calcRes = calcRes';
		% Close the waitbar
		close(h)
	    end

end

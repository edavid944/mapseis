function [xRange,yRange,calcRes]=CalcGrid(evStruct,bBox,ptSep,rad,calcObj,varargin)
% CalcGrid : Calculate event properties averaged around grid points

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation

% Check if we are doing a distributed computation
userDataKeys = evStruct.getUserDataKeys();
if any(ismember(userDataKeys,'DistMode'))
    % If a distributed mode is defined get the state, ie false for single
    % processor or true for distributed computation
    distMode = evStruct.getUserData('DistMode');
else
    try
        % See whether we have a matlabpool available
        matlabpool open;
        poolSize = matlabpool('size');
        matlabpool close;
        distMode = poolSize>0;
    catch ME       
        distMode = false
    end
end

% If only one value of point separation is defined then use same separation
% in both longitude and latitude
if numel(ptSep)<2
    ptSep(end+1) = ptSep(1);
end

% Ranges for grid
xRange = (bBox(1):ptSep(1):bBox(2))';
yRange = (bBox(3):ptSep(2):bBox(4))';

% Element counts of rows and columns
rowCount = numel(xRange);
colCount = numel(yRange);

% Make a cell array to hold the calculation result
calcRes = cell(rowCount,colCount);

%% Extract the event data needed for the calculation

% Define 2D matrices of ranges
[XX,YY] = meshgrid(xRange,yRange);
elementCount = numel(XX);
% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
    disp('Calculate Serial')
else
    nDoParallelCalc();
    disp('Calculate Parallel')

end
% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        calcRes = calcRes';
        matlabpool open;
        parfor (elementIndex = 1:elementCount)
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), YY(elementIndex)];
            % Define the filter function about the grid point
            filterFun = DistFilter(thisGridPoint,rad,'Dist');
            % Use the filter function to define the logical index vector
            filterFun.execute(evStruct);
            logicalIndices = filterFun.getSelected();
            % Perform the calculation
            calcRes{elementIndex} = calcObj.calculate(evStruct,logicalIndices);
            %pause(0.2);
        end
        matlabpool close;
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        calcRes = calcRes';
        for elementIndex = 1:elementCount
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), YY(elementIndex)];
            % Define the filter function about the grid point
            filterFun = DistFilter(thisGridPoint,rad,'Dist');
            % Use the filter function to define the logical index vector
            filterFun.execute(evStruct);
            logicalIndices = filterFun.getSelected();
            % Perform the calculation
            calcRes{elementIndex} = calcObj.calculate(evStruct,logicalIndices);
            if rem(elementIndex,50) == 0
                waitbar(elementIndex./elementCount,h);
            end
        end
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
        % Close the waitbar
        close(h)
    end

end
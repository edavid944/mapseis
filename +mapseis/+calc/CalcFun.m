function calcRes = CalcFun(calcFun,calcRes)
% CalcFun : Calculate a function over a cell array of data

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

% disp('Distributed calculation')
% Loop over the vector performing the calculation.  Make use of distributed
% computing resources if a matlabpool has been opened
elementCount = numel(calcRes);
parfor (elementIndex = 1:elementCount)
    funArg = calcRes{elementIndex};
    calcRes{elementIndex} = calcFun(funArg);
end

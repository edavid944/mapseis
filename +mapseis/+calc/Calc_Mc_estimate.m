function CalcRes =  Calc_Mc_estimate(zmapCatalog,CalcConfig)
	%This function reproduces the functionalty of the zmap procedure mcperc 
	%and estimates the Mc value on different confidence levels
	
	%Needed Input
	%	zmapCatalog: zmap formated catalog (projector can be used)
	%	CalcConfig: structure
	%		FullDataOut:	If set to true the full dataset dat will be written
	%				into the results
	%		TextOutput:	If set to true the text used for graphics will be
	%				generated, saves some effort. 	
	
	
	
	%Additional optional Parameter might be added in a later version
	
	
	%Output
		%Mc90:
		%Mc95:
		%Residual_Line:
	%Optional Output
	%	WholeData:
	%	Mc90_Text:
	%	Mc95_Text:	
		
		
		
	import mapseis.calc.Mc_calc.*;
	import mapseis.b_val_calc.*;
	import mapseis.calc.synth_catalogs.synthb_aut;
	
	%make things easier and shorter, write structur variables in single variables
	WholeData=CalcConfig.FullDataOut;
	TextOutput=CalcConfig.TextOutput;

	
	%set up all Output values and needed internal values
	CalcRes.Mc95=[];
	CalcRes.Mc90=[];
	CalcRes.Residual_Line=[];
	
	if WholeData
		CalcRes.WholeData=[];
	end
	
	if TextOutput
		CalcRes.Mc90_Text=[];
		CalcRes.Mc95_Text=[];
	end
	
	%from  mcperc
	%where are this inb1 and inb2 defined?
	%inb2 is set default to 1 and modified in bdiff2 by the method selector list
	
	[b_value_overall maxmag std_b a_value ] =  bvalca3(zmapCatalog);
	%old var names: [bv magco0 stan av]
	
	dat = []; 

	for i = maxmag - 0.9:0.1:maxmag+1.5
   		magselect = zmapCatalog(:,6) >= i - 0.0499; nu = length(zmapCatalog(l,6));

   		[meanmag b_value_mag std_bval_mag a_value] =  bmemag(zmapCatalog(magselect,:)); % ex [mw bv2 stan2 a_value]
   		resCatalog = synthb_aut(zmapCatalog,b_value_mag,i);
   		nc = 10.^(a_value - b_value_mag*(i+0.05)) ;
		nc1 = 10.^(a_value - (b_value_mag-std_bval_mag/2)*(i+0.05)) ;
		nc2 = 10.^(a_value - (b_value_mag+std_bval_mag/2)*(i+0.05)) ;
		dat = [ dat ; i nc nu nu/nc nu/nc1 nu/nc2 resCatalog  ];
		%display(['Completeness Mc: ' num2str(i) ';  rati = ' num2str(nu/nc)]); 

	end

	%Find Mc90
	j =  min(find(dat(:,7) < 10 )); 
	if isempty(j) == 1; Mc90 = nan 
	else; 
	   Mc90 = dat(j,1); 
	end
	
	%Find Mc95
	j =  min(find(dat(:,7) < 5 )); 
	if isempty(j) == 1; Mc95 = nan 
	else; 
	   Mc95 = dat(j,1); 
	end

	
	Mc90_Text=['Mc at 90% confidence: ' num2str(Mc90) ];
	Mc95_Text=['Mc at 95% confidence: ' num2str(Mc95) ];
	
	%Pack the data
	CalcRes.Mc95=Mc95;
	CalcRes.Mc90=Mc90;
	CalcRes.Residual_Line=[dat(:,1),dat(:,7)];
	
	if WholeData
		CalcRes.WholeData=dat;
	end
	
	if TextOutput
		CalcRes.Mc90_Text=Mc90_Text;
		CalcRes.Mc95_Text=Mc95_Text;
	end

	
	
	
	
end

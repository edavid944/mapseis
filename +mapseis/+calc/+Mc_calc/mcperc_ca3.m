function [magco prf Mc90 Mc95]=mcperc_ca3(zmapCatalog)

	
	%disp('This is /src/mcperc_ca3.m');
	
	% This is a comleteness determination test 
	
	
	
	%DE 2010:
	%NEEDS either to be replace, integrated into the master function or to be
	%rewritten.
	%--->rewrite as function
	
	import mapseis.calc.b_val_calc.*;
	import mapseis.calc.synth_catalogs.synthb_aut;
	
	%count the numbers of earthquakes per magnitude bin (b-value)
	
	[EqperBin,magbins] = hist(zmapCatalog(:,6),-2:0.1:6);
	
	
	
	%find the bin with the most events in it.
	l = max(find(EqperBin == max(EqperBin))); 
	maxmagbin =  magbins(l);
	
	synthdata = []; 
	
	%for i = magco0-0.6:0.1:magco0+0.2
	for i = maxmagbin-0.5:0.1:maxmagbin+0.7
	   l = zmapCatalog(:,6) >= i - 0.0499;
	   
	   selectedMags = length(zmapCatalog(l,6));
	   
	   if length(zmapCatalog(l,6)) >= 25; 
	      %[bv magco stan av] =  bvalca3(newt2(l,:),2,2); 
	      [meanMag_sub bval_sub sig_sub aval_sub] =  bmemag(zmapCatalog(l,:));
	      resCatalog=synthb_aut(zmapCatalog,bval_sub,i,l);  
	      synthdata = [ synthdata ; i resCatalog];
	   else     
	      %disp('synthetic data not generated');
	      synthdata= [ synthdata ; i nan];
	   end
	   
	end
	
	%Mc90 determination
	less10 =  min(find(synthdata(:,2) < 10 )); 
	if isempty(less10); 
		Mc90 = nan ;
	else 
		Mc90 = synthdata(less10,1); 
	end
	
	%Mc95 determintaton
	less5 =  min(find(synthdata(:,2) < 5 )); 
	if isempty(less5)
		Mc95 = nan ;
	else 
		Mc95 = synthdata(less5,1); 
	end
	
	%find lowest McXX
	lesserevil =  min(find(synthdata(:,2) < 10 ));
	if isempty(lesserevil) == 1; lesserevil =  min(find(synthdata(:,2) < 15 )); end
	if isempty(lesserevil) == 1; lesserevil =  min(find(synthdata(:,2) < 20 )); end
	if isempty(lesserevil) == 1; lesserevil =  min(find(synthdata(:,2) < 25 )); end
	lowparts =  min(find(synthdata(:,2) == min(synthdata(:,2)) )); 
	%j = min([j j2]); 
	
	Mc = synthdata(lesserevil,1); 
	magco = Mc; 
	prf = 100 - synthdata(lowparts,2); 
	
	if isempty(magco) == 1
		magco = nan; 
		prf = 100 - min(synthdata(:,2)); 
	end
	
	%display(['Completeness Mc: ' num2str(Mc) ]); 


end

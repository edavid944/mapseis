function [fMc] = calc_McMaxCurvature(mCatalog,MagBin);
% function [fMc] = calc_McMaxCurvature(mCatalog);
% -----------------------------------------------
% Determines the magnitude of completeness at the point of maximum
%   curvature of the frequency magnitude distribution
%
% Input parameter:
%   mCatalog        Earthquake catalog
%
% Output parameter:
%   fMc             Magnitude of completeness, nan if not computable
%
% Danijel Schorlemmer
% November 7, 2001

%modified for Mapseis by David Eberhard 2010

if nargin<2
	MagBin=0.1;
end

try 
  % Get maximum and minimum magnitudes of the catalog
  fMaxMagnitude = max(mCatalog(:,6));
  fMinMagnitude = min(mCatalog(:,6));
  if fMinMagnitude > 0
    fMinMagnitude = 0;
  end;
  
  % Number of magnitudes units
  nNumberMagnitudes = ceil(fMaxMagnitude*(1/MagBin));
  
  % Create a histogram over magnitudes
  vHist = zeros(1, nNumberMagnitudes);
  [vHist, vMagBins] = hist(mCatalog(:,6), (fMinMagnitude:MagBin:fMaxMagnitude));
  
  % Get the points with highest number of events -> maximum curvature  
  [max_curve_val max_curve_id] = max(vHist);
  fMc = vMagBins(max(max_curve_id));
  
  if isempty(fMc)
    fMc = nan;
  end;  
catch
  fMc = nan;
end;

end




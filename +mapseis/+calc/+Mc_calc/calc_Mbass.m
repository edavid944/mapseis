function M_breaks = calc_Mbass(Mags,mag_bin)
	

	coenf_limit=0.05;
	%disp('MBass is used')
	if numel(Mags(1,:))>1
		%zmapcatalog
		Mags=Mags(:,6);
	end	
	
	%Sketch Mbass routine
	minval=min(round(Mags/mag_bin)*mag_bin);
	maxval=max(round(Mags/mag_bin)*mag_bin);
	
	mincr = minval:mag_bin:maxval;
	
	cumnbmag=zeros(numel(mincr),1);
	nbmag=zeros(numel(mincr),1);
	
	%first loop seems to count how many mags are above a certain mag
	for i=1:numel(mincr)
		cumnbmag(i)=sum(Mags>(mincr(i)-mag_bin/2));
	end
	
	%I guess this is to avoid problems with the last value
	%Here the differences between the adejacenz is taken, results in a discretized difference d/dx (next step)
	cumnbmagtmp=[cumnbmag;0];
	nbmag = abs(diff(cumnbmagtmp));
	
	
	count_N = numel(mincr)-1;
	slopeArray =zeros(count_N,1);
	
	%vector version
	nbmag_plus=nbmag(2:count_N+1);
	nbmag_minus=nbmag(1:count_N);
	mincr_plus(:,1)=mincr(2:count_N+1);
	mincr_minus(:,1)=mincr(1:count_N);
	%disp(size(nbmag_plus));
	%disp(size(nbmag_minus));
	%disp(size(mincr_plus));
	%disp(size(mincr_minus));
	slopeArray=(log10(nbmag_plus)-log10(nbmag_minus))./(mincr_plus-mincr_minus);
	
	%this might be vectorized
	%for i=1:count_N
	%	%the slope (in log scale) between the bins
	%	slopeArray(i)=(log10(nbmag(i+1))-log10(nbmag(i)))/(mincr(i+1)-mincr(i));		
	%end
	
	mincr_corr = mincr(2:end);
	
	%set infinite values to NaN and kick them out of the array
	slopeArray(isinf(slopeArray))=NaN;
	NotNAN=~isnan(slopeArray);
	count_N=sum(NotNAN);
	slopeArray = slopeArray(NotNAN);
	mincr_corr = mincr_corr(NotNAN);
	
	%sets some start values
	total_iteration=3;	
	curr_I=0; %current iteration
	discont_k=0; %discontinuities
	SA = zeros(count_N,1);
	pva=[]; %matlab needs no variable setting
	tau=[];
	
	while (curr_I<total_iteration)
		%only needed to be done once, so out of the loop
		%delete everyting to be save
		SA = zeros(count_N,1);
		p_val=[];
		%rank=tiedrank in matlab
		Slope_ind=tiedrank(slopeArray);
		for i=1:count_N
			%I don't exactly get this one (in to original it was the R command rank)
			%[sorted Slope_ind]=sort(slopeArray);
			SA(i)=abs(2*sum(Slope_ind(1:i))-i*(count_N+1));
		end
			
		%find first maximum
		[res max_index]=max(SA);
		max_index=max_index(1);
		
		xn1=slopeArray(1:max_index);
		try
			xn2=slopeArray(max_index+1:end);
		catch
			%this is the case if max_index==end
			xn2=[];
		end	
			
		%wilcoxon ranksum test
		p_val = ranksum(xn1,xn2,'method','approximate');
		%disp(p_val);
		
		if (max_index>2)&(max_index<=(count_N-2))&(p_val<coenf_limit)
			discont_k=discont_k+1;
			pva(discont_k)=p_val;
			tau(discont_k)=max_index;
			
			if discont_k>1
				medsl1 = median(slopeArray(1:n0));
				try
					medsl2 = median(slopeArray(n0+1:end));
				catch
					medsl2 = [];
				end
					
				%vectorization possible?
				%for i=1:n0
				%	slopeArray(i)=slopeArray(i)+medsl1;
				%end
					
				
				%vectorization possible?
				%for i=n0+1:numel(slopeArray)
				%	slopeArray(i)=slopeArray(i)+medsl2;
				%end
				
				%vectored
				slopeArray(1:n0)=slopeArray(1:n0)+medsl1;
				slopeArray((n0+1):numel(slopeArray))=slopeArray((n0+1):numel(slopeArray))+medsl1;	
						
			end
			% In the org code of Arnaud is no "else" is one needed?
			medsl1 = median(xn1);
			medsl2 = median(xn2);
				
			%vectorization possible?
			%for i=1:max_index
			%	slopeArray(i)=slopeArray(i)-medsl1;
			%end
				
			%vectorization possible?
			%for i=max_index+1:numel(slopeArray)
			%	slopeArray(i)=slopeArray(i)-medsl2;
			%end
			
			%vectored
			slopeArray(1:max_index)=slopeArray(1:max_index)-medsl1;
			slopeArray((max_index+1):numel(slopeArray))=slopeArray((max_index+1):numel(slopeArray))-medsl1;
			
			%important
			n0=max_index;
				
				
		end
			
		curr_I=curr_I+1;
			

	
	end
	
	if ~isempty(pva)&~isempty(tau)
	
		[sorted pva_ind]=sort(pva);
		%break 1
		try
			breakPoint1=round(mincr_corr(tau(pva_ind(1)))*100)/100;
		catch
			breakPoint1=NaN;
		end
		
		
		try
			breakPoint2=round(mincr_corr(tau(pva_ind(2)))+100)/100;
		catch	
			breakPoint2=NaN;
		end	
		
		M_breaks=[breakPoint1 breakPoint2];
		
	else
		M_breaks=[NaN,NaN];
	end
	
	disp(M_breaks)
end

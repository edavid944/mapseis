function [xRange,yRange,calcRes]=norm2DGridder(datastore,calcObj,TheParameters)
% CalcGrid : Calculate event properties averaged around grid points
%bBox,ptSep,rad,selectedEvents,calcObj,varargin)
% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

%modified for the new CalcWrapper type by David Eberhard 2010

%v1.2 changes
%	-Allow to select the selection mode
%			'Number': for fixed Number of events
%			'Radius': for fixed selection radius
%			'Both': for fixed Number without selecting events
%				further away than the specified radius
%	-SelectionNumber: Number of events selected
%	-DistanceSend: 	This allows to send the calculated distances from the
%			from the current grid point to the calculation
%			The calculation has to have a suitable parameter input 
%			for this.
%v1.25 changer
%	Added support for location shifts
%	-NoShifter: 	if set to true and the catalog is shifted the calculation 
%			will be done in original coord system instead of the shifted
%			one, Note: the bBox still has to be shifted and the 
%			projector have to be set to bypass the offsets if needed.


%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;
import mapseis.util.ShiftCoords;

%% Initialise the calculation

%Unpack the parameters
bBox=TheParameters.boundaryBox;
ptSep=TheParameters.Gridspacing;
rad=TheParameters.SelectionRadius;
selected=TheParameters.SelectedEvents;
distMode=TheParameters.ParallelCalc;

if isfield(TheParameters,'SelectionNumber')
	Sel_Nr=TheParameters.SelectionNumber;
else
	Sel_Nr=100;
end

if isfield(TheParameters,'SelectionMode')
	Sel_Mode=TheParameters.SelectionMode;
else
	Sel_Mode='Radius';
end

if isfield(TheParameters,'DistanceSend')
	SendDist=TheParameters.DistanceSend;
else
	SendDist=false;
end	

if isfield(TheParameters,'NoShifter')
	NoShifter=TheParameters.NoShifter;
else
	NoShifter=false;
end	


%Get parameter UseShift from datastore
UseShift = datastore.UseShift;
[LonOff LatOff] = getLocationOffset(datastore);
Offsets = [LonOff LatOff];


% If only one value of point separation is defined then use same separation
% in both longitude and latitude
if numel(ptSep)<2
    ptSep(end+1) = ptSep(1);
end

% Ranges for grid
xRange = (bBox(1):ptSep(1):bBox(2))';
yRange = (bBox(3):ptSep(2):bBox(4))';

% Element counts of rows and columns
rowCount = numel(xRange);
colCount = numel(yRange);

% Make a cell array to hold the calculation result
calcRes = cell(rowCount,colCount);

%% Extract the event data needed for the calculation

% Define 2D matrices of ranges
[XX,YY] = meshgrid(xRange,yRange);
elementCount = numel(XX);


	
if UseShift&NoShifter
	XXVec=reshape(XX,numel(XX),1);
	YYVec=reshape(YY,numel(YY),1);
	ShiftedGrid=ShiftCoords([XXVec,YYVec],-Offsets);
	XX=reshape(ShiftedGrid(:,1),size(XX));
	YY=reshape(ShiftedGrid(:,2),size(YY));
end


% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
    disp('Calculate Serial')
else
    nDoParallelCalc();
    disp('Calculate Parallel')

end

if UseShift
	%Add a converted Range to the normal range (second column)
	MrX=ShiftCoords([xRange,zeros(numel(xRange))],-Offsets);
	MrY=ShiftCoords([zeros(numel(yRange)),yRange],-Offsets);
	xRange(:,2)=MrX(:,1);
	yRange(:,2)=MrY(:,2);
end




% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        calcRes = calcRes';
        matlabpool open;
        parfor (elementIndex = 1:elementCount)
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), YY(elementIndex)];
            % Define the filter function about the grid point
            filterFun = RadNumberFilter(thisGridPoint,rad,Sel_Nr,Sel_Mode,NoShifter,'RadDist');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected() & selected;
      
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            %pause(0.2);
        end
        matlabpool close;
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        calcRes = calcRes';
        for elementIndex = 1:elementCount
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), YY(elementIndex)];
            % Define the filter function about the grid point
            filterFun = RadNumberFilter(thisGridPoint,rad,Sel_Nr,Sel_Mode,NoShifter,'RadDist');
            % Use the filter function to define the logical index vector
            filterFun.LimitSelection(selected);
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected() & selected;
            
            % Perform the calculation
            if SendDist
            	calcRes{elementIndex} = calcObj.calculate_PD(datastore,logicalIndices,...
            				filterFun.CalculatedDistances);
            else
            	calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            end
            
            if rem(elementIndex,50) == 0
                waitbar(elementIndex./elementCount,h);
            end
        end
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
        % Close the waitbar
        close(h)
    end

end

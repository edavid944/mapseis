function [fLogLikelihood] = calc_AkiLikelihood(mCatalog, fBValue, fBinning);
% function [fLogLikelihood] = calc_AkiLikelihood(mCatalog, fBValue, fBinning)
% ---------------------------------------------------------------------------
% Calculates the likelihood of a b-value fit 
%
% Input parameters:
%   mCatalog        Earthquake catalog
%   fBValue         b-value
%   fBinning        Binning of the earthquake magnitudes (default 0.1)
%
% Output parameters:
%   fLogLikelihood  Log-likelihood
%
% Danijel Schorlemmer
% October 31, 2005
%
%@ARTICLE{Aki1965,
%  author =       "K. Aki",
%  title =        "Maximum likelihood estimate of $b$ in the formula
%                  $\log N = a-bM$ and its confidence limits",
%  journal =      "Bull. Earthquake Re. Inst., Tokyo Univ.",
%  year =         "1965",
%  volume =       "43",
%  pages =        "237-239",
%}

fBPrime = fBValue/(log10(exp(1)));
fMinMag = min(mCatalog(:,6))-(fBinning/2);

fL = ones(length(mCatalog(:,1)),1)*nan;

fL = log(fBPrime) - (fBPrime * (mCatalog(:,6) - fMinMag));
fLogLikelihood = sum(fL);
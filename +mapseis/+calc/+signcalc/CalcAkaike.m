function [fAkaike] = CalcAkaike(fLikelihood, nFreeParam, nSampleSize)

	fAkaike = (-2 * fLikelihood) + (2 * nFreeParam) + (2 * nFreeParam * (nFreeParam + 1))/(nSampleSize - nFreeParam - 1);

	
	%Comment DE 2011:
	%Maybe the Bayes Schwarz Information criterion could be used instead, it penalicy more for higher
	%parameter number.
	%This would be in this case:
	%
	%BIC= -2*(fLikelihood) + nFreeParam * log(nSampleSize);
	%log on likelihood is already applied before
	
	%Although AICc supposed to be more sensible, te problem is that in cases with high number of earthquakes
	%per grid is very hard to beat the local value with regional value. 
	%What is actually needed would be a comparision tool which would allow to express the trust one have the
	%local value over the regional value. Kind of a threshold value.
	
	
end % function CalcAkaike
function [modelinfo] = winningmodel(mNodeCatalog, fBValueOverall, blocal);
	import mapseis.calc.signcalc.*;
	
	% Default result
	nWinningModel = 1;
	fLikelihood_1 = 0;
	fLikelihood_2 = 0;
	fAkaike_1 = 0;
	fAkaike_2 = 0;
	fBValue = fBValueOverall;
	fBValue_2= blocal;
	
	% Select events within fRadius
	%vIndices = find(vDistances <= fRadius);
	%mNodeCatalog = mCatalog(vIndices,:);
	%vSel = mNodeCatalog(:,6) >= fMc;
	%mNodeCatalog = mNodeCatalog(vSel,:);
	nLen = length(mNodeCatalog(:,1));
	
	%if nLen >= 50
	% Compute likelihood of both models
	% Model 1 (Fixed b, variable a)
	fLikelihood_1 = calc_AkiLikelihood(mNodeCatalog, fBValueOverall, 0.1);
	fAkaike_1 = CalcAkaike(fLikelihood_1, 0, nLen);
	
	% Model 2 (Variable a & b)
	%[fBValue_2] = calc_bvalue(mNodeCatalog, 0.1);
	fLikelihood_2 = calc_AkiLikelihood(mNodeCatalog, fBValue_2, 0.1);
	fAkaike_2 = CalcAkaike(fLikelihood_2, 1, nLen);
	
	if fAkaike_2 < fAkaike_1
	    nWinningModel = 2;
	    fBValue = fBValue_2;
	end;
	%end;
	modelinfo.nWinningModel=nWinningModel;
	modelinfo.bpreferred=fBValue;
	modelinfo.likelihood1=fLikelihood_1;
	modelinfo.likelihood2=fLikelihood_2;
	modelinfo.nevents=nLen;
	modelinfo.akaike1=fAkaike_1;
	modelinfo.akaike2=fAkaike_2;
	
	%Comment DE 2011
	%Is 0 for regional value and 1 for local value as the number of parameters
	%really right? After all der is more that has to be defined in order to 
	%calculate a b-value for a grid cell.
	%Technical it is of course not wrong, to use 0 for regional values (no 
	%parameter needed here) and using 1 for local value (one parameter set, the
	%b-value itself), and when ever the function returns the region value as
	%winning model it definitely is.
	%
	%It of course make more sense, if this is looked at from a full grid point of 
	%view. Because then you can say you need for each grid cell to set with a
	%different b-value an additional parameter. This however would be a lot more
	%difficult, as it would involve an iterative algorithm. Something like the 
	%following:
	%Goal: find the maximum AICc solution
	%-start point would be a model with only on grid cell spanning the whole region
	% and the number of parameter would be 1.
	%-next step would be to use 2 parameters and split the region in half.
	%-again split in half and add a parameter, contiue until a minimum is reached
	% or the AICc can not be improved anymore. 
	%
	%Problem with that would be how to split the region. One way to overcome this
	%would be to define a grid first and target the region with a low likelihood for the regional 
	%value first. Best would be to do it iteratively. Use a coarse grid as start and process each subregion
	%on its own. 
	%
	%Also a problem is that the AICc might just be the wrong measurement anyway for such a method, maybe a
	%maximization of the entropy would be a better idea.
	
	%As this all would need some more time and thoughts:	
	%What would be possible alternative parameter number?
	%for regional value of course 1, after all the 
	%for local value:
	%has to be set is b-value, Mc, and the Eq selection parameter (radius, selection criterion, num eq)
	%but this also just a claim, in the end there is always only parameter needed in a region to describe 
	%the bvalue, the bvalue itself, the problem is that the region is defined by some parameters and also, the
	%calculation needs some needs some.
	
	
	
end % function UseRadius

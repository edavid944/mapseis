function resCatalog = synthb_aut(zmapCatalog,bvalue,hypMc,l)
	
	%--------------------------------------------------------------------------
	%| Modified as function for mapseis by David Eberhard 2010		  |
	%| The original code is quiet a mess with a lot of variables not known if |
	%| are needed, this version might be change further in future release.    |
	%--------------------------------------------------------------------------

	%This is synthetic219
	%This program generates a synthetic catalog of given total number of events, b-value, minimum magnitude, 
	%and magnitude increment.
	%The synthetic catalog will be sotred in a file "synt.mat" 
	% Yuzo Toya 2/1999
	
	%disp('This is /src/synthb_aut.m');
	
	%Variables renamed (typical in "Estimate Mc")
	%i -> hypMc
	%newt2 -> zmapCatalog
	%bv2 ->bvalue
	%l -> l, it is the index in zmapCatalog of all events with Mag Larger hypMc-0.0499
	%Don't know why they putted it not into the function
	
	%disp(['bvalue: ',num2str(bvalue)])
	%disp(['hypMc: ',num2str(hypMc)])
	
	
	Nr_eq = length(zmapCatalog(:,1));  %total number of events (ex TN)
	bvalue = bvalue; %b-value (ex B)
	hypMc = hypMc;%starting magnitude (hypothetical Mc) (ex IM)
	inc = 0.1 ;%magnitude increment
	
	% log10(N)=A-B*M
	MagBins=[hypMc:inc:15];  %(ex M)
	N=10.^(log10(Nr_eq)-bvalue*(MagBins-hypMc));
	
	%seems to be unused, will be added later to the result if it is needed in 
	%any function calling it.
	%aval=(log10(Nr_eq)-bvalue*(0-hypMc));
	
	N=round(N);
	%N=floor(N);
	%N=ceil(N);
	
	%syntheticCat is not used any further here, comment out until proven 
	%otherwise
	%syntheticCat = ones(Nr_eq,9)*nan; %(ex syn)
	new = ones(Nr_eq,1)*nan;
	
	%[ttt,indt]=sortrows(new,[3]);
	%new=ttt;
	%new=a
	ct1=1;
	
	
	ct1  = min(find(N == 0)) - 1;
	
	
	
	if isempty(ct1)
		ct1 = length(N); 
	end
	
	
	%disp(['ct1 is ',num2str(ct1)])
	
	%This whole part can be improved and unneccessary stuff can be 
	%left out, at least in this version
	%ctM=MagBins(ct1);
	%count=0;
	%ct=0;
	%swt=0;
	%sc=0;
	%for i=hypMc:inc:ctM;
	%   ct=ct+1;
	%   if i~=ctM;
	%      for sc=1:(N(ct)-N(ct+1));
	%	 count=count+1;
	%	 new(count)=i;
	%      end
	%   else   
	%      count=count+1;
	%      new(count)=i;
	%   end
	%end;   
	
	%only ct seems to be needed
	ctM=MagBins(ct1);
	ct=numel(hypMc:inc:ctM);
	
	
	PM=MagBins(1:ct);
	PN=log10(N(1:ct));
	%bdiff(newt2)
	%ga = findobj('Tag','cufi'); 
	%axes(ga); hold on; 
	%plot(PM,N(1:ct));
	%pause
	N = N(1:ct); 
	le = length(zmapCatalog(l,:));
	[bval,xt2] = hist(zmapCatalog(l,6),PM);
	b3 = fliplr(cumsum(fliplr(bval)));    % N for M >= (counted backwards)
	res2 = sum(abs(b3 - N))/sum(b3)*100; 
	resCatalog = res2; 
	
	%return
	
	%old plotting stuff from the original procedure
	%  
	%  fi = findobj('tag','mcfig2'); 
	%  if isempty(fi) == 1
	   %  figure('pos',[300 300 300 300],...
	      %  'tag','mcfig2');
	%  else
	   %  figure(fi); delete(gca);delete(gca);
	%  end
	%  
	%  
	%  axes('pos',[0.15 0.2 0.7 0.7])
	%  
	%  pl = semilogy(PM,b3,'bo')
	%  set(pl,'LineWidth',[1.5],'MarkerSize',[5],...
	   %  'MarkerFaceColor',[0 0 0 ],'MarkerEdgeColor','k');
	%  
	%  hold on 
	%  pl = semilogy(PM,N(1:ct),'rs')
	%  set(pl,'LineWidth',[1.5],'MarkerSize',[5],...
	   %  'MarkerFaceColor',[0.9 0.9 0.9],'MarkerEdgeColor','k');
	%  
	%  %re = (abs(b3 - N));
	%  %pl = semilogy(PM,re,'rv')
	%  %set(pl,'LineWidth',[1.0],'MarkerSize',[5],...
	 %  %  'MarkerFaceColor','w','MarkerEdgeColor','k');
	 %  
	%  set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
	    %  'TickDir','out','LineWidth',[1.0],...
	    %  'Box','on')
	 %  legend('Observed','Synthetic')
	 %  xlabel('Magnitude')
	 %  ylabel('Cumulative Number')
	 %  title('Goodness of FMD fit to GR')
	 %  set(gca,'Ylim',[1 1300])
	 %  i
	%  pause



end

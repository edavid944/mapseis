function CalcRes = CalcMagSign(zmapcata,bval_CalcRes, CalcParam)
	% This function is based on the old zmap function synsig3
	% Original Header of the Comments:
		
		% ------------------------------------------------------------------
		% Calculates magnitude signatures, yet another version, this time
		% Computes a synthetic signature, using corrections found by bvalfit
		%
		%                                  R. Zuniga IGF-UNAM/GI-UAF  7/94
		%-------------------------------------------------------------------
	
	%Changes:
		% -It is now a function and not procedure
		% -Only one Intervall is calculated
	
	
	%TO BE finished
			
			

	time1 = CalcConfig.time1;
	time2 = CalcConfig.time2;
	
	xt_selcata = time1:CalcConfig.par1/365:time2;
	len_xt_selcata = length(xt_backg);
	
	magis = min(zmapcata(:,6))

	mmin = floor(min(zmapcata(:,6))*10)/10;     %  round towards zero to 0.1
	mmax = ceil(max(zmapcata(:,6))*10)/10;  %  round towards inf to 0.1
	masi = zeros(size(mmin:0.1:mmax));
	masi2 = masi;
	
	selcata = bval_CalcRes.selected_catalog;
	
	%
	%                     loop over all magnitude bands
	% 

	for i = mmin:0.1:mmax,

	  
	 %JUNK is the right word for this %&*[|#&  (BELOW)
	 %----------------------------------------------------------------------------------------------
	  l = selcata(:,6) <= i;  
	  junk = selcata(l,:);
	   		 
	   		if length(junk) > 0,  % junk1
	   			 [cum_mag xt_selcata] = hist(junk(:,3),xt_selcata);      %    background
	
				%l = foreg(:,6) <= i;                                     
	    		%junk = foreg(l,:); 
	     	
	     		%	if length(junk) > 0, % junk2
	     		%		 [cum_mag2 xt_foreg] = hist(junk(:,3),xt_foreg);     %    foreground
	    			  
	    			 	 %  l =  backg_new(:,6) <= i;
	    			 	 %junk = backg_new(l,:);
	      	
	      			 		if length(junk) > 0, % junk3
	       						
	       						 l =  junk(:,6) <= magis;    % find out events below cut off for rate factor
	        
	         						if length(junk(l,:)) > 0   % junk4
	         							 [cum_junk xt_selcata] = hist(junk(l,3),xt_selcata);
	         						end  % if junk4
	        
	        				mean1 = mean(cum_mag(1:len_xt_selcata));                  
              
	    				    var1 = cov(cum_mag(1:len_xt_selcata));

	        					
	        					 %if sqrt(var1/tbckg+var2/tforg) > 0
	     						%	     masi(ind) = (mean1 - mean2)/(sqrt(var1/tbckg+var2/tforg)); 
	     						% end;
	         				end   % if junk3 
	         		%end   % if junk2
	        end   % if junk1
	%-----------------------------------------------------------------------------------------------
	
	
	% and above
	%
	%and JUNK again (ABOVE) --->still in the for-loop
	%------------------------------------------------------------------------------------------------
		
		l = selcata(:,6) >= i;  
	 	junk = selcata(l,:);
	 	
	 		if length(junk) > 0, 
	 			[cum_mag xt_selcata] = hist(junk(:,3),xt_cata);       %    background
				
				%l = foreg(:,6) >= i;                                 %    foreground
	 			%junk = foreg(l,:);
		
				%	if length(junk) > 0,
	 			%		[cum_mag2 xt_foreg] = hist(junk(:,3),xt_foreg);
						
						%l =  backg_new(:,6) >= i;
						% junk = backg_new(l,:);
							
							if length(junk) > 0,
								
								if i <= magis
	  								l =  junk(:,6) <= magis;    % find out events below cut off for rate factor
										
										if length(junk(l,:)) > 0;
	  											[cum_junk xt_selcata] = hist(junk(l,3),xt_selcata);
										end  %  if junk4
								end  % if i < magis
	  
	  							mean1 = mean(cum_mag(1:len_xt_selcata));                  
                 				mean2 = mean(cum_mag2(1:tforg));                  
 
	 
	 								if mean1 | mean2 > 0 
	  									var1 = cov(cum_mag(1:len_xt_selcata));
										var2 = cov(cum_mag2(1:tforg));
										
										%  masi2 = [masi2  (mean1 - mean2)/(sqrt(var1/tbckg+var2/tforg))]; 
										masi2(ind) = (mean1 - mean2)/(sqrt(var1/tbckg+var2/tforg));
							 		end   % if mean1
	
	 						end   % if junk3
	 				%end   % if junk2
	 		end   % if junk1
	 		
	 %mag(i) = i;
		%cum_mag = []; cum_mag2 = [];  cum_syn = []; cum_junk = [];
	
	end  %    for i 

	%write into outputstructur
	
	CalcRes.

end
function [xRange,zRange,calcRes]=CalcDepthGrid(calcStruct,calcObj,varargin)
% CalcGrid : Calculate event properties averaged around grid points


% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
% Author: Matt McDonnell

%% Import the necessary utility functions and objects
import mapseis.datastore.*;
import mapseis.filter.*;
import mapseis.projector.*;

%% Initialise the calculation
datastore = calcStruct.DataStore;
ptSep = calcStruct.GridSeparation;
rad = calcStruct.SelectionRadius;
pline = calcStruct.ProfileLine;
width = calcStruct.ProfileWidth;
rowIndices = calcStruct.SelectedRows;

% Check if we are doing a distributed computation
userDataKeys = datastore.getUserDataKeys();
if any(ismember(userDataKeys,'DistMode'))
    % If a distributed mode is defined get the state, ie false for single
    % processor or true for distributed computation
    distMode = datastore.getUserData('DistMode');
else
    try
        % See whether we have a matlabpool available
        matlabpool open
        poolSize = matlabpool('size');
        distMode = poolSize>0;
        matlabpool close
    catch ME       
        distMode = false;
    end
end

% If only one value of point separation is defined then use same separation
% in both longitude and latitude
if numel(ptSep)<2
    ptSep(end+1) = ptSep(1);
end

%calculate the depth bBox
bBox = getLineDepthBbox(datastore,pline,width,rowIndices);

% Ranges for grid
xRange = (bBox(1,1):ptSep(1):bBox(2,1))'; %on the lin
zRange = (bBox(1,2):ptSep(2):bBox(2,2))'; %Depth

% Element counts of rows and columns
rowCount = numel(xRange);
colCount = numel(zRange);

% Make a cell array to hold the calculation result
calcRes = cell(rowCount,colCount);

%% Extract the event data needed for the calculation

% Define 2D matrices of ranges
[XX,ZZ] = meshgrid(xRange,zRange);
elementCount = numel(XX);
% Make the pointer look 'busy'
set(gcf,'Pointer','watch');
drawnow;
% Do the calculation

if ~distMode
    nDoSerialCalc();
else
    nDoParallelCalc();
end
% Make the pointer look normal again
set(gcf,'Pointer','arrow');

    function nDoParallelCalc()
        % Perform the calculation using a parfor loop
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        calcRes = calcRes';
        matlabpool open
        parfor (elementIndex = 1:elementCount)
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), ZZ(elementIndex)];
            % Define the filter function about the grid point
            filterFun = ProfileFilter(thisGridPoint,rad,pline,width,rowIndices,'Dist');
            % Use the filter function to define the logical index vector
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected();
            % Perform the calculation
            calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
        end
        matlabpool close
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
    end

    function nDoSerialCalc()
        % Perform the calculation using a for loop, also display a waitbar
        % Transpose the result matrix since we are accessing elements using
        % linear indexing and hence are going down the columns of the
        % matrix.
        
        import mapseis.filter.*;
        
        h = waitbar(0,'Please wait...');
        calcRes = calcRes';
        for elementIndex = 1:elementCount
            % Get the current coordinate
            thisGridPoint = [XX(elementIndex), ZZ(elementIndex)];
            % Define the filter function about the grid point
            filterFun = ProfileFilter(thisGridPoint,rad,pline,width,rowIndices,'Dist');
            % Use the filter function to define the logical index vector
            filterFun.execute(datastore);
            logicalIndices = filterFun.getSelected();
            % Perform the calculation
            calcRes{elementIndex} = calcObj.calculate(datastore,logicalIndices);
            if rem(elementIndex,50) == 0
                waitbar(elementIndex./elementCount,h);
            end
        end
        % Make the matrix have the correct orientation again.
        calcRes = calcRes';
        % Close the waitbar
        close(h)
    end
    
    function glbox = getLineDepthBbox(datastore,pline,width,rowIndices)
				import mapseis.projector.*
				
				%get data from datastore
				
				[locSelected,locUnSelected] = getSliceProjections(datastore,pline,width,rowIndices);
				[depSelected, depUnSelected] = getDepths(datastore,rowIndices);
				
				glbox = [min(locSelected), min(depSelected);...
						 max(locSelected), max(depSelected)	];	
				
	end
	
	
				
end
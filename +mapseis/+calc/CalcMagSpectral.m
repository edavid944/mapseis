function ResStruct = CalcMagSpectral(Datastore,selected,MagRes)
	%This function calculates kind of a 3D histogram, with time and Magnitude
	%It is a prototype, if it is usable, I will write more about it.
	
	%Many of the DataSpectras will probably not be used in the end
	
	if nargin<3
		MagRes=0.5;
	end	
	
	
	import mapseis.projector.*;
	
	
	%first get datenum, decyear and mag
	dateNum=getDateNum(Datastore,selected);
	decyear=getDecYear(Datastore,selected);
	mags=getMagnitudes(Datastore,selected);
	

	%build the bands
	minMag=floor(min(mags*(1/MagRes)))*MagRes;
	maxMag=ceil(max(mags*(1/MagRes)))*MagRes;
	MagBands=minMag:MagRes:maxMag;
	
	

	%init data and master sum
	CumMagSpectra=zeros(numel(MagBands)-1,numel(decyear));
	MasterSum=1:numel(decyear);
	
	for i=1:numel(MagBands)-1
		inBand=MagBands(i)<mags&MagBands(i+1)>=mags;
		if any(inBand)
			CumMagSpectra(i,inBand)=1:sum(inBand);
			
			%span the values
			KeyPoints=MasterSum(inBand);
			
			for j=1:numel(KeyPoints)-1
				if (KeyPoints(j+1)-KeyPoints(j))>1
					%spanning needed
					CumMagSpectra(i,KeyPoints(j):KeyPoints(j+1)-1)=	CumMagSpectra(i,KeyPoints(j));
				end
			end
			
			if KeyPoints(end)~=numel(decyear)
				%spann to the last element
				CumMagSpectra(i,KeyPoints(end):end)=CumMagSpectra(i,KeyPoints(end));
			end	
			
		end
	
	end
	
	%build normalized TotalNumber Spectra
	NormMagSpectra=CumMagSpectra./repmat(MasterSum,numel(MagBands)-1,1);
	
	%create Label for mags (use middle point)
	magLabel=MagBands(1:end-1);
	
	%build weighted spectras
	MagSpectraW=CumMagSpectra.*repmat(10.^magLabel'+0.1,1,numel(decyear));
	NormMagSpectraW=NormMagSpectra.*repmat(10.^magLabel',1,numel(decyear));
	
	%build spectras stretched to 1
	theMax=max(CumMagSpectra,[],2);
	MagSpectraAddOne=CumMagSpectra./repmat(theMax,1,numel(decyear));
	
	NormMagSpectraAddOne=MagSpectraAddOne - repmat(MasterSum/numel(MasterSum),numel(MagBands)-1,1);
	
	
	%logarithmic
	LogMagSpectra=CumMagSpectra;
	LogMagSpectra(LogMagSpectra~=0)=log10(LogMagSpectra(LogMagSpectra~=0));
	NormLogMagSpectra=NormMagSpectra;
	NormLogMagSpectra(NormLogMagSpectra~=0)=log10(NormLogMagSpectra(NormLogMagSpectra~=0));
	
	%b-val Like map
	Temp=[zeros(numel(MagBands)-1,1),CumMagSpectra(:,1:end-1)];
	MagBval=cumsum(Temp,2)-cumsum(CumMagSpectra,1);
	
	if any(any(MagBval<0))
		MagBval=MagBval-min(min(MagBval));
	end	
	MagBval(MagBval~=0)=log10(MagBval(MagBval~=0));
	RevLabel=magLabel(end):-MagRes:magLabel(1);
	
	%Pack the stuff
	ResStruct.MagSpectra=CumMagSpectra;
	ResStruct.NormMagSpectra=NormMagSpectra;
	ResStruct.MagSpectraWeight=MagSpectraW;
	ResStruct.NormMagSpectraWeight=NormMagSpectraW;
	
	ResStruct.MagSpectraAddOne=MagSpectraAddOne;
	ResStruct.NormMagSpectraAddOne=NormMagSpectraAddOne;
	ResStruct.LogMagSpectra=LogMagSpectra;
	ResStruct.NormLogMagSpectra=NormLogMagSpectra;
	
	ResStruct.MagBval=MagBval;
	ResStruct.RevLabel=RevLabel;
	
	
	ResStruct.MasterSum=MasterSum;
	ResStruct.magLabels=magLabel;
	ResStruct.MagBands=MagBands;
	ResStruct.MagRes=MagRes;
	ResStruct.DecYear=decyear;
	ResStruct.dateNum=dateNum;
	
end

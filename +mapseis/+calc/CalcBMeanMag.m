function  resStruct =  CalcBMeanMag(vMag, fBinning)
% function resStruct =  CalcBMeanMag(vMag, fBinning)
% ---------------------------------------------------------------------------------
% Calculates the mean magnitute, the b-value based
% on the maximum likelihood estimation, the a-value and the
% standard deviation of the b-value
%
% Input parameters:
%   vMag             Vector of event magnitudes
%   fBinning        Binning of the earthquake magnitudes (default 0.1)
%
% Output parameters:
%   resStruct:
%     meanMag        Mean magnitude
%     b              b-value
%     stdDev         Standard deviation of b-value
%     a              a-value
%
% Danijel Schorlemmer
% June 2, 2003

% Modified version of calc_bmemag by Danijel Schorlemmer
% This version differs in taking a vector of input magnitudes instead of
% the entire catalog.  The result is returned as a structure.
% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

% Set the default value if not passed to the function
if nargin<2
    fBinning = 0.1;
end;

if ~isempty(vMag)
    % Calculate the minimum and mean magnitude, length of catalog
    nLen = length(vMag);
    fMinMag = min(vMag);
    fMeanMag = mean(vMag);
    % Calculate the b-value (maximum likelihood)
    fBValue = (1/(fMeanMag-(fMinMag-(fBinning/2))))*log10(exp(1));
    % Calculate the standard deviation
    fStdDev = (sum((vMag-fMeanMag).^2))/(nLen*(nLen-1));
    fStdDev = 2.30 * sqrt(fStdDev) * fBValue^2;
    % Calculate the a-value
    fAValue = log10(nLen) + fBValue * fMinMag;

    % Output the results as a struct
    resStruct = struct(...
        'meanMag',fMeanMag,...
        'a',fAValue,...
        'b',fBValue,...
        'stdDev',fStdDev);
else
    resStruct = struct(...
        'meanMag',NaN,...
        'a',NaN,...
        'b',NaN,...
        'stdDev',NaN);
end

end
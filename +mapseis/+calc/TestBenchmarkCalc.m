function CalcRes = TestBenchmarkCalc(zmapCatalog,CalcConfig)
	%This calculation is meant for testing and benchmarking (later) of grids and
	%similar things
	
	CalcRes.StdOut=[];
	
	switch CalcConfig.Mode;
		case 'Density'
			CalcRes.StdOut=numel(zmapCatalog(:,1));
			
		case 'Echo'
			disp('The Cake is a lie');	
			CalcRes.StdOut=true;
			
		case 'Benchmark'
		
		case 'MeanMag'
			CalcRes.StdOut=nanmean(zmapCatalog(:,6));
		case 'Counter'
			%needs counterobject;
			CalcRes.StdOut=CalcConfig.Counter.getCounter;
			CalcConfig.Counter.IncCounter;
		
		case 'BarCounter'
			CalcRes.StdOut=CalcConfig.Counter.getCounter;
			CalcConfig.Counter.CountBar;
		
		case 'CustomCalc'
			%this allows to use any calculation of the form CalcRes=MyCalc(zmapCatalog)
			%no config possible
			CalcRes=CalcConfig.ScriptHandle(zmapCatalog);
			%disp('CalciCalci')
			
			
		
			
	end
	
	
	





end

function FMDGUIPlug(mainGUI,work2do,plotAxis)
	%This is PlugIn will add the function calc FMD to the bottom left plot
	import mapseis.guiPlugIns.*;
	
	if nargin<3
		plotAxis=[];
	else
		disp('I received axis')	
	end
	
	switch work2do
		case '-Init'
			if ~isfield(mainGUI.PlugInMemory,'FMDPlug');
				defaultParameters=struct(	'Mc_method',1,...
							'Use_bootstrap',false,...
							'MinNumber',10,...
							'Mc_Binning',0.1,...
							'Nr_bootstraps',100,...
							'Mc_correction',0,...
							'NoCurve',false,...
							'Write_Text',false);
			
				mainGUI.PlugInMemory.FMDPlug=defaultParameters
			end	
			%edit the two menus
			%first the parameter modifier
			plotAxis=findobj(mainGUI.MainGUI,'Tag','axis_top_right');
			EditorParent=mainGUI.OptionMenu;
			MouseParent=get(plotAxis,'UIContextMenu');
			
			uimenu(EditorParent,'Label','Edit FMD Parameters',...
			'Callback',@(s,e) FMDGUIPlug(mainGUI,'-ParamSet'));
			
			%uimenu(MouseParent,'Label','Draw FMD',...
			%'Callback',@(s,e) FMDGUIPlug(mainGUI,'-DoWork'));
			tochange=findobj(MouseParent,'Label','Cumulative FMD');
			set(tochange,'Label', 'Draw FMD','Callback',@(s,e) FMDGUIPlug(mainGUI,'-DoWork'));
			
			
		case '-ParamSet'
			%just for safety reason
			if ~isfield(mainGUI.PlugInMemory,'FMDPlug');
				defaultParameters=struct('Mc_method',1,...
							'Use_bootstrap',false,...
							'MinNumber',10,...
							'Mc_Binning',0.1,...
							'Nr_bootstraps',100,...
							'Mc_correction',0,...
							'NoCurve',false,...
							'Write_Text',false);
			
				mainGUI.PlugInMemory.FMDPlug=defaultParameters
			end	
			
			Params=mainGUI.PlugInMemory.FMDPlug;
			
			
			
			Title = 'Mc Input Parameter';
			Prompt={'Mc Method:', 'Mc_method';...
					'Mc bootstraps:','Use_bootstrap';...
					'Number of Bootstraps:','Nr_bootstraps';...		
					'Mc Correction for Maxc:','Mc_correction';...
					'Magnitude Bin Size:','Mc_Binning';...
					'Show numeric Result','Write_Text' };
				
				labelList2 =  {'1: Maximum curvature'; ...
					'2: Fixed Mc = minimum magnitude (Mmin)'; ...
					'3: Mc90 (90% probability)'; ...
					'4: Mc95 (95% probability)'; ...
					'5: Best combination (Mc95 - Mc90 - maximum curvature)'; ...
					'6: EMR-method'; ...
					'7: Mc due b using Shi & Bolt uncertainty'; ...
					'8: Mc due b using bootstrap uncertainty'; ...
					'9: Mc due b Cao-criterion';...
					'10: MBass'}; 
				
				
				%Mc Method
				Formats(1,1).type='list';
				Formats(1,1).style='popupmenu';
				Formats(1,1).items=labelList2;
				Formats(1,2).type='none';
				Formats(1,1).size = [-1 0];
				Formats(1,2).limits = [0 1];
				
				%bootstrap toggle
				Formats(2,1).type='check';
				
				%bootstrap number
				Formats(2,2).type='edit';
				Formats(2,2).format='integer';
				Formats(2,2).limits = [0 9999];
				
				
				
				%Mc Correction
				Formats(3,1).type='edit';
				Formats(3,1).format='float';
				Formats(3,1).limits = [-99 99];
				Formats(3,1).size = [-1 0];
				Formats(3,2).limits = [0 1];
				Formats(3,2).type='none';
				
				%Mag Binning
				Formats(4,1).type='edit';
				Formats(4,1).format='float';
				Formats(4,1).limits = [-99 99];
				Formats(4,1).size = [-1 0];
				Formats(4,2).limits = [0 1];
				Formats(4,2).type='none';
				
				%bootstrap toggle
				Formats(5,1).type='check';
				Formats(5,2).limits = [0 1];
				Formats(5,2).type='none';        
				
				%%%% SETTING DIALOG OPTIONS
				Options.WindowStyle = 'modal';
				Options.Resize = 'on';
				Options.Interpreter = 'tex';
				Options.ApplyButton = 'off';
		
				
				
				%default values
				defval =struct('Mc_method',Params.Mc_method,...
					'Use_bootstrap',Params.Use_bootstrap,...
					'Nr_bootstraps',Params.Nr_bootstraps,...		
					'Mc_correction',Params.Mc_correction,...
					'Mc_Binning',Params.Mc_Binning,...
					'Write_Text',Params.Write_Text);
					
				%open the dialog window
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
				
				if Canceled~=1
					Params.Mc_method=NewParameter.Mc_method;
					Params.Use_bootstrap=NewParameter.Use_bootstrap;
					Params.Nr_bootstraps=NewParameter.Nr_bootstraps;
					Params.Mc_correction=NewParameter.Mc_correction;
					Params.Mc_Binning=NewParameter.Mc_Binning;
					Params.Write_Text=NewParameter.Write_Text;
				end
				
				mainGUI.PlugInMemory.FMDPlug=Params;	
			
		
		case '-DoWork'
			  import mapseis.plot.*;
    
			  %get current Catalog & Filter
			  Datastore = mainGUI.CommanderGUI.getCurrentDatastore;
			  Filterlist = mainGUI.CommanderGUI.getCurrentFilterlist; 
			  selected=Filterlist.getSelected;
			  Params=mainGUI.PlugInMemory.FMDPlug;
			  
			  if isempty(plotAxis)
			  	plotAxis=findobj(mainGUI.MainGUI,'Tag','axis_top_right')
			  end
			  
			  FMDConfig	= struct('PlotType','FMDex',...
					'Data',Datastore,...
					'BinSize',Params.Mc_Binning,...
					'b_line',true,...
					'CustomCalcParameter',Params,...
					'Selected',selected,...
					'Mc_Method',1,...
					'Write_Text',Params.Write_Text,...
					'LineStylePreset','normal',...
					'X_Axis_Label','Magnitude ',...
					'Y_Axis_Label','cum. Number of Events ',...
					'Colors','red',...
					'X_Axis_Limit','auto',...
					'Y_Axis_Limit','auto',...
					'LegendText','FMD');
			 
			[handle entry] = PlotFMD_expanded(plotAxis,FMDConfig);
	
			mainGUI.reTag('right_top');
			
			%add yourself as last plot
			mainGUI.HistType='EXTERN';
			mainGUI.ExternPlotFunction{2}=@(x) FMDGUIPlug(mainGUI,'-DoWork',x);
		
	end



end

classdef listenerArray < handle
    % listenerArray : Based on the assocArray of DataStore. A hash to store the 
    % different eventlistener in a hash
    %   set(Key, Value), getKeys(), lookup(Key), remove(Key), isempty()
    
    % $Revision: 1 $    $Date: 2009-01-06 11:39:22 +0100 (Tue, 06 Jan 2009) $
    % Author: David Eberhard
    
    %% Atributes
    properties
        data
    end

    %% Public Methods
    methods
        function obj = listenerArray(varargin)
            % Constructor for assocArray
            % Initialise to empty cell array with 2 columns
            obj.data = cell(0,2);
        end
        
        function set(obj,key,val)
            import mapseis.util.compareKeys;
        	%key = struct('Event','Callback','Object')
            % Define a key-value pair
			
			
            logInd = compareKeys(obj.data(:,1),key);
            if isempty(logInd) || all(~logInd)
                obj.data(end+1,:) = {key, val};
            else
                obj.data{logInd,2} = val;
            end
        end
        
        function keys=getKeys(obj)
            % Get the keys as a cell array
            keys = obj.data(:,1);
        end
        
        function val=lookup(obj,key)
             import mapseis.util.compareKeys;
            % Lookup a key value
            logInd = compareKeys(obj.data(:,1),key);
            try
            	val = obj.data{logInd,2};
        	catch
        		val = [];
        		%disp('eventlistener does not exist')
        	end	
        end
        
        function b=isEmpty(obj)
            % Test whether the associative array is empty
            b = isempty(obj.data);
        end
        
        function remove(obj,key)
             import mapseis.util.compareKeys;
            % Remove a key and value from the array
            logInd = compareKeys(obj.data(:,1),key);
            obj.data = obj.data(~logInd,:);
        end
       
   end    
   
   methods (Static)     
         function logInd = compareKeys(sav,key)
        	
        	n = numel(sav);
        	%logInd = logical(zeros(n,1));
        	logInd = [];
        	
        	%compare only the string of the function not the handle
        	key.Callback=func2str(key.Callback);
        	for c=1:n
        		
        		tempE=sav{c};
        		tempE.Callback=func2str(tempE.Callback);
        		
        		logInd(c) = isequal(key,tempE);
        	end
        end
        
    end

    
end
classdef addproxylistener < handle
    % addproxylistener : Based on the assocArray of DataStore. A hash to store the 
    % different eventlistener in a hash
    %   set(Key, Value), getKeys(), lookup(Key), remove(Key), isempty()
    
    % $Revision: 1 $    $Date: 2009-01-06 11:39:22 +0100 (Tue, 06 Jan 2009) $
    % Author: David Eberhard
    
    %% Atributes
    properties
        Hash
    end

    %% Public Methods
    methods
			
		function obj = addproxylistener()
			import mapseis.listenerproxy.listenerArray;
			
			obj.Hash = listenerArray();
			 
		end
		 
		function listen(obj,target,event,Callback,prefix)
        	%creates a new listener and deletes the old one if existing
        	%The prefix can be used or not, it will be added to the structur
        	%this allows to use more than one listener with equal syntax 
        	%(the key consisting of Event, Callback and Object, if they are 
        	%equally named, the listener will be overwritten)
        	%If the prefix is missing or empty the prefix 'std' will be used
        	
        	if nargin<5
        		prefix='std';
        	end
        	
        	if isempty(prefix)
        		prefix='std';
        	end	
        	
        	
        	key = struct(	'Event',event,...
        			'Callback',Callback,...
        			'Object',target,...
        			'Prefix',prefix);
        		
        	try 
        		hand = obj.Hash.lookup(key);
        		delete(hand);
        		obj.Hash.remove(key);
        		        		
        	end
        	
        	newhand = addlistener(target,event, @(s,e) Callback());
        	obj.Hash.set(key,newhand);
        	disp(['listener added ', event])
        		
        end	
        
        
        function quiet(obj,target,event,Callback,prefix)
       		%deletes a certain listener
        	
       		if nargin<5
        		prefix='std';
        	end
        	
        	if isempty(prefix)
        		prefix='std';
        	end	
       		
       		
        	key = struct(	'Event',event,...
        			'Callback',Callback,...
        			'Object',target,...
        			'Prefix',prefix);

        	
        	try 
        		hand = obj.Hash.lookup(key);
        		delete(hand);
        		obj.Hash.remove(key);
        		disp(['listener removed ', event])	
        	end

        end
        
        
        function shoutup(obj)
        	%delete all listeners
        	
        	allkeys = obj.Hash.getKeys;
        	n = numel(allkeys);
        	
        	for c=1:n
        		try 
        		hand = obj.Hash.lookup(allkeys{c});
        		delete(hand);
        		obj.Hash.remove(allkeys{c});
        		end
        	end
        
        end
        
        
        function PrefixQuiet(obj,prefix)
        	%stops and deletes all listener from a certain prefix
        	allkeys = obj.Hash.getKeys;
        	
        	for i=1:numel(allkeys)
        		try
        			hand=obj.Hash.lookup(allkeys{i});
        			if strcmp(allkeys{i}.Prefix,prefix)
        				delete(hand);	
        				obj.Hash.remove(allkeys{i});
        				%disp(['PrefixQuiet: removed ',prefix])
        			end
        		catch
        			%disp('I did something wrong')
        			
        		end
        		
        		
        	end
        	
        	
        end
        
        
        function foundEntries=PrefixFinder(obj,prefix)
        	%Returns the number of found entries with the given prefix
        	
        	foundEntries=0;
        	allkeys = obj.Hash.getKeys;
        	
        	for i=1:numel(allkeys)
       			if strcmp(allkeys{i}.Prefix,prefix)        			
       				foundEntries=foundEntries+1;
       			
       			end

        		
        		
        	end
        	
        	
        
        end
        
        
        function reinit(obj)
        	%delete and reinits all listeners
        
        	allkeys = obj.Hash.getKeys;
        	n = numel(allkeys);
        	
        	for c=1:n
        		try 
        		hand = obj.Hash.lookup(allkeys{c});
        		delete(hand);
        		obj.Hash.remove(allkeys{c});
        		end
        		
        		newhand = addlistener(allkeys{c}.Object, allkeys{c}.Event, @(s,e) allkeys{c}.Callback());
        		obj.Hash.set(allkeys{c},newhand);
        	end

        
        end
        
    end

    
end
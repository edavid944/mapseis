classdef RegionFilter < mapseis.filter.Filter
    % RegionFilter : creates a filter function based on event location
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified region.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 83 $    $Date: 2008-11-06 17:14:33 +0000 (Thu, 06 Nov 2008) $
    % Author: Matt McDonnell
    
    % Addition July 2011: 	Support for shifted coordinates:
    % 				No support is directly needed, add the moment 
    %				Everything is compared in the original coordinate
    %				system. The filter will set the region back to original
    %				coordinate system, but the offset has to be set in the 
    %				region, as the filter has no direct access to datastore
    %				in the constructor function.
    
    
    properties
        Region
        DepthRegion
        RangeSpec
        Width
        Radius
        Shifted
        UsedOffset
        
    end
    
    methods
        function obj = RegionFilter(rangeSpec, pRegion, varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            
            % Construct the subclass-specific properties
            obj.Region = pRegion;
            obj.RangeSpec = rangeSpec; 
            
            %out of compatibilty reason the the DepthRegion is set to empty 
            %and has to be set with setDepthRegion
            obj.DepthRegion=[];
            obj.Width = [];
            obj.Radius = [];
            obj.Shifted = false;
 	    if ~isempty(obj.Region)           
		    if ~isempty(obj.Region.Offsets)
			obj.UsedOffset=obj.Region.Offsets;
			if any(obj.UsedOffset~=0)
				obj.Shifted = true;
				if ~obj.Region.ShiftedBack
					obj.Region.ShiftBack([]);
				end
			end
			
		    end
            end
        end
        
        
        function setRegion(obj,regionType,newRegion)
            obj.Region = newRegion;
            obj.RangeSpec = regionType;
            %obj.DepthRegion=[];
            %obj.Width = [];
            
        end
        
        function logicAns=isProfileFilter(obj)
        	logicAns=false;
        
        end
        
        
        function setDepthRegion(obj,depthregion,width)
            obj.DepthRegion = depthregion;
            obj.Width = width;
        end

        
        function setWidth(obj,width)
        	obj.Width=width;
        end
        
        
        function setRadius(obj,rad)
        	obj.Radius=rad;
        end
        
        
        function regionParams = getRegion(obj)
            % Return the selection region
            regionParams = cell(2,1);
            regionParams{1} = obj.RangeSpec;
            regionParams{2} = obj.Region;
        end
        
         function regionParams = getDepthRegion(obj)
            % Return the selection region
            regionParams = cell(4,1);
            regionParams{1} = obj.RangeSpec;
            regionParams{2} = obj.Region;
            regionParams{3} = obj.DepthRegion;
            regionParams{4} = obj.Width;
        end

        
        function execute(obj,dataStore)
            % Region filter selects events based on longitude and latitude
            lonLatStruct = dataStore.getFields({'lon','lat'});
            lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
                        
            switch obj.RangeSpec
                case 'in'
                    obj.Selected = obj.Region.isInside(lonLatMatrix);                     
                case 'out'
                    obj.Selected = not(obj.Region.isInside(lonLatMatrix)); 
                case 'line'
                    obj.Selected = obj.Region.isInside(lonLatMatrix);  
                case 'circle'
                	%get the radius from datastore and select all events within the radius
                	%datagrid = dataStore.getUserData('gridPars');
			%reworked, now only a point will be drawn
				rad=obj.Radius;
                		lonmid = obj.Region(1);
				latmid = obj.Region(2);
				dista = distance(latmid,lonmid,...
				             lonLatMatrix(:,2),lonLatMatrix(:,1));
					
				obj.Selected = dista <= rad;
				
				
                otherwise
%                     warning('RegionFilter:unknown_range_type',...
%                         'RegionFilter: unknown range specification')
                    obj.Selected = true(dataStore.getRowCount,1);
            end
            % Call the execute method of the superclass, this should notify
            % any views of the filter
            execute@mapseis.filter.Filter(obj);
        end
    end    
    
end
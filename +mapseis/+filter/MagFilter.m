classdef MagFilter < mapseis.filter.RangeFilter
    % MagFilter : creates a filter function based on event magnitude
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified magnitude range.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        Projector
        TypeStr
        EmptyRange %needed when no datastore available
    end
    
    methods
        function obj = MagFilter(rangeSpec,range,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.RangeFilter(rangeSpec,range,varargin{:});
            % Set the subclass-specific properties
            obj.Projector = @mapseis.projector.getMagnitudes;
            obj.TypeStr =  {'all','above','below','in'};
            obj.EmptyRange = [1 9];
        end
        
        
        function execute(obj,dataStore,varargin)
            % Extract the magnitude vector from the data store
            magVect = dataStore.getFields({'mag'}).mag;
            
            switch obj.RangeSpec
                case 'in'
                    obj.Selected = (magVect >= obj.Range(1)) & (magVect <= obj.Range(2));
                case 'above'
                    obj.Selected = (magVect >= obj.Range(1));
                case 'below'
                    obj.Selected = (magVect <= obj.Range(2));
                case 'all'
                    obj.Selected = (magVect >= -Inf);
                otherwise
                    error('MagFilter:unknown_range_type',...
                        'MagFilter: unknown range specification')
            end
            % Call the superclass method
            execute@mapseis.filter.RangeFilter(obj,varargin{:});
        end

    end
end
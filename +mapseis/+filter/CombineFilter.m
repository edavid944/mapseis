function logIndVect=CombineFilter(filterCellArray,dataStore,updateFilter)
% CombineFilter : Combine filter functions to create a new filter

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell

% h = @(s) f(s) & g(s);

if nargin<3
    updateFilter = false;
end

% Number of filters
filterCount = numel(filterCellArray);
% Number of rows in the data
rowCount = dataStore.getRowCount();
% Accumulator vector
logIndVect = true(rowCount,1);
for filterIndex = 1:filterCount
    thisFilter = filterCellArray{filterIndex};
    % Execute each filter on the dataStore
    if updateFilter
        thisFilter.execute(dataStore);
    end
    logIndVect = logIndVect & thisFilter.getSelected();
end

end
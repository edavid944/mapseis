classdef RangeFilter < mapseis.filter.Filter
    %RANGEFILTER : filter defined by a scalar property range eg magnitude
    %   Abstract class
    
    properties
        Range
        RangeSpec
    end

    methods
        function obj = RangeFilter(rangeSpec,range,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            % Set the subclass-specific properties
            obj.Range = range;
            obj.RangeSpec = rangeSpec;
        end
        
        function rangeParams = getRange(obj)
            rangeParams = cell(2,1);
            rangeParams{1} = obj.RangeSpec;
            rangeParams{2} = obj.Range;
        end
        
        function setRange(obj,rangeSpec,range)
            obj.RangeSpec = rangeSpec;
            obj.Range = range;
        end
    end
    
end


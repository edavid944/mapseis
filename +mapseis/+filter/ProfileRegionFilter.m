classdef ProfileRegionFilter < mapseis.filter.Filter
   %Similar to the RegionFilter, but it works on a depth profile.
   
    
    properties
        Region
        RangeSpec
        Radius
        ProfileLine
        ProfileWidth
        selectedRegion
    end
    
    methods
        function obj = ProfileRegionFilter(rangeSpec,pline,width,rowIndices,pRegion, varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            
            % Construct the subclass-specific properties
            obj.Region = pRegion;
            obj.RangeSpec = rangeSpec; 
            obj.ProfileLine = pline;
            obj.ProfileWidth = width;
            obj.selectedRegion = rowIndices; 
            %out of compatibilty reason the the DepthRegion is set to empty 
            %and has to be set with setDepthRegion
         
            obj.Radius = [];
                       
        end
        
        
        function logicAns=isProfileFilter(obj)
        	logicAns=true;
        
        end
        
        
        function setRegion(obj,regionType,newRegion)
            obj.Region = newRegion;
            obj.RangeSpec = regionType;
            %obj.DepthRegion=[];
            %obj.Width = [];
            
        end
        
  
        
        function setRadius(obj,rad)
        	obj.Radius=rad;
        end
	

	function setProfile(obj,profline,profwid)
		 obj.ProfileLine = profline;
		 obj.ProfileWidth = profwid;
		
	end     
	
	function setMasterSelection(obj,selected)
		obj.selectedRegion = selected; 	
	
	end
        
        function regionParams = getRegion(obj)
            % Return the selection region
            regionParams = cell(2,1);
            regionParams{1} = obj.RangeSpec;
            regionParams{2} = obj.Region;
        end
        
         function regionParams = getProfile(obj)
            % Return the selection region
            regionParams = cell(2,1);
            regionParams{1} = obj.ProfileLine;
            regionParams{2} = obj.ProfileWidth;
           
        end

        
        function execute(obj,dataStore)
            import mapseis.projector.*;
             
        	
        
            % Region filter selects events based on longitude and latitude
            %lonLatStruct = dataStore.getFields({'lon','lat'});
            %lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
            
            rowCount = dataStore.getRowCount();
            proj=NaN(rowCount,1);
            unproj=proj;
            dep=proj;
            [proj(obj.selectedRegion) unproj] = getSliceProjections(dataStore,obj.ProfileLine,obj.ProfileWidth,obj.selectedRegion);
            dep(obj.selectedRegion) = getDepths(dataStore,obj.selectedRegion);
            
            LineDepthMatrix = [proj(:),dep(:)];

            
            switch obj.RangeSpec
                case 'in'
                    obj.Selected = obj.Region.isInside(LineDepthMatrix)&obj.selectedRegion;                     
                case 'out'
                    obj.Selected = not(obj.Region.isInside(LineDepthMatrix))&obj.selectedRegion; 
                case 'circle'
                	%get the radius from datastore and select all events within the radius
                	%datagrid = dataStore.getUserData('gridPars');
			%reworked, now only a point will be drawn
				rad=obj.Radius;
                		projmid = obj.Region(1);
				depmid = obj.Region(2);
				dista = sqrt(sum(((LineDepthMatrix(obj.selectedRegion,:)-...
					repmat([projmid depmid],sum(obj.selectedRegion),1)).^2),2));
					
				obj.Selected = (dista <= rad)&obj.selectedRegion;
				
				
                otherwise
%                     warning('RegionFilter:unknown_range_type',...
%                         'RegionFilter: unknown range specification')
                    obj.Selected = true(dataStore.getRowCount,1);
            end
            % Call the execute method of the superclass, this should notify
            % any views of the filter
            execute@mapseis.filter.Filter(obj);
        end
    end    
    
end

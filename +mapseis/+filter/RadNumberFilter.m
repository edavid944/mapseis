classdef RadNumberFilter < mapseis.filter.Filter
    
    % Similar to the DistFilter, but allows to specify a maximum number or only 
    %use a number
    %Added additional logic filtering, needed for the right selection
    
    
    properties
        Location
        Distance
        SelectionNumber
        SelectionMode
        CalculatedDistances
        AddSelection
        PaleRider
    end
    
    methods        
        function obj = RadNumberFilter(loc,dist,SelNum,SelMode,PaleRider,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            
            if (nargin-numel(varargin))<5
            	obj.PalerRider=false;
            end
            
            % Set the subclass-specific properties
            obj.Location = loc;
            obj.Distance = dist;
            obj.SelectionMode=SelMode;
            obj.SelectionNumber=SelNum;
            obj.CalculatedDistances=[];
            obj.AddSelection=[];
            obj.PaleRider=PaleRider;
            
        end
        
        function LimitSelection(obj,addLogic)
        	obj.AddSelection=addLogic;
        end
        
        function execute(obj,dataStore,varargin)
            % Extract the matrix of longitudes and latitudes from the
            % datastore
%             lonLatStruct = dataStore.getFields({'lon','lat'});
%             lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
%             
            import mapseis.projector.*;
           
            lonLatMatrix = getLocations(dataStore,[],obj.PaleRider);
            rowCount = dataStore.getRowCount();
            
            
            % Update the selected event vector
            
	    
	    if ~isempty(obj.AddSelection)
	    	obj.CalculatedDistances=NaN(rowCount,1);
	    	
	    	obj.CalculatedDistances(obj.AddSelection) = sqrt(sum(...
			((lonLatMatrix(obj.AddSelection,:)-repmat(obj.Location,sum(obj.AddSelection),1)).^2),2));
		NoMam=logical(zeros(rowCount,1));
	    		    	
	    else
	    	obj.CalculatedDistances = sqrt(sum(...
			((lonLatMatrix-repmat(obj.Location,rowCount,1)).^2),2));
		NoMam=logical(zeros(rowCount,1));
	    end
	    
            switch obj.SelectionMode
            	case 'Radius'
		    obj.Selected = obj.CalculatedDistances < obj.Distance;
			
		case 'Number'
			[sortedDist, TheInd] = sort(obj.CalculatedDistances);	
			obj.Selected=NoMam;
			obj.Selected(TheInd(1:obj.SelectionNumber)) = true;
			
		case 'Both'
			[sortedDist, TheInd] = sort(obj.CalculatedDistances);
			obj.Selected=NoMam;
			TheNearestIn=TheInd(1:obj.SelectionNumber);
			InRange=sortedDist(1:obj.SelectionNumber) < obj.Distance;
			obj.Selected(TheNearestIn(InRange)) = true;
			
		
	    end
            % Call the superclass method
            execute@mapseis.filter.Filter(obj,varargin{:});
        end
    end        
end
classdef SphereRangeFilter < mapseis.filter.Filter
    % SphereRangeFilter : creates a filter function based on event sphere range
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified sphere range.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        Range
        RangeSpec
        Location
    end
    
    methods
        function obj = SphereRangeFilter(rangeSpec,range,loc,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            % Set the subclass-specific properties
            obj.Range = range;
            obj.RangeSpec = rangeSpec;
            obj.Location = loc;
        end
        
        
        function execute(obj,dataStore,varargin)
            % Region filter selects events based on longitude and latitude
            
            lonVect = dataStore.getFields({'lon'}).lon;
            latVect = dataStore.getFields({'lat'}).lat;
            
            switch rangeSpec
                case 'circle'
                    rad = range(1);                    
                    obj.Selected = ...
                        deg2km(distance(latVect, lonVect, ...
                        repmat(obj.Location(2),numel(latVect),1),...
                        repmat(obj.Location(1),numel(lonVect),1))) < rad;                    
                case 'sphere'
                    rad = range(1);                    
                    obj.Selected = ...
                        sqrt( (s.depth-obj.Location(3)).^2 + ...
                        (deg2km(distance(...
                        latVect, lonVect, repmat(obj.Location(2),numel(latVect),1),...
                        repmat(obj.Location(1),numel(lonVect),1)))).^2 ) < rad;
                case 'shell'
                    
                case 'all'
                    obj.Selected = latVect >= -Inf;
                otherwise
                    error('SphereRangeFilter:unknown_range_type',...
                        'SphereRangeFilter: unknown range specification')
            end
            % Call the superclass method
            execute@mapseis.filter.Filter(obj,varargin{:});
        end
    end
    
end
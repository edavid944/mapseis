classdef MinuteRangeFilter < mapseis.filter.RangeFilter
    % MakeTimeFilter : creates a filter function based on event date
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified time range.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 76 $    $Date: 2008-10-20 16:56:26 +0100 (Mon, 20 Oct 2008) $
    % Author: Matt McDonnell
    
    properties
        Projector
        TypeStr
        EmptyRange
    end
    
    methods
        function obj = MinuteRangeFilter(rangeSpec,range,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.RangeFilter(rangeSpec,range,varargin{:});
            % Set subclass-specific properties
            obj.Projector = @mapseis.projector.getMinutes;
            obj.TypeStr = {'all','above','below','in'};
            obj.EmptyRange = [0 59]; %in case no datastore is set 
        end
        
        
        function execute(obj,dataStore,varargin)
            % Execute the HourRangeFilter on the dataStore specified
            
            % Region filter selects events based on longitude and latitude
            % dateNumVect = dataStore.getFields({'dateNum'}).dateNum;
            dateNumVect = dataStore.getUserData('Minute');
                                    
            switch obj.RangeSpec
                case 'in'
                    startHour = obj.Range(1);
                    endHour = obj.Range(2);
                    %obj.Selected =  (str2num(datestr(dateNumVect,'MM')) >= startHour)...
                    %    & (str2num(datestr(dateNumVect,'MM')) <= endHour);
                	
                	obj.Selected =  (dateNumVect >= startHour)...
                        & (dateNumVect <= endHour);

                
                case 'above'
                    % Depth is measured with +ve towards the centre of the earth, ie if
                    % an event is 'above' a given depth is is actually 'deeper'
                    startHour = obj.Range(1);
                    %obj.Selected = (str2num(datestr(dateNumVect,'MM')) >= startHour);
                
                	obj.Selected = (dateNumVect >= startHour);
                	
                case 'below'
                    endHour = obj.Range(2);
                    %obj.Selected = (str2num(datestr(dateNumVect,'MM')) <= endHour);
               		obj.Selected = (dateNumVect <= endHour);
               		
                case 'all'
                    %obj.Selected = (str2num(datestr(dateNumVect,'MM')) >= -Inf);
                	obj.Selected = (dateNumVect >= -Inf);
                
                otherwise
                    error('MinuteRangeFilter:unknown_range_type',...
                        'MinuteRangeFilter: unknown range specification')
            end
            % Call the superclass method
            execute@mapseis.filter.RangeFilter(obj,varargin{:});
        end
        
    end
    
end

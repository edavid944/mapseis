classdef DepthFilter < mapseis.filter.RangeFilter
    % DepthFilter : creates a filter function based on event depth
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified depth range.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        Projector
        TypeStr
        EmptyRange
    end
    
    methods
        function obj = DepthFilter(rangeSpec,range,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.RangeFilter(rangeSpec,range,varargin{:});
            % Set the subclass-specific properties
            obj.Projector = @mapseis.projector.getDepths;
            obj.TypeStr = {'all','above','below','in'};
            obj.EmptyRange = [0 20]; %set a default value if no datastore is available
        end
        
        function execute(obj,dataStore,varargin)
            % Extract the depth vector from the data store
            depthVect = dataStore.getFields({'depth'}).depth;
            
            switch obj.RangeSpec
                case 'in'
                    startDepth = obj.Range(1);
                    endDepth = obj.Range(2);
                    obj.Selected = (depthVect >= startDepth) & (depthVect <= endDepth);
                case 'above'
                    % Depth is measured with +ve towards the centre of the earth, ie if
                    % an event is 'above' a given depth is is actually 'deeper'
                    startDepth = obj.Range(1);
                    obj.Selected = (depthVect >= startDepth);
                case 'below'
                    endDepth = obj.Range(2);
                    obj.Selected = (depthVect <= endDepth);
                case 'all'
                    obj.Selected = (depthVect >= -Inf);
                otherwise
                    error('DepthFilter:unknown_range_type',...
                        'DepthFilter: unknown range specification')
            end
            % Call the superclass method
            execute@mapseis.filter.RangeFilter(obj,varargin{:});
        end
        
    end
end
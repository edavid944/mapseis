classdef Filter < handle
    %FILTER : Selection filter to apply to DataStore
    %   Filter object is defined by its range, 
    
    properties
        Selected
        Name
    end
    
    events
        FilterUpdated
    end
    
    methods
        function obj = Filter(filterName) % , dataStoreHandle)
            % Filter constructor
            % Specify the filter name
            obj.Name = filterName;
            % Default to all elements selected
            obj.Selected = []; % true(dataStoreHandle.getRowCount(),1);
        end        
        
        function logIndexVect = getSelected(obj)
            logIndexVect = obj.Selected;
        end
        
        function logIndexVect = getUnselected(obj)
            logIndexVect = ~obj.Selected;
        end
        
        function execute(obj,doUpdate)
            % Template execute method, emits the FilterUpdated event when
            % the selection criterion changes or the data store is modified
            
            % Default to notifying the observers
            if nargin<2
                doUpdate=true;
            end
            if doUpdate
                notify(obj,'FilterUpdated');
            end
        end
    end    
    
end


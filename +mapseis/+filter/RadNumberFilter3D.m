classdef RadNumberFilter3D < mapseis.filter.Filter
    
    % Similar to the DistFilter, but allows to specify a maximum number or only 
    %use a number
    %This is the 3D version of the RadNumberFilter, it is loosely based on the
    %SphereRangeFilter (which seems to unfinished, anyway)
    
    
    properties
        Location
        Distance
        SelectionNumber
        SelectionMode
        CalculatedDistances
        AddSelection
    end
    
    methods        
        function obj = RadNumberFilter3D(loc,dist,SelNum,SelMode,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            % Set the subclass-specific properties
            obj.Location = loc;
            obj.Distance = dist;
            obj.SelectionMode=SelMode;
            obj.SelectionNumber=SelNum;
            obj.CalculatedDistances=[];
            obj.AddSelection=[];
        end
        
        
        function LimitSelection(obj,addLogic)
        	obj.AddSelection=addLogic;
        end
        
        
        function execute(obj,dataStore,varargin)
            % Extract the matrix of longitudes and latitudes from the
            % datastore
%             lonLatStruct = dataStore.getFields({'lon','lat'});
%             lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
%             
            import mapseis.projector.*;
            
            %get data
            lonLatMatrix = getLocations(dataStore);
            depths=getDepths(dataStore);
            rowCount = dataStore.getRowCount();
            
            if isempty(obj.AddSelection)
            	    obj.CalculatedDistances =  sqrt( (depths-obj.Location(3)).^2 + ...
                        (deg2km(distance(...
                        lonLatMatrix(:,2), lonLatMatrix(:,1), ...
                        repmat(obj.Location(2),numel( lonLatMatrix(:,2)),1),...
                        repmat(obj.Location(1),numel(lonLatMatrix(:,1)),1)))).^2 )
                    NoMam=logical(zeros(rowCount,1));  
                
                    
            else
            	obj.CalculatedDistances=NaN(rowCount,1);
            	
            	obj.CalculatedDistances(obj.AddSelection) =  sqrt( (depths(obj.AddSelection)-obj.Location(3)).^2 + ...
                        (deg2km(distance(...
                        lonLatMatrix(obj.AddSelection,2), lonLatMatrix(obj.AddSelection,1), ...
                        repmat(obj.Location(2),numel( lonLatMatrix(obj.AddSelection,2)),1),...
                        repmat(obj.Location(1),numel(lonLatMatrix(obj.AddSelection,1)),1)))).^2 );
                    NoMam=logical(zeros(rowCount,1));  
            	
            	
            end
            
            
           
            
            % Update the selected event vector
         
            
            
            
	    
            switch obj.SelectionMode
            	case 'Radius'
		    obj.Selected = obj.CalculatedDistances < obj.Distance;
			
		case 'Number'
			[sortedDist, TheInd] = sort(obj.CalculatedDistances);	
			obj.Selected=NoMam;
			obj.Selected(TheInd(1:obj.SelectionNumber)) = true;
			
		case 'Both'
			[sortedDist, TheInd] = sort(obj.CalculatedDistances);
			obj.Selected=NoMam;
			TheNearestIn=TheInd(1:obj.SelectionNumber);
			InRange=sortedDist(1:obj.SelectionNumber) < obj.Distance;
			obj.Selected(TheNearestIn(InRange)) = true;
			
		
	    end
            % Call the superclass method
            execute@mapseis.filter.Filter(obj,varargin{:});
        end
    end        
end

classdef DistFilter < mapseis.filter.Filter
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        Location
        Distance
    end
    
    methods        
        function obj = DistFilter(loc,dist,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            % Set the subclass-specific properties
            obj.Location = loc;
            obj.Distance = dist;
        end
        
        function execute(obj,dataStore,varargin)
            % Extract the matrix of longitudes and latitudes from the
            % datastore
%             lonLatStruct = dataStore.getFields({'lon','lat'});
%             lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
%             
            import mapseis.projector.*;
            lonLatMatrix = getLocations(dataStore);
            rowCount = dataStore.getRowCount();
            % Update the selected event vector
            obj.Selected = sqrt(...
                sum(...
                ((lonLatMatrix-repmat(obj.Location,rowCount,1)).^2),2)...
                ) < obj.Distance;
            % Call the superclass method
            execute@mapseis.filter.Filter(obj,varargin{:});
        end
    end        
end
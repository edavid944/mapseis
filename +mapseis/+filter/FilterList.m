classdef FilterList < handle
    %FILTERLIST : Composite filter object
    %   Used to apply a group of filters in series
    
    properties
        Filters
        Selected
        DataStore
        FilterListName
        ListenForUpdate
        ListID
        Changed
        RandomID
        Packed
    end
    
    events
        Update
    end
    
    methods
    	function obj = FilterList(filterArray,dataStore,ListProxy)
    		% Constructor : input is cell array of filters
    		obj.Filters = filterArray;

    		if ~isempty(dataStore)
    			obj.Selected = true(dataStore.getRowCount,1);
    			obj.DataStore = dataStore;
			%obj.ListProxy = ListProxy;
					    
			%changed can be used to determine if a list was changed, it will be 
			%set to true only if the filters are changed, not when the datstore
			%is changed. Note: the variable will never be set to false in the 
			%filterlist itself, this has to be done from the outside.
			obj.Changed=true;
					    
			%ListID will be used in the Listproxy as means of identification
			obj.ListID=num2str(round(abs(100*randn)));
			obj.Packed=false;
					    
			%add name from the datastore (no name parameter on creation 
			%out of compatibility reason)
			try
				cataname = obj.DataStore.getUserData('filename');
			catch	
				cataname = 'not defined';
			end	
			
			obj.FilterListName = ['Filterlist ', cataname];	
						
			% Addlistener for filter updates
			for filterIndex = 1:numel(filterArray)
				if isempty(ListProxy)
					addlistener(filterArray{filterIndex},'FilterUpdated',...
						@(s,e) obj.updateFilters());
				else
					ListProxy.listen(filterArray{filterIndex},'FilterUpdated',...
						@(s,e) obj.updateFilters(),['filtlist',obj.ListID]);    
				end			    
			end
			
			% Default to listening to filter changes
			obj.ListenForUpdate = true;
					    
			%reset Random ID
			obj.RandomID=rand;
				    
		else
			%generate a packed filterlist
			obj.Changed=true;
			obj.ListID=num2str(round(abs(100*randn)));
			obj.FilterListName = ['Filterlist ', 'noname'];	
			obj.ListenForUpdate = true;
			
			obj.DataStore=[];
			obj.RandomID=-1;
			obj.Selected=[];
			obj.Packed=true;
		
			    
		end
   
            
	end
        
        
        
        function outFilter = getByName(obj,filterName)
            % Return a filter indexed by name
            filterFound = false;
            for filterIndex=1:numel(obj.Filters)
                thisFilter = obj.Filters{filterIndex};
                if strcmp(thisFilter.Name,filterName)
                    outFilter = thisFilter;
                    filterFound = true;
                    break % Out of for loop
                end
            end
            if ~filterFound
                outFilter = [];
            end
        end
            
        function logIndVect = excludeFilter(obj,filterName)
        	%returns the events selected by all filters
        	%expect the one mentioned with filterName
        	
        	%raw array
        	logIndVect=logical(ones(numel(obj.Selected),1));
        	
        	for filterIndex=1:numel(obj.Filters)
                	thisFilter = obj.Filters{filterIndex};
                	if ~strcmp(thisFilter.Name,filterName)
                		logIndVect=logIndVect&thisFilter.getSelected;
                	end	
                	
                end
                  	
              
        end
        
        
        
        function logIndVect = getSelected(obj)
            logIndVect = obj.Selected;
        end
        
        function logIndVect = getUnselected(obj)
            logIndVect = ~obj.Selected;
        end
        
        function update(obj)
            % Update the selected rows when the datastore is updated
            import mapseis.filter.*;
            oldselect=obj.Selected;
            obj.setListenForUpdate(false);
            doUpdate = true; % Update all child filters 
            obj.Selected = CombineFilter(...
            obj.Filters,obj.DataStore,doUpdate);
            
            try
            	changed=~all(oldselect==obj.Selected);
            catch
            	changed=true;
            end
            
            %reset Random ID
            if changed
            	obj.RandomID=rand;
    	    end
    	    
            notify(obj,'Update');
            %disp('Yipiahee 1')
            obj.setListenForUpdate(true);  
        end
        
        function updateNoEvent(obj)
            % Update the selected rows when the datastore is updated
            %does not send a event
            import mapseis.filter.*;
            oldselect=obj.Selected;
            obj.setListenForUpdate(false);
            doUpdate = true; % Update all child filters 
            obj.Selected = CombineFilter(...
            obj.Filters,obj.DataStore,doUpdate);
            
            try
            	changed=~all(oldselect==obj.Selected);
            catch
            	changed=true;
            end
            
            %reset Random ID
            if changed
            	obj.RandomID=rand;
    	    end
    	    
            %notify(obj,'Update');
            %disp('Yipiahee 1')
            obj.setListenForUpdate(true);  
        end
        
        
        function updateFilters(obj)
            % Update the selected rows when the filter is updated
            import mapseis.filter.*;
            
            
            if obj.ListenForUpdate
                doUpdate = false; % Since called by filter update event
                obj.Changed=true;
                oldselect=obj.Selected;
                obj.Selected = CombineFilter(...
                    obj.Filters,obj.DataStore,doUpdate);       
                try
			changed=~all(oldselect==obj.Selected);
		catch
			changed=true;
		end
		    
		%reset Random ID
		if changed
			obj.RandomID=rand;
		end    
                notify(obj,'Update');
                %disp('Yipiahee 2')
            end
        end
        
        function setListenForUpdate(obj,doUpdate)
            % Turn on or off update when filters in list are changed.  Used
            % mainly when loading a new data set
            obj.ListenForUpdate = doUpdate;
        end
        
        function filterList = getRangeFilters(obj)
            % Return the logical index vector of range filters
            filterCount = numel(obj.Filters);
            logIndVect = false(filterCount,1);
            for filterIndex = 1:filterCount      
                %added workaround
                tempvar=obj.Filters{filterIndex};
                logIndVect(filterIndex) = ...
                    isa(tempvar,...
                    'mapseis.filter.RangeFilter');
            end

            filterList = obj.Filters(logIndVect);
        	
        end
        
        function changeData(obj,dataStore)
        	%The function is now smarter it will only replace and reset 
        	%the id if the datastore is really different
        	if isempty(obj.DataStore)
        		obj.DataStore=dataStore;
        		obj.Selected = true(obj.DataStore.getRowCount,1);
        		obj.RandomID=rand;
        	else
        	
        		if ~(obj.DataStore==dataStore) 
        			obj.DataStore=dataStore;
        			obj.Selected = true(obj.DataStore.getRowCount,1);
        			obj.RandomID=rand;
        		end
        	end
        end
        
        
        
        function listname = getName(obj)
        	
        	if ~isempty(obj.FilterListName)
        		listname = obj.FilterListName;
        	else
        		listname = 'noname'
        	end		
        
        end
        
        
        
        function setName(obj, newname)
        	if ~isempty(newname)
        		obj.FilterListName = newname
        	else
        		cataname = obj.DataStore.getUserData('filename');
        		obj.FilterListName = ['Filterlist ', cataname];	
        	end
        end
        
        function MuteList(obj,ListProxy)
        	ListProxy.PrefixQuiet(['filtlist',obj.ListID]);
        	disp(['filtlist',obj.ListID, ' muted' ])
        end
        
        
        function youOk=AreYouListen(obj,ListProxy);
        	NRLister=ListProxy.PrefixFinder(['filtlist',obj.ListID]);
        	youOk=(NRLister>0);
        end
        
        
        function youOk=AutoReinit(obj,ListProxy)
        	%Same as reInitList but checks if Reinit is needed
        	youOk=AreYouListen(obj,ListProxy);
        	
        	if ~youOk
        		obj.reInitList(ListProxy);
        	end	
        end
        
        
        function reInitList(obj,ListProxy)
        	% reapplies the listeners to the filters
        	filterArray = obj.Filters;
        	ListProxy.PrefixQuiet(['filtlist',obj.ListID]);
            % Addlistener for filter updates
            for filterIndex = 1:numel(filterArray)
                %addlistener(filterArray{filterIndex},'FilterUpdated',...
                %    @(s,e) obj.updateFilters());
            	 ListProxy.listen(filterArray{filterIndex},'FilterUpdated',...
                    @(s,e) obj.updateFilters(),['filtlist',obj.ListID]); 
            end
            % Default to listening to filter changes
            obj.ListenForUpdate = true;
            
            disp(['filtlist',obj.ListID, ' reinited' ])
        
        end
        
        
        function ResetRandID(obj)
    		%forces a new Random ID
    		obj.RandomID=rand;
    	
    	end
    	
    	function RandID=GiveID(obj)
    		%Returns the current random ID
    		%reset Random ID
    		RandID=obj.RandomID;
	end
	
	function PackIt(obj)
		%This removes datastore and selected from the filterlist
		%makes the files smaller used to save a datastore
		obj.DataStore=[];
		obj.RandomID=-1;
		obj.Selected=[];
		obj.Packed=true;
		
	end
        
       end
    
end


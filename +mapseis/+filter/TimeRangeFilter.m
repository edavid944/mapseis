classdef TimeRangeFilter <  mapseis.filter.RangeFilter
    % TimeRangeFilter : creates a filter function based on event date
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified time range.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        Projector
        TypeStr
        EmptyRange
    end
    
    methods
        function obj = TimeRangeFilter(rangeSpec,range,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.RangeFilter(rangeSpec,range,varargin{:});
            % Set the subclass-specific properties
            % Set the subclass-specific properties
            obj.Projector = @mapseis.projector.getTimes;
            obj.TypeStr = {'all','after','before','in'};
            obj.EmptyRange = [726834 734500];
        end
        
        
        function execute(obj,dataStore,varargin)
            % Region filter selects events based on longitude and latitude
            dateNumVect = dataStore.getFields({'dateNum'}).dateNum;                        
            switch obj.RangeSpec
                case 'in'
                    startDateNum = obj.Range(1);
                    endDateNum = obj.Range(2);
                    obj.Selected = (dateNumVect >= startDateNum) & (dateNumVect <= endDateNum);
                case 'after'
                    startDateNum = obj.Range(1);
                    obj.Selected = (dateNumVect >= startDateNum);
                case 'before'
                    endDateNum = obj.Range(2);
                    obj.Selected = (dateNumVect <= endDateNum);
                case 'all'
                    obj.Selected = dateNumVect >= -Inf;
                otherwise
                    error('TimeRangeFilter:unknown_range_type',...
                        'TimeRangeFilter: unknown range specification')
            end
            % Call the superclass method
            execute@mapseis.filter.RangeFilter(obj,varargin{:});
        end        
    end
    
end
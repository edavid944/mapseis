function filterList = FilterListCloner(orgFList,Datastore,ListProxy)
	%clones a filterlist, either completely with selection vector and 
	%everything, if a Datastore is include, or as a empty filter, if 
	%no datastore is specified	
	

	import mapseis.filter.*;
	import mapseis.datastore.*;
	import mapseis.plot.*;
	
	if nargin<2
		Datastore=[];
		ListProxy=[];
		
	elseif nargin<3
		ListProxy=[];
	end
	
	%get data of the old filter
	RegFilt=orgFList.getByName('Region');
	TimeFilt=orgFList.getByName('Time');
	MagFilt=orgFList.getByName('Magnitude');
	DepthFilt=orgFList.getByName('Depth');
	HrFilt=orgFList.getByName('Hour');
	MinFilt=orgFList.getByName('Minute');
	
	Regio=RegFilt.getRegion;
	Timing=TimeFilt.getRange;
	Mags=MagFilt.getRange;
	Depths=DepthFilt.getRange;
	Hours=HrFilt.getRange;
	Minutes=MinFilt.getRange;
	
	%build new filters
	regionFilter = RegionFilter(Regio{1},Regio{2},'Region');
	timeFilter = TimeRangeFilter(Timing{1},Timing{2},'Time');
	magFilter = MagFilter(Mags{1},Mags{2},'Magnitude');
	depthFilter = DepthFilter(Depths{1},Depths{2},'Depth');
	hourFilter = HourRangeFilter(Hours{1},Hours{2},'Hour');
	minuteFilter = MinuteRangeFilter(Minutes{1},Minutes{2},'Minute');
	
	if ~isempty(Datastore)
		% Initialise the filters to make the selection vectors the correct length
		regionFilter.execute(Datastore);
		timeFilter.execute(Datastore);
		magFilter.execute(Datastore);
		depthFilter.execute(Datastore);
		hourFilter.execute(Datastore);
		minuteFilter.execute(Datastore);
		
		%Add Region to regionfilter
		SetRegion([],regionFilter,'all',Datastore);
	end
	
	% Composite FilterList object
	filterList = FilterList({regionFilter,timeFilter,magFilter,...
	    depthFilter,hourFilter,minuteFilter},Datastore,ListProxy);
	    
	%change name of the cloned filterlist
	oldName=orgFList.FilterListName;
	filterList.FilterListName=['Cloned ',oldName];

end

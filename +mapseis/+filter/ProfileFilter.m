classdef ProfileFilter < mapseis.filter.Filter
    
    %     % $Revision: 1 $    $Date: 2008-09-12$
    %     % $Author: David Eberhard
    % The Profile filter is similar to the DistFilter, but works on depth profile
    
    properties
        Location
        Distance
        ProfileLine
        ProfileWidth
        selectedRegion
    end
    
    methods        
        function obj = ProfileFilter(loc,dist,pline,width,rowIndices,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            % Set the subclass-specific properties
            obj.Location = loc;
            obj.Distance = dist;
            obj.ProfileLine = pline;
            obj.ProfileWidth = width;
            obj.selectedRegion = rowIndices; 
            
        end
        
        function execute(obj,dataStore,varargin)
            % Extract the matrix of longitudes and latitudes from the
            % datastore
%             lonLatStruct = dataStore.getFields({'lon','lat'});
%             lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
%             
            import mapseis.projector.*;
            [proj unproj] = getSliceProjections(dataStore,obj.ProfileLine,obj.ProfileWidth,obj.selectedRegion);
            dep = getDepths(dataStore,obj.selectedRegion);
            
            LineDepthMatrix = [proj(:),dep(:)];
            try rowCount = sum(obj.selectedRegion);
            	catch
            		rowCount = dataStore.getRowCount();
            end
            % Update the selected event vector
            obj.Selected = sqrt(...
                sum(...
                ((LineDepthMatrix-repmat(obj.Location,rowCount,1)).^2),2)...
                ) < obj.Distance;
            % Call the superclass method
            execute@mapseis.filter.Filter(obj,varargin{:});
        end
    end        
end
classdef HourRangeFilter < mapseis.filter.RangeFilter
    % MakeTimeFilter : creates a filter function based on event date
    % Returns a filter function that can be applied to a data store to return
    % events occurring within a specified time range.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        Projector
        TypeStr
        EmptyRange
    end
    
    methods
        function obj = HourRangeFilter(rangeSpec,range,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.RangeFilter(rangeSpec,range,varargin{:});
            % Set subclass-specific properties
            obj.Projector = @mapseis.projector.getHours;
            obj.TypeStr = {'all','above','below','in'};
            obj.EmptyRange = [0 24];
        end
        
        
        function execute(obj,dataStore,varargin)
            % Execute the HourRangeFilter on the dataStore specified
            
            % Region filter selects events based on longitude and latitude
            dateNumVect = dataStore.getUserData('Hour');
                        
            switch obj.RangeSpec
                case 'in'
                    startHour = obj.Range(1);
                    endHour = obj.Range(2);
                    %obj.Selected =  (str2num(datestr(dateNumVect,'HH')) >= startHour)...
                    %    & (str2num(datestr(dateNumVect,'HH')) <= endHour);
                
                	obj.Selected =  (dateNumVect >= startHour)...
                        & (dateNumVect <= endHour);

                case 'above'
                    % Depth is measured with +ve towards the centre of the earth, ie if
                    % an event is 'above' a given depth is is actually 'deeper'
                    startHour = obj.Range(1);
                %    obj.Selected = (str2num(datestr(dateNumVect,'HH')) >= startHour);
                	obj.Selected = (dateNumVect >= startHour);
                
                case 'below'
                    endHour = obj.Range(2);
                %    obj.Selected = (str2num(datestr(dateNumVect,'HH')) <= endHour);
                	obj.Selected = (dateNumVect <= endHour);
                
                case 'all'
                %    obj.Selected = (str2num(datestr(dateNumVect,'HH')) >= -Inf);
                	obj.Selected = (dateNumVect >= -Inf);
                
                otherwise
                    error('HourRangeFilter:unknown_range_type',...
                        'HourRangeFilter: unknown range specification')
            end
            % Call the superclass method
            execute@mapseis.filter.RangeFilter(obj,varargin{:});
        end
        
    end
    
end

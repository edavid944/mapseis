classdef ProfileFilterV2 < mapseis.filter.Filter
    
    %     % $Revision: 1 $    $Date: 2008-09-12$
    %     % $Author: David Eberhard
    % The Profile filter is similar to the DistFilter, but works on depth profile
    %New in v2: support for number and number-radius selection like RadNumberFilter
    
    
    properties
        Location
        Distance
        SelectionNumber
        SelectionMode
        CalculatedDistances
        ProfileLine
        ProfileWidth
        selectedRegion
    end
    
    methods        
        function obj = ProfileFilterV2(loc,dist,pline,width,rowIndices,SelNum,SelMode,varargin)
            % Call the superclass constructor
            obj = obj@mapseis.filter.Filter(varargin{:});
            % Set the subclass-specific properties
            obj.Location = loc;
            obj.Distance = dist;
            obj.SelectionMode=SelMode;
            obj.SelectionNumber=SelNum;
            obj.ProfileLine = pline;
            obj.ProfileWidth = width;
            obj.selectedRegion = rowIndices; 
            
        end
        
        function execute(obj,dataStore,varargin)
            % Extract the matrix of longitudes and latitudes from the
            % datastore
%             lonLatStruct = dataStore.getFields({'lon','lat'});
%             lonLatMatrix = [lonLatStruct.lon, lonLatStruct.lat];
%             
            import mapseis.projector.*;
            
            %pad it with NaN
            rowCount = dataStore.getRowCount();
            proj=NaN(rowCount,1);
            unproj=proj;
            dep=proj;
            obj.CalculatedDistances=proj;
            
            [proj(obj.selectedRegion) unproj] = getSliceProjections(dataStore,obj.ProfileLine,obj.ProfileWidth,obj.selectedRegion);
            dep(obj.selectedRegion) = getDepths(dataStore,obj.selectedRegion);
            
            LineDepthMatrix = [proj(:),dep(:)];
            %try 
            %	rowCount = sum(obj.selectedRegion);
            %catch
            %	rowCount = dataStore.getRowCount();
            %end
            
            %now calc the distances
            
            % Update the selected event vector
            obj.CalculatedDistances(obj.selectedRegion) = sqrt(sum(...
                ((LineDepthMatrix(obj.selectedRegion,:)-repmat(obj.Location,sum(obj.selectedRegion),1)).^2),2));
            NoMam=logical(zeros(rowCount,1));
             switch obj.SelectionMode
            	case 'Radius'
		    obj.Selected = obj.CalculatedDistances < obj.Distance;
			
		case 'Number'
			[sortedDist, TheInd] = sort(obj.CalculatedDistances);	
			obj.Selected=NoMam;
			obj.Selected(TheInd(1:obj.SelectionNumber)) = true;
			
		case 'Both'
			[sortedDist, TheInd] = sort(obj.CalculatedDistances);
			obj.Selected=NoMam;
			TheNearestIn=TheInd(1:obj.SelectionNumber);
			InRange=sortedDist(1:obj.SelectionNumber) < obj.Distance;
			obj.Selected(TheNearestIn(InRange)) = true;
			
		
	    end    
            
            % Call the superclass method
            
            
            
            execute@mapseis.filter.Filter(obj,varargin{:});
        end
    end        
end
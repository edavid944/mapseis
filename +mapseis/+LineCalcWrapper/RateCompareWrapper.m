classdef RateCompareWrapper < handle

	properties
		CalculationHash
		ResultGUI
		CalcParameterGUI
		PointConfig
		ListProxy
		Commander
		ErrorDialog
		ParamGUIEntries %not optional
		ResultGUIConfig %not optional
		ResultStructur
		DataNFilter
		CalcThings
		CalcKeyList
		PointColor
		ColorWheel
		AvailableCalcTypes
		NextKey
		NeededResultGUI
		NeededParamGUI
		CumNumData
	end

	
	events
		CalcDone
		CalcHashUpdated
		UpdateResultGUI
	end

	

	methods
	
		function obj = RateCompareWrapper(ListProxy,Commander,GUIswitch)
			import mapseis.datastore.*;
			
			%Constuctor, generates the Calculation objects and opens a suitable gui if 
			% GUIswitch = true else now GUI is produced, maybe usefull if only calculations are
			% wanted
			
			%only needed if entrypoints are used
			obj.PointColor=1;
			obj.ColorWheel={'blue','red','green','black','cyan','magenta'};
			obj.NextKey='1';
			
			%The two needed Gui version, no function at the moment but good idea to not them anyway
			obj.NeededResultGUI='1.0'
			obj.NeededParamGUI='1.0'
			obj.CalcThings=false;
			obj.CumNumData={};
			%for the GUI if set to one only the first type of calculation is available.
			obj.AvailableCalcTypes(1)=0;
			obj.AvailableCalcTypes(2)=0;
			
			obj.PointConfig={};
			obj.ParamGUIEntries={};
			
			obj.CalculationHash=assocArray();
			
			obj.ListProxy=ListProxy;
			obj.Commander = Commander; 
			
			%get the current datastore and filterlist
			currentData = obj.Commander.getCurrentDatastore;
			currentFilter = obj.Commander.getCurrentFilterlist; 	
			
			%by default: produce to Calculation elements of Calc_bval_nofit
			%now produce two calculation object in the hash
			CalcKey{1} = addCalculation(obj,'bval_rate');
			CalcKey{2} = addCalculation(obj,'bval_rate');
			
			
			
			%if a gui is existing:
			if GUIswitch				
			
				%build the CalcSlide for the Parameter GUI
				obj.ModifyCalcConfig('add',CalcKey{1});
				obj.ModifyCalcConfig('add',CalcKey{2});
				
				%Update the ResultGUI Config
				obj.ResultConfigBuilder;
				
			
				
			end
			
			
		end
	
		
		function RegisterGUI(obj,toRegister,GUIType,RegParameter)
			%simply writes the GUI to the internal variables
			%May add support for more than one GUI of one Type
			%The RegParameter variable can be used to specify the interface
			%a bit more
			
			switch GUIType
				case 'ParamGUI'
					obj.CalcParameterGUI=toRegister;
					%not needed anymore, will be written for each calculation
					%obj.CalcParameterGUI.CalcParams=obj.ParamGUIEntries;
					
					%update ParameterWindow
					obj.notify('CalcHashUpdated');
					
				case 'ResultGUI'	
					obj.ResultGUI=toRegister;					
					obj.ResultGUI.PointConfig=obj.createPointStructur;
					obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;
					obj.ResultGUI.PointMaker('start');
					
					
					%try delete and set listener (just to be cautious)
					obj.ListProxy.quiet(obj.ResultGUI,'DrawIt',...
						@(s,e) obj.drawPlots(RegParameter),'wrapper'); 
					obj.ListProxy.listen(obj.ResultGUI,'DrawIt',...
						@(s,e) obj.drawPlots(RegParameter),'wrapper'); 
	
					%Update ResultViewer
					obj.notify('CalcHashUpdated')
						
			end	
			
		
		end
		
		
		function ResultEndProcess(obj)
			%This function can be called after the creation of the windows 
			%allowing to used some default drawings. Had to be added because of 
			%multithreading and synchronity.
			%Can be used by setting the field EndProcess in the GUIConfig of the ResultGUI
			%to the string 'call'
			obj.ResultGUI.Drawer('TopPlot');
			
		end
		
		
		
		
		function RefreshGUI(obj,GUIType,RegParameter);
			%Similar to RegisterGUI, but does just rewrite the GUI Parameter
			switch GUIType
				case 'ParamGUI'
					obj.CalcParameterGUI.CalcParams=obj.ParamGUIEntries;
					
				case 'ResultGUI'	
					obj.ResultGUI.ResultConfig=obj.ResultGUIConfig;	
			end	
			
		end	
		
		
		function CalcKey = addCalculation(obj,CalcType)
			%adds a new rate Calculation and a new Magnitude Signature Calculation to the hash 
			% A hash entry consists of:
				%Key: normal number
				%Entry: a structur with the following fields
						% - Name
						% - CalcType: maybe needed later at the moment not needed
						% - Calc_Element: Calculation Object
						% - Datastore: either an actual datastore, nothing (if not needed) 
						%   or the string 'Current' for the currently selected on
						% - Filterlist: same as Datastore
						% - Selected: saves the selected events from the filterlist
						% - CalcParam: The input parameter for the calculation
						% - ParamChanged: true if Parameters are changed without a calculation
						% - Results
						% - Calculation_ID
						% - PreSuccessor: Results are needed from this Calculation to Calculate (Key or Cell with Keys other )
						% - Calc_ID_preSuc: the calculation ID of the preSuccessor, used to check if recalculation is needed
						% - Successor: The mentioned key or keys uses result from this calculation
						% - Loaded: Should be set to true if the Calculation is loaded from a file.
						% - Additions: A free field which can be used or not for additional parameter used in the GUI or whereever
						% - GUIParameters: Can be used for Calculation dependend GUI Parameters like the color and so on.
			%The Calculation Element can be an object of the original MapSeis Calculation type but it is strongly recommened to use
			%the updated Version CalculationEX which allows to modify calculation parameter after the object is build. Some pre-written
			%functions will not work or not work without modification with the old Calculation object.
				
			import mapseis.calc.*;
			import mapseis.projector.*;
			
	
			switch CalcType
				case 'bval_rate'
					%The normal Frequency Magnitude function
					
					%get the current selected DataStore and filterlist
					DataStore=obj.Commander.getCurrentDatastore;
					FilterList=obj.Commander.getCurrentFilterlist;
					DataComNumber=obj.Commander.getCurrentDatastoreNumber;
					FilterComNumber=obj.Commander.getCurrentFilterlistNumber;
					
					%Default Values are needed.
					%Set filter to data
					FilterList.changeData(DataStore);
					FilterList.update;
					
					%get selected 
					Selected=FilterList.getSelected;
				
					%get EventTimes
					[evtimes temp] = getDecYear(DataStore,Selected);
					
					%min and max of the years
					minyear=min(evtimes);
					maxyear=max(evtimes);
					
					timestep=0.1;
					time1=minyear+(rand*0.4)*abs(maxyear-minyear);
					time2=maxyear-(rand*0.4)*abs(maxyear-minyear);
					
					CalcParam =	struct(	'timestep',timestep,...
								'time1',time1,...
								'time2',time2);
					
					%Calcfunction
					 rateCalcObject = mapseis.calc.CalculationEX(...
					 @mapseis.calc.Calc_bval_nofit,@mapseis.projector.getZMAPFormat,CalcParam,...
						'Name','Rate Compare');
						
					%Name has to be changed by something more meaningfull
					%Name field in the GUI may be an option
					CalcName=['Period_' num2str(obj.AvailableCalcTypes(1)+1)];
						
					
					%build Calculatio Element
					CalcEntry=	struct(	'Name',CalcName,...
								'CalcType','bval_rate',...
								'Calc_Element',rateCalcObject,...
								'Datastore',DataStore,...
								'DatastoreID',[],...
								'Filterlist',FilterList,...
								'FilterlistID',[],...
								'Selected',Selected,...
								'CalcParam',CalcParam,...
								'ParamChanged',true,...
								'Results',[],...
								'Calculation_ID',rand,...
								'PreSuccessor',[],...
								'Calc_ID_preSuc',[],...
								'Successor',[],...
								'Loaded',false,...
								'Additions',[DataComNumber FilterComNumber],...
								'GUIParameters',{{obj.ColorWheel{obj.PointColor}}});
								
					
								
					%Change Color for the next entry
					obj.PointColor=mod(obj.PointColor,6)+1			
								
					% Write Calculation into the hash
					CalcKey=obj.NextKey;
					obj.CalculationHash.set(CalcKey,CalcEntry);
					
					%update Key
					newKey=str2num(obj.NextKey);
					newKey=newKey+1;
					obj.NextKey=num2str(newKey);
					
					%add a calculation
					obj.AvailableCalcTypes(1)=obj.AvailableCalcTypes(1)+1;
					
					%update KeyList
					obj.BuildCalcKeyList;
			
					
					
				
				case 'mag_sign'
					%The Magnitude signature, needs Freg-Mag functions.
					
					%not Yet ready
					
					%Call the GUI for the selection of the depended calculations
					%obj.CalcParameterGUI.PreSuccessSelector(Types,Texts,[])
					
					%obj.AvailableCalcTypes(2)=obj.AvailableCalcTypes(2)+1;
					
			end
			
			
			
					    

		
		end
		
		
		
		function DeleteCalcEntry(obj,Key,howdy)
			% Allows to delete a Calculation object in the Hash
			% Successor Caclulation Elements will also be deleted
			% Thus: BE CAREFUL USING IT.
			
			% howdy selects the way the Element(s) are deleted:
			% 'only': only the called object will be deleted
			% 'single_depended': deletes all successor elements which have no other depended element
			% 'clean': deletes the whole depended set of calculation  (potentially dangerous)
			disp(isstr(Key))
			switch howdy
				case 'only'
					obj.CalculationHash.remove(Key);
				
				%Case single depended
				%--------------------------------------------------------------
				case 'single_depended'
					
					%get the entry
					try
						todelete = obj.CalculationHash.lookup(Key);
							
						%get the successors
						suckeys = todelete.Successor; 
						
						
						if iscell(suckeys)
							for i=1:numel(suckeys)
								if ~isempty(suckeys{i})
									obj.DeleteCalcEntry(suckeys{i},'single_depended');
								end
							end
							
						elseif ~isempty(suckeys)
							obj.DeleteCalcEntry(suckeys,'single_depended');
						end
		
						obj.CalculationHash.remove(Key);
					end
					
				case 'single_dep_recursive'
					%get the entry
					todelete =  obj.CalculationHash.lookup(Key);
			
					%get the successors
					suckeys = todelete.Successor; 
					
					%get presuccessor keys
					prekeys = todelete.PreSuccessor;
					
					if	iscell(suckeys)
						for i=1:numel(suckeys)
							obj.DeleteCalcEntry(suckeys{i},'single_dep_recursive');
						end
						
					elseif ~isempty(suckeys)
						obj.DeleteCalcEntry(suckeys,'single_dep_recursive');
					end
					
					if isempty(prekeys)
						obj.CalculationHash.remove(Key);
					end
				%--------------------------------------------------------------
	
				case 'clean'
				%WARNING: THIS OPTION IS UNTESTED AND MAY DELETE TO MANY ENTRIES
				%---------------------------------------------------------------
					%get the entry
					todelete =  obj.CalculationHash.lookup(Key);
			
					%get the successors
					suckeys = todelete.Successor; 
					
					%get presuccessor keys
					prekeys = todelete.PreSuccessor;
					
					if	iscell(suckeys)
						for i=1:numel(suckeys)
							obj.DeleteCalcEntry(suckeys{i},'clean');
						end
					else
						obj.DeleteCalcEntry(suckeys,'clean');
					end
					
					if	iscell(prekeys)
						for i=1:numel(prekeys)
							obj.DeleteCalcEntry(prekeys{i},'clean');
						end
					else
						obj.DeleteCalcEntry(prekeys,'clean');
					end
	
					
				obj.CalculationHash.remove(Key);
	
			end
			
			
			%Update the CalcKeyList
			obj.BuildCalcKeyList;
			
		end
		
		
		function CalcEntry(obj,Key,state)
			% This function calculates the result for a certain entry
			
			% get the main entry 
			  toCalc = obj.CalculationHash.lookup(Key);
			   
			 %disp('The CalcEntry')
			  
			% get key of the presuccessor Calculations
			  preKeys = toCalc.PreSuccessor;
			
			% Call yourself with the presuccessor Keys (recursion)
			  if iscell(preKeys)
					
					%more than one presuccessor	
					for i=1:numel(preKeys)
						obj.CalcEntry(preKeys{i},'depended');
																
					end	
					
					
						
			  elseif ~isempty(preKeys)
				 obj.CalcEntry(preKeys,'depended');
			  end
					
					
			% Get result from the presuccessor Calculations
			% Second loop to avoid possible problems with cross relations
			  if iscell(preKeys)
					preResults={};
					emptyness = true;
					
					%more than one presuccessor	
					for i=1:numel(preKeys)
						 temp = obj.CalculationHash.lookup(preKeys{i});	
						 preResults{i} = temp.Results;
						 emptyness = emptyness & isempty(preResults{i});
						 preIDs(i) = temp.Calculation_ID;			  	 				  	 	
					end	
					
					
						
			  elseif ~isempty(preKeys)
				  preResults = [];
				  temp = obj.CalculationHash.lookup(preKeys);
				  preResults = temp.Results;	
				  emptyness = isempty(preResults);
				  preIDs = temp.Calculation_ID;
			  
			  else %no preSuccessor
			  	emptyness = false; 
			  end
	
			       
					
			% Check if the preResults are alright (means existing)
			  if emptyness %if everything works this should actually never happen
				
			  	if strcmp(state,'initial')
					notify(obj,'CalcDone');
				end	
				disp('Something went wrong Results are missing')
				obj.ErrorDialog = 'Something went wrong, a result is missing';
				return
			  end
				
			  
			% Check if the Parameter entries are complete
			  canwestart = obj.CheckParameter(toCalc.CalcParam,toCalc.CalcType);
			  
			if ~canwestart	
				if strcmp(state,'initial')
					notify(obj,'CalcDone');
				end	
				obj.ErrorDialog = 'Not all necessary Calculation Parameters are set';
				return
	
			end
			
			% Check wether datastore and filter is needed or the current ones are wanted
			datastore = toCalc.Datastore;
			filterlist = toCalc.Filterlist;   
		  
			if isstr(datastore)
					datastore = obj.Commander.getCurrentDatastore;
			end
			
			if isstr(filterlist)
					filterlist = obj.Commander.getCurrentFilterlist; 	
			end
			
			%Check the data and filter IDs
			DataFilterSame = datastore.GiveID == toCalc.DatastoreID &...
						filterlist.GiveID == toCalc.FilterlistID;
	
			
			if isempty(DataFilterSame)
				DataFilterSame=false;
			end
	
	
			% Check if the Parameter, Datastore and FilterList entries defiate from the current parameter 
			% Also check the Calculation IDs of the presuccessor with the saved ones.
			% Only start a Calculation if any of this is different. 
				
			% Compare the IDs
			TheID = true;
			
			if iscell(toCalc.Calc_ID_preSuc)
				for i=1:numel(toCalc.Calc_ID_preSuc)
						iDe = preIDs(i) == toCalc.Calc_ID_preSuc(i); 
						TheID = TheID & iDe;
				end		
			elseif ~isempty(preKeys)
			
				TheID = preIDs == toCalc.Calc_ID_preSuc; 
			
			else %no Presuccessor, no IDs needed
				TheID = true;
				preIDs =[];
			end
	
			
			if ~TheID | toCalc.ParamChanged | ~DataFilterSame
				
				
				if ~isempty(filterlist) & ~isempty(datastore)
				
				    if ~DataFilterSame | isempty(toCalc.Selected)
				    	nothingDo=filterlist.AutoReinit(obj.ListProxy);
				    	filterlist.changeData(datastore);
				    	filterlist.update;
				    	logInd=filterlist.getSelected;
				    	toCalc.Selected=logInd;
				    	filterlist.Changed=false;
				    else
				    	nothingDo=true;
				    	logInd=toCalc.Selected;
				    end
				    
				    %kill listeners if they have been applied
				    if ~nothingDo
				    	filterlist.MuteList(obj.ListProxy);
				    end
				    
					
				    % Calculate the Elemente
				    result = toCalc.Calc_Element.calculate(datastore,logInd);

				elseif isempty(filterlist) & ~isempty(datastore)
					result = toCalc.Calc_Element.calculate(datastore);			

				else
					result = toCalc.Calc_Element.calculate;

				end
				
				%make new hash entry
				toCalc.Results = result;
				toCalc.ParamChanged = false;
				
				%write new IDs to data and filter
				toCalc.DatastoreID=datastore.GiveID;
				toCalc.FilterlistID=filterlist.GiveID;
				
				disp(result)
				%generate new CalcIDs
				toCalc.Calculation_ID=rand;
				toCalc.Calc_ID_preSuc=preIDs;
			
				% Write Result into the hash
				obj.CalculationHash.set(Key,toCalc)
				
					
			end
				
				
						  
			% notify if state is 'inital'
			 if strcmp(state,'initial')
				notify(obj,'CalcDone');
				
			 end	
			
			
		end
			
			
		function parcheck = CheckParameter(obj,CalcEntries,CalcType);
			%Checks wether the inputparameters are complete for a certain calculation
			
			switch CalcType
				case 'bval_rate'
					parcheck = true;
					parcheck = parcheck & ~isempty(CalcEntries.timestep);
					parcheck = parcheck & ~isempty(CalcEntries.time1);
					parcheck = parcheck & ~isempty(CalcEntries.time2);
		
					
				case 'mag_sign'
					parcheck = true;
					parcheck = parcheck & isempty(CalcEntries.timestep);
					parcheck = parcheck & isempty(CalcEntries.time1);
					parcheck = parcheck & isempty(CalcEntries.time2);
	
				case 'comp_stat'
				
				end
			
		
		end
		
		
		
		function Calculation(obj,val)
			% needed as bridge between the GUIs and the actual calculation.
			% The goal is to have a unified interface similar to every wrapper.
			% It gets either a specified value from the gui which can consist of different parameters 
			% or a value determining which calculation entry has to be calculated.  	
			
			% This version is from the Rate Compare Wrapper, it gets the values from the CalcParamGUI
			% calculates the result and plots the result in ResultViewer
			import mapseis.util.gui.allWaiting;
			disp('Working...')
			allWaiting('wait');
			if ~isstr(val)
				%get the key
				CalcKey=obj.CalcKeyList(val,1);
				
				obj.CalcEntry(CalcKey,'Inital');
						
				obj.CalcThings=true;
				
				disp('Calculation done')
				obj.notify('CalcDone');
			
			elseif strcmp(val,'all')
				Keys=obj.CalculationHash.getKeys;
				
				for i=1:numel(Keys)
				
					obj.CalcEntry(Keys{i},'Inital');
			
				end
				
				obj.CalcThings=true;
				disp('Calculation done')
				obj.notify('CalcDone');
				
			end
			
			allWaiting('move');
			
		end
		
		
		
		function ParameterChanger(obj,objHandler,CurrentCalcSlide)
			%If the Callback keyword 'Change' is used, this function shoul be defined.
			%It will be called everytime, a parameter is changed in the Parameter GUI
			%The handle of the changed uicontrol and the currentCalcSlide number will 
			%be send to this function.
			
			%get the calcElement and the key
			TheKey=obj.CalcKeyList{CurrentCalcSlide,1};
			TheCalc=obj.CalculationHash.lookup(TheKey);
			
			%get the Style of the uicontrol object
			daStyle=get(objHandler,'Style');
			
			%get the Tag (needed here as the input parameter)
			TheTag=get(objHandler,'Tag');
			
			%do the necessary transitions and write into the CalcObject
			%not needed in this case, I only use entryfields, but needed
			%in other cases and this should be a more general example.s
			switch daStyle
				case 'edit'
					newVal=get(objHandler,'String');
					TheCalc.CalcParam.(TheTag)=str2num(newVal);
					
					%Update Calc Element
					TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);
					
					
				case 'pushbutton'
					%The value have to be logical 
					TheCalc.CalcParam.(TheTag)=~TheCalc.CalcParam.(TheTag);
					
					%Update Calc Element
					TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);
					
				case 'checkbox'
					newVal=get(objHandler,'Value');
					TheCalc.CalcParam.(TheTag)=newVal;
					
					%Update Calc Element
					TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);
					
				case 'listbox'
					newVal=get(objHandler,'Value');
					theStrings=get(objHander,'String');
					if newVal~=0
						TheCalc.CalcParam.(TheTag)=theStrings{newVal};
					else
						TheCalc.CalcParam.(TheTag)=[];
					end
					
					%Update Calc Element
					TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);
					
				case 'popupmenu'
					newVal=get(objHandler,'Value');
					
					%This file could be a datastore or filterlist selector
					%In case of the datastore, the plots may have to be renewed in the resultviewer
					if strcmp(TheTag,'DataStore')
						%At the moment set to one if empty
						if newVal==0
							newVal=1
							set(objHandler,'Value',newVal);
						end
						
						%get datastore
						datastore = obj.Commander.resolveDataEntry(newVal);	
						
						%Apply
						TheCalc.Datastore=datastore;
						TheCalc.Selected=[];
						
						%save the position the selector into the CalcParamGUI
						obj.ParamGUIEntries{CurrentCalcSlide,3}=newVal;
						TheCalc.Additions(1)=newVal;
											
						
					elseif strcmp(TheTag,'FilterList')
						
						if newVal==0
							newVal=1;
							set(objHandler,'Value',newVal);
						end
						
					
						%get filterlist
						filterlist = obj.Commander.resolveFilterEntry(newVal);	
						
						%Apply
						TheCalc.Filterlist=filterlist;
						TheCalc.Additions(2)=newVal;
						TheCalc.Selected=[];
						
						%save the position the selector into the CalcParamGUI
						obj.ParamGUIEntries{CurrentCalcSlide,4}=newVal;
						
						
					else
					
						if newVal==0
							newVal=1;
							set(objHandler,'Value',newVal);
						end
						
						newVal=get(objHandler,'Value');
						theStrings=get(objHander,'String');
						TheCalc.CalcParam.(TheTag)=theStrings{newVal};
					
						%Update Calc Element
						TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);	
						
					
					
					end
					
			end	
			
			%put the Calculation back into the hash
			TheCalc.ParamChanged=true;
			obj.CalculationHash.set(TheKey,TheCalc);
			
		
		end
		
		
		
		function addGUICalc(obj,options)
			% This function works as interface between the GUIs and the wrapper
			if isstr(options)
				switch options
					case 'bval_rate'
					
						%add Calculation
						CalcKey = obj.addCalculation('bval_rate');
						
						%build the CalcSlide for the Parameter GUI
						obj.ModifyCalcConfig('add',CalcKey);
						
						%Update the ResultGUI Config
						obj.ResultConfigBuilder;
						
						%write pointconfig and GUI Config to ResultViewer 
						obj.ResultGUI.PointConfig=obj.createPointStructur;
						obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;	
						
						%update ParameterWindow
						obj.notify('CalcHashUpdated');
						
						%Update ResultViewer
						obj.notify('UpdateResultGUI');
						
						
					
					case 'mag_sign'
						%The Magnitude signature, needs Freg-Mag functions.
						
						%not Yet ready
						
						%Call the GUI for the selection of the depended calculations
						%obj.CalcParameterGUI.PreSuccessSelector(Types,Texts,[])
						
						%obj.AvailableCalcTypes(2)=obj.AvailableCalcTypes(2)+1;
						
				end
			else
			
			
			
			
			
			end
		end
	
	
		function deleteCalculation(obj,val)
			% This function  is an interface between the GUIs and the wrapper and allow to delete 
			% Calculation entries. 
			% The received value 'val' is the position of the PopUpList 'CalcSelector', Using the key instead
			% would be saver, but also more timecomsuming. As the GUIs have no internal keylist it should acually 
			% be safe enough, but there is potential that something can go wrong
			
			%get element
			CalcKey=obj.CalcKeyList{val,1};
			CalcEntry=obj.CalculationHash.lookup(CalcKey);
			
			%the CalcType is needed
			CalcType=CalcEntry.CalcType;
			
			disp(CalcKey)
			%remove from the hash
			obj.DeleteCalcEntry(CalcKey,'single_depended');
			
			
			%subtract a calculation
			switch CalcType
				case 'bval_rate'
					obj.AvailableCalcTypes(1)=obj.AvailableCalcTypes(1)-1;
				case 'mag_sign'	
					obj.AvailableCalcTypes(2)=obj.AvailableCalcTypes(2)-1;
			end
			
			%update KeyList
			obj.BuildCalcKeyList
			
			%build the CalcSlide for the Parameter GUI
			obj.ModifyCalcConfig('delete',CalcKey);
			
			%Update the ResultGUI Config
			obj.ResultConfigBuilder;
			
			%write pointconfig and GUI Config to ResultViewer 
			obj.ResultGUI.PointConfig=obj.createPointStructur;
			obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;	
			
			%update ParameterWindow
			obj.notify('CalcHashUpdated');
			
			%Update ResultViewer
			obj.notify('UpdateResultGUI');
			
			
		
		end
		
		
		
		function ModifyCalcConfig(obj,DoWhat,CalcKey)
			%This function writes or deletes a configuration for the CalcParamGUI and saves it in
			%the object variable ParamGUIEntries
			
			
			try
				%get the calculation element
				CalcEntry=obj.CalculationHash.lookup(CalcKey);
				CalcType=CalcEntry.CalcType;
			catch
				%Entry probably already deleted, CalcType does not matter anymore.
				CalcType='bval_rate';
			end
			
			switch CalcType
				case 'bval_rate'
					switch DoWhat
						case 'add'
							
							
							%build point and entry field for time 1
							[TempSlide point1]=CreateSelectionPoints(obj,CalcKey,'Time 1','time1','x','time1','TopPlot',true);
							CalcSlide=TempSlide;
							
							%build point and entry field for time 2
							[TempSlide point2]=CreateSelectionPoints(obj,CalcKey,'Time 2','time2','x','time2','TopPlot',true);
							CalcSlide=[CalcSlide,TempSlide];
							
							%build the field for the timestep
							Textfield = struct(	'ModulType', 'Texter',...
										'Tag','timestep_text',...
										'String','Timestep');
										
							TimeStepField = struct(	'ModulType','Entryfield',...
										'Tag','timestep',...
										'String',num2str(CalcEntry.CalcParam.timestep),...
										'Callback','Change');	
							CalcSlide=[CalcSlide,Textfield,TimeStepField];
							
							%build datastore and filterlist selector
							DataSelector = struct(	'ModulType', 'DataSelector',...
										'Callback','Change',...
										'Value',CalcEntry.Additions(1));
										
							FilterSelector=	struct(	'ModulType', 'FilterSelector',...
										'Callback','Change',...
										'Value',CalcEntry.Additions(2));
							CalcSlide=[CalcSlide,DataSelector,FilterSelector];
							
							%create Calculation Selector and modifier
							CalcSelector = struct(	'ModulType', 'CalcSelector');
							
							CalcOption(:,1)={'bval_rate';'mag_sign'};
							CalcOption(:,2)={'bval_rate';'mag_sign'};
							CalcModifier = struct(	'ModulType', 'CalcModify',...
										'PopUpMenu','true',...
										'Option',{CalcOption});
										
							CalcSlide=[CalcSlide,CalcSelector,CalcModifier];
							
							%create CalcButton
							CalcButton = struct(	'ModulType', 'CalcButton',...
										'how','variable');
										
							
							AllCalcButton = struct(	'ModulType', 'CalcButton',...
										'Name', 'Calculate All',...
										'how','fixed',...
										'value','all');
										
							CalcSlide=[CalcSlide,CalcButton,AllCalcButton];
							
							%add the CalcSlide to the end of the ParamGUIEntries together with its  CalcKey
							obj.ParamGUIEntries{end+1,1}=CalcKey;
							obj.ParamGUIEntries{end,2}=CalcSlide;
							obj.ParamGUIEntries{end,3}=CalcEntry.Additions(1);
							obj.ParamGUIEntries{end,4}=CalcEntry.Additions(2);
							
							%Now deal with the points (note end always takes the last entry)
							obj.PointConfig{end+1,3}=point1.Tag;
							obj.PointConfig{end,2}=point1;
							obj.PointConfig{end,1}=CalcKey;
							obj.PointConfig{end+1,3}=point2.Tag;
							obj.PointConfig{end,2}=point2;
							obj.PointConfig{end,1}=CalcKey;
							
							%The CalcKeys are added to make it easier finding the points in case they have to be deleted
							
							%Change Color for the next entry
							%NOT NEEDED ANYMORE
							%obj.PointColor=mod(obj.PointColor+1,6)+1;
							
							
						case 'delete'
							%find entry in the ParamGUIEntries
							foundEntry=strcmp(obj.ParamGUIEntries(:,1),CalcKey);
							
							%remove entry
							obj.ParamGUIEntries=obj.ParamGUIEntries(~foundEntry,:);
							
							%find the two points
							foundPoints=strcmp(obj.PointConfig(:,1),CalcKey);	
							
							%remove points
							obj.PointConfig=obj.PointConfig(~foundPoints,:);
							
							
						case 'update'
							%find entry in the ParamGUIEntries
							foundEntry=strcmp(obj.ParamGUIEntries(:,1),CalcKey);
							CalcSlide=obj.ParamGUIEntries{foundEntry,2};
							
							%get Position of the filterselector and dataselector
							try
								dataPos=obj.ParamGUIEntries{foundEntry,3};
							catch
								dataPos=1;
							end	
							
							try
								filterPos=obj.ParamGUIEntries{foundEntry,4};
							catch
								filterPos=1;
							end	
							
							%write back
							obj.ParamGUIEntries{foundEntry,3}=dataPos;
							obj.ParamGUIEntries{foundEntry,4}=filterPos;
							
							%The positions of the fields are know, so counting should be enough
							CalcSlide{2}.String=point2Entry(obj,CalcEntry.CalcParam.time1);
							CalcSlide{4}.String=point2Entry(obj,CalcEntry.CalcParam.time2);
							CalcSlide{6}.String=num2str(CalcEntry.CalcParam.timestep);
							
							%add Position to the DataStore and FilterList selector
							CalcSlide{7}.Value=dataPos;
							CalcSlide{8}.Value=filterPos;
							
							obj.ParamGUIEntries(foundEntry,2)={CalcSlide};
							
							%Now update the points
							%find the two points
							foundPoint1=strcmp(obj.PointConfig(:,3),['time1',CalcKey]);	
							foundPoint2=strcmp(obj.PointConfig(:,3),['time2',CalcKey]);
							
							obj.PointConfig{foundPoint1,2}.Coord(1)=CalcEntry.CalcParam.time1;
							obj.PointConfig{foundPoint2,2}.Coord(1)=CalcEntry.CalcParam.time2;
					end	
				
				case 'mag_sign'
					%has to be done, at the moment the CalcMagSign function is not ready as the original function
					%is a bloody mess.
					
					switch CalcType
						case 'add'
						case 'delete'
						case 'update'
					end	
				
				
			end	
		end
		
		
		function CalcConfig=ParamConfigSender(obj,CalcSlideNr)
			%This function writes out the Configuration for the ParameterGUI
			%normally called from the outside
			%Depending on how the GUI is build it is best to preproduce the CalcSlide
			%before. Import is not to forget to update the CalcSlide values in the internal 
			%memory. The object variable ParamGUIEntries can be used to save the entries
			
			%(In this version the configurations are predefined but updated each time the
			%function ParamConfigSender is called, this is needed because of the time selection 
			%points which have to been show all the time in the resultGUI).
				
			%get Key
			CalcKey=obj.CalcKeyList{CalcSlideNr,1};
			
			%update the slide
			obj.ModifyCalcConfig('update',CalcKey);
			
			%get the slide
			CalcConfig=obj.ParamGUIEntries{CalcSlideNr,2};
			
			
		end
		
		
		function TheColor=getCalcColor(obj,CalcSlideNr)
			%Similat to ParamConfigSender, but sends a Color to the ParamGUI (or ResultGUI) 
			%If the function is missing the windows will just be grey. 
			%It has to be done that way because the ParamGUI uses a cell array as config and not a structur.
			
			%get Key
			CalcKey=obj.CalcKeyList{CalcSlideNr,1};
			
			%Lookup
			CalcEntry=obj.CalculationHash.lookup(CalcKey);
			
			TheColor=CalcEntry.GUIParameters{1};
		
		end
		
		
		
		function ResultConfigBuilder(obj)
			%This function writes the configuration of the ResultViewerGUI
			%It is called everytime, the Calculations are modified. 
			%It is possible to improve performance by avoiding steps 
			%already done in a the case of a parameter update (not done yet)
			
			%WindowMenu
			CurrentCalcs=obj.CalcKeyList(:,2);
			NumofCalc=numel(CurrentCalcs);
			
			%Listbox Element
			TopListBoxer=	struct(	'ModulType','ListBox',...
						'Callback','MultiParam',...
						'Name','Calculations',...
						'Tag','TopCalcSelector',...
						'EntryStrings',{CurrentCalcs},...
						'maxSelection',NumofCalc,...
						'DoThings',false,...
						'WindowTag','TopPlot',...
						'ValueType','number',...
						'Param',[],...
						'Valu',1);
						
		
			BottomListBoxer=struct(	'ModulType','ListBox',...
						'Callback','MultiParam',...
						'Name','Calculations',...
						'Tag','BottomCalcSelector',...
						'EntryStrings',{CurrentCalcs},...
						'maxSelection',NumofCalc,...
						'DoThings',false,...
						'WindowTag','BottomPlot',...
						'ValueType','number',...
						'Param',[],...
						'Valu','obj.AdditionalGUIParam.BottomPlotCalc',...
						'InitValu',0,...
						'EvalMode',true);
										
						
			
			%For starting do not use a TopModuls it is the cum. number anyway			
			TopModuls = {TopListBoxer};
			
			BottomModuls ={BottomListBoxer};
			
			
			
			
			%MouseMenu
			TopdrawButton= 	struct(	'Callback', 'draw',...
						'Label', 'Redraw Plot',...
						'WindowTag', 'TopPlot');
			
			BottomdrawButton=struct('Callback', 'draw',...
						'Label', 'Redraw Plots',...
						'WindowTag', 'BottomPlot');
			CumRateYear=	struct(	'Callback', 'AddParam',...
						'Label', 'Cum/year - Magnitude',...
						'WindowTag', 'BottomPlot',...
						'Tag','CumeRateYear',...
						'Param','BottomPlotCalc',...
						'ValueType','number',...
						'Valu',1,...
						'ChangeMode','set',...
						'Checker',false,...
						'DoThings',true);
						
			RateYear=	struct(	'Callback', 'AddParam',...
						'Label', 'rate/year - Magnitude',...
						'WindowTag', 'BottomPlot',...
						'Tag','RateYear',...
						'Param','BottomPlotCalc',...
						'ValueType','number',...
						'Valu',2,...
						'ChangeMode','set',...
						'Checker',false,...
						'DoThings',true);			
			
			CalcTypeMagSign=struct(	'Callback', 'AddParam',...
						'Label', 'Magnitude Signature',...
						'WindowTag', 'BottomPlot',...
						'Tag','MagSign',...
						'Param','BottomPlotCalc',...
						'ValueType','number',...
						'Valu',3,...
						'ChangeMode','set',...
						'Checker',false,...
						'DoThings',true);					
			
			TopMouseMenu = {TopdrawButton};
			
			BottomMouseMenu = {BottomdrawButton};
			
			if obj.AvailableCalcTypes(1)>0;
				BottomMouseMenu = [BottomMouseMenu,CumRateYear,RateYear];
			end	
			
			if obj.AvailableCalcTypes(2)>0;
				BottomMouseMenu = [BottomMouseMenu,CalcTypeMagSign];
			end	
			
	
			
			
			
			%Menubar
			
			%Overall Draw Button
			DrawButton= 	struct(	'Callback', 'draw',...
						'Label', 'Redraw all plots',...
						'WindowTag', 'all');
			LegendButton1=	struct(	'Callback', 'Legend',...
						'Label', 'Legend On/Off Bottom Plot',...
						'WindowTag', 'BottomPlot');
			
			LegendButton2=	struct(	'Callback', 'Legend',...
						'Label', 'Legend On/Off Top Plot',...
						'WindowTag', 'TopPlot');			
						
			PlotBar = {LegendButton1,LegendButton2,DrawButton};
			
			MenuBar	=	struct(	'Label','Plot Options',...
						'Entries',{PlotBar});
			
			%AutoUpdater
			AutoUpdater=	struct(	'Event',{{'UpdateResultGUI', 'CalcDone'}},...
						'UpdateType',{{'all','plots'}},...
						'WindowTag',{{'TopPlot','all'}});
			
						
			%Build the general parameter Init (InitValues)
			DataNFilter=obj.FindUniqueData;

			try
				old_DataNFilter=obj.DataNFilter;
			catch
				old_DataNFilter=[];
			end			
			
			%compare old with new
			try 
				ListSame=~all(strcmp(old_DataNFilter(:,3),DataNFilter(:,3)));
			catch
				ListSame=false;
			end
			
			if ~ListSame
				obj.DataNFilter=DataNFilter;
			end
			
			TheInits = num2cell(ones(numel(obj.DataNFilter(:,3)),1));
			disp(TheInits)
			for i=1:numel(obj.DataNFilter(:,3))
				ValueStrings(i)= {['obj.PlotParameter.TopPlot.',obj.DataNFilter{i,3}]};
			end
			
			InitValue = 	struct(	'ValueString',{ValueStrings},...
						'InitValue',{TheInits});
			
			
			%Build the Config
			TopPlotConfig = 	 struct('ToolbarPosition','none',...
							'ToolbarModuls',{TopModuls},...
							'RightButtonMenu',{TopMouseMenu});	
			
			BottomPlotConfig = 	struct('ToolbarPosition','Bottom',...
							'ToolbarModuls',{BottomModuls},...
							'RightButtonMenu',{BottomMouseMenu});	
			
			rawStruct = 		struct(	'Layout','TwoWindowHor',...
							'ToolbarStyle','Single',...
							'ToolbarPosition','Bottom',...
							'WindowMenu',{{'LoadSaveCalc','LoadSaveAll','LoadSaveDataFilter',MenuBar}},...
							'AutoUpdater',AutoUpdater,...
							'InitValues',InitValue,...
							'SubPlotNames',{{'TopPlot','BottomPlot'}},...
							'TopPlot',TopPlotConfig,...
							'BottomPlot',BottomPlotConfig,...
							'EndProcess','call');
								
								
			%write to the obj configvariable
			obj.ResultGUIConfig = [];
			obj.ResultGUIConfig = rawStruct;
			
			
			
			
								
		end
		
		
		
		function drawPlots(obj,RegParameter)
			%This function gets the parameter from the specified ResultGUI and uses the defined plotfunction
			%to plot the functions into the window 
			%It is not needed to use this function, instead the PlotWriter can be used, in this case the 
			%ResultViewerGUI will do the whole plotting. This function is only needed if an entirely different
			%plotting function is wished. 
			
			%In this case here the function is NOT USED	
			 
			obj.ResultGUI.Drawer('all')
		
		end
	
		
		function ResultStructur = PlotWriter(obj,PlotParameter)
		
			import mapseis.projector.*;
		
			%This function writes the result for the ResultGUI and returns them as a structur
			
			%In this case we do not care about the PlotParameter, as everything calculated will be send back 
			%to the ResultViewerGUI
				
			%first get the old ResultStructur
			old_ResultStructur=obj.ResultStructur;
			
			%Check if anything exists already
			TopExist=isfield(obj.ResultStructur,'TopPlot');
			BottomExist=isfield(obj.ResultStructur,'BottomPlot')
			
			%first the data of cum num plot
			%This means every filterlist and datastore combination has to be plotted. 
			DataNFilter=obj.FindUniqueData;
			if TopExist
				old_DataNFilter=obj.DataNFilter;
			else
				old_DataNFilter=[];
			end
			
			%compare old with new
			try 
				ListSame=~all(strcmp(old_DataNFilter(:,3),DataNFilter(:,3)));
				
				if numel(old_DataNFilter(:,3))~=numel(DataNFilter(:,3))
					ListSame=false;
				end	
			catch
				ListSame=false;
			end
			
			ListSame=false;
				
				
			%only do the loop if needed (new catalog&filterlists added)
			if  ListSame | ~TopExist
				LayerNr=0;
				LayerNames={};
				
				%get through all the combinations and get data
				for i=1:numel(DataNFilter(:,1))
					DataStore=obj.Commander.resolveDataEntry(DataNFilter{i,1});
					FilterList=obj.Commander.resolveFilterEntry(DataNFilter{i,2});
					disp(['doing ',DataNFilter{:,3}]);
					
					%Set filter to data
					nothingDo=FilterList.AutoReinit(obj.ListProxy);
					FilterList.changeData(DataStore);
					FilterList.update;
					
					%get selected 
					Selected=FilterList.getSelected;
									    
					%kill listeners if they have been applied
					if ~nothingDo
				 	   	FilterList.MuteList(obj.ListProxy);
				 	end
					
					
					%get EventTimes
					[evtimes temp] = getDecYear(DataStore,Selected);
					
					%LayerStructur
					Layer = struct(	'PlotType','timeplot',...
							'Data', evtimes,...
							'X_Axis_Label',[],...
							'Y_Axis_Label',[],...
							'Line_or_Point','line',...
							'LineStylePreset','Fatline',...
							'Colors','automatic',...
							'X_Axis_Limit','auto',...
							'Y_Axis_Limit','auto',...
							'LegendText',DataNFilter{i,3});
					
					LayerNr=LayerNr+1;
					disp(DataNFilter{i,3})
					LayerNames(LayerNr)=DataNFilter(i,3);
					
					ResultStructur.TopPlot.(DataNFilter{i,3})={Layer};
					
					%set the PlotParameter in the ResultGUI to 1
					obj.ResultGUI.PlotParameter.TopPlot.(DataNFilter{i,3})=1;
					%(might be replaced if the TopPlots become selectable)
					
				end
				
				%Complete structur
				ResultStructur.TopPlot.Nr_Layers=LayerNr;
				ResultStructur.TopPlot.Layer_Names=LayerNames;
				ResultStructur.TopPlot.Title='Cumulative Number';
				ResultStructur.TopPlot.LegendToggle=false;
				
				obj.DataNFilter=DataNFilter;
				
				
			else
				DataNFilter=old_DataNFilter
				ResultStructur.TopPlot=old_ResultStructur.TopPlot;
			end	
			
			
			%Now the calculations of the bottom plot, only add something to resultstruct if 
			%anything is calculate means CalcThings=true
			%if obj.CalcThings
				Keys=obj.CalcKeyList(:,1);
				Names=obj.CalcKeyList(:,2);
				CalcTypes=obj.CalcKeyList(:,3);
				%disp(Names)
				SelectEvent=[];
				for i=1:numel(Keys)
					%Get CalcElement
					CalcElement=obj.CalculationHash.lookup(Keys{i})
					TheColor=CalcElement.GUIParameters{1};
					CalcName=CalcElement.Name;
					
					%only do something if Result is not empty
					if ~isempty(CalcElement.Results)
						switch CalcTypes{i}
							case 'bval_rate'
							
								%two entries needed for each calculation
								%further layers may be added later
			
								PlotData=[];
								
								
								%first the cum. rate/year
								PlotData(:,1)=CalcElement.Results.bval_bins_reverse(:);
								PlotData(:,2)=CalcElement.Results.bvalue_sum_reverse(:);
								cumrate= struct('PlotType','log2D',...
										'Data',PlotData,...
										'LogMode','Ylog',...
										'Line_or_Point','both',...
										'X_Axis_Label','Magnitude',...
										'Y_Axis_Label','Cum. rate / year ',...
										'LineStylePreset','Fatline',...
										'MarkerStylePreset','diamond',...
										'Colors',TheColor,...
										'X_Axis_Limit','auto',...
										'Y_Axis_Limit','auto',...
										'LegendText',Names{i});
								PlotData=[];
								
								%second the rate/year
								PlotData(:,1)=CalcElement.Results.bval_bins(:);
								PlotData(:,2)=CalcElement.Results.bvalue(:);
								rateplot= struct('PlotType','generic2D',...
										'Data',PlotData,...
										'Line_or_Point','both',...
										'X_Axis_Label','Magnitude',...
										'Y_Axis_Label','Rate / year ',...
										'LineStylePreset','Fatline',...
										'MarkerStylePreset','diamond',...
										'Colors',TheColor,...
										'X_Axis_Limit','auto',...
										'Y_Axis_Limit','auto',...
										'LegendText',Names{i});
										
								ResultStructur.BottomPlot.(Names{i})={cumrate, rateplot};
								
								%set the AddParam to 1 one if not existing
								try
									if obj.ResultGUI.AdditionalGUIParam.BottomPlotCalc==0
										obj.ResultGUI.AdditionalGUIParam.BottomPlotCalc=1;
									end
								catch
									obj.ResultGUI.AdditionalGUIParam.BottomPlotCalc=1;
								end
								
								%Set the according parameter in the gui to on
								SelectEvent(end+1)=i;
								
								%set the plot parameter
								obj.ResultGUI.PlotParameter.BottomPlot.(CalcName)='obj.AdditionalGUIParam.BottomPlotCalc';
								
							case 'mag_sign'
								%not finished now
								
								
								
							
						end	
						
						
					else
						%write a text which says nothing is calculated	
						TextElement= struct(	'PlotType','textfield',...
									'Position',[0.5 0.5],...
									'CoordType','normalized',...
									'String',' ' ,...
									'TextType','normal',...
									'TextStyle','normal');
						ResultStructur.BottomPlot.(Names{i})={TextElement, TextElement};			
						%'Calculation not done, no Results'
					end
					
				
				%end
				
				
				ResultStructur.BottomPlot.Nr_Layers=numel(Keys);
				ResultStructur.BottomPlot.Layer_Names=Names;
				ResultStructur.BottomPlot.Title='Result Plot';
				ResultStructur.BottomPlot.LegendToogle=true;
			
			end
			
			%Update the selection of the period in the ResultGUI
			Moduls=get(obj.ResultGUI.TheGUI,'children');
			PeriodSelect=findobj(Moduls,'Tag','BottomCalcSelector');
			set(PeriodSelect,'Value',SelectEvent);
			
			
			
		end
		
		
		function DataNFilter=FindUniqueData(obj);
			%The function checks all CalcElement and save the unique DataStore FilterList combinations
			%in a array with first column datastore number in the commander and second column Filterlist number
			
			%get through the ParamGUIEntries and check the position of every datastore filterlist selector.
			for i=1:numel(obj.ParamGUIEntries(:,1))
				%get Position of the filterselector and dataselector
				disp(obj.ParamGUIEntries)
				try
				dataPos=obj.ParamGUIEntries{i,3};
				catch
				dataPos=1;
				disp('notfound');
				end	
				
				try
				filterPos=obj.ParamGUIEntries{i,4};
				catch
				filterPos=1;
				disp('notfound');
				end	
				
				UniqueString=['df',num2str(dataPos),'_',num2str(filterPos)];
				
				RawList(i,:)={dataPos,filterPos,UniqueString};
				
			end
			
			%Get unique combinations
			[b m n]=unique(RawList(:,3));
			
			DataNFilter=RawList(m,:);
			
		end
		
		function BuildCalcKeyList(obj)
			%This function builds the CalcKeyList and should be called every time the calculation
			%Hash is modified. 
			%The CalcKeyList is a Cellarray with 3 column: 
			%CalcKeyList{:,1}: The Keys of the calculation
			%CalcKeyList{:,2}: The Name (used for the Selector) of the calculation
			%CalcKeyList{:,3}: The CalcType of the calculation
			
			%EmptyList
			obj.CalcKeyList={};
			
			%get all keys
			keys=obj.CalculationHash.getKeys;
			
			for i=1:numel(keys)
				CalcEntry=obj.CalculationHash.lookup(keys{i})
				
				%Name
				NameEntry=CalcEntry.Name;
				
				%Type
				CalcType=CalcEntry.CalcType;
				
				%generate entry name if not existing
				if isempty(NameEntry)
					NameEntry=[CalcType,' Calculation_', num2str(keys{i})];
				end
			
				%write into CalcKeyList	
				obj.CalcKeyList(i,:)={keys{i},NameEntry,CalcType};
				
				%has to be done in case an element has no name.
				NameEntry=[];
			end
			
			
		
		end
		
		
		
		function [entryfields points]=CreateSelectionPoints(obj,CalcKey,EntryText,Parameter,WhichCoord,TagTemp,WindowTag,Autofire)
			%This function creates the entries for the dragable points set in the ResultGUI
			%and also the entryfields for the CalcParamGUI. 
			%Has to be called each time a calculation which needs the points is added to the hash
			
			%The Tags will be generated automatically dependen on the TagTemp, the CalcKey and the 
			%WhichCoord variable: [TagTemp, Calckey, 'x'] (the WhichCoord entry x or y) will only be used
			%if set to both.
			
			%WhichCoord sets which coordinate will be used, if set to both, two fields will be produces
			
			%EntryText: Will be used for the text above the entryfield, if left empty now text above the
			%	    Entryfield will be used.
			
			entryfields={};
			
			

			
			%get entry from the hash
			TheCalc=obj.CalculationHash.lookup(CalcKey);
			
			%Color
			TheColor=TheCalc.GUIParameters{1};
			
			%produce Tags and modify the entries if needed.
			switch WhichCoord
				case 'x'
					Tags{1}=[TagTemp,CalcKey];
					
					if ~isempty(EntryText)
						Text{1}=EntryText;
					else
						Text{1}=[];
					end
					
					DefaultVal=TheCalc.CalcParam.(Parameter);
					PointDefault(1)=DefaultVal;
					PointDefault(2)=nan;
					
					Para{1}=Parameter;
					
					Direct{1}='x';
					
				case 'y'
					Tags{1}=[TagTemp,CalcKey];
					
					if ~isempty(EntryText)
						Text{1}=EntryText;
					else
						Text{1}=[];
					end
					
					DefaultVal=TheCalc.CalcParam.(Parameter);
					PointDefault(1)=nan;
					PointDefault(2)=DefaultVal;
					
					Para{1}=Parameter;
					
					Direct{1}='y';
					
				case 'both'
					Tags{1}=[TagTemp,CalcKey,'x'];
					
					if ~isempty(EntryText{1})
						Text{1}=EntryText{1};
					else
						Text{1}=[];
					end
					DefaultVal(1)=TheCalc.CalcParam.(Parameter{1});
					Para{1}=Parameter{1};
					
					
					Tags{2}=[TagTemp,CalcKey,'y'];
					
					if ~isempty(EntryText{2})
						Text{2}=EntryText{2};
					else
						Text{2}=[];
					end
					DefaultVal(2)=TheCalc.CalcParam.(Parameter{2});
					Para{2}=Parameter{2};
					
					Direct{1}='x';
					Direct{2}='y';
					
					PointDefault=DefaultVal;
					
			end
			
			%The point needs only one tag
			PointTag=[TagTemp,CalcKey];
			
			
			%produce entryfields
			
			for i=1:numel(Tags)
				if ~isempty(Text{i})
					modul = struct(	'ModulType','Texter',...
							'Tag',[Tags{i},'_text'],...
							'String',Text{i});
					
					entryfields{end+1}=modul;
				end
				
				modul =struct(	'ModulType','Positionfield',...
						'Tag',Tags{i},...
						'String',point2Entry(obj,DefaultVal(i)),...
						'WhichCoord',Direct{i},...
						'PointTag',PointTag,...
						'WindowTag',WindowTag,...
						'CalculationKey',CalcKey,...
						'Parameter',Para{i});
				entryfields{end+1}=modul;		
			end
			
			
			
			%produce pointstructure
			PointModul= struct( 	'Tag', PointTag,...
						'EntryFieldTag',{Tags},...
						'Colors',TheColor,...
						'WindowTag',WindowTag,...
						'Coord',PointDefault,...
						'CalculationKey',CalcKey,...
						'WhichCoord',WhichCoord,...
						'Parameter',{Para},...
						'AutoFire',Autofire);
						
			points=PointModul;
		
		end
		
		
		function PointConfStruct=createPointStructur(obj)
			%The function takes the PointConfig and builds a structur for the ResultGUI
			
			PointConfig=obj.PointConfig
			
			for i=1:numel(PointConfig(:,3))
				PointConfStruct.(PointConfig{i,3})=PointConfig{i,2};
			end	
			
		
		end
		
		
		function ChangeParameterPoint(obj,FromGUI,TheHandle,WindowTag,WhichCoord,Tags,CalcKey,Parameter)
			%This function will be called from the ResultGUI and the Parameter GUI
			%It is a link between a certain ImPoint and a Entryfield in either the ResultGUI
			%or in the ParameterGUI.
			%When called by either object it will change the other object to the selected parameter
			%and also change the calculation parameter of the wrapper calc element.
			%Tag,CalcSlide,Parameter and TheHandle have to be a two dimensional cell vector (1)=x 
			%and (2)=y when the parameter WhichCoord is set to both (x and y) 
			
			%In this case the Tags of the Entryfield have to be unique in the whole Wrapper.
			%It is not enough that the Tags are unique in one Calculation.
			
			
			%Transform to cell array where it is needed
			if ~iscell(TheHandle)
				TheHandle={TheHandle};
				
			end
			
			if ~iscell(Parameter)
				Parameter={Parameter};
			end	
			
			%Only if the Tag represent a EntryField two tags are needed.
			if ~iscell(Tags)
				Tags={Tags};
			end
			
			
			switch FromGUI
				case 'ResultGUI'
					%TheHandle, should be the object and not the handle
					%get position
					newPos=getPosition(TheHandle{:});
					
					
					%get the entryfields
					ModulList=obj.CalcParameterGUI.ModulList;
					
					TagList={};
					HandleList={};
					
					count=1;
					for i=1:numel(ModulList)
						%future proof, in case in future version more than one 
						%entryfield per modul is allowed
						if iscell(ModulList{i})
							subModuls=ModulList{i};
							for j=1:numel(subModuls)
								TagList{count}=get(subModuls{j},'Tag');
								HandleList{count}=subModuls{j};
								count=count+1;
							end
						else
						TagList{count}=get(ModulList{i},'Tag');
						HandleList{count}=ModulList{i};
						count=count+1;
						end
					end
					
					%try to find the tags
					for i=1:numel(Tags)
						foundTags(:,i)=strcmp(TagList,Tags{i});
					end
					
					%now decide which coordinate is used
					switch WhichCoord
						case 'both'
							%check if x entryfield is existing
							if any(foundTags(:,1))
								set(HandleList{foundTags(:,1)},...
								'String',point2Entry(obj,newPos(1)));
							end
							
							%check if y entryfield is existing
							if any(foundTags(:,2))
								set(HandleList{foundTags(:,2)},...
								'String',point2Entry(obj,newPos(2)));
							end
							
						case 'x'
							%check if x entryfield is existing
							if any(foundTags(:,1))
								set(HandleList{foundTags(:,1)},...
								'String',point2Entry(obj,newPos(1)));
							end
							
						case 'y'
							%check if y entryfield is existing
							if any(foundTags(:,1))
								set(HandleList{foundTags(:,1)},...
								'String',point2Entry(obj,newPos(2)));
							end
						
					end
					
				case 'ParamGUI'
					%TheHandle should be the handle of the entry field
	
					
					%Normally only one handle is possible in a Entryfield Callback 
					%but in case a button or similar is used, two Callbacks
					%might be used
					for i=1:numel(TheHandle)
						%get the value
						strval=get(TheHandle{i},'String');
						newPos(i)=Entry2Point(obj,strval);
					end
					
					%find the impoint from the ResultGUI
					pointList=obj.ResultGUI.point_objects.(WindowTag);
					
					TagList={};
					for i=1:numel(pointList)
						TagList{i}=get(pointList{i},'Tag');
					end
					if ~isempty(TagList)
						foundTag=strcmp(TagList,Tags{1});
						
						PointObject=pointList{foundTag};
						
						%get old position (needed when not both coordinates are used)
						oldPos=PointObject.getPosition;
						
						%now decide which coordinate is used
						switch WhichCoord
							case 'both'
								PointObject.setPosition(newPos(1),newPos(2));
							case 'x'
								PointObject.setPosition([newPos(1),oldPos(2)]);
							case 'y'
								PointObject.setPosition([oldPos(1),newPos(1)]);
						end		
						
						%COMMENT: 	The points normally have a position callback, if the 
						%		position is set with the command setPosition, the callback 
						%		will also be called, means this function (if set to autofire)
						%		will be called again and the result will be written into to the
						%		entryfield, but those will not trigger the Callback again, so it
						%		should stop there. It might be an idea for the perfomance to 
						%		prevent this behavior, a way could be to check if the values 
						%		change and only do something if so, or the callback function can
						%		remove before changing the position of the point and set again
						%		afterwards.
					end	
			
			end
			
			
			%In both cases the new value(s) have to be written into the Hash

			%get the calcElement
			TheCalc=obj.CalculationHash.lookup(CalcKey);
			
			switch WhichCoord
				case 'both'
					TheCalc.CalcParam.(Parameter{1})=newPos(1);
					TheCalc.CalcParam.(Parameter{2})=newPos(2);
					
				case 'x'
					TheCalc.CalcParam.(Parameter{1})=newPos(1);
					
				case 'y'
					TheCalc.CalcParam.(Parameter{1})=newPos(2);
			end
			%disp(TheCalc.CalcParam)
			
			
			%Update Calc Element
			TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);	
			
			%put the Calculation back into the hash
			TheCalc.ParamChanged=true;
			obj.CalculationHash.set(CalcKey,TheCalc);
			
			
			%refresh the point config:
			switch FromGUI
				case 'ResultGUI'
					PointName=get(TheHandle{:},'Tag');					
				
				case 'ParamGUI'
					PointName=get(PointObject,'Tag');
					
				
			end	
			
		
			
			PoiEntry=strcmp(obj.PointConfig(:,3),PointName);
			
			
			
			if any(PoiEntry)
		
				obj.PointConfig{PoiEntry,2}.Coord=newPos;
				%disp(obj.PointConfig{PoiEntry})
			end	
			
			
		end
		
		function outString=point2Entry(obj,numValue)
			%It seems strange to use two function on after an other to
			%convert a simple value in the pointchanger, the idea behind this
			%is to bann every function which might not be modifed by the user to 
			%the superclass, this means also the function of the pointchanger
			
			%basicfunction
			%outString=num2str(numValue);	
			
			%datevec to timestring
			outString=mapseis.LineCalcWrapper.RateCompareWrapper.decyear2string(numValue);
			
			
			
		end
		
		function outNumber=Entry2Point(obj,StringValue)
			%It seems strange to use two function on after an other to
			%convert a simple value in the pointchanger, the idea behind this
			%is to bann every function which might not be modifed by the user to 
			%the superclass, this means also the function of the pointchanger
			
			%basicfunction
			%outNumber=str2num(StringValue);	
			
			%datevec to timestring
			outNumber=mapseis.LineCalcWrapper.RateCompareWrapper.string2decyear(StringValue);
			
			
			
		end	
	
		
		function SaveCalculations(obj,filename,All_Single,CalcKey)
			%This function allows to save parts of the calculation Hash or
			%the whole hash. Needed are the filename (has to be given by the GUI)
			%the variable All_Single, with 'all' for saving the whole hash or 'single'
			%for saving only a single Entries, in this cas the CalcKey is needed
			
			if nargin<4
				CalcKey=[];
			end
			
			if ~isempty(CalcKey) & ~iscell(CalcKey)
				CalcKey={CalcKey};
			end
			
			switch All_Single
				case 'single'
					for i=1:numel(CalcKey)
						entry = obj.CalculationHash,lookup(CalcKey{i});
						CalcHash(i,1)=CalcKey(i);
						CalcHash{i,2}=entry;
					end	
					
				case 'all'
					CalcHash=obj.CalculationHash.ExportHash;
					
				
			end
			
			try 
				save(filename,'CalcHash');
			catch
				errordlg('File could not be saved');
			end	
			
			
		
		end
		
		
		function LoadCalculation(obj,filename,add_replace)
			%This function loads a Calculation entry or a whole hash (depended on the 
			%saved data) filename gives the location of the Calculation and add_replace
			%determines how the data will be loaded, 'add' will give entries new keys and 
			%add them to the existing hash, 'replace' will delete all entry existing in the 
			%hash and use the new Calculation Elements instead and 'overwrite' will overwrite 
			%all Calculation entries which have the same key as an entry in the loaded data
			
			try 
				loadedHash=load(filename);
			catch
				errordlg('File could not be loaded');
				add_replace='error';
			end
			
			try
				newCalcHash=loadedHash.CalcHash;
			catch
				errordlg('The file did not contain any Calculations');
				add_replace='error';
			end
			
			
			switch add_replace
				case 'add'
					newCalcHash=obj.HashKeyCorrector(newCalcHash,1,'inital');
					%set to loaded true and add them
					for i=1:numel(newCalcHash(:,1))
						newCalcHash{i,2}.Loaded=true;
						obj.CalculationHash.set(newCalcHash{i,1},newCalcHash{i,2});
					end
					
				
				case 'replace'
					%set to loaded true
					for i=1:numel(newCalcHash(:,1))
						newCalcHash{i,2}.Loaded=true;
					end
					
					%replace
					obj.CalculationHash.replaceHash(newCalcHash);
					
					%set the the lastkey right
					lastKey=max(cellfun(@str2num,newCalcHash(:,1)));
					obj.NextKey=num2str(lastKey+1);
					
					
				case 'overwrite'
				
					%overwrite and add
					for i=1:numel(newCalcHash(:,1))
						newCalcHash{i,2}.Loaded=true;
						obj.CalculationHash.set(newCalcHash{i,1},newCalcHash{i,2});
					end
					
					%set the the lastkey right
					TheKeys=obj.CalculationHash.getKeys;
					lastKey=max(cellfun(@str2num,TheKeys));
					obj.NextKey=num2str(lastKey+1);
					
				case 'error'
					%do nothing, there was an error
					%might add a operation later if needed
			end	
		
		
		end
		
		
		function correctHash = HashKeyCorrector(obj,TheHash,EntryNumber, state)
			%The function is needed for the correction of the key incases where the loaded
			%Calculations are just added to the existing Hash, it is a recursive function
			%To the hash there will be added a third column with a logical value determine if the
			%The entry has already been changed, if the function is called with the state 'inital'
			if nargin< 4
				EntryNumber=1;
			end
			
			switch state
				case 'inital'
					Hashlength=numel(TheHash(:,1));
					TheHash(:,3)=num2cell(logical(zeros(Hashlength,1)));
					countID=1:Hashlength;
					
					%go througth it by recursion
					TheHash=obj.HashCorrector(TheHash,EntryNumber,'recursion');
					
					%Remove the Logicals
					TheHash=TheHash(:,1:2);
					
					
				case 'recursion'
				
					if ~TheHash{EntryNumber,3}
							if ~isempty(TheHash{EntryNumber,2}.Successor)
								for i=1:numel(TheHash{EntryNumber,2}.PreSuccessor)
									found=strcmp(TheHash(:,1),TheHash{EntryNumber,2}.PreSuccessor{i});
									if any(found) & ~TheHash{found,3}
										TheHash=HashCorrector(obj,TheHash,countID(found),'recursion');
										TheHash{EntryNumber,2}.Successor(i)=TheHash(found,1);
									end	
									
								end	
							end
							
							%save old key incase of successors
							oldKey=TheHash{EntryNumber,1};
							
							%set new Key
							TheKey=obj.NextKey;
							newKey=str2num(TheKey);
							newKey=newKey+1;
							obj.NextKey=num2str(newKey);
							%write to hash
							TheHash{EntryNumber,1}=TheKey;
							
							
							if ~isempty(TheHash{EntryNumber,2}.Successor)
								for i=1:numel(TheHash{EntryNumber,2}.Successor)
									found=strcmp(TheHash(:,1),TheHash{EntryNumber,2}.Successor{i});
									if any(found)
										oldPosi=strcmp(TheHash{i,2}.PreSuccessor,oldKey);
										TheHash{i,2}.PreSuccessor{oldPosi}=TheKey;
									end	
									
								end	
							
							end
							
							if EntryNumber<=numel(TheHash(:,1))
								TheHash=obj.HashCorrector(TheHash,EntryNumber+1,'recursion');
							end	
							
					else
						
						if EntryNumber<=numel(TheHash(:,1))
								TheHash=obj.HashCorrector(TheHash,EntryNumber+1,'recursion');
						end	
							
					end
					
				
			end
			
			
				
			
		end
		
		
				
		function SaveSession(obj,filename)
			%Saves all the calculations and all the datastores and filterlists
			
			%get Hashes from Commander
			Hashes=obj.Commander.SendSession('BothHash');
			DataHash=Hashes{1};
			FilterHash=Hashes{2};
			
			%get Hash from CalculationHash
			CalcHash=obj.CalculationHash.ExportHash;
			
			
			%save it
			try 
				save(filename,'DataHash','FilterHash','CalcHash');
			catch
				errordlg('File could not be saved');
			end	
			
		end
		


		function LoadSession(obj,filename)
			%Loads all the calculations and all the datastores and filterlists

			ErrorHandle=false;
			
			%Check if all data is available, at the moment every part is needed, later
			%it might be possible to only load certain parts
			
			try 
				loadedHash=load(filename);
			catch
				errordlg('File could not be loaded');
				ErrorHandle=true;
			end
			
			try
				CalcHash=loadedHash.CalcHash;
			catch
				errordlg('The file did not contain any Calculations');
				ErrorHandle=true;
			end
			
			try
				DataHash=loadedHash.DataHash;
			catch
				errordlg('The file did not contain any DataStoreHash');
				ErrorHandle=true;
			end
			
			try
				FilterHash=loadedHash.FilterHash;
			catch
				errordlg('The file did not contain any FilterListHash');
				ErrorHandle=true;
			end
			
			
			if ~ErrorHandle
				%CalculationHash
				%set to loaded true
				for i=1:numel(CalcHash(:,1))
					CalcHash{i,2}.Loaded=true;
				end
					
				%replace
				obj.CalculationHash.replaceHash(CalcHash);
					
				%set the the lastkey right
				lastKey=max(cellfun(@str2num,CalcHash(:,1)));
				obj.NextKey=num2str(lastKey+1);
			
				%Commander update
				CommandHash{1}=DataHash;
				CommandHash{2}=FilterHash;
				
				obj.Commander.ReplaceSession(CommandHash,'BothHash');
			
			end
			
			
			
			
		end
		
		
		
		
		function loadCalcGUI(obj,HowTo)
		
			[fName,pName] = uigetfile('*.mat','Enter MapSeis Result *.mat file name...');
				
			if isequal(fName,0) || isequal(pName,0)
				errordlg('No filename selected. ');
			else
				filename=fullfile(pName,fName);
				
				obj.LoadCalculation(filename,HowTo);
				
				obj.RebuildCalcSlides;
				obj.BuildCalcKeyList;
				obj.ResultConfigBuilder;
				obj.ResultGUI.PointMaker('stopall');
				obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;
				obj.ResultGUI.PointConfig=obj.createPointStructur;
				obj.ResultGUI.rebuild;
				obj.ResultGUI.PointMaker('start');
				obj.drawPlots([]);
				obj.notify('CalcHashUpdated');
				
			end	     
			
		
		end
		
		
		
		
		function saveCalcGUI(obj,HowTo)
			 [fName,pName] = uiputfile('*.mat','Enter save file name...');
			 if isequal(fName,0) || isequal(pName,0)
			 	errordlg('No filename selected.  Data not saved');
			 else
			 	filename=fullfile(pName,fName);
			 	
			 	switch HowTo
			 		case 'single'
			 			CalcEntry=obj.CalcParameterGUI.CurrentCalcSlide;
			 			CalcKey=obj.CalcKeyList{CalcEntry,1};
			 			obj.SaveCalculation(filename,'single',CalcKey);
			 			
			 		case 'all'
			 			obj.SaveCalculation(filename,'single',[]);
			 	end
			 	
			 	
			 end
		end
		
		
		
		
		function loadSessionGUI(obj,HowTo)
			%HowTo here not needed but, will be delivered for future use by the GUI
			
			[fName,pName] = uigetfile('*.mat','Enter MapSeis Session *.mat file name...');
		 	
			if isequal(fName,0) || isequal(pName,0)
				errordlg('No filename selected. ');
			else
				filename=fullfile(pName,fName);
				obj.LoadSession(filename);
				
			end
			
			obj.RebuildCalcSlides;
			obj.BuildCalcKeyList;
			obj.ResultConfigBuilder;
			obj.ResultGUI.PointMaker('stopall');
			obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;
			obj.ResultGUI.PointConfig=obj.createPointStructur;
			obj.ResultGUI.rebuild;
			obj.ResultGUI.PointMaker('start');
			obj.drawPlots([]);
			obj.notify('CalcHashUpdated');
		end	
		
		
		
		
		function saveSessionGUI(obj,HowTo)
			%HowTo here not needed but, will be delivered for future use by the GUI
			
			 [fName,pName] = uiputfile('*.mat','Enter save file name...');
			 if isequal(fName,0) || isequal(pName,0)
			 	errordlg('No filename selected.  Data not saved');
			 else
			 	filename=fullfile(pName,fName);
			 	obj.SaveSession(filename);
			 	
			 end
		
		end
		
		function RebuildCalcSlides(obj)
			TheKeys=obj.CalculationHash.getKeys;
			obj.PointColor=1;
			obj.ColorWheel={'blue','red','green','black','cyan','magenta'};
			obj.NextKey='1';
			obj.PointConfig={};
			obj.ParamGUIEntries={};
			obj.AvailableCalcTypes(1)=0;
			obj.AvailableCalcTypes(2)=0;
			obj.CalcThings=false;
			obj.CumNumData={};
			
			for i=1:numel(TheKeys)
				CalcEntry=obj.CalculationHash.lookup(TheKeys{i});
				switch CalcEntry.CalcType
					case 'bval_rate'
						obj.AvailableCalcTypes(1)=obj.AvailableCalcTypes(1)+1;
					case 'mag_sign'
						obj.AvailableCalcTypes(2)=obj.AvailableCalcTypes(2)+1;
				end	
				
				if ~isempty(CalcEntry.Results)
					obj.CalcThings=true;
				end
				
				obj.ModifyCalcConfig('add',TheKeys{i});
			end
		
		end	
		
		
		
end	


methods (Static)
		

		function TheDateString=decyear2string(decYR)
			import mapseis.util.decyear2matS;	
		
			[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
			%TheDateString=DateConvert([fYr, nMn, nDay, nHr, nMin, nSec],'string');
			TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
		end
		
		
		function decimalYear=string2decyear(instring)
			import mapseis.util.decyear;	
			%rawDate=DateStr2Num(instring,31);
			%[fYr, nMn, nDay, nHr, nMin, nSec]=DateConvert(rawDate,'vector');
			[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
			decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
		end
end

end
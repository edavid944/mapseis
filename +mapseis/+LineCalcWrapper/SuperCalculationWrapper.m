classdef SuperCalculationWrapper < handle
		
	%This is the superclass of the wrapper (prototyp was the RateCompareWrapper)
	%All wrapper can use this class as superclass
	
	%AT THE MOMENT IT IS ONLY THE UNFINISHED RateCompareWrapper
	%With functions which should not be specified here excluded
		
		
	properties
		CalculationHash
		ResultGUI
		CalcParameterGUI
		PointConfig
		ListProxy
		Commander
		ErrorDialog
		ParamGUIEntries %not optional
		ResultGUIConfig %not optional
		DataNFilter
		CalcKeyList
		CalcThings
		ParallelMode
		NextKey
		NeededResultGUI
		NeededParamGUI
	end

	events
		CalcDone
		CalcHashUpdate
		UpdateResultGUI
	end


	methods
	
	function obj = SuperCalculationWrapper(ListProxy,Commander,GUIswitch)
		Import mapseis.datastore.*;
		%Constuctor, generates the Calculation objects and opens a suitable gui if 
		% GUIswitch = true else now GUI is produced, maybe usefull if only calculations are
		% wanted
		
		
		
		obj.CalculationHash=assocArray();
		
		obj.PointConfig={};
		obj.ParamGUIEntries={};
		
		obj.ListProxy=ListProxy;
		obj.Commander = Commander; 
		
		%get the current datastore and filterlist
		currentData = obj.Commander.getCurrentDatastore;
		currentFilter = obj.Commander.getCurrentFilterlist; 	
		
		%by default: produce to Calculation elements of Calc_bval_nofit
		
		
	end

	
	function RegisterGUI(obj,toRegister,GUIType,RegParameter)
		%simply writes the GUI to the internal variables
		%May add support for more than one GUI of one Type
		
		switch GUIType
			case 'ParamGUI'
				obj.CalcParameterGUI=toRegister;
			case 'ResultGUI'	
				obj.ResultGUI=toRegister;	
		end	
		
	
	end
	
	
	function RefreshGUI(obj,GUIType,RegParameter);
		%Similar to RegisterGUI, but does just rewrite the GUI Parameter
		switch GUIType
			case 'ParamGUI'
				obj.CalcParameterGUI.CalcParams=obj.ParamGUIEntries;
				
			case 'ResultGUI'	
				obj.ResultGUI.ResultConfig=obj.ResultGUIConfig;	
		end	
		
	end	
		
	
	
	function ResultEndProcess(obj)
		%This function can be called after the creation of the windows 
		%allowing to used some default drawings. Had to be added because of 
		%multithreading and synchronity.
		%Can be used by setting the field EndProcess in the GUIConfig of the ResultGUI
		%to the string 'call'
		%Optional: does not need to be defined if not used
		
	end				
					
	
	
	function CalcKey = addCalculation(obj,CalcType)
		%adds a new  Calculation and a new Magnitude Signature Calculation to the hash 
		% A hash entry consists of:
			%Key: normal number
			%Entry: a structur with the following fields
					% - Name
					% - CalcType: maybe needed later at the moment not needed
					% - Calc_Element: Calculation Object
					% - Datastore: either an actual datastore, nothing (if not needed) 
					%   or the string 'Current' for the currently selected on
					% - DatastoreID: The RandomID of the DataStore, it will be set within a calculation 
					%   and compare later again with the datastore to determine if there was a change		
					% - Filterlist: same as Datastore
					% - FilterlistID: same as DatastoreID
					% - CommanderKeys: 	Stores the Keys of the Commander used in the last calculation struct(ShownDataID, ShownFilterID)
					%			only used in case of 'Current' Datastore and Filterlist.
					% - Selected: saves the selected events from the filterlist
					% - CalcParam: The input parameter for the calculation
					% - ParamChanged: true if Parameters are changed without a calculation
					% - Gridded: if set to true the calculation will be done over a grid
					% - Gridfunction: determines which grid will be used, can be norm2DGrid,DepthProfileGrid,TimeGrid,MagGrid,
					%   DepthGrid or Custom1D. The Gridparameters have to be according to it.
					% - GridParameter: Sets the parameter for the grid, has to be a structure.
					% - Results
					% - Calculation_ID
					% - PreSuccessor: Results are needed from this Calculation to Calculate (Key or Cell with Keys other )
					% - Calc_ID_preSuc: the calculation ID of the preSuccessor, used to check if recalculation is needed
					% - Successor: The mentioned key or keys uses result from this calculation
					% - Loaded: Should be set to true if the Calculation is loaded from a file.
					% - Additions: A free field which can be used or not for additional parameter used in the GUI or whereever
					% - GUIParameters: Can be used for Calculation dependend GUI Parameters like the color and so on.
		%The Calculation Element can be an object of the original MapSeis Calculation type but it is strongly recommened to use
		%the updated Version CalculationEX which allows to modify calculation parameter after the object is build. Some pre-written
		%functions will not work or not work without modification with the old Calculation object.
		
		%GridParameter structur: GridSeparation: 	only for 2D grids, determines the spacing of the grid, can
		%					 	be either have one or two values
		%			 SelectionRadius:	Again only for 2D grids, determines the selection radius around the grid point
		%			 SelectionNumber:	Again only for 2D grids, selectes how many events are selected around a grid cell,
		%						currently not working
		%			 Reshaping:		Only 2D grids, if set to true the result will be returned as 2d matrix instead as a vector.
		%			 BinningType:		Only 1D, Can either be 'fixrange', where all events are selected without the specified bin or 'fixnumber'
		%						where the binsize is replace by a binnumber and a overlay and a fix number of events are used per bin.
		%			 Intervall:		Only for 1D grids, the Range of the Grid (first and last value)
		%			 Binsize:		Only for 1D grids, the size of the bins.
		%			 Binnumber:		sets the number of events used per bin, only in the 'fixnumber' mode
		%			 Overlap:		sets the overlay between the bins, incase of the of the fixrange it is a procentage of of binsize overlay and in
		%						case of the 'fixnumber' the overlay is calculated as BinNumber/Overlay
		%			 MinNumber:		Set the minimum Number of events to be used in the grid, if less data is available in a grid cell
		%						nothing will be calculated and the grid cell will be set to nan (at the moment only in 1D grids)
		%			 AxisVariable:		Only for Custom1D, determines on which parameter the grid is applied on.
		%			 UserData:		Only for Custom1D, set true if the parameter is saved in the user data of the datastore.
		%			 ParallelCalc:		For all, if set to true the calculation will be done with parallel processing
		%It is best to not include any GUI stuff in this function. This function is not directly called
		%from to the GUI and is in this sense optional and another function can be defined to do the job if needed
		%but it is wise to use this function skeleton if an add and delete to the hash is needed.
		
		import mapseis.calc.*;
		import mapseis.projector.*;
		


		
		
		
		
				    

	
	end
	
	
	
	function DeleteCalcEntry(obj,Key,howdy)
		% Allows to delete a Calculation object in the Hash
		% Successor Caclulation Elements will also be deleted
		% Thus: BE CAREFUL USING IT.
		
		% howdy selects the way the Element(s) are deleted:
		% 'only': only the called object will be deleted
		% 'single_depended': deletes all successor elements which have no other depended element
		% 'clean': deletes the whole depended set of calculation  (potentially dangerous)
		disp(isstr(Key))
		switch howdy
			case 'only'
				obj.CalculationHash.remove(Key);
			
			%Case single depended
			%--------------------------------------------------------------
			case 'single_depended'
				
				%get the entry
				try
					todelete = obj.CalculationHash.lookup(Key);
						
					%get the successors
					suckeys = todelete.Successor; 
					
					
					if iscell(suckeys)
						for i=1:numel(suckeys)
							if ~isempty(suckeys{i})
								obj.DeleteCalcEntry(suckeys{i},'single_depended');
							end
						end
						
					elseif ~isempty(suckeys)
						obj.DeleteCalcEntry(suckeys,'single_depended');
					end
	
					obj.CalculationHash.remove(Key);
				end
				
			case 'single_dep_recursive'
				%get the entry
				todelete =  obj.CalculationHash.lookup(Key);
		
				%get the successors
				suckeys = todelete.Successor; 
				
				%get presuccessor keys
				prekeys = todelete.PreSuccessor;
				
				if	iscell(suckeys)
					for i=1:numel(suckeys)
						obj.DeleteCalcEntry(suckeys{i},'single_dep_recursive');
					end
					
				elseif ~isempty(suckeys)
					obj.DeleteCalcEntry(suckeys,'single_dep_recursive');
				end
				
				if isempty(prekeys)
					obj.CalculationHash.remove(Key);
				end
			%--------------------------------------------------------------

			case 'clean'
			%WARNING: THIS OPTION IS UNTESTED AND MAY DELETE TO MANY ENTRIES
			%---------------------------------------------------------------
				%get the entry
				todelete =  obj.CalculationHash.lookup(Key);
		
				%get the successors
				suckeys = todelete.Successor; 
				
				%get presuccessor keys
				prekeys = todelete.PreSuccessor;
				
				if	iscell(suckeys)
					for i=1:numel(suckeys)
						obj.DeleteCalcEntry(suckeys{i},'clean');
					end
				else
					obj.DeleteCalcEntry(suckeys,'clean');
				end
				
				if	iscell(prekeys)
					for i=1:numel(prekeys)
						obj.DeleteCalcEntry(prekeys{i},'clean');
					end
				else
					obj.DeleteCalcEntry(prekeys,'clean');
				end

				
			obj.CalculationHash.remove(Key);

		end
		
		
		%Update the CalcKeyList
		obj.BuildCalcKeyList;
		
	end
	
	
	
	
	function CalcEntry(obj,Key,state)
		% This function calculates the result for a certain entry
		
		% get the main entry 
		  toCalc = obj.CalculationHash.lookup(Key);
		   
		 disp('The CalcEntry')
		
		% Check for empty or not existing Gridded variable
		if ~isfield(toCalc,'Gridded');
			toCalc.Gridded=false;
		end
		
		if isempty(toCalc.Gridded)
			toCalc.Gridded=false;
		end	
		 
		% get key of the presuccessor Calculations
		  preKeys = toCalc.PreSuccessor;
		
		% Call yourself with the presuccessor Keys (recursion)
		  if iscell(preKeys)
				
				%more than one presuccessor	
				for i=1:numel(preKeys)
					obj.CalcEntry(preKeys{i},'depended');
															
				end	
				
				
					
		  elseif ~isempty(preKeys)
			 obj.CalcEntry(preKeys,'depended');
		  end
				
				
		% Get result from the presuccessor Calculations
		% Second loop to avoid possible problems with cross relations
		  if iscell(preKeys)
				preResults={};
				emptyness = true;
				
				%more than one presuccessor	
				for i=1:numel(preKeys)
					 temp = obj.CalculationHash.lookup(preKeys{i});	
					 preResults{i} = temp.Results;
					 emptyness = emptyness & isempty(preResults{i});
					 preIDs(i) = temp.Calculation_ID;			  	 				  	 	
				end	
				
				
					
		  elseif ~isempty(preKeys)
			  preResults = [];
			  temp = obj.CalculationHash.lookup(preKeys);
			  preResults = temp.Results;	
			  emptyness = isempty(preResults);
			  preIDs = temp.Calculation_ID;
		  
		  else %no preSuccessor
			emptyness = false; 
		  end

		       
				
		% Check if the preResults are alright (means existing)
		  if emptyness %if everything works this should actually never happen
			
			if strcmp(state,'initial')
				notify(obj,'CalcDone');
			end	
			disp('Something went wrong Results are missing')
			obj.ErrorDialog = 'Something went wrong, a result is missing';
			return
		  end
			
		  
		% Check if the Parameter entries are complete
		  canwestart = obj.CheckParameter(toCalc.CalcParam,toCalc.CalcType);
		  
		if ~canwestart	
			if strcmp(state,'initial')
				notify(obj,'CalcDone');
			end	
			obj.ErrorDialog = 'Not all necessary Calculation Parameters are set';
			return

		end
		
		% Check wether datastore and filter is needed or the current ones are wanted
		datastore = toCalc.Datastore;
		filterlist = toCalc.Filterlist;   
		
		%set default equals
		filterequal=true;
		dataequal=true;
		
		
		%in case of current 'Current' in either datastore or Commander get the keys
		if isstr(datastore) | isstr(filterlist)
			newKeys=obj.Commander.getMarkedKey;
		end
		
		if isstr(datastore)
				datastore = obj.Commander.getCurrentDatastore;
				
				%check if changed
				try
					dataequal=strcmp(toCalc.CommanderKeys.ShownDataID,...
								newKeys.ShownDataID);
				end	
		end
		
		if isstr(filterlist)
				filterlist = obj.Commander.getCurrentFilterlist; 
				%check if changed
				try
					filterequal=strcmp(toCalc.CommanderKeys.ShownFilterID,...
								newKeys.ShownFilterID);
				end	
		end
		
		%in case of current 'Current' in either datastore or Commander get the keys
 			
		%Check the data and filter IDs
		DataFilterSame = datastore.GiveID == toCalc.DatastoreID &...
					filterlist.GiveID == toCalc.FilterlistID & ...
					dataequal & filterequal;
		
		if isempty(DataFilterSame)
			DataFilterSame=false;
		end
		
		

		% Check if the Parameter, Datastore and FilterList entries defiate from the current parameter 
		% Also check the Calculation IDs of the presuccessor with the saved ones.
		% Only start a Calculation if any of this is different. 
			
		% Compare the IDs
		TheID = true;
		
		if iscell(toCalc.Calc_ID_preSuc)
			for i=1:numel(toCalc.Calc_ID_preSuc)
					iDe = preIDs(i) == toCalc.Calc_ID_preSuc(i); 
					TheID = TheID & iDe;
			end		
		elseif ~isempty(preKeys)
		
			TheID = preIDs == toCalc.Calc_ID_preSuc; 
		
		else %no Presuccessor, no IDs needed
			TheID = true;
			preIDs =[];
		end
			
		if ~TheID | toCalc.ParamChanged | ~DataFilterSame
			
			
			if ~isempty(filterlist) & ~isempty(datastore)
			    
			    if ~DataFilterSame | isempty(toCalc.Selected)
				nothingDo=filterlist.AutoReinit(obj.ListProxy);
				filterlist.changeData(datastore);
				filterlist.update;
				logInd=filterlist.getSelected;
				toCalc.Selected=logInd;
				filterlist.Changed=false;
			    else
				nothingDo=true;
				logInd=toCalc.Selected;
			    end
			    
			    %kill listeners if they have been applied
			    if ~nothingDo
				filterlist.MuteList(obj.ListProxy);
			    end
			    
				
			    % Calculate the Elemente
			    if ~toCalc.Gridded
			    	result = toCalc.Calc_Element.calculate(datastore,logInd);
			    else
			    	result = CalcOnGrid(CalcEntry,datastore,filterlist); 
			    end
			
			elseif isempty(filterlist) & ~isempty(datastore)
				result = toCalc.Calc_Element.calculate(datastore);
				
			else
				result = toCalc.Calc_Element.calculate;

			end
			
			
			
			%make new hash entry
			toCalc.Results = result;
			toCalc.ParamChanged = false;
			
			%write new IDs to data and filter
			toCalc.DatastoreID=datastore.GiveID;
			toCalc.FilterlistID=filterlist.GiveID;
			
			
			%in case of current 'Current' write CommanderKeys
			if isstr(datastore) | isstr(filterlist)
				toCalc.CommanderKeys=newKeys;
			end
		
			
			%disp(result)
			%generate new CalcIDs
			toCalc.Calculation_ID=rand;
			toCalc.Calc_ID_preSuc=preIDs;
		
			% Write Result into the hash
			obj.CalculationHash.set(Key,toCalc)
			
				
		end
			
			
					  
		% notify if state is 'inital'
		 if strcmp(state,'initial')
			notify(obj,'CalcDone');
			
		 end	
		
		
	end
	

	
		
	function parcheck = CheckParameter(obj,CalcEntries,CalcType);
		%Checks wether the inputparameters are complete for a certain calculation
		

		
	
	end
	
	
	function Calculation(obj,val)
		% needed as bridge between the GUIs and the actual calculation.
		% The goal is to have a unified interface similar to every wrapper.
		% It gets either a specified value from the gui which can consist of different parameters 
		% or a value determining which calculation entry has to be calculated.  	
		
		disp('Working...')	
			
					
		
	
	end
	
	
	
	function ParameterChanger(obj,objHandler,CurrentCalcSlide)
		%If the Callback keyword 'Change' is used, this function shoul be defined.
		%It will be called everytime, a parameter is changed in the Parameter GUI
		%The handle of the changed uicontrol and the currentCalcSlide number will 
		%be send to this function.
		
		%get the calcElement and the key
		TheKey=obj.CalcKeyList{CurrentCalcSlide,1};
		TheCalc=obj.CalculationHash.lookup(TheKey);
		
		%get the Style of the uicontrol object
		daStyle=get(objHandler,'Style');
		
		%get the Tag (needed here as the input parameter)
		TheTag=get(objHandler,'Tag');
		%Set parameterspace
		ParamSpace='CalcParam';
		
		%use addtional separation: the string 'grid*-' as the first 6 values
		%of the TheTag will send the parameters to the grid instead of the calcparam
		if numel(TheTag)>6
			if strcmp(TheTag(1:6),'grid*-')
				ParamSpace='GridParam');
			end
		end
		
		%do the necessary transitions and write into the CalcObject
		%not needed in this case, I only use entryfields, but needed
		%in other cases and this should be a more general example.s
		switch daStyle
			case 'edit'
				newVal=get(objHandler,'String');
				TheCalc.(ParamSpace).(TheTag)=str2num(newVal);
				
				%Update Calc Element
				TheCalc.Calc_Element.ChangeValues(TheCalc.(ParamSpace));
				
				
			case 'pushbutton'
				%The value have to be logical 
				TheCalc.(ParamSpace).(TheTag)=~TheCalc.(ParamSpace).(TheTag);
				
				%Update Calc Element
				TheCalc.Calc_Element.ChangeValues(TheCalc.(ParamSpace));
				
			case 'checkbox'
				newVal=get(objHandler,'Value');
				TheCalc.(ParamSpace).(TheTag)=newVal;
				
				%Update Calc Element
				TheCalc.Calc_Element.ChangeValues(TheCalc.(ParamSpace));
				
			case 'listbox'
				newVal=get(objHandler,'Value');
				theStrings=get(objHander,'String');
				if newVal~=0
					TheCalc.(ParamSpace).(TheTag)=theStrings{newVal};
				else
					TheCalc.(ParamSpace).(TheTag)=[];
				end
				
				%Update Calc Element
				TheCalc.Calc_Element.ChangeValues(TheCalc.(ParamSpace));
				
			case 'popupmenu'
				newVal=get(objHandler,'Value');
				
				%This file could be a datastore or filterlist selector
				%In case of the datastore, the plots may have to be renewed in the resultviewer
				if strcmp(TheTag,'DataStore')
					%At the moment set to one if empty
					if newVal==0
						newVal=1
						set(objHandler,'Value',newVal);
					end
					
					%get datastore
					datastore = obj.Commander.resolveDataEntry(newVal);	
					
					%Apply
					TheCalc.Datastore=datastore;
					TheCalc.Selected=[];
					
					%save the position the selector into the CalcParamGUI
					obj.ParamGUIEntries{CurrentCalcSlide,3}=newVal;
					TheCalc.Additions(1)=newVal;
										
					
				elseif strcmp(TheTag,'FilterList')
					
					if newVal==0
						newVal=1;
						set(objHandler,'Value',newVal);
					end
					
				
					%get filterlist
					filterlist = obj.Commander.resolveFilterEntry(newVal);	
					
					%Apply
					TheCalc.Filterlist=filterlist;
					TheCalc.Additions(2)=newVal;
					TheCalc.Selected=[];
					
					%save the position the selector into the CalcParamGUI
					obj.ParamGUIEntries{CurrentCalcSlide,4}=newVal;
					
					
				else
				
					if newVal==0
						newVal=1;
						set(objHandler,'Value',newVal);
					end
					
					newVal=get(objHandler,'Value');
					theStrings=get(objHander,'String');
					TheCalc.(ParamSpace).(TheTag)=theStrings{newVal};
				
					%Update Calc Element
					TheCalc.Calc_Element.ChangeValues(TheCalc.(ParamSpace));	
					
				
				
				end
				
		end	
		
		%put the Calculation back into the hash
		TheCalc.ParamChanged=true
		obj.CalculationHash.set(TheKey,TheCalc);
		
	
	end
	
	

	
	function addGUICalc(obj,options)
		% This function works as interface between the GUIs and the wrapper
		% and is not optional if an "add" function is used in GUI

	end


	function deleteCalculation(obj,val)
		% This function  is an interface between the GUIs and the wrapper and allow to delete 
		% Calculation entries. 
		% The received value 'val' is the position of the PopUpList 'CalcSelector', Using the key instead
		% would be saver, but also more timecomsuming. As the GUIs have no internal keylist it should acually 
		% be safe enough, but there is potential that something can go wrong
		

		%update ParameterWindow
		obj.notify('CalcHashUpdated');
		
		%Update ResultViewer
		obj.notify('UpdateResultGUI');
		
			
		
	end
	
	
	function ModifyCalcConfig(obj,DoWhat,CalcKey)
		%This function writes or deletes a configuration for the CalcParamGUI and saves it in
		%the object variable ParamGUIEntries
		%The function is optional in a way that now GUI depend on it, but in case of more than one ParameterGUI
		%it might be an idea to use this function template 
		%DoWhat should be a string allowing to select three different "functions": 'add','delete' and 'update'
		%(can be done with a switch case statement 
	end		

	
	function CalcRes = CalcOnGrid(CalcEntry,DataStore,FilterList)
	    	%Calculates the Calculation object on the selected grid
	    	import mapseis.calc.*;
	    	
			
		%get all the selected
		TheSelected = CalcEntry.Selected;
		
		%GridParameter
		GridData=CalcEntry.GridParameter;
		
		%get GridType
		GridType=GridData.Gridfunction;
		
		switch GridType
			case 'norm2DGrid'
				%check if ParallelProcessing variable is existing
				if ~isfield(GridData,'ParallelCalc')
					GridData.ParallelCalc=false;
				end
				
				%Build boundary box
				regionFilter = obj.FilterList.getByName('Region');
				regionParms = regionFilter.getRegion();
				theRegion = regionParms{2};
				if strcmp(regionParms{1},'circle')
					radius=regionFilter.Radius;
					circ=scircle1(theRegion(1),theRegion(2),radius);
					mini=min(circ);
					maxi=max(circ);
					bBox=[mini(1),maxi(1),mini(2),maxi(2)];
				else
					bBox=theRegion.getBoundingBox();
				end
				
				%add Boundary Box
				GridData.boundaryBox=bBox;
				
				%add selected events
				GridData.SelectedEvents=TheSelected;
				
				%go calc
				[xRange,yRange,UnModRes]norm2DGridder(DataStore,CalcEntry.Calc_Element,GridData);
				
				%get into the right shape
				if isfield(GridData, 'Reshaping')
					ResultStruct=reformGrid(UnModRes,numel(xRange),numel(yRange),GridData.Reshaping);
				else
					ResultStruct=reformGrid(UnModRes);
				end
				
				%pack Result
				CalcRes=ResultStruct;
				CalcRes.xRange=xRange;
				CalcRes.yRange=yRange;
				
				
			case 'DepthProfileGrid'	
				%check if ParallelProcessing variable is existing
				if ~isfield(GridData,'ParallelCalc')
					GridData.ParallelCalc=false;
				end
				
				%Build ProfileLine
				regionFilter = obj.FilterList.getByName('Region');
				regionParms = regionFilter.getRegion();
				theRegion = regionParms{2};
				rBdry = theRegion.getBoundary();
				pline = [rBdry(1,:);rBdry(4,:)];
				
				%get ProfileWidth
				width=regionFilter.Width;
				
				%add ProfileLine
				GridData.ProfileLine=pline;
				
				%add ProfileWidth
				GridData.ProfileWidth=width;
				
				%add selected events
				GridData.SelectedEvents=TheSelected;
				
				%go calc
				[xRange,zRange,UnModRes]Profile2DGridder(DataStore,CalcEntry.Calc_Element,GridData);
				
				%get into the right shape
				if isfield(GridData, 'Reshaping')
					ResultStruct=reformGrid(UnModRes,numel(xRange),numel(zRange),GridData.Reshaping);
				else
					ResultStruct=reformGrid(UnModRes);
				end
				
				%pack Result
				CalcRes=ResultStruct;
				CalcRes.xRange=xRange;
				CalcRes.zRange=yRange;
				
				
			case 'TimeGrid'
				%check if ParallelProcessing variable is existing
				if ~isfield(GridData,'ParallelCalc')
					GridData.ParallelCalc=false;
				end
				
				%add selected events
				GridData.SelectedEvents=TheSelected;
				
				%go calc
				[Ranger,RawRes]=OneDGridder(Datastore,CalcEntry.Calc_Element,...
						GridData,'DecYear',true);
						
				%Reform Result
				ResultStruct=reformGrid(RawRes);
				
				%pack Result
				CalcRes=ResultStruct;
				CalcRes.Range=Ranger;
				
				
			case 'MagGrid'
				%check if ParallelProcessing variable is existing
				if ~isfield(GridData,'ParallelCalc')
					GridData.ParallelCalc=false;
				end
				
				%add selected events
				GridData.SelectedEvents=TheSelected;
				
				%go calc
				[Ranger,RawRes]=OneDGridder(Datastore,CalcEntry.Calc_Element,...
						GridData,'mag',false);
						
				%Reform Result
				ResultStruct=reformGrid(RawRes);
				
				%pack Result
				CalcRes=ResultStruct;
				CalcRes.Range=Ranger;
				
			
			case 'DepthGrid'
				%check if ParallelProcessing variable is existing
				if ~isfield(GridData,'ParallelCalc')
					GridData.ParallelCalc=false;
				end
				
				%add selected events
				GridData.SelectedEvents=TheSelected;
				
				%go calc
				[Ranger,RawRes]=OneDGridder(Datastore,CalcEntry.Calc_Element,...
						GridData,'depth',false);
						
				%Reform Result
				ResultStruct=reformGrid(RawRes);
				
				%pack Result
				CalcRes=ResultStruct;
				CalcRes.Range=Ranger;
				
				
			case 'Custom1D'
				%check if ParallelProcessing variable is existing
				if ~isfield(GridData,'ParallelCalc')
					GridData.ParallelCalc=false;
				end
				
				%add selected events
				GridData.SelectedEvents=TheSelected;
				
				%PlayField Parameters
				PlayField=GridData.AxisVariable;
				UserData=GridData.UserData;
				
				%go calc
				[Ranger,RawRes]=OneDGridder(Datastore,CalcEntry.Calc_Element,...
						GridData,PlayField,UserData);
						
				%Reform Result
				ResultStruct=reformGrid(RawRes);
				
				%pack Result
				CalcRes=ResultStruct;
				CalcRes.Range=Ranger;
				
		end
	
	end
	

	function ModCalc=reformGrid(OrgCalc,xdim,ydim,Reshaping)
		%the function will take the first cell element of the OrgCalc variable, get the fields
		%of the result structur and reshape the whole cell array into a structur with arrays
		%Reshaping will form the matrixes back to 2D matrix form, might be needed in some plotting forms.
		
		if nargin<4
			Reshaping=false;
		end
		
		ResFields=fields(OrgCalc{1});
		if ~Reshaping
			for i=1:numel(ResFields)
				try 
					ModCalc.(ResFields{i})=cellfun(@x x.(ResFields{i}),OrgCalc);
				catch
					%content is not a numeric or logical
					ModCalc.(ResFields{i})=cellfun(@x x.(ResFields{i}),OrgCalc,'UniformOutput',false);
				end	
			end
		
		else
			for i=1:numel(ResFields)
				try 
					ToReshape=cellfun(@x x.(ResFields{i}),OrgCalc);
				catch
					%content is not a numeric or logical
					ToReshape=cellfun(@x x.(ResFields{i}),OrgCalc,'UniformOutput',false);
				end	
					ModCalc.(ResFields{i})=reshape(ToReshape,xdim,ydim);
			end
		
		end
	end
	
	
	
	function CalcConfig=ParamConfigSender(obj,CalcSlideNr)
		%This function writes out the Configuration for the ParameterGUI
		%normally called from the outside
		%Depending on how the GUI is build it is best to preproduce the CalcSlide
		%before. Import is not to forget to update the CalcSlide values in the internal 
		%memory. The object variable ParamGUIEntries can be used to save the entries
		
		%(In this version the configurations are predefined but updated each time the
		%function ParamConfigSender is called, this is needed because of the time selection 
		%points which have to been show all the time in the resultGUI).
			
		%get Key
		CalcKey=obj.CalcKeyList{CalcSlideNr,1};
		
		%update the slide
		obj.ModifyCalcConfig('update',CalcKey);
		
		%get the slide
		CalcConfig=obj.ParamGUIEntries{CalcSlideNr,2};
		
		
	end

	
	function TheColor=getCalcColor(obj,CalcSlideNr)
		%Similat to ParamConfigSender, but sends a Color to the ParamGUI (or ResultGUI) 
		%If the function is missing the windows will just be grey. 
		%It has to be done that way because the ParamGUI uses a cell array as config and not a structur.
		
		%get Key
		CalcKey=obj.CalcKeyList{CalcSlideNr,1};
		
		%Lookup
		CalcEntry=obj.CalculationHash.lookup(CalcKey);
		
		try
			TheColor=CalcEntry.GUIParameters{1};
		catch 
			TheColor=[];
		end	
			
	end
	
	
	function ResultConfigBuilder(obj)
		%This function writes the configuration of the ResultViewerGUI
		%It is called everytime, the Calculations are modified.
		%The function is optional similar to ModifyCalcConfig, only for 
		%ResultGUI and does not need and DoWhat option, as it will be called
		%only if calculations are added or remove. 
		
	end
	
	
	
	
	function drawPlots(obj,RegParameter)
		%This function gets the parameter from the specified ResultGUI and uses the defined plotfunction
		%to plot the functions into the window 
		%It is not needed to use this function, instead the PlotWriter can be used, in this case the 
		%ResultViewerGUI will do the whole plotting. This function is only needed if an entirely different
		%plotting function is wished. 
		
		
		 
	
	end


	
	function ResultStruct = PlotWriter(obj,PlotParameter);
		%This function writes the result for the ResultGUI and returns them as a structur
		%the function is called from the ResultGUI and at least a skeleton needs to be existing
		%(which is done by using this Superobject)
		
		




	end
	
	
	
	
	function DataNFilter=FindUniqueData(obj);
		%The function checks all CalcElement and save the unique DataStore FilterList combinations
		%in a array with first column datastore number in the commander and second column Filterlist number
		%needed by BuildCalcKeyList
		
		%get through the ParamGUIEntries and check the position of every datastore filterlist selector.
		for i=1:numel(obj.ParamGUIEntries(:,1))
			%get Position of the filterselector and dataselector
			disp(obj.ParamGUIEntries)
			try
			dataPos=obj.ParamGUIEntries{i,3};
			catch
			dataPos=1;
			disp('notfound');
			end	
			
			try
			filterPos=obj.ParamGUIEntries{i,4};
			catch
			filterPos=1;
			disp('notfound');
			end	
			
			UniqueString=['df',num2str(dataPos),'_',num2str(filterPos)];
			
			RawList(i,:)={dataPos,filterPos,UniqueString};
			
		end
		
		%Get unique combinations
		[b m n]=unique(RawList(:,3));
		
		DataNFilter=RawList(m,:);
		
	end
		
		
		
	function BuildCalcKeyList(obj)
		%This function builds the CalcKeyList and should be called every time the calculation
		%Hash is modified. 
		%The CalcKeyList is a Cellarray with 3 column: 
		%CalcKeyList{:,1}: The Keys of the calculation
		%CalcKeyList{:,2}: The Name (used for the Selector) of the calculation
		%CalcKeyList{:,3}: The CalcType of the calculation
		
		%The CalcKeyList my not be always needed, in case not needed some function have to be replace
		%in wrapper using this class
		
		
		%EmptyList
		obj.CalcKeyList={};
		
		%get all keys
		keys=obj.CalculationHash.getKeys;
		
		for i=1:numel(keys)
			CalcEntry=obj.CalculationHash.lookup(keys{i})
			
			%Name
			NameEntry=CalcEntry.Name;
			
			%Type
			CalcType=CalcEntry.CalcType;
			
			%generate entry name if not existing
			if isempty(NameEntry)
				NameEntry=[CalcType,' Calculation_', num2str(keys{i})];
			end
		
			%write into CalcKeyList	
			obj.CalcKeyList(i,:)={keys{i},NameEntry,CalcType};
			
			%has to be done in case an element has no name.
			NameEntry=[];
		end
		
		
	
	end
		
		
		
	function [entryfields points]=CreateSelectionPoints(obj,CalcKey,EntryText,Parameter,WhichCoord,TagTemp,WindowTag,Autofire)
		%This function creates the entries for the dragable points set in the ResultGUI
		%and also the entryfields for the CalcParamGUI. 
		%Has to be called each time a calculation which needs the points is added to the hash
		
		%The Tags will be generated automatically dependen on the TagTemp, the CalcKey and the 
		%WhichCoord variable: [TagTemp, Calckey, 'x'] (the WhichCoord entry x or y) will only be used
		%if set to both.
		
		%WhichCoord sets which coordinate will be used, if set to both, two fields will be produces
		
		%EntryText: Will be used for the text above the entryfield, if left empty now text above the
		%	    Entryfield will be used.
		
		entryfields={};
		
		

		
		%get entry from the hash
		TheCalc=obj.CalculationHash.lookup(CalcKey);
		
		%Color
		TheColor=TheCalc.GUIParameters{1};
		
		%produce Tags and modify the entries if needed.
		switch WhichCoord
			case 'x'
				Tags{1}=[TagTemp,CalcKey];
				
				if ~isempty(EntryText)
					Text{1}=EntryText;
				else
					Text{1}=[];
				end
				
				DefaultVal=TheCalc.CalcParam.(Parameter);
				PointDefault(1)=DefaultVal;
				PointDefault(2)=nan;
				
				Para{1}=Parameter;
				
				Direct{1}='x';
				
			case 'y'
				Tags{1}=[TagTemp,CalcKey];
				
				if ~isempty(EntryText)
					Text{1}=EntryText;
				else
					Text{1}=[];
				end
				
				DefaultVal=TheCalc.CalcParam.(Parameter);
				PointDefault(1)=nan;
				PointDefault(2)=DefaultVal;
				
				Para{1}=Parameter;
				
				Direct{1}='y';
				
			case 'both'
				Tags{1}=[TagTemp,CalcKey,'x'];
				
				if ~isempty(EntryText{1})
					Text{1}=EntryText{1};
				else
					Text{1}=[];
				end
				DefaultVal(1)=TheCalc.CalcParam.(Parameter{1});
				Para{1}=Parameter{1};
				
				
				Tags{2}=[TagTemp,CalcKey,'y'];
				
				if ~isempty(EntryText{2})
					Text{2}=EntryText{2};
				else
					Text{2}=[];
				end
				DefaultVal(2)=TheCalc.CalcParam.(Parameter{2});
				Para{2}=Parameter{2};
				
				Direct{1}='x';
				Direct{2}='y';
				
				PointDefault=DefaultVal;
				
		end
		
		%The point needs only one tag
		PointTag=[TagTemp,CalcKey];
		
		
		%produce entryfields
		
		for i=1:numel(Tags)
			if ~isempty(Text{i})
				modul = struct(	'ModulType','Texter',...
						'Tag',[Tags{i},'_text'],...
						'String',Text{i});
				
				entryfields{end+1}=modul;
			end
			
			modul =struct(	'ModulType','Positionfield',...
					'Tag',Tags{i},...
					'String',point2Entry(obj,DefaultVal(i)),...
					'WhichCoord',Direct{i},...
					'PointTag',PointTag,...
					'WindowTag',WindowTag,...
					'CalculationKey',CalcKey,...
					'Parameter',Para{i});
			entryfields{end+1}=modul;		
		end
		
		
		
		%produce pointstructure
		PointModul= struct( 	'Tag', PointTag,...
					'EntryFieldTag',{Tags},...
					'Colors',TheColor,...
					'WindowTag',WindowTag,...
					'Coord',PointDefault,...
					'CalculationKey',CalcKey,...
					'WhichCoord',WhichCoord,...
					'Parameter',{Para},...
					'AutoFire',Autofire);
					
		points=PointModul;
	
	end
		
		
		
		
	function PointConfStruct=createPointStructur(obj)
		%The function takes the PointConfig and builds a structur for the ResultGUI
		
		PointConfig=obj.PointConfig
		
		for i=1:numel(PointConfig(:,3))
			PointConfStruct.(PointConfig{i,3})=PointConfig{i,2};
		end	
		
	
	end
		
		
	function ChangeParameterPoint(obj,FromGUI,TheHandle,WindowTag,WhichCoord,Tags,CalcKey,Parameter)
		%This function will be called from the ResultGUI and the Parameter GUI
		%It is a link between a certain ImPoint and a Entryfield in either the ResultGUI
		%or in the ParameterGUI.
		%When called by either object it will change the other object to the selected parameter
		%and also change the calculation parameter of the wrapper calc element.
		%Tag,CalcSlide,Parameter and TheHandle have to be a two dimensional cell vector (1)=x 
		%and (2)=y when the parameter WhichCoord is set to both (x and y) 
		
		%In this case the Tags of the Entryfield have to be unique in the whole Wrapper.
		%It is not enough that the Tags are unique in one Calculation.
		
		
		%Transform to cell array where it is needed
		if ~iscell(TheHandle)
			TheHandle={TheHandle};
			
		end
		
		if ~iscell(Parameter)
			Parameter={Parameter};
		end	
		
		%Only if the Tag represent a EntryField two tags are needed.
		if ~iscell(Tags)
			Tags={Tags};
		end
		
		
		switch FromGUI
			case 'ResultGUI'
				%TheHandle, should be the object and not the handle
				%get position
				newPos=getPosition(TheHandle{:});
				
				
				%get the entryfields
				ModulList=obj.CalcParameterGUI.ModulList;
				
				TagList={};
				HandleList={};
				
				count=1;
				for i=1:numel(ModulList)
					%future proof, in case in future version more than one 
					%entryfield per modul is allowed
					if iscell(ModulList{i})
						subModuls=ModulList{i};
						for j=1:numel(subModuls)
							TagList{count}=get(subModuls{j},'Tag');
							HandleList{count}=subModuls{j};
							count=count+1;
						end
					else
					TagList{count}=get(ModulList{i},'Tag');
					HandleList{count}=ModulList{i};
					count=count+1;
					end
				end
				
				%try to find the tags
				for i=1:numel(Tags)
					foundTags(:,i)=strcmp(TagList,Tags{i});
				end
				
				%now decide which coordinate is used
				switch WhichCoord
					case 'both'
						%check if x entryfield is existing
						if any(foundTags(:,1))
							set(HandleList{foundTags(:,1)},...
							'String',point2Entry(obj,newPos(1)));
						end
						
						%check if y entryfield is existing
						if any(foundTags(:,2))
							set(HandleList{foundTags(:,2)},...
							'String',point2Entry(obj,newPos(2)));
						end
						
					case 'x'
						%check if x entryfield is existing
						if any(foundTags(:,1))
							set(HandleList{foundTags(:,1)},...
							'String',point2Entry(obj,newPos(1)));
						end
						
					case 'y'
						%check if y entryfield is existing
						if any(foundTags(:,1))
							set(HandleList{foundTags(:,1)},...
							'String',point2Entry(obj,newPos(2)));
						end
					
				end
				
			case 'ParamGUI'
				%TheHandle should be the handle of the entry field

				
				%Normally only one handle is possible in a Entryfield Callback 
				%but in case a button or similar is used, two Callbacks
				%might be used
				for i=1:numel(TheHandle)
					%get the value
					strval=get(TheHandle{i},'String');
					newPos(i)=Entry2Point(obj,strval);
				end
				
				%find the impoint from the ResultGUI
				pointList=obj.ResultGUI.point_objects.(WindowTag);
				
				TagList={};
				for i=1:numel(pointList)
					TagList{i}=get(pointList{i},'Tag');
				end
				if ~isempty(TagList)
					foundTag=strcmp(TagList,Tags{1});
					
					PointObject=pointList{foundTag};
					
					%get old position (needed when not both coordinates are used)
					oldPos=PointObject.getPosition;
					
					%now decide which coordinate is used
					switch WhichCoord
						case 'both'
							PointObject.setPosition(newPos(1),newPos(2));
						case 'x'
							PointObject.setPosition([newPos(1),oldPos(2)]);
						case 'y'
							PointObject.setPosition([oldPos(1),newPos(1)]);
					end		
					
					%COMMENT: 	The points normally have a position callback, if the 
					%		position is set with the command setPosition, the callback 
					%		will also be called, means this function (if set to autofire)
					%		will be called again and the result will be written into to the
					%		entryfield, but those will not trigger the Callback again, so it
					%		should stop there. It might be an idea for the perfomance to 
					%		prevent this behavior, a way could be to check if the values 
					%		change and only do something if so, or the callback function can
					%		remove before changing the position of the point and set again
					%		afterwards.
				end	
		
		end
		
		
		%In both cases the new value(s) have to be written into the Hash

		%get the calcElement
		TheCalc=obj.CalculationHash.lookup(CalcKey);
		
		switch WhichCoord
			case 'both'
				TheCalc.CalcParam.(Parameter{1})=newPos(1);
				TheCalc.CalcParam.(Parameter{2})=newPos(2);
				
			case 'x'
				TheCalc.CalcParam.(Parameter{1})=newPos(1);
				
			case 'y'
				TheCalc.CalcParam.(Parameter{1})=newPos(2);
		end
		%disp(TheCalc.CalcParam)
		
		
		%Update Calc Element
		TheCalc.Calc_Element.ChangeValues(TheCalc.CalcParam);	
		
		%put the Calculation back into the hash
		TheCalc.ParamChanged=true
		obj.CalculationHash.set(CalcKey,TheCalc);
		
		
		%refresh the point config:
		switch FromGUI
			case 'ResultGUI'
				PointName=get(TheHandle{:},'Tag');					
			
			case 'ParamGUI'
				PointName=get(PointObject,'Tag');
				
			
		end	
		
	
		
		PoiEntry=strcmp(obj.PointConfig(:,3),PointName);
		
		
		
		if any(PoiEntry)
	
			obj.PointConfig{PoiEntry,2}.Coord=newPos;
			%disp(obj.PointConfig{PoiEntry})
		end	
		
		
	end
	
		
		
	function outString=point2Entry(obj,numValue)
		%It seems strange to use two function on after an other to
		%convert a simple value in the pointchanger, the idea behind this
		%is to bann every function which might not be modifed by the user to 
		%the superclass, this means also the function of the pointchanger
		
		%basicfunction
		%outString=num2str(numValue);	
		
		%datevec to timestring
		outString=mapseis.LineCalcWrapper.RateCompareWrapper.decyear2string(numValue);
		
		
		
	end
	
	function outNumber=Entry2Point(obj,StringValue)
		%It seems strange to use two function on after an other to
		%convert a simple value in the pointchanger, the idea behind this
		%is to bann every function which might not be modifed by the user to 
		%the superclass, this means also the function of the pointchanger
		
		%basicfunction
		%outNumber=str2num(StringValue);	
		
		%datevec to timestring
		outNumber=mapseis.LineCalcWrapper.RateCompareWrapper.string2decyear(StringValue);
		
		
		
	end	
	
		
	function SaveCalculations(obj,filename,All_Single,CalcKey)
		%This function allows to save parts of the calculation Hash or
		%the whole hash. Needed are the filename (has to be given by the GUI)
		%the variable All_Single, with 'all' for saving the whole hash or 'single'
		%for saving only a single Entries, in this cas the CalcKey is needed
		
		if nargin<4
			CalcKey=[];
		end
		
		if ~isempty(CalcKey) & ~iscell(CalcKey)
			CalcKey={CalcKey};
		end
		
		switch All_Single
			case 'single'
				for i=1:numel(CalcKey)
					entry = obj.CalculationHash,lookup(CalcKey{i});
					CalcHash(i,1)=CalcKey(i);
					CalcHash{i,2}=entry;
				end	
				
			case 'all'
				CalcHash=obj.CalculationHash.ExportHash;
				
			
		end
		
		try 
			save(filename,'CalcHash');
		catch
			errordlg('File could not be saved');
		end	
		
		
	
	end
		
		
	function LoadCalculation(obj,filename,add_replace)
		%This function loads a Calculation entry or a whole hash (depended on the 
		%saved data) filename gives the location of the Calculation and add_replace
		%determines how the data will be loaded, 'add' will give entries new keys and 
		%add them to the existing hash, 'replace' will delete all entry existing in the 
		%hash and use the new Calculation Elements instead and 'overwrite' will overwrite 
		%all Calculation entries which have the same key as an entry in the loaded data
		
		try 
			loadedHash=load(filename);
		catch
			errordlg('File could not be loaded');
			add_replace='error';
		end
		
		try
			newCalcHash=loadedHash.CalcHash;
		catch
			errordlg('The file did not contain any Calculations');
			add_replace='error';
		end
		
		
		switch add_replace
			case 'add'
				newCalcHash=obj.HashKeyCorrector(newCalcHash,1,'inital');
				%set to loaded true and add them
				for i=1:numel(newCalcHash(:,1))
					newCalcHash{i,2}.Loaded=true;
					obj.CalculationHash.set(newCalcHash{i,1},newCalcHash{i,2});
				end
				
			
			case 'replace'
				%set to loaded true
				for i=1:numel(newCalcHash(:,1))
					newCalcHash{i,2}.Loaded=true;
				end
				
				%replace
				obj.CalculationHash.replaceHash(newCalcHash);
				
				%set the the lastkey right
				lastKey=max(cellfun(@str2num,newCalcHash(:,1)));
				obj.NextKey=num2str(lastKey+1);
				
				
			case 'overwrite'
			
				%overwrite and add
				for i=1:numel(newCalcHash(:,1))
					newCalcHash{i,2}.Loaded=true;
					obj.CalculationHash.set(newCalcHash{i,1},newCalcHash{i,2});
				end
				
				%set the the lastkey right
				TheKeys=obj.CalculationHash.getKeys;
				lastKey=max(cellfun(@str2num,TheKeys));
				obj.NextKey=num2str(lastKey+1);
				
			case 'error'
				%do nothing, there was an error
				%might add a operation later if needed
		end	
	
	
	end
		
	
		
	function correctHash = HashKeyCorrector(obj,TheHash,EntryNumber, state)
		%The function is needed for the correction of the key incases where the loaded
		%Calculations are just added to the existing Hash, it is a recursive function
		%To the hash there will be added a third column with a logical value determine if the
		%The entry has already been changed, if the function is called with the state 'inital'
		if nargin< 4
			EntryNumber=1;
		end
		
		switch state
			case 'inital'
				Hashlength=numel(TheHash(:,1));
				TheHash(:,3)=num2cell(logical(zeros(Hashlength,1)));
				countID=1:Hashlength;
				
				%go througth it by recursion
				TheHash=obj.HashCorrector(TheHash,EntryNumber,'recursion');
				
				%Remove the Logicals
				TheHash=TheHash(:,1:2);
				
				
			case 'recursion'
			
				if ~TheHash{EntryNumber,3}
						if ~isempty(TheHash{EntryNumber,2}.Successor)
							for i=1:numel(TheHash{EntryNumber,2}.PreSuccessor)
								found=strcmp(TheHash(:,1),TheHash{EntryNumber,2}.PreSuccessor{i});
								if any(found) & ~TheHash{found,3}
									TheHash=HashCorrector(obj,TheHash,countID(found),'recursion');
									TheHash{EntryNumber,2}.Successor(i)=TheHash(found,1);
								end	
								
							end	
						end
						
						%save old key incase of successors
						oldKey=TheHash{EntryNumber,1};
						
						%set new Key
						TheKey=obj.NextKey;
						newKey=str2num(TheKey);
						newKey=newKey+1;
						obj.NextKey=num2str(newKey);
						%write to hash
						TheHash{EntryNumber,1}=TheKey;
						
						
						if ~isempty(TheHash{EntryNumber,2}.Successor)
							for i=1:numel(TheHash{EntryNumber,2}.Successor)
								found=strcmp(TheHash(:,1),TheHash{EntryNumber,2}.Successor{i});
								if any(found)
									oldPosi=strcmp(TheHash{i,2}.PreSuccessor,oldKey);
									TheHash{i,2}.PreSuccessor{oldPosi}=TheKey;
								end	
								
							end	
						
						end
						
						if EntryNumber<=numel(TheHash(:,1))
							TheHash=obj.HashCorrector(TheHash,EntryNumber+1,'recursion');
						end	
						
				else
					
					if EntryNumber<=numel(TheHash(:,1))
							TheHash=obj.HashCorrector(TheHash,EntryNumber+1,'recursion');
					end	
						
				end
				
			
		end
		
		
			
		
	end
	
		
				
	function SaveSession(obj,filename)
		%Saves all the calculations and all the datastores and filterlists
		
		%get Hashes from Commander
		Hashes=obj.Commander.SendSession('BothHash');
		DataHash=Hashes{1};
		FilterHash=Hashes{2};
		
		%get Hash from CalculationHash
		CalcHash=obj.CalculationHash.ExportHash;
		
		
		%save it
		try 
			save(filename,'DataHash','FilterHash','CalcHash');
		catch
			errordlg('File could not be saved');
		end	
		
	end
	


	function LoadSession(obj,filename)
		%Loads all the calculations and all the datastores and filterlists

		ErrorHandle=false;
		
		%Check if all data is available, at the moment every part is needed, later
		%it might be possible to only load certain parts
		
		try 
			loadedHash=load(filename);
		catch
			errordlg('File could not be loaded');
			ErrorHandle=true;
		end
		
		try
			CalcHash=loadedHash.CalcHash;
		catch
			errordlg('The file did not contain any Calculations');
			ErrorHandle=true;
		end
		
		try
			DataHash=loadedHash.DataHash;
		catch
			errordlg('The file did not contain any DataStoreHash');
			ErrorHandle=true;
		end
		
		try
			FilterHash=loadedHash.FilterHash;
		catch
			errordlg('The file did not contain any FilterListHash');
			ErrorHandle=true;
		end
		
		
		if ~ErrorHandle
			%CalculationHash
			%set to loaded true
			for i=1:numel(CalcHash(:,1))
				CalcHash{i,2}.Loaded=true;
			end
				
			%replace
			obj.CalculationHash.replaceHash(CalcHash);
				
			%set the the lastkey right
			lastKey=max(cellfun(@str2num,CalcHash(:,1)));
			obj.NextKey=num2str(lastKey+1);
		
			%Commander update
			CommandHash{1}=DataHash;
			CommandHash{2}=FilterHash;
			
			obj.Commander.ReplaceSession(CommandHash,'BothHash');
		
		end
		
		
		
		
	end
	
	
	
	
	function loadCalcGUI(obj,HowTo)
		%loads a calculation, called by the GUI	
	
		[fName,pName] = uigetfile('*.mat','Enter MapSeis Result *.mat file name...');
			
		if isequal(fName,0) || isequal(pName,0)
			errordlg('No filename selected. ');
		else
			filename=fullfile(pName,fName);
			
			obj.LoadCalculation(filename,HowTo);
			
			obj.RebuildCalcSlides;
			obj.BuildCalcKeyList;
			obj.ResultConfigBuilder;
			obj.ResultGUI.PointMaker('stopall');
			obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;
			obj.ResultGUI.PointConfig=obj.createPointStructur;
			obj.ResultGUI.rebuild;
			obj.ResultGUI.PointMaker('start');
			obj.drawPlots([]);
			obj.notify('CalcHashUpdated');
			
		end	     
		
	
	end
	
	
	
	
	function saveCalcGUI(obj,HowTo)
		%save calculation, this is called from the GUI
		
		 [fName,pName] = uiputfile('*.mat','Enter save file name...');
		 if isequal(fName,0) || isequal(pName,0)
			errordlg('No filename selected.  Data not saved');
		 else
			filename=fullfile(pName,fName);
			
			switch HowTo
				case 'single'
					CalcEntry=obj.CalcParameterGUI.CurrentCalcSlide;
					CalcKey=obj.CalcKeyList{CalcEntry,1};
					obj.SaveCalculation(filename,'single',CalcKey);
					
				case 'all'
					obj.SaveCalculation(filename,'single',[]);
			end
			
			
		 end
	end
	
	
	
	
	function loadSessionGUI(obj,HowTo)
		%HowTo here not needed but, will be delivered for future use by the GUI
		
		[fName,pName] = uigetfile('*.mat','Enter MapSeis Session *.mat file name...');
		
		if isequal(fName,0) || isequal(pName,0)
			errordlg('No filename selected. ');
		else
			filename=fullfile(pName,fName);
			obj.LoadSession(filename);
			
		end
		
		obj.RebuildCalcSlides;
		obj.BuildCalcKeyList;
		obj.ResultConfigBuilder;
		obj.ResultGUI.PointMaker('stopall');
		obj.ResultGUI.GUIConfig=obj.ResultGUIConfig;
		obj.ResultGUI.PointConfig=obj.createPointStructur;
		obj.ResultGUI.rebuild;
		obj.ResultGUI.PointMaker('start');
		obj.drawPlots([]);
		obj.notify('CalcHashUpdated');
	end	
	
	
	
	
	function saveSessionGUI(obj,HowTo)
		%HowTo here not needed but, will be delivered for future use by the GUI
		
		 [fName,pName] = uiputfile('*.mat','Enter save file name...');
		 if isequal(fName,0) || isequal(pName,0)
			errordlg('No filename selected.  Data not saved');
		 else
			filename=fullfile(pName,fName);
			obj.SaveSession(filename);
			
		 end
	
	end
	
	
	
	function RebuildCalcSlides(obj)
		%needed for the load option, it should take every element of the CalcHash
		%and rebuild the Parameter window entry (for instance by calling ModifyCalcConfig) 
	
	end	
	
		
		
end	


methods (Static)
	
	%usefull methods to convert decyear to datestrings and vice versa
	function TheDateString=decyear2string(decYR)
		import mapseis.util.decyear2matS;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=decyear2matS(decYR);
		TheDateString=datestr([fYr, nMn, nDay, nHr, nMin, nSec],'yyyy-mm-dd HH:MM:SS');
	end
	
	
	function decimalYear=string2decyear(instring)
		import mapseis.util.decyear;	
	
		[fYr, nMn, nDay, nHr, nMin, nSec]=datevec(instring,'yyyy-mm-dd HH:MM:SS');
		decimalYear=decyear([fYr, nMn, nDay, nHr, nMin, nSec]);
	end
end
	
	

end
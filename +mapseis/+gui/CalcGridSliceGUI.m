classdef CalcGridSliceGUI < handle
    % CalcGridSliceGUI : GUI for the MapSeis application grid calculations on a depth slice
    % This function creates a GUI with a single plot axis that shows the
    % results of extracting all the events within the given radius around each
    % grid point, applying the calculation function calcFun and projecting out
    % the required result using projFun.
    
    % $Revision: 1 $    $Date: 2008-15-12 
    % Author: David Eberhard    
    properties
        DataStore
        FilterList
        
        GUIHandle
        GridSep
        Rad
        XRange
        YRange
        CalcRes
        CalcVals
        InputProjFun
        CalcObj
        OutputProjFun
        ProfileWidth
        OptionMenu
        GridMenu
        PlotQuality
        WindowName
        
    end
    
    methods
        
        function obj = CalcGridSliceGUI(dataStore,viewName,calcObj,outputProj,width,filterList)
            % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.filter.*;
            
            obj.DataStore = dataStore;
            obj.FilterList = filterList;
            obj.PlotQuality = dataStore.getUserData('PlotQuality');
			obj.ProfileWidth = width;
			obj.WindowName = viewName;
			
            % Create the figure window that contains the GUI.  Use a one panel layout.
            obj.GUIHandle = onePanelLayout('MapSeis Grid  Depth Profile Calculation Plot',...
                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
            set(obj.GUIHandle,'Position',[ 50 25 900 275 ]);
          
            
            % Parameters that the define the grid and the region of interest around
            % each grid point.
            try
                [gridSep,rad]=dataStore.getUserData('gridDepthPars');
            catch ME
                gridSep = [];
                rad = 0;
            end
            obj.GridSep = gridSep;
            obj.Rad = rad;
            
            % Calculation results
            obj.XRange = [];
            obj.YRange = [];
            obj.CalcRes = {};
            obj.CalcVals = {};
            
            % Extract the processing functions from the input struct
            
            % Input projector to extract the needed data fields from the event data
            % about each grid point eg @(dataStore) dataStore.getMagnitudes('gridPt')
%             obj.InputProjFun = calcStruct.inputProjFun;
            % Calculation to be applied to the data matrix about each grid point
%             obj.CalcFun = calcStruct.calcFun;
            obj.CalcObj = calcObj;
            % Output projector to apply to each element of cell array returned by
            % applying calcFun to data about each grid point
            obj.OutputProjFun = outputProj;
            
            % Register this view with the model.  The subject of the view uses this
            % function handle to update the view.
            dataStore.subscribe(viewName,@() updateGUI(obj));
            
            % Update the GUI to plot the initial data
            obj.updateGUI();
            
            % Make the GUI visible
            set(obj.GUIHandle,'Visible','on');                        
            
            % Create the menu (here we refer to the object itself)
            
            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
            uimenu(obj.OptionMenu,'Label','Zmap style',...
                'Callback',@(s,e) obj.setqual('low'));
            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
                'Callback',@(s,e) obj.setqual('med'));
            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
                'Callback',@(s,e) obj.setqual('hi'));
            
			obj.setqual('chk');

            
            
            obj.GridMenu=uimenu(obj.GUIHandle,'Label','  --  Grid');
            uimenu(obj.GridMenu,'Label','Set grid',...
                'Callback',@obj.setGridParams);
            uimenu(obj.GridMenu,'Label','Calculate',...
                'Callback',@(s,e) obj.calculate());
            uimenu(obj.GridMenu,'Label','Clear',...
                'Callback',@(s,e) obj.clearCalculate());
                
                
        end
        
        
        
     
        
      
        
        function calculate(obj)
            import mapseis.calc.*;
            
            regionFilter = obj.FilterList.getByName('Region');
            regionParms = regionFilter.getRegion();
            Select = obj.FilterList.getSelected;
             
            if ~isempty(regionParms) && ~isempty(obj.GridSep) && obj.Rad ~= 0
                
                theRegion = regionParms{2};
                rBdry = theRegion.getBoundary();
				pline = [rBdry(1,:);rBdry(4,:)];
                
                tic;
                [obj.XRange,obj.YRange,obj.CalcRes] = ...
                    CalcDepthGrid(struct('DataStore',obj.DataStore,...
                                    'GridSeparation',obj.GridSep,...,
                                    'SelectionRadius',obj.Rad,...
                                    'ProfileLine',pline,...
                                    'ProfileWidth',obj.ProfileWidth,...
                                    'SelectedRows',Select),obj.CalcObj);
                timeTaken = toc;
                disp(['Grid calculation took ',num2str(timeTaken),'s']);
                obj.CalcVals = cellfun(obj.OutputProjFun,obj.CalcRes);
                obj.updateGUIWithResults();
            else
                return
            end
        end
        
        function updateGUI(obj)
            % This method will plot the events only, and clear any
            % calculation results.
            import mapseis.plot.PlotGridCalc;
            import mapseis.plot.PlotRegion;
            import mapseis.projector.*;
            import mapseis.region.*
            
            % Read the grid parameters from the UserData of the event data
            % store
            gridPars = obj.DataStore.getUserData('gridDepthPars');
            obj.GridSep = gridPars.gridSep;
            obj.Rad = gridPars.rad;
            % Update the GUI
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            regionFilter = obj.FilterList.getByName('Region');
            regionParms = regionFilter.getDepthRegion();
            pRegion = regionParms{2};
            DRegion = regionParms{3};
            [inReg,outReg] = ...
                getLocations(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            [selectedMags, unselectedMags] = ...
                getMagnitudes(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            [selectedDepths, unselectedDepths] = ...
                getDepths(obj.DataStore,obj.FilterList.getSelected()); 
            
            mags.selected = selectedMags;
            mags.unselected = zeros(0,1);
            
            if ~isempty(pRegion) && ~isempty(obj.GridSep) && obj.Rad ~= 0
                %             PlotRegion(plotAxis,inReg,mags);
                rBdry = pRegion.getBoundary();
                
                 %calculate the distances on the profile
            	ProfileLine =  Slicer([rBdry(1,:);rBdry(4,:)],0.5); 
                distanc = ProfileLine.LineProjection(inReg(:,1),inReg(:,2));
                 
                % Grid points
                gPts = DRegion.getGrid(obj.GridSep);
                PlotRegion(plotAxis,struct('inRegion',distanc,...
                    'outRegion',zeros(0,2),...
                    'boundary',rBdry,'grid',gPts,...
                    'Depths',selectedDepths,...
                    'PlotType','DepthSlice',...
                    'Quality',obj.PlotQuality),mags);
            else
                PlotRegion(plotAxis,inReg,mags);
            end
            set(plotAxis,'Tag','axis');
        end
        
        function updateGUIWithResults(obj)
            % This method will only be called after the grid calculation
            % has been performed.
            import mapseis.plot.PlotDepthGridCalc;
            import mapseis.plot.PlotRegion;
            import mapseis.projector.*;
            import mapseis.region.*;
            
            % Read the grid parameters from the UserData of the event data
            % store
            gridPars = obj.DataStore.getUserData('gridDepthPars');
            obj.GridSep = gridPars.gridSep;
            obj.Rad = gridPars.rad;
            % Update the GUI
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            regionFilter = obj.FilterList.getByName('Region');
            regionParms = regionFilter.getDepthRegion();
            pRegion = regionParms{2};
            DRegion = regionParms{3};
            [inReg,outReg] = ...
                getLocations(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            [selectedMags, unselectedMags] = ...
                getMagnitudes(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            [selectedDepths, unselectedDepths] = ...
                getDepths(obj.DataStore,obj.FilterList.getSelected()); 
            mags.selected = selectedMags;
            mags.unselected = zeros(0,1);
            if ~isempty(obj.CalcVals)
                % Plot the b mean mag values
                PlotDepthGridCalc(plotAxis,{obj.XRange, obj.YRange}, obj.CalcVals);
            elseif ~isempty(pRegion) && ~isempty(obj.GridSep) && obj.Rad ~= 0
                %             PlotRegion(plotAxis,inReg,mags);
                rBdry = pRegion.getBoundary();
                
                 %calculate the distances on the profile
            	ProfileLine =  Slicer([rBdry(1,:);rBdry(4,:)],0.5); 
                distanc = ProfileLine.LineProjection(inReg(:,1),inReg(:,2));
                
                % Grid points
                gPts = DRegion.getGrid(obj.GridSep);
                PlotRegion(plotAxis,struct('inRegion',distanc,...
                    'outRegion',zeros(0,2),...
                    'boundary',rBdry,'grid',gPts,...
                    'Depths',selectedDepths,...
                    'PlotType','DepthSlice',...
                    'Quality',obj.PlotQuality),mags);
            else
                PlotRegion(plotAxis,inReg,mags);
            end
            set(plotAxis,'Tag','axis');
        end
        
        function setGridParams(obj,source,event) %#ok<INUSD>
            % Create a dialog box to set the grid separation and radius of
            % interest
            
            import mapseis.gui.setGridDepthParams;
            
            try
                setGridDepthParams(obj.DataStore);
                obj.updateGUI();
            catch ME
            end
        end
        
        function clearCalculate(obj)
            % Clear the calculated values and replot the events.
            obj.CalcVals = {};
            obj.updateGUI();
        end
        
        
        function setqual(obj,qual)
			%sets the quality of the plot (just for the calculation plot
			%chk just gets the userdata parameter and sets the 'checked' to the menu
			Menu2Check = get(obj.OptionMenu,'Children');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
                case 'low'
                	obj.PlotQuality = 'low';
                	set(Menu2Check(3), 'Checked', 'on');
                	obj.updateGUI;
                	
				case 'med'
					obj.PlotQuality = 'med';
                	set(Menu2Check(2), 'Checked', 'on');
					obj.updateGUI;
					
				case 'hi'
					obj.PlotQuality = 'hi';
					set(Menu2Check(1), 'Checked', 'on');
					obj.updateGUI;
					
				case 'chk'
					StoredQuality = obj.PlotQuality;

							switch StoredQuality
								case 'low'
									sel=3;
								case 'med'
									sel=2;
								case 'hi'
									sel=1;
								end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
		end
		
		
		function rebuildGUI(obj,dataStore, filterList)
			%allows to rebuild a closed GUI window
			
			   % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.filter.*;
            
            
            try 
            	set(obj.GUIHandle,'Visible','on');
            
            catch		
	            obj.DataStore = dataStore;
	            obj.FilterList = filterList;
		
			
				% Create the figure window that contains the GUI.  Use a one panel layout.
	            obj.GUIHandle = onePanelLayout('MapSeis Grid  Depth Profile Calculation Plot',...
	                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
	            set(obj.GUIHandle,'Position',[ 50 25 900 275 ]);
	          
	           	            % Register this view with the model.  The subject of the view uses this
	            % function handle to update the view.
	            dataStore.subscribe(viewName,@() updateGUI(obj));
	            
	            % Update the GUI to plot the initial data
	            obj.updateGUI();
	            
	            % Make the GUI visible
	            set(obj.GUIHandle,'Visible','on');                        
	            
	            % Create the menu (here we refer to the object itself)
	            
	            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
	            uimenu(obj.OptionMenu,'Label','Zmap style',...
	                'Callback',@(s,e) obj.setqual('low'));
	            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
	                'Callback',@(s,e) obj.setqual('med'));
	            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
	                'Callback',@(s,e) obj.setqual('hi'));
	            
				obj.setqual('chk');
	
	            
	            
	            obj.GridMenu=uimenu(obj.GUIHandle,'Label','  --  Grid');
	            uimenu(obj.GridMenu,'Label','Set grid',...
	                'Callback',@obj.setGridParams);
	            uimenu(obj.GridMenu,'Label','Calculate',...
	                'Callback',@(s,e) obj.calculate());
	            uimenu(obj.GridMenu,'Label','Clear',...
	                'Callback',@(s,e) obj.clearCalculate());

			
			
			end
			
			
			
		
		end

        
    end
    
end
classdef CompareRateGUI < handle
    % CompareRateGUI : GUI to select two time intervals with points and compare the two rates in a third subplot
         
    % $Revision: 1 $    $Date: 2008-15-12 
    % Author: David Eberhard
    
    properties
        DataStore
        FilterList
        
        GUIHandle
        OptionMenu
        GridMenu
        PlotQuality
    end
    
    methods
        
        function obj = CompareRateGUI(dataStore,filterList)
           			
            obj.DataStore = dataStore;
            obj.FilterList = filterList;
            obj.FilterIndexVect = filterList.getSelected();
            
             %Set the properties of the figure window
            CompGUIFigureProperties = {'Position',[ 50 300 600 700 ],'Renderer','painters'};

            % Set the axes properties by hand in the order left plot, top right, bottom
            % right
            
            CompGUIAxesProperties = {...
            	{'pos',[ 0.06 0.15 0.42 0.80],'FontSize',12},...
            	{'pos',[ 0.54 0.15 0.42 0.80],'FontSize',12}};
            
            %CompGUIAxesProperties = {...
            %    {'pos',[ 0.06 0.05 0.88 0.28],'FontSize',12},...
            %    {'pos',[ 0.06 0.37 0.88 0.28],'FontSize',12},...
            %    {'pos',[ 0.06 0.69 0.88 0.28],'FontSize',12}};

            obj.GUIHandle = twolinePanelLayout('Mapseis Rate Compare',...
                CompGUIFigureProperties,CompGUIAxesProperties);

            % Register this view with the model.  The subject of the view uses this
            % function handle to update the view.
            dataStore.subscribe('Comperator',@() updateGUI(obj));

            
            
            
            
            
            
            
            
            % Update the GUI to plot the initial data
            obj.updateGUI();
            
            % Make the GUI visible
			set(obj.MainGUI,'Visible','on');           
           
           
           
           
            
                      
            
            obj.GridMenu=uimenu(gui,'Label','  --  Grid');
            uimenu(obj.GridMenu,'Label','Set grid',...
                'Callback',@obj.setGridParams);
            uimenu(obj.GridMenu,'Label','Calculate',...
                'Callback',@(s,e) obj.calculate());
            uimenu(obj.GridMenu,'Label','Clear',...
                'Callback',@(s,e) obj.clearCalculate());
                
                
        end
        
        
        
     
        
      
        
        
        
    end
    
end
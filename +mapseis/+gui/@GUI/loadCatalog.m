function loadCatalog(obj,filename)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.*;
	import mapseis.datastore.*;
	import mapseis.importfilter.*;
				
	%open load dialog if no filename specified
	if nargin<2
		[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
		
		if isequal(fName,0) || isequal(pName,0)
			disp('No filename selected. ');
			return
			
		else
			filename=fullfile(pName,fName);
	
		end
	     
	end


	%load catalog
	prevData = load(filename);

	
	%Determine what datatyp it is and add necessary filters
	if isfield(prevData,'dataStore')
		% We have loaded a saved session, including filter values
		dataStore = prevData.dataStore;
		
		try
			filterList = prevData.filterList
		catch
			filterList = DefaultFilters(dataStore,obj.ListProxy);
		end
                    	
	elseif isfield(prevData,'dataCols')
		% We have loaded only the data columns, which now need to be turned
		% into an mapseis.datastore.DataStore
		dataStore = mapseis.datastore.DataStore(prevData.dataCols);
		setDefaultUserData(dataStore);
                    
		%create new filterlist
		filterList = DefaultFilters(dataStore,obj.ListProxy);
		elseif isfield(prevData,'a')
		% We have loaded in a ZMAP format file with data in matrix 'a'
		dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
		setDefaultUserData(dataStore)
		
		%create new filterlist
		filterList = DefaultFilters(dataStore,obj.ListProxy);

	else
		error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');
	
	end

	%%Prepare catalogue if Needed
	%check if bufferdata and userdata exists
	try %check wether the datastore is already buffered
    	temp=dataStore.getUserData('MilliSecond');
	catch
		dataStore.bufferDate;
	end
                	
	try %check wether the plot quality is already set
		temp=datStore.getUserData('PlotQuality');
	catch
		dataStore.SetPlotQuality;
	end
	
	%In case the name has not been set
	if isempty(fName) 
		fName = 'noname';
	end	 

	%write filename and path to userdata
	dataStore.setUserData('filename',fName);
	dataStore.setUserData('filepath',filename);


	%%pushit to the commander
	newdata = struct(	'DataName',fName,...
						'Datastore',dataStore,...
						'FilterName',filterList.getName,...
						'Filterlist',filterList);

	%let's push it to the Commander            
	obj.CommanderGUI.pushIt(newdata);
            		
	%get the marked ones back
	keys = obj.CommanderGUI.getMarkedKey;
					
	obj.ShownDataID = keys.ShownDataID;
	obj.ShownFilterID = keys.ShownFilterID;
					
	%make the new loaded catalog the current one
	obj.FilterList=filterList;	
	obj.ParamGUI.FilterList=filterList;
	obj.FilterList.update;
	obj.DataStore = dataStore;

	%reapply the listeners
	relisten(obj);

	% Update the GUI to plot the initial data
	updateGUI(obj);

end
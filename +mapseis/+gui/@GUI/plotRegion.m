function plotRegion(obj,plotAxis)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.plot.*;
	import mapseis.projector.*;
	import mapseis.region.*;
	import mapseis.util.gui.SetImMenu;

	%if it is to external function, directly go to the one
	if obj.ExternRegionPlot & ~isempty(obj.ExternPlotFunction{1})
		obj.ExternPlotFunction{1}(plotAxis);
		return;
	end

	% Get the data to plot from the DataStore
	[inReg,outReg] = getLocations(obj.DataStore,obj.FilterIndexVect);
	regionFilter = obj.FilterList.getByName('Region');
	
	filterRegion = getRegion(regionFilter);
	pRegion = filterRegion{2};
	RegRange=filterRegion{1};

	[selectedMags, unselectedMags] = getMagnitudes(obj.DataStore,obj.FilterIndexVect);
	mags.selected = selectedMags;
	mags.unselected = unselectedMags;
    
     
	%get quality from datastore
	try 
		StoredQuality = obj.DataStore.getUserData('PlotQuality');
	catch 
		obj.DataStore.setUserData('PlotQuality','med');
		StoredQuality = obj.DataStore.getUserData('PlotQuality');		
	end
            
            
	if ~isempty(pRegion)
		if isobject(pRegion)
			rBdry = pRegion.getBoundary();	
		
		else 
			rBdry = pRegion; %circle
			%modify on radius
            %latlim = get(plotAxis,'Ylim');
            %rBdry(4)= rBdry(3)*cos(pi/180*mean(latlim));
		end
        
        tic;	
		PlotRegion(plotAxis,...
			struct(	'inRegion',inReg,...
					'outRegion',outReg,...
					'boundary',rBdry,...
					'plotCoastline',obj.CoastToggle,...
                    'plotBorder',obj.BorderToggle,...
                    'showlegend',obj.LegendToggle,...
                    'drawnpoly',obj.drawnpoly,...
                    'dataStore',obj.DataStore,...
                    'regionFilter',regionFilter,...
                    'PlotType','2Dpolygon',...
                    'FilterRange', RegRange,...
                    'Quality',StoredQuality),mags);
		
		disp(['PlotRegion time: ',num2str(toc)]); 
                    

		%really needed the axes command
		axes(plotAxis)    
        
    	
		%recreate boundaries object if needed
		switch RegRange
			case {'in','out'}
				selection=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
				selection.addNewPositionCallback(@(p) ...
				filterupdater(p,obj.DataStore,regionFilter));
				selection.setColor('r');		
                
				%set the menu point depth slice to 'off'	
				anamenu=get(obj.AnalysisMenu,'Children');
				set(anamenu(5),'Enable','off');
                			
				%add menus to the object
				SetImMenu(obj,selection,regionFilter,[]);
                			
				if obj.AutoProfile
					obj.ListProxy.PrefixQuiet('DepthWindow');
					
					try
						close(obj.DepthWindow.ResultGUI);
					end
						
					try
						clear(obj.DepthWindow);
					end
                
				end
            
    			
			case 'line'
            	%Box around the profile
				hold on 
				handle = drawBox(plotAxis,false,rBdry);
				selection=imline(plotAxis, [rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)]);
				hold off
                			            				
				selection.addNewPositionCallback(@(p) ...
						filterupdater(p,obj.DataStore,regionFilter));
				selection.setColor('r');
                		
             			
				%set the menu point depth slice to 'on'
				anamenu=get(obj.AnalysisMenu,'Children');
				set(anamenu(5),'Enable','on');			
                			
				%add menus to the object
				SetImMenu(obj,selection,regionFilter,handle);


			case 'circle'
				rBdry(3)=regionFilter.Radius;
				if isempty(rBdry(3))
					oldgrid = datastore.getUserData('gridPars');
					rBdry(3) = oldgrid.rad;
					regionFilter.setRadius(rBdry(3));
				end	
            			
				%draw circle
				hold on
				handle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
				selection = impoint(plotAxis,rBdry(1:2));
				hold off
            				
				%selection.setFixedAspectRatioMode(1)
				selection.addNewPositionCallback(@(p) ...
						filterupdater([p(1),p(2),rBdry(3)],...
				obj.DataStore,regionFilter));
            				
				selection.setColor('r');

				anamenu=get(obj.AnalysisMenu,'Children');
				set(anamenu(5),'Enable','off');			
                		
				%add menus to the object
				SetImMenu(obj,selection,regionFilter,handle);
                
				if obj.AutoProfile
					obj.ListProxy.PrefixQuiet('DepthWindow');
					
					try
						close(obj.DepthWindow.ResultGUI);
					end
					
					try
						clear(obj.DepthWindow);
					end
				end
			
                			
			case 'all'
				%set the menu point depth slice to 'off'
				anamenu=get(obj.AnalysisMenu,'Children');
				set(anamenu(5),'Enable','off');		
							
				if obj.AutoProfile
					obj.ListProxy.PrefixQuiet('DepthWindow');

					try
						close(obj.DepthWindow.ResultGUI);
					end
						
					try
						clear(obj.DepthWindow);
					end
				end
                			
		end	
	


	else
		tic;
		%disp(num2str(max(mags)))
		PlotRegion(plotAxis,...
				struct(	'inRegion',inReg,...
						'plotCoastline',obj.CoastToggle,...
						'plotBorder',obj.BorderToggle,...
						'showlegend',obj.LegendToggle,...
						'dataStore',obj.DataStore,...
						'regionFilter',regionFilter,...
						'PlotType','2Dwhole',...
						'Quality',StoredQuality),mags);
		disp(['PlotRegion time: ',num2str(toc)]);

	end


	tic
	
	handleS=[];
	
	if obj.StationToggle
		%plot stations

		%get timelimits in the filterlist
		timefilter=obj.FilterList.getByName('Time');
		timeSelect=timefilter.getSelected;
		thetimes=obj.DataStore.getFields('dateNum',timeSelect);
		minTime=min(thetimes.dateNum);
		maxTime=max(thetimes.dateNum);
		
		axes(plotAxis);
		lonLimi=xlim(plotAxis);
		latLimi=ylim(plotAxis);

		hold on
		StationPloTConf=struct(	'Colors','m',....
								'Size',3,...
								'Marker','s',...
								'NameTag',true,...
								'StartDate',minTime,...
								'EndDate',maxTime);
            	
		[handleS legendentry] = PlotStations(plotAxis,obj.DataStore,StationPloTConf);
		axes(plotAxis);
		hold off;
		xlim(plotAxis,lonLimi);
		ylim(plotAxis,latLimi);
	end
            

	handlePl=[];
	if obj.PlateToggle
		if ~isstruct(obj.PlatePlotSetting)
			obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
											'Ridge',true,...
											'Transform',true,...
											'Trench',true,...
											'RidgeLineStylePreset',1,...
											'TransformLineStylePreset',1,...
											'TrenchLineStylePreset',1,...
											'RidgeColors',2,...
											'TransformColors',3,...
											'TrenchColors',1);
            	
		end
		
		PlotConfig=obj.PlatePlotSetting;


		LineStyles={'normal','Fineline','dotted','Fatline'};
		ColorList={'blue','red','green','black','cyan','magenta'};
		PlotConfig.RidgeLineStylePreset=LineStyles{obj.PlatePlotSetting.RidgeLineStylePreset};
		PlotConfig.TransformLineStylePreset=LineStyles{obj.PlatePlotSetting.TransformLineStylePreset};
		PlotConfig.TrenchLineStylePreset=LineStyles{obj.PlatePlotSetting.TrenchLineStylePreset};
		PlotConfig.RidgeColors=ColorList{obj.PlatePlotSetting.RidgeColors};
		PlotConfig.TransformColors=ColorList{obj.PlatePlotSetting.TransformColors};
		PlotConfig.TrenchColors=ColorList{obj.PlatePlotSetting.TrenchColors};
			
		PlotConfig.X_Axis_Limit='Longitude';
		PlotConfig.Y_Axis_Limit='Latitude';
				      
		xlimiter=get(plotAxis,'xlim');
		ylimiter=get(plotAxis,'ylim');
            	
		hold on
		[handlePl legendentry] = PlotPlateBoundaries(plotAxis,PlotConfig); 
		xlim(xlimiter);
		ylim(ylimiter);
		latlim = get(plotAxis,'Ylim');
		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
		
		hold off;
	end


	handleM=[];

	if obj.MarkEQ
		axes(plotAxis);
		lonLimi=xlim(plotAxis);
		latLimi=ylim(plotAxis);
		
		hold on
		[handleM legendentry]=PlotLargeEQ(plotAxis,obj.DataStore,obj.FilterIndexVect,'map');
		axes(plotAxis);
		hold off;
            
		xlim(plotAxis,lonLimi);
		ylim(plotAxis,latLimi);
	end


	%try to correct the legend
	%so much to do just for updating the legend.
	%some commands might be not needed, but I'm not
	%sure and the documentation about this is lousy
           
	legend('location','NorthEastOutside');
	legend('location','NorthEast');

	%find legend
	leg=findobj(obj.MainGUI,'Type','axes','Tag','legend');
	legdata=get(leg,'UserData');

	if ~isempty(handlePl)&~isempty(legdata)
		Names={'Ridge','Transform','Trench'};


		for i=1:3
			pos=0;

			if ~isempty(handlePl{i})
				pos=handlePl{i}==legdata.handles;
			end
			
			if any(pos)
				legdata.lstrings{pos}=Names{i};

			else
				legdata.handles(end+1)=handlePl{i};
				legdata.lstrings{end+1}=Names{i};
				set(leg,'UserData',legdata);
			end
			
		end


		set(leg,'UserData',legdata);
		try 	
			set(leg,'String',legdata.lstrings);
        end	

	end


	if ~isempty(handleS)&~isempty(legdata)
		pos=handleS==legdata.handles;
		
		if any(pos)
			legdata.lstrings{pos}='Seismic Stations';
			set(leg,'UserData',legdata);
			
			try 
				set(leg,'String',legdata.lstrings);
			end	
            
		else
			legdata.handles(end+1)=handleS;
			legdata.lstrings{end+1}='Seismic Stations';
			set(leg,'UserData',legdata);
		end
	end


	if ~isempty(handleM)&~isempty(legdata)
		pos=handleM==legdata.handles;

		if any(pos)
			legdata.lstrings{pos}='Large Eq';
			set(leg,'UserData',legdata);
            
			try 
				set(leg,'String',legdata.lstrings);
			end	
		
		else
			legdata.handles(end+1)=handleM;
			legdata.lstrings{end+1}='LargeEq';
			set(leg,'UserData',legdata);
		end

	end


	disp(['Addon time: ',num2str(toc)]);
	
	%in case of autoprofile
	if strcmp(RegRange,'line')
		if obj.AutoProfile
			obj.startDepthSlice;
		end
	end
             
             
end
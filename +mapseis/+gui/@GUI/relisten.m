function relisten(obj)
	%Allows to reset all listeneres over the list proxy and reconnect them to the current filterlist 
	%and datastore		


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	import mapseis.gui.*;
	
	%suicide gui
	obj.ListProxy.PrefixQuiet('main');
	clf(obj.MainGUI);
	obj.rebuildGUI;	
	
	%mute the internal listeners of the filterlist
	obj.FilterList.MuteList(obj.ListProxy);
	
	%from the rebuild part of this gui
	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
	
	%reapply listeners
	obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');
	
	obj.FilterList.reInitList(obj.ListProxy);
			
	%delete paramGUI and rebuild
	obj.ParamGUI.silence;
	close(obj.ParamGUI.mainGUI);
	clear obj.ParamGUI;
	obj.ParamGUI=ParamGUI(obj.DataStore,obj.FilterList,obj.ListProxy);


end
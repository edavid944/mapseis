function setDeveloper(obj,doWhat)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	Menu2Check= findobj(obj.OptionMenu,'Label','Developer Mode');
	TheInvisible=findobj(obj.MainGUI,'Label','Dev-Utils');
	
	switch doWhat
		case 'toggle'
			if isempty(obj.DeveloperMode)
				obj.DeveloperMode=false;
			end
				obj.DeveloperMode=~obj.DeveloperMode;
			
		case 'init'
			obj.DeveloperMode=false;
		
		case 'reinit'
			%do nothing
	
	end
	
	set(Menu2Check, 'Checked', 'off');
	set(TheInvisible,'Visible','off');
	
	%change check
	if obj.DeveloperMode
		set(Menu2Check, 'Checked', 'on');
		set(TheInvisible,'Visible','on');
	end
	
end
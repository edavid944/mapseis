function CallCalculation(obj,CalcHandle)
	%This function acts as man in the middle, and writes the recieved objects into
	%the internal storage
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	TheObj=CalcHandle(obj,'-start');
	
	if isfield(TheObj,'CalcWrapper')
		obj.Wrapper = TheObj.CalcWrapper;
		obj.ResGUI = TheObj.ResultGUI;
		obj.ParGUI = TheObj.CalcParamGUI;
	
	else
		%do nothing at the moment
		obj.Wrapper = TheObj;
		obj.ResGUI = [];
		obj.ParGUI = [];

	end

end
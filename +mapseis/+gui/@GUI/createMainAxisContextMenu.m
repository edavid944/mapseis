function createMainAxisContextMenu(obj,parentAxis)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.plot.SetRegion;
	
	% Create the contextmenu with parent the main figure
	cmenu = uicontextmenu('Parent',obj.MainGUI);

	% Now make the menu be associated with the correct axis
	set(obj.MainGUI,'UIContextMenu',cmenu);
	set(parentAxis,'UIContextMenu',cmenu);

	% Add uimenu items for the different region selections
	uimenu(cmenu,'Label','Select all',...
			'Callback',@(s,e) SetRegionGlove(obj,'all'));
	uimenu(cmenu,'Label','Select inside',...
			'Callback',@(s,e) SetRegionGlove(obj,'in'));
	uimenu(cmenu,'Label','Select Profile',...
			'Callback',@(s,e) SetRegionGlove(obj,'polygon'));
	uimenu(cmenu,'Label','Select outside',...
			'Callback',@(s,e) SetRegionGlove(obj,'out'));
	uimenu(cmenu,'Label','Select circular region',...
			'Callback',@(s,e) SetRegionGlove(obj,'circle'));
	uimenu(cmenu,'Label','Import region coordinates',...
			'Callback',@(s,e) SetRegionGlove(obj,'import'));
	uimenu(cmenu,'Label','Redraw Map',...
			'Callback',@(s,e) updateGUI(obj));
	uimenu(cmenu,'Label','Plot this frame in new figure ',...
			'Callback',@(s,e) plotInNewFigure(obj,...
			'Event Locations',@plotRegion));

end
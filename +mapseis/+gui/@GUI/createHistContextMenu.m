function createHistContextMenu(obj,parentAxis)
	% Create a context menu for the histogram in the top right subplot,
	% that selects the histogram type


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	% Create the contextmenu with parent the main figure
	cmenu = uicontextmenu('Parent',obj.MainGUI);

	% Now make the menu be associated with the correct axis
	set(obj.MainGUI,'UIContextMenu',cmenu);
	set(parentAxis,'UIContextMenu',cmenu);

	% Add uimenu items for the different histogram types
	uimenu(cmenu,'Label','Magnitude','Callback',...
				@(s,e) setHistType(obj,'mag'));
	uimenu(cmenu,'Label','Depth','Callback',...
				@(s,e) setHistType(obj,'depth'));
	uimenu(cmenu,'Label','Hour','Callback',...
				@(s,e) setHistType(obj,'hour'));
	uimenu(cmenu,'Label','Cumulative FMD','Callback',...
				@(s,e) setHistType(obj,'FMD'));
	uimenu(cmenu,'Label','Plot this frame in new window ', 'Callback',...
				@(s,e) plotInNewFigure(obj,'Histogram',@plotHist));


end
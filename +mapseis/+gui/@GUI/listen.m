function listen(obj)
	%reapply listeners


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.gui.*;
	
	%from the rebuild part of this gui
	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
	obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');
	
	obj.FilterList.reInitList(obj.ListProxy);

end
function setqual(obj,qual)
	%sets the quality of the plot 
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%chk just gets the userdata parameter and sets the 'checked' to the menu
	dataStore = obj.DataStore;
	
	Menu2Check(3) = findobj(obj.OptionMenu,'Label','Zmap style');
	Menu2Check(2) = findobj(obj.OptionMenu,'Label','Medium Quality Plot');
	Menu2Check(1) = findobj(obj.OptionMenu,'Label','High Quality Plot');
	
	%uncheck all menupoints
	set(Menu2Check(1), 'Checked', 'off');
	set(Menu2Check(2), 'Checked', 'off');
	set(Menu2Check(3), 'Checked', 'off');
	
	switch qual
		case 'low'
			dataStore.setUserData('PlotQuality','low');
			set(Menu2Check(3), 'Checked', 'on');
			obj.updateGUI;
		
		case 'med'
			dataStore.setUserData('PlotQuality','med');
			set(Menu2Check(2), 'Checked', 'on');
			obj.updateGUI;
		
		case 'hi'
			dataStore.setUserData('PlotQuality','hi');
			set(Menu2Check(1), 'Checked', 'on');
			obj.updateGUI;
		
		case 'chk'
			try 
				StoredQuality = dataStore.getUserData('PlotQuality');
			catch 
				dataStore.setUserData('PlotQuality','med');
				StoredQuality = dataStore.getUserData('PlotQuality');		
			end
			
			switch StoredQuality
				case 'low'
					sel=3;
				case 'med'
					sel=2;
				case 'hi'
					sel=1;
			end		
			
			set(Menu2Check(sel), 'Checked', 'on');
	
	end
end
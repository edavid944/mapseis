function functable = CalcSniffer(homepath)
	% searches all Calculations in the path ./+mapseis/+calcLoaders and returns them
	% as function handles.


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	import mapseis.util.importfilter.*;
	import mapseis.importfilter.*;
	
	%get all necessary reg expressionpatterns
	rp = regexpPatternCatalog();
	
	%where are I now
	curdir=pwd;
	fs=filesep;
	
	cd([homepath,fs,'+mapseis',fs,'+calcLoaders']);

	if ispc
		[sta res] = dos(['dir /b *.m']);
	else
		[sta res] = unix(['ls -1 *.m']);
	end
	
	lines = regexp(res,rp.line,'match');
	
	for i=1:numel(lines)	
		[pathStr,nameStr{i},extStr] = fileparts(lines{i});
		%this has to be done for a certain object structur
		nameStr{i} = ['mapseis.calcLoaders.',nameStr{i}];
		
	end
	
	functable=cellfun(@str2func, nameStr, ...
			'UniformOutput', false);

	%get back to original dir
	cd(curdir);

end
function saveData(obj)
	% Save the datastore and filter list struct to a .mat file
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	[fName,pName] = uiputfile('*.mat','Enter save file name...');
	
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected.  Data not saved');
		return

	else
		dataStore = obj.DataStore; 
		filterList = obj.FilterList; 
		filterList.PackIt;
		save(fullfile(pName,fName),'dataStore','filterList');
		msgbox('Catalog & Filterlist saved')

	end

end
function startMapAnalysis(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.datastore.*;
	import mapseis.gui.*;
	import mapseis.projector.*;
	import mapseis.calc.*;
	import mapseis.plot.SetRegion;

	if isempty(obj.CalcGUI)
		%set the region filter to all if not set otherwise
		regionFilter = obj.FilterList.getByName('Region');
		filterRegion = getRegion(regionFilter);
		pRegion = filterRegion{2};

		if isempty(pRegion)
			SetRegion(regionFilter,'all',obj.DataStore);
		end

		bMeanMagCalcObject = mapseis.calc.Calculation(...
			@mapseis.calc.CalcBMeanMag,@mapseis.projector.getMagnitudes,{},...
			'Name','b Mean Mag');

		obj.CalcGUI=CalcGridGUI(obj.DataStore,'BMeanMagView',...
			bMeanMagCalcObject,@(x) x.meanMag,obj.FilterList);

		obj.ListProxy.listen(obj.FilterList,'Update',...
			@(s,e) obj.CalcGUI.updateGUI());	    

	else
		obj.CalcGUI.rebuildGUI(obj.DataStore, obj.FilterList);
			obj.ListProxy.listen(obj.FilterList,'Update',...
			@(s,e) obj.CalcGUI.updateGUI());	
	
	end

end
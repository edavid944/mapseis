function setMenu(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	g = obj.MainGUI;
	
	obj.OptionMenu = uimenu(g,'Label','- Plot Option');
	
	uimenu(obj.OptionMenu,'Label','Zmap style',...
		'Callback',@(s,e) obj.setqual('low'));
	uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
		'Callback',@(s,e) obj.setqual('med'));
	uimenu(obj.OptionMenu,'Label','High Quality Plot',...
		'Callback',@(s,e) obj.setqual('hi'));
	uimenu(obj.OptionMenu,'Label','Legend on/off','Separator','on',...
		'Callback',@(s,e) obj.legendary('set'));
	
	uimenu(obj.OptionMenu,'Label','Show Coastline','Separator','on',...
		'Callback',@(s,e) obj.setparam('Show Coastline','CoastToggle'));    
	uimenu(obj.OptionMenu,'Label','Show Internal Borders',...
		'Callback',@(s,e) obj.setparam('Show Internal Borders','BorderToggle'));    
	uimenu(obj.OptionMenu,'Label','Show Seismic Stations',...
		'Callback',@(s,e) obj.setparam('Show Seismic Stations','StationToggle'));  
	uimenu(obj.OptionMenu,'Label','Mark Largest Earthquakes',...
		'Callback',@(s,e) obj.setparam('Mark Largest Earthquakes','MarkEQ'));  
	uimenu(obj.OptionMenu,'Label','Draw Plate Boundaries',...
		'Callback',@(s,e) obj.setparam('Draw Plate Boundaries','PlateToggle'));  
	uimenu(obj.OptionMenu,'Label','Automatic DepthProfile','Separator','on',...
		'Callback',@(s,e) obj.setparam('Automatic DepthProfile','AutoProfile'));
	uimenu(obj.OptionMenu,'Label','Edit Plate Boundary Plot Config',...
		'Callback',@(s,e) obj.setPlatePlotConfig);    
	uimenu(obj.OptionMenu,'Label','Developer Mode',...
		'Callback',@(s,e) obj.setDeveloper('toggle'));
	
	obj.setqual('chk');
	
	obj.DataMenu = uimenu(g,'Label','- Data');
	
	uimenu(obj.DataMenu,'Label','Save as MapSeis Session',...
		'Callback',@(s,e) obj.saveData);
	uimenu(obj.DataMenu,'Label','Cut Selected Data (cut at filters)...',...
		'Callback',@(s,e) obj.CutSelectedData);
	uimenu(obj.DataMenu,'Label','Load MapSeis session *.mat file ..',...
		'Callback',@(s,e) obj.loadCatalog);
	uimenu(obj.DataMenu,'Label','Load ZMAP *.mat file ..',...
		'Callback',@(s,e) obj.loadCatalog);
	uimenu(obj.DataMenu,'Label','Add Station List ..',...
		'Callback',@(s,e) obj.AddStationList);
	
	obj.AnalysisMenu = uimenu(g,'Label','- Analysis','ForegroundColor','r');
	
	%uimenu(obj.AnalysisMenu,'Label','Map view analysis',...
	%	'Callback',@(s,e) obj.startMapAnalysis);
	uimenu(obj.AnalysisMenu,'Label','Depth Profile','Enable','off',...
		'Callback',@(s,e) obj.startDepthSlice);
	uimenu(obj.AnalysisMenu,'Label','3D view',...
		'Callback',@(s,e) obj.startThirdDimension);
	uimenu(obj.AnalysisMenu,'Label','Open Parameter Window','Enable','on',...
		'Callback',@(s,e) obj.openParamWindow);
	uimenu(obj.AnalysisMenu,'Label','Set Profile Width',...
		'Callback',@(s,e) obj.setProfWidth);
	uimenu(obj.AnalysisMenu,'Label','Set Circle Radius',...
		'Callback',@(s,e) obj.setCircleRadius);

end
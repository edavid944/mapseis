function wantedaxis = reTag(obj,whichone)
	%This function can help in case of lost 'axis_left' tags errors
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%get all childrens of the main gui
	gracomp = get(obj.MainGUI, 'Children');
	
	%find axis and return axis
	wantedaxis = findobj(gracomp,'Type','axes','Tag','');
		
	%correct 
	switch whichone
		case 'left'
			set(wantedaxis,'Tag','axis_left');
		
		case 'right_top'
			set(wantedaxis,'Tag','axis_top_right');
			
		case 'right_bottom'
			set(wantedaxis,'Tag','axis_bottom_right');

	end		

end
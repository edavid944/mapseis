function plotInNewFigure(obj,figTitleStr,plotFun)
	% Plot the region plot in a new figure for printing.  The new
	% figure will be decoupled from the underlying data.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.gui.*;

	% Create a new figure
	newGUI = onePanelLayout(figTitleStr);

	% Get the main plot axis
	plotAxis = findobj(newGUI,'Tag','axis');

	% Plot the region plot
	plotFun(obj,plotAxis)
	set(newGUI,'Visible','on');

end
function SetRegionGlove(obj,WhichOne)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.plot.SetRegion;
	import mapseis.gui.PointImport_newPoly;


	regionFilter = obj.FilterList.getByName('Region');
	dataStore = obj.DataStore;

	if strcmp(WhichOne,'import')
		%special import routine. It has to be added here to get the profile window
		%working correctly, appart from that it would be better to address it directly
		%and not over the glove
		FilterType = PointImport_newPoly(obj,dataStore,regionFilter);
		
		%The import function could be changed to an internal GUI function, but I think
		%sub function in it and the fact the GUI class is large enough and also that the
		%similar import function (without the new poly part) is already external speak 
		%against it, at least for the moment. 
		
	else
		SetRegion(obj,regionFilter,WhichOne,dataStore);
	end

	
	if obj.AutoProfile
		switch  WhichOne
			case {'in','out'}
				obj.ListProxy.PrefixQuiet('DepthWindow');
				
				try
					close(obj.DepthWindow.ResultGUI);
				end

				try
					clear(obj.DepthWindow);
				end

    			
            case 'line'
				obj.startDepthSlice;

                            		                			
			case 'circle'
				obj.ListProxy.PrefixQuiet('DepthWindow');
				
				try
					close(obj.DepthWindow.ResultGUI);
				end
				
				try
					clear(obj.DepthWindow);
				end
            
            			
			case 'all'
				obj.ListProxy.PrefixQuiet('DepthWindow');

				try
					close(obj.DepthWindow.ResultGUI);
				end

				try
					clear(obj.DepthWindow);
				end


		end

	end

end
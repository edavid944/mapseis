function getparam(obj,mlabel,guipar)
	%sets the plot parameter and writes it to the datastore and updates the gui


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	dataStore = obj.DataStore;
	Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
	
	%get pararmeter from datastore
	try 
		cstate = dataStore.getUserData(guipar);
	catch 
		dataStore.setUserData(guipar,true);
		cstate = dataStore.getUserData(guipar);	
	end				  
	
	obj.(guipar) = cstate;
	
	set(Menu2Check, 'Checked', 'off');
	
	if cstate
		set(Menu2Check, 'Checked', 'on');
	end

end
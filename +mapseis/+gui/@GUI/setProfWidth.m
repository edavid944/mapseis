function setProfWidth(obj)

	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.gui.setProfileWidthGUI;
	import mapseis.region.filterupdater;
	
	%check if a profile is already existing 
	regionFilter = obj.FilterList.getByName('Region');
	setProfileWidthGUI(obj.DataStore,regionFilter);
	regparams =regionFilter.getDepthRegion;
	
	if strcmp(regparams{1},'line')
		pRegion = regparams{2};
		rBdry = pRegion.getBoundary();
		pos = [rBdry(1,:);rBdry(4,:)];

		filterupdater(pos,obj.DataStore,regionFilter);
		regionFilter.execute(obj.DataStore);
		obj.updateGUI;
	end


end
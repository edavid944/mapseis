function GuiRebuildSeq(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.gui.*;
	
	%get position of param gui and store it
	oldPosition=get(obj.ParamGUI.mainGUI,'Position');
	obj.ParamGUI.oldPosition = oldPosition;
	%The storage inside the ParamGUI is currently not really needed as the
	%whole object is replace, but in future version this may change, so it is
	%done here already and the ParamGUI would also already know what to do with
	%it
	
	%close(obj.MainGUI);
	clf(obj.MainGUI);
	obj.rebuildGUI;	
	
	close(obj.ParamGUI.mainGUI);
	clear obj.ParamGUI;
	obj.ParamGUI=ParamGUI(obj.DataStore,obj.FilterList,obj.ListProxy,oldPosition);
	
end
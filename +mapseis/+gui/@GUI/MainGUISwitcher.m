function MainGUISwitcher(obj,newdata)
	%input is a cell structur with either DataStore, Filterlist or DataStore and Filterlist
	%The function replaces the existing datastore/filterlist with the new one and updates the gui


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	%silence all listeners
	obj.quiet;
	
	if isfield(newdata,'Datastore')
		obj.DataStore = newdata.Datastore;
		obj.ShownDataID = newdata.ShownDataID;
	end
	
	if isfield(newdata,'Filterlist')
		obj.FilterList = newdata.Filterlist;
		obj.ShownFilterID = newdata.ShownFilterID;
	end
	
	%set the filterlist to the new data
	obj.FilterList.changeData(obj.DataStore);
	obj.ParamGUI.FilterList=obj.FilterList;
	obj.FilterList.update;
	
	obj.GuiRebuildSeq;
	
	obj.listen;
	    	
	% Update the GUI to plot the initial data
	updateGUI(obj);

end
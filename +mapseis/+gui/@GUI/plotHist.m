function plotHist(obj,plotAxis)
	% Plot the appropriate histogram depending on histType parameter


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.plot.*;
    import mapseis.projector.*;	
	
	switch obj.HistType
		case 'mag'
			PlotMagHist(plotAxis,...
					getMagnitudes(obj.DataStore,obj.FilterIndexVect));
		
		case 'depth'
			PlotDepthHist(plotAxis,...
					getDepths(obj.DataStore,obj.FilterIndexVect));
		
		case 'hour'
			PlotHourHist(plotAxis,...
					getHours(obj.DataStore,obj.FilterIndexVect));
		
		case 'FMD'
			PlotFMD(plotAxis,...
					getMagnitudes(obj.DataStore,obj.FilterIndexVect));
		
		case 'EXTERN'			
			if ~isempty(obj.ExternPlotFunction{2})
				obj.ExternPlotFunction{2}(plotAxis);
			end
			
		otherwise

	end
        
end
function updateGUI(obj,force)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if nargin<2
    	force=false;
	end
            
	%if less time is used than a 0.25s don't do an update
	MinUpdateTime= 1/(3600*24*4);
            
	if  (now-obj.LastGraphicUpdate)<=MinUpdateTime&~force
		return
	end
            
           
            
	if obj.AutoUpdate|force
        	
		% Extract the GUI handle
		%disp(['update graphic ' num2str(obj.debugtag)]);
		g = obj.MainGUI;
		    
		if obj.FilterList.Packed&~isempty(obj.DataStore)
			obj.FilterList.changeData(obj.DataStore);
		end	
		    
		    
		if obj.AutoUpdate&force
			obj.FilterList.updateNoEvent();
		end	
		    
		obj.FilterIndexVect = obj.FilterList.getSelected();
		% Update the GUI
				
		plotAxis = findobj(g,'Tag','axis_left');	
		%in case of an error retag
		if isempty(plotAxis)
			disp('Try to retag left');
			plotAxis = obj.reTag('left');
		end
		    
		obj.plotRegion(plotAxis);
		set(plotAxis,'Tag','axis_left');
				
				
		plotAxis = findobj(g,'Tag','axis_top_right');
		%in case of an error retag
		if isempty(plotAxis)
			disp('Try to retag right top');
			plotAxis = obj.reTag('right_top');
		end

		obj.plotHist(plotAxis);
		set(plotAxis,'Tag','axis_top_right');
	
	
		plotAxis = findobj(g,'Tag','axis_bottom_right');
		%in case of an error retag	
		if isempty(plotAxis)
			disp('Try to retag right bottom');
			plotAxis = obj.reTag('right_bottom');
		end
		    
		obj.plotTime(plotAxis)
		set(plotAxis,'Tag','axis_bottom_right');
		    
		notify(obj,'MainWindowUpdate');
		    
	end
		
	%update last update time
	obj.LastGraphicUpdate=now;
		
end
function createCalcMenu(obj)
	%This function scans the folder calcLoaders in the mapseis directory and 
	%adds all functions to the menu
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.emptier;
	
	%Reset Menu
	obj.CalculationMenus={};
	
	%get loaders
	functable = mapseis.gui.GUI.CalcSniffer(obj.MapSeisFolder);
	
	%get the Names and Categories of the the loaders
	for i=1:numel(functable)
		Descript=functable{i}([],'-description');
		CartName{i,1}=Descript.Category;
		CartName{i,2}=Descript.displayname;
		CartName{i,3}=Descript.description;
		CartName{i,4}=Descript.SubCategory;
	end
	
	%exclude the Templates
	DoNotUse=strcmp(CartName(:,1),'Template');
	CartName=CartName(~DoNotUse,:);
	usedFunction=functable(~DoNotUse);
	%get the different Categries
	UniqueCats=unique(CartName(:,1));
	
	g = obj.MainGUI;
	
	%Build the menu entries
	for i=1:numel(UniqueCats)
		
		%TopMenu
		obj.CalculationMenus{i} = uimenu(g,'Label',UniqueCats{i});
		
		%The single menu entries
		InCategory=strcmp(CartName(:,1),UniqueCats{i});
		SubCats=CartName(InCategory,4);
		NamesInCat=CartName(InCategory,2);
		CategoryFunc=usedFunction(InCategory);
		
		%get all entries with out subcategory
		NoSub=logical(emptier(SubCats));
		%|strcmp(SubCats,'none');
		NoSubName=NamesInCat(NoSub);
		NoSubFunc=CategoryFunc(NoSub);
		
		
		%No the ones with subcategories
		inSubName=NamesInCat(~NoSub);
		inSubFunc=CategoryFunc(~NoSub);
		inSubSub=SubCats(~NoSub);
		
		uniSub=unique(inSubSub);
		for j=1:numel(uniSub)
			submenu=uimenu(obj.CalculationMenus{i},'Label',uniSub{j});
			inTheSub=strcmp(inSubSub,uniSub{j});
			TheNames=inSubName(inTheSub);
			TheFunc=inSubFunc(inTheSub);
							
			for k=1:numel(TheNames)
				uimenu(submenu,'Label',TheNames{k},...
					'Callback',@(s,e) obj.CallCalculation(TheFunc{k}));
			end
		
		end
		
		
		%Add the entries without subCategory
		for j=1:numel(NoSubName)
			uimenu(obj.CalculationMenus{i},'Label',NoSubName{j},...
				'Callback',@(s,e) obj.CallCalculation(NoSubFunc{j}));
		end	

	end		

end
function setPlatePlotConfig(obj)
	%sets the plot config for the plate boundary plot


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if ~isstruct(obj.PlatePlotSetting)
		obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
										'Ridge',true,...
										'Transform',true,...
										'Trench',true,...
										'RidgeLineStylePreset',1,...
										'TransformLineStylePreset',1,...
										'TrenchLineStylePreset',1,...
										'RidgeColors',2,...
										'TransformColors',3,...
										'TrenchColors',1);
	end
	
	%build gui to enter plate config options
	LineStyles={'normal','Fineline','dotted','Fatline'};
	ColorList={'blue','red','green','black','cyan','magenta'}; 
	
	Title = 'Select Plot Config';
	Prompt={	'Plot Ridges:', 'Ridge';...
				'Ridge Color:','RidgeColors';...
				'Ridge LineStyle:','RidgeLineStylePreset';...
				'Plot Transforms:', 'Transform';...
				'Transforms Color:','TransformColors';...
				'Transforms LineStyle:','TransformLineStylePreset';...
				'Plot Trenches:', 'Trench';...
				'Trenches Color:','TrenchColors';...
				'Trenches LineStyle:','TrenchLineStylePreset'};			
	
	%Ridge
	Formats2(1,1).type='check';		
	
	Formats2(1,2).type='list';
	Formats2(1,2).style='popupmenu';
	Formats2(1,2).items=ColorList;
	
	Formats2(1,3).type='list';
	Formats2(1,3).style='popupmenu';
	Formats2(1,3).items=LineStyles;
	
	%Transform
	Formats2(2,1).type='check';		
	
	Formats2(2,2).type='list';
	Formats2(2,2).style='popupmenu';
	Formats2(2,2).items=ColorList;
	
	Formats2(2,3).type='list';
	Formats2(2,3).style='popupmenu';
	Formats2(2,3).items=LineStyles;
	
	%Trench
	Formats2(3,1).type='check';		
	
	Formats2(3,2).type='list';
	Formats2(3,2).style='popupmenu';
	Formats2(3,2).items=ColorList;
	
	Formats2(3,3).type='list';
	Formats2(3,3).style='popupmenu';
	Formats2(3,3).items=LineStyles;
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	
	
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats2,obj.PlatePlotSetting,Options); 	
	
	if Canceled~=1
		obj.PlatePlotSetting=NewParameter;
		obj.updateGUI;
	end	

end
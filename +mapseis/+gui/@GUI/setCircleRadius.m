function setCircleRadius(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.gui.setCircleRadiusGUI;
	import mapseis.region.filterupdater;
	
	
	
	%check if a profile is already existing 
	regionFilter = obj.FilterList.getByName('Region');
	
	setCircleRadiusGUI(obj.DataStore,regionFilter);
	
	regparams =regionFilter.getRegion;
	
	if strcmp(regparams{1},'circle');
		%reload the radius from the datastore	
		%datagrid = obj.DataStore.getUserData('gridPars');
		rad=regionFilter.Radius;			
		
		%rebuild the region filter
		pRegion = regparams{2};
		filterupdater(pRegion,obj.DataStore,regionFilter);
		regionFilter.execute(obj.DataStore);
		obj.updateGUI;
	end


end
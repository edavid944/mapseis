function setCircle(obj)
	% Set the selection region by using a circle defined by radius
	% value
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.region.Region;
	import mapseis.util.Ginput;
	% Define the circle boundary
	thetaVals = linspace(0,2*pi,101)';
	circleBoundary = obj.SelectRadius*[cos(thetaVals) sin(thetaVals)];
	
	% Get the centre of the circle
	centreOffset = Ginput(1);

	% Add the centre offset to the circle
	cicleBoundary = circleBoundary + ...
		repmat(centreOffset,numel(circleBoundary(:,1)),1);
	pRegion = Region(cicleBoundary);

	% Get the region filter
	regionFilter = obj.FilterList.getByName('Region');
	regionFilter.setRegion('in',pRegion);
	
	% Execute the filter
	regionFilter.execute(obj.DataStore);

end
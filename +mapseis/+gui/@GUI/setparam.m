function setparam(obj,mlabel,guipar)
	%sets the plot parameter and writes it to the datastore and updates the gui
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	dataStore = obj.DataStore;
	Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
	
	%get pararmeter from gui
	cstate=obj.(guipar);
	
	set(Menu2Check, 'Checked', 'off');
	
	%change the to the opposite
	cstate=~cstate;
	
	%write into gui and datastore	
	obj.(guipar)=cstate;
	dataStore.setUserData(guipar,cstate);
	
	%change check
	if cstate
		set(Menu2Check, 'Checked', 'on');
	end

	obj.updateGUI;

end
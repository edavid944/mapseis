function startThirdDimension(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.datastore.*;
	import mapseis.gui.*;
	import mapseis.projector.*;
	import mapseis.calc.*;
	import mapseis.plot.SetRegion;
	
	
	if ~isempty(obj.ThirdDimension)
		try
			obj.ThirdDimension.Rebuild;
		catch
		
		disp('Listeners 3DWindow stopped')
		obj.ListProxy.PrefixQuiet('3DWindow');
		
		
		
		try
			close(obj.ThirdDimension.ResultGUI);
		end
		
		try
			clear(obj.ThirdDimension)
		end
			obj.ThirdDimension=ThirdSpace(obj.ListProxy,obj.CommanderGUI,obj,'light');
		end	

	else
		obj.ThirdDimension=ThirdSpace(obj.ListProxy,obj.CommanderGUI,obj,'light');
	
	end


end
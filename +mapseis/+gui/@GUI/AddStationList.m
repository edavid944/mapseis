function AddStationList(obj)
	%add a stationlist to the datastore
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.special_import.importStationList;
	
	[fName,pName] = uigetfile('*.txt','Enter Stationlist *.txt file name...');
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected. ');
		return
	else
		filename=fullfile(pName,fName);

	end
	
	try	
		StationList=importStationList(filename,'-file');
	catch
		disp('Corrupt Stationlist file')
		return
	end
	
	%add to datastore
	obj.DataStore.setUserData('StationList',StationList);

end
classdef GUI < handle
	% GUI : GUI frontend for the MapSeis application
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	properties
		MapSeisFolder
		DataStore
		CurrentDataID
		ShownDataID
		CurrentFilterID
		ShownFilterID
		FilterList
		FilterIndexVect
		MainGUIFigureProperties
		MainGUIAxesProperties
		MainGUI
		SelectRadius
		HistType
		TimeType
		OptionMenu
		DataMenu
		AnalysisMenu
		CalculationMenus
		CalcGUI
		CalcDepthGUI
		ParamGUI
		CommanderGUI
		ListProxy
		debugtag
		LegendToggle
		CoastToggle
		BorderToggle
		StationToggle
		PlateToggle
		MarkEQ
		drawnpoly
		Wrapper
		ResGUI
		ParGUI
		ParallelMode
		GUIPlugIns
		PlugInMemory
		MainCustomPlot
		TRCustomPlot
		BRCustomPlot
		AutoUpdate
		DeveloperMode
		DepthWindow
		AutoProfile
		ThirdDimension
		PlatePlotSetting
		LastGraphicUpdate
		ExternPlotFunction
		ExternRegionPlot
	end
	
	events
		MainWindowUpdate
	end
	
	
	methods
		function obj=GUI(dataStore,filterList,ListProxy)
			import mapseis.gui.*;
			import mapseis.util.gui.*;
			import mapseis.datastore.*;
			
			% GUI constructor
			% Create the figure window that contains the GUI.  Use a three panel layout
			% consisting of the main axis in the left hand side of the window and two
			% subplots on the right hand side
			
			%let the random generator spin a bit and set it to the clock
			%older matlab version do need another command than newer versions
			if ~verLessThan('matlab', '7.13.0')
				disp('Use new random setup stream')
				RandStream.setGlobalStream ...
				(RandStream('mt19937ar','seed',sum(100*clock)));
			
			else
				disp('Use old random setup stream')	
				RandStream.setDefaultStream ...
				(RandStream('mt19937ar','seed',sum(100*clock)));
				
			end
			
			obj.MapSeisFolder=pwd;
			
			%Allow to use Hashes (there are recognized by the fact that they are cell arrays)
			DataHashLoad=false;
			FilterHashLoad=false;
			
			if iscell(dataStore)
				DataHash=dataStore;
				dataStore=DataHash{2,1};
				DataHashLoad=true;
			end
			
			if iscell(filterList)
				FilterHash=filterList;
				filterList=FilterHash{2,1};
				FilterHashLoad=true;
			end
			
			% Store the handles of the DataStore and FilterList and create the hash for additional catalogs
			obj.DataStore = dataStore;
			obj.FilterList = filterList;
			obj.ShownDataID = [];
			obj.ShownFilterID = [];
			
			if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
				obj.FilterIndexVect = filterList.getSelected();
			end
			
			obj.ListProxy = ListProxy;
			obj.CalcGUI = [];
			obj.CalcDepthGUI = [];
			obj.ParamGUI = [];
			obj.CommanderGUI=[];
			obj.debugtag=rand*100;
			obj.ParallelMode=false;
			obj.AutoUpdate=true;
			obj.DepthWindow=[];
			obj.LastGraphicUpdate=now;
			
			obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
											'Ridge',true,...
											'Transform',true,...
											'Trench',true,...
											'RidgeLineStylePreset',1,...
											'TransformLineStylePreset',1,...
											'TrenchLineStylePreset',1,...
											'RidgeColors',2,...
											'TransformColors',3,...
											'TrenchColors',1);

			%get the plugs
			obj.InitGUIPlugs;
						
			% Set the properties of the figure window
			DefaultPos=[ 50 300 1100 690 ];
			
			%change screensize if needed
			MonSize=get(0,'ScreenSize');
			if 0.75*MonSize(3)<=DefaultPos(3)|0.75*MonSize(4)<=DefaultPos(4);
				Screener=[50 150 MonSize(3)*0.75 MonSize(4)*0.75-150];	
			else
				Screener=DefaultPos;
			end

			mainGUIFigureProperties = {'Position',Screener,'Renderer','painters'};
			
			% Set the axes properties by hand in the order left plot, top right, bottom
			% right
			mainGUIAxesProperties = {...
			{'pos',[ 0.06 0.1 0.55 0.85],'FontSize',12},...
			{'pos',[ 0.7 0.58 0.25 0.37],'FontSize',12},...
			{'pos',[ 0.7 0.1 0.25 0.4],'FontSize',12}};
			
			obj.MainGUI = threePanelLayout('MapSeis Main Window',...
			mainGUIFigureProperties,mainGUIAxesProperties);
			
			set(obj.MainGUI,'ToolBar','figure');
		
			% Register this view with the model.  The subject of the view uses this
			% function handle to update the view.
			
			%Replace with proxy
			if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
				obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
				obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');	
			
				%NOTE: If the GUI is switched and more than one DataStore and FilterList is
				%used a listener per datastore and filterlist will exist, at the moment
				%I do not have a good idea at the moment how to prevent this or if it is 
				%even needed to be prevented
			
				% Create the menu
				setMenu(obj);
				
				%set plot parameters
				obj.legendary('chk');
				obj.getparam('Show Coastline','CoastToggle');
				obj.getparam('Show Internal Borders','BorderToggle');
				obj.getparam('Show Seismic Stations','StationToggle');  
				obj.getparam('Mark Largest Earthquakes','MarkEQ');
				obj.getparam('Automatic DepthProfile','AutoProfile');
				obj.getparam('Draw Plate Boundaries','PlateToggle')
				
				%Create the Calculation Entries
				obj.createCalcMenu;
				
				obj.setDeveloper('init');
				
				% Create a context menu to choose histogram type
				% NB: context menu only appears if you right click in the histogram window,
				% but not actually on the histogram bars themselves
				createHistContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_top_right'));
				
				% Create context menu for the main event plot axis to select the region of
				% interest
				createMainAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_left'));
				
				% Create context menu for the main event plot axis to select the region of
				% interest
				createTimeAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_bottom_right'));
				
				try
					mainPars = dataStore.getUserData('mainPars');
					obj.SelectRadius = mainPars.selectRadius;
					obj.HistType = mainPars.histType;
					obj.TimeType = mainPars.timeType;

				catch ME
					% Show different histogram plots depending on histType
					% {'mag','depth',...}, set from hist plot context menu.
					obj.HistType = 'mag';
					
					% Show different time plots depending on timeType, set from time plot
					% context menu.
					obj.TimeType = 'cumnum';
					% Radius for selection about a point
					obj.SelectRadius = 0.25;

				end
				
				%external function can not be plot automatically
				if strcmp(obj.HistType,'EXTERN');
					obj.HistType = 'mag';
				end
				
				if strcmp(obj.TimeType,'EXTERN');
					obj.TimeType = 'cumnum';
				end
				
				%update Button
				updateButton = uicontrol(obj.MainGUI, 'Units', 'Normalized',...
						'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update',...
						'Callback',@(s,e) obj.updateGUI(true));
				
				
				% Update the GUI to plot the initial data
				updateGUI(obj);
				
				%load plugIns
				obj.startPlugs;
				
				%empty external plot functions
				obj.ExternPlotFunction=cell(3,1);
				obj.ExternRegionPlot=false;
				
				% Make the GUI visible
				set(obj.MainGUI,'Visible','on');

			end
			
			
			%Invoke the Commander
			%--------------------
			%Should be changed later, because the Commander should be really Commander

			%create CommanderGUi
			obj.CommanderGUI = CommanderGUI(obj);
			
			%set the hashes with the current datastore and filterlist
			if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
				nameStr = dataStore.getUserData('filename');
				
				if isempty(nameStr) 
					nameStr= 'noname';
				end	 
			
				%pushit to the commander
				initdata = struct('DataName',nameStr,...
				'Datastore',dataStore,...
				'FilterName',nameStr,...
				'Filterlist',filterList);
				
				obj.ShownDataID = num2str(1);
				obj.ShownFilterID = num2str(1);
				
				obj.CommanderGUI.pushIt(initdata);

			end
			
			if isempty(obj.DataStore) & isempty(obj.FilterList)
				obj.CommanderGUI.EmptyStart=true;
			end
			
		end
		
		%additional external file methods
		%--------------------------------

		updateGUI(obj,force)
		
		updateGUI_leftaxesonly(obj,force)

		plotRegion(obj,plotAxis)

		plotHist(obj,plotAxis)		

		plotTime(obj,plotAxis)

		createHistContextMenu(obj,parentAxis)		

		setHistType(obj,newHistType)

		setTimeType(obj,newTimeType)

		createMainAxisContextMenu(obj,parentAxis)

		SetRegionGlove(obj,WhichOne)

		setCircle(obj)

		plotInNewFigure(obj,figTitleStr,plotFun)
		
		saveData(obj)

		saveSelectedData(obj)

		CutSelectedData(obj)

		loadCatalog(obj,filename)

		loadZMAPData(obj)
	
		startMapAnalysis(obj)
	
		openParamWindow(obj)

		createTimeAxisContextMenu(obj,parentAxis)

		setMenu(obj)

		setqual(obj,qual)
		
		createCalcMenu(obj)

		CallCalculation(obj,CalcHandle)

		setparam(obj,mlabel,guipar)

		getparam(obj,mlabel,guipar)

		setDeveloper(obj,doWhat)

		legendary(obj,com)

		startThirdDimension(obj)

		startDepthSlice(obj)

		setProfWidth(obj)

		setCircleRadius(obj)

		setPlatePlotConfig(obj)

		setExistingWindows(obj,paramWindow,calcWindow)
		
		rebuildGUI(obj)

		wantedaxis = reTag(obj,whichone)

		relisten(obj)

		quiet(obj)

		listen(obj)

		GuiRebuildSeq(obj)	

		MainGUISwitcher(obj,newdata)

		InitGUIPlugs(obj)

		startPlugs(obj)

		AddStationList(obj)
	
	
	end

	methods (Static)
		
		functable = CalcSniffer(homepath)

	end


end



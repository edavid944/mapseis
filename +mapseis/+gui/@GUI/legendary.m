function legendary(obj,com)
	%switches the legend in the plot on and off
	%or just checks it
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	dataStore = obj.DataStore;
	Menu2Check= findobj(obj.OptionMenu,'Label','Legend on/off');
	
	switch com
		case 'chk'
			try 
				leg = dataStore.getUserData('LegendToggle');
			catch 
				dataStore.setUserData('LegendToggle',true);
				leg = dataStore.getUserData('LegendToggle');	
			end
			
			obj.LegendToggle=leg;
			
			set(Menu2Check, 'Checked', 'off');
			
			if leg
				set(Menu2Check, 'Checked', 'on');
			end
		
		case 'set'		
			obj.LegendToggle=~obj.LegendToggle;
			dataStore.setUserData('LegendToggle',obj.LegendToggle);
			leg=obj.LegendToggle;
			
			g = obj.MainGUI;
			plotAxis = findobj(g,'Tag','axis_left');
			
				
			if leg
				set(Menu2Check, 'Checked', 'on');
				legend(plotAxis,'show');
			else
				set(Menu2Check, 'Checked', 'off');
				legend(plotAxis,'hide');
			end
		
	end	
	
end
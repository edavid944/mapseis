function loadZMAPData(obj)
	% Load ZMAP file
	% as well


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.datastore.*;
	import mapseis.importfilter.*;
	import mapseis.gui.*;
	import mapseis.projector.*;
	import mapseis.calc.*;

	[fName,pName] = uigetfile('*.mat','Enter ZMAP or MapSeis *.mat file name...');
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected. ');
		return	
	
	else
		close all;
		
		prevData = load(fullfile(pName,fName));
		
		% We have loaded in a ZMAP format file with data in matrix 'a'
		dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));

		%buffer the dates
		dataStore.bufferDate;
		dataStore.SetPlotQuality;
		
		% Set the filterList to refer to this dataStore
		obj.FilterList.DataStore = dataStore;
		obj.FilterList.update();
		
		% Get the data out of the file we have loaded
		newData = dataStore.getFields();
		
		% Set the data of the current DataStore property to be the
		% data that we've just loaded in.
		obj.DataStore.setData(newData);
		mapseis.startApp(fullfile(pName,fName));
		
	end


end
function rebuildGUI(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%allows to rebuild a closed window
	
	import mapseis.gui.*;
	import mapseis.util.gui.*;
	
	%try close and delete old windows
	oldExist=false;
	try
		%try to get old position
		try 
			oldPos=get(obj.MainGUI,'Position');
			disp('Got old position')
			oldExist=true;
		end
		
		clf(obj.MainGUI);
		set(obj.MainGUI,'Visible','on');
	end

	dataStore = obj.DataStore;
	filterList = obj.FilterList;
	obj.FilterIndexVect = filterList.getSelected();
	
	% Set the properties of the figure window
	DefaultPos=[ 50 300 1100 690 ];
	

	%change screensize if needed
	if oldExist
		Screener= oldPos;
	else
		MonSize=get(0,'ScreenSize');
		if 0.9*MonSize(3)<=DefaultPos(3)|0.9*MonSize(4)<=DefaultPos(4);
			Screener=[50 150 MonSize(3)*0.8 MonSize(4)*0.9-150];	
		else
			Screener=DefaultPos;
		end
	end
	
	mainGUIFigureProperties = {'Position',Screener,'Renderer','painters'};
	
	% Set the axes properties by hand in the order left plot, top right, bottom
	% right
	mainGUIAxesProperties = {...
							{'pos',[ 0.06 0.1 0.55 0.85],'FontSize',12},...
							{'pos',[ 0.7 0.58 0.25 0.37],'FontSize',12},...
							{'pos',[ 0.7 0.1 0.25 0.4],'FontSize',12}};
	
	obj.MainGUI = threePanelLayout('MapSeis Main Window',...
	mainGUIFigureProperties,mainGUIAxesProperties,obj.MainGUI);
	
	set(obj.MainGUI,'ToolBar','figure');
	
	% Register this view with the model.  The subject of the view uses this
	% function handle to update the view.
	try
		obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'main');
		obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
	end

	% Create the menu
	setMenu(obj);
	createCalcMenu(obj);
	obj.setDeveloper('reinit');
	
	
	%set plot parameters
	obj.legendary('chk');
	obj.getparam('Show Coastline','CoastToggle');
	obj.getparam('Show Internal Borders','BorderToggle');
	obj.getparam('Show Seismic Stations','StationToggle'); 
	obj.getparam('Mark Largest Earthquakes','MarkEQ');
	obj.getparam('Automatic DepthProfile','AutoProfile');
	obj.getparam('Draw Plate Boundaries','PlateToggle');
	
	if isempty(obj.PlatePlotSetting)
		obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
		'Ridge',true,...
		'Transform',true,...
		'Trench',true,...
		'RidgeLineStylePreset',1,...
		'TransformLineStylePreset',1,...
		'TrenchLineStylePreset',1,...
		'RidgeColors',2,...
		'TransformColors',3,...
		'TrenchColors',1);

	end				
	
	% Create a context menu to choose histogram type
	% NB: context menu only appears if you right click in the histogram window,
	% but not actually on the histogram bars themselves
	createHistContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_top_right'));
	
	% Create context menu for the main event plot axis to select the region of
	% interest
	createMainAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_left'));
	
	% Create context menu for the main event plot axis to select the region of
	% interest
	createTimeAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_bottom_right'));
	
	try
		mainPars = dataStore.getUserData('mainPars');
		obj.SelectRadius = mainPars.selectRadius;
		obj.HistType = mainPars.histType;
		obj.TimeType = mainPars.timeType;
	
	catch ME
		% Show different histogram plots depending on histType
		% {'mag','depth',...}, set from hist plot context menu.
		obj.HistType = 'mag';
		
		% Show different time plots depending on timeType, set from time plot
		% context menu.
		obj.TimeType = 'cumnum';
		% Radius for selection about a point
		obj.SelectRadius = 0.25;
	end
	
	%external function can not be plot automatically
	if strcmp(obj.HistType,'EXTERN');
		obj.HistType = 'mag';
	end
				
	if strcmp(obj.TimeType,'EXTERN');
		obj.TimeType = 'cumnum';
	end	
	
	%update Button
	updateButton = uicontrol(obj.MainGUI, 'Units', 'Normalized',...
					'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update',...
					'Callback',@(s,e) obj.updateGUI(true));
	
	% Update the GUI to plot the initial data
	updateGUI(obj);
	
	%load plugIns
	obj.startPlugs;
	
	%empty external plot functions
	obj.ExternPlotFunction=cell(3,1);
	obj.ExternRegionPlot=false;
	
	% Make the GUI visible
	set(obj.MainGUI,'Visible','on');


end
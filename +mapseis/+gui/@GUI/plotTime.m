function plotTime(obj,plotAxis)	
	% Plot the appropriate histogram depending on histType parameter


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.plot.*;
	import mapseis.projector.*;

	switch obj.TimeType
		case 'cumnum'
			PlotTime(plotAxis,...
					getDecYear(obj.DataStore,obj.FilterIndexVect));
                    
			if obj.MarkEQ
				axes(plotAxis);
				lonLimi=xlim(plotAxis);
				latLimi=ylim(plotAxis);   
				[handle legendentry]=PlotLargeEQ(plotAxis,obj.DataStore,obj.FilterIndexVect,'time');
				axes(plotAxis);
				hold off;
				xlim(plotAxis,lonLimi);
				ylim(plotAxis,latLimi);
			end
			
		case 'stem'
			PlotTimeStem(plotAxis,...
					getDecYear(obj.DataStore,obj.FilterIndexVect),...
					getMagnitudes(obj.DataStore,obj.FilterIndexVect));
				                
		case 'timedepths'
			PlotTimeDepth(plotAxis,...
					getDecYear(obj.DataStore,obj.FilterIndexVect),...
					getDepths(obj.DataStore,obj.FilterIndexVect));
		
		case 'EXTERN'
			if ~isempty(obj.ExternPlotFunction{3})
				obj.ExternPlotFunction{3}(plotAxis);			
			end
			
		otherwise
		
		end

end
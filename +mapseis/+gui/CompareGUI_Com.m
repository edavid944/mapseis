classdef CompareGUI_Com < handle
    % Prototyp of a GUI to compare two catalogs with each other, the GUI will change later to the more flexible Resultviewer of Mapseis.
    
    % 
    % $Revision: 1 $    $Date: 2009-04-28  $
    % Author: David Eberhard
    
    %This version can be used with the Commander
    
    properties
        DataStore_A
        DataStore_B
        Select_A
        Select_B
        Commander
        GUIHandle
        CatController
	ListProxy
	CalcRes
        CalcParameters
        shownParameter
        CoastToggle
        BorderToggle
        LegendToggle
        OptionMenu
        WindowName
        PlotQuality
    end
    
    methods
        
        function obj = CompareGUI_Com(viewName,Commander,ListProxy)
            % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.listenerproxy.*;
            
            Commander.AutoUpdate('off');
            
            if isempty(ListProxy)
            	ListProxy = addproxylistener;
            end
            
            obj.WindowName = viewName;
            obj.ListProxy = ListProxy;
            obj.Commander=Commander;
            obj.PlotQuality = 'lowcomp';
            obj.CoastToggle = true;
	    obj.BorderToggle = true;
	    obj.LegendToggle = true;
 
            % Create the figure window that contains the GUI.  Use a one panel layout.
            obj.GUIHandle = onePanelLayout('CompareGUI Main Window',...
                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
            set(obj.GUIHandle,'Position',[ 50 300 1100 690 ],'Renderer','painters');
            set(obj.GUIHandle,'DeleteFcn',@(s,e) obj.Commander.AutoUpdate('on'));
            
            %create the controller and listen to it
            obj.CatController = CatControlGUI_Com(obj.Commander);
           
           
           
            obj.ListProxy.listen(obj.CatController, 'Calculate', @() obj.calculate);          

                        
            % Calculation results
            obj.CalcRes = {};
            obj.CalcParameters = {};
            
            %set basic setting
            obj.shownParameter = 'map';
            

            %obj.updateGUI();
            
            % Make the GUI visible
            set(obj.GUIHandle,'Visible','on');                        
            
            %create Rightbutton mouse menu
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            obj.createContextMenu(plotAxis);
            
            % Create the menu (here we refer to the object itself)
            
            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
            uimenu(obj.OptionMenu,'Label','Symbol .',...
                'Callback',@(s,e) obj.setqual('lowcomp'));
            uimenu(obj.OptionMenu,'Label','Symbol o',...
                'Callback',@(s,e) obj.setqual('hicomp'));
            uimenu(obj.OptionMenu,'Label','Legend on/off','Separator','on',...
                'Callback',@(s,e) obj.legendary('set'));
            
            uimenu(obj.OptionMenu,'Label','Show Coastline','Separator','on',...
                'Callback',@(s,e) obj.setparam('Show Coastline','CoastToggle'));    
            uimenu(obj.OptionMenu,'Label','Show Internal Borders',...
                'Callback',@(s,e) obj.setparam('Show Internal Borders','BorderToggle'));    
    
                    
                
            plotmenu= uimenu(obj.GUIHandle,'Label','- Plots');
			
            
             uimenu(plotmenu,'Label','Map','Callback',...
                @(s,e) setParameterType(obj,'map'));
            uimenu(plotmenu,'Label','Cumulative FMD',...
                'Callback',@(s,e) setParameterType(obj,'cumnum'));
            uimenu(plotmenu,'Label','DepthHistogram','Callback',...
                @(s,e) setParameterType(obj,'depthist'));
            uimenu(plotmenu,'Label','Depth CDF','Callback',...
                @(s,e) setParameterType(obj,'depthCDF'));
            uimenu(plotmenu,'Label','Magnitude',...
                'Callback',@(s,e) setParameterType(obj,'Mag'));
             uimenu(plotmenu,'Label','Magnitude Histogram',...
                'Callback',@(s,e) setParameterType(obj,'MagHist'));    
            uimenu(plotmenu,'Label','Magnitude CDF',...
                'Callback',@(s,e) setParameterType(obj,'MagCDF'));
            uimenu(plotmenu,'Label','Magnitude - Time',...
                'Callback',@(s,e) setParameterType(obj,'MagTime'));    
            uimenu(plotmenu,'Label','Plot this frame in new window ',...
                'Callback',...
                @(s,e) plotInNewFigure(obj,'Histogram',@plotHist));
            
            
            obj.setqual('chk');

            
            
                            
        end
        
        function calculate(obj)
            import mapseis.catalogtools.*;
            import mapseis.util.gui.allWaiting;
            
        	%get data and parameters from the controller and save
        	obj.DataStore_A = obj.CatController.DataStore_A;
        	obj.DataStore_B = obj.CatController.DataStore_B;
        	obj.Select_A = obj.CatController.Select_A;
        	obj.Select_B = obj.CatController.Select_B;
        	
        	obj.CalcParameters.timediff = str2num(get(obj.CatController.dtime,'String'));
        	obj.CalcParameters.maxdist = str2num(get(obj.CatController.distan,'String'));
        	
        	%check if the catalogs are not empty, else calculate
        	if isempty(obj.DataStore_A) | isempty(obj.DataStore_B)
        		errordlg('Not enough catalogs')
        	else
        		parame = struct('mode','catalog',...
        						'timedifference',obj.CalcParameters.timediff,...
        						'spacedifference',obj.CalcParameters.maxdist,...
        						'maxmagdiff', inf,...
        						'Select_A',obj.Select_A,...
        						'Select_B',obj.Select_B);
        		allWaiting('wait');
        		obj.CalcRes = catalog_duplicates(obj.DataStore_A, obj.DataStore_B, parame);
        		allWaiting('move');
        		
        		
        		%names for the legend are needed in mosts plots so build an array here once.
        		obj.CalcRes.filenames(1) = {obj.DataStore_A.getUserData('filename')};
        		obj.CalcRes.filenames(2) = {obj.DataStore_B.getUserData('filename')};
        		
        		
        		%DeltaMagnitude over time calculation
        		obj.CalcRes = Calc_dmag_time(obj.CalcRes,obj.DataStore_A,obj.DataStore_B);
        		
        		%update the gui
        		obj.updateGUI;
        		
        	end	
            
        end
        
        function updateGUI(obj)
            % This method creates the accord function to plot the wanted parameter
            % 
            import mapseis.plot.*;
            import mapseis.projector.*;
            
            %determine which plot
            plotparam = obj.shownParameter;
            
            dataA=obj.DataStore_A.getFields;
            dataB=obj.DataStore_B.getFields;
            
            
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
           	figure(obj.GUIHandle);
           	
           		figure(obj.GUIHandle);
           		plotAxis = subplot(1,1,1);
				set(plotAxis,'Tag','axis','TickDir','out','color','r','FontWeight','bold','FontSize',12);
           	
           
           switch plotparam
           		case 'map'
  						
						%similar to a normal plot but we need three "catalogs"
		                
		                %Locations
		                %---------
		                [uniqA,temp] = ...
			                getLocations(obj.DataStore_A,obj.CalcRes.uniqA); %#unique locations in cata A
			            [uniqB,temp] = ...
			                getLocations(obj.DataStore_B,obj.CalcRes.uniqB); %#unique locations in cata B
			            
			            %indentical from catalog A should be enough for the overview plot
			            [indiA,temp] = ...
			                getLocations(obj.DataStore_A,obj.CalcRes.identicalA); %#unique in cata A
    
    					%Magnitudes
			            %----------
			              	%uniq A
				            [selectedMags, unselectedMags] = ...
				                getMagnitudes(obj.DataStore_A,obj.CalcRes.uniqA); 
				            mags(1).selected = selectedMags;
				            mags(1).unselected = zeros(0,1);
			            	
			            	%uniq B
				            [selectedMags, unselectedMags] = ...
				                getMagnitudes(obj.DataStore_B,obj.CalcRes.uniqB); 
				            mags(2).selected = selectedMags;
				            mags(2).unselected = zeros(0,1);
							
							%indentical A
				            [selectedMags, unselectedMags] = ...
				                getMagnitudes(obj.DataStore_A,obj.CalcRes.identicalA); 
				            mags(3).selected = selectedMags;
				            mags(3).unselected = zeros(0,1);

			            
			                %now plot
			                PlotRegion(plotAxis,struct('uniqA',uniqA,...
			                    'uniqB',uniqB,...
			                    'indentical',indiA,...
			                    'plotCoastline',obj.CoastToggle,...
			                    'plotBorder',obj.BorderToggle,...
			                    'showlegend',obj.LegendToggle,...
			                    'Filenames',{obj.CalcRes.filenames},...
			                    'dataStore',obj.DataStore_B,...
			                    'PlotType','Compare',...
			                    'Quality',obj.PlotQuality),mags);
			                    
			               %set properties
	           				ax=findobj(plotAxis,'Type','axes');
	           				xlab=get(ax,'Xlabel');
	           				ylab=get(ax,'Ylabel');
	           				
	           				set(ax,'FontWeight','bold','FontSize'   , 14,'LineWidth',1.5);
	           				set(xlab,'FontWeight','bold','FontSize'   , 14);
	           				set(ylab,'FontWeight','bold','FontSize'   , 14);
	     
			         
           		
           		
           		case 'cumnum'
           				
           				%yearAt = getDecYear(obj.DataStore_A);
           				%yearBt = getDecYear(obj.DataStore_B);
           				
           				%try
					%	magtypeA=dataA.magtype(obj.CalcRes.id(:,2));
					%	magtypeB=dataB.magtype(obj.CalcRes.id(:,1));
	
					%		%only ML, Ml, MlMDN or MlREG or not specified
					%	ml=(strcmp(magtypeA,'ML') | strcmp(magtypeA,'Ml') | strcmp(magtypeA,'MlMDN') | ...
					%	    strcmp(magtypeA,'MlREG') | strcmp(magtypeA,'')) & ...
					%	    (strcmp(magtypeB,'ML') | strcmp(magtypeB,'Ml') | strcmp(magtypeB,'MlMDN') | ...
					%	    strcmp(magtypeB,'MlREG') | strcmp(magtypeB,''));
					%catch
					%	ml=logical(ones(length(yearAt,1)));
					%end	
					[yearA temp] = getDecYear(obj.DataStore_A,obj.CalcRes.identicalA);
					%yearB = yearBt(obj.CalcRes.id(:,1));
					[yearB temp] = getDecYear(obj.DataStore_B,obj.CalcRes.identicalB);
						
						

           				
           				%first the indentical from datastore A
           				
           				
           				%h1 = PlotTime(plotAxis,...
           				%		 getDecYear(obj.DataStore_A,obj.CalcRes.identicalA));
                       			h1 = PlotTime(plotAxis,yearA);

                       	
                       	hold on
                       	                       	
                       	%second the indentical from datastore B
           				%h2 = PlotTime(plotAxis,...
                       	%		 getDecYear(obj.DataStore_B,obj.CalcRes.identicalB));		 
						h2 = PlotTime(plotAxis,yearB);
           				
           				hold off
           				
           				%change color of the second line
           				set(h2,'Color','b');
           				
           				%set properties
           				ax=findobj(plotAxis,'Type','axes');
           				xlab=get(ax,'Xlabel');
           				ylab=get(ax,'Ylabel');
           				
           				set(ax,'FontWeight','bold','FontSize'   , 14,'LineWidth',1.5);
           				set(xlab,'FontWeight','bold','FontSize'   , 14);
           				set(ylab,'FontWeight','bold','FontSize'   , 14);
           				
           				
           				%build legend
           				legend(plotAxis, obj.CalcRes.filenames{:},'Interpreter','none');
           					if obj.LegendToggle
           						legend(plotAxis, 'show');
           					else 	
           						legend(plotAxis, 'hide');
           					end	
           					
           					
           		case 'depthist'
           			
           		
           				%[depthA temp]=getDepths(obj.DataStore_A,obj.CalcRes.identicalA);
           				%[depthB temp]=getDepths(obj.DataStore_B,obj.CalcRes.identicalB);
           				[depthA temp]=getDepths(obj.DataStore_A,obj.Select_A);
           				[depthB temp]=getDepths(obj.DataStore_B,obj.Select_B);

           				
           				depths(:,1)=depthA(obj.CalcRes.id(:,2));
           				depths(:,2)=depthB(obj.CalcRes.id(:,1));
           				
           				%depths(:,1)=getDepths(obj.DataStore_A,obj.CalcRes.identicalA);
           				%depths(:,2)=getDepths(obj.DataStore_B,obj.CalcRes.identicalB);
           				
           				PlotDepthHistDiff(plotAxis,depths);
						xlabel(plotAxis,['Depth Difference (',obj.CalcRes.filenames{1},' - ',obj.CalcRes.filenames{2},') in [km]']);


           				
				
			case 'depthCDF' 
					[depthA temp]=getDepths(obj.DataStore_A,obj.Select_A);
           				[depthB temp]=getDepths(obj.DataStore_B,obj.Select_B);

           				
           				depths(:,1)=depthA(obj.CalcRes.id(:,2));
           				depths(:,2)=depthB(obj.CalcRes.id(:,1));
           				
           				
						
						
						%first the indentical from datastore A
           				%h1 = PlotDepthCDF(plotAxis,...
                        %		getDepths(obj.DataStore_A,obj.CalcRes.identicalA));
						
						h1 = PlotDepthCDF(plotAxis,depths(:,1));
           				
           				hold on
                       	                       	
           				%second the indentical from datastore B
							%h2 = PlotDepthCDF(plotAxis,...
					%		getDepths(obj.DataStore_B,obj.CalcRes.identicalB));
                        
					h2 = PlotDepthCDF(plotAxis,depths(:,2));		
           				
           				hold off

           				
           				%change color of the first plot
           				set(h1,'Color','b');
           				
           				%build legend
           				legend(plotAxis, obj.CalcRes.filenames{:},'Interpreter','none','Location','Southeast');
           					if obj.LegendToggle
           						legend(plotAxis, 'show');
           					else 	
           						legend(plotAxis, 'hide');
           					end	

						
						
					          
           		case 'Mag'
           				[magA temp] = getMagnitudes(obj.DataStore_A,obj.Select_A);
           				[magB temp] = getMagnitudes(obj.DataStore_B,obj.Select_B);
						
					
					mags(:,1)=magA(obj.CalcRes.id(:,2));
					mags(:,2)=magB(obj.CalcRes.id(:,1));
           				%mags(:,1) = getMagnitudes(obj.DataStore_A,obj.CalcRes.identicalA);
						%mags(:,2) = getMagnitudes(obj.DataStore_B,obj.CalcRes.identicalB);
        				
           				PlotMagDiff(plotAxis,mags(:,1),mags(:,2),obj.CalcRes);
           		
           		
			case 'MagHist'
						
					[magA temp] = getMagnitudes(obj.DataStore_A,obj.Select_A);
           				[magB temp] = getMagnitudes(obj.DataStore_B,obj.Select_B);
						
					
					mags(:,1)=magA(obj.CalcRes.id(:,2));
					mags(:,2)=magB(obj.CalcRes.id(:,1));
           				%mags(:,1) = getMagnitudes(obj.DataStore_A,obj.CalcRes.identicalA);
						%mags(:,2) = getMagnitudes(obj.DataStore_B,obj.CalcRes.identicalB);
						
           				PlotMagHistDiff(plotAxis,mags);
						xlabel(plotAxis,['Magnitude Difference (',obj.CalcRes.filenames{1},' - ',obj.CalcRes.filenames{2},') ']);
           				           				
           				
						
				
			case 'MagCDF'
						
					[magA temp] = getMagnitudes(obj.DataStore_A,obj.Select_A);
           				[magB temp] = getMagnitudes(obj.DataStore_B,obj.Select_B);
						
					
					mags(:,1)=magA(obj.CalcRes.id(:,2));
					mags(:,2)=magB(obj.CalcRes.id(:,1));
						
						%first the indentical from datastore A
           				%h1 = PlotMagCDF(plotAxis,...
                        %		getMagnitudes(obj.DataStore_A,obj.CalcRes.identicalA));
						h1 = PlotMagCDF(plotAxis,mags(:,1));
						
           				hold on
                       	                       	
                       	%second the indentical from datastore B
						%h2 = PlotMagCDF(plotAxis,...
                        %		getMagnitudes(obj.DataStore_B,obj.CalcRes.identicalB));
                        h2 = PlotMagCDF(plotAxis,mags(:,2));		
           				
           				hold off

           				
           				%change color of the first plot
           				set(h1,'Color','b');
           				
           				%build legend
           				legend(plotAxis, obj.CalcRes.filenames{:},'Interpreter','none','Location','Southeast');
           					if obj.LegendToggle
           						legend(plotAxis, 'show');
           					else 	
           						legend(plotAxis, 'hide');
           					end	

						
						
						
				case 'MagTime'
						PlotMagTimeDiff(plotAxis,obj.CalcRes)
				end           
           
           

            set(plotAxis,'Tag','axis');
        
        
        end
        
               
        
         function createContextMenu(obj,parentAxis)
            % Create a context menu for the histogram in the top right subplot,
            % that selects the histogram type

            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.GUIHandle);
            % Now make the menu be associated with the correct axis
            set(obj.GUIHandle,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different histogram types
            uimenu(cmenu,'Label','Map','Callback',...
                @(s,e) setParameterType(obj,'map'));
            uimenu(cmenu,'Label','Cumulative FMD',...
                'Callback',@(s,e) setParameterType(obj,'cumnum'));
            uimenu(cmenu,'Label','DepthHistogram','Callback',...
                @(s,e) setParameterType(obj,'depthist'));
            uimenu(cmenu,'Label','Depth CDF','Callback',...
                @(s,e) setParameterType(obj,'depthCDF'));
            uimenu(cmenu,'Label','Magnitude',...
                'Callback',@(s,e) setParameterType(obj,'Mag'));
             uimenu(cmenu,'Label','Magnitude Histogram',...
                'Callback',@(s,e) setParameterType(obj,'MagHist'));    
            uimenu(cmenu,'Label','Magnitude CDF',...
                'Callback',@(s,e) setParameterType(obj,'MagCDF'));
            uimenu(cmenu,'Label','Magnitude - Time',...
                'Callback',@(s,e) setParameterType(obj,'MagTime'));    
            uimenu(cmenu,'Label','Plot this frame in new window ',...
                'Callback',...
                @(s,e) plotInNewFigure(obj,'Histogram',@plotHist));
        end




        function setParameterType(obj,newHistType)
            % Set the parameter to plot on the histogram plot
            obj.shownParameter = newHistType;
            % Update the GUI
            updateGUI(obj);
        end

        
        
        
        
        
        
        function setGridParams(obj,source,event) %#ok<INUSD>
            % Create a dialog box to set the grid separation and radius of
            % interest
            
            import mapseis.gui.setGridParams;
            
            try
                setGridParams(obj.DataStore);
                obj.updateGUI();
            catch ME
            end
        end
        
        function clearCalculate(obj)
            % Clear the calculated values and replot the events.
            obj.CalcVals = {};
            obj.updateGUI();
        end
        
        
        function setqual(obj,qual)
			%sets the quality of the plot (just for the calculation plot
			%chk just gets the userdata parameter and sets the 'checked' to the menu
			Menu2Check = get(obj.OptionMenu,'Children');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
			
				case 'lowcomp'
					obj.PlotQuality = 'lowcomp';
					set(Menu2Check(2), 'Checked', 'on');
					obj.updateGUI;
				
				case 'hicomp'
					obj.PlotQuality = 'hicomp';
					set(Menu2Check(1), 'Checked', 'on');
					obj.updateGUI;
						
				case 'chk'
					StoredQuality = obj.PlotQuality;

							switch StoredQuality
								
								case 'lowcomp'
									sel=2;
								case 'hicomp'
									sel=1;
								end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
		end
		
		
		
			function setparam(obj,mlabel,guipar)
			%sets the plot parameter and writes it to the datastore and updates the gui
			
		 	dataStore = obj.DataStore_A;
			Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
			
			%get pararmeter from gui
			cstate=obj.(guipar);

			set(Menu2Check, 'Checked', 'off');
			
			%change the to the opposite
			cstate=~cstate;
			
			%write into gui and datastore	
			obj.(guipar)=cstate;
			dataStore.setUserData(guipar,cstate);
			
			%change check
				if cstate
					set(Menu2Check, 'Checked', 'on');
				end
			obj.updateGUI;
		end
		
		
		function getparam(obj,mlabel,guipar)
			%sets the plot parameter and writes it to the datastore and updates the gui
			
		 	dataStore = obj.DataStore_A;
			Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
			
			%get pararmeter from datastore
			try cstate = dataStore.getUserData(guipar);
						catch dataStore.setUserData(guipar,true);
							  cstate = dataStore.getUserData(guipar);	
			end				  
			
			obj.(guipar) = cstate;
				
			set(Menu2Check, 'Checked', 'off');
			
				if cstate
					set(Menu2Check, 'Checked', 'on');
				end
			
		end

		
		
		
		function legendary(obj,com)
			%switches the legend in the plot on and off
			%or just checks it
			
			dataStore = obj.DataStore_A;
			Menu2Check= findobj(obj.OptionMenu,'Label','Legend on/off');
			
			switch com
				case 'chk'
					try leg = dataStore.getUserData('LegendToggle');
						catch dataStore.setUserData('LegendToggle',true);
							  leg = dataStore.getUserData('LegendToggle');	
					end
					
						obj.LegendToggle=leg;
						
						set(Menu2Check, 'Checked', 'off');
			
						if leg
							set(Menu2Check, 'Checked', 'on');
						end

				case 'set'		
					obj.LegendToggle=~obj.LegendToggle;
					dataStore.setUserData('LegendToggle',obj.LegendToggle);
					leg=obj.LegendToggle;
					
					g = obj.GUIHandle;
					plotAxis = findobj(obj.GUIHandle,'Tag','axis');
					
											
						if leg
							set(Menu2Check, 'Checked', 'on');
							legend(plotAxis,'show');
						else
							set(Menu2Check, 'Checked', 'off');
							legend(plotAxis,'hide');
						end

			end	
							  

		
		end
		
		
				
		function rebuildGUI(obj)
			%allows to rebuild a closed GUI window
			
			   % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.filter.*;
            
            
            try 
            	set(obj.GUIHandle,'Visible','on');
            
            catch		
	            dataStore =  obj.DataStore;
	            filterList = obj.FilterList;
	                        
	            % Create the figure window that contains the GUI.  Use a one panel layout.
	            obj.GUIHandle = onePanelLayout('MapSeis Grid Calculation Plot',...
	                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
	            
	                        
	              % Register this view with the model.  The subject of the view uses this
	            % function handle to update the view.
	            dataStore.subscribe(obj.WindowName,@() updateGUI(obj));
	            
	            % Update the GUI to plot the initial data
	            obj.updateGUI();
	            
	            % Make the GUI visible
	            set(obj.GUIHandle,'Visible','on');                        
	            
	            % Create the menu (here we refer to the object itself)
	            
	            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
	            uimenu(obj.OptionMenu,'Label','Zmap style',...
	                'Callback',@(s,e) obj.setqual('low'));
	            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
	                'Callback',@(s,e) obj.setqual('med'));
	            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
	                'Callback',@(s,e) obj.setqual('hi'));
	            
				obj.setqual('chk');
	
	            
	            
	            obj.GridMenu=uimenu(obj.GUIHandle,'Label','  --  Grid');
	            uimenu(obj.GridMenu,'Label','Set grid',...
	                'Callback',@obj.setGridParams);
	            uimenu(obj.GridMenu,'Label','Calculate',...
	                'Callback',@(s,e) obj.calculate());
	            uimenu(obj.GridMenu,'Label','Clear',...
	                'Callback',@(s,e) obj.clearCalculate());
			end
			
		
		end
		
		
		
        
    end
    
end
classdef ImportGUI < handle



    properties
     	TheGUI
		CommanderGUI
		SelectionList
		Texter
		SelectionArray
		Datastore                 
    end

	
	
	events
		Loaded
	end	



	methods

		function obj=ImportGUI(Commander)
			%This function creates a GUI for selecting the importfilters and allows to import 
			%.txt files as Mapseis datastore
			
			%Imports
			import mapseis.gui.*;
			import mapseis.util.gui.*;
			import mapseis.datastore.*;
			import mapseis.importfilter.*;	
			
			obj.SelectionArray=cell(0,4);
			obj.CommanderGUI=Commander
			
			%first get all the import filters
			thefunctions = mapseis.gui.ImportGUI.littleSniffer
			
			
			obj.Selectionbuilder(thefunctions);
			
			
			
			obj.TheGUI = figure('Name','Importer',...
				'Position',[500 500 480 300],...
				'Toolbar','none','MenuBar','none','NumberTitle','off');
		            
		            
		            
			%create all the buttons and stuff
			obj.CreateImporterLayout;
			
			% Make the GUI visible
			set(obj.TheGUI,'Visible','on');
				
				
		
			
		end
			
		function CreateImporterLayout(obj)	
			%creates the whole layout of the figure
			
				TGUI = obj.TheGUI;
				
				obj.Texter = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'BackgroundColor',[1 1 1],...
				'Position',[4.71428571428571 4.85714285714285 47.2857142857143 9.78571428571428],...
				'String','Description',...
				'Style','text',...
				'Tag','description');
				
				
				obj.SelectionList = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.changeTexter,...
				'Position',[4 16.5 49.1428571428571 2.57142857142857],...
				'String',obj.SelectionArray(:,1),...
				'Style','popupmenu',...
				'Value',1,...
				'Tag','Importfilterselect');
		
				
				h4 = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.MisterLoad,...
				'Position',[38 1.07142857142857 15 2.42857142857143],...
				'String','Get from file',...
				'Tag','startImport');
				
				h5 = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.noImport,...
				'Position',[4 1.07142857142857 15 2.42857142857143],...
				'String','Cancel',...
				'Tag','cancel');
				
						
				h6 = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[4.71428571428571 14.9285714285714 46.8571428571429 1.28571428571429],...
				'String','Description:',...
				'Style','text',...
				'Tag','destitle');
		
				h7 =  uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.Webload,...
				'Position',[21 1.07142857142857 15 2.42857142857143],...
				'String','Get from Web',...
				'Tag','WebImport');
		
			
		end	
			
			
				
		function Selectionbuilder(obj,functable)
				%call every function, get the description and the name and set the handle
				
				for i=1:numel(functable)
					whoareyou=functable{i}([],'-description');
					obj.SelectionArray(i,:) = {whoareyou.displayname,whoareyou.description,functable{i},whoareyou.filetype};
				
				end
		
		
		end
			
			
		function changeTexter(obj)	
			%change the text in the description
			
			%get position of the marker
			val = get(obj.SelectionList, 'Value');
			
			%set the text	
			set(obj.Texter,'String',obj.SelectionArray{val,2});	
			
		
		end	
		
		
		
		function noImport(obj)
			%Do nothing and return an empty object.
			
			obj.Datastore=[];
			notify(obj,'Loaded');
		end
		
		
		function MisterLoad(obj)
			import mapseis.datastore.*;
			
			
			set(obj.Texter,'String','Importing Data, this may take a while...');
			%load and import the file and notify the Commander
			
			
			%get the importfilter and apply
			val = get(obj.SelectionList, 'Value');
			
			%load dialog
			[fName,pName] = uigetfile(obj.SelectionArray{val,4},'Select File...');
          					  if isequal(fName,0) || isequal(pName,0)
               						 disp('No filename selected. ');
           					  else
								     filename=fullfile(pName,fName);
							  end	     

			
			
			

			
			try
				eventStruct=obj.SelectionArray{val,3}(filename,'-file');	
			catch 
				errordlg('There was an error')
				eventStruct=[];

			end
			
			if ~isempty(eventStruct);
				%check for Routing and Info First
				RoutingTable=[];
				FieldInfo=[];
				CatalogInfo=[];
				
				if isfield(eventStruct,'ROUTING_INFO')
					RoutingTable=eventStruct.ROUTING_INFO;
					eventStruct=rmfield(eventStruct,'ROUTING_INFO');
				end
				
				if isfield(eventStruct,'CATALOG_FIELD_DESCRIPTION')
					FieldInfo=eventStruct.CATALOG_FIELD_DESCRIPTION;
					eventStruct=rmfield(eventStruct,'CATALOG_FIELD_DESCRIPTION');
				end
				
				if isfield(eventStruct,'CATALOG_INFO_TEXT')
					CatalogInfo=eventStruct.CATALOG_INFO_TEXT;
					eventStruct=rmfield(eventStruct,'CATALOG_INFO_TEXT');
				end
				
				%create datastore
				obj.Datastore=DataStore(eventStruct);
				
				if ~isempty(RoutingTable)
					obj.Datastore.setRouting(RoutingTable);
				end
				
				
				%Buffer and set the name of the files
				setDefaultUserData(obj.Datastore);
				obj.Datastore.setUserData('filename',fName);
				obj.Datastore.setUserData('filepath',filename);
				
				if ~isempty(FieldInfo)
					obj.Datastore.setUserData('FieldInfo',FieldInfo);
				end
				
				if ~isempty(CatalogInfo)
					obj.Datastore.setUserData('CatalogInfo',CatalogInfo);
				end
				
				
				
			else
				obj.Datastore=eventStruct;
			end	
			
			set(obj.Texter,'String',obj.SelectionArray{val,2});
			
			%notify the Commander and let yourself be killed
			notify(obj,'Loaded');

		end
		
		
		function Webload(obj)
			%similar to  Misterload but it gets it data from a webaddress
			%saves it and send it to the import filter
			import mapseis.datastore.*;
			
			set(obj.Texter,'String','Importing Data, this may take a while...');
			%load and import the file and notify the Commander
			
			
			%get the importfilter and apply
			val = get(obj.SelectionList, 'Value');
			
			WebTarget=inputdlg('Please Enter Webadress');
			
	
			%save the data 
			filename=['./Temporary_Files/TempWebData',obj.SelectionArray{val,4}];
			urlwrite(WebTarget{:},filename);
		
			
			try
				eventStruct=obj.SelectionArray{val,3}(filename,'-file');	
			catch 
				errordlg('There was an error')
				eventStruct=[];

			end
			
			if ~isempty(eventStruct);
				%check for Routing and Info First
				RoutingTable=[];
				FieldInfo=[];
				CatalogInfo=[];
				
				if isfield(eventStruct,'ROUTING_INFO')
					RoutingTable=eventStruct.ROUTING_INFO;
					eventStruct=rmfield(eventStruct,'ROUTING_INFO');
				end
				
				if isfield(eventStruct,'CATALOG_FIELD_DESCRIPTION')
					FieldInfo=eventStruct.CATALOG_FIELD_DESCRIPTION;
					eventStruct=rmfield(eventStruct,'CATALOG_FIELD_DESCRIPTION');
				end
				
				if isfield(eventStruct,'CATALOG_INFO_TEXT')
					CatalogInfo=eventStruct.CATALOG_INFO_TEXT;
					eventStruct=rmfield(eventStruct,'CATALOG_INFO_TEXT');
				end
				
				%create datastore
				obj.Datastore=DataStore(eventStruct);
				
				if ~isempty(RoutingTable)
					obj.Datastore.setRouting(RoutingTable);
				end
				
				
				%Buffer and set the name of the files
				setDefaultUserData(obj.Datastore);
				obj.Datastore.setUserData('filename',['WebImport_',date]);
				obj.Datastore.setUserData('LastUpdate',date);
				obj.Datastore.setUserData('filepath',filename);
				obj.Datastore.setUserData('webadress',WebTarget{:});
				%maybe needed later for an update
				obj.Datastore.setUserData('importfilter',obj.SelectionArray{val,3});
				
				if ~isempty(FieldInfo)
					obj.Datastore.setUserData('FieldInfo',FieldInfo);
				end
				
				if ~isempty(CatalogInfo)
					obj.Datastore.setUserData('CatalogInfo',CatalogInfo);
				end
				
				
			else
				obj.Datastore=eventStruct;
			end	
			
			set(obj.Texter,'String',obj.SelectionArray{val,2});
			
			%notify the Commander and let yourself
			notify(obj,'Loaded');
		end
		
		

	end
	
	
	methods (Static)
			function functable = littleSniffer()
				% searches all importfilters in the path ./+mapseis/+importfilter and returns them
				% as function handles.
				
				import mapseis.util.importfilter.*;
				import mapseis.importfilter.*;
				
				%get all necessary reg expressionpatterns
				rp = regexpPatternCatalog();
		
				%where are I now
				curdir=pwd;
				
				cd './+mapseis/+importfilter';
				
				
				
				if ispc
					[sta res] = dos(['dir /b *.m']);
				else
					[sta res] = unix(['ls -1 *.m']);
				end
				
				lines = regexp(res,rp.line,'match');
				
				
				for i=1:numel(lines)
			
					[pathStr,nameStr{i},extStr] = fileparts(lines{i});
					%this has to be done for a certain object structur
					nameStr{i} = ['mapseis.importfilter.',nameStr{i}];
			
				end
		
				functable=cellfun(@str2func, nameStr, ...
		                   'UniformOutput', false);
				
				
				%get back to original dir
				cd(curdir);
		
				
		end
		

	
	end
	
end		
		
		

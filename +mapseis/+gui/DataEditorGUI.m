classdef DataEditorGUI < handle



    properties
     		TheGUI
		CommanderGUI
		FieldSelector
		BasicFieldSelector
		ConverterSelector
		NameEntry
		TheFields
		BasicFields
		Converters
		RoutingTable
		Datastore
    end

	
	
	events
		Edited
	end	



	methods

		function obj=DataEditorGUI(Commander,WhichEdit)
			%This modul creates a gui to edit the datastore, at the moment
			%two editors are available, one to set the default fields with data
			%and one to rename fields. An additional editor for adding and removing
			%fields from the datastore.
			%And maybe even more editors will be added.
			
			%Imports
			import mapseis.gui.*;
			import mapseis.util.gui.*;
			import mapseis.datastore.*;

			obj.CommanderGUI=Commander
			
			%get datastore
			datakey = Commander.CurrentDataList{Commander.MarkedData};
			data = Commander.DataHash.lookup(datakey);
			obj.Datastore = data.Datastore;
			
			%get the available fields
			obj.TheFields = [{'NO ROUTING'} ; obj.Datastore.getOriginalFieldNames];
			
			%The Fields which are common to all datastores and should contain data.
			obj.BasicFields = {'dateNum','lon','lat','mag','depth','focal_strike','focal_dip',...
						'focal_rake','dataSource'};
			
			%Build Converterlist
			obj.BuildConvertList;
			
			
			%This are the minimum defined and needed fields (expect dataSource)
			%Later more fields have to been defined to be standard.
			
			
			%get routing table
			obj.RoutingTable=obj.Datastore.getRouting;
			
			
			obj.TheGUI = figure('Name','DataEditor',...
				'Position',[500 500 280 200],...
				'Toolbar','none','MenuBar','none','NumberTitle','off');
		            
		            
		        
				
				
			%create all the buttons and stuff
			switch WhichEdit
				case 'Fielder'
					obj.CreateFieldSelector;
				case 'Renamer'
					obj.CreateRenamer;
				case 'AddRemover'
					%not build yet
			end	
			
			% Make the GUI visible
			set(obj.TheGUI,'Visible','on');
				
				
		
			
		end
			
		function CreateFieldSelector(obj)	
			%creates the whole layout for the fieldselector
			
				TGUI = obj.TheGUI;
				
				Temp = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[10 175 260 20],...
				'String','Basic Fields:',...
				'Style','text',...
				'Tag','title1');
							
				obj.BasicFieldSelector = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Position',[10 150 260 20],...
				'String',obj.BasicFields,...
				'Style','popupmenu',...
				'Value',1,...
				'Callback',@(s,e) obj.FieldSwitcher('Fielder'),... 
				'Tag','BasicFieldsSelector');
		
				Temp = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[10 125 260 20],...
				'String','Available Fields:',...
				'Style','text',...
				'Tag','title2');
				
				obj.FieldSelector = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Position',[10 100 260 20],...
				'String',obj.TheFields,...
				'Style','popupmenu',...
				'Value',1,...
				'Tag','FieldsSelector');
				
				Temp = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[10 75 260 20],...
				'String','Available Converters:',...
				'Style','text',...
				'Tag','title2');
				
				obj.ConverterSelector = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Position',[10 50 260 20],...
				'String',obj.Converters(:,1),...
				'Style','popupmenu',...
				'Value',1,...
				'Tag','ConverterSelector');
		
				
				
				h4 = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Callback',@(s,e) obj.FinishedEdit,...
				'Position',[190 10 80 25],...
				'String','Edit & Quit',...
				'Tag','EditIt');
				
				
				h4 = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Callback',@(s,e) obj.MasterEditor('Fielder'),...
				'Position',[100 10 80 25],...
				'String','Write',...
				'Tag','EditIt');
				
				
				h5 = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Callback',@(s,e) obj.noEdit,...
				'Position',[10 10 80 25],...
				'String','Cancel',...
				'Tag','cancel');
				
						
				
		
			
		end	
		
			
			
		function CreateRenamer(obj)	
			%creates the whole layout for the Renamer
			
				TGUI = obj.TheGUI;
				
				Temp = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[10 175 260 20],...
				'String','Field to Rename:',...
				'Style','text',...
				'Tag','title1');
							
				obj.FieldSelector = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Position',[10 150 260 20],...
				'String',obj.TheFields(2:end),...
				'Style','popupmenu',...
				'Callback',@(s,e) obj.FieldSwitcher('Renamer'),... 
				'Value',1,...
				'Tag','FieldsSelector');
		
				Temp = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[10 125 260 20],...
				'String','New Name:',...
				'Style','text',...
				'Tag','title2');
				
				
				obj.NameEntry = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Position',[10 100 260 25],...
				'String','',...
				'Style','edit',...
				'Tag','RenameField');
				
				
				Temp = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[10 75 260 20],...
				'String','Available Converters:',...
				'Style','text',...
				'Tag','title2');
				
				obj.ConverterSelector = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Position',[10 50 260 20],...
				'String',obj.Converters(:,1),...
				'Style','popupmenu',...
				'Value',1,...
				'Tag','ConverterSelector');
		
				
				
				h4 = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Callback',@(s,e) obj.FinishedEdit,...
				'Position',[190 10 80 25],...
				'String','Edit & Quit',...
				'Tag','EditIt');
				
				
				h4 = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Callback',@(s,e) obj.MasterEditor('Renamer'),...
				'Position',[100 10 80 25],...
				'String','Write',...
				'Tag','EditIt');
				
				
				h5 = uicontrol(...
				'Parent',TGUI,...
				'Units','Pixels',...
				'Callback',@(s,e) obj.noEdit,...
				'Position',[10 10 80 25],...
				'String','Cancel',...
				'Tag','cancel');
				
						
				
		
			
		end			

			
			
		
		
		function noEdit(obj)
			%Do nothing and return an empty object.
			
			obj.Datastore=[];
			notify(obj,'Edited');
		end
		
		
		function FinishedEdit(obj)
			%Apply Changes
			if ~isempty(obj.RoutingTable)
				obj.Datastore.setRouting(obj.RoutingTable);
			end
			
			%notify the Master
			notify(obj,'Edited');
			
		end
		
		function MasterEditor(obj,WhichEdit)
			import mapseis.datastore.*;
			
			
			
			switch WhichEdit
				case 'Fielder'
					%get basic field to change
					basix=obj.BasicFields{get(obj.BasicFieldSelector, 'Value')};
					
					%the new field to set
					if get(obj.FieldSelector, 'Value') ~= 1
						newfield=obj.TheFields{get(obj.FieldSelector, 'Value')};
					else
						newfield=[];
					end
					
					%get converter (position 1 is set to [] no check needed here)
					TheConverter=obj.Converters{get(obj.ConverterSelector, 'Value'),2};
					
					%Check if a entry is already existing
					if ~isempty(obj.RoutingTable)
						foundEntry=strcmp(obj.RoutingTable(:,1),basix);
					else
						foundEntry=0;
					end	
					
					if sum(foundEntry)>0 
						%old entry, rewrite it
						obj.RoutingTable{foundEntry,1}=basix;
						obj.RoutingTable{foundEntry,2}=newfield;
						obj.RoutingTable{foundEntry,3}=TheConverter;
					
					else
						%create a new coulomb
						obj.RoutingTable{end+1,1}=basix;
						obj.RoutingTable{end,2}=newfield;
						obj.RoutingTable{end,3}=TheConverter;
					
						
					end
					
					
				case 'Renamer'
					%get the field to edit
					ReField=obj.TheFields(2:end);
					fieldy=ReField{get(obj.FieldSelector, 'Value')};
					
					%get the new name
					newname=get(obj.NameEntry,'String');
					
					%replace '' with [] 
					if strcmp(newname,'') | strcmp(newname,' ')
						newname=[];
					end
					
					%get converter (position 1 is set to [] no check needed here)
					TheConverter=obj.Converters{get(obj.ConverterSelector, 'Value'),2};
					
					
					%check if there is already an entry for fieldy
					if ~isempty(obj.RoutingTable)
						foundEntry=strcmp(obj.RoutingTable(:,2),fieldy);
					else
						foundEntry=0;
					end
					
					if sum(foundEntry)>0
						if isempty(newname)
							%delete entry
							obj.RoutingTable=obj.RoutingTable(~foundEntry,:);
						else
							obj.RoutingTable{foundEntry,2}=fieldy;
							obj.RoutingTable{foundEntry,1}=newname;
							obj.RoutingTable{foundEntry,3}=TheConverter;
						end
						
					else
						%create new entry if newname is not empty
						if ~isempty(newname)
							obj.RoutingTable{end+1,2}=fieldy;
							obj.RoutingTable{end,1}=newname;
							obj.RoutingTable{end,3}=TheConverter;
						end	
					end
					
								
				case 'Adder'
				case 'Remover'
			
			end
			
			
				
		end
		
		
		
		function FieldSwitcher(obj,WhichEdit)
			%gets the entry for the select field or basic field write it into the gui
			
			switch WhichEdit
				case 'Fielder'
					%get value
					basix=obj.BasicFields{get(obj.BasicFieldSelector, 'Value')};
					
					%default value:
					positField=1;
					positConvert=1;
					
					%length of the entrylists
					FieldListVec=1:numel(obj.TheFields);
					ConvertListVec=1:numel(obj.Converters(:,1));
					
					%look if the field is already set
					if ~isempty(obj.RoutingTable)
						foundEntry=strcmp(obj.RoutingTable(:,1),basix);
						
						if sum(foundEntry)>0						
							CatField=obj.RoutingTable{foundEntry,2};
							ConvertEntry=obj.RoutingTable{foundEntry,3};
							
							if ~isempty(CatField)
								positField=FieldListVec(strcmp(obj.TheFields,CatField));
							end
							
							if ~isempty(ConvertEntry)
								ConverterName=ConvertEntry([],'-name');
								positConvert=ConvertListVec(strcmp(obj.Converters(:,1),ConverterName));
							end
							
							
						end
						
					
					end
					
					%set field to the position
					set(obj.FieldSelector, 'Value',positField);
					set(obj.ConverterSelector, 'Value',positConvert);
				
				case 'Renamer'
					%get value
					ReField=obj.TheFields(2:end);
					catField=ReField{get(obj.FieldSelector, 'Value')};
					
					%default value:
					ReName='';
					positConvert=1;
					
					%length of the entrylists
					ConvertListVec=1:numel(obj.Converters(:,1));
					
					%look if the field is already set
					if ~isempty(obj.RoutingTable)
						foundEntry=strcmp(obj.RoutingTable(:,2),catField);
						
						if sum(foundEntry)>0				
							%first field cannot be empty anyway, so no check needed
							ReName=obj.RoutingTable{foundEntry,1};
							ConvertEntry=obj.RoutingTable{foundEntry,3};
							
							
							if ~isempty(ConvertEntry)
								ConverterName=ConvertEntry([],'-name');
								positConvert=ConvertListVec(strcmp(obj.Converters(:,1),ConverterName));
							end
							
						
						end
					end
					
					
					%change fields
					set(obj.NameEntry, 'String',ReName);
					set(obj.ConverterSelector, 'Value',positConvert);
				
			end	
			
			
		
		
		end
		
		
		
		function BuildConvertList(obj)
			%scans the directory ./+mapseis/+converter and builds a list with names and handles for the converter
			%the first entry in the list is the empty, "not set" entry.
			import mapseis.util.importfilter.*;
			import mapseis.converter.*;
						
			%get all necessary reg expressionpatterns
			rp = regexpPatternCatalog();
				
			%where are I now
			curdir=pwd;
			
			cd './+mapseis/+converter';
				
			if ispc
				[sta res] = dos(['dir /b *.m']);
			else
				[sta res] = unix(['ls -1 *.m']);
			end
			
			lines = regexp(res,rp.line,'match');
				
			for i=1:numel(lines)
				[pathStr,nameStr{i},extStr] = fileparts(lines{i});
				%this has to be done for a certain object structur
				nameStr{i} = ['mapseis.converter.',nameStr{i}];
			
			end
		
			functable=cellfun(@str2func, nameStr, ...
		                   'UniformOutput', false);
		        
		        %get back to original dir
			cd(curdir);         
		        
			
			%Now build the actuall list
			obj.Converters{1,1}='Not Set';
			obj.Converters{1,2}=[];
			
			for i=1:numel(functable)
				whoareyou=functable{i}([],'-name');
				obj.Converters(i+1,:) = {whoareyou,functable{i}};
			
			end
			
		                   
		end
		

	end
	
	
	
	
end		
		
		

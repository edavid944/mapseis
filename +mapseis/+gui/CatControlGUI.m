classdef CatControlGUI < handle
    % ParamGUI : GUI frontend for the MapSeis application parameters
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        DataStore_A
        DataStore_B
        CatGUI
        dtime
        distan
        screenA
        screenB
        
    end
    
    events
        Calculate
    end
    	
    
    methods
        function obj = CatControlGUI()
            import mapseis.projector.*;
            
            % builds the gui, which is a controller and dataentry for the main compare Catalog gui
            
            
           
            %Init Variables
            obj.DataStore_A = [];
        	obj.DataStore_B = [];
                        
                       
            
            
            % Initialise the GUI
            
            % Create the figure
            try
            Map_handle = findobj('name','CompareGUI Main Window');
            pos = get(Map_handle,'pos');
            catch
            pos = [0 0 640 480]
            end
            
            %Use 6 for the sizing (cat1,cat2, timediff, spacediff, load cat1 & cat2 buttons)
            winheight=410;
            %pos = [0 0 640 480]
            obj.CatGUI = figure('Name','Compare Parameters',...
                'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 360 winheight],...
                'Toolbar','none','MenuBar','none','NumberTitle','off');
            
            %create all the buttons and stuff
            obj.createLayout;
            
    
            
            
            
            
            %            
            % Make the GUI visible
            set(obj.CatGUI,'Visible','on');
            
        end
        
        function updateGUI(obj)
            % Just sends a notification to the main catControl           
        	notify(obj,'Calculate');
        end
        
        
        function loadCat(obj,catslot)
        	%loads the catalog and save it to suitable slot A or B
        	import mapseis.*;
			import mapseis.datastore.*;
				
					 
					 %open load dialog if no filename specified
				
						[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
          					  if isequal(fName,0) || isequal(pName,0)
               						 disp('No filename selected. ');
           					  else
								     filename=fullfile(pName,fName);
							  end	     
				
			%----------------------------------------------------------------------------------
					
					%load catalog
					prevData = load(filename);
			
			%----------------------------------------------------------------------------------
			
			%Determine what datatyp it is 
			if isfield(prevData,'dataStore')
                    % We have loaded a saved session, 
                    dataStore = prevData.dataStore;
                                       	
                elseif isfield(prevData,'dataCols')
                    % We have loaded only the data columns, which now need to be turned
                    % into an mapseis.datastore.DataStore
                    dataStore = mapseis.datastore.DataStore(prevData.dataCols);
                    setDefaultUserData(dataStore);
                    
                    
                elseif isfield(prevData,'a')
                    % We have loaded in a ZMAP format file with data in matrix 'a'
                    dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
                    setDefaultUserData(dataStore)
                                      
                else
                    error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');
             end
             
            %----------------------------------------------------------------------------------		 
            %check if bufferdata and userdata exists
            	try %check wether the datastore is already buffered
                		temp=dataStore.getUserData('DecYear');
                		catch
                		dataStore.bufferDate;
                	end
                	
                	try %check wether the plot quality is already set
                		temp=dataStore.getUserData('PlotQuality');
                		catch
                		dataStore.SetPlotQuality;
                	end

                
			%----------------------------------------------------------------------------------		
   					%save the data in the usefull slot and set name of the field
   					
   					%just in case
           			if isempty(fName) 
            				fName = 'noname';
           		 	end	 
					
					%write filename and path to userdata
					dataStore.setUserData('filename',fName);
					dataStore.setUserData('filepath',filename);					
					
					switch catslot
						case 'A'
							obj.DataStore_A = dataStore;
							set(obj.screenA,'String',fName);
						case 'B'
							obj.DataStore_B = dataStore;
							set(obj.screenB,'String',fName);
	
					end		
					
		
		end

        
        
           
                
        function createLayout(obj)
        	import mapseis.gui.*;
        	
        	h1 = obj.CatGUI;
        	bgcol = get(obj.CatGUI,'Color');
        	 	
			 obj.screenA = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],...
			'FontSize',10,...
			'Position',[2.5 6.66666666666667 45.5 2.66666666666667],...
			'String',{  'no catalog A' },...
			'Style','text',...
			'Tag','cataA');
			
			
			h3 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'Callback',@(s,e) obj.loadCat('A'),...
			'FontSize',10,...
			'Position',[2.16666666666667 9.16666666666667 12.1666666666667 2.58333333333333],...
			'String','Load',...
			'Tag','loadA');
			
			
			
			 obj.screenB = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],...
			'FontSize',10,...
			'Position',[2.66666666666667 0.75 45.5 2.66666666666667],...
			'String',{  'no catalog B' },...
			'Style','text',...
			'Tag','cataB');
			
			
			
			h5 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'Callback',@(s,e) obj.loadCat('B'),...
			'FontSize',10,...
			'Position',[2.5 3.33333333333333 12.1666666666667 2.58333333333333],...
			'String','Load',...
			'Tag','LoadB');
			
			
			
			h6 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'Callback',@(s,e) obj.updateGUI(),...
			'FontSize',12,...
			'FontWeight','bold',...
			'Position',[2.33333333333333 24.9166666666667 45 3.33333333333333],...
			'String','Calculate',...
			'Tag','CalcButton');
			
			
			obj.distan = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[1 1 1],...
			'FontSize',10,...
			'Position',[33.1666666666667 19.5 12.6666666666667 2.5],...
			'String','10',...
			'Style','edit',...
			'Tag','distanEntry');
			
			
			
			obj.dtime = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[1 1 1],...
			'FontSize',10,...
			'Position',[33.1666666666667 15 12.6666666666667 2.5],...
			'String','0.25',...
			'Style','edit',...
			'Tag','timeEntry');
			
			
			
			h9 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[2 19.5 24.8333333333333 1.75],...
			'String','Maximal Distance [km]',...
			'Style','text',...
			'Tag','text6');
			
			
			
			h10 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[2 15.25 27.8333333333333 1.83333333333333],...
			'String','Max. Time difference [min]',...
			'Style','text',...
			'Tag','text7');
			
			
			
			h11 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[15 9.75 21.5 1.58333333333333],...
			'String','Catalog A',...
			'Style','text',...
			'Tag','text8');
			
			
			h12 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[15 4.08333333333333 22.1666666666667 1.41666666666667],...
			'String','Catalog B',...
			'Style','text',...
			'Tag','text9' );


        
        
        end
        
        function rebuildGUI(obj)
        	%allows to rebuild the closed gui
        	
        	import mapseis.projector.*;
        	
        	try
        		 set(obj.mainGUI,'Visible','on');	
        	
        	catch
        		 	%delete(obj.mainGUI);	
		        	dataStore = obj.DataStore;
		            filterList = obj.FilterList;
		            
		            rangeFilters = filterList.getRangeFilters();
		            filterCount = numel(rangeFilters);
		
		        	
		        	
		        	for filterIndex=1:filterCount
		                paramStruct(filterIndex) = ...
		                    mapseis.gui.ParamGUI.initFilter(dataStore,...
		                    rangeFilters{filterIndex});
		            end
		            
		            %obj.RangeFilters = rangeFilters;
		            
		            obj.ParamWidgets = cell(numel(paramStruct),1);
					
					      	
		        			        	
		        	% Create the figure
		            Map_handle = findobj('name','MapSeis Main Window');
		            pos = get(Map_handle,'pos');
		            
		            %Added automatic resizing of the paramwindow
		            winheight=filterCount*80;
		            
		            obj.mainGUI = figure('Name','Filter Parameters',...
		                'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight],...
		                'Toolbar','none','MenuBar','none','NumberTitle','off');
		            % Create a uipanel in the figure to store the filter parameter widgets
		            paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
		                'Position',[0 0 1 0.8]);
		            % Create the widgets inside the uipanel
		            obj.createParamWidgets(paramPanel,paramStruct);
		            % Create a button to call the update
		            updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
		                'Position',[0 0.85 0.5 0.1],'String','Update',...
		                'Callback',@(s,e) obj.updateGUI());
		            % Register this view with the model.  The subject of the view uses this
		            % function handle to update the view.
		            %dataStore.subscribe('ParamGUI',@() updateGUI(obj));
		            obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI);
		            %             evStruct.subscribe('ParamGUI',@() nUpdateGUI(gui));
		            
		            % Update the GUI to plot the initial data
		            obj.updateGUI();
		            
		            % Make the GUI visible
		            set(obj.mainGUI,'Visible','on');

        	end
        
        
        end
        
        
    end
    
       
end
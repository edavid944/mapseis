function Filtereditor(obj)
	%Allow to change the name of the filterlist at the moment
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	key = obj.CurrentFilterList{obj.MarkedFilter};
	filterlist = obj.FilterHash.lookup(key);
	
	Listname=filterlist.FilterName;
	
	promptValues = {'FilterList Name'};
	dlgTitle = 'Rename the FilterList';
	
	gParams = inputdlg(promptValues,dlgTitle,1,{Listname});
	NewListName=gParams{1};
	
	filterlist.Filterlist.setName(NewListName);
	filterlist.FilterName=NewListName;
	
	obj.FilterHash.set(key,filterlist);
	obj.InitList('Filter');
	obj.PopUp_Update;
	
end
function DataEdited(obj)
	%Something arrived from the DataEditor, load and finish
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%get datastore
	datastore = obj.DataEditor.Datastore;
	
	%cut the connection to the Importer and kill it
	obj.ListProxy.quiet(obj.DataEditor,'Edited',@(s,e) obj.DataEdited()); 
	
	%close the window
	close(obj.DataEditor.TheGUI);
	
	%Now lets move the data to the hash and update
	if ~isempty(datastore)
		%get original data
		datakey = obj.CurrentDataList{obj.MarkedData};
		dataentry = obj.DataHash.lookup(datakey);
		
		%just to be sure
		if isempty(datastore.GiveID)
			datastore.ResetRandID;
		end
		
		dataentry.Datastore = datastore;

		obj.DataHash.set(datakey,dataentry);
  			
		newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
		newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});

		loadstructur = struct(	'Datastore',newdatastore.Datastore,...
								'ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
								'Filterlist',newfilterlist.Filterlist,...
								'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});					  
		
		%popup update
		obj.PopUp_Update;
		
		%set to "new" position
		set(obj.DataSelection,'Value',obj.MarkedData);

		%change the MainGUI
		obj.MainGUI.MainGUISwitcher(loadstructur);
		%refresh plugins
		notify(obj,'rebuildMenus');

	end
	
	%clear leftovers of the editor
	clear obj.DataEditor;
	obj.DataEditor= [];	
	
	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');
	
	%msgbox('Catalog edited')

end
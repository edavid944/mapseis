function changeMarker(obj,handle)
	%changes the position of the selection switch
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%get the position
	val=get(handle,'Value');
	
	%get the tag to now which one has to be changed
	WhichOne = get(handle,'Tag');
		
	switch WhichOne
		case 'DataStore'
			obj.MarkedData=val;
			obj.switcher('Cata');
	
		case 'FilterList'
			obj.MarkedFilter=val;
			obj.switcher('Filter');
		
	end
	
	notify(obj,'Switched');	
	
end
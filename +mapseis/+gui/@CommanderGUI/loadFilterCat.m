function loadFilterCat(obj,filename)
	%loads both catalog and filterlist if present.

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.*;
	import mapseis.datastore.*;
	import mapseis.importfilter.*;
	import mapseis.util.gui.allWaiting;
	
	%open load dialog if no filename specified
	if nargin<2
		[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
	
		if isequal(fName,0) || isequal(pName,0)
			disp('No filename selected. ');
			return
		
		else
			filename=fullfile(pName,fName);
	
		end
	
	else
		%filename is defined
		[pName,fName,extStr] = fileparts(filename);
	
	end
		
	allWaiting('wait');		 

	
	%load catalog
	prevData = load(filename);
	

	%Determine what datatyp it is and add necessary filters
	if isfield(prevData,'dataStore')
		% We have loaded a saved session, including filter values
		dataStore = prevData.dataStore;
		try
			filterList = prevData.filterList;
		catch
			if obj.EmptyStart
				filterList = DefaultFilters(dataStore,obj.ListProxy); 
				prevData.filterList=filterList;
			end
		end

	elseif isfield(prevData,'dataCols')
		% We have loaded only the data columns, which now need to be turned
		% into an mapseis.datastore.DataStore
		dataStore = mapseis.datastore.DataStore(prevData.dataCols);
		setDefaultUserData(dataStore);
	
	elseif isfield(prevData,'a')
		% We have loaded in a ZMAP format file with data in matrix 'a'
		dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
		setDefaultUserData(dataStore)
		
	else
		error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');

	end

	
	%check if bufferdata and userdata exists
	try %check wether the datastore is already buffered
		temp=dataStore.getUserData('MilliSecond');
	catch
		dataStore.bufferDate;
	end
	
	try %check wether the plot quality is already set
		temp=datStore.getUserData('PlotQuality');
	catch
		dataStore.SetPlotQuality;
	end
	
	
	%add data to the catalog hash
	if isempty(fName) 
		fName = 'noname';
	end	 
	
	
	%write filename and path to userdata
	dataStore.setUserData('filename',fName);
	dataStore.setUserData('Name',fName);
	dataStore.setUserData('filepath',filename);
	
	%set a new random id
	dataStore.ResetRandID;
	
	%datastore hash
	dataentry = struct(	'DataName',fName,...
						'Datastore',dataStore);
	
	obj.DataHash.set(num2str(obj.CurrentDataID+1),dataentry);
	obj.CurrentDataID = obj.CurrentDataID+1;
	
	obj.CurrentDataList(end+1,:) = {num2str(obj.CurrentDataID),fName};
	
	%filterhash    
	if isfield(prevData,'filterList') 
		%set a new random id
		filterList.ResetRandID;
		
		filterentry = struct(	'FilterName',filterList.getName,...
								'Filterlist',filterList);
		
		obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
		obj.CurrentFilterID = obj.CurrentFilterID+1;
		
		%list for the gui
		filname =filterList.getName;
		obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname};
		
	end
	
	
	%make the new loaded catalog the current one
	obj.MarkedData=length(obj.CurrentDataList(:,1));
	
	
	%check if a filterlist exists if not, build one
	if isempty(obj.CurrentFilterList)
		obj.newfilterNoUpdate;
	end	
	
	obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
	
	
	newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
	
	
	newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
	
	
	loadstructur = struct(	'Datastore',newdatastore.Datastore,...
							'ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
							'Filterlist',newfilterlist.Filterlist,...
							'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});	  
	
	%popup update
	obj.PopUp_Update;
	
	%set to "new" position
	set(obj.DataSelection,'Value',obj.MarkedData);
	set(obj.FilterSelection,'Value',obj.MarkedFilter);
	
	%change the MainGUI
	obj.MainGUI.MainGUISwitcher(loadstructur);
	
	%refresh plugins
	notify(obj,'rebuildMenus');
	
	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');
	
	allWaiting('move');

end
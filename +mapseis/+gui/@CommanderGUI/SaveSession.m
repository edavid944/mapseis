function SaveSession(obj,WhichOne)
	%filters will not be packed in this version, it will be done later
	%if there is a need for it. 
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.gui.allWaiting;
	
	
	[fName,pName] = uiputfile('*.mat','Enter save file name...');
 	
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected.  Data not saved');
		return

	else
		allWaiting('wait');
		TheHash = obj.SendSession(WhichOne);			 
		filename=fullfile(pName,fName);
		
		switch WhichOne
			case 'DataHash'
				DataHash=TheHash;
				save(filename,'DataHash');
				allWaiting('move');
				msgbox('Catalog Hash saved')

			case 'FilterHash'
				FilterHash=TheHash;
				save(filename,'FilterHash');
				allWaiting('move');
				msgbox('Filter Hash saved')

			case 'BothHash'
				DataHash=TheHash{1};
				FilterHash=TheHash{2};
				save(filename,'DataHash','FilterHash');
				allWaiting('move');
				msgbox('Session saved')

		end	

	end
	
end
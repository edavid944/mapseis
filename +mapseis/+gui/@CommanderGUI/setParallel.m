function setParallel(obj,com)
	%activates or deactivates the the parallel processing mode	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%find menu entry
	Menu2Check= findobj(obj.EditMenu,'Label','Parallel Calculation');
	
	switch com
		case 'chk'
			if isempty(obj.ParallelMode)
				obj.ParallelMode=false;
			end
			
			set(Menu2Check, 'Checked', 'off');
		
			if obj.ParallelMode
				set(Menu2Check, 'Checked', 'on');
			end

		case 'set'		
			if isempty(obj.ParallelMode)
				obj.ParallelMode=false;
			end
			
			obj.ParallelMode=~obj.ParallelMode;
			
				

			
								
			if obj.ParallelMode
				set(Menu2Check, 'Checked', 'on');

			else
				set(Menu2Check, 'Checked', 'off');
			
			end
	end	

end
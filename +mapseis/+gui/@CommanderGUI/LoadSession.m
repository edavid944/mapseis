function LoadSession(obj)
	%open load dialog if no filename specified
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.gui.allWaiting;
	
	[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
	 	
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected. ');
		return

	else
		filename=fullfile(pName,fName);

	end	     
	
	allWaiting('wait');
	
	%load catalog
	prevData = load(filename);
	
	try
		FilterHash=prevData.FilterHash;
		FHash=true;
	catch
		FHash=false;
	end
	

	try
		DataHash=prevData.DataHash;
		DHash=true;
	catch
		DHash=false;
	end
	
	
	if FHash & DHash
		TheHash{1}=DataHash;
		TheHash{2}=FilterHash;
		obj.ReplaceSession(TheHash,'BothHash');

	elseif DHash & ~FHash
		obj.ReplaceSession(DataHash,'DataHash');

	elseif ~DHash & FHash
		obj.ReplaceSession(FilterHash,'FilterHash');

	else
		errordlg('No Data found in the matfile');

	end
	
	allWaiting('move');

end
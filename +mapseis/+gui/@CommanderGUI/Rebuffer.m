function Rebuffer(obj)
	%reapplies the buffer of the selected datastore.
	%Needed if some buffers are missing


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.util.gui.allWaiting;
	
	allWaiting('wait');
		
	key = obj.CurrentDataList{obj.MarkedData};
	datastore = obj.DataHash.lookup(key);
	datastore.Datastore.bufferDate;
	
	obj.DataHash.set(key,datastore);
	obj.InitList('Cata');
	obj.PopUp_Update;
	allWaiting('move');

end
function pushWithKey(obj,data,Key)
	%Allows to push a catalogue or filterlist into the hash with the hashkey already 
	%known. Good for replacing objects or put them away in the hash (often not needed, 
	%an object is saved as handle)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%get type
	theclass=class(data);
	
	switch theclass
		case 'mapseis.datastore.DataStore'
			try
				dataEntry = obj.DataHash.lookup(Key);
				dataEntry.Datastore=data;
				obj.DataHash.set(Key,dataEntry);
				
				obj.InitList('Cata');
				obj.PopUp_Update;
				
			catch			
				error('Commander: Datastore Key does not exist, use "pushIt" instead');
			end
			
			
			
		case 'mapseis.filter.FilterList'
			try
				filterEntry = obj.FilterHash.lookup(Key);
				filterEntry.Filterlist=data;
				obj.FilterHash.set(Key,filterEntry);
				
				obj.InitList('Filter');
				obj.PopUp_Update;
				
			catch					
				error('Commander: Filterlist Key does not exist, use "pushIt" instead');
			end
				
	end	
	
end
function SetNewIDs(obj,whichHash)
	%This function goes through the selected Hashes and sets new 
	%RandomIDs if needed
	%In this case only elements with have no RandomId will be set
	%all others remain untouched
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	switch whichHash
		case 'DataHash'
			for i=1:numel(obj.DataHash.data(:,2))
				%set a new random id
				if isempty(obj.DataHash.data{i,2}.Datastore.GiveID)
					obj.DataHash.data{i,2}.Datastore.ResetRandID;
				end
			end
			
		case 'FilterHash'
			for i=1:numel(obj.FilterHash.data(:,2))
				%set a new random id
				if isempty(obj.FilterHash.data{i,2}.Filterlist.GiveID)
					obj.FilterHash.data{i,2}.Filterlist.ResetRandID;
				end
			end
		
		case 'BothHash'
			for i=1:numel(obj.DataHash.data(:,2))
				%set a new random id
				if isempty(obj.DataHash.data{i,2}.Datastore.GiveID)
					obj.DataHash.data{i,2}.Datastore.ResetRandID;
				end
			end
			
			for i=1:numel(obj.FilterHash.data(:,2))
				%set a new random id
				if isempty(obj.FilterHash.data{i,2}.Filterlist.GiveID)
					obj.FilterHash.data{i,2}.Filterlist.ResetRandID;
				end
			end
		
	end
	
end
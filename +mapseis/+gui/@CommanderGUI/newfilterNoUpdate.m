function newfilterNoUpdate(obj)
	%creates a new filterlist with the currently shown catalog
	%same as newfilter, but does not update the screens, called
	%from datastore adding function which should do that part.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.*;
	import mapseis.datastore.*;
	
	%get marked datastore
	datakey = obj.CurrentDataList{obj.MarkedData};
	newdatastore = obj.DataHash.lookup(datakey);
					
	datastore = newdatastore.Datastore;
	
	%the default filterlist
	Filterlist = DefaultFilters(datastore,obj.ListProxy);
	
	
	%register the list in the hash
	filname = Filterlist.getName;

	filterentry = struct(	'FilterName',filname,...
							'Filterlist',Filterlist);
          
	obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
	obj.CurrentFilterID = obj.CurrentFilterID+1;
          		
	%list for the gui
	obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname}
	
	%update marker
	obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
	
	%updata GUIs and switch to marked datastore
	newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
	
	loadstructur = struct(	'Datastore',newdatastore.Datastore,...
							'ShownDataID',datakey,...
							'Filterlist',newfilterlist.Filterlist,...
							'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});

	%popup update
	obj.PopUp_Update;

	%set to "new" position
	set(obj.DataSelection,'Value',obj.MarkedData);
	set(obj.FilterSelection,'Value',obj.MarkedFilter);
	
	%refresh plugins
	notify(obj,'rebuildMenus');

	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');

end
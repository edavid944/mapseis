function ViewFieldInfo(obj)
	%show the catalogue
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	key = obj.CurrentDataList{obj.MarkedData};
	datastore = obj.DataHash.lookup(key);

	dlgTitle = 'Field of the Catalog';


	try
		FieldInfo=datastore.Datastore.getUserData('FieldInfo');
	catch
		FieldInfo=[];
	end
	
	%options for the window
	options.Resize='on';
	options.WindowStyle='normal';
	
	if ~isempty(FieldInfo)
		FieldInfo= cellfun(@(x,y) [x ':   ' y],FieldInfo(:,1),FieldInfo(:,2), 'UniformOutput',false );
		FieldInfo=[{'Field Description: '};FieldInfo];
		msgbox(FieldInfo,dlgTitle,'help');
	else
		msgbox('No information set',dlgTitle,'help');
	
	end

end
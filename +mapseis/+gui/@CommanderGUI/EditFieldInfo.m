function EditFieldInfo(obj)
	%Alows to edit the description of each data field in the catalogue
	%This is just a first version of an editor, the layout is pretty 
	%messed up if catalogue with many data fields are edited.
		

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	key = obj.CurrentDataList{obj.MarkedData};
	datastore = obj.DataHash.lookup(key);
	
	%get the fields
	thefields=datastore.Datastore.getOriginalFieldNames
	Howmanyfields=numel(thefields)
	
	dlgTitle = 'Edit the Catalog Field Description';
	
	try
		FieldInfo=datastore.Datastore.getUserData('FieldInfo');
	catch
	
		FieldInfo=[thefields,cell(Howmanyfields,1)];
		FieldInfo(:,2)=cellfun(@(x) '',FieldInfo(:,2), 'UniformOutput',false );
	end
	
	%check if there are more fields in than descripted, if so add empty
	%cells 
	if numel(FieldInfo(:,1))<Howmanyfields
		oldFieldInfo=FieldInfo;
		FieldInfo={};
		for i=1:Howmanyfields
			foundit=strcmp(oldFieldInfo(:,1),thefields{i});
			if sum(foundit)>0
				FieldInfo(i,1)=thefields(i);
				FieldInfo(i,2)=oldFieldInfo(foundit,2);
			else
				FieldInfo(i,1)=thefields(i);
				FieldInfo(i,2)={''};
			
			end	
		
		end
	end
	
	%options for the window
	options.Resize='on';
	options.WindowStyle='normal';
		
	
	gParams = inputdlg(FieldInfo(:,1),dlgTitle,1,FieldInfo(:,2),options);
	
	if ~isempty(gParams)
		FieldInfo(:,2)=gParams;
	end
	
	datastore.Datastore.setUserData('FieldInfo',FieldInfo);
	
	obj.DataHash.set(key,datastore);
	obj.InitList('Cata');
	obj.PopUp_Update;

end
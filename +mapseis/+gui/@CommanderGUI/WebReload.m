function WebReload(obj,How)
	%This function tries to either add new available data from the web ('update')
	%or to renew the whole data ('reload'). At the moment there is at least from the 
	%performance side no difference but, if I find a solution I will change that.

	%the try - catch block may not be the most proper programming style but it 
	%is relatively forgivening if a datastore misses some information.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.datastore.*;
	import mapseis.catalogtools.catalogmerger;
	import mapseis.catalogtools.LogicalExtractor;
	import mapseis.util.gui.allWaiting;
	
	%get datastore
	key = obj.CurrentDataList{obj.MarkedData};
	datastore = obj.DataHash.lookup(key);

	try
		filepath=datastore.Datastore.getUserData('filepath');
		[pathstr, name, ext] = fileparts(filepath); 
	catch
		ext='.dat';
	end	
	
	if isempty(ext)
		ext='.dat';
	end
	
	%try get webadress 
	try
		WebTarget=datastore.Datastore.getUserData('webadress');
		Worked=true;
	catch
		Worked=false;
		errordlg('There was no webadress specified in the datastore');
		return;
	end
	
	%try get importfilter 
	try
		ImportFilter=datastore.Datastore.getUserData('importfilter');
		Worked=Worked & true;
	catch
		Worked=false;
		errordlg('The importfilter is not specified in the datastore');
		return;
	end
	
	%try get the content
	try	
		filename=['./Temporary_Files/TempWebData',ext];
		urlwrite(WebTarget,filename);
		
		Worked=Worked & true;
	catch
		Worked=false;
		errordlg('Something went wrong when tried to get the data from the web');
		return;
	end
	
	
	%Finally do all the work
	if Worked
		allWaiting('wait');
		
		switch How
			case 'reload'
				%import data 
				try
					eventStruct=ImportFilter(filename,'-file');	
				catch 
					errordlg('Import did not work')
					eventStruct=[];
				end
				
				if ~isempty(eventStruct);
					datastore.Datastore.ReplaceInternalData(eventStruct);
					datastore.Datastore.setUserData('LastUpdate',date);
					obj.DataHash.set(key,datastore);
					obj.InitList('Cata');
					obj.PopUp_Update;
					msgbox('The current catalog is updated','Commander','help')
				end	
				

			case 'update'
				%import data 
				try
					eventStruct=ImportFilter(filename,'-file');	
				catch 
					errordlg('Import did not work')
					eventStruct=[];
				end
				
				if ~isempty(eventStruct);
					%the old data
					oldStruct=datastore.Datastore.getFields;
					
					%get last date
					lastEntry=max(oldStruct.dateNum)
					
					%check if newer data is available
					newSelection=eventStruct.dateNum>lastEntry;
					
					if ~any(newSelection)
						datastore.Datastore.setUserData('LastUpdate',date);
						obj.DataHash.set(key,datastore);
						msgbox('The catalog is already up to date','Commander','help')
					
					else
						%merge the to structures
						newStruct=LogicalExtractor(eventStruct,newSelection);
						newEvent=catalogmerger(oldStruct,newStruct);
						%write
						datastore.Datastore.ReplaceInternalData(newEvent);
						datastore.Datastore.setUserData('LastUpdate',date);
						obj.DataHash.set(key,datastore);
						obj.InitList('Cata');
						obj.PopUp_Update;
						msgbox('The current catalog is updated','Commander','help')

					end

				end
				

			case 'check'
				try
					TheDate=datastore.Datastore.getUserData('LastUpdate');
					msgbox(['The current catalog was last checked: ',TheDate],'Commander','help')
				catch
					msgbox('This catalog was never updated','Commander','help')
				end
			
		end
	
		allWaiting('move');
	
	end
	
end
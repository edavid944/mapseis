function OutElement = Keyresolver(obj,key,whichone);
	%Allows to get a catalog or filterlist with the hash key. Saver in
	%case of change Hashlists.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	switch whichone
		case 'data'
			TheEntry = obj.DataHash.lookup(key);
			OutElement = TheEntry.Datastore;
			
		case 'filter'
			TheEntry = obj.FilterHash.lookup(key);
			OutElement = TheEntry.Filterlist;
	end

end
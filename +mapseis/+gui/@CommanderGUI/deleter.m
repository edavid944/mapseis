function deleter(obj,WhichOne)    
	%removes a Catalog or a Filterlist from the hash
	%if a current catalog or Filterlist is deleted, the 
	%catalog/filterlist will still be shown in the mainmenu 
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.importfilter.*;
          
	switch WhichOne
		case 'Cata'
			%resolve key
			key=obj.CurrentDataList{obj.MarkedData,1};
			
			%remove entry
			obj.DataHash.remove(key);
			
			%rework Currentdatalist 
			logInd = strcmp(obj.CurrentDataList(:,1),key);
			obj.CurrentDataList = obj.CurrentDataList(~logInd,:);
			
			%set marker to save position
			obj.MarkedData=length(obj.CurrentDataList(:,1));
         			 
		case 'Filter'
			%resolve key
			key=obj.CurrentFilterList{obj.MarkedFilter,1};
			
			%remove entry
			obj.FilterHash.remove(key);
			
			%rework Currentdatalist 
			logInd = strcmp(obj.CurrentFilterList(:,1),key);
			obj.CurrentFilterList = obj.CurrentFilterList(~logInd,:);
			
			%set marker to save position
			obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
		
		end	
         		
	%popup update
	obj.PopUp_Update;
	
	%set to "new" position
	set(obj.DataSelection,'Value',obj.MarkedData);
	set(obj.FilterSelection,'Value',obj.MarkedFilter);

	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');

	
end
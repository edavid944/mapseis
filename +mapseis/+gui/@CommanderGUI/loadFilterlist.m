function loadFilterlist(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.*;
	import mapseis.datastore.*;
	import mapseis.util.gui.allWaiting;
		
		 
	%open load dialog if no filename specified
	if nargin<2
		[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
		
		if isequal(fName,0) || isequal(pName,0)
			disp('No filename selected. ');
			return
		
		else
			filename=fullfile(pName,fName);
		
		end	  
   
	end

	allWaiting('wait');	
		
	
	%load catalog
	prevData = load(filename);
	
		
	%Determine what datatyp it is and add necessary filters
	if isfield(prevData,'filterList')
		% FilterList found
		filterList = prevData.filterList
		 	
	else
		error('No FilterList found');
	
	end
	
	
	%set a new random id
	filterList.ResetRandID;
	
	%add to the hash
	filterentry = struct(	'FilterName',filterList.getName,...
							'Filterlist',filterList);
	
	obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
	obj.CurrentFilterID = obj.CurrentFilterID+1;
	
	%list for the gui
	filname =filterList.getName;
	obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname};

	%make the list the current one 	
	obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
		
	newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
	loadstructur = struct(	'Filterlist',newfilterlist.Filterlist,...
							'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});
		

	%popup update
	obj.PopUp_Update;
		
	%change the MainGUI
	obj.MainGUI.MainGUISwitcher(loadstructur);
		
	%refresh plugins
	notify(obj,'rebuildMenus');
		
	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');

	allWaiting('move');	
	
end
function pushIt(obj,newdata)
	%function to be called from outside, pushes datastore and filterlist into hash
	%and makes it the marked one.

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	
	if isfield(newdata,'Datastore')
		try
			fName = getUserData(newdata.Datastore,'Name');
		catch
			fName = getUserData(newdata.Datastore,'filename');
			newdata.Datastore.setUserData('Name',fName);
		end
			
		%check if a randomID exists
		%set a new random id
		if isempty(newdata.Datastore.GiveID)
			newdata.Datastore.ResetRandID;
		end
			
		dataentry = struct('DataName',fName,...
        		  		   'Datastore',newdata.Datastore);
          			
        %check if a filterlist exists if not, build one
        if isempty(obj.CurrentFilterList)
        	obj.newfilterNoUpdate;
        end
        
		obj.DataHash.set(num2str(obj.CurrentDataID+1),dataentry);
        obj.CurrentDataID = obj.CurrentDataID+1;

		obj.CurrentDataList(end+1,:) = {num2str(obj.CurrentDataID),fName}

	end	
	

	if isfield(newdata,'Filterlist')
		 %add to the hash
		 filname = newdata.Filterlist.getName;
		 	
		 %set a new random id
		if isempty(newdata.Filterlist.GiveID)
			newdata.Filterlist.ResetRandID;
		end
			
		filterentry = struct('FilterName',filname,...
        			  		 'Filterlist',newdata.Filterlist);
          
        obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
        obj.CurrentFilterID = obj.CurrentFilterID+1;
          		
		%list for the gui
		obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname}
			 		
	end	 		

	
	%make the new loaded catalog the current one
 	obj.MarkedData=length(obj.CurrentDataList(:,1));
 	obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
	
	%popup update
	obj.PopUp_Update;
	
	%set to "new" position
	set(obj.DataSelection,'Value',obj.MarkedData);
	set(obj.FilterSelection,'Value',obj.MarkedFilter);
	
	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');
	
end
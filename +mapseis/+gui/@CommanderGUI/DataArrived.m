function DataArrived(obj)
	%Something arrived from the Importer, load and finish the importer

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.gui.allWaiting;
	
	allWaiting('wait');
	%get datastore
	datastore = obj.Importer.Datastore;
	
	%cut the connection to the Importer and kill it
	obj.ListProxy.quiet(obj.Importer,'Loaded',...
		@(s,e) obj.DataArrived()); 
	
	%close the window
	close(obj.Importer.TheGUI);
	
	%Now lets move the data to the hash and update
	if ~isempty(datastore)
		try
			fName=datastore.getUserData('Name');
		catch
			fName=datastore.getUserData('filename');
			datastore.setUserData('Name',fName);
		end
		
		%just to be sure
		if isempty(datastore.GiveID)
			datastore.ResetRandID;
		end
		
		dataentry = struct(	'DataName',fName,...
							'Datastore',datastore);
           
		obj.DataHash.set(num2str(obj.CurrentDataID+1),dataentry);
		obj.CurrentDataID = obj.CurrentDataID+1;
           
		%gui list							
		obj.CurrentDataList(end+1,:) = {num2str(obj.CurrentDataID),fName};
				
		%make the new imported catalog the current one
		obj.MarkedData=length(obj.CurrentDataList(:,1));
	
		newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
		  
		%check if a filterlist exists if not, build one
		if isempty(obj.CurrentFilterList)
			obj.newfilterNoUpdate;
		end
							  
		newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});

		loadstructur = struct(	'Datastore',newdatastore.Datastore,...
								'ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
								'Filterlist',newfilterlist.Filterlist,...
								'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});	
				  
		%popup update
		obj.PopUp_Update;
		
		%set to "new" position
		set(obj.DataSelection,'Value',obj.MarkedData);
		
		%change the MainGUI
		obj.MainGUI.MainGUISwitcher(loadstructur);

		%refresh plugins
		notify(obj,'rebuildMenus');

	end

	%delete leftovers
	clear obj.Importer;
	obj.Importer= [];	

	%notify so everyone who needs the list can update
	notify(obj,'HashUpdate');

	allWaiting('move');

end
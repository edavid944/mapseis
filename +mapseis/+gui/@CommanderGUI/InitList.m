function InitList(obj,WhichOne)
	%adds the current datahash and filterhash to the selectionlists
	%creates a cell array with key and namestring
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	switch WhichOne
		case 'Cata'
			Datakeys = obj.DataHash.getKeys;
			
			%get every data and add to cell array
			for i=1:numel(Datakeys)
				datastore= obj.DataHash.lookup(Datakeys{i});
				
				%get name
				entryname = datastore.DataName;
				
				%write 
				obj.CurrentDataList(i,:) = {Datakeys{i},entryname}		
			end
		
		
		case 'Filter'
			Filterkeys = obj.FilterHash.getKeys;
			
			%get every filterlist and to cell array
			for i=1:numel(Filterkeys)
				filterlist = obj.FilterHash.lookup(Filterkeys{i});
				
				%get name
				entryname = filterlist.FilterName;
				
				%write 
				obj.CurrentFilterList(i,:) = {Filterkeys{i},entryname}		
			end
		
		
		case 'FilterCata'
			Datakeys = obj.DataHash.getKeys;
			Filterkeys = obj.FilterHash.getKeys;
			
			
			%get every data and add to cell array
			for i=1:numel(Datakeys)
				datastore = obj.DataHash.lookup(Datakeys{i});
				
				%get name
				entryname = datastore.DataName;
				
				%write 
				obj.CurrentDataList(i,:) = {Datakeys{i},entryname}		
			end
			
			%get every filterlist and to cell array
			for i=1:numel(Filterkeys)
				filterlist = obj.FilterHash.lookup(Filterkeys{i});
				
				%get name
				entryname = filterlist.FilterName;
				
				%write 
				obj.CurrentFilterList(i,:) = {Filterkeys{i},entryname}		
			end
		
		
		end
		
		obj.EmptyStart=false;
		
		%notify so everyone who needs the list can update
		notify(obj,'HashUpdate');
			
end
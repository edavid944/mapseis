function saver(obj,WhichOne)
	%saves the catalog (only catalog) or the filterlist


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	import mapseis.util.gui.allWaiting;	
					

	[fName,pName] = uiputfile('*.mat','Enter save file name...');
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected.  Data not saved');
		return

	else

		allWaiting('wait');
		
		switch WhichOne
			case 'Cata'
				dataStore = obj.MainGUI.DataStore;
				
				save(fullfile(pName,fName),'dataStore');
				
				allWaiting('move');
				
				msgbox('Catalog saved')
				
			case 'Filter'
				filterList = obj.MainGUI.FilterList;
				filterList.PackIt;
				
				save(fullfile(pName,fName),'filterList');
				
				allWaiting('move');
				
				msgbox('Filterlist saved')

			case 'FilterCata'
				dataStore = obj.MainGUI.DataStore;
				filterList = obj.MainGUI.FilterList;
				filterList.PackIt;
				
				save(fullfile(pName,fName),'dataStore','filterList');
				
				allWaiting('move');
				
				msgbox('Catalog & Filterlist saved')
			
			end
		
		
		
	end
	
	

	
end
function ReApplyOverlay(obj)
	%writes the newest coastline and border into the datastore
	%and allows to change the color and resolution of the overlay
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.plot.GetOverlay;
	import mapseis.util.gui.allWaiting;
	
	Title = 'Reapply Coastlines and Internalborders';
	Prompt={'Resolution:', 'Resolution';...
			'Coastline Color:','Coast_Color';...
			'Internalborder Color:','Border_Color'};
		
	labelList2 =  {	'Crude'; ...
					'Low'; ...
					'Intermediate'; ...
					'High'; ...
					'Full'};

	ColorList = {'black';'blue';'red';'green';'cyan';'magenta'};
	%To come as soon as the plot functions support it
	%ColorList = {'black';'blue';'red';'green';'cyan';'magenta';'grey'};
			
	%Mc Method
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=labelList2;
	Formats(2,1).type='list';
	Formats(2,1).style='popupmenu';
	Formats(2,1).items=ColorList;
	Formats(3,1).type='list';
	Formats(3,1).style='popupmenu';
	Formats(3,1).items=ColorList;
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	defval =struct(	'Resolution',3,...
					'Coast_Color',2,...
					'Border_Color',1);
	
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
	if Canceled == 0
		allWaiting('wait');
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		[Coast_PlotArgs, InternalBorder_PlotArgs] = GetOverlay(datastore.Datastore,NewParameter);
		
		%write into datastore
		datastore.Datastore.setUserData('Coast_PlotArgs',Coast_PlotArgs);
		datastore.Datastore.setUserData('InternalBorder_PlotArgs',InternalBorder_PlotArgs);
		
		
		obj.DataHash.set(key,datastore);
		obj.InitList('Cata');
		obj.PopUp_Update;
		allWaiting('move');
	end	
	
end
classdef CommanderGUI < handle
	%GUI for swichting the catalog and filterlist and a lot of other parameters 	
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	properties
		MyGUI
		MainGUI
		DataMenu
		EditMenu
		SessionMenu
		Importer
		Exporter
		DataEditor
		DataHash
		FilterHash
		CurrentDataID
		%NextDataID
		CurrentFilterID
		%NextFilterID
		DataSelection
		FilterSelection
		CurrentDataList
		CurrentFilterList
		MarkedData
		MarkedFilter
		ListProxy
		ParallelMode       
		EmptyStart
		PlugIn
		MapSeisFolder

	end
	
	events
		HashUpdate
		rebuildMenus
		Switched
	end	
	
	
	methods
		function obj = CommanderGUI(mainGUI,ListProxy)
			import mapseis.gui.*;
			import mapseis.util.gui.*;
			import mapseis.datastore.*;
			
			if nargin<2
				ListProxy=[];		
			end
			
			obj.DataHash = assocArray();
			obj.FilterHash = assocArray();
			obj.CurrentDataID = 0;
			%obj.NextDataID = 1;
			obj.CurrentFilterID = 0;
			%obj.NextFilterID = 1;
			obj.CurrentDataList = {};
			obj.CurrentFilterList = {};
			
			obj.ParallelMode=false;
			
			obj.Importer =[];
			obj.Exporter =[];
			
			if ~isempty(mainGUI)
				obj.MainGUI = mainGUI;
				obj.ListProxy = obj.MainGUI.ListProxy;
				obj.MapSeisFolder=obj.MainGUI.MapSeisFolder;
	
			else
				obj.MainGUI = [];
				obj.ListProxy = ListProxy;
				obj.MapSeisFolder=pwd;
			
			end
			
			obj.ParallelMode=false;		
			
			% Initialise the GUI
			% Create the figure
			try
				Map_handle = obj.MainGUI.MainGUI;
				pos = get(Map_handle,'pos');
			catch
				pos = [0 0 640 480]
			end
			
			%Use 6 for the sizing (cat1,cat2, timediff, spacediff, load cat1 & cat2 buttons)
			winheight=290;
			%pos = [0 0 640 480]
			obj.MyGUI = figure('Name','File Commander',...
					'Position',[pos(1)+pos(3) pos(2)-30 300 winheight],...
					'Toolbar','none','MenuBar','none','NumberTitle','off');
			
			%create all the buttons and stuff
			obj.createLayout;
			obj.setMenu;
			
			%init the filterlists
			obj.InitList('FilterCata')
			         
			% Make the GUI visible
			set(obj.MyGUI,'Visible','on');
			
			
		end
	
		
		%functions located in external files
			
		createLayout(obj)
		
		setMenu(obj)

		switcher(obj,WhichOne)

		saver(obj,WhichOne)
		
		deleter(obj,WhichOne)

		loadFilterCat(obj,filename)

		loadCatalog(obj,filename)
		
		loadFilterlist(obj)

		InitList(obj,WhichOne)

		changeMarker(obj,handle)

		PopUp_Update(obj)
				
		PushWithKey(obj,data,Key)

		pushIt(obj,newdata)
	
		keys = getMarkedKey(obj)

		newfilter(obj)

		newfilterNoUpdate(obj)

		CatalogImport(obj)

		CatalogExport(obj)

		CatalogModify(obj,WhichEdit)

		ExportFinished(obj)

		DataArrived(obj)

		DataEdited(obj)

		datastore = getCurrentDatastore(obj)

		filterlist = getCurrentFilterlist(obj)

		PopUpLists = getCurrentEntries(obj)

		IDs = getCurrentIDs(obj)

		DataNR = getCurrentDatastoreNumber(obj)

		FilterNR = getCurrentFilterlistNumber(obj)

		datastore = resolveDataEntry(obj,val);

		filterlist = resolveFilterEntry(obj,val);

		OutElement = Keyresolver(obj,key,whichone);

		SaveSession(obj,WhichOne)

		LoadSession(obj)

		TheHash = SendSession(obj,WhichOne)

		ReplaceSession(obj,TheHash,WhichOne)

		Filtereditor(obj)

		DataStoreRename(obj)

		WebReload(obj,How)

		SetNewIDs(obj,whichHash)

		ViewFieldInfo(obj)

		ViewCatalogInfo(obj)

		EditFieldInfo(obj)

		EditCatalogInfo(obj)

		Rebuffer(obj)

		ReApplyOverlay(obj)

		setParallel(obj,com)

		setDecluster(obj)

		AutoUpdate(obj,Thestate)

		AddPlugIn(obj,PlugIn,Name)

		RemovePlugIn(obj,PlugIn,Name)	

	end



end



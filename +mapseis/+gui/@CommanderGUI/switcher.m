function switcher(obj,WhichOne)
	%changes the datastore or filterlist

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	import mapseis.util.gui.allWaiting;
	
	allWaiting('wait');
	switch WhichOne
		case 'Cata'
			key = obj.CurrentDataList{obj.MarkedData};
			newdatastore = obj.DataHash.lookup(key);
			loadstructur = struct('Datastore',newdatastore.Datastore,...
								  'ShownDataID',key);		  
		
		case 'Filter'
			key = obj.CurrentFilterList{obj.MarkedFilter};
			newfilterlist = obj.FilterHash.lookup(key);
			loadstructur = struct('Filterlist',newfilterlist.Filterlist,...
					      'ShownFilterID',key);
		
		case 'Both'
			datakey = obj.CurrentDataList{obj.MarkedData};
			newdatastore = obj.DataHash.lookup(datakey);
			filterkey = obj.CurrentFilterList{obj.MarkedFilter};
			newfilterlist = obj.FilterHash.lookup(filterkey);
			loadstructur = struct(	'Datastore',newdatastore.Datastore,...
						'ShownDataID',datakey,...
						'Filterlist',newfilterlist.Filterlist,...
						'ShownFilterID',filterkey);
			
	end
		
	%popup update
	obj.PopUp_Update;

	%change the MainGUI
	obj.MainGUI.MainGUISwitcher(loadstructur);
	
	%refresh plugins
	notify(obj,'rebuildMenus');
	
	allWaiting('move');

end
function DataStoreRename(obj)
	%Allow to change the name of the filterlist at the moment
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	key = obj.CurrentDataList{obj.MarkedData};
	datastore = obj.DataHash.lookup(key);
	
	Dataname=datastore.DataName;
	
	promptValues = {'Datastore Name'};
	dlgTitle = 'Rename the Catalog';
	
	gParams = inputdlg(promptValues,dlgTitle,1,{Dataname});
	NewDataName=gParams{1};
	
	datastore.Datastore.setUserData('Name',NewDataName);
	datastore.DataName=NewDataName;
	
	obj.DataHash.set(key,datastore);
	obj.InitList('Cata');
	obj.PopUp_Update;
	
end	
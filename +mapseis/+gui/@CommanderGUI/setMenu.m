function setMenu(obj)
	%generates the top menu bar	


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	g = obj.MyGUI;
	
	obj.DataMenu = uimenu(g,'Label','- Data');
	uimenu(obj.DataMenu,'Label','Load Catalog/Filterlist',... 
		'Callback',@(s,e) obj.loadFilterCat);
	uimenu(obj.DataMenu,'Label','Import Catalog',...
		'Callback',@(s,e) obj.CatalogImport);
	uimenu(obj.DataMenu,'Label','Reload Catalog from Web','Separator','on',...
		'Callback',@(s,e) obj.WebReload('reload'));
	uimenu(obj.DataMenu,'Label','Update Catalog from Web',...
		'Callback',@(s,e) obj.WebReload('update'));
	uimenu(obj.DataMenu,'Label','Check last update',...
		'Callback',@(s,e) obj.WebReload('check'));    
	uimenu(obj.DataMenu,'Label','Save Catalog','Separator','on',...
		'Callback',@(s,e) obj.saver('Cata'));
	uimenu(obj.DataMenu,'Label','Save Catalog & Filterlist',...
		'Callback',@(s,e) obj.saver('FilterCata'));
	uimenu(obj.DataMenu,'Label','Save Filterlist',...
		'Callback',@(s,e) obj.saver('Filter'));
	uimenu(obj.DataMenu,'Label','Export Catalog',...
		'Callback',@(s,e) obj.CatalogExport);
	
	
	obj.EditMenu = uimenu(g,'Label','- Edit');
	uimenu(obj.EditMenu,'Label','Parallel Calculation',...
		'Callback',@(s,e) obj.setParallel('set'));
	uimenu(obj.EditMenu,'Label','Reapply datastore buffers',...
		'Callback',@(s,e) obj.Rebuffer);
	uimenu(obj.EditMenu,'Label','Reapply Coast&Borders',...
		'Callback',@(s,e) obj.ReApplyOverlay);     
	uimenu(obj.EditMenu,'Label','Switch Declustering','Separator','on',...
		'Callback',@(s,e) obj.setDecluster);
	uimenu(obj.EditMenu,'Label','Rename Catalog Field','Separator','on',...
		'Callback',@(s,e) obj.CatalogModify('Renamer'));
	uimenu(obj.EditMenu,'Label','Select Basic Fields',...
		'Callback',@(s,e) obj.CatalogModify('Fielder'));
	uimenu(obj.EditMenu,'Label','View Field Description','Separator','on',...
		'Callback',@(s,e) obj.ViewFieldInfo);
	uimenu(obj.EditMenu,'Label','Edit Field Description',...
		'Callback',@(s,e) obj.EditFieldInfo);
	uimenu(obj.EditMenu,'Label','View Catalog Comments','Separator','on',...
		'Callback',@(s,e) obj.ViewCatalogInfo);
	uimenu(obj.EditMenu,'Label','Edit Catalog Comments',...
		'Callback',@(s,e) obj.EditCatalogInfo);
	
	obj.SessionMenu = uimenu(g,'Label','- Session');   	
	uimenu(obj.SessionMenu,'Label','Load Session',...
		'Callback',@(s,e) obj.LoadSession);
	uimenu(obj.SessionMenu,'Label','Save Session',...
		'Callback',@(s,e) obj.SaveSession('BothHash'));  	
	uimenu(obj.SessionMenu,'Label','Save DataStore Hash',...
		'Callback',@(s,e) obj.SaveSession('DataHash'));  
	uimenu(obj.SessionMenu,'Label','Save FilterList Hash',...
		'Callback',@(s,e) obj.SaveSession('FilterHash'));     	
	
	
	%set Parallel mode flag
	obj.setParallel('chk');
	
end
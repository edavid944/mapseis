function ReplaceSession(obj,TheHash,WhichOne)
	%Allows to load the whole filter or datastore hash(es)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	switch WhichOne
		case 'DataHash'
			obj.DataHash.replaceHash(TheHash);
			obj.MarkedData=1;
							
			%set the the lastkey right
			lastKey=max(cellfun(@str2num,TheHash(:,1)));
			obj.CurrentDataID=num2str(lastKey);
			
			obj.InitList('Cata');
			
		case 'FilterHash'
			obj.FilterHash.replaceHash(TheHash);
			obj.MarkedFilter=1;
			
			%set the the lastkey right
			lastKey=max(cellfun(@str2num,TheHash(:,1)));
			obj.CurrentFilterID=num2str(lastKey);
			
			obj.InitList('Filter');
			
		case 'BothHash'
			obj.DataHash.replaceHash(TheHash{1});
			obj.FilterHash.replaceHash(TheHash{2});
			obj.MarkedData=1;
			obj.MarkedFilter=1;
			
			DataHash=TheHash{1};
			FilterHash=TheHash{2};
			
			%set the the lastkey right
			lastKey=max(cellfun(@str2num,DataHash(:,1)));
			obj.CurrentDataID=num2str(lastKey)
			lastKey=max(cellfun(@str2num,FilterHash(:,1)));
			obj.CurrentFilterID=num2str(lastKey);
			
			obj.InitList('FilterCata');
			
	end
	
	%Check if everyone has a random ID
	obj.SetNewIDs(WhichOne);
	
	obj.PopUp_Update;
	
	notify(obj,'HashUpdate');
	
	%Update the MainGUI;
	datakey = obj.CurrentDataList{obj.MarkedData};
	newdatastore = obj.DataHash.lookup(datakey);
	filterkey = obj.CurrentFilterList{obj.MarkedFilter};
	newfilterlist = obj.FilterHash.lookup(filterkey);

		
	loadstructur = struct(	'Datastore',newdatastore.Datastore,...
							'ShownDataID',datakey,...
							'Filterlist',newfilterlist.Filterlist,...
							'ShownFilterID',filterkey);
	
	obj.MainGUI.MainGUISwitcher(loadstructur);
	
	%refresh plugins
	notify(obj,'rebuildMenus');
	
end
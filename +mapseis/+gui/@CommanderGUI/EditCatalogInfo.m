function EditCatalogInfo(obj)
	%Opens a GUI which allows to edit the catalogue information
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	key = obj.CurrentDataList{obj.MarkedData};
	datastore = obj.DataHash.lookup(key);
	
	dlgTitle = 'Edit the Catalog Comments';
	
	try
		CatalogInfo=datastore.Datastore.getUserData('CatalogInfo');
	catch
		CatalogInfo='';
	end
	
	%options for the window
	options.Resize='on';
	options.WindowStyle='normal';
	options.Interpreter='tex';
	
	gParams = inputdlg('Catalog Comments',dlgTitle,10,{CatalogInfo},options);
	CatalogInfo=gParams{1};
	
	datastore.Datastore.setUserData('CatalogInfo',CatalogInfo);
	
	obj.DataHash.set(key,datastore);
	obj.InitList('Cata');
	obj.PopUp_Update;

end
function PointImport(MainGUI,imobj,RegionFilter,FilterType)
	%This function loads a ascii file and set the regionfilter with the coordinates
	%and radius or profile width. The format is depended on the filtertype.
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard
	
	import mapseis.util.importfilter.*;
	import mapseis.util.*;
	
	[fName,pName] = uigetfile('*.dat','Select File...');
	
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected.  Data not loaded');
		return;
	else
		%create file
		fid =fopen([pName fName]) ;
		inStr = fscanf(fid,'%c'); % Read the entire text file into an array
		fclose(fid);
		
		%stuff for the reading
		rp = regexpPatternCatalog();
		ws = rp.ws;
		linepattern=rp.line;
		
		%read lines
		TheLines=regexp(inStr,linepattern,'match');
		
		%separate entries
		matches = regexp(TheLines,'\s+','split');
		matches = matches(cellfun(@(x) ~isempty(x),matches));
		
		%build coordinate matrix
		for i=1:numel(matches)
			singleLine=matches{i};
			singleLine = singleLine(cellfun(@(x) ~isempty(x),singleLine));
			NewCoords(i,:)=cellfun(@(x) str2num(x),singleLine);
		end
		
		%Should not be needed, left standing in case it is needed later, else
		%DELETE
		%--------
		%Get Region from FilterRegion
		%SavedFilterRegion=getRegion(RegionFilter);
		%Regio=filterRegion{2};
		%-----------
		
		
		switch FilterType
			case 'polygon'
				%The data should be saved as points, first coulomn Longitude
				%second couloumn Latitude
				Coord=[NewCoords(:,1),NewCoords(:,2)];	
				imobj.setPosition(Coord);
			
			case 'line'
				%The two points should be saved together with the width of the 
				%profile in the third coulomn. 
				
				Coord=[NewCoords(:,1),NewCoords(:,2)];
				ProfWidth=NewCoords(1,3)
				
				%Update Width in datastore
				RegionFilter.setWidth(ProfWidth);
				
				imobj.setPosition(Coord);
			
			case 'circle'
				%In this case the middle point and the radius should  be 
				%saved in one line
				Coord=[NewCoords(1,1),NewCoords(1,2)];
				Radius=NewCoords(1,3);
				
				%Update Radius in the datastore
				RegionFilter.setRadius(Radius);
				
				imobj.setPosition(Coord);
			
			
		end
			
		%Update the filter
		MainGUI.ParamGUI.updateGUI;
	
	end
	
end

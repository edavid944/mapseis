classdef CatControlGUI_Com < handle
    % CatControlGUI : GUI frontend for the MapSeis application parameters
    
    % $Revision: 1 $    $Date: 2009-04-28  $
    % Author: David Eberhard
    
    %This version can be used with the Commander
    
    properties
        DataStore_A
        DataStore_B
        Filterlist
        Select_A
        Select_B
        Commander
        CatGUI
        dtime
        distan
        screenA
        screenB
        FiltSelect
        DataEntry
        FilterEntry
        
    end
    
    events
        Calculate
    end
    	
    
    methods
        function obj = CatControlGUI_Com(Commander)
            import mapseis.projector.*;
            
            % builds the gui, which is a controller and dataentry for the main compare Catalog gui
            obj.Commander=Commander;
            
           
            %Init Variables
            obj.DataStore_A = [];
            obj.DataStore_B = [];
            obj.Filterlist = [];
            obj.Select_A = [];
            obj.Select_B = [];           
            
            PopUpLists = getCurrentEntries(obj.Commander);           
            obj.DataEntry=PopUpLists.Data;
            obj.FilterEntry=PopUpLists.Filter;
            
            
            
            % Initialise the GUI
            
            % Create the figure
            try
            Map_handle = findobj('name','CompareGUI Main Window');
            pos = get(Map_handle,'pos');
            catch
            pos = [0 0 640 480]
            end
            
            %Use 6 for the sizing (cat1,cat2, timediff, spacediff, load cat1 & cat2 buttons)
            winheight=500;
            %pos = [0 0 640 480]
            obj.CatGUI = figure('Name','Compare Parameters',...
                'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 360 winheight],...
                'Toolbar','none','MenuBar','none','NumberTitle','off');
            
            %create all the buttons and stuff
            obj.createLayout;
            
    
            
            
            
            
            %            
            % Make the GUI visible
            set(obj.CatGUI,'Visible','on');
            
        end
        
        function updateGUI(obj)
        	
        	%get the two datastore and the filter
        	Key_A = obj.DataEntry{get(obj.screenA,'Value'),1};
        	Key_B = obj.DataEntry{get(obj.screenB,'Value'),1};
        	FiltKey = obj.FilterEntry{get(obj.FiltSelect,'Value'),1};
        	
        	obj.DataStore_A = obj.Commander.Keyresolver(Key_A,'data');
        	obj.DataStore_B = obj.Commander.Keyresolver(Key_B,'data');
        	obj.Filterlist = obj.Commander.Keyresolver(FiltKey,'filter');
        	
        	%now get the selected of each datastore
        	obj.Filterlist.changeData(obj.DataStore_A);
        	obj.Filterlist.update;
        	obj.Select_A=obj.Filterlist.getSelected;
        	obj.Filterlist.changeData(obj.DataStore_B);
        	obj.Filterlist.update;
        	obj.Select_B=obj.Filterlist.getSelected;
        	
            	%sends a notification to the main catControl           
        	notify(obj,'Calculate');
        end
        
        
       

        
        
           
                
        function createLayout(obj)
        	import mapseis.gui.*;
        	
        	h1 = obj.CatGUI;
        	bgcol = get(obj.CatGUI,'Color');
        	 	
			 obj.screenA = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],...
			'FontSize',10,...
			'Position',[2.5 13.3 45.5 2.66666666666667],...
			'String',obj.DataEntry(:,2),...
			'Style','popupmenu',...
			'Tag','cataA');
			
			
					
			
			
			 obj.screenB = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],...
			'FontSize',10,...
			'Position',[2.66666666666667 7.5 45.5 2.66666666666667],...
			'String',obj.DataEntry(:,2),...
			'Style','popupmenu',...
			'Tag','cataB');
			
			
			h12 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[15 4.08333333333333 22.1666666666667 1.41666666666667],...
			'String','Filter',...
			'Style','text',...
			'Tag','text9' );

			
			
			 obj.FiltSelect = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[0.941176470588235 0.941176470588235 0.941176470588235],...
			'FontSize',10,...
			'Position',[2.66666666666667 0.75 45.5 2.66666666666667],...
			'String',obj.FilterEntry(:,2),...
			'Style','popupmenu',...
			'Tag','FilterSelect');
			
			
			
			h6 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'Callback',@(s,e) obj.updateGUI(),...
			'FontSize',12,...
			'FontWeight','bold',...
			'Position',[2.33333333333333 31.5 45 3.33333333333333],...
			'String','Calculate',...
			'Tag','CalcButton');
			
			
			obj.distan = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[1 1 1],...
			'FontSize',10,...
			'Position',[33.1666666666667 26.1 12.6666666666667 2.5],...
			'String','10',...
			'Style','edit',...
			'Tag','distanEntry');
			
			
			
			obj.dtime = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'BackgroundColor',[1 1 1],...
			'FontSize',10,...
			'Position',[33.1666666666667 21.6 12.6666666666667 2.5],...
			'String','0.25',...
			'Style','edit',...
			'Tag','timeEntry');
			
			
			
			h9 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[2 26.1 24.8333333333333 1.75],...
			'String','Maximal Distance [km]',...
			'Style','text',...
			'Tag','text6');
			
			
			
			h10 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[2 21.6 27.8333333333333 1.83333333333333],...
			'String','Max. Time difference [min]',...
			'Style','text',...
			'Tag','text7');
			
			
			
			h11 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[15 16.3 21.5 1.58333333333333],...
			'String','Catalog A',...
			'Style','text',...
			'Tag','text8');
			
			
			h12 = uicontrol(...
			'Parent',h1,...
			'Units','characters',...
			'FontSize',12,...
			'BackgroundColor',bgcol,...
			'Position',[15 10.7 22.1666666666667 1.41666666666667],...
			'String','Catalog B',...
			'Style','text',...
			'Tag','text9' );


        
        
        end
        
        function rebuildGUI(obj)
        	%allows to rebuild the closed gui
        	
        	import mapseis.projector.*;
        	
        	try
        		 set(obj.mainGUI,'Visible','on');	
        	
        	catch
        		 	%delete(obj.mainGUI);	
		        	dataStore = obj.DataStore;
		            filterList = obj.FilterList;
		            
		            rangeFilters = filterList.getRangeFilters();
		            filterCount = numel(rangeFilters);
		
		        	
		        	
		        	for filterIndex=1:filterCount
		                paramStruct(filterIndex) = ...
		                    mapseis.gui.ParamGUI.initFilter(dataStore,...
		                    rangeFilters{filterIndex});
		            end
		            
		            %obj.RangeFilters = rangeFilters;
		            
		            obj.ParamWidgets = cell(numel(paramStruct),1);
					
					      	
		        			        	
		        	% Create the figure
		            Map_handle = findobj('name','MapSeis Main Window');
		            pos = get(Map_handle,'pos');
		            
		            %Added automatic resizing of the paramwindow
		            winheight=filterCount*80;
		            
		            obj.mainGUI = figure('Name','Filter Parameters',...
		                'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight],...
		                'Toolbar','none','MenuBar','none','NumberTitle','off');
		            % Create a uipanel in the figure to store the filter parameter widgets
		            paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
		                'Position',[0 0 1 0.8]);
		            % Create the widgets inside the uipanel
		            obj.createParamWidgets(paramPanel,paramStruct);
		            % Create a button to call the update
		            updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
		                'Position',[0 0.85 0.5 0.1],'String','Update',...
		                'Callback',@(s,e) obj.updateGUI());
		            % Register this view with the model.  The subject of the view uses this
		            % function handle to update the view.
		            %dataStore.subscribe('ParamGUI',@() updateGUI(obj));
		            obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI);
		            %             evStruct.subscribe('ParamGUI',@() nUpdateGUI(gui));
		            
		            % Update the GUI to plot the initial data
		            obj.updateGUI();
		            
		            % Make the GUI visible
		            set(obj.mainGUI,'Visible','on');

        	end
        
        
        end
        
        
    end
    
       
end
function setProfileWidthGUI(datastore,regionFilter)
% setProfileWidthGUI : Dialog to set the width of depth slice

% $Revision: 1 $    $Date: 2008-12-12 
% Author: David Eberhard

import mapseis.util.*;

if isempty(regionFilter.Width)
	% Get the old Profile width parameters
	oldW = datastore.getUserData('ProfileWidth');
	oldWidth=deg2km(2*oldW);
else
	oldWidth=deg2km(2*regionFilter.Width)
end

% Dialog fields 
promptValues = {'Width [km]'};
dlgTitle = 'Selection width of the Depth Profile';

% Construct the dialog box and read the values
gParams = inputdlg(promptValues,dlgTitle,1,...
    cellfun(@num2str,{oldWidth},'UniformOutput',false));
newW = str2double(gParams{1});
newWidth = km2deg(newW/2);

% Set the new grid parameters
regionFilter.Width=newWidth;

end
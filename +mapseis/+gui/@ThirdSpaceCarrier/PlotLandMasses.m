function PlotLandMasses(obj)
	%plots only the landmasses
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	import mapseis.plot.*;
	import mapseis.projector.*;
	
	
	if isempty(obj.plotAxis)
		obj.plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(obj.plotAxis)
			%try retag
			%get all childrens of the main gui
			gracomp = get(obj.ResultGUI, 'Children');
			
			%find axis and return axis
			obj.plotAxis = findobj(gracomp,'Type','axes','Tag','');
			set(obj.plotAxis,'Tag','MainResultAxis');
		end	
	
	end
	
	
	plotAxis=obj.plotAxis;
	
	[xlimiter ylimiter zlimiter Selected UnSelected] = ReturnLimits(obj);
	
	if obj.LandToggle
		
		%plot3(plotAxis,mean(xlimiter),mean(ylimiter),0,'Color','w');
		%hold on
		LandConf= struct( 	'PlotType','LandMass',...
							'Data',obj.Datastore,...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'X_Axis_Limit',[xlimiter(1)-10,xlimiter(2)+10],...
							'Y_Axis_Limit',[ylimiter(1)-10,ylimiter(2)+10],...
							'CutArea',true,...
							'LineStylePreset','none',...
							'Colors','landgreen',...
							'SmallestMass',50,...
							'Transparence',obj.AlphaMan,...
							'Dimension','3D');
		try		   	  
			[LandHand LandEntry] = PlotLandMass(plotAxis,LandConf);
		catch
			%use larger area
			LandConf.SmallestMass=5;
			try
				[LandHand LandEntry] = PlotLandMass(plotAxis,LandConf);
			catch
				disp('Now LandMasses drawn')
			end	
		end

	
	end

end
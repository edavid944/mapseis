function buildMenus(obj)
	%sets the plot mode in plot mode, 'check' can be used to set the ticks in the menu
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
	
	uimenu(obj.PlotOptions,'Label','Redraw',... 
		'Callback',@(s,e) obj.updateGUI);
	uimenu(obj.PlotOptions,'Label','Set superelevation',... 
		'Callback',@(s,e) obj.setSuperElevation);
	uimenu(obj.PlotOptions,'Label','Set Transparency',... 
		'Callback',@(s,e) obj.setAlphaMan);
	
	uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
		'Callback',@(s,e) obj.setqual('low'));
	uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
		'Callback',@(s,e) obj.setqual('med'));
	uimenu(obj.PlotOptions,'Label','High Quality Plot',...
		'Callback',@(s,e) obj.setqual('hi'));
	
	uimenu(obj.PlotOptions,'Label','Plot all Earthquakes','Separator','on',... 
		'Callback',@(s,e) obj.SetPlotMode('full'));
	uimenu(obj.PlotOptions,'Label','Main Window mode',...
		'Callback',@(s,e) obj.SetPlotMode('region'));	
	uimenu(obj.PlotOptions,'Label','Only Selected Earthquakes',...
		'Callback',@(s,e) obj.SetPlotMode('light'));
	
	uimenu(obj.PlotOptions,'Label','Draw Coastlines','Separator','on',... 
		'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
	uimenu(obj.PlotOptions,'Label','Draw Country Borders',...
		'Callback',@(s,e) obj.TooglePlotOptions('Border'));
	uimenu(obj.PlotOptions,'Label','Draw Shadows',...
		'Callback',@(s,e) obj.TooglePlotOptions('Shadow'));
	uimenu(obj.PlotOptions,'Label','Draw LandMasses',...
		'Callback',@(s,e) obj.TooglePlotOptions('Land'));
	
	obj.TooglePlotOptions('-chk');
	obj.setqual('chk');
	obj.SetPlotMode('check');
	
end
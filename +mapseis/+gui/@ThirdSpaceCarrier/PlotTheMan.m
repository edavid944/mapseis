function PlotTheMan(obj)
	%plots the carried data
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	import mapseis.plot.*;
	import mapseis.projector.*;
	
	
	if isempty(obj.plotAxis)
		obj.plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(obj.plotAxis)
			%try retag
			%get all childrens of the main gui
			gracomp = get(obj.ResultGUI, 'Children');
			
			%find axis and return axis
			obj.plotAxis = findobj(gracomp,'Type','axes','Tag','');
			set(obj.plotAxis,'Tag','MainResultAxis');
		end	
		
	end
	
	
	plotAxis=obj.plotAxis;
	
	if ~isempty(obj.ManInTheMiddle)
		if isempty(obj.TheManParameter) 
			if obj.SelfSend
				obj.ManInTheMiddle(plotAxis,obj);
			else
				obj.ManInTheMiddle(plotAxis);
			end
	
		else
			if obj.SelfSend
				obj.ManInTheMiddle(plotAxis,obj,obj.TheManParameter);
			else
				obj.ManInTheMiddle(plotAxis,obj.TheManParameter);
			end
	
		end	
	
	end
	
end
function [xlimiter ylimiter zlimiter Selected UnSelected] = ReturnLimits(obj)
	%returns the axis limits
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	import mapseis.projector.*;
	
	
	regionFilter = obj.Filterlist.getByName('Region');	
	TheRegion = getRegion(regionFilter);
	RegRange=TheRegion{1};
	
	
	
	if strcmp(obj.PlotMode,'full')
		Selected=obj.SendedEvents;
		UnSelected=~obj.SendedEvents;
		[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
		[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
		[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
		
		xmini=min([min(selectedLonLat(:,1)),min(unselectedLonLat(:,1))]);
		xmaxi=max([max(selectedLonLat(:,1)),max(unselectedLonLat(:,1))]);
		xlimiter=[xmini xmaxi];
		
		ymini=min([min(selectedLonLat(:,2)),min(unselectedLonLat(:,2))]);
		ymaxi=max([max(selectedLonLat(:,2)),max(unselectedLonLat(:,2))]);
		ylimiter=[ymini ymaxi];
		
		zmini=min([min(selectedDepth),min(unselectedDepth)]);
		zmaxi=max([max(selectedDepth),max(unselectedDepth)]);
		zlimiter=[zmini zmaxi];
	
	elseif strcmp(obj.PlotMode,'region')
		Selected=obj.SendedEvents;
		UnSelected=obj.Filterlist.excludeFilter('Region');
		
		[selectedLonLat,temp] = getLocations(obj.Datastore,Selected);
		[selectedDepth,temp] = getDepths(obj.Datastore,Selected);
		[selectedMag,temp] = getMagnitudes(obj.Datastore,Selected);
		[unselectedLonLat,temp] = getLocations(obj.Datastore,UnSelected);
		[unselectedDepth,temp] = getDepths(obj.Datastore,UnSelected);
		[unselectedMag,temp] = getMagnitudes(obj.Datastore,UnSelected);
		
		xmini=min([min(selectedLonLat(:,1)),min(unselectedLonLat(:,1))]);
		xmaxi=max([max(selectedLonLat(:,1)),max(unselectedLonLat(:,1))]);
		xlimiter=[xmini xmaxi];
		
		ymini=min([min(selectedLonLat(:,2)),min(unselectedLonLat(:,2))]);
		ymaxi=max([max(selectedLonLat(:,2)),max(unselectedLonLat(:,2))]);
		ylimiter=[ymini ymaxi];
		
		zmini=min([min(selectedDepth),min(unselectedDepth)]);
		zmaxi=max([max(selectedDepth),max(unselectedDepth)]);
		zlimiter=[zmini zmaxi];
	
	
	else
		Selected=obj.SendedEvents;
		UnSelected=obj.SendedEvents;
		[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
		[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
		[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
		
		xlimiter=[min(selectedLonLat(:,1)) max(selectedLonLat(:,1))];
		ylimiter=[min(selectedLonLat(:,2)) max(selectedLonLat(:,2))];
		zlimiter=[min(selectedDepth) max(selectedDepth)];
	end
	
	
	
end
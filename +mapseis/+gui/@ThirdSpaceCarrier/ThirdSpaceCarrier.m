classdef ThirdSpaceCarrier < handle
	%Similar to ThirdSpace but it is not automatic and allows to additional 
	%data to the plot like colormaps on the floor and colored dots
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	properties
		ListProxy
		Commander
		Datastore
		SendedEvents
		Keys
		MainWindowGUI
		Filterlist
		ErrorDialog
		Region
		ResultGUI
		PlotQuality
		BorderToogle
		CoastToogle
		ShadowToogle
		LandToggle
		PlotOptions
		plotAxis
		PlotMode
		TheAspect
		AlphaMan
		ManInTheMiddle
		TheManParameter
		SelfSend
		CustomUpdate
		UpdateMode
		SpareParameter
	end
	
	events
		CalcDone
	end
	
	
	methods
		function obj = ThirdSpaceCarrier(ListProxy,Commander,MainWindowGUI,PlotMode)
			import mapseis.datastore.*;
			
			%The plotmode defines if everything ('full') is plotted from the MainWindow
			%(selected unselected and Polygons, etc) or if only the selected events are
			%Plotted ('light'), with 'region' the same mode like the maingui uses is
			%used, draw only selected by filterlist but differentiate between regionfilter
			%selected events;
			
			%ManInTheMiddle can be function handle it will be before the earthquakes and after
			%the coastlines and area
			%has to have the input parameters like following: MySuperFunction(plotAxis) or if 
			%TheManParameter is used (not empty), MySuperFunction(plotAxis,TheManParameter) 
			%TheManParameter can be used to send additional parameters to the CustomFunction.
			
			%The variable CustomUpdate can be used to bypass the existing standard update function
			%and replace it with a external one (has to be a function handle with the object as input)
			
			%If SelfSend is set to true the gui send itself to the  ManInTheMiddle 
			%plot function, the handle have then to be like following:
			%MySuperFunction(plotAxis,ThirdSpaceCarrier,(TheManParameter)) 
			
			
			obj.ListProxy=ListProxy;
			obj.Commander=Commander;
			%obj.PlotMode=PlotMode;
			obj.PlotMode='light';		
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.TheAspect=10;
			obj.AlphaMan='none';
			obj.BorderToogle=false;
			obj.CoastToogle=false;
			obj.ShadowToogle=true;
			obj.LandToggle=false;
			obj.ManInTheMiddle=[];
			obj.TheManParameter=[];
			obj.CustomUpdate=[];
			obj.UpdateMode='normal';
			obj.SelfSend=false;
			obj.SpareParameter=[];
			
			
			%check if a profile is selected
			regionFilter = obj.Filterlist.getByName('Region');
			
			
			obj.ListProxy.PrefixQuiet('3DWindow');
			obj.ListProxy.listen(obj.Commander, 'Switched', @() obj.DataSwitcher,'3DWindow');
			if ~isempty(MainWindowGUI)
				%normal case, get everything form the mainGUI
				obj.MainWindowGUI=MainWindowGUI;
				obj.setqual('chkData');
				obj.ListProxy.listen(obj.MainWindowGUI, 'MainWindowUpdate', @() obj.updateGUI,'3DWindow');
			else
				obj.MainWindowGUI=[];
				obj.PlotQuality = 'low';
				
			end
			
			%DO NOTHING AUTOMATICALLY
		
		end
		
		%external file methods
		DataSwitcher(obj)
		
		SetMiddleMan(obj,TheMan,TheParam)
		
		SetMiddleParam(obj,TheParam)
		
		SetUpdateFunction(obj,Updater)
		
		BuildResultGUI(obj)
		
		buildMenus(obj)
		
		updateGUI(obj)
		
		org_updateGUI(obj)
		
		plotAxis=getPlotAxis(obj)
		
		PlotLandMasses(obj)
		
		PlotTheMan(obj)
		
		PlotEarthquakes(obj)
		
		plotCoastBorder(obj)
		
		plotShadow(obj)
		
		FinalizePlot(obj)
		
		setSuperElevation(obj)
		
		setAlphaMan(obj)
		
		SetPlotMode(obj,WhichMode)
		
		[xlimiter ylimiter zlimiter Selected UnSelected] = ReturnLimits(obj)
		
		Rebuild(obj)
		
		setqual(obj,qual)
		
		TooglePlotOptions(obj,WhichOne)

		
		
	end
end
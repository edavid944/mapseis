function FinalizePlot(obj)
	%The last bit of the plot
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	import mapseis.plot.*;
	import mapseis.projector.*;
	
	
	if isempty(obj.plotAxis)
		obj.plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
		if isempty(obj.plotAxis)
			%try retag
			%get all childrens of the main gui
			gracomp = get(obj.ResultGUI, 'Children');
			
			%find axis and return axis
			obj.plotAxis = findobj(gracomp,'Type','axes','Tag','');
			set(obj.plotAxis,'Tag','MainResultAxis');
		end	
	
	end
	
	[xlimiter ylimiter zlimiter Selected UnSelected] = ReturnLimits(obj);
	
	plotAxis=obj.plotAxis;
	
	%set all axes right
	xlim(plotAxis,xlimiter);
	ylim(plotAxis,ylimiter);
	zlim(plotAxis,zlimiter);
	
	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(ylimiter)) obj.TheAspect]);
	set(plotAxis,'ZDir','reverse');
	box off
	set(plotAxis,'DrawMode','fast','XColor',[0.7 0.7 0.7],'YColor',[0.7 0.7 0.7],'ZColor',[0.7 0.7 0.7]);
	hold off
	
end
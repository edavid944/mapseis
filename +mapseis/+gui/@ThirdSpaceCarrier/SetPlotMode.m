function SetPlotMode(obj,WhichMode)
	%sets the plot mode in plot mode, 'check' can be used to set the ticks in the menu
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	Menu2Check(3) = findobj(obj.PlotOptions,'Label','Plot all Earthquakes');
	Menu2Check(2) = findobj(obj.PlotOptions,'Label','Main Window mode');
	Menu2Check(1) = findobj(obj.PlotOptions,'Label','Only Selected Earthquakes');
	
	%uncheck all menupoints
	set(Menu2Check(1), 'Checked', 'off');
	set(Menu2Check(2), 'Checked', 'off');
	set(Menu2Check(3), 'Checked', 'off');
	
	switch WhichMode
		case 'check'
			
			switch obj.PlotMode
				case 'full'
					sel=3;
				case 'region'
					sel=2;	
				case 'light'
					sel=1;
			end		
			set(Menu2Check(sel), 'Checked', 'on');
			
		case 'light'
			obj.PlotMode='light';
			set(Menu2Check(1), 'Checked', 'on');
			obj.updateGUI;
			
		case 'region'
			obj.PlotMode='region';
			set(Menu2Check(2), 'Checked', 'on');
			obj.updateGUI;
			
		case 'full'
			obj.PlotMode='full';
			set(Menu2Check(3), 'Checked', 'on');
			obj.updateGUI;
		
	end

end
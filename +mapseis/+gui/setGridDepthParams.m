function setGridDepthParams(datastore)
% setGridParams : set the calculation grid parameters from input dialog

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

import mapseis.util.*;

% Get the old grid spacing parameters
[oldSep,oldRad] = GetGridDepthParameters(datastore,'gridDepthPars');

% Dialog fields 
promptValues = {'Grid Depth spacing','Averaging region'};
dlgTitle = 'Calculation Depth grid parameters';

% Construct the dialog box and read the values
gParams = inputdlg(promptValues,dlgTitle,1,...
    cellfun(@num2str,{oldSep,oldRad},'UniformOutput',false));
gridDepthPars.gridSep = str2double(gParams{1});
gridDepthPars.rad = str2double(gParams{2});

% Set the new grid parameters
datastore.setUserData('gridDepthPars',gridDepthPars);
end
function FilterType = PointImport_newPoly(MainGUI,Datastore,RegionFilter)
	%Similar to PointImport but it generates a polygon itself, this allows import points without first
	%generate a unneeded polygon

	%UNFINISHED

	import mapseis.util.importfilter.*;
	import mapseis.util.*;
	import mapseis.region.*;
	import mapseis.projector.*;
	import mapseis.util.gui.SetImMenu;

	%Ask what filter should be imported
	Title = 'Choose Filter Type';
	Prompt={'Filter Type:', 'FilterType'};
		
	labelList2 =  {	'1: Inside Polygon'; ...
					'2: Profile'; ...
					'3: OutSide Polygon'; ...
					'4: Cirular Region'}; 
	KeyWordList={'in';'line';'out';'circle'};	

	%Mc Method
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=labelList2;
	Formats(1,2).type='none';
	Formats(1,1).size = [-1 0];
	Formats(1,2).limits = [0 1];

	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';

	DefaultParameters.FilterType=1;
	
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,DefaultParameters,Options); 
		
	if Canceled==1
		return;
	end
	
	FilterType=KeyWordList{NewParameter. FilterType};

	%ask for the file
	[fName,pName] = uigetfile('*.dat','Select File...');
	
	if isequal(fName,0) || isequal(pName,0)
		disp('No filename selected.  Data not loaded');
		return;
	else
		%create file
		fid =fopen([pName fName]) ;
		inStr = fscanf(fid,'%c'); % Read the entire text file into an array
		fclose(fid);
		
		%stuff for the reading
		rp = regexpPatternCatalog();
		ws = rp.ws;
		linepattern=rp.line;
		
		%read lines
		TheLines=regexp(inStr,linepattern,'match');
		
		%separate entries
		matches = regexp(TheLines,'\s+','split');
		matches = matches(cellfun(@(x) ~isempty(x),matches));
		
		%build coordinate matrix
		for i=1:numel(matches)
			singleLine=matches{i};
			singleLine = singleLine(cellfun(@(x) ~isempty(x),singleLine));
			NewCoords(i,:)=cellfun(@(x) str2num(x),singleLine);
		end
		
		%Should not be needed, left standing in case it is needed later, else
		%DELETE
		%--------
		%Get Region from FilterRegion
		%SavedFilterRegion=getRegion(RegionFilter);
		%Regio=filterRegion{2};
		%-----------

		plotAxis = findobj(MainGUI.MainGUI,'Tag','axis_left');
		if isempty(plotAxis)
			plotAxis=gca;
		end		

		
		disp(FilterType)

		switch FilterType
			case {'in','out','polygon'}
				%The data should be saved as points, first coulomn Longitude
				%second couloumn Latitude
				Coord=[NewCoords(:,1),NewCoords(:,2)];	
				
				polyg=impoly(plotAxis, Coord);
       		    newRegion = Region(polyg.getPosition);
         	 	%newRegion = Region(pts);
          	  	RegionFilter.setRegion(FilterType,newRegion);
				
			case 'line'
				%The two points should be saved together with the width of the 
				%profile in the third coulomn. 
				
				Coord=[NewCoords(:,1),NewCoords(:,2)];
				ProfWidth=NewCoords(1,3)
				
				polyg=imline(plotAxis, Coord);
         	   	pline=polyg.getPosition;
         
				%Update Width in datastore
				RegionFilter.setWidth(ProfWidth);
				
				Regio=Slicer(pline, ProfWidth);
				
				%Create DepthRegion
	            [Selected,Unselected] = getDepths(Datastore);
	         	[lat lon dep] = depthpoly(pline,Selected);
	         	Dregion = DepthRegion(lat,lon,dep);
	         	
	            RegionFilter.setRegion('line',Regio.pRegion);
	            RegionFilter.setDepthRegion(Dregion, ProfWidth);
			
			case 'circle'
				%In this case the middle point and the radius should  be 
				%saved in one line
				Coord=[NewCoords(1,1),NewCoords(1,2)];
				Radius=NewCoords(1,3);
			
				
				RegionFilter.setRadius(Radius);
				
				polyg=impoint(plotAxis, Coord);
		       
		        RegionFilter.setRegion(FilterType, Coord);

				%Update Radius in the datastore
				RegionFilter.setRadius(Radius);

			
		end
			
		%Update the filter
		MainGUI.ParamGUI.updateGUI;
	
	end
	
	function [lat,lon,dep] = depthpoly(pos, depths)
			%creates the input for the depth region
						
			%create a new retangular for the depthRegion with 
			%minmax depths from the catalog
			depmin = min(depths);
			depmax = max(depths);
			
			%first point
			lat(1) = pos(1,1);
			lon(1) = pos(1,2);
			dep(1) = depmin;
			
			%second dpoint
			lat(2) = pos(1,1);
			lon(2) = pos(1,2);
			dep(2) = depmax;
			
			%third dpoint
			lat(3) = pos(2,1);
			lon(3) = pos(2,2);
			dep(3) = depmax;
			
			%last dpoint
			lat(4) = pos(2,1);
			lon(4) = pos(2,2);
			dep(4) = depmin;
	end	



end


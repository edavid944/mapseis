classdef CommanderGUI < handle
	%GUI for swichting the catalog and filterlist and a lot of other parameters 	
	

    properties
     	MyGUI
     	MainGUI
     	DataMenu
     	EditMenu
     	SessionMenu
     	Importer
     	Exporter
     	DataEditor
     	DataHash
     	FilterHash
     	CurrentDataID
     	%NextDataID
     	CurrentFilterID
     	%NextFilterID
        DataSelection
        FilterSelection
        CurrentDataList
        CurrentFilterList
        MarkedData
        MarkedFilter
        ListProxy
        ParallelMode       
        EmptyStart
        PlugIn
        MapSeisFolder
  
        
    end

	events
		HashUpdate
		rebuildMenus
		Switched
	end	

	
    methods
		
	function obj = CommanderGUI(mainGUI,ListProxy)
	    import mapseis.gui.*;
	    import mapseis.util.gui.*;
	    import mapseis.datastore.*;
	    
	    if nargin<2
		ListProxy=[];		
 	    end
 	    
	    obj.DataHash = assocArray();
            obj.FilterHash = assocArray();
            obj.CurrentDataID = 0;
            %obj.NextDataID = 1;
            obj.CurrentFilterID = 0;
            %obj.NextFilterID = 1;
            obj.CurrentDataList = {};
            obj.CurrentFilterList = {};
            
            obj.ParallelMode=false;
			
            obj.Importer =[];
            obj.Exporter =[];
            
            if ~isempty(mainGUI)
            	obj.MainGUI = mainGUI;
            	obj.ListProxy = obj.MainGUI.ListProxy;
            	obj.MapSeisFolder=obj.MainGUI.MapSeisFolder;
            else
            	obj.MainGUI = [];
            	obj.ListProxy = ListProxy;
            	obj.MapSeisFolder=pwd;
            end
            
	    obj.ParallelMode=false;		
			
            % Initialise the GUI
            % Create the figure
            try
            	Map_handle = obj.MainGUI.MainGUI;
            	pos = get(Map_handle,'pos');
            catch
            	pos = [0 0 640 480]
            end
            
            %Use 6 for the sizing (cat1,cat2, timediff, spacediff, load cat1 & cat2 buttons)
            winheight=290;
            %pos = [0 0 640 480]
            obj.MyGUI = figure('Name','File Commander',...
                'Position',[pos(1)+pos(3) pos(2)-30 300 winheight],...
                'Toolbar','none','MenuBar','none','NumberTitle','off');
            
            %create all the buttons and stuff
            obj.createLayout;
            obj.setMenu;
            
    
            %init the filterlists
            obj.InitList('FilterCata')


            %popup update
			%obj.PopUp_Update;

            
            %            
            % Make the GUI visible
            set(obj.MyGUI,'Visible','on');
		
		
            end

		
	function createLayout(obj)
		
			obj.FilterSelection = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.changeMarker(s),...
			'Position',[2.42857142857143 6.35714285714285 37.4285714285714 1.92857142857143],...
			'String','FilterList',...
			'Style','popupmenu',...
			'Tag','FilterList');

			obj.DataSelection = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.changeMarker(s),...
			'Position',[2.42857142857143 15.7142857142857 37.4285714285714 1.92857142857143],...
			'String','DataStore',...
			'Style','popupmenu',...
			'Tag','DataStore');

			
			%h6 = uicontrol(...
			%'Parent',obj.MyGUI,...
			%'Units','characters',...
			%'Callback',@(s,e) obj.switcher('Cata'),...
			%'Position',[2.71428571428571 13.8571428571428 10 1.78571428571429],...
			%'String','Switch',...
			%'Tag','switchcata');
			

			h7 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.loadCatalog,...
			'Position',[12.8571428571428 13.8571428571428 10 1.78571428571429],...
			'String','Load',...
			'Tag','loadcata');

			h8 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.deleter('Cata'),...
			'Position',[29.2857142857141 12.0714285714285 10 1.78571428571429],...
			'String','Delete',...
			'Tag','deletecata');

			
			%h9 = uicontrol(...
			%'Parent',obj.MyGUI,...
			%'Units','characters',...
			%'Callback',@(s,e) obj.switcher('Filter'),...
			%'Position',[2.85714285714285 4.42857142857143 10 1.78571428571429],...
			%'String','Switch',...
			%'Tag','switchfilter');

			
			h10 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.loadFilterlist,...
			'Position',[13 4.42857142857143 10 1.78571428571429],...
			'String','Load',...
			'Tag','loadfilter');

			
			h11 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.deleter('Filter'),...
			'Position',[29.2857142857142 2.64285714285714 10 1.78571428571429],...
			'String','Delete',...
			'Tag','deletefilter');

			
			h17 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.newfilter,...
			'Position',[29.2857142857142 4.42857142857143 10 1.78571428571429],...
			'String','New',...
			'Tag','newfilter');
			
			
			h12 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.saver('Cata'),...
			'Position',[12.8571428571428 12.0714285714286 10 1.78571428571429],...
			'String','Save',...
			'Tag','savecata');


			h13 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.saver('Filter'),...
			'Position',[13.1428571428571 2.64285714285714 10 1.78571428571429],...
			'String','Save',...
			'Tag','savefilter');
			
	
			
			h14 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.Filtereditor,...
			'Position',[2.85714285714285 2.64285714285714 10 1.78571428571429],...
			'String','Rename',...
			'Tag','editfilter');


			
			h15 = uipanel(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'FontWeight','bold',...
			'Title','Catalog',...
			'Tag','catalog',...
			'Clipping','on',...
			'BackgroundColor',[0.8 0.8 0.8],...
			'Position',[0.285714285714286 11.5714285714286 42.5714285714286 7.78571428571429]);
			
			
			h16 = uipanel(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'FontWeight','bold',...
			'Title','FilterList',...
			'Tag','filterlist',...
			'Clipping','on',...
			'BackgroundColor',[0.8 0.8 0.8],...
			'Position',[0.428571428571429 2.14285714285714 42.4285714285714 7.92857142857143]);
			
			%new feature, rename of the datastore
			h17 = uicontrol(...
			'Parent',obj.MyGUI,...
			'Units','characters',...
			'Callback',@(s,e) obj.DataStoreRename,...
			'Position',[2.71428571428571 12.0714285714286 10 1.78571428571429],...
			'String','Rename',...
			'Tag','datarename');
		
	end
		
		
		
	function setMenu(obj)

            g = obj.MyGUI;

            
            
            obj.DataMenu = uimenu(g,'Label','- Data');
            uimenu(obj.DataMenu,'Label','Load MapSeis/Zmap Catalog',...
                'Callback',@(s,e) obj.loadCatalog);
            uimenu(obj.DataMenu,'Label','Import Catalog',...
                'Callback',@(s,e) obj.CatalogImport);
            uimenu(obj.DataMenu,'Label','Reload Catalog from Web','Separator','on',...
                'Callback',@(s,e) obj.WebReload('reload'));
            uimenu(obj.DataMenu,'Label','Update Catalog from Web',...
                'Callback',@(s,e) obj.WebReload('update'));
            uimenu(obj.DataMenu,'Label','Check last update',...
                'Callback',@(s,e) obj.WebReload('check'));    
            uimenu(obj.DataMenu,'Label','Save Catalog','Separator','on',...
                'Callback',@(s,e) obj.saver('Cata'));
            uimenu(obj.DataMenu,'Label','Save Catalog & Filterlist',...
               	'Callback',@(s,e) obj.saver('FilterCata'));
            uimenu(obj.DataMenu,'Label','Save Filterlist',...
               	'Callback',@(s,e) obj.saver('Filter'));
            uimenu(obj.DataMenu,'Label','Export Catalog',...
                'Callback',@(s,e) obj.CatalogExport);
               	
               	
            obj.EditMenu = uimenu(g,'Label','- Edit');
            uimenu(obj.EditMenu,'Label','Parallel Calculation',...
                'Callback',@(s,e) obj.setParallel('set'));
            uimenu(obj.EditMenu,'Label','Reapply datastore buffers',...
                'Callback',@(s,e) obj.Rebuffer);
            uimenu(obj.EditMenu,'Label','Reapply Coast&Borders',...
                'Callback',@(s,e) obj.ReApplyOverlay);     
            uimenu(obj.EditMenu,'Label','Switch Declustering','Separator','on',...
                'Callback',@(s,e) obj.setDecluster);
            uimenu(obj.EditMenu,'Label','Rename Catalog Field','Separator','on',...
                'Callback',@(s,e) obj.CatalogModify('Renamer'));
            uimenu(obj.EditMenu,'Label','Select Basic Fields',...
                'Callback',@(s,e) obj.CatalogModify('Fielder'));
            uimenu(obj.EditMenu,'Label','View Field Description','Separator','on',...
                'Callback',@(s,e) obj.ViewFieldInfo);
            uimenu(obj.EditMenu,'Label','Edit Field Description',...
                'Callback',@(s,e) obj.EditFieldInfo);
            uimenu(obj.EditMenu,'Label','View Catalog Comments','Separator','on',...
                'Callback',@(s,e) obj.ViewCatalogInfo);
            uimenu(obj.EditMenu,'Label','Edit Catalog Comments',...
                'Callback',@(s,e) obj.EditCatalogInfo);
            
            obj.SessionMenu = uimenu(g,'Label','- Session');   	
            uimenu(obj.SessionMenu,'Label','Load Session',...
               	'Callback',@(s,e) obj.LoadSession);
            uimenu(obj.SessionMenu,'Label','Save Session',...
               	'Callback',@(s,e) obj.SaveSession('BothHash'));  	
            uimenu(obj.SessionMenu,'Label','Save DataStore Hash',...
               	'Callback',@(s,e) obj.SaveSession('DataHash'));  
            uimenu(obj.SessionMenu,'Label','Save FilterList Hash',...
               	'Callback',@(s,e) obj.SaveSession('FilterHash'));     	
               	
               	
            %set Parallel mode flag
            obj.setParallel('chk');
            
         end
				
				
				
		
	function switcher(obj,WhichOne)
			%changes the datastore or filterlist
			import mapseis.util.gui.allWaiting;
			
			allWaiting('wait');
			switch WhichOne
				case 'Cata'
					key = obj.CurrentDataList{obj.MarkedData};
					newdatastore = obj.DataHash.lookup(key);
					loadstructur = struct('Datastore',newdatastore.Datastore,...
										  'ShownDataID',key);
					%obj.CurrentDataID = obj.MarkedData;					  
				
				case 'Filter'
					key = obj.CurrentFilterList{obj.MarkedFilter};
					newfilterlist = obj.FilterHash.lookup(key);
					loadstructur = struct('Filterlist',newfilterlist.Filterlist,...
							      'ShownFilterID',key);
					%obj.CurrentFilterID = obj.MarkedFilter;	
				
				case 'Both'
					datakey = obj.CurrentDataList{obj.MarkedData};
					newdatastore = obj.DataHash.lookup(datakey);
					filterkey = obj.CurrentFilterList{obj.MarkedFilter};
					newfilterlist = obj.FilterHash.lookup(filterkey);
					loadstructur = struct(	'Datastore',newdatastore.Datastore,...
								'ShownDataID',datakey,...
								'Filterlist',newfilterlist.Filterlist,...
								'ShownFilterID',filterkey);
					
				end
				
			%popup update
			obj.PopUp_Update;
		
			%change the MainGUI
			obj.MainGUI.MainGUISwitcher(loadstructur);
			
			%refresh plugins
			notify(obj,'rebuildMenus');
			
			%refresh plugins
			%notify(obj,'rebuildMenus');
			
			allWaiting('move');
	end
		
		
	function saver(obj,WhichOne)
		
		import mapseis.util.gui.allWaiting;
		%saves the catalog (only catalog) or the filterlist
						

            [fName,pName] = uiputfile('*.mat','Enter save file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected.  Data not saved');
            else
                
            	allWaiting('wait');
            	
                switch WhichOne
			case 'Cata'
				dataStore = obj.MainGUI.DataStore;
				save(fullfile(pName,fName),'dataStore');
				allWaiting('move');
				msgbox('Catalog saved')
                
                	case 'Filter'
				filterList = obj.MainGUI.FilterList;
				filterList.PackIt;
				save(fullfile(pName,fName),'filterList');
				allWaiting('move');
				msgbox('Filterlist saved')
			case 'FilterCata'
				dataStore = obj.MainGUI.DataStore;
				filterList = obj.MainGUI.FilterList;
				filterList.PackIt;
				save(fullfile(pName,fName),'dataStore','filterList');
				allWaiting('move');
				msgbox('Catalog & Filterlist saved')
					
		end
				

                
             end
					
					
						
		
	end
		
		
	function deleter(obj,WhichOne)
            
			%removes a Catalog or a Filterlist from the hash
			%if a current catalog or Filterlist is deleted, the 
			%catalog/filterlist will still be shown in the mainmenu 
			import mapseis.importfilter.*;
            
            switch WhichOne
				case 'Cata'
					%resolve key
					key=obj.CurrentDataList{obj.MarkedData,1};
					
					%remove entry
					obj.DataHash.remove(key);
					
					%rework Currentdatalist 
					 logInd = strcmp(obj.CurrentDataList(:,1),key);
           			 obj.CurrentDataList = obj.CurrentDataList(~logInd,:);
           			 
           			 %set marker to save position
					obj.MarkedData=length(obj.CurrentDataList(:,1));
			 	
 
           			 
           			 %update gui
           			 %set(obj.DataSelection,'Value',obj.MarkedData);
           			 
				case 'Filter'
					%resolve key
					key=obj.CurrentFilterList{obj.MarkedFilter,1};
					
					%remove entry
					obj.FilterHash.remove(key);
					
					%rework Currentdatalist 
					 logInd = strcmp(obj.CurrentFilterList(:,1),key);
           			 obj.CurrentFilterList = obj.CurrentFilterList(~logInd,:);
           			 
           			 %set marker to save position
           			 obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
           			 
           			 %update gui
           			 %set(obj.FilterSelection,'Value',obj.MarkedFilter);
           		end	
           		
           	%popup update
			obj.PopUp_Update;
			
			%set to "new" position
			set(obj.DataSelection,'Value',obj.MarkedData);
			set(obj.FilterSelection,'Value',obj.MarkedFilter);

			
			%notify so everyone who needs the list can update
			notify(obj,'HashUpdate');

			
	end
		
		
		
	function loadCatalog(obj,filename)
		
		import mapseis.*;
		import mapseis.datastore.*;
		import mapseis.importfilter.*;
		import mapseis.util.gui.allWaiting;
					 
		%open load dialog if no filename specified
		if nargin<2
			[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
			if isequal(fName,0) || isequal(pName,0)
				disp('No filename selected. ');
				return
			else
				filename=fullfile(pName,fName);
			end
		else
			%filename is defined
			[pName,fName,extStr] = fileparts(filename);
		end
		
		
		allWaiting('wait');		 
		%----------------------------------------------------------------------------------
					
		%load catalog
		prevData = load(filename);
		
		%----------------------------------------------------------------------------------
		%Determine what datatyp it is and add necessary filters
		if isfield(prevData,'dataStore')
			% We have loaded a saved session, including filter values
			dataStore = prevData.dataStore;
			try
				filterList = prevData.filterList;
	    
			catch
			
				if obj.EmptyStart
					filterList = DefaultFilters(dataStore,obj.ListProxy); 
					prevData.filterList=filterList;
				end
			end
		
		elseif isfield(prevData,'dataCols')
			% We have loaded only the data columns, which now need to be turned
			% into an mapseis.datastore.DataStore
			dataStore = mapseis.datastore.DataStore(prevData.dataCols);
			setDefaultUserData(dataStore);
	    
		elseif isfield(prevData,'a')
			% We have loaded in a ZMAP format file with data in matrix 'a'
			dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
			setDefaultUserData(dataStore)
	    
		else
			error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');
		end
     
		%----------------------------------------------------------------------------------		 
		%check if bufferdata and userdata exists
            	try %check wether the datastore is already buffered
			temp=dataStore.getUserData('DecYear');
		catch
			dataStore.bufferDate;
		end
		
		try %check wether the plot quality is already set
			temp=datStore.getUserData('PlotQuality');
		catch
			dataStore.SetPlotQuality;
		end

	
		%----------------------------------------------------------------------------------		
		%add data to the catalog hash
		if isempty(fName) 
			fName = 'noname';
		end	 
		
					
		%write filename and path to userdata
		dataStore.setUserData('filename',fName);
		dataStore.setUserData('Name',fName);
		dataStore.setUserData('filepath',filename);
		
		%set a new random id
		dataStore.ResetRandID;
		
		%datastore hash
		dataentry = struct('DataName',fName,...
						   'Datastore',dataStore);
    
		obj.DataHash.set(num2str(obj.CurrentDataID+1),dataentry);
		obj.CurrentDataID = obj.CurrentDataID+1;
							
		obj.CurrentDataList(end+1,:) = {num2str(obj.CurrentDataID),fName};
				
		%filterhash    
		if isfield(prevData,'filterList') 
			%set a new random id
			filterList.ResetRandID;
			
			filterentry = struct('FilterName',filterList.getName,...
							     'Filterlist',filterList);
    
			obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
			obj.CurrentFilterID = obj.CurrentFilterID+1;
									
			%list for the gui
			filname =filterList.getName;
			obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname};
					
		end
		
		
		
		%----------------------------------------------------------------------------------
		%make the new loaded catalog the current one
		obj.MarkedData=length(obj.CurrentDataList(:,1));
			
		
		%check if a filterlist exists if not, build one
		if isempty(obj.CurrentFilterList)
			obj.newfilterNoUpdate;
		end	
		
		obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
			
					
		newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
				

		newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});


		loadstructur = struct(	'Datastore',newdatastore.Datastore,...
					'ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
					'Filterlist',newfilterlist.Filterlist,...
					'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});	  
		
		%popup update
		obj.PopUp_Update;
		
		%set to "new" position
		set(obj.DataSelection,'Value',obj.MarkedData);
		set(obj.FilterSelection,'Value',obj.MarkedFilter);

		
		%change the MainGUI
		obj.MainGUI.MainGUISwitcher(loadstructur);
		
		%refresh plugins
		notify(obj,'rebuildMenus');
		
		
		
		%notify so everyone who needs the list can update
		notify(obj,'HashUpdate');
		
		allWaiting('move');
			
	end
		



		
		
	function loadFilterlist(obj)
		import mapseis.*;
		import mapseis.datastore.*;
		import mapseis.util.gui.allWaiting;
			
					 
		%open load dialog if no filename specified
		if nargin<2
			[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
          		if isequal(fName,0) || isequal(pName,0)
               			disp('No filename selected. ');
				return
           		else
				filename=fullfile(pName,fName);
			end	     
		end
		allWaiting('wait');	
			
		%----------------------------------------------------------------------------------
					
		%load catalog
		prevData = load(filename);
		
		%----------------------------------------------------------------------------------
			
		%Determine what datatyp it is and add necessary filters
		if isfield(prevData,'filterList')
                	% FilterList found
	                filterList = prevData.filterList
						                    	
	        else
                    	error('No FilterList found');
                end

			
             	%set a new random id
             	filterList.ResetRandID;
			
             	%add to the hash
		filterentry = struct('FilterName',filterList.getName,...
            	  		     'Filterlist',filterList);
            
            	obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
            	obj.CurrentFilterID = obj.CurrentFilterID+1;
            
			
		%list for the gui
		filname =filterList.getName;
		obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname};

			
			
		%make the list the current one 	
		obj.MarkedFilter=length(obj.CurrentFilterList(:,1));

			
		newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
		loadstructur = struct(	'Filterlist',newfilterlist.Filterlist,...
					'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});
			

			
		%popup update
		obj.PopUp_Update;
			
		%change the MainGUI
		obj.MainGUI.MainGUISwitcher(loadstructur);
			
		%refresh plugins
		notify(obj,'rebuildMenus');
			
			
		%notify so everyone who needs the list can update
		notify(obj,'HashUpdate');

		allWaiting('move');	
		
	end
		
		
		
	function InitList(obj,WhichOne)
			%adds the current datahash and filterhash to the selectionlists
			%creates a cell array with key and namestring
			
			% ----------------------------------------------------------
			%| this can be inproved by just adding only the name needed |
			% ---------------------------------------------------------- 
			%LET'S DO IT
			
			switch WhichOne
				case 'Cata'
					Datakeys = obj.DataHash.getKeys;
					
					%get every data and add to cell array
					for i=1:numel(Datakeys)
						datastore= obj.DataHash.lookup(Datakeys{i});
						
						%get name
						entryname = datastore.DataName;
						
						%write 
						obj.CurrentDataList(i,:) = {Datakeys{i},entryname}		
					end
				
				
				case 'Filter'
					Filterkeys = obj.FilterHash.getKeys;
					
					%get every filterlist and to cell array
					for i=1:numel(Filterkeys)
						filterlist = obj.FilterHash.lookup(Filterkeys{i});
						
						%get name
						entryname = filterlist.FilterName;
						
						%write 
						obj.CurrentFilterList(i,:) = {Filterkeys{i},entryname}		
					end
				
				
				case 'FilterCata'
					Datakeys = obj.DataHash.getKeys;
					Filterkeys = obj.FilterHash.getKeys;
					
					
					%get every data and add to cell array
					for i=1:numel(Datakeys)
						datastore = obj.DataHash.lookup(Datakeys{i});
						
						%get name
						entryname = datastore.DataName;
						
						%write 
						obj.CurrentDataList(i,:) = {Datakeys{i},entryname}		
					end
					
					%get every filterlist and to cell array
					for i=1:numel(Filterkeys)
						filterlist = obj.FilterHash.lookup(Filterkeys{i});
						
						%get name
						entryname = filterlist.FilterName;
						
						%write 
						obj.CurrentFilterList(i,:) = {Filterkeys{i},entryname}		
					end
				
				
				end
				
				obj.EmptyStart=false;
				
				%notify so everyone who needs the list can update
				notify(obj,'HashUpdate');


			
					
	end
		
		
	function changeMarker(obj,handle)
		%used a handle instead of a switch, does not change much
			
			%get the position
			val=get(handle,'Value');
			
			%get the tag to now which one has to be changed
			WhichOne = get(handle,'Tag');
				
				switch WhichOne
					case 'DataStore'
						obj.MarkedData=val;
						obj.switcher('Cata');
					case 'FilterList'
						obj.MarkedFilter=val;
						obj.switcher('Filter');
					end
		notify(obj,'Switched');	
			
	end
				
		
	function PopUp_Update(obj)
			%just for less typing
				set(obj.DataSelection, 'String' , obj.CurrentDataList(:,2));
				set(obj.FilterSelection, 'String' , obj.CurrentFilterList(:,2));
			
			
			
		
	end
		
	function pushWithKey(obj,data,Key)
		
		%get type
		theclass=class(data);
		
		switch theclass
			case 'mapseis.datastore.DataStore'
				try
					dataEntry = obj.DataHash.lookup(Key);
					dataEntry.Datastore=data;
					obj.DataHash.set(Key,dataEntry);
					
					obj.InitList('Cata');
					obj.PopUp_Update;
					
					
				catch
					%not existing build newone
					%dataEntry.DataName=data.getUserData('Name');
					%dataEntry.Datastore=data;
					
					error('Commander: Datastore Key does not exist, use "pushIt" instead');
				end
				
				
				
			case 'mapseis.filter.FilterList'
				try
					filterEntry = obj.FilterHash.lookup(Key);
					filterEntry.Filterlist=data;
					obj.FilterHash.set(Key,filterEntry);
					
					obj.InitList('Filter');
					obj.PopUp_Update;
					
				catch
										
					error('Commander: Filterlist Key does not exist, use "pushIt" instead');
				end
			
			
		end	
		
	end
		
	function pushIt(obj,newdata)
			%function to be called from outside, pushes datastore and filterlist into hash
			%and makes it the marked one.
			
			if isfield(newdata,'Datastore')
					try
						fName = getUserData(newdata.Datastore,'Name');
					catch
						fName = getUserData(newdata.Datastore,'filename');
						newdata.Datastore.setUserData('Name',fName);
					end
					
					%check if a randomID exists
					%set a new random id
					if isempty(newdata.Datastore.GiveID)
						newdata.Datastore.ResetRandID;
					end
					
					dataentry = struct('DataName',fName,...
            				  		   'Datastore',newdata.Datastore);
            			
            			%check if a filterlist exists if not, build one
            			if isempty(obj.CurrentFilterList)
            				obj.newfilterNoUpdate;
            			end
           			obj.DataHash.set(num2str(obj.CurrentDataID+1),dataentry);
           			obj.CurrentDataID = obj.CurrentDataID+1;

					
				obj.CurrentDataList(end+1,:) = {num2str(obj.CurrentDataID),fName}
	
			end	
		
			if isfield(newdata,'Filterlist')
				 	%add to the hash
				 	filname = newdata.Filterlist.getName;
				 	
				 	%set a new random id
					if isempty(newdata.Filterlist.GiveID)
						newdata.Filterlist.ResetRandID;
					end
					
					filterentry = struct('FilterName',filname,...
            					  		 'Filterlist',newdata.Filterlist);
            
           			obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
          			obj.CurrentFilterID = obj.CurrentFilterID+1;
            		
					
					%list for the gui
					obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname}

					
					 		
			end	 		
			
			%make the new loaded catalog the current one
			 	obj.MarkedData=length(obj.CurrentDataList(:,1));
			 	obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
			
			%popup update
			obj.PopUp_Update;
			
			%set to "new" position
			set(obj.DataSelection,'Value',obj.MarkedData);
			set(obj.FilterSelection,'Value',obj.MarkedFilter);
			
			%notify so everyone who needs the list can update
			notify(obj,'HashUpdate');

			
	end
		
	function  keys = getMarkedKey(obj)
			%returns the keys of the marked data		
				keys = struct('ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
							  'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});

	end
		
		
	function newfilter(obj)
			%creates a new filterlist with the currently shown catalog
			
			import mapseis.*;
			import mapseis.datastore.*;
			
			%get marked datastore
			datakey = obj.CurrentDataList{obj.MarkedData};
			newdatastore = obj.DataHash.lookup(datakey);
							
			datastore = newdatastore.Datastore;
			
			%the default filterlist
			Filterlist = DefaultFilters(datastore,obj.ListProxy);
			
			
			%register the list in the hash
			filname = Filterlist.getName;

			filterentry = struct('FilterName',filname,...
            			  	  'Filterlist',Filterlist);
            
           	obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
          	obj.CurrentFilterID = obj.CurrentFilterID+1;
            		
					
			%list for the gui
			obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname}
			
			%update marker
			obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
			
			%updata GUIs and switch to marked datastore
			newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
			
			loadstructur = struct('Datastore',newdatastore.Datastore,...
								  'ShownDataID',datakey,...
								  'Filterlist',newfilterlist.Filterlist,...
								  'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});


			
			%popup update
			obj.PopUp_Update;
			
			
			%set to "new" position
			set(obj.DataSelection,'Value',obj.MarkedData);
			set(obj.FilterSelection,'Value',obj.MarkedFilter);
			
			
			%change the MainGUI
			obj.MainGUI.MainGUISwitcher(loadstructur);
			
			%refresh plugins
			notify(obj,'rebuildMenus');

			%notify so everyone who needs the list can update
			notify(obj,'HashUpdate');

			
			
	end
	
	function newfilterNoUpdate(obj)
			%creates a new filterlist with the currently shown catalog
			%same as newfilter, but does not update the screens, called
			%from datastore adding function which should do that part.
			
			import mapseis.*;
			import mapseis.datastore.*;
			
			%get marked datastore
			datakey = obj.CurrentDataList{obj.MarkedData};
			newdatastore = obj.DataHash.lookup(datakey);
							
			datastore = newdatastore.Datastore;
			
			%the default filterlist
			Filterlist = DefaultFilters(datastore,obj.ListProxy);
			
			
			%register the list in the hash
			filname = Filterlist.getName;

			filterentry = struct('FilterName',filname,...
            			  	  'Filterlist',Filterlist);
            
            		obj.FilterHash.set(num2str(obj.CurrentFilterID+1),filterentry);
            		obj.CurrentFilterID = obj.CurrentFilterID+1;
            		
					
			%list for the gui
			obj.CurrentFilterList(end+1,:) = {num2str(obj.CurrentFilterID),filname}
			
			%update marker
			obj.MarkedFilter=length(obj.CurrentFilterList(:,1));
			
			%updata GUIs and switch to marked datastore
			newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
			
			loadstructur = struct('Datastore',newdatastore.Datastore,...
								  'ShownDataID',datakey,...
								  'Filterlist',newfilterlist.Filterlist,...
								  'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});


			
			%popup update
			obj.PopUp_Update;
			
			
			%set to "new" position
			set(obj.DataSelection,'Value',obj.MarkedData);
			set(obj.FilterSelection,'Value',obj.MarkedFilter);
			
			
			%change the MainGUI
			%obj.MainGUI.MainGUISwitcher(loadstructur);
			
			%refresh plugins
			notify(obj,'rebuildMenus');

			%notify so everyone who needs the list can update
			notify(obj,'HashUpdate');

			
			
	end
		
	
	function CatalogImport(obj)
			%create the gui for the Import selector
			import mapseis.gui.*;
			
			obj.Importer=ImportGUI(obj)
			
			%set the Listener
			obj.ListProxy.listen(obj.Importer,'Loaded',...
				@(s,e) obj.DataArrived()); 

			%wait and listen	
					
	end
	
	
	function CatalogExport(obj)
		%create the gui for the Import selector
		import mapseis.gui.*;
			
		obj.Exporter=ExportGUI(obj)
			
		%set the Listener
		obj.ListProxy.listen(obj.Exporter,'Exported',...
			@(s,e) obj.ExportFinished()); 

		%wait and listen	
					
	end
	
	
	function CatalogModify(obj,WhichEdit)
			%create the gui for the Import selector
			import mapseis.gui.*;
			
			
			
			obj.DataEditor=DataEditorGUI(obj,WhichEdit)
			
			%set the Listener
			obj.ListProxy.listen(obj.DataEditor,'Edited',...
				@(s,e) obj.DataEdited()); 

			%wait and listen	
					
	end	

	function ExportFinished(obj)
		%Something arrived from the Importer, load and finish the importer
		import mapseis.util.gui.allWaiting;
			
		allWaiting('wait');
			
		%cut the connection to the Importer and kill it
		obj.ListProxy.quiet(obj.Exporter,'Exported',...
				@(s,e) obj.ExportFinished()); 
			
		%close the window
		close(obj.Exporter.TheGUI);
			
		
		clear obj.Exporter;
		obj.Exporter= [];	
	

		allWaiting('move');
	
	
	end
	
	
	function DataArrived(obj)
			%Something arrived from the Importer, load and finish the importer
			import mapseis.util.gui.allWaiting;
			
			allWaiting('wait');
			%get datastore
			datastore = obj.Importer.Datastore;
			
			%cut the connection to the Importer and kill it
			obj.ListProxy.quiet(obj.Importer,'Loaded',...
				@(s,e) obj.DataArrived()); 
			
			%close the window
			close(obj.Importer.TheGUI);
			
			%Now lets move the data to the hash and update
			if ~isempty(datastore)
				try
					fName=datastore.getUserData('Name');
				catch
					fName=datastore.getUserData('filename');
					datastore.setUserData('Name',fName);
				end
				
				%just to be sure
				if isempty(datastore.GiveID)
					datastore.ResetRandID;
				end
				
				dataentry = struct('DataName',fName,...
	            				   'Datastore',datastore);
	            
	            		obj.DataHash.set(num2str(obj.CurrentDataID+1),dataentry);
	            		obj.CurrentDataID = obj.CurrentDataID+1;
	            
	            		%gui list							
				obj.CurrentDataList(end+1,:) = {num2str(obj.CurrentDataID),fName};
						
				
							
	
				%----------------------------------------------------------------------------------
				%make the new imported catalog the current one
				obj.MarkedData=length(obj.CurrentDataList(:,1));
	
				 	
				   			
				newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
	
						  
				%check if a filterlist exists if not, build one
				if isempty(obj.CurrentFilterList)
					obj.newfilterNoUpdate;
				end
									  
				newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});


				loadstructur = struct('Datastore',newdatastore.Datastore,...
							  'ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
							  'Filterlist',newfilterlist.Filterlist,...
							   'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});					  
				%popup update
				obj.PopUp_Update;
				
				%set to "new" position
				set(obj.DataSelection,'Value',obj.MarkedData);
	
				
				%change the MainGUI
				obj.MainGUI.MainGUISwitcher(loadstructur);
				%refresh plugins
				notify(obj,'rebuildMenus');
		end

			clear obj.Importer;
			obj.Importer= [];	
	
			%notify so everyone who needs the list can update
			notify(obj,'HashUpdate');

		allWaiting('move');
		
	end
	

	
	function DataEdited(obj)
			%Something arrived from the DataEditor, load and finish
			
			%get datastore
			datastore = obj.DataEditor.Datastore;
			
			%cut the connection to the Importer and kill it
			obj.ListProxy.quiet(obj.DataEditor,'Edited',...
				@(s,e) obj.DataEdited()); 
			
			%close the window
			close(obj.DataEditor.TheGUI);
			
			%Now lets move the data to the hash and update
			if ~isempty(datastore)
				
				%get original data
				datakey = obj.CurrentDataList{obj.MarkedData};
				dataentry = obj.DataHash.lookup(datakey);
				
				%just to be sure
				if isempty(datastore.GiveID)
					datastore.ResetRandID;
				end
				
				dataentry.Datastore = datastore;
	            
	            		obj.DataHash.set(datakey,dataentry);
	            		
	            
				 	
				   			
				newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
				newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});


				loadstructur = struct('Datastore',newdatastore.Datastore,...
							  'ShownDataID',obj.CurrentDataList{obj.MarkedData,1},...
							  'Filterlist',newfilterlist.Filterlist,...
							   'ShownFilterID',obj.CurrentFilterList{obj.MarkedFilter,1});					  
				%popup update
				obj.PopUp_Update;
				
				%set to "new" position
				set(obj.DataSelection,'Value',obj.MarkedData);
	
				
				%change the MainGUI
				obj.MainGUI.MainGUISwitcher(loadstructur);
				%refresh plugins
				notify(obj,'rebuildMenus');
		end

			clear obj.DataEditor;
			obj.DataEditor= [];	
			
			%notify so everyone who needs the list can update
			notify(obj,'HashUpdate');
			%msgbox('Catalog edited')
		
	end
		

	
		
	function datastore = getCurrentDatastore(obj)				
			newdatastore = obj.DataHash.lookup(obj.CurrentDataList{obj.MarkedData,1});
			datastore = newdatastore.Datastore;
	end
		
		
	function filterlist = getCurrentFilterlist(obj)
			newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{obj.MarkedFilter,1});
			filterlist = newfilterlist.Filterlist;
	end
	

	
		
	function PopUpLists = getCurrentEntries(obj)
			PopUpLists.Data = obj.CurrentDataList;
			PopUpLists.Filter = obj.CurrentFilterList;

	end
	
	function IDs = getCurrentIDs(obj)
		%returns the IDs of the current Datastore and Filterlist
		datastore = getCurrentDatastore(obj);
		filterlist = getCurrentFilterlist(obj);
		IDs(1)=datastore.GiveID;
		IDs(2)=filterlist.GiveID;
		
	end
	
	
	function DataNR = getCurrentDatastoreNumber(obj)				
		%Similar to getCurrentDataStore, but just returns the marked Number, which can
		%be used to resolve datastore later (less memory consuming)
		DataNR=obj.MarkedData;

	end
		
		
	function FilterNR = getCurrentFilterlistNumber(obj)
		%Similar to getCurrentFilterlist, but just returns the marked Number, which can
		%be used to resolve filterlist later (less memory consuming)
		FilterNR=obj.MarkedFilter;
	end	
	
	
	
	function datastore = resolveDataEntry(obj,val);
			%Enter the value of the selection (row number of the  CurrentDatalist)
			newdatastore = obj.DataHash.lookup(obj.CurrentDataList{val,1});
			datastore = newdatastore.Datastore;
	end
		
	

	
	function filterlist = resolveFilterEntry(obj,val);
			%Enter the value of the selection (row number of the  CurrentFilterlist)
			newfilterlist = obj.FilterHash.lookup(obj.CurrentFilterList{val,1});
			filterlist = newfilterlist.Filterlist;
	end
				
	
	function OutElement = Keyresolver(obj,key,whichone);
		%Allows to get a catalog or filterlist with the hash key. Saver in
		%case of change Hashlists.
		switch whichone
			case 'data'
				TheEntry = obj.DataHash.lookup(key);
				OutElement = TheEntry.Datastore;
				
			case 'filter'
				TheEntry = obj.FilterHash.lookup(key);
				OutElement = TheEntry.Filterlist;
		end
	
	end
	
	function SaveSession(obj,WhichOne)
		%filters will not be packed in this version, it will be done later
		%if there is a need for it. 
		import mapseis.util.gui.allWaiting;
		
		
	 	[fName,pName] = uiputfile('*.mat','Enter save file name...');
	 	if isequal(fName,0) || isequal(pName,0)
               		 disp('No filename selected.  Data not saved');
               	else
			allWaiting('wait');
			TheHash = obj.SendSession(WhichOne);			 
			filename=fullfile(pName,fName);
			
			switch WhichOne
				case 'DataHash'
					DataHash=TheHash;
					save(filename,'DataHash');
					allWaiting('move');
					msgbox('Catalog Hash saved')
				case 'FilterHash'
					FilterHash=TheHash;
					save(filename,'FilterHash');
					allWaiting('move');
					msgbox('Filter Hash saved')
				case 'BothHash'
					DataHash=TheHash{1};
					FilterHash=TheHash{2};
					save(filename,'DataHash','FilterHash');
					allWaiting('move');
					msgbox('Session saved')
			end	
		end
		
		
		
	end
	
	
	
	
	function LoadSession(obj)
		%open load dialog if no filename specified
		import mapseis.util.gui.allWaiting;
		
		[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
		 	
          	if isequal(fName,0) || isequal(pName,0)
          		disp('No filename selected. ');
        	else
			filename=fullfile(pName,fName);
		end	     
		
		allWaiting('wait');
		
		%load catalog
		prevData = load(filename);
		
		try
			FilterHash=prevData.FilterHash;
			FHash=true;
		catch
			FHash=false;
		end
		
		try
			DataHash=prevData.DataHash;
			DHash=true;
		catch
			DHash=false;
		end
		
		
		if FHash & DHash
			TheHash{1}=DataHash;
			TheHash{2}=FilterHash;
			obj.ReplaceSession(TheHash,'BothHash');
		elseif DHash & ~FHash
			obj.ReplaceSession(DataHash,'DataHash');
		elseif ~DHash & FHash
			obj.ReplaceSession(FilterHash,'FilterHash');
		else
			errordlg('No Data found in the matfile');
		end
		
		allWaiting('move');
	end
	
	
	
	function TheHash = SendSession(obj,WhichOne)
		%sends the whole filter or datastore hash away
		
		switch WhichOne
			case 'DataHash'
				TheHash=obj.DataHash.ExportHash;
				
			case 'FilterHash'
				TheHash=obj.FilterHash.ExportHash;
			
			case 'BothHash'
				TheHash{1}=obj.DataHash.ExportHash;
				TheHash{2}=obj.FilterHash.ExportHash;
		
		end
		
	end
	
	
	
	function ReplaceSession(obj,TheHash,WhichOne)
		%Allows to load the whole filter or datastore hash(es)
		switch WhichOne
			case 'DataHash'
				obj.DataHash.replaceHash(TheHash);
				obj.MarkedData=1;
								
				%set the the lastkey right
				lastKey=max(cellfun(@str2num,TheHash(:,1)));
				obj.CurrentDataID=num2str(lastKey);
				
				obj.InitList('Cata');
				
			case 'FilterHash'
				obj.FilterHash.replaceHash(TheHash);
				obj.MarkedFilter=1;
				
				%set the the lastkey right
				lastKey=max(cellfun(@str2num,TheHash(:,1)));
				obj.CurrentFilterID=num2str(lastKey);
				
				obj.InitList('Filter');
				
			case 'BothHash'
				obj.DataHash.replaceHash(TheHash{1});
				obj.FilterHash.replaceHash(TheHash{2});
				obj.MarkedData=1;
				obj.MarkedFilter=1;
				
				DataHash=TheHash{1};
				FilterHash=TheHash{2};
				
				%set the the lastkey right
				lastKey=max(cellfun(@str2num,DataHash(:,1)));
				obj.CurrentDataID=num2str(lastKey)
				lastKey=max(cellfun(@str2num,FilterHash(:,1)));
				obj.CurrentFilterID=num2str(lastKey);
				
				obj.InitList('FilterCata');
				
		end
		
		%Check if everyone has a random ID
		obj.SetNewIDs(WhichOne);
		
		obj.PopUp_Update;
		
		notify(obj,'HashUpdate');
		
		%Update the MainGUI;
		datakey = obj.CurrentDataList{obj.MarkedData};
			newdatastore = obj.DataHash.lookup(datakey);
			filterkey = obj.CurrentFilterList{obj.MarkedFilter};
			newfilterlist = obj.FilterHash.lookup(filterkey);

			
		loadstructur = struct(	'Datastore',newdatastore.Datastore,...
					'ShownDataID',datakey,...
					'Filterlist',newfilterlist.Filterlist,...
					'ShownFilterID',filterkey);
		
		obj.MainGUI.MainGUISwitcher(loadstructur);
		%refresh plugins
		notify(obj,'rebuildMenus');
		
	end
	
	
	
	
	function Filtereditor(obj)
		%Allow to change the name of the filterlist at the moment
		key = obj.CurrentFilterList{obj.MarkedFilter};
		filterlist = obj.FilterHash.lookup(key);
		
		Listname=filterlist.FilterName;
		
		promptValues = {'FilterList Name'};
		dlgTitle = 'Rename the FilterList';
		
		gParams = inputdlg(promptValues,dlgTitle,1,{Listname});
		NewListName=gParams{1};
		
		filterlist.Filterlist.setName(NewListName);
		filterlist.FilterName=NewListName;
		
		obj.FilterHash.set(key,filterlist);
		obj.InitList('Filter');
		obj.PopUp_Update;
		
		
	end
	
	function DataStoreRename(obj)
		%Allow to change the name of the filterlist at the moment
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		
		Dataname=datastore.DataName;
		
		promptValues = {'Datastore Name'};
		dlgTitle = 'Rename the Catalog';
		
		gParams = inputdlg(promptValues,dlgTitle,1,{Dataname});
		NewDataName=gParams{1};
		
		datastore.Datastore.setUserData('Name',NewDataName);
		datastore.DataName=NewDataName;
		
		obj.DataHash.set(key,datastore);
		obj.InitList('Cata');
		obj.PopUp_Update;
		
			
		
	end	
	
	
	
	
	function WebReload(obj,How)
		%This function tries to either add new available data from the web ('update')
		%or to renew the whole data ('reload'). At the moment there is at least from the 
		%performance side no difference but, if I find a solution I will change that.
		import mapseis.datastore.*;
		import mapseis.catalogtools.catalogmerger;
		import mapseis.catalogtools.LogicalExtractor;
		import mapseis.util.gui.allWaiting;
		
		%get datastore
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		try
			filepath=datastore.Datastore.getUserData('filepath');
			[pathstr, name, ext] = fileparts(filepath); 
		catch
			ext='.dat';
		end	
		if isempty(ext)
			ext='.dat';
		end
		
		%try get webadress 
		try
			WebTarget=datastore.Datastore.getUserData('webadress');
			Worked=true;
		catch
			Worked=false;
			errordlg('There was no webadress specified in the datastore');
			return;
		end
		
		%try get importfilter 
		try
			ImportFilter=datastore.Datastore.getUserData('importfilter');
			Worked=Worked & true;
		catch
			Worked=false;
			errordlg('The importfilter is not specified in the datastore');
			return;
		end
		
		%try get the content
		try
			
			filename=['./Temporary_Files/TempWebData',ext];
			urlwrite(WebTarget,filename);
			
			Worked=Worked & true;
		catch
			Worked=false;
			errordlg('Something went wrong when tried to get the data from the web');
			return;
		end
		
		
		%Finally do all the work
		if Worked
			allWaiting('wait');
			
			switch How
				case 'reload'
					%import data 
					try
						eventStruct=ImportFilter(filename,'-file');	
					catch 
						errordlg('Import did not work')
						eventStruct=[];
					end
					
					if ~isempty(eventStruct);
						datastore.Datastore.ReplaceInternalData(eventStruct);
						datastore.Datastore.setUserData('LastUpdate',date);
						obj.DataHash.set(key,datastore);
						obj.InitList('Cata');
						obj.PopUp_Update;
						msgbox('The current catalog is updated','Commander','help')
					end	
					
				case 'update'
					%import data 
					try
						eventStruct=ImportFilter(filename,'-file');	
					catch 
						errordlg('Import did not work')
						eventStruct=[];
					end
					
					if ~isempty(eventStruct);
						%the old data
						oldStruct=datastore.Datastore.getFields;
						
						%get last date
						lastEntry=max(oldStruct.dateNum)
						
						%check if newer data is available
						newSelection=eventStruct.dateNum>lastEntry;
						
						if ~any(newSelection)
							datastore.Datastore.setUserData('LastUpdate',date);
							obj.DataHash.set(key,datastore);
							msgbox('The catalog is already up to date','Commander','help')
						
						else
							%merge the to structures
							newStruct=LogicalExtractor(eventStruct,newSelection);
							newEvent=catalogmerger(oldStruct,newStruct);
							%write
							datastore.Datastore.ReplaceInternalData(newEvent);
							datastore.Datastore.setUserData('LastUpdate',date);
							obj.DataHash.set(key,datastore);
							obj.InitList('Cata');
							obj.PopUp_Update;
							msgbox('The current catalog is updated','Commander','help')
						end
					end
					
				case 'check'
					
					try
						TheDate=datastore.Datastore.getUserData('LastUpdate');
						msgbox(['The current catalog was last checked: ',TheDate],'Commander','help')
					catch
						msgbox('This catalog was never updated','Commander','help')
					end
				
			end
		
			allWaiting('move');
		
		end
		
		
		
		
	end
	
	
	
	function SetNewIDs(obj,whichHash)
		%This function goes through the selected Hashes and sets new 
		%RandomIDs if needed
		%In this case only elements with have no RandomId will be set
		%all others remain untouched
		
		switch whichHash
			case 'DataHash'
				for i=1:numel(obj.DataHash.data(:,2))
					%set a new random id
					if isempty(obj.DataHash.data{i,2}.Datastore.GiveID)
						obj.DataHash.data{i,2}.Datastore.ResetRandID;
					end
				end
				
			case 'FilterHash'
				for i=1:numel(obj.FilterHash.data(:,2))
					%set a new random id
					if isempty(obj.FilterHash.data{i,2}.Filterlist.GiveID)
						obj.FilterHash.data{i,2}.Filterlist.ResetRandID;
					end
				end
			
			case 'BothHash'
				for i=1:numel(obj.DataHash.data(:,2))
					%set a new random id
					if isempty(obj.DataHash.data{i,2}.Datastore.GiveID)
						obj.DataHash.data{i,2}.Datastore.ResetRandID;
					end
				end
				
				for i=1:numel(obj.FilterHash.data(:,2))
					%set a new random id
					if isempty(obj.FilterHash.data{i,2}.Filterlist.GiveID)
						obj.FilterHash.data{i,2}.Filterlist.ResetRandID;
					end
				end
			
		end
		
	end
	
	
	function ViewFieldInfo(obj)
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);

		dlgTitle = 'Field of the Catalog';


		try
			FieldInfo=datastore.Datastore.getUserData('FieldInfo');
		catch
			FieldInfo=[];
		end
		
		%options for the window
		options.Resize='on';
		options.WindowStyle='normal';
		
		if ~isempty(FieldInfo)
			FieldInfo= cellfun(@(x,y) [x ':   ' y],FieldInfo(:,1),FieldInfo(:,2), 'UniformOutput',false );
			FieldInfo=[{'Field Description: '};FieldInfo];
			msgbox(FieldInfo,dlgTitle,'help');
		else
			msgbox('No information set',dlgTitle,'help');
		
		end
	
	end
	
	
	function ViewCatalogInfo(obj)
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);

		dlgTitle = 'Catalog comments';
		try
			CatalogInfo=datastore.Datastore.getUserData('CatalogInfo');
		catch
			CatalogInfo=[];
		end	
		
		%options for the window
		options.Resize='on';
		options.WindowStyle='normal';
		
		
		if ~isempty(CatalogInfo)
			msgbox(CatalogInfo,dlgTitle,'help');
		else
			msgbox('No information set',dlgTitle,'help');
		
		end
	end
	
	
	function EditFieldInfo(obj)
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		
		%get the fields
		thefields=datastore.Datastore.getOriginalFieldNames
		Howmanyfields=numel(thefields)
		
		dlgTitle = 'Edit the Catalog Field Description';
		
		try
			FieldInfo=datastore.Datastore.getUserData('FieldInfo');
		catch
		
			FieldInfo=[thefields,cell(Howmanyfields,1)];
			FieldInfo(:,2)=cellfun(@(x) '',FieldInfo(:,2), 'UniformOutput',false );
		end
		
		%check if there are more fields in than descripted, if so add empty
		%cells 
		if numel(FieldInfo(:,1))<Howmanyfields
			oldFieldInfo=FieldInfo;
			FieldInfo={};
			for i=1:Howmanyfields
				foundit=strcmp(oldFieldInfo(:,1),thefields{i});
				if sum(foundit)>0
					FieldInfo(i,1)=thefields(i);
					FieldInfo(i,2)=oldFieldInfo(foundit,2);
				else
					FieldInfo(i,1)=thefields(i);
					FieldInfo(i,2)={''};
				
				end	
			
			end
		end
		
		%options for the window
		options.Resize='on';
		options.WindowStyle='normal';
			
		
		gParams = inputdlg(FieldInfo(:,1),dlgTitle,1,FieldInfo(:,2),options);
		if ~isempty(gParams)
			FieldInfo(:,2)=gParams;
		end
		
		datastore.Datastore.setUserData('FieldInfo',FieldInfo);
		
		obj.DataHash.set(key,datastore);
		obj.InitList('Cata');
		obj.PopUp_Update;
	
	end
	
	
	function EditCatalogInfo(obj)
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		

		dlgTitle = 'Edit the Catalog Comments';
		
		try
			CatalogInfo=datastore.Datastore.getUserData('CatalogInfo');
		catch
		
			CatalogInfo='';
			
		end
		
		%options for the window
		options.Resize='on';
		options.WindowStyle='normal';
		options.Interpreter='tex';
		
		gParams = inputdlg('Catalog Comments',dlgTitle,10,{CatalogInfo},options);
		CatalogInfo=gParams{1};
		
		datastore.Datastore.setUserData('CatalogInfo',CatalogInfo);
		
		obj.DataHash.set(key,datastore);
		obj.InitList('Cata');
		obj.PopUp_Update;
	
	end
	
	function Rebuffer(obj)
		import mapseis.util.gui.allWaiting;
		allWaiting('wait');
		%reapplies the buffer of the selected datastore, needed if some buffers are missing
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		datastore.Datastore.bufferDate;
		
		obj.DataHash.set(key,datastore);
		obj.InitList('Cata');
		obj.PopUp_Update;
		allWaiting('move');
	
	end
	
	function ReApplyOverlay(obj)
		%writes the newest coastline and border into the datastore
		import mapseis.plot.GetOverlay;
		import mapseis.util.gui.allWaiting;
		
		Title = 'Reapply Coastlines and Internalborders';
		Prompt={'Resolution:', 'Resolution';...
			'Coastline Color:','Coast_Color';...
			'Internalborder Color:','Border_Color'};
			
		labelList2 =  {'Crude'; ...
				'Low'; ...
				'Intermediate'; ...
				'High'; ...
				'Full'};
		ColorList = {'black';'blue';'red';'green';'cyan';'magenta'};
		
		
				
		%Mc Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=labelList2;
		Formats(2,1).type='list';
		Formats(2,1).style='popupmenu';
		Formats(2,1).items=ColorList;
		Formats(3,1).type='list';
		Formats(3,1).style='popupmenu';
		Formats(3,1).items=ColorList;
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		defval =struct('Resolution',3,...
			'Coast_Color',2,...
			'Border_Color',1);
		
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
			
		if Canceled == 0
			allWaiting('wait');
			key = obj.CurrentDataList{obj.MarkedData};
			datastore = obj.DataHash.lookup(key);
			[Coast_PlotArgs, InternalBorder_PlotArgs] = GetOverlay(datastore.Datastore,NewParameter);
			
			%write into datastore
			datastore.Datastore.setUserData('Coast_PlotArgs',Coast_PlotArgs);
			datastore.Datastore.setUserData('InternalBorder_PlotArgs',InternalBorder_PlotArgs);
			
			
			obj.DataHash.set(key,datastore);
			obj.InitList('Cata');
			obj.PopUp_Update;
			allWaiting('move');
		end	
		
	end
	
	
	
	function setParallel(obj,com)
		%find menu entry
		Menu2Check= findobj(obj.EditMenu,'Label','Parallel Calculation');
		
		switch com
			case 'chk'
				if isempty(obj.ParallelMode)
					obj.ParallelMode=false;
				end
				
				set(Menu2Check, 'Checked', 'off');
			
				if obj.ParallelMode
					set(Menu2Check, 'Checked', 'on');
				end

			case 'set'		
				if isempty(obj.ParallelMode)
					obj.ParallelMode=false;
				end
				
				obj.ParallelMode=~obj.ParallelMode;
				
					

				
									
				if obj.ParallelMode
					set(Menu2Check, 'Checked', 'on');

				else
					set(Menu2Check, 'Checked', 'off');
				
				end
		end	
		
		

	
	end
	
	
	function setDecluster(obj)
		%calls the decluster gui:
		import mapseis.gui.SwitchDecluster;
		
		key = obj.CurrentDataList{obj.MarkedData};
		datastore = obj.DataHash.lookup(key);
		
		SwitchDecluster(datastore.Datastore,'extended');
		
		%key = obj.CurrentFilterList{obj.MarkedFilter};
		%filterlist = obj.FilterHash.lookup(key);
		
		%filterlist.Filterlist.PackIt;
		%filterlist.Filterlist.changeData(datastore.Datastore);
	
	end
	
	
	function AutoUpdate(obj,Thestate)
		%sets the maingui to either on or off
		
		if ~isempty(obj.MainGUI)
			switch Thestate
				case 'on'
					obj.MainGUI.AutoUpdate=true;
				case 'off'
					obj.MainGUI.AutoUpdate=false;
			end	
		
		end	
	end
	
	
	%New PlugIn functions
	function AddPlugIn(obj,PlugIn,Name)
		%adds the PlugIn the variable PlugIn (structure), the idea is, that
		%this special plugins 
		obj.PlugIn.(Name)=PlugIn;
	end
	
	
	function RemovePlugIn(obj,PlugIn,Name)
		%Deletes the under obj.PlugIn.(Name) saved plugIn and removes the field form the
		%structure
		try 
			clear obj.PlugIn.(Name);
			rmfield(obj.PlugIn,Name);
		end	
		
		
	end
	%-------------
	
	
	
	
	end
end	
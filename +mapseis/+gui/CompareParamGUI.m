classdef CompareParamGUI < handle
    % ParamGUI : GUI frontend for the MapSeis application parameters
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        ParamWidgets
        RangeFilters
        DataStore
        FilterList
    end
    
    methods
        function obj = CompareParamGUI(dataStore,filterList)
            import mapseis.projector.*;
            
            % Create the list of parameters and read off the maximum and minimum
            % values from the current list of events.  Each parameter has a string
            % that is displayed on the label next to the parameter edit fields, as
            % well as the intialisation struct.
            
            obj.DataStore = dataStore;
            obj.FilterList = filterList;
            
            rangeFilters = filterList.getRangeFilters();
            filterCount = numel(rangeFilters);
            for filterIndex=1:filterCount
                paramStruct(filterIndex) = ...
                    mapseis.gui.ParamGUI.initFilter(dataStore,...
                    rangeFilters{filterIndex});
            end
            
            obj.RangeFilters = rangeFilters;
            
            obj.ParamWidgets = cell(numel(paramStruct),1);
            
            % Initialise the GUI
            
            % Create the figure
            Map_handle = findobj('name','MapSeis Main Window');
            pos = get(Map_handle,'pos');
            
            %Added automatic resizing of the paramwindow
            winheight=filterCount*80;
            
            mainGUI = figure('Name','Filter Parameters',...
                'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight],...
                'Toolbar','none','MenuBar','none','NumberTitle','off');
            % Create a uipanel in the figure to store the filter parameter widgets
            paramPanel = uipanel(mainGUI, 'Units','Normalized',...
                'Position',[0 0 1 0.8]);
            % Create the widgets inside the uipanel
            obj.createParamWidgets(paramPanel,paramStruct);
            % Create a button to call the update
            updateButton = uicontrol(mainGUI, 'Units', 'Normalized',...
                'Position',[0 0.85 0.5 0.1],'String','Update',...
                'Callback',@(s,e) obj.updateGUI());
            % Register this view with the model.  The subject of the view uses this
            % function handle to update the view.
            dataStore.subscribe('ParamGUI',@() updateGUI(obj));
            %             evStruct.subscribe('ParamGUI',@() nUpdateGUI(gui));
            
            % Update the GUI to plot the initial data
            obj.updateGUI();
            
            % Make the GUI visible
            set(mainGUI,'Visible','on');
            
        end
        
        function updateGUI(obj)
            % Update the filter setting widgets from the state of the event
            % data struct.  This enables us to store the state of the
            % application by saving dataStore to a .mat file
            if obj.FilterList.ListenForUpdate
                for paramIndex=1:numel(obj.ParamWidgets)
                    obj.ParamWidgets{paramIndex}.updateGUI();
                    
                    % Execute the filter
                    obj.ParamWidgets{paramIndex}.Filter.execute(obj.DataStore,false);
                end
            end
            % Update the filters all at the same time.
            obj.FilterList.update();
        end
        
        function createParamWidgets(obj,paramPanel,paramStruct)
            import mapseis.util.gui.*;
            
            % Number of widgets to create
            paramCount = numel(paramStruct);
            
            % Give each parameter equal amounts of space
            paramHeight = 1/paramCount;
            
            for paramIndex=1:paramCount
                % Create a panel in which to put the parameter widget
                panelHnd = uipanel('Parent',paramPanel,'BorderType','none',...
                    'Units','Normalized',...
                    'Position',[0.0 (1-paramHeight*paramIndex) 0.99 paramHeight]);
                obj.ParamWidgets{paramIndex} = ...
                    paramMinMaxWidget(panelHnd,...
                    paramStruct(paramIndex).initPars,...
                    paramStruct(paramIndex).label,...
                    obj.DataStore, obj.RangeFilters{paramIndex});                
            end
        end
    end
    
    methods (Static)
        function paramStruct=initFilter(dataStore,filterObj)
            paramStruct.label = filterObj.Name;
            paramStruct.initPars.min = min(filterObj.Projector(dataStore));
            paramStruct.initPars.max = max(filterObj.Projector(dataStore));
            paramStruct.initPars.typeStrs = filterObj.TypeStr;
            paramStruct.initPars.callbackFcn = ...
                @(varargin) filterObj.setRange(varargin{:}).execute(dataStore);
            if strcmp(filterObj.Name,'Time')
                paramStruct.initPars.min = ...
                    datestr(paramStruct.initPars.min,'yyyy-mm-dd HH:MM:SS');
                paramStruct.initPars.max =...
                    datestr(paramStruct.initPars.max,'yyyy-mm-dd HH:MM:SS');
                paramStruct.initPars.editDisplayFcn = ...
                    @(dNum) datestr(dNum,'yyyy-mm-dd HH:MM:SS');
            end
        end
    end
    
end
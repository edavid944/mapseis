classdef ExportGUI < handle



    properties
     	TheGUI
		CommanderGUI
		SelectionList
		Texter
		SelectionArray
		Datastore 
		Filterlist                
    end

	
	
	events
		Exported
	end	



	methods

		function obj=ExportGUI(Commander)
			%This function creates a GUI for selecting the exportfilters and allows to export 
			%Datastores and filterlist in several file format
			
			
			%UNFINISHED
			
			%Imports
			import mapseis.gui.*;
			import mapseis.util.gui.*;
			import mapseis.datastore.*;
			import mapseis.importfilter.*;	
			
			obj.SelectionArray=cell(0,4);
			obj.CommanderGUI=Commander
			
			%get current Filterlist and Datastore
			obj.Datastore = obj.CommanderGUI.getCurrentDatastore;
			obj.Filterlist = obj.CommanderGUI.getCurrentFilterlist;
			
			%first get all the import filters
			thefunctions = mapseis.gui.ExportGUI.littleSniffer
			
			
			obj.Selectionbuilder(thefunctions);
			
			
			
			obj.TheGUI = figure('Name','Exporter',...
				'Position',[500 500 480 300],...
				'Toolbar','none','MenuBar','none','NumberTitle','off');
		            
		            
		            
			%create all the buttons and stuff
			obj.CreateExporterLayout;
			
			% Make the GUI visible
			set(obj.TheGUI,'Visible','on');
				
				
		
			
		end
			
		function CreateExporterLayout(obj)	
			%creates the whole layout of the figure
			
				TGUI = obj.TheGUI;
				
				obj.Texter = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'BackgroundColor',[1 1 1],...
				'Position',[4.71428571428571 4.85714285714285 47.2857142857143 9.78571428571428],...
				'String','Description',...
				'Style','text',...
				'Tag','description');
				
				
				obj.SelectionList = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.changeTexter,...
				'Position',[4 16.5 49.1428571428571 2.57142857142857],...
				'String',obj.SelectionArray(:,1),...
				'Style','popupmenu',...
				'Value',1,...
				'Tag','Exportfilterselect');
		
				
				h4 = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.ExportCatalog(true),...
				'Position',[38 1.07142857142857 15 2.42857142857143],...
				'String','Export',...
				'Tag','startExport');
				
				h5 = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.noExport,...
				'Position',[4 1.07142857142857 15 2.42857142857143],...
				'String','Cancel',...
				'Tag','cancel');
				
						
				h6 = uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'FontSize',11,...
				'FontWeight','bold',...
				'Position',[4.71428571428571 14.9285714285714 46.8571428571429 1.28571428571429],...
				'String','Description:',...
				'Style','text',...
				'Tag','destitle');
		
				h7 =  uicontrol(...
				'Parent',TGUI,...
				'Units','characters',...
				'Callback',@(s,e) obj.ExportCatalog(false),...
				'Position',[21 1.07142857142857 15 2.42857142857143],...
				'String','Export no Filter',...
				'Tag','startExportNoFilter');
		
			
		end	
			
			
				
		function Selectionbuilder(obj,functable)
				%call every function, get the description and the name and set the handle
				
				for i=1:numel(functable)
					whoareyou=functable{i}([],[],[],'-description');
					obj.SelectionArray(i,:) = {whoareyou.displayname,whoareyou.description,functable{i},whoareyou.filetype};
				
				end
		
		
		end
			
			
		function changeTexter(obj)	
			%change the text in the description
			
			%get position of the marker
			val = get(obj.SelectionList, 'Value');
			
			%set the text	
			set(obj.Texter,'String',obj.SelectionArray{val,2});	
			
		
		end	
		
		
		
		function noExport(obj)
			%Do nothing and return an empty object.
			
			notify(obj,'Exported');
		end
		
		
		function ExportCatalog(obj,useFilter)
			import mapseis.datastore.*;
			import mapseis.exportfilter.*;
			
			
			set(obj.Texter,'String','Importing Data, this may take a while...');
			%load and import the file and notify the Commander
			
			
			%get the importfilter and apply
			val = get(obj.SelectionList, 'Value');
			
			%load dialog
			[fName,pName] = uiputfile(obj.SelectionArray{val,4},'Select File...');
          		if isequal(fName,0) || isequal(pName,0)
               			 disp('No filename selected. ');
               			 obj.noExport;
           		else
				 filename=fullfile(pName,fName);
			end	     

			
			
			

			if useFilter
				selected=obj.Filterlist.getSelected;	
			else
				selected=true(obj.Datastore.getRowCount,1);
			end
							  
							  
			try
				obj.SelectionArray{val,3}(obj.Datastore,selected,filename,'-file');	
			catch 
				errordlg('There was an error')
				eventStruct=[];

			end

			
			set(obj.Texter,'String',obj.SelectionArray{val,2});
			
			%notify the Commander and let yourself be killed
			notify(obj,'Exported');

		end
		
		
		
		
		

	end
	
	
	methods (Static)
			function functable = littleSniffer()
				% searches all exportfilters in the path ./+mapseis/+exportfilter and returns them
				% as function handles.
				
				import mapseis.util.importfilter.*;
				import mapseis.exportfilter.*;
				
				%get all necessary reg expressionpatterns
				rp = regexpPatternCatalog();
		
				%where are I now
				curdir=pwd;
				
				cd './+mapseis/+exportfilter';
				
				
				
				if ispc
					[sta res] = dos(['dir /b *.m']);
				else
					[sta res] = unix(['ls -1 *.m']);
				end
				
				lines = regexp(res,rp.line,'match');
				
				
				for i=1:numel(lines)
			
					[pathStr,nameStr{i},extStr] = fileparts(lines{i});
					%this has to be done for a certain object structur
					nameStr{i} = ['mapseis.exportfilter.',nameStr{i}];
			
				end
		
				functable=cellfun(@str2func, nameStr, ...
		                   'UniformOutput', false);
				
				
				%get back to original dir
				cd(curdir);
		
				
		end
		

	
	end
	
end		
		
		

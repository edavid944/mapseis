classdef CalcGridGUI < handle
    % CalcGridGUI : GUI for the MapSeis application grid calculations
    % This function creates a GUI with a single plot axis that shows the
    % results of extracting all the events within the given radius around each
    % grid point, applying the calculation function calcFun and projecting out
    % the required result using projFun.
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 87 $    $Date: 2008-11-26 12:47:05 +0000 (Wed, 26 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        DataStore
        FilterList
        
        GUIHandle
        GridSep
        Rad
        XRange
        YRange
        CalcRes
        CalcVals
        InputProjFun
        CalcObj
        OutputProjFun
        OptionMenu
        GridMenu
        PlotQuality
		WindowName
    end
    
    methods
        
        function obj = CalcGridGUI(dataStore,viewName,calcObj,outputProj,filterList)
            % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.filter.*;
            
            obj.DataStore = dataStore;
            obj.FilterList = filterList;
            obj.PlotQuality = dataStore.getUserData('PlotQuality');
            obj.WindowName = viewName;
             
            % Create the figure window that contains the GUI.  Use a one panel layout.
            obj.GUIHandle = onePanelLayout('MapSeis Grid Calculation Plot',...
                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
            
                      
            % Parameters that the define the grid and the region of interest around
            % each grid point.
            try
                [gridSep,rad]=dataStore.getUserData('gridDepthPars');
            catch ME
                gridSep = [];
                rad = 0;
            end
            obj.GridSep = gridSep;
            obj.Rad = rad;
            
            % Calculation results
            obj.XRange = [];
            obj.YRange = [];
            obj.CalcRes = {};
            obj.CalcVals = {};
            
            % Extract the processing functions from the input struct
            
            % Input projector to extract the needed data fields from the event data
            % about each grid point eg @(dataStore) dataStore.getMagnitudes('gridPt')
%             obj.InputProjFun = calcStruct.inputProjFun;
            % Calculation to be applied to the data matrix about each grid point
%             obj.CalcFun = calcStruct.calcFun;
            obj.CalcObj = calcObj;
            % Output projector to apply to each element of cell array returned by
            % applying calcFun to data about each grid point
            obj.OutputProjFun = outputProj;
            
            % Register this view with the model.  The subject of the view uses this
            % function handle to update the view.
            dataStore.subscribe(obj.WindowName,@() updateGUI(obj));
            
            % Update the GUI to plot the initial data
            obj.updateGUI();
            
            % Make the GUI visible
            set(obj.GUIHandle,'Visible','on');                        
            
            % Create the menu (here we refer to the object itself)
            
            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
            uimenu(obj.OptionMenu,'Label','Zmap style',...
                'Callback',@(s,e) obj.setqual('low'));
            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
                'Callback',@(s,e) obj.setqual('med'));
            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
                'Callback',@(s,e) obj.setqual('hi'));
            
			obj.setqual('chk');

            
            
            obj.GridMenu=uimenu(obj.GUIHandle,'Label','  --  Grid');
            uimenu(obj.GridMenu,'Label','Set grid',...
                'Callback',@obj.setGridParams);
            uimenu(obj.GridMenu,'Label','Calculate',...
                'Callback',@(s,e) obj.calculate());
            uimenu(obj.GridMenu,'Label','Clear',...
                'Callback',@(s,e) obj.clearCalculate());
                
                
        end
        
        function calculate(obj)
            import mapseis.calc.*;
            
            regionFilter = obj.FilterList.getByName('Region');
            regionParms = regionFilter.getRegion();
            if ~isempty(regionParms) && ~isempty(obj.GridSep) && obj.Rad ~= 0
                theRegion = regionParms{2};
                bBox=theRegion.getBoundingBox();
                tic;
                [obj.XRange,obj.YRange,obj.CalcRes] = ...
                    CalcGrid(obj.DataStore,bBox,obj.GridSep,obj.Rad,...
                    obj.CalcObj);
                timeTaken = toc;
                disp(['Grid calculation took ',num2str(timeTaken),'s']);
                obj.CalcVals = cellfun(obj.OutputProjFun,obj.CalcRes);
                obj.updateGUIWithResults();
            else
                return
            end
        end
        
        function updateGUI(obj)
            % This method will plot the events only, and clear any
            % calculation results.
            import mapseis.plot.PlotGridCalc;
            import mapseis.plot.PlotRegion;
            import mapseis.projector.*;
            
            % Read the grid parameters from the UserData of the event data
            % store
            gridPars = obj.DataStore.getUserData('gridPars');
            obj.GridSep = gridPars.gridSep;
            obj.Rad = gridPars.rad;
            % Update the GUI
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            regionFilter = obj.FilterList.getByName('Region');
            regionParms = regionFilter.getRegion();
            pRegion = regionParms{2};
            RegRange= regionParms{1};
            [inReg,outReg] = ...
                getLocations(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            [selectedMags, unselectedMags] = ...
                getMagnitudes(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            mags.selected = selectedMags;
            mags.unselected = zeros(0,1);
            if ~isempty(pRegion) && ~isempty(obj.GridSep) && obj.Rad ~= 0
                %             PlotRegion(plotAxis,inReg,mags);
                rBdry = pRegion.getBoundary();
                % Grid points
                gPts = pRegion.getGrid(obj.GridSep);
                PlotRegion(plotAxis,struct('inRegion',inReg,...
                    'outRegion',zeros(0,2),...
                    'boundary',rBdry,'grid',gPts,...
                    'PlotType','2Dpolygon',...
                    'Quality',obj.PlotQuality,...
                    'plotCoastline',false,...
                    'plotBorder',false,...
                    'showlegend',false,...
                    'drawnpoly',true,...
                    'dataStore',obj.DataStore,...
                    'regionFilter',regionFilter,...
                    'FilterRange', RegRange),mags);
            else
                PlotRegion(plotAxis,inReg,mags);
            end
            set(plotAxis,'Tag','axis');
        end
        
        function updateGUIWithResults(obj)
            % This method will only be called after the grid calculation
            % has been performed.
            import mapseis.plot.PlotGridCalc;
            import mapseis.plot.PlotRegion;
            import mapseis.projector.*;
            
            % Read the grid parameters from the UserData of the event data
            % store
            gridPars = obj.DataStore.getUserData('gridPars');
            obj.GridSep = gridPars.gridSep;
            obj.Rad = gridPars.rad;
            % Update the GUI
            plotAxis = findobj(obj.GUIHandle,'Tag','axis');
            regionFilter = obj.FilterList.getByName('Region');
            regionParms = regionFilter.getRegion();
            pRegion = regionParms{2};
            [inReg,outReg] = ...
                getLocations(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            [selectedMags, unselectedMags] = ...
                getMagnitudes(obj.DataStore,obj.FilterList.getSelected()); %#ok<NASGU>
            mags.selected = selectedMags;
            mags.unselected = zeros(0,1);
            if ~isempty(obj.CalcVals)
                % Plot the b mean mag values
                PlotGridCalc(plotAxis,{obj.XRange, obj.YRange}, obj.CalcVals)
            elseif ~isempty(pRegion) && ~isempty(obj.GridSep) && obj.Rad ~= 0
                %             PlotRegion(plotAxis,inReg,mags);
                rBdry = pRegion.getBoundary();
                % Grid points
                gPts = pRegion.getGrid(obj.GridSep);
                PlotRegion(plotAxis,struct('inRegion',inReg,...
                    'outRegion',zeros(0,2),...
                    'boundary',rBdry,'grid',gPts,...
                    'PlotType','2Dpolygon',...
                    'Quality',obj.PlotQuality),mags);
            else
                PlotRegion(plotAxis,inReg,mags);
            end
            set(plotAxis,'Tag','axis');
        end
        
        function setGridParams(obj,source,event) %#ok<INUSD>
            % Create a dialog box to set the grid separation and radius of
            % interest
            
            import mapseis.gui.setGridParams;
            
            try
                setGridParams(obj.DataStore);
                obj.updateGUI();
            catch ME
            end
        end
        
        function clearCalculate(obj)
            % Clear the calculated values and replot the events.
            obj.CalcVals = {};
            obj.updateGUI();
        end
        
        
        function setqual(obj,qual)
			%sets the quality of the plot (just for the calculation plot
			%chk just gets the userdata parameter and sets the 'checked' to the menu
			Menu2Check = get(obj.OptionMenu,'Children');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
                case 'low'
                	obj.PlotQuality = 'low';
                	set(Menu2Check(3), 'Checked', 'on');
                	obj.updateGUI;
                	
				case 'med'
					obj.PlotQuality = 'med';
                	set(Menu2Check(2), 'Checked', 'on');
					obj.updateGUI;
					
				case 'hi'
					obj.PlotQuality = 'hi';
					set(Menu2Check(1), 'Checked', 'on');
					obj.updateGUI;
					
				case 'chk'
					StoredQuality = obj.PlotQuality;

							switch StoredQuality
								case 'low'
									sel=3;
								case 'med'
									sel=2;
								case 'hi'
									sel=1;
								end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
		end
		
		
		function rebuildGUI(obj,dataStore,filterList)
			%allows to rebuild a closed GUI window
			
			   % Import utility functions
            import mapseis.util.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;
            import mapseis.filter.*;
            
            
            try 
            	set(obj.GUIHandle,'Visible','on');
            
            catch		
            
            	%CHANGE: Allow to get the new datastore and filterlist, it may changed.  	
	            obj.DataStore = dataStore;
	            obj.FilterList = filterList;
	                        
	            % Create the figure window that contains the GUI.  Use a one panel layout.
	            obj.GUIHandle = onePanelLayout('MapSeis Grid Calculation Plot',...
	                'TickDir','out','color','r','FontWeight','bold','FontSize',12);
	            
	                        
	              % Register this view with the model.  The subject of the view uses this
	            % function handle to update the view.
	            dataStore.subscribe(obj.WindowName,@() updateGUI(obj));
	            
	            % Update the GUI to plot the initial data
	            obj.updateGUI();
	            
	            % Make the GUI visible
	            set(obj.GUIHandle,'Visible','on');                        
	            
	            % Create the menu (here we refer to the object itself)
	            
	            obj.OptionMenu = uimenu(obj.GUIHandle,'Label','- Plot Option');
	            uimenu(obj.OptionMenu,'Label','Zmap style',...
	                'Callback',@(s,e) obj.setqual('low'));
	            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
	                'Callback',@(s,e) obj.setqual('med'));
	            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
	                'Callback',@(s,e) obj.setqual('hi'));
	            
				obj.setqual('chk');
	
	            
	            
	            obj.GridMenu=uimenu(obj.GUIHandle,'Label','  --  Grid');
	            uimenu(obj.GridMenu,'Label','Set grid',...
	                'Callback',@obj.setGridParams);
	            uimenu(obj.GridMenu,'Label','Calculate',...
	                'Callback',@(s,e) obj.calculate());
	            uimenu(obj.GridMenu,'Label','Clear',...
	                'Callback',@(s,e) obj.clearCalculate());
			end
			
		
		end
		
		
		
        
    end
    
end
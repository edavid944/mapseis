function SwitchDecluster(Datastore,GUIType)
	%A small GUI to turn the different parts of the decluster catalog 
	%on and off
	%GUIType might be used later in case additional features are added
	
	if nargin<2
		GUIType='extended';
	end
	
	
	if Datastore.checkCluster
		ShowParts=Datastore.getUsedType;
		UnClasser=Datastore.ShowUnclassified;
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		
		switch GUIType
			case 'info'
				%info is wanted so show the the info again	
				
			
			case 'basic'
				Title = 'Decluster Switcher (basic)';
				Prompt={'Single Events:', 'TheSingles';...
					'Main shocks:','TheMainMan';...
					'Aftershocks:','TheDesert'}
				Formats(1,1).type='check';
				Formats(2,1).type='check';
				Formats(3,1).type='check';
			
				DefaultData.TheSingles=ShowParts(1);		
				DefaultData.TheMainMan=ShowParts(2);
				DefaultData.TheDesert=ShowParts(3);
				
				%ask for it
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,DefaultData,Options); 
				
				if Canceled==0 
					%write to matrix
					ShowParts(1)=logical(NewParameter.TheSingles);
					ShowParts(2)=logical(NewParameter.TheMainMan);
					ShowParts(3)=logical(NewParameter.TheDesert);
				end
				
				
			case 'extended'
				Title = 'Decluster Switcher (extended)';
				Prompt={'Single Events:', 'TheSingles';...
					'Main shocks:','TheMainMan';...
					'Aftershocks:','TheDesert';...
					'Unclassified earthquakes:','UnderDogs'}
				Formats(1,1).type='check';
				Formats(2,1).type='check';
				Formats(3,1).type='check';
				Formats(4,1).type='check';
				
				DefaultData.TheSingles=ShowParts(1);		
				DefaultData.TheMainMan=ShowParts(2);
				DefaultData.TheDesert=ShowParts(3);
				DefaultData.UnderDogs=UnClasser;
				
				%ask for it
				[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,DefaultData,Options); 
				
				if Canceled==0 
					%write to matrix
					ShowParts(1)=logical(NewParameter.TheSingles);
					ShowParts(2)=logical(NewParameter.TheMainMan);
					ShowParts(3)=logical(NewParameter.TheDesert);
					UnClasser=logical(NewParameter.UnderDogs);
				end
				
			case 'developer'
				%later (reset option and similar stuff
		end
		
		
		if ~strcmp(GUIType,'developer')
			%developer might need a different datastore handling
			Datastore.setUsedType(ShowParts);
			Datastore.ShowUnclassified=UnClasser;
			
			
		else
		
		end
	
	else
		msgbox('Sorry, no decluster data available in this catalog');
	
	end


end

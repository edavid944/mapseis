classdef ParamGUI_Multi < handle
    % ParamGUI_Multi : GUI frontend for the MapSeis application parameters
    % Similar to ParamGUI, but allows to use more than one Filterlist&DataStore Selection
    
    % At the moment filterlists and datastore have to be entered directly, future version
    % may use commander keys for this
    
    % $Revision: 1 $    $Date: 2010-26-05 15:14:25 +0000 $
    % Author: David Eberhard
    
    properties
        ParamWidgets
        RangeFilters
        DataStore
        FilterList
        GUIColor
        BackCatalog
        Selector
        LastSelect
        mainGUI
        ListProxy
    end
    
    methods
        function obj = ParamGUI_Multi(dataStore,filterList,GUIColor,ListProxy)
            import mapseis.projector.*;
            
            % Create the list of parameters and read off the maximum and minimum
            % values from the current list of events.  Each parameter has a string
            % that is displayed on the label next to the parameter edit fields, as
            % well as the intialisation struct.
            
            if iscell(filterList)
            	obj.DataStore = dataStore{1};
            	obj.FilterList = filterList{1};
            	obj.ListProxy = ListProxy;
            	
            	obj.BackCatalog=cell(1,1)
            	
            	obj.BackCatalog(1:numel(dataStore),1)=dataStore;
            	obj.BackCatalog(1:numel(filterList),2)=filterList;
            	obj.LastSelect=1;
            	if ~isempty(GUIColor)
            		obj.BackCatalog(1:numel(GUIColor),3)=GUIColor;
            		obj.GUIColor=GUIColor{1};
            	else
            		obj.GUIColor=[0.7 0.7 0.7];
            		for i=1:numel(filterList)
            			obj.BackCatalog{i,3}=[0.7 0.7 0.7];
            		end
            	end
            	
            	%Build names
            	for i=1:numel(filterList)
            		if ~isempty(obj.BackCatalog{i,1})
            			try
            				catname=obj.BackCatalog{i,1}.getUserData('Name');
            			catch
            				catname='noNameCat';
            			end
            		else
            			catname='catless';
            		end
            		filtname=filterList{i}.getName;
            		
            		obj.BackCatalog{i,4}=[filtname,'-',catname];
            	end
            	
            else
            	obj.DataStore = dataStore;
            	obj.FilterList = filterList;
            	obj.ListProxy = ListProxy;
            	
            	obj.BackCatalog=[];
            	obj.LastSelect=1;
            	
            	if ~isempty(GUIColor)
            		obj.GUIColor=GUIColor;
            	else
            		obj.GUIColor=[0.7 0.7 0.7];
            	end
            	
            end
            
            % catalog-less filterlist are now allowed
            if  ~isempty(obj.FilterList) 
		    rangeFilters = obj.FilterList.getRangeFilters();
		    filterCount = numel(rangeFilters);
		    for filterIndex=1:filterCount
			paramStruct(filterIndex) = ...
			    mapseis.gui.ParamGUI_Multi.initFilter(obj.DataStore,...
			    rangeFilters{filterIndex});
		    end
		    
		    obj.RangeFilters = rangeFilters;
		    
		    obj.ParamWidgets = cell(numel(paramStruct),1);
		    
		    % Initialise the GUI
		    
		    % Create the figure
		    Map_handle = findobj('name','MapSeis Main Window');
		    pos = get(Map_handle,'Position');
		    
		    %Added automatic resizing of the paramwindow
		    if ~isempty(obj.BackCatalog)
		    	winheight=40+filterCount*80;
		    else
		    	winheight=filterCount*80;
		    end
		    
		    obj.mainGUI = figure('Name','Filter Parameters',...
			'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight],...
			'Toolbar','none','MenuBar','none','NumberTitle','off');
		   if ~isempty(obj.BackCatalog)
			    % Create a uipanel in the figure to store the filter parameter widgets
			    paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
				'Position',[0 0 1 0.6],'BackgroundColor',obj.GUIColor);
			    % Create the widgets inside the uipanel
			    obj.createParamWidgets(paramPanel,paramStruct);
			    % Create a button to call the update
			    updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
				'Position',[0.1 0.65 0.8 0.1],'String','Update',...
				'Callback',@(s,e) obj.updateGUI());	
			    obj.Selector = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
				'Position',[0.1 0.85 0.8 0.1],'String',obj.BackCatalog(:,4),...
				'Callback',@(s,e) obj.switchList(),'Value',obj.LastSelect,'Style','popupmenu');	
			   	
		    else
		    	    % Create a uipanel in the figure to store the filter parameter widgets
			    paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
				'Position',[0 0 1 0.8],'BackgroundColor',obj.GUIColor);
			    % Create the widgets inside the uipanel
			    obj.createParamWidgets(paramPanel,paramStruct);
			    % Create a button to call the update
			    updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
				'Position',[0.1 0.85 0.8 0.1],'String','Update',...
				'Callback',@(s,e) obj.updateGUI());	
		    end
		    
		    % Register this view with the model.  The subject of the view uses this
		    % function handle to update the view.
		    %dataStore.subscribe('ParamGUI',@() updateGUI(obj));
		    if ~isempty(obj.DataStore)
		    	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
		    end
		    
		    obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
	
		    %             evStruct.subscribe('ParamGUI',@() nUpdateGUI(gui));
		    
		    % Update the GUI to plot the initial data
		    obj.updateGUI();
		    
		    % Make the GUI visible
		    set(obj.mainGUI,'Visible','on');
            end
        end
        
        
        
        function updateGUI(obj)
            % Update the filter setting widgets from the state of the event
            % data struct.  This enables us to store the state of the
            % application by saving dataStore to a .mat file
           
            if obj.FilterList.ListenForUpdate
                for paramIndex=1:numel(obj.ParamWidgets)
                    obj.ParamWidgets{paramIndex}.updateGUI();
                    
                    % Execute the filter
                    obj.ParamWidgets{paramIndex}.Filter.execute(obj.DataStore,false);
                end
            end
            % Update the filters all at the same time.
            obj.FilterList.update();
        end
        
        
        
        function createParamWidgets(obj,paramPanel,paramStruct)
            import mapseis.util.gui.*;
            
            % Number of widgets to create
            paramCount = numel(paramStruct);
            
            
            
            % Give each parameter equal amounts of space
            paramHeight = 1/paramCount;
            
            for paramIndex=1:paramCount
                % Create a panel in which to put the parameter widget
                panelHnd = uipanel('Parent',paramPanel,'BorderType','none',...
                    'Units','Normalized',...
                    'Position',[0.0 (1-paramHeight*paramIndex) 0.99 paramHeight]);
                obj.ParamWidgets{paramIndex} = ...
                    paramMinMaxWidget(panelHnd,...
                    paramStruct(paramIndex).initPars,...
                    paramStruct(paramIndex).label,...
                    obj.DataStore, obj.RangeFilters{paramIndex});  
                    
            end
        end
        
        
        
        
        function rebuildGUI(obj)
        	%allows to rebuild the closed gui
        	
        	import mapseis.projector.*;
        	
        	try
        		 set(obj.mainGUI,'Visible','on');	
        	
        	catch
        		 	%delete(obj.mainGUI);	
		            dataStore = obj.DataStore;
		            filterList = obj.FilterList;
		            		            
		           
		            rangeFilters = filterList.getRangeFilters();
		            filterCount = numel(rangeFilters);
		
		        	
		        	
		            for filterIndex=1:filterCount
		                paramStruct(filterIndex) = ...
		                    mapseis.gui.ParamGUI_Multi.initFilter(dataStore,...
		                    rangeFilters{filterIndex});
		            end
		            
		            obj.RangeFilters = rangeFilters;
		            
		            obj.ParamWidgets = cell(numel(paramStruct),1);
					
					      	
		        			        	
		        	% Create the figure
		            Map_handle = findobj('name','MapSeis Main Window');
		            pos = get(Map_handle,'pos');
		            
		            %Added automatic resizing of the paramwindow
		            if ~isempty(obj.BackCatalog)
		            	winheight=40+filterCount*80;
		            else
		            	winheight=filterCount*80;
		            end
		    
		            
		            obj.mainGUI = figure('Name','Filter Parameters',...
		                'Position',[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight],...
		                'Toolbar','none','MenuBar','none','NumberTitle','off');
		            
		                
		            if ~isempty(obj.BackCatalog)
				    % Create a uipanel in the figure to store the filter parameter widgets
				    paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
					'Position',[0 0 1 0.6],'BackgroundColor',obj.GUIColor);
				    % Create the widgets inside the uipanel
				    obj.createParamWidgets(paramPanel,paramStruct);
				    % Create a button to call the update
				    updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
					'Position',[0.1 0.65 0.8 0.1],'String','Update',...
					'Callback',@(s,e) obj.updateGUI());	
				    obj.Selector = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
					'Position',[0.1 0.85 0.8 0.1],'String',obj.BackCatalog(:,4),...
					'Callback',@(s,e) obj.switchList(),'Value',obj.LastSelect,'Style','popupmenu');
			    else
				    % Create a uipanel in the figure to store the filter parameter widgets
				    paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
					'Position',[0 0 1 0.8],'BackgroundColor',obj.GUIColor);
				    % Create the widgets inside the uipanel
				    obj.createParamWidgets(paramPanel,paramStruct);
				    % Create a button to call the update
				    updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
					'Position',[0.1 0.85 0.8 0.1],'String','Update',...
					'Callback',@(s,e) obj.updateGUI());	
			    end    
		                
		                
		                
		                
		            if ~isempty(obj.DataStore)
		            	obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
		            	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
		            %             evStruct.subscribe('ParamGUI',@() nUpdateGUI(gui));
		            end
		            
		            obj.ListProxy.quiet(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
		            obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
		            
		            % Update the GUI to plot the initial data
		            obj.updateGUI();
		            
		            % Make the GUI visible
		            set(obj.mainGUI,'Visible','on');

        	end
        
        
        end
      
        
        
     function relisten(obj)
     		%obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
    		obj.ListProxy.PerfixQuiet('pram');	
     		%evStruct.subscribe('ParamGUI',@() nUpdateGUI(gui));
     		%obj.ListProxy.quiet(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
		    
     		obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
     		obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
     		
     		% Update the GUI to plot the initial data
     		obj.updateGUI();

     
     end   
       
     
     
     function silence(obj)
     	%removes all listener needed to prevent multiples when the gui is switched
     	%obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
     	%obj.ListProxy.quiet(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
     	obj.ListProxy.PrefixQuiet('pram');
     
     end
    
     
     
     
     
    function switchList(obj)
    	%changes the filterlist-datastore combination
    	if ~isempty(obj.BackCatalog)
    		newselect=get(obj.Selector,'Value');
    		
    		newDatastore=obj.BackCatalog{newselect,1};
    		newFilterlist=obj.BackCatalog{newselect,2};
    		newColor=obj.BackCatalog{newselect,3};
    		obj.LastSelect=newselect;
    		
    		%break old filterlist-datastore connection if needed.
		if ~isempty(obj.DataStore)
			obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
		end    	
		
		%set new entries
		obj.DataStore=newDatastore;
		obj.FilterList=newFilterlist;
		obj.GUIColor=newColor;
		
		%close(obj.mainGUI)
		delete(obj.mainGUI);
		obj.rebuildGUI;
    	
    	
    	end
    
    end
    
    
    
    
    function addEntry(obj,Datastore,Filterlist,Color)
    	%adds a new entry to the BackCatalog, if not a similar one is existing
    	if ~isempty(obj.BackCatalog)
    		doesExist=cellfun(@(x) x==Datastore,obj.BackCatalog(:,1))&...
    				cellfun(@(x) x==Filterlist,obj.BackCatalog(:,2));
    	
		if sum(doesExist)==0
			obj.BackCatalog{end+1,1}=Datastore;
			obj.BackCatalog{end,2}=Filterlist;
			if ~isempty(Color)
				obj.BackCatalog{end,3}=Color;
			else
				obj.BackCatalog{end,3}=[0.7 0.7 0.7];
			end
			if ~isempty(Datastore)
            			try
            				catname=Datastore.getUserData('Name');
            			catch
            				catname='noNameCat';
            			end
            		else
            			catname='catless';
            		end
            		filtname=Filterlist.getName;
        
			obj.BackCatalog{end,4}=[filtname,'-',catname];
			
			
		end
		set(obj.Selector,'String',obj.BackCatalog(:,4));
    	else
    		if ~isempty(obj.DataStore)&~isempty(obj.FilterList)
    			%first add the current entry to the list
    			obj.BackCatalog{1,1}=obj.DataStore;
    			obj.BackCatalog{1,2}=obj.FilterList;
    			obj.BackCatalog{1,3}=obj.GUIColor;
    			if ~isempty(obj.DataStore)
            			try
            				catname=obj.DataStore.getUserData('Name');
            			catch
            				catname='noNameCat';
            			end
            		else
            			catname='catless';
            		end
            		filtname=obj.FilterList.getName;
    			obj.BackCatalog{1,4}=[filtname,'-',catname];
    			
    			%now check with the new entry if it already exising
    			doesExist=cellfun(@(x) x==Datastore,obj.BackCatalog(:,1))&...
    				cellfun(@(x) x==Filterlist,obj.BackCatalog(:,2));
    	
			if sum(doesExist)==0
				obj.BackCatalog{end+1,1}=Datastore;
				obj.BackCatalog{end,2}=Filterlist;
				if ~isempty(Color)
					obj.BackCatalog{end,3}=Color;
				else
					obj.BackCatalog{end,3}=[0.7 0.7 0.7];
				end
				if ~isempty(Datastore)
					try
						catname=Datastore.getUserData('Name');
					catch
						catname='noNameCat';
					end
				else
					catname='catless';
				end
				filtname=Filterlist.getName;
		
				obj.BackCatalog{end,4}=[filtname,'-',catname];
				
				
			end
			
			close(obj.mainGUI);
		
    		else
    			%empty FilterlistGUI, needs to be build then
    			
			if ~isempty(Color)
				obj.GUIColor=Color;
			else
				obj.GUIColor=[0.7 0.7 0.7];
			end
			
    			
			obj.DataStore=Datastore;
			obj.FilterList=Filterlist;
			obj.LastSelect=1;
			
			
    		end
    		
    		obj.rebuildGUI;
    	
    	
    	end
    	
    
    end
    
    end
    
    methods (Static)
        function paramStruct=initFilter(dataStore,filterObj)
            if ~isempty(dataStore)
		    paramStruct.label = filterObj.Name;
		    paramStruct.initPars.min = min(filterObj.Projector(dataStore));
		    paramStruct.initPars.max = max(filterObj.Projector(dataStore));
		    paramStruct.initPars.typeStrs = filterObj.TypeStr;
		    paramStruct.initPars.callbackFcn = ...
			@(varargin) filterObj.setRange(varargin{:}).execute(dataStore);
		    if strcmp(filterObj.Name,'Time')
			paramStruct.initPars.min = ...
				datestr(paramStruct.initPars.min,'yyyy-mm-dd HH:MM:SS');
				
			paramStruct.initPars.max =...
				datestr(paramStruct.initPars.max,'yyyy-mm-dd HH:MM:SS');
				%DateConvert(paramStruct.initPars.max, 'string');
				
			paramStruct.initPars.editDisplayFcn = ...
			    	@(dNum) datestr(dNum,'yyyy-mm-dd HH:MM:SS');
				
		    end
	     else
	     	%support for "empty" (catalog-less) filterlists.
	     	 paramStruct.label = filterObj.Name;
		 paramStruct.initPars.min = min(filterObj.EmptyRange);
		 paramStruct.initPars.max = max(filterObj.EmptyRange);
		 paramStruct.initPars.typeStrs = filterObj.TypeStr;
		 paramStruct.initPars.callbackFcn = [];
		 if strcmp(filterObj.Name,'Time')
			paramStruct.initPars.min = ...
                	datestr(paramStruct.initPars.min,'yyyy-mm-dd HH:MM:SS');
                	%DateConvert(paramStruct.initPars.min, 'string');
                	
                	paramStruct.initPars.max =...
                	datestr(paramStruct.initPars.max,'yyyy-mm-dd HH:MM:SS');
                	%DateConvert(paramStruct.initPars.max, 'string');
                	
                	paramStruct.initPars.editDisplayFcn = ...
                		@(dNum) datestr(dNum,'yyyy-mm-dd HH:MM:SS');
		 end
	     
	     
	     end
        end
    end
    
end
function setqual(obj,qual)
	%sets the quality of the plot 
	%chk just gets the userdata parameter and sets the 'checked' to the menu
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	if ~strcmp(qual,'chkData')
		Menu2Check(3) = findobj(obj.PlotOptions,'Label','Zmap style');
		Menu2Check(2) = findobj(obj.PlotOptions,'Label','Medium Quality Plot');
		Menu2Check(1) = findobj(obj.PlotOptions,'Label','High Quality Plot');
		
		%uncheck all menupoints
		set(Menu2Check(1), 'Checked', 'off');
		set(Menu2Check(2), 'Checked', 'off');
		set(Menu2Check(3), 'Checked', 'off');
	end
	
	switch qual
		case 'low'
			set(Menu2Check(3), 'Checked', 'on');
			obj.PlotQuality='low';
			obj.updateGUI;
			      		
		case 'med'
			set(Menu2Check(2), 'Checked', 'on');
			obj.PlotQuality='med';
			obj.updateGUI;
		
		case 'hi'
			set(Menu2Check(1), 'Checked', 'on');
			obj.PlotQuality='hi';
			obj.updateGUI;
		
		case 'chk'
			StoredQuality = obj.PlotQuality;
			switch StoredQuality
				case 'low'
					sel=3;
				case 'med'
					sel=2;
				case 'hi'
					sel=1;
			end		
			set(Menu2Check(sel), 'Checked', 'on');
		case 'chkData'
			try  
				obj.PlotQuality = obj.Datastore.getUserData('PlotQuality');
			catch 
			
				obj.PlotQuality = 'med';		
			end
	
	end
	
end

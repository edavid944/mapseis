function BuildResultGUI(obj)
	%builds the gui
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard


	import mapseis.util.gui.*;
	import mapseis.gui.*;
	
	
	pos=[50 300 1100 600];
	
	[existFlag,figNumber]=figflag('3DWindow',1);
	newbmapWindowFlag=~existFlag;                          
	%disp(newbmapWindowFlag)
	
	% Set up the Seismicity Map window Enviroment
	if newbmapWindowFlag,
		obj.ResultGUI = onePanelLayout('3DWindow',...
		'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		%obj.ResultGUI=figure('name','3DWindow'); 
		%set(obj.ResultGUI,'Position',pos,'Renderer','painters');
		set(obj.ResultGUI,'Position',pos,'Color','w');
		obj.plotAxis=gca;
		set(obj.plotAxis,'Tag','MainResultAxis');
		
		obj.buildMenus;
		
	end	
	
	set(obj.ResultGUI,'visible','on');
	
end
function TooglePlotOptions(obj,WhichOne)
	%This functions sets the toogles in this modul
	%WhichOne can be the following strings:
	%	'Border':	Borderlines
	%	'Coast'	:	Coastlines
	%	'-chk'	:	This will set all checks in the menu
	%			to the in toggles set state.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard

	
	CoastMenu = findobj(obj.PlotOptions,'Label','Draw Coastlines');
	BorderMenu = findobj(obj.PlotOptions,'Label','Draw Country Borders');
	ShadowMenu = findobj(obj.PlotOptions,'Label','Draw Shadows');
	LandMenu = findobj(obj.PlotOptions,'Label','Draw LandMasses');
	
	switch WhichOne
		case '-chk'
			if obj.CoastToogle
				set(CoastMenu, 'Checked', 'on');
			else
				set(CoastMenu, 'Checked', 'off');
			end
			
			if obj.BorderToogle
				set(BorderMenu, 'Checked', 'on');
			else
				set(BorderMenu, 'Checked', 'off');
			end
			
			if obj.ShadowToogle
				set(ShadowMenu, 'Checked', 'on');
			else
				set(ShadowMenu, 'Checked', 'off');
			end
			
			if obj.LandToggle
				set(LandMenu, 'Checked', 'on');
			else
				set(LandMenu, 'Checked', 'off');
			end
	
	
		case 'Border'
			obj.BorderToogle=~obj.BorderToogle;
	
			if obj.BorderToogle
				set(BorderMenu, 'Checked', 'on');
			else
				set(BorderMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
			
		case 'Coast'
			
			obj.CoastToogle=~obj.CoastToogle;
			
			if obj.CoastToogle
				set(CoastMenu, 'Checked', 'on');
			else
				set(CoastMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
			
		case 'Shadow'
	
			obj.ShadowToogle=~obj.ShadowToogle;
			
			if obj.ShadowToogle
				set(ShadowMenu, 'Checked', 'on');
			else
				set(ShadowMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;
			
		case 'Land'
			
			obj.LandToggle=~obj.LandToggle;
			
			if obj.LandToggle
				set(LandMenu, 'Checked', 'on');
			else
				set(LandMenu, 'Checked', 'off');
			end
			
			obj.updateGUI;	
		
	end
	
end
classdef ThirdSpace < handle
	%This class plots a 3D view of the main window
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard
	
	
	properties
		ListProxy
		Commander
		Datastore
		SendedEvents
		Keys
		MainWindowGUI
		Filterlist
		ErrorDialog
		Region
		ResultGUI
		PlotQuality
		BorderToogle
		CoastToogle
		ShadowToogle
		LandToggle
		PlotOptions
		plotAxis
		PlotMode
		TheAspect
		AlphaMan
		LegendEntry
		PlotHandles
		LegendToogle
	end
	
	events
		CalcDone
	end
	
	
	methods
	
	
		function obj = ThirdSpace(ListProxy,Commander,MainWindowGUI,PlotMode)
			import mapseis.datastore.*;
			
			%The plotmode defines if everything ('full') is plotted from the MainWindow
			%(selected unselected and Polygons, etc) or if only the selected events are
			%Plotted ('light'), with 'region' the same mode like the maingui uses is
			%used, draw only selected by filterlist but differentiate between regionfilter
			%selected events;
			
			
			
			obj.ListProxy=ListProxy;
			obj.Commander=Commander;
			obj.PlotMode='light';		
			obj.Datastore = obj.Commander.getCurrentDatastore;
			obj.Filterlist = obj.Commander.getCurrentFilterlist; 
			obj.Keys=obj.Commander.getMarkedKey;
			selected=obj.Filterlist.getSelected;
			obj.SendedEvents=selected;
			obj.TheAspect=10;
			obj.AlphaMan='none';
			obj.BorderToogle=false;
			obj.CoastToogle=false;
			obj.ShadowToogle=true;
			obj.LandToggle=false;
			obj.LegendEntry={};
			obj.PlotHandles=[];
			obj.LegendToogle=false;
			
			%check if a profile is selected
			regionFilter = obj.Filterlist.getByName('Region');
			
			obj.ListProxy.PrefixQuiet('3DWindow');
			obj.ListProxy.listen(obj.Commander, 'Switched', @() obj.DataSwitcher,'3DWindow');
			if ~isempty(MainWindowGUI)
				%normal case, get everything form the mainGUI
				obj.MainWindowGUI=MainWindowGUI;
				obj.setqual('chkData');
				obj.ListProxy.listen(obj.MainWindowGUI, 'MainWindowUpdate', @() obj.updateGUI,'3DWindow');
			else
				obj.MainWindowGUI=[];
				obj.PlotQuality = 'low';
				
			end
			
			%Build Window
			obj.BuildResultGUI
			
			%plot eq
			obj.updateGUI;
			
			%Switched
			
		end
		
		
		%external file methods
		DataSwitcher(obj);
		
		BuildResultGUI(obj);
		
		buildMenus(obj);
		
		updateGUI(obj)
		
		LegendSwitcher(obj)
		
		setSuperElevation(obj)
		
		setAlphaMan(obj)
		
		SetPlotMode(obj,WhichMode)
		
		Rebuild(obj)
		
		setqual(obj,qual)
		
		TooglePlotOptions(obj,WhichOne)
		
	end
	
end	
function setAlphaMan(obj)
	% gui for setting the alpha value
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2010 David Eberhard


	promptValues = {'Transparency Factor LandMass'};
	dlgTitle = 'Set Transparency of LandMass (set to 1 for none)';
	if strcmp(obj.AlphaMan,'none')
		Alpha=1;
	else
		Alpha=obj.AlphaMan;
	end
	
	gParams = inputdlg(promptValues,dlgTitle,1,...
	cellfun(@num2str,{Alpha},'UniformOutput',false));
	AlphaMan=	str2double(gParams{1});
	
	if AlphaMan==1
		obj.AlphaMan='none';
	else
		obj.AlphaMan=AlphaMan;
	end
	
	obj.updateGUI;
	
end
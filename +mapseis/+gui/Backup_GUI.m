classdef GUI < handle
    % GUI : GUI frontend for the MapSeis application

    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell

    properties
        MapSeisFolder
    	DataStore
        %DataHash
        %FilterHash
        CurrentDataID
        ShownDataID
        CurrentFilterID
        ShownFilterID
        FilterList
        FilterIndexVect
        MainGUIFigureProperties
        MainGUIAxesProperties
        MainGUI
        SelectRadius
        HistType
        TimeType
        OptionMenu
        DataMenu
        AnalysisMenu
        CalculationMenus
        CalcGUI
        CalcDepthGUI
        ParamGUI
        CommanderGUI
        ListProxy
        debugtag
        LegendToggle
        CoastToggle
        BorderToggle
        StationToggle
        PlateToggle
        MarkEQ
        drawnpoly
        Wrapper
        ResGUI
        ParGUI
        ParallelMode
        GUIPlugIns
        PlugInMemory
        MainCustomPlot
        TRCustomPlot
        BRCustomPlot
        AutoUpdate
        DeveloperMode
        DepthWindow
        AutoProfile
        ThirdDimension
        PlatePlotSetting
        LastGraphicUpdate
    end
    
    events
	MainWindowUpdate
    end
    
    
    methods
        function obj=GUI(dataStore,filterList,ListProxy)
            import mapseis.gui.*;
            import mapseis.util.gui.*;
            import mapseis.datastore.*;
			
            % GUI constructor
            % Create the figure window that contains the GUI.  Use a three panel layout
            % consisting of the main axis in the left hand side of the window and two
            % subplots on the right hand side
            
            %let the random generator spin a bit and set it to the clock
            if ~verLessThan('matlab', '7.13.0')
            	    disp('Use new random setup stream')
	            RandStream.setGlobalStream ...
        	    		(RandStream('mt19937ar','seed',sum(100*clock)));
            
            else
            	     disp('Use old random setup stream')	
            	     RandStream.setDefaultStream ...
        	    		(RandStream('mt19937ar','seed',sum(100*clock)));
            
            
            end
            
            
            obj.MapSeisFolder=pwd;
            
            %Allow to use Hashes (there are recognized by the fact that they are cell arrays)
            DataHashLoad=false;
            FilterHashLoad=false;
            if iscell(dataStore)
            	DataHash=dataStore;
            	dataStore=DataHash{2,1};
            	DataHashLoad=true;
            end
            
            if iscell(filterList)
            	FilterHash=filterList;
            	filterList=FilterHash{2,1};
            	FilterHashLoad=true;
            end
            
            
            % Store the handles of the DataStore and FilterList and create the hash for additional catalogs
            obj.DataStore = dataStore;
            obj.FilterList = filterList;
            %obj.DataHash = assocArray();
            %obj.FilterHash = assocArray();
            obj.ShownDataID = [];
            obj.ShownFilterID = [];
            if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
            	obj.FilterIndexVect = filterList.getSelected();
            end
            
            obj.ListProxy = ListProxy;
            obj.CalcGUI = [];
            obj.CalcDepthGUI = [];
            obj.ParamGUI = [];
            obj.CommanderGUI=[];
            obj.debugtag=rand*100;
            obj.ParallelMode=false;
            obj.AutoUpdate=true;
            obj.DepthWindow=[];
            obj.LastGraphicUpdate=now;
            
            obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
						'Ridge',true,...
						'Transform',true,...
						'Trench',true,...
						'RidgeLineStylePreset',1,...
						'TransformLineStylePreset',1,...
						'TrenchLineStylePreset',1,...
						'RidgeColors',2,...
						'TransformColors',3,...
						'TrenchColors',1);
            %get the plugs
            obj.InitGUIPlugs;
          	
          	            
            % Set the properties of the figure window
            DefaultPos=[ 50 300 1100 690 ];
            
            %change screensize if needed
            MonSize=get(0,'ScreenSize');
            if 0.75*MonSize(3)<=DefaultPos(3)|0.75*MonSize(4)<=DefaultPos(4);
            	Screener=[50 150 MonSize(3)*0.75 MonSize(4)*0.75-150];	
            else
            	Screener=DefaultPos;
            end
            mainGUIFigureProperties = {'Position',Screener,'Renderer','painters'};

            % Set the axes properties by hand in the order left plot, top right, bottom
            % right
            mainGUIAxesProperties = {...
                {'pos',[ 0.06 0.1 0.55 0.85],'FontSize',12},...
                {'pos',[ 0.7 0.58 0.25 0.37],'FontSize',12},...
                {'pos',[ 0.7 0.1 0.25 0.4],'FontSize',12}};

            obj.MainGUI = threePanelLayout('MapSeis Main Window',...
                mainGUIFigureProperties,mainGUIAxesProperties);
		
                set(obj.MainGUI,'ToolBar','figure');
                    

        	
        
        
            % Register this view with the model.  The subject of the view uses this
            % function handle to update the view.
            
            %dataStore.subscribe('MainView',@() updateGUI(obj));
            %Replace with proxy
            if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
            	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
            	obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');	
	
		%NOTE: If the GUI is switched and more than one DataStore and FilterList is
		%used a listener per datastore and filterlist will exist, at the moment
		%I do not have a good idea at the moment how to prevent this or if it is 
		%even needed to be prevented
		    
		% Create the menu
        	setMenu(obj);
		
           	%set plot parameters
            	obj.legendary('chk');
            	obj.getparam('Show Coastline','CoastToggle');
            	obj.getparam('Show Internal Borders','BorderToggle');
            	obj.getparam('Show Seismic Stations','StationToggle');  
            	obj.getparam('Mark Largest Earthquakes','MarkEQ');
            	obj.getparam('Automatic DepthProfile','AutoProfile');
            	obj.getparam('Draw Plate Boundaries','PlateToggle')
            	
            	%Create the Calculation Entries
            	obj.createCalcMenu;
            	
            	obj.setDeveloper('init');
            	
            	% Create a context menu to choose histogram type
            	% NB: context menu only appears if you right click in the histogram window,
            	% but not actually on the histogram bars themselves
            	createHistContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_top_right'));

            	% Create context menu for the main event plot axis to select the region of
            	% interest
            	createMainAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_left'));

            	% Create context menu for the main event plot axis to select the region of
            	% interest
            	createTimeAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_bottom_right'));


            	try
            	    mainPars = dataStore.getUserData('mainPars');
            	    obj.SelectRadius = mainPars.selectRadius;
            	    obj.HistType = mainPars.histType;
            	    obj.TimeType = mainPars.timeType;
            	catch ME
            	    % Show different histogram plots depending on histType
            	    % {'mag','depth',...}, set from hist plot context menu.
            	    obj.HistType = 'mag';
            	    
            	    % Show different time plots depending on timeType, set from time plot
            	    % context menu.
            	    obj.TimeType = 'cumnum';
            	    % Radius for selection about a point
            	    obj.SelectRadius = 0.25;
            	end
            
            	%update Button
            	updateButton = uicontrol(obj.MainGUI, 'Units', 'Normalized',...
            	    'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update',...
            	    'Callback',@(s,e) obj.updateGUI(true));
	    
            	    %if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
            	% Update the GUI to plot the initial data
            	updateGUI(obj);
		
            	%load plugIns
		obj.startPlugs;
            	
            	% Make the GUI visible
            	set(obj.MainGUI,'Visible','on');
            end
            
            %Invoke the Commander
            %--------------------
            %Should be changed later, because the Commander should be really Commander
            
            	
            %create CommanderGUi
            obj.CommanderGUI = CommanderGUI(obj);
          	
            %set the hashes with the current datastore and filterlist
            if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
            	nameStr = dataStore.getUserData('filename');
            	if isempty(nameStr) 
            		nameStr= 'noname';
            	end	 
            
            	%pushit to the commander
            	initdata = struct('DataName',nameStr,...
            				  'Datastore',dataStore,...
            				  'FilterName',nameStr,...
                 			  'Filterlist',filterList);
            				   

                %obj.CurrentDataID = obj.NextDataID;
                %obj.NextDataID = obj.NextDataID+1;

                %obj.CurrentFilterID = obj.NextFilterID;
                %obj.NextFilterID = obj.NextFilterID+1;
            
                obj.ShownDataID = num2str(1);
                obj.ShownFilterID = num2str(1);
            
                obj.CommanderGUI.pushIt(initdata);
            end
            
            if isempty(obj.DataStore) & isempty(obj.FilterList)
            	obj.CommanderGUI.EmptyStart=true;
            end
        end




        function updateGUI(obj,force)
            if nargin<2
            	force=false;
            end
            
            %if less time is used than a 0.25s don't do an update
            MinUpdateTime= 1/(3600*24*4);
            
            if  (now-obj.LastGraphicUpdate)<=MinUpdateTime&~force
            	return
            end
            
           
            
            if obj.AutoUpdate|force
        	
		    % Extract the GUI handle
		    %disp(['update graphic ' num2str(obj.debugtag)]);
		    g = obj.MainGUI;
		    
		    if obj.FilterList.Packed&~isempty(obj.DataStore)
		    	obj.FilterList.changeData(obj.DataStore);
		    end	
		    
		    
		    if obj.AutoUpdate&force
		    	obj.FilterList.updateNoEvent();
		    end	
		    
		    obj.FilterIndexVect = obj.FilterList.getSelected();
		    % Update the GUI
				
		    plotAxis = findobj(g,'Tag','axis_left');	
				%in case of an error retag
				if isempty(plotAxis)
			disp('Try to retag left');
			plotAxis = obj.reTag('left');
		    end
		    
		    obj.plotRegion(plotAxis);
		    set(plotAxis,'Tag','axis_left');
				
				
		    plotAxis = findobj(g,'Tag','axis_top_right');
		    %in case of an error retag
		    if isempty(plotAxis)
			disp('Try to retag right top');
			plotAxis = obj.reTag('right_top');
		    end
		    obj.plotHist(plotAxis);
		    set(plotAxis,'Tag','axis_top_right');
	
	
		    plotAxis = findobj(g,'Tag','axis_bottom_right');
		    %in case of an error retag
		    if isempty(plotAxis)
			disp('Try to retag right bottom');
			plotAxis = obj.reTag('right_bottom');
		    end
		    obj.plotTime(plotAxis)
		    set(plotAxis,'Tag','axis_bottom_right');
		    
		    notify(obj,'MainWindowUpdate');
		    
		end
		
		%update last update time
		obj.LastGraphicUpdate=now;
		
	end

        
        
        function updateGUI_leftaxesonly(obj,force)
            g = obj.MainGUI;
            
            if nargin<2
            	force=false;
            end
            
            if obj.AutoUpdate|force
		     
            	    if obj.FilterList.Packed&~isempty(obj.DataStore)
            	    	obj.FilterList.changeData(obj.DataStore);
            	    end
            
		    obj.FilterIndexVect = obj.FilterList.getSelected();
		    % Update the GUI
		    plotAxis = findobj(g,'Tag','axis_top_right');
		    %in case of an error retag
		    if isempty(plotAxis)
			disp('Try to retag right top');
			plotAxis = obj.reTag('right_top');
		    end
		    obj.plotHist(plotAxis);
		    set(plotAxis,'Tag','axis_top_right');
	
		   
		    plotAxis = findobj(g,'Tag','axis_bottom_right');
		    %in case of an error retag
		    if isempty(plotAxis)
			disp('Try to retag right bottom');
			plotAxis = obj.reTag('right_bottom');
		    end
		    obj.plotTime(plotAxis)
		    set(plotAxis,'Tag','axis_bottom_right');
            end
        end





        function plotRegion(obj,plotAxis)
            import mapseis.plot.*;
            import mapseis.projector.*;
            import mapseis.region.*;
	    import mapseis.util.gui.SetImMenu;
	    
	    
            % Get the data to plot from the DataStore
            [inReg,outReg] = getLocations(obj.DataStore,obj.FilterIndexVect);
            regionFilter = obj.FilterList.getByName('Region');
            filterRegion = getRegion(regionFilter);
            pRegion = filterRegion{2};
            RegRange=filterRegion{1};
            [selectedMags, unselectedMags] = ...
                getMagnitudes(obj.DataStore,obj.FilterIndexVect);
            mags.selected = selectedMags;
            mags.unselected = unselectedMags;
            
            %get quality from datastore
            try 
            	StoredQuality = obj.DataStore.getUserData('PlotQuality');
            catch 
            	obj.DataStore.setUserData('PlotQuality','med');
		StoredQuality = obj.DataStore.getUserData('PlotQuality');		
            end
            
            
            if ~isempty(pRegion)
            
                if isobject(pRegion)
                	rBdry = pRegion.getBoundary();	
                else 
                	rBdry = pRegion; %circle
                	%modify on radius
                	%latlim = get(plotAxis,'Ylim');
                	%rBdry(4)= rBdry(3)*cos(pi/180*mean(latlim));
                end
                tic;	
                PlotRegion(plotAxis,...
                    struct('inRegion',inReg,...
                    'outRegion',outReg,...
                    'boundary',rBdry,...
                    'plotCoastline',obj.CoastToggle,...
                    'plotBorder',obj.BorderToggle,...
                    'showlegend',obj.LegendToggle,...
                    'drawnpoly',obj.drawnpoly,...
                    'dataStore',obj.DataStore,...
                    'regionFilter',regionFilter,...
                    'PlotType','2Dpolygon',...
                    'FilterRange', RegRange,...
                    'Quality',StoredQuality),mags);
                disp(['PlotRegion time: ',num2str(toc)]); 
                    
                %really needed the axes command
		axes(plotAxis)    
            	
		%recreate boundaries object if needed
            	 switch RegRange
        			case {'in','out'}
                			selection=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
                			selection.addNewPositionCallback(@(p) ...
                					filterupdater(p,obj.DataStore,regionFilter));
                			selection.setColor('r');		
                			%set the menu point depth slice to 'off'
                			anamenu=get(obj.AnalysisMenu,'Children');
                			set(anamenu(5),'Enable','off');
                			
                			%add menus to the object
                			SetImMenu(obj,selection,regionFilter,[]);
                			
                			if obj.AutoProfile
                				obj.ListProxy.PrefixQuiet('DepthWindow');
					
						try
							close(obj.DepthWindow.ResultGUI);
						end
						
						try
							clear(obj.DepthWindow);
						end
                			end
                			
            			case 'line'
            			
            				hold on 
            					handle = drawBox(plotAxis,false,rBdry);
            					selection=imline(plotAxis, [rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)]);
            				           				
                			hold off
                			            				
            				selection.addNewPositionCallback(@(p) ...
                					filterupdater(p,obj.DataStore,regionFilter));
                			selection.setColor('r');
                			
                			
                			%set the menu point depth slice to 'on'
                			anamenu=get(obj.AnalysisMenu,'Children');
                			set(anamenu(5),'Enable','on');			
                			
                			%add menus to the object
                			SetImMenu(obj,selection,regionFilter,handle);
                			
                			
                			
                			                	
                			
            			case 'circle'
            				           				
            				rBdry(3)=regionFilter.Radius;
            				if isempty(rBdry(3))
            					oldgrid = datastore.getUserData('gridPars');
            					rBdry(3) = oldgrid.rad;
            					regionFilter.setRadius(rBdry(3));
            				end	
            				
            				%draw circle
            				hold on
            				handle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
            				selection = impoint(plotAxis,rBdry(1:2));
            				hold off
            				
            				%selection.setFixedAspectRatioMode(1)
					selection.addNewPositionCallback(@(p) ...
                					filterupdater([p(1),p(2),rBdry(3)],...
                					obj.DataStore,regionFilter));
            				
                			selection.setColor('r');
                					
            				anamenu=get(obj.AnalysisMenu,'Children');
                			set(anamenu(5),'Enable','off');			
                			
                			%add menus to the object
                			SetImMenu(obj,selection,regionFilter,handle);
                			
                			if obj.AutoProfile
                				obj.ListProxy.PrefixQuiet('DepthWindow');
					
						try
							close(obj.DepthWindow.ResultGUI);
						end
						
						try
							clear(obj.DepthWindow);
						end
                			end
                			
            			case 'all'
            			%set the menu point depth slice to 'off'
                			anamenu=get(obj.AnalysisMenu,'Children');
                			set(anamenu(5),'Enable','off');		
                			
                			
                			if obj.AutoProfile
                				obj.ListProxy.PrefixQuiet('DepthWindow');
					
						try
							close(obj.DepthWindow.ResultGUI);
						end
						
						try
							clear(obj.DepthWindow);
						end
                			end
                			
            	end	
            else
%111
                %PlotRegion(plotAxis,inReg,mags);
            
            	%rBdry = pRegion.getBoundary();
%222
		tic;
                %disp(num2str(max(mags)))
                PlotRegion(plotAxis,...
                    struct('inRegion',inReg,...
                     'plotCoastline',obj.CoastToggle,...
                    'plotBorder',obj.BorderToggle,...
                    'showlegend',obj.LegendToggle,...
                    'dataStore',obj.DataStore,...
                    'regionFilter',regionFilter,...
                    'PlotType','2Dwhole',...
                    'Quality',StoredQuality),mags);
                
               disp(['PlotRegion time: ',num2str(toc)]);
            
            end
        	
            
	    tic
            handleS=[];
            if obj.StationToggle
            	%plot stations
            	
            	%get timelimits in the filterlist
            	timefilter=obj.FilterList.getByName('Time');
            	timeSelect=timefilter.getSelected;
            	thetimes=obj.DataStore.getFields('dateNum',timeSelect);
            	minTime=min(thetimes.dateNum);
            	maxTime=max(thetimes.dateNum);
            	axes(plotAxis);
            	lonLimi=xlim(plotAxis);
            	latLimi=ylim(plotAxis);
            	
            	hold on
            	StationPloTConf=struct(	'Colors','m',....
            				'Size',3,...
            				'Marker','s',...
            				'NameTag',true,...
            				'StartDate',minTime,...
            				'EndDate',maxTime);
            	
            	[handleS legendentry] = PlotStations(plotAxis,obj.DataStore,StationPloTConf);
            	axes(plotAxis);
            	hold off;
            	xlim(plotAxis,lonLimi);
            	ylim(plotAxis,latLimi);
            	
           
            	
            end
            
            handlePl=[];
            if obj.PlateToggle
            	if ~isstruct(obj.PlatePlotSetting)
            		  obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
						'Ridge',true,...
						'Transform',true,...
						'Trench',true,...
						'RidgeLineStylePreset',1,...
						'TransformLineStylePreset',1,...
						'TrenchLineStylePreset',1,...
						'RidgeColors',2,...
						'TransformColors',3,...
						'TrenchColors',1);
            	
		end
            	PlotConfig=obj.PlatePlotSetting;
	    	
	    	
	    	LineStyles={'normal','Fineline','dotted','Fatline'};
	    	ColorList={'blue','red','green','black','cyan','magenta'}; 
	    	PlotConfig.RidgeLineStylePreset=LineStyles{obj.PlatePlotSetting.RidgeLineStylePreset};
	    	PlotConfig.TransformLineStylePreset=LineStyles{obj.PlatePlotSetting.TransformLineStylePreset};
	    	PlotConfig.TrenchLineStylePreset=LineStyles{obj.PlatePlotSetting.TrenchLineStylePreset};
	    	PlotConfig.RidgeColors=ColorList{obj.PlatePlotSetting.RidgeColors};
	    	PlotConfig.TransformColors=ColorList{obj.PlatePlotSetting.TransformColors};
	    	PlotConfig.TrenchColors=ColorList{obj.PlatePlotSetting.TrenchColors};
	    	
	    	PlotConfig.X_Axis_Limit='Longitude';
	    	PlotConfig.Y_Axis_Limit='Latitude';
	    		    
			    
		xlimiter=get(plotAxis,'xlim');
		ylimiter=get(plotAxis,'ylim');
            	
		hold on
		[handlePl legendentry] = PlotPlateBoundaries(plotAxis,PlotConfig); 
		xlim(xlimiter);
		ylim(ylimiter);
		latlim = get(plotAxis,'Ylim');
		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
		
		hold off;
		
            end
            
            
            handleM=[];
            
            if obj.MarkEQ
            	axes(plotAxis);
            	lonLimi=xlim(plotAxis);
            	latLimi=ylim(plotAxis);
            	
            	hold on
            	[handleM legendentry]=PlotLargeEQ(plotAxis,obj.DataStore,obj.FilterIndexVect,'map');
            	
            	axes(plotAxis);
            	hold off;
            	xlim(plotAxis,lonLimi);
            	ylim(plotAxis,latLimi);
            	
            
            	
            end
            
            
            
            %try to correct the legend
           
            %so much to do just for updating the legend.
            %some commands might be not needed, but I'm not
            %sure and the documentation about this is lousy
           
            legend('location','NorthEastOutside');
            legend('location','NorthEast');
            
            
             %find legend
             leg=findobj(obj.MainGUI,'Type','axes','Tag','legend');
             legdata=get(leg,'UserData');
            
             
             if ~isempty(handlePl)&~isempty(legdata)
             		Names={'Ridge','Transform','Trench'};
            		for i=1:3
				pos=0;
				if ~isempty(handlePl{i})
					pos=handlePl{i}==legdata.handles;
				end
            		
				if any(pos)
					legdata.lstrings{pos}=Names{i};
					
				else
					legdata.handles(end+1)=handlePl{i};
					legdata.lstrings{end+1}=Names{i};
					set(leg,'UserData',legdata);
				end
			end
			
            		set(leg,'UserData',legdata);
            		try 
            			set(leg,'String',legdata.lstrings);
            		end	
            end
	    
             
             
             if ~isempty(handleS)&~isempty(legdata)
            		
            		pos=handleS==legdata.handles;
            		
            		if any(pos)
            			legdata.lstrings{pos}='Seismic Stations';
            			set(leg,'UserData',legdata);
            			try 
            				set(leg,'String',legdata.lstrings);
            			end	
            		else
            			legdata.handles(end+1)=handleS;
            			legdata.lstrings{end+1}='Seismic Stations';
            			set(leg,'UserData',legdata);
            		end
             end
	    
             if ~isempty(handleM)&~isempty(legdata)
            		
            		pos=handleM==legdata.handles;
            		
            		if any(pos)
            			legdata.lstrings{pos}='Large Eq';
            			set(leg,'UserData',legdata);
            			try 
            				set(leg,'String',legdata.lstrings);
            			end	
            		else
            			legdata.handles(end+1)=handleM;
            			legdata.lstrings{end+1}='LargeEq';
            			set(leg,'UserData',legdata);
            		end
             end
             
             disp(['Addon time: ',num2str(toc)]);
             
             
             %in case of autoprofile
             if strcmp(RegRange,'line')
           	if obj.AutoProfile
                	obj.startDepthSlice;
                end
             end
             
             
        end
        
        
        




        function plotHist(obj,plotAxis)
            import mapseis.plot.*;
            import mapseis.projector.*;

            % Plot the appropriate histogram depending on histType parameter
            switch obj.HistType
                case 'mag'
                    PlotMagHist(plotAxis,...
                        getMagnitudes(obj.DataStore,obj.FilterIndexVect));
                case 'depth'
                    PlotDepthHist(plotAxis,...
                        getDepths(obj.DataStore,obj.FilterIndexVect));
                case 'hour'
                    PlotHourHist(plotAxis,...
                        getHours(obj.DataStore,obj.FilterIndexVect));
                case 'FMD'
                    PlotFMD(plotAxis,...
                        getMagnitudes(obj.DataStore,obj.FilterIndexVect));
                otherwise
            end
        end




        function plotTime(obj,plotAxis)
            import mapseis.plot.*;
            import mapseis.projector.*;

            % Plot the appropriate histogram depending on histType parameter
            switch obj.TimeType
                case 'cumnum'
                    PlotTime(plotAxis,...
                        getDecYear(obj.DataStore,obj.FilterIndexVect));
                    
                       if obj.MarkEQ
				axes(plotAxis);
				lonLimi=xlim(plotAxis);
				latLimi=ylim(plotAxis);   
				[handle legendentry]=PlotLargeEQ(plotAxis,obj.DataStore,obj.FilterIndexVect,'time');
				axes(plotAxis);
				hold off;
				xlim(plotAxis,lonLimi);
				ylim(plotAxis,latLimi);
            end
		case 'stem'
                    PlotTimeStem(plotAxis,...
                        getDecYear(obj.DataStore,obj.FilterIndexVect),...
                        getMagnitudes(obj.DataStore,obj.FilterIndexVect));
                case 'timedepths'
                    PlotTimeDepth(plotAxis,...
                        getDecYear(obj.DataStore,obj.FilterIndexVect),...
                        getDepths(obj.DataStore,obj.FilterIndexVect));
                otherwise
            end
        end




        function createHistContextMenu(obj,parentAxis)
            % Create a context menu for the histogram in the top right subplot,
            % that selects the histogram type

            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.MainGUI);
            % Now make the menu be associated with the correct axis
            set(obj.MainGUI,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different histogram types
            uimenu(cmenu,'Label','Magnitude','Callback',...
                @(s,e) setHistType(obj,'mag'));
            uimenu(cmenu,'Label','Depth','Callback',...
                @(s,e) setHistType(obj,'depth'));
            uimenu(cmenu,'Label','Hour','Callback',...
                @(s,e) setHistType(obj,'hour'));
            uimenu(cmenu,'Label','Cumulative FMD',...
                'Callback',@(s,e) setHistType(obj,'FMD'));
            uimenu(cmenu,'Label','Plot this frame in new window ',...
                'Callback',...
                @(s,e) plotInNewFigure(obj,'Histogram',@plotHist));
        end




        function setHistType(obj,newHistType)
            % Set the parameter to plot on the histogram plot
            obj.HistType = newHistType;
            % Update the GUI
            updateGUI_leftaxesonly(obj);
        end

        function setTimeType(obj,newTimeType)
            % Set the parameter to plot on the histogram plot
            obj.TimeType = newTimeType;
            % Update the GUI
            updateGUI_leftaxesonly(obj);
        end




        function createMainAxisContextMenu(obj,parentAxis)
            import mapseis.plot.SetRegion;

            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.MainGUI);
            % Now make the menu be associated with the correct axis
            set(obj.MainGUI,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different region selections
           % regionFilter = obj.FilterList.getByName('Region');
           % dataStore = obj.DataStore;
            uimenu(cmenu,'Label','Select all',...
                'Callback',@(s,e) SetRegionGlove(obj,'all'));
            uimenu(cmenu,'Label','Select inside',...
                'Callback',@(s,e) SetRegionGlove(obj,'in'));
            uimenu(cmenu,'Label','Select Profile',...
                'Callback',@(s,e) SetRegionGlove(obj,'polygon'));
            uimenu(cmenu,'Label','Select outside',...
                'Callback',@(s,e) SetRegionGlove(obj,'out'));
            uimenu(cmenu,'Label','Select circular region',...
                'Callback',@(s,e) SetRegionGlove(obj,'circle'));
            uimenu(cmenu,'Label','Redraw Map',...
                'Callback',@(s,e) updateGUI(obj));
            uimenu(cmenu,'Label','Plot this frame in new figure ',...
                'Callback',@(s,e) plotInNewFigure(obj,...
                'Event Locations',@plotRegion));
        end

        function SetRegionGlove(obj,WhichOne)
         	import mapseis.plot.SetRegion;
         	
         	regionFilter = obj.FilterList.getByName('Region');
         	dataStore = obj.DataStore;
        	SetRegion(obj,regionFilter,WhichOne,dataStore);
         	
         	if obj.AutoProfile
         		switch  WhichOne
				case {'in','out'}
                			obj.ListProxy.PrefixQuiet('DepthWindow');
					
					try
						close(obj.DepthWindow.ResultGUI);
					end
					
					try
						clear(obj.DepthWindow);
					end
                	
                			
            			case 'line'
                			obj.startDepthSlice;
                			
                			                			
            			case 'circle'
            				obj.ListProxy.PrefixQuiet('DepthWindow');
					
					try
						close(obj.DepthWindow.ResultGUI);
					end
					
					try
						clear(obj.DepthWindow);
					end
        
                			
            			case 'all'
            		

                			obj.ListProxy.PrefixQuiet('DepthWindow');
					
					try
						close(obj.DepthWindow.ResultGUI);
					end
					
					try
						clear(obj.DepthWindow);
					end
                	end

         	
         	end
         	
         
        end

        
        function setCircle(obj)
            import mapseis.region.Region;
            import mapseis.util.Ginput;

            % Set the selection region by using a circle defined by radius
            % value

            % Define the circle boundary
            thetaVals = linspace(0,2*pi,101)';
            circleBoundary = obj.SelectRadius*[cos(thetaVals) sin(thetaVals)];
            % Get the centre of the circle
            centreOffset = Ginput(1);
            % Add the centre offset to the circle
            cicleBoundary = circleBoundary + ...
                repmat(centreOffset,numel(circleBoundary(:,1)),1);
            pRegion = Region(cicleBoundary);
            % Get the region filter
            regionFilter = obj.FilterList.getByName('Region');
            regionFilter.setRegion('in',pRegion);
            % Execute the filter
            regionFilter.execute(obj.DataStore);
        end




        function plotInNewFigure(obj,figTitleStr,plotFun)
            import mapseis.util.gui.*;

            % Plot the region plot in a new figure for printing.  The new
            % figure will be decoupled from the underlying data.
            % Create a new figure
            newGUI = onePanelLayout(figTitleStr);
            % Get the main plot axis
            plotAxis = findobj(newGUI,'Tag','axis');
            % Plot the region plot
            plotFun(obj,plotAxis)
            % Save the filter parameters to the UserData of the figure
            %             set(newGUI,'UserData',struct(...
            %                 'timeFilter',{obj.FilterList.getTimeRange()},...
            %                 'depthFilter',{obj.FilterList.getDepthRange()},...
            %                 'magnitudeFilter',{obj.FilterList.getMagnitudeRange()},...
            %                 'regionFilter',{obj.FilterList.getRegion()}...
            %                 ));
            % Make the GUI visible
            set(newGUI,'Visible','on');
        end




        function saveData(obj)
            % Save the datastore and filter list struct to a .mat file
            [fName,pName] = uiputfile('*.mat','Enter save file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected.  Data not saved');
            else
                dataStore = obj.DataStore; %#ok<NASGU>
                filterList = obj.FilterList; %#ok<NASGU>
                filterList.PackIt;
                save(fullfile(pName,fName),'dataStore','filterList');
                msgbox('Catalog & Filterlist saved')
            end
        end

        function saveSelectedData(obj)
            % Save only the data columns to .mat file, ie not filter settings
            % as well
            [fName,pName] = uiputfile('*.mat','Enter save file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected.  Data not saved');
            else
                dataCols = obj.DataStore.getFields([],obj.FilterIndexVect); %#ok<NASGU>
                save(fullfile(pName,fName),'dataCols');
            end
        end

	function CutSelectedData(obj)
            % Replacement for the saveSelectedData, cuts the catalog without saving
            

                obj.DataStore.CutCatalog(obj.FilterIndexVect); %#ok<NASGU>
                
        end

	

       


		function loadCatalog(obj,filename)
		
			import mapseis.*;
			import mapseis.datastore.*;
			import mapseis.importfilter.*;
				
					 
					 %open load dialog if no filename specified
					 if nargin<2
						[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
          					  if isequal(fName,0) || isequal(pName,0)
               						 disp('No filename selected. ');
           					  else
								     filename=fullfile(pName,fName);
							  end	     
					 end
			%----------------------------------------------------------------------------------
					
					%load catalog
					prevData = load(filename);
			
			%----------------------------------------------------------------------------------
			
			%Determine what datatyp it is and add necessary filters
		if isfield(prevData,'dataStore')
                    % We have loaded a saved session, including filter values
                    dataStore = prevData.dataStore;
                    try
                    	filterList = prevData.filterList
						%filterList.ListProxy = obj.ListProxy;
                    catch
                    	filterList = DefaultFilters(dataStore,obj.ListProxy);
                    	%filterList = obj.FilterList;	
                    end
                    	
                elseif isfield(prevData,'dataCols')
                    % We have loaded only the data columns, which now need to be turned
                    % into an mapseis.datastore.DataStore
                    dataStore = mapseis.datastore.DataStore(prevData.dataCols);
                    setDefaultUserData(dataStore);
                    
                    %create new filterlist
               		filterList = DefaultFilters(dataStore,obj.ListProxy);

                elseif isfield(prevData,'a')
                    % We have loaded in a ZMAP format file with data in matrix 'a'
                    dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
                    setDefaultUserData(dataStore)
                    
                    %create new filterlist
               		filterList = DefaultFilters(dataStore,obj.ListProxy);
                    
                else
                    error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');
                end
             
             		%----------------------------------------------------------------------------------		 
             		%check if bufferdata and userdata exists
             		try %check wether the datastore is already buffered
                		temp=dataStore.getUserData('DecYear');
                		catch
                		dataStore.bufferDate;
                	end
                	
                	try %check wether the plot quality is already set
                		temp=datStore.getUserData('PlotQuality');
                		catch
                		dataStore.SetPlotQuality;
                	end

                
			%----------------------------------------------------------------------------------		
   					%pushit to the Commander
           			if isempty(fName) 
            				fName = 'noname';
           		 	end	 
            		
            		           		
            		%write filename and path to userdata
					dataStore.setUserData('filename',fName);
					dataStore.setUserData('filepath',filename);
					
					
					
					%pushit to the commander
           			 newdata = struct('DataName',fName,...
            				  'Datastore',dataStore,...
            				  'FilterName',filterList.getName,...
                 			  'Filterlist',filterList);
            				   

            		
            		%let's push it to the Commander            
            		obj.CommanderGUI.pushIt(newdata);
            		
            		%get the marked ones back
					keys = obj.CommanderGUI.getMarkedKey;
					
					obj.ShownDataID = keys.ShownDataID;
					obj.ShownFilterID = keys.ShownFilterID;
					
					%datastore hash
            		%dataentry = struct('Name',fName,...
            		%		  		   'Datastore',dataStore);
            
           			%obj.DataHash.set(num2str(obj.NextDataID),dataentry);
            		%obj.CurrentDataID = obj.NextDataID;
            		%obj.NextDataID = obj.NextDataID+1;
					
					%filterhash                
                	%filterentry = struct('Name',fName,...
            		%		  		     'Filterlist',filterList);
            
           			%obj.FilterHash.set(num2str(obj.NextFilterID),filterentry);
            		%obj.CurrentFilterID = obj.NextFilterID;
            		%obj.NextFilterID = obj.NextFilterID+1;

			%----------------------------------------------------------------------------------
			%make the new loaded catalog the current one
			 	obj.FilterList=filterList;	
                obj.ParamGUI.FilterList=filterList;
                obj.FilterList.update;
               	obj.DataStore = dataStore;
      			
            	%reapply the listeners
            	relisten(obj);
            	
            	% Update the GUI to plot the initial data
		        updateGUI(obj);

			
		
		end
		



        function loadZMAPData(obj)
            import mapseis.datastore.*;
            import mapseis.importfilter.*;
            import mapseis.gui.*;
            import mapseis.projector.*;
            import mapseis.calc.*;

            % Load ZMAP file
            % as well
            [fName,pName] = uigetfile('*.mat','Enter ZMAP or MapSeis *.mat file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected. ');
            else
                                 close all;
                prevData = load(fullfile(pName,fName));
                % We have loaded in a ZMAP format file with data in matrix 'a'
                dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
                %buffer the dates
                dataStore.bufferDate;
                dataStore.SetPlotQuality;
                % Set the filterList to refer to this dataStore
                obj.FilterList.DataStore = dataStore;
                obj.FilterList.update();
                % Get the data out of the file we have loaded
                newData = dataStore.getFields();
                % Set the data of the current DataStore property to be the
                % data that we've just loaded in.
                obj.DataStore.setData(newData);
                mapseis.startApp(fullfile(pName,fName));
            end
        end




        function startMapAnalysis(obj)
            import mapseis.datastore.*;
			import mapseis.gui.*;
			import mapseis.projector.*;
			import mapseis.calc.*;
			import mapseis.plot.SetRegion;


            if isempty(obj.CalcGUI)
            
		            %set the region filter to all if not set otherwise
		            regionFilter = obj.FilterList.getByName('Region');
		            filterRegion = getRegion(regionFilter);
		            pRegion = filterRegion{2};
					if isempty(pRegion)
		            	SetRegion(regionFilter,'all',obj.DataStore);
		            end
		            
		            bMeanMagCalcObject = mapseis.calc.Calculation(...
		    			@mapseis.calc.CalcBMeanMag,@mapseis.projector.getMagnitudes,{},...
		    			'Name','b Mean Mag');
		            %guiWindowStruct.calcStruct=CalcGridGUI(obj.DataStore,'BMeanMagView',...
		             %   struct('inputProjFun',@mapseis.projector.getMagnitudes,...
		              %  'calcFun',@mapseis.calc.CalcBMeanMag,...
		               % 'outputProjFun',@(x) x.meanMag),obj.FilterList);
		 			
		 			 obj.CalcGUI=CalcGridGUI(obj.DataStore,'BMeanMagView',...
						    bMeanMagCalcObject,...
		    				@(x) x.meanMag,obj.FilterList);
		
					%addlistener(obj.FilterList,'Update',...
					%	    @(s,e) obj.CalcGUI.updateGUI());
						    
					obj.ListProxy.listen(obj.FilterList,'Update',...
						    @(s,e) obj.CalcGUI.updateGUI());	    
    		else
    				obj.CalcGUI.rebuildGUI(obj.DataStore, obj.FilterList);
    				obj.ListProxy.listen(obj.FilterList,'Update',...
						    @(s,e) obj.CalcGUI.updateGUI());		
    		end
        end



        function openParamWindow(obj)
            import mapseis.datastore.*;
			import mapseis.importfilter.*;
			import mapseis.gui.*;
			import mapseis.projector.*;
			import mapseis.calc.*;


            if isempty(obj.ParamGUI)
				obj.ParamGUI=ParamGUI(obj.DataStore,obj.FilterList);
        	
        	else
        		obj.ParamGUI.rebuildGUI;
        	
        	end
        	
        end


		

        function createTimeAxisContextMenu(obj,parentAxis)
            % Create a context menu for the time axis

            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.MainGUI);
            % Now make the menu be associated with the correct axis
            set(obj.MainGUI,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different histogram types
            uimenu(cmenu,'Label','Cumulative Number of Events',...
                'Callback',@(s,e) setTimeType(obj,'cumnum'));
            uimenu(cmenu,'Label','Time-Magnitude Stem Plot',...
                'Callback',@(s,e) setTimeType(obj,'stem'));
            uimenu(cmenu,'Label','Time-Depth Plot',...
                'Callback',@(s,e) setTimeType(obj,'timedepths'));
            uimenu(cmenu,'Label','Plot this frame in new figure ',...
                'Callback',@(s,e) plotInNewFigure(obj,'Time',@plotTime));

        end


       
        function setMenu(obj)
            g = obj.MainGUI;
            
            obj.OptionMenu = uimenu(g,'Label','- Plot Option');
            uimenu(obj.OptionMenu,'Label','Zmap style',...
                'Callback',@(s,e) obj.setqual('low'));
            uimenu(obj.OptionMenu,'Label','Medium Quality Plot',...
                'Callback',@(s,e) obj.setqual('med'));
            uimenu(obj.OptionMenu,'Label','High Quality Plot',...
                'Callback',@(s,e) obj.setqual('hi'));
            
            uimenu(obj.OptionMenu,'Label','Legend on/off','Separator','on',...
                'Callback',@(s,e) obj.legendary('set'));
            
            uimenu(obj.OptionMenu,'Label','Show Coastline','Separator','on',...
                'Callback',@(s,e) obj.setparam('Show Coastline','CoastToggle'));    
            uimenu(obj.OptionMenu,'Label','Show Internal Borders',...
                'Callback',@(s,e) obj.setparam('Show Internal Borders','BorderToggle'));    
	    uimenu(obj.OptionMenu,'Label','Show Seismic Stations',...
                'Callback',@(s,e) obj.setparam('Show Seismic Stations','StationToggle'));  
            uimenu(obj.OptionMenu,'Label','Mark Largest Earthquakes',...
                'Callback',@(s,e) obj.setparam('Mark Largest Earthquakes','MarkEQ'));  
            uimenu(obj.OptionMenu,'Label','Draw Plate Boundaries',...
                'Callback',@(s,e) obj.setparam('Draw Plate Boundaries','PlateToggle'));  
            uimenu(obj.OptionMenu,'Label','Automatic DepthProfile','Separator','on',...
                'Callback',@(s,e) obj.setparam('Automatic DepthProfile','AutoProfile'));
            uimenu(obj.OptionMenu,'Label','Edit Plate Boundary Plot Config',...
                'Callback',@(s,e) obj.setPlatePlotConfig);    
            uimenu(obj.OptionMenu,'Label','Developer Mode',...
                'Callback',@(s,e) obj.setDeveloper('toggle'));
             
	    obj.setqual('chk');
            
	    
            
            obj.DataMenu = uimenu(g,'Label','- Data');
            uimenu(obj.DataMenu,'Label','Save as MapSeis Session',...
                'Callback',@(s,e) obj.saveData);
            uimenu(obj.DataMenu,'Label','Cut Selected Data (cut at filters)...',...
                'Callback',@(s,e) obj.CutSelectedData);
            uimenu(obj.DataMenu,'Label','Load MapSeis session *.mat file ..',...
                'Callback',@(s,e) obj.loadCatalog);
            uimenu(obj.DataMenu,'Label','Load ZMAP *.mat file ..',...
                'Callback',@(s,e) obj.loadCatalog);
            uimenu(obj.DataMenu,'Label','Add Station List ..',...
                'Callback',@(s,e) obj.AddStationList);
			
            obj.AnalysisMenu = uimenu(g,'Label','- Analysis','ForegroundColor','r');
            uimenu(obj.AnalysisMenu,'Label','Map view analysis',...
                'Callback',@(s,e) obj.startMapAnalysis);
            uimenu(obj.AnalysisMenu,'Label','Depth Profile','Enable','off',...
                'Callback',@(s,e) obj.startDepthSlice);
            uimenu(obj.AnalysisMenu,'Label','3D view',...
                'Callback',@(s,e) obj.startThirdDimension);
            uimenu(obj.AnalysisMenu,'Label','Open Parameter Window','Enable','on',...
                'Callback',@(s,e) obj.openParamWindow);
            uimenu(obj.AnalysisMenu,'Label','Set Profile Width',...
                'Callback',@(s,e) obj.setProfWidth);
	    uimenu(obj.AnalysisMenu,'Label','Set Circle Radius',...
                'Callback',@(s,e) obj.setCircleRadius);
        end

		
		function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	dataStore = obj.DataStore;
			Menu2Check(3) = findobj(obj.OptionMenu,'Label','Zmap style');
			Menu2Check(2) = findobj(obj.OptionMenu,'Label','Medium Quality Plot');
			Menu2Check(1) = findobj(obj.OptionMenu,'Label','High Quality Plot');
			
			%uncheck all menupoints
			set(Menu2Check(1), 'Checked', 'off');
			set(Menu2Check(2), 'Checked', 'off');
			set(Menu2Check(3), 'Checked', 'off');
			
 			switch qual
 				case 'low'
 					dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.updateGUI;
                	
				case 'med'
					dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.updateGUI;
					
				case 'hi'
					dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.updateGUI;
					
				case 'chk'
					try StoredQuality = dataStore.getUserData('PlotQuality');
						catch dataStore.setUserData('PlotQuality','med');
							  StoredQuality = dataStore.getUserData('PlotQuality');		
					end
							switch StoredQuality
								case 'low'
									sel=3;
								case 'med'
									sel=2;
								case 'hi'
									sel=1;
								end		
					set(Menu2Check(sel), 'Checked', 'on');
						
			end
		end
		
		
		
		
		
		
		function createCalcMenu(obj)
			%This function scans the folder calcLoaders in the mapseis directory and 
			%adds all functions to the menu
			import mapseis.util.emptier;
			
			%Reset Menu
			obj.CalculationMenus={};
			
			%get loaders
			functable = mapseis.gui.GUI.CalcSniffer(obj.MapSeisFolder);
			
			%get the Names and Categories of the the loaders
			for i=1:numel(functable)
				Descript=functable{i}([],'-description');
				CartName{i,1}=Descript.Category;
				CartName{i,2}=Descript.displayname;
				CartName{i,3}=Descript.description;
				CartName{i,4}=Descript.SubCategory;
			end
			
			%exclude the Templates
			DoNotUse=strcmp(CartName(:,1),'Template');
			CartName=CartName(~DoNotUse,:);
			usedFunction=functable(~DoNotUse);
			%get the different Categries
			UniqueCats=unique(CartName(:,1));
			
			g = obj.MainGUI;
			
			%Build the menu entries
			for i=1:numel(UniqueCats)
				
				%TopMenu
				obj.CalculationMenus{i} = uimenu(g,'Label',UniqueCats{i});
				
				%The single menu entries
				InCategory=strcmp(CartName(:,1),UniqueCats{i});
				SubCats=CartName(InCategory,4);
				NamesInCat=CartName(InCategory,2);
				CategoryFunc=usedFunction(InCategory);
				
				%get all entries with out subcategory
				NoSub=logical(emptier(SubCats));
				%|strcmp(SubCats,'none');
				NoSubName=NamesInCat(NoSub);
				NoSubFunc=CategoryFunc(NoSub);
				
				
				%No the ones with subcategories
				inSubName=NamesInCat(~NoSub);
				inSubFunc=CategoryFunc(~NoSub);
				inSubSub=SubCats(~NoSub);
				
				uniSub=unique(inSubSub);
				for j=1:numel(uniSub)
					submenu=uimenu(obj.CalculationMenus{i},'Label',uniSub{j});
					inTheSub=strcmp(inSubSub,uniSub{j});
					TheNames=inSubName(inTheSub);
					TheFunc=inSubFunc(inTheSub);
					
					for k=1:numel(TheNames)
						uimenu(submenu,'Label',TheNames{k},...
						'Callback',@(s,e) obj.CallCalculation(TheFunc{k}));
					end
				
				end
				
				
				
				
				%Add the entries without subCategory
				for j=1:numel(NoSubName)
				
					uimenu(obj.CalculationMenus{i},'Label',NoSubName{j},...
					'Callback',@(s,e) obj.CallCalculation(NoSubFunc{j}));
				end	
			end		
		end
		
		
		
		function CallCalculation(obj,CalcHandle)
			%This function acts as man in the middle, and writes the recieved objects into
			%the internal storage
			
			TheObj=CalcHandle(obj,'-start');
			
			if isfield(TheObj,'CalcWrapper')
				obj.Wrapper = TheObj.CalcWrapper;
				obj.ResGUI = TheObj.ResultGUI;
				obj.ParGUI = TheObj.CalcParamGUI;
				
			
			else
				%do nothing at the moment
				obj.Wrapper = TheObj;
				obj.ResGUI = [];
				obj.ParGUI = [];
			end
		end
		
		
		
		function setparam(obj,mlabel,guipar)
			%sets the plot parameter and writes it to the datastore and updates the gui
			
		 	dataStore = obj.DataStore;
			Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
			
			%get pararmeter from gui
			cstate=obj.(guipar);

			set(Menu2Check, 'Checked', 'off');
			
			%change the to the opposite
			cstate=~cstate;
			
			%write into gui and datastore	
			obj.(guipar)=cstate;
			dataStore.setUserData(guipar,cstate);
			
			%change check
				if cstate
					set(Menu2Check, 'Checked', 'on');
				end
			obj.updateGUI;
		end
		
		
		function getparam(obj,mlabel,guipar)
			%sets the plot parameter and writes it to the datastore and updates the gui
			
		 	dataStore = obj.DataStore;
			Menu2Check= findobj(obj.OptionMenu,'Label',mlabel);
			
			%get pararmeter from datastore
			try 
				cstate = dataStore.getUserData(guipar);
			
			catch 
				dataStore.setUserData(guipar,true);
				 cstate = dataStore.getUserData(guipar);	
			end				  
			
			obj.(guipar) = cstate;
				
			set(Menu2Check, 'Checked', 'off');
			
				if cstate
					set(Menu2Check, 'Checked', 'on');
				end
			
		end
		
		function setDeveloper(obj,doWhat)
        		Menu2Check= findobj(obj.OptionMenu,'Label','Developer Mode');
        		TheInvisible=findobj(obj.MainGUI,'Label','Dev-Utils');
			switch doWhat
				case 'toggle'
					if isempty(obj.DeveloperMode)
						obj.DeveloperMode=false;
					end
					obj.DeveloperMode=~obj.DeveloperMode;
					
				case 'init'
					obj.DeveloperMode=false;
				case 'reinit'
					%do nothing
					
			end
			
			set(Menu2Check, 'Checked', 'off');
			set(TheInvisible,'Visible','off');
			%change check
			if obj.DeveloperMode
				set(Menu2Check, 'Checked', 'on');
				set(TheInvisible,'Visible','on')
			end
			
			
			
			%obj.updateGUI;
			
			
		end

		
		
		
		function legendary(obj,com)
			%switches the legend in the plot on and off
			%or just checks it
			
			dataStore = obj.DataStore;
			Menu2Check= findobj(obj.OptionMenu,'Label','Legend on/off');
			
			switch com
				case 'chk'
					try leg = dataStore.getUserData('LegendToggle');
						catch dataStore.setUserData('LegendToggle',true);
							  leg = dataStore.getUserData('LegendToggle');	
					end
					
						obj.LegendToggle=leg;
						
						set(Menu2Check, 'Checked', 'off');
			
						if leg
							set(Menu2Check, 'Checked', 'on');
						end

				case 'set'		
					obj.LegendToggle=~obj.LegendToggle;
					dataStore.setUserData('LegendToggle',obj.LegendToggle);
					leg=obj.LegendToggle;
					
					g = obj.MainGUI;
					plotAxis = findobj(g,'Tag','axis_left');
					
											
						if leg
							set(Menu2Check, 'Checked', 'on');
							legend(plotAxis,'show');
						else
							set(Menu2Check, 'Checked', 'off');
							legend(plotAxis,'hide');
						end

			end	
							  

		
		end
		
		
		function startThirdDimension(obj)
				import mapseis.datastore.*;
				import mapseis.gui.*;
				import mapseis.projector.*;
				import mapseis.calc.*;
				import mapseis.plot.SetRegion;
				
				
				if ~isempty(obj.ThirdDimension)
					
					try
						obj.ThirdDimension.Rebuild;
						%obj.DepthWindow.UpdateGUI;
					catch
						
						disp('Listeners DepthWindow stopped')
						obj.ListProxy.PrefixQuiet('3DWindow');
					
					
					
						try
							close(obj.ThirdDimension.ResultGUI);
						end
					
						try
							clear(obj.ThirdDimension)
						end
						obj.ThirdDimension=ThirdSpace(obj.ListProxy,obj.CommanderGUI,obj,'light');
					end	
				else
				
					obj.ThirdDimension=ThirdSpace(obj.ListProxy,obj.CommanderGUI,obj,'light');
				
				end
				
				
		end

		function startDepthSlice(obj)
				import mapseis.datastore.*;
				import mapseis.gui.*;
				import mapseis.projector.*;
				import mapseis.calc.*;
				import mapseis.plot.SetRegion;
				
				%old stuff
				%if isempty(obj.CalcDepthGUI)
				%		bMeanMagSliceCalcObject = mapseis.calc.Calculation(...
		    		%			@mapseis.calc.CalcBMeanMag,@mapseis.projector.getMagnitudes,{},...
		    		%			'Name','b Mean Mag');
		             	%		
		 		%	 	obj.CalcDepthGUI=CalcGridSliceGUI(obj.DataStore,'BMeanMagView',...
				%		    	bMeanMagSliceCalcObject,...
		    		%			@(x) x.meanMag,0.5,obj.FilterList);
		
				%		%addlistener(obj.FilterList,'Update',...
						%	    @(s,e) guiWindowStruct.calcSliceStruct.updateGUI());
				%		obj.ListProxy.listen(obj.FilterList,'Update',...
				%		    @(s,e) obj.CalcDepthGUI.updateGUI());	
				%else
				%		obj.CalcDepthGUI.rebuildGUI(obj.DataStore,obj.FilterList);
				%		obj.ListProxy.listen(obj.FilterList,'Update',...
				%		    @(s,e) obj.CalcDepthGUI.updateGUI());	
				%		
				%end		
				
				%kill old ones
				if ~isempty(obj.DepthWindow)
					
					try
						obj.DepthWindow.Rebuild;
						%obj.DepthWindow.UpdateGUI;
					catch
						
						disp('Listeners DepthWindow stopped')
						obj.ListProxy.PrefixQuiet('DepthWindow');
					
					
					
						try
							close(obj.DepthWindow.ResultGUI);
						end
					
						try
							clear(obj.DepthWindow)
						end
						obj.DepthWindow=DepthProfiler(obj.ListProxy,obj.CommanderGUI,obj);
					end	
				else
				
					obj.DepthWindow=DepthProfiler(obj.ListProxy,obj.CommanderGUI,obj);
				
				end
				
				
		end



		function setProfWidth(obj)
				import mapseis.gui.setProfileWidthGUI;
				import mapseis.region.filterupdater;
				
				
				
				%check if a profile is already existing 
				regionFilter = obj.FilterList.getByName('Region');
				setProfileWidthGUI(obj.DataStore,regionFilter);
				regparams =regionFilter.getDepthRegion;
				
				if strcmp(regparams{1},'line')
						 pRegion = regparams{2};
						 rBdry = pRegion.getBoundary();
						 pos = [rBdry(1,:);rBdry(4,:)];
						 filterupdater(pos,obj.DataStore,regionFilter);
						 regionFilter.execute(obj.DataStore);
						 obj.updateGUI;
				end
				
					
		end
		
		
    	
    	function setCircleRadius(obj)
				import mapseis.gui.setCircleRadiusGUI;
				import mapseis.region.filterupdater;
				
				
				
				%check if a profile is already existing 
				regionFilter = obj.FilterList.getByName('Region');
				
				setCircleRadiusGUI(obj.DataStore,regionFilter);
				
				regparams =regionFilter.getRegion;
								
				if strcmp(regparams{1},'circle');
						 %reload the radius from the datastore	
						 %datagrid = obj.DataStore.getUserData('gridPars');
						 rad=regionFilter.Radius;			
						 
						 %rebuild the region filter
						 pRegion = regparams{2};
						 filterupdater(pRegion,obj.DataStore,regionFilter);
						 regionFilter.execute(obj.DataStore);
						 obj.updateGUI;
				end
				
					
		end

    	function setPlatePlotConfig(obj)
    		%sets the plot config for the plate boundary plot
    		if ~isstruct(obj.PlatePlotSetting)
			obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
							'Ridge',true,...
							'Transform',true,...
							'Trench',true,...
							'RidgeLineStylePreset',1,...
							'TransformLineStylePreset',1,...
							'TrenchLineStylePreset',1,...
							'RidgeColors',2,...
							'TransformColors',3,...
							'TrenchColors',1);
		end
		
		LineStyles={'normal','Fineline','dotted','Fatline'};
		ColorList={'blue','red','green','black','cyan','magenta'}; 
		
		Title = 'Select Plot Config';
		Prompt={	'Plot Ridges:', 'Ridge';...
				'Ridge Color:','RidgeColors';...
				'Ridge LineStyle:','RidgeLineStylePreset';...
				'Plot Transforms:', 'Transform';...
				'Transforms Color:','TransformColors';...
				'Transforms LineStyle:','TransformLineStylePreset';...
				'Plot Trenches:', 'Trench';...
				'Trenches Color:','TrenchColors';...
				'Trenches LineStyle:','TrenchLineStylePreset'};			
		%Ridge
		Formats2(1,1).type='check';		
		 
		Formats2(1,2).type='list';
		Formats2(1,2).style='popupmenu';
		Formats2(1,2).items=ColorList;
		 
		Formats2(1,3).type='list';
		Formats2(1,3).style='popupmenu';
		Formats2(1,3).items=LineStyles;
		    
		%Transform
		Formats2(2,1).type='check';		
		  
		Formats2(2,2).type='list';
		Formats2(2,2).style='popupmenu';
		Formats2(2,2).items=ColorList;
		    
		Formats2(2,3).type='list';
		Formats2(2,3).style='popupmenu';
		Formats2(2,3).items=LineStyles;
		   
		%Trench
		Formats2(3,1).type='check';		
		    
		Formats2(3,2).type='list';
		Formats2(3,2).style='popupmenu';
		Formats2(3,2).items=ColorList;
		    
		Formats2(3,3).type='list';
		Formats2(3,3).style='popupmenu';
		Formats2(3,3).items=LineStyles;
						
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
	
		
		
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats2,obj.PlatePlotSetting,Options); 	
		    
		if Canceled~=1
			obj.PlatePlotSetting=NewParameter;
			obj.updateGUI;
		end	
	    
	    
	    
    	
    	end
    	
    	
    	function setExistingWindows(obj,paramWindow,calcWindow)
    			%allows to set the gui properties to prevent extra
    			%opening of existing windows
    			
    			if nargin<3
    					obj.ParamGUI = paramWindow;
    			else
    					obj.ParamGUI = paramWindow;		
    					obj.CalcGUI = calcWindow;	
    	
   				end
   				 	
    	
    	end 
    
    
    	
    	function rebuildGUI(obj)
    			%allows to rebuild a closed window
    	
    			
    			%try
    			%	set(obj.MainGUI,'Visible','on');
    				
    			%catch
    			    try
    			       clf(obj.MainGUI);
    			       set(obj.MainGUI,'Visible','on');
    			    end
    			    
		    	    import mapseis.gui.*;
		            import mapseis.util.gui.*;
					
					%delete(obj.MainGUI);
		            dataStore = obj.DataStore;
		            filterList = obj.FilterList;
		            obj.FilterIndexVect = filterList.getSelected();
		            %obj.MainGUI = [];
		           		            
		            % Set the properties of the figure window
		            DefaultPos=[ 50 300 1100 690 ];
		            
		            
		            %change screensize if needed
			    MonSize=get(0,'ScreenSize');
			    if 0.9*MonSize(3)<=DefaultPos(3)|0.9*MonSize(4)<=DefaultPos(4);
				Screener=[50 150 MonSize(3)*0.8 MonSize(4)*0.9-150];	
			    else
				Screener=DefaultPos;
			    end
		            mainGUIFigureProperties = {'Position',Screener,'Renderer','painters'};
		
		            % Set the axes properties by hand in the order left plot, top right, bottom
		            % right
		            mainGUIAxesProperties = {...
		                {'pos',[ 0.06 0.1 0.55 0.85],'FontSize',12},...
		                {'pos',[ 0.7 0.58 0.25 0.37],'FontSize',12},...
		                {'pos',[ 0.7 0.1 0.25 0.4],'FontSize',12}};
		
		            obj.MainGUI = threePanelLayout('MapSeis Main Window',...
		                mainGUIFigureProperties,mainGUIAxesProperties,obj.MainGUI);
		                
		             set(obj.MainGUI,'ToolBar','figure');
		            % Register this view with the model.  The subject of the view uses this
		            % function handle to update the view.
		            %dataStore.subscribe('MainView',@() updateGUI(obj));
		            try
		            	obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'main');
		            	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
		            end
					% Create the menu
		            setMenu(obj);
		            createCalcMenu(obj);
		            obj.setDeveloper('reinit');
		            
		            
		            %set plot parameters
			    obj.legendary('chk');
			    obj.getparam('Show Coastline','CoastToggle');
			    obj.getparam('Show Internal Borders','BorderToggle');
			    obj.getparam('Show Seismic Stations','StationToggle'); 
			    obj.getparam('Mark Largest Earthquakes','MarkEQ');
			    obj.getparam('Automatic DepthProfile','AutoProfile');
			    obj.getparam('Draw Plate Boundaries','PlateToggle');
			    
			    
			    if isempty(obj.PlatePlotSetting)
			    
			    
				    obj.PlatePlotSetting= struct(	'PlotType','PlateBoundary',...
							'Ridge',true,...
							'Transform',true,...
							'Trench',true,...
							'RidgeLineStylePreset',1,...
							'TransformLineStylePreset',1,...
							'TrenchLineStylePreset',1,...
							'RidgeColors',2,...
							'TransformColors',3,...
							'TrenchColors',1);
			    end				
			    
			    
		            % Create a context menu to choose histogram type
		            % NB: context menu only appears if you right click in the histogram window,
		            % but not actually on the histogram bars themselves
		            createHistContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_top_right'));
		
		            % Create context menu for the main event plot axis to select the region of
		            % interest
		            createMainAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_left'));
		
		            % Create context menu for the main event plot axis to select the region of
		            % interest
		            createTimeAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_bottom_right'));
		
		
		            try
		                mainPars = dataStore.getUserData('mainPars');
		                obj.SelectRadius = mainPars.selectRadius;
		                obj.HistType = mainPars.histType;
		                obj.TimeType = mainPars.timeType;
		            catch ME
		                % Show different histogram plots depending on histType
		                % {'mag','depth',...}, set from hist plot context menu.
		                obj.HistType = 'mag';
		
		                % Show different time plots depending on timeType, set from time plot
		                % context menu.
		                obj.TimeType = 'cumnum';
		                % Radius for selection about a point
		                obj.SelectRadius = 0.25;
		            end
					
					%update Button
					updateButton = uicontrol(obj.MainGUI, 'Units', 'Normalized',...
                		'Position',[0.0174    0.0174    0.1055    0.0500],'String','Update',...
                		'Callback',@(s,e) obj.updateGUI(true));
										
		            % Update the GUI to plot the initial data
		            updateGUI(obj);
		            
		            %load plugIns
		            obj.startPlugs;
		            
		            % Make the GUI visible
		            set(obj.MainGUI,'Visible','on');
		
   					
    			
    			
    			%end
    			
    			    			
    			
    			
    	end
    	
    	
    	
    	
    	function wantedaxis = reTag(obj,whichone)
    				%This function can help in case of lost 'axis_left' tags errors
    				
    				%get all childrens of the main gui
    				gracomp = get(obj.MainGUI, 'Children');
    				
    				%find axis and return axis
    				wantedaxis = findobj(gracomp,'Type','axes','Tag','');
    				    				
    				%correct 
    				switch whichone
    					
    					case 'left'
    						set(wantedaxis,'Tag','axis_left');
    					
    					case 'right_top'
    						set(wantedaxis,'Tag','axis_top_right');
    			
    					case 'right_bottom'
    						set(wantedaxis,'Tag','axis_bottom_right');
    				end		
    			end
		
	
	
		function relisten(obj)
				%Allows to reset all listeneres over the list proxy and reconnect them to the current filterlist 
				%and datastore		
				import mapseis.gui.*;
				%reapply listeners
				
					
				%clear listeners first
					%obj.ListProxy.shoutup;
					
					%suicide gui
					%obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'main');
					%obj.ListProxy.quiet(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');
					obj.ListProxy.PrefixQuiet('main');
					clf(obj.MainGUI);
					obj.rebuildGUI;	
					
					%mute the internal listeners of the filterlist
					obj.FilterList.MuteList(obj.ListProxy);
					
					%from the rebuild part of this gui
					obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
					
							
										
					%reapply listeners
					%filterList = obj.FilterList;
					
					%obj.ListProxy.quiet(filterList,'Update',...
   					%			 @(s,e) obj.ParamGUI.updateGUI());
					
					%reapply listeners
					obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');
					
					obj.FilterList.reInitList(obj.ListProxy);
					
					
					
					
					%delete paramGUI and rebuild
					obj.ParamGUI.silence;
					close(obj.ParamGUI.mainGUI);
					clear obj.ParamGUI;
					obj.ParamGUI=ParamGUI(obj.DataStore,obj.FilterList,obj.ListProxy);
					
					
					%obj.ListProxy.listen(obj.FilterList,'Update',...
   					%			 @(s,e) obj.ParamGUI.updateGUI());
		
		end
		
		function quiet(obj)
			%kills all listeners not needed elsewhere
			import mapseis.gui.*;
			obj.ListProxy.PrefixQuiet('main');
			
			try
				%mute the internal listeners of the filterlist
				obj.FilterList.MuteList(obj.ListProxy);
			end
			
			obj.ParamGUI.silence;
			
			
		end
		
		
		
		function listen(obj)
			import mapseis.gui.*;
			%reapply listeners
			
			%from the rebuild part of this gui
			obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'main');
			obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'main');
					
			obj.FilterList.reInitList(obj.ListProxy);
			
			
		end
		
		
		function GuiRebuildSeq(obj)
			import mapseis.gui.*;
			
			%close(obj.MainGUI);
			clf(obj.MainGUI);
			obj.rebuildGUI;	
			
			close(obj.ParamGUI.mainGUI);
			clear obj.ParamGUI;
			obj.ParamGUI=ParamGUI(obj.DataStore,obj.FilterList,obj.ListProxy);
		
		end
		
		
		function MainGUISwitcher(obj,newdata)
				%input is a cell structur with either DataStore, Filterlist or DataStore and Filterlist
				%The function replaces the existing datastore/filterlist with the new one and updates the gui
				
				%silence all listeners
				obj.quiet;
				
				if isfield(newdata,'Datastore')
					obj.DataStore = newdata.Datastore;
					obj.ShownDataID = newdata.ShownDataID;
				end
				
				if isfield(newdata,'Filterlist')
					obj.FilterList = newdata.Filterlist;
					obj.ShownFilterID = newdata.ShownFilterID;
				end
				
				
				
				%set the filterlist to the new data
				obj.FilterList.changeData(obj.DataStore);
				obj.ParamGUI.FilterList=obj.FilterList;
				obj.FilterList.update;
				
				obj.GuiRebuildSeq;
				
				obj.listen;
				
				
				%reapply the listeners
				%relisten(obj);
            	
            	
				% Update the GUI to plot the initial data
				updateGUI(obj);
		
				
				
		
		end
		    
		
		function InitGUIPlugs(obj)
			%creates the list with plugins which will be called everytime the window is updated
			obj.GUIPlugIns=[];
			obj.PlugInMemory=[];
			
			import mapseis.util.importfilter.*;
			import mapseis.guiPlugIns.*;
			
			%get all necessary reg expressionpatterns
			rp = regexpPatternCatalog();
	
			%where are I now
			curdir=pwd;
			fs=filesep;
			
			cd([obj.MapSeisFolder,fs,'+mapseis',fs,'+guiPlugIns']);
			
			
			if ispc
				[sta res] = dos(['dir /b *.m']);
			else
				[sta res] = unix(['ls -1 *.m']);
			end
			
			lines = regexp(res,rp.line,'match');
			
			
			for i=1:numel(lines)
		
				[pathStr,nameStr{i},extStr] = fileparts(lines{i});
				%this has to be done for a certain object structur
				nameStr{i} = ['mapseis.guiPlugIns.',nameStr{i}];
		
			end
	
			obj.GUIPlugIns=cellfun(@str2func, nameStr, ...
			   'UniformOutput', false);
			
			
			%get back to original dir
			cd(curdir);
		
			
		
		end
		
		
		function startPlugs(obj)
			%This function will initalize every PlugIn in the obj.GUIPlugIns list.
			
			if ~isempty(obj.GUIPlugIns)
				for i=1:numel(obj.GUIPlugIns)
					obj.GUIPlugIns{i}(obj,'-Init');
				end
			
			end
			
		
		end
	
		
		
		
		function AddStationList(obj)
			%add a stationlist to the datastore
			
			import mapseis.special_import.importStationList;
			
			[fName,pName] = uigetfile('*.txt','Enter Stationlist *.txt file name...');
          			if isequal(fName,0) || isequal(pName,0)
               				 disp('No filename selected. ');
           			else
				         filename=fullfile(pName,fName);
				end
			try	
				StationList=importStationList(filename,'-file');
			catch
				disp('Corrupt Stationlist file')
				return
			end
			
			%add to datastore
			obj.DataStore.setUserData('StationList',StationList);
			
			
			
			
			
		end
    end
    
    
    
    
    methods (Static)
		function functable = CalcSniffer(homepath)
			% searches all Calculations in the path ./+mapseis/+calcLoaders and returns them
			% as function handles.
			
			import mapseis.util.importfilter.*;
			import mapseis.importfilter.*;
			
			%get all necessary reg expressionpatterns
			rp = regexpPatternCatalog();
	
			%where are I now
			curdir=pwd;
			fs=filesep;
			
			cd([homepath,fs,'+mapseis',fs,'+calcLoaders']);
			
			
			if ispc
				[sta res] = dos(['dir /b *.m']);
			else
				[sta res] = unix(['ls -1 *.m']);
			end
			
			lines = regexp(res,rp.line,'match');
			
			
			for i=1:numel(lines)
		
				[pathStr,nameStr{i},extStr] = fileparts(lines{i});
				%this has to be done for a certain object structur
				nameStr{i} = ['mapseis.calcLoaders.',nameStr{i}];
		
			end
	
			functable=cellfun(@str2func, nameStr, ...
			   'UniformOutput', false);
			
			
			%get back to original dir
			cd(curdir);
	
			
		end
	

	
	end
    
end
classdef ThirdSpace < handle
	
	%This function plots a 3D view of the main window
	
	properties
		ListProxy
		Commander
		Datastore
		SendedEvents
		Keys
		MainWindowGUI
		Filterlist
		ErrorDialog
		Region
		ResultGUI
		PlotQuality
		BorderToogle
		CoastToogle
		ShadowToogle
		LandToggle
		PlotOptions
		plotAxis
		PlotMode
		TheAspect
		AlphaMan
		LegendEntry
		PlotHandles
		LegendToogle
		
		
	end

	events
		CalcDone
	end


	methods
	
	
	function obj = ThirdSpace(ListProxy,Commander,MainWindowGUI,PlotMode)
		import mapseis.datastore.*;
		
		%The plotmode defines if everything ('full') is plotted from the MainWindow
		%(selected unselected and Polygons, etc) or if only the selected events are
		%Plotted ('light'), with 'region' the same mode like the maingui uses is
		%used, draw only selected by filterlist but differentiate between regionfilter
		%selected events;
		
		
		
		obj.ListProxy=ListProxy;
		obj.Commander=Commander;
		%obj.PlotMode=PlotMode;
		obj.PlotMode='light';		
		obj.Datastore = obj.Commander.getCurrentDatastore;
		obj.Filterlist = obj.Commander.getCurrentFilterlist; 
		obj.Keys=obj.Commander.getMarkedKey;
		selected=obj.Filterlist.getSelected;
		obj.SendedEvents=selected;
		obj.TheAspect=10;
		obj.AlphaMan='none';
		obj.BorderToogle=false;
		obj.CoastToogle=false;
		obj.ShadowToogle=true;
		obj.LandToggle=false;
		obj.LegendEntry={};
		obj.PlotHandles=[];
		obj.LegendToogle=false;
		
		%check if a profile is selected
		regionFilter = obj.Filterlist.getByName('Region');
		
		
		obj.ListProxy.PrefixQuiet('3DWindow');
		obj.ListProxy.listen(obj.Commander, 'Switched', @() obj.DataSwitcher,'3DWindow');
		if ~isempty(MainWindowGUI)
			%normal case, get everything form the mainGUI
			obj.MainWindowGUI=MainWindowGUI;
			obj.setqual('chkData');
			obj.ListProxy.listen(obj.MainWindowGUI, 'MainWindowUpdate', @() obj.updateGUI,'3DWindow');
		else
			obj.MainWindowGUI=[];
			obj.PlotQuality = 'low';
		
		end
			
		%Build Window
		obj.BuildResultGUI
		
		%plot eq
		obj.updateGUI;
		
		%Switched
		
	end
	
	
	function DataSwitcher(obj)
		%get new data
		obj.Datastore = obj.Commander.getCurrentDatastore;
		obj.Filterlist = obj.Commander.getCurrentFilterlist; 
		obj.Keys=obj.Commander.getMarkedKey;
		selected=obj.Filterlist.getSelected;
		obj.SendedEvents=selected;
		
		%check if the filter is still alright
		
		
	end
	
	
	
	function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
	
	
		pos=[50 300 1100 600];
	
		
		[existFlag,figNumber]=figflag('3DWindow',1);
		newbmapWindowFlag=~existFlag;                          
		disp(newbmapWindowFlag)
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('3DWindow',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		  %obj.ResultGUI=figure('name','3DWindow'); 
		  %set(obj.ResultGUI,'Position',pos,'Renderer','painters');
		  set(obj.ResultGUI,'Position',pos,'Color','w');
		   obj.plotAxis=gca;
		   set(obj.plotAxis,'Tag','MainResultAxis');
		   
		   obj.buildMenus;
		   
		end	
		set(obj.ResultGUI,'visible','on');
	end
	
	
	
	
	function buildMenus(obj)
		%sets the plot mode in plot mode, 'check' can be used to set the ticks in the menu
	
		obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
		
		uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
		uimenu(obj.PlotOptions,'Label','Set superelevation',... 
			'Callback',@(s,e) obj.setSuperElevation);
		uimenu(obj.PlotOptions,'Label','Set Transparency',... 
			'Callback',@(s,e) obj.setAlphaMan);

		uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
		uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
		uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			
		uimenu(obj.PlotOptions,'Label','Plot all Earthquakes','Separator','on',... 
			'Callback',@(s,e) obj.SetPlotMode('full'));
		uimenu(obj.PlotOptions,'Label','Main Window mode',...
			'Callback',@(s,e) obj.SetPlotMode('region'));	
		uimenu(obj.PlotOptions,'Label','Only Selected Earthquakes',...
			'Callback',@(s,e) obj.SetPlotMode('light'));
		
		uimenu(obj.PlotOptions,'Label','Draw Coastlines','Separator','on',... 
			'Callback',@(s,e) obj.TooglePlotOptions('Coast'));
		uimenu(obj.PlotOptions,'Label','Draw Country Borders',...
			'Callback',@(s,e) obj.TooglePlotOptions('Border'));
		uimenu(obj.PlotOptions,'Label','Draw Shadows',...
			'Callback',@(s,e) obj.TooglePlotOptions('Shadow'));
		uimenu(obj.PlotOptions,'Label','Draw LandMasses',...
			'Callback',@(s,e) obj.TooglePlotOptions('Land'));
		uimenu(obj.PlotOptions,'Label','Show Legend','Separator','on',... 
			'Callback',@(s,e) obj.LegendSwitcher);
			
		obj.TooglePlotOptions('-chk');
		obj.setqual('chk');
		obj.SetPlotMode('check');
		
	end
	
	
	function updateGUI(obj)
		import mapseis.plot.*;
		import mapseis.projector.*;
		
		if isempty(obj.plotAxis)
			 obj.plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(obj.plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				obj.plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(obj.plotAxis,'Tag','MainResultAxis');
    			end	
    			
		end
		
				
		plotAxis=obj.plotAxis;
		%figure
		%plotAxis=gca;
		
		
		%Empty legend and Handles
		obj.LegendEntry={};
		obj.PlotHandles=[];
		
		
		
		%if strcmp(obj.PlotQuality,'low')
		%	FastMode=true;
		%else
			FastMode=false;
		%end	
		
		%plot a top
		%earthsurf=[-180,-90;180,-90;180,90;-180,90;-180,-90];
		%TheSurf=fill3(earthsurf(:,1),earthsurf(:,2),zeros(5,1),[0.7 0.7 1]);
		%alpha(TheSurf,0.2);
		
		regionFilter = obj.Filterlist.getByName('Region');	
		TheRegion = getRegion(regionFilter);
		RegRange=TheRegion{1};
		
	
		
		if strcmp(obj.PlotMode,'full')
			Selected=obj.SendedEvents;
			UnSelected=~obj.SendedEvents;
			[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
			[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
			[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
			
			xmini=min([min(selectedLonLat(:,1)),min(unselectedLonLat(:,1))]);
			xmaxi=max([max(selectedLonLat(:,1)),max(unselectedLonLat(:,1))]);
			xlimiter=[xmini xmaxi];
			
			ymini=min([min(selectedLonLat(:,2)),min(unselectedLonLat(:,2))]);
			ymaxi=max([max(selectedLonLat(:,2)),max(unselectedLonLat(:,2))]);
			ylimiter=[ymini ymaxi];
			
			zmini=min([min(selectedDepth),min(unselectedDepth)]);
			zmaxi=max([max(selectedDepth),max(unselectedDepth)]);
			zlimiter=[zmini zmaxi];
			
		elseif strcmp(obj.PlotMode,'region')
			Selected=obj.SendedEvents;
			UnSelected=obj.Filterlist.excludeFilter('Region');
			
			[selectedLonLat,temp] = getLocations(obj.Datastore,Selected);
			[selectedDepth,temp] = getDepths(obj.Datastore,Selected);
			[selectedMag,temp] = getMagnitudes(obj.Datastore,Selected);
			[unselectedLonLat,temp] = getLocations(obj.Datastore,UnSelected);
			[unselectedDepth,temp] = getDepths(obj.Datastore,UnSelected);
			[unselectedMag,temp] = getMagnitudes(obj.Datastore,UnSelected);
			
			xmini=min([min(selectedLonLat(:,1)),min(unselectedLonLat(:,1))]);
			xmaxi=max([max(selectedLonLat(:,1)),max(unselectedLonLat(:,1))]);
			xlimiter=[xmini xmaxi];
			
			ymini=min([min(selectedLonLat(:,2)),min(unselectedLonLat(:,2))]);
			ymaxi=max([max(selectedLonLat(:,2)),max(unselectedLonLat(:,2))]);
			ylimiter=[ymini ymaxi];
			
			zmini=min([min(selectedDepth),min(unselectedDepth)]);
			zmaxi=max([max(selectedDepth),max(unselectedDepth)]);
			zlimiter=[zmini zmaxi];
			
			
		else
			Selected=obj.SendedEvents;
			UnSelected=obj.SendedEvents;
			[selectedLonLat,unselectedLonLat] = getLocations(obj.Datastore,obj.SendedEvents);
			[selectedDepth,unselectedDepth] = getDepths(obj.Datastore,obj.SendedEvents);
			[selectedMag,unselectedMag] = getMagnitudes(obj.Datastore,obj.SendedEvents);
			
			xlimiter=[min(selectedLonLat(:,1)) max(selectedLonLat(:,1))];
			ylimiter=[min(selectedLonLat(:,2)) max(selectedLonLat(:,2))];
			zlimiter=[min(selectedDepth) max(selectedDepth)];
		end
		
		if obj.LandToggle
			  
			plot3(plotAxis,mean(xlimiter),mean(ylimiter),0,'Color','w');
			hold on
			LandConf= struct( 	'PlotType','LandMass',...
					   	  'Data',obj.Datastore,...
					   	  'X_Axis_Label','Longitude ',...
					   	  'Y_Axis_Label','Latitude ',...
					   	  'X_Axis_Limit',[xlimiter(1)-10,xlimiter(2)+10],...
					   	  'Y_Axis_Limit',[ylimiter(1)-10,ylimiter(2)+10],...
					   	  'CutArea',true,...
					   	  'LineStylePreset','none',...
					   	  'Colors','landgreen',...
					   	  'SmallestMass',50,...
					   	  'Transparence',obj.AlphaMan,...
					   	  'Dimension','3D');
			LandHand=[];		   	  
					   	  
			try		   	  
				[LandHand LandEntry] = PlotLandMass(plotAxis,LandConf);
			catch
				%use larger area
				LandConf.SmallestMass=5;
				try
					[LandHand LandEntry] = PlotLandMass(plotAxis,LandConf);
				catch
					disp('No LandMasses drawn')
				end	
			end
			
			disp(size(LandHand))
			if ~isempty(LandHand)
				obj.LegendEntry{end+1}='Earth Surface';
				obj.PlotHandles(end+1)=LandHand;	
			end
			
			
			%disp('The land is ours')
			hold on
		end
		
		
		
		
		if strcmp(obj.PlotMode,'full')|strcmp(obj.PlotMode,'region')
			%both needed
			if any(UnSelected)
				UnEqConfig= struct(	'PlotType','Earthquakes',...
							'Data',obj.Datastore,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','selected',...
							'SelectedEvents',UnSelected,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit','auto',...
							'Y_Axis_Limit','auto',...
							'Z_Axis_Limit','auto',...
							'C_Axis_Limit','auto',...
							'DepthReverse',true,...
							'FastMode',FastMode);	
				%disp('now plotting');		
				[unhandle1 unentry1] = PlotEarthquake3D(plotAxis,UnEqConfig);
				
				if ~isempty(unhandle1)
					if iscell(unentry1)	
						obj.LegendEntry(end+1:end+numel(unentry1))=unentry1;
					else
						obj.LegendEntry{end+1}=unentry1;
					end
					
					obj.PlotHandles(end+1:end+numel(unhandle1))=unhandle1;	
				end
				
				hold on
			end
			
			
			
			if ~strcmp(RegRange,'all')
				%polygon
				PolyConf=struct(	'PlotType','Polygon',...
							'Data',regionFilter,...
							'Colors','magenta',...
							'LineStylePreset','Fatline',...
							'Plain',true);
				[polhandle polyentry] = PlotPolygon(plotAxis,PolyConf)	
				%if ~isempty(polhandle)
				%	obj.LegendEntry(end+1)='Selection';
				%	obj.PlotHandls(end+1)=polhandle;
				%end	
				
			end
			
			%selected in red
			EqConfig= struct(	'PlotType','Earthquakes',...
						'Data',obj.Datastore,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','selected',...
						'SelectedEvents',Selected,...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','red',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',FastMode);
			[handle1 entry1] = PlotEarthquake3D(plotAxis,EqConfig);
			
			if ~isempty(handle1)
				if iscell(entry1)	
					obj.LegendEntry(end+1:end+numel(entry1))=entry1;
				else
					obj.LegendEntry{end+1}=entry1;
				end
				
				obj.PlotHandles(end+1:end+numel(handle1))=handle1;	
			end
			
			hold off
			
		else
		
		
			EqConfig= struct(	'PlotType','Earthquakes',...
						'Data',obj.Datastore,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','selected',...
						'SelectedEvents',obj.SendedEvents,...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',FastMode);	
			disp('now plotting');		
			[handle1 entry1] = PlotEarthquake3D(plotAxis,EqConfig);
			
			if ~isempty(handle1)
				if iscell(entry1)	
					obj.LegendEntry(end+1:end+numel(entry1))=entry1;
				else
					obj.LegendEntry{end+1}=entry1;
				end
				
				obj.PlotHandles(end+1:end+numel(handle1))=handle1;	
			end
		
		end
		
		
		%obj.BorderToogle=false;
		if obj.BorderToogle
		  	  hold on	
			  BorderConf= struct(	'PlotType','Border',...
						  'Data',obj.Datastore,...
						  'X_Axis_Label','Longitude ',...
						  'Y_Axis_Label','Latitude ',...
						  'LineStylePreset','normal',...
						  'Colors','black');
						  [BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
						  
			if ~isempty(BorderHand)
				obj.LegendEntry{end+1}='Borders';
				obj.PlotHandles(end+1)=BorderHand;	
			end			  
			  
		end
			  
		  
		%obj.CoastToogle=false;	
		if obj.CoastToogle
			  hold on
			  CoastConf= struct( 	'PlotType','Coastline',...
					   	  'Data',obj.Datastore,...
					   	  'X_Axis_Label','Longitude ',...
					   	  'Y_Axis_Label','Latitude ',...
					   	  'LineStylePreset','Fatline',...
					   	  'Colors','blue');
			  [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
			  
			  if ~isempty(CoastHand)
				obj.LegendEntry{end+1}='Coastlines';
				obj.PlotHandles(end+1)=CoastHand;	
			  end	
			  
		end
		
		if obj.ShadowToogle
			hold on
			%prepare data
			maxDepth=max(selectedDepth)+1;
			NrEvents=numel(selectedDepth);
			NrNot=numel(unselectedDepth);
			
			
			
			%build "floor"
			FloorPan=[xlimiter(1),ylimiter(1),maxDepth;...
				  xlimiter(1),ylimiter(2),maxDepth;...
				  xlimiter(2),ylimiter(2),maxDepth;...
				  xlimiter(2),ylimiter(1),maxDepth];
			%fill3(FloorPan(:,1),FloorPan(:,2),FloorPan(:,3),[0.95 0.95 0.95],...
			%	'LineStyle','none');	  
			
			
			if strcmp(obj.PlotMode,'full')|strcmp(obj.PlotMode,'region')
				maxDepth=max([max(selectedDepth),max(unselectedDepth)])+1;
				seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
				unseldata=[unselectedLonLat(:,1),unselectedLonLat(:,2),ones(NrNot,1)*maxDepth,unselectedMag];
			
				if NrNot>0
					UnShadConfig= struct(	'PlotType','Earthquakes',...
							'Data',unseldata,...
							'PlotMode','old',...
							'PlotQuality',obj.PlotQuality,...
							'SelectionSwitch','unselected',...
							'SelectedEvents',logical(zeros(NrNot,1)),...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit','auto',...
							'Y_Axis_Limit','auto',...
							'Z_Axis_Limit','auto',...
							'C_Axis_Limit','auto',...
							'DepthReverse',true,...
							'FastMode',FastMode);
							
					[shadhandle1 shadentry1] = PlotEarthquake3D(plotAxis,UnShadConfig);
				end
				
				ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','red',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',FastMode);
						
				[shadhandle2 shadentry2] = PlotEarthquake3D(plotAxis,ShadConfig);
				
			else
				seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
							
				ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','old',...
						'PlotQuality',obj.PlotQuality,...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','none',...
						'MarkerStylePreset','none',...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',FastMode);
						
				[handle2 entry2] = PlotEarthquake3D(plotAxis,ShadConfig);
			end
		end	  
		
		  
		%set all axes right
		xlim(plotAxis,xlimiter);
		ylim(plotAxis,ylimiter);
		zlim(plotAxis,zlimiter);
				
		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(ylimiter)) obj.TheAspect]);
		set(plotAxis,'ZDir','reverse');
		box off
		set(plotAxis,'DrawMode','fast','XColor',[0.7 0.7 0.7],'YColor',[0.7 0.7 0.7],'ZColor',[0.7 0.7 0.7]);
		hold off
		
		legend(plotAxis,obj.PlotHandles,obj.LegendEntry);
		
		if ~obj.LegendToogle
			legend(plotAxis,'hide');
		end
		
		
		
	
	end
	
	
	function LegendSwitcher(obj)
		%simple legendToogle function
		
		if isempty(obj.LegendToogle)
			obj.LegendToogle=false;
		end
		
		%find menu
		Menu2Check = findobj(obj.PlotOptions,'Label','Show Legend');
		
		
		obj.LegendToogle=~obj.LegendToogle;
		
		if obj.LegendToogle
			set(Menu2Check, 'Checked', 'on');
			legend(obj.plotAxis,'show');
			
		else
			set(Menu2Check, 'Checked', 'off');
			legend(obj.plotAxis,'hide');
		end
		
		
	end
	
	
	
	function setSuperElevation(obj)
		promptValues = {'SuperElevation Factor'};
		dlgTitle = 'Set SuperElevation Factor';
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{obj.TheAspect},'UniformOutput',false));
		obj.TheAspect=	str2double(gParams{1});
				
				
		obj.updateGUI;
				
	
	
	end
	
	function setAlphaMan(obj)
		promptValues = {'Transparency Factor LandMass'};
		dlgTitle = 'Set Transparency of LandMass (set to 1 for none)';
		if strcmp(obj.AlphaMan,'none')
			Alpha=1;
		else
			Alpha=obj.AlphaMan;
		end
		
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{Alpha},'UniformOutput',false));
		AlphaMan=	str2double(gParams{1});
		
		if AlphaMan==1
			obj.AlphaMan='none';
		else
			obj.AlphaMan=AlphaMan;
		end
		
		
				
		obj.updateGUI;
				
	
	
	end
	
	
	function SetPlotMode(obj,WhichMode)
		%sets the plot mode in plot mode, 'check' can be used to set the ticks in the menu
	
		Menu2Check(3) = findobj(obj.PlotOptions,'Label','Plot all Earthquakes');
		Menu2Check(2) = findobj(obj.PlotOptions,'Label','Main Window mode');
		Menu2Check(1) = findobj(obj.PlotOptions,'Label','Only Selected Earthquakes');
			
		%uncheck all menupoints
		set(Menu2Check(1), 'Checked', 'off');
		set(Menu2Check(2), 'Checked', 'off');
		set(Menu2Check(3), 'Checked', 'off');
		
		
		switch WhichMode
			case 'check'
				
				switch obj.PlotMode
					case 'full'
						sel=3;
					case 'region'
						sel=2;	
					case 'light'
						sel=1;
				end		
				set(Menu2Check(sel), 'Checked', 'on');
				
			case 'light'
				obj.PlotMode='light';
				set(Menu2Check(1), 'Checked', 'on');
				obj.updateGUI;
				
			case 'region'
				obj.PlotMode='region';
				set(Menu2Check(2), 'Checked', 'on');
				obj.updateGUI;
			
			case 'full'
				obj.PlotMode='full';
				set(Menu2Check(3), 'Checked', 'on');
				obj.updateGUI;
			
		
		end
		
	
	end
	
	
	
	function Rebuild(obj)
		
		obj.DataSwitcher;
		obj.BuildResultGUI;
		obj.updateGUI;
		
		
	
	end
	
	function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
		 	if ~strcmp(qual,'chkData')
				Menu2Check(3) = findobj(obj.PlotOptions,'Label','Zmap style');
				Menu2Check(2) = findobj(obj.PlotOptions,'Label','Medium Quality Plot');
				Menu2Check(1) = findobj(obj.PlotOptions,'Label','High Quality Plot');
				
				%uncheck all menupoints
				set(Menu2Check(1), 'Checked', 'off');
				set(Menu2Check(2), 'Checked', 'off');
				set(Menu2Check(3), 'Checked', 'off');
			end
			
 			switch qual
 				case 'low'
 					%dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotQuality='low';
 					obj.updateGUI;
                		
				case 'med'
					%dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotQuality='med';
					obj.updateGUI;
					
				case 'hi'
					%dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotQuality='hi';
					obj.updateGUI;
					
				case 'chk'
					%try StoredQuality = dataStore.getUserData('PlotQuality');
					%catch dataStore.setUserData('PlotQuality','med');
					%StoredQuality = dataStore.getUserData('PlotQuality');		
					%end
					StoredQuality = obj.PlotQuality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
				case 'chkData'
					try  
						obj.PlotQuality = obj.Datastore.getUserData('PlotQuality');
					catch 
						
						obj.PlotQuality = 'med';		
					end

			end
	end
	
	function TooglePlotOptions(obj,WhichOne)
		%This functions sets the toogles in this modul
		%WhichOne can be the following strings:
		%	'Border':	Borderlines
		%	'Coast'	:	Coastlines
		%	'-chk'	:	This will set all checks in the menu
		%			to the in toggles set state.
		
		CoastMenu = findobj(obj.PlotOptions,'Label','Draw Coastlines');
		BorderMenu = findobj(obj.PlotOptions,'Label','Draw Country Borders');
		ShadowMenu = findobj(obj.PlotOptions,'Label','Draw Shadows');
		LandMenu = findobj(obj.PlotOptions,'Label','Draw LandMasses');
	
		
		switch WhichOne
			case '-chk'
				if obj.CoastToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				if obj.BorderToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				if obj.ShadowToogle
					set(ShadowMenu, 'Checked', 'on');
				else
					set(ShadowMenu, 'Checked', 'off');
				end
				
				if obj.LandToggle
					set(LandMenu, 'Checked', 'on');
				else
					set(LandMenu, 'Checked', 'off');
				end
				
				
			case 'Border'
				
				obj.BorderToogle=~obj.BorderToogle;
				
				if obj.BorderToogle
					set(BorderMenu, 'Checked', 'on');
				else
					set(BorderMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'Coast'
				
				obj.CoastToogle=~obj.CoastToogle;
				
				if obj.CoastToogle
					set(CoastMenu, 'Checked', 'on');
				else
					set(CoastMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
			
			case 'Shadow'
				
				obj.ShadowToogle=~obj.ShadowToogle;
				
				if obj.ShadowToogle
					set(ShadowMenu, 'Checked', 'on');
				else
					set(ShadowMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;
				
			case 'Land'
				
				obj.LandToggle=~obj.LandToggle;
				
				if obj.LandToggle
					set(LandMenu, 'Checked', 'on');
				else
					set(LandMenu, 'Checked', 'off');
				end
				
				obj.updateGUI;	
				
		end
		
		
	end
	
	
	
	end
end


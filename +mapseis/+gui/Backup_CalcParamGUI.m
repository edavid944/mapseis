classdef CalcParamGUI < handle
 % This GUI allows to custom build a GUI for a certain Calculation
 
 
properties
	TheGUI
	Wrapper
	Commander
	FigureName
	ListProxy
	CalcParams
	WinCol
	CurrentCalcSlide
	windowwide
	xPos
	yPos
	ModulList
	PopUpWindow
	RegParameter
	Version
end

events
	Calculate	
end


methods
	function obj = CalcParamGUI(Wrapper,FigureName,WindowPos,ListProxy,RegParameter)
		%Constructor
		
		obj.Version='1.0';
		
		obj.Wrapper = Wrapper;
		obj.FigureName=FigureName;
		obj.ListProxy = ListProxy; 
		obj.RegParameter=RegParameter;
		obj.Commander=obj.Wrapper.Commander;
				
		obj.windowwide = 320;
		obj.xPos=WindowPos(1);
		obj.yPos=WindowPos(2);
		
		%Set default CurrentCalcSlide to 1
		obj.CurrentCalcSlide=1;
		
		%Register yourself in the wrapper, this should also add 
		%CalcParams to this modul
		obj.Wrapper.RegisterGUI(obj,'ParamGUI',RegParameter);

		%Create GUI		
	
		
		%Go through every modul and create them 
		obj.Widowmaker;
		
		
		
		
				
	end
	
	
	function Widowmaker(obj)
		%creates the actuall GUI for ONE Calculation, needs to be called again if another Calculation is shown.
		
		%not needed anymore as only the GUI for ONE calculation is saved
		%entrynumber=obj.CurrentCalcSlide;
		%Calcslide = obj.CalcParams{entrynumber};
		
		%get current calcslide form the wrapper
		obj.CalcParams=ParamConfigSender(obj.Wrapper,obj.CurrentCalcSlide);
		Calcslide = obj.CalcParams;
		
		%init the run
		windowlength = numel(Calcslide)*40;  % Every modul is 40 pixels height
		currentposition = 0;
		%obj.ModulList = zeros(numel(Calcslide),1);
		%use cell array because of more than one object per slide
		obj.ModulList = cell(numel(Calcslide),1);
		
		%clear window and set position
		
		if ~isempty(obj.TheGUI)
			close(obj.TheGUI)
			%clf(obj.TheGUI);
			obj.TheGUI=[];
		end
		
		%Use Color if wanted
		try 
			GUIColor=obj.Wrapper.getCalcColor(obj.CurrentCalcSlide);
			if isstr(GUIColor)
				switch GUIColor
					case 'blue'
						obj.WinCol=[0.7 0.7 1];
					case 'red' 
						obj.WinCol=[1 0.7 0.7];
					case 'green'
						obj.WinCol=[0.7 1 0.7];
					case 'black' 
						obj.WinCol=[0.9 0.9 0.9];
					case 'cyan'
						obj.WinCol=[0.7 1 1];
					case 'magenta'
						obj.WinCol=[1 0.7 1];
				end
			else
				obj.WinCol=GUIColor;
			end
			
		catch
			obj.WinCol=[0.8000    0.8000    0.8000];
		end		
		
		
		obj.TheGUI=figure('name',obj.FigureName,'visible','off','Toolbar','none',...
				'MenuBar','none','NumberTitle','off','Color',obj.WinCol);
		
		set(obj.TheGUI,'Position', [obj.xPos obj.yPos obj.windowwide windowlength]);
		
		currentposition=numel(Calcslide);
		
		for i=1:numel(Calcslide)
			entry=Calcslide{i};
			
			switch entry.ModulType
				case 'CalcButton'
					obj.CreateCalcButton(Calcslide{i},currentposition);
					
				case 'DataSelector'
					obj.CreateDataSelector(Calcslide{i},currentposition);
					
				case 'FilterSelector'
					obj.CreateFilterSelector(Calcslide{i},currentposition);
				
				case 'CalcSelector'
					obj.CreateCalcSelector(Calcslide{i},currentposition);
					
				case 'CalcModify'
					obj.CreateCalcModify(Calcslide{i},currentposition);
					
				case 'PopUp'
					obj.CreatePopUp(Calcslide{i},currentposition);
					
				case 'ListBox'
					obj.CreateListBox(Calcslide{i},currentposition);
					
				case 'Button'
					obj.CreateButton(Calcslide{i},currentposition);
					
				%case 'Button_Small'
				%	obj.CreateButton(Calcslide{i},currentposition,'small');
					
				case 'Texter'
					obj.CreateTexter(Calcslide{i},currentposition);
					
				case 'Checkerbox'
					obj.CreateCheckerbox(Calcslide{i},currentposition);
				
				case 'Entryfield'
					obj.CreateEntryfield(Calcslide{i},currentposition);
				
				case 'Positionfield'
					obj.CreatePositionfield(Calcslide{i},currentposition);
					
				case 'Empty'
					%just do nothing
			end
			
			%shift down
		 	currentposition=currentposition-1;
		 end
		 
		 % Make the GUI visible
		 set(obj.TheGUI,'Visible','on');	

	end
	
	
	
	function CreateCalcButton(obj,DataSlide,currentposition);
		%create a button which starts the calculation
		
		Name='Calculate';
		
		if isfield(DataSlide,'Name')
			if ~isempty(DataSlide.Name)
				Name=DataSlide.Name;
			end	
		end
		
		switch DataSlide.how
		 	case 'fixed'
				obj.ModulList{currentposition} = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
               				'Position',[10 (currentposition-1)*40+3 300 34],'Style','pushbutton',...
               				'String',Name,'Tag','CalcButton',...
               				'Callback',@(s,e) obj.goCalc('fixed',DataSlide.value));
		
			case 'variable'
				obj.ModulList{currentposition} = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
            	    		  'Position',[10 (currentposition-1)*40+3 300 34],'Style','pushbutton',...
            	    		  'String',Name,'Tag','CalcButton',...
            	    		  'Callback',@(s,e) obj.goCalc('variable',[]));
			
			case 'screamer'
				obj.ModulList{currentposition} = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
            	    		  'Position',[10 (currentposition-1)*40+3 300 34],'Style','pushbutton',...
            	    		  'String',Name,'Tag','CalcButton',...
            	    		  'Callback',@(s,e) obj.goCalc('screamer',[]));
			
		end
		
	end
	
	
					
	function CreateDataSelector(obj,DataSlide,currentposition);
		
		%check if callback is empty, not existing or not a handle
		CallSwitch='none';
		try 
			if ~isempty(DataSlide.Callback) & isa(DataSlide.Callback,'function_handle');
				CallSwitch='Callback';
			elseif strcmp(DataSlide.Callback,'Change')
				CallSwitch='Change';
			end
		end
		
		%Check if the parameter is meant for a specific calculation
		CalcEntry=[];
		if isfield(DataSlide,'CalcEntry')
			if ~isempty(DataSlide.CalcEntry)
				CalcEntry=DataSlide.CalcEntry;
			end
		end		
		
		obj.ModulList{currentposition} = uicontrol(...
										'Parent',obj.TheGUI,...
										'Units','Pixels',...
										'Position',[10 (currentposition-1)*40+3 300 34],...
										'String','DataStore',...
										'Style','popupmenu',...
										'Tag','DataStore');
											
		
		%set callback if needed
		switch CallSwitch
			case 'none'
				%do nothing
			case 'Callback'
				set(obj.ModulList{currentposition},'Callback',DataSlide.Callback);
			case 'Change'
				set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
		end
		
										
		%get the list from the commander
		datastorelist = obj.Wrapper.Commander.CurrentDataList;
		set(obj.ModulList{currentposition}, 'String' , datastorelist(:,2));
		
		
		%set position
		if ~isfield(DataSlide,'Value')
			DataSlide.Value=1;
		end		
		
		if isempty(DataSlide.Value)
			DataSlide.Value=1;
		end
		set(obj.ModulList{currentposition},'Value',DataSlide.Value);
		
		%One has to be selected
		set(obj.ModulList{currentposition}, 'min', 1);
		
		
		%try delete and set listener (just to be cautious)
		obj.ListProxy.quiet(obj.Commander,'HashUpdate',...
                    @(s,e) obj.DataListChanged()); 
                obj.ListProxy.listen(obj.Commander,'HashUpdate',...
                    @(s,e) obj.DataListChanged()); 

		
	end				
	
	
	
		
	function CreateFilterSelector(obj,DataSlide,currentposition);
		
		%check if callback is empty, not existing or not a handle
		CallSwitch='none';
		try 
			if ~isempty(DataSlide.Callback) & isa(DataSlide.Callback,'function_handle');
				CallSwitch='Callback';
			elseif strcmp(DataSlide.Callback,'Change')
				CallSwitch='Change';
			end
		end
		
		%Check if the parameter is meant for a specific calculation
		CalcEntry=[];
		if isfield(DataSlide,'CalcEntry')
			if ~isempty(DataSlide.CalcEntry)
				CalcEntry=DataSlide.CalcEntry;
			end
		end		
		
		
		obj.ModulList{currentposition} = uicontrol(...
										'Parent',obj.TheGUI,...
										'Units','Pixels',...
										'Position',[10 (currentposition-1)*40+3 300 34],...
										'String','FilterList',...
										'Style','popupmenu',...
										'Tag','FilterList');
		
		%set callback if needed								
		switch CallSwitch
			case 'none'
				%do nothing
			case 'Callback'
				set(obj.ModulList{currentposition},'Callback',DataSlide.Callback);
			case 'Change'
				set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
		end
		
		
		%get the list from the commander
		filterlist = obj.Wrapper.Commander.CurrentFilterList;
		set(obj.ModulList{currentposition}, 'String' , filterlist(:,2));
		
		
		%set position
		if ~isfield(DataSlide,'Value')
			DataSlide.Value=1;
		end		
		
		if isempty(DataSlide.Value)
			DataSlide.Value=1;
		end
		set(obj.ModulList{currentposition},'Value',DataSlide.Value);
		
		%One has to be selected
		set(obj.ModulList{currentposition}, 'min', 1);
		
		%try delete and set listener (just to be cautious)
		obj.ListProxy.quiet(obj.Commander,'HashUpdate',...
                    @(s,e) obj.FilterListChanged()); 
                obj.ListProxy.listen(obj.Commander,'HashUpdate',...
                    @(s,e) obj.FilterListChanged()); 


	
	
	end
	
	
	
	
	function CreateCalcSelector(obj,DataSlide,currentposition);
	    %finished: who is saving the list with calculation entries? -> wrapper
		obj.ModulList{currentposition} = uicontrol(...
										'Parent',obj.TheGUI,...
										'Units','Pixels',...
										'Callback',@(s,e) obj.CalcChange(),...
										'Position',[10 (currentposition-1)*40+3 300 34],...
										'String','CalcSelector',...
										'Style','popupmenu',...
										'Tag','CalcSelector');
	
		%get the list from the wrapper
		calclist = obj.Wrapper.CalcKeyList;
		set(obj.ModulList{currentposition}, 'String' , calclist(:,2));
		
		%set the selection to the current position
		set(obj.ModulList{currentposition}, 'Value', obj.CurrentCalcSlide);
		
		%One has to be selected
		set(obj.ModulList{currentposition}, 'min', 1);
		
		%try delete and set listener (just to be cautious)
		obj.ListProxy.quiet(obj.Wrapper,'CalcHashUpdated',...
                    @(s,e) obj.CalcListChanged()); 
                obj.ListProxy.listen(obj.Wrapper,'CalcHashUpdated',...
                    @(s,e) obj.CalcListChanged()); 
		


	
	end
	


	
	function CreateCalcModify(obj,DataSlide,currentposition);
		%Althought the Modifyer for the Calculation is optional, it might be a good idea to use it.
		
		newbutton   = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
                		  'Position',[10 (currentposition-1)*40+3 140 34],'Style','pushbutton','String','New',...
              			  'Tag','NewCalcButton','Callback',@(s,e) obj.NewCalc(DataSlide.PopUpMenu,DataSlide.Option));
		deletebutton   = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
                		  'Position',[170 (currentposition-1)*40+3 140 34],'Style','pushbutton','String','Delete',...
              			  'Tag','DeleteCalcButton','Callback',@(s,e) obj.DeleteCalc());
		
		%write the button into the ModulList as an array
		obj.ModulList{currentposition} = [newbutton, deletebutton];
	end
	
	
	
	
	function CreatePopUp(obj,DataSlide,currentposition);
		%needed input for the PopUp:
		%Name
		%Tag
		%Callback (optional)
		%String with entries
		
		%check if callback is empty, not existing or not a handle
		CallSwitch='none'
		try 
			if ~isempty(DataSlide.Callback) & isa(DataSlide.Callback,'function_handle');
				CallSwitch='Callback';
			elseif strcmp(DataSlide.Callback,'Change')
				CallSwitch='Change';
			end
		end
		
		%Check if the parameter is meant for a specific calculation
		CalcEntry=[];
		if isfield(DataSlide,'CalcEntry')
			if ~isempty(DataSlide.CalcEntry)
				CalcEntry=DataSlide.CalcEntry;
			end
		end		
		
		
		obj.ModulList{currentposition} = uicontrol(...
										'Parent',obj.TheGUI,...
										'Units','Pixels',...
										'Position',[10 (currentposition-1)*40+3 300 34],...
										'String',DataSlide.Name,...
										'Style','popupmenu',...
										'Tag',DataSlide.Tag);
		
										
		%set callback if needed								
		switch CallSwitch
			case 'none'
				%do nothing
			case 'Callback'
				set(obj.ModulList{currentposition},'Callback',DataSlide.Callback);
			case 'Change'
				set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
		end
										
		set(obj.ModulList{currentposition}, 'String' , DataSlide.EntryStrings);
		
		
		%set position
		if ~isfield(DataSlide,'Value')
			DataSlide.Value=1;
		end		
		
		if isempty(DataSlide.Value)
			DataSlide.Value=1;
		end
		set(obj.ModulList{currentposition},'Value',DataSlide.Value);
		
		%One has to be selected
		set(obj.ModulList{currentposition}, 'min', 1);
		
	
	end
	
	
	
	
	function CreateListBox(obj,DataSlide,currentposition);
		%needed input for the PopUp:
		%Name
		%Tag
		%How Many Selection allowed
		%Callback (optional)
		%String with entries
		%Value: sets the position of the selector
		
		%check if callback is empty, not existing or not a handle
		CallSwitch='none'
		try 
			if ~isempty(DataSlide.Callback) & isa(DataSlide.Callback,'function_handle');
				CallSwitch='Callback';
			elseif strcmp(DataSlide.Callback,'Change')
				CallSwitch='Change';
			end
		end
		
		%Check if the parameter is meant for a specific calculation
		CalcEntry=[];
		if isfield(DataSlide,'CalcEntry')
			if ~isempty(DataSlide.CalcEntry)
				CalcEntry=DataSlide.CalcEntry;
			end
		end		
		
		
		
		obj.ModulList{currentposition} = uicontrol(...
										'Parent',obj.TheGUI,...
										'Units','Pixels',...
										'Position',[10 (currentposition-1)*40+3 300 34],...
										'String',DataSlide.Name,...
										'Style','listbox',...
										'Tag',DataSlide.Tag);
		
		%set callback if needed								
		switch CallSwitch
			case 'none'
				%do nothing
			case 'Callback'
				set(obj.ModulList{currentposition},'Callback',DataSlide.Callback);
			case 'Change'
				set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
		end								
										
										
		set(obj.ModulList{currentposition}, 'String' , DataSlide.EntryStrings);
		
		% Allow multiple selections
		set(obj.ModulList{currentposition}, 'min', 0, 'max', DataSlide.maxSelection);	
		
		%set position
		if ~isfield(DataSlide,'Value')
			DataSlide.Value=1;
		end		
		
		if isempty(DataSlide.Value)
			DataSlide.Value=1;
		end
		set(obj.ModulList{currentposition},'Value',DataSlide.Value);
	
	end

	
	
	
	function CreateButton(obj,DataSlide,currentposition);
		%just make the buttons so big the still fit 
		
		%it is possibe having no callback is possible but not suggested. 
		%Keep also in mind that there will be no value for a button,
		%so the function called in the wrapper can not read a value 
		%from the button. But it is possible to use the button as 
		%a toggle switch 
		%For each button a callback definition is needed, but they can
		%be different from each other
		
		buttonwidth = (300 - (DataSlide.ButtonNumber-1)*2)/DataSlide.ButtonNumber;
		xpos=10;
		Buttons=[];
		
		for i=1:DataSlide.ButtonNumber
			
			%check if callback is empty, not existing or not a handle
			CallSwitch='none'
			try 
				if ~isempty(DataSlide.Callback{i}) & isa(DataSlide.Callback{i},'function_handle');
					CallSwitch='Callback';
				elseif strcmp(DataSlide.Callback{i},'Change')
					CallSwitch='Change';
				end
			end
			
			
			%Check if the parameter is meant for a specific calculation
			CalcEntry=[];
			if isfield(DataSlide,'CalcEntry')
				if ~isempty(DataSlide.CalcEntry)
					CalcEntry=DataSlide.CalcEntry;
				end
			end		
			
			
			%create button
			newbutton   = uicontrol('Parent',obj.TheGUI,...
							'Units', 'Pixels',...
                	  				'Position',[xpos (currentposition-1)*40+3 buttonwidth 34],...
                	  				'String',DataSlide.Name{i},...
							'Style','pushbutton',...
              		  				'Tag',DataSlide.Tag{i});
			
              		  		
              		%set callback if needed								
			switch CallSwitch
				case 'none'
					%do nothing
				case 'Callback'
					set(obj.ModulList{currentposition},'Callback',DataSlide{i}.Callback);
				case 'Change'
					set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
			end		 				
              		
			%shift
			
			xpos=xpos+buttonwidth+2;
			Buttons=[Buttons,newbutton];
				
		end		
		obj.ModulList{currentposition} = Buttons;
		
	
	end
	
	
	
	
	function CreateTexter(obj,DataSlide,currentposition);
		disp(currentposition)
		obj.ModulList{currentposition} = uicontrol(...
									'Parent',obj.TheGUI,...
									'Units','Pixels',...
									'Position',[10 (currentposition-1)*40+3 300 34],...
									'String',DataSlide.String,...
									'Tag',DataSlide.Tag,...
									'BackgroundColor',obj.WinCol,...
									'Style','text');
		
									
		

	end
	
	
	
	
	function CreateCheckerbox(obj,DataSlide,currentposition);
		%just make the buttons so big the still fit 
		
		checkwidth = (300 - (DataSlide.ButtonNumber-1)*2)/DataSlide.ButtonNumber;
		xpos=10;
		Checkers=[];
		
		for i=1:DataSlide.CheckerNumber
			
		%check if callback is empty, not existing or not a handle
			CallSwitch='none'
			try 
				if ~isempty(DataSlide.Callback{i}) & isa(DataSlide.Callback{i},'function_handle');
					CallSwitch='Callback';
				elseif strcmp(DataSlide.Callback{i},'Change')
					CallSwitch='Change';
				end
			end
			
			
			%Check if the parameter is meant for a specific calculation
			CalcEntry=[];
			if isfield(DataSlide,'CalcEntry')
				if ~isempty(DataSlide.CalcEntry)
					CalcEntry=DataSlide.CalcEntry;
				end
			end		
			
			
			%create checkbox
			newCheck   = uicontrol('Parent',obj.TheGUI,...
							'Units', 'Pixels',...
                					'Position',[xpos (currentposition-1)*40+3 buttonwidth 34],...
                					'String',DataSlide.Name{i},...
                	 				'Style','checkbox',...
              		  				'Tag',DataSlide.Tag{i});
			
              		  		
              		%set callback if needed								
			switch CallSwitch
				case 'none'
					%do nothing
				case 'Callback'
					set(obj.ModulList{currentposition},'Callback',DataSlide{i}.Callback);
				case 'Change'
					set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
			end	
              		  		
			
              		%shift
			xpos=xpos+buttonwidth+2;
			Checkers=[Checkers,newCheck];
				
		end		
		obj.ModulList{currentposition} = Checkers;
		
	
	end
	
	
	
	
	
	function CreateEntryfield(obj,DataSlide,currentposition);

		%check if callback is empty, not existing or not a handle
		CallSwitch='none';
		try 
			if ~isempty(DataSlide.Callback) & isa(DataSlide.Callback,'function_handle');
				CallSwitch='Callback';
			elseif strcmp(DataSlide.Callback,'Change')
				CallSwitch='Change';
			end
		end
				
		
		%Check if the parameter is meant for a specific calculation
		CalcEntry=[];
		if isfield(DataSlide,'CalcEntry')
			if ~isempty(DataSlide.CalcEntry)
				CalcEntry=DataSlide.CalcEntry;
			end
		end		
		
		
		obj.ModulList{currentposition} = uicontrol(...
								'Parent',obj.TheGUI,...
								'Units','Pixels',...
								'Position',[10 (currentposition-1)*40+3 300 34],...
								'String',DataSlide.String,...
								'Tag',DataSlide.Tag,...
								'BackgroundColor',obj.WinCol,...
								'Style','edit');
		switch CallSwitch
			case 'none'
				%do nothing
			case 'Callback'
				set(obj.ModulList{currentposition},'Callback',DataSlide.Callback);
			case 'Change'
				set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
		end				
								
	end
	
	
	
	function CreatePositionfield(obj,DataSlide,currentposition);
		%This function creates also an entryfield but the field is linked with draggable Point in the ResultGUI
				
		obj.ModulList{currentposition} = uicontrol(...
								'Parent',obj.TheGUI,...
								'Units','Pixels',...
								'Position',[10 (currentposition-1)*40+3 300 34],...
								'String',DataSlide.String,...
								'Tag',DataSlide.Tag,...
								'BackgroundColor',obj.WinCol,...
								'Style','edit');

		set(obj.ModulList{currentposition},'Callback',@(s,e) obj.Wrapper.ChangeParameterPoint(...
				'ParamGUI',s,DataSlide.WindowTag,DataSlide.WhichCoord,DataSlide.PointTag,...
				DataSlide.CalculationKey,DataSlide.Parameter));
				
								
	end
	
	
	
	
	
	
	
	
	function goCalc(obj,how,val)
		%The Calculation function in the wrapper needs to be standardized in Name and input values
		%Listener Needed for the update management
		
		switch how
			case 'fixed'
				%this allows to pass any value to the calculation function, usefull for speciallized Calcbuttons
				obj.Wrapper.Calculation(val);	
			case 'variable'
				%Sends the position of the CalcSelector to the wrapper, if existing, else it just sends the default value 1
				
				obj.Wrapper.Calculation(obj.CurrentCalcSlide);
			
			case 'screamer'
				%if just a call is needed
				notify(obj,'Calculate');
			
					
		end	
	end
	
	
	
	
	function CalcChange(obj)
		%switching the calculation (->rebuild parameter window)
		HandleList=obj.TagGetter;
		
		%get position of the CalcSelector
		posit = strcmp(get(HandleList,'Tag'),'CalcSelector');
		val=get(HandleList(posit), 'Value');
		
		if val==0
			val=obj.CurrentCalcSlide
			set(HandleList(posit), 'Value',obj.CurrentCalcSlide);
		end
		
		%set the new currentCalcSlide
		obj.CurrentCalcSlide = val;
		
		%rebuild gui
		obj.Widowmaker;
		
	end
	
	
	
	
	function DataListChanged(obj)
		HandleList=obj.TagGetter;
		disp('DataChangeeeeeeeeee')
		
		datastorelist = obj.Wrapper.Commander.CurrentDataList;
		posit = strcmp(get(HandleList,'Tag'),'DataStore')
		set(HandleList(posit), 'String' , datastorelist(:,2));
	
	end
	
	
	
		
	function FilterListChanged(obj)
		HandleList=obj.TagGetter;
		filterlist = obj.Wrapper.Commander.CurrentFilterList;
		posit = strcmp(get(HandleList,'Tag'),'FilterList')
		set(HandleList(posit), 'String' , filterlist(:,2));
		
	end
	
	
	
	
	function CalcListChanged(obj)
		HandleList=obj.TagGetter;
		calclist = obj.Wrapper.CalcKeyList;
		new_Nr = numel(calclist(:,2));
		
		%Refresh the CalcParams Not needed anymore, tb deleted
		%obj.Wrapper.RefreshGUI('ParamGUI',obj.RegParameter);

		
		%get the CalcSelector 
		posit = strcmp(get(HandleList,'Tag'),'CalcSelector')
		
		%check if entries are added or delete
		old_list=get(HandleList(posit), 'String');
		
		if isstr(old_list) %->only one entry
			old_Nr=1;
		else
			old_Nr=numel(old_list);
		end
		
		%the different case		
		if (old_Nr>new_Nr) & obj.CurrentCalcSlide>new_Nr
			obj.CurrentCalcSlide=obj.CurrentCalcSlide-1;
		elseif (old_Nr<new_Nr)
			obj.CurrentCalcSlide=new_Nr; %set to last (just added) element
		else
			%no change needed
		end
		
		%not needed anymore, rebuild resets the List anyway					
		%set(HandleList(posit), 'String' , calclist(:,2));
		
		%rebuild window
		obj.Widowmaker;
		
		
	end




	function TheHandleList = TagGetter(obj)
		%because a cell array can not be searched like a normal array this function is needed.
		
		%get all handles in one array
		count=1;
		for i=1:numel(obj.ModulList)
			nums=numel(obj.ModulList{i});

				for j=1:nums
					tempModuls(count) = obj.ModulList{i}(j);
					count=count+1;
				end

		end
		TheHandleList = tempModuls;
		
	end
	
	
	
	
	function NewCalc(obj,PopUpToggle,Option)
		% This creates a new function by calling the function addCalculation in the wrapper
		% if PopUpToggle is true, a window with the different type of possible Calculation is opended speciefied 
		% with the Option entry of the Modulentry.
		% If PopUpToggle is false, the options are send directly to the wrapper.
		
		if PopUpToggle
			
			Calcslide = obj.CalcParams;
			windowlength = numel(Calcslide)*40;
			
			try
				close(obj.PopUpWindow);
			end
			
			obj.PopUpWindow=figure;
			set(obj.PopUpWindow,'Position', [obj.xPos+20 obj.yPos+windowlength/2 ...
				 obj.windowwide-40 120],'Toolbar','none','MenuBar','none','NumberTitle','off');
			
			%the list with the different calculatuin types
			PossibleCalcList = uicontrol(...
										'Parent',obj.PopUpWindow,...
										'Units','Pixels',...
										'Position',[10 60 obj.windowwide-60 34],...
										'String','CalcType',...
										'Style','popupmenu',...
										'Tag','CalcType');
										
										
	
			%set the list
			set(PossibleCalcList, 'String' , Option(:,1));

			
			%the buttons
			buttonwidth = (obj.windowwide-60)/2-6;
			cancelbutton   =  uicontrol('Parent',obj.PopUpWindow,...
								'Units', 'Pixels',...
                		  			   	'Position',[10 10 buttonwidth 34],...
                		  			   	'String','Cancel',...
                		  			   	'Style','pushbutton',...
              			  				'Tag','CancelAddCalc',...
              			  				'Callback',@(s,e) obj.NewCalcButton('cancel',Option));

			cancelbutton   =  uicontrol('Parent',obj.PopUpWindow,...
								'Units', 'Pixels',...
                		  			   	'Position',[13+buttonwidth 10 buttonwidth 34],...
                		  			   	'String','Ok',...
                		  			   	'Style','pushbutton',...
              			  				'Tag','OkAddCalc',...
              			  				'Callback',@(s,e) obj.NewCalcButton('ok',Option));


			%The whole rest does the button function
		
			
		else
			obj.Wrapper.addCalculation(Option);
			
						
		end
		
		
		
	end
	
	
	
	function NewCalcButton(obj,whichone,Option)
		%this function is used for the 'cancel' and 'ok' button in the popupwindow 
		
		switch whichone
			case 'cancel'
				%Do nothing just close the window
				close(obj.PopUpWindow);
				try
					clear(obj.PopUpWindow);
				end	
				
			case 'ok'
				%get the wanted calcType, send it and close the window
				childelements = get(obj.PopUpWindow,'children');
				CalcTypePop = findobj(childelements,'Tag','CalcType');
				CalcType = get(CalcTypePop,'Value');
				
				%send to the Wrapper
				obj.Wrapper.addGUICalc(Option{CalcType,2}); 
				
				%close
				close(obj.PopUpWindow);
				try
					clear(obj.PopUpWindow);
				end	
		end			
		
		
	end
	
	
	
	
	function DeleteCalc(obj)
		%Deletes the currently selected calculation 
		HandleList = obj.TagGetter;
		%get position of the CalcSelector
		posit = strcmp(get(HandleList,'Tag'),'CalcSelector');
		val=get(HandleList(posit), 'Value');
		
		%send value to the wrapper, the rest is done by the wrapper
		obj.Wrapper.deleteCalculation(val);
		


	
	end
	
	
	function [CurrentEntry CalcKey] = EntryValues(obj)
		%This function will get every value from the current Calcslide
		%and save the result in a structure with fields named after the
		%tags
		%The function is meant to be called from the outside (Wrapper)
		%All Values are written into the structure, it does not matter
		%if the values are not needed by the wrapper
		
		%the calckey (mostly not needed, as the GoCalc function sends 
		%CurrentCalcSlide number anyway, but it might needed in a other
		%CalcFunction.
		CalcKey = obj.CurrentCalcSlide;
		
		%now get the data
		leModuls=obj.ModulList;
		
		for i=1:numel(LeModuls)
			
			CurMod=LeModuls{i}
			
			%for button/checkbox fields 
			for j=1:numel(CurMod)
				
				%The Tag
				enTag=get(CurMod(j),'Tag');
				
				%The Style
				enStyle=get(CurMod(j),'Style');
				
				%The Value
				if strcmp(enStyle,'edit') & strcmp(enStyle,'text')
					enValue=get(CurMod(j),'String');
				else	
					enValue=get(CurMod(j),'Value');
				end
				
				%write to the structure
				CurrentEntry.(enTag)=enValue;
				
				
				
			end
			
		end
	
	end
	
	
	function ChangeSender(obj,objHandle,CalcEntry)
		%This function is a generic function for the different uimoduls
		%It does call a function ParameterChanger in the Wrapper and send
		%it the handle of the uiobject. 
		%Added support for parameters from a different Calculation than 
		%the current one.
		
		
		if isempty(CalcEntry)
			CalcEntry=obj.CurrentCalcSlide;
		end
		
		% very simple, but can be improved in future releases
		obj.Wrapper.ParameterChanger(objHandle,CalcEntry);
		
		
	
	end
	
	
	
	function PreSuccessSelector(obj,Types,Texts,AddOptions)
		%A special function, it is a GUI but meant to be called by the
		%wrapper. If a Calculation is added, sometimes the result of 
		%another calculation is needed, this gui allows to select an
		%existing calculation of a certain type.
		
		%Get the calcKeyList
		Keylist=obj.Wrapper.CalcKeyList;
		
		%convert to cell if needed
		if isstr(Types)
			Types={Types};
			Texts={Texts};
		end
		
		Calcslide = obj.CalcParams;
		windowlength = numel(Types)*80+60;
		el_num=numel(Types);
		
		
		%the PopUpWindow variable is used, it should not be a problem
		%but could be changed later if there is a problem.
		clf(obj.PopUpWindow);
		set(obj.PopUpWindow,'Position', [obj.xPos+20 obj.yPos+windowlenght/2 ...
				obj.windowwide-40 120],'Toolbar','none','MenuBar','none','NumberTitle','off');
		
		for i=1:el_num
			%the list with the different Calculations of one type
			
			TheText = uicontrol('Parent',obj.PopUpWindow,...
										'Units','Pixels',...
										'Position',[10 112+(i-1)*42 obj.windowwide-40 34],...
										'String',Texts{i},...
										'Style','text');
			
			
			PossibleCalcList = uicontrol(...
										'Parent',obj.PopUpWindow,...
										'Units','Pixels',...
										'Position',[10 70+(i-1)*42 obj.windowwide-40 34],...
										'String','CalcType',...
										'Style','popupmenu',...
										'Tag',['Calc',num2str(i)]);
										
										
			%create the list with possible calculation
			posCalcLogical=strcmp(KeyList(:,3),Types{i});
			posCalc=KeyList(posCalcLogical,2);
											
			%set the list
			set(PossibleCalcList, 'String' , posCalc);
		

		end
		
		
		
		%the buttons
		buttonwidth = (obj.windowwide-60)/2-6;
		cancelbutton   =  uicontrol('Parent',obj.PopUpWindow,...
							'Units', 'Pixels',...
							'Position',[10 10 buttonwidth 34],...
							'String','Cancel',...
							'Style','pushbutton',...
							'Tag','CancelAddCalc',...
							'Callback',@(s,e) obj.SuccessButton('cancel',Types,AddOptions));

		okbutton   =  uicontrol('Parent',obj.PopUpWindow,...
							'Units', 'Pixels',...
							'Position',[13+buttonwith 10 buttonwidth 34],...
							'String','Ok',...
							'Style','pushbutton',...
							'Tag','OkAddCalc',...
							'Callback',@(s,e) obj.SuccessButton('ok',Types,AddOptions));

		%the buttons do the actual work
		
		
		
		
	end
	
	
	function SuccessButton(obj,whichone,Types,AddOptions)
		switch whichone
			case 'cancel'
				%Do nothing just close the window
				clf(obj.PopUpWindow);
				
			case 'ok'
				%get the wanted Calculations, send it back to the wrapper
				%and close the window
				childelements = get(obj.PopUpWindow,'children');
				
				for i=1:numel(Types)
					
					%create the list with possible calculation keys
					posCalcLogical=strcmp(KeyList(:,3),Types{i});
					posCalc=KeyList(posCalcLogical,1);	
				
					CalcPop = findobj(childelements,'Tag',['Calc',num2str(i)]);
					SelCalc = get(CalcTypePop,'Value');
					
					SelectedCalc(i)=posCalc(SelCalc)
				
				end
				
				if isempty(AddOptions)
					WrapPacket=SelectedCalc;
				else
					WrapPacket.SelectedCalc=SelectedCalc;
					WrapPacket.AddOptions=AddOptions;
				end	
				
				%send back to the Wrapper
				obj.Wrapper.addGUICalc(WrapPacket); 
				
				%close
				clf(obj.PopUpWindow);
		end			
		
	
	
	end
	
	
end

end
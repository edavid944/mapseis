classdef ResultViewerGUI1_1 < handle
 % This GUI allows to custom build a GUI for showing the results of calculations
 
 %New version 1.1
 	%At the moment only a place holder which is similar to the 1.0
 	%But if new features are needed they will be added here.
 
 
properties
	TheGUI
	Wrapper
	ListProxy
	Commander
	GUIConfig
	FigureName
	PlotParameter
	windowheight
	windowwide
	xPos
	yPos
	WidePercent
	wantedModulHeight
	wantedModulWide
	ModulList
	MenuHandle
	AdditionalGUIParam
	handles
	imroi_objects
	point_objects
	PointConfig
	PointStructur
	legendentries
	LegendToggle
	Version
	RandID
end

events
	DrawIt
	Finished
end


methods
	function obj = ResultViewerGUI1_1(Wrapper,FigureName,WindowPos,ListProxy,RegParameter)
		%Constructor
		
		obj.Version='1.1';
		
		obj.Wrapper = Wrapper;
		obj.ListProxy = ListProxy; 
		obj.FigureName = FigureName;
		
		%Get the Commander from the CalculationWrapper (at the moment the Commander is not needed)
		obj.Commander=Wrapper.Commander;
		
		%may change to variable position, probably not so much the size
		%add to the wrapper config later
		obj.windowwide = 1024;
		obj.windowheight = 768;
		obj.xPos=WindowPos(1);
		obj.yPos=WindowPos(2);		

		%RandID is needed in case more than one GUI is used and the AutoUpdater is used
		obj.RandID=num2str(abs(round(100*randn)));
		
		
		%fix values for the modulbuilder 
		obj.WidePercent=0.96;
		obj.wantedModulHeight=40;
		obj.wantedModulWide=100;
		obj.imroi_objects={};
		%obj.PointStructur={};
		
		
		%Register yourself in the wrapper, this should also add 
		%GUIConfig and PlotParameter defaults to this modul
		obj.Wrapper.RegisterGUI(obj,'ResultGUI',RegParameter);
		
		%Create GUI
		

		
		%make the windows
		obj.makePlotWindows;
		
		
		
		%set visible on
		set(obj.TheGUI,'Visible','on');

	end
	
	function rebuild(obj)
		obj.imroi_objects={};
		obj.makePlotWindows;
		%kill all points
		
		%set visible on
		set(obj.TheGUI,'Visible','on');
	end
	
	
	
	
	function makePlotWindows(obj)
		%This function creates the needed number of needed windows to plot the results
		%clear all window and build new
		%Structur of the GUIConfig:
		%Layout:			Determines the Preset of the window
		%CustomConfig: 		Only needed in case of custom configuration
		%ToolbarStyle:		This will determine wether there is one single space for the Toolbar ('Common')
		%					or for a single space for each subplot ('Single')
		%ToolbarPosition:	Sets the position of the toolbar can either 'none', if no toolbar is wanted or 'all' where
		%					there is no plotwindow at all (usefull for extended toolbars), or it can be
		%					'Left', 'Right' and 'Bottom'. This will be ignored if 'Single' is selected as ToolbarStyle,
		%					in that case the ToolbarPosition has to be set under each Single entry 		
		%ToolbarModuls:		This contains the Moduls for the toolbar (only used in 'Common' style, else the Moduls have to 
		%					be set under the single window entries.
		%SubPlotNames:		Contains the fieldnames of the subplot, the order has to be the same as in the choosen preset, and
		%					it is best to use the same names as in the preset. This field will be ignored in the 'Common' style.
		%MouseMenu:			Always have to be in the "Window Entries", as the work on one subfigure (copying them from window to
		%					window should not be a problem.
		%MainMenu:			Describes the MainMenu.
		%"Window 1"
		%"Window 2":		In case of the style 'Single' each Window of the plot needs it's one Toolbar setting, for this
		%					a field with the name of the corresponding subplot (they have to have the same name as entered in 
		%					SubPlotNames) under each field there has to be an entry for ToolbarPosition, ToolbarModul and Mouse
		%					Menu.	  	
		
		import mapseis.util.gui.ResultViewerPresets;
		
		flagger=false;
		if ~isempty(obj.TheGUI)
			
			close(obj.TheGUI);
			obj.TheGUI=[];
			flagger=true;
		end
			
		obj.TheGUI=figure('name',obj.FigureName,'visible','off');
		set(obj.TheGUI,'Position', [obj.xPos obj.yPos obj.windowwide obj.windowheight],'NumberTitle','off');
		
		
		
		
		%many different layouts selectable to create different plot windows
		switch obj.GUIConfig.Layout
			case 'OneWindow'
				ResConf = ResultViewerPresets('OneWindow');
			
			case 'TwoWindowHor'
				ResConf = ResultViewerPresets('TwoWindowHor');			
			
			case 'TwoWindowVer'
				ResConf = ResultViewerPresets('TwoWindowVer');			
			
			case 'ThreeWindowVer'
				ResConf = ResultViewerPresets('ThreeWindowVer');			
			
			case 'ThreeWindowHor'
				ResConf = ResultViewerPresets('ThreeWindowHor');
			
			case 'FourWindowVer'
				ResConf = ResultViewerPresets('FourWindowVer');
			
			case 'FourWindowHor'
				ResConf = ResultViewerPresets('FourWindowHor');
			
			case 'FourWindowTile'
				ResConf = ResultViewerPresets('FourWindowTile');
			
			case 'MainWindowLike'
				ResConf = ResultViewerPresets('MainWindowLike');
			
			case 'Custom' %Needs the field CustomConfig
				ResConf = obj.GUIConfig.CustomConfig;
		end
		
		%now build the subplots
		if strcmp(obj.GUIConfig.ToolbarStyle,'Common')
		
			for i=1:numel(ResConf.PlotNames)
				
				ToolPos=obj.GUIConfig.ToolbarPosition;
				
				%Build only if the Toolbar is not set to all
				if ~strcmp(ToolPos,'all');
					axisHandle(i)=subplot(ResConf.SubplotIndices(1),ResConf.SubplotIndices(2),i);
				
					%Tag it
					TheTag=ResConf.Common.(ResConf.PlotNames{i}).Tag;
					set(axisHandle(i),'Tag',TheTag); 
								
					%Size it
					TheSize=ResConf.Common.(ResConf.PlotNames{i}).(ToolPos);
					set(axisHandle(i),'Position',TheSize);
				
					%make RightButtonMenu
					if isfield(obj.GUIConfig.(ResConf.PlotNames{i}),'RightButtonMenu')
						if ~isempty(obj.GUIConfig.(ResConf.PlotNames{i}).RightButtonMenu)
							obj.makeRightButtonMenu(TheTag);
						end
					end
					
					
					
				end	
				
				
				
			end	
			
			
			if ~strcmp(obj.GUIConfig.ToolbarPosition,'none')
				%Build the Toolbar if needed
				makeToolbar(obj,'Common',obj.GUIConfig.ToolbarPosition,obj.GUIConfig.ToolbarModuls);
			end 
			
		elseif strcmp(obj.GUIConfig.ToolbarStyle,'Single')
		 
			for i=1:numel(ResConf.PlotNames)
				
				WindowName=obj.GUIConfig.SubPlotNames{i};
				ToolPos=obj.GUIConfig.(WindowName).ToolbarPosition;
				
				%Build only if the Toolbar is not set to all
				if ~strcmp(ToolPos,'all');
					axisHandle(i)=subplot(ResConf.SubplotIndices(1),ResConf.SubplotIndices(2),i);
					
					%Tag it
					TheTag=ResConf.Single.(ResConf.PlotNames{i}).Tag;
					set(axisHandle(i),'Tag',TheTag); 
									
					%Size it
					TheSize=ResConf.Single.(ResConf.PlotNames{i}).(ToolPos);
					set(axisHandle(i),'Position',TheSize);
					
				end
				
				if ~strcmp(ToolPos,'none')
					%Build the Toolbar if needed
					makeToolbar(obj,TheTag,ToolPos,obj.GUIConfig.(ResConf.PlotNames{i}).ToolbarModuls);
				end 
				
				%make RightButtonMenu
				if isfield(obj.GUIConfig.(ResConf.PlotNames{i}),'RightButtonMenu')
					if ~isempty(obj.GUIConfig.(ResConf.PlotNames{i}).RightButtonMenu)
						obj.makeRightButtonMenu(TheTag);
					end
				end

				
				
	
			end	
		
		end

		
		%make the WindowMenu if not empty
		if isfield(obj.GUIConfig,'WindowMenu')
			if ~isempty(obj.GUIConfig.WindowMenu)
			disp('I Do WindowMenu')
				obj.makeWindowMenu;
			end
		end
		
		%Set Initvalues of the AddParam entries
		if isfield(obj.GUIConfig,'InitValues')
				if ~isempty(obj.GUIConfig.InitValues)
				obj.GeneralInit(obj.GUIConfig.InitValues);
			end
		end
		
		%Apply AutoUpdater if not empty
		if isfield(obj.GUIConfig,'AutoUpdater')
			if ~isempty(obj.GUIConfig.AutoUpdater)
			disp('I Do AutoUpdater')
				obj.AutoUpdateCreator(obj.GUIConfig.AutoUpdater,'listen')
			end
		end
		
		%Do "EndProcess" if needed
		if isfield(obj.GUIConfig,'EndProcess')
			if ~isempty(obj.GUIConfig.EndProcess)
				%disp('Lets do it')
				switch obj.GUIConfig.EndProcess
					case 'call'
						obj.Wrapper.ResultEndProcess;
				end
			end
		end	
		
		%Notify all listeners that the windows are finished, this allows to ensure 
		%that certain commands are only executed when the gui is finished, this event
		%is also called after every plot update.		
		notify(obj,'Finished');
		
		if flagger
			%set visible on
			set(obj.TheGUI,'Visible','on');
		end
			
	end
	
	
	
	function makeToolbar(obj,WindowName,ToolbarPosition,ToolbarModuls)
		%This function builds the toolbar for the plot if needed.
		%The Tag is used to determine in which corner the window is. 
		
		%This value may needs to be modified.
		%The Common Positon (always the bottom left corner)
		CommonBottom=[0.02*obj.windowwide 0.02*obj.windowheight];
		CommonLeft=[0.02*obj.windowwide 0.97*obj.windowheight];
		CommonRight=[0.8*obj.windowwide 0.97*obj.windowheight];
		
		%width and height of the area (may not be needed)
		CommonBottomSize=[0.95*obj.windowwide 0.16*obj.windowheight];
		CommonLeftSize=[0.16*obj.windowwide 0.97*obj.windowheight];		
		CommonRightSize=[0.16*obj.windowwide 0.97*obj.windowheight];			
		
		
		%fix values to adjust 
		WidePercent=obj.WidePercent;
		wantedModulHeight=obj.wantedModulHeight;
		wantedModulWide=obj.wantedModulWide;
		
		%The common case, position are fixed in this function
		if strcmp(WindowName,'Common')
			switch ToolbarPosition
				case 'Left'
					startPoint=CommonLeft;
					ModulWide= CommonLeftSize(1);
					MaxModulNumber=floor(CommonLeftSize(2)/wantedModulHeight);
					ModulDirection='Vertical';
					
				case 'Right'
					startPoint=CommonRight;
					ModulWide= CommonRightSize(1);
					MaxModulNumber=floor(CommonRightSize(2)/wantedModulHeight);
					ModulDirection='Vertical';
					
				case 'Bottom'
					startPoint=CommonBottom;
					MaxModulNumber=floor(CommonBottomSize(1)/wantedModulWide);
					ModulDirection='Horizontal';
					
				case 'all'
					startPoint=[obj.windowwide, obj.windowheight];
					ModulWide= abs(obj.windowwide)*WidePercent;
					MaxModulNumber=floor(abs(obj.windowheight)/wantedModulHeight);
					ModulDirection='Vertical';
					
			end	
			
			%Allow at least one modul
			if MaxModulNumber<=0
				MaxModulNumber=1;
			end
	
		
		
		%case with at not common toolbar, position needed.
		else
			%get the position vector (findobj, could also be used, but would may return other objects with 
			%same tag)
			childish=get(obj.TheGUI,'Children');
			subTags=get(childish, 'Tag');
			thisit=strcmp(subTags,WindowName);
			LePosition=get(childish(thisit),'Position');
			
			
			%decide were the toolbar should be
			switch ToolbarPosition
				case 'Left'
					startPoint=[LePosition(1)*obj.windowwide-100, LePosition(2)*obj.windowheight-20];
					ModulWide= abs(LePosition(1)*obj.windowwide - LePosition(3)*obj.windowwide)*obj.WidePercent;
					MaxModulNumber=floor(abs(LePosition(2)*obj.windowheight - LePosition(4)*obj.windowheight)/wantedModulHeight);
					ModulDirection='Vertical';
					
				case 'Right'
					startPoint=[(LePosition(1)+LePosition(3))*obj.windowwide+20, LePosition(2)*obj.windowheight];
					ModulWide= abs(LePosition(1)*obj.windowwide)*obj.WidePercent;
					MaxModulNumber=floor(abs(LePosition(2)*obj.windowheight - LePosition(4)*obj.windowheight)/wantedModulHeight);
					ModulDirection='Vertical';
				
				case 'Bottom'
					startPoint=[LePosition(1)*obj.windowwide-80, LePosition(2)*obj.windowheight-60];
					MaxModulNumber=floor(abs(LePosition(1)*obj.windowwide - LePosition(3)*obj.windowwide)/wantedModulWide);
					ModulDirection='Horizontal';
				
				case 'all'
					startPoint=[LePosition(1)*obj.windowwide, LePosition(2)*obj.windowheight];
					ModulWide= abs(1-LePosition(1)*obj.windowwide)*obj.WidePercent;
					MaxModulNumber=floor(abs(1-LePosition(2)*obj.windowheight)/wantedModulHeight);
					ModulDirection='Vertical';
	
					
			end
				
			%Allow at least one modul
			if MaxModulNumber<=0
				MaxModulNumber=1;
			end
					
				
		
		end
		
		
		
		%Position should now be set, next is the building of the Moduls
		%--------------------------------------------------------------
		
		%take smaller number of moduls (either the MaxModulNumber or the Number of moduls in ToolbarModuls)
		NumberModuls=min(MaxModulNumber,numel(ToolbarModuls));
		
		currentposition=NumberModuls;
			
		%the loop 
			for i=1:NumberModuls
				if strcmp(ModulDirection,'Horizontal')
					EntrySize=[startPoint(1)+currentposition*wantedModulWide, startPoint(2),...
								wantedModulWide,wantedModulHeight];			
				else	
					EntrySize=[startPoint(1), startPoint(2)+currentposition*wantedModulHeight,...
								wantedModulWide,wantedModulHeight];
				end
				
				switch ToolbarModuls{i}.ModulType
					
					%case 'CalcButton'
					%obj.CreateCalcButton(ToolbarModuls{i},currentposition);
					
					%case 'DataSelector'
					%	obj.CreateDataSelector(ToolbarModuls{i},currentposition);
						
					%case 'FilterSelector'
					%	obj.CreateFilterSelector(ToolbarModuls{i},currentposition);
					
					%case 'CalcSelector'
					%	obj.CreateCalcSelector(ToolbarModuls{i},currentposition);
						
					%case 'CalcModify'
					%	obj.CreateCalcModify(ToolbarModuls{i},currentposition);
						
					case 'PopUp'
						obj.CreatePopUp(ToolbarModuls{i},currentposition,EntrySize);
						
					case 'ListBox'
						obj.CreateListBox(ToolbarModuls{i},currentposition,EntrySize);
						
					case 'Button'
						obj.CreateButton(ToolbarModuls{i},currentposition,EntrySize,ModulDirection);
						
						
					case 'Texter'
						obj.CreateTexter(ToolbarModuls{i},currentposition,EntrySize);
						
					case 'Checkerbox'
						obj.CreateCheckerbox(ToolbarModuls{i},currentposition,EntrySize,ModulDirection);
					
					case 'Entryfield'
						obj.CreateEntryfield(ToolbarModuls{i},currentposition,EntrySize);
						
					case 'Empty'
						%just do nothing
					
				end
			
				% ---------------------------------------------------------------
				%|The commented cases are at the moment not available, calcultion|
				%|related buttons are may be added later						 |	
				% ---------------------------------------------------------------
			
				%shift down or right
				currentposition=currentposition-1;


				
			end
		
		
		
	end
	
	
	
	
	function makeWindowMenu(obj)
		%This function creates the menu on top of the figure window
		%The configuration is stored under obj.GUIConfig.WindowMenu
		%and is save as cell array 
		%Each "main" cell element (obj.GUIConfig.WindowMenu{i}) contains
		%a structur with two field:
		%Name: the Name of the MenuPoint
		%Entries: A cell array with the selectable Menuentries
		%
		
		%Each element of the Entries cell array 
		%(obj.GUIConfig.(WindowMenu{i}).Entries{j}) consist again of a structur:
		%Label: Name of the entry
		%Tag: Tag of the entry
		%Callback: Either a function handle for a function or one of the keywords
		%The keywords can be: 
		%	'draw': Adds the menu point drawing to the menu
		%	'AddParam':  Writes a specified value into the AdditionalPlotParams
		%	'Param': Writes a specified value into the PlotParameters
		%	'Legend': %Switches the legend on and off
		%	'LoadData': Currently not available
		%	'LoadFilter': Currently not available
		%	'LoadResult': Currently not available
		%	'SaveData': Currently not available
		%	'SaveFilter': Currently not available
		%	'SaveResult': Currently not available
		%Checker: 	This can be true or false in case of true the menu will have a checker
		%			if selected.
		%WindowTag: needed if Param or AddParam 
		%Param: needed if Param or AddParam 
		%Valu: Needed in the case of Toggle, has to be numeric 
		%DoThings: needed if Param or AddParam 
		%EvalMode: can only be used in the 'Param' mode, the value will be saved as string 
		%	   and evaluated if needed
		%Instead of a structur each entry can also be a keyword in order to use prebuild menus:
		%	'LoadSave':	Load Save dialoge, at the moment not working
		
		
		
		if isfield(obj.GUIConfig,'WindowMenu');
			WindowConfig=obj.GUIConfig.WindowMenu;
		else 
			WindowConfig={};	
		end
		
		%Pack the single menu into a cell if needed
		if ~iscell(WindowConfig)
			WindowConfig={WindowConfig};
		end
		
		for i=1:numel(WindowConfig)
			if ~isstr(WindowConfig{i})
			
				% Create the contextmenu with parent the main figure			
				obj.MenuHandle{i} = uimenu(obj.TheGUI,'Label',WindowConfig{i}.Label);
				
				%get the menu entries
				MenuEntries=WindowConfig{i}.Entries;
				
				for j=1:numel(MenuEntries)
					
					if ~isstr(MenuEntries{j}.Callback)
						uimenu(obj.MenuHandle{i},'Label',MenuEntries{j}.Label,...
               					 'Callback',MenuEntries{j}.Callback);
					
					
					else
						%set the evalmode variable
						if isfield(MenuEntries{j},'EvalMode');
							EvalMode=MenuEntries{j}.EvalMode;
						else
							EvalMode=false;
						end
						
						if isempty(EvalMode)
							EvalMode=false;
						end	
					
						switch MenuEntries{j}.Callback
							
							%Normal draw Menupoint
							case 'draw'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Redraw Plot';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								if isempty(MenuEntries{j}.WindowTag)
									WindowTag='allPlots';
								else
									WindowTag=MenuEntries{j}.WindowTag;	
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
								'Callback',@(s,e) obj.Drawer(WindowTag));
               				
               					 
							case 'Param'
								uimenu(obj.MenuHandle{i},'Label',MenuEntries{j}.Label,...
								 'Callback',@(s,e) MenuPoint2value(obj,s,'Param',Checker,...
								 MenuEntries{j}.WindowTag, MenuEntries{j}.Param, ...
								 MenuEntries{j}.ValueType, MenuEntries{j}.Valu, ...
								 MenuEntries{j}.ChangeMode, MenuEntries{j}.DoThings,EvalMode));
							
							
							case 'AddParam'
								uimenu(obj.MenuHandle{i},'Label',MenuEntries{j}.Label,...
								 'Callback',@(s,e) MenuPoint2value(obj,s,'AddParam',Checker,...
								 MenuEntries{j}.WindowTag, MenuEntries{j}.Param, ...
								 MenuEntries{j}.ValueType, MenuEntries{j}.Valu, ...
								 MenuEntries{j}.ChangeMode, MenuEntries{j}.DoThings));
								 
								
							
							
							case 'Legend'
								if isempty(MenuEntries{j}.Label)
									TheLabel=['Legend on/off',MenuEntries{j}.WindowTag];
								else
									TheLabel=MenuEntries{j}.Label;
								end

								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.legendary('set',MenuEntries{j}.WindowTag,s));
								
								%initate the LegendToggle
								obj.legendary('chk',MenuEntries{j}.WindowTag,[]);
							
								
							case 'LoadData'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Load DataStore';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Commander.loadCatalog);
							
							case 'LoadFilter'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Load FilterList';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Commander.loadFilterlist);
							
							case 'LoadResult'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Load Calculation Results';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Wrapper.loadCalcGUI(MenuEntries{j}.How));
								
							case 'LoadSession'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Load Calculation Session';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Wrapper.loadSessionGUI(MenuEntries{j}.How));
							
							case 'SaveData'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Save DataStore';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Commander.saver('Cata'));
									
							
							case 'SaveFilter'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Save FilterList';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Commander.saver('Filter'));
							
							case 'SaveResult'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Save Calculation Result';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Wrapper.saveCalcGUI(MenuEntries{j}.How));
							
							case 'SaveSession'
								if isempty(MenuEntries{j}.Label)
									TheLabel='Save Calculation Session';
								else
									TheLabel=MenuEntries{j}.Label;
								end
								
								uimenu(obj.MenuHandle{i},'Label',TheLabel,...
									'Callback',@(s,e) obj.Wrapper.saveSessionGUI(MenuEntries{j}.How));
							
													
							
						end		
					
					
					
					
					end
				
				
				end
				
			
			else
				switch WindowConfig{i}
					case 'LoadSaveCalc'
						obj.MenuHandle{i} = uimenu(obj.TheGUI,'Label','Calculation');
						uimenu(obj.MenuHandle{i},'Label','Load Results (overwrite)',...
									'Callback',@(s,e) obj.Wrapper.loadCalcGUI('overwrite'));
						uimenu(obj.MenuHandle{i},'Label','Add Results from file',...
									'Callback',@(s,e) obj.Wrapper.loadCalcGUI('add'));
						uimenu(obj.MenuHandle{i},'Label','Replace Result from file',...
									'Callback',@(s,e) obj.Wrapper.loadCalcGUI('replace'));
						uimenu(obj.MenuHandle{i},'Label','Save Calculation',...
									'Callback',@(s,e) obj.Wrapper.saveCalcGUI('single'));			
						uimenu(obj.MenuHandle{i},'Label','Save All Calculation',...
									'Callback',@(s,e) obj.Wrapper.saveCalcGUI('all'));
									
					case 'LoadSaveAll'
						obj.MenuHandle{i} = uimenu(obj.TheGUI,'Label','Session');
						uimenu(obj.MenuHandle{i},'Label','Load Session',...
									'Callback',@(s,e) obj.Wrapper.loadSessionGUI([]));
						uimenu(obj.MenuHandle{i},'Label','Save Session',...
									'Callback',@(s,e) obj.Wrapper.saveSessionGUI([]));
									
					case 'LoadSaveDataFilter'
						obj.MenuHandle{i} = uimenu(obj.TheGUI,'Label','Load/Save Data');
						uimenu(obj.MenuHandle{i},'Label','Load Catalog',...
									'Callback',@(s,e) obj.Commander.loadCatalog);
						uimenu(obj.MenuHandle{i},'Label','Load FilterList',...
									'Callback',@(s,e) obj.Commander.loadFilterlist);
						uimenu(obj.MenuHandle{i},'Label','Save Catalog',...
									'Callback',@(s,e) obj.Commander.saver('Cata'));	
						uimenu(obj.MenuHandle{i},'Label','Save FilterList',...
									'Callback',@(s,e) obj.Commander.saver('Filter'));			
						
									
				end
				
			end
			
		end	 
		
		
	
	end	
	
	
	
	function makeRightButtonMenu(obj,WindowTag)
		%This function creates the rightbutton menu for a given plot 
		%The configuration is stored under obj.GUIConfig.(WindowTag).RightButtonMenu
		%and is save as cell array 
		%Each "main" cell element (obj.GUIConfig.WindowMenu{i}) contains
		%
		%
		
		%Each element of the Entries cell array 
		%(obj.GUIConfig.(WindowMenu{i}).Entries{j}) consist again of a structur:
		%Label: Name of the entry
		%Tag: Tag of the entry
		%Callback: Either a function handle for a function or one of the keywords
		%The keywords can be: 
		%	'draw': Adds the menu point drawing to the menu
		%	'AddParam':  Writes a specified value into the AdditionalPlotParams
		%	'Param': Writes a specified value into the PlotParameters
		%	'Legend': %Switches the legend on and off
		%	'LoadData': Currently not available
		%	'LoadFilter': Currently not available
		%	'LoadResult': Currently not available
		%	'SaveData': Currently not available
		%	'SaveFilter': Currently not available
		%	'SaveResult': Currently not available
		%Checker: 	This can be true or false in case of true the menu will have a checker
		%			if selected
		%WindowTag: needed if Param or AddParam 
		%Param: needed if Param or AddParam 
		%ValueType: 
		%Valu: Needed in the case of Toggle, has to be numeric 
		%ChangeMode:
		%DoThings: needed if Param or AddParam 
		%

		
		if isfield(obj.GUIConfig.(WindowTag),'RightButtonMenu');
			RightConfig=obj.GUIConfig.(WindowTag).RightButtonMenu;
		else 
			RightConfig={};	
		end
		
		%find the plotaxis
		parentAxis = findobj(obj.TheGUI,'Tag',WindowTag);

		% Create the contextmenu with parent the main figure
		cmenu = uicontextmenu('Parent',obj.TheGUI);
		
		% Now make the menu be associated with the correct axis
		set(obj.TheGUI,'UIContextMenu',cmenu);
		set(parentAxis,'UIContextMenu',cmenu);
		set(cmenu,'Tag','RightButtonMenu');
		

		
		for i=1:numel(RightConfig)
			
			if ~isstr(RightConfig{i}.Callback)
				uimenu(cmenu,'Label',RightConfig{j}.Label,...
	               					 'Callback',RightConfig{j}.Callback);	
	        else
	        
	        	%set the evalmode variable
			if isfield(RightConfig{i},'EvalMode');
				EvalMode=RightConfig{i}.EvalMode;
			else
				EvalMode=false;
			end
			
			if isempty(EvalMode)
				EvalMode=false;
			end	
	        
	        	switch RightConfig{i}.Callback
							
					%Normal draw Menupoint
					case 'draw'
						if isempty(RightConfig{i}.Label)
							TheLabel='Redraw Plot';
						else
							TheLabel=RightConfig{i}.Label;
						end
						
						if isempty(RightConfig{i}.WindowTag)
							WindowTagger='allPlots';
						else
							WindowTagger=WindowTag;	
						end
						
						uimenu(cmenu,'Label',TheLabel,...
             					 'Callback',@(s,e) obj.Drawer(WindowTagger));
             				
             					 
					case 'Param'
						uimenu(cmenu,'Label',RightConfig{i}.Label,...
             					 'Callback',@(s,e) MenuPoint2value(obj,s,'Param',RightConfig{i}.Checker,...
             					 WindowTag, RightConfig{i}.Param, ...
             					 RightConfig{i}.ValueType, RightConfig{i}.Valu, ...
             					 RightConfig{i}.ChangeMode, RightConfig{i}.DoThings,EvalMode));
					
					
					case 'AddParam'
						
						uimenu(cmenu,'Label',RightConfig{i}.Label,...
             					 'Callback',@(s,e) MenuPoint2value(obj,s,'AddParam',RightConfig{i}.Checker,...
             					 WindowTag, RightConfig{i}.Param, ...
             					 RightConfig{i}.ValueType, RightConfig{i}.Valu, ...
             					RightConfig{i}.ChangeMode, RightConfig{i}.DoThings));
             					 
						
					
					
					case 'Legend'
						if isempty(RightConfig{i}.Label)
							TheLabel=['Legend on/off',RightConfig{i}.WindowTag];
						else
							TheLabel=RightConfig{i}.Label;
						end

						%initate the LegendToggle
						obj.legendary('chk',WindowTag);
							
						uimenu(cmenu,'Label',TheLabel,...
             					 'Callback',@(s,e) obj.legendary('set',WindowTag,s));
						
					
					case 'LoadData'
						%not defined at the moment
					
					case 'LoadFilter'
						%not defined at the moment
					
					case 'LoadResult'
						%not defined at the moment
					
					case 'SaveData'
						%not defined at the moment
					
					case 'SaveFilter'
						%not defined at the moment
					
					case 'SaveResult'
						%not defined at the moment
				end		
					
					
	
	        
	        end       					 
		end
	end
	
	
	function PointMaker(obj,DoWhat)
		%This function can be called to make draggable point in the subplots
		%The function will prepare the date for the different points and call the 
		%function BuildPoint to plot the points
		%DoWhat can be used to either 'start' the whole point set, 'update' the points
		%(where new points are added or old deleted) or 'refresh' points (where) all points
		%are deleted and newly set)
		disp('PointMaker')
		switch DoWhat
			case 'start'
				PointConfig=obj.Wrapper.createPointStructur;
				obj.PointConfig=PointConfig;
				%get the fieldnames
				fields=fieldnames(obj.PointConfig)
				
				for i=1:numel(fields)
					ThePoint=obj.PointConfig.(fields{i});
					obj.BuildPoint(ThePoint,'add');
				end
				
			case 'update'
				PointConfig=obj.Wrapper.createPointStructur;
				obj.PointConfig=PointConfig;
				
				%get the fieldnames
				fields=fieldnames(PointConfig)

				windowfields=fieldnames(obj.point_objects);
				TagList=fieldnames(obj.PointStructur);
				
				%disp(TagList)
				%now compare the to lists
				
				%add points
				for i=1:numel(fields)
					founded=strcmp(TagList,fields{i})
					if ~any(founded)
						ThePoint=PointConfig.(fields{i});
						obj.BuildPoint(ThePoint,'add');
					end
				end
				
				%remove points
				for i=1:numel(TagList)
					founded=strcmp(fields,TagList{i})
					if ~any(founded)
						ThePoint=obj.PointStructur.(TagList{i});
						obj.BuildPoint(ThePoint,'remove');
						disp('try remove points')
					end
				end
				
				
				
			case 'refresh'
				PointConfig=obj.Wrapper.createPointStructur;
				obj.PointConfig=PointConfig;
				
				%get the fieldnames
				fields=fieldnames(PointConfig);
				disp(obj.point_objects)
				for i=1:numel(fields)
					ThePoint=PointConfig.(fields{i});
					obj.BuildPoint(ThePoint,'remove');
					obj.BuildPoint(ThePoint,'add');
				end
				
				%disp(PointConfig)
			
			case 'stopall'
				PointConfig=obj.PointConfig;
				fields=fieldnames(PointConfig);
				for i=1:numel(fields)
					try
						ThePoint=PointConfig.(fields{i});
						obj.BuildPoint(ThePoint,'remove');
					catch
						disp('point not removed')
					end	
				end
				
		end	
		
		
	
	end
	
	
	
	function BuildPoint(obj,PointStructur,Work)
		%The function build the actuall point, it adds an entry to the plot function
		% and directly plots the point
		% The new Layer have to be written into a special structur, because the ResultStructur
		% is always rewritten when the draw routine is called.
		
		%In this version of the ResultGUI, the BuildPoint function has to be called form the wrapper
		%as it makes no sense to draw points when there is no Graphics drawn.
		%In future releases this might be changing.
		%pos and WinTag are only needed in the remove_obj, where the point is not specified in the PointConfig
		%anymore
		

		
		import mapseis.plot.*;
		
		
		switch Work
			case 'add'
			
				%get the axis of the subplot
				TheAxis = findobj(obj.TheGUI,'Tag',PointStructur.WindowTag);
				
				%to save in the right bin
				WindowTag=PointStructur.WindowTag;
				
				%Produce entry into to Layer structur
				LayerName=PointStructur.Tag;
				NewPointLayer=PointStructur;
				NewPointLayer.Name=LayerName;
				
				NewPointLayer.PlotType='point';

				NewPointLayer.PostProcess=...
				@(plotAxis,Element,handle,legendentry) ... 
				obj.PointFunctionAdder(plotAxis,Element,handle,legendentry,...
				PointStructur.Tag,PointStructur.AutoFire);
				
				%Check the if both default values are set, if not put the point
				%into the middle of the missing axis
				if isnan(NewPointLayer.Coord(1))
					axlevalues=get(TheAxis,'xlim');

					if isempty(axlevalues)
						axlevalues=[0 100];
					end
					
					NewPointLayer.Coord(1)=(axlevalues(2)-axlevalues(1))/2;
				end
				
				if isnan(NewPointLayer.Coord(2))
					axlevalues=get(TheAxis,'ylim');
					
					if isempty(axlevalues)
						axlevalues=[0 100];
					end
					
					NewPointLayer.Coord(2)=(axlevalues(2)-axlevalues(1))/2;
				end
				
				disp(NewPointLayer.Coord)
				%Put the entry at the end of the PointStructur
				obj.PointStructur.(LayerName)=NewPointLayer;
				
				%set Parameter so it is plotted every time.
				obj.PlotParameter.(PointStructur.WindowTag).(LayerName)=1;
				
				if ~isempty(TheAxis)
					%produce point
					[handle legendentry] = PlotPoint(TheAxis,NewPointLayer);	
					obj.point_objects.(WindowTag){end+1}=handle;
					
					%set callback
					obj.PointFunctionAdder(TheAxis,[],handle,[],...
					PointStructur.Tag,PointStructur.AutoFire);
				end
				
			case 'remove'
				%Always just "try" it is possible, that the point is not existing anymore
				%find Entry in the Layer Structur
				LayerName=PointStructur.Tag;
				disp(LayerName);
				
				
				
				%to delete in the right bin
				WindowTag=PointStructur.WindowTag;
				disp(WindowTag),
				
				%If found remove and repair structur.
				try
					obj.PointStructur=rmfield(obj.PointStructur, LayerName);
				catch
					disp('not removed')
				end
				
				
				%search entry in the Parameters and remove
				try
					obj.PlotParameter.(WindowTag)=...
						rmfield(obj.PlotParameter.(WindowTag),LayerName);
				catch	
					disp('could not be removed')
				end
				
				
				%delete point
				%find point
				for i=1:numel(obj.point_objects.(WindowTag))
						TagList{i}=get(obj.point_objects.(WindowTag){i},'Tag');

				end	
				
				FoundPoint=strcmp(TagList,LayerName);
				ThePoint=obj.point_objects.(WindowTag){FoundPoint};
				
				if ~isempty(ThePoint)
					obj.point_objects.(WindowTag)=obj.point_objects.(WindowTag)(~FoundPoint);
					clear ThePoint;
				end	
			
			case 'remove_obj'
				%Can be done when only the object is existing anymore
				%Needs the position in the 
				PointTag=get(PointStructur,'Tag');
				
			
			
			
		end	
		
	
	end
	
	
	function AutoUpdateCreator(obj,ConfigStructur, DoWhat)
		%creates all the needed AutoUpdater
		%the Entries of the ConfigStructur has to be the following
		%Event: The String with the event (cell array if more than on are wanted
		%UpdateType: decides which part has to be updated:
		%				'all': updates Toolbars, Points and Plots, 
		%				'tools': just updates the toolbar
		%				'points': just updates the points (if available)
		%				'pointsNtools': updates points and toolbars.
		%				'plots':only the plots are updated.
		%	     (cell array has to be used in case of more than one Updater)
		%WindowTag: Only needed if plots are updated, can also be left empty in that case
		%	    all plots will be updated (again cell array for multiple updaters)
		%The Parameter DoWhat decides what will be done, 'Listen' sets all the Updater to on
		%and 'quiet' ereases all updaters.
		
		if ~isfield(ConfigStructur,'WindowTag')
			ConfigStructur.WindowTag=[];
		end
		
		if ~iscell(ConfigStructur.Event)
			ConfigStructur.Event={ConfigStructur.Event};
			ConfigStructur.UpdateType={ConfigStructur.UpdateType};
			ConfigStructur.WindowTag={ConfigStructur.WindowTag};
		end
		
		switch DoWhat
			case 'listen'
				for i=1:numel(ConfigStructur.Event)
					
					TheEvent=ConfigStructur.Event{i};
					prefix=['ResGUI_',obj.RandID,'_',num2str(i)];
					%in case a windowtag is missing
					try
						WindowTag=ConfigStructur.WindowTag{i};
					catch
						WindowTag=[];
					end
					
					WhichListen=ConfigStructur.UpdateType{i};
					
					obj.ListProxy.quiet(obj.Wrapper,TheEvent,...
						@(s,e) obj.AutoUpdater(WhichListen,WindowTag),prefix); 
					obj.ListProxy.listen(obj.Wrapper,TheEvent,...
						@(s,e) obj.AutoUpdater(WhichListen,WindowTag),prefix); 
				end
	
			case 'quiet'
				for i=1:numel(ConfigStructur.Event)
					TheEvent=ConfigStructur.Event{i};
					prefix=['ResGUI_',obj.RandID,'_',num2str(i)];
					%in case a windowtag is missing
					try
						WindowTag=ConfigStructur.WindowTag{i};
					catch
						WindowTag=[];
					end
					
					WhichListen=ConfigStructur.UpdateType{i};
					
					obj.ListProxy.quiet(obj.Wrapper,TheEvent,...
						@(s,e) obj.AutoUpdater(WhichListen,WindowTag),prefix); 

				end
		
				
		end
		
	end
	
	
	
	function AutoUpdater(obj,whichParts,WindowTag)
		%This function updates the ResultGUI automatically up one the specified event
		%The option for whichParts are: 'all': updates Toolbars, Points and Plots, 
		%				'tools': just updates the toolbar
		%				'points': just updates the points (if available)
		%				'pointsNtools': updates points and toolbars.
		%WindowTag can be used to update only a certain plot, if left empty all plots will
		%updated. 
		disp('AutoUpdater')
		if isempty(WindowTag)
			WindowTag='all';
		end
		disp('Called the Updater')
		switch whichParts
			case 'all'				
				obj.makePlotWindows;
				disp('update all')
				if ~isempty(obj.PointConfig)
					obj.PointMaker('update')
				end
				
				obj.Drawer(WindowTag);
				
				
			case 'tools'
				obj.makePlotWindows;

			
			case 'points'
				if ~isempty(obj.PointConfig)
					obj.PointMaker('update')
				end

			
			
			case 'pointsNtools'
				obj.makePlotWindows;
			
				if ~isempty(obj.PointConfig)
					obj.PointMaker('update')
				end
				
				
			case 'plots'
				obj.Drawer(WindowTag);
			
		end	
		
		
	end
	
	
	
	
	
	
	function ParameterChanger(obj,WindowTag, Param, ValueType, Valu, ValMod, DoThings,EvalMode) 
		%This is a generic function for changing the PlotParameters
		%It can be used in the most cases, alternativ it is also possible
		%to use external function for a GUI element.
		%WindowTag: The Tag of the plot the Parameter is "working" on
		% 			Also the string 'nonePlot' (will be safed under PlotParameters.nonePlot.(Param))
		%			can be used if the Parameter does not belong to a specific plotwindow
		%Param: 	The Parameter which has to be changed (string)
		%ValueType:	Defines the type the value has, different types allow different way of manipulation 
		%			'general': unspecified type could be anything
		%			'string' : the value has to be a string, at the moment similar to 'general',
		%					   but further string operator may be added later
		%			'number':  the value has to be a number, special operator are available
		%			'logical': the value has to be true or false, special operator are available.					   
		%Valu:		The Value which has to be set to the selected Parameter Param
		%ValMod:	This is needed for the special operators, in the other case it will be ignored:
		%			'number': 	'inc': adds 1 to the original value (Valu ignored)
		%						'dec': subtracts 1 from the original value (Valu ignored)
		%						'add': adds Valu to the original value
		%						'sub': subtracts Valu from the original value									
		%						'mul': multiplys the original value with Valu
		%						'div': divides the original value with Valu
		%						'set': Replaces the original value with Valu (leave ValMod empty ([]) has the same effect)
		%			'logical'	'toggle':	toggles the original value 1 -> 0 and 0 -> 1 (~)
		%						'and':		uses an 'and' (&) between original value and Valu
		%						'or':		uses an 'or' (|) between original value and Valu			
		%						'xor':		uses a 'xor' (xor()) between original value and Valu (xor(a,b) = (a&~b) | (~a&b))
		%						'set':		Replaces the original value with Valu (leave ValMod empty ([]) has the same effect)
		%DoThings:	If this is set to true, the notify the wrapper to draw 
		%EvalMode: 	Meant for the plotting, it allows to save a numerical values as location string which will be evaluated when the plot function is called

		if nargin<8
			EvalMode=false;
		end
		
		switch ValueType
			case 'general'
				obj.PlotParameter.(WindowTag).(Param)=Valu;
			
			case 'string'
				%new feature automatic transition into string if numeric
				if isnumeric(Valu)
					Valu=num2str(Valu);
				end
				
				if ischar(Valu) %allows only strings
					if isempty(ValMod)
						obj.PlotParameter.(WindowTag).(Param)=Valu;
					
					else	
						switch ValMod
							case 'set'
								obj.PlotParameter.(WindowTag).(Param)=Valu;
							case 'toggle'
								if isfield(obj.PlotParameter,WindowTag)
									if isfield(obj.PlotParameter.(WindowTag),Param)
										%finally compare if saved Valu is identical true:set to 0
										if strcmp(obj.PlotParameter.(WindowTag).(Param),Valu)
											obj.PlotParameter.(WindowTag).(Param)=[];
										else
											obj.PlotParameter.(WindowTag).(Param)=Valu;
										end
									else
										obj.PlotParameter.(WindowTag).(Param)=Valu;
									end
									
									
								else
									obj.PlotParameter.(WindowTag).(Param)=Valu;
								end		 	
	
						end		
					end
					
				
				
				
				end
					
			case 'number'
				%new feature automatic transition into numeric if possible
				orgValu=Valu;
				if ~isnumeric(Valu)
					rawValu=str2num(Valu);
					disp(Valu)
					%if empty try to eval
					if isempty(rawValu)
						rawValu=eval(Valu);
					end
					
					if ~isempty(rawValu)
					Valu=rawValu;
					end
				end	
				
				%override everything if EvalMode
				if EvalMode
					Valu=orgValu;
					obj.PlotParameter.(WindowTag).(Param)=Valu;
				end
				
				%only do something if it is a number
				if 	isnumeric(Valu)
					
					if isempty(ValMod)
						obj.PlotParameter.(WindowTag).(Param)=Valu;
					
					else	
						switch ValMod
							case 'inc'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param)+1;
							case 'dec'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param)-1;
							case 'add'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param)+Valu;
							case 'sub'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param)-Valu;
							case 'mul'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param)*Valu;
							case 'div'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param)/Valu;
							case 'set'
								obj.PlotParameter.(WindowTag).(Param)=Valu;	
							case 'toggle'
								if isfield(obj.PlotParameter,WindowTag)
									if isfield(obj.PlotParameter.(WindowTag),Param)
										%finally compare if saved Valu is identical true:set to 0
										if obj.PlotParameter.(WindowTag).(Param)==Valu
											obj.PlotParameter.(WindowTag).(Param)=0;
										else
											obj.PlotParameter.(WindowTag).(Param)=Valu;
										end
									else
										obj.PlotParameter.(WindowTag).(Param)=Valu;
									end
									
									
								else
									obj.PlotParameter.(WindowTag).(Param)=Valu;
								end		 	
						end
					
					end			
				end
				
			case 'logical' 
				%only do something if it is a logical value
				if 	islogical(Valu)
					
					if isempty(ValMod)
					obj.PlotParameter.(WindowTag).(Param)=Valu;
					
					else	
						switch ValMod
							case 'toggle'
								obj.PlotParameter.(WindowTag).(Param)=~obj.PlotParameter.(WindowTag).(Param);
							case 'and'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param) & Valu;
							case 'or'
								obj.PlotParameter.(WindowTag).(Param)=obj.PlotParameter.(WindowTag).(Param) | Valu;
							case 'xor'
								obj.PlotParameter.(WindowTag).(Param)=xor(obj.PlotParameter.(WindowTag).(Param),Valu);
							case 'set'
								obj.PlotParameter.(WindowTag).(Param)=Valu;	
						end
					
					end			
	
	
				end
		end	
	
		if DoThings %notify the wrapper if wanted
			notify(obj,'DrawIt');
		end
	
	end
	
	
	
	
	
	
	function AdditionalParaChanger(obj, Param, ValueType, Valu, ValMod, DoThings) 
		%This is a generic function for changing the AdditionalGUIParam, it is in way similar to ParameterChanger
		%but with the AdditionalGUIParam
		%Why the differntiation between the PlotParameter and the Additional Parameters? The PlotParameter are send 
		%to Wrapper and are be default also set be the Wrapper. Some Parameter are not meant for the Wrapper and 
		%should if possible not be overwritten or set by the Wrapper, this values can be written into AdditionalGUIParam 
		%and are only meant for each ResultGUI itself (Remember: More than one ResultGUI can be used at one time).
		%In the end it is just cleaner and safer to use two parameter set and not mix the two parametersets. 
		%Also the AdditionalGUIParam are common between all windows and can be shared easier.

		%Param: 	The Parameter which has to be changed (string)
		%ValueType:	Defines the type the value has, different types allow different way of manipulation 
		%			'general': unspecified type could be anything
		%			'string' : the value has to be a string, at the moment similar to 'general',
		%					   but further string operator may be added later
		%			'number':  the value has to be a number, special operator are available
		%			'logical': the value has to be true or false, special operator are available.					   
		%Valu:		The Value which has to be set to the selected Parameter Param
		%ValMod:	This is needed for the special operators, in the other case it will be ignored:
		%			'number': 	'inc': adds 1 to the original value (Valu ignored)
		%						'dec': subtracts 1 from the original value (Valu ignored)
		%						'add': adds Valu to the original value
		%						'sub': subtracts Valu from the original value									
		%						'mul': multiplys the original value with Valu
		%						'div': divides the original value with Valu
		%						'set': Replaces the original value with Valu (leave ValMod empty ([]) has the same effect)
		%			'logical'	'toggle':	toggles the original value 1 -> 0 and 0 -> 1 (~)
		%						'and':		uses an 'and' (&) between original value and Valu
		%						'or':		uses an 'or' (|) between original value and Valu			
		%						'xor':		uses a 'xor' (xor()) between original value and Valu (xor(a,b) = (a&~b) | (~a&b))
		%						'set':		Replaces the original value with Valu (leave ValMod empty ([]) has the same effect)
		%DoThings:	If this is set to true, the notify the wrapper to draw 

		
		switch ValueType
			case 'general'
				obj.AdditionalGUIParam.(Param)=Valu;
			
			case 'string'
				if ischar(Valu) %allows only strings
					if isnumeric(Valu)
						Valu=num2str(Valu);
					end
					
					if isempty(ValMod)
						obj.AdditionalGUIParam.(Param)=Valu;
					
					else	
						switch ValMod
							case 'set'
								obj.AdditionalGUIParam.(Param)=Valu;
							
							case 'toggle'
								if isfield(obj.AdditionalGUIParam,Param)
									%finally compare if saved Valu is identical true:set to 0
									if strcmp(obj.AdditionalGUIParam.(Param),Valu)
										obj.obj.AdditionalGUIParam.(Param)=0;
									else
										obj.obj.AdditionalGUIParam.(Param)=Valu;
									end
								else
									obj.obj.AdditionalGUIParam.(Param)=Valu;
								end
								
						end
						
					end
				end
					
			case 'number'
				%new feature automatic transition into numeric if possible
				if ~isnumeric(Valu)
					rawValu=str2num(Valu);
					
					%if empty try to eval
					if isempty(rawValu)
						rawValu=eval(Valu);
					end
					
					if ~isempty(rawValu)
					Valu=rawValu;
					end
				end		
			
			
				%only do something if it is a number
				if isnumeric(Valu)
					
					if isempty(ValMod)
						obj.AdditionalGUIParam.(Param)=Valu;
					
					else	
						switch ValMod
							case 'inc'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param)+1;
							case 'dec'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param)-1;
							case 'add'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param)+Valu;
							case 'sub'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param)-Valu;
							case 'mul'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param)*Valu;
							case 'div'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param)/Valu;
							case 'set'
								obj.AdditionalGUIParam.(Param)=Valu;
							
							case 'toggle'

								if isfield(obj.AdditionalGUIParam,Param)
									%finally compare if saved Valu is identical true:set to 0
									if obj.AdditionalGUIParam.(Param)==Valu
										obj.obj.AdditionalGUIParam.(Param)=0;
									else
										obj.obj.AdditionalGUIParam.(Param)=Valu;
									end
								else
									obj.obj.AdditionalGUIParam.(Param)=Valu;
								end
									

						end
					
					end			
				end
				
			case 'logical' 
				%only do something if it is a logical value
				if 	islogical(Valu)
					
					if isempty(ValMod)
					obj.AdditionalGUIParam.(Param)=Valu;
					
					else	
						switch ValMod
							case 'toggle'
								obj.AdditionalGUIParam.(Param)=~obj.AdditionalGUIParam.(Param);
							case 'and'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param) & Valu;
							case 'or'
								obj.AdditionalGUIParam.(Param)=obj.AdditionalGUIParam.(Param) | Valu;
							case 'xor'
								obj.AdditionalGUIParam.(Param)=xor(obj.AdditionalGUIParam.(Param),Valu);
							case 'set'
								obj.AdditionalGUIParam.(Param)=Valu;	
						end
					
					end			
	
	
				end
		end	
	
		if DoThings %notify the wrapper if wanted
			notify(obj,'DrawIt');
		end
	
	end

	
	
	
	
	
	
	function GroupChecker(obj,objecthandle,WindowTag,Group,Param,DoThings)
		%This function is a specialized version of the ParameterChanger for checkerboxes
		%It allows to used checkerbox as switch for turning layers on and off and using a 
		%PopUpList or similar to select the plotted Parameter
		%The Value can be saved in the variable obj.AdditionalGUIParam.WindowTag.Group
		
		%get state the from object 
		 logstate=get(objecthandle,'Value');
		 	
		 if ~logstate
		 	obj.PlotParameter.(WindowTag).(Param)=0;
		 else
		 	obj.PlotParameter.(WindowTag).(Param)=obj.AdditionalGUIParam.Group;
		 end
		 
		 
		 
		if DoThings %notify the wrapper if wanted
			notify(obj,'DrawIt');
		end
	
	end
	
	
	
	
	
	
	
	function GroupLister_simply(obj,objecthandle,WindowTag,Group,DoThings)
		%This function is a specialized version of the ParameterChanger for Listboxes
		%It allows to used a Listbox as selector for the layers and using a 
		%PopUpList or similar to select the plotted Parameter
		%The Value can be saved in the variable obj.AdditionalGUIParam.WindowTag.Group
		
		%This is older simplified version of the grouptagger, which supports only values 
		%saved in the AdditionalGUIParam
		
		%get state the from object 
		selectedPosition=get(objecthandle,'Value');
		 
		%get layerlist
		LayerNames=obj.PlotParameter.(WindowTag).Layer_Names;	
		
		for i=1:numel(LayerNames)				 
		 	if any(i==selectedPosition)	 
		 		obj.PlotParameter.(WindowTag).(LayerNames{i})=obj.AdditionalGUIParam.Group;
			else
				obj.PlotParameter.(WindowTag).(LayerNames{i})=0;
			end
		end
		 
		 
		 
		if DoThings %notify the wrapper if wanted
			notify(obj,'DrawIt');
		end
	
	end

	function GroupLister(obj,objecthandle,WindowTag,Group,DoWhat,VariableList,DoThings)
		%This function is a specialized version of the ParameterChanger for Listboxes
		%It allows to used a Listbox as selector for different parameters 
		%PopUpList or similar to select the plotted Parameter and set them true or false 
		%or to a given value.
		%The Value can be saved in the variable obj.AdditionalGUIParam.WindowTag.Group
		%(Probably not offen needed, at least some parts of it)
		
		%VariableList is a list of entries in the PopUpList (can be saved anywhere) (?)
		
		%get state the from object 
		selectedPosition=get(objecthandle,'Value');
		
		switch DoWhat
			case 'logical'
				for i=1:numel(VariableList)				 
		 			if any(i==selectedPosition)	 
		 				obj.PlotParameter.(WindowTag).(VariableList{i})=true;
					else
						obj.PlotParameter.(WindowTag).(VariableList{i})=false;
					end
				end		 	
			
			case 'fixValue'
				for i=1:numel(VariableList)				 
		 			if any(i==selectedPosition)	 
		 				obj.PlotParameter.(WindowTag).(VariableList{i})=Group(1);
					else
						obj.PlotParameter.(WindowTag).(VariableList{i})=Group(2);
					end
				end
			
			case 'groupvalue'
				for i=1:numel(VariableList)				 
		 			if any(i==selectedPosition)	 
		 				obj.PlotParameter.(WindowTag).(VariableList{i})=obj.AdditionalGUIParam.Group;
					else
						obj.PlotParameter.(WindowTag).(VariableList{i})=0;
					end
				end	
		end 
		 
		if DoThings %notify the wrapper if wanted
			notify(obj,'DrawIt');
		end
	
	end

	
	
	
	
	function Valu = GetParameter(obj,Param,WindowTag)
		%This function returns the saved Parameter from the PlotParameterStructur
		
		Valu = obj.PlotParameter.(WindowTag).(Param);
		
	end
	
	function Valu = GetAddParam(obj,Param)
		%Same as GetPArameter but for the Additional ParameterStructur
		
		Valu = obj.AdditionalGUIParam.(Param);
		
	end

	
	
	function SimpleNotifier(obj)
		%Very simple function, it just notifies to wrapper, that it should start drawing
		%Can be used in case no internal plotting routine is needed and all the work should be done by the wrapper 
		notify(obj,'DrawIt');		
		
	
	end
	
	
	function Drawer(obj,WindowTag)
		% Generic Drawing function, hopefully this function can satisfy the most needs
		% WindowTag: 	Defines the PlotWindow wanted to be drawn. Alternativly the keyword 'allPlots' can be used
		% 				to plot every plot
		%
		% The Resultstructur normally consists of a structur of the following form: 
		% WindowTag: 	The name (Tag) of the plotwindow this tag belongs to, there are normaly 
		%				1-4 of those WindowTag each containing the following structur:
		%			 
		% 	Nr_Layers: 		The Number of Layers ihn the Structur
		% 	LegendToggle:	Sets the legend to either on or off in the according plotwindow
		%	Title:			Says it all
		% 	Layer_Names: 	The Name of the Layers as CellArray Bsp: {'data' 'coast' 'border'} 
		%					(also defined in the PlorParameters (needed for selection)) 
		% 	Layer 1: 		A plotable layer consisting of one or more plot elements. The Name of the Layer
		%					has to correspond to the Name mentioned under Layer_Names Bsp. ResultStruct.coast
		%	Layer 2:		All Layers can be plotted together, the order in which the are plotted correspends 
		% 	...				to the order there Name are mentioned in the Layer_Names entry, in the example the 
		% 	Layer x:		data would be plotted first then the coast then the border.
		%
		% 	Each layer consists of different plot elements, only one of the elements can be plotted in each layer,
		% 	which one is determined in the PlotParameter. Elements are arranged as a cellarray in the Layer 
		% 	Bsp: ResStructur.data={Element1 Element2 ...} 			
		%
		% 	A plot element is again a structur consisting mainly of the following components
		% 	PlotType: 	This set the different Type of plots and defines also the plotting function, the 
		%			functions themself and their syntax is known by the ResultViewerGUI
		% 	Name: 		The Name of the PlotElement, this name is also used for title and similar things like Legends
		% 	X_Axis_Label:	Sets the Label under the X-Axis, not always needed an can be empty
		% 	Y_Axis_Label:	Same for the Y_Axis
		% 	Z_Axis_Label:	Same for the Z_Axis, only needed in 3D plots 
		% 	C_Axis_Label: 	The Label for the Colorbar, needed only case of colormaps and the like.
		% 	X_Axis_Limit: 	Limits the range of the X-Axis, optional command not always needed
		% 	Y_Axis_Limit: 	Same for the Y-Axis
		% 	Z_Axis_Limit: 	Same for the Z-Axis, again only usable in 3D plots
		% 	C_Axis_Limit: 	Same for the Colorbar, works only in colormap type of plots
		% 	Colors:			This can either be used to set the color of lines and similar plot or to set the colormap
		%					colormap plots, the command is optional if not specified or missing the GUI will just self determine
		%					color of the plot
		% 	LegendText:		Optional command, allows to set an entry for the legend, does not work with every tyoe
		% 	ColorToggle:	Sets the Colorbar to on or off
		% 	Data:			The actuall data needed for the plot, can be anything 
		% 	Additional:		Other Parameters dependend on the select Type, can for instance be Quality of the plot in case
		%					of the normal region plot. More than one is possible and also the name is of the field is changing
		%					(see in the description of the different Types)
		
		%Types of plots (Keywords)
		%The generic plot routines
		%	'generic2D':	generic 2D line or point plot
		%	'log2D':		logarithmic 2D plot, similar to generic2D, both axis can be set to log
		%	'errorbar':		plots a line with errorbars	
		%	'histplot':	 	generic histogram plot
		%	'polar':		generic 2D polar plot
		%	'contour':		2D contour plot either filled or only lines is possible
		%	'vector2D':		A 2D quiver plot, arrows can be turned on or off
		%	'colorplot':	classic colormap plot, suitable for ploting colorcoded values in 2D
		%	'scatter':		A scatter plot, in this plot color and size of the points can be changed per point
		%
		%The specific plots (Keywords)
		%	'EQplot_old':	The classic erthquake plot of mapseis with an integrated coastline and borderline plot, and
		%					with internal quality selection. This function will be replaced with a more simply and more 
		%					modular function package
		%	'EQplot':		The new Earthquake plot function derived from the original mapseis plot function. This function plots
		%					only the earthquake with an additional parameter (Bsp. Magnitude). The parameter intevall and color 
		%					should be more flexible with this version (function to be written)
		%	'coast':		This function plots only the coastline, color and thickniss of the line can be changed know (function to be written)
		%	'border':		This function plots only the country border, color, thinkness and style of the line can be changed know. 
		%					(function to be written)
		%	'cdfplot':		This plots a cumulative density function of the inputed data, it is in way also generic plot, as every input will be 
		%					processed
		%	'fmdplot:		This function plots a "classic b-value" curve like in the mainwindow
		
		
		import mapseis.util.*;					  						
		disp('iDraw')
		if strcmp(WindowTag,'all') | strcmp(WindowTag,'allplots');
			%get handle for all plotwindows
			try
				plotnames=obj.PlotParameter.PlotNames;
			catch
				plotnames=fields(obj.PlotParameter);
			end
			
			
			
			
			
			figure(obj.TheGUI)
			%for i=1:numel(plotnames)
				%obj.PointMaker('update');
			%	plotAxis(i) = findobj(obj.TheGUI,'Tag',plotnames{i});
				
			%end
		else
			plotnames{1}=WindowTag % done, to avoid a if in the next for-loop
			plotAxis = findobj(obj.TheGUI,'Tag',WindowTag);
		
		end
		
		
		%Update Points
		
		
		%get the configuration and the results from the wrapper
		ResultStructur=obj.Wrapper.PlotWriter(obj.PlotParameter);
		
		%Add the points to the ResultStructur
		if ~isempty(obj.PointStructur)
			%Update Points
			visio=get(obj.TheGUI,'visible')
			if ~isempty(obj.point_objects) & strcmp(visio,'on')
				obj.PointMaker('refresh')
			end
			ResultStructur=obj.Reunit(ResultStructur);
			
		end
		
		%disp(ResultStructur.TopPlot)
		
		
		
		%now do the work, if plotAxis has only one element the loop will be run just once as it should
		for i=1:numel(plotnames)
			plotAxis=findobj(obj.TheGUI,'Tag',plotnames{i});
			
			%really needed the axes command
			axes(plotAxis)
			
			CurrentStructur=ResultStructur.(plotnames{i});
			%disp(plotnames{i});
			CurrentPlotParameter=obj.PlotParameter.(plotnames{i});
			%disp(obj.PlotParameter.TopPlot)
			%get the name of th layers in the structur
			LayerNames=CurrentStructur.Layer_Names;
			
			
			%Reset all entries of the Window
			obj.point_objects.(plotnames{i})={};
			obj.imroi_objects.(plotnames{i})={};
			obj.handles.(plotnames{i})={};
			obj.legendentries.(plotnames{i})={};
		
			%Sometimes the tag goes missing, retag if needed
			WindowTag=get(plotAxis,'Tag');
			if isempty(WindowTag)
				set(plotAxis,'Tag',plotnames{i});			
			end		
			
			%clear the current figure
			%clf(plotAxis);
			
			
			
				for j=1:CurrentStructur.Nr_Layers
					%go through all layer and decide what is to draw
					try
						WhichPart=CurrentPlotParameter.(LayerNames{j});
					catch
						WhichPart='undefined';
						disp(['The Layer ',LayerNames{j}, ' is missing in the PlotParameters']); 
					end	
					
					if j==2
						%set plotwindow on hold
						hold on;
					end
					
					if isstr(WhichPart) & ~strcmp(WhichPart,'undefined')
						try
							WhichPart=eval(WhichPart);
						catch
							WhichPart='undefined'
							disp(['Evalulation of the PlotParameter did not work']); 
						end	
					end
					
					
					%if it is logical just us the first element
					if islogical(WhichPart)
						if WhichPart
							CurrentElement=CurrentStructur.(LayerNames{j}){1};
							%call the Drawer for the Element
							[handle legendentry] = DrawElement(obj,CurrentElement,plotAxis);
							
							%write to legendentryand handle
							obj.legendentries.(plotnames{i}){end+1} = legendentry;
							obj.handles.(plotnames{i}){end+1} =handle;
							disp(obj.legendentries.(plotnames{i}))
							
						else
							continue %skip this loop
						end	
					
					elseif isnumeric(WhichPart) 
						%The normal case WhichPart is a number, every other case will be ignore
						if WhichPart ~= 0

							CurrentElement=CurrentStructur.(LayerNames{j}){WhichPart};	
							%call the Drawer for the Element
							[handle legendentry] = DrawElement(obj,CurrentElement,plotAxis);
														
							%write to legendentryand handle
							obj.legendentries.(plotnames{i}){end+1} = legendentry;
							obj.handles.(plotnames{i}){end+1} = handle;
							disp(obj.legendentries.(plotnames{i}))
						else
							continue %skip the loop
						end			
					
					end
					
					

				end
		
			%set figure to hold off
			hold off;
			
			%set the plot window to basic configurations
			set(plotAxis, ...
	  			'Box'         , 'off'     , ...
	  			'TickDir'     , 'out'     , ...
	  			'TickLength'  , [.02 .02] , ...
	  			'XMinorTick'  , 'on'      , ...
	  			'YMinorTick'  , 'on'      , ...
	  			'YGrid'       , 'off'      , ...
	  			'XGrid'       , 'off'      , ...
	  			'XColor'      , [.3 .3 .3], ...
	  			'YColor'      , [.3 .3 .3], ...
	  			'LineWidth'   , 1         );	
			
			%define title if existing
			if ~isempty(CurrentStructur.Title)
				hTitle=title(CurrentStructur.Title);
				set( hTitle, 	'FontName'   , 'AvantGarde',...
								'FontSize', 12 , ...
		                  		'FontWeight' , 'bold'      );
			end		
			
			%PostProcessing for the whole subplot
			if isfield(CurrentStructur,'PostProcess')
				if ~isempty(CurrentStructur.PostProcess)
					CurrentStructur.PostProcess(plotAxis,ResStruct);
				end
			end
						
			
			
			% exclude all empty entries
			noentry=emptier(obj.legendentries.(plotnames{i}));
			disp('-----------')
			
			handd=obj.handles.(plotnames{i});
			entr=obj.legendentries.(plotnames{i});
			disp(plotnames{i})
			disp(handd)
			disp(entr)
			disp(noentry)
			disp(plotAxis)
			disp(ishandle(plotAxis))
			disp('-----------')
			if ~all(noentry) & ~isempty(noentry)
				disp('========')
				disp([handd{~noentry}])
				disp(entr(~noentry))
				disp('========')
				needHand=[handd{~noentry}]
				needEntr=entr(~noentry)
				try
				legend(plotAxis,needHand,needEntr,'hide');
				end
			end
			
			%build the legend if wanted
			if isfield(CurrentStructur,'LegendToogle')
				if ~isempty(CurrentStructur.LegendToogle) & CurrentStructur.LegendToogle
					
					legend(plotAxis,'show');
					
				end
			end
			
			%reset values
			handd={};
			entr={};
			noentry=[];
			

				
		end
	   
		%Notify all listeners that the windows are finished, this allows to ensure 
		%that certain commands are only executed when the gui is finished, this event
		%is also called after every plot update.		
		notify(obj,'Finished');
	
	

	end
	
	
	function [handle legendentry] = DrawElement(obj,Element,plotAxis)
		%This function actual draws the the Element of a layer, could have been integrated
		%into the drawer but making a new function out of it makes the code more readable.
		%For the Elementsyntax see the comments in the function Drawer
		
		import mapseis.plot.*;
		
		%Sometimes tags get lost,
		CurrentTag=get(plotAxis,'Tag');
		
		disp(Element)
		switch Element.PlotType
				case 'generic2D'
					[handle legendentry] = PlotGeneric2D(plotAxis,Element);
				case 'log2D'
					[handle legendentry] = PlotLog2D(plotAxis,Element);				
				case 'errorbar'
					[handle legendentry] = PlotErrorbar(plotAxis,Element);
				case 'histplot'
					[handle legendentry] = PlotHistogram(plotAxis,Element);
				case 'polar'
					[handle legendentry] = PlotPolar(plotAxis,Element);
				case 'contour'
					[handle legendentry] = PlotContour(plotAxis,Element);
				case 'vector2D'
					[handle legendentry] = PlotVector2D(plotAxis,Element);
				case 'colorplot'
					[handle legendentry] = PlotColor(plotAxis,Element);
				case 'scatter'
					[handle legendentry] = PlotScatter(plotAxis,Element);
				case 'EQplot_old'
					%too be written
				case 'EQplot'
					[handle legendentry] = PlotEarthquake(plotAxis,Element);
				case 'coast'
					[handle legendentry] = PlotCoastline(plotAxis,Element);
				case 'border'
					[handle legendentry] = PlotBorder(plotAxis,Element);
				case 'cdfplot'
					[handle legendentry] = PlotCDF2(plotAxis,Element);
				case 'fmdplot'
					[handle legendentry] = PlotFMD2(plotAxis,Element);
				case 'timeplot'
					[handle legendentry] = PlotTime2(plotAxis,Element);
				case 'textfield'
					[handle legendentry] = PlotText(plotAxis,Element);
				case 'polygon'
					[handle legendentry] = PlotPolygon(plotAxis,Element);
					if ~Element.Plain
				 		obj.imroi_objects.(CurrentTag){end+1}=handle;
				 		handle=[];
				 		legendentry=[];
				 	end
				 	
				 	
				case 'point'
					[handle legendentry] = PlotPoint(plotAxis,Element);
					obj.point_objects.(CurrentTag){end+1}=handle;	
					handle=[];
					legendentry=[];
					
		end				
		
		%PostProcessing for the layer, handle and legendentry are also sended here.
		if isfield(Element,'PostProcess')
			if ~isempty(Element.PostProcess)
				Element.PostProcess(plotAxis,Element,handle,legendentry);
			end
		end
		
		%so the code has to retag them
		set(plotAxis,'Tag',CurrentTag);
		
	
	end	
	
	
	
	function PointCallbackFunc(obj,WindowTag,PointTag,AutoFire)
		%This function is called by every Point, instead of the Wrappers 
		%function, it allows to select between directly send the values to the wrapper
		%or to send not until a certain event happens (button press or similar)
		%Also does the function updates the interal parameter of the points.
		
		%find point
		for i=1:numel(obj.point_objects.(WindowTag))
			try
				TagList{i}=get(obj.point_objects.(WindowTag){i},'Tag');
			catch
				TagList{i}=[];
			end	
		end	
		
		FoundPoint=strcmp(TagList,PointTag);
		%disp(TagList)
		%only do something if the point is found, points can went missing
		if any(FoundPoint)
			ThePoint=obj.point_objects.(WindowTag){FoundPoint};
			
			%get Position
			ThePosition=ThePoint.getPosition;
			%disp(ThePoint)
			%write to PointConfig
			obj.PointConfig.(PointTag).Position=ThePosition;
			
			
			%decide if values should be send
			if AutoFire
				WhichCoord=obj.PointConfig.(PointTag).WhichCoord;
				FieldTag=obj.PointConfig.(PointTag).EntryFieldTag;
				CalcKey=obj.PointConfig.(PointTag).CalculationKey;
				Parameter=obj.PointConfig.(PointTag).Parameter;
				
				obj.Wrapper.ChangeParameterPoint('ResultGUI',ThePoint,WindowTag,...
				WhichCoord,FieldTag,CalcKey,Parameter);
			end
		end
	
	end
	
	
	
	function PointFunctionAdder(obj,plotAxis,Element,handle,legendentry,PointTag,AutoFire)
		%This function will be written into to the postprocessing of the point, it 
		%sets the Callback of the point to the PointCallbackFunc.
		
		%plotAxis,Element,handle,legendentry are not really needed but need to be 
		%added to use the function in the postprocessing.
		
		WindowTag=get(plotAxis,'Tag');
		
		%find point
		for i=1:numel(obj.point_objects.(WindowTag))
			TagList{i}=get(obj.point_objects.(WindowTag){i},'Tag');
		end	
		
		FoundPoint=strcmp(TagList,PointTag);
		ThePoint=obj.point_objects.(WindowTag){FoundPoint};
		
		%set Callback
		ThePoint.addNewPositionCallback(@(s,e) obj.PointCallbackFunc(WindowTag,PointTag,AutoFire));
		
		
	end
	
	function CombiStruct=Reunit(obj,ResultStruct)
		%This function combines the PointStructur with a given ResultStructur
		disp('Reunion')
		Points=obj.PointStructur;
		
		CombiStruct=ResultStruct;
		
		Fields=fieldnames(Points);
		
		%go through all the Layers and add them.
		for i=1:numel(Fields)
			WinTag=Points.(Fields{i}).WindowTag;
			CombiStruct.(WinTag).(Fields{i})={Points.(Fields{i})};
			CombiStruct.(WinTag).Layer_Names=[CombiStruct.(WinTag).Layer_Names,Fields(i)];
			CombiStruct.(WinTag).Nr_Layers=numel(CombiStruct.(WinTag).Layer_Names);
		end
		
		disp(CombiStruct.TopPlot)
	end	
	
	
	%The functions of the ToolbarModuls form CalcParamGUI
	%====================================================
	%====================================================	

	
	function CreatePopUp(obj,DataSlide,currentposition,EntrySize);
		%EntrySize give the size of the slot, this needed because size can be different depending on the preset
		%needed input for the PopUp:
		%Name
		%Tag
		%Callback, this can either be a function handle or a string wiht either 'Param' or 'AddParam'
		%WindowTag, needed if Param or AddParam
		%Param, needed if Param or AddParam
		%ValueType, needed if Param or AddParam
		%DoThings, needed if Param or AddParam
		%String with entries
		%EvalMode, see ParameterChanger
		
		if ~isstr(DataSlide.Callback)
		
			obj.ModulList{currentposition} = uicontrol(...
											'Parent',obj.TheGUI,...
											'Units','Pixels',...
											'Callback',@(s,e) DataSlide.Callback,...
											'Position',EntrySize,...
											'String',DataSlide.Name,...
											'Style','popupmenu',...
											'Tag',DataSlide.Tag);
		
		else
			
			if strcmp(DataSlide.Callback,'AddParam')
				%Add Empty Array to send to the handle2value function
				DataSlide.WindowTag=[];
			end
			
			if isfield(DataSlide,'EvalMode')
				EvalMode=DataSlide.EvalMode;
			else
				EvalMode=false;
			end
			if isempty(EvalMode)
				EvalMode=false;
			end	
			
			obj.ModulList{currentposition} = uicontrol(...
											'Parent',obj.TheGUI,...
											'Units','Pixels',...
											'Callback',@(s,e) obj.PopUp2value(s,DataSlide.Callback,...
											DataSlide.WindowTag,DataSlide.Param,DataSlide.ValueType,'set',DataSlide.DoThings,EvalMode),...
											'Position',EntrySize,...
											'String',DataSlide.Name,...
											'Style','popupmenu',...
											'Tag',DataSlide.Tag);

		
			
		end			
		set(obj.ModulList{currentposition}, 'String' , DataSlide.EntryStrings);
		
		%init
		if isstr(DataSlide.Callback)
			%Allows to use a init value in case the value is not set a creation time
			if isfield(DataSlide,'InitValu')
				obj.initValue(DataSlide.Valu,DataSlide.InitValu,DataSlide.ValueType);
			end
			
		 	obj.PopUp2value(obj.ModulList{currentposition},DataSlide.Callback,DataSlide.WindowTag,...
		 		DataSlide.Param,DataSlide.ValueType,'set',DataSlide.DoThings,EvalMode);
		end
		
	
	end
	
	
	
	
	function CreateListBox(obj,DataSlide,currentposition,EntrySize);
		%needed input for the PopUp:
		%Name
		%Tag
		%How Many Selection allowed
		%Callback, this can either be a function handle or a string wiht either 'Param', 'AddParam' or 'MultiParam'
		%WindowTag, needed if Param or AddParam
		%Param, needed if Param or AddParam
		%ValueType, needed if Param or AddParam
		%DoThings, needed if Param or AddParam
		%Valu, needed in case of MultiParam, this value can either be from internal Additional Parameter or a fixed value 
		%String with entries
		%'MultiParam' treats the selected strings as entries in the structur and sets them to defined value valu, this is
		%handy in case of a layer on-off selector
		%EvalMode, see ParameterChanger
		
		if isfield(DataSlide,'EvalMode')
				EvalMode=DataSlide.EvalMode;
			else
				EvalMode=false;
			end
			if isempty(EvalMode)
				EvalMode=false;
			end	
		
		
		
		if ~isstr(DataSlide.Callback)
			obj.ModulList{currentposition} = uicontrol(...
											'Parent',obj.TheGUI,...
											'Units','Pixels',...
											'Callback',@(s,e) DataSlide.Callback,...
											'Position',EntrySize,...
											'String',DataSlide.Name,...
											'Style','listbox',...
											'Tag',DataSlide.Tag);
		
		
		else
			switch DataSlide.Callback
				case 'Param'
					Selector=false;
					DataSlide.Valu=[];
					WhichMemory='Param';
					%initValue(obj,DataSlide.EntryStrings(1),whichMemory, Selector,DataSlide.WindowTag,...
					%DataSlide.Param,DataSlide.ValueType, [],'set', false);
					
				case 'AddParam'
					Selector=false;
					DataSlide.WindowTag=[];
					DataSlide.Valu=[];
					WhichMemory='AddParam';
				
					%initValue(obj,DataSlide.EntryStrings(1),whichMemory, Selector,DataSlide.WindowTag,...
					%DataSlide.Param,DataSlide.ValueType, [],'set', false);
				
				case 'MultiParam'
					Selector=true;
					WhichMemory='Param';
					
					%initValue(obj,DataSlide.EntryStrings,whichMemory, Selector,DataSlide.WindowTag,...
					%DataSlide.Param,DataSlide.ValueType, 0,'set', false);
					
					%initValue(obj,DataSlide.EntryStrings(1),whichMemory, Selector,DataSlide.WindowTag,...
					%DataSlide.Param,DataSlide.ValueType, DataSlide.Valu,'set', false);
					
			end
			

			
			obj.ModulList{currentposition} = uicontrol(...
											'Parent',obj.TheGUI,...
											'Units','Pixels',...
											'Callback',@(s,e) obj.ListBox2value(s,WhichMemory,Selector,...
											DataSlide.WindowTag,DataSlide.Param,DataSlide.ValueType,DataSlide.Valu,'set',DataSlide.DoThings,EvalMode),...
											'Position',EntrySize,...
											'String',DataSlide.Name,...
											'Style','listbox',...
											'Tag',DataSlide.Tag);


			
		end	
			
		set(obj.ModulList{currentposition}, 'String' , DataSlide.EntryStrings);
		
		% Allow multiple selections
		set(obj.ModulList{currentposition}, 'min', 0, 'max', DataSlide.maxSelection);	
		
		%init
		if isstr(DataSlide.Callback)
			%Allows to use a init value in case the value is not set a creation time
					if isfield(DataSlide,'InitValu')
						obj.initValue(DataSlide.Valu,DataSlide.InitValu,DataSlide.ValueType);

					end
		
			obj.ListBox2value(obj.ModulList{currentposition},WhichMemory,Selector,DataSlide.WindowTag,...
				DataSlide.Param,DataSlide.ValueType,DataSlide.Valu,'set',DataSlide.DoThings,EvalMode);
		end
	end

	
	
	
	
	
	function CreateButton(obj,DataSlide,currentposition,EntrySize,PosDirect);
		%just make the buttons so big the still fit 
		%Callback, this can either be a function handle or a string wiht either a cell array with 'Param' or 'AddParam'), 
		%		   'draw' for a button to update the plots
		%ChangeMode: At the moment only Toggle and LogToggle possible (cell array)
		%WindowTag, needed if Param or AddParam (cell array)
		%Param, needed if Param or AddParam (cell array)
		%Valu, Needed in the case of Toggle, has to be numeric (array
		%DoThings, needed if Param or AddParam (cell array)
		
		%PosDirect, decides in which direction the button should be alinght
		%EvalMode, see ParameterChanger
		

		
		switch PosDirect
			case 'Vertical'
				buttonwidth = (obj.wantedModulWide - (DataSlide.ElementNumber-1)*2)/DataSlide.ElementNumber;
				EntryPosition=[EntrySize(1)+5,EntrySize(2) buttonwidth, obj.wantedModulHeight-4];
			
			case 'Horizontal'
				buttonheight = (obj.wantedModulHeight - (DataSlide.ElementNumber-1)*2)/DataSlide.ElementNumber;
				EntryPosition=[EntrySize(1),EntrySize(2)+5, obj.wantedModulWide-4, buttonheight];
		end
		
		Buttons=[];
		
		if isfield(DataSlide,'EvalMode')
				EvalMode=DataSlide.EvalMode;
			else
				EvalMode=false;
			end
			if isempty(EvalMode)
				EvalMode=false;
			end	
		
		
		for i=1:DataSlide.ElementNumber
				
		   if ~isstr(DataSlide.Callback{i})
		   	newbutton   = uicontrol('Parent',obj.TheGUI, ...
						'Units', 'Pixels',...
	                			'Position',EntryPosition,...
	                			'String',DataSlide.Name{i},...
						'Style','pushbutton',...
	              				'Tag',DataSlide.Tag{i},...
	              				'Callback',@(s,e) DataSlide.Callback{i});
	            
	           elseif ~strcmp(DataSlide.Callback{i},'draw')
	             	
	             	
	             	
	             	
	           	  	if strcmp(DataSlide.Callback{i},'AddParam')
	             			DataSlide.WindowTag{i}=[]
	             		end
	           		
	           		%Not needed anymore
	           		%if strcmp(DataSlide.ChangeMode{i},'LogToggle')
	           		%	DataSlide.Valu[i]=[];
	           		%	ValueType='logical'
	           		%end	  				
	            	
	           		newbutton   = uicontrol('Parent',obj.TheGUI,...
						'Units','Pixels',...
						'Callback',@(s,e) obj.Button2value(DataSlide.Callback{i},DataSlide.ChangeMode{i},...
							DataSlide.WindowTag{i},DataSlide.Param{i},DataSlide.ValueType{i},DataSlide.Valu{i},DataSlide.DoThings,EvalMode),...
						'Position',EntrySize,...
						'String',DataSlide.Name{i},...
						'Style','pushbutton',...
						'Tag',DataSlide.Tag{i});
				
						
				
				%Allows to use a init value in case the value is not set a creation time
				if isfield(DataSlide,'InitValu')
					obj.initValue(DataSlide.Valu{i},DataSlide.InitValu{i},DataSlide.ValueType{i});
				end
						
						
				obj.Button2value(DataSlide.Callback{i},DataSlide.ChangeMode{i},...
							DataSlide.WindowTag{i},DataSlide.Param{i},DataSlide.ValueType{i},DataSlide.Valu{i},false,EvalMode)		

			 
					
						
		    else
					
				if isempty(DataSlide.WindowTag{i})
					DataSlide.WindowTag{i}='allPlots'						
				end
	              	
				newbutton   = uicontrol('Parent',obj.TheGUI,...
						'Units','Pixels',...
						'Callback',@(s,e) obj.Drawer(DataSlide.WindowTag{i}),...
						'Position',EntrySize,...
						'String',DataSlide.Name{i},...
						'Style','pushbutton',...
						'Tag',DataSlide.Tag{i});
	              	
	              	
	              			  				
		    end
				
				
				
				%update position
				switch PosDirect
					case 'Vertical'
						EntryPosition=[EntrySize(1)+buttonwidth+2,EntrySize(2) buttonwidth, obj.wantedModulHeight-4];
					
					case 'Horizontal'
						EntryPosition=[EntrySize(1),EntrySize(2)+buttonheigth+2, obj.wantedModulWide-4, buttonheight];
				end
				
				
				
				Buttons=[Buttons,newbutton];
				
		end		
		
		
		
		
		
		
		obj.ModulList{currentposition} = Buttons;
		
	
	end
	
	
	
	
	
	
	
	function CreateTexter(obj,DataSlide,currentposition,EntrySize);
		obj.ModulList{currentposition} = uicontrol(...
										'Parent',obj.TheGUI,...
										'Units','Pixels',...
										'Position',EntrySize,...
										'String',DataSlide.EntryStrings,...
										'Tag',DataSlide.Tag,...
										'Style','text');

		

	end
	
	
	
	
	function CreateCheckerbox(obj,DataSlide,currentposition,EntrySize,PosDirect);
		%just make the buttons so big the still fit 
		%Callback, this can either be a function handle or a string wiht either a cell array with 'Param' or 'AddParam'
		%ChangeMode: only Toggle and LogToggle possible (cell array)
		%WindowTag, needed if Param or AddParam (cell array)
		%Param, needed if Param or AddParam (cell array)
		%Valu, Needed in the case of Toggle, has to be numeric (array
		%DoThings, needed if Param or AddParam (cell array)
		%EvalMode, see ParameterChanger
		
		%PosDirect, decides in which direction the button should be alinght
		
		

		
		switch PosDirect
			case 'Vertical'
				buttonwidth = (obj.wantedModulWide - (DataSlide.ElementNumber-1)*2)/DataSlide.ElementNumber;
				EntryPosition=[EntrySize(1)+5,EntrySize(2) buttonwidth, obj.wantedModulHeight-4];
			
			case 'Horizontal'
				buttonheight = (obj.wantedModulHeight - (DataSlide.ElementNumber-1)*2)/DataSlide.ElementNumber;
				EntryPosition=[EntrySize(1),EntrySize(2)+5, obj.wantedModulWide-4, buttonheight];
		end
		
		
		if isfield(DataSlide,'EvalMode')
				EvalMode=DataSlide.EvalMode;
			else
				EvalMode=false;
			end
			if isempty(EvalMode)
				EvalMode=false;
			end	
		
		
		Checkers=[];
		
		for i=1:DataSlide.ElementNumber
		
				if ~isstr(DataSlide.Callback{i})
					newCheck   = uicontrol('Parent',obj.TheGUI,...
											'Units', 'Pixels',...
	                		  				'Position',EntryPosition,...
	                		 				'String',DataSlide.Name{i},...
	                		 				'Style','checkbox',...
	              			  				'Tag',DataSlide.Tag{i},...
	              			  				'Callback',@(s,e) DataSlide.Callback{i});
				else
					ValueType='number'
	             	
					if strcmp(DataSlide.Callback{i},'AddParam')
						DataSlide.WindowTag{i}=[]
					end
	           		
					if strcmp(DataSlide.ChangeMode{i},'LogToggle')
	           				DataSlide.Valu(i)=[];
	           				ValueType='logical'
	           			end	  				
	            	
	           			newCheck   = uicontrol('Parent',obj.TheGUI,...
								'Units','Pixels',...
								'Callback',@(s,e) obj.Check2value(s,DataSlide.Callback{i},DataSlide.ChangeMode{i},...
									DataSlide.WindowTag{i},DataSlide.Param{i},ValueType,DataSlide.Valu{i},DataSlide.DoThings,EvalMode),...
								'Position',EntrySize,...
								'String',DataSlide.Name{i},...
								'Style','checkbox',...
								'Tag',DataSlide.Tag{i});
					
					%Allows to use a init value in case the value is not set a creation time
					if isfield(DataSlide,'InitValu')
						obj.initValue(DataSlide.Valu{i},DataSlide.InitValu{i},DataSlide.ValueType{i});
					else
						InValu=DataSlide.Valu{i};
					end
					
					obj.Check2value(newCheck,DataSlide.Callback{i},DataSlide.ChangeMode{i},...
									DataSlide.WindowTag{i},DataSlide.Param{i},DataSlide.ValueType{i},DataSlide.Valu{i},DataSlide.DoThings,EvalMode);			
				
				end
				
				
				%update position
				switch PosDirect
					case 'Vertical'
						EntryPosition=[EntrySize(1)+buttonwidth+2,EntrySize(2) buttonwidth, obj.wantedModulHeight-4];
					
					case 'Horizontal'
						EntryPosition=[EntrySize(1),EntrySize(2)+buttonheigth+2, obj.wantedModulWide-4, buttonheight];
				end
				

				Checkers=[Checkers,newCheck];
				
		end		
		obj.ModulList{currentposition} = Checkers;
		
	
	end
	
	
	
		
	
	function CreateEntryfield(obj,DataSlide,currentposition,EntrySize);	
		%Callback, this can either be a function handle or a string wiht either a cell array with 'Param' or 'AddParam'
		%WindowTag, needed if Param or AddParam 
		%Param, needed if Param or AddParam 
		%Valu, 
		%DoThings, needed if Param or AddParam 

		
		if ~isstr(DataSlide.Callback)
			obj.ModulList{currentposition} = uicontrol(...
								'Parent',obj.TheGUI,...
								'Units','Pixels',...
								'Callback',@(s,e) DataSlide.Callback,...
								'Position',EntrySize,...
								'String',DataSlide.EntryStrings,...
								'Tag',DataSlide.Tag,...
								'Style','edit');
	
		else
			
			switch DataSlide.Callback
				case 'Param'
					WhichMemory='Param';
					
				case 'AddParam'
					DataSlide.WindowTag=[];
					WhichMemory='AddParam';
			end
			
			obj.ModulList{currentposition} = uicontrol(...
								'Parent',obj.TheGUI,...
								'Units','Pixels',...
								'Callback',@(s,e) obj.Text2value(s,WhichMemory,...
									DataSlide.WindowTag,DataSlide.Param,DataSlide.ValueType,DataSlide.ChangeMode,DataSlide.DoThings),...
								'Position',EntrySize,...
								'String',DataSlide.Name,...
								'Style','edit',...
								'Tag',DataSlide.Tag);
		
			obj.Text2value(obj.ModulList{currentposition},WhichMemory,DataSlide.WindowTag,DataSlide.Param,DataSlide.ValueType,DataSlide.ChangeMode,DataSlide.DoThings);						
		end
	
	end
	
	
	
	function PopUp2value(obj,obhandle,whichMemory,WindowTag, Param, ValueType, ValMod, DoThings,EvalMode)
		%Simple function, all it does is using the handle and get the string from the popup 
		%uicontrol element and send it to either ParameterChanger or AddParamChanger (selelcted with 
		%whichMemory
		if nargin<9
			EvalMode=false;
		end
		
		 SelectedValues=get(obhandle,'Value');
		 TheEntries=get(obhandle,'String');
		 
		 TheString=TheEntries(SelectedValues);
		 
		 switch whichMemory
		 	case 'Param'
		 		obj.ParameterChanger(WindowTag,Param,ValueType,TheString,'set',DoThings,EvalMode);	
		 	case 'AddParam'
		 		obj.AdditionalParaChanger(Param,ValueType,TheString,'set',DoThings);
		 end	
	
	end
	
	
	
	function ListBox2value(obj,obhandle,whichMemory,Selector,WindowTag, Param, ValueType, Valu, ValMod, DoThings,EvalMode)
		%Simple function, all it does is using the handle and get the string from the listbox 
		%uicontrol element and send it to either ParameterChanger or AddParamChanger (selelcted with 
		%whichMemory
		%Selector decides if the Strings are taken as Param (true) or if the if the Strings are taken as Value (false)
		%in case of the true the the Strings have to match the LayerNames of the Plot and the spaces in the string are
		%(at the moment) not allowed. Also the parameter 'Valu' has to be defined. This works only in The Param setting
		% (does not make sense in 'AddParam' setting 				
		
		 if nargin<11
		 	EvalMode=false
		 end
		 
		 SelectedValues=get(obhandle,'Value');
		 TheEntries=get(obhandle,'String');
		 
		 if isstr(TheEntries)
		 	TheEntries={TheEntries};
		 end
		 
		 TheStrings=TheEntries(SelectedValues);
		 
		 if isstr(TheStrings)
		 	TheStrings={TheStrings};
		 end

		 
		 switch whichMemory
		 	case 'Param'
		 		if ~Selector
		 			for i=1:numel(TheStrings)
		 				obj.ParameterChanger(WindowTag,Param,ValueType,TheStrings{i},'set',DoThings,EvalMode);
		 			end	
		 		else
		 			if ~EvalMode
						%set all to 0 (deselect
						for i=1:numel(TheEntries)
							obj.ParameterChanger(WindowTag,TheEntries{i},ValueType,0,'set',DoThings);
						end	
	
						%set to value
						for i=1:numel(TheStrings)
							obj.ParameterChanger(WindowTag,TheStrings{i},ValueType,Valu,'set',DoThings);
						end	
					else
						%set to value
						for i=1:numel(TheStrings)
							obj.ParameterChanger(WindowTag,TheStrings{i},ValueType,Valu,'set',DoThings,EvalMode);
						end

					end	
		 		end
		 		
		 	case 'AddParam'
		 		for i=1:numel(TheStrings)
		 			obj.AdditionalParaChanger(Param,ValueType,TheStrings{i},'set',DoThings);
		 		end
		 end	
	
	end
	
	
	function initValue(obj,EntryValue,InitValue,ValueType)
		%The function checks if EntryValue is existing and try to set it if possible
		%only possible if it is a string 
		
		%convert the InitValue into the matching type
		if isnumeric(InitValue)
			InValu=num2str(InitValue);
		elseif islogical(InitValue)
			if InitValue
				InValu='1';
			else
				InValu='0';
			end
		else
			InValu=InValue;
		end	
		
		if isstr(EntryValue) & ~strcmp(ValueType,'string')
			%try to convert to number
			rawValu=str2num(EntryValue);
			
			%if empty try to eval
			if isempty(rawValu)
				try
					rawValu=eval(EntryValue);
				catch
					%write the InitValue into the position the string is giving
					eval([EntryValue,'=',InValu]);
				end	
			end

		
		end
	end
	
	function GeneralInit(obj,ToInit)
		%In a way similar to initValue, but it is done for a whole set and does set values only if they are not existing
		%ValueString: 	defines the variable as a string e.g.: 'obj.AddParam.MyVar', this string will be evaluted, if it fails
		%		it will be set with the parameter set in InitValue
		%InitValue: defines the value which will be written to the string in case the location does not exist
		
		
		
		for i=1:numel(ToInit.ValueString)
			try 
				eval(['temp=',ToInit.ValueString{i}]);
			catch		
				%Convert to string if needed
				InValu=ToInit.InitValue{i};
				if ~isstr(InValu)
					InValu=num2str(InValu);
				end	
				
				%write value
				eval([ToInit.ValueString{i},'=',InValu]);
			end	
			
			
		end
		
		
	end

	
	function Button2value(obj,ChangeMode,whichMemory,WindowTag, Param, ValueType, Valu,ValMod, DoThings,EvalMode)
		%Simple function, all it does is using the handle and get the string from the popup 
		%uicontrol element and send it to either ParameterChanger or AddParamChanger (selelcted with 
		%whichMemory
		%ChangeMode is at the moment not needed, but added, because it will be needed later with more
		%functionality
		
		if nargin<10
			EvalMode=false;
		end	
	
		%%%%%Not needed anymore replace in the Parameter functions
		%		 switch ValueType
		%		 	case 'number'
		%		 		%get value and compare
		%		 		switch whichMemory
		%		 			case 'Param'
		%		 				oldValue=obj.getParameter(Param,WindowTag);
		%		 			case 'AddParam'
		%		 				oldValue=obj.getAddParam(Param);
		%		 		end
		%		 		
		%		 		if oldValue==Valu
		%		 			Valu=0
		%		 		end
		%		 		
		%		 		work2Do='set';
		%		 			
		%		 	case 'logical'
		%		 		Valu=false;
		%		 		
		%		 		work2Do='toggle'
		%		 		
		%		 		
		%		 end	
		%%%%%%			
			
		 %work2Do='toggle'
		 work2Do=ChangeMode;
		 	
		 switch whichMemory
		 	case 'Param'
		 		obj.ParameterChanger(WindowTag,Param,ValueType,Valu,work2Do,DoThings,EvalMode);	
		 	case 'AddParam'
		 		obj.AdditionalParaChanger(Param,ValueType,Valu,work2Do,DoThings);
		 end	
	
	end

	
	
	
	function Check2value(obj,obhandle,ChangeMode,whichMemory,WindowTag, Param, ValueType, Valu,ValMod, DoThings,EvalMode)
		%Simple function, all it does is using the handle and get the string from the popup 
		%uicontrol element and send it to either ParameterChanger or AddParamChanger (selelcted with 
		%whichMemory
		%ChangeMode is at the moment not needed, but added, because it will be needed later with more
		%functionality
		
		if nargin<11
			EvalMode=false;
		end	
		
		 SelectedValues=get(obhandle,'Value');

			
		 switch ValueType
		 	case 'number'
				setVal=0;
				
				if SelectedValues
				 	setVal=Valu;
				end	
		 		
		 		work2Do='set';
		 			
		 	case 'logical'
		 		setVal=false;
		 		
		 		if SelectedValue
		 			setVal=true;
		 		end	
		 		
		 		work2Do='set'
		 		
		 		
		 end	
		
			
		 	
		 switch whichMemory
		 	case 'Param'
		 		obj.ParameterChanger(WindowTag,Param,ValueType,setVal,work2Do,DoThings,EvalMode);	
		 	case 'AddParam'
		 		obj.AdditionalParaChanger(Param,ValueType,setVal,work2Do,DoThings);
		 end	
	
	end

	
	
	
	
	function text2value(obj,obhandle,whichMemory,WindowTag, Param, ValueType,ChangeMode, ValMod, DoThings)
		%Simple function, all it does is using the handle and get the string from the popup 
		%uicontrol element and send it to either ParameterChanger or AddParamChanger (selelcted with 
		%whichMemory
		

		 
		 TheString=get(obhandle,'String');
		 
		 switch whichMemory
		 	case 'Param'
		 		obj.ParameterChanger(WindowTag,Param,ValueType,TheString,ChangeMode,DoThings);	
		 	case 'AddParam'
		 		obj.AdditionalParaChanger(Param,ValueType,TheString,ChangeMode,DoThings);
		 end	
	
	end

	
	function MenuPoint2value(obj,obhandle,whichMemory,Checker,WindowTag, Param, ValueType, Valu, ValMod, DoThings, EvalMode);
		%Simple function, all it does is set the specified Param to value or 0 if 'toogle' is used
		%and send it to either ParameterChanger or AddParamChanger (selelcted with whichMemory)
		%Checker: 	If set to true, the function will check the menupoint if activated (works only with 'toggle'
		%WindowTag: Only needed for 'Param'
		%Param: 	This Parameter will be set with Valu
		%Valu: 		This can be any value matching the value type in case of 'logical' value and toggle no value is needed
		%ValueType: Set the type of the value can either be 'number' (numeric), 'string' or 'logical', determines also 
		%			the possible operations in ValMod
		%ValMod:	This sets the operation done on the parameter, at the moment this can either be 'toggle' or 'set', but 'add'
		%			'sub' and other functions will be added soon.
		%DoThings:	Just a simple passthrough to the ParameteChanger (or AddParamChanger), set to true will notify the eventhandler
		
		if nargin<11
			EvalMode=false;
		end	
		
		 switch whichMemory
		 	case 'Param'
				
				if strcmp(ValueType,'string') & strcmp(ValMod,'toggle') 
					%toggle for string is currently not allowed	
					ValMod='set'
				end
				
		 		obj.ParameterChanger(WindowTag,Param,ValueType,Valu,ValMod,DoThings,EvalMode);
		 	 	
		 	 	if Checker
		 			set(obhandle, 'Checked', 'off');
		 			
		 			%get "old" value
		 			oldValue=obj.getParameter(Param,WindowTag);
		 				
		 			switch ValueType
		 				case 'number'
		 					if oldValue==Valu
		 						set(obhandle, 'Checked', 'on');
		 					end
		 				
		 				case 'logical'
		 					if oldValue
		 						set(obhandle, 'Checked', 'on');
		 					end
		 					
		 				case 'string'
		 					if strcmp(oldValue,Valu)
		 						set(obhandle, 'Checked', 'on');
		 					end
		 			end
		 		end
		 	
		 	
		 	case 'AddParam'
		 		if strcmp(ValueType,'string') & strcmp(ValMod,'toggle') 
					%toggle for string is currently not allowed	
					ValMod='set'
				end
				
		 		obj.AdditionalParaChanger(Param,ValueType,Valu,ValMod,DoThings);
		 	 	
		 	 	if Checker
		 			set(obhandle, 'Checked', 'off');
		 			
		 			%get "old" value
		 			oldValue=obj.getAddParam(Param);
		 				
		 			switch ValueType
		 				case 'number'
		 					if oldValue==Valu
		 						set(obhandle, 'Checked', 'on');
		 					end
		 				
		 				case 'logical'
		 					if oldValue
		 						set(obhandle, 'Checked', 'on');
		 					end
		 					
		 				case 'string'
		 					if strcmp(oldValue,Valu)
		 						set(obhandle, 'Checked', 'on');
		 					end
		 			end
		 		end

		 
		 end
		 
		 
		 

		
	
	end
	
	
	
	
	
	function legendary(obj,com,WindowTag,buttonMenu)
		%switches the legend in the plot on and off
		%or just checks it
		
		%needed in case of a mousemenu
		if isempty(buttonMenu)
			Menu2Check= findobj([obj.MenuHandle{:}],'Label','Legend on/off');
		else	
			Menu2Check = buttonMenu;
		end	
		
		switch com
			case 'chk'

					if ~isfield(obj.LegendToggle,WindowTag)
						obj.LegendToggle.(WindowTag) = false;
					end
					
					leg = obj.LegendToggle.(WindowTag);
					
					set(Menu2Check, 'Checked', 'off');
		
					if leg
						set(Menu2Check, 'Checked', 'on');
					end

			case 'set'		
				obj.LegendToggle.(WindowTag) = ~obj.LegendToggle.(WindowTag);
				leg=obj.LegendToggle.(WindowTag);
				
				g = obj.TheGUI;
				plotAxis = findobj(g,'Tag',WindowTag);
				
										
					if leg
						set(Menu2Check, 'Checked', 'on');
						legend(plotAxis,'show');
					else
						set(Menu2Check, 'Checked', 'off');
						legend(plotAxis,'hide');
					end

		end	
	end						  
 
	
	
end	
	




end
classdef GUI < handle
    % GUI : GUI frontend for the MapSeis application
    
    % Copyright 2007-2008 The MathWorks, Inc.
    % $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
    % Author: Matt McDonnell
    
    properties
        DataStore
        FilterList
        FilterIndexVect
        MainGUIFigureProperties
        MainGUIAxesProperties
        MainGUI
        SelectRadius
        HistType
        TimeType
    end
    
    methods
        function obj=GUI(dataStore,filterList)
            import mapseis.gui.*;
            import mapseis.util.gui.*;
            
            % GUI constructor
            % Create the figure window that contains the GUI.  Use a three panel layout
            % consisting of the main axis in the left hand side of the window and two
            % subplots on the right hand side
            
            % Store the handles of the DataStore and FilterList
            obj.DataStore = dataStore;
            obj.FilterList = filterList;
            obj.FilterIndexVect = filterList.getSelected();
            % Set the properties of the figure window
            mainGUIFigureProperties = {'Position',[ 50 50 1100 680 ],'Renderer','painters'};
            
            % Set the axes properties by hand in the order left plot, top right, bottom
            % right
            mainGUIAxesProperties = {...
                {'pos',[ 0.06 0.1 0.55 0.85],'FontSize',12},...
                {'pos',[ 0.7 0.58 0.25 0.37],'FontSize',12},...
                {'pos',[ 0.7 0.1 0.25 0.4],'FontSize',12}};
            
            obj.MainGUI = threePanelLayout('MapSeis Main Window',...
                mainGUIFigureProperties,mainGUIAxesProperties);
            
            % Register this view with the model.  The subject of the view uses this
            % function handle to update the view.
            dataStore.subscribe('MainView',@() updateGUI(obj));
            
            
            % Create a context menu to choose histogram type
            % NB: context menu only appears if you right click in the histogram window,
            % but not actually on the histogram bars themselves
            createHistContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_top_right'));
            
            % Create context menu for the main event plot axis to select the region of
            % interest
            createMainAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_left'));
            
            % Create context menu for the main event plot axis to select the region of
            % interest
            createTimeAxisContextMenu(obj,findobj(obj.MainGUI,'Tag','axis_bottom_right'));
            
            
            try
                mainPars = dataStore.getUserData('mainPars');
                obj.SelectRadius = mainPars.selectRadius;
                obj.HistType = mainPars.histType;
                obj.TimeType = mainPars.timeType;
            catch ME
                % Show different histogram plots depending on histType
                % {'mag','depth',...}, set from hist plot context menu.
                obj.HistType = 'mag';
                
                % Show different time plots depending on timeType, set from time plot
                % context menu.
                obj.TimeType = 'cumnum';
                % Radius for selection about a point
                obj.SelectRadius = 0.25;
            end
            
            % Update the GUI to plot the initial data
            updateGUI(obj);
            
            % Create the menu
            setMenu(obj);
            
            % Make the GUI visible
            set(obj.MainGUI,'Visible','on');
            
        end
        
        function updateGUI(obj)
            % Extract the GUI handle
            g = obj.MainGUI;
            
            obj.FilterIndexVect = obj.FilterList.getSelected();
            % Update the GUI
            plotAxis = findobj(g,'Tag','axis_left');
            assert(~isempty(plotAxis),'GUI:update_left_plot',...
                'GUI left plot axis not found');
            obj.plotRegion(plotAxis);
            set(plotAxis,'Tag','axis_left');
            
            plotAxis = findobj(g,'Tag','axis_top_right');
            assert(~isempty(plotAxis),'GUI:update_top_right',...
                'GUI top right plot axis not found');
            obj.plotHist(plotAxis);
            set(plotAxis,'Tag','axis_top_right');
            
            plotAxis = findobj(g,'Tag','axis_bottom_right');
            assert(~isempty(plotAxis),'GUI:update_bottom_right',...
                'GUI bottom right plot axis not found');
            obj.plotTime(plotAxis)
            set(plotAxis,'Tag','axis_bottom_right');
        end
        
        function updateGUI_leftaxesonly(obj)
            g = obj.MainGUI;
            
            obj.FilterIndexVect = obj.FilterList.getSelected();
            % Update the GUI            
            plotAxis = findobj(g,'Tag','axis_top_right');
            assert(~isempty(plotAxis),'GUI:update_top_right',...
                'GUI top right plot axis not found');
            obj.plotHist(plotAxis);
            set(plotAxis,'Tag','axis_top_right');
            
            plotAxis = findobj(g,'Tag','axis_bottom_right');
            assert(~isempty(plotAxis),'GUI:update_bottom_right',...
                'GUI bottom right plot axis not found');
            obj.plotTime(plotAxis)
            set(plotAxis,'Tag','axis_bottom_right');
        end
        
        function plotRegion(obj,plotAxis)
            import mapseis.plot.PlotRegion;
            import mapseis.projector.*;
            
            % Get the data to plot from the DataStore
            [inReg,outReg] = getLocations(obj.DataStore,obj.FilterIndexVect);
            regionFilter = obj.FilterList.getByName('Region');
            filterRegion = getRegion(regionFilter);
            pRegion = filterRegion{2};
            [selectedMags, unselectedMags] = ...
                getMagnitudes(obj.DataStore,obj.FilterIndexVect);
            mags.selected = selectedMags;
            mags.unselected = unselectedMags;
            if ~isempty(pRegion)
                rBdry = pRegion.getBoundary();
                
                PlotRegion(plotAxis,...
                    struct('inRegion',inReg,...
                    'outRegion',outReg,...
                    'boundary',rBdry,...
                    'plotCoastline',true,...
                    'dataStore',obj.DataStore,...
                    'regionFilter',regionFilter),mags);
            else
                
                PlotRegion(plotAxis,inReg,mags);
            end
        end
        
        function plotHist(obj,plotAxis)
            import mapseis.plot.*;
            import mapseis.projector.*;
            
            % Plot the appropriate histogram depending on histType parameter
            switch obj.HistType
                case 'mag'
                    PlotMagHist(plotAxis,...
                        getMagnitudes(obj.DataStore,obj.FilterIndexVect));
                case 'depth'
                    PlotDepthHist(plotAxis,...
                        getDepths(obj.DataStore,obj.FilterIndexVect));
                case 'hour'
                    PlotHourHist(plotAxis,...
                        getHours(obj.DataStore,obj.FilterIndexVect));
                case 'FMD'
                    PlotFMD(plotAxis,...
                        getMagnitudes(obj.DataStore,obj.FilterIndexVect));
                otherwise
            end
        end
        
        function plotTime(obj,plotAxis)
            import mapseis.plot.*;
            import mapseis.projector.*;
            
            % Plot the appropriate histogram depending on histType parameter
            switch obj.TimeType
                case 'cumnum'
                    PlotTime(plotAxis,...
                        getTimes(obj.DataStore,obj.FilterIndexVect));
                case 'stem'
                    PlotTimeStem(plotAxis,...
                        getTimes(obj.DataStore,obj.FilterIndexVect),...
                        getMagnitudes(obj.DataStore,obj.FilterIndexVect));
                case 'timedepths'
                    PlotTimeDepth(plotAxis,...
                        getTimes(obj.DataStore,obj.FilterIndexVect),...
                        getDepths(obj.DataStore,obj.FilterIndexVect));
                otherwise
            end
        end
        
        function createHistContextMenu(obj,parentAxis)
            % Create a context menu for the histogram in the top right subplot,
            % that selects the histogram type
            
            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.MainGUI);
            % Now make the menu be associated with the correct axis
            set(obj.MainGUI,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different histogram types
            uimenu(cmenu,'Label','Magnitude','Callback',...
                @(s,e) setHistType(obj,'mag'));
            uimenu(cmenu,'Label','Depth','Callback',...
                @(s,e) setHistType(obj,'depth'));
            uimenu(cmenu,'Label','Hour','Callback',...
                @(s,e) setHistType(obj,'hour'));
            uimenu(cmenu,'Label','Cumulative FMD',...
                'Callback',@(s,e) setHistType(obj,'FMD'));
            uimenu(cmenu,'Label','Plot this frame in new window ',...
                'Callback',...
                @(s,e) plotInNewFigure(obj,'Histogram',@plotHist));
        end
        
        function setHistType(obj,newHistType)
            % Set the parameter to plot on the histogram plot
            obj.HistType = newHistType;
            % Update the GUI
            updateGUI_leftaxesonly(obj);
        end
        
        function setTimeType(obj,newTimeType)
            % Set the parameter to plot on the histogram plot
            obj.TimeType = newTimeType;
            % Update the GUI
            updateGUI_leftaxesonly(obj);
        end
        
        function createMainAxisContextMenu(obj,parentAxis)
            import mapseis.plot.SetRegion;
            
            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.MainGUI);
            % Now make the menu be associated with the correct axis
            set(obj.MainGUI,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different region selections
            regionFilter = obj.FilterList.getByName('Region');
            dataStore = obj.DataStore;
            uimenu(cmenu,'Label','Select all',...
                'Callback',@(s,e) SetRegion(...
                regionFilter,'all',dataStore));
            uimenu(cmenu,'Label','Select inside',...
                'Callback',@(s,e) SetRegion(regionFilter,'in',dataStore));
            uimenu(cmenu,'Label','Select Polygon',...
                'Callback',@(s,e) SetRegion(regionFilter,'polygon',dataStore));
            uimenu(cmenu,'Label','Select outside',...
                'Callback',@(s,e) SetRegion(regionFilter,'out',dataStore));
            uimenu(cmenu,'Label','Select circular region',...
                'Callback',@(s,e) setCircle(obj));
            uimenu(cmenu,'Label','Redraw Map',...
                'Callback',@(s,e) updateGUI(obj));
            uimenu(cmenu,'Label','Plot this frame in new figure ',...
                'Callback',@(s,e) plotInNewFigure(obj,...
                'Event Locations',@plotRegion));
        end
        
        function setCircle(obj)
            import mapseis.region.Region;
            import mapseis.util.Ginput;
            
            % Set the selection region by using a circle defined by radius
            % value
            
            % Define the circle boundary
            thetaVals = linspace(0,2*pi,101)';
            circleBoundary = obj.SelectRadius*[cos(thetaVals) sin(thetaVals)];
            % Get the centre of the circle
            centreOffset = Ginput(1);
            % Add the centre offset to the circle
            cicleBoundary = circleBoundary + ...
                repmat(centreOffset,numel(circleBoundary(:,1)),1);
            pRegion = Region(cicleBoundary);
            % Get the region filter
            regionFilter = obj.FilterList.getByName('Region');
            regionFilter.setRegion('in',pRegion);
            % Execute the filter
            regionFilter.execute(obj.DataStore);
        end
        
        function plotInNewFigure(obj,figTitleStr,plotFun)
            import mapseis.util.gui.*;
            
            % Plot the region plot in a new figure for printing.  The new
            % figure will be decoupled from the underlying data.
            % Create a new figure
            newGUI = onePanelLayout(figTitleStr);
            % Get the main plot axis
            plotAxis = findobj(newGUI,'Tag','axis');
            % Plot the region plot
            plotFun(obj,plotAxis)
            % Save the filter parameters to the UserData of the figure
%             set(newGUI,'UserData',struct(...
%                 'timeFilter',{obj.FilterList.getTimeRange()},...
%                 'depthFilter',{obj.FilterList.getDepthRange()},...
%                 'magnitudeFilter',{obj.FilterList.getMagnitudeRange()},...
%                 'regionFilter',{obj.FilterList.getRegion()}...
%                 ));
            % Make the GUI visible
            set(newGUI,'Visible','on');
        end
        
        function saveData(obj)
            % Save the datastore and filter list struct to a .mat file
            [fName,pName] = uiputfile('*.mat','Enter save file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected.  Data not saved');
            else
                dataStore = obj.DataStore; %#ok<NASGU>
                filterList = obj.FilterList; %#ok<NASGU>
                save(fullfile(pName,fName),'dataStore','filterList');
            end
        end
        
        function saveSelectedData(obj) 
            % Save only the data columns to .mat file, ie not filter settings
            % as well
            [fName,pName] = uiputfile('*.mat','Enter save file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected.  Data not saved');
            else
                dataCols = obj.DataStore.getFields([],obj.FilterIndexVect); %#ok<NASGU>
                save(fullfile(pName,fName),'dataCols');
            end
        end
        
        function loadMapSeisData(obj) 
            import mapseis.*;
            import mapseis.gui.*;
            
            % Load an existing MapSeis session
            [fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected. ');
            else
                % Create a new dataStore for the data
                prevData = load(fullfile(pName,fName));
                if isfield(prevData,'dataStore')
                    % We have loaded a saved session, including filter values
                    dataStore = prevData.dataStore;                    
                elseif isfield(prevData,'dataCols')
                    % We have loaded only the data columns, which now need to be turned
                    % into an mapseis.datastore.DataStore
                    dataStore = mapseis.datastore.DataStore(prevData.dataCols);
                elseif isfield(prevData,'a')
                    % We have loaded in a ZMAP format file with data in matrix 'a'
                    dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
                else
                    error('mapseis_gui_GUI:unknown_mat_file','Unknown data in .mat file');
                end
                % Set the filterList to refer to this dataStore
                obj.FilterList.DataStore = dataStore;
                obj.FilterList.update();
                % Get the data out of the file we have loaded
                newData = dataStore.getFields();
                % Set the data of the current DataStore property to be the
                % data that we've just loaded in.
                obj.DataStore.setData(newData);

%                 % Create the main GUI window showing event locations, magnitudes and times
%                 guiWindowStruct.mainStruct=GUI(dataStore);
%                 
%                 % Create the GUI for setting filter parameters and other analysis
%                 % parameters
%                 guiWindowStruct.mainParamStruct=ParamGUI(dataStore);
            end
        end
        
        function loadZMAPData(obj) 
            import mapseis.*;
            import mapseis.importfilter.*;
            
            % Load ZMAP file
            % as well
            [fName,pName] = uigetfile('*.mat','Enter ZMAP or MapSeis *.mat file name...');
            if isequal(fName,0) || isequal(pName,0)
                disp('No filename selected. ');
            else
%                 close all
                prevData = load(fullfile(pName,fName));
                % We have loaded in a ZMAP format file with data in matrix 'a'
                dataStore = mapseis.datastore.DataStore(importZMAPCatRead(prevData.a));
                % Set the filterList to refer to this dataStore
                obj.FilterList.DataStore = dataStore;
                obj.FilterList.update();
                % Get the data out of the file we have loaded
                newData = dataStore.getFields();
                % Set the data of the current DataStore property to be the
                % data that we've just loaded in.
                obj.DataStore.setData(newData);
                mapseis.startApp(fullfile(pName,fName));
            end
        end
        
        function createTimeAxisContextMenu(obj,parentAxis)
            % Create a context menu for the time axis
            
            % Create the contextmenu with parent the main figure
            cmenu = uicontextmenu('Parent',obj.MainGUI);
            % Now make the menu be associated with the correct axis
            set(obj.MainGUI,'UIContextMenu',cmenu);
            set(parentAxis,'UIContextMenu',cmenu);
            % Add uimenu items for the different histogram types
            uimenu(cmenu,'Label','Cumulative Number of Events',...
                'Callback',@(s,e) setTimeType(obj,'cumnum'));
            uimenu(cmenu,'Label','Time-Magnitude Stem Plot',...
                'Callback',@(s,e) setTimeType(obj,'stem'));
            uimenu(cmenu,'Label','Time-Depth Plot',...
                'Callback',@(s,e) setTimeType(obj,'timedepths'));
            uimenu(cmenu,'Label','Plot this frame in new figure ',...
                'Callback',@(s,e) plotInNewFigure(obj,'Time',@plotTime));
            
        end
        
        
        function setMenu(obj)
            g = obj.MainGUI;
            fileMenu = uimenu(g,'Label',' --  Data');
            uimenu(fileMenu,'Label','Save as MapSeis Session',...
                'Callback',@(s,e) obj.saveData);
            uimenu(fileMenu,'Label','Save Selected Data (cut at filters)...',...
                'Callback',@(s,e) obj.saveSelectedData);
            uimenu(fileMenu,'Label','Load MapSeis session *.mat file ..',...
                'Callback',@(s,e) obj.loadMapSeisData);
            uimenu(fileMenu,'Label','Load ZMAP *.mat file ..',...
                'Callback',@(s,e) obj.loadZMAPData);
        end
    end
end
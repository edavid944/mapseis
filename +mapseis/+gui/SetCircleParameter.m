function SetCircleParameter(MainGUI,imobj,RegionFilter)
	%This function opens a gui which allows to set the start and end coordinate
	%and the width of the profile and sets the profile in the plot the entered 
	%values. 
	
	import mapseis.util.*;
	
	if ~isempty(imobj)
		%get current values
		curCoords = imobj.getPosition;
		unRadius=RegionFilter.Radius;
		if isempty(unRadius)
			oldgrid = datastore.getUserData('gridPars');
			unRadius=oldgrid.rad;
		end	
		curRadius = deg2km(unRadius);
	
		% Dialog fields 
		promptValues = {'Longitude Middlepoint','Latitude Middlepoint',...
				'Radius'};
		dlgTitle = 'Circle Parameters';
		
		%currentData
		currentData=curCoords;
		currentData(end+1)=curRadius;
		currentData=num2cell(currentData);
		
		gParams = inputdlg(promptValues,dlgTitle,1,...
		cellfun(@num2str,currentData,'UniformOutput',false));
		
		%read new values
		newCoords(1,1)=str2double(gParams{1});
		newCoords(1,2)=str2double(gParams{2});
		newRadius=km2deg(str2double(gParams{3}));
		
		%insert them into the line and the datastore
		RegionFilter.setRadius(newRadius);
		imobj.setPosition(newCoords);
		
		%update gui
		MainGUI.ParamGUI.updateGUI;
		
	else
		%at the moment only with imline is supported
	end

end

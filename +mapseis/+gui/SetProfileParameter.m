function SetProfileParameter(MainGUI,imobj,RegionFilter)
	%This function opens a gui which allows to set the start and end coordinate
	%and the width of the profile and sets the profile in the plot the entered 
	%values. 
	
	import mapseis.util.*;
	
	if ~isempty(imobj)
		%get current values
		curCoords = imobj.getPosition;
		unWidth=RegionFilter.Width;
		if isempty(unWidth)
			unWidth=GetProfileWidth(MainGUI.DataStore);
		end	
		curWidth = deg2km(2*unWidth);
	
		% Dialog fields 
		promptValues = {'Longitude Point A','Latitude Point A',...
				'Longitude Point B','Latitude Point B',...
				'Profile Width (km)'};
		dlgTitle = 'Profile Parameters';
		
		currentData=reshape(curCoords',1,numel(curCoords));
		currentData(end+1)=curWidth;
		currentData=num2cell(currentData);
		
		gParams = inputdlg(promptValues,dlgTitle,1,...
		cellfun(@num2str,currentData,'UniformOutput',false));
		
		%read new values
		newCoords(1,1)=str2double(gParams{1});
		newCoords(1,2)=str2double(gParams{2});
		newCoords(2,1)=str2double(gParams{3});
		newCoords(2,2)=str2double(gParams{4});
		newWidth=km2deg(str2double(gParams{5})/2);
		
		%insert them into the line and the datastore
		RegionFilter.setWidth(newWidth);
		imobj.setPosition(newCoords);
		
		%update gui
		MainGUI.ParamGUI.updateGUI;
		
	else
		%at the moment only with imline is supported
	end

end

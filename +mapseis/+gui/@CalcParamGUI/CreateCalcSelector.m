function CreateCalcSelector(obj,DataSlide,currentposition);
    %finished: who is saving the list with calculation entries? -> wrapper
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	obj.ModulList{currentposition} = uicontrol(...
									'Parent',obj.TheGUI,...
									'Units','Pixels',...
									'Callback',@(s,e) obj.CalcChange(),...
									'Position',[10 (currentposition-1)*40+3 300 34],...
									'String','CalcSelector',...
									'Style','popupmenu',...
									'Tag','CalcSelector');

	%get the list from the wrapper
	calclist = obj.Wrapper.CalcKeyList;
	set(obj.ModulList{currentposition}, 'String' , calclist(:,2));
	
	%set the selection to the current position
	set(obj.ModulList{currentposition}, 'Value', obj.CurrentCalcSlide);
	
	%One has to be selected
	set(obj.ModulList{currentposition}, 'min', 1);
	
	%try delete and set listener (just to be cautious)
	obj.ListProxy.quiet(obj.Wrapper,'CalcHashUpdated',...
					@(s,e) obj.CalcListChanged()); 
	obj.ListProxy.listen(obj.Wrapper,'CalcHashUpdated',...
					@(s,e) obj.CalcListChanged()); 

end
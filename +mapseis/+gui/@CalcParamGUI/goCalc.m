function goCalc(obj,how,val)
	%The Calculation function in the wrapper needs to be standardized in Name and input values
	%Listener Needed for the update management
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	switch how
		case 'fixed'
			%this allows to pass any value to the calculation function, usefull for speciallized Calcbuttons
			obj.Wrapper.Calculation(val);	
		case 'variable'
			%Sends the position of the CalcSelector to the wrapper, if existing, else it just sends the default value 1
			
			obj.Wrapper.Calculation(obj.CurrentCalcSlide);
		
		case 'screamer'
			%if just a call is needed
			notify(obj,'Calculate');
		
				
	end	

end
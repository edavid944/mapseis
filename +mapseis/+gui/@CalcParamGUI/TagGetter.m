function TheHandleList = TagGetter(obj)
	%because a cell array can not be searched like a normal array this function is needed.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%get all handles in one array
	count=1;
	for i=1:numel(obj.ModulList)
		nums=numel(obj.ModulList{i});

			for j=1:nums
				tempModuls(count) = obj.ModulList{i}(j);
				count=count+1;
			end

	end
	TheHandleList = tempModuls;
	
end

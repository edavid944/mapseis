function CreateCalcButton(obj,DataSlide,currentposition);
	%create a button which starts the calculation
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	Name='Calculate';
	
	if isfield(DataSlide,'Name')
		if ~isempty(DataSlide.Name)
			Name=DataSlide.Name;
		end	
	end
	
	switch DataSlide.how
	 	case 'fixed'
			obj.ModulList{currentposition} = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
              				'Position',[10 (currentposition-1)*40+3 300 34],'Style','pushbutton',...
              				'String',Name,'Tag','CalcButton',...
              				'Callback',@(s,e) obj.goCalc('fixed',DataSlide.value));
	
		case 'variable'
			obj.ModulList{currentposition} = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
           	    		  'Position',[10 (currentposition-1)*40+3 300 34],'Style','pushbutton',...
           	    		  'String',Name,'Tag','CalcButton',...
           	    		  'Callback',@(s,e) obj.goCalc('variable',[]));
		
		case 'screamer'
			obj.ModulList{currentposition} = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
           	    		  'Position',[10 (currentposition-1)*40+3 300 34],'Style','pushbutton',...
           	    		  'String',Name,'Tag','CalcButton',...
           	    		  'Callback',@(s,e) obj.goCalc('screamer',[]));
		
	end
	
end
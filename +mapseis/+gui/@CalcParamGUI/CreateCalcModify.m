function CreateCalcModify(obj,DataSlide,currentposition);
	%Althought the Modifyer for the Calculation is optional, it might be a good idea to use it.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	newbutton   = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
               		  'Position',[10 (currentposition-1)*40+3 140 34],'Style','pushbutton','String','New',...
             			  'Tag','NewCalcButton','Callback',@(s,e) obj.NewCalc(DataSlide.PopUpMenu,DataSlide.Option));

	deletebutton   = uicontrol('Parent',obj.TheGUI, 'Units', 'Pixels',...
               		  'Position',[170 (currentposition-1)*40+3 140 34],'Style','pushbutton','String','Delete',...
             			  'Tag','DeleteCalcButton','Callback',@(s,e) obj.DeleteCalc());
	
	%write the button into the ModulList as an array
	obj.ModulList{currentposition} = [newbutton, deletebutton];

end

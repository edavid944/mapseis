function NewCalcButton(obj,whichone,Option)
	%this function is used for the 'cancel' and 'ok' button in the popupwindow 
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	switch whichone
		case 'cancel'
			%Do nothing just close the window
			close(obj.PopUpWindow);
			try
				clear(obj.PopUpWindow);
			end	
			
		case 'ok'
			%get the wanted calcType, send it and close the window
			childelements = get(obj.PopUpWindow,'children');
			CalcTypePop = findobj(childelements,'Tag','CalcType');
			CalcType = get(CalcTypePop,'Value');
			
			%send to the Wrapper
			obj.Wrapper.addGUICalc(Option{CalcType,2}); 
			
			%close
			close(obj.PopUpWindow);
			try
				clear(obj.PopUpWindow);
			end	

	end			
	
	
end
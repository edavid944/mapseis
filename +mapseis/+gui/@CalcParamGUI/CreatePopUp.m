function CreatePopUp(obj,DataSlide,currentposition);
	%needed input for the PopUp:
	%Name
	%Tag
	%Callback (optional)
	%String with entries
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%check if callback is empty, not existing or not a handle
	CallSwitch='none'
	try 
		if ~isempty(DataSlide.Callback) & isa(DataSlide.Callback,'function_handle');
			CallSwitch='Callback';
		elseif strcmp(DataSlide.Callback,'Change')
			CallSwitch='Change';
		end
	end
	
	%Check if the parameter is meant for a specific calculation
	CalcEntry=[];
	if isfield(DataSlide,'CalcEntry')
		if ~isempty(DataSlide.CalcEntry)
			CalcEntry=DataSlide.CalcEntry;
		end
	end		
	
	
	obj.ModulList{currentposition} = uicontrol(...
									'Parent',obj.TheGUI,...
									'Units','Pixels',...
									'Position',[10 (currentposition-1)*40+3 300 34],...
									'String',DataSlide.Name,...
									'Style','popupmenu',...
									'Tag',DataSlide.Tag);
	
									
	%set callback if needed								
	switch CallSwitch
		case 'none'
			%do nothing
		case 'Callback'
			set(obj.ModulList{currentposition},'Callback',DataSlide.Callback);
		case 'Change'
			set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
	end
									
	set(obj.ModulList{currentposition}, 'String' , DataSlide.EntryStrings);
	
	
	%set position
	if ~isfield(DataSlide,'Value')
		DataSlide.Value=1;
	end		
	
	if isempty(DataSlide.Value)
		DataSlide.Value=1;
	end
	set(obj.ModulList{currentposition},'Value',DataSlide.Value);
	
	%One has to be selected
	set(obj.ModulList{currentposition}, 'min', 1);
	
end

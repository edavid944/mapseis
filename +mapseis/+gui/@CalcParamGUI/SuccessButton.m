function SuccessButton(obj,whichone,Types,AddOptions)
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard



	switch whichone
		case 'cancel'
			%Do nothing just close the window
			clf(obj.PopUpWindow);
			
		case 'ok'
			%get the wanted Calculations, send it back to the wrapper
			%and close the window
			childelements = get(obj.PopUpWindow,'children');
			
			for i=1:numel(Types)
				
				%create the list with possible calculation keys
				posCalcLogical=strcmp(KeyList(:,3),Types{i});
				posCalc=KeyList(posCalcLogical,1);	
			
				CalcPop = findobj(childelements,'Tag',['Calc',num2str(i)]);
				SelCalc = get(CalcTypePop,'Value');
				
				SelectedCalc(i)=posCalc(SelCalc)
			
			end
			
			if isempty(AddOptions)
				WrapPacket=SelectedCalc;
			else
				WrapPacket.SelectedCalc=SelectedCalc;
				WrapPacket.AddOptions=AddOptions;
			end	
			
			%send back to the Wrapper
			obj.Wrapper.addGUICalc(WrapPacket); 
			
			%close
			clf(obj.PopUpWindow);
	end			

end
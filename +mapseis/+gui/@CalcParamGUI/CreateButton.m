function CreateButton(obj,DataSlide,currentposition);
	%just make the buttons so big the still fit 
	
	%it is possibe having no callback is possible but not suggested. 
	%Keep also in mind that there will be no value for a button,
	%so the function called in the wrapper can not read a value 
	%from the button. But it is possible to use the button as 
	%a toggle switch 
	%For each button a callback definition is needed, but they can
	%be different from each other
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	buttonwidth = (300 - (DataSlide.ButtonNumber-1)*2)/DataSlide.ButtonNumber;
	xpos=10;
	Buttons=[];
	
	for i=1:DataSlide.ButtonNumber
		
		%check if callback is empty, not existing or not a handle
		CallSwitch='none'
		try 
			if ~isempty(DataSlide.Callback{i}) & isa(DataSlide.Callback{i},'function_handle');
				CallSwitch='Callback';
			elseif strcmp(DataSlide.Callback{i},'Change')
				CallSwitch='Change';
			end
		end
		
		
		%Check if the parameter is meant for a specific calculation
		CalcEntry=[];
		if isfield(DataSlide,'CalcEntry')
			if ~isempty(DataSlide.CalcEntry)
				CalcEntry=DataSlide.CalcEntry;
			end
		end		
		
		
		%create button
		newbutton   = uicontrol('Parent',obj.TheGUI,...
								'Units', 'Pixels',...
               	  				'Position',[xpos (currentposition-1)*40+3 buttonwidth 34],...
               	  				'String',DataSlide.Name{i},...
								'Style','pushbutton',...
           		  				'Tag',DataSlide.Tag{i});
		
             		  		
             		%set callback if needed								
		switch CallSwitch
			case 'none'
				%do nothing
			case 'Callback'
				set(obj.ModulList{currentposition},'Callback',DataSlide{i}.Callback);
			case 'Change'
				set(obj.ModulList{currentposition},'Callback',@(s,e) obj.ChangeSender(s,CalcEntry));	
		end		 				
             		
		%shift
		
		xpos=xpos+buttonwidth+2;
		Buttons=[Buttons,newbutton];
			
	end		
	
	obj.ModulList{currentposition} = Buttons;
	
end
function NewCalc(obj,PopUpToggle,Option)
	% This creates a new function by calling the function addCalculation in the wrapper
	% if PopUpToggle is true, a window with the different type of possible Calculation is opended speciefied 
	% with the Option entry of the Modulentry.
	% If PopUpToggle is false, the options are send directly to the wrapper.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	if PopUpToggle
		
		Calcslide = obj.CalcParams;
		windowlength = numel(Calcslide)*40;
		
		try
			close(obj.PopUpWindow);
		end
		
		obj.PopUpWindow=figure;
		set(obj.PopUpWindow,'Position', [obj.xPos+20 obj.yPos+windowlength/2 ...
			 obj.windowwide-40 120],'Toolbar','none','MenuBar','none','NumberTitle','off');
		
		%the list with the different calculatuin types
		PossibleCalcList = uicontrol(...
									'Parent',obj.PopUpWindow,...
									'Units','Pixels',...
									'Position',[10 60 obj.windowwide-60 34],...
									'String','CalcType',...
									'Style','popupmenu',...
									'Tag','CalcType');
									
									

		%set the list
		set(PossibleCalcList, 'String' , Option(:,1));

		
		%the buttons
		buttonwidth = (obj.windowwide-60)/2-6;
		cancelbutton   =  uicontrol('Parent',obj.PopUpWindow,...
							'Units', 'Pixels',...
               		  			   	'Position',[10 10 buttonwidth 34],...
               		  			   	'String','Cancel',...
               		  			   	'Style','pushbutton',...
             			  				'Tag','CancelAddCalc',...
             			  				'Callback',@(s,e) obj.NewCalcButton('cancel',Option));

		cancelbutton   =  uicontrol('Parent',obj.PopUpWindow,...
							'Units', 'Pixels',...
               		  			   	'Position',[13+buttonwidth 10 buttonwidth 34],...
               		  			   	'String','Ok',...
               		  			   	'Style','pushbutton',...
             			  				'Tag','OkAddCalc',...
             			  				'Callback',@(s,e) obj.NewCalcButton('ok',Option));


		%The whole rest does the button function
	
		
	else
		obj.Wrapper.addCalculation(Option);
					
	end
	
end
function Widowmaker(obj)
	%creates the actuall GUI for ONE Calculation, needs to be called again if another Calculation is shown.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%get current calcslide form the wrapper
	obj.CalcParams=ParamConfigSender(obj.Wrapper,obj.CurrentCalcSlide);
	Calcslide = obj.CalcParams;
	
	%init the run
	windowlength = numel(Calcslide)*40;  % Every modul is 40 pixels height
	currentposition = 0;

	%use cell array because of more than one object per slide
	obj.ModulList = cell(numel(Calcslide),1);
	
	%clear window and set position
	if ~isempty(obj.TheGUI)
		close(obj.TheGUI)
		obj.TheGUI=[];

	end
	
	%Use Color if wanted (needed for some applications)
	try 
		GUIColor=obj.Wrapper.getCalcColor(obj.CurrentCalcSlide);
		if isstr(GUIColor)
			switch GUIColor
				case 'blue'
					obj.WinCol=[0.7 0.7 1];
				case 'red' 
					obj.WinCol=[1 0.7 0.7];
				case 'green'
					obj.WinCol=[0.7 1 0.7];
				case 'black' 
					obj.WinCol=[0.9 0.9 0.9];
				case 'cyan'
					obj.WinCol=[0.7 1 1];
				case 'magenta'
					obj.WinCol=[1 0.7 1];
			end
		else
			obj.WinCol=GUIColor;
		end
		
	catch
		%default window color	
		obj.WinCol=[0.8000    0.8000    0.8000];
	end		
	
	obj.TheGUI=figure('name',obj.FigureName,'visible','off','Toolbar','none',...
			'MenuBar','none','NumberTitle','off','Color',obj.WinCol);
	
	set(obj.TheGUI,'Position', [obj.xPos obj.yPos obj.windowwide windowlength]);
	
	currentposition=numel(Calcslide);
	
	for i=1:numel(Calcslide)
		entry=Calcslide{i};
		
		switch entry.ModulType
			case 'CalcButton'
				obj.CreateCalcButton(Calcslide{i},currentposition);
				
			case 'DataSelector'
				obj.CreateDataSelector(Calcslide{i},currentposition);
				
			case 'FilterSelector'
				obj.CreateFilterSelector(Calcslide{i},currentposition);
			
			case 'CalcSelector'
				obj.CreateCalcSelector(Calcslide{i},currentposition);
				
			case 'CalcModify'
				obj.CreateCalcModify(Calcslide{i},currentposition);
				
			case 'PopUp'
				obj.CreatePopUp(Calcslide{i},currentposition);
				
			case 'ListBox'
				obj.CreateListBox(Calcslide{i},currentposition);
				
			case 'Button'
				obj.CreateButton(Calcslide{i},currentposition);
				
			case 'Texter'
				obj.CreateTexter(Calcslide{i},currentposition);
				
			case 'Checkerbox'
				obj.CreateCheckerbox(Calcslide{i},currentposition);
			
			case 'Entryfield'
				obj.CreateEntryfield(Calcslide{i},currentposition);
			
			case 'Positionfield'
				obj.CreatePositionfield(Calcslide{i},currentposition);
				
			case 'Empty'
				%just do nothing
		end
		
		%shift down
	 	currentposition=currentposition-1;
		
	 end
	 
	 % Make the GUI visible
	 set(obj.TheGUI,'Visible','on');	

end
function CalcListChanged(obj)


	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	HandleList=obj.TagGetter;
	calclist = obj.Wrapper.CalcKeyList;
	new_Nr = numel(calclist(:,2));
	
	%get the CalcSelector 
	posit = strcmp(get(HandleList,'Tag'),'CalcSelector')
	
	%check if entries are added or delete
	old_list=get(HandleList(posit), 'String');
	
	if isstr(old_list) %->only one entry
		old_Nr=1;
	else
		old_Nr=numel(old_list);
	end
	
	%the different case		
	if (old_Nr>new_Nr) & obj.CurrentCalcSlide>new_Nr
		obj.CurrentCalcSlide=obj.CurrentCalcSlide-1;
	elseif (old_Nr<new_Nr)
		obj.CurrentCalcSlide=new_Nr; %set to last (just added) element
	else
		%no change needed
	end
	
	%rebuild window
	obj.Widowmaker;
	
end

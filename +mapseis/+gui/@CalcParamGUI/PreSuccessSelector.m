function PreSuccessSelector(obj,Types,Texts,AddOptions)
	%A special function, it is a GUI but meant to be called by the
	%wrapper. If a Calculation is added, sometimes the result of 
	%another calculation is needed, this gui allows to select an
	%existing calculation of a certain type.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	%Get the calcKeyList
	Keylist=obj.Wrapper.CalcKeyList;
	
	%convert to cell if needed
	if isstr(Types)
		Types={Types};
		Texts={Texts};
	end
	
	Calcslide = obj.CalcParams;
	windowlength = numel(Types)*80+60;
	el_num=numel(Types);
	
	
	%the PopUpWindow variable is used, it should not be a problem
	%but could be changed later if there is a problem.
	clf(obj.PopUpWindow);
	set(obj.PopUpWindow,'Position', [obj.xPos+20 obj.yPos+windowlenght/2 ...
			obj.windowwide-40 120],'Toolbar','none','MenuBar','none','NumberTitle','off');
	
	for i=1:el_num
		%the list with the different Calculations of one type
		
		TheText = uicontrol(	'Parent',obj.PopUpWindow,...
								'Units','Pixels',...
								'Position',[10 112+(i-1)*42 obj.windowwide-40 34],...
								'String',Texts{i},...
								'Style','text');
		
		
		PossibleCalcList = uicontrol(...
									'Parent',obj.PopUpWindow,...
									'Units','Pixels',...
									'Position',[10 70+(i-1)*42 obj.windowwide-40 34],...
									'String','CalcType',...
									'Style','popupmenu',...
									'Tag',['Calc',num2str(i)]);
									
									
		%create the list with possible calculation
		posCalcLogical=strcmp(KeyList(:,3),Types{i});
		posCalc=KeyList(posCalcLogical,2);
										
		%set the list
		set(PossibleCalcList, 'String' , posCalc);
	

	end

	%the buttons
	buttonwidth = (obj.windowwide-60)/2-6;
	cancelbutton   =  uicontrol('Parent',obj.PopUpWindow,...
								'Units', 'Pixels',...
								'Position',[10 10 buttonwidth 34],...
								'String','Cancel',...
								'Style','pushbutton',...
								'Tag','CancelAddCalc',...
								'Callback',@(s,e) obj.SuccessButton('cancel',Types,AddOptions));

	okbutton   =  uicontrol('Parent',obj.PopUpWindow,...
							'Units', 'Pixels',...
							'Position',[13+buttonwith 10 buttonwidth 34],...
							'String','Ok',...
							'Style','pushbutton',...
							'Tag','OkAddCalc',...
							'Callback',@(s,e) obj.SuccessButton('ok',Types,AddOptions));

	%the buttons do the actual work
	
	
	
	
end
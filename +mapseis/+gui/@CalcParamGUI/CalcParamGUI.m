classdef CalcParamGUI < handle
	 % This GUI allows to custom build a GUI for a certain Calculation
	 
	 
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	properties
		TheGUI
		Wrapper
		Commander
		FigureName
		ListProxy
		CalcParams
		WinCol
		CurrentCalcSlide
		windowwide
		xPos
		yPos
		ModulList
		PopUpWindow
		RegParameter
		Version
	end
	
	events
		Calculate	
	end
	
	
	methods
		function obj = CalcParamGUI(Wrapper,FigureName,WindowPos,ListProxy,RegParameter)
			%Constructor
			
			obj.Version='1.0';
			
			obj.Wrapper = Wrapper;
			obj.FigureName=FigureName;
			obj.ListProxy = ListProxy; 
			obj.RegParameter=RegParameter;
			obj.Commander=obj.Wrapper.Commander;
					
			obj.windowwide = 320;
			obj.xPos=WindowPos(1);
			obj.yPos=WindowPos(2);
			
			%Set default CurrentCalcSlide to 1
			obj.CurrentCalcSlide=1;
			
			%Register yourself in the wrapper, this should also add 
			%CalcParams to this modul
			obj.Wrapper.RegisterGUI(obj,'ParamGUI',RegParameter);
			
			%Go through every modul and create them 
			obj.Widowmaker;
					
		end
	
		
		%As usual the external file functions
		%------------------------------------

		Widowmaker(obj)
		
		CreateCalcButton(obj,DataSlide,currentposition);
		
		CreateDataSelector(obj,DataSlide,currentposition);
		
		CreateFilterSelector(obj,DataSlide,currentposition);

		CreateCalcSelector(obj,DataSlide,currentposition);

		CreateCalcModify(obj,DataSlide,currentposition);

		CreatePopUp(obj,DataSlide,currentposition);

		CreateListBox(obj,DataSlide,currentposition);

		CreateButton(obj,DataSlide,currentposition);

		CreateTexter(obj,DataSlide,currentposition);
		
		CreateCheckerbox(obj,DataSlide,currentposition);

		CreateEntryfield(obj,DataSlide,currentposition);

		CreatePositionfield(obj,DataSlide,currentposition);

		goCalc(obj,how,val)

		CalcChange(obj)

		DataListChanged(obj)

		FilterListChanged(obj)

		CalcListChanged(obj)

		TheHandleList = TagGetter(obj)

		NewCalc(obj,PopUpToggle,Option)

		NewCalcButton(obj,whichone,Option)

		DeleteCalc(obj)

		[CurrentEntry CalcKey] = EntryValues(obj)

		ChangeSender(obj,objHandle,CalcEntry)

		PreSuccessSelector(obj,Types,Texts,AddOptions)

		SuccessButton(obj,whichone,Types,AddOptions)


	end


end
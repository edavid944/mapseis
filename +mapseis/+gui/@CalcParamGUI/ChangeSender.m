function ChangeSender(obj,objHandle,CalcEntry)
	%This function is a generic function for the different uimoduls
	%It does call a function ParameterChanger in the Wrapper and send
	%it the handle of the uiobject. 
	%Added support for parameters from a different Calculation than 
	%the current one.
	

	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard

	
	if isempty(CalcEntry)
		CalcEntry=obj.CurrentCalcSlide;
	end
	
	% very simple, but can be improved in future releases
	obj.Wrapper.ParameterChanger(objHandle,CalcEntry);

end

function [CurrentEntry CalcKey] = EntryValues(obj)
	%This function will get every value from the current Calcslide
	%and save the result in a structure with fields named after the
	%tags
	%The function is meant to be called from the outside (Wrapper)
	%All Values are written into the structure, it does not matter
	%if the values are not needed by the wrapper
	
	%the calckey (mostly not needed, as the GoCalc function sends 
	%CurrentCalcSlide number anyway, but it might needed in a other
	%CalcFunction.
	
	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2013 David Eberhard


	CalcKey = obj.CurrentCalcSlide;
	
	%now get the data
	leModuls=obj.ModulList;
	
	for i=1:numel(LeModuls)
		
		CurMod=LeModuls{i}
		
		%for button/checkbox fields 
		for j=1:numel(CurMod)
			
			%The Tag
			enTag=get(CurMod(j),'Tag');
			
			%The Style
			enStyle=get(CurMod(j),'Style');
			
			%The Value
			if strcmp(enStyle,'edit') & strcmp(enStyle,'text')
				enValue=get(CurMod(j),'String');
			else	
				enValue=get(CurMod(j),'Value');
			end
			
			%write to the structure
			CurrentEntry.(enTag)=enValue;
			
		end
		
	end

end

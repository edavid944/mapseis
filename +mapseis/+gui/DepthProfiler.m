classdef DepthProfiler < handle
	
	%This function plots a profile of the main window
	
	properties
		ListProxy
		Commander
		Datastore
		SendedEvents
		Keys
		MainWindowGUI
		Filterlist
		ErrorDialog
		Region
		ResultGUI
		PlotQuality
		PlotOptions
		plotAxis
		TheAspect
		LegendEntry
		PlotHandles
		LegendToogle
	end

	events
		CalcDone
	end


	methods
	
	
	function obj = DepthProfiler(ListProxy,Commander,MainWindowGUI)
		import mapseis.datastore.*;
		
		obj.ListProxy=ListProxy;
		obj.Commander=Commander;
		
		obj.Datastore = obj.Commander.getCurrentDatastore;
		obj.Filterlist = obj.Commander.getCurrentFilterlist; 
		obj.Keys=obj.Commander.getMarkedKey;
		selected=obj.Filterlist.getSelected;
		obj.SendedEvents=selected;
		obj.TheAspect=2;
		obj.LegendEntry={};
		obj.PlotHandles=[];
		obj.LegendToogle=false;
		
		
		%check if a profile is selected
		regionFilter = obj.Filterlist.getByName('Region');
		filterRegion = getRegion(regionFilter);
		RegRange=filterRegion{1};	
		if ~strcmp(RegRange,'line')
			errordlg('No profile selected in the regionfilter');
			obj.ErrorDialog='No profile selected in the regionfilter';
		end
		
		obj.ListProxy.PrefixQuiet('DepthWindow');
		obj.ListProxy.listen(obj.Commander, 'Switched', @() obj.DataSwitcher,'DepthWindow');
		if ~isempty(MainWindowGUI)
			%normal case, get everything form the mainGUI
			obj.MainWindowGUI=MainWindowGUI;
			obj.setqual('chkData');
			obj.ListProxy.listen(obj.MainWindowGUI, 'MainWindowUpdate', @() obj.updateGUI,'DepthWindow');
		else
			obj.MainWindowGUI=[];
			obj.PlotQuality = 'low';
		
		end
			
		%Build Window
		obj.BuildResultGUI
		
		%plot eq
		obj.updateGUI;
		
		%Switched
		
	end
	
	
	function DataSwitcher(obj)
		%get new data
		obj.Datastore = obj.Commander.getCurrentDatastore;
		obj.Filterlist = obj.Commander.getCurrentFilterlist; 
		obj.Keys=obj.Commander.getMarkedKey;
		selected=obj.Filterlist.getSelected;
		obj.SendedEvents=selected;
		
		%check if the filter is still alright
		
		
	end
	
	
	
	function BuildResultGUI(obj)
		import mapseis.util.gui.*;
		import mapseis.gui.*;
	
		if ~isempty(obj.MainWindowGUI)
			mainPos=get(obj.MainWindowGUI.MainGUI,'Position');
			pos=[mainPos(1) mainPos(2)-300 mainPos(3) 250];
		else
			pos=[50 300 1100 250];
		end
		
		[existFlag,figNumber]=figflag('Profile Window',1);
		newbmapWindowFlag=~existFlag;                          
		disp(newbmapWindowFlag)
		% Set up the Seismicity Map window Enviroment
		%
		if newbmapWindowFlag,
		   obj.ResultGUI = onePanelLayout('Profile Window',...
		   'TickDir','out','color','r','FontWeight','bold','FontSize',12);
		   set(obj.ResultGUI,'Position',pos,'Renderer','painters');
		   obj.plotAxis=gca;
		   set(obj.plotAxis,'Tag','MainResultAxis');
		   
		   obj.buildMenus;
		   
		end	
		set(obj.ResultGUI,'visible','on');
	end
	
	
	
	
	function buildMenus(obj)
		
		obj.PlotOptions = uimenu( obj.ResultGUI,'Label','- Plot Options');
		if ~isempty(obj.MainWindowGUI)
			%means not all menus are needed
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			uimenu(obj.PlotOptions,'Label','Set superelevation',... 
			'Callback',@(s,e) obj.setSuperElevation);
			uimenu(obj.PlotOptions,'Label','Show Legend','Separator','on',... 
			'Callback',@(s,e) obj.LegendSwitcher);
		else
			uimenu(obj.PlotOptions,'Label','Redraw',... 
			'Callback',@(s,e) obj.updateGUI);
			uimenu(obj.PlotOptions,'Label','Set superelevation',... 
			'Callback',@(s,e) obj.setSuperElevation);
			uimenu(obj.PlotOptions,'Label','Zmap style','Separator','on',... 
			'Callback',@(s,e) obj.setqual('low'));
			uimenu(obj.PlotOptions,'Label','Medium Quality Plot',...
			'Callback',@(s,e) obj.setqual('med'));
			uimenu(obj.PlotOptions,'Label','High Quality Plot',...
			'Callback',@(s,e) obj.setqual('hi'));
			uimenu(obj.PlotOptions,'Label','Show Legend','Separator','on',... 
			'Callback',@(s,e) obj.LegendSwitcher);
			
			obj.setqual('chk');
		end
	
	end
	
	
	function updateGUI(obj)
		import mapseis.plot.*;
		
		
		if isempty(obj.plotAxis)
			 obj.plotAxis = findobj(obj.ResultGUI,'Tag','MainResultAxis');
			 if isempty(obj.plotAxis)
			 	%try retag
			 	%get all childrens of the main gui
    				gracomp = get(obj.ResultGUI, 'Children');
    				
    				%find axis and return axis
    				obj.plotAxis = findobj(gracomp,'Type','axes','Tag','');
    				set(obj.plotAxis,'Tag','MainResultAxis');
    			end	
    			
		end
		
		if ~isempty(obj.MainWindowGUI)
			obj.setqual('chkData')
		end
		
		
		plotAxis=obj.plotAxis;
		
		
		%Empty legend and Handles
		obj.LegendEntry={};
		obj.PlotHandles=[];
		
		
		regionFilter = obj.Filterlist.getByName('Region');
		filterRegion = getDepthRegion(regionFilter);
		pRegion = filterRegion{2};
		RegRange=filterRegion{1};
		ProfWidth=filterRegion{4};
		rBdry = pRegion.getBoundary();
		profLine=[rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)];
			
		
		EqConfig= struct(	'PlotType','Earthquakes',...
					'Data',obj.Datastore,...
					'PlotMode','old',...
					'ProfileLine',profLine,...
					'ProfileWidth',ProfWidth,...
					'PlotQuality',obj.PlotQuality,...
					'SelectionSwitch','selected',...
					'SelectedEvents',obj.SendedEvents,...
					'MarkedEQ','max',...
					'X_Axis_Label','Distance [km] ',...
					'Y_Axis_Label','Depth [km] ',...
					'MarkerSize','none',...
					'MarkerStylePreset','none',...
					'Colors','black',...
					'X_Axis_Limit','auto',...
					'Y_Axis_Limit','auto',...
					'C_Axis_Limit','auto');
			
		[handle1 entry1] = PlotEarthquakeSlice(plotAxis,EqConfig);
		
		if ~isempty(handle1)
			if iscell(entry1)	
				obj.LegendEntry(end+1:end+numel(entry1))=entry1;
			else
				obj.LegendEntry{end+1}=entry1;
			end
					
			obj.PlotHandles(end+1:end+numel(handle1))=handle1;	
		end
			
		%set all axes right
		%xlim(plotAxis,xlimiter);
		%ylim(plotAxis,ylimiter);
		
				
		set(plotAxis,'dataaspect',[obj.TheAspect 1 1]);
		set(plotAxis,'YDir','reverse');
		
		legend(plotAxis,obj.PlotHandles,obj.LegendEntry);
		
		if ~obj.LegendToogle
			legend(plotAxis,'hide');
		end
	
	end
	
	
	function LegendSwitcher(obj)
		%simple legendToogle function
		
		if isempty(obj.LegendToogle)
			obj.LegendToogle=false;
		end
		
		%find menu
		Menu2Check = findobj(obj.PlotOptions,'Label','Show Legend');
		
		
		obj.LegendToogle=~obj.LegendToogle;
		
		if obj.LegendToogle
			set(Menu2Check, 'Checked', 'on');
			legend(obj.plotAxis,'show');
			
		else
			set(Menu2Check, 'Checked', 'off');
			legend(obj.plotAxis,'hide');
		end
		
		
	end
	
	
	function setSuperElevation(obj)
		promptValues = {'SuperElevation Factor'};
		dlgTitle = 'Set SuperElevation Factor';
		gParams = inputdlg(promptValues,dlgTitle,1,...
				cellfun(@num2str,{obj.TheAspect},'UniformOutput',false));
		obj.TheAspect=	str2double(gParams{1});
				
				
		obj.updateGUI;
				
	
	
	end
	
	
	function Rebuild(obj)
		
		obj.DataSwitcher;
		obj.BuildResultGUI;
		obj.updateGUI;
		
		
	
	end
	
	function setqual(obj,qual)
			%sets the quality of the plot 
			%chk just gets the userdata parameter and sets the 'checked' to the menu
		 	%dataStore = obj.DataStore;
		 	if ~strcmp(qual,'chkData')
				Menu2Check(3) = findobj(obj.PlotOptions,'Label','Zmap style');
				Menu2Check(2) = findobj(obj.PlotOptions,'Label','Medium Quality Plot');
				Menu2Check(1) = findobj(obj.PlotOptions,'Label','High Quality Plot');
				
				%uncheck all menupoints
				set(Menu2Check(1), 'Checked', 'off');
				set(Menu2Check(2), 'Checked', 'off');
				set(Menu2Check(3), 'Checked', 'off');
			end
			
 			switch qual
 				case 'low'
 					%dataStore.setUserData('PlotQuality','low');
 					set(Menu2Check(3), 'Checked', 'on');
 					obj.PlotQuality='low';
 					obj.updateGUI;
                		
				case 'med'
					%dataStore.setUserData('PlotQuality','med');
					set(Menu2Check(2), 'Checked', 'on');
					obj.PlotQuality='med';
					obj.updateGUI;
					
				case 'hi'
					%dataStore.setUserData('PlotQuality','hi');
					set(Menu2Check(1), 'Checked', 'on');
					obj.PlotQuality='hi';
					obj.updateGUI;
					
				case 'chk'
					%try StoredQuality = dataStore.getUserData('PlotQuality');
					%catch dataStore.setUserData('PlotQuality','med');
					%StoredQuality = dataStore.getUserData('PlotQuality');		
					%end
					StoredQuality = obj.PlotQuality;
					switch StoredQuality
						case 'low'
							sel=3;
						case 'med'
							sel=2;
						case 'hi'
							sel=1;
					end		
					set(Menu2Check(sel), 'Checked', 'on');
				case 'chkData'
					try  
						obj.PlotQuality = obj.Datastore.getUserData('PlotQuality');
					catch 
						
						obj.PlotQuality = 'med';		
					end

			end
	end
	
	
	
	
	
	end
end


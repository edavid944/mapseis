function PointExport(MainGUI,RegionFilter,FilterType)
	%This function opens a dialog to define select and output file
	%and writes the parameters of the filterregion into it in a clear text 
	%format. The format of the entry is defined by the type of the region
	
        [fName,pName] = uiputfile('*.dat','Enter save file name...');
        
        if isequal(fName,0) || isequal(pName,0)
           	disp('No filename selected.  Data not saved');
        else
        	%create file
        	fid = fopen([pName fName],'w') ;
        	
        	%Get Region from FilterRegion
		SavedFilterRegion=getRegion(RegionFilter);
        	Regio=SavedFilterRegion{2};
        	
        	switch FilterType
			case 'polygon'
				%The data is saved as points, first coulomn Longitude
				%second couloumn Latitude
				
				%get point coords 
				Coord=Regio.getBoundary;
				

				fprintf(fid,'%f %f\n',Coord');
				
			case 'line'
				%The two points are saved together with the width of the 
				%profile in the third coulomn. The width saved both points
				%in a future release multisegment profiles with varying 
				%profile width might be possible.
				
				Coord=Regio.getBoundary;
				ProfWidth=RegionFilter.Width;

				saveCoord(1,:)=[Coord(1,1),Coord(1,2),ProfWidth];
				saveCoord(2,:)=[Coord(4,1),Coord(4,2),ProfWidth];

				fprintf(fid,'%f %f %f\n',saveCoord');
				
			case 'circle'
				%In this case the middle point and the radius will be 
				%saved in one line
				Coord=[Regio(1:2),RegionFilter.Radius];
				fprintf(fid,'%f %f %f\n',Coord');
				
			
		end
		
		fclose(fid)
	
	end

end

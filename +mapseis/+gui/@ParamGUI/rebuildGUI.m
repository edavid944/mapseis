function rebuildGUI(obj)
	%allows to rebuild the closed gui
	
	import mapseis.projector.*;
	
	try
		set(obj.mainGUI,'Visible','on');
	catch
		%delete(obj.mainGUI);	
		dataStore = obj.DataStore;
		filterList = obj.FilterList;
		
		rangeFilters = filterList.getRangeFilters();
		filterCount = numel(rangeFilters);

		for filterIndex=1:filterCount
			paramStruct(filterIndex) = ...
			mapseis.gui.ParamGUI.initFilter(dataStore,...
			rangeFilters{filterIndex});
		end
		
		obj.ParamWidgets = cell(numel(paramStruct),1);
		
		% Create the figure
		Map_handle = findobj('name','MapSeis Main Window');
		pos = get(Map_handle,'pos');
		
		%Added automatic resizing of the paramwindow
		winheight=filterCount*80;
		
		
		%try close and delete old windows
		if isempty(obj.oldPosition);
			obj.oldPosition=[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight];
		end
	
		obj.mainGUI = figure('Name','Filter Parameters',...
					'Position', obj.oldPosition,...
					'Toolbar','none','MenuBar','none','NumberTitle','off');
		
		% Create a uipanel in the figure to store the filter parameter widgets
		paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
					'Position',[0 0 1 0.8]);
		
		% Create the widgets inside the uipanel
		obj.createParamWidgets(paramPanel,paramStruct);
		
		% Create a button to call the update
		updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
					'Position',[0 0.85 0.5 0.1],'String','Update',...
					'Callback',@(s,e) obj.updateGUI());
		
		% Register this view with the model.  The subject of the view uses this
		% function handle to update the view.
		%dataStore.subscribe('ParamGUI',@() updateGUI(obj));
		obj.ListProxy.quiet(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
		obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
		
		obj.ListProxy.quiet(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
		obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
		
		% Update the GUI to plot the initial data
		obj.updateGUI();
		
		% Make the GUI visible
		set(obj.mainGUI,'Visible','on');
		
	end
	
end
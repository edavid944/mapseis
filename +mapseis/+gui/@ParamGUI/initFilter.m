function paramStruct=initFilter(dataStore,filterObj)
	
	paramStruct.label = filterObj.Name;
	paramStruct.initPars.min = min(filterObj.Projector(dataStore));
	paramStruct.initPars.max = max(filterObj.Projector(dataStore));
	paramStruct.initPars.typeStrs = filterObj.TypeStr;
	paramStruct.initPars.callbackFcn = ...
				@(varargin) filterObj.setRange(varargin{:}).execute(dataStore);
	
	if strcmp(filterObj.Name,'Time')
			paramStruct.initPars.min = ...
					datestr(paramStruct.initPars.min,'yyyy-mm-dd HH:MM:SS');
	
			paramStruct.initPars.max =...
					datestr(paramStruct.initPars.max,'yyyy-mm-dd HH:MM:SS');
	
			paramStruct.initPars.editDisplayFcn = ...
					@(dNum) datestr(dNum,'yyyy-mm-dd HH:MM:SS');
	end

end
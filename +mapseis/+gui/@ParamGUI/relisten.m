function relisten(obj)
	
	obj.ListProxy.PerfixQuiet('pram');	

	obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
	obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
	
	% Update the GUI to plot the initial data
	obj.updateGUI();
	
end
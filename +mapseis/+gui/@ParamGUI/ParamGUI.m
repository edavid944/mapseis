classdef ParamGUI < handle
	% ParamGUI : GUI frontend for the MapSeis application parameters
	
	% Copyright 2007-2008 The MathWorks, Inc.
	% $Revision: 84 $    $Date: 2008-11-10 10:14:25 +0000 (Mon, 10 Nov 2008) $
	% Author: Matt McDonnell
	
	%Reworked 2014 by David Eberhard
	
	properties
		ParamWidgets
		RangeFilters
		DataStore
		FilterList
		mainGUI
		ListProxy
		oldPosition
	end
	
	methods
		function obj = ParamGUI(dataStore,filterList,ListProxy,WindowPosition)
			import mapseis.projector.*;
			
			% Create the list of parameters and read off the maximum and minimum
			% values from the current list of events.  Each parameter has a string
			% that is displayed on the label next to the parameter edit fields, as
			% well as the intialisation struct.
			
			if nargin<4
				WindowPosition=[];
			end
				
			obj.DataStore = dataStore;
			obj.FilterList = filterList;
			obj.ListProxy = ListProxy;
			obj.oldPosition = WindowPosition;

			if ~isempty(obj.DataStore) & ~isempty(obj.FilterList)
				rangeFilters = filterList.getRangeFilters();
				filterCount = numel(rangeFilters);
				
				for filterIndex=1:filterCount
					paramStruct(filterIndex) = ...
					mapseis.gui.ParamGUI.initFilter(dataStore,...
					rangeFilters{filterIndex});
				end
			
				obj.RangeFilters = rangeFilters;
				
				obj.ParamWidgets = cell(numel(paramStruct),1);
				
				% Initialise the GUI
				
				% Create the figure
				Map_handle = findobj('name','MapSeis Main Window');
				pos = get(Map_handle,'Position');
				
				%Added automatic resizing of the paramwindow
				winheight=filterCount*80;
				if isempty(obj.oldPosition)
					obj.oldPosition=[pos(1)+pos(3) pos(2)+pos(4)-winheight 300 winheight];
				end
				
				obj.mainGUI = figure('Name','Filter Parameters',...
							'Position',obj.oldPosition,...
							'Toolbar','none','MenuBar','none','NumberTitle','off');
				
				% Create a uipanel in the figure to store the filter parameter widgets
				paramPanel = uipanel(obj.mainGUI, 'Units','Normalized',...
							'Position',[0 0 1 0.8]);
		
				% Create the widgets inside the uipanel
				obj.createParamWidgets(paramPanel,paramStruct);
		
				% Create a button to call the update
				updateButton = uicontrol(obj.mainGUI, 'Units', 'Normalized',...
							'Position',[0 0.85 0.5 0.1],'String','Update',...
							'Callback',@(s,e) obj.updateGUI());
		
				% Register this view with the model.  The subject of the view uses this
				% function handle to update the view.
				%dataStore.subscribe('ParamGUI',@() updateGUI(obj));
				obj.ListProxy.listen(obj.DataStore, 'Update', @() obj.updateGUI,'pram');
				obj.ListProxy.listen(obj.FilterList,'Update',@(s,e) obj.updateGUI(),'pram');
				
				
				% Update the GUI to plot the initial data
				obj.updateGUI();
				
				% Make the GUI visible
				set(obj.mainGUI,'Visible','on');

			end
		
		end


		%And a again the external file methods
			
		updateGUI(obj)

		createParamWidgets(obj,paramPanel,paramStruct)

		rebuildGUI(obj)

		relisten(obj)

		silence(obj)

		
	end


	methods (Static)

		paramStruct=initFilter(dataStore,filterObj)

	end


end
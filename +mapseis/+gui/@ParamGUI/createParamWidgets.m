function createParamWidgets(obj,paramPanel,paramStruct)
	import mapseis.util.gui.*;
	
	% Number of widgets to create
	paramCount = numel(paramStruct);
	
	% Give each parameter equal amounts of space
	paramHeight = 1/paramCount;
	
	for paramIndex=1:paramCount
		% Create a panel in which to put the parameter widget
		panelHnd = uipanel('Parent',paramPanel,'BorderType','none',...
					'Units','Normalized',...
					'Position',[0.0 (1-paramHeight*paramIndex) 0.99 paramHeight]);
		
		obj.ParamWidgets{paramIndex} = ...
						paramMinMaxWidget(panelHnd,...
						paramStruct(paramIndex).initPars,...
						paramStruct(paramIndex).label,...
						obj.DataStore, obj.RangeFilters{paramIndex});                
	end

end
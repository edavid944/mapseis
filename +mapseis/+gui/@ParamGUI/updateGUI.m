function updateGUI(obj)
	% Update the filter setting widgets from the state of the event
	% data struct.  This enables us to store the state of the
	% application by saving dataStore to a .mat file
	
	if obj.FilterList.Packed&~isempty(obj.DataStore)
		obj.FilterList.changeData(obj.DataStore);
	
	end
	
	if obj.FilterList.ListenForUpdate
		for paramIndex=1:numel(obj.ParamWidgets)
			obj.ParamWidgets{paramIndex}.updateGUI();
			
			% Execute the filter
			obj.ParamWidgets{paramIndex}.Filter.execute(obj.DataStore,false);
		end
	end

	% Update the filters all at the same time.
	obj.FilterList.update();

end
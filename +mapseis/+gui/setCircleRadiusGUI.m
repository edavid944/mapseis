function setCircleRadiusGUI(datastore,regionFilter)
% setProfileWidthGUI : Dialog to set the width of depth slice

% $Revision: 1 $    $Date: 2008-16-12 
% Author: David Eberhard

import mapseis.util.*;

% Get the old Profile width parameters
if isempty(regionFilter.Radius)
	oldgrid = datastore.getUserData('gridPars');
	oldrad=deg2km(oldgrid.rad);
else
	oldrad=deg2km(regionFilter.Radius)
end

% Dialog fields 
promptValues = {'Width [km]'};
dlgTitle = 'Selection of the circle radius';

% Construct the dialog box and read the values
gParams = inputdlg(promptValues,dlgTitle,1,...
    cellfun(@num2str,{oldrad},'UniformOutput',false));
newR = str2double(gParams{1});
newRadius = km2deg(newR);

% Set the new grid parameters
regionFilter.Radius = newRadius;

end
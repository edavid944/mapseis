function TheRes=polyLTest(PrepStruct,TheCatalog,numCatalogs,minMag,ParallelMode,FeedItBack)
	%A first prototype of a L-Test rebuild, rought an no features at all
	%Polygon version
	
	%TheCatalog is a zmap formate catalog.
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<6
		FeedItBack=[];
	end	
	
	%FeedItBack=[];
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	MagBin=0.1;

	
	maxMag=max(max(PrepStruct.MaxMags));
	TheMags=(0:MagBin:maxMag)';
	SearchMag=[(minMag:MagBin:maxMag)';Inf];
	MagVec=(minMag:MagBin:maxMag)';
	
	
		
	
	%calculate the log likelihood for the observation
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(PrepStruct.PolyCoordX),numel(MagVec));
	RateMatrix=zeros(numel(PrepStruct.PolyCoordX),numel(MagVec));
	
	%write all rates into a matric
	for i=1:numel(PrepStruct.PolyCoordX)
		aboveMag=PrepStruct.MagVector{i}>=minMag;
		RateVec=PrepStruct.CombFMDdiff{i}(aboveMag);
		RateMatrix(i,1:numel(RateVec))=RateVec;
	
	end
	
	
	for i=1:numel(PrepStruct.PolyCoordX)
		%no parfor steps are probably to small
		InRegio = inpolygon(TheCatalog(:,1),TheCatalog(:,2),PrepStruct.PolyCoordX{i},PrepStruct.PolyCoordY{i});
		
		for k=1:numel(SearchMag)-1
		
			InMag=TheCatalog(:,6)>=SearchMag(k)&TheCatalog(:,6)<SearchMag(k+1);
			TheObserver(i,k)=sum(InRegio&InMag);
		
		end
	end
	
	
	
	if isempty(FeedItBack)
	
		%assume the cells are in order
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			TheCat=poissrnd(RateMatrix);	

			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-RateMatrix+TheCat.*log(RateMatrix)-log(factorial(TheCat));
			LogLiker(i)=nansum(nansum(LikeIt(~isinf(LikeIt))));
			
			
		end
	
	else	
		LogLiker=FeedItBack;
	
	end
	
	
	%calc logLikelihood
	LikeIt=-RateMatrix+TheObserver.*log(RateMatrix)-log(factorial(TheObserver));
	ObsLiker=nansum(nansum(LikeIt(~isinf(LikeIt))));
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='LTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

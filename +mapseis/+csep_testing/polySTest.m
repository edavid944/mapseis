function TheRes=polySTest(PrepStruct,TheCatalog,numCatalogs,minMag,ParallelMode,FeedItBack)
	%A first prototype of a S-Test rebuild, rought an no features at all
	%Polygon version
	
	
	%TheCatalog is a zmap formate catalog.
	import mapseis.csep_testing.*;
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<6
		FeedItBack=[];
	end	
	
	%FeedItBack=[];

	
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	

	
	%calculate the log likelihood for the observation
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(PrepStruct.PolyCoordX),1);
	RegioRate=zeros(numel(PrepStruct.PolyCoordX),1);
	

	
	for i=1:numel(PrepStruct.PolyCoordX)
		%could be added into the next loop, but it is more readable like
		%this
		aboveMinMag=(PrepStruct.MagVector{i}>=minMag);
		RegioRate(i)=sum(PrepStruct.CombFMDdiff{i}(aboveMinMag));
	
	end
	
	
	for i=1:numel(PrepStruct.PolyCoordX)
		%no parfor steps are probably to small
		InRegio = inpolygon(TheCatalog(:,1),TheCatalog(:,2),PrepStruct.PolyCoordX{i},PrepStruct.PolyCoordY{i});
		

		InMag=TheCatalog(:,6)>=minMag;
		
		
		TheObserver(i)=sum(InRegio&InMag);
		
		
	end
	
	
	

	
	
	%Normalize Lamba
	%Add up Lambas (MagBin) 
	RawLambaSpace = RegioRate;
	%should be saved not used bins are set to 0 anyway
	
	
	%LambaSpace=(TotalEq/sum(Mmodel(:,9)))*RawLambaSpace;
	ExpNum=sum(RawLambaSpace);
	
	%Number of eq in observation
	%TotalEq=numel(TheCatalog(:,1));
	TotalEq=sum(TheObserver); %saver in case a too large catalog is used
	
	LambaSpace=(TotalEq/ExpNum)*RawLambaSpace;
	UnityLamba=ExpNum*RawLambaSpace;
	if isempty(FeedItBack)
		parfor i=1:numCatalogs
			
			
			
			%changed it to LambaSpace from UnityLamba, don't know why
			%used it before, but it does not matter, poissrnd_limited 
			%correct for it anyway
			TheCat=poissrnd_limited(LambaSpace,TotalEq);
			
		
			LowCat=TheCat;
			
			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-LambaSpace+LowCat.*log(LambaSpace)-log(factorial(LowCat));
			LogLiker(i)=nansum(LikeIt(~isinf(LikeIt)));
			
			
		end
		
	else
		LogLiker=FeedItBack;
	
	end
	
	%calc logLikelihood for catalog
	%LambaSpace = (TotalEq/ExpNum)*RawLambaSpace;
	LikeIt=-LambaSpace+TheObserver.*log(LambaSpace)-log(factorial(TheObserver));
	ObsLiker=nansum(LikeIt(~isinf(LikeIt)));
	
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='STest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

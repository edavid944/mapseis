function ConsistTableMaker(Results,NameList,Filename,ConfigOut,LatexMode)
	%Simple scripts it takes the Result of the consistecy tests and write 
	%them into a table ascii file or a latex formated file

	if nargin<4
		LatexMode=false;
		ConfigOut=struct(	'N_forecast',false,...
					'Models',[[]],...
					'NTest',true,...
					'LTest',true,...
					'STest',true,...
					'MTest',false,...
					'Median',false,...
					'Mean',true,...
					'Quantil',0.05,...
					'QuantTwoLine',false,...
					'AltNameList',[[]]);
					
		disp('Standard Config used')
	end
	
	if nargin<5
		LatexMode=false;
	end
	
	%Unpack Config
	N_forecast=ConfigOut.N_forecast;
	NTest=ConfigOut.NTest;
	LTest=ConfigOut.LTest;
	STest=ConfigOut.STest;
	MTest=ConfigOut.MTest;
	Medianer=ConfigOut.Median;
	Meaner=ConfigOut.Mean;
	Models=ConfigOut.Models;
	
	if isfield(ConfigOut,'Quantil')
		Quanter=ConfigOut.Quantil;
	else
		Quanter=Inf;
	end	
	
	if isfield(ConfigOut,'QuantTwoLine')
		QuantTwoLine=ConfigOut.QuantTwoLine;
	else
		QuantTwoLine=false;
	end	
	
	
	if isfield(ConfigOut,'AltNameList')
		AltNameList=ConfigOut.AltNameList;
	else
		%create an alternatice namelist (without underscore '_'),
		%because Latex does not like that
		for i=1:numel(NameList)
			CurName=NameList{i};
			CurName(CurName=='_')=' ';
			AltNameList{i}=CurName;
		end
	end	
	
	numberOfTest=sum([N_forecast,NTest,NTest,LTest,STest,MTest]);
	%NTest is twice on the list because it has two scores
	if QuantTwoLine
		ShowPart=num2str(sum([Medianer,Medianer,Meaner])+1);
	else
		ShowPart=num2str(sum([Medianer,Medianer,Medianer,Meaner])+1);
	end
	
	TheScores=cell(numel(NameList),numberOfTest);
	MakeDark=false(numel(NameList),numberOfTest);
	
	ScoreNum=zeros(numel(NameList),numberOfTest);
	
	if Meaner
		MeanScore=cell(numel(NameList),numberOfTest);
		StdScore=cell(numel(NameList),numberOfTest);
		MakeDarkMean=false(numel(NameList),numberOfTest);
		
		MeanScoreNum=zeros(numel(NameList),numberOfTest);
	end
	
	if Medianer
		MedianScore=cell(numel(NameList),numberOfTest);
		Q16Score=cell(numel(NameList),numberOfTest);
		Q84Score=cell(numel(NameList),numberOfTest);
		MakeDarkMed=false(numel(NameList),numberOfTest);
		
		MedianScoreNum=zeros(numel(NameList),numberOfTest);
	end
	
	
	%get data
	for i=1:numel(NameList)
		%Like this the Headerlines will be set more than once,
		%but this should not matter too much.
		HeaderLine='Model';
		HeaderLineLatex='\hline Model';
		
		count=1;
		
		if N_forecast
			try
				ScoreNum(i,count)=sum(Models{i}(:,9));
			catch
				%Asume it is already calculated
				ScoreNum(i,count)=Models{i};
			end
			
			TheScores{i,count}=num2str(ScoreNum(i,1),3);
			
			if Meaner
				MeanScore{i,count}=' ';
				StdScore{i,count}=' ';
				MeanScoreNum(i,count)=0;
			end
			
			if Medianer
				MedianScore{i,count}=' ';
				Q16Score{i,count}=' ';
				Q84Score{i,count}=' ';
				MedianScoreNum(i,count)=0;
			end
			
			count=count+1;
			HeaderLine=[HeaderLine,'    N_forecasted'];
			HeaderLineLatex=[HeaderLineLatex,'& $N_{fore}$'];
		end
			
		
		if NTest
			Name=['Ntest_',NameList{i}];
			NRes=Results.(Name);
			
			TheScores{i,count}=num2str(NRes.QLowMonte(1),3);
			TheScores{i,count+1}=num2str(NRes.QHighMonte(1),3);
			
			ScoreNum(i,count)=NRes.QLowMonte(1);
			ScoreNum(i,count+1)=NRes.QHighMonte(1);
			
			MakeDark(i,count)=ScoreNum(i,count)<=Quanter/2;
			MakeDark(i,count+1)=ScoreNum(i,count+1)<=Quanter/2;
			
			if Meaner
				MeanScore{i,count}=num2str(NRes.QScoreLow,3);
				MeanScore{i,count+1}=num2str(NRes.QScoreHigh,3);;
				StdScore{i,count}=num2str(NRes.QScoreLow_std_dev,3);
				StdScore{i,count+1}=num2str(NRes.QScoreHigh_std_dev,3);
				MeanScoreNum(i,count)=NRes.QScoreLow;
				MeanScoreNum(i,count+1)=NRes.QScoreHigh;
				
				MakeDarkMean(i,count)=MeanScoreNum(i,count)<=Quanter/2;
				MakeDarkMean(i,count+1)=MeanScoreNum(i,count+1)<=Quanter/2;
			end
			
			if Medianer
				MedianScore{i,count}=num2str(NRes.QScoreLowMedian,3);
				MedianScore{i,count+1}=num2str(NRes.QScoreHighMedian,3);
				Q16Score{i,count}=num2str(NRes.QScoreLow68Per(1),3);
				Q84Score{i,count}=num2str(NRes.QScoreLow68Per(2),3);
				Q16Score{i,count+1}=num2str(NRes.QScoreHigh68Per(1),3);
				Q84Score{i,count+1}=num2str(NRes.QScoreHigh68Per(2),3);
				MedianScoreNum(i,count)=NRes.QScoreLowMedian;
				MedianScoreNum(i,count+1)=NRes.QScoreHighMedian;
				
				MakeDarkMed(i,count)=MedianScoreNum(i,count)<=Quanter/2;
				MakeDarkMed(i,count+1)=MedianScoreNum(i,count+1)<=Quanter/2;
			end
			
			count=count+2;
			HeaderLine=[HeaderLine,'    Ntest Low       Ntest High'];
			HeaderLineLatex=[HeaderLineLatex,'& $\delta_1$ & $\delta_2$'];
			
			
		end
		
		
		if STest
			Name=['Stest_',NameList{i}];
			SRes=Results.(Name);
			
			TheScores{i,count}=num2str(SRes.QScoreMonte(1),3);
			
			ScoreNum(i,count)=SRes.QScoreMonte(1);
			
			MakeDark(i,count)=ScoreNum(i,count)<=Quanter;
			
			if Meaner
				MeanScore{i,count}=num2str(SRes.QScore,3);
				StdScore{i,count}=num2str(SRes.QScore_std_dev,3);
				
				MeanScoreNum(i,count)=SRes.QScore;
				MakeDarkMean(i,count)=MeanScoreNum(i,count)<=Quanter;
				
			end
			
			if Medianer
				MedianScore{i,count}=num2str(SRes.QScoreMedian,3);
				Q16Score{i,count}=num2str(SRes.QScore68Per(1),3);
				Q84Score{i,count}=num2str(SRes.QScore68Per(2),3);
				
				MedianScoreNum(i,count)=SRes.QScoreMedian;
				MakeDarkMed(i,count)=MedianScoreNum(i,count)<=Quanter;
			end
			
			count=count+1;
			HeaderLine=[HeaderLine,'   STest'];
			HeaderLineLatex=[HeaderLineLatex,'& $\zeta$ (S-test)'];
			
		end
		
		
		if MTest
			Name=['Mtest_',NameList{i}];
			MRes=Results.(Name);
			
			TheScores{i,count}=num2str(MRes.QScoreMonte(1),3);
			
			ScoreNum(i,count)=MRes.QScoreMonte(1);
			MakeDark(i,count)=ScoreNum(i,count)<=Quanter;
			
			if Meaner
				MeanScore{i,count}=num2str(MRes.QScore,3);
				StdScore{i,count}=num2str(MRes.QScore_std_dev,3);
				
				MeanScoreNum(i,count)=MRes.QScore;
				MakeDarkMean(i,count)=MeanScoreNum(i,count)<=Quanter;
			end
			
			if Medianer
				MedianScore{i,count}=num2str(MRes.QScoreMedian,3);
				Q16Score{i,count}=num2str(MRes.QScore68Per(1),3);
				Q84Score{i,count}=num2str(MRes.QScore68Per(2),3);
				
				MedianScoreNum(i,count)=MRes.QScoreMedian;
				MakeDarkMed(i,count)=MedianScoreNum(i,count)<=Quanter;
			end
			
			count=count+1;
			HeaderLine=[HeaderLine,'   MTest'];
			HeaderLineLatex=[HeaderLineLatex,'& $\kappa$ (M-test)'];
			
			
		end
		
						
		if LTest
			Name=['Ltest_',NameList{i}];
			LRes=Results.(Name);
			
			TheScores{i,count}=num2str(LRes.QScoreMonte(1),3);
			
			ScoreNum(i,count)=LRes.QScoreMonte(1);
			MakeDark(i,count)=ScoreNum(i,count)<=Quanter;
			
			if Meaner
				MeanScore{i,count}=num2str(LRes.QScore,3);
				StdScore{i,count}=num2str(LRes.QScore_std_dev,3);
				
				MeanScoreNum(i,count)=LRes.QScore;
				MakeDarkMean(i,count)=MeanScoreNum(i,count)<=Quanter;
				
			end
			
			if Medianer
				MedianScore{i,count}=num2str(LRes.QScoreMedian,3);
				Q16Score{i,count}=num2str(LRes.QScore68Per(1),3);
				Q84Score{i,count}=num2str(LRes.QScore68Per(2),3);
				
				MedianScoreNum(i,count)=LRes.QScoreMedian;
				MakeDarkMed(i,count)=MedianScoreNum(i,count)<=Quanter;
			end
			
			count=count+1;
			HeaderLine=[HeaderLine,'   LTest'];
			HeaderLineLatex=[HeaderLineLatex,'& $\gamma$ (L-test'];
			
		end
			
	
	
	end
	
	%find the winners
	%maxiMan=max(ScoreNum);
	%MakeDark=ScoreNum==repmat(maxiMan,numel(NameList),1);
	%N-forecasted
	%MakeDark(:,1)=false;
	
	
	%if Meaner
	%	maxiManMean=max(MeanScoreNum);
	%	MakeDarkMean=ScoreNum==repmat(maxiManMean,numel(NameList),1);
	%	MakeDarkMean(:,1)=false;
	%end	
	
	
	%if Medianer
	%	maxiManMed=max(MedianScoreNum);
	%	MakeDarkMed=ScoreNum==repmat(maxiManMed,numel(NameList),1);
	%	MakeDarkMed(:,1)=false;
	%end
	
	
	HeaderLineLatex=[HeaderLineLatex '  \\'];
	
	
	
	
	
	
	%May could be more effective, but it should not matter to much
	if ~LatexMode
		%Normal text file
		%write data to file
		fid = fopen(Filename,'w'); 
		fprintf(fid,'%s',HeaderLine); 
		fprintf(fid,'\n');
		
		for i=1:numel(NameList)
			MainLine=[NameList{i},'     ']
			MeanLine=['            '];
			MedianLine1=['            '];
			MedianLine2=['            '];
			for j=1:numberOfTest
				MainLine=[MainLine,TheScores{i,j}];
				
				if Meaner
					MeanLine=[MeanLine,MeanScore{i,j},' +- ',StdScore{i,j},'     '];
				end
				
				if Medianer	
					MedianLine1=[MedianLine1,MedianScore{i,j},'    '];
					MedianLine2=[MedianLine2,' -',Q16Score{i,j},' +',Q84Score{i,j},'    '];
				end
			end
			
			fprintf(fid,'%s',MainLine); 
			fprintf(fid,'\n');
			
			if Meaner
				fprintf(fid,'%s',MeanLine); 
				fprintf(fid,'\n');
			end
			
			if Medianer
				fprintf(fid,'%s',MedianLine1); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',MedianLine2); 
				fprintf(fid,'\n');
			end
		end
		
		fclose(fid); 
	
	else
		%Latex formated text file (uncomplete, needs some tuning afterwards)
		%write data to file
		fid = fopen(Filename,'w'); 
		
		fprintf(fid,'%s','\begin{table}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{tabular}{|c||c|c|c|c|c|}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\multicolumn{6}{c}{CONSISTENCY TEST MISSING TITLE $N_{obs} = XX$} \\');
		fprintf(fid,'\n');
		
		
		
		fprintf(fid,'%s',HeaderLineLatex); 
		fprintf(fid,'\n');
		
		for i=1:numel(NameList)
		
			MainLine=['\hline \multirow{',ShowPart,'}{*}{',AltNameList{i},'}'];
			MeanLine=[' '];
			if QuantTwoLine
				MedianLine1=[' '];
				MedianLine2=[' '];
				MedianLine3=[' '];
			else
				MedianLine1=[' '];
				MedianLine2=[' '];	
			end
			
			start=1;
			
			if N_forecast
				MainLine=[MainLine,' & \multirow{',ShowPart,'}{*}{$\textbf{',TheScores{i,1},'}$} '];
				
				if Meaner
					MeanLine=[MeanLine, ' & '];
				end
				
				if Medianer
					if QuantTwoLine
						MedianLine1=[MedianLine1, ' & '];
						MedianLine2=[MedianLine2, ' & '];
						MedianLine3=[MedianLine3, ' & '];
						
					else
						MedianLine1=[MedianLine1, ' & '];
						MedianLine2=[MedianLine2, ' & '];
					end
				end
				
				start=2;
			end
			
			for j=start:numberOfTest
				if MakeDark(i,j)
					MainLine=[MainLine,' & \cellcolor[gray]{0.8}  $\textbf{',TheScores{i,j},'}$ '];
				else
					MainLine=[MainLine,' & $\textbf{',TheScores{i,j},'}$ '];
				end
				
				if Meaner
					if MakeDarkMean(i,j)
						MeanLine=[MeanLine,' & \cellcolor[gray]{0.8}  $',MeanScore{i,j},' \pm ',StdScore{i,j},'$ '];
					else
						MeanLine=[MeanLine,' & $',MeanScore{i,j},' \pm ',StdScore{i,j},'$ '];
					end
				end
				
				if Medianer
					if MakeDarkMed(i,j)
						if QuantTwoLine
							MedianLine1=[MedianLine1,' & \cellcolor[gray]{0.8}  Median: $',MedianScore{i,j},'$ '];
							MedianLine2=[MedianLine2,' & \cellcolor[gray]{0.8} $Q_{16}= ',Q16Score{i,j},'$ '];
							MedianLine3=[MedianLine3,' & \cellcolor[gray]{0.8} $Q_{84}= ',Q84Score{i,j},'$ '];
						else
							MedianLine1=[MedianLine1,' & \cellcolor[gray]{0.8}  Median: $',MedianScore{i,j},'$ '];
							MedianLine2=[MedianLine2,' & \cellcolor[gray]{0.8} $Q_{16}= ',Q16Score{i,j},'; Q_{84}= ',Q84Score{i,j},'$ '];
						end
					else
						if QuantTwoLine
							MedianLine1=[MedianLine1,' & Median: $',MedianScore{i,j},'$ '];
							MedianLine2=[MedianLine2,' & $Q_{16}= ',Q16Score{i,j},'$ '];
							MedianLine3=[MedianLine3,' & $Q_{84}= ',Q84Score{i,j},'$ '];
						else
							MedianLine1=[MedianLine1,' & Median: $',MedianScore{i,j},'$ '];
							MedianLine2=[MedianLine2,' & $Q_{16}= ',Q16Score{i,j},'; Q_{84}= ',Q84Score{i,j},'$ '];
						end
					end
				end
			end
			
			%endLine
			MainLine=[MainLine, '  \\'];
			MeanLine=[MeanLine, '  \\'];
			if QuantTwoLine
				MedianLine1=[MedianLine1, '  \\'];
				MedianLine2=[MedianLine2, '  \\'];
				MedianLine3=[MedianLine3, '  \\'];
			else
				MedianLine1=[MedianLine1, '  \\'];
				MedianLine2=[MedianLine2, '  \\'];
			end
			
			fprintf(fid,'%s',MainLine); 
			fprintf(fid,'\n');
			
			if Meaner
				fprintf(fid,'%s',MeanLine); 
				fprintf(fid,'\n');
			end
			
			if Medianer
				if QuantTwoLine
					fprintf(fid,'%s',MedianLine1); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',MedianLine2); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',MedianLine3); 
					fprintf(fid,'\n');
				else
					fprintf(fid,'%s',MedianLine1); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',MedianLine2); 
					fprintf(fid,'\n');
				end	
			end	
			
		end
		fprintf(fid,'%s','\hline');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{tabular}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\caption{TEMPLATE TEXT:Results from the consistency tests of the MISSING PART. $\delta_1$ and $\delta_2$ are the two scores of the N-test, $\zeta$	is the score of the S-test and $\gamma$ is the score of the L-test. For cells containing two rows of results, the first row is the score of the forecast measured against the observed catalog, and the second and third row is the score of the forecast against the perturbed catalogues.}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\label{tab:ADD A LABEL}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{table}');
		fprintf(fid,'\n');
 
		
		fclose(fid); 
	
	
	end
	
	
end

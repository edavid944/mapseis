function RatePlotterScript(DataName,RateMatrix,NameVector)
	%Plots the Rates got from the RateExtractor (at the moment only west pacific
	%with 3 models and two years, a script after all)

	
	
	fig=figure
	set(fig,'Position',[150,350,900,800]);
	
	%First plot
	subplot(3,1,1);
	plo1=plot(RateMatrix(:,1),'LineWidth',2,'Color','b');
	hold on
	plo2=plot(RateMatrix(:,4),'LineWidth',2,'Color','r');
	hold off
	
	xlab1=xlabel(' ');
	ylab1=ylabel('Poisson Rate ');
	set([xlab1, ylab1],'FontSize',13,'FontWeight','bold','Interpreter','none');
	
	tit1=title([DataName,': ',NameVector{1},' vs. ', NameVector{4}]);
	set(tit1,'FontSize',16,'FontWeight','bold','Interpreter','none');

	set(gca,'LineWidth',2,'FontSize',12);
	
	
	
	%Second plot
	subplot(3,1,2);
	plo1=plot(RateMatrix(:,2),'LineWidth',2,'Color','b');
	hold on
	plo2=plot(RateMatrix(:,5),'LineWidth',2,'Color','r');
	hold off
	
	xlab2=xlabel(' ');
	ylab2=ylabel('Poisson Rate ');
	set([xlab2, ylab2],'FontSize',13,'FontWeight','bold','Interpreter','none');
	
	tit2=title([DataName,': ',NameVector{2},' vs. ', NameVector{5}]);
	set(tit2,'FontSize',16,'FontWeight','bold','Interpreter','none');

	set(gca,'LineWidth',2,'FontSize',12);
	
	
	%third plot
	subplot(3,1,3);
	plo1=plot(RateMatrix(:,3),'LineWidth',2,'Color','b');
	hold on
	plo2=plot(RateMatrix(:,6),'LineWidth',2,'Color','r');
	hold off
	
	xlab3=xlabel('Earthquake Nr. ');
	ylab3=ylabel('Poisson Rate ');
	set([xlab3, ylab3],'FontSize',13,'FontWeight','bold','Interpreter','none');
	
	tit3=title([DataName,': ',NameVector{3},' vs. ', NameVector{6}]);
	set(tit3,'FontSize',16,'FontWeight','bold','Interpreter','none');

	set(gca,'LineWidth',2,'FontSize',12);

end

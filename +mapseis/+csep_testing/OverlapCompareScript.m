function [CellsNW CellsSW ValList] = OverlapCompareScript(ModelNW,ModelSW,TheName)
	%simple script which compares the overlaping cells of two models
	%kind of a one of script for the scep Pacific models, but could be 
	%addopted for other regions
	
	if nargin<3
		TheName=[];
	end
	
	LatRangeNW=ModelNW(ModelNW(:,3)==-0.25&ModelNW(:,4)==0.25,:);
	LatRangeSW=ModelSW(ModelSW(:,3)==-0.25&ModelSW(:,4)==0.25,:);

	%The cells are in order anyway so no loop needed just take NW as reference
	CellsNW=LatRangeNW;
	CellsSW=LatRangeSW(1:numel(LatRangeNW(:,1)),:);
	
	ValList(:,1)=CellsNW(:,9);
	ValList(:,2)=CellsSW(:,9);
	ValList(:,3)=CellsNW(:,9)-CellsSW(:,9);
	
	%comparison values:
	minNW=min(ModelNW(:,9));
	maxNW=max(ModelNW(:,9));
	meanNW=mean(ModelNW(:,9));
	
	minSW=min(ModelSW(:,9));
	maxSW=max(ModelSW(:,9));
	meanSW=mean(ModelSW(:,9));
	
	minLon=min(CellsNW(:,1));
	maxLon=max(CellsNW(:,1));
	%plot the stuff
	figure
	hist(abs(ValList(:,3)));
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',[0.5 0.5 0.7],'EdgeColor','b')
	hold on
	%plot([minNW,minNW],[0 100],'LineWidth',1,'LineStyle',':','Color','r');
	h1=plot([meanNW,meanNW],[0 120],'LineWidth',2,'LineStyle','-','Color','r');
	%plot([maxNW,maxNW],[0 100],'LineWidth',1,'LineStyle','--','Color','r');
	
	%plot([minSW,minSW],[0 100],'LineWidth',1,'LineStyle',':','Color','g');
	h2=plot([meanSW,meanSW],[0 120],'LineWidth',2,'LineStyle','-','Color','g');
	%plot([maxSW,maxSW],[0 100],'LineWidth',1,'LineStyle','--','Color','g');
	leg1=legend([h1 h2],{'mean Nw model','mean Sw model'});
	lab1(1)=xlabel('abs(Nw Pacific - Sw Pacific)  ');
	lab1(2)=ylabel('Number  ');
	set(gca,'LineWidth',2,'FontSize',12);
	set(lab1,'FontSize',12,'FontWeight','bold');
	set(leg1,'FontSize',11);
	
	if ~isempty(TheName)
		theTit=title(['Histogram: ',TheName]);
		set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	end
	
	
	
	figure
	plot(CellsNW(:,1),ValList(:,1),'LineWidth',2,'LineStyle','--','Color','r');
	hold on
	plot(CellsSW(:,1),ValList(:,2),'LineWidth',2,'LineStyle','--','Color','g');
	plot(CellsNW(:,1),ValList(:,3),'LineWidth',2,'Color','k');
	%plot([minLon,maxLon],[maxNW maxNW],'LineWidth',1,'LineStyle',':','Color','r');
	%plot([minLon,maxLon],[maxSW maxSW],'LineWidth',1,'LineStyle',':','Color','g');
	hold off	
	leg=legend({'Nw Pacific','Sw Pacific','Difference'});
	lab(1)=xlabel('Longitude  ');
	lab(2)=ylabel('$$\lambda$$  ','Interpreter','Latex');
	set(gca,'LineWidth',2,'FontSize',12);
	set(lab,'FontSize',12,'FontWeight','bold');
	set(leg,'FontSize',11);
	xlim([minLon maxLon]);
	
	if ~isempty(TheName)
		theTit=title(['CloseUp: ',TheName]);
		set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	end
	
	
	
	figure
	plot(CellsNW(:,1),ValList(:,1),'LineWidth',2,'LineStyle','--','Color','r');
	hold on
	plot(CellsSW(:,1),ValList(:,2),'LineWidth',2,'LineStyle','--','Color','g');
	plot(CellsNW(:,1),ValList(:,3),'LineWidth',2,'Color','k');
	plot([minLon,maxLon],[maxNW maxNW],'LineWidth',1,'LineStyle',':','Color','r');
	plot([minLon,maxLon],[maxSW maxSW],'LineWidth',1,'LineStyle',':','Color','g');
	hold off	
	leg=legend({'Nw Pacific','Sw Pacific','Difference','max. Nw model','max. Sw model'});
	lab(1)=xlabel('Longitude  ');
	lab(2)=ylabel('$$\lambda$$  ','Interpreter','Latex');
	set(gca,'LineWidth',2,'FontSize',12);
	set(lab,'FontSize',12,'FontWeight','bold');
	set(leg,'FontSize',11);
	xlim([minLon maxLon]);
	
	if ~isempty(TheName)
		theTit=title(TheName);
		set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	end
	
end

function [SmoothIndex,TimeDec] = SmoothBatch(LocationFolder,PlotIDX,saveMaps,MapSaveFolder)
	%A script to batch process ETAS or STEP forecast and analyse
	%them in Sense of Smoothness over time
	%saveMaps can be used to plot the forecasts and save them into a folder
	%saveMaps(1) sets the rate plot on, and saveMaps(2) the FFT plot. The 
	%plots will be saved into the folder MapSaveFolder. CAREFULL, this may 
	%use a lot of memory.
	
	import mapseis.util.importfilter.*;
	import mapseis.importfilter.*;
	import mapseis.util.*;
	import mapseis.csep_testing.*;
	
	if nargin<4
		MapSaveFolder=[];
	end
	
	if isempty(MapSaveFolder);
		saveMaps=[false false];
	end
	
	if numel(saveMaps)==1;
		saveMaps(2)=saveMaps(1);
	end
	
	
	%get all necessary reg expressionpatterns
	rp = regexpPatternCatalog();
	fs=filesep;

	%save current dir
	curdir=pwd;
		
	%go to dir with data
	cd(LocationFolder);
	
	%get files
	
	if ispc
		[sta res] = dos(['dir /b *.mat']);
	else
		[sta res] = unix(['ls -1 *.mat']);
	end
			
	lines = regexp(res,rp.line,'match');
	
	disp(['Files found: ',num2str(numel(lines))]);
	
	%get back to original dir
	cd(curdir);
	
	%create reg pattern for name
	us = rp.us;
	nm = rp.name;
	rowPat = [nm('ModelType',rp.text),us,...
		nm('month',rp.anything),us,...
		nm('day',rp.realPat),us,...
		nm('year',rp.realPat)];
	
		
	dateStruct = regexp(lines,rowPat,'names');
	
	isETAS=strcmp(dateStruct{1}.ModelType,'ETAS');
	
	SmoothIndex=[];
	TimeDec=[];
	
	for i=1:numel(lines)
		
		[pathStr,nameStr{i},extStr] = fileparts(lines{i});
		%for unknown reason matlab adds a ' ' to the end of the 
		%file extension, this will cause, that the file will not be found
		%if the line is used directly or, combined out of nameStr and
		%extStr, to prevent this either cut off the last char from 
		%line or replace the extension with the correct on. I did 
		%go for the last method, because it will still work even when
		%the bug is not present anymore 		
		
		disp(i)
		
		%load file (variable name should be mModel)
		load([LocationFolder,fs,nameStr{i},'.mat']);
		
		%get year month and day (this could be vectorized before the loop,
		%but it is a lot work and does not pay off)
		CurYear=str2num(dateStruct{i}.year(1:end-1)); %stupid point at the end
		CurMonth=str2num(dateStruct{i}.month);
		CurDay=str2num(dateStruct{i}.day);
		
		TimeDec(i)=decyear([CurYear,CurMonth,CurDay]);
		
		%reshape and combine forecast
		[SumForecast SingleMagForecast SingleMags XCoords YCoords] = MagForeCastMerger(mModel,true);
		
		%Make FFT and SmoothIndex
		[SmoothMan NewSmooth MagField] = SmoothTest2D(SumForecast,true,false);
		
		SmoothIndex(i)=SmoothMan;
		
		%now plot and save if it is wanted
		if saveMaps(1)
			%rates
			fig=figure
			p1=pcolor(log10(SumForecast));
			set(p1,'LineStyle','none');
			xla=xlabel('Longitude');
			yla=ylabel('Latitude');
			tit=title([nameStr{i},': log10 Rate']);
			set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
			set([xla,yla],'FontSize',18,'FontWeight','bold');
			set(tit,'FontSize',20,'FontWeight','bold');
			
			%save
			FileName=[nameStr{i},'_LogRates.eps'];
			cd(MapSaveFolder)
			saveas(fig,FileName,'psc');
			cd(curdir);
			
			%Close plot
			close(fig);
		end
		
		
		if saveMaps(2)
			%FFT
			fig=figure
			p1=pcolor(MagField);
			set(p1,'LineStyle','none');
			xla=xlabel('Longitude');
			yla=ylabel('Latitude');
			tit=title([nameStr{i},': FFT Amp. Mag.']);
			set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
			set([xla,yla],'FontSize',18,'FontWeight','bold');
			set(tit,'FontSize',20,'FontWeight','bold');
			
			%save
			FileName=[nameStr{i},'_FFTamp.eps'];
			cd(MapSaveFolder)
			saveas(fig,FileName,'psc');
			cd(curdir);
			
			%Close plot
			close(fig);
			
		end
		
		
		
		
	end
	
	
	%Sort the data by year
	[TimeDec, SortIDX]=sort(TimeDec);
	SmoothIndex=SmoothIndex(SortIDX);
	
	%Now Plot the results of the smoothing
	if PlotIDX
		figure
		SmoPlo=plot(TimeDec,SmoothIndex);
		set(SmoPlo,'LineWidth',4,'Color','r');
		xla=xlabel('DecYear');
		yla=ylabel('SmoothIDX');
		tit=title([dateStruct{1}.ModelType,':SmoothIDX vs. Time']);
		set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
		set([xla,yla],'FontSize',18,'FontWeight','bold');
		set(tit,'FontSize',20,'FontWeight','bold');
	end	
	
	
	
	

end

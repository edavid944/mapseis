function TheRes=protoPermuteTest(ModelA,ModelB,TheCatalog,signLevel,TestStat,NrPermute,ParallelMode)
	%A first prototype of permutation comparison test based on two test statistics
	
	
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	
	if ParallelMode
		matlabpool open
	end
	
	if isempty(signLevel)
		% 5% standard alpha, and it is assumed to be one sided
		signLevel=0.05;
	end
	
	if isempty(NrPermute)
		% number of permutation used for the test
		NrPermute=10000;
	end
	
	if isempty(TestStat)
		% default test statistic is the same as for T-test and W-test
		TestStat='InfoGain';
	end
	
	
	
	%for safety set cells which are zero to 10^-24, that is more or less zero, but
	%still has a log not -Inf, I did not see models which have such low rates, so it should be outside
	ModelA(ModelA(:,9)==0,9)=realmin;
	ModelB(ModelB(:,9)==0,9)=realmin;
	
	
	%added column 10 if not defined (it should be)
	if numel(ModelA(1,:))<10
		ModelA(:,10)=true(numel(ModelA(:,1),1));
	end
	
	if numel(ModelB(1,:))<10
		ModelB(:,10)=true(numel(ModelB(:,1),1));
	end

	UsedBinsA=logical(ModelA(:,10));
	UsedBinsB=logical(ModelB(:,10));

	%set every bin not used in one of the  model to zero (to avoid that the
	%influence the total). They should not be used anyway. Set it up like this the 
	%Cells not used will be used will be zero but no cell used will be zero 
	%UnModRate=Mmodel(:,9);
	ModelA(~UsedBinsA|~UsedBinsB,9)=0;
	ModelB(~UsedBinsA|~UsedBinsB,9)=0;
	
	
	
		
	LamdaA=NaN;
	LamdaB=NaN;
		
	count=1;
		
	for i=1:numel(TheCatalog(:,1))
		LamA=NaN;
		LamB=NaN;
		InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
		InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
		InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
		InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
			
		if any(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB)
			LamA=ModelA(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB,9);
			LamB=ModelB(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB,9);
			LamdaA(count)=LamA;
			LamdaB(count)=LamB;
			count=count+1;
		end
			
	end
	
	%generate data for permutation
	RateVector=[LamdaA;LamdaB];
	GroupVector=[zeros(size(LamdaA));ones(size(LamdaB))];
	NrData=numel(GroupVector);
	
	switch TestStat
		case 'InfoGain'
			
			%Test statistic of the original set
			X_A=log(RateVector(GroupVector==0));
			X_B=log(RateVector(GroupVector==1));
			TotalNum=count-1;
				
			
			%total expectation
			N_A=sum(ModelA(UsedBinsA&UsedBinsB,9));
			N_B=sum(ModelB(UsedBinsA&UsedBinsB,9));
				
			%normalized log(Lamda) for A and B
			I_A=X_A-(N_A/TotalNum);
			I_B=X_B-(N_B/TotalNum);
					
			%average information gain of original data
			OrgTestStat=mean(I_A-I_B);
			
			
			PerTestStat=[];
			
			for i=1:NrPermute	
				%Permute IDs
				PerIDX = randperm(NrData);
				PerGroup = GroupVector(PerIDX);
				
				X_A=log(RateVector(PerGroup==0));
				X_B=log(RateVector(PerGroup==1));
				TotalNum=count-1;
				
			
				
				
					
				%normalized log(Lamda) for A and B
				I_A=X_A-(N_A/TotalNum);
				I_B=X_B-(N_B/TotalNum);
					
				%average information gain
				PerTestStat(i)=mean(I_A-I_B);
					
			end		
					
			
			
		case 'Kernel'
			%Test statistic of the original set
			X_A=log(RateVector(PerGroup==0));
			X_B=log(RateVector(PerGroup==1));
			TotalNum=count-1;
				
			
			%total expectation
			N_A=sum(ModelA(UsedBinsA&UsedBinsB,9));
			N_B=sum(ModelB(UsedBinsA&UsedBinsB,9));
				
			%normalized log(Lamda) for A and B
			I_A=X_A-(N_A/TotalNum);
			I_B=X_B-(N_B/TotalNum);
					
			%average information gain of original data
			OrgTestStat=mean(I_A-I_B);
			
			
			PerTestStat=[];
			
			for i=1:NrPermute	
				%Permute IDs
				PerIDX = randperm(NrData);
				PerGroup = GroupVector(PerIDX);
				
				X_A=log(RateVector(PerGroup==0));
				X_B=log(RateVector(PerGroup==1));
				TotalNum=count-1;
				
			
				
				
					
				%normalized log(Lamda) for A and B
				I_A=X_A-(N_A/TotalNum);
				I_B=X_B-(N_B/TotalNum);
					
				%average information gain
				PerTestStat(i)=mean(I_A-I_B);
					
			end		
		
	end	
		
	
	%similar for all again
	pval=1/NrPermute * sum(PerTestStat>OrgTestStat);
	SignDiff=pval<=signLevel
	
	
		
	
	TheRes.TestType='PerTest';
	TheRes.TestVersion=1;
	TheRes.UsedTestStat=TestStat;
	
	TheRes.SignLevel=signLevel;
	TheRes.OrgTestStat=OrgTestStat;
	TheRes.PerTestStat=PerTestStat;
	TheRes.p_org=pval;
	TheRes.SignDiff=SignDiff;
	TheRes.LamdaA=LamdaA;
	TheRes.LamdaB=LamdaB;
	TheRes.TotalNum=TotalNum;
	TheRes.ExpModA=N_A;
	TheRes.ExpModB=N_B;
	
	if ParallelMode
		matlabpool close
	end
	
	

end

function Results=TestSuiteProto3(ModelA,ModelB,ModelC,TheCatalog,numCatalogs,Names,ParallelMode,TheColor)
	%Just a script for leting three forecast test against each other and 
	%the observation
	
	
	import mapseis.csep_testing.*;
	import mapseis.plot.*;
	
	if nargin<8
		TheColor=[0.5 0.5 0.7];
	end
	
	
	%The N-Test
	NResA = protoNTest(ModelA,TheCatalog,numCatalogs,ParallelMode);
	NResB = protoNTest(ModelB,TheCatalog,numCatalogs,ParallelMode);
	NResC = protoNTest(ModelC,TheCatalog,numCatalogs,ParallelMode);
	
	%The S-Test
	SResA = protoSTest(ModelA,TheCatalog,numCatalogs,ParallelMode);
	SResB = protoSTest(ModelB,TheCatalog,numCatalogs,ParallelMode);
	SResC = protoSTest(ModelC,TheCatalog,numCatalogs,ParallelMode);
	
	%The L-Test
	LResA = protoLTest(ModelA,TheCatalog,numCatalogs,ParallelMode);
	LResB = protoLTest(ModelB,TheCatalog,numCatalogs,ParallelMode);
	LResC = protoLTest(ModelC,TheCatalog,numCatalogs,ParallelMode);
	
	
	%The R-Tests (not sure if the test is working right)
	RRes_AB = protoRTest(ModelA,ModelB,TheCatalog,numCatalogs,ParallelMode);
	RRes_BA = protoRTest(ModelB,ModelA,TheCatalog,numCatalogs,ParallelMode);
	RRes_AC = protoRTest(ModelA,ModelC,TheCatalog,numCatalogs,ParallelMode);
	RRes_CA = protoRTest(ModelC,ModelA,TheCatalog,numCatalogs,ParallelMode);
	RRes_BC = protoRTest(ModelB,ModelC,TheCatalog,numCatalogs,ParallelMode);
	RRes_CB = protoRTest(ModelC,ModelB,TheCatalog,numCatalogs,ParallelMode);

	
	%Pack the Results
	
	Results.NameA=Names{1};
	Results.NameB=Names{2};
	Results.NameC=Names{3};
	
	Results.NTestA=NResA;
	Results.NTestB=NResB;
	Results.NTestC=NResC;
	
	Results.STestA=SResA;
	Results.STestB=SResB;
	Results.STestC=SResC;
	
	Results.LTestA=LResA;
	Results.LTestB=LResB;
	Results.LTestC=LResC;
	
	Results.RTestAB=RRes_AB;
	Results.RTestAC=RRes_AC;
	Results.RTestBC=RRes_BC;
	Results.RTestBA=RRes_BA;
	Results.RTestCA=RRes_CA;
	Results.RTestCB=RRes_CB;
	
	
	
	%Plot the results: hist and CDF
	
	%N-Test
	%-------
	
	%hist N-test model A
	figure
	minH=min(NResA.LogLikeModel);
	maxH=max(NResA.LogLikeModel);
	hist(NResA.LogLikeModel,minH:maxH);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([NResA.LogLikeCatalog,NResA.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Number of Earthquakes   ');
	ylabel('Number of occurence   ');	
	QuantString={['$$\delta_1=$$ ',num2str(round(1000*NResA.QScoreLow)/1000)],...
			['$$\delta_2=$$ ',num2str(round(1000*NResA.QScoreHigh)/1000)]};
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['N-Test: ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%CDF N-test model A
	figure
	CDM=plotCDF(NResA.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) NResA.LogLikeCatalog NResA.LogLikeCatalog],[NResA.QScoreLow NResA.QScoreLow 0],...
				'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Number of Earthquakes   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF N-Test: ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%hist N-test model B
	figure
	minH=min(NResB.LogLikeModel);
	maxH=max(NResB.LogLikeModel);
	hist(NResB.LogLikeModel,minH:maxH);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([NResB.LogLikeCatalog,NResB.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Number of Earthquakes   ');
	ylabel('Number of occurence   ');
	QuantString={['$$\delta_1=$$ ',num2str(round(1000*NResB.QScoreLow)/1000)],...
			['$$\delta_2=$$ ',num2str(round(1000*NResB.QScoreHigh)/1000)]};
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['N-Test: ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	%CDF N-test model B
	figure
	CDM=plotCDF(NResB.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) NResB.LogLikeCatalog NResB.LogLikeCatalog],[NResB.QScoreLow NResB.QScoreLow 0],...
				'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Number of Earthquakes   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF N-Test: ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%hist N-test model C
	figure
	minH=min(NResC.LogLikeModel);
	maxH=max(NResC.LogLikeModel);
	hist(NResC.LogLikeModel,minH:maxH);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([NResC.LogLikeCatalog,NResC.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Number of Earthquakes   ');
	ylabel('Number of occurence   ');
	QuantString={['$$\delta_1=$$ ',num2str(round(1000*NResC.QScoreLow)/1000)],...
			['$$\delta_2=$$ ',num2str(round(1000*NResC.QScoreHigh)/1000)]};
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['N-Test: ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	%CDF N-test model C
	figure
	CDM=plotCDF(NResC.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) NResC.LogLikeCatalog NResC.LogLikeCatalog],[NResC.QScoreLow NResC.QScoreLow 0],...
				'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Number of Earthquakes   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF N-Test: ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	
	
	
	%S-Test
	%-------
	
	%hist S-test model A
	figure
	hist(SResA.LogLikeModel,50);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([SResA.LogLikeCatalog,SResA.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Number of occurence   ');
	QuantString=['$$\zeta=$$ ',num2str(round(1000*SResA.QScore)/1000)];
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['S-Test: ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	%CDF S-test model A
	figure
	CDM=plotCDF(SResA.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) SResA.LogLikeCatalog SResA.LogLikeCatalog],[SResA.QScore SResA.QScore 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF S-Test: ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%hist S-test model B
	figure
	hist(SResB.LogLikeModel,50);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([SResB.LogLikeCatalog,SResB.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Number of occurence   ');
	QuantString=['$$\zeta=$$ ',num2str(round(1000*SResB.QScore)/1000)];
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['S-Test: ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	%CDF S-test model B
	figure
	CDM=plotCDF(SResB.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) SResB.LogLikeCatalog SResB.LogLikeCatalog],[SResB.QScore SResB.QScore 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF S-Test: ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%hist S-test model C
	figure
	hist(SResC.LogLikeModel,50);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([SResC.LogLikeCatalog,SResC.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Number of occurence   ');
	QuantString=['$$\zeta=$$ ',num2str(round(1000*SResC.QScore)/1000)];
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['S-Test: ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	%CDF S-test model C
	figure
	CDM=plotCDF(SResC.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) SResC.LogLikeCatalog SResC.LogLikeCatalog],[SResC.QScore SResC.QScore 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF S-Test: ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	
	
	
	%L-Test
	%-------
	
	%hist L-test model A
	figure
	hist(LResA.LogLikeModel,50);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([LResA.LogLikeCatalog,LResA.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Number of occurence   ');
	QuantString=['$$\gamma=$$ ',num2str(round(1000*LResA.QScore)/1000)];
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['L-Test: ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%CDF L-test model A
	figure
	CDM=plotCDF(LResA.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) LResA.LogLikeCatalog LResA.LogLikeCatalog],[LResA.QScore LResA.QScore 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF L-Test: ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%hist L-test model B
	figure
	hist(LResB.LogLikeModel,50);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([LResB.LogLikeCatalog,LResB.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Number of occurence   ');
	QuantString=['$$\gamma=$$ ',num2str(round(1000*LResB.QScore)/1000)];
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['L-Test: ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%CDF L-test model B
	figure
	CDM=plotCDF(LResB.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) LResB.LogLikeCatalog LResB.LogLikeCatalog],[LResB.QScore LResB.QScore 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF L-Test: ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%hist L-test model C
	figure
	hist(LResC.LogLikeModel,50);
	h = findobj(gca,'Type','patch');
	set(h,'FaceColor',TheColor,'EdgeColor','k');
	hold on;
	ylimer=ylim;
	plot([LResC.LogLikeCatalog,LResC.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
		'Color','r','LineWidth',2);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Number of occurence   ');
	QuantString=['$$\gamma=$$ ',num2str(round(1000*LResC.QScore)/1000)];
	leText=text(0.8,0.8,QuantString,'Interpreter','Latex','Units','normalized');
	set(leText,'FontSize',12);
	theTit=title(['L-Test: ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%CDF L-test model C
	figure
	CDM=plotCDF(LResC.LogLikeModel, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) LResC.LogLikeCatalog LResC.LogLikeCatalog],[LResC.QScore LResC.QScore 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated log-likelihood L   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF L-Test: ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	
	
	%R-Test
	%-------
	
	
	%CDF R-test model A vs. model B
	figure
	CDM=plotCDF(RRes_AB.Ratio_AB, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) RRes_AB.ObsRatio_AB RRes_AB.ObsRatio_AB],[RRes_AB.QScoreRatioAB RRes_AB.QScoreRatioAB 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated ratio of log-likelihood R   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF R-Test: ',Names{1},' vs. ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%CDF R-test model A vs. model C
	figure
	CDM=plotCDF(RRes_AC.Ratio_AB, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) RRes_AC.ObsRatio_AB RRes_AC.ObsRatio_AB],[RRes_AC.QScoreRatioAB RRes_AC.QScoreRatioAB 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated ratio of log-likelihood R   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF R-Test: ',Names{1},' vs. ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%CDF R-test model B vs. model C
	figure
	CDM=plotCDF(RRes_BC.Ratio_AB, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) RRes_BC.ObsRatio_AB RRes_BC.ObsRatio_AB],[RRes_BC.QScoreRatioAB RRes_BC.QScoreRatioAB 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated ratio of log-likelihood R   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF R-Test: ',Names{2},' vs. ',Names{3}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%reverse the order
	
	%CDF R-test model B vs. model A
	figure
	CDM=plotCDF(RRes_BA.Ratio_AB, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) RRes_BA.ObsRatio_AB RRes_BA.ObsRatio_AB],[RRes_BA.QScoreRatioAB RRes_BA.QScoreRatioAB 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated ratio of log-likelihood R   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF R-Test: ',Names{2},' vs. ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
	%CDF R-test model C vs. model A
	figure
	CDM=plotCDF(RRes_CA.Ratio_AB, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) RRes_CA.ObsRatio_AB RRes_CA.ObsRatio_AB],[RRes_CA.QScoreRatioAB RRes_CA.QScoreRatioAB 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated ratio of log-likelihood R   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF R-Test: ',Names{3},' vs. ',Names{1}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	%CDF R-test model C vs. model B
	figure
	CDM=plotCDF(RRes_CB.Ratio_AB, gca, 'k');
	set(CDM,'LineWidth',3);
	hold on;
	xlimer=xlim;
	plot([xlimer(1) RRes_CB.ObsRatio_AB RRes_CB.ObsRatio_AB],[RRes_CB.QScoreRatioAB RRes_CB.QScoreRatioAB 0],'LineStyle','--','Color','r','LineWidth',1.5);
	xlabel('Simulated ratio of log-likelihood R   ');
	ylabel('Empirical CDF   ');
	theTit=title(['CDF R-Test: ',Names{3},' vs. ',Names{2}]);
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	
	
	
end

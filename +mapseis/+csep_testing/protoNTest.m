function TheRes=protoNTest(Mmodel,TheCatalog,numCatalogs,ParallelMode,FeedItBack)
	%A first prototype of a N-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<5
		FeedItBack=[];
	end	
	
	%added column 10 if not defined (it should be)
	if numel(Mmodel(1,:))<10
		Mmodel(:,10)=true(numel(Mmodel(:,1),1));
	end
	
	UsedBins=logical(Mmodel(:,10));
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	
	%set every bin not used to zero (to avoid that the influence the total)
	%UnModRate=Mmodel(:,9);
	Mmodel(~UsedBins,9)=0;
	
	
	if isempty(FeedItBack)
	
		%assume the cells are in order
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			TheCat=poissrnd(Mmodel(:,9));	
			
			TheCat(~UsedBins)=0;
			
			%calculate likelihood (assume poisson distribution)
			LogLiker(i)=nansum(TheCat);
			
			
		end
		
	else	
		LogLiker=FeedItBack;
	
	end
	
	
	%calculate the log likelihood for the observation
	
	
	
	%Here only needed to check wether a earthquake is in a bin actually used
	TheObserver=zeros(numel(Mmodel(:,1)),1);
	
	for i=1:numel(Mmodel(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>=Mmodel(i,1)&TheCatalog(:,1)<Mmodel(i,2);
		InLat=TheCatalog(:,2)>=Mmodel(i,3)&TheCatalog(:,2)<Mmodel(i,4);
		InDepth=TheCatalog(:,7)>=Mmodel(i,5)&TheCatalog(:,7)<Mmodel(i,6);
		InMag=TheCatalog(:,6)>=Mmodel(i,7)&TheCatalog(:,6)<Mmodel(i,8);
		
		
		TheObserver(i)=sum(InLon&InLat&InDepth&InMag&UsedBins(i));
		
		
	end
	
	
	%calc logLikelihood
	ObsLiker=sum(TheObserver);
	
	
	%Calculate quantil
	quant1=sum(LogLiker<=ObsLiker)/numCatalogs;
	quant2=sum(LogLiker>=ObsLiker)/numCatalogs;
	%changed quant2 to >= from > to be consistent with the "official" version
	
	%Pack result
	TheRes.TestType='NTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScoreLow=quant1;
	TheRes.QScoreHigh=quant2;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

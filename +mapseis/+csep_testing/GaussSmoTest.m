function [KernRange,SmoothIDX] = GaussSmoTest(XRange,YRange,MidPoint,KernRange,PlotIt,SingPlot)
	import mapseis.util.*;
	import mapseis.csep_testing.*;
	
	
	if SingPlot
		pXDim=5;
		pYDim=ceil(numel(KernRange)/pXDim);
		gaussFig=figure;
		fftFig=figure;
	
	end
	
	
	
	
	for i=1:numel(KernRange)
		%calculate Kernel
		[X Y Pgauss] = GaussKernel2D(XRange,YRange,MidPoint,KernRange(i));
		%Pgauss=Pgauss./max(Pgauss(:));
		%Smoothtest
		[SmoothIndex NewSmooth MagField] = SmoothTest2D(Pgauss,true,false);
		
		SmoothIDX(i) = SmoothIndex;
		
		if SingPlot
			figure(gaussFig)
			
			%Gaussian
			gauPlo = subplot(pXDim,pYDim,i); 
			p1=pcolor(Pgauss),set(p1,'LineStyle','none');
			tit=title(gauPlo,['Gauss: ',num2str(KernRange(i))]);
			
			figure(fftFig)
			
			fftPlo = subplot(pXDim,pYDim,i); 
			p1=pcolor(MagField),set(p1,'LineStyle','none');
			tit=title(fftPlo,['Gauss FFT: ',num2str(KernRange(i))]);
		end
		
		
		[M N] = size(MagField);
		xctr = 1 + bitshift(N, -1); % x coordinate of center point
		yctr = 1 + bitshift(M, -1); % y coordinate of center point
		selX=ceil(N*0.1);
		selY=ceil(M*0.1);
		
		
		%Additional statistics
		MaxAmp(i)=max(MagField(:));
		MinAmp(i)=MagField(xctr,yctr);
		%min(MagField(:));
		MeanAmp(i)=mean(MagField(:));
		MedianAmp(i)=median(MagField(:));
		ModeAmp(i)=mode(MagField(:));
		%TopMode(i)=sum(sum(MagField>MeanAmp(i)));
		%BottomMode(i)=sum(sum(MagField<MeanAmp(i)));
		
		MaxDist=MaxAmp(i);
		MagField2=(MagField-MinAmp(i))/MaxAmp(i);
		%MagField2(xctr,yctr)=0;
		
		TopMode(i)=sum(sum(MagField2(xctr-selX:xctr+selX,yctr-selY:yctr-selY)))
		%BottomMode(i)=mean(mean(MagField((MagField./MaxDist)<0.1)));
		%ModeRatio(i)=BottomMode(i)/TopMode(i);		
		ModeRatio(i)=abs(max(MagField(:))-median(MagField(:)));
		%abs(MaxAmp(i))/sum(abs(MagField(:)));
		
		%NewSmooth;
		%1/(abs((MaxAmp(i)-MinAmp(i)))+abs((MeanAmp(i)-MinAmp(i))));
		
		MaxDist=MaxAmp(i);
		top10(i)=sum(sum((MagField./MaxDist)>0.9));
		
	end
	
	if PlotIt
		%main feature
		figure;
		p1=plot(KernRange,SmoothIDX);
		set(p1,'LineWidth',4,'Color','r');
		xlab=xlabel('Kernel Width  ');
		ylab=ylabel('Smoothness Index  ');
		tit1=title(['Smoothness of Gauss Kernel: X= ',num2str(XRange(1)), ...
			' to ',num2str(XRange(end)),'; Y= ',num2str(YRange(1)), ...
			' to ',num2str(YRange(end))]);
		set([xlab,ylab],'FontSize',16,'Fontweight','bold');	
		set(tit1,'FontSize',18,'Fontweight','bold');
		set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
		
		
		figure;
		p1=plot(KernRange,MaxAmp);
		set(p1,'LineWidth',4,'Color','r');
		hold on
		p2=plot(KernRange,MeanAmp);
		set(p2,'LineWidth',4,'Color','b');
		hold off
		legend({'Max. Mag. Amp','Mean. Mag. Amp'});
		
		xlab=xlabel('Kernel Width  ');
		ylab=ylabel('Magnitude Amp.  ');
		tit1=title(['Max. and mean Amp.: X= ',num2str(XRange(1)), ...
			' to ',num2str(XRange(end)),'; Y= ',num2str(YRange(1)), ...
			' to ',num2str(YRange(end))]);
		set([xlab,ylab],'FontSize',16,'Fontweight','bold');	
		set(tit1,'FontSize',18,'Fontweight','bold');
		set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
		
		
		figure;
		p1=plot(KernRange,MinAmp);
		set(p1,'LineWidth',4,'Color','r');
		hold on
		p2=plot(KernRange,MedianAmp);
		set(p2,'LineWidth',4,'Color','b');
		p3=plot(KernRange,ModeAmp);
		set(p3,'LineWidth',4,'Color','m');
		hold off
		legend({'Min. Mag. Amp','Median. Mag. Amp','Mode. Mag. Amp'});
		
		xlab=xlabel('Kernel Width  ');
		ylab=ylabel('Magnitude Amp.  ');
		tit1=title(['Min, medain and mode Amp.: X= ',num2str(XRange(1)), ...
			' to ',num2str(XRange(end)),'; Y= ',num2str(YRange(1)), ...
			' to ',num2str(YRange(end))]);
		set([xlab,ylab],'FontSize',16,'Fontweight','bold');	
		set(tit1,'FontSize',18,'Fontweight','bold');
		set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
		
		
		
		figure;
		p1=plot(KernRange,TopMode);
		set(p1,'LineWidth',4,'Color','r');
		hold on
		%p2=plot(KernRange,BottomMode);
		%set(p2,'LineWidth',4,'Color','b');
		hold off
		legend({'Above Mode Amp','Below Mode Amp'});
		
		xlab=xlabel('Kernel Width  ');
		ylab=ylabel('Nr. cells  ');
		tit1=title(['Top and Bottom mode Amp.: X= ',num2str(XRange(1)), ...
			' to ',num2str(XRange(end)),'; Y= ',num2str(YRange(1)), ...
			' to ',num2str(YRange(end))]);
		set([xlab,ylab],'FontSize',16,'Fontweight','bold');	
		set(tit1,'FontSize',18,'Fontweight','bold');
		set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
		
		
		
		figure;
		p1=plot(KernRange,ModeRatio);
		set(p1,'LineWidth',4,'Color','r');

		
		xlab=xlabel('Kernel Width  ');
		ylab=ylabel('TopMode / BottomMode  ');
		tit1=title(['Mode Ratio.: X= ',num2str(XRange(1)), ...
			' to ',num2str(XRange(end)),'; Y= ',num2str(YRange(1)), ...
			' to ',num2str(YRange(end))]);
		set([xlab,ylab],'FontSize',16,'Fontweight','bold');	
		set(tit1,'FontSize',18,'Fontweight','bold');
		set(gca,'LineWidth',3,'FontSize',16,'FontWeight','bold','Box','on');
		
	end


end

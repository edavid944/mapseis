function TheRes=protoWTest(ModelA,ModelB,TheCatalog,signLevel,MatMethod,ParallelMode)
	%A first prototype of a W-Test rebuild, rought an no features at all
	
	%It is a wilcoxon signed-rank test
	
	%Less assumations about the distribution of the values are needed than 
	%for a t-test. (no normal distribution needed, only symmetry around the
	%median is needed)
	%Wilcoxon's idea was to used the rank of the values instead of the data
	%itself.
	
	
	
	%tiedrank probably needed
	
	
	%H0: 1/N*sum(X_A-X_B) == (sum(ModelA(:,9)-sum(ModelB:,9))/N
	%		-> 	median of X_A-X_B is equal to the difference of the expected values 
	%		 	normalized by the total number of observation
	
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	
	if ParallelMode
		matlabpool open
	end
	
	if isempty(signLevel)
		% 5% standard alpha, and it is assumed to be one sided
		signLevel=0.05;
	end
	
	%for safety set cells which are zero to 10^-24, that is more or less zero, but
	%still has a log not -Inf, I did not see models which have such low rates, so it should be outside
	ModelA(ModelA(:,9)==0,9)=realmin;
	ModelB(ModelB(:,9)==0,9)=realmin;
	
	%added column 10 if not defined (it should be)
	if numel(ModelA(1,:))<10
		ModelA(:,10)=true(numel(ModelA(:,1),1));
	end
	
	if numel(ModelB(1,:))<10
		ModelB(:,10)=true(numel(ModelB(:,1),1));
	end

	UsedBinsA=logical(ModelA(:,10));
	UsedBinsB=logical(ModelB(:,10));

	%set every bin not used in one of the  model to zero (to avoid that the
	%influence the total). They should not be used anyway. Set it up like this the 
	%Cells not used will be used will be zero but no cell used will be zero 
	%UnModRate=Mmodel(:,9);
	ModelA(~UsedBinsA|~UsedBinsB,9)=0;
	ModelB(~UsedBinsA|~UsedBinsB,9)=0;
	
	%  
	%  %now to the observations (ModelA and ModelB have to be on the same grid)
	
	
	%get a probability for each earthquake in each model
	
	LamdaA=NaN;
	LamdaB=NaN;
	count=1;	
	for i=1:numel(TheCatalog(:,1))
		LamA=NaN;
		LamB=NaN;
		InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
		InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
		InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
		InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
		
		if any(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB)
			LamA=ModelA(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB,9);
			LamB=ModelB(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB,9);
			LamdaA(count)=LamA;
			LamdaB(count)=LamB;
			count=count+1;
		end
		%disp(LamA);
		%disp(LamB);
		
	end
	
	if ~MatMethod	
		%use custom implementation
		X_A=log(LamdaA);
		X_B=log(LamdaB);
		TotalNum=count-1;
			
		diffMed=(X_A-X_B)-(sum(ModelA(UsedBinsA&UsedBinsB,9))...
				-sum(ModelB(UsedBinsA&UsedBinsB,9)))/TotalNum;
		
		MedNoZero=diffMed(diffMed~=0);
		NegVal=MedNoZero<0;
		TheRanks=tiedrank(abs(MedNoZero));
		
		W_plus=sum(TheRanks(~NegVal));
		W_minus=sum(TheRanks(NegVal));
		W_overall=W_plus+W_minus;
		
		%for getting the p-value
		[p,h,stats] = signrank(diffMed,0,'alpha',signLevel);
	
		TheRes.TestType='Wtest';
		TheRes.RawDiffs=(X_A-X_B);
		TheRes.W_plus=W_plus;
		TheRes.SignLevel=signLevel;
		TheRes.W_minus=W_minus;
		TheRes.W_overall=W_overall;
		TheRes.p_org=p;
		TheRes.TotalNum=TotalNum;
		TheRes.ExpModA=sum(ModelA(:,9));
		TheRes.ExpModB=sum(ModelB(:,9));
		TheRes.MatMethod=MatMethod;
		
	else	
		%use matlab version
		X_A=log(LamdaA);
		X_B=log(LamdaB);
		TotalNum=count-1;
			
		diffMed=(X_A-X_B)-(sum(ModelA(UsedBinsA&UsedBinsB,9))...
				-sum(ModelB(UsedBinsA&UsedBinsB,9)))/TotalNum;
		
		[p,h,stats] = signrank(diffMed,0,'alpha',signLevel);
		
		TheRes.TestType='Wtest';
		TheRes.TestVersion=1;
		TheRes.SignLevel=signLevel;
		TheRes.RawDiffs=(X_A-X_B);
		TheRes.p_medZero=p;
		TheRes.NullHypoReject=h;
		TheRes.RawStat=stats;
		TheRes.TotalNum=TotalNum;
		TheRes.ExpModA=sum(ModelA(:,9));
		TheRes.ExpModB=sum(ModelB(:,9));
		TheRes.MatMethod=MatMethod;
		
	end
	
	if ParallelMode
		matlabpool close
	end
	
	

end

function plotAxis=TestRegPolyPlotter(Datastore,CatColor,Filterlist,Offset,TheName,plotAxis,AxLimit)
	%simple script it plots a catalogs and filter polygons into one plot together with a borders and coastlines
	%CatColor is a cell array with strings:  'blue','red','green','black','cyan','magenta' and the 'automatic' settings
	import mapseis.util.*;
	import mapseis.plot.*;
	
	if nargin<7
		plotAxis=[];
		disp('set plotAxis to []')
	end	
	
	if isempty(Offset)
		Offset=0;
	end	
	
	if ~iscell(Datastore)
		Datastore={Datastore};
	end

	if ~iscell(CatColor)
		CatColor={CatColor};
	end

	
	xlimiter=AxLimit{1};
	ylimiter=AxLimit{2};
	
	
	
	%plot the stuff manually till I have modified the colorplot function
	if isempty(plotAxis)
		figure
		plotAxis=gca
	end
	
	
	
	%Border
	BorderConf= struct(	'PlotType','Border',...
	  			'Data',Datastore{1},...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','normal',...
				'Colors','black',...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
				
	%[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
		
	hold on	
	
	%CoastLine
	CoastConf= struct( 	'PlotType','Coastline',...
				'Data',Datastore{1},...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','Fatline',...
				'Colors','black',...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
				
	[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
	
	hold on
	
	for i=1:numel(Datastore)
		%now go for the Earthquakes
		NumEQ=Datastore{i}.getRowCount;
		SendedEvents=true(NumEQ,1);
		
		%Earthquakes
		EqConfig= struct(	'PlotType','Earthquakes',...
					'Data',Datastore{i},...
					'PlotMode','old',...
					'PlotQuality','hi',...
					'SelectionSwitch','selected',...
					'SelectedEvents',SendedEvents,...
					'MarkedEQ','max',...
					'X_Axis_Label','Longitude ',...
					'Y_Axis_Label','Latitude ',...
					'MarkerSize','none',...
					'MarkerStylePreset','none',...
					'Colors',CatColor{i},...
					'X_Axis_Limit',xlimiter,...
					'Y_Axis_Limit',ylimiter,...
					'C_Axis_Limit','auto',...
					'LegendText',[],...
					'Use_Offset','manual',...
					'Manual_Offset',[[Offset 0]],...
					'PostLim',true);
		
		[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
	end
	
	
	%now the polygon
	regionFilter = Filterlist.getByName('Region');
	filterRegion = getRegion(regionFilter);
	pRegion = filterRegion{2};
	RegRange=filterRegion{1};	
	
	if isobject(pRegion)
		rBdry = pRegion.getBoundary();	
	else 
		rBdry = pRegion; 
	end
        
	if ~isempty(rBdry)
		%convert to shifted coordinates if needed
		if Offset~=0
			rBdry=ShiftCoords(rBdry,[Offset,0]);
			%ModelConv(:,[2,4])=ShiftCoords(rBdry(:,[2,4]),[Offset,0]);	
		end
		
		FiltPol=plot(rBdry(:,1),rBdry(:,2));
		
		set(FiltPol,'LineWidth',3,'Color','r');
	end
        
	%disp(handle1)
	%disp(entry1)
	%legend(handle1, entry1)
	set(plotAxis,'LineWidth',2,'FontSize',14);
	set(plotAxis,'YDir','normal');
	set(plotAxis,'Box','on','TickDir','out','TickLength'  ,[.015 .01] ,'XMinorTick','on','YMinorTick' ,...
			'on', 'YGrid','off','XGrid', 'off', 'XColor', [.3 .3 .3],'YColor', [.3 .3 .3]);
	latlim = get(plotAxis,'Ylim');
	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);		
	
	if ~isempty(TheName)
		tit=title(plotAxis,TheName);
		set(tit,'FontSize',18,'FontWeight','bold','Interpreter','none');
	end
	

	
	hold off
	
	
end

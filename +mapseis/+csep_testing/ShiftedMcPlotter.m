function ShiftedMcPlotter(plotAxis,Datastore,Offset)
	%function to plot a before calculated Mc calculation with a offset
	import mapseis.datastore.*;
	import mapseis.export.Export2Zmap;
	import mapseis.plot.*;
	import mapseis.util.AutoBiColorScale;
	
	if isempty(Offset)
		Offset=0;
	end	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end

	
	%check if there are old calculations
	try
		oldCalcs=Datastore.getUserData('new-bval-calc');
	catch
		disp('no calculation available in this datastore')
		return;
	end
	
	Title = 'Select Calculation';
	Prompt={'Which Calculation:', 'WhichOne'};
		
	AvailCalc=oldCalcs(:,1);			
	
	%Calc
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=AvailCalc;
	Formats(1,1).size = [-1 0];
		
	Formats(1,2).type='none';
	Formats(1,2).limits = [0 1];
	Formats(1,3).type='none';
	Formats(1,3).limits = [0 1];
		
		
		
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';

		
		
	%default values
	defval =struct('WhichOne',1);
			
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
		
	if Canceled~=1
		%getCalculation
		TheCalc=oldCalcs{NewParameter.WhichOne,2};
		CalcParameter=TheCalc.CalcParameter;
		AlphaMap=TheCalc.AlphaMap;
		CalcRes=TheCalc.CalcResult;
		SendedEvents=TheCalc.Selected;
		CalcName=oldCalcs{NewParameter.WhichOne,1};
		
		%get possible values
		rawFields=fieldnames(CalcRes);
		AvailFields={};
		
		for i=1:numel(rawFields)
			Sizer=size(CalcRes.(rawFields{i}));
			if all(Sizer>1)
				%matrix, can be used, the rest (vectors and empty) 
				%throw away
				AvailFields{end+1}=rawFields{i};
			end	
			
		end
		
		%ask for which parameter
		Title = 'Select Value';
		Prompt={'Which Value:', 'WhichOne'};
			
			
		%Calc
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailFields;
		Formats(1,1).size = [-1 0];
			
		Formats(1,2).type='none';
		Formats(1,2).limits = [0 1];
		Formats(1,3).type='none';
		Formats(1,3).limits = [0 1];
			
			
			
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
	
			
			
		%default values
		defval =struct('WhichOne',1);
				
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 

	
	end

	
	%check again if one want to calc
	if Canceled==1
		return
	end
	
	%Now the actual work
	%-------------------
	
	%Get Data
	TheVal=CalcRes.(AvailFields{NewParameter.WhichOne})';
	ValName=AvailFields{NewParameter.WhichOne};
	
	%shift Coordinates
	X=CalcRes.X;
	Y=CalcRes.Y;
	
	XShifted=X+Offset;
	
	%find position to wrap around
	NoShift=false;
	if Offset<0
		UnSuitable=find(XShifted<-180);
		BreakPoint=UnSuitable(end);
		XShifted(UnSuitable)=360+XShifted(UnSuitable);
		XShifted=[XShifted(BreakPoint+1:end);XShifted(1:BreakPoint)];
		
		%Change Map
		TheValShifted=[TheVal(:,BreakPoint+1:end),TheVal(:,1:BreakPoint)];
		if ~isempty(AlphaMap)
			TheAlphaShifted=[AlphaMap(:,BreakPoint+1:end),AlphaMap(:,1:BreakPoint)];
		else
			TheAlphaShifted=[];
		end	
		
	elseif Offset>0
		UnSuitable=find(XShifted>180);
		BreakPoint=UnSuitable(1);
		XShifted(UnSuitable)=-360+XShifted(UnSuitable);
		XShifted=[XShifted(BreakPoint+1:end);XShifted(1:BreakPoint)];
		
		%Change Map
		TheValShifted=[TheVal(:,BreakPoint+1:end),TheVal(:,1:BreakPoint)];
		if ~isempty(AlphaMap)
			TheAlphaShifted=[AlphaMap(:,BreakPoint+1:end),AlphaMap(:,1:BreakPoint)];		
		else
			TheAlphaShifted=[];
		end	
	else
		NoShift=true;
		XShifted=X;
		TheValShifted=TheVal;
		TheAlphaShifted=AlphaMap;
	end
	
	
	NewAlpha=~isnan(TheValShifted);
	
	%Plot da shit
	%Smoothing if set
	if ~isempty(TheAlphaShifted)
		minVal=min(min(TheValShifted(~TheAlphaShifted&NewAlpha)));	
		maxVal=max(max(TheValShifted(~TheAlphaShifted&NewAlpha)));
	else
		minVal=min(min(TheValShifted(NewAlpha)));
		maxVal=max(max(TheValShifted(NewAlpha)));
	end
			
	caxislimit=[floor(10*minVal)/10 ceil(10*maxVal)/10];
	if isempty(caxislimit)|any(isnan(caxislimit))
		caxislimit='auto';
	end
	
	
	if CalcParameter.SmoothMode==1;
		%Smooth the map before plotting
		TheValNaN=isnan(TheValShifted);
				
		%use the mean of the data instead of 0
		if ~isempty(TheAlphaShifted)
			ValMean=mean(mean(TheValShifted(~TheAlphaShifted&NewAlpha)));
		else
			ValMean=mean(mean(TheValShifted(NewAlpha)));
		end
				
				
				
		if ~isempty(ValMean)
			TheValShifted(TheValNaN)=ValMean;
		else
			TheValShifted(TheValNaN)=0;
		end
				
				
				
		hFilter = fspecial('gaussian',CalcParameter.SmoothKernelSize, CalcParameter.SmoothSigma);
		TheValShifted= imfilter((TheValShifted), hFilter, 'replicate');
			
		%Smoothing adds to the total sum, subtract that to compensate.
		%TheVal=TheVal-obj.CalcParameter.SmoothFactor;
				
		TheValShifted(TheValNaN)=NaN;
		
		if ~isstr(caxislimit)
			%add the factor to caxislimit
			caxislimit=caxislimit;
		end
				
	end
	
	%limits
	xlimiter=[(min(XShifted))-0.1 (max(XShifted))+0.1];
	ylimiter=[(min(Y))-0.1 (max(Y))+0.1];
	
	xlab='Longitude ';
	ylab='Latitude ';
	
	
	
	xlim(plotAxis,xlimiter);
	ylim(plotAxis,ylimiter);
	
	latlim = get(plotAxis,'Ylim');
	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
	set(plotAxis,'DrawMode','fast');
			
			
	if ~isempty(TheAlphaShifted)
		TheValShifted(TheAlphaShifted)=NaN;
	end	
	
	
	
	PlotData={XShifted,Y,TheValShifted};
					
	%build config for the plot
	PlotConfig	= struct(	'PlotType','ColorPlot',...
					'Data',{PlotData},...
					'MapStyle','smooth',...
					'AlphaData','auto',...
					'X_Axis_Label',xlab,...
					'Y_Axis_Label',ylab,...
					'Colors','jet',...
					'X_Axis_Limit',xlimiter,...
					'Y_Axis_Limit',ylimiter,...
					'C_Axis_Limit',caxislimit,...
					'ColorToggle',true,...
					'LegendText',ValName);
	%disp(plotAxis)				
	[handle legendentry] = PlotColor(plotAxis,PlotConfig); 
	
	
	hold on;
	%Now the rest;
	%Border
	BorderConf= struct(	'PlotType','Border',...
	  			'Data',Datastore,...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','normal',...
				'Colors','black',...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
	[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
	set(BorderHand,'Marker','.','MarkerSize',4,'LineStyle','none');
	
	hold on
	CoastConf= struct( 	'PlotType','Coastline',...
				'Data',Datastore,...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','Fatline',...
				'Colors','black',...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
	[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
	set(CoastHand,'Marker','.','MarkerSize',4,'LineStyle','none');
					
	hold off;				
	
	
	set(plotAxis,'LineWidth',2,'FontSize',14);
	set(plotAxis,'YDir','normal');
	set(plotAxis,'Box','on','TickDir','out','TickLength'  ,[.015 .01] ,'XMinorTick','on','YMinorTick' ,...
			'on', 'YGrid','off','XGrid', 'off', 'XColor', [.3 .3 .3],'YColor', [.3 .3 .3]);
	latlim = [-90 90];%get(plotAxis,'Ylim');
	%set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
	
	TheName=[CalcName,': ',ValName,'  '];
	tit=title(plotAxis,TheName);
	set(tit,'FontSize',18,'FontWeight','bold','Interpreter','none');
	
		
	mybar=colorbar;
	set(mybar,'LineWidth',2,'FontSize',14);
	
	
	
end

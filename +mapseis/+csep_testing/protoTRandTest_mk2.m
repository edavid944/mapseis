function TheRes=protoTRandTest_mk2(ModelB,TheCatalog,signLevel,BinMode,NoiseComp,NumRand,WelchT,ParallelMode)
	%A prototype test it calculates the information gain against a random distribution 
	%with the same total value of expectancy
	
	%update similar to protoTTest_mk2 with a added Welch T-test
	%The support for Binmode is currently not available, also the Noise mode 
	%may not work correct if it comes to some additional output variable
	
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	%NoiseComp=false;
	
	if nargin<7
		NumRand=1;
	end
	
	if ParallelMode
		matlabpool open
	end
	
	if isempty(signLevel)
		% 5% standard alpha, and it is assumed to be one sided
		signLevel=0.05;
	end
	
	if ~NoiseComp
		%doing several calcs with uniform distro does not make sense
		NumRand=1;
	end
	
	
	if isempty(WelchT)
		% use a Welch T-test (unpaired and with unequal variances) instead
		% of 
		WelchT=false;
	end
	
	
	
	%  %use the formulas from Rhoades 2010 (so I hope)
	%  %now to the observations (ModelA and ModelB have to be on the same grid)
	
	%for safety set cells which are zero to 10^-24, that is more or less zero, but
	%still has a log not -Inf, I did not see models which have such low rates, so it should be outside
	ModelB(ModelB(:,9)==0,9)=realmin;
		
		
	
	%create the ModelA from Noise
	ModelA=ModelB;
	NumOfGrid=numel(ModelB(:,9));
	
	if NoiseComp
		ModelA(:,9)=rand(NumOfGrid,1);
	else
		ModelA(:,9)=ones(NumOfGrid,1);
	end
	
	
	%added column 10 if not defined (it should be)
	if numel(ModelB(1,:))<10
		ModelB(:,10)=true(numel(ModelB(:,1),1));
	end

	UsedBinsB=logical(ModelB(:,10));

	%set every bin not used in one of the  model to zero (to avoid that the
	%influence the total). They should not be used anyway. Set it up like this the 
	%Cells not used will be used will be zero but no cell used will be zero 
	%UnModRate=Mmodel(:,9);
	ModelB(~UsedBinsB,9)=0;
	ModelA(~UsedBinsB,9)=0;
	
	
	totalExp=sum(ModelB(UsedBinsB,9));
	
	
	%Norm it
	ModelA(:,9)=ModelA(:,9)/sum(ModelA(:,9))*totalExp;
	
	
	%Build perfect model
	EQinGrid=zeros(numel(ModelA(:,1)),1);
			
		
	for i=1:numel(ModelA(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>=ModelA(i,1)&TheCatalog(:,1)<ModelA(i,2);
		InLat=TheCatalog(:,2)>=ModelA(i,3)&TheCatalog(:,2)<ModelA(i,4);
		InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<ModelA(i,6);
		InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
			
			
		EQinGrid(i)=sum(InLon&InLat&InDepth&InMag&UsedBinsB(i));
			
			
	end
	
	PerfectModel=ModelA;
	PerfectModel(:,9)=EQinGrid;
	
	if BinMode
	
		TheObserver=zeros(numel(ModelA(:,1)),1);
			
		
		for i=1:numel(ModelA(:,1))
			%no parfor steps are probably to small
			InLon=TheCatalog(:,1)>=ModelA(i,1)&TheCatalog(:,1)<ModelA(i,2);
			InLat=TheCatalog(:,2)>=ModelA(i,3)&TheCatalog(:,2)<ModelA(i,4);
			InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<ModelA(i,6);
			InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
			
			
			TheObserver(i)=sum(InLon&InLat&InDepth&InMag&UsedBinsB(i));
			
		end
		
		
		
		Bin_w_Eq=TheObserver~=0;
		Ttable=tinv(1-signLevel,sum(Bin_w_Eq)-1);
		
		
		
		
		
		
		for i=1:NumRand
		
			%create the ModelA from Noise
			ModelA=ModelB;
			NumOfGrid=numel(ModelB(:,9));
			totalExp=sum(ModelB(UsedBinsB,9));
			
			if NoiseComp
				ModelA(:,9)=rand(NumOfGrid,1);
			else
				ModelA(:,9)=ones(NumOfGrid,1);
			end
			
			ModelA(~UsedBinsB,9)=0;
			
			%Norm it
			ModelA(:,9)=ModelA(:,9)/sum(ModelA(:,9))*totalExp;
			
			X_A=log(ModelA(Bin_w_Eq,9));
			X_B=log(ModelB(Bin_w_Eq,9));
			%InAB=1/sum(TheObserver)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(TheObserver);
			%SVar=1/(sum(TheObserver)-1)*sum((X_A-X_B).^2)-1/(sum(TheObserver)^2-sum(TheObserver))*sum(X_A-X_B)^2;
			%T=InAB/(sqrt(SVar)/sqrt(sum(TheObserver)));
			%Ttable=tinv(1-signLevel/2,sum(TheObserver)-1);
			InAB_raw(i)=1/sum(Bin_w_Eq)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(Bin_w_Eq);
			SVar_raw(i)=1/(sum(Bin_w_Eq)-1)*sum((X_A-X_B).^2)-1/(sum(Bin_w_Eq)^2-sum(Bin_w_Eq))*sum(X_A-X_B)^2;
			T_raw(i)=InAB_raw(i)/(sqrt(SVar_raw(i))/sqrt(sum(Bin_w_Eq)));
			
			%Calculate the perfect gain
			X_Per=log(PerfectModel(Bin_w_Eq,9));
			InAPer_raw(i)=1/sum(Bin_w_Eq)*sum(X_A-X_Per)-(sum(ModelA(:,9))-sum(PerfectModel(:,9)))/sum(Bin_w_Eq);
			SVarPer_raw(i)=1/(sum(Bin_w_Eq)-1)*sum((X_A-X_Per).^2)-1/(sum(Bin_w_Eq)^2-sum(Bin_w_Eq))*sum(X_A-X_Per)^2;
			TPer_raw(i)=InAPer_raw(i)/(sqrt(SVarPer_raw(i))/sqrt(sum(Bin_w_Eq)));
			
		end	
		
		
		
			
	else
		%get a probability for each earthquake in each model
				
	
		
		PosArray=1:NumOfGrid;
		SelArray=[];
		for i=1:numel(TheCatalog(:,1))
			LamA=NaN;
			LamB=NaN;
			InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
			InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
			InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
			InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
			
			if any(InLon&InLat&InDepth&InMag&UsedBinsB)
				SelArray(end+1)=PosArray(InLon&InLat&InDepth&InMag&UsedBinsB);
			end
			%disp(LamA);
			%disp(LamB);
			%LamdaA(i)=LamA;
			%LamdaB(i)=LamB;
			
		end
		
		
		%TotalNum=numel(TheCatalog(:,1));
		TotalNum=numel(SelArray);
		Ttable=tinv(1-signLevel,TotalNum-1);
		
		for j=1:NumRand
			LamdaA=[];
			LamdaB=[];	
		
			%create the ModelA from Noise
			ModelA=ModelB;
			NumOfGrid=numel(ModelB(:,9));
			totalExp=sum(ModelB(UsedBinsB,9));
			
			if NoiseComp
				ModelA(:,9)=rand(NumOfGrid,1);
			else
				ModelA(:,9)=ones(NumOfGrid,1);
			end
			
			ModelA(~UsedBinsB,9)=0;
			
			%Norm it
			ModelA(:,9)=ModelA(:,9)/sum(ModelA(:,9))*totalExp;
			
			%if any(InLon&InLat&InDepth&InMag)
			%	LamA=ModelA(InLon&InLat&InDepth&InMag,9);
			%	LamB=ModelB(InLon&InLat&InDepth&InMag,9);
			%end
			%disp(LamA);
			%disp(LamB);
			LamdaA=ModelA(SelArray,9);
			LamdaB=ModelB(SelArray,9);
		
		
			X_A=log(LamdaA);
			X_B=log(LamdaB);
			X_Per=log(PerfectModel(SelArray,9));
			
			%total expectation (actually the same, but I'm to lazy to
			%rewrite the codes snippets form protoTTest_mk2;
			N_A=sum(ModelA(:,9));
			N_B=sum(ModelB(:,9));			
			N_X=sum(PerfectModel(:,9));
			
			%normalized log(Lamda) for A and B and perfect model
			I_A=X_A-(N_A/TotalNum);
			I_B=X_B-(N_B/TotalNum);
			I_X=X_Per-(N_X/TotalNum);
			
			%average information gain and perfect gain
			InAB_raw(j)=mean(I_A-I_B);
			InAPer_raw(j)=mean(I_A-I_X);
			
			if ~WelchT
				%Student t-test
				SVar_raw(j)=var(I_A-I_B);
				T_raw(j)=InAB_raw(j)/(sqrt(SVar_raw(j))/sqrt(TotalNum));
				Ttable=tinv(1-signLevel,TotalNum-1);
				IConf_raw(j)= Ttable* SVar_raw(j).^(1/2) / sqrt(TotalNum);
				PVal_raw(j)=1-tcdf(abs(T_raw(j)),TotalNum-1);
				
				SVarPer_raw(j)=var(I_A-I_X);
				TPer_raw(j)=InAPer_raw(j)/(sqrt(SVarPer_raw(j))/sqrt(TotalNum));
				IConfPer_raw(j)= Ttable* SVarPer_raw(j).^(1/2) / sqrt(TotalNum);
				PValPer_raw(j)=1-tcdf(abs(TPer_raw(j)),TotalNum-1);
				
				Bin_w_Eq=NaN;
				
				%degree of freedom used (to be consistent with the welch)
				d_f=TotalNum-1;
				d_f2=TotalNum-1;
				
			else
				%Welch t-test
				
				%different variance is needed
				S_A=var(I_A);
				S_B=var(I_B);
				S_A_B=sqrt(S_A^2/TotalNum+S_B^2/TotalNum);
				
				%and also an other degree of freedom
				d_f   =  ((S_A^2/TotalNum+S_B^2/TotalNum)^2)/...
					 ((S_A^2/TotalNum)^2/(TotalNum-1)+...
					 (S_B^2/TotalNum)^2/(TotalNum-1));
				
				T_raw(j)=(mean(I_A)-mean(I_B))/S_A_B;
				
				Ttable=tinv(1-signLevel,d_f);
				IConf_raw(j)= Ttable* S_A_B;
				PVal_raw(j)=1-tcdf(T_raw(j),d_f);
				
				%perfect model stuff
				S_X=var(I_X);
				SVarPer_raw(j)=sqrt(S_A^2/TotalNum+S_X^2/TotalNum);
				
				
				%again degree of freedom (probalby the same as before)
				d_f2   =  ((S_A^2/TotalNum+S_X^2/TotalNum)^2)/...
					 ((S_A^2/TotalNum)^2/(TotalNum-1)+...
					 (S_X^2/TotalNum)^2/(TotalNum-1));
				TPer_raw(j)=InAPer_raw(j)/(sqrt(SVarPer_raw(j))/sqrt(TotalNum));
				IConfPer_raw(j)= Ttable* SVarPer_raw(j);
				PValPer_raw(j)=1-tcdf(abs(TPer_raw(j)),d_f);
				
				
				Bin_w_Eq=NaN;
				
				%to make packing easier
				SVar_raw(j)=S_A_B;
					
			end
			
			%InAB_raw(j)=1/TotalNum*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/TotalNum;
			%SVar_raw(j)=1/(TotalNum-1)*sum((X_A-X_B).^2)-1/(TotalNum^2-TotalNum)*sum(X_A-X_B)^2;
			%T_raw(j)=InAB_raw(j)/(sqrt(SVar_raw(j))/sqrt(TotalNum));
			
			%IConf_raw(j)= Ttable* SVar_raw(j).^(1/2) / sqrt(TotalNum);
			
			%Calculate the perfect gain
			
			%InAPer_raw(j)=1/TotalNum*sum(X_A-X_Per)-(sum(ModelA(:,9))-sum(PerfectModel(:,9)))/TotalNum;
			%SVarPer_raw(j)=1/(TotalNum-1)*sum((X_A-X_Per).^2)-1/(TotalNum^2-TotalNum)*sum(X_A-X_Per)^2;
			%TPer_raw(j)=InAPer_raw(j)/(sqrt(SVarPer_raw(j))/sqrt(TotalNum));
			%IConfPer_raw(j)= Ttable* SVarPer_raw(j).^(1/2) / sqrt(TotalNum);
			
			
			
			%Bin_w_Eq=NaN;
		
		end
	end
	
	%Addition Parameters with Perfect Gain
	PercentGain=round(InAB_raw./InAPer_raw*1000)./10;
	PercentConf=round(IConf_raw./InAPer_raw*10000)./100;
	
	TheRes.TestType='TRTest';
	TheRes.TestVersion=2;
	TheRes.Welch_Mode=WelchT;
	
	TheRes.SignLevel=signLevel;
	TheRes.Bins_w_Eq=sum(Bin_w_Eq);
	TheRes.InAB=mean(InAB_raw);
	TheRes.I_A=I_A;
	TheRes.I_B=I_B;
	TheRes.I_X=I_X;
	TheRes.Alibaba=log(LamdaA)-log(LamdaB);
	TheRes.VarianceI=mean(SVar_raw);
	TheRes.T=mean(T_raw);
	TheRes.p_org=mean(PVal_raw);
	TheRes.ttable=Ttable;
	TheRes.IConf=mean(IConf_raw);
	TheRes.MaxGain=mean(InAPer_raw);
	TheRes.IGainPerc=mean(PercentGain);
	TheRes.IConfPerc=mean(PercentConf);
	TheRes.Deg_of_Free=d_f;
	
	TheRes.AliPerfect=X_A-X_Per;
	TheRes.ExpPer=sum(PerfectModel(:,9));
	TheRes.p_per=mean(PValPer_raw);
	TheRes.Deg_of_Free_Per=d_f2;
	TheRes.TotalNum=TotalNum;
	%TheRes.ExpZeroCase=sum(ModelA(:,9)); It is equal to ExpModB anyway
	TheRes.ExpModB=sum(ModelB(:,9));
	
	if ParallelMode
		matlabpool close
	end
	
	

end

function RateEqMapper(Datastore,Model,Offset,TheName,plotAxis,altReshape)
	%plots the rates of model into a window and overlays coastline and borders and eq
	%AltReshape sets the alternative reshaping on, it is used for irregulare shape regions
	
	import mapseis.util.*;
	import mapseis.plot.*;
	
	if nargin<6
		altReshape=false;
		disp('set plotAxis to []')
	end	
	
	if nargin<5
		plotAxis=[];
		altReshape=false;
		disp('set plotAxis to []')
	end	
	
	if isempty(Offset)
		Offset=0;
	end	
	
	ModelConv=Model;
	
	%convert coordinates of the model if needed
	if Offset~=0
		ModelConv(:,[1,3])=ShiftCoords(Model(:,[1,3]),[Offset,0]);
		ModelConv(:,[2,4])=ShiftCoords(Model(:,[2,4]),[Offset,0]);	
	end
	
	xvec=unique(ModelConv(:,1));
	yvec=unique(ModelConv(:,3));
	
	if altReshape
		NrX=numel(xvec);
		NrY=numel(yvec);
		
		EmptyRegion=zeros(NrY,NrX);
		
		XMarker=zeros(numel(Model(:,1)),1);
		YMarker=zeros(numel(Model(:,1)),1);
		
		%Mark X coords
		for i=1:NrX
			thisOne=ModelConv(:,1)==xvec(i,1);
			XMarker(thisOne)=i;
		end
		
		%Mark Y coords
		for i=1:NrY
			thisOne=ModelConv(:,3)==yvec(i,1);
			YMarker(thisOne)=i;
		end		
		
		ModMap=EmptyRegion;
		ModMap((XMarker-1) * NrY + YMarker)=ModelConv(:,9);
		
	else
		ModMap=reshape(ModelConv(:,9),numel(yvec),numel(xvec));
	end
	
	%plot the stuff manually till I have modified the colorplot function
	if isempty(plotAxis)
		figure
		plotAxis=gca
	end
	%
	%MapHandle=image(xvec,yvec,log10(ModMap));
	MapHandle=image(xvec,yvec,(ModMap));
	shading flat
	TheAlpha=double(ModMap~=0);
	thresLam=(max(ModelConv(:,9))-min(ModelConv(:,9)))*0.05;
	TheAlpha(ModMap<=thresLam)=0.5;
	TheAlpha(ModMap==0)=0;
	alpha(TheAlpha);
	set(MapHandle,'CDataMapping','scaled');
	
	hold on
	
	%Border
	BorderConf= struct(	'PlotType','Border',...
	  			'Data',Datastore,...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','normal',...
				'Colors','black',...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
				
	[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
				
	hold on
	%CoastLine
	CoastConf= struct( 	'PlotType','Coastline',...
				'Data',Datastore,...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','Fatline',...
				'Colors','black',...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
				
	[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
	
	hold on
	NumEQ=Datastore.getRowCount;
	SendedEvents=true(NumEQ,1);
	xlimiter=[min(ModelConv(:,1)),max(ModelConv(:,2))];
	ylimiter=[min(ModelConv(:,3)),max(ModelConv(:,4))];
	
	%Earthquakes
	EqConfig= struct(	'PlotType','Earthquakes',...
				'Data',Datastore,...
				'PlotMode','old',...
				'PlotQuality','hi',...
				'SelectionSwitch','selected',...
				'SelectedEvents',SendedEvents,...
				'MarkedEQ','max',...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'MarkerSize','none',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit',xlimiter,...
				'Y_Axis_Limit',ylimiter,...
				'C_Axis_Limit','auto',...
				'LegendText',[],...
				'Use_Offset','manual',...
				'Manual_Offset',[[Offset 0]],...
				'PostLim',true);
	
	[handle1 entry1] = PlotEarthquake(plotAxis,EqConfig);
	%disp(handle1)
	%disp(entry1)
	%legend(handle1, entry1)
	set(plotAxis,'LineWidth',2,'FontSize',14);
	set(plotAxis,'YDir','normal');
	set(plotAxis,'Box','on','TickDir','out','TickLength'  ,[.015 .01] ,'XMinorTick','on','YMinorTick' ,...
			'on', 'YGrid','off','XGrid', 'off', 'XColor', [.3 .3 .3],'YColor', [.3 .3 .3]);
	latlim = get(plotAxis,'Ylim');
	set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);		
	
	if ~isempty(TheName)
		tit=title(plotAxis,TheName);
		set(tit,'FontSize',18,'FontWeight','bold','Interpreter','none');
	end
	
	mybar=colorbar;
	set(mybar,'LineWidth',2,'FontSize',14);
	
	hold off
	
	
end

function NumEv = poissrnd_limited(Lamdas,expectNumber)
	%Similar to poissrnd but creates only the expectNumber of events in
	%total if expectNumber=[] then the sum(Lamdas) will be used.
	
	%based on the Java code below (but does only one catalog)

%	    /**
%	     * Create a simulated catalog that is consistent with the rates specified
%	     * in the expectation vector, as described on p 1186 of Zechar et al 2010
%	     * (first full paragraph following eq 12)
%	     *
%	     * @param forecast vector of model rates for each lat/lon/mag bin
%	     * @param numberOfEventsToSimulate number of events to simulate
%	     * @return simulated catalog, represented as a vector with each entry
%	     *      representing the number of earthquakes occurring in each bin.
%	     */
%	    public static short[] simulatedEqkCatalog(float[] forecast,
%		    int numberOfEventsToSimulate) {
%		int bins = forecast.length;
%		Random rndgen = new Random();
%		float rnd = rndgen.nextFloat();
%	
%		// Normalize the expectations so that their sum is unity, use this
%		// construct to build the simulated catalog
%		float[] normalizedExpectations =
%			ArrayUtil.normalizeIgnoreNegativeValues(forecast);
%	
%		float[] cumulativeFractionConstruct =
%			new float[normalizedExpectations.length];
%		cumulativeFractionConstruct[0] = normalizedExpectations[0];
%		for (int i = 1; i < cumulativeFractionConstruct.length; i++) {
%		    cumulativeFractionConstruct[i] = cumulativeFractionConstruct[i - 1]
%			    + normalizedExpectations[i];
%		}
%	
%		// Simulate the catalog by drawing random numbers and mapping each
%		// random number to a given forecast bin, based on its normalized rate
%		short[] simulatedObservations = new short[bins];
%		for (int i = 0; i < numberOfEventsToSimulate; i++) {
%		    rnd = rndgen.nextFloat();
%		    for (int j = 0; j < normalizedExpectations.length; j++) {
%			if (rnd < cumulativeFractionConstruct[j]) {
%			    simulatedObservations[j]++;
%			    break;
%			}
%		    }
%		}
%		return simulatedObservations;
%	    }


	if isempty(expectNumber)
		expectNumber=round(nansum(Lamdas));
	end	

	%first normalize the Lamdas, so that there sum is unity
	totalExp=nansum(Lamdas);
	normLamdas=Lamdas./totalExp;
	
	%Init the cumulative Fraction
	%cumulativeFractionConstruct=zeros(size(normLamdas));
	cumulativeFractionConstruct=cumsum(normLamdas);
	
	%some stuff needed for vectorization
	countArray=1:numel(cumulativeFractionConstruct);
	
	NumEv=zeros(size(normLamdas));
	
	%now do the simulation with drawing numbers
	for i=1:expectNumber
		newRand=rand;
		lowCum=newRand<cumulativeFractionConstruct;
		
		%define lowest "higher rand"
		toInc=min(countArray(lowCum));
		
		NumEv(toInc)=NumEv(toInc)+1;
		
	end
	

end
    

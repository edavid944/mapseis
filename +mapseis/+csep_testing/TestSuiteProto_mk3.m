function Results=TestSuiteProto_mk3(Models,Names,Datastore,Filterlist,TestSwitch,MonteNum,numCatalogs,WelchT,ParallelMode)
	%This is a bit more complicated but it offers a lot more.
	%Models:	Cell array with models, number of models is not important
	%Names:		Cell array same length as 'Models', describes the field in the result structure
	%		under which the single models are saved, so no funny spaces and the like
	%Datastore:	This is the catalog, in case of MonteNum>1 it needs to be a catalog of the Randomizer
	%		subclass (e.g. CatalogRandimizerCMT)
	%Filterlist:	The Filterlist with which the catalog is cut after each randomizing step.
	%TestSwitch:	Is a structure with the fields Ntest,Stest,Ltest,Rtest,Ttest and Wtest (Mtest will be added later)
	%		each can be set to true or false in order to perform the test or not.
	%MonteNum:	Sets the number of Catalog variations which are used to calculation of the quantil error.
	%numCatalogs:	Sets the number of simulated Catalogs in the N-.S-,L- and R-test.
	%WelchT:	if true, a Welch T-test instead of a Student's T-test is used.
	%ParallelMode:	Sets Parallel Processing on and off.
	
	%Now Plotting in this function, it is done in another function.
	
	%New in mk3, support for Welch test and direct integration of p-values
	
	import mapseis.csep_testing.*;
	import mapseis.projector.*;
	
	
	if ParallelMode
		matlabpool open
	end
	
	
	NumRand=1;
	NoiseComp=false;
	
	%unpack and set the testswitchs
	NTestSwitch=false;
	STestSwitch=false;
	LTestSwitch=false;
	RTestSwitch=false;
	TTestSwitch=false;
	WTestSwitch=false;
	MTestSwitch=false;
	TRTestSwitch=false;
	TRInvTestSwitch=false;
	
	try 
		NTestSwitch=TestSwitch.Ntest;
	end
	
	try	
		STestSwitch=TestSwitch.Stest;
	end
	
	try 
		MTestSwitch=TestSwitch.Mtest;
	end
	
	try	
		LTestSwitch=TestSwitch.Ltest;
	end
	
	try
		RTestSwitch=TestSwitch.Rtest;
	end
	
	try
		TTestSwitch=TestSwitch.Ttest;
	end
	
	try
		WTestSwitch=TestSwitch.Wtest;
	end
	
	try
		TRTestSwitch=TestSwitch.TRtest;
	end
	
	try
		TRInvTestSwitch=TestSwitch.TRInvtest;
	end
	
	%set the Filterlist to the catalog
	Filterlist.changeData(Datastore);
	
	
	%The N-tests
	if NTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				NTestRes=protoNTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				Results.(['Ntest_',Names{i}])=NTestRes;
			end
			
		else	
			for i=1:numel(Models)
				Datastore.ResetShaker;
				
				%do the first pilot test
				Filterlist.updateNoEvent;
				selected=Filterlist.getSelected;
				ZmapCatalog=getZMAPFormat(Datastore,selected);
				NTestStart=protoNTest(Models{i},ZmapCatalog,numCatalogs,false,[]);

				NumberMonte=NaN(MonteNum,1);				
				QHighMonte=NaN(MonteNum,1);
				QLowMonte=NaN(MonteNum,1);
				numCatalogMonte=NaN(MonteNum,1);
				QHighMonte(1)=NTestStart.QScoreHigh;
				QLowMonte(1)=NTestStart.QScoreLow;
				NumberMonte(1)=NTestStart.LogLikeCatalog;
				numCatalogMonte(1)=NTestStart.numCatalogs;
				
				%Now the modified one
				parfor j=2:MonteNum
					%pertubate the catalog
					Datastore.ShakeCatalog;
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					%calc with refeed of the synthetic catalog loglikelihood
					N_Monty=protoNTest(Models{i},ZmapCatalog,numCatalogs,false,NTestStart.LogLikeModel);
					QHighMonte(j)=N_Monty.QScoreHigh;
					QLowMonte(j)=N_Monty.QScoreLow;
					NumberMonte(j)=N_Monty.LogLikeCatalog;
					numCatalogMonte(j)=N_Monty.numCatalogs;
				end
				
				
				
				%Now do the statistic and add the result
				
				%median 68% interval
				quantHigh=quantile(QHighMonte,[0.16,0.5,0.84]);
				quantLow=quantile(QLowMonte,[0.16,0.5,0.84]);
				quantCat=quantile(NumberMonte,[0.16,0.5,0.84]);
				
				
				NRes=NTestStart;
				NRes.QHighMonte=QHighMonte;
				NRes.QLowMonte=QLowMonte;
				NRes.QScoreHigh=mean(QHighMonte);
				NRes.QScoreLow=mean(QLowMonte);
				NRes.QScoreHigh_std_dev=std(QHighMonte);
				NRes.QScoreLow_std_dev=std(QLowMonte);
				NRes.LogLikeCatalog=mean(NumberMonte);
				NRes.LikeCatMonte=NumberMonte;
				NRes.LogLikeCatalog_std_dev=std(NumberMonte);
				
				NRes.QScoreHighMedian=quantHigh(2);
				NRes.QScoreHigh68Per=[quantHigh(1),quantHigh(3)];
				NRes.QScoreLowMedian=quantLow(2);
				NRes.QScoreLow68Per=[quantLow(1),quantLow(3)];
				NRes.LogLikeCatalogMedian=quantCat(2);
				NRes.LogLikeCatalog68Per=[quantCat(1),quantCat(3)];
				
				NRes.numCatalogMonte=numCatalogMonte;
				
				
				
				%write result
				Results.(['Ntest_',Names{i}])=NRes;
				
			end
		
		end
	end
	
	
	%The S-tests
	if STestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				STestRes=protoSTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				Results.(['Stest_',Names{i}])=STestRes;
			end
			
		else	
			for i=1:numel(Models)
				Datastore.ResetShaker;
				
				%do the first pilot test
				Filterlist.updateNoEvent;
				selected=Filterlist.getSelected;
				ZmapCatalog=getZMAPFormat(Datastore,selected);
				STestStart=protoSTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				
				QScoreMonte=NaN(MonteNum,1);
				numCatalogMonte=NaN(MonteNum,1);
				LikeCatMonte=NaN(MonteNum,1);
				QScoreMonte(1)=STestStart.QScore;
				numCatalogMonte(1)=STestStart.numCatalogs;
				LikeCatMonte(1)=STestStart.LogLikeCatalog;
				
				%Now the modified one
				parfor j=2:MonteNum
					%pertubate the catalog
					Datastore.ShakeCatalog;
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					%calc with refeed of the synthetic catalog loglikelihood
					S_Monty=protoSTest(Models{i},ZmapCatalog,numCatalogs,false,STestStart.LogLikeModel);
					QScoreMonte(j)=S_Monty.QScore;
					numCatalogMonte(j)=S_Monty.numCatalogs;
					LikeCatMonte(j)=S_Monty.LogLikeCatalog;
				end
				
				%Now do the statistic and add the result
				
				%median 68% interval
				quant=quantile(QScoreMonte,[0.16,0.5,0.84]);
				quantCat=quantile(LikeCatMonte,[0.16,0.5,0.84]);
				
				SRes=STestStart;
				SRes.QScoreMonte=QScoreMonte;
				SRes.QScore=mean(QScoreMonte);
				SRes.QScore_std_dev=std(QScoreMonte);
				SRes.QScoreMedian=quant(2);
				SRes.QScore68Per=[quant(1),quant(3)];
				
				SRes.LikeCatMean=mean(LikeCatMonte);
				SRes.LikeCat_std_dev=std(LikeCatMonte);
				SRes.LikeCatMedian=quantCat(2);
				SRes.LikeCat68Per=[quantCat(1),quantCat(3)];
				SRes.LogLikeCatMonte=LikeCatMonte;
				SRes.numCatalogMonte=numCatalogMonte;
				
				%write result
				Results.(['Stest_',Names{i}])=SRes;
				
			end
		
		end
	end
	
	
	%The M-tests
	if MTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				MTestRes=protoMTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				Results.(['Mtest_',Names{i}])=MTestRes;
			end
			
		else	
			for i=1:numel(Models)
				Datastore.ResetShaker;
				
				%do the first pilot test
				Filterlist.updateNoEvent;
				selected=Filterlist.getSelected;
				ZmapCatalog=getZMAPFormat(Datastore,selected);
				MTestStart=protoMTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				
				QScoreMonte=NaN(MonteNum,1);
				numCatalogMonte=NaN(MonteNum,1);
				LikeCatMonte=NaN(MonteNum,1);
				QScoreMonte(1)=MTestStart.QScore;
				numCatalogMonte(1)=MTestStart.numCatalogs;
				LikeCatMonte(1)=MTestStart.LogLikeCatalog;
				
				%Now the modified one
				parfor j=2:MonteNum
					%pertubate the catalog
					Datastore.ShakeCatalog;
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					%calc with refeed of the synthetic catalog loglikelihood
					M_Monty=protoMTest(Models{i},ZmapCatalog,numCatalogs,false,MTestStart.LogLikeModel);
					QScoreMonte(j)=M_Monty.QScore;
					numCatalogMonte(j)=M_Monty.numCatalogs;
					LikeCatMonte(j)=M_Monty.LogLikeCatalog;
				end
				
				%Now do the statistic and add the result
				
				%median 68% interval
				quant=quantile(QScoreMonte,[0.16,0.5,0.84]);
				quantCat=quantile(LikeCatMonte,[0.16,0.5,0.84]);
				
				MRes=MTestStart;
				MRes.QScoreMonte=QScoreMonte;
				MRes.QScore=mean(QScoreMonte);
				MRes.QScore_std_dev=std(QScoreMonte);
				MRes.QScoreMedian=quant(2);
				MRes.QScore68Per=[quant(1),quant(3)];
				
				MRes.LikeCatMean=mean(LikeCatMonte);
				MRes.LikeCat_std_dev=std(LikeCatMonte);
				MRes.LikeCatMedian=quantCat(2);
				MRes.LikeCat68Per=[quantCat(1),quantCat(3)];
				MRes.LogLikeCatMonte=LikeCatMonte;
				MRes.numCatalogMonte=numCatalogMonte;
				
				%write result
				Results.(['Mtest_',Names{i}])=MRes;
				
			end
		
		end
	end
	
	
	%The L-tests
	if LTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				LTestRes=protoLTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				Results.(['Ltest_',Names{i}])=LTestRes;
			end
			
		else	
			for i=1:numel(Models)
				Datastore.ResetShaker;
				
				%do the first pilot test
				Filterlist.updateNoEvent;
				selected=Filterlist.getSelected;
				ZmapCatalog=getZMAPFormat(Datastore,selected);
				LTestStart=protoLTest(Models{i},ZmapCatalog,numCatalogs,false,[]);
				
				QScoreMonte=NaN(MonteNum,1);
				numCatalogMonte=NaN(MonteNum,1);
				LikeCatMonte=NaN(MonteNum,1);
				QScoreMonte(1)=LTestStart.QScore;
				numCatalogMonte(1)=LTestStart.numCatalogs;
				LikeCatMonte(1)=LTestStart.LogLikeCatalog;
				
				%Now the modified one
				parfor j=2:MonteNum
					%pertubate the catalog
					Datastore.ShakeCatalog;
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					%calc with refeed of the synthetic catalog loglikelihood
					L_Monty=protoLTest(Models{i},ZmapCatalog,numCatalogs,false,LTestStart.LogLikeModel);
					QScoreMonte(j)=L_Monty.QScore;
					numCatalogMonte(j)=L_Monty.numCatalogs;
					LikeCatMonte(j)=L_Monty.LogLikeCatalog;
				end
				
				%Now do the statistic and add the result
				
				%median 68% interval
				quant=quantile(QScoreMonte,[0.16,0.5,0.84]);
				quantCat=quantile(LikeCatMonte,[0.16,0.5,0.84]);
				
				
				LRes=LTestStart;
				LRes.QScoreMonte=QScoreMonte;
				LRes.QScore=mean(QScoreMonte);
				LRes.QScore_std_dev=std(QScoreMonte);
				LRes.QScoreMedian=quant(2);
				LRes.QScore68Per=[quant(1),quant(3)];
				
				LRes.LikeCatMean=mean(LikeCatMonte);
				LRes.LikeCat_std_dev=std(LikeCatMonte);
				LRes.LikeCatMedian=quantCat(2);
				LRes.LikeCat68Per=[quantCat(1),quantCat(3)];
				LRes.LogLikeCatMonte=LikeCatMonte;
				LRes.numCatalogMonte=numCatalogMonte;
				
				%write result
				Results.(['Ltest_',Names{i}])=LRes;
				
			end
		
		end
		
		
		
		
	end
	
	
	
	
	%The R-tests
	if RTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				MySelf=strcmp(Names,Names{i});
				NotMyselfModels=Models(~MySelf);
				NotMyselfNames=Names(~MySelf);
				for j=1:numel(NotMyselfModels)
					RTestRes=protoRTest(Models{i},NotMyselfModels{j},ZmapCatalog,numCatalogs,false,[]);
					Results.(['Rtest_',Names{i},'_',NotMyselfNames{j}])=RTestRes;
				end
			end
			
		else	
			for i=1:numel(Models)
				MySelf=strcmp(Names,Names{i});
				NotMyselfModels=Models(~MySelf);
				NotMyselfNames=Names(~MySelf);
				
				for j=1:numel(NotMyselfModels)
					Datastore.ResetShaker;
					
					%do the first pilot test
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					RTestStart=protoRTest(Models{i},NotMyselfModels{j},ZmapCatalog,numCatalogs,false,[]);
					
					QScoreMonte=NaN(MonteNum,1);
					numCatalogMonte=NaN(MonteNum,1);
					LikeCatMonteA=NaN(MonteNum,1);
					LikeCatMonteB=NaN(MonteNum,1);
					Obs_RatioMonte=NaN(MonteNum,1);
					
					QScoreMonte(1)=RTestStart.QScoreRatioAB;
					numCatalogMonte(1)=RTestStart.numCatalogs;
					LikeCatMonteA(1)=RTestStart.LogLikeACatalog;
					LikeCatMonteB(1)=RTestStart.LogLikeBCatalog;
					Obs_RatioMonte(1)=RTestStart.ObsRatio_AB;
					
					%Now the modified one
					parfor k=2:MonteNum
						%pertubate the catalog
						Datastore.ShakeCatalog;
						Filterlist.updateNoEvent;
						selected=Filterlist.getSelected;
						ZmapCatalog=getZMAPFormat(Datastore,selected);
						
						%calc with refeed of the synthetic catalog loglikelihood
						R_Monty=protoRTest(Models{i},NotMyselfModels{j},ZmapCatalog,numCatalogs,false,RTestStart.Ratio_AB);
						QScoreMonte(k)=R_Monty.QScoreRatioAB;
						numCatalogMonte(k)=R_Monty.numCatalogs;
						LikeCatMonteA(k)=R_Monty.LogLikeACatalog;
						LikeCatMonteB(k)=R_Monty.LogLikeBCatalog;
						Obs_RatioMonte(k)=R_Monty.ObsRatio_AB;
					end
					
					%Now do the statistic and add the result
					
					%median 68% interval
					quant=quantile(QScoreMonte,[0.16,0.5,0.84]);
					quantCatA=quantile(LikeCatMonteA,[0.16,0.5,0.84]);
					quantCatB=quantile(LikeCatMonteB,[0.16,0.5,0.84]);
					quantRatioObs=quantile(Obs_RatioMonte,[0.16,0.5,0.84]);
					
					RRes=RTestStart;
					RRes.QScoreMonte=QScoreMonte;
					RRes.QScoreRatioAB=mean(QScoreMonte);
					RRes.QScoreRatioAB_std_dev=std(QScoreMonte);
					RRes.QScoreRatioABMedian=quant(2);
					RRes.QScoreRatioAB68Per=[quant(1),quant(3)];
					
					RRes.LikeCatAMean=mean(LikeCatMonteA);
					RRes.LikeCatA_std_dev=std(LikeCatMonteA);
					RRes.LikeCatAMedian=quantCatA(2);
					RRes.LikeCatA68Per=[quantCatA(1),quantCatA(3)];
					RRes.LogLikeCatAMonte=LikeCatMonteA;
					
					RRes.LikeCatBMean=mean(LikeCatMonteB);
					RRes.LikeCatB_std_dev=std(LikeCatMonteB);
					RRes.LikeCatBMedian=quantCatB(2);
					RRes.LikeCatB68Per=[quantCatB(1),quantCatB(3)];
					RRes.LogLikeCatBMonte=LikeCatMonteB;
					
					RRes.Obs_RatioMonteMean=mean(Obs_RatioMonte);
					RRes.Obs_RatioMonte_std_dev=std(Obs_RatioMonte);
					RRes.Obs_RatioMonteMedian=quantRatioObs(2);
					RRes.Obs_RatioMonte68Per=[quantRatioObs(1),quantRatioObs(3)];
					RRes.Obs_RatioMonte=Obs_RatioMonte;
					
					RRes.numCatalogMonte=numCatalogMonte;
					
					%write result
					Results.(['Rtest_',Names{i},'_',NotMyselfNames{j}])=RRes;
				end
			end
		
		end
		
		
		
		
	end
	
	
	
	if TTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				MySelf=strcmp(Names,Names{i});
				NotMyselfModels=Models(~MySelf);
				NotMyselfNames=Names(~MySelf);
				for j=1:numel(NotMyselfModels)
					TTestRes=protoTTest_mk2(Models{i},NotMyselfModels{j},ZmapCatalog,0.05,false,WelchT,false);
					Results.(['Ttest_',Names{i},'_',NotMyselfNames{j}])=TTestRes;
				end
			end
			
		else	
			for i=1:numel(Models)
				MySelf=strcmp(Names,Names{i});
				NotMyselfModels=Models(~MySelf);
				NotMyselfNames=Names(~MySelf);
				
				for j=1:numel(NotMyselfModels)
					Datastore.ResetShaker;
					
					%do the first pilot test
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					TTestStart=protoTTest_mk2(Models{i},NotMyselfModels{j},ZmapCatalog,0.05,false,WelchT,false);
					IGainMonte=NaN(MonteNum,1);
					IVarMonte=NaN(MonteNum,1);
					TValMonte=NaN(MonteNum,1);
					IConfMonte=NaN(MonteNum,1);
					TotalNum=NaN(MonteNum,1);
					MonteAli=cell(MonteNum,1);
					
					%new mk3
					PValMonte=NaN(MonteNum,1);
					I_A_Monte=cell(MonteNum,1);
					I_B_Monte=cell(MonteNum,1);
					DegFreedMonte=NaN(MonteNum,1);
					
					TValMonte(1)=TTestStart.T;
					IGainMonte(1)=TTestStart.InAB;
					IVarMonte(1)=TTestStart.VarianceI;
					IConfMonte(1)=TTestStart.IConf;
					TotalNum(1)=TTestStart.TotalNum;
					MonteAli{1}=TTestStart.Alibaba;
					
					%new mk3
					PValMonte(1)=TTestStart.p_org;
					I_A_Monte{1}=TTestStart.I_A;
					I_B_Monte{1}=TTestStart.I_B;
					DegFreedMonte(1)=TTestStart.Deg_of_Free;
					
					
					%Now the modified one
					parfor k=2:MonteNum
						%pertubate the catalog
						Datastore.ShakeCatalog;
						Filterlist.updateNoEvent;
						selected=Filterlist.getSelected;
						ZmapCatalog=getZMAPFormat(Datastore,selected);
						
						%calc with refeed of the synthetic catalog loglikelihood
						T_Monty=protoTTest_mk2(Models{i},NotMyselfModels{j},ZmapCatalog,0.05,false,WelchT,false);
						TValMonte(k)=T_Monty.T;
						IGainMonte(k)=T_Monty.InAB;
						IVarMonte(k)=T_Monty.VarianceI;
						IConfMonte(k)=T_Monty.IConf;
						TotalNum(k)=T_Monty.TotalNum;
						MonteAli{k}=T_Monty.Alibaba;
						
						%new mk3
						PValMonte(k)=T_Monty.p_org;
						I_A_Monte{k}=T_Monty.I_A;
						I_B_Monte{k}=T_Monty.I_B;
						DegFreedMonte(k)=T_Monty.Deg_of_Free;
					end
					
					%Now do the statistic and add the result
					
					%median 68% interval
					quantInAB=quantile(IGainMonte,[0.16,0.5,0.84]);
					quantIVar=quantile(IVarMonte,[0.16,0.5,0.84]);
					quantT=quantile(TValMonte,[0.16,0.5,0.84]);
					quantIConf=quantile(IConfMonte,[0.16,0.5,0.84]);
					
					%new mk3
					quantP=quantile(PValMonte,[0.16,0.5,0.84]);
					
					TRes=TTestStart;
					TRes.TValMonte=TValMonte;
					TRes.InABMonte=IGainMonte;
					TRes.VarI=IVarMonte;
					TRes.IConfMonte=IConfMonte;
					TRes.InAB=mean(IGainMonte);
					TRes.VarianceI=mean(IVarMonte);
					TRes.InAB_Mostd=std(IGainMonte);
					TRes.VarI_Mostd=std(IVarMonte);
					TRes.T=mean(TValMonte);
					TRes.T_std_dev=std(TValMonte);
					TRes.IConf=mean(IConfMonte);
					TRes.IConf_std=std(IConfMonte);
					
					TRes.InABMedian=quantInAB(2);
					TRes.InAB68Per=[quantInAB(1),quantInAB(3)];
					TRes.VarianceIMedian=quantIVar(2);
					TRes.VarianceI68Per=[quantIVar(1),quantIVar(3)];
					TRes.TMedian=quantT(2);
					TRes.T68Per=[quantT(1),quantT(3)];
					TRes.IConfMedian=quantIConf(2);
					TRes.IConf68Per=[quantIConf(1),quantIConf(3)];
					
					%needed for Combining years, AliMonte is a bit larger,
					%around ~2-10Mb but it cannot be prevented
					TRes.AliMonte=MonteAli;
					TRes.TotalNum=TotalNum;
					
					%new for mk3
					%P-value related (same as AddPval)
					TRes.PValMonte=PValMonte;
					TRes.p_mean=mean(PValMonte);
					TRes.p_std=std(PValMonte);
					TRes.p_median=quantP(2);
					TRes.p_Conf68Per=[quantP(1),quantP(3)];
					
					%rest (no average needed)
					TRes.I_A_Monte=I_A_Monte;
					TRes.I_B_Monte=I_B_Monte;
					TRes.DegFreedMonte=DegFreedMonte;
					
					%write result
					Results.(['Ttest_',Names{i},'_',NotMyselfNames{j}])=TRes;
				end
			end
		
		end
		
		
		
		
	end
	
	
	
	
	
	if WTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				MySelf=strcmp(Names,Names{i});
				NotMyselfModels=Models(~MySelf);
				NotMyselfNames=Names(~MySelf);
				for j=1:numel(NotMyselfModels)
					WTestRes=protoWTest(Models{i},NotMyselfModels{j},ZmapCatalog,0.05,false,false);
					Results.(['Wtest_',Names{i},'_',NotMyselfNames{j}])=WTestRes;
				end
			end
			
		else	
			for i=1:numel(Models)
				MySelf=strcmp(Names,Names{i});
				NotMyselfModels=Models(~MySelf);
				NotMyselfNames=Names(~MySelf);
				
				for j=1:numel(NotMyselfModels)
					Datastore.ResetShaker;
					
					%do the first pilot test
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					WTestStart=protoWTest(Models{i},NotMyselfModels{j},ZmapCatalog,0.05,false,false);
					
					W_plus_Monte=NaN(MonteNum,1);
					W_plus_Monte(1)=WTestStart.W_plus;
					W_minus_Monte=NaN(MonteNum,1);
					W_minus_Monte(1)=WTestStart.W_minus;
					W_overall_Monte=NaN(MonteNum,1);
					W_overall_Monte(1)=WTestStart.W_overall;
					
					%new mk3 (p-value stuff)
					PValMonte=NaN(MonteNum,1);
					
					
					TotalNum=NaN(MonteNum,1);
					MonteAli=cell(MonteNum,1);
					TotalNum(1)=WTestStart.TotalNum;
					MonteAli{1}=WTestStart.RawDiffs;

					%new mk3 (p-value stuff)
					PValMonte(1)=WTestStart.p_org;
					
					%Now the modified one
					parfor k=2:MonteNum
						%pertubate the catalog
						Datastore.ShakeCatalog;
						Filterlist.updateNoEvent;
						selected=Filterlist.getSelected;
						ZmapCatalog=getZMAPFormat(Datastore,selected);
						
						%calc with refeed of the synthetic catalog loglikelihood
						W_Monty=protoWTest(Models{i},NotMyselfModels{j},ZmapCatalog,0.05,false,false);
						W_plus_Monte(k)=W_Monty.W_plus;
						W_minus_Monte(k)=W_Monty.W_minus;
						W_overall_Monte(k)=W_Monty.W_overall;
						TotalNum(k)=W_Monty.TotalNum;
						MonteAli{k}=W_Monty.RawDiffs;
						
						%new mk3 (p-value stuff)
						PValMonte(k)=W_Monty.p_org;
					end
					
					%Now do the statistic and add the result
					
					%median 68% interval
					quantWP=quantile(W_plus_Monte,[0.16,0.5,0.84]);
					quantWM=quantile(W_minus_Monte,[0.16,0.5,0.84]);
					quantWO=quantile(W_overall_Monte,[0.16,0.5,0.84]);
					
					%new mk3
					quantP=quantile(PValMonte,[0.16,0.5,0.84]);
					
					
					WRes=WTestStart;
					WRes.W_plus_Monte=W_plus_Monte;
					WRes.W_minus_Monte=W_minus_Monte;
					WRes.W_overall_Monte=W_overall_Monte;
					
					WRes.W_plus=mean(W_plus_Monte);
					WRes.W_plus_std_dev=std(W_plus_Monte);
					WRes.W_minus=mean(W_minus_Monte);
					WRes.W_minus_std_dev=std(W_minus_Monte);
					WRes.W_overall=mean(W_overall_Monte);
					WRes.W_overall_std_dev=std(W_overall_Monte);
					
					WRes.W_PlusMedian=quantWP(2);
					WRes.W_Plus68Per=[quantWP(1),quantWP(3)];
					WRes.W_MinusMedian=quantWM(2);
					WRes.W_Minus68Per=[quantWM(1),quantWM(3)];
					WRes.W_OverallMedian=quantWO(2);
					WRes.W_Overall68Per=[quantWO(1),quantWO(3)];
					
					
					%needed for Combining years, AliMonte is a bit larger,
					%around ~2-10Mb but it cannot be prefented
					WRes.AliMonte=MonteAli;
					WRes.TotalNum=TotalNum;
					
					%new for mk3
					%P-value related (same as AddPval)
					WRes.PValMonte=PValMonte;
					WRes.p_mean=mean(PValMonte);
					WRes.p_std=std(PValMonte);
					WRes.p_median=quantP(2);
					WRes.p_Conf68Per=[quantP(1),quantP(3)];
					
					
					%write result
					Results.(['Wtest_',Names{i},'_',NotMyselfNames{j}])=WRes;
				end
			end
		
		end
		
		
		
		
	end
	
	
	
	if TRTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				
				
				TRTestRes=protoTRandTest_mk2(Models{i},ZmapCatalog,0.05,false,NoiseComp,NumRand,WelchT,false);
				Results.(['TRtest_',Names{i}])=TRTestRes;
			end
			
		else	
			for i=1:numel(Models)

				Datastore.ResetShaker;
				
				%do the first pilot test
				Filterlist.updateNoEvent;
				selected=Filterlist.getSelected;
				ZmapCatalog=getZMAPFormat(Datastore,selected);
				
				TRTestStart=protoTRandTest_mk2(Models{i},ZmapCatalog,0.05,false,NoiseComp,NumRand,WelchT,false);
				IGainMonte=NaN(MonteNum,1);
				IVarMonte=NaN(MonteNum,1);
				TValMonte=NaN(MonteNum,1);
				IConfMonte=NaN(MonteNum,1);
				MaxGain=NaN(MonteNum,1);
				IGainPerc=NaN(MonteNum,1);
				IConfPerc=NaN(MonteNum,1);
				TotalNum=NaN(MonteNum,1);
				ExpPer=NaN(MonteNum,1);
				MonteAli=cell(MonteNum,1);
				MontePer=cell(MonteNum,1);
				
				%new mk3
				PValMonte=NaN(MonteNum,1);
				PValPerMonte=NaN(MonteNum,1);
				I_A_Monte=cell(MonteNum,1);
				I_B_Monte=cell(MonteNum,1);
				I_X_Monte=cell(MonteNum,1);
				DegFreedMonte=NaN(MonteNum,1);
				DegFreedPerMonte=NaN(MonteNum,1);
				
				TValMonte(1)=TRTestStart.T;
				IGainMonte(1)=TRTestStart.InAB;
				IVarMonte(1)=TRTestStart.VarianceI;
				IConfMonte(1)=TRTestStart.IConf;
				MaxGain(1)=TRTestStart.MaxGain;
				IGainPerc(1)=TRTestStart.IGainPerc;
				IConfPerc(1)=TRTestStart.IConfPerc;
				TotalNum(1)=TRTestStart.TotalNum;
				ExpPer(1)=TRTestStart.ExpPer;
				MonteAli{1}=TRTestStart.Alibaba;
				MontePer{1}=TRTestStart.AliPerfect;
				
				%new mk3
				PValMonte(1)=TRTestStart.p_org;
				PValPerMonte(1)=TRTestStart.p_per;
				I_A_Monte{1}=TRTestStart.I_A;
				I_B_Monte{1}=TRTestStart.I_B;
				I_X_Monte{1}=TRTestStart.I_X;
				DegFreedMonte(1)=TRTestStart.Deg_of_Free;
				DegFreedPerMonte(1)=TRTestStart.Deg_of_Free_Per;
				
				
				%Now the modified one
				parfor k=2:MonteNum
					%pertubate the catalog
					Datastore.ShakeCatalog;
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					%calc with refeed of the synthetic catalog loglikelihood
					TR_Monty=protoTRandTest_mk2(Models{i},ZmapCatalog,0.05,false,NoiseComp,NumRand,WelchT,false);
					TValMonte(k)=TR_Monty.T;
					IGainMonte(k)=TR_Monty.InAB;
					IVarMonte(k)=TR_Monty.VarianceI;
					IConfMonte(k)=TR_Monty.IConf;
					MaxGain(k)=TR_Monty.MaxGain;
					IGainPerc(k)=TR_Monty.IGainPerc;
					IConfPerc(k)=TR_Monty.IConfPerc;
					TotalNum(k)=TR_Monty.TotalNum;
					ExpPer(k)=TR_Monty.ExpPer;
					MonteAli{k}=TR_Monty.Alibaba;
					MontePer{k}=TR_Monty.AliPerfect;
					
					%new mk3
					PValMonte(k)=TR_Monty.p_org;
					PValPerMonte(k)=TR_Monty.p_per;
					I_A_Monte{k}=TR_Monty.I_A;
					I_B_Monte{k}=TR_Monty.I_B;
					I_X_Monte{k}=TR_Monty.I_X;
					DegFreedMonte(k)=TR_Monty.Deg_of_Free;
					DegFreedPerMonte(k)=TR_Monty.Deg_of_Free_Per;
					
				end
				
				%Now do the statistic and add the result
				
				%median 68% interval
				quantInAB=quantile(IGainMonte,[0.16,0.5,0.84]);
				quantIVar=quantile(IVarMonte,[0.16,0.5,0.84]);
				quantT=quantile(TValMonte,[0.16,0.5,0.84]);
				quantIConf=quantile(IConfMonte,[0.16,0.5,0.84]);
				quantMaxGain=quantile(MaxGain,[0.16,0.5,0.84]);
				quantIGainPer=quantile(IGainPerc,[0.16,0.5,0.84]);
				quantIConfPer=quantile(IConfPerc,[0.16,0.5,0.84]);
				
				%new mk3
				quantP=quantile(PValMonte,[0.16,0.5,0.84]);				
				quantPer=quantile(PValPerMonte,[0.16,0.5,0.84]);	
				
				TRRes=TRTestStart;
				TRRes.TValMonte=TValMonte;
				TRRes.InABMonte=IGainMonte;
				TRRes.VarI=IVarMonte;
				TRRes.IConfMonte=IConfMonte;
				TRRes.MaxGainMonte=MaxGain;
				TRRes.IGainPercMonte=IGainPerc;
				TRRes.IConfPercMonte=IConfPerc;
				
				TRRes.InAB=mean(IGainMonte);
				TRRes.VarianceI=mean(IVarMonte);
				TRRes.InAB_Mostd=std(IGainMonte);
				TRRes.VarI_Mostd=std(IVarMonte);
				TRRes.T=mean(TValMonte);
				TRRes.T_std_dev=std(TValMonte);
				TRRes.IConf=mean(IConfMonte);
				TRRes.IConf_std=std(IConfMonte);
				TRRes.MaxGain=mean(MaxGain);
				TRRes.MaxGain_Mostd=std(MaxGain);
				TRRes.IGainPerc=mean(IGainPerc);
				TRRes.IGainPerc_Mostd=std(IGainPerc);
				TRRes.IConfPerc=mean(IConfPerc);
				TRRes.IConfPerc_Mostd=std(IConfPerc);
				
				TRRes.InABMedian=quantInAB(2);
				TRRes.InAB68Per=[quantInAB(1),quantInAB(3)];
				TRRes.VarianceIMedian=quantIVar(2);
				TRRes.VarianceI68Per=[quantIVar(1),quantIVar(3)];
				TRRes.TMedian=quantT(2);
				TRRes.T68Per=[quantT(1),quantT(3)];
				TRRes.IConfMedian=quantIConf(2);
				TRRes.IConf68Per=[quantIConf(1),quantIConf(3)];
				TRRes.MaxGainMedian=quantMaxGain(2);
				TRRes.MaxGain68Per=[quantMaxGain(1),quantMaxGain(3)];
				TRRes.IGainPercMedian=quantIGainPer(2);
				TRRes.IGainPerc68Per=[quantIGainPer(1),quantIGainPer(3)];
				TRRes.IConfPercMedian=IConfPerc(2);
				TRRes.ICOnfPerc68Per=[IConfPerc(1),IConfPerc(3)];
				
				TRRes.AliMonte=MonteAli;
				TRRes.MontePer=MontePer;
				TRRes.ExpPerMonte=ExpPer;
				TRRes.TotalNum=TotalNum;
				
				%new for mk3
				%P-value related (same as AddPval)
				TRRes.PValMonte=PValMonte;
				TRRes.p_mean=mean(PValMonte);
				TRRes.p_std=std(PValMonte);
				TRRes.p_median=quantP(2);
				TRRes.p_Conf68Per=[quantP(1),quantP(3)];
				
				TRRes.PValPerMonte=PValPerMonte;
				TRRes.p_per_mean=mean(PValPerMonte);
				TRRes.p_per_std=std(PValPerMonte);
				TRRes.p_per_median=quantPer(2);
				TRRes.p_per_Conf68Per=[quantPer(1),quantPer(3)];
				
				%rest (no average needed)
				TRRes.I_A_Monte=I_A_Monte;
				TRRes.I_B_Monte=I_B_Monte;
				TRRes.I_X_Monte=I_X_Monte;
				TRRes.DegFreedMonte=DegFreedMonte;
				TRRes.DegFreedPerMonte=DegFreedPerMonte;
				
				%write result
				Results.(['TRtest_',Names{i}])=TRRes;
				
			end
		
		end
		
		
		
		
	end
	
	
	%inverted TRtest (everycell NOT containing a eq will be used)	
	if TRInvTestSwitch
		if MonteNum==1
			%get the catalog	
			Filterlist.updateNoEvent;
			selected=Filterlist.getSelected;
			ZmapCatalog=getZMAPFormat(Datastore,selected);
			
			for i=1:numel(Models)
				
				
				TRTestRes=protoInvTRandTest(Models{i},ZmapCatalog,0.05,false,NoiseComp,NumRand,false);
				Results.(['TRInvtest_',Names{i}])=TRTestRes;
			end
			
		else	
			for i=1:numel(Models)

				Datastore.ResetShaker;
				
				%do the first pilot test
				Filterlist.updateNoEvent;
				selected=Filterlist.getSelected;
				ZmapCatalog=getZMAPFormat(Datastore,selected);
				
				TRTestStart=protoInvTRandTest(Models{i},ZmapCatalog,0.05,false,NoiseComp,NumRand,false);
				IGainMonte=NaN(MonteNum,1);
				IVarMonte=NaN(MonteNum,1);
				TValMonte=NaN(MonteNum,1);
				IConfMonte=NaN(MonteNum,1);
				TotalNum=NaN(MonteNum,1);
				MonteAli=cell(MonteNum,1);
				TValMonte(1)=TRTestStart.T;
				IGainMonte(1)=TRTestStart.InAB;
				IVarMonte(1)=TRTestStart.VarianceI;
				IConfMonte(1)=TRTestStart.IConf;
				TValMonte(1)=TRTestStart.T;
				TotalNum(1)=TRTestStart.TotalNum;
				MonteAli{1}=TRTestStart.Alibaba;
				
				%Now the modified one
				parfor k=2:MonteNum
					%pertubate the catalog
					Datastore.ShakeCatalog;
					Filterlist.updateNoEvent;
					selected=Filterlist.getSelected;
					ZmapCatalog=getZMAPFormat(Datastore,selected);
					
					%calc with refeed of the synthetic catalog loglikelihood
					TR_Monty=protoInvTRandTest(Models{i},ZmapCatalog,0.05,false,NoiseComp,NumRand,false);
					TValMonte(k)=TR_Monty.T;
					IGainMonte(k)=TR_Monty.InAB;
					IVarMonte(k)=TR_Monty.VarianceI;
					IConfMonte(k)=TR_Monty.IConf;
					TotalNum(k)=TR_Monty.TotalNum;
					MonteAli{k}=TR_Monty.Alibaba;
				end
				
				%Now do the statistic and add the result
				
				%median 68% interval
				quantInAB=quantile(IGainMonte,[0.16,0.5,0.84]);
				quantIVar=quantile(IVarMonte,[0.16,0.5,0.84]);
				quantT=quantile(TValMonte,[0.16,0.5,0.84]);
				quantIConf=quantile(IConfMonte,[0.16,0.5,0.84]);
				
				TRRes=TRTestStart;
				TRRes.TValMonte=TValMonte;
				TRRes.InABMonte=IGainMonte;
				TRRes.VarI=IVarMonte;
				TRRes.IConfMonte=IConfMonte;
				TRRes.InAB=mean(IGainMonte);
				TRRes.VarianceI=mean(IVarMonte);
				TRRes.InAB_Mostd=std(IGainMonte);
				TRRes.VarI_Mostd=std(IVarMonte);
				TRRes.T=mean(TValMonte);
				TRRes.T_std_dev=std(TValMonte);
				TRRes.IConf=mean(IConfMonte);
				TRRes.IConf_std=std(IConfMonte);
				
				TRRes.InABMedian=quantInAB(2);
				TRRes.InAB68Per=[quantInAB(1),quantInAB(3)];
				TRRes.VarianceIMedian=quantIVar(2);
				TRRes.VarianceI68Per=[quantIVar(1),quantIVar(3)];
				TRRes.TMedian=quantT(2);
				TRRes.T68Per=[quantT(1),quantT(3)];
				TRRes.IConfMedian=quantIConf(2);
				TRRes.IConf68Per=[quantIConf(1),quantIConf(3)];
					
				%needed for Combining years, AliMonte is a bit larger,
				%around ~2-10Mb but it cannot be prefented
				TRRes.AliMonte=MonteAli;
				TRRes.TotalNum=TotalNum;
					
				
				
				%write result
				Results.(['TRInvtest_',Names{i}])=TRRes;
				
			end
		
		end
		
		
		
		
	end

	
	
	%Add additional Data
	Results.Names=Names;
	Results.Nr_MonteCarlo=MonteNum;
	Results.Simulate_Catalog=numCatalogs;
	
	
	
	%reset the data
	Filterlist.PackIt;
	
	if MonteNum~=1
		Datastore.ResetShaker;
	end
	
	

	
	if ParallelMode
		matlabpool close
	end
	
	

	
	
	
end

function ConsistTestPlotter_mk2(Results,TheFold,FilePrefix,PlotConfig,PaperBoy)
	%A new way to plot results of the conistency test
	
	import mapseis.plot.*;
	import mapseis.csep_testing.*;
	import mapseis.util.*;
	
	
	FailCol='r';
	%MeanCol=[0.6 0.8 0.6];
	%MeanCol2=[0.2 0.8 0.2];
	MeanCol=[0.8 0.6 0.8];
	MeanCol2=[0.8 0.2 0.8];
	MedCol=[0.6 0.6 0.8];
	MedCol2=[0.2 0.2 0.8];
	
	Tranpa=0.5;
	
	RoundIt=true;
	
	SuccessCol='g';
	
	if iscell(Results)
		Results=StructureMerger(Results,true);
	end
	
	
	
	%Needed before in case of not PlotConfig is given
	ModelNames=Results.Names;
	
	if nargin<5
		PaperBoy=false;
		PlotConfig=struct( 'Models',{ModelNames},...
				   'TestsIncluded',{{'Ntest','Stest','Ltest'}},...
				   'WriteToFile',true,...
				   'KeepFig',false,...
				   'ShowDist',true,...
				   'ShowOriginal',true,...
				   'ShowMean',false,...
				   'ShowMedian',true,...
				   'NTestNumber',false,...
				   'PlotTitle',[[]],...
				   'TestLabels',{{}},...
				   'Quantil',0.05,...
				   'RoundIt',true);
				   
	end	
	
	if nargin<4
		PaperBoy=false;
	end	
	
	
	%unpack=
	TestsIncluded=PlotConfig.TestsIncluded;
	NTestNumber=PlotConfig.NTestNumber;
	ShowMean=PlotConfig.ShowMean;
	ShowMedian=PlotConfig.ShowMedian;
	ShowOriginal=PlotConfig.ShowOriginal;
	ShowDist=PlotConfig.ShowDist;
	PlotTitle='Consistency Tests: ';
	WriteToFile=PlotConfig.WriteToFile;
	KeepFig=PlotConfig.KeepFig;
	
	oldFold=pwd;
	
	if isfield(PlotConfig,'RoundIt')
		RoundIt=PlotConfig.RoundIt;
	end
	
		
	%for setting the Ylabel rights
	%set(gca,'YTick',plotyPos,'YTickLabel',{'N','S','M','L'},'FontSize',16,'FontWeight','bold')

	
					
	if isfield(PlotConfig,'PlotTitle')
		ResStruct.DisplayName=PlotConfig.PlotTitle;
		RawTitle=PlotConfig.PlotTitle;
	end
	
	%Add some default plotnames (Modelnames without '_')
	for i=1:numel(ModelNames)
		NewName=ModelNames{i};
		NewName(NewName=='_')=' ';
		PlotNames{i}=NewName;
	end
	
	
	for i=1:numel(TestsIncluded)
		TLabel=TestsIncluded{i};
		TLabel(TLabel=='_')=' ';
		TestLabels{i}=TLabel;
	end	

	
	if isfield(PlotConfig,'PlotNames')
		if ~isempty(PlotConfig.PlotNames)
			PlotNames=PlotConfig.PlotNames;
		end	
	end

	
	%Default value for Quantil
	Quant=0.05;
	
	if isfield(PlotConfig,'Quantil')
		if ~isempty(PlotConfig.Quantil)
			Quant=PlotConfig.Quantil;
		end
	end
				
	
	if isfield(PlotConfig,'TestLabels')
		if ~isempty(PlotConfig.TestLabels)
			TestLabels=PlotConfig.TestLabels;
		end
	end
	
	
	if isfield(PlotConfig,'PlotTitle')
		if ~isempty(PlotConfig.PlotTitle)
			PlotTitle=PlotConfig.PlotTitle;
		end
	end
	
	if isfield(PlotConfig,'Models')
		if ~isempty(PlotConfig.Models)
			PlotNames=PlotConfig.Models;
		end
	end
	
	
	
	%generate Data needed
	OneOrTwo=false(size(TestsIncluded));
	IamN=strcmp(TestsIncluded,'Ntest');
	if any(IamN)
		OneOrTwo(IamN)=true;
	end
	
	numTests=numel(TestsIncluded);
	TestSpacing=2;
	HalfSpacer=TestSpacing/2;
	LinCor=0.025;
	
	for j=1:numel(ModelNames)
		
			
		fig=figure;
		plotAxis=gca;
		
		%A bit ackward but it has to be done like this
		for k=1:numel(TestsIncluded)
			TestName=[TestsIncluded{k},'_',ModelNames{j}];
			Testdat=Results.(TestName);
			
			if Results.Nr_MonteCarlo==1&strcmp(TestsIncluded{k},'Ntest')
				Results.(TestName).QLowMonte=Testdat.QScoreLow(1);
				Results.(TestName).QHighMonte=Testdat.QScoreHigh(1);
				Testdat=Results.(TestName);
						
			elseif 	Results.Nr_MonteCarlo==1&~strcmp(TestsIncluded{k},'Ntest')
				Results.(TestName).QScoreMonte=Testdat.QScore(1);
				Testdat=Results.(TestName);
				
			end
			
			
			if RoundIt
				%round the scores to 0.005 for the check
				if strcmp(TestsIncluded{k},'Ntest');
					LowQ=round(Testdat.QLowMonte(1)*200)/200;
					HiQ=round(Testdat.QHighMonte(1)*200)/200;
					%Passed(k)=(LowQ>=Quant/2)&(HiQ<=(1-Quant/2));
					%changed for a saver way to determine if it passed
					%because the scores are "symmetric" the test
					%is passed if both scores are above Quant/2.
					Passed(k)=(LowQ>=Quant/2)&(HiQ>=(Quant/2));
					
				else
					
					MrQ=round(Testdat.QScoreMonte(1)*200)/200;
					Passed(k)=MrQ>=Quant;
				
				end
			
			else
			
				if strcmp(TestsIncluded{k},'Ntest');
					%Passed(k)=(Testdat.QLowMonte(1)>=Quant/2)&(Testdat.QHighMonte(1)<=(1-Quant/2));
					Passed(k)=(Testdat.QLowMonte(1)>=Quant/2)&(Testdat.QHighMonte(1)>=(Quant/2));
						
				else
					
					Passed(k)=Testdat.QScoreMonte(1)>=Quant;
				
				end
			end
		end
		
		%build raw plot
		plotyPos=InitConsPlot(plotAxis,numTests,TestSpacing,Quant,OneOrTwo,Passed);
		
		hold on;
		
		%now go through every test
		for k=1:numel(TestsIncluded)
			TestName=[TestsIncluded{k},'_',ModelNames{j}];
			Testdat=Results.(TestName);
			
			if strcmp(TestsIncluded{k},'Ntest');
				if NTestNumber
					%Data needs to be "normalized"
					minNum=min(Testdat.LogLikeModel);
					maxNum=max(Testdat.LogLikeModel);
					NumDist=abs(maxNum-minNum);
					if Results.Nr_MonteCarlo>1
						OrgVal=(Testdat.LikeCatMonte(1)-minNum)/NumDist;
						MeanVal=(Testdat.LogLikeCatalog-minNum)/NumDist;
						StdVal=(Testdat.LogLikeCatalog_std_dev-minNum)/NumDist;
						MedVal=(Testdat.LogLikeCatalogMedian-minNum)/NumDist;
						QuantVal=(Testdat.LogLikeCatalog68Per-minNum)./NumDist;
						DistVal=(Testdat.LikeCatMonte-minNum)./NumDist;
					else
						OrgVal=(Testdat.LogLikeCatalog(1)-minNum)/NumDist;
					end
					
					%This is to avoid invisible lines
					if OrgVal<=0.01
						OrgVal=0.01;
					end
					
					if OrgVal>=0.99
						OrgVal=0.99;
					end
					
					
					
					
					if ShowMean
						MeanBoxX=[MeanVal-StdVal,MeanVal+StdVal,MeanVal+StdVal,...
							MeanVal-StdVal,MeanVal-StdVal];
						MeanBoxY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
							(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
							(k-1)*TestSpacing+LinCor];
						MeanLineX=[MeanVal,MeanVal];
						MeanLineY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						%plot it
						Mnboxer=fill(MeanBoxX,MeanBoxY,MeanCol);
						set(Mnboxer,'FaceAlpha',Tranpa);
						MnLine=plot(MeanLineX,MeanLineY,'m');
						set(MnLine,'LineWidth',2);
					end
					
					
					if ShowMedian
						MedBoxX=[QuantVal(1),QuantVal(2),QuantVal(2),QuantVal(1),QuantVal(1)];
						MedBoxY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
							(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
							(k-1)*TestSpacing+LinCor];
						MedLineX=[MedVal,MedVal];
						MedLineY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						%plot it
						Mdboxer=fill(MedBoxX,MedBoxY,MedCol);
						set(Mdboxer,'FaceAlpha',Tranpa);
						MdLine=plot(MedLineX,MedLineY,'b');
						set(MdLine,'LineWidth',2);
					end
					
					
					if ShowDist
						%Build a scale for the hist
						Scaler=((minNum:maxNum)-minNum)./NumDist;
						[hi bi]=hist(DistVal,Scaler);
						
						notZero=hi~=0;
						bi=bi(notZero);
						hi=hi(notZero);
						
						%norm hi to TestSpacing-0.1
						normhi=((hi-min(hi))./(abs(max(hi)-min(hi)))).*(TestSpacing-0.1);
						
						myStair=stairs(bi,normhi+(k-1)*TestSpacing);
						
						set(myStair,'LineStyle','--','LineWidth',1,'Color','b');
						
						
					end
					
					
					if ShowOriginal
						OrgLineX=[OrgVal,OrgVal]
						OrgLineY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						OrgLine=plot(OrgLineX,OrgLineY,'k');
						
						
						%if (Testdat.QLowMonte(1)>=Quant/2)&(Testdat.QHighMonte(1)<=(1-Quant/2))
						if Passed(k)
							set(OrgLine,'LineWidth',5,'Color',SuccessCol);
						else
							set(OrgLine,'LineWidth',5,'Color',FailCol);
						end
					end
					
					
				else
					%Plot both scores, no need for normalization
					
					OrgLowVal=Testdat.QLowMonte(1);
					OrgHighVal=Testdat.QHighMonte(1);
					if Results.Nr_MonteCarlo>1
						MeanLowVal=Testdat.QScoreLow;
						MeanHighVal=Testdat.QScoreHigh;
						StdLowVal=Testdat.QScoreLow_std_dev;
						StdHighVal=Testdat.QScoreHigh_std_dev;
						MedLowVal=Testdat.QScoreLowMedian;
						MedHighVal=Testdat.QScoreHighMedian;
						QuantLowVal=Testdat.QScoreLow68Per;
						QuantHighVal=Testdat.QScoreHigh68Per;
						DistLowVal=Testdat.QLowMonte;
						DistHighVal=Testdat.QHighMonte;
					end
					
					%This is to avoid invisible lines
					if OrgLowVal<=0.01
						OrgLowVal=0.01;
					end
					
					if OrgHighVal<=0.01
						OrgHighVal=0.01;
					end
					
					if OrgLowVal>=0.99
						OrgLowVal=0.99;
					end
					
					if OrgHighVal>=0.99
						OrgHighVal=0.99;
					end
					
					if ShowMean
						%Lower Quantil
						MeanBoxLowX=[MeanLowVal-StdLowVal,MeanLowVal+StdLowVal,MeanLowVal+StdLowVal,...
							MeanLowVal-StdLowVal,MeanLowVal-StdLowVal];
						MeanBoxLowY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
							(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
							(k-1)*TestSpacing+LinCor];
						MeanLineLowX=[MeanLowVal,MeanLowVal];
						MeanLineLowY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						%Higher Quantil
						MeanBoxHighX=[MeanHighVal-StdHighVal,MeanHighVal+StdHighVal,MeanHighVal+StdHighVal,...
							MeanHighVal-StdHighVal,MeanHighVal-StdHighVal];
						MeanBoxHighY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
							(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
							(k-1)*TestSpacing+LinCor];
						MeanLineHighX=[MeanHighVal,MeanHighVal];
						MeanLineHighY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						
						%plot it
						MnboxerL=fill(MeanBoxLowX,MeanBoxLowY,MeanCol);
						set(MnboxerL,'FaceAlpha',Tranpa);
						MnLineL=plot(MeanLineLowX,MeanLineLowY,'p');
						set(MnLineL,'LineWidth',2);
						
						MnboxerH=fill(MeanBoxHighX,MeanBoxHighY,MeanCol);
						set(MnboxerH,'FaceAlpha',Tranpa);
						MnLineH=plot(MeanLineHighX,MeanLineHighY,'m');
						set(MnLineL,'LineWidth',2);
					end
					
					
					if ShowMedian
						MedBoxLowX=[QuantLowVal(1),QuantLowVal(2),QuantLowVal(2),QuantLowVal(1),...
								QuantLowVal(1)];
						MedBoxLowY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
							(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
							(k-1)*TestSpacing+LinCor];
						MedLineLowX=[MedLowVal,MedLowVal];
						MedLineLowY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						MedBoxHighX=[QuantHighVal(1),QuantHighVal(2),QuantHighVal(2),QuantHighVal(1),...
							  	QuantHighVal(1)];
						MedBoxHighY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
							(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
							(k-1)*TestSpacing+LinCor];
						MedLineHighX=[MedHighVal,MedHighVal];
						MedLineHighY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						%plot it
						MdboxerL=fill(MedBoxLowX,MedBoxLowY,MedCol);
						set(MdboxerL,'FaceAlpha',Tranpa);
						MdLineL=plot(MedLineLowX,MedLineLowY,'b');
						set(MdLineL,'LineWidth',2);
						
						MdboxerH=fill(MedBoxHighX,MedBoxHighY,MedCol);
						set(MdboxerH,'FaceAlpha',Tranpa);
						MdLineH=plot(MedLineHighX,MedLineHighY,'b');
						set(MdLineH,'LineWidth',2);
					end
					
					
					if ShowDist
						%Build a scale for the hist
						Scaler=linspace(0,1,50);
						[hiLow biLow]=hist(DistLowVal,Scaler);
						[hiHigh biHigh]=hist(DistHighVal,Scaler);
						
						notZeroLow=hiLow~=0;
						biLow=biLow(notZeroLow);
						hiLow=hiLow(notZeroLow);
						
						notZeroHigh=hiHigh~=0;
						biHigh=biHigh(notZeroHigh);
						hiHigh=hiHigh(notZeroHigh);
						
						%norm hi to TestSpacing-0.1
						normhiLow=((hiLow-min(hiLow))./(abs(max(hiLow)-min(hiLow)))).*(TestSpacing-0.1);
						normhiHigh=((hiHigh-min(hiLow))./(abs(max(hiHigh)-min(hiHigh)))).*(TestSpacing-0.1);
						
						myStairLow=stairs(biLow,normhiLow+(k-1)*TestSpacing);
						myStairHigh=stairs(biHigh,normhiHigh+(k-1)*TestSpacing);
						
						set([myStairHigh],'LineStyle','--','LineWidth',1,'Color','b');
						set([myStairLow],'LineStyle','--','LineWidth',1,'Color','g');
						
					end
					
					
					if ShowOriginal
						OrgLineLowX=[OrgLowVal,OrgLowVal]
						OrgLineLowY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						OrgLineHighX=[OrgHighVal,OrgHighVal]
						OrgLineHighY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
						OrgLineL=plot(OrgLineLowX,OrgLineLowY,'k');
						OrgLineH=plot(OrgLineHighX,OrgLineHighY,'k');
						
						
						%if (Testdat.QLowMonte(1)>=Quant/2)&(Testdat.QHighMonte(1)<=(1-Quant/2))
						if Passed(k)
							set([OrgLineL,OrgLineH],'LineWidth',5,'Color',SuccessCol);
						else
							set([OrgLineL,OrgLineH],'LineWidth',5,'Color',FailCol);
						end
					end
					
					
				
				end
				
				
			else
				%Now the other tests
				OrgVal=Testdat.QScoreMonte(1);
				if Results.Nr_MonteCarlo>1
					MeanVal=Testdat.QScore;
					StdVal=Testdat.QScore_std_dev;
					MedVal=Testdat.QScoreMedian;
					QuantVal=Testdat.QScore68Per;
					DistVal=Testdat.QScoreMonte;
				end			
				%This is to avoid invisible lines
				if OrgVal<=0.01
					OrgVal=0.01;
				end
				
				if OrgVal>=0.99
					OrgVal=0.99;
				end
					
				
				if ShowMean
					MeanBoxX=[MeanVal-StdVal,MeanVal+StdVal,MeanVal+StdVal,...
						MeanVal-StdVal,MeanVal-StdVal];
					MeanBoxY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
						(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
						(k-1)*TestSpacing+LinCor];
					MeanLineX=[MeanVal,MeanVal];
					MeanLineY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
					%plot it
					Mnboxer=fill(MeanBoxX,MeanBoxY,MeanCol);
					set(Mnboxer,'FaceAlpha',Tranpa);
					MnLine=plot(MeanLineX,MeanLineY,'m');
					set(MnLine,'LineWidth',2);
				end
					
					
				if ShowMedian
					MedBoxX=[QuantVal(1),QuantVal(2),QuantVal(2),QuantVal(1),QuantVal(1)];
					MedBoxY=[(k-1)*TestSpacing+LinCor,(k-1)*TestSpacing+LinCor,...
						(k)*TestSpacing-LinCor,(k)*TestSpacing-LinCor,...
						(k-1)*TestSpacing+LinCor];
					MedLineX=[MedVal,MedVal];
					MedLineY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
						
					%plot it
					Mdboxer=fill(MedBoxX,MedBoxY,MedCol);
					set(Mdboxer,'FaceAlpha',Tranpa);
					MdLine=plot(MedLineX,MedLineY,'b');
					set(MdLine,'LineWidth',2);
				end
					
					
				if ShowDist
					%Build a scale for the hist
					Scaler=linspace(0,1,50);
					[hi bi]=hist(DistVal,Scaler);
					
					notZero=hi~=0;
					bi=bi(notZero);
					hi=hi(notZero);
					
					%norm hi to TestSpacing-0.1
					normhi=((hi-min(hi))./(abs(max(hi)-min(hi)))).*(TestSpacing-0.1);
					
					myStair=stairs(bi,normhi+(k-1)*TestSpacing);
					
					set(myStair,'LineStyle','--','LineWidth',1,'Color','b');
						
						
				end
					
					
				if ShowOriginal
					OrgLineX=[OrgVal,OrgVal]
					OrgLineY=[(k-1)*TestSpacing+LinCor,(k)*TestSpacing-LinCor];
					
					OrgLine=plot(OrgLineX,OrgLineY,'k');
					
					
					%if Testdat.QScoreMonte(1)>=Quant
					if Passed(k)
						set(OrgLine,'LineWidth',5,'Color',SuccessCol);
					else
						set(OrgLine,'LineWidth',5,'Color',FailCol);
					end
				end
			
			
			
			end
			
			
		
		
		end
		
		%Now formating the plot
		%for setting the Ylabel rights
		set(plotAxis,'YTick',plotyPos,'YTickLabel',TestLabels,'FontSize',16,'FontWeight','bold');
		
		%x axis label
		xLab=xlabel('Score %   ');
		set(xLab,'FontSize',16,'FontWeight','bold');
		
		%Title
		theTit=title([PlotTitle,  PlotNames{j}]);
		set(theTit,'FontSize',18,'FontWeight','bold','Interpreter','none');

		if WriteToFile
			%create filename
			filename=[FilePrefix,'_ConsistTests_',ModelNames{j},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		if ~KeepFig	
			close(fig);
		end			
		
	end
	

	
	
end	

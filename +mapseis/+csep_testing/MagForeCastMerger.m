function [SumForecast SingleMagForecast SingleMags XCoords YCoords] = MagForeCastMerger(Forecast,MatrixSwitch)
	%Separates each magnitude bin in a seperate array and calculates a summed forecast
	%for all magnitude bins. It is assumed that the single entries are equal sorted 
	%(it would be possible to allow badly sorted table, but I think it is not needed)
	%MatrixSwitch: if true the output arrays will be matrixes, as needed for plotting
	
	disp('Merging MagBins')
	
	if nargin<2
		MatrixSwitch=false;
	end

	
	if isempty(MatrixSwitch)
		MatrixSwitch=false;
	end

	
	
	%Magnitude bins
	SingleMags(:,1)=unique(Forecast(:,7));
	SingleMags(:,2)=unique(Forecast(:,8));
	NrMags=numel(SingleMags(:,1));
	
	%Longitude Bins (XCoord)
	XCoords(:,1)=unique(Forecast(:,1));
	XCoords(:,2)=unique(Forecast(:,2));
	NrX=numel(XCoords(:,1));
	
	%Latitude Bins (YCoord)
	YCoords(:,1)=unique(Forecast(:,3));
	YCoords(:,2)=unique(Forecast(:,4));
	NrY=numel(YCoords(:,1));	
	
	SingleMagForecast={};
	FirstBin=Forecast(:,7)==SingleMags(1,1);
	
	
	
	if ~MatrixSwitch
		%create "normal" tables
		SumForecast=Forecast(FirstBin,:);
		SumForecast(:,7)=SingleMags(1,1);
		SumForecast(:,8)=SingleMags(end,2);
		SumForecast(:,9)=0;
		
		for i=1:NrMags
			FindMag=Forecast(:,7)==SingleMags(i,1);
			ThisMagFore=Forecast(FindMag,:);
			
			SumForecast(:,9)=SumForecast(:,9)+ThisMagFore(:,9);
			
			SingleMagForecast{i}=ThisMagFore;
			
			
		end
	
	else
		disp('Create Matrixes')
		
		%create reshaped matrixes
		SumForecast=Forecast(FirstBin,:);
		SumForecast(:,7)=SingleMags(1,1);
		SumForecast(:,8)=SingleMags(end,2);
		SumForecast(:,9)=0;
		
		EmptyRegion=zeros(NrY,NrX);
		
		XMarker=zeros(numel(Forecast(:,1)),1);
		YMarker=zeros(numel(Forecast(:,1)),1);
		
		%disp('Mark X');
		%Mark X coords
		for i=1:NrX
			thisOne=Forecast(:,1)==XCoords(i,1);
			XMarker(thisOne)=i;
		end
		
		%disp('Mark Y');
		%Mark Y coords
		for i=1:NrY
			thisOne=Forecast(:,3)==YCoords(i,1);
			YMarker(thisOne)=i;
		end		
		
		
		for i=1:NrMags
			%disp(i)
			FindMag=Forecast(:,7)==SingleMags(i,1);
			ThisMagFore=Forecast(FindMag,:);
			
			SumForecast(:,9)=SumForecast(:,9)+ThisMagFore(:,9);
			
			ReshapeArea=EmptyRegion;
			
			%ReshapeArea((YMarker(FindMag)-1) * NrY + XMarker(FindMag))=ThisMagFore(:,9);
			ReshapeArea((XMarker(FindMag)-1) * NrY + YMarker(FindMag))=ThisMagFore(:,9);
			SingleMagForecast{i}=ReshapeArea;
			
			
		end
		ReshapeArea=EmptyRegion;
		%ReshapeArea((YMarker(FirstBin)-1) * NrY + XMarker(FirstBin))=SumForecast(:,9);
		ReshapeArea((XMarker(FirstBin)-1) * NrY + YMarker(FirstBin))=SumForecast(:,9);
		
		SumForecast=ReshapeArea;
		
	end
	

end

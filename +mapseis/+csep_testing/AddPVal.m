function Results=AddPVal(oldResults,Names)
	%Adds the missing p-value for t-test and the W-test, will be added
	%automatically to the test later
	
	%Changes: correct W-test p-values, there were previously applied only on 
	%on log(LambA)-log(LambB) instead of log(LambA)-Log(LambB)-(sumA-sumB)/N
	%Also the student t-test is slightly of, degree of freedom should N-1 instead
	%of N. The new Welch T-test and also the new version the student T-test (both
	%in mk2) as the new version of the W-test not need this function anymore, and
	%have already the correction version.
	
	Results=oldResults;
	
	for i=1:numel(Names)
			MySelf=strcmp(Names,Names{i});
			NotMyselfNames=Names(~MySelf);
			
			for j=1:numel(NotMyselfNames)
				%Name for T-test
				TName=['Ttest_',Names{i},'_',NotMyselfNames{j}];
				
				%Name for W-test
				WName=['Wtest_',Names{i},'_',NotMyselfNames{j}];
				
				MrMonty=oldResults.Nr_MonteCarlo;
				
				if MrMonty>1
					%With MonteCarlo method
					for k=1:MrMonty
						%Ttest
						PValMonteT(k)=1-tcdf(abs(oldResults.(TName).TValMonte(k)),...
								oldResults.(TName).TotalNum(k)-1);
							
						%Wtest	
						diffMed=oldResults.(WName).AliMonte{k}-...
						(oldResults.(WName).ExpModA-oldResults.(WName).ExpModB)/...
						oldResults.(WName).TotalNum(k);
						PValMonteW(k) = signrank(diffMed);
					
					end
					
					
					%The original value and mean,median, etc.
					
					%Ttest
					PValOrgT=PValMonteT(1);
					PValOrgMeanT=mean(PValMonteT);
					PValOrgStdT=std(PValMonteT);
					quantT=quantile(PValMonteT,[0.16,0.5,0.84]);
					PValOrgMedianT=quantT(2);
					PValOrgQuantilT=[quantT(1),quantT(3)];
					
					%Wtest
					PValOrgW=PValMonteW(1);
					PValOrgMeanW=mean(PValMonteW);
					PValOrgStdW=std(PValMonteW);
					quantW=quantile(PValMonteW,[0.16,0.5,0.84]);
					PValOrgMedianW=quantW(2);
					PValOrgQuantilW=[quantW(1),quantW(3)];
					
					%Pack it
					Results.(TName).p_org=PValOrgT;
					Results.(TName).p_mean=PValOrgMeanT;
					Results.(TName).p_std=PValOrgStdT;
					Results.(TName).p_median=PValOrgMedianT;
					Results.(TName).p_Conf68Per=PValOrgQuantilT;
					
					Results.(WName).p_org=PValOrgW;
					Results.(WName).p_mean=PValOrgMeanW;
					Results.(WName).p_std=PValOrgStdW;
					Results.(WName).p_median=PValOrgMedianW;
					Results.(WName).p_Conf68Per=PValOrgQuantilW;
					
				else
					%later
				
				end
				
				
			end
	end
			


end

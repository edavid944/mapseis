function TheRes=protoSTest(Mmodel,TheCatalog,numCatalogs,ParallelMode,FeedItBack)
	%A first prototype of a S-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	import mapseis.csep_testing.*;
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<5
		FeedItBack=[];
	end	
	
	%added column 10 if not defined (it should be)
	if numel(Mmodel(1,:))<10
		Mmodel(:,10)=true(numel(Mmodel(:,1),1));
	end
	
	
	UsedBins=logical(Mmodel(:,10));
	
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	
	%set every bin not used to zero (to avoid that the influence the total)
	%UnModRate=Mmodel(:,9);
	Mmodel(~UsedBins,9)=0;
	
			
	%No support for iregular grids at the moment, the magnitude bins have to similar
	%at each locaction

	%All cell having the same magnitude have to summed
	TheMags=unique(Mmodel(:,7));
	
	for i=1:numel(TheMags)
		LogSelector(:,i)=Mmodel(:,7)==TheMags(i);
	end
	
	%Number of MagBins
	NumMag=numel(TheMags);
	
	%Number of Remainingbins
	NumRemBin=sum(LogSelector(:,1));
	NumRemBinCor=sum(LogSelector(:,1)&UsedBins);
	disp(size(LogSelector))
	%summary coords (take the first mag bin)
	SumMod=Mmodel(LogSelector(:,1),:);
	
	disp(size(SumMod))
	
	%calculate the log likelihood for the observation
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(SumMod(:,1)),1);
	TempObs=zeros(numel(Mmodel(:,1)),1);
	
	
	%To be sure nothing gets init which should not
	%minMag=min(Mmodel(:,7));
	%maxMag=max(Mmodel(:,8));
	%InMag=TheCatalog(:,6)>=minMag&TheCatalog(:,6)<maxMag;
	
	for i=1:numel(Mmodel(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>=Mmodel(i,1)&TheCatalog(:,1)<Mmodel(i,2);
		InLat=TheCatalog(:,2)>=Mmodel(i,3)&TheCatalog(:,2)<Mmodel(i,4);
		InDepth=TheCatalog(:,7)>=Mmodel(i,5)&TheCatalog(:,7)<Mmodel(i,6);
		InMag=TheCatalog(:,6)>=Mmodel(i,7)&TheCatalog(:,6)<Mmodel(i,8);
		
		
		%TheObserver(count)=TheObserver(count)+sum(InLon&InLat&InDepth&InMag&UsedBins(i));
		%count=count+1;
		%if mod(i,NumRemBin)==0
			%next spatial bin
			%disp(TheObserver(count))
			%count=count+1;
		%	count=1;
		%end
		
		TempObs(i)=sum(InLon&InLat&InDepth&InMag&UsedBins(i));
		
	end
	%disp(TempObs(TempObs~=0))
	
	%sum them up according to LogSelector
	for i=1:numel(LogSelector(1,:))
		%disp(sum((LogSelector(:,i))))
		%disp('+++++')
		TheObserver=TheObserver+TempObs(LogSelector(:,i));
		
	end
	disp(sum(TheObserver))
	disp(numel(TheObserver~=0))
	disp(max(TheObserver))
	disp(min(TheObserver))
	disp('------')
	
	
	%Normalize Lamba and adding up all mags

	rawAr=repmat(Mmodel(:,9),1,NumMag);
	UseIt=repmat(UsedBins,1,NumMag);
	selectit=rawAr(LogSelector);
	
	%Add up Lambas (MagBin) 
	RawLambaSpace=sum(reshape(selectit,NumRemBin,NumMag),2);
	%should be saved not used bins are set to 0 anyway
	
	
	%LambaSpace=(TotalEq/sum(Mmodel(:,9)))*RawLambaSpace;
	ExpNum=sum(Mmodel(UsedBins,9));
	
	%Number of eq in observation
	%TotalEq=numel(TheCatalog(:,1));
	TotalEq=sum(TheObserver); %saver in case a too large catalog is used
	
	LambaSpace=(TotalEq/ExpNum)*RawLambaSpace;
	UnityLamba=ExpNum*RawLambaSpace;
	if isempty(FeedItBack)
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			%TheCat=poissrnd(Mmodel(:,9));	
			%needs a new random generator
			%TheCat=poissrnd(UnityLamba);
			
			%changed it to LambaSpace from UnityLamba, don't know why
			%used it before, but it does not matter, poissrnd_limited 
			%correct for it anyway
			TheCat=poissrnd_limited(LambaSpace,TotalEq);
			
			%TotalCat=sum(TheCat);
			
			%LambaSpace = (TotalCat/ExpNum)*RawLambaSpace;
			%LambaSpace=RawLambaSpace;
			
			%Now Sum up the magbins
			%rawMultCat=repmat(TheCat,1,NumMag);
			%selMultCat=rawMultCat(LogSelector);
			%LowCat=sum(reshape(selMultCat,NumRemBin,NumMag),2);
			LowCat=TheCat;
			
			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-LambaSpace+LowCat.*log(LambaSpace)-log(factorial(LowCat));
			LogLiker(i)=nansum(LikeIt(~isinf(LikeIt)));
			
			
		end
		
	else
		LogLiker=FeedItBack;
	
	end
	
	%calc logLikelihood for catalog
	%ObsLiker=nansum(-RawLambaSpace+TheObserver.*log(RawLambaSpace)-log(factorial(TheObserver)));
	
	%LambaSpace = (TotalEq/ExpNum)*RawLambaSpace;
	LikeIt=-LambaSpace+TheObserver.*log(LambaSpace)-log(factorial(TheObserver));
	ObsLiker=nansum(LikeIt(~isinf(LikeIt)));
	
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='STest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

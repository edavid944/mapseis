function plotAxis=ModelRatePlotter(Datastores,Models,Offsets,TheNames,ColorBarShow,LonTickShow,LatTickShow,CLimit,BlowUp,PShift,Color)
	%similar to RateEqMapper (actually uses it) but plots
	%more than one Models into subplots
	
	import mapseis.csep_testing.*;
	
	PlotLayout=size(Models);
	
	if numel(Offsets)==1
		Offsets=ones(PlotLayout)*OffSets;
	end	
	
	if isempty(ColorBarShow)
		ColorBarShow=true(PlotLayout);
	end	
	
	if isempty(LonTickShow)
		LonTickShow=true(PlotLayout);
	end	
	
	if isempty(LatTickShow)
		LatTickShow=true(PlotLayout);
	end

	if isempty(BlowUp)
		BlowUp=1.5;
	end	

	if isempty(PShift)
		PShift(1)=0;
		PShift(2)=0.1;
	end		
	
	if isempty(Color)
		Color=jet(256);
	end
	
	%Build figure
	figure;
	plotAxis=zeros(PlotLayout);
	posCor=reshape(1:numel(Models),PlotLayout(2),PlotLayout(1))';
	
	for i=1:numel(Models)
		plotAxis(i)=subplot(PlotLayout(1),PlotLayout(2),posCor(i));
		RateEqMapper(Datastores{i},Models{i},Offsets(i),TheNames{i},plotAxis(i));
		
		pos=get(plotAxis(i),'Position');
		pos(1)=pos(1)-PShift(1);
		pos(2)=pos(2)-PShift(2);
		pos(3)=pos(3)*BlowUp;
		pos(4)=pos(4)*BlowUp;
		set(plotAxis(i),'Position',pos);
		
		if ~isempty(CLimit)
			caxis(CLimit);
		end
		
		if ~isempty(TheNames{i})
			%That is done have bigger letters and custom formating
			tit=title(plotAxis(i),TheNames{i});
			set(tit,'FontSize',24,'FontWeight','bold','Interpreter','none');
		end
		
		if ~ColorBarShow(i)
			colorbar('off');
			
			%This have to be done to avoid size differences
			myBar=colorbar;
			set(myBar,'Visible','off');
		end
		
		if ~LonTickShow(i)
			set(plotAxis(i),'XTickLabel',[]);
			xlabel(plotAxis(i),[]);
		else
			xlab=xlabel(plotAxis(i),'Longitude');
			set(xlab,'FontSize',18,'FontWeight','bold','Interpreter','none');
		end
		
		if ~LatTickShow(i)
			set(plotAxis(i),'YTickLabel',[]);
			ylabel(plotAxis(i),[]);
		else
			ylab=ylabel(plotAxis(i),'Latitude');
			set(ylab,'FontSize',18,'FontWeight','bold','Interpreter','none');	
		end
		
		set(plotAxis(i),'LineWidth',3,'FontSize',16,'FontWeight','bold');
		
	end
	
	%set colormap
	colormap(Color);
	
	
	

end

function TheRes=protoRTest(ModelA,ModelB,TheCatalog,numCatalogs,ParallelMode,FeedItBack)
	%A first prototype of a N-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<6
		FeedItBack=[];
	end	
	
	%added column 10 if not defined (it should be)
	if numel(ModelA(1,:))<10
		ModelA(:,10)=true(numel(ModelA(:,1),1));
	end
	
	if numel(ModelB(1,:))<10
		ModelB(:,10)=true(numel(ModelB(:,1),1));
	end

	UsedBinsA=logical(ModelA(:,10));
	UsedBinsB=logical(ModelB(:,10));

	%set every bin not used in one of the  model to zero (to avoid that the
	%influence the total)
	%UnModRate=Mmodel(:,9);
	ModelA(~UsedBinsA|~UsedBinsB,9)=0;
	ModelB(~UsedBinsA|~UsedBinsB,9)=0;
	
	LogLikerA=nan(numCatalogs,1);
	LogLikerB=nan(numCatalogs,1);
	%Ratio_AB=nan(numCatalogs,1);
	ObsLiker=nan;
	
	if isempty(FeedItBack)
	
		%assume the cells are in order
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			TheCat=poissrnd(ModelA(:,9));	
			
			%set to zero all bins not used in one of the models
			TheCat(~UsedBinsA|~UsedBinsB)=0;
			
			%calculate likelihood (assume poisson distribution)
			LikeA=-ModelA(:,9)+TheCat.*log(ModelA(:,9))-log(factorial(TheCat));
			LogLikerA(i)=nansum(LikeA(~isinf(LikeA)&UsedBinsA));
			
			%calculate likelihood (assume poisson distribution)
			LikeB=-ModelB(:,9)+TheCat.*log(ModelB(:,9))-log(factorial(TheCat));
			LogLikerB(i)=nansum(LikeB(~isinf(LikeB)&UsedBinsB));
			
		%	Ratio_AB(i)=nansum(ModelB(:,9)-ModelA(:,9)+TheCat.*(log(ModelA(:,9)-log(ModelB(:,9)))));
		end
		
			
		%Calculate Ratio
		Ratio_AB=LogLikerA-LogLikerB;
	
	else
		Ratio_AB=FeedItBack;
	end	
		
	
	%now to the observations (ModelA and ModelB have to be on the same grid)
	TheObserver=zeros(numel(ModelA(:,1)),1);
		
	for i=1:numel(ModelA(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>=ModelA(i,1)&TheCatalog(:,1)<ModelA(i,2);
		InLat=TheCatalog(:,2)>=ModelA(i,3)&TheCatalog(:,2)<ModelA(i,4);
		InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<ModelA(i,6);
		InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
		
		
		TheObserver(i)=sum(InLon&InLat&InDepth&InMag&UsedBinsA(i)&UsedBinsB(i));
		
		
	end
	
	%calc logLikelihood A&B to observation
	LikeA=-ModelA(:,9)+TheObserver.*log(ModelA(:,9))-log(factorial(TheObserver));
	LikeB=-ModelB(:,9)+TheObserver.*log(ModelB(:,9))-log(factorial(TheObserver));
	ObsLikerA=nansum(LikeA(~isinf(LikeA)&UsedBinsA&UsedBinsB));
	ObsLikerB=nansum(LikeB(~isinf(LikeB)&UsedBinsA&UsedBinsB));	
	
	Ratio_Obs_AB=ObsLikerA-ObsLikerB;
	%Ratio_Obs_AB=nansum(ModelB(:,9)-ModelA(:,9)+TheObserver.*(log(ModelA(:,9)-log(ModelB(:,9)))));
	quantAB=sum(Ratio_AB<=Ratio_Obs_AB)/numCatalogs;
	
	
	%Pack result
	TheRes.TestType='RTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModelA=LogLikerA;
	TheRes.LogLikeModelB=LogLikerB;
	TheRes.LogLikeACatalog=ObsLikerA;
	TheRes.LogLikeBCatalog=ObsLikerB;
	TheRes.Ratio_AB=Ratio_AB;
	TheRes.ObsRatio_AB=Ratio_Obs_AB;
	TheRes.QScoreRatioAB=quantAB;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end
	
	

end

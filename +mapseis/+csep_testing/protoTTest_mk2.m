function TheRes=protoTTest_mk2(ModelA,ModelB,TheCatalog,signLevel,BinMode,WelchT,ParallelMode)
	%A first prototype of a T-Test rebuild, rought an no features at all
	%second version of the T-test, new added is the WelchT test if needed and
	%p-values will automatically calculated.
	%Also the normal T-test is written slightly different to better understand its
	%nature as a paired T-test
	
	%The bin models is currently not updated, may will do that or kick it out 
	%of the code
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	
	if ParallelMode
		matlabpool open
	end
	
	if isempty(signLevel)
		% 5% standard alpha, and it is assumed to be one sided
		signLevel=0.05;
	end
	
	if isempty(BinMode)
		% default: use earthquakes instead of bins
		BinMode=false;
	end
	
	if isempty(WelchT)
		% use a Welch T-test (unpaired and with unequal variances) instead
		% of 
		WelchT=false;
	end
	
	%for safety set cells which are zero to 10^-24, that is more or less zero, but
	%still has a log not -Inf, I did not see models which have such low rates, so it should be outside
	ModelA(ModelA(:,9)==0,9)=realmin;
	ModelB(ModelB(:,9)==0,9)=realmin;
	
	
	%added column 10 if not defined (it should be)
	if numel(ModelA(1,:))<10
		ModelA(:,10)=true(numel(ModelA(:,1),1));
	end
	
	if numel(ModelB(1,:))<10
		ModelB(:,10)=true(numel(ModelB(:,1),1));
	end

	UsedBinsA=logical(ModelA(:,10));
	UsedBinsB=logical(ModelB(:,10));

	%set every bin not used in one of the  model to zero (to avoid that the
	%influence the total). They should not be used anyway. Set it up like this the 
	%Cells not used will be used will be zero but no cell used will be zero 
	%UnModRate=Mmodel(:,9);
	ModelA(~UsedBinsA|~UsedBinsB,9)=0;
	ModelB(~UsedBinsA|~UsedBinsB,9)=0;
	
	
	%  %use the formulas from Rhoades 2010 (so I hope)
	%  
	%  %now to the observations (ModelA and ModelB have to be on the same grid)
	
	if BinMode
	
		TheObserver=zeros(numel(ModelA(:,1)),1);
			
		
		for i=1:numel(ModelA(:,1))
			%no parfor steps are probably to small
			InLon=TheCatalog(:,1)>=ModelA(i,1)&TheCatalog(:,1)<ModelA(i,2);
			InLat=TheCatalog(:,2)>=ModelA(i,3)&TheCatalog(:,2)<ModelA(i,4);
			InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<ModelA(i,6);
			InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
			
			
			TheObserver(i)=sum(InLon&InLat&InDepth&InMag&UsedBinsA(i)&UsedBinsB(i));
			
			
		end
		
		Bin_w_Eq=TheObserver~=0;
		
		
		X_A=log(ModelA(Bin_w_Eq,9));
		X_B=log(ModelB(Bin_w_Eq,9));
		%InAB=1/sum(TheObserver)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(TheObserver);
		%SVar=1/(sum(TheObserver)-1)*sum((X_A-X_B).^2)-1/(sum(TheObserver)^2-sum(TheObserver))*sum(X_A-X_B)^2;
		%T=InAB/(sqrt(SVar)/sqrt(sum(TheObserver)));
		%Ttable=tinv(1-signLevel/2,sum(TheObserver)-1);
		InAB=1/sum(Bin_w_Eq)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(Bin_w_Eq);
		SVar=1/(sum(Bin_w_Eq)-1)*sum((X_A-X_B).^2)-1/(sum(Bin_w_Eq)^2-sum(Bin_w_Eq))*sum(X_A-X_B)^2;
		T=InAB/(sqrt(SVar)/sqrt(sum(Bin_w_Eq)));
		Ttable=tinv(1-signLevel,sum(Bin_w_Eq)-1);
		
	else
		%get a probability for each earthquake in each model
		
		LamdaA=NaN;
		LamdaB=NaN;
		
		count=1;
		
		for i=1:numel(TheCatalog(:,1))
			LamA=NaN;
			LamB=NaN;
			InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
			InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
			InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
			InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
			
			if any(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB)
				LamA=ModelA(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB,9);
				LamB=ModelB(InLon&InLat&InDepth&InMag&UsedBinsA&UsedBinsB,9);
				LamdaA(count)=LamA;
				LamdaB(count)=LamB;
				count=count+1;
			end
			%disp(LamA);
			%disp(LamB);
			%LamdaA(i)=LamA;
			%LamdaB(i)=LamB;
		end
		
		X_A=log(LamdaA);
		X_B=log(LamdaB);
		TotalNum=count-1;
		
		%total expectation
		N_A=sum(ModelA(UsedBinsA&UsedBinsB,9));
		N_B=sum(ModelB(UsedBinsA&UsedBinsB,9));
	
		
		%normalized log(Lamda) for A and B
		I_A=X_A-(N_A/TotalNum);
		I_B=X_B-(N_B/TotalNum);
		
		%average information gain
		InAB=mean(I_A-I_B);
		
		
		
		if ~WelchT
			%Student t-test
			SVar=var(I_A-I_B);
			T=InAB/(sqrt(SVar)/sqrt(TotalNum));
			Ttable=tinv(1-signLevel,TotalNum-1);
			IConf= Ttable* SVar.^(1/2) / sqrt(TotalNum);
			PVal=1-tcdf(abs(T),TotalNum-1);
			
			Bin_w_Eq=NaN;
			
			%degree of freedom used (to be consistent with the welch)
			d_f=TotalNum-1;
			
		else
			%Welch t-test
			
			%different variance is needed
			S_A=var(I_A);
			S_B=var(I_B);
			S_A_B=sqrt(S_A^2/TotalNum+S_B^2/TotalNum);
			
			%and also an other degree of freedom
			d_f   =  ((S_A^2/TotalNum+S_B^2/TotalNum)^2)/...
				 ((S_A^2/TotalNum)^2/(TotalNum-1)+...
				 (S_B^2/TotalNum)^2/(TotalNum-1));
			
			%actually mean(I_A)-mean(I_B)= mean(I_A-I_B) in the case
			%with the same number of events in both forecasts (which 
			%is the case). I left it like this to emphasize that the
			%Welch test also works with unequal number of events. In 
			%this case however the IConf would be difficult.
			T=(mean(I_A)-mean(I_B))/S_A_B;
			
			Ttable=tinv(1-signLevel,d_f);
			IConf= Ttable* S_A_B;
			PVal=1-tcdf(abs(T),d_f);
			
			Bin_w_Eq=NaN;
			
			%to make packing easier
			SVar=S_A_B;
				
		end
		
		
	end
	
	TheRes.TestType='TTest';
	TheRes.TestVersion=2;
	TheRes.Welch_Mode=WelchT;
	
	TheRes.SignLevel=signLevel;
	TheRes.Bins_w_Eq=sum(Bin_w_Eq);
	TheRes.InAB=InAB;
	TheRes.p_org=PVal;
	TheRes.Alibaba=log(LamdaA)-log(LamdaB);
	TheRes.I_A=I_A;
	TheRes.I_B=I_B;
	TheRes.VarianceI=SVar;
	TheRes.T=T;
	TheRes.ttable=Ttable;
	TheRes.IConf=IConf;
	TheRes.Deg_of_Free=d_f;
	TheRes.TotalNum=TotalNum;
	TheRes.ExpModA=N_A;
	TheRes.ExpModB=N_B;
	
	if ParallelMode
		matlabpool close
	end
	
	

end

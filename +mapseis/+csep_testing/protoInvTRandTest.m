function TheRes=protoInvTRandTest(ModelB,TheCatalog,signLevel,BinMode,NoiseComp,NumRand,ParallelMode)
	%Similar to the TRand Test but it inverses the catalog, to see how strongly the Model overpredicts
	%In this case the model should has a in general lower average information gain than the uniform or
	%random model
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	%NoiseComp=false;
	
	if nargin<7
		NumRand=1;
	end
	
	if ParallelMode
		matlabpool open
	end
	
	if isempty(signLevel)
		% 5% standard alpha, and it is assumed to be one sided
		signLevel=0.05;
	end
	
	%  %use the formulas from Rhoades 2010 (so I hope)
	%  
	%  %now to the observations (ModelA and ModelB have to be on the same grid)
	
	%for safety set cells which are zero to 10^-24, that is more or less zero, but
	%still has a log not -Inf, I did not see models which have such low rates, so it should be outside
	ModelB(ModelB(:,9)==0,9)=realmin;
	
	
	%create the ModelA from Noise
	ModelA=ModelB;
	NumOfGrid=numel(ModelB(:,9));
	totalExp=sum(ModelB(:,9));
	if NoiseComp
		ModelA(:,9)=rand(NumOfGrid,1);
	else
		ModelA(:,9)=ones(NumOfGrid,1);
	end
	
	%Norm it
	ModelA(:,9)=ModelA(:,9)/sum(ModelA(:,9))*totalExp;
	
	
	if BinMode
	
		TheObserver=zeros(numel(ModelA(:,1)),1);
			
		
		for i=1:numel(ModelA(:,1))
			%no parfor steps are probably to small
			InLon=TheCatalog(:,1)>=ModelA(i,1)&TheCatalog(:,1)<ModelA(i,2);
			InLat=TheCatalog(:,2)>=ModelA(i,3)&TheCatalog(:,2)<ModelA(i,4);
			InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<ModelA(i,6);
			InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
			
			
			TheObserver(i)=sum(~InLon&~InLat&~InDepth&~InMag);
			
			
		end
		
		Bin_w_Eq=TheObserver~=0;
		Ttable=tinv(1-signLevel,sum(Bin_w_Eq)-1);
		
		for i=1:NumRand
		
			%create the ModelA from Noise
			ModelA=ModelB;
			NumOfGrid=numel(ModelB(:,9));
			totalExp=sum(ModelB(:,9));
			if NoiseComp
				ModelA(:,9)=rand(NumOfGrid,1);
			else
				ModelA(:,9)=ones(NumOfGrid,1);
			end
			
			%Norm it
			ModelA(:,9)=ModelA(:,9)/sum(ModelA(:,9))*totalExp;
			
			X_A=log(ModelA(Bin_w_Eq,9));
			X_B=log(ModelB(Bin_w_Eq,9));
			%InAB=1/sum(TheObserver)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(TheObserver);
			%SVar=1/(sum(TheObserver)-1)*sum((X_A-X_B).^2)-1/(sum(TheObserver)^2-sum(TheObserver))*sum(X_A-X_B)^2;
			%T=InAB/(sqrt(SVar)/sqrt(sum(TheObserver)));
			%Ttable=tinv(1-signLevel/2,sum(TheObserver)-1);
			InAB_raw(i)=1/sum(Bin_w_Eq)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(Bin_w_Eq);
			SVar_raw(i)=1/(sum(Bin_w_Eq)-1)*sum((X_A-X_B).^2)-1/(sum(Bin_w_Eq)^2-sum(Bin_w_Eq))*sum(X_A-X_B)^2;
			T_raw(i)=InAB_raw(i)/(sqrt(SVar_raw(i))/sqrt(sum(Bin_w_Eq)));
			
		
		end	
			
	else
		%get a probability for each earthquake in each model
				
		
		%PosArray=1:NumOfGrid;
		SelArray=true(NumOfGrid,1);
		
		
		for i=1:numel(TheCatalog(:,1))
			LamA=NaN;
			LamB=NaN;
			InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
			InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
			InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
			InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
			
			if any(InLon&InLat&InDepth&InMag)
				%LamA=ModelA(~InLon&~InLat&~InDepth&~InMag,9);
				%LamB=ModelB(~InLon&~InLat&~InDepth&~InMag,9);
				SelArray=SelArray&(~InLon&~InLat&~InDepth&~InMag);
			end
			%disp(LamA);
			%disp(LamB);
			
		end
		
		
		TotalNum=NumOfGrid-numel(TheCatalog(:,1));
		Ttable=tinv(1-signLevel,TotalNum-1);
		
		for j=1:NumRand
		
			LamdaA=[];
			LamdaB=[];
			
			%create the ModelA from Noise
			ModelA=ModelB;
			NumOfGrid=numel(ModelB(:,9));
			totalExp=sum(ModelB(:,9));
			
			if NoiseComp
				ModelA(:,9)=rand(NumOfGrid,1);
			else
				ModelA(:,9)=ones(NumOfGrid,1);
			end
			
			%Norm it
			ModelA(:,9)=ModelA(:,9)/sum(ModelA(:,9))*totalExp;
			
			LamdaA=ModelA(SelArray,9);
			LamdaB=ModelB(SelArray,9);
		
		
			X_A=log(LamdaA);
			X_B=log(LamdaB);
			
			InAB_raw(j)=1/TotalNum*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/TotalNum;
			SVar_raw(j)=1/(TotalNum-1)*sum((X_A-X_B).^2)-1/(TotalNum^2-TotalNum)*sum(X_A-X_B)^2;
			T_raw(j)=InAB_raw(j)/(sqrt(SVar_raw(j))/sqrt(TotalNum));
			
			IConf_raw(j)= Ttable* SVar_raw(j).^(1/2) / sqrt(TotalNum);
			
		
			Bin_w_Eq=NaN;
		
		end
	end
	
	TheRes.TestType='TRInvTest';
	TheRes.TestVersion=1;
	TheRes.SignLevel=signLevel;
	TheRes.Bins_w_Eq=sum(Bin_w_Eq);
	TheRes.InAB=mean(InAB_raw);
	TheRes.Alibaba=log(LamdaA)-log(LamdaB);
	TheRes.VarianceI=mean(SVar_raw);
	TheRes.T=mean(T_raw);
	TheRes.ttable=Ttable;
	TheRes.IConf=mean(IConf_raw);
	TheRes.TotalNum=TotalNum;
	TheRes.ExpModA=sum(ModelA(:,9));
	TheRes.ExpModB=sum(ModelB(:,9));
	
	
	if ParallelMode
		matlabpool close
	end
	
	

end

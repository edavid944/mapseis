function TWTestPrinter(Results,TheFold,FilePrefix,PlotConfig,PaperBoy)
	%Extract from the MassTestPrinter function, it produces a special T-test
	%plot with PerfectModel and Uniform Model and includes the W-test there
	%Also it produces a special TR-test plot with no confidence intervalls
	%but wiht a uncertianty fromt the perturbed catalogs
	
	import mapseis.plot.*;
	import mapseis.csep_testing.*;
	import mapseis.util.*;
	
	
	if iscell(Results)
		%This is important to do here or else it might give a problem
		%with not existing fields
		TName=['Ttest_',Results{1}.Names{1},'_',Results{1}.Names{2}];
		Results=StructureMerger(Results,true);
		PlotConfig.T_Plot=false;
	else
		TName=['Ttest_',Results.Names{1},'_',Results.Names{2}];
	end
	
	
	
	%Needed before in case of not PlotConfig is given
	ModelNames=Results.Names;
	
	if nargin<5
		PaperBoy=false;
		PlotConfig=struct( 'PlotNames',{ModelNames},...
				   'T_Plot',true,...
				   'TR_Plot',true,...
				   'ShowUniform',true,...
				   'ShowPerfect',true,...
				   'MarkWtest',true,...
				   'PlotTitle',[[]],...
				   'Quantil',0.05);
				   
	end	
	
	if nargin<4
		PaperBoy=false;
	end	
	
	
	
	oldFold=pwd;
	%first run as template for the rest
	ResStruct=struct(	'Name',[],...
				'TestType','Ttest',...
				'PlotType',[],...
				'PrintText',true,...
				'ScoreSwitch','meanorg',...
				'DisplayName',[],...
				'Colors',[[0.5 0.5 0.7]],...
				'LineStyle','-',...
				'LineWidth',3,...
				'ScoreWidth',2,...
				'PaperBoy',PaperBoy,...
				'MiniHist',false,...
				'ModDispName',[]);
				
	if isfield(PlotConfig,'PlotTitle')
		ResStruct.DisplayName=PlotConfig.PlotTitle;
		RawTitle=PlotConfig.PlotTitle;
	end
	
	%Add some default plotnames (Modelnames without '_')
	for i=1:numel(ModelNames)
		NewName=ModelNames{i};
		NewName(NewName=='_')=' ';
		PlotNames{i}=NewName;
	end
	
	ResStruct.ModDispName=PlotNames;
	RawNames=PlotNames;
	
	if isfield(PlotConfig,'PlotNames')
		if ~isempty(PlotConfig.PlotNames)
			ResStruct.ModDispName=PlotConfig.PlotNames;
			RawNames=PlotConfig.PlotNames;
		end	
	end

	
	%Default value for Quantil
	Quant=0.05;
	
	if isfield(PlotConfig,'Quantil')
		if ~isempty(PlotConfig.Quantil)
			Quant=PlotConfig.Quantil;
		end
	end
				
	LineLength=2*numel(ModelNames)
				
	
	%Checki if p-values exists, if not -> add them
	try
		TTemp=Results.(TName)
		if ~isfield(TTemp,'p_org')
			Results=AddPVal(Results,ModelNames);
		end
	catch
		disp('Could not determine if p-values exist')
	end
	
	
	if PlotConfig.T_Plot
	
		%modified T-test plot with simpler errorbars
		TestModesT={'HistI','HistIConf','HistT','ScoreT','ScoreI'};
		ResStruct.PlotType=TestModesT{2};	
		ResStruct.ScoreSwitch='original';	
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			DeDiName=RawNames(notMe);
			
			ResStruct.Name={ModelNames{i},DependModels{:}};
			ResStruct.ModDispName={RawNames{i},DeDiName{:}};
			ResStruct.DisplayName=[RawTitle,', Reference: ',RawNames{i},'   '];
			
			
			fig=figure;
			plotAxis=gca;
			
			TRName=['TRtest_',ModelNames{i}];
			
			%try
				%plot
				%first plot the uniform and the Perfect model
				if PlotConfig.ShowUniform
					%get the result from the TRtest
					TRTest=Results.(TRName);
					UniGain=-TRTest.InABMonte(1);
					UniLine=plot([UniGain(1);UniGain(1)],[0;LineLength])
					set(UniLine,'LineStyle',':','Color','k','LineWidth',2);
					hold on;
				end
				
				if PlotConfig.ShowPerfect
					TRTest=Results.(TRName);
					PerfGain=TRTest.MaxGainMonte(1);
					MaxLine=plot([PerfGain(1);PerfGain(1)],[0;LineLength])
					set(MaxLine,'LineStyle','--','Color','b','LineWidth',2);
					hold on;
				end
				
				%The actuall T-test
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				
				%plot Wtest
				if PlotConfig.MarkWtest
					hold on;
					for j=1:numel(DependModels)
						WName=['Wtest_',ModelNames{i},'_',DependModels{j}];
						TName=['Ttest_',ModelNames{i},'_',DependModels{j}];
						
						%Check if a star is needed -> P_w<Quantil
						if Results.(WName).p_org<Quant
							%y Position of the star
							yPos=j*2+0.35;
							
							%x Position (same as Igain Org)
							xPos=Results.(TName).InABMonte(1);
							
							%Now lets see that star (may have to tune the setting
							%a bit
							TheStar(i)=plot(xPos,yPos,'LineStyle','none');
							set(TheStar(i),'Marker','p','MarkerFaceColor','y',...
									'MarkerSize',10)
							
						end
						
						
					end
				end
				
				hold off
				
				ylim([1 LineLength-1]);
				
				%create filename
				filename=[FilePrefix,'_Ttest_',TestModesT{2},'_Spec_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			%end
			
			close(fig);
				
		end
	
	end
	
	
	
	if PlotConfig.TR_Plot	
	
		%Now for the Special TRtest
		%TR-test 'Mean'
		ResStruct.ScoreSwitch='meanorg';
		ResStruct.TestType='TRtest';
		ResStruct.WideBar=true;
		ResStruct.BarTrail=true;
		NoLoopNeed={'HistINoConf','HistINoPerc'}
		TestModesT={'ScoreT','ScoreI'};
		
		
		for j=1:numel(NoLoopNeed)
			ResStruct.PlotType=NoLoopNeed{j};		
				
				
			ResStruct.Name=ModelNames;
			
			ResStruct.ModDispName=RawNames;
			ResStruct.DisplayName=RawTitle;
			
			fig=figure;
			plotAxis=gca;
				
			%try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				%set(plotAxis,'XGrid','off','YGrid','on')
				%create filename
				filename=[FilePrefix,'_TRtest_Mean_',NoLoopNeed{j},'_',ModelNames{1},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			%end
				
			close(fig);
					
			
		end
	
	
	end
	
	
end	

function [OutStruct PowerT PowerTW PowerW] = CompTestPower(Distro1,Distro2,signLevel,NrDraws,MonteNumber,SameCell,biased)
	%this function tests the power of the students t-test, welch t-test and
	%w-test (wilcoxon rank sum) on a inputed distribtuion. By drawing NrDraws
	%random values from Distro1 and 2, and compare the two samples, for equality
	%This is done MonteNumber times and a statistic is calculated giving the 
	%power of the test as percentage of positive tests
	
	%All done in the CSEP "style" (information gain, normalized)
	
	if nargin<7
		biased=false;
	end
	
	
	if isempty(signLevel)
		signLevel=0.05;
	end
	
	if isempty(Distro2)
		Distro2=Distro1;
		
	end
	
	if isempty(SameCell)
		SameCell=0.05;
	end
	
	
	%init arrays
	T_equal=false(MonteNumber,1);
	TW_equal=false(MonteNumber,1);
	W_equal=false(MonteNumber,1); 
	T_pval=zeros(MonteNumber,1); 
	TW_pval=zeros(MonteNumber,1); 
	W_pval=zeros(MonteNumber,1); 
	
	IgainT=zeros(MonteNumber,1); 
	IConfT=zeros(MonteNumber,1); 
	IgainTW=zeros(MonteNumber,1); 
	IConfTW=zeros(MonteNumber,1); 
	VarT=zeros(MonteNumber,1); 
	VarTW=zeros(MonteNumber,1); 
	
	%Has to be done before replacing zeros
	Distro1Norm=Distro1/sum(Distro1)*NrDraws;
	Distro2Norm=Distro2/sum(Distro2)*NrDraws;
	
	%modify Distros by replacing zeros with realmin
	Distro1(Distro1==0)=realmin;
	Distro2(Distro2==0)=realmin;
	
	
	%needed values which do not change
	TotalExpA=sum(Distro1);
	TotalExpB=sum(Distro2);
	NrDistPoint=numel(Distro1);
	
	for i=1:MonteNumber
		
		if ~biased 
			%random selection
			if ~SameCell
				SelA=randint(NrDraws,1,[1 NrDistPoint]);	
				SelB=randint(NrDraws,1,[1 NrDistPoint]);
			else
				%use both times the same selection
				%(only useful for Distro1~=Distro2) 
				SelA=randint(NrDraws,1,[1 NrDistPoint]);
				SelB=SelA;
			end
			
			
			ModelA=Distro1(SelA);
			ModelB=Distro2(SelB);
			
			
		else
			%use the forecasts itself as generator (kind of like asking
			%the forecasts where it the most prefers to be tested)
			%In this case the NrDraws can not be hold, but it will be
			%guarantied that the same amount of events are used for
			%the two sets with Distro1 being the master. Also the rates
			%to generate a catalogs will be normalized to NrDraw
			
			TheCatA=poissrnd(Distro1Norm);
			
			%simple case first
			MrOneA=TheCatA==1;
			ModelA=Distro1(MrOneA);
			
			%now with the multiples
			MoreThanOne=find(TheCatA>1);
			
			for j=1:numel(MoreThanOne)
				NrTimes=TheCatA(MoreThanOne(j));
				TheVal=Distro1(MoreThanOne(j));
				ModelA=[ModelA;repmat(TheVal,NrTimes,1)];
			end
			
			NrDraws=numel(ModelA);
			
			if ~SameCell
			%The "depended" Distro.
				Worked=false;
				
				%for loop to prevent infinit loops
				for j=1:100000 
					%disp(j);
					TheCatB=poissrnd(Distro2Norm);
					
					if sum(TheCatB)==NrDraws;
						%simple case first
						MrOneB=TheCatB==1;
						ModelB=Distro2(MrOneB);
						
						%now with the multiples
						MoreThanOne=find(TheCatB>1);
						
						for k=1:numel(MoreThanOne)
							NrTimes=TheCatB(MoreThanOne(k));
							TheVal=Distro2(MoreThanOne(k));
							ModelB=[ModelB;repmat(TheVal,NrTimes,1)];
						end
						
						Worked=true;
						break;
					end
				end
				
				if ~Worked
					error('Equal number of events could not reached');
				
				end
			else
				%use same Selection
				%simple case first
				MrOneB=TheCatA==1;
				ModelB=Distro2(MrOneB);
				
				%now with the multiples
				MoreThanOne=find(TheCatA>1);
				
				for j=1:numel(MoreThanOne)
					NrTimes=TheCatA(MoreThanOne(j));
					TheVal=Distro2(MoreThanOne(j));
					ModelB=[ModelB;repmat(TheVal,NrTimes,1)];
				end
			
			
			end
				
		end
		
		%t-tests
		%--------------
		
		X_A=log(ModelA);
		X_B=log(ModelB);
		
		%normalized log(Lamda) for A and B
		I_A=X_A-(TotalExpA/NrDraws);
		I_B=X_B-(TotalExpB/NrDraws);
		
		%average information gain
		InAB=mean(I_A-I_B);
		
		
		%Student
		SVar=var(I_A-I_B);
		T=InAB/(sqrt(SVar)/sqrt(NrDraws));
		Ttable=tinv(1-signLevel,NrDraws-1);
		IConf= Ttable* SVar.^(1/2) / sqrt(NrDraws);
		T_pval(i)=1-tcdf(abs(T),NrDraws);
		
		%Welch
		S_A=var(I_A);
		S_B=var(I_B);
		S_A_B=sqrt(S_A^2/NrDraws+S_B^2/NrDraws);
			
		%and also an other degree of freedom
		d_f   =  ((S_A^2/NrDraws+S_B^2/NrDraws)^2)/...
			 ((S_A^2/NrDraws)^2/(NrDraws-1)+...
			 (S_B^2/NrDraws)^2/(NrDraws-1));
		
		%this case however the IConf would be difficult.
		T=(mean(I_A)-mean(I_B))/S_A_B;
			
		Ttable=tinv(1-signLevel,d_f);
		IConf2= Ttable* S_A_B;
		TW_pval(i)=1-tcdf(abs(T),d_f);	 
		
		%Wtest
		diffMed=(X_A-X_B)-(TotalExpA-TotalExpB)/NrDraws;
		
		
		MedNoZero=diffMed(diffMed~=0);
		NegVal=MedNoZero<0;
		TheRanks=tiedrank(abs(MedNoZero));
		
		W_plus=sum(TheRanks(~NegVal));
		W_minus=sum(TheRanks(NegVal));
		W_overall=W_plus+W_minus;
		
		%for getting the p-value
		[W_pval(i),h,stats] = signrank(diffMed,0,'alpha',signLevel);
			 
		
		%write some values out
		IgainT(i)=InAB;
		IConfT(i)=IConf;
		IgainTW(i)=InAB;
		IConfTW(i)=IConf2;
		VarT(i)=SVar;
		VarTW(i)=S_A_B;
		
		
			 
	end
	
	
	%Decide which pass and which not
	%Means the Null Hypothesis Mean==0 can not be neglected 
	T_equal=T_pval>signLevel;
	TW_equal=TW_pval>signLevel;
	W_equal=W_pval>signLevel;
	
	%statistic
	PowerT = sum(T_equal)/MonteNumber;
	PowerTW = sum(TW_equal)/MonteNumber;
	PowerW = sum(W_equal)/MonteNumber;
	
	
	%Packit
	OutStruct.T_pval=T_pval;
	OutStruct.TW_pval=TW_pval;
	OutStruct.W_pval=W_pval;
	
	OutStruct.T_equal=T_equal;
	OutStruct.TW_equal=TW_equal;
	OutStruct.W_equal=W_equal;
	
	OutStruct.IgainT=IgainT;
	OutStruct.IConfT=IConfT;
	OutStruct.IgainTW=IgainTW;
	OutStruct.IConfTW=IConfTW;
	OutStruct.VarT=VarT;
	OutStruct.VarTW=VarTW;
	
	OutStruct.NrDraws=NrDraws;
end

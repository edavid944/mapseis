function SteamMachineCSEP(Model,TheCatalog,Name,PlotMode,Offset)
	import mapseis.util.*;
	import mapseis.plot.*;
	import mapseis.projector.*;
	
	
	greyHound=[0.8 0.8 0.7];
	
	if isobject(TheCatalog)
		%MapSeis DataStore
		ZmapCat=getZMAPFormat(TheCatalog);
	else
		ZmapCat=TheCatalog;	
	
	end
	
	
	%find the cells with eq
	countArray=1:numel(Model(:,1));
	TheObserver=zeros(size(countArray));
	
	for i=1:numel(Model(:,1))
		%no parfor steps are probably to small
		InLon=ZmapCat(:,1)>Model(i,1)&ZmapCat(:,1)<=Model(i,2);
		InLat=ZmapCat(:,2)>Model(i,3)&ZmapCat(:,2)<=Model(i,4);
		InDepth=ZmapCat(:,7)>=Model(i,5)&ZmapCat(:,7)<=Model(i,6);
		
		
		TheObserver(i)=sum(InLon&InLat&InDepth);
		
		
	end
	
	%find all cells with at least a single eq in them
	BoomArray=~(TheObserver==0);
	
	toBeBlack=countArray(BoomArray);
	toBeGrey=countArray(~BoomArray);
	toBeGrey3D=toBeGrey(Model(toBeGrey,9)>=0.02);
	figure
	
	switch PlotMode
		case '2D'
			greymen=stem(toBeGrey,Model(toBeGrey,9),'Color',greyHound);
			hold on
			blackmen=stem(toBeBlack,Model(toBeBlack,9),'Color','k');
			hold off
			lab(1)=xlabel('Bin. Nr   ');
			lab(2)=ylabel('Lamda   ');
			ylim([0 1]);
			set(greymen,'LineWidth',2);
			
			
			
		case '3D'
			if isempty(Offset)
				Offset=0;
			end	
			
			ModelConv=Model;
	
			%convert coordinates of the model if needed
			if Offset~=0
				ModelConv(:,[1,3])=ShiftCoords(Model(:,[1,3]),[Offset,0]);
				ModelConv(:,[2,4])=ShiftCoords(Model(:,[2,4]),[Offset,0]);	
			end
				
			greymen=stem3(ModelConv(toBeGrey3D,1),ModelConv(toBeGrey3D,3),ModelConv(toBeGrey3D,9),'Color',greyHound-0.05);
			hold on
			blackmen=stem3(ModelConv(toBeBlack,1),ModelConv(toBeBlack,3),ModelConv(toBeBlack,9),'Color','k');
			hold off
			lab(1)=xlabel('Longitude  ');
			lab(2)=ylabel('Latitude   ');
			lab(3)=zlabel('Lamda   ');
			zlim([0 1]);
			set(greymen,'LineWidth',1);
			
			plotAxis=gca;
			OffSets=[Offset 0];
			%get all ticks and convert them
			xticker=get(plotAxis,'XTick');
			ShiftedLocations=ShiftCoords([xticker',zeros(size(xticker'))],-OffSets);
			set(plotAxis,'XTickLabel',num2str(ShiftedLocations(:,1)));
			
			yticker=get(plotAxis,'YTick');
			ShiftedLocations=ShiftCoords([zeros(size(yticker')),yticker'],-OffSets);
			set(plotAxis,'YTickLabel',num2str(ShiftedLocations(:,2)));
			
			
	end

	
	%format plot
	if ~isempty(Name)
		theTit=title(Name);
		set(theTit,'FontSize',18,'FontWeight','bold','Interpreter','none');
	end
	
	
	set(blackmen,'LineWidth',4);
	set(gca,'LineWidth',2,'FontSize',14,'FontWeight','bold');
	set(lab,'FontSize',16,'FontWeight','bold');
	
	
end

function TheRes=polyMTest(PrepStruct,TheCatalog,numCatalogs,minMag,ParallelMode,FeedItBack)
	%A first prototype of a M-Test rebuild, rought an no features at all
	%Special version for polynomial regions, at the moment only a sketch 
	%but will be improved afterwards
	
	%minMag for EU=5
	
	%TheCatalog is a zmap formate catalog.
	import mapseis.csep_testing.*;
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<6
		FeedItBack=[];
	end	
	
	%FeedItBack=[];
		
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	MagBin=0.1;

	
	%easier here, the regions are fully used, 
	maxMag=max(max(PrepStruct.MaxMags));
	TheMags=(0:MagBin:maxMag)';
	SearchMag=[(minMag:MagBin:maxMag)';Inf];
	MagVec=(minMag:MagBin:maxMag)';
	RawLambMag=zeros(size(TheMags));
	
	
	
	
	%IsUsed=[];
	for i=1:numel(PrepStruct.PolyCoordX)
		RawLambMag(1:numel(PrepStruct.MagVector{i}))=...
			RawLambMag(1:numel(PrepStruct.MagVector{i}))+PrepStruct.CombFMDdiff{i};
		
		%check the magnitude bin contains any active bins
		%IsUsed(i)=any(UsedBins(LogSelector(:,i)));
	end
	CutRawMag=RawLambMag(MagVec>=minMag);
	
	
	
	
	%calculate the log likelihood for the observation
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(SearchMag)-1,1);
	
	
	for i=1:numel(SearchMag)-1
		%no parfor steps are probably to small
		
		InMag=TheCatalog(:,6)>=SearchMag(i)&TheCatalog(:,6)<SearchMag(i+1);
		
		
		TheObserver(i)=sum(InMag);
		
		
		
	end
	
	
	
	
	%LambaSpace=(TotalEq/sum(Mmodel(:,9)))*RawLambaSpace;
	ExpNum=sum(CutRawMag);
		
	%Number of eq in observation
	%TotalEq=numel(TheCatalog(:,1));
	TotalEq=sum(TheObserver); %saver in case a to large catalog is used
	
	LambaSpace=(TotalEq/ExpNum)*CutRawMag;
	if isempty(FeedItBack)
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			%TheCat=poissrnd(Mmodel(:,9));	
			%needs a new random generator
			%TheCat=poissrnd(UnityLamba);
			TheCat=poissrnd_limited(LambaSpace,TotalEq);
			
			LowCat=TheCat;
			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-LambaSpace+LowCat.*log(LambaSpace)-log(factorial(LowCat));
			LogLiker(i)=nansum(LikeIt(~isinf(LikeIt)));
			
			
		end
	else
		LogLiker=FeedItBack;
	
	end
	
	%calc logLikelihood for catalog
	%ObsLiker=nansum(-RawLambaSpace+TheObserver.*log(RawLambaSpace)-log(factorial(TheObserver)));
	
	%LambaSpace = (TotalEq/ExpNum)*RawLambaSpace;
	LikeIt=-LambaSpace+TheObserver.*log(LambaSpace)-log(factorial(TheObserver));
	ObsLiker=nansum(LikeIt(~isinf(LikeIt)));
	
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='MTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

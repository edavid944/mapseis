function PrepStruc = PrepareShapeDataEU(ShapeStruct,ForeYear)
	%Takes the shape structure and brings it into a better form
	%Calculates the FMDs and the weighted FMD
	
	
	%  Wichtig sind folgende Parameter:
	%  X, Y : Koordinaten in Lat, Lon (geografische Koordinaten mit dezimalen)
	%  IDAS: ID der Area Source
	%  MAXMAG01: Maximale Magnitude 1
	%  MAXMAG02: Maximale Magnitude 2
	%  MAXMAG03: Maximale Magnitude 3
	%  MAXMAG04: Maximale Magnitude 4
	%  WMAXMAG01: Gewicht Maximale Magnitude 1
	%  WMAXMAG02: Gewicht Maximale Magnitude 2
	%  WMAXMAG03: Gewicht Maximale Magnitude 3
	%  WMAXMAG04: Gewicht Maximale Magnitude 4
	%  A : a-Wert nei M=0, normiert f�r 1Jahr
	%  B: b-wert
	%  Alle anderen wirst du nicht brauchen.
	
	MagBin=0.1;
	
	NumPoly=numel(ShapeStruct);
	
	PolyCoordX=cell(NumPoly,1);
	PolyCoordY=cell(NumPoly,1);
	IDAS=cell(NumPoly,1);
	MaxMags=zeros(NumPoly,4);
	WMags=zeros(NumPoly,4);
	FourVal=false(NumPoly,1);
	
	bval=zeros(NumPoly,1);
	aval=zeros(NumPoly,1);
	
	FMDs=cell(NumPoly,4);
	FMDdiff=cell(NumPoly,4);;
	
	CombFMDdiff=cell(NumPoly,1);
	CombFMDs=cell(NumPoly,1);
	MagVector=cell(NumPoly,1);
	
	for i=1:NumPoly
		PolyCoordX{i}=ShapeStruct(i).X(1:end-1);
		PolyCoordY{i}=ShapeStruct(i).Y(1:end-1);
		IDAS{i}=ShapeStruct(i).IDAS;
		
		MaxMags(i,1)=ShapeStruct(i).MAXMAG01;
		MaxMags(i,2)=ShapeStruct(i).MAXMAG02;
		MaxMags(i,3)=ShapeStruct(i).MAXMAG03;
		MaxMags(i,4)=ShapeStruct(i).MAXMAG04;
		
		WMags(i,1)=ShapeStruct(i).WMAXMAG01;
		WMags(i,2)=ShapeStruct(i).WMAXMAG02;
		WMags(i,3)=ShapeStruct(i).WMAXMAG03;
		WMags(i,4)=ShapeStruct(i).WMAXMAG04;
		
		FourVal(i)=WMags(i,3)~=0;
		
		bval(i)=ShapeStruct(i).B;
		aval(i)=ShapeStruct(i).A;
		
		%create FMD stuff
		FMDs{i,1}=ForeYear*10.^(aval(i)-bval(i)*(0:MagBin:MaxMags(i,1))');
		FMDdiff{i,1}=[-diff(FMDs{i,1});FMDs{i,1}(end)];
		
		FMDs{i,2}=ForeYear*10.^(aval(i)-bval(i)*(0:MagBin:MaxMags(i,2))');
		FMDdiff{i,2}=[-diff(FMDs{i,2});FMDs{i,2}(end)];
		
		if FourVal(i)
			FMDs{i,3}=ForeYear*10.^(aval(i)-bval(i)*(0:MagBin:MaxMags(i,3))');
			FMDdiff{i,3}=[-diff(FMDs{i,3});FMDs{i,3}(end)];
		
			FMDs{i,4}=ForeYear*10.^(aval(i)-bval(i)*(0:MagBin:MaxMags(i,4))');
			FMDdiff{i,4}=[-diff(FMDs{i,4});FMDs{i,4}(end)];
			
			%Make Weight vectors
			maxLength=max([numel(FMDdiff{i,1}),numel(FMDdiff{i,2}),...
					numel(FMDdiff{i,3}),numel(FMDdiff{i,4})]);
			
					
			W1=zeros(maxLength,1);
			W2=zeros(maxLength,1);
			W3=zeros(maxLength,1);
			W4=zeros(maxLength,1);
			
			FMDWork1=zeros(maxLength,1);
			FMDWork2=zeros(maxLength,1);
			FMDWork3=zeros(maxLength,1);
			FMDWork4=zeros(maxLength,1);
			
			W1(1:numel(FMDdiff{i,1}))=WMags(i,1);
			W2(1:numel(FMDdiff{i,2}))=WMags(i,2);
			W3(1:numel(FMDdiff{i,3}))=WMags(i,3);
			W4(1:numel(FMDdiff{i,4}))=WMags(i,4);
			
			FMDWork1(1:numel(FMDdiff{i,1}))=FMDdiff{i,1};
			FMDWork2(1:numel(FMDdiff{i,2}))=FMDdiff{i,2};
			FMDWork3(1:numel(FMDdiff{i,3}))=FMDdiff{i,3};
			FMDWork4(1:numel(FMDdiff{i,4}))=FMDdiff{i,4};
			
			%total sums
			SumWeight=W1+W2+W3+W4;
			
			CombFMDdiff{i}=(W1.*FMDWork1 + W2.*FMDWork2 +...
					W3.*FMDWork3 + W4.*FMDWork4)./SumWeight;
			
			CombFMDs{i}=flipud(-cumsum(-flipud(CombFMDdiff{i})));
			
			%Magnitude vector
			maxMagAll=max(MaxMags(i,1:4));
			MagVector{i}=(0:MagBin:maxMagAll)';
			
		else
			%Make Weight vectors
			maxLength=max([numel(FMDdiff{i,1}),numel(FMDdiff{i,2})]);
			
			W1=zeros(maxLength,1);
			W2=zeros(maxLength,1);
			
			
			FMDWork1=zeros(maxLength,1);
			FMDWork2=zeros(maxLength,1);
			
			
			W1(1:numel(FMDdiff{i,1}))=WMags(i,1);
			W2(1:numel(FMDdiff{i,2}))=WMags(i,2);
			
			
			FMDWork1(1:numel(FMDdiff{i,1}))=FMDdiff{i,1};
			FMDWork2(1:numel(FMDdiff{i,2}))=FMDdiff{i,2};
			
			
			%total sums
			SumWeight=W1+W2;
			
			CombFMDdiff{i}=(W1.*FMDWork1 + W2.*FMDWork2)./SumWeight;
			
			CombFMDs{i}=flipud(-cumsum(-flipud(CombFMDdiff{i})));
		
			%Magnitude vector
			maxMagAll=max(MaxMags(i,1:2));
			MagVector{i}=(0:MagBin:maxMagAll)';
		
		end
		
		
	end
	
	PrepStruc.IDAS=IDAS;
	PrepStruc.PolyCoordX=PolyCoordX;
	PrepStruc.PolyCoordY=PolyCoordY;
	PrepStruc.MaxMags=MaxMags;
	PrepStruc.WMags=WMags;
	PrepStruc.FourVal=FourVal;
	
	PrepStruc.bval=bval;
	PrepStruc.aval=aval;
	
	PrepStruc.FMDs=FMDs;
	PrepStruc.FMDdiff=FMDdiff;
	
	PrepStruc.CombFMDs=CombFMDs;
	PrepStruc.CombFMDdiff=CombFMDdiff;
	PrepStruc.MagVector=MagVector;

end

function TRTableMaker(Results,NameComb,Filename,ConfigOut,LatexMode)
	%Simple scripts it takes the Result of the TrTest tests and write 
	%them into a table ascii file or a latex formated file
	%In this case Results has to be a cell array with structures, and
	%NameComb has to be a cell matrix with the first column being the Modelname
	%and the following column being the names of the models in the Result
	%structure.
	

	if nargin<4
		LatexMode=false;
		ConfigOut=struct(	'LikeliHood',true,...
					'Percent',true,...
					'Median',false,...
					'Mean',false,...
					'QuantTwoLine',false,...
					'PerfectModel',true,...
					'AltNameList',[[]]);
					
		disp('Standard Config used')
	end
	
	if nargin<5
		LatexMode=false;
	end
	
	SizeEntry=size(NameComb);
	
	
	%Unpack Config
	LikeliHood=ConfigOut.LikeliHood;
	Percent=ConfigOut.Percent;
	PerfectModel=ConfigOut.PerfectModel;
		
	
	%Cut NameComb
	MasterNames=NameComb(:,1);
	ModelNames=NameComb(:,2:end);
	
	
	ShowPart=num2str(sum([Percent,LikeliHood]));
	
	if isfield(ConfigOut,'AltNameList')
		AltNameList=ConfigOut.AltNameList;
	else
		%create an alternatice namelist (without underscore '_'),
		%because Latex does not like that, it is suggested to use a
		%custom list here, because automatic results my not be correct
		for i=2:SizeEntry(2)
			CurName=NameComb{1,i};
			CurName(CurName=='_')=' ';
			AltNameList{i-1}=CurName;
		end
	end	
	
	
	
	if PerfectModel
		IGain=cell(SizeEntry(1)+1,SizeEntry(2)-1);
		PercGain=cell(SizeEntry(1)+1,SizeEntry(2)-1);
		MasterNames={'Perfect Model',MasterNames{:}}';
		MrBold=false(SizeEntry(1)+1,SizeEntry(2)-1);
		GainNum=NaN(SizeEntry(1)+1,SizeEntry(2)-1);
		Repper=SizeEntry(1)+1;
	else
		IGain=cell(SizeEntry(1),SizeEntry(2)-1);
		PercGain=cell(SizeEntry(1),SizeEntry(2)-1);
		MrBold=false(SizeEntry(1),SizeEntry(2)-1);
		GainNum=NaN(SizeEntry(1),SizeEntry(2)-1);
		Repper=SizeEntry(1);
	end
	
	
	StartPoint=1;
	
	if PerfectModel
		%This could have bin in the if before, but it is more readable
		%like this.
		for i=1:SizeEntry(2)-1
			Name=['TRtest_',ModelNames{1,i}]
			CurRes=Results{i}.(Name);
			IGain{1,i}=num2str(-CurRes.MaxGainMonte(1),3);
			PercGain{1,i}='100';
		end
		
		StartPoint=2;
		
	end
	
	
	%get data
	for i=1:SizeEntry(2)-1
		count=StartPoint;
		for k=1:SizeEntry(1);
			Name=['TRtest_',ModelNames{k,i}];	
			CurRes=Results{i}.(Name);
			
			IGain{count,i}=num2str(-CurRes.InABMonte(1),3);
			GainNum(count,i)=-CurRes.InABMonte(1);
			PercGain{count,i}=num2str(CurRes.IGainPercMonte(1),3);
			count=count+1;
		end
			
	
	
	end
	
	%find the winners
	maxiMan=max(GainNum);
	MrBold=GainNum==repmat(maxiMan,Repper,1);
	
	%N-forecasted
	%MakeDark(:,1)=false;
	
	
	%if Meaner
	%	maxiManMean=max(MeanScoreNum);
	%	MakeDarkMean=ScoreNum==repmat(maxiManMean,numel(NameList),1);
	%	MakeDarkMean(:,1)=false;
	%end	
	
	
	%if Medianer
	%	maxiManMed=max(MedianScoreNum);
	%	MakeDarkMed=ScoreNum==repmat(maxiManMed,numel(NameList),1);
	%	MakeDarkMed(:,1)=false;
	%end
	
	
	%Make the Header Lines
	HeaderLine='       ';
	HeaderLineLatex='\hline  Model';
	TabCode='c';
	for i=1:numel(AltNameList);
		HeaderLine=[HeaderLine,'    ',AltNameList{i}];
		HeaderLineLatex=[HeaderLineLatex,' & ',AltNameList{i}];
		TabCode=[TabCode,'|c'];
	end
	
	HeaderLineLatex = [HeaderLineLatex, '   \\'];
	
	
	size(IGain)
	
	%May could be more effective, but it should not matter to much
	if ~LatexMode
		%Normal text file
		%write data to file
		fid = fopen(Filename,'w'); 
		fprintf(fid,'%s',HeaderLine); 
		fprintf(fid,'\n');
	
		
		for i=1:numel(MasterNames)
			
			PercLine=['               '];
			
			if LikeliHood
				LikeLine=[MasterNames{i},'  '];
			else
				LikeLine=[MasterNames{i},'  '];
				PercLine=[MasterNames{i},'  '];
			end
			
			
			
						
			for j=1:SizeEntry(2);
				
				if LikeliHood
					LikeLine=[LikeLine,'   ',IGain{i,j}];
				end
				
				if Percent&~((i==1)&PerfectModel)
					PercLine=[PercLine,'   ',PercGain{i,j},'%'];
				end
			end
			
			if LikeliHood
				fprintf(fid,'%s',LikeLine); 
				fprintf(fid,'\n');
			end

			if Percent&~((i==1)&PerfectModel)
				fprintf(fid,'%s',PercLine); 
				fprintf(fid,'\n');
			end
			
		end
		
		fclose(fid); 
	
	else
		%Latex formated text file (uncomplete, needs some tuning afterwards)
		%write data to file
		
		TabWide=num2str(numel(AltNameList)+1);
		
		fid = fopen(Filename,'w'); 
		
		fprintf(fid,'%s','\begin{table}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{tabular}{|',TabCode,'|}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\multicolumn{',TabWide,'}{c}{Avg. Information Gain against Uniform Rate in $\%$} \\');
		fprintf(fid,'\n');
		
		
		
		fprintf(fid,'%s',HeaderLineLatex); 
		fprintf(fid,'\n');
		
		for i=1:numel(MasterNames)
		
			PercLine=['  '];
			
			if LikeliHood
				if PerfectModel&(i==1)
					LikeLine=['\hline \multirow{1}{*}{\cellcolor[gray]{0.8} Perfect model}']
				else
					LikeLine=['\hline \multirow{',ShowPart,'}{*}{',MasterNames{i},'}  ']
				
				end
			else
				PercLine=['\hline \multirow{',ShowPart,'}{*}{',MasterNames{i},'}  '];
				LikeLine=' ';
			end
			
			
			
			for j=1:SizeEntry(2)-1;
				[i,j] 
				
				if PerfectModel&(i==1)
					LikeLine=[LikeLine,' & \cellcolor[gray]{0.8}  $',IGain{i,j},'$'];
				
				else
					if MrBold(i,j)
						if LikeliHood
							LikeLine=[LikeLine,' & $\textbf{',IGain{i,j},'}$'];
						end
						
						if Percent
							PercLine=[PercLine,' & $\textbf{',PercGain{i,j},'\%}$'];
						end
					else
					
						if LikeliHood
							LikeLine=[LikeLine,' & $',IGain{i,j},'$'];
						end
						
						if Percent
							PercLine=[PercLine,' & $',PercGain{i,j},'\%$'];
						end
					
					end
				end
				
			
			end
			
			%endLine
			LikeLine=[LikeLine, '  \\'];
			PercLine=[PercLine, '  \\'];
			
			%Write Lines
			if LikeliHood
				fprintf(fid,'%s',LikeLine); 
				fprintf(fid,'\n');
			end

			if Percent&~((i==1)&PerfectModel)
				fprintf(fid,'%s',PercLine); 
				fprintf(fid,'\n');
			end
			
			
		end	
		
		
		
		
		%Finalize Table	
		fprintf(fid,'%s','\hline');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{tabular}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\caption{The table shows the avg. information gain per earthquake against an uniform rate model. For every model the average information gain given directly and as percentage of the maximum possible information gain. The model with the largest gain is printed bold.}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\label{tab:ADD A LABEL}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{table}');
		fprintf(fid,'\n');
 
		
		fclose(fid); 
	
	
	end
	
	
end

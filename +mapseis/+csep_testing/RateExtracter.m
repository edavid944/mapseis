function TheRes=RateExtracter(ModelA,TheCatalog)
	%Returns a Rate for each earthquake in the catalog, which inside the
	%testregion
	
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	

	
	

	
	
		
		

	%get a probability for each earthquake in each model
		
	NumOfGrid=numel(ModelA(:,9));
	PosArray=1:NumOfGrid;
	SelArray=[];
	CatSel=[];
	for i=1:numel(TheCatalog(:,1))
		
		InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
		InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
		InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
		InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
		
		if any(InLon&InLat&InDepth&InMag)
			SelArray(end+1)=PosArray(InLon&InLat&InDepth&InMag);
			CatSel(end+1)=i;
		end

	end
		
	TheRes.ModelRates=ModelA(SelArray,9);
	TheRes.IndexModel=SelArray;
	TheRes.IndexCatalog=CatSel;
	TheRes.TotalNumEq=numel(SelArray);
		

end

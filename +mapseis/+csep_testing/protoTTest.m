function TheRes=protoTTest(ModelA,ModelB,TheCatalog,signLevel,BinMode,ParallelMode)
	%A first prototype of a T-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	
	import mapseis.csep_testing.*;
	
	
	if ParallelMode
		matlabpool open
	end
	
	if isempty(signLevel)
		% 5% standard alpha, and it is assumed to be one sided
		signLevel=0.05;
	end
	
	if isempty(BinMode)
		% default: use earthquakes instead of bins
		BinMode=false;
	end
	
	%for safety set cells which are zero to 10^-24, that is more or less zero, but
	%still has a log not -Inf, I did not see models which have such low rates, so it should be outside
	ModelA(ModelA(:,9)==0,9)=realmin;
	ModelB(ModelB(:,9)==0,9)=realmin;
	
	
	%  %use the formulas from Rhoades 2010 (so I hope)
	%  
	%  %now to the observations (ModelA and ModelB have to be on the same grid)
	
	if BinMode
	
		TheObserver=zeros(numel(ModelA(:,1)),1);
			
		
		for i=1:numel(ModelA(:,1))
			%no parfor steps are probably to small
			InLon=TheCatalog(:,1)>=ModelA(i,1)&TheCatalog(:,1)<ModelA(i,2);
			InLat=TheCatalog(:,2)>=ModelA(i,3)&TheCatalog(:,2)<ModelA(i,4);
			InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<ModelA(i,6);
			InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
			
			
			TheObserver(i)=sum(InLon&InLat&InDepth&InMag);
			
			
		end
		
		Bin_w_Eq=TheObserver~=0;
		
		
		X_A=log(ModelA(Bin_w_Eq,9));
		X_B=log(ModelB(Bin_w_Eq,9));
		%InAB=1/sum(TheObserver)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(TheObserver);
		%SVar=1/(sum(TheObserver)-1)*sum((X_A-X_B).^2)-1/(sum(TheObserver)^2-sum(TheObserver))*sum(X_A-X_B)^2;
		%T=InAB/(sqrt(SVar)/sqrt(sum(TheObserver)));
		%Ttable=tinv(1-signLevel/2,sum(TheObserver)-1);
		InAB=1/sum(Bin_w_Eq)*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/sum(Bin_w_Eq);
		SVar=1/(sum(Bin_w_Eq)-1)*sum((X_A-X_B).^2)-1/(sum(Bin_w_Eq)^2-sum(Bin_w_Eq))*sum(X_A-X_B)^2;
		T=InAB/(sqrt(SVar)/sqrt(sum(Bin_w_Eq)));
		Ttable=tinv(1-signLevel,sum(Bin_w_Eq)-1);
		
	else
		%get a probability for each earthquake in each model
		
		LamdaA=NaN;
		LamdaB=NaN;
		
		count=1;
		
		for i=1:numel(TheCatalog(:,1))
			LamA=NaN;
			LamB=NaN;
			InLon=ModelA(:,1)<=TheCatalog(i,1)&ModelA(:,2)>TheCatalog(i,1);	
			InLat=ModelA(:,3)<=TheCatalog(i,2)&ModelA(:,4)>TheCatalog(i,2);	
			InDepth=ModelA(:,5)<=TheCatalog(i,7)&ModelA(:,6)>TheCatalog(i,7);	
			InMag=ModelA(:,7)<=TheCatalog(i,6)&ModelA(:,8)>TheCatalog(i,6);	
			
			if any(InLon&InLat&InDepth&InMag)
				LamA=ModelA(InLon&InLat&InDepth&InMag,9);
				LamB=ModelB(InLon&InLat&InDepth&InMag,9);
				LamdaA(count)=LamA;
				LamdaB(count)=LamB;
				count=count+1;
			end
			%disp(LamA);
			%disp(LamB);
			%LamdaA(i)=LamA;
			%LamdaB(i)=LamB;
		end
		
		X_A=log(LamdaA);
		X_B=log(LamdaB);
		TotalNum=count-1;
		InAB=1/TotalNum*sum(X_A-X_B)-(sum(ModelA(:,9))-sum(ModelB(:,9)))/TotalNum;
		SVar=1/(TotalNum-1)*sum((X_A-X_B).^2)-1/(TotalNum^2-TotalNum)*sum(X_A-X_B)^2;
		T=InAB/(sqrt(SVar)/sqrt(TotalNum));
		Ttable=tinv(1-signLevel,TotalNum-1);
		IConf= Ttable* SVar.^(1/2) / sqrt(TotalNum);
		
	
		Bin_w_Eq=NaN;
	
	end
	
	TheRes.TestType='TTest';
	TheRes.TestVersion=1;
	TheRes.SignLevel=signLevel;
	TheRes.Bins_w_Eq=sum(Bin_w_Eq);
	TheRes.InAB=InAB;
	TheRes.Alibaba=log(LamdaA)-log(LamdaB);
	TheRes.VarianceI=SVar;
	TheRes.T=T;
	TheRes.ttable=Ttable;
	TheRes.IConf=IConf;
	TheRes.TotalNum=TotalNum;
	TheRes.ExpModA=sum(ModelA(:,9));
	TheRes.ExpModB=sum(ModelB(:,9));
	
	if ParallelMode
		matlabpool close
	end
	
	

end

function MatTables=CompareForecastTable(Models,TheCatalog,Names,Filename)
	%The script search the earthquake in the catalog with the grid
	%defined by the Models (cellarray with the models and writes them 
	%into an ascii file as table with the following coloums:
	%CellID   Nr.Eq   Lamda Mod1   Lamda Mod2  Lamda Mod3 ....
	%..
	%..
	%..
	%Summary  Tot Nr  sum Lamda1   sum Lamda2  sum Lamda3 ....
	

	%create density of the catalog
	ModelA=Models{1};
	countArray=1:numel(ModelA(:,1));
	for i=1:numel(ModelA(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>ModelA(i,1)&TheCatalog(:,1)<=ModelA(i,2);
		InLat=TheCatalog(:,2)>ModelA(i,3)&TheCatalog(:,2)<=ModelA(i,4);
		InDepth=TheCatalog(:,7)>=ModelA(i,5)&TheCatalog(:,7)<=ModelA(i,6);
		InMag=TheCatalog(:,6)>=ModelA(i,7)&TheCatalog(:,6)<ModelA(i,8);
		
		
		TheObserver(i)=sum(InLon&InLat&InDepth&InMag);
		
		
	end
	
	%find all cells with at least a single eq in them
	BoomArray=~(TheObserver==0);
	TotalLine=sum(BoomArray);
	TotalEQ=numel(TheCatalog(:,1));
	
	%Header for the file
	HeaderLine={'ID','Num.Eq.',Names{:}};
	
	%create table
	CompTable=zeros(TotalLine,numel(Models)+2);
	SumLine=zeros(1,numel(Models)+2);

	
	%enter the ID and Nr.Eq rw
	CompTable(:,1) = countArray(BoomArray);
	CompTable(:,2) = TheObserver(BoomArray);
	SumLine(1)=-1;
	SumLine(2)=TotalEQ;
	
	for i=3:numel(Models)+2
		TheMod=Models{i-2};
		CompTable(:,i)=	TheMod(BoomArray,9);
		SumLine(i)=sum(TheMod(:,9));
		
	end	

	if ~isempty(Filename)
		%write to file
		fid = fopen(Filename,'w'); 
		Heath='ID';
		for i=2:numel(HeaderLine);
			Heath=[Heath,';',HeaderLine{i}];
		end
		
		fprintf(fid,'%s',Heath); 
		fprintf(fid,'\n');
		
		%now go through the lines
		for i=1:TotalLine
			TheLine=[num2str(CompTable(i,1))];
			for j=2:numel(Models)+2
				TheLine=[TheLine,';',num2str(CompTable(i,j))];
			end
			fprintf(fid,'%s',TheLine); 
			fprintf(fid,'\n');
		end
		
		%Write final lines and close
		fprintf(fid,'%s','----------------'); 
		fprintf(fid,'\n');
		
		EndLine='Summary:';
		for i=2:numel(SumLine);
			EndLine=[EndLine,';',num2str(SumLine(i))];
		end
		
		fprintf(fid,'%s',EndLine); 
		fprintf(fid,'\n');
		
		%close file
		fclose(fid); 
		
	end
	
	%Pack data
	MatTables.CompTable=CompTable;
	MatTables.SummaryLine=SumLine;
	MatTables.Header=HeaderLine;
	
	
	%Plot results in a figure
	figure
	plot(CompTable(:,2),'Color','k','LineWidth',3)
	hold on
	ColorArray={'r','b','g','c','m'};
	MrColor=1;
	for i=3:numel(Models)+2
		plot(CompTable(:,i),'Color',ColorArray{i},'LineWidth',1.5);
		MrColor=mod(MrColor,5)+1;
	end
	
	xlabel('  ');
	xlim([0 TotalLine-1]);
	ylabel('Nr. of Eq / Lamda   ');
	theTit=title('Gridcell Comparison: Models vs. Observation');
	set(gca,'LineWidth',2,'FontSize',12);
	set(theTit,'FontSize',14,'FontWeight','bold','Interpreter','none');
	hold off
	Legend(HeaderLine(2:end));
	
	
	
	
end

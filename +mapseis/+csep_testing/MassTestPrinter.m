function MassTestPrinter(Results,TheFold,FilePrefix,PaperBoy)
	%plots everything there is, it is just a script with no options
	%it goes throught the 3 models of the Pacific testseries
	
	%%%
	%The try blocks are bad programming style I have to say, but they work, I have to
	%rework the code anyway later. The problem is that not always all tests are done
	%without out the the try-blocks it just crashes, checking what exists and what not
	%on the other hand cost probably more computing time.
	%%%
	
	import mapseis.plot.*;
	
	if nargin<4
		PaperBoy=false;
	end	
	
	
	ModelNames=Results.Names;
	
	oldFold=pwd;
	%first run as template for the rest
	ResStruct=struct(	'Name',[],...
				'TestType','Ntest',...
				'PlotType',[],...
				'PrintText',true,...
				'ScoreSwitch','meanorg',...
				'DisplayName',[],...
				'Colors',[0.5 0.5 0.7],...
				'LineStyle','-',...
				'LineWidth',3,...
				'ScoreWidth',2,...
				'PaperBoy',PaperBoy,...
				'MiniHist',false);
				
				
	TestModesN={'Hist','CDF','HScore','LScore'};
	%N-test 'original'
	ResStruct.ScoreSwitch='original';
	for j=1:numel(TestModesN)
		ResStruct.PlotType=TestModesN{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
			%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Ntest_Original_',TestModesN{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
		end
	end
	
	
	%N-test 'mean'
	for j=1:numel(TestModesN)
		ResStruct.PlotType=TestModesN{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
			%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Ntest_Mean_',TestModesN{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
		end
	end
	
	
	%N-test 'median'
	ResStruct.ScoreSwitch='medianorg';
	for j=1:numel(TestModesN)
		ResStruct.PlotType=TestModesN{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
			%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Ntest_Median_',TestModesN{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
		end
	end
	
	
	
	%Stest 'original'
	ResStruct.ScoreSwitch='original';
	ResStruct.TestType='Stest';
	TestModesS={'Hist','CDF','Score'};
	for j=1:numel(TestModesS)
		ResStruct.PlotType=TestModesS{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Stest_Original_',TestModesS{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			close(fig);
		end
	end
	
	
	
	
	%Stest 'mean'
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='Stest';
	TestModesS={'Hist','CDF','Score'};
	for j=1:numel(TestModesS)
		ResStruct.PlotType=TestModesS{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Stest_Mean_',TestModesS{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			close(fig);
		end
	end
	
	
	%Stest 'median'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='Stest';
	TestModesS={'Hist','CDF','Score'};
	for j=1:numel(TestModesS)
		ResStruct.PlotType=TestModesS{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Stest_Median_',TestModesS{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			close(fig);
		end
	end
	
	
	
	%Mtest 'original'
	ResStruct.ScoreSwitch='original';
	ResStruct.TestType='Mtest';
	TestModesS={'Hist','CDF','Score'};
	for j=1:numel(TestModesS)
		ResStruct.PlotType=TestModesS{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Mtest_Original_',TestModesS{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			close(fig);
		end
	end
	
	
	
	%Mtest 'mean'
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='Mtest';
	TestModesS={'Hist','CDF','Score'};
	for j=1:numel(TestModesS)
		ResStruct.PlotType=TestModesS{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Mtest_Mean_',TestModesS{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			close(fig);
		end
	end
	
	
	%Mtest 'median'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='Mtest';
	TestModesS={'Hist','CDF','Score'};
	for j=1:numel(TestModesS)
		ResStruct.PlotType=TestModesS{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Mtest_Median_',TestModesS{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			close(fig);
		end
	end
	
	
	
	
	
	%Ltest 'original'
	ResStruct.ScoreSwitch='original';
	ResStruct.TestType='Ltest';
	TestModesL={'Hist','CDF','Score'};
	for j=1:numel(TestModesL)
		ResStruct.PlotType=TestModesL{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Ltest_Original_',TestModesL{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end	
			
			close(fig);
		end
	end
	
	
	
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='Ltest';
	TestModesL={'Hist','CDF','Score'};
	for j=1:numel(TestModesL)
		ResStruct.PlotType=TestModesL{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Ltest_Mean_',TestModesL{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end	
			
			close(fig);
		end
	end
	
	
	%Ltest 'median'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='Ltest';
	TestModesL={'Hist','CDF','Score'};
	for j=1:numel(TestModesL)
		ResStruct.PlotType=TestModesL{j};		
		for i=1:numel(ModelNames)			
			
			ResStruct.Name=ModelNames{i};
			fig=figure;
			plotAxis=gca;
			
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
				%create filename
				filename=[FilePrefix,'_Ltest_Median_',TestModesL{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end	
			
			close(fig);
		end
	end
	
	
	
	%Rtest 'mean'
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='Rtest';
	TestModesR={'Hist','CDF','Score'};
	for j=1:numel(TestModesR)
		ResStruct.PlotType=TestModesR{j};		
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			for k=1:numel(DependModels);
			
				ResStruct.Name={ModelNames{i},DependModels{k}};
				fig=figure;
				plotAxis=gca;
				
				try
					%plot
					[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
					%create filename
					filename=[FilePrefix,'_Rtest_Mean_',TestModesR{j},'_',ModelNames{i},'_',DependModels{k},'.eps'];
					cd(TheFold)
					saveas(fig,filename,'psc');
					cd(oldFold);
				end
				
				close(fig);
			end	
		end
	end
	
	
	%Rtest 'median'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='Rtest';
	TestModesR={'Hist','CDF','Score'};
	for j=1:numel(TestModesR)
		ResStruct.PlotType=TestModesR{j};		
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			for k=1:numel(DependModels);
			
				ResStruct.Name={ModelNames{i},DependModels{k}};
				fig=figure;
				plotAxis=gca;
				
				try
					%plot
					[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
					%create filename
					filename=[FilePrefix,'_Rtest_Median_',TestModesR{j},'_',ModelNames{i},'_',DependModels{k},'.eps'];
					cd(TheFold)
					saveas(fig,filename,'psc');
					cd(oldFold);
				end
				
				close(fig);
			end	
		end
	end
	
	%Ttest 'mean'
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='Ttest';
	TestModesT={'HistI','HistIConf','HistT','ScoreT','ScoreI'};
	for j=1:numel(TestModesT)
		ResStruct.PlotType=TestModesT{j};		
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			
			
			ResStruct.Name={ModelNames{i},DependModels{:}};
			fig=figure;
			plotAxis=gca;
			
			try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_Ttest_Mean_',TestModesT{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%Ttest 'median'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='Ttest';
	TestModesT={'HistI','HistIConf','HistT','ScoreT','ScoreI'};
	for j=1:numel(TestModesT)
		ResStruct.PlotType=TestModesT{j};		
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			
			
			ResStruct.Name={ModelNames{i},DependModels{:}};
			fig=figure;
			plotAxis=gca;
			
			try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_Ttest_Median_',TestModesT{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%modified T-test plot with simpler errorbars
	ResStruct.PlotType=TestModesT{2};	
	ResStruct.ScoreSwitch='original';	
	for i=1:numel(ModelNames)			
		
		notMe=~strcmp(ModelNames,ModelNames{i});
		DependModels=ModelNames(notMe);
		
		
		ResStruct.Name={ModelNames{i},DependModels{:}};
		fig=figure;
		plotAxis=gca;
		
		try
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
			%create filename
			filename=[FilePrefix,'_Ttest_',TestModesT{2},'2_',ModelNames{i},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
			
	end
	
	
	ResStruct.ScoreSwitch='meanorg';	
	
	
	
	
	
	%Wtest 'mean'
	ResStruct.TestType='Wtest';
	TestModesW={'Hist','W+Score','W-Score','WScore'};
	PrintModes={'Hist','WPlus','WMinus','WScore'};
	for j=1:numel(TestModesW)
		ResStruct.PlotType=TestModesW{j};		
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			
			
			ResStruct.Name={ModelNames{i},DependModels{:}};
			fig=figure;
			plotAxis=gca;
			
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_Wtest_Mean_',PrintModes{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%Wtest 'median'
	ResStruct.ScoreSwitch='medianorg';	
	ResStruct.TestType='Wtest';
	TestModesW={'Hist','W+Score','W-Score','WScore'};
	PrintModes={'Hist','WPlus','WMinus','WScore'};
	for j=1:numel(TestModesW)
		ResStruct.PlotType=TestModesW{j};		
		for i=1:numel(ModelNames)			
			
			notMe=~strcmp(ModelNames,ModelNames{i});
			DependModels=ModelNames(notMe);
			
			
			ResStruct.Name={ModelNames{i},DependModels{:}};
			fig=figure;
			plotAxis=gca;
			
			try
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_Wtest_Median_',PrintModes{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%----------------------------------------------
	
	
	%Comparing test N-,S-,L-test 'original'
	ResStruct.Name=ModelNames;
	ResStruct.TestType='Ntest';
	ResStruct.PlotType='CompLow'
	ResStruct.ScoreSwitch='original';
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompLow_Original_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompHigh';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompHigh_Original_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	ResStruct.PlotType='CompNumber';
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompNumber_Original_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	TestNames={'Ntest','Stest','Ltest'};
	ResStruct.PlotType='CompScore';
	for i=1:numel(TestNames)
		ResStruct.TestType=TestNames{i};
		fig=figure;
		plotAxis=gca;
			
		try		
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
			%create filename
			filename=[FilePrefix,'_',TestNames{i},'_CompScore_Original_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
	end
	
	
	
	
	%Comparing test N-,S-,L-test 'mean'
	ResStruct.Name=ModelNames;
	ResStruct.TestType='Ntest';
	ResStruct.PlotType='CompLow'
	ResStruct.ScoreSwitch='meanorg';
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompLow_Mean_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompHigh';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompHigh_Mean_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	ResStruct.PlotType='CompNumber';
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompNumber_Mean_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	TestNames={'Ntest','Stest','Ltest'};
	ResStruct.PlotType='CompScore';
	for i=1:numel(TestNames)
		ResStruct.TestType=TestNames{i};
		fig=figure;
		plotAxis=gca;
			
		try		
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
			%create filename
			filename=[FilePrefix,'_',TestNames{i},'_CompScore_Mean_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
	end
	
	
	%-------------------------------------------------
	
	
	%Comparing test N-,S-,L-test 'MEDIAN'
	ResStruct.Name=ModelNames;
	ResStruct.TestType='Ntest';
	ResStruct.PlotType='CompLow'
	ResStruct.ScoreSwitch='medianorg';
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompLow_Median_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompHigh';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompHigh_Median_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	ResStruct.PlotType='CompNumber';
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_CompNumber_Median_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	TestNames={'Ntest','Stest','Ltest'};
	ResStruct.PlotType='CompScore';
	for i=1:numel(TestNames)
		ResStruct.TestType=TestNames{i};
		fig=figure;
		plotAxis=gca;
			
		try		
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
			%create filename
			filename=[FilePrefix,'_',TestNames{i},'_CompScore_Median_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
	end
	
	
	
	%----------------------------------------
	%Comparing test N-,S-,L-test (minihist) 'original'
	ResStruct.Name=ModelNames;
	ResStruct.ScoreSwitch='original';
	ResStruct.TestType='Ntest';
	ResStruct.PlotType='CompLow';
	ResStruct.MiniHist=true;
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompLow_Original_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompHigh';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompHigh_Original_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompNumber';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompNumber_Original_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);

	
	TestNames={'Ntest','Stest','Ltest'};
	ResStruct.PlotType='CompScore';
	for i=1:numel(TestNames)
		ResStruct.TestType=TestNames{i};
		fig=figure;
		plotAxis=gca;
		
		try
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
			%create filename
			filename=[FilePrefix,'_',TestNames{i},'_MiniHist_CompScore_Original_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
	end
	
	
	
	%----------------------------------------------------
	
	%Comparing test N-,S-,L-test (minihist) 'MEAN'
	ResStruct.Name=ModelNames;
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='Ntest';
	ResStruct.PlotType='CompLow';
	ResStruct.MiniHist=true;
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompLow_Mean_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompHigh';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompHigh_Mean_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompNumber';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompNumber_Mean_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);

	
	TestNames={'Ntest','Stest','Ltest'};
	ResStruct.PlotType='CompScore';
	for i=1:numel(TestNames)
		ResStruct.TestType=TestNames{i};
		fig=figure;
		plotAxis=gca;
		
		try
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
			%create filename
			filename=[FilePrefix,'_',TestNames{i},'_MiniHist_CompScore_Mean_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
	end
	
	
	
	%----------------------------------------------------
	
	
	
	%Comparing test N-,S-,L-test (minihist) 'MEDIAN'
	ResStruct.Name=ModelNames;
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='Ntest';
	ResStruct.PlotType='CompLow';
	ResStruct.MiniHist=true;
	fig=figure;
	plotAxis=gca;
	
	try			
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompLow_Median_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompHigh';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompHigh_Median_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);
	
	
	ResStruct.PlotType='CompNumber';
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
		%create filename
		filename=[FilePrefix,'_Ntest_MiniHist_CompNumber_Median_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
	
	close(fig);

	
	TestNames={'Ntest','Stest','Ltest'};
	ResStruct.PlotType='CompScore';
	for i=1:numel(TestNames)
		ResStruct.TestType=TestNames{i};
		fig=figure;
		plotAxis=gca;
		
		try
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
			%create filename
			filename=[FilePrefix,'_',TestNames{i},'_MiniHist_CompScore_Median_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
		
		close(fig);
	end
	
	
	%----------------------
	
	
	%TR-test 'Mean'
	ResStruct.ScoreSwitch='meanorg';
	ResStruct.TestType='TRtest';
	NoLoopNeed={'HistI','HistIConf','HistIPerc','HistT'}
	TestModesT={'ScoreT','ScoreI'};
	for j=1:numel(NoLoopNeed)
		ResStruct.PlotType=NoLoopNeed{j};		
			
			
		ResStruct.Name=ModelNames;
		fig=figure;
		plotAxis=gca;
			
		try	
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
			%create filename
			filename=[FilePrefix,'_TRtest_Mean_',NoLoopNeed{j},'_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
			
		close(fig);
				
		
	end
	
	for j=1:numel(TestModesT)
		ResStruct.PlotType=TestModesT{j};		
		for i=1:numel(ModelNames)			
						
			ResStruct.Name={ModelNames{i}};
			fig=figure;
			plotAxis=gca;
			
			try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_TRtest_Mean_',TestModesT{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%----------------------------------------
	
	
	%TR-test 'Median'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='TRtest';
	NoLoopNeed={'HistI','HistIConf','HistIPerc','HistT'}
	TestModesT={'ScoreT','ScoreI'};
	for j=1:numel(NoLoopNeed)
		ResStruct.PlotType=NoLoopNeed{j};		
			
			
		ResStruct.Name=ModelNames;
		fig=figure;
		plotAxis=gca;
			
		try	
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
			%create filename
			filename=[FilePrefix,'_TRtest_Median_',NoLoopNeed{j},'_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
			
		close(fig);
				
		
	end
	
	for j=1:numel(TestModesT)
		ResStruct.PlotType=TestModesT{j};		
		for i=1:numel(ModelNames)			
						
			ResStruct.Name={ModelNames{i}};
			fig=figure;
			plotAxis=gca;
			
			try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_TRtest_Median_',TestModesT{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%----------------------------------------
	
	
	%modified T-test plot with simpler errorbars
	ResStruct.PlotType=NoLoopNeed{2};	
	ResStruct.ScoreSwitch='original';	
			
		
		
	ResStruct.Name=ModelNames;
	fig=figure;
	plotAxis=gca;
		
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
		
		%create filename
		filename=[FilePrefix,'_TRtest_',NoLoopNeed{2},'2_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
		
	close(fig);
	
	
	ResStruct.PlotType=NoLoopNeed{3};
	ResStruct.Name=ModelNames;
	fig=figure;
	plotAxis=gca;
	
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
		
		%create filename
		filename=[FilePrefix,'_TRtest_',NoLoopNeed{3},'2_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
		
	close(fig);
	
	
	ResStruct.ScoreSwitch='meanorg';	
	
	
	
	%TRInv-test 'mean'
	ResStruct.TestType='TRInvtest';
	NoLoopNeed={'HistI','HistIConf','HistT'}
	TestModesT={'ScoreT','ScoreI'};
	for j=1:numel(NoLoopNeed)
		ResStruct.PlotType=NoLoopNeed{j};		
			
			
		ResStruct.Name=ModelNames;
		fig=figure;
		plotAxis=gca;
			
		try	
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
			%create filename
			filename=[FilePrefix,'_TRInvtest_Mean_',NoLoopNeed{j},'_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
			
		close(fig);
				
		
	end
	
	for j=1:numel(TestModesT)
		ResStruct.PlotType=TestModesT{j};		
		for i=1:numel(ModelNames)			
						
			ResStruct.Name={ModelNames{i}};
			fig=figure;
			plotAxis=gca;
			
			try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_TRInvtest_Mean_',TestModesT{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	%------------------------------------------------------------
	
	%TRInv-test 'mean'
	ResStruct.ScoreSwitch='medianorg';
	ResStruct.TestType='TRInvtest';
	NoLoopNeed={'HistI','HistIConf','HistT'}
	TestModesT={'ScoreT','ScoreI'};
	for j=1:numel(NoLoopNeed)
		ResStruct.PlotType=NoLoopNeed{j};		
			
			
		ResStruct.Name=ModelNames;
		fig=figure;
		plotAxis=gca;
			
		try	
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			
			%create filename
			filename=[FilePrefix,'_TRInvtest_Median_',NoLoopNeed{j},'_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
			
		close(fig);
				
		
	end
	
	for j=1:numel(TestModesT)
		ResStruct.PlotType=TestModesT{j};		
		for i=1:numel(ModelNames)			
						
			ResStruct.Name={ModelNames{i}};
			fig=figure;
			plotAxis=gca;
			
			try	
				%plot
				[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
				
				%create filename
				filename=[FilePrefix,'_TRInvtest_Median_',TestModesT{j},'_',ModelNames{i},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
			
			close(fig);
				
		end
	end
	
	
	%------------------------------------------
	
	%modified T-test plot with simpler errorbars
	ResStruct.PlotType=NoLoopNeed{2};	
	ResStruct.ScoreSwitch='original';	
			
		
		
	ResStruct.Name=ModelNames;
	fig=figure;
	plotAxis=gca;
		
	try
		%plot
		[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
		
		%create filename
		filename=[FilePrefix,'_TRInvtest_',NoLoopNeed{2},'2_',ModelNames{1},'.eps'];
		cd(TheFold)
		saveas(fig,filename,'psc');
		cd(oldFold);
	end
		
	close(fig);
			
	
	
	
	ResStruct.ScoreSwitch='meanorg';	
	
	
	
end

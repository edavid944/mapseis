function [X Y Pgauss] = GaussKernel2D(XRange,YRange,MidPoint,Kwidth)
	%creates a gaussian smoothing kernel at location MidPoint with the
	%width of Kwidth
	
	%generate grid
	[X Y] = meshgrid(XRange,YRange);
	
	%generate distance to ever point in the grid
	DistRay = ((X-MidPoint(1)).^2 + (Y-MidPoint(2)).^2).^(1/2);
	Pgauss = 1/(2*pi*Kwidth^2) * exp((-DistRay.^2)./(2*Kwidth^2));
	
	
	
end

function TheRes=SHprotoMTest(Mmodel,TheCatalog,numCatalogs,ParallelMode,FeedItBack)
	%A first prototype of a S-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
    %SH jan12
	import mapseis.csep_testing.*;
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<5
		FeedItBack=[];
	end	
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	
	
	
    
			
	%No support for iregular grids at the moment, the magnitude bins have to similar
	%at each locaction

	%All cell having the same locations have to summed in the each catalog
	TheMags=unique(Mmodel(:,7));   
	for i=1:numel(TheMags)
		LogSelector(:,i)=Mmodel(:,7)==TheMags(i);
	end
	
	%Number of MagBins
	NumMag=numel(TheMags);
	
	%Number of Remainingbins
	NumRemBin=sum(LogSelector(:,1));
	
	%summary coords (take the first mag bin)
	RawLambaMag=zeros(NumMag,1);
	for i=1:NumMag
        	RawLambaMag(i)=sum(Mmodel(LogSelector(:,i),9));
        end
    

	
    
    
% 	%NEW - M TEST!!
% 	%All cells having the same magnitude have to be summed in the each catalog    
%     Mmodel(:,11)=0;
%     for k=1:NumRemBin
%         Mmodel((k-1)*NumMag+1:k*NumMag,11)=k;
%     end
%     TheSpace=unique(Mmodel(:,11)); 
%     TheSpace=TheSpace(1:50);
%     for i=1:numel(TheSpace)
% 		LogSelector2(:,i)=Mmodel(:,11)==TheSpace(i);
%     end
%     %Number of SpaceBins
% 	NumSpace=numel(TheSpace);
%     %
%     NumRemBin=sum(LogSelector2(:,1));
% 	%summary mags (take the first space bin)
% 	SumMod=Mmodel(LogSelector2(:,1),:);    
    
    
	
	%calculate the log likelihood for the observation
	
	%calc the num of events for each magnitude bin
	TheObserver=zeros(NumMag,1);
	
	for i=1:NumMag
		%no parfor steps are probably to small
		%InLon=TheCatalog(:,1)>=Mmodel(i,1)&TheCatalog(:,1)<Mmodel(i,2);
		%InLat=TheCatalog(:,2)>=Mmodel(i,3)&TheCatalog(:,2)<Mmodel(i,4);
		InDepth=TheCatalog(:,7)>=Mmodel(i,5)&TheCatalog(:,7)<Mmodel(i,6);
		InMag=TheCatalog(:,6)>=Mmodel(i,7)&TheCatalog(:,6)<Mmodel(i,8);
		
		
		TheObserver(i)=sum(InDepth&InMag);
	end
	
	
	
	
% 	%Normalize Lamba and adding up all cells
% 
% 	rawAr2=repmat(Mmodel(:,9),1,NumSpace);
% 	selectit2=rawAr2(LogSelector2);
% 	
% 	%Add up Lambas (Space)
% 	RawLambaMag=sum(reshape(selectit2,NumRemBin,NumSpace),2);
% 	
%     %---
    
	%LambaSpace=(TotalEq/sum(Mmodel(:,9)))*RawLambaSpace;
	ExpNum=sum(Mmodel(:,9));
	
	%Number of eq in observation
	TotalEq=numel(TheCatalog(:,1));
	
	LambaMag=(TotalEq/ExpNum)*RawLambaMag;
	UnityLamba=ExpNum*RawLambaMag;
	if isempty(FeedItBack)
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			%TheCat=poissrnd(Mmodel(:,9));	
			%needs a new random generator
			%TheCat=poissrnd(UnityLamba);
			
			TheCat=poissrnd_limited(UnityLamba,TotalEq);
			
			%TotalCat=sum(TheCat);
			
			%LambaSpace = (TotalCat/ExpNum)*RawLambaSpace;
			%LambaSpace=RawLambaSpace;
			
			%Now Sum up the magbins
			%rawMultCat=repmat(TheCat,1,NumMag);
			%selMultCat=rawMultCat(LogSelector);
			%LowCat=sum(reshape(selMultCat,NumRemBin,NumMag),2);
			LowCat=TheCat;
			
			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-LambaMag+LowCat.*log(LambaMag)-log(factorial(LowCat));
			LogLiker(i)=nansum(LikeIt(~isinf(LikeIt)));
			
			
		end
	else
		LogLiker=FeedItBack;
	
	end
	
	%calc logLikelihood for catalog
	%ObsLiker=nansum(-RawLambaSpace+TheObserver.*log(RawLambaSpace)-log(factorial(TheObserver)));
	
	%LambaSpace = (TotalEq/ExpNum)*RawLambaSpace;
	LikeIt=-LambaMag+TheObserver.*log(LambaMag)-log(factorial(TheObserver));
	ObsLiker=nansum(LikeIt(~isinf(LikeIt)));
	
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='MTest';
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

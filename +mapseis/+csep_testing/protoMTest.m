function TheRes=protoMTest(Mmodel,TheCatalog,numCatalogs,ParallelMode,FeedItBack)
	%A first prototype of a M-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	import mapseis.csep_testing.*;
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<5
		FeedItBack=[];
	end	

	%added column 10 if not defined (it should be)
	if numel(Mmodel(1,:))<10
		Mmodel(:,10)=true(numel(Mmodel(:,1),1));
	end
	
	UsedBins=logical(Mmodel(:,10));
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	
	%set every bin not used to zero (to avoid that the influence the total)
	%UnModRate=Mmodel(:,9);
	Mmodel(~UsedBins,9)=0;
	
			
	%No support for iregular grids at the moment, the bins have to similar
	%at each locaction

	%All cell having the same locations have to summed
	TheMags=unique(Mmodel(:,7));  
	%IsUsed=[];
	for i=1:numel(TheMags)
		LogSelector(:,i)=Mmodel(:,7)==TheMags(i);
		
		%check the magnitude bin contains any active bins
		%IsUsed(i)=any(UsedBins(LogSelector(:,i)));
	end

	%Number of MagBins
	NumMag=numel(TheMags(:,1));
	
	%Number of Remainingbins
	%NumRemBin=sum(LogSelector(:,1));
	NumRemBin=numel(unique(Mmodel(:,7)));
	
	%summary Magnitude Bins 
	SumMod=[unique(Mmodel(:,7)),unique(Mmodel(:,8))];
	
	
	
	%calculate the log likelihood for the observation
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(SumMod(:,1)),1);
	TempObs=zeros(numel(Mmodel(:,1)),1);
	
	
	for i=1:numel(Mmodel(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>=Mmodel(i,1)&TheCatalog(:,1)<Mmodel(i,2);
		InLat=TheCatalog(:,2)>=Mmodel(i,3)&TheCatalog(:,2)<Mmodel(i,4);
		InDepth=TheCatalog(:,7)>=Mmodel(i,5)&TheCatalog(:,7)<Mmodel(i,6);
		InMag=TheCatalog(:,6)>=Mmodel(i,7)&TheCatalog(:,6)<Mmodel(i,8);
		
		
		TempObs(i)=sum(InLon&InLat&InDepth&InMag&UsedBins(i));
		
		
		
	end
	
	for i=1:numel(LogSelector(1,:))
		disp(sum(TempObs(LogSelector(:,i))))
		disp('+++++')
		TheObserver(i)=sum(TempObs(LogSelector(:,i)));
		
	end
	disp((TheObserver))
	disp('=====')
	disp(numel(TheObserver~=0))
	disp(max(TheObserver))
	disp(min(TheObserver))
	disp('------')
	
	
	%Normalize Lamba and adding up all mags
	RawLambaMag=zeros(NumMag,1);
	for i=1:NumMag
        	RawLambaMag(i)=sum(Mmodel(LogSelector(:,i)&UsedBins,9));
        end
	disp(RawLambaMag)
	%LambaSpace=(TotalEq/sum(Mmodel(:,9)))*RawLambaSpace;
	ExpNum=sum(Mmodel(UsedBins,9));
		
	%Number of eq in observation
	%TotalEq=numel(TheCatalog(:,1));
	TotalEq=sum(TheObserver); %saver in case a to large catalog is used
	
	LambaSpace=(TotalEq/ExpNum)*RawLambaMag;
	UnityLamba=ExpNum*RawLambaMag;
	if isempty(FeedItBack)
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			%TheCat=poissrnd(Mmodel(:,9));	
			%needs a new random generator
			%TheCat=poissrnd(UnityLamba);
			TheCat=poissrnd_limited(LambaSpace,TotalEq);
			
			%TotalCat=sum(TheCat);
			
			
			%LambaSpace = (TotalCat/ExpNum)*RawLambaSpace;
			%LambaSpace=RawLambaSpace;
			
			%Now Sum up the magbins
			%rawMultCat=repmat(TheCat,1,NumMag);
			%selMultCat=rawMultCat(LogSelector);
			%LowCat=sum(reshape(selMultCat,NumRemBin,NumMag),2);
			LowCat=TheCat;
			
			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-LambaSpace+LowCat.*log(LambaSpace)-log(factorial(LowCat));
			LogLiker(i)=nansum(LikeIt(~isinf(LikeIt)));
			
			
		end
	else
		LogLiker=FeedItBack;
	
	end
	
	%calc logLikelihood for catalog
	%ObsLiker=nansum(-RawLambaSpace+TheObserver.*log(RawLambaSpace)-log(factorial(TheObserver)));
	
	%LambaSpace = (TotalEq/ExpNum)*RawLambaSpace;
	LikeIt=-LambaSpace+TheObserver.*log(LambaSpace)-log(factorial(TheObserver));
	ObsLiker=nansum(LikeIt(~isinf(LikeIt)));
	
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='MTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

function TTableMaker(Results,NameList,Filename,ConfigOut,LatexMode)
	%Simple scripts it takes the Result and write everthing into a table ascii file
	
	import mapseis.csep_testing.*;
	
	if nargin<4
		LatexMode=false;
		ConfigOut=struct(	'Original',true,...
					'Mean',true,...
					'PVal_T',false,...
					'PVal_W',false,...
					'Median',false,...
					'QuantTwoLine',false,...
					'AltNameList',[[]]);
					
		disp('Standard Config used')
	end
	
	
	if nargin<5
		LatexMode=false;
	end
	
	
	
	%Check if Wtest exist
	Name=['Wtest_',NameList{1},'_',NameList{2}];
	if ~isfield(Results,Name)&ConfigOut.PVal_W
		ConfigOut.PVal_W=false;
	
	end
	
	
	
	%Check if P_value is available
	Name=['Ttest_',NameList{1},'_',NameList{2}];
	TTest=Results.(Name);
	if ~isfield(TTest,'p_org')&ConfigOut.PVal_T
		Results=AddPVal(Results,NameList);
	end
	
	
	
	if isfield(ConfigOut,'QuantTwoLine')
		QuantTwoLine=ConfigOut.QuantTwoLine;
	else
		QuantTwoLine=false;
	end	
	
	
	if isfield(ConfigOut,'AltNameList')
		AltNameList=ConfigOut.AltNameList;
	else
		%create an alternatice namelist (without underscore '_'),
		%because Latex does not like that
		for i=1:numel(NameList)
			CurName=NameList{i};
			CurName(CurName=='_')=' ';
			AltNameList{i}=CurName;
		end
	end	
	
	
	Medianer=ConfigOut.Median;
	Meaner=ConfigOut.Mean;
	PT_select=ConfigOut.PVal_T;
	PW_select=ConfigOut.PVal_W;
	
	
	if ~QuantTwoLine
		ShowPart=num2str(sum([Medianer,Medianer,Medianer,Medianer,Meaner,Meaner,...
				PT_select,PW_select])+1);
	else
		ShowPart=num2str(sum([Medianer,Medianer,Medianer,Medianer,Medianer,...
					Medianer,Meaner,Meaner,PT_select,PW_select])+1);
	end
	
	
	IgainOrg=cell(numel(NameList),numel(NameList));
	IConfOrg=cell(numel(NameList),numel(NameList));
	
	IgainMean=cell(numel(NameList),numel(NameList));
	IgainSTD=cell(numel(NameList),numel(NameList));
	
	IConfMean=cell(numel(NameList),numel(NameList));
	IConfSTD=cell(numel(NameList),numel(NameList));
	
	
	IgainMedian=cell(numel(NameList),numel(NameList));
	Q16Igain=cell(numel(NameList),numel(NameList));
	Q84Igain=cell(numel(NameList),numel(NameList));
		
	IConfMedian=cell(numel(NameList),numel(NameList));
	Q16IConf=cell(numel(NameList),numel(NameList));
	Q84IConf=cell(numel(NameList),numel(NameList));
	
	PvalT=cell(numel(NameList),numel(NameList));
	PvalW=cell(numel(NameList),numel(NameList));
	
	
	%get data
	for i=1:numel(NameList)
		for j=1:numel(NameList)
			if i~=j
				Name=['Ttest_',NameList{i},'_',NameList{j}];
				NameW=['Wtest_',NameList{i},'_',NameList{j}];
				
				disp(Name)
				TRes=Results.(Name);
				
				
				IgainOrg{i,j}=num2str(TRes.InABMonte(1),3);
				IConfOrg{i,j}=num2str(TRes.IConfMonte(1),3);
				
				IgainMean{i,j}=num2str(TRes.InAB(1),3);
				IgainSTD{i,j}=num2str(TRes.InAB_Mostd(1),3);
				
				IConfMean{i,j}=num2str(TRes.IConf(1),3);
				IConfSTD{i,j}=num2str(TRes.IConf_std(1),3);
				
				
				if Medianer
					IgainMedian{i,j}=num2str(TRes.InABMedian(1),3);
					Q16Igain{i,j}=num2str(TRes.InAB68Per(1),3);
					Q84Igain{i,j}=num2str(TRes.InAB68Per(2),3);
					
					IConfMedian{i,j}=num2str(TRes.IConfMedian(1),3);
					Q16IConf{i,j}=num2str(TRes.IConf68Per(1),3);
					Q84IConf{i,j}=num2str(TRes.IConf68Per(2),3);
					
				end
				
				
				if PT_select
					PvalT{i,j}=num2str(TRes.p_org(1),3);
				end
				
				if PW_select
					WRes=Results.(NameW);
					PvalW{i,j}=num2str(WRes.p_org(1),3);
				end
				
				
			else
				IgainOrg{i,j}='n/a';
				IConfOrg{i,j}='n/a';
				
				IgainMean{i,j}='n/a';
				IgainSTD{i,j}='n/a';
	
				IConfMean{i,j}='n/a';
				IConfSTD{i,j}='n/a';
				
				IgainMedian{i,j}='n/a';
				Q16Igain{i,j}='n/a';
				Q84Igain{i,j}='n/a';
					
				IConfMedian{i,j}='n/a';
				Q16IConf{i,j}='n/a';
				Q84IConf{i,j}='n/a';
				
				PvalT{i,j}='n/a';
				PvalW{i,j}='n/a';
			end
			
		
		end
	
	end
	
	if ~LatexMode
		%Normal text file
		%write data to file
		fid = fopen(Filename,'w'); 
		TheLine='            ';
		for i=1:numel(NameList)
			TheLine=[TheLine,NameList{i},'     '];
		end
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
		
	
		
		
		for i=1:numel(NameList)
			OrgLine=[NameList{i},'     ']
			PTLine=['            '];
			PWLine=['            '];
			IgainLine=['            '];
			IConfLine=['            '];
			GainMedLine1=['            '];
			GainMedLine2=['            '];
			ConfMedLine1=['            '];
			ConfMedLine2=['            '];
			
			for j=1:numel(NameList)
				OrgLine=[OrgLine,IgainOrg{i,j},' +- ',IConfOrg{i,j},'     '];
				
				if Meaner
					IgainLine=[IgainLine,IgainMean{i,j},' +- ',IgainSTD{i,j},'     '];
					IConfLine=[IConfLine,IConfMean{i,j},' +- ',IConfSTD{i,j},'     '];
				end
				
				if Medianer
					GainMedLine1=[GainMedLine1,IgainMedian{i,j},'     '];
					GainMedLine2=[GainMedLine2,' - ',Q16Igain{i,j},' + ',Q84Igain{i,j},'     '];
					ConfMedLine1=[ConfMedLine1,IConfMedian{i,j},'     '];
					ConfMedLine2=[ConfMedLine2,' - ',Q16IConf{i,j},' + ',Q84IConf{i,j},'     '];
					
				end
				
				if PT_select
					PTLine=[PTLine,PvalT{i,j},'     '];
				end
				
				
				if PW_select
					PWLine=[PWLine,PvalW{i,j},'     '];
				end
				
			end
			
			fprintf(fid,'%s',OrgLine); 
			fprintf(fid,'\n');
			
			
			if PT_select
				fprintf(fid,'%s',PTLine); 
				fprintf(fid,'\n');
			end
			
			
			if PW_select
				fprintf(fid,'%s',PWLine); 
				fprintf(fid,'\n');
			end
			
			
			if Meaner
				fprintf(fid,'%s',IgainLine); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',IConfLine); 
				fprintf(fid,'\n');
			end
			
			
			if Medianer
				%Will probably look horrible with everthing turned on
				fprintf(fid,'%s',GainMedLine1); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',GainMedLine2); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',ConfMedLine1); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',ConfMedLine2); 
				fprintf(fid,'\n');
				
			end
			
			
			%update TabLine
			
			
		end
		
		fclose(fid); 
	
	else
		%Latex formated text file (uncomplete, needs some tuning afterwards)
		%write data to file
		
		
		%build TabLine
		TabLine=[repmat('|c',1,numel(NameList)+1),'|'];
		NumCol=num2str(numel(NameList)+1);
		
		
		fid = fopen(Filename,'w'); 
		
		fprintf(fid,'%s','\begin{table}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\begin{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s',['\begin{tabular}{',TabLine,'}']);
		fprintf(fid,'\n');
		fprintf(fid,'%s',['\multicolumn{',NumCol,'}{c}{T-test: TITLE MISSING} \\']);
		fprintf(fid,'\n');
		
		TheLine='\hline Model  ';
		for i=1:numel(NameList)
			TheLine=[TheLine,'& ',AltNameList{i},'     '];
		end
		
		TheLine=[TheLine,'\\ '];
		fprintf(fid,'%s',TheLine); 
		fprintf(fid,'\n');
		
		for i=1:numel(NameList)
			OrgLine=['\hline \multirow{',ShowPart,'}{*}{',AltNameList{i},'}'];
			PTLine=[' '];
			PWLine=[' '];
			IgainLine=[' '];
			IConfLine=[' '];
			GainMedLine1=[' '];
			GainMedLine2=[' '];
			GainMedLine3=[' '];
			ConfMedLine1=[' '];
			ConfMedLine2=[' '];
			ConfMedLine3=[' '];
			
			for j=1:numel(NameList)
				if ~strcmp(IgainOrg{i,j},'n/a')
					OrgLine=[OrgLine,' & $\textbf{',IgainOrg{i,j},'} \pm \textbf{',IConfOrg{i,j},'}$ '];
					
					if PT_select
						PTLine=[PTLine,' & $P_{T}= ',PvalT{i,j},'$ '];
					end
					
					
					if PW_select
						PWLine=[PWLine,' & $P_{W}= ',PvalW{i,j},'$ '];
					end
					
					if Meaner
						IgainLine=[IgainLine,' & $',IgainMean{i,j},' \pm ',IgainSTD{i,j},'$ '];
						IConfLine=[IConfLine,' & conf. Int.  $',IConfMean{i,j},' \pm ',IConfSTD{i,j},'$ '];
					end
					
					
					if Medianer
						if QuantTwoLine
							GainMedLine1=[GainMedLine1,' & Median: $',IgainMedian{i,j},'$ '];
							GainMedLine2=[GainMedLine2,' & $Q_{16}= ',Q16Igain{i,j},'$ '];
							GainMedLine3=[GainMedLine3,' & $Q_{84}= ',Q84Igain{i,j},'$ '];
							
							ConfMedLine1=[ConfMedLine1,' & Median: $',IConfMedian{i,j},'$ '];
							ConfMedLine2=[ConfMedLine2,' & $Q_{16}= ',Q16IConf{i,j},'$ '];
							ConfMedLine3=[ConfMedLine3,' & $Q_{84}= ',Q84IConf{i,j},'$ '];	
						else
							GainMedLine1=[GainMedLine1,' & Median: $',IgainMedian{i,j},'$ '];
							GainMedLine2=[GainMedLine2,' & $Q_{16}= ',Q16Igain{i,j},' ; Q_{84}= ',Q84Igain{i,j},'$ '];
							
							ConfMedLine1=[ConfMedLine1,' & Median: $',IConfMedian{i,j},'$ '];
							ConfMedLine2=[ConfMedLine2,' & $Q_{16}= ',Q16IConf{i,j},' ; Q_{84}= ',Q84IConf{i,j},'$ '];
													
						end
					
					end
						
					
				else
					OrgLine=[OrgLine,' & \multirow{',ShowPart,'}{*}{n/a} '];	
					PTLine=[PTLine, ' &    '];
					PWLine=[PWLine, ' &    '];
					IgainLine=[IgainLine, ' &    '];
					IConfLine=[IConfLine, ' &    '];
					GainMedLine1=[GainMedLine1, ' &    '];
					GainMedLine2=[GainMedLine2, ' &    '];
					GainMedLine3=[GainMedLine3, ' &    '];
					ConfMedLine1=[ConfMedLine1, ' &    '];
					ConfMedLine2=[ConfMedLine2, ' &    '];
					ConfMedLine3=[ConfMedLine3, ' &    '];
				end
			end
			
			%endLine
			OrgLine=[OrgLine, '  \\'];
			PTLine=[PTLine, '  \\'];
			PWLine=[PWLine, '  \\'];
			IgainLine=[IgainLine, '  \\'];
			IConfLine=[IConfLine, '  \\'];
			GainMedLine1=[GainMedLine1, '  \\'];
			GainMedLine2=[GainMedLine2, '  \\'];
			GainMedLine3=[GainMedLine3, '  \\'];
			ConfMedLine1=[ConfMedLine1, '  \\'];
			ConfMedLine2=[ConfMedLine2,'  \\'];
			ConfMedLine3=[ConfMedLine3, '  \\'];
			
			%disp(OrgLine)
			fprintf(fid,'%s',OrgLine); 
			fprintf(fid,'\n');
			
			if PT_select
				fprintf(fid,'%s',PTLine); 
				fprintf(fid,'\n');
			end
			
			
			if PW_select
				fprintf(fid,'%s',PWLine); 
				fprintf(fid,'\n');
			end
			
			
			if Meaner
				fprintf(fid,'%s',IgainLine); 
				fprintf(fid,'\n');
				fprintf(fid,'%s',IConfLine); 
				fprintf(fid,'\n');
			end
			
			
			if Medianer
				%Will probably look horrible with everthing turned on
				if QuantTwoLine
					fprintf(fid,'%s',GainMedLine1); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',GainMedLine2); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',GainMedLine3); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',ConfMedLine1); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',ConfMedLine2); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',ConfMedLine3); 
					fprintf(fid,'\n');
					
				else
					fprintf(fid,'%s',GainMedLine1); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',GainMedLine2); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',ConfMedLine1); 
					fprintf(fid,'\n');
					fprintf(fid,'%s',ConfMedLine2); 
					fprintf(fid,'\n');
				end
			end
			
		end
		fprintf(fid,'%s','\hline');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{tabular}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\caption{ THIS IS A TEMPLATE TEXT The table shows the average information gain determined with a T-test with a t-distribution based confidence interval between the three different models of MISSING PART. For every cell the first row defines the reference model and the first column defines the alternative model. In the cell itself the average information gain and the confidence intervall in respect to the ''original``catalog (first line), the mean average information gain with its standard deviation (second line) and the mean confidence interval with its standard deviation (third line) is written.}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\label{tab:ADD A LABEL}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{center}');
		fprintf(fid,'\n');
		fprintf(fid,'%s','\end{table}');
		fprintf(fid,'\n');
 
		
		fclose(fid); 
	
	
	end
	
	
end

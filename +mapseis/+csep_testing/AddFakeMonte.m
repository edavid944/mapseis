function Results=AddFakeMonte(orgResults)
	%Needed for plotting of csep testresults without monte carlo approach
	%only consistency for the moment
	
	Results=orgResults
	
	import mapseis.csep_testing.*;
	import mapseis.projector.*;
	
	ModelNames=orgResults.Names
	
	if orgResults.Nr_MonteCarlo>1
		return;
	end
	
	
	for i=1:numel(ModelNames)
		%NTest
		NTestRes=orgResults.(['Ntest_',ModelNames{i}]);
		
		QHighMonte(1)=NTestRes.QScoreHigh;
		QLowMonte(1)=NTestRes.QScoreLow;
		NumberMonte(1)=NTestRes.LogLikeCatalog;
		numCatalogMonte(1)=NTestRes.numCatalogs;
		
		NTestRes.QHighMonte=QHighMonte;
		NTestRes.QLowMonte=QLowMonte;
		
		NTestRes.QScoreHigh_std_dev=NaN;
		NTestRes.QScoreLow_std_dev=NaN;
		
		NTestRes.LikeCatMonte=NumberMonte;
		NTestRes.LogLikeCatalog_std_dev=NaN;
				
		NTestRes.QScoreHighMedian=NaN;
		NTestRes.QScoreHigh68Per=[NaN,NaN];
		NTestRes.QScoreLowMedian=NaN;
		NTestRes.QScoreLow68Per=[NaN,NaN];
		NTestRes.LogLikeCatalogMedian=NaN;
		NTestRes.LogLikeCatalog68Per=[NaN,NaN];
				
		NTestRes.numCatalogMonte=numCatalogMonte;
		
		Results.(['Ntest_',ModelNames{i}])=NTestRes;
		
		
		%Stest
		STestRes=orgResults.(['Stest_',ModelNames{i}]);
		
		QScoreMonte(1)=STestRes.QScore;
		numCatalogMonte(1)=STestRes.numCatalogs;
		LikeCatMonte(1)=STestRes.LogLikeCatalog;
		
		STestRes.QScoreMonte=QScoreMonte;
		
		STestRes.QScore_std_dev=NaN;
		STestRes.QScoreMedian=NaN;
		STestRes.QScore68Per=[NaN,NaN];
				
		STestRes.LikeCatMean=mean(LikeCatMonte);
		STestRes.LikeCat_std_dev=NaN;
		STestRes.LikeCatMedian=NaN;
		STestRes.LikeCat68Per=[NaN,NaN];
		STestRes.LogLikeCatMonte=LikeCatMonte;
		STestRes.numCatalogMonte=numCatalogMonte;
		
		Results.(['Stest_',ModelNames{i}])=STestRes;
		
		
		%Mtest
		MTestRes=orgResults.(['Mtest_',ModelNames{i}]);
		
		QScoreMonte(1)=MTestRes.QScore;
		numCatalogMonte(1)=MTestRes.numCatalogs;
		LikeCatMonte(1)=MTestRes.LogLikeCatalog;
		
		MTestRes.QScoreMonte=QScoreMonte;

		MTestRes.QScore_std_dev=NaN;
		MTestRes.QScoreMedian=NaN;
		MTestRes.QScore68Per=[NaN,NaN];
				
		MTestRes.LikeCatMean=mean(LikeCatMonte);
		MTestRes.LikeCat_std_dev=NaN;
		MTestRes.LikeCatMedian=NaN
		MTestRes.LikeCat68Per=[NaN,NaN];
		MTestRes.LogLikeCatMonte=LikeCatMonte;
		MTestRes.numCatalogMonte=numCatalogMonte;
		
		Results.(['Mtest_',ModelNames{i}])=MTestRes;
		
		
		%LTest
		LTestRes=orgResults.(['Ltest_',ModelNames{i}]);
		
		QScoreMonte(1)=LTestRes.QScore;
		numCatalogMonte(1)=LTestRes.numCatalogs;
		LikeCatMonte(1)=LTestRes.LogLikeCatalog;
				
		LTestRes.QScoreMonte=QScoreMonte;
		
		LTestRes.QScore_std_dev=NaN;
		LTestRes.QScoreMedian=NaN;
		LTestRes.QScore68Per=[NaN,NaN];
		
		LTestRes.LikeCatMean=mean(LikeCatMonte);
		LTestRes.LikeCat_std_dev=NaN;
		LTestRes.LikeCatMedian=NaN;
		LTestRes.LikeCat68Per=[NaN,NaN];
		LTestRes.LogLikeCatMonte=LikeCatMonte;
		LTestRes.numCatalogMonte=numCatalogMonte;
		
		Results.(['Ltest_',ModelNames{i}])=LTestRes;
		
	end
	
	
	
end

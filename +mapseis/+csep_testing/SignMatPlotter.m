function SignMatPlotter(Results,TheFold,FilePrefix,PlotConfig,PaperBoy)
	%This function plots the significance of the T-test and W-test into
	%a matrix together with TR plot
	
	import mapseis.plot.*;
	import mapseis.csep_testing.*;
	import mapseis.util.*;
	
	
	%Colors
	WsuccCol=[0.6 0.9 0.6];
	WfailCol=[0.9 0.6 0.6];
	TsuccCol='g';
	TfailCol='r';
	
	PlotTitle=' ';
	
	%Needed before in case of not PlotConfig is given
	ModelNames=Results.Names;
	
	
	if nargin<5
		PaperBoy=false;
		PlotConfig=struct( 'PlotNames',{ModelNames},...
				   'SignMat',true,...
				   'TR_Plot',true,...
				   'ShowUniform',true,...
				   'ShowPerfect',true,...
				   'Ranking',true,...
				   'Tsign',true,...
				   'Wsign',true,...
				   'ShowPNr',false,...
				   'KeepFig',false,...
				   'WriteToFile',true,...
				   'PlotTitle',[[]],...
				   'Quantil',0.05);
				   
	end	
	
	if nargin<4
		PaperBoy=false;
	end	
	
	
	
	oldFold=pwd;
	
	%first run as template for the rest (needed for TRtest)
	ResStruct=struct(	'Name',[],...
				'TestType','Ttest',...
				'PlotType',[],...
				'PrintText',true,...
				'ScoreSwitch','meanorg',...
				'DisplayName',[],...
				'Colors',[[0.5 0.5 0.7]],...
				'LineStyle','-',...
				'LineWidth',3,...
				'ScoreWidth',2,...
				'PaperBoy',PaperBoy,...
				'MiniHist',false,...
				'ModDispName',[]);
				
	if isfield(PlotConfig,'PlotTitle')
		ResStruct.DisplayName=PlotConfig.PlotTitle;
		RawTitle=PlotConfig.PlotTitle;
		PlotTitle=PlotConfig.PlotTitle;
	end
	
	%Add some default plotnames (Modelnames without '_')
	for i=1:numel(ModelNames)
		NewName=ModelNames{i};
		NewName(NewName=='_')=' ';
		PlotNames{i}=NewName;
	end
	
	ResStruct.ModDispName=PlotNames;
	RawNames=PlotNames;
	WriteToFile=PlotConfig.WriteToFile;
	KeepFig=PlotConfig.KeepFig;
	
	
	if isfield(PlotConfig,'PlotNames')
		if ~isempty(PlotConfig.PlotNames)
			ResStruct.ModDispName=PlotConfig.PlotNames;
			RawNames=PlotConfig.PlotNames;
			PlotNames=PlotConfig.PlotNames;
		end	
	end

	
	%Default value for Quantil
	Quant=0.05;
	
	if isfield(PlotConfig,'Quantil')
		if ~isempty(PlotConfig.Quantil)
			Quant=PlotConfig.Quantil;
		end
	end
				
	LineLength=2*numel(ModelNames)
	Spacer=2;			
	
	%Checki if p-values exists, if not -> add them
	TName=['Ttest_',Results.Names{1},'_',Results.Names{2}];
	TTemp=Results.(TName)
	if ~isfield(TTemp,'p_org')
		Results=AddPVal(Results,ModelNames);
	end
	
	
	if ~PlotConfig.SignMat
		%A ranking needs the pvalue matrix
		PlotConfig.Ranking=false;
	end
	
	
	if PlotConfig.SignMat
		%Matrixes for the P values
		T_pVal=NaN(numel(ModelNames),numel(ModelNames));
		W_pVal=NaN(numel(ModelNames),numel(ModelNames));
		T_pStr=cell(numel(ModelNames),numel(ModelNames));
		W_pStr=cell(numel(ModelNames),numel(ModelNames));
		
		%MiddlePoints
		Xpos=NaN(numel(ModelNames),numel(ModelNames));
		Ypos=NaN(numel(ModelNames),numel(ModelNames));
		
		%Rectangles
		XRec=cell(numel(ModelNames),numel(ModelNames));
		YRex=cell(numel(ModelNames),numel(ModelNames));
		
		%markers for W-fields and T-fields
		inT=false(numel(ModelNames),numel(ModelNames));
		inW=false(numel(ModelNames),numel(ModelNames));
		inDiag=false(numel(ModelNames),numel(ModelNames));
		
		%Get the Values
		for i=1:numel(ModelNames)
			for j=1:numel(ModelNames)
				if i~=j
					Name=['Ttest_',ModelNames{i},'_',ModelNames{j}];
					NameW=['Wtest_',ModelNames{i},'_',ModelNames{j}];
					
					disp(Name)
					TRes=Results.(Name);
					WRes=Results.(NameW);
					
					T_pVal(i,j)=TRes.p_org(1);
					W_pVal(i,j)=WRes.p_org(1);
					
					T_pStr{i,j}=num2str(TRes.p_org(1),2);
					W_pStr{i,j}=num2str(WRes.p_org(1),2);
					
										
					
				else
					T_pStr{i,j}='n/a';
					W_pStr{i,j}='n/a';
					T_pVal(i,j)=1;
					W_pVal(i,j)=1;
					inDiag(i,j)=true;
				end
				
				%Middle points are needed in any case
				Xpos(i,j)=Spacer/2+(i-1)*Spacer;
				Ypos(i,j)=Spacer/2+(j-1)*Spacer;
				
				%same for rectangles
				RectX=[(i-1)*Spacer,(i)*Spacer,(i)*Spacer,(i-1)*Spacer,(i-1)*Spacer];
				RectY=[(j-1)*Spacer,(j-1)*Spacer,(j)*Spacer,(j)*Spacer,(j-1)*Spacer];
				XRec{i,j}=RectX;
				YRec{i,j}=RectY;
				
				%mark LR=A in the matrix
				if i>j
					inT(i,j)=true;
				end
				
				
				if i<j
					inW(i,j)=true;
				end
				
				
			end
		
		end			
			
		
		%Check which one are significant 
		%This means H0: the means of the forecast are equal, can be rejected with 
		%a probability of 1-Quant;
		WeSignT=T_pVal<=Quant;
		WeSignW=W_pVal<=Quant;
		%disp(Xpos)
		%disp(Ypos)
		%now plot it
		fig=figure;
		plotAxis=gca;
		hold on;
		for i=1:numel(T_pVal)
			if inT(i)&PlotConfig.Tsign
				TheRect(i)=fill(XRec{i},YRec{i},'g','LineWidth',2);
				
				if PlotConfig.ShowPNr
					Texx(i)=text(Xpos(i),Ypos(i),T_pStr{i},'Units','data');
					set(Texx(i),'FontSize',18,'FontWeight','bold',...
					'HorizontalAlignment','center','VerticalAlignment','middle');
				end
				
				if WeSignT(i)
					set(TheRect(i),'FaceColor',TsuccCol);
				else
					set(TheRect(i),'FaceColor',TfailCol);
				end
				
			end
			
			if inW(i)&PlotConfig.Wsign
				TheRect(i)=fill(XRec{i},YRec{i},'g','LineWidth',2);
				
				if PlotConfig.ShowPNr
					Texx(i)=text(Xpos(i),Ypos(i),W_pStr{i},'Units','data');
					set(Texx(i),'FontSize',18,'FontWeight','bold',...
					'HorizontalAlignment','center','VerticalAlignment','middle');
				end
				
				if WeSignW(i)
					set(TheRect(i),'FaceColor',WsuccCol);
				else
					set(TheRect(i),'FaceColor',WfailCol);
				end
			end
		
			if inDiag(i)
				TheRect(i)=fill(XRec{i},YRec{i},[0.3 0.3 0.3],'LineWidth',2);
				%set(TheRect(i),'FaceColor','k');
			end
			
			
		end
		
		%Add some additional stuff
		set(plotAxis,'XTick',unique(Xpos),'XTickLabel',PlotNames,...
			'YTick',unique(Ypos),'YTickLabel',PlotNames,'FontSize',16,...
			'FontWeight','bold','LineWidth',3,'Box','on');
		
		xlab=xlabel('T-Test');
		ylab=ylabel('W-Test');
		set([xlab,ylab],'FontSize',16,'FontWeight','bold');	
		
		tit=title(['P-value matrix: ',PlotTitle]);
		set(tit,'FontSize',18,'FontWeight','bold');
		
		hold off
				
		%ylim([1 LineLength-1]);
		
		if WriteToFile	
		%create filename
			filename=[FilePrefix,'_Ttest_SignMat_',ModelNames{1},'.eps'];
			cd(TheFold)
			saveas(fig,filename,'psc');
			cd(oldFold);
		end
			
		if ~KeepFig	
			close(fig);
		end	
				
		
	
	end
	
	
	
	if PlotConfig.TR_Plot	
	
		%Now for the Special TRtest
		%TR-test 'Mean'
		ResStruct.ScoreSwitch='meanorg';
		ResStruct.TestType='TRtest';
		ResStruct.WideBar=true;
		ResStruct.BarTrail=true;
		NoLoopNeed={'HistINoConf','HistINoPerc'}
		TestModesT={'ScoreT','ScoreI'};
		
		if PlotConfig.Ranking
			%Add Rankings to the Labels
		end
		
		
		for j=1:numel(NoLoopNeed)
			ResStruct.PlotType=NoLoopNeed{j};		
				
				
			ResStruct.Name=ModelNames;
			
			ResStruct.ModDispName=RawNames;
			ResStruct.DisplayName=RawTitle;
			
			fig=figure;
			plotAxis=gca;
				
			
			%plot
			[handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
			%set(plotAxis,'XGrid','off','YGrid','on')
			%create filename
			
			%No Ranking at the moment to many things unclear
			if PlotConfig.Ranking
				%Add Rankings to the Labels
				get(plotAxis,'YTick');
				

				hold on;
				for i=1:numel(ModelNames)
				
				end
				hold off
			end
			
			
			set(plotAxis,'FontWeight','bold','FontSize',16);
			
			if WriteToFile
				filename=[FilePrefix,'_TRtest_Mean_',NoLoopNeed{j},'_',ModelNames{1},'.eps'];
				cd(TheFold)
				saveas(fig,filename,'psc');
				cd(oldFold);
			end
				
			if ~KeepFig	
				close(fig);
			end	
					
			
		end
		
		
	
	end
	
	
end	

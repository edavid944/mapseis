function TheRes=protoLTest(Mmodel,TheCatalog,numCatalogs,ParallelMode,FeedItBack)
	%A first prototype of a L-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<5
		FeedItBack=[];
	end	
	
	%added column 10 if not defined (it should be)
	if numel(Mmodel(1,:))<10
		Mmodel(:,10)=true(numel(Mmodel(:,1),1));
	end
	
	UsedBins=logical(Mmodel(:,10));
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;
	
	Mmodel(~UsedBins,9)=0;
	
		
	if isempty(FeedItBack)
	
		%assume the cells are in order
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			TheCat=poissrnd(Mmodel(:,9));	
			
			%set unused cell to 0
			TheCat(~UsedBins)=0;
			
			%calculate likelihood (assume poisson distribution)
			LikeIt=-Mmodel(:,9)+TheCat.*log(Mmodel(:,9))-log(factorial(TheCat));
			LogLiker(i)=nansum(LikeIt(~isinf(LikeIt)&UsedBins));
			
			
		end
	
	else	
		LogLiker=FeedItBack;
	
	end
	%calculate the log likelihood for the observation
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(Mmodel(:,1)),1);
	
	for i=1:numel(Mmodel(:,1))
		%no parfor steps are probably to small
		InLon=TheCatalog(:,1)>=Mmodel(i,1)&TheCatalog(:,1)<Mmodel(i,2);
		InLat=TheCatalog(:,2)>=Mmodel(i,3)&TheCatalog(:,2)<Mmodel(i,4);
		InDepth=TheCatalog(:,7)>=Mmodel(i,5)&TheCatalog(:,7)<Mmodel(i,6);
		InMag=TheCatalog(:,6)>=Mmodel(i,7)&TheCatalog(:,6)<Mmodel(i,8);
		
		
		TheObserver(i)=sum(InLon&InLat&InDepth&InMag&UsedBins(i));
		
		
	end
	
	%calc logLikelihood
	LikeIt=-Mmodel(:,9)+TheObserver.*log(Mmodel(:,9))-log(factorial(TheObserver));
	ObsLiker=nansum(LikeIt(~isinf(LikeIt)&UsedBins));
	
	
	%Calculate quantil
	quant=sum(LogLiker<=ObsLiker)/numCatalogs;
	
	%Pack result
	TheRes.TestType='LTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScore=quant;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

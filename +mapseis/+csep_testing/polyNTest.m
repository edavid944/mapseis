function TheRes=polyNTest(PrepStruct,TheCatalog,numCatalogs,minMag,ParallelMode,FeedItBack)
	%A first prototype of a N-Test rebuild, rought an no features at all
	
	%TheCatalog is a zmap formate catalog.
	
	if ParallelMode
		matlabpool open
	end
	
	if nargin<6
		FeedItBack=[];
	end	
	
	%FeedItBack=[];

	
	
	LogLiker=nan(numCatalogs,1);
	ObsLiker=nan;

	
	
	
	%calculate the log likelihood for the observation
	
	
	
	%calc the num of events for each cell
	TheObserver=zeros(numel(PrepStruct.PolyCoordX),1);
	RegioRate=zeros(numel(PrepStruct.PolyCoordX),1);
	

	
	for i=1:numel(PrepStruct.PolyCoordX)
		%could be added into the next loop, but it is more readable like
		%this
		aboveMinMag=(PrepStruct.MagVector{i}>=minMag);
		RegioRate(i)=sum(PrepStruct.CombFMDdiff{i}(aboveMinMag));
	
	end
	
	
	for i=1:numel(PrepStruct.PolyCoordX)
		%no parfor steps are probably to small
		InRegio = inpolygon(TheCatalog(:,1),TheCatalog(:,2),PrepStruct.PolyCoordX{i},PrepStruct.PolyCoordY{i});
		

		InMag=TheCatalog(:,6)>=minMag;
		
		
		TheObserver(i)=sum(InRegio&InMag);
		
		
	end
	
	%InMag=TheCatalog(:,6)>=minMag;
	%TheObserver=sum(InMag);
	
	
	if isempty(FeedItBack)
	
		%assume the cells are in order
		parfor i=1:numCatalogs
			
			%generate catalog with rate
			TheCat=poissrnd(RegioRate);	
			
			
			%calculate likelihood (assume poisson distribution)
			LogLiker(i)=nansum(TheCat);
			
			
		end
		
	else	
		LogLiker=FeedItBack;
	
	end
	
	
	%calc logLikelihood
	ObsLiker=sum(TheObserver);
	
	
	%Calculate quantil
	quant1=sum(LogLiker<=ObsLiker)/numCatalogs;
	quant2=sum(LogLiker>=ObsLiker)/numCatalogs;
	%changed quant2 to >= from > to be consistent with the "official" version
	
	%Pack result
	TheRes.TestType='NTest';
	TheRes.TestVersion=1;
	TheRes.LogLikeModel=LogLiker;
	TheRes.LogLikeCatalog=ObsLiker;
	TheRes.QScoreLow=quant1;
	TheRes.QScoreHigh=quant2;
	TheRes.numCatalogs=numCatalogs;
	
	if ParallelMode
		matlabpool close
	end

end

function Results=TestCombiner(TestResults,NameList,RawNames)
	%This function combines TestResult periods into a single result
	%TestResults needs to be a cell array with the Result structures of 
	%time periods in the cells. The structures inside have to have the same 
	%shape, means they need to have the same tests and the same order of the
	%models. NameList can be used to give new Names to single tests, has to be
	%a cell array with string, it can be left empty or missing, in this case
	%the fieldnames of the first Result structur is used.
	%Also equal between the different test have to be the number of synthetic
	%catalogs and the number of Catalog pertubation 
	
	if nargin<3
		NameList=[];
		RawNames=[];
	end
	
	MasterFields=fieldnames(TestResults{1});
	
	if isempty(NameList)
		NameList=MasterFields;
	end	
	
	if isempty(RawNames)
		RawNames=TestResults{1}.Names;
	end	

	
	if TestResults{1}.Nr_MonteCarlo>1
		Monte=true;
	else
		Monte=false;
	end
	
		
	for i=1:numel(MasterFields)
		
		TheTests={};
		try
			TestType=TestResults{1}.(MasterFields{i}).TestType;
		catch
			TestType='none';
		end
		
		for k=1:numel(TestResults)
			TempFields=fieldnames(TestResults{k});
			TheTests{k}=TestResults{k}.(TempFields{i});
			
		end
		
		try
			TestOut=SingleTestMerge(TheTests,TestType,Monte);
		catch
			TestOut='Combiner did not work';
		end
		
		Results.(MasterFields{i})=TestOut;
	end
	
	%Generate Name entries
	for k=1:numel(TestResults)
		NewNames{k}=TestResults{k}.Names;
	end
	
	Results.Nr_MonteCarlo=TestResults{1}.Nr_MonteCarlo;
	Results.Simulate_Catalog=TestResults{1}.Simulate_Catalog;
	Results.Names=RawNames;
	Results.CombinedNames=NewNames;


end %end function


function TestOut=SingleTestMerge(TheTests,TestType,Monte)
	%Subfunction which does the work
	
	switch TestType
	
		case {'LTest', 'STest', 'MTest'}
			if Monte 
				%RawData
				TestOut=TheTests{1};
				LogLikeModel=TheTests{1}.LogLikeModel;
				LogLikeCatMonte=TheTests{1}.LogLikeCatMonte;
				LogLikeCatalog=TheTests{1}.LogLikeCatalog;
				
				%This does not change for each perturbed catalog
				for j=2:numel(TheTests)
					LogLikeModel=LogLikeModel+TheTests{j}.LogLikeModel;
					LogLikeCatalog=LogLikeCatalog+TheTests{j}.LogLikeCatalog;
					LogLikeCatMonte=LogLikeCatMonte+TheTests{j}.LogLikeCatMonte;
				end
				
				%This one does
				
				for k=1:numel(TheTests{1}.LogLikeCatMonte)
					TestOut.QScoreMonte(k)=sum(LogLikeModel<=LogLikeCatMonte(k))/TestOut.numCatalogMonte(k);
					
				end
				
				%Re-calc statistic
				quant=quantile(TestOut.QScoreMonte,[0.16,0.5,0.84]);
				quantCat=quantile(LogLikeCatMonte,[0.16,0.5,0.84]);
				
				TestOut.QScore=mean(TestOut.QScoreMonte)
				TestOut.QScore_std_dev=std(TestOut.QScoreMonte);
				TestOut.QScoreMedian=quant(2);
				TestOut.QScore68Per=[quant(1),quant(3)];
				
				TestOut.LogLikeCatMonte=LogLikeCatMonte;
				TestOut.LikeCatMean=mean(LogLikeCatMonte);
				TestOut.LikeCat_std_dev=std(LogLikeCatMonte);
				TestOut.LikeCatMedian=quantCat(2);
				TestOut.LikeCat68Per=[quantCat(1),quantCat(3)];
				
				TestOut.LogLikeModel=LogLikeModel;
				TestOut.LogLikeCatalog=LogLikeCatalog;
				
				TestOut.Combined=true;
				
				
			else
				%RawData
				TestOut=TheTests{1};
				LogLikeModel=TheTests{1}.LogLikeModel;
				LogLikeCatalog=TheTests{1}.LogLikeCatalog;
				
				for j=2:numel(TheTests)
					LogLikeModel=LogLikeModel+TheTests{j}.LogLikeModel;	
					LogLikeCatalog=LogLikeCatalog+TheTests{j}.LogLikeCatalog;
				end
				
				TestOut.QScore=sum(LogLikerModel<=LogLikeCatalog)/TestOut.numCatalogs;
				TestOut.LogLikeModel=LogLikeModel;
				TestOut.LogLikeCatalog=LogLikeCatalog;
				TestOut.Combined=true;
				
			end
			
			
			
			
		case 'NTest'
			if Monte 
				%RawData
				TestOut=TheTests{1};
				LogLikeModel=TheTests{1}.LogLikeModel;
				LikeCatMonte=TheTests{1}.LikeCatMonte;
				LogLikeCatalog=TheTests{1}.LogLikeCatalog;
				
				%This does not change for each perturbed catalog
				for j=2:numel(TheTests)
					LogLikeModel=LogLikeModel+TheTests{j}.LogLikeModel;
					LogLikeCatalog=LogLikeCatalog+TheTests{j}.LogLikeCatalog;
					LikeCatMonte=LikeCatMonte+TheTests{j}.LikeCatMonte;
				end
				
				%This one does
				
				for k=1:numel(TheTests{1}.QHighMonte)
					TestOut.QHighMonte(k)=sum(LogLikeModel<=LikeCatMonte(k))/TestOut.numCatalogMonte(k);
					TestOut.QLowMonte(k)=sum(LogLikeModel>LikeCatMonte(k))/TestOut.numCatalogMonte(k);
					
				end
				
				%Re-calc statistic
				quantHigh=quantile(TestOut.QHighMonte,[0.16,0.5,0.84]);
				quantLow=quantile(TestOut.QLowMonte,[0.16,0.5,0.84]);
				quantCat=quantile(LikeCatMonte,[0.16,0.5,0.84]);
				
				TestOut.QScoreHigh=mean(TestOut.QHighMonte)
				TestOut.QScoreLow=mean(TestOut.QLowMonte)
				TestOut.QScoreHigh_std_dev=std(TestOut.QHighMonte);
				TestOut.QScoreLow_std_dev=std(TestOut.QLowMonte);
				TestOut.LogLikeCatalog=mean(LikeCatMonte);
				TestOut.LogLikeCatalog_std_dev=std(LikeCatMonte);
				TestOut.LikeCatMonte=LikeCatMonte;
				
				TestOut.QScoreHighMedian=quantHigh(2);
				TestOut.QScoreHigh68Per=[quantHigh(1),quantHigh(3)];
				TestOut.QScoreLowMedian=quantLow(2);
				TestOut.QScoreLow68Per=[quantLow(1),quantLow(3)];
				TestOut.LogLikeCatalogMedian=quantCat(2);
				TestOut.LogLikeCatalog68Per=[quantCat(1),quantCat(3)];
				
				TestOut.LogLikeModel=LogLikeModel;
				TestOut.Combined=true;
				
				
			else
				%RawData
				TestOut=TheTests{1};
				LogLikeModel=TheTests{1}.LogLikeModel;
				LogLikeCatalog=TheTests{1}.LogLikeCatalog;
				
				for j=2:numel(TheTests)
					LogLikeModel=LogLikeModel+TheTests{j}.LogLikeModel;	
					LogLikeCatalog=LogLikeCatalog+TheTests{j}.LogLikeCatalog;
				end
				
				TheRes.QScoreLow=sum(LogLikerModel<=LogLikeCatalog)/TestOut.numCatalogs;
				TheRes.QScoreHigh=sum(LogLikerModel>LogLikeCatalog)/TestOut.numCatalogs;
				TestOut.LogLikeModel=LogLikeModel;
				TestOut.LogLikeCatalog=LogLikeCatalog;
				TestOut.Combined=true;
				
			end
		
		
		case 'RTest'
			if Monte 
				%RawData
				TestOut=TheTests{1};
				LogLikeModelA=TheTests{1}.LogLikeModelA;
				LogLikeModelB=TheTests{1}.LogLikeModelB;
				LogLikeACatalog=TheTests{1}.LogLikeACatalog;
				LogLikeBCatalog=TheTests{1}.LogLikeBCatalog;
				LogLikeCatAMonte=TheTests{1}.LogLikeCatAMonte;
				LogLikeCatBMonte=TheTests{1}.LogLikeCatBMonte;
				Ratio_AB=TheTests{1}.Ratio_AB;
				ObsRatio_AB=TheTests{1}.ObsRatio_AB;
				Obs_RatioMonte=TheTests{1}.Obs_RatioMonte;
				
				%This does not change for each perturbed catalog
				for j=2:numel(TheTests)
					LogLikeModelA=LogLikeModelA+TheTests{j}.LogLikeModelA;
					LogLikeModelB=LogLikeModelB+TheTests{j}.LogLikeModelB;
					
					LogLikeACatalog=LogLikeACatalog+TheTests{j}.LogLikeACatalog;
					LogLikeBCatalog=LogLikeBCatalog+TheTests{j}.LogLikeBCatalog;
					
					LogLikeCatAMonte=LogLikeCatAMonte+TheTests{j}.LogLikeCatAMonte;
					LogLikeCatBMonte=LogLikeCatBMonte+TheTests{j}.LogLikeCatBMonte;
					
					Ratio_AB=Ratio_AB+TheTests{j}.Ratio_AB;
					
					ObsRatio_AB=ObsRatio_AB+TheTests{j}.ObsRatio_AB;
					Obs_RatioMonte=Obs_RatioMonte+TheTests{j}.Obs_RatioMonte;
					
				end
				
				%This one does
				
				for k=1:numel(TheTests{1}.QScoreMonte)
					TestOut.QScoreMonte(k)=sum(Ratio_AB<=Obs_RatioMonte(k))/TestOut.numCatalogMonte(k);
				end
				
				%Re-calc statistic
				quant=quantile(TestOut.QScoreMonte,[0.16,0.5,0.84]);
				quantCatA=quantile(LogLikeCatAMonte,[0.16,0.5,0.84]);
				quantCatB=quantile(LogLikeCatBMonte,[0.16,0.5,0.84]);				
				quantRatioObs=quantile(Obs_RatioMonte,[0.16,0.5,0.84]);
				
				TestOut.QScoreRatioAB=mean(TestOut.QScoreMonte)
				TestOut.QScoreRatioAB_std_dev=std(TestOut.QScoreMonte);
				TestOut.QScoreRatioABMedian=quant(2);
				TestOut.QScoreRatioAB68Per=[quant(1),quant(3)];
				
				TestOut.LikeCatAMean=mean(LogLikeCatAMonte);
				TestOut.LikeCatA_std_dev=std(LogLikeCatAMonte);
				TestOut.LikeCatAMedian=quantCatA(2);
				TestOut.LikeCatA68Per=[quantCatA(1),quantCatA(3)];
				TestOut.LogLikeCatAMonte=LogLikeCatAMonte;
					
				TestOut.LikeCatBMean=mean(LogLikeCatBMonte);
				TestOut.LikeCatB_std_dev=std(LogLikeCatBMonte);
				TestOut.LikeCatBMedian=quantCatB(2);
				TestOut.LikeCatB68Per=[quantCatB(1),quantCatB(3)];
				TestOut.LogLikeCatBMonte=LogLikeCatBMonte;
				
				TestOut.Obs_RatioMonteMean=mean(Obs_RatioMonte);
				TestOut.Obs_RatioMonte_std_dev=std(Obs_RatioMonte);
				TestOut.Obs_RatioMonteMedian=quantRatioObs(2);
				TestOut.Obs_RatioMonte68Per=[quantRatioObs(1),quantRatioObs(3)];
				TestOut.Obs_RatioMonte=Obs_RatioMonte;
				
				TestOut.LogLikeModelA=LogLikeModelA;
				TestOut.LogLikeModelB=LogLikeModelB;
				TestOut.Ratio_AB=Ratio_AB;
				
								
				TestOut.Combined=true;
				
				
			else
				%RawData
				TestOut=TheTests{1};
				LogLikeModelA=TheTests{1}.LogLikeModelA;
				LogLikeModelB=TheTests{1}.LogLikeModelB;
				LogLikeACatalog=TheTests{1}.LogLikeACatalog;
				LogLikeBCatalog=TheTests{1}.LogLikeBCatalog;
				Ratio_AB=TheTests{1}.Ratio_AB;
				ObsRatio_AB=TheTests{1}.ObsRatio_AB;
				
				
				for j=2:numel(TheTests)
					LogLikeModelA=LogLikeModelA+TheTests{j}.LogLikeModelA;
					LogLikeModelB=LogLikeModelB+TheTests{j}.LogLikeModelB;
					LogLikeACatalog=LogLikeACatalog+TheTests{j}.LogLikeACatalog;
					LogLikeBCatalog=LogLikeBCatalog+TheTests{j}.LogLikeBCatalog;
					Ratio_AB=Ratio_AB+TheTests{j}.Ratio_AB;
					ObsRatio_AB=ObsRatio_AB+TheTests{j}.ObsRatio_AB;
				end
				
				TestOut.QScoreRatioAB=sum(Ratio_AB<=Ratio_Obs_AB)/TestOut.numCatalogs;
				TestOut.LogLikeModelA=LogLikeModelA;
				TestOut.LogLikeModelB=LogLikeModelB;
				TestOut.LogLikeACatalog=LogLikeACatalog;
				TestOut.LogLikeBCatalog=LogLikeBCatalog;
				TestOut.Ratio_AB=Ratio_AB;
				TestOut.ObsRatio_AB=ObsRatio_AB;
				TestOut.Combined=true;
				
			end
			
			
			
		case 'TTest'
			if Monte 
				TestOut=TheTests{1};
				Alibaba=TestOut.Alibaba;
				AliMonte=TestOut.AliMonte;
				Bins_w_Eq=TestOut.Bins_w_Eq;
				TotalNum=TestOut.TotalNum;
				ExpModA=TestOut.ExpModA;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					Bins_w_Eq=Bins_w_Eq+TheTests{j}.Bins_w_Eq;
					Alibaba=[Alibaba,TheTests{j}.Alibaba];
					ExpModA=ExpModA+TheTests{j}.ExpModA;
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
					
					%Sum over all the pertubations
					for k=1:numel(AliMonte)
						AliMonte{k}=[AliMonte{k},TheTests{j}.AliMonte{k}];
					end
				end
				
				%Recalc everything needed
				for k=1:numel(AliMonte)
					InABMonte(k)=1/TotalNum(k)*sum(AliMonte{k})-(ExpModA-ExpModB)/TotalNum(k);
					IVarMonte(k)=1/(TotalNum(k)-1)*sum(AliMonte{k}.^2)-1/(TotalNum(k)^2-TotalNum(k))*sum(AliMonte{k})^2;
					TValMonte(k)=InABMonte(k)/(sqrt(IVarMonte(k))/sqrt(TotalNum(k)));
					TtableMonte(k)=tinv(1-TestOut.SignLevel,TotalNum(k)-1);
					IConfMonte(k)= TtableMonte(k)* IVarMonte(k).^(1/2) / sqrt(TotalNum(k));
				end
				
				%Pack it back into the structure
				
				%median 68% interval
				quantInAB=quantile(InABMonte,[0.16,0.5,0.84]);
				quantIVar=quantile(IVarMonte,[0.16,0.5,0.84]);
				quantT=quantile(TValMonte,[0.16,0.5,0.84]);
				quantIConf=quantile(IConfMonte,[0.16,0.5,0.84]);
				
				TestOut.Bins_w_Eq=Bins_w_Eq;
				TestOut.TValMonte=TValMonte;
				TestOut.InABMonte=InABMonte;
				TestOut.VarI=IVarMonte;
				TestOut.IConfMonte=IConfMonte;
				TestOut.InAB=mean(InABMonte);
				TestOut.VarianceI=mean(IVarMonte);
				TestOut.InAB_Mostd=std(InABMonte);
				TestOut.VarI_Mostd=std(IVarMonte);
				TestOut.T=mean(TValMonte);
				TestOut.T_std_dev=std(TValMonte);
				TestOut.IConf=mean(IConfMonte);
				TestOut.IConf_std=std(IConfMonte);
					
				TestOut.InABMedian=quantInAB(2);
				TestOut.InAB68Per=[quantInAB(1),quantInAB(3)];
				TestOut.VarianceIMedian=quantIVar(2);
				TestOut.VarianceI68Per=[quantIVar(1),quantIVar(3)];
				TestOut.TMedian=quantT(2);
				TestOut.T68Per=[quantT(1),quantT(3)];
				TestOut.IConfMedian=quantIConf(2);
				TestOut.IConf68Per=[quantIConf(1),quantIConf(3)];
				
				TestOut.AliMonte=AliMonte;
				TestOut.Alibaba=Alibaba;
				TestOut.TotalNum=TotalNum;
				TestOut.ExpModA=ExpModA;
				TestOut.ExpModB=ExpModB;
				TestOut.Combined=true;
				
			else
				TestOut=TheTests{1};
				Alibaba=TestOut.Alibaba;
				Bins_w_Eq=TestOut.Bins_w_Eq;
				TotalNum=TestOut.TotalNum;
				ExpModA=TestOut.ExpModA;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					Bins_w_Eq=Bins_w_Eq+TheTests{j}.Bins_w_Eq;
					Alibaba=[Alibaba,TheTests{j}.Alibaba];
					ExpModA=ExpModA+TheTests{j}.ExpModA;
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
				end
				
				%Recalc everything needed
				InAB=1/TotalNum*sum(Alibaba)-(ExpModA-ExpModB)/TotalNum;
				SVar=1/(TotalNum-1)*sum(Alibaba.^2)-1/(TotalNum^2-TotalNum)*sum(Alibaba)^2;
				T=InAB/(sqrt(SVar)/sqrt(TotalNum));
				Ttable=tinv(1-TestOut.SignLevel,TotalNum-1);
				IConf= Ttable* SVar.^(1/2) / sqrt(TotalNum);
				
				%Pack it back into the structure
				TestOut.Bins_w_Eq=Bins_w_Eq;
				TestOut.InAB=InAB;
				TestOut.Alibaba=Alibaba;
				TestOut.VarianceI=SVar;
				TestOut.T=T;
				TestOut.ttable=Ttable;
				TestOut.IConf=IConf;
				TestOut.TotalNum=TotalNum;
				TestOut.ExpModA=ExpModA;
				TestOut.ExpModB=ExpModB;
				TestOut.Combined=true;
			end
		
			
		case 'Wtest'
			if Monte 
				TestOut=TheTests{1};
				
				if ~isfield('MatMethod',TestOut);
					TestOut.MatMethod=false;
				end
				
				RawDiffs=TestOut.RawDiffs;
				AliMonte=TestOut.AliMonte;
				TotalNum=TestOut.TotalNum;
				ExpModA=TestOut.ExpModA;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					RawDiffs=[RawDiffs,TheTests{j}.RawDiffs];
					ExpModA=ExpModA+TheTests{j}.ExpModA;
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
					
					%Sum over all the pertubations
					for k=1:numel(AliMonte)
						AliMonte{k}=[AliMonte{k},TheTests{j}.AliMonte{k}];
					end
				end
				
				
				if ~TestOut.MatMethod;
					%Recalc everything needed
					for k=1:numel(AliMonte)
						diffMed=AliMonte{k}-(ExpModA-ExpModB)/TotalNum(k);
						MedNoZero=diffMed(diffMed~=0);
						NegVal=MedNoZero<0;
						TheRanks=tiedrank(abs(MedNoZero));
					
						W_plus_Monte(k)=sum(TheRanks(~NegVal));
						W_minus_Monte(k)=sum(TheRanks(NegVal));
						W_overall_Monte(k)=W_plus_Monte(k)+W_minus_Monte(k);
					end
					
					
					%median 68% interval
					quantWP=quantile(W_plus_Monte,[0.16,0.5,0.84]);
					quantWM=quantile(W_minus_Monte,[0.16,0.5,0.84]);
					quantWO=quantile(W_overall_Monte,[0.16,0.5,0.84]);
					
					%Pack it back into the structure
					TestOut.W_plus_Monte=W_plus_Monte;
					TestOut.W_minus_Monte=W_minus_Monte;
					TestOut.W_overall_Monte=W_overall_Monte;
					
					TestOut.W_plus=mean(W_plus_Monte);
					TestOut.W_plus_std_dev=std(W_plus_Monte);
					TestOut.W_minus=mean(W_minus_Monte);
					TestOut.W_minus_std_dev=std(W_minus_Monte);
					TestOut.W_overall=mean(W_overall_Monte);
					TestOut.W_overall_std_dev=std(W_overall_Monte);
					
					TestOut.W_PlusMedian=quantWP(2);
					TestOut.W_Plus68Per=[quantWP(1),quantWP(3)];
					TestOut.W_MinusMedian=quantWM(2);
					TestOut.W_Minus68Per=[quantWM(1),quantWM(3)];
					TestOut.W_OverallMedian=quantWO(2);
					TestOut.W_Overall68Per=[quantWO(1),quantWO(3)];
					
					
					TestOut.RawDiffs=RawDiffs;
					
					TestOut.TotalNum=TotalNum;
					TestOut.ExpModA=ExpModA;
					TestOut.ExpModB=ExpModB;
					
				
				else
					%THIS IS NOT YET IMPLEMENTED
					%diffMed=RawDiffs-(ExpModA-ExpModB)/TotalNum;
					%[p,h,stats] = signrank(diffMed,0,'alpha',TheRes.SignLevel);
					
					%Pack it back into the structure
					%TestOut.TestType='Wtest';
					%TestOut.RawDiffs=RawDiffs;
					%TestOut.p_medZero=p;
					%TestOut.NullHypoReject=h;
					%TestOut.RawStat=stats;
					%TestOut.TotalNum=TotalNum;
					%TestOut.ExpModA=ExpModA:
					%TestOut.ExpModB=ExpModB;
				end
				
		
				TestOut.Combined=true;
				
			else
				TestOut=TheTests{1};
				
				if ~isfield('MatMethod',TestOut);
					TestOut.MatMethod=false;
				end
				
				RawDiffs=TestOut.RawDiffs;
				TotalNum=TestOut.TotalNum;
				ExpModA=TestOut.ExpModA;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					RawDiffs=[RawDiffs,TheTests{j}.RawDiffs];
					ExpModA=ExpModA+TheTests{j}.ExpModA;
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
				end
				
				
				if ~TestOut.MatMethod;
					%Recalc everything needed
					diffMed=RawDiffs-(ExpModA-ExpModB)/TotalNum;
					MedNoZero=diffMed(diffMed~=0);
					NegVal=MedNoZero<0;
					TheRanks=tiedrank(abs(MedNoZero));
					
					W_plus=sum(TheRanks(~NegVal));
					W_minus=sum(TheRanks(NegVal));
					W_overall=W_plus+W_minus;
					
					%Pack it back into the structure
					TestOut.RawDiffs=RawDiffs;
					TestOut.W_plus=W_plus;
					TestOut.W_minus=W_minus;
					TestOut.W_overall=W_overall;
					TestOut.TotalNum=TotalNum;
					TestOut.ExpModA=ExpModA;
					TestOut.ExpModB=ExpModB;
					
				
				else
					diffMed=RawDiffs-(ExpModA-ExpModB)/TotalNum;
					[p,h,stats] = signrank(diffMed,0,'alpha',TheRes.SignLevel);
					
					%Pack it back into the structure
					TestOut.TestType='Wtest';
					TestOut.RawDiffs=RawDiffs;
					TestOut.p_medZero=p;
					TestOut.NullHypoReject=h;
					TestOut.RawStat=stats;
					TestOut.TotalNum=TotalNum;
					TestOut.ExpModA=ExpModA;
					TestOut.ExpModB=ExpModB;
				end
				
		
				TestOut.Combined=true;
		
			end
		
			
		case 'TRTest'
			%At moment one iteration
			%It is the only case used so far anyway
			
			%This one has to be tested, that nothing is missing
			
			if Monte 
				TestOut=TheTests{1};
				Alibaba=TestOut.Alibaba;
				AliPerfect=TestOut.AliPerfect;
				AliMonte=TestOut.AliMonte;
				MontePer=TestOut.MontePer;
				Bins_w_Eq=TestOut.Bins_w_Eq;
				TotalNum=TestOut.TotalNum;
				ExpPer=TestOut.ExpPer;
				ExpPerMonte=TestOut.ExpPerMonte;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					Bins_w_Eq=Bins_w_Eq+TheTests{j}.Bins_w_Eq;
					Alibaba=[Alibaba;TheTests{j}.Alibaba];
					AliPerfect=[AliPerfect;TheTests{j}.AliPerfect];
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					ExpPer=ExpPer+TheTests{j}.ExpPer;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
					
					%Sum over all the pertubations
					for k=1:numel(AliMonte)
						AliMonte{k}=[AliMonte{k};TheTests{j}.AliMonte{k}];
						MontePer{k}=[MontePer{k};TheTests{j}.MontePer{k}];
						ExpPerMonte(k)=ExpPerMonte(k)+TheTests{j}.ExpPerMonte(k);
					end
					
				end
				
				%Recalc everything needed
				for k=1:numel(AliMonte)
					TtableMonte(k)=tinv(1-TestOut.SignLevel,TotalNum(k)-1);
				
					IGainMonte(k)=1/TotalNum(k)*sum(AliMonte{k});
					IVarMonte(k)=1/(TotalNum(k)-1)*sum(AliMonte{k}.^2)-1/(TotalNum(k)^2-TotalNum(k))*sum(AliMonte{k})^2;
					TValMonte(k)=IGainMonte(k)/(sqrt(IVarMonte(k))/sqrt(TotalNum(k)));
					IConfMonte(k)= TtableMonte(k)* IVarMonte(k).^(1/2) / sqrt(TotalNum(k));
					MaxGain(k)=1/TotalNum(k)*sum(MontePer{k})-(ExpModB-ExpPerMonte(k))/TotalNum(k);
					IVarPerMonte(k)=1/(TotalNum(k)-1)*sum(MontePer{k}.^2)-1/(TotalNum(k)^2-TotalNum(k))*sum(MontePer{k})^2;
					TValPerMonte(k)=MaxGain(k)/(sqrt(IVarPerMonte(k))/sqrt(TotalNum(k)));
					IConfPerMonte(k)= TValPerMonte(k)* IVarPerMonte(k).^(1/2) / sqrt(TotalNum(k));
					
					%Addition Parameters with Perfect Gain
					PercentGain(k)=round(IGainMonte(k)./MaxGain(k)*1000)./10;
					PercentConf(k)=round(IConfMonte(k)./MaxGain(k)*10000)./100;
				end
				
				%Pack it back into the structure
				
				%Three parameters are not stored here, because there are
				%not stored in the original script but it may change.
				
				%median 68% interval
				quantInAB=quantile(IGainMonte,[0.16,0.5,0.84]);
				quantIVar=quantile(IVarMonte,[0.16,0.5,0.84]);
				quantT=quantile(TValMonte,[0.16,0.5,0.84]);
				quantIConf=quantile(IConfMonte,[0.16,0.5,0.84]);
				quantMaxGain=quantile(MaxGain,[0.16,0.5,0.84]);
				quantIGainPer=quantile(PercentGain,[0.16,0.5,0.84]);
				quantIConfPer=quantile(PercentConf,[0.16,0.5,0.84]);
				
				
				TestOut.Bins_w_Eq=Bins_w_Eq;
				TestOut.TValMonte=TValMonte;
				TestOut.InABMonte=IGainMonte;
				TestOut.VarI=IVarMonte;
				TestOut.IConfMonte=IConfMonte;
				TestOut.MaxGainMonte=MaxGain;
				TestOut.IGainPercMonte=PercentGain;
				TestOut.IConfPercMonte=PercentConf;
				
				TestOut.InAB=mean(IGainMonte);
				TestOut.VarianceI=mean(IVarMonte);
				TestOut.InAB_Mostd=std(IGainMonte);
				TestOut.VarI_Mostd=std(IVarMonte);
				TestOut.T=mean(TValMonte);
				TestOut.T_std_dev=std(TValMonte);
				TestOut.IConf=mean(IConfMonte);
				TestOut.IConf_std=std(IConfMonte);
				TestOut.MaxGain=mean(MaxGain);
				TestOut.MaxGain_Mostd=std(MaxGain);
				TestOut.IGainPerc=mean(PercentGain);
				TestOut.IGainPerc_Mostd=std(PercentGain);
				TestOut.IConfPerc=mean(PercentConf);
				TestOut.IConfPerc_Mostd=std(PercentConf);
				
				TestOut.InABMedian=quantInAB(2);
				TestOut.InAB68Per=[quantInAB(1),quantInAB(3)];
				TestOut.VarianceIMedian=quantIVar(2);
				TestOut.VarianceI68Per=[quantIVar(1),quantIVar(3)];
				TestOut.TMedian=quantT(2);
				TestOut.T68Per=[quantT(1),quantT(3)];
				TestOut.IConfMedian=quantIConf(2);
				TestOut.IConf68Per=[quantIConf(1),quantIConf(3)];
				TestOut.MaxGainMedian=quantMaxGain(2);
				TestOut.MaxGain68Per=[quantMaxGain(1),quantMaxGain(3)];
				TestOut.IGainPercMedian=quantIGainPer(2);
				TestOut.IGainPerc68Per=[quantIGainPer(1),quantIGainPer(3)];
				TestOut.IConfPercMedian=quantIConfPer(2);
				TestOut.ICOnfPerc68Per=[quantIConfPer(1),quantIConfPer(3)];
				
				TestOut.Alibaba=Alibaba;
				TestOut.AliPerfect=AliPerfect;
				TestOut.AliMonte=AliMonte;
				TestOut.MontePer=MontePer;
				TestOut.ExpPerMonte=ExpPerMonte;
				TestOut.ExpPer=ExpPer;
				TestOut.ExpModB=ExpModB;
				TestOut.TotalNum=TotalNum;
				
				
			else
				TestOut=TheTests{1};
				Alibaba=TestOut.Alibaba;
				AliPerfect=TestOut.AliPerfect;
				Bins_w_Eq=TestOut.Bins_w_Eq;
				TotalNum=TestOut.TotalNum;
				ExpModB=TestOut.ExpModB;
				ExpPer=TestOut.ExpPer;
				
				for j=2:numel(TheTests)
					Bins_w_Eq=Bins_w_Eq+TheTests{j}.Bins_w_Eq;
					Alibaba=[Alibaba;TheTests{j}.Alibaba];
					AliPerfect=[AliPerfect;TheTests{j}.AliPerfect];
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					ExpPer=ExpPer+TheTests{j}.ExpPer;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
				end
				
				%Recalc everything needed
				Ttable=tinv(1-TestOut.SignLevel,TotalNum-1);
				
				InAB=1/TotalNum*sum(Alibaba);
				SVar=1/(TotalNum-1)*sum(Alibaba.^2)-1/(TotalNum^2-TotalNum)*sum(Alibaba)^2;
				T=InAB/(sqrt(SVar)/sqrt(TotalNum));
				IConf= Ttable* SVar.^(1/2) / sqrt(TotalNum);
				InAPer=1/TotalNum*sum(AliPerfect)-(ExpModB-ExpPer)/TotalNum;
				SVarPer=1/(TotalNum-1)*sum(AliPerfect.^2)-1/(TotalNum^2-TotalNum)*sum(AliPerfect)^2;
				TPer=InAPer/(sqrt(SVarPer)/sqrt(TotalNum));
				IConfPer= Ttable* SVarPer.^(1/2) / sqrt(TotalNum);
				
				%Addition Parameters with Perfect Gain
				PercentGain=round(InAB./InAPer*1000)./10;
				PercentConf=round(IConf./InAPer*10000)./100;
	
				%Pack it back into the structure
				TestOut.Bins_w_Eq=Bins_w_Eq;
				TestOut.InAB=InAB;
				TestOut.Alibaba=Alibaba;
				TestOut.AliPerfect=AliPerfect;
				TestOut.VarianceI=SVar;
				TestOut.T=T;
				TestOut.ttable=Ttable;
				TestOut.IConf=IConf;
				TestOut.MaxGain=InAPer;
				TestOut.IGainPerc=PercentGain;
				TestOut.IConfPerc=PercentConf;
				
				TestOut.TotalNum=TotalNum;
				TestOut.ExpPer=ExpPer;
				TestOut.ExpModB=ExpModB;
				TestOut.Combined=true;
			end
		
			
		case 'TRInvTest'
			%At moment indentical to TTest because the only changes is
			%the selection of the events, which is already done by the 
			%original code.
			
			if Monte 
				TestOut=TheTests{1};
				Alibaba=TestOut.Alibaba;
				AliMonte=TestOut.AliMonte;
				Bins_w_Eq=TestOut.Bins_w_Eq;
				TotalNum=TestOut.TotalNum;
				ExpModA=TestOut.ExpModA;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					Bins_w_Eq=Bins_w_Eq+TheTests{j}.Bins_w_Eq;
					Alibaba=[Alibaba,TheTests{j}.Alibaba];
					ExpModA=ExpModA+TheTests{j}.ExpModA;
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
					
					%Sum over all the pertubations
					for k=1:numel(AliMonte)
						AliMonte{k}=[AliMonte{k},TheTests{j}.AliMonte{k}];
					end
				end
				
				%Recalc everything needed
				for k=1:numel(AliMonte)
					InABMonte(k)=1/TotalNum(k)*sum(AliMonte{k})-(ExpModA-ExpModB)/TotalNum(k);
					IVarMonte(k)=1/(TotalNum(k)-1)*sum(AliMonte{k}.^2)-1/(TotalNum(k)^2-TotalNum(k))*sum(AliMonte{k})^2;
					TValMonte(k)=InABMonte(k)/(sqrt(IVarMonte(k))/sqrt(TotalNum(k)));
					TtableMonte(k)=tinv(1-TestOut.SignLevel,TotalNum(k)-1);
					IConfMonte(k)= TtableMonte(k)* IVarMonte(k).^(1/2) / sqrt(TotalNum(k));
				end
				
				%Pack it back into the structure
				
				%median 68% interval
				quantInAB=quantile(InABMonte,[0.16,0.5,0.84]);
				quantIVar=quantile(IVarMonte,[0.16,0.5,0.84]);
				quantT=quantile(TValMonte,[0.16,0.5,0.84]);
				quantIConf=quantile(IConfMonte,[0.16,0.5,0.84]);
				
				TestOut.Bins_w_Eq=Bins_w_Eq;
				TestOut.TValMonte=TValMonte;
				TestOut.InABMonte=InABMonte;
				TestOut.VarI=IVarMonte;
				TestOut.IConfMonte=IConfMonte;
				TestOut.InAB=mean(InABMonte);
				TestOut.VarianceI=mean(IVarMonte);
				TestOut.InAB_Mostd=std(InABMonte);
				TestOut.VarI_Mostd=std(IVarMonte);
				TestOut.T=mean(TValMonte);
				TestOut.T_std_dev=std(TValMonte);
				TestOut.IConf=mean(IConfMonte);
				TestOut.IConf_std=std(IConfMonte);
					
				TestOut.InABMedian=quantInAB(2);
				TestOut.InAB68Per=[quantInAB(1),quantInAB(3)];
				TestOut.VarianceIMedian=quantIVar(2);
				TestOut.VarianceI68Per=[quantIVar(1),quantIVar(3)];
				TestOut.TMedian=quantT(2);
				TestOut.T68Per=[quantT(1),quantT(3)];
				TestOut.IConfMedian=quantIConf(2);
				TestOut.IConf68Per=[quantIConf(1),quantIConf(3)];
				
				TestOut.AliMonte=AliMonte;
				TestOut.Alibaba=Alibaba;
				TestOut.TotalNum=TotalNum;
				TestOut.ExpModA=ExpModA;
				TestOut.ExpModB=ExpModB;
				TestOut.Combined=true;
				
			else
				TestOut=TheTests{1};
				Alibaba=TestOut.Alibaba;
				Bins_w_Eq=TestOut.Bins_w_Eq;
				TotalNum=TestOut.TotalNum;
				ExpModA=TestOut.ExpModA;
				ExpModB=TestOut.ExpModB;
				
				for j=2:numel(TheTests)
					Bins_w_Eq=Bins_w_Eq+TheTests{j}.Bins_w_Eq;
					Alibaba=[Alibaba,TheTests{j}.Alibaba];
					ExpModA=ExpModA+TheTests{j}.ExpModA;
					ExpModB=ExpModB+TheTests{j}.ExpModB;
					TotalNum=TotalNum+TheTests{j}.TotalNum;
				end
				
				%Recalc everything needed
				InAB=1/TotalNum*sum(Alibaba)-(ExpModA-ExpModB)/TotalNum;
				SVar=1/(TotalNum-1)*sum(Alibaba.^2)-1/(TotalNum^2-TotalNum)*sum(Alibaba)^2;
				T=InAB/(sqrt(SVar)/sqrt(TotalNum));
				Ttable=tinv(1-TestOut.SignLevel,TotalNum-1);
				IConf= Ttable* SVar.^(1/2) / sqrt(TotalNum);
				
				%Pack it back into the structure
				TestOut.Bins_w_Eq=Bins_w_Eq;
				TestOut.InAB=InAB;
				TestOut.Alibaba=Alibaba;
				TestOut.VarianceI=SVar;
				TestOut.T=T;
				TestOut.ttable=Ttable;
				TestOut.IConf=IConf;
				TestOut.TotalNum=TotalNum;
				TestOut.ExpModA=ExpModA;
				TestOut.ExpModB=ExpModB;
				TestOut.Combined=true;
			end
		
	end %end switch
	
end %end subfunction

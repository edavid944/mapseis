% Test ZMAP export and import

import mapseis.datastore.*;
import mapseis.importfilter.*;
import mapseis.projector.*;

% Read in data from SCEC file
fname = 'scec_SearchResults.txt';

% Create a datastore from this file
dataStore = DataStore(importSCECCatRead(fname,'-file'));

% Extract the events as a Nx9 ZMAP array
a = getZMAPFormat(dataStore);

% Create a new datastore using this matrix
dataStoreNew = DataStore(importZMAPCatRead(a));

% Extract the data from this matrix into ZMAP format
b = getZMAPFormat(dataStoreNew);

% Check that all elments of a are present in b
testPassed = all(ismember(a,b,'rows'));

assert(testPassed,'mapseis:import_export_ZMAP','Failed to import and export ZMAP format')

disp('Import/Export ZMAP format passed test')
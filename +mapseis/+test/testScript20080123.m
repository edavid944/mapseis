% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 61 $    $Date: 2008-10-13 11:39:22 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell
%% Import the data
%fname = '../Import/SearchResults_1983_to_Present_local.txt';
fname='ms_Test/testData20080123_01.mat'; % 410kB file to test

% Start with previously saved session
[evStruct,guiWindowStruct] = ms_StartApp(fname);

%% Plot the data and selected region
% Occurs automatically as GUI is linked to underlying data

%% Filter based on distance to grid points
sep =0.2;
% Grid points - construct a uniform grid over the entire bounding box,
% points outside the box will return NaN
rad=1; % Radius of interest in units of grid separation

%% Set the grid parameters for calculating b mean magnitude
calcStruct=guiWindowStruct.calcStruct;
calcStruct.setGrid(sep);
calcStruct.setRad(rad);
drawnow

%% Set some different filter ranges
pauseTime = 0.5; 
disp(['Changing filter parameters, pausing ',num2str(pauseTime),' seconds between each change'])
disp('Position windows so that you can see them all, then press enter')
pause()
disp('Setting depth range to < 100km');
pause(pauseTime)


% Note how the GUIs all change in response to change to underlying data
evStruct.setDepthRange('below',100);
drawnow
disp('Setting magnitude range to > 2');
pause(pauseTime)
evStruct.setMagnitudeRange('above',2);
drawnow
disp('Setting magnitude range to in [1 5]');
pause(pauseTime)
evStruct.setMagnitudeRange('in',[1 5]);
disp('Finished!')
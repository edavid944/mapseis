% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 61 $    $Date: 2008-10-13 11:39:22 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

%% Import the data
%fname = '../Import/SearchResults_1983_to_Present_local.txt';
fname='scec_SearchResults.txt'; % 410kB file to test

[fd,calcData] = ms_StartApp();


%% Define filters based on event magnitude
lowerMag = 2;
fd.setMagnitudeRange('above',lowerMag);

%% Select some points in the plot region
%  x=ginput;
% Some preselected points for this data set for testing purposes
x = [   -117.6973   34.2295
    -117.3514   34.5804
    -117.1784   34.9751
    -116.0108   35.1798
    -115.4919   34.7705
    -115.3189   34.1564
    -115.6649   33.8640];

fd.setRegion('in',ms_Region(x));

%% Plot the data and selected region
% Occurs automatically as GUI is linked to underlying data

%% Filter based on distance to grid points
sep =0.5;
% Grid points - construct a uniform grid over the entire bounding box,
% points outside the box will return NaN
rad=3*sep; % Radius of interest in units of grid separation

%% Set the grid parameters for calculating b mean magnitude
calcData.setGrid(sep);
calcData.setRad(rad);
%% Test GUI application

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 61 $    $Date: 2008-10-13 11:39:22 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

% This script file demonstrates the basic functionality of the MapSeis
% application.  The script starts the application with a default data set
% and defines magnitude and region of interest filters.  

[fd,calcData] = ms_StartApp();

% Pause the application at this stage, press enter to continue
disp('Application started.  Press enter to continue.')
pause();

%% Define filters based on event magnitude
% Only look at events that are above magnitude 2.  Once the magnitude range
% is set the two views update automatically since they are subscribed to
% the underlying data object.
lowerMag = 2;
fd.setMagnitudeRange('above',lowerMag);

% Pause the application at this stage, press enter to continue
disp('Lower magnitude filter defined, press enter to continue')
pause();

%% Select some points in the plot region
% The selection region can be defined interactively through the Select menu
% which calls ms_SetRegion to use ginput to select the points on the
% boundary.  Alternatively, the region can be set by calling fd.setRegion
% directly from the command line.
%
% The GUI views update automatically when the region is defined

% Some preselected points for this data set for testing purposes
x = [   -117.6973   34.2295
    -117.3514   34.5804
    -117.1784   34.9751
    -116.0108   35.1798
    -115.4919   34.7705
    -115.3189   34.1564
    -115.6649   33.8640];

fd.setRegion('in',ms_Region(x));

% Pause the application at this stage, press enter to continue
disp('Region of interest defined.  Press enter to continue')
pause();

%% Filter based on distance to grid points
sep =0.5;
% Grid points - construct a uniform grid over the entire bounding box,
% points outside the box will return NaN
rad=3*sep; % Radius of interest in units of grid separation

%% Set the grid parameters for calculating b mean magnitude
% The calculation grid view updates once both parameters are defined
calcData.setGrid(sep);
calcData.setRad(rad);

%% Filter the data programatically
% As well as the GUI control of the application it is possible to control
% the data from the command line.  The following cells demonstrate various
% filtering possibilities.  Press CTRL+Enter to evaluate a cell.  After
% each cell is evaluated the views will update.

%% Commented out examples
% fd.setMagnitudeRange('above',3)
% fd.getTimeRange()
% fd.getMagnitudeRange()
% fd.getRegion()
% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 67 $    $Date: 2008-10-13 18:11:17 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell
%% Import the data
% % 30MB of data - use this for efficiency testing
% fname = '../Import/SearchResults_1983_to_Present_local.txt';
% % 400kB of data
% fname = '../Import/scec_SearchResults.txt';
% 400kB of data
fname = 'scec_SearchResults.txt';
% Stored session - need to update this to use the new object system
% fname='ms_Test/testData20080204.mat'; % 410kB file to test

% Start with previously saved session
[evStruct,guiWindowStruct] = mapseis.startApp(fname);

%% Plot the data and selected region
% Occurs automatically as GUI is linked to underlying data

%% Filter based on distance to grid points
gridPars.gridSep =0.2;
% Grid points - construct a uniform grid over the entire bounding box,
% points outside the box will return NaN
gridPars.rad=1; % Radius of interest in units of grid separation

%% Set the grid parameters for calculating b mean magnitude
evStruct.setUserData('gridPars',gridPars);
drawnow

%% Set some different filter ranges
pauseTime = 0.5; 
disp(['Changing filter parameters, pausing ',num2str(pauseTime),' seconds between each change'])
disp('Position windows so that you can see them all, then press enter')
pause()
disp('Setting depth range to < 100km');
pause(pauseTime)


% Note how the GUIs all change in response to change to underlying data
evStruct.setDepthRange('below',100);
drawnow
disp('Setting magnitude range to > 2');
pause(pauseTime)
evStruct.setMagnitudeRange('above',2);
drawnow
disp('Setting magnitude range to in [1 5]');
pause(pauseTime)
evStruct.setMagnitudeRange('in',[1 5]);
disp('Finished!')
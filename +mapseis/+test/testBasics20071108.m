% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 61 $    $Date: 2008-01-23 10:52:45 +0000 (Wed, 23 Jan 2008) $
% Author: Matt McDonnell
%% Import the data 
%fname = '../Import/SearchResults_1983_to_Present_local.txt';
fname='scec_searchResults.txt'; % 410kB file to test
tic; 
fd = ms_EventStore(ms_importSCECCatRead(fname,'-file')); 
toc

%% Define filters based on event magnitude
lowerMag = 2;
fd.setMagnitudeRange('above',lowerMag);

%% Use the magnitude filter 
eventLocs = fd.getLocations();
disp(['Number of events ',num2str(numel(eventLocs(:,1)))]);
figure(1)

subplot(2,2,[1 3])
ms_PlotRegion(fd.getLocations())

subplot(2,2,2)
ms_PlotMagHist(fd.getMagnitudes());

subplot(2,2,4)
ms_PlotTime(fd.getTimes());

drawnow;
pause
%% Select some points in the plot region
%  x=ginput;
% Some preselected points for this data set for testing purposes
x = [   -117.6973   34.2295
 -117.3514   34.5804
 -117.1784   34.9751
 -116.0108   35.1798
 -115.4919   34.7705
 -115.3189   34.1564
 -115.6649   33.8640];

pRegion = ms_Region(x);

fd.setRegion('in',pRegion);
%fd.setFilter('inRegion',ms_CombineFilter(fd.getDefaultFilter(),ms_RegionFilter('in',pRegion)));
% fd.setFilter('outRegion',ms_CombineFilter(ms_MagFilter('above',2),ms_RegionFilter('out',pRegion)));

%% Plot the data and selected region
subplot(2,2,[1 3])
[inReg,outReg] = fd.getLocations();

bBox = pRegion.getBoundingBox();
nTicks=20; 
xSep = (bBox(2)-bBox(1))/nTicks; ySep=(bBox(4)-bBox(3))/nTicks;
% Grid points
gPts = pRegion.getGrid([xSep ySep]);
rBdry = pRegion.getBoundary();

ms_PlotRegion(...
    struct('inRegion',inReg,...
    'outRegion',outReg,...
    'boundary',rBdry));%,...
%    'grid',gPts));


%% Filter based on distance to grid points
inRegData = ms_EventStore(fd.getAllData());
% Define the 
numGridPoints = numel(gPts(:,1)); % Number of grid points

rad=3*max([xSep, ySep]); % Radius of interest in units of grid separation
% Boundary of region


%% Get b mean mag
bMeanMag = zeros(numGridPoints,4);
tic;
for i = 1:numGridPoints
    thisGridPoint = gPts(i,:);
    % Create a filter that selects points within 'rad' of the gridpoint
    % based on sum of squares distance
    inRegData.setFilter('gridPt',ms_DistFilter(thisGridPoint,rad));

    mags = inRegData.getMagnitudes('gridPt');
    res = ms_CalcBMeanMag(mags);
    figure(2)
    theseLocs = inRegData.getLocations('gridPt');
    % Plot things as we cycle through the plot region
    circRad = [arrayfun(@(x) thisGridPoint(1) + rad*cos(x),linspace(0,2*pi,100)'),...
        arrayfun(@(x) thisGridPoint(2) + rad*sin(x),linspace(0,2*pi,100)')];
    plot(eventLocs(:,1),eventLocs(:,2),'b.',theseLocs(:,1),theseLocs(:,2),'rx',...
        gPts(i,1),gPts(i,2),'go',circRad(:,1),circRad(:,2),'k--',...
        rBdry(:,1),rBdry(:,2),'k-')
    drawnow;
    %pause(0.1)
    bMeanMag(i,:) = [res.meanMag, res.stdDev, res.a, res.b];
end
toc

%% Plot the b mean mag values
figure(2)
% subplot(2,2,2)
axis(bBox)
bMeanVals = bMeanMag(:,1);
scaleFun = @(x) (x-min(bMeanVals))/(max(bMeanVals)-min(bMeanVals));
for i=1:numGridPoints
    patch(gPts(i,1)+xSep/2*[-1 1 1 -1 -1],gPts(i,2)+ySep/2*[-1 -1 1 1 -1],scaleFun(bMeanMag(i,1)))
end
title('Mean Mag')
function  [handle legendentry] = PlotPolar(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	%Plots everything with polar coordinates
	
	%possible config fields for this function:
	%Data:				Not optional, the data to plot Data(:,1) for the distance (theta)and
	%					Data(:,2) for the angle (rho)
	%AngleType:			%default is 'rad' (or empty) it can be set to 'degree'
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) no labels are drawn,
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) no labels are drawn
	%Line_or_Point:		Defines the plotting style, left empty or set to 'line' will 
	%					draw only the line, set to 'point' will only draw markers and
	%					set to 'both' will draw markers and the line
	%LineStylePreset:	This allows to change the style of the plotted line (if plotted)
	%					with simple presets if not not specified the default style ('normal') 
	%					will be used. The presets are: 'normal','dotted','slashed','Fatline'
	%					'Fatdot','Fatslash and the two automatic settings which try use different
	%					LineStyle every plot: 'automatic' and 'Fatauto' for wider lines.
	%CustomLine:	 	This allows to specify the line properties freely, CustomLine{1} defines
	%					LineStyle and CustomLine{2} defines the LineWidth. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomLine should be missing in
	%					ResStruct
	%MarkerStylePreset:	This allows to change the Style of the Marker (if plotted) with simple Presets 
	%					if not not specified the default style ('normal') will be used. The presets 
	%					are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%					which try use different MarkerStyles every plot: 'automatic' and 'auto_nodot' which 
	%					does not use the point ('.').
  	%CustomMarker:	 	This allows to specify the Marker properties freely, CustomMarker{1} defines
	%					the Marker and CustomMarker{2} defines the MarkerSize. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomMarker should be missing in
	%					ResStruct
	%Colors:			This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%					if not not specified the default Color ('blue') will be used. The presets 
	%					are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%					which try use different Color every plot.
	%MarkerColors:		If specified the Color of the Markers will be set different than the line, {1} for the MarkerEdgeColor
	%			{2} for the MarkerFaceColor, the entries have to be standard matlab color option (e.g. 'r')
	%CustomColors:	 	This allows to specify the Color properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColor should be missing in
	%					ResStruct
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%					'Polardata' will be used as LegendEntry.
	
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	
	
	
	%Build some internal data settings
	wheelLine={'-','--',':'};
	wheelMarker={'.','+','o','x','diamond'}
	wheelMarker_nodot={'+','o','x','diamond'}
	wheelColor={'b','r','g','k','c','m'}
	
	%by default the angles are rads, they will be change if specified
	
	theta=ResStruct.Data(:,1);
	rho=ResStruct.Data(:,2);
	
	%Selected the right angles
	if isfield(ResStruct,'AngleType')
		if strcmp(ResStruct.AngleType,'degree')
			rho=rho/360*2*pi;
		end
	end
	
	
	
	%plot the data
	handle = polar(plotAxis,theta,rho);
	
	%change to default linewitdth of 1
	set(handle,'LineStyle','-','LineWidth',1)
	
	%now change the parameters of the plot
	%-------------------------------------

		%the labels: if not set, set to '' and ''
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='';
			end
			
			hYLabel = ylabel(plotAxis,Y_axisname);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
		
		
		%set to points and/or line
		if ~isempty(ResStruct.Line_or_Point) & strcmp(ResStruct.Line_or_Point,'points')
			set(handle,'LineStyle','none');
			set(handle,'Marker','.','MarkerSize',5);
		
		elseif ~isempty(ResStruct.Line_or_Point) & strcmp(ResStruct.Line_or_Point,'both')
			set(handle,'Marker','.','MarkerSize',5);
		end	

		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.LineStylePreset) & ~strcmp(ResStruct.Line_or_Point,'points')
			switch ResStruct.LineStylePreset
				case 'normal'
					set(handle,'LineStyle','-','LineWidth',1);
				case 'dotted'
					set(handle,'LineStyle',':','LineWidth',1);
				case 'slashed'
					set(handle,'LineStyle','--','LineWidth',1);
				case 'Fatline'
					set(handle,'LineStyle','-','LineWidth',1.5);
				case 'Fatdot'
					set(handle,'LineStyle',':','LineWidth',1.5);
				case 'Fatslash'
					set(handle,'LineStyle','--','LineWidth',1.5);
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1);
				case 'Fatautomatic'
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1.5);
			
			end
		end
		
		
		
		%Customization Line
		if isfield(ResStruct,'CustomLine')
			%This option allows to customize the line with the common matlab commands
			set(handle,'LineStyle',ResStruct.CustomLine{1},'LineWidth',ResStruct.CustomLine{2});
		end	
		
		
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.MarkerStylePreset) & ...
			(strcmp(ResStruct.Line_or_Point,'points') | strcmp(ResStruct.Line_or_Point,'both'))
			
			switch ResStruct.MarkeStylePreset
				case 'normal'
					set(handle,'Marker','.','MarkerSize',5)
				case 'plus'
					set(handle,'Marker','+','MarkerSize',5)
				case 'circle'
					set(handle,'Marker','o','MarkerSize',5)
				case 'cross'
					set(handle,'Marker','x','MarkerSize',5)
				case 'diamond'
					set(handle,'Marker','diamond','MarkerSize',5)
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1},'MarkerSize',5);
				case 'auto_nodot'
					%maybe usefull to avoid the small dot symbol
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker_nodot{mod(existPlots,4)+1},'MarkerSize',5);
			
			end
		end

		
		%Customization Line
		if isfield(ResStruct,'CustomMarker')
			%This option allows to customize the line with the common matlab commands
			set(handle,'MarkerStyle',ResStruct.CustomMarker{1},'MarkerSize',ResStruct.CustomMarker{2});
		end	

		
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
			
			switch ResStruct.Colors
				case 'blue'
					set(handle,'Color','b')
				case 'red'
					set(handle,'Color','r')
				case 'green'
					set(handle,'Color','g')
				case 'black'
					set(handle,'Color','k')
				case 'cyan'
					set(handle,'Color','c')
				case 'magenta'
					set(handle,'Color','m')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Color',wheelColor{mod(existPlots,6)+1});
							
			end
		end

		%Customization Color
		if isfield(ResStruct,'CustomColors')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Color',ResStruct.CustomColors);
		end	

		
		if isfield(ResStruct,'MarkerColors')
			if ~isempty(ResStruct.MarkerColors)
				set(handle,'MarkerEdgeColor',ResStruct.MarkerColors{1},...
					'MarkerFaceColor',ResStruct.MarkerColors{1});
			end		
		end

		
		
		if isfield(ResStruct,'LegendText');	
			if ~isempty(ResStruct.LegendText)
				legendentry=ResStruct.LegendText;
			else	
				legendentry='PolarData';
			end
		else
		legendentry=[];
					
		end				

end
 function h = PlotMagCDF(plotAxis,evMags)
% PlotMagCDF : Plot CDF plot of event magnitudes


% $Revision: 1 $    $Date: 2009-5-4 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: David Eberhard

import mapseis.plot.plotCDF;

if nargin<2
    plotAxis=gca;
end

h = [];

if ~isempty(evMags)
		%hist(plotAxis,evMags,min(evMags)-0.05:0.1:max(evMags)+0.05);
		h=plotCDF(evMags, plotAxis, 'r');
		set(h,'Linewidth',2);

		hXLabel = xlabel(plotAxis,'Magnitude');
		hYLabel = ylabel(plotAxis,' ');
		hTitle = title(plotAxis,'CDF plot Magnitude ');
		
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 14,'FontWeight' , 'bold');
		set( hTitle                          , 'FontSize'   , 16          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'off'     , ...
		  'TickDir'     , 'out'     , ...
		  'Xlim'        , [min(evMags)-0.1 max(evMags)+0.1 ] , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'off'      , ...
		  'XGrid'       , 'off'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1.5,         ...
		  'FontWeight' , 'bold',     ...
		  'FontSize'   , 14);
else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end

end
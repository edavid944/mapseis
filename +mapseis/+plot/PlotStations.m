function  [handle legendentry] = PlotStations(plotAxis,DataStore,PlotConfig)
	%plots the station into map
	%the stationlist has to be contained in the Datastore
	%PlotConfig is a structure with the following fields:
	%	Colors:	Color of the Symbols, if left empty, it will set to default
	%	Size:	Size of the Markers
	%	Marker:	Allows to set a different marker than the default one
	%	NameTag:Will write the name of the station next to the symbol
	%	StartDate:The earliest time of the shown catalog (only stations after
	%		  this time will be plotted (only available in timevar mode)
	%		  if left empty it will be set to -Inf (has to be in the serial 
	%		  date format
	%	EndDate:  Same as StartDate but for the last shown date.
	
	
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	legendentry='Seimsic Stations';
	
	%try getting the Stationdata from the datastore
	try
		StationData=DataStore.getUserData('StationList');
	catch
		disp('No stationlist available')
		handle=[];
		legendentry='No stationlist available';
		return
	end	
	
	%distance text
	dx=0.1;
	
	if strcmp(StationData.ListMode,'timevar')
		if isempty(PlotConfig.StartDate)
			StartDate=-Inf;
		else
			StartDate=PlotConfig.StartDate;
		end	
		
		if isempty(PlotConfig.EndDate)
			EndDate=-Inf;
		else
			EndDate=PlotConfig.EndDate;
		end	
		
		%find the not set entries
		NotSetStart=isnan(StationData.starttime);
		NotSetEnd=isnan(StationData.endtime);
		
		%lower bound & upper bound
		LowTime=StationData.starttime<=EndDate|NotSetStart;
		HightTime=StationData.endtime>=StartDate|NotSetEnd;
		
		InTime=LowTime&HightTime;
		
		
	else
		InTime=logical(ones(numel(StationData.lat),1));
	
	
	end
	
	%plot data
	handle = plot(plotAxis,StationData.lon(InTime),StationData.lat(InTime));
	
	%change to default parameters
	set(handle,'LineStyle','none','Marker','s','MarkerFaceColor','m','MarkerEdgeColor','m','MarkerSize',5);
	MarkCol='m';
	
	
	%now change the parameters of the plot
	%-------------------------------------
	%Set User Color
	if isfield(PlotConfig,'Colors')
		if ~isempty(PlotConfig.Colors)
			set(handle,'MarkerEdgeColor',PlotConfig.Colors,...
				'MarkerFaceColor',PlotConfig.Colors);
			MarkCol=PlotConfig.Colors;
		end		
	end
	
	%Set Marker
	if isfield(PlotConfig,'Marker')
		if ~isempty(PlotConfig.Marker)
			set(handle,'Marker',PlotConfig.Marker);
		end		
	end
		
	%Set Size
	if isfield(PlotConfig,'Size')
		if ~isempty(PlotConfig.Size)
			set(handle,'MarkerSize',PlotConfig.Size);
		end		
	end
		
	if isfield(PlotConfig,'NameTag')
		if ~isempty(PlotConfig.NameTag)
			if PlotConfig.NameTag
				hold on
				
				%plot the name texts
				names=StationData.Name(InTime);
				lons=StationData.lon(InTime);
				lats=StationData.lat(InTime);					
                axes(plotAxis);

				for i=1:numel(lons)
					te1 = text(lons(i)+dx,lats(i),names(i),'era','back','clipping','on');
					set(te1,'FontWeight','bold','Color',MarkCol,'FontSize',[9]);
				
				end
			end
		end		
	end

	
		
		
	
end
function  [handle legendentry] = PlotVolume3D(plotAxis,ResStruct)
	%This function creates isosurfaces and isocaps in with the entered values
	
	%possible config fields for this function:
	%similar to generic 2D but *d
	%Data:			Not optional, the data to plot Data{1} for the x-values
	%			Data{2} for the y-values and Data{3} for z-values and Data{4}
	%			for the the value (Matrix(x,y,z)	
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%				will be set to 'X', if the field is missing, nothing will be
	%				done	
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y', if the field is missing, nothing will be
	%					done			
	%Z_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y', if the field is missing, nothing will be
	%					done			
	%Surfaces:		Sets the plotted surfaces: [minVal maxVal Spacing]
	%UseCustom:		If set to true the custom surfaces will be used
	%CustomSurfaces:	Allows to set custom surfaces at which surfaces are plotted, just an array with values.
	%DrawCaps:		if set to true the function will draw caps at the end of the surfaces
	%AlphaValue:		Sets the overall transparnce of the surfaces
 	%Colors:		This defines either the ColorMap or the Color if only 3 coloumbs are defined
	%			Option for the ColorMap are: 'jet', 'hsv', 'hot', 'gray', 'cool', 'bone', 'autumn', 'summer'	
	%CustomColors:		This allows to specify the ColorMap properties freely, CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomColorMap should be missing in
	%			ResStruct
	
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%Z_Axis_Limit: 		Same for the Z-Axis
	%C_Axis_Limit:		Same for the Color axis
	%ColorToggle:		Draws colorbar
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%					'Data' will be used as LegendEntry.
	
	%more option
	
	
	%use the last plot if not specified
	if nargin<2
    		plotAxis=gca;	
	end	
	
	AlphaVal=1;
	if isfield(ResStruct,'AlphaValue')	
		if ~isempty(ResStruct.AlphaValue)
			AlphaVal=ResStruct.AlphaValue;
		end
	end
	
		
	MrData=ResStruct.Data;
	
	if ~ResStruct.UseCustom
		TheSurf=ResStruct.Surfaces;
		count=1;
		
		axes(plotAxis)
		if ResStruct.DrawCaps
			for i=TheSurf(1):TheSurf(3):TheSurf(2)
				isosurface(MrData{1},MrData{2},MrData{3},MrData{4},i);
				handy=findobj(plotAxis,'Type','patch');
				handle(count)=handy(end);
				
				isocaps(MrData{1},MrData{2},MrData{3},MrData{4},i);
						
			end
		else
			for i=TheSurf(1):TheSurf(3):TheSurf(2)
				isosurface(MrData{1},MrData{2},MrData{3},MrData{4},i);
				handy=findobj(plotAxis,'Type','patch');
				handle(count)=handy(end);
													
			end
		
		
		end
	
	else
		TheSurf=ResStruct.CustomSurfaces;
		axes(plotAxis)
		if ResStruct.DrawCaps
			for i=1:numel(TheSurf)
				isosurface(MrData{1},MrData{2},MrData{3},MrData{4},TheSurf(i));
				handy=findobj(plotAxis,'Type','patch');
				handle(i)=handy(end);
				
				isocaps(MrData{1},MrData{2},MrData{3},MrData{4},TheSurf(i));
						
			end
		else
			for i=1:numel(TheSurf)
				isosurface(MrData{1},MrData{2},MrData{3},MrData{4},TheSurf(i));
				handy=findobj(plotAxis,'Type','patch');
				handle(i)=handy(end);
						
			end
		
		end
	
	
	
	
	end
	
	%set alpha
	axes(plotAxis);
	alpha(AlphaVal);

	%now change the parameters of the plot
	%-------------------------------------
	
	%the labels: if not set, set to 'X' and 'Y'
	if isfield(ResStruct,'X_Axis_Label')
		X_axis_name=ResStruct.X_Axis_Label;
			
		if isempty(X_axis_name)
			X_axis_name='X';
		end
		
		hXLabel = xlabel(plotAxis,X_axis_name);
		set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
	end
		
	
	if isfield(ResStruct,'Y_Axis_Label')
		Y_axis_name=ResStruct.Y_Axis_Label; 	
		
		if isempty(Y_axis_name)
			Y_axis_name='Y';
		end
		
		hYLabel = ylabel(plotAxis,Y_axis_name);
		set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
		
	if isfield(ResStruct,'Z_Axis_Label')
		Z_axis_name=ResStruct.Z_Axis_Label; 	
		
		if isempty(Z_axis_name)
			Z_axis_name='Y';
		end
		
		hZLabel = zlabel(plotAxis,Z_axis_name);
		set(hZLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
	
	
	%change the color to the preset if needed
	if ~isempty(ResStruct.Colors) 
			switch ResStruct.Colors
				case 'jet'
					colormap(plotAxis,jet);
				case 'hsv'
					colormap(plotAxis,hsv);
				case 'hot'
					colormap(plotAxis,hot);
				case 'cool'
					colormap(plotAxis,cool);
				case 'gray'
					colormap(plotAxis,gray);
				case 'bone'
					colormap(plotAxis,bone);
				case 'summer'
					colormap(plotAxis,summer);
				case 'autumn'
					colormap(plotAxis,autumn);
								
			end
				
	end


	%Customization Color
	if isfield(ResStruct,'CustomColors')
			colormap(plotAxis,ResStruct.CustomColors);
	end	

	set(handle,'CDataMapping','scaled');

		

	%set limits if needed
	if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
		xlim(plotAxis,'manual');
		xlim(plotAxis,ResStruct.X_Axis_Limit);	
	end
		
	if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
		ylim(plotAxis,'manual');
		ylim(plotAxis,ResStruct.Y_Axis_Limit);	
	end
	
	if ~isempty(ResStruct.Z_Axis_Limit) | strcmp(ResStruct.Z_Axis_Limit,'auto')
		zlim(plotAxis,'manual');
		zlim(plotAxis,ResStruct.Z_Axis_Limit);	
	end

	if ~isempty(ResStruct.C_Axis_Limit) | strcmp(ResStruct.C_Axis_Limit,'auto')
		caxis(plotAxis,'manual');
		caxis(plotAxis,ResStruct.C_Axis_Limit);	
	end

		
	if ~isempty(ResStruct.ColorToggle) & ResStruct.ColorToggle
		colorbar;
	end
		
	%build legend (if not empty it will just copy the entry from ResStruct)
	if ~isempty(ResStruct.LegendText)
		legendentry=ResStruct.LegendText;
	else	
		legendentry='Data';
	end
		
		
		
	
end

function h = PlotMagTimeDiff(plotAxis,res_struct)
% PlotMagDiff : Plots the cumulative time distribution of events

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

import mapseis.calc.CalcTimeDist;
import mapseis.util.decyear;

if nargin<2
    plotAxis=gca;
end

h=[];

if ~isempty(res_struct.deltaMagTime)
		
		deltaMag = res_struct.deltaMagTime;
		h(1) = errorbar(plotAxis,deltaMag(:,1),deltaMag(:,2),deltaMag(:,3))
	   	hold on
	   	h(2) = plot(plotAxis,deltaMag(:,1),deltaMag(:,2),'rs','LineWidth',[2.0]);
   		h(3) = plot(plotAxis,deltaMag(:,1),deltaMag(:,2),'k','LineWidth',[2.0]);
   
   		%set(plotAxis,'FontSize',fs12,'FontWeight','normal',...
      	%	'FontWeight','bold','LineWidth',[2.0],...
      	%	'Box','on','drawmode','fast','TickDir','out')
   		hold off	
	
	
	
	
		
		hXLabel = xlabel('Time [years]','Interpreter','none')
		hYLabel =  ylabel([ 'M(' res_struct.filenames(2) ') - M(' res_struct.filenames(1) ')'],'Interpreter','none');
		hTitle = title(plotAxis,' ');
		datetick(plotAxis,'x');
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 14,'FontWeight' , 'bold');
		set( hTitle                          , 'FontSize'   , 16          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'on'     , ...
		  'TickDir'     , 'out'     , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'of'      , ...
		  'XGrid'       , 'of'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1.5,         ...
		  'FontWeight' , 'bold',     ...
		  'FontSize'   , 14);

else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end


end
function VarGridPlotter(plotAxis,RefGrid,plotdata,PlotConfig)
	%Prototyp function for plotting grids with variable gridspacing
	%Current Options:
	%Which_nodes: 	can be 0,1 or 2 according to the Select option in the RefGrid
	%		only the nodes marked as this values will be plotted,
	%		CAREFULL, the plotted nodes have to be of the same number
	%		as the plotdata
	%filter_plotdata: has to be set true if plotdata have the same length as
	%		  the total RefGrid. The node select will then also be
	%		  applied to the plotdata
	%sort_mode:	if true the data will be sorted by their dx grid size, largest
	%		on top
	%plot_mode:	can be 'square' for normal grid plotting or 'circle' where the 
	%		cells are plotted as circle with the selection radius as circle
	%		radius or 'squarecircle', with circles having the radius of 
	%		gridcell.
	%rad_coeff:	allows in the 'circle' and 'squarecircle' mode to modify the ploting
	%		radius
	
	
	if numel(PlotConfig.Which_nodes)==1
		TheSelect=RefGrid(:,5)==PlotConfig.Which_nodes;
	else
		TheSelect=logical(zeros(numel(RefGrid(:,5)),1));
		for i=1:numel(PlotConfig.Which_nodes)
			TheSelect=TheSelect|RefGrid(:,5)==PlotConfig.Which_nodes(i);
		end
	end
	
	if ~isfield(PlotConfig, 'plot_mode')
		plot_mode='square';
	else
		plot_mode=PlotConfig.plot_mode;
		
		if ~isfield(PlotConfig, 'rad_coeff')
			rad_coeff=1;
		else
			rad_coeff=PlotConfig.rad_coeff;
		end	

	end	

	
	
	GridPart=RefGrid(TheSelect,:);
	
	if PlotConfig.filter_plotdata
		plotpart=plotdata(TheSelect);
	else
		plotpart=plotdata;
	end	
	
	if PlotConfig.sort_mode
		[temp, sortOrder]=sort(GridPart(:,3),'descend');
		GridPart=GridPart(sortOrder,:);
		plotpart=plotpart(sortOrder);
	
	end
	
	switch plot_mode
		case 'square'
			c=1;
			for i=1:numel(GridPart(:,1))
				
				if ~isnan(plotpart(i))
					ThePatchX(:,c)=[GridPart(i,1)-GridPart(i,3)/2 ; GridPart(i,1)+GridPart(i,3)/2;...
						GridPart(i,1)+GridPart(i,3)/2 ; GridPart(i,1)-GridPart(i,3)/2];	
					ThePatchY(:,c)=[GridPart(i,2)-GridPart(i,4)/2 ; GridPart(i,2)-GridPart(i,4)/2;...
						GridPart(i,2)+GridPart(i,4)/2 ; GridPart(i,2)+GridPart(i,4)/2];
					ColPatch(:,c)=[plotpart(i);plotpart(i);plotpart(i);plotpart(i)];
					c=c+1;
				end	
			end	
	
			
		case 'circle'
			
			c=1;
			N=16;
			circsteps=(0:N)*2*pi/N;
			latlim = get(plotAxis,'Ylim');
			
			
			for i=1:numel(GridPart(:,1))
				
				if ~isnan(plotpart(i))
					rad=GridPart(i,6)*rad_coeff;
					xrad=1/cos(pi/180*mean(latlim))*rad;
					%xrad=rad;
					
					ThePatchX(:,c)=xrad*cos(circsteps)+GridPart(i,1);	
					ThePatchY(:,c)=rad*sin(circsteps)+GridPart(i,2);
					ColPatch(:,c)=ones(N+1,1)*plotpart(i);
					c=c+1;
				end	
			end	
		
			
		case 'squarecircle'
			c=1;
			N=16;
			circsteps=(0:N)*2*pi/N;
			latlim = get(plotAxis,'Ylim');
			
			
			for i=1:numel(GridPart(:,1))
				
				if ~isnan(plotpart(i))
					rad=GridPart(i,3)*rad_coeff;
					xrad=1/cos(pi/180*mean(latlim))*rad;
					
					ThePatchX(:,c)=xrad*cos(circsteps)+GridPart(i,1);	
					ThePatchY(:,c)=rad*sin(circsteps)+GridPart(i,2);
					ColPatch(:,c)=ones(N+1,1)*plotpart(i);
					c=c+1;
				end	
			end	
		
	end
	
			
	axes(plotAxis);
	patch(ThePatchX,ThePatchY,ColPatch);


end

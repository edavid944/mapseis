function h = PlotMagDiff(plotAxis,mags_A,mags_B,res_struct)
% PlotMagDiff : Plots the cumulative time distribution of events

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

import mapseis.calc.CalcTimeDist;
import mapseis.util.decyear;

if nargin<2
    plotAxis=gca;
end

h=[];

if ~isempty(res_struct.deltaMagTime)
			
			
			
		   %single events as an mag-x magy plot	
		   h(1) = plot(plotAxis,mags_B,mags_A,'^')
		   
		   hold on
		   magline = (0:0.1:6);
		   
		   %middle line: 1:1 coorelation
		   h(2) = plot(plotAxis,magline,magline,'k','LineWidth',[1])
		   
		   [p,s] = polyfit(mags_B,mags_A,1);
		   f = polyval(p,(0:0.1:7));
		   
		   hold on
		   r = corrcoef(mags_A,mags_B);
		   r = r(1,2);
		   stri = [ 'p = ' num2str(p(1)) '*m +' num2str(p(2))  ];
		   stri2 = [ 'r = ' num2str(r) ];
		   te1 = text(1,5.8,stri);
		   set(te1,'FontSize',[12],'FontWeight','bold')
		   te1 = text(1,5.4,stri2);
		   set(te1,'FontSize',[12],'FontWeight','bold')
		   mb2 = polyval(p,0:0.1:7);
		   
		   %linear relation between Mag A and Mag B
		   h(3) = plot(plotAxis,0:0.1:7,mb2,'r','LineWidth',[2.5])
		   
		   hold off
		   
		  % set(plotAxis,'FontSize',fs12,'FontWeight','normal',...
		   %   'FontWeight','bold','LineWidth',[2.0],...
		   %   'Box','on','drawmode','fast','TickDir','out')
		   axis([ 0 6 0 6.5])
		   
		   grid
	
	
	
	
	
	
		
		hXLabel = xlabel([ res_struct.filenames(2) ' Magnitudes'],'Interpreter','none')
		hYLabel =  ylabel([ res_struct.filenames(1) ' Magnitudes'],'Interpreter','none')
		hTitle = title(plotAxis,'Magnitude - Magnitude relation between the two catalogs');
		%datetick(plotAxis,'x');
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 14,'FontWeight' , 'bold');
		set( hTitle                          , 'FontSize'   , 16          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'on'     , ...
		  'TickDir'     , 'out'     , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'of'      , ...
		  'XGrid'       , 'of'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1.5,         ...
		  'FontWeight' , 'bold',     ...
		  'FontSize'   , 14);
		  
		 
		  legentries{1}= 'single identical events'
		  legentries{2}= 'linear 1:1 Relation'
		  legentries{3}= 'actual linear Relation'
		  
		  legend(plotAxis,legentries{:},'Location','SouthEast');
		  
else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end


end
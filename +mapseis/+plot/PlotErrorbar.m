function  [handle legendentry] = PlotErrobar(plotAxis,ResStruct)
	%NOT FINISHED: ---> should be finished now
	%	Legends needs to be checked (order of the entries)
	%	Can more than one handle be used in one set command?
	%	Colorline setting may be changed
	
	
	%This function is a genericplot used by the ResultGUI
	%possible config fields for this function:
	%Data:				Not optional, the data to plot Data(:,1) for the x-values
	%					Data(:,2) for the y-values, Data(:,3) for the error or for
	%					the lower in case a forth coloum is used. Data(:,4) upper 
	%					error (optional)
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'X',if the field is missing, nothing will be
	%					done
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y',if the field is missing, nothing will be
	%					done			
	%Line_or_Point:		Defines the plotting style, left empty or set to 'line' will 
	%					draw only the line, set to 'point' will only draw markers and
	%					set to 'both' will draw markers and the line, this sets only the main
	%					plot line
	%ErrobarStyle:		Defines the style of the errorbars: 'normal' for the normal errorbars,
	%					'fatbars' sames normal but with wider lines, 'slashed' draws a slashed line for the 
	%					errorbars, 'dotted' draws a dotted line and 'thinline' uses a thin line for the bars 
	%LineStylePreset:	This allows to change the style of the plotted line (if plotted)
	%					with simple presets if not not specified the default style ('normal') 
	%					will be used. The presets are: 'normal','dotted','slashed','Fatline'
	%					'Fatdot','Fatslash and the two automatic settings which try use different
	%					LineStyle every plot: 'automatic' and 'Fatauto' for wider lines.
	%CustomLine:	 	This allows to specify the line properties freely, CustomLine{1} defines
	%					LineStyle and CustomLine{2} defines the LineWidth. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomLine should be missing in
	%					ResStruct
	%MarkerStylePreset:	This allows to change the Style of the Marker (if plotted) with simple Presets 
	%					if not not specified the default style ('normal') will be used. The presets 
	%					are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%					which try use different MarkerStyles every plot: 'automatic' and 'auto_nodot' which 
	%					does not use the point ('.').
  	%CustomMarker:	 	This allows to specify the Marker properties freely, CustomMarker{1} defines
	%					the Marker and CustomMarker{2} defines the MarkerSize. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomMarker should be missing in
	%					ResStruct
	%Colors:				This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%					if not not specified the default Color ('blue') will be used. The presets 
	%					are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%					which try use different Color every plot.
	%BarColors:			Same as Color but for the errorbars, also here exists a 'same' option which uses the same
	%					Color as in the rest of the plot
	%CustomColors:	 	This allows to specify the Color properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColor should be missing in
	%					ResStruct
	%CustomBarColors:	Same as CustomColor but for the bars.
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%BarTextToggle:		Sets the errorbars in the legend either to on (true) or off (false), and sends either all handles out or only 
	%					the one from the main line
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%					'Data' will be used as LegendEntry.
	
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end
	
	
	
	
	%Build some internal data settings
	wheelLine={'-','--',':'};
	wheelMarker={'.','+','o','x','diamond'}
	wheelMarker_nodot={'+','o','x','diamond'}
	wheelColor={'b','r','g','k','c','m'}
	
	dime=size(ResStruct.Data);
	
	
	%plot the data
	if dime(2)==3 %symmetric errobars
		
		if isempty(ResStruct.ErrorbarStyle) | strcmp(ResStruct.ErrorbarStyle,'normal') | strcmp(ResStruct.ErrorbarStyle,'fatbars')
			handle = errorbar(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2),ResStruct.Data(:,3));
			childs=get(handle,'children');
			
		else
			%mainline
			handle = plot(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2))	
			childs(1)=handle
			
			%lower error
			childs(2)=plot(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2)-ResStruct.Data(:,3))
			set(childs(2),'LineStyle','--','LineWidth',1)
			
			%upper error
			childs(3)=plot(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2)+ResStruct.Data(:,3))
			set(childs(3),'LineStyle','--','LineWidth',1)
			
		end
	
	elseif dime(2)==4 %asymmetric errorbars
		
		if isempty(ResStruct.ErrorbarStyle) | strcmp(ResStruct.ErrorbarStyle,'normal') | strcmp(ResStruct.ErrorbarStyle,'fatbars')
			handle = errorbar(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2),ResStruct.Data(:,3),ResStruct.Data(:,4));
			childs=get(handle,'children');
		
		else
			%mainline
			handle = plot(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2))	
			childs(1)=handle
			
			%lower error
			childs(2)=plot(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2)-ResStruct.Data(:,3))
			set(childs(2),'LineStyle','--','LineWidth',1)
			
			%upper error
			childs(3)=plot(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2)+ResStruct.Data(:,4))
			set(childs(3),'LineStyle','--','LineWidth',1)
			
		end
		
			
	end
	
	
	%change to default linewitdth of 1
	set(handle,'LineStyle','-','LineWidth',1)
	
	%now change the parameters of the plot
	%-------------------------------------
	
		%the labels: if not set, set to 'X' and 'Y'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Y';
			end
			
			hYLabel = ylabel(plotAxis,Y_axisname);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end
		
		
		%set to points and/or line
		if ~isempty(ResStruct.Line_or_Point) & strcmp(ResStruct.Line_or_Point,'points')
			set(handle,'LineStyle','none');
			set(handle,'Marker','.','MarkerSize',5);
		
		elseif ~isempty(ResStruct.Line_or_Point) & strcmp(ResStruct.Line_or_Point,'both')
			set(handle,'Marker','.','MarkerSize',5);
		end	

		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.LineStylePreset) & ~strcmp(ResStruct.Line_or_Point,'points')
			switch ResStruct.LineStylePreset
				case 'normal'
					set(handle,'LineStyle','-','LineWidth',1);
				case 'dotted'
					set(handle,'LineStyle',':','LineWidth',1);
				case 'slashed'
					set(handle,'LineStyle','--','LineWidth',1);
				case 'Fatline'
					set(handle,'LineStyle','-','LineWidth',1.5);
				case 'Fatdot'
					set(handle,'LineStyle',':','LineWidth',1.5);
				case 'Fatslash'
					set(handle,'LineStyle','--','LineWidth',1.5);
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1);
				case 'Fatautomatic'
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1.5);
			
			end
		end
		
		
		
		
		
		%Customization Line
		if isfield(ResStruct,'CustomLine')
			%This option allows to customize the line with the common matlab commands
			set(handle,'LineStyle',ResStruct.CustomLine{1},'LineWidth',ResStruct.CustomLine{2});
		end	
		
		
		
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.ErrobarStyle)
			switch ResStruct.ErrobarStyle
				case 'thinline'
					set(childs(2:end),'LineStyle','-','LineWidth',0.5);
				case 'dotted'
					set(childs(2:end),'LineStyle',':','LineWidth',1);
				case 'slashed'
					set(childs(2:end),'LineStyle','--','LineWidth',1);

			
			end
		end

		
		
		
		
		%change the Markertyle with a preset if needed 
		if ~isempty(ResStruct.MarkerStylePreset) & ...
			(strcmp(ResStruct.Line_or_Point,'points') | strcmp(ResStruct.Line_or_Point,'both'))
			
			switch ResStruct.MarkeStylePreset
				case 'normal'
					set(handle,'Marker','.','MarkerSize',5)
				case 'plus'
					set(handle,'Marker','+','MarkerSize',5)
				case 'circle'
					set(handle,'Marker','o','MarkerSize',5)
				case 'cross'
					set(handle,'Marker','x','MarkerSize',5)
				case 'diamond'
					set(handle,'Marker','diamond','MarkerSize',5)
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1},'MarkerSize',5);
				case 'auto_nodot'
					%maybe usefull to avoid the small dot symbol
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker_nodot{mod(existPlots,4)+1},'MarkerSize',5);
			
			end
		end

		
		%Customization Line
		if isfield(ResStruct,'CustomMarker')
			%This option allows to customize the line with the common matlab commands
			set(handle,'MarkerStyle',ResStruct.CustomMarker{1},'MarkerSize',ResStruct.CustomMarker{2});
		end	

		
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
			
			switch ResStruct.Colors
				case 'blue'
					set(handle,'Color','b')
				case 'red'
					set(handle,'Color','r')
				case 'green'
					set(handle,'Color','g')
				case 'black'
					set(handle,'Color','k')
				case 'cyan'
					set(handle,'Color','c')
				case 'magenta'
					set(handle,'Color','m')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Color',wheelColor{mod(existPlots,6)+1});
							
			end
		end
		
		%Customization Color
		if isfield(ResStruct,'CustomColors')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Color',ResStruct.CustomColors);
		end	
		
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.BarColors) 
			if strcmp(ResStruct.BarColors,'same')
				BarCol=ResStruct.Colors;
			else
				BarCol=ResStruct.BarColors;
			end
				
			switch BarCol
				case 'blue'
					set(childs(2:end),'Color','b')
				case 'red'
					set(childs(2:end),'Color','r')
				case 'green'
					set(childs(2:end),'Color','g')
				case 'black'
					set(childs(2:end),'Color','k')
				case 'cyan'
					set(childs(2:end),'Color','c')
				case 'magenta'
					set(childs(2:end),'Color','m')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(childs(2:end),'Color',wheelColor{mod(existPlots,6)+1});
							
			end
		end

		
		%Customization Color
		if isfield(ResStruct,'CustomBarColors')
			%This option allows to customize the line with the common matlab commands
			set(childs(2:end),'Color',ResStruct.CustomColors);
		end	




		%set limits if needed
		if ~isempty(ResStruct.X_AxisLimit) | strcmp(ResStruct.X_AxisLimit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_AxisLimit);	
		end
		
		if ~isempty(ResStruct.Y_AxisLimit) | strcmp(ResStruct.Y_AxisLimit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_AxisLimit);	
		end
		
		
		%legendtoggle
		if BarTextToggle
			handle=childs
		end
			
		%build logical legendbuidling criterium (if true only two legendentries are needed)
		legendCriterium=strcmp(ResStruct.ErrobarStyle,'normal') | strcmp(ResStruct.ErrobarStyle,'fatbars')|…
						isempty(ResStruct.ErrobarStyle);
		
		
		%build legend (if not empty it will just copy the entry from ResStruct)
		if isfield(ResStruct,'LegendText')
			if BarTextToggle
				if ~isempty(ResStruct.LegendText)
					if dime(2)==3 | legendCriterium %symmetric errobars
						legendentry={ResStruct.LegendText,['Error of ',ResStruct.LegendText]};
					elseif dime(2)==4 & ~legendCriterium
						legendentry={ResStruct.LegendText,['Lower error of ',ResStruct.LegendText],['Upper error of ',ResStruct.LegendText]};
					end
				else
					if dime(2)==3 | legendCriterium %symmetric errobars 
						legendentry={'Data','Error of the data'};
					elseif dime(2)==4 & ~legendCriterium
						legendentry={'Data','Lower error of the data','Upper error of the data'};
					end	
				end
			
			else
				%Errorbar entry not wanted	
				if ~isempty(ResStruct.LegendText)
					legendentry=ResStruct.LegendText;
				else
					legendentry='Data';
				end
			end
		
		else
			
			legendentry=[];
			
		end	
	
end
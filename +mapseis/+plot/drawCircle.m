function handle = drawCircle(plotAxis,geomode,midPoint,rad,CircRes)
	%simply draws a cirlce with a given middlepoint and a given radius
	%CircRes can be used to increase or decrease the resolution of the plot
	%and geomode allows to correct the distortion due to the geographic 
	%coordinate system$
	
	if nargin<5
		N=256;
	else	
		N=CircRes;
	end	

	circsteps=(0:N)*2*pi/N;
	
	latlim = get(plotAxis,'Ylim');
	
	if geomode
		xrad=1/cos(pi/180*mean(latlim))*rad;
	else
		xrad=rad;
	end
	
	%changed it, now the patch is just a ring leaving the interior free, needed because
	%some times the patch is draw after the middle point. 
	handle(1) = plot( xrad*cos(circsteps)+midPoint(1), rad*sin(circsteps)+midPoint(2));
	handle(2) = patch([xrad*cos(circsteps)+midPoint(1),(0.25*xrad)*cos(circsteps)+midPoint(1)], ...
			[rad*sin(circsteps)+midPoint(2),(0.25*rad)*sin(circsteps)+midPoint(2)],...
			ones(1,2*numel(circsteps)),'FaceColor','none');
	
	set(handle(1), 'LineWidth',1.5,'Color','r');
	set(handle(2), 'LineStyle','none');
end

function  [handle legendentry] = PlotPoint(plotAxis,ResStruct)
%This function plots a movable point in the ResultGUI 
%possible Configs
	%Coord:			Just two coordinates are needed (first value for x and second y)
	%			It is possible to leave this field empty, in this case the coordinate
	%			will be two random number in the axis limits	
	%Tag:			The tag will be applied on the point impoly object, this allows to find it later
	%			with other functions
	%Colors:		This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%			if not not specified the default Color ('blue') will be used. The presets 
	%			are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%			which try use different Color every plot.
		
	
	%not the handle is here not a handle but an object
	
	%some needed constants
	wheelColor={'b','r','g','k','c','m'};

			
	
	disp('I work')

	handle=impoint(plotAxis,[ResStruct.Coord(1),ResStruct.Coord(2)]);
	

	
	
	%set the color if wanted
	if isfield(ResStruct,'Colors')
		if ~isempty(ResStruct.Colors)
			switch ResStruct.Colors
				case 'blue'
					handle.setColor('b')
				case 'red'
					handle.setColor('r')
				case 'green'
					handle.setColor('g')
				case 'black'
					handle.setColor('k')
				case 'cyan'
					handle.setColor('c')
				case 'magenta'
					handle.setColor('m')
				case 'automatic'
					%experimental but should work,
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','hggroup'));
					handle.setColor(wheelColor{mod(existPlots,6)+1});
		
											
			end
		end
	end	
	
	%Set the tag if existing
	if isfield(ResStruct,'Tag')
		if ~isempty(ResStruct.Tag)
			set(handle,'Tag',ResStruct.Tag)
		end
	end
	

	legendentry=[];
	
	
end

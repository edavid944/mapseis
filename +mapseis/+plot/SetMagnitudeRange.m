function SetMagnitudeRange(evStruct,rangeType)
% SetMagnitudeRange : set the time range of interest

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

import mapseis.util.*;

if nargin<2
    rangeType = 'in';
end

try
    switch rangeType
        case {'above','below'}
            [x,y] = Ginput(1); %#ok<NASGU>
            evStruct.setMagnitudeRange(rangeType,x);
        case 'in'
            [x,y] = Ginput(2); %#ok<NASGU>
            evStruct.setMagnitudeRange(rangeType,x);
        case 'all'
            eStruct = evStruct.getData();
            evStruct.setMagnitudeRange('above',min(eStruct.mag));
        otherwise
            error('SetMagnitudeRange:unknown_range_type','Unknown range type');
    end
catch ME
    warning('SetMagnitudeRange:range_not_set','Unable to set magnitude range');
end


end
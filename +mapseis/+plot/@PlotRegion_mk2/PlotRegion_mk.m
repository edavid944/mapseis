classdef PlotRegion_mk2 < handle
	%Version to of the PlotRegion plot function. This time it is a class and it
	%cleaner built	
	
	%compatibility to the original version will be handle through adapter 
	%functions

	
	% This file is part of MapSeis.

	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Copyright 2014 David Eberhard

	
	properties
		plotAxis
		PlotConfig
		Datastore

	end
	
	
	
	methods
		function obj = PlotRegion_mk2(plotAxis,Datastore,PlotConfig)
			
			PlotConfigDefault= struct(	'Selected',[[]],...
										'RegionType','FullRegion',...
										'MagnitudeQuality','low',...
										'Color','black',...
										'PlotSelected',true,...
										'PlotUnSelected',true,...
										'PlotCoasline',true,...
										'PlotBorder',true,...
										'PlotPlateBoundary',false,...
										'PlotStations',false,...
										'Plot)
		

			
			
		end
	
		
		%functions located in external files
			
		

	end




end



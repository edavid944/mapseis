function  [handle legendentry] = PlotDensityPoints2D(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	%Plots points and and sets the color (color to white) for the density, this allows to 
	%better see regions with many points
	%As it uses a loop it might not be the fastest function on earth
	
	
	%possible config fields for this function:
	%Data:			Not optional, the data to plot Data(:,1) for the x-values
	%			Data(:,2) for the y-values
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%			will be set to 'X', if the field is missing, nothing will be
	%			done	
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%			will be set to 'Y', if the field is missing, nothing will be
	%			done			
	%MarkerStylePreset:	This allows to change the Style of the Marker (if plotted) with simple Presets 
	%			if not not specified the default style ('normal') will be used. The presets 
	%			are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%			which try use different MarkerStyles every plot: 'automatic' and 'auto_nodot' which 
	%			does not use the point ('.').
	%MarkerSize:		Says it all
	%CustomMarker		Markertype,
	%Colors:		This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%			if not not specified the default Color ('blue') will be used. The presets 
	%			are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%			which try use different Color every plot.
	%CustomColors:	 	This allows to specify the Color properties freely, CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomColor should be missing in
	%			ResStruct
	%ColorResolution:	how many steps are used for the density
	%minColor:		sets the whitest used color [0 1]
	%maxDense:		sets the highest density used, can be set to 'unlimited'
	%DenseSpace:		Sets the minimum spacing between points, for the be neightbours, can be set to 'auto'  
	%LogMode:		If set to true a logarithmic scale is used for the color
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%			the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%ParallelMode:		If set to true parallel processing will be used
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%			'Data' will be used as LegendEntry.
	
	
	
	%use the last plot if not specified
	if nargin<2
    		plotAxis=gca;	
	end	
	
	SpaceMan=0.005;
	
	
	
	%Build some internal data settings
	wheelMarker={'.','+','o','x','diamond'};
	wheelMarker_nodot={'+','o','x','diamond'};
	wheelColor={'b','r','g','k','c','m'};
	
	
	
	%prepare data
	MarkerSize=10;
	ColorResolution=128;
	minColor=0.1;
	maxDense='unlimited';
	DenseSpace='auto';
	LogMode=false;
	ParallelMode=false;
	
	if isfield(ResStruct,'ParallelMode')
		if ~isempty(ResStruct.ParallelMode)
			ParallelMode=ResStruct.ParallelMode;
		end
	end
	
	
	if isfield(ResStruct,'ColorResolution')
		if ~isempty(ResStruct.ColorResolution)
			ColorResolution=ResStruct.ColorResolution;
		end
	end
	
	
	if isfield(ResStruct,'MarkerSize')
		if ~isempty(ResStruct.MarkerSize)
			MarkerSize=ResStruct.MarkerSize;
		end
	end
	
	if isfield(ResStruct,'minColor')
		if ~isempty(ResStruct.minColor)
			minColor=ResStruct.minColor;
		end
	end
	
	if isfield(ResStruct,'maxDense')
		if ~isempty(ResStruct.maxDense)
			maxDense=ResStruct.maxDense;
		end
	end
	
	if isfield(ResStruct,'DenseSpace')
		if ~isempty(ResStruct.DenseSpace)
			DenseSpace=ResStruct.DenseSpace;
		end
	end
	
	if isfield(ResStruct,'LogMode')
		if ~isempty(ResStruct.LogMode)
			LogMode=ResStruct.LogMode;
		end
	end
	
	%The sizes
	SizeArray=MarkerSize*ones(numel(ResStruct.Data(:,1)),1);	
	XX=ResStruct.Data(:,1);
	YY=ResStruct.Data(:,2);
	
	%determine spacing if needed
	if any(strcmp(DenseSpace,'auto'))
		Xmax=max(ResStruct.Data(:,1));
		Ymax=max(ResStruct.Data(:,2));
		Xmin=min(ResStruct.Data(:,1));
		Ymin=min(ResStruct.Data(:,2));
		
		clear DenseSpace;
		DenseSpace(1)=SpaceMan*abs(Xmax-Xmin);
		DenseSpace(2)=SpaceMan*abs(Ymax-Ymin);

		
	elseif any(~isstr(DenseSpace))&numel(DenseSpace)==1
		%1D spacing, double it	
		DenseSpace=DenseSpace*ones(2,1);
		
	end
	
	
	RawDense=ones(numel(XX),1);
	
	%now get the density
	if ParallelMode
		matlabpool open
		
		disp('calc Parallel')
		
		parfor i=1:numel(XX)
			EventsFound=(XX>=(XX(i)-DenseSpace(1))&...
    				XX<(XX(i)+DenseSpace(1)))&...
    				(YY>=(YY(i)-DenseSpace(2))&...
    				YY<(YY(i)+DenseSpace(2)));
    				RawDense(i)=sum(double(EventsFound));
    			
	
    		end
    		
    		matlabpool close
	
	else
	
		for i=1:numel(XX)
			EventsFound=(XX>=(XX(i)-DenseSpace(1))&...
    				XX<(XX(i)+DenseSpace(1)))&...
    				(YY>=(YY(i)-DenseSpace(2))&...
    				YY<(YY(i)+DenseSpace(2)));
    				RawDense(i)=sum(double(EventsFound));
    			
	
    		end
    	end
    	
	%set minimum to one
	RawDense(RawDense==0)=1;
	
	
	
	%limit top if needed
	if ~strcmp(maxDense,'unlimited')
		RawDense(RawDense>maxDense)=maxDense;	
	end
	
	if LogMode
		iszero=RawDense==0;
		RawDense=log10(RawDense);
		RawDense(iszero)=0;
	end
	
	%normit
	normfact=abs(max(RawDense)-min(RawDense));
	if normfact<=0
		normfact=1;
	end
	
	
	normdense=RawDense./normfact;
	
	
	
	%sort the normdense with the 
	[normdense,idx] = sort(normdense,'descend');
	
	%sort rest according to it
	XX=XX(idx);
	YY=YY(idx);
	SizeArray=SizeArray(idx);
	
	selNum=round(normdense*ColorResolution);
	selNum(selNum==0)=1;
	%disp(min(selNum))
	%disp(max(selNum))
	
	%just to be sure
	selNum(selNum>ColorResolution)=ColorResolution;
	
	%get minimum index
	minIndex=round(ColorResolution*minColor);
	if minIndex<=0;
		minIndex=1;
	end
	
	selNum(selNum<minIndex)=minIndex;
		
	
	%now get the colorscale
	%at the moment only gray or customcolor 
	TheColors=flipud(gray(ColorResolution));
	
	%Customization Color
	if isfield(ResStruct,'CustomColors')
		colFun=str2func(ResStruct.CustomColors);
		TheColors=colFun(ColorResolution);
			
	end	
	
	SelectedColor=TheColors(selNum,:);
	
	%plot the data
	handle = scatter(plotAxis,XX,YY,SizeArray,SelectedColor,'Marker','.');
	
	
	%now change the parameters of the plot
	%-------------------------------------
	
	%the labels: if not set, set to 'X' and 'Y'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Y';
			end
			
			hYLabel = ylabel(plotAxis,Y_axis_name);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
			
				
		
		
			
		
		
		if ~isfield(ResStruct,'MarkerStylePreset')
			ResStruct.MarkerStylePreset=[];
		end
		
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.MarkerStylePreset)			
			switch ResStruct.MarkerStylePreset
				case 'normal'
					set(handle,'Marker','.')
				case 'plus'
					set(handle,'Marker','+')
				case 'circle'
					set(handle,'Marker','o')
				case 'cross'
					set(handle,'Marker','x')
				case 'diamond'
					set(handle,'Marker','diamond')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1});
				case 'auto_nodot'
					%maybe usefull to avoid the small dot symbol
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker_nodot{mod(existPlots,4)+1});
			
			end
		end

		
		%Customization Marker
		if isfield(ResStruct,'CustomMarker')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Marker',ResStruct.CustomMarker);
		end	

		
		
		
		

		%set limits if needed
		if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_Axis_Limit);	
		end
		
		
		%build legend (if not empty it will just copy the entry from ResStruct)
		if ~isempty(ResStruct.LegendText)
			legendentry=ResStruct.LegendText;
		else	
			legendentry='Data';
		end
		
		
		
	
end
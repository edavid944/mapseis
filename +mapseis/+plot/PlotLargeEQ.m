function [handle legendentry] = PlotLargeEQ(plotAxis,datastore,selected,TheMode)
	import mapseis.projector.*;
	import mapseis.calc.CalcTimeDist;
	
	dx=0.1;
	dy=0.5;
	dt=0.5;
	
	if isempty(plotAxis)
		plotAxis=gca;
	end	
	
	if ~isempty(selected)
		selected=true(datastore.getRowCount,1);
	end
	
	%get data
	[selectedLonLat,unselectedLonLat] = getLocations(datastore,selected);
	[selectedMag,unselectedMag] = getMagnitudes(datastore,selected);
	[selectedYears,unselectedYears]= getDecYear(datastore,selected);
	
	%find maximum mag
	Themark=selectedMag>=max(selectedMag);
	
	MarkLonLat=selectedLonLat(Themark,:);
	MarkMag=selectedMag(Themark,:);
	MarkYear=selectedYears(Themark);
	
	if ~isempty(MarkLonLat)
		switch TheMode
			case 'map'
				handle = plot(plotAxis,MarkLonLat(:,1),MarkLonLat(:,2));
				set(handle, 'LineStyle','none', 'Marker', 'p', 'MarkerEdgeColor','k','MarkerFaceColor','y','MarkerSize',9);
				
				if ishold
					holdbefore=true;
				else
					hold on
					holdbefore=false;
				end	
				for i=1:numel(MarkMag)
					names=['M ',num2str(MarkMag(i))];
					te1 = text(MarkLonLat(i,1)+dx,MarkLonLat(i,2),names,'era','back','clipping','on');
							set(te1,'FontWeight','bold','Color','k','FontSize',[9]);
					
					
				end
				if ~holdbefore
					hold off
				end
			case 'time'
			
				
				
				
				if ishold
					holdbefore=true;
				else
					hold on
					holdbefore=false;
				end	
				
				for i=1:numel(MarkMag)
					names=['M ',num2str(MarkMag(i))];
					TimeDist=numel(selectedYears(selectedYears<=MarkYear(i)));
					
					
					handle(i) = plot(plotAxis,MarkYear(i),TimeDist);
					
					te1 = text(MarkYear(i)+dt,TimeDist-dy,names,'era','back','clipping','on');
							set(te1,'FontWeight','bold','Color','k','FontSize',[9]);
					
					
				end
				
				set(handle, 'LineStyle','none', 'Marker', 'p', 'MarkerEdgeColor','k','MarkerFaceColor','y','MarkerSize',9);
				if ~holdbefore
					hold off
				end
			
			
			
		end	
		
		legendentry='Largest Earthquakes';
		
	end
	
	
end

function  [handle legendentry] = PlotContour(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	
	%the colorplot allows to plot a data value spatially coded as color
	%possible config fields for this function:
	%Data:				Not optional, the data to plot, in this case the data has to be a bit
	%					different, needed is a cell array with 3 entries, 1st one is a vector or matrix
	%					x-coordinates, 2nd one is a vector or matrix with z-coordinates and the 3rd element					
	%					has matrix with the color information
	%MapStyle:			Defines the plotting style wanted: 'normal', for the normal contour plot, and 'filled'
	%					for a plot with filled contours.  
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'X',if the field is missing, nothing will be
	%					done
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y',if the field is missing, nothing will be
	%					done			
	%ContourLabel:		Adds labels to the contours, 'all' will add a lebel to every contour, 'auto' adds only 
	%					every n-step (specified under ContourOption) a label. 'custom' allows to use vector with positions
	%					of the contour labels.
	%ContourOption:		Needed for the ContourLabel = 'auto' or 'custom'
	%LineStylePreset:	This allows to change the style of the plotted line (if plotted)
	%					with simple presets if not not specified the default style ('normal') 
	%					will be used. The presets are: 'normal','dotted','slashed','Fatline'
	%					'Fatdot','Fatslash and the two automatic settings which try use different
	%					LineStyle every plot: 'automatic' and 'Fatauto' for wider lines.
	%CustomLine:	 	This allows to specify the line properties freely, CustomLine{1} defines
	%					LineStyle and CustomLine{2} defines the LineWidth. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomLine should be missing in
	%					ResStruct	
	%Colors:			This defines either the ColorMap or the Color if only 3 coloumbs are defined
	%					Option for the ColorMap are: 'jet', 'hsv', 'hot', 'gray', 'cool', 'bone', 'autumn', 'summer'	
	%CustomColors:		This allows to specify the ColorMap properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColorMap should be missing in
	%					ResStruct
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%C_Axis_Limit:		Limits the Range of the Colorbar, syntax, the same as X_Axis_Limit
	%ColorToggle:		Set to true will show the ColorBar
	%LegendText:		Write anything in it, it will not be send, Colormap do not support Legend entries
	
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	

	
	
	
	%Prepare data
	%--------------
	%Both countour plots do not care if x andy are vectors or matrixes 	
	%default
	x_coords=ResStruct.Data{1};
	y_coords=ResStruct.Data{2};
	DataMatrix=ResStruct.Data{3};
	
	
	
	%plot the data
	%--------------
	if isempty(ResStruct.MapStyle) | strcmp(ResStruct.MapStyle,'normal')
		[ContLevel handle] = contour(plotAxis,x_coord,y_coord,DataMatrix);
	
	else
		[ContLevel handle] = contourf(plotAxis,x_coord,y_coord,DataMatrix);

	end
		
		
	%now change the parameters of the plot
	%-------------------------------------
	
		%the labels: if not set, set to 'X' and 'Y'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Y';
			end
			
			hYLabel = ylabel(plotAxis,Y_axisname);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
		

		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.LineStylePreset)
			switch ResStruct.LineStylePreset
				case 'normal'
					set(handle,'LineStyle','-','LineWidth',1);
				case 'dotted'
					set(handle,'LineStyle',':','LineWidth',1);
				case 'slashed'
					set(handle,'LineStyle','--','LineWidth',1);
				case 'Fatline'
					set(handle,'LineStyle','-','LineWidth',1.5);
				case 'Fatdot'
					set(handle,'LineStyle',':','LineWidth',1.5);
				case 'Fatslash'
					set(handle,'LineStyle','--','LineWidth',1.5);
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','hggroup'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1);
				case 'Fatautomatic'
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','hggroup'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1.5);
			
			end
		end
		
		
		
		%Customization Line
		if isfield(ResStruct,'CustomLine')
			%This option allows to customize the line with the common matlab commands
			set(handle,'LineStyle',ResStruct.CustomLine{1},'LineWidth',ResStruct.CustomLine{2});
		end	
		

		
		%Add contour labels if wanted
		if isfield(ResStruct,'ContourLabel') 	
			if ~isempty(ResStruct.ContourLabel)
				switch ResStruct.ContourLabel
					case 'all'
						clabel(ContLevel,handle)		
					
					case 'custom'
						clabel(ContLevel,handle,ResStruct.ContourOption);

					case 'auto'
						minData=min(min(DataMatrix));
						maxData=max(max(DataMatrix));
						ContVector=minData:ResStruct.ContourOption:maxData;
						clabel(ContLevel,handle,ContVector);
				
				end
			end
		end	

		

		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
				switch ResStruct.Colors
					case 'jet'
						colormap(plotAxis,jet);
					case 'hsv'
						colormap(plotAxis,hsv);
					case 'hot'
						colormap(plotAxis,hot);
					case 'cool'
						colormap(plotAxis,cool);
					case 'gray'
						colormap(plotAxis,gray);
					case 'bone'
						colormap(plotAxis,bone);
					case 'summer'
						colormap(plotAxis,summer);
					case 'autumn'
						colormap(plotAxis,autumn);
								
				end
				

		end


		%Customization Color
		if isfield(ResStruct,'CustomColors')
				colormap(plotAxis,ResStruct.CustomColors);
		end	




		%set limits if needed
		if ~isempty(ResStruct.X_AxisLimit) | strcmp(ResStruct.X_AxisLimit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_AxisLimit);	
		end
		
		if ~isempty(ResStruct.Y_AxisLimit) | strcmp(ResStruct.Y_AxisLimit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_AxisLimit);	
		end
		
		if ~isempty(ResStruct.C_AxisLimit) | strcmp(ResStruct.C_AxisLimit,'auto')
			caxis(plotAxis,'manual');
			caxis(plotAxis,ResStruct.C_AxisLimit);	
		end

		
		if ~isempty(ResStruct.ColorToggle) & ResStruct.ColorToggle
			colorbar;
		end
		
		
		
		if isfield(ResStruct,'LegendText');	
			if ~isempty(ResStruct.LegendText)
				legendentry=[];
			else	
				legendentry=[];
			end
		else
		legendentry=[];
					
		end		

	
end
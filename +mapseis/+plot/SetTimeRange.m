function SetTimeRange(evStruct,rangeType)
% SetTimeRange : set the time range of interest

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

import mapseis.util.*;

if nargin<2
    rangeType = 'in';
end

try
    switch rangeType
        case {'after','before'}
            [x,y] = Ginput(1); %#ok<NASGU>
            evStruct.setTimeRange(rangeType,x);
        case 'in'
            [x,y] = Ginput(2); %#ok<NASGU>
            evStruct.setTimeRange(rangeType,x);
        case 'all'
            eStruct = evStruct.getData();
            evStruct.setTimeRange('after',min(eStruct.dateNum));
        otherwise
            error('SetTimeRange:unknown_range_type','Unknown range type');
    end
catch ME
    warning('SetTimeRange:range_not_set','Unable to set time range');
end


end
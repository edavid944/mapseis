function  [handle legendentry] = PlotSlice3D(plotAxis,ResStruct)
	%This function creates slices through a volume at the entered values
	
	%possible config fields for this function:
	%similar to generic 2D but *d
	%Data:			Not optional, the data to plot Data{1} for the x-values
	%			Data{2} for the y-values and Data{3} for z-values and Data{4}
	%			for the the value (Matrix(x,y,z)	
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%				will be set to 'X', if the field is missing, nothing will be
	%				done	
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y', if the field is missing, nothing will be
	%					done			
	%Z_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y', if the field is missing, nothing will be
	%					done			
	%ZSlice:		Sets the horizontal Slices: [minVal maxVal Spacing]
	%UseCustom_Z:		If set to true the custom horizontal slices will be used
	%CustomZSlice:		Allows to set vertical positions at which horizontal slices are plotted, just an array with values.
	%XSlice:		Sets the vertical Slices: [minVal maxVal Spacing]
	%UseCustom_X:		If set to true the custom vertical slices will be used
	%CustomXSlice:		Allows to set horizontal positions at which vertical slices are plotted, just an array with values.
	%YSlice:		Sets the vertical Slices: [minVal maxVal Spacing]
	%UseCustom_Y:		If set to true the custom vertical slices will be used
	%CustomYSlice:		Allows to set horizontal positions at which vertical slices are plotted, just an array with values.
	%Use3DSlice:		If set to true the free defineable slices will be used, no vertical or horizontal slices will be used.
	%Custom3DSlice:		Is a cell(3 rows (x,y,z)) array which contains the slice info every cell will be used in a slice command
	%AlphaValue:		Sets the overall transparnce of the slices
 	%Colors:		This defines either the ColorMap or the Color if only 3 coloumbs are defined
	%			Option for the ColorMap are: 'jet', 'hsv', 'hot', 'gray', 'cool', 'bone', 'autumn', 'summer'	
	%CustomColors:		This allows to specify the ColorMap properties freely, CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomColorMap should be missing in
	%			ResStruct
	%ShadeMode:		'faceted','flat' or 'interp'
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%Z_Axis_Limit: 		Same for the Z-Axis
	%C_Axis_Limit:		Same for the Color axis
	%ColorToggle:		Draws colorbar
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%					'Data' will be used as LegendEntry.
	
	%more option
	
	
	%use the last plot if not specified
	if nargin<2
    		plotAxis=gca;	
	end	
	
	AlphaVal=1;
	if isfield(ResStruct,'AlphaValue')	
		if ~isempty(ResStruct.AlphaValue)
			AlphaVal=ResStruct.AlphaValue;
		end
	end
	
	
	ShadeMode='flat';
	if isfield(ResStruct,'ShadeMode')	
		if ~isempty(ResStruct.ShadeMode)
			ShadeMode=ResStruct.ShadeMode;
		end
	end
	
	MrData=ResStruct.Data;
	
	
	%prepare slices
	if ~ResStruct.Use3DSlice
		if ResStruct.UseCustom_Z
			ZSlices=ResStruct.CustomZSlice;
		else
			if ~isempty(ResStruct.ZSlice)
				ZSlices=ResStruct.ZSlice(1):ResStruct.ZSlice(3):ResStruct.ZSlice(2);
			else
				ZSlices=[];
			end	
			
		end
		
		if ResStruct.UseCustom_X
			XSlices=ResStruct.CustomXSlice;
		else
			if ~isempty(ResStruct.XSlice)
				XSlices=ResStruct.XSlice(1):ResStruct.XSlice(3):ResStruct.XSlice(2);
			else
				XSlices=[];
			end	
		end
		
		if ResStruct.UseCustom_Y
			YSlices=ResStruct.CustomYSlice;
		else
			if ~isempty(ResStruct.YSlice)
				YSlices=ResStruct.YSlice(1):ResStruct.YSlice(3):ResStruct.YSlice(2);
			else
				YSlices=[];
			end	
		end
		
	
		handle=slice(plotAxis,MrData{1},MrData{2},MrData{3},MrData{4},XSlices,YSlices,ZSlices);
		
		
	else
		CustomSlice=ResStruct.Custom3DSlice;
		
		for i=1:numel(CustomSlice(:,1))
			handle(i)=slice(plotAxis,MrData{1},MrData{2},MrData{3},MrData{4},...
					CustomSlice{i,1},CustomSlice{i,2},CustomSlice{i,3});	
		
		
		end
	
	
	
	end
	

	%set alpha
	axes(plotAxis);
	alpha(AlphaVal);
	
	%set shading
	shading(plotAxis,ShadeMode); 
	
	
	%now change the parameters of the plot
	%-------------------------------------
	
	%the labels: if not set, set to 'X' and 'Y'
	if isfield(ResStruct,'X_Axis_Label')
		X_axis_name=ResStruct.X_Axis_Label;
			
		if isempty(X_axis_name)
			X_axis_name='X';
		end
		
		hXLabel = xlabel(plotAxis,X_axis_name);
		set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
	end
		
	
	if isfield(ResStruct,'Y_Axis_Label')
		Y_axis_name=ResStruct.Y_Axis_Label; 	
		
		if isempty(Y_axis_name)
			Y_axis_name='Y';
		end
		
		hYLabel = ylabel(plotAxis,Y_axis_name);
		set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
		
	if isfield(ResStruct,'Z_Axis_Label')
		Z_axis_name=ResStruct.Z_Axis_Label; 	
		
		if isempty(Z_axis_name)
			Z_axis_name='Y';
		end
		
		hZLabel = zlabel(plotAxis,Z_axis_name);
		set(hZLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
	
	
	%change the color to the preset if needed
	if ~isempty(ResStruct.Colors) 
			switch ResStruct.Colors
				case 'jet'
					colormap(plotAxis,jet);
				case 'hsv'
					colormap(plotAxis,hsv);
				case 'hot'
					colormap(plotAxis,hot);
				case 'cool'
					colormap(plotAxis,cool);
				case 'gray'
					colormap(plotAxis,gray);
				case 'bone'
					colormap(plotAxis,bone);
				case 'summer'
					colormap(plotAxis,summer);
				case 'autumn'
					colormap(plotAxis,autumn);
								
			end
				
	end


	%Customization Color
	if isfield(ResStruct,'CustomColors')
			colormap(plotAxis,ResStruct.CustomColors);
	end	

	set(handle,'CDataMapping','scaled');

		

	%set limits if needed
	if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
		xlim(plotAxis,'manual');
		xlim(plotAxis,ResStruct.X_Axis_Limit);	
	end
		
	if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
		ylim(plotAxis,'manual');
		ylim(plotAxis,ResStruct.Y_Axis_Limit);	
	end
	
	if ~isempty(ResStruct.Z_Axis_Limit) | strcmp(ResStruct.Z_Axis_Limit,'auto')
		zlim(plotAxis,'manual');
		zlim(plotAxis,ResStruct.Z_Axis_Limit);	
	end

	if ~isempty(ResStruct.C_Axis_Limit) | strcmp(ResStruct.C_Axis_Limit,'auto')
		caxis(plotAxis,'manual');
		caxis(plotAxis,ResStruct.C_Axis_Limit);	
	end

		
	if ~isempty(ResStruct.ColorToggle) & ResStruct.ColorToggle
		colorbar;
	end
		
	%build legend (if not empty it will just copy the entry from ResStruct)
	if ~isempty(ResStruct.LegendText)
		legendentry=ResStruct.LegendText;
	else	
		legendentry='Data';
	end
		
		
		
	
end

function [DenseData,refCDF,countCDF] = EqualDenseData(RefData,ActualData,StepSize,minVal,maxVal)

	%This function is based on the program eqldens by D. C. Agnew, 2009.
	%It arranges the point in ActualData in such a way that the points
	%have a equal density according to the empirical cdf built with the 
	%RefData. 
	
	%DE 2013
	
	%The original algorithm does work like this:
	% First you get the reference data into the program
	% together with some minimum and maximum (both optional)
	% as well as step-size used later (for dithering, but it can be zero)
	% next the refernce values are sorted normalized to one. Also each 
	% element gets number equals its position in the array -0.5 divide by 
	% the total number of elements. It is a bit similar to a cumulative 
	% number, but not really.
	% After that the array is search for identical values and the array is 
	% corrected by setting a single number (the assigned number) in the 
	% middle of the identical values. This seems to be a bit like 
	% a normalized histogram (actually a CDF).
	% This is then used to estimate the derivatives for the piecewiese 
	% cubic hermite interpolation (a polynomial spline interpolation) and 
	% with this derivative the dithered/not dithered data is assigned.
	% This is done by normalizing the values with same parameters as the 
	% reference and them send them to the cubic hermite interpolation
	% algorithm together with the derivatives from the reference.
	% If a dithering is needed, the the "border-values" of each values 
	% (val-sep/2 and val+sep/2) are calculated and both values are then 
	% treated like normal non-dithered values. Afterwards a random number 
	% is draw and a random amount of the difference between the two values
	% is added to the lower value of the two.    

	%This version will follow mostly the sketched algorithm above, but with 
	%few change:
	%	- Use of pchip (matlab) which does the derivative estimation and
	%	  interpolation in one step.
	%	- Support for 2D to 3D data.
	%	- Output of ref. CDF and maybe also something for transforming
	%	  the labels of a plot
	
	%Input:
	%	RefData: Data used as reference, has to be a single vector with 
	%		 either one column or row. The reference should be a 
	%		 good represenation of the actual data, for instance
	%		 a background seismicity set.
	%	ActualData: The data which should be modified. I can be a single
	%		    vector or a 2-3 row/column vector, the last vector
	%		    will be treated as the data to be modified.
	%	StepSize: Used for the dithering (addition of random noise, so 
	%		  that the values are uniformly distributed inside there
	%		  acuracy. It can be set to 0, in which case no 
	%		  dithering is applied on the data. Example with 
	%		  magnitudes 3.4 , 3.5, 4.6 ... a dithering of 0.1 
	%		  should be used.
	%	minVal: if not left empty this value will be added to the 
	%		reference data, allowing to expand the data, which is  
	%		needed because the CDF has to have larger and smaller
	%		values then the data which should be modified
	%	maxVal:	if not left empty this value will be added to the 
	%		reference data, allowing to expand the data, which is  
	%		needed because the CDF has to have larger and smaller
	%		values then the data which should be modified
	%It is suggested to use a row vector whenever possible, because in the
	%case of one Data point consisting of 2 or 3 coordinates a columm vector
	%will be transposed.
	

	%correct for not sent parameters
	if nargin<5
		maxVal=[];
	end
	
	if nargin<4
		minVal=[];
	end
	
	%decide if dithering is needed
	doDither=StepSize~=0;
	
	
	%Add min and max if specified
	if ~isempty(minVal)
		RefData(end+1)=minVal;
	end
	
	if ~isempty(maxVal)
		RefData(end+1)=maxVal;
	end
	
	
	%sort data and normalize to 1
	SortRef=sort(RefData);
	botRef=SortRef(1);
	topRef=SortRef(end);
	numRefPoint=numel(SortRef);
	
	%generate cdf values running from 1/2n to 1-(1/2n) (n=number of points);
	normFactor=1/(topRef-botRef);
	SortRef=normFactor.*(SortRef-botRef);
	posVec=((1:numRefPoint)-0.5)./numRefPoint;
	
	%search normalized CDF for duplicates, remove them and adjust CDF level
	%(posVec)
	numPoint=1;
	oldVal=-1;
	for i=1:numRefPoint
		if (SortRef(i)~=oldVal) 
			posVec(numPoint)=posVec(i);
		else
			numPoint=numPoint-1;
			posVec(numPoint) = posVec(numPoint)+(0.5/numRefPoint);
		end	
		SortRef(numPoint)=SortRef(i);
		numPoint=numPoint+1;
		oldVal=SortRef(i);
	
	end

	
	numRefPoint=numPoint-1;
	refCDF=SortRef(1:numRefPoint);
	countCDF=posVec(1:numRefPoint);
	
	%correct if needed dimension of the reference vector
	if all(size(refCDF)==1)|all(size(countCDF)==1)
		%this hopefully never happens
		error('Resulting CDF is to small');
	end
	
	if numel(refCDF(1,:))>1
		refCDF=refCDF';
	end
	
	if numel(countCDF(1,:))>1
		countCDF=countCDF';
	end
	
	
	%determine format of the input and correct if  necessary
	InputDim=size(ActualData);
	
	Shifted=false;
	if InputDim(2)~=1
		if InputDim(1)<=3
			ActualData=ActualData';
			Shifted=true;
		end
	
	end
	
	%Extract values to modify
	if numel(ActualData(1,:))==1
		ToNorm=ActualData;
		CoordVec=[];
	elseif numel(ActualData(1,:))==2
		ToNorm=ActualData(:,2);
		CoordVec=ActualData(:,1);
	
	elseif numel(ActualData(1,:))==3
		ToNorm=ActualData(:,3);
		CoordVec=ActualData(:,1:2);
	else
		error('something wrong with the data');
	end

	
	%norm the data and used the reference
	if doDither
		LowData = normFactor.*(ToNorm-StepSize/2-botRef);
		HiData = normFactor.*(ToNorm+StepSize/2-botRef);
		
		%interpolation
		LowDenseData = pchip(refCDF,countCDF,LowData);
		HiDenseData = pchip(refCDF,countCDF,HiData);
		
		MixIt = rand(size(LowData));
		RawData = LowDenseData+MixIt.*(HiDenseData-LowDenseData);
		
	else
		NormedData = normFactor.*(ToNorm-botRef);
        	
		%interpolation
		RawData = pchip(refCDF,countCDF,NormedData);
	
        end

	
	
	if ~isempty(CoordVec);
       		DenseData=[CoordVec,RawData];
	else
		DenseData=RawData;
	end
	
	if Shifted
		DenseData=DenseData';
	end
	
	
	
                                             
end

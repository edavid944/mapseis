function  [handle legendentry] = PlotPlateBoundaries(plotAxis,ResStruct)	
	%This function belongs to the new earthquake plot function package
	%and plots only the the plate boundaries
	
	%Ridge:				If set to true the Ridges will be plotted
	%Transform:			If set to true the Transforms will be plotted
	%Trench:			If set to true the Trench will be plotted
	%X_Axis_Label:			The label under the X-Axis, if empty (not specified) it 
	%				will be set to 'Longitude'
	%Y_Axis_Label:			The label under the Y-Axis, if empty (not specified) it 
	%				will be set to 'Latitude'
	%RidgeLineStylePreset:		This allows to select different style of lines: available are:
	%				'normal','Fineline', 'Fatline' and 'dotted'
	%TransformLineStylePreset:	This allows to select different style of lines: available are:
	%				'normal','Fineline', 'Fatline' and 'dotted'
	%TrenchLineStylePreset:		This allows to select different style of lines: available are:
	%				'normal','Fineline', 'Fatline' and 'dotted'
	%RidgeColors:			With this entry the color of the line can be selected, possible are:
	%				'blue','red','green','black','cyan','magenta', default: 'red'
	%TransformColors:		With this entry the color of the line can be selected, possible are:
	%				'blue','red','green','black','cyan','magenta', default: 'green'
	%TrenchColors:			With this entry the color of the line can be selected, possible are:
	%				'blue','red','green','black','cyan','magenta', default: 'blue'
	
	
	
	
	%use the last plot if not specified	
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	%load data from Addon directory
	try
		PlateData=load('./AddOneFiles/plateboundaries/plateBoundaries.mat');
	catch
		disp('plateBoundaries.mat does not exist');
		return
	end
	
	
	MrHolden=ishold(plotAxis);
	
	handle={};
	legendentry={};
	
	%now plot it (ask questions later)
	if ResStruct.Ridge
		handle{end+1} = plot(plotAxis,PlateData.fullRidge(:,1),PlateData.fullRidge(:,2));
		
		
		%change to default linewitdth of 1
		set(handle{end},'LineStyle','-','LineWidth',2,'Color','r');
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.RidgeLineStylePreset) 
			switch ResStruct.RidgeLineStylePreset
				case 'normal'
					set(handle{end},'LineStyle','-','LineWidth',2);
				case 'Fineline'
					set(handle{end},'LineStyle','-','LineWidth',1);	
				case 'dotted'
					set(handle{end},'LineStyle',':','LineWidth',1);
				case 'Fatline'
					set(handle{end},'LineStyle','-','LineWidth',3);
	
			
			end
		end
	
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.RidgeColors) 
			
			switch ResStruct.RidgeColors
				case 'blue'
					set(handle{end},'Color','b')
				case 'red'
					set(handle{end},'Color','r')
				case 'green'
					set(handle{end},'Color','g')
				case 'black'
					set(handle{end},'Color','k')
				case 'cyan'
					set(handle{end},'Color','c')
				case 'magenta'
					set(handle{end},'Color','m')
	
			end
		end

		legendentry{end+1}='Plate Ridges';
	
	end
	
	
	%now plot it (ask questions later)
	if ResStruct.Transform
		hold on
		handle{end+1} = plot(plotAxis,PlateData.fullTransform(:,1),PlateData.fullTransform(:,2));
		
		
		%change to default linewitdth of 1
		set(handle{end},'LineStyle','-','LineWidth',2,'Color','g');
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.TransformLineStylePreset) 
			switch ResStruct.TransformLineStylePreset
				case 'normal'
					set(handle{end},'LineStyle','-','LineWidth',2);
				case 'Fineline'
					set(handle{end},'LineStyle','-','LineWidth',1);	
				case 'dotted'
					set(handle{end},'LineStyle',':','LineWidth',1);
				case 'Fatline'
					set(handle{end},'LineStyle','-','LineWidth',3);
	
			
			end
		end
	
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.TransformColors) 
			
			switch ResStruct.TransformColors
				case 'blue'
					set(handle{end},'Color','b')
				case 'red'
					set(handle{end},'Color','r')
				case 'green'
					set(handle{end},'Color','g')
				case 'black'
					set(handle{end},'Color','k')
				case 'cyan'
					set(handle{end},'Color','c')
				case 'magenta'
					set(handle{end},'Color','m')
	
			end
		end

		legendentry{end+1}='Plate Transforms';
	
	end
	
	
	%now plot it (ask questions later)
	if ResStruct.Trench
		hold on
		handle{end+1} = plot(plotAxis,PlateData.fullTrench(:,1),PlateData.fullTrench(:,2));
		
		
		%change to default linewitdth of 1
		set(handle{end},'LineStyle','-','LineWidth',2,'Color','b');
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.TrenchLineStylePreset) 
			switch ResStruct.TrenchLineStylePreset
				case 'normal'
					set(handle{end},'LineStyle','-','LineWidth',2);
				case 'Fineline'
					set(handle{end},'LineStyle','-','LineWidth',1);	
				case 'dotted'
					set(handle{end},'LineStyle',':','LineWidth',1);
				case 'Fatline'
					set(handle{end},'LineStyle','-','LineWidth',3);
	
			
			end
		end
	
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.TrenchColors) 
			
			switch ResStruct.TrenchColors
				case 'blue'
					set(handle{end},'Color','b')
				case 'red'
					set(handle{end},'Color','r')
				case 'green'
					set(handle{end},'Color','g')
				case 'black'
					set(handle{end},'Color','k')
				case 'cyan'
					set(handle{end},'Color','c')
				case 'magenta'
					set(handle{end},'Color','m')
	
			end
		end

		legendentry{end+1}='Plate Trenches';
	
	end
	
	
	%now change plot style
	%---------------------
	
		
	%set the axis labels (should not be needed but it is a good idea anyway
	if isfield(ResStruct,'X_Axis_Label')
		X_axis_name=ResStruct.X_Axis_Label;
		
		if isempty(X_axis_name)
			X_axis_name='Longitude';
		end
		
		hXLabel = xlabel(plotAxis,X_axis_name);
		set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
	end
	
	
	if isfield(ResStruct,'Y_Axis_Label')
		Y_axis_name=ResStruct.Y_Axis_Label; 	
		
		if isempty(Y_axis_name)
			Y_axis_name='Latitude';
		end
		
		hYLabel = ylabel(plotAxis,Y_axis_name);
		set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
	
	
	if ~MrHolden
		hold off
	end	
	
			
end

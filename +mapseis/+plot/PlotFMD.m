function PlotFMD(plotAxis,evMags)
% PlotFMD : Plot histogram of event magnitudes

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Stefan Wiemer
% Last-Modified: Matt McDonnell

if nargin<2
    plotAxis=gca;
end

% bvalsum is the cum. sum in each bin
% bval2 is number events in each bin, in reverse order
% bvalsum3 is reverse order cum. sum.
% xt3 is the step in magnitude for the bins == .1
%
%%

if ~isempty(evMags)

		maxmag = ceil(10*max(evMags))/10;
		mima = min(evMags);
		if mima > 0 ; mima = 0 ; end
		
		% number of mag units
		nmagu = (maxmag*10)+1;
		
		bval = zeros(1,nmagu);
		bvalsum = zeros(1,nmagu);
		bvalsum3 = zeros(1,nmagu);
		
		[bval,xt2] = hist(evMags,(mima:0.1:maxmag));
		bvalsum = cumsum(bval); % N for M <=
		bval2 = bval(length(bval):-1:1);
		bvalsum3 = cumsum(bval(length(bval):-1:1));    % N for M >= (counted backwards)
		xt3 = (maxmag:-0.1:mima);
		
		backg_ab = log10(bvalsum3);
		
		
		
		%%
		% plot the cum. sum in each bin  %%
		%%
		
		pl = semilogy(plotAxis,xt3,bvalsum3,'sb');
		set(pl,'LineWidth',[1.0],'MarkerSize',[6],...
		    'MarkerFaceColor','g','MarkerEdgeColor','k','tag','FMD_line');
		set(plotAxis,'TickDir','out');
		
		%%
		% CALCULATE the diff in cum sum from the previous biin
		%%
		
		
		xlabel(plotAxis,'Magnitude','FontWeight','normal','FontSize',12)
		ylabel(plotAxis,'Cumulative Number','FontWeight','normal','FontSize',12)
		title(plotAxis,'Cumulative FMD')

else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
		
end


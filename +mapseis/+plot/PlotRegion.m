function PlotRegion(plotAxis,eventStruct,mags)
% PlotRegion : Plot event locations
% Plots the location of events.  If passed a matrix of longitudes and
% latitudes then all events are plotted.  If passed a struct with selected,
% unselected and boundary fields then the unselected events are plotted in
% a different colour.

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 79 $    $Date: 2008-11-06 10:09:59 +0000 (Thu, 06 Nov 2008) $
% Author: Matt McDonnell

if nargin<2
    plotAxis=gca;
end

if nargin<3
    mags=[];
end
disp('The region plot')

%simple plot function
if isnumeric(eventStruct)
    % If given a magnitude vector then partition the events based on magnitude
    % and plot with different symbols
    %at the moment set default to low quality
    magconfig = LowQualConfig;
    
    [plotArgCell,nonEmptyInRegionIndices]=nMakePlotArgs(...
        eventStruct(:,1),eventStruct(:,2),mags.selected,magconfig);
    lHnds = nPlotEvents(plotAxis,plotArgCell);
    inRegHnds = lHnds;
    nSetPlotProperties_hq(inRegHnds,nonEmptyInRegionIndices,'in');

%--------------------------------------------------------------------------------------

elseif isstruct(eventStruct)
    % Define a name for event location as [lat lon] array
   	
    	override=false;
    	%check for override switch
    	if isfield(eventStruct,'OverRide')
    		if ~isempty(eventStruct.OverRide)
    			override=eventStruct.OverRide;
    		end
    	end	
    	
   	
   	%create configuration of the magnitude
   	qualityselector=eventStruct.Quality;
   	switch qualityselector
   		case 'low'
   			    magconfig = LowQualConfig;
   		case 'med'
			    magconfig = MedQualConfig;
		case 'hi'
   				magconfig = HiQualConfig;
   		case 'lowcomp'
   				magconfig = LowCompareConfig;
 		case 'hicomp'
   				magconfig = HiCompareConfig;
		
   		end		
 	
 	%Use the 'Type' Field as plot selector
 	PlotType=eventStruct.PlotType;
 	  
    %the old case with polygon and everything
    switch PlotType	 
	   case '2Dpolygon'
		    inReg = eventStruct.inRegion;
		    outReg = eventStruct.outRegion;
		    rBdry = eventStruct.boundary;

		   		 %only in profiles
		    	% if isfield(eventStruct,'FilterRange') & eventStruct.FilterRange == 'line'
		    	%	bdryArgs = {rBdry(:,1),rBdry(:,2),'ko-'};
		    	% end
		    
		    [outRegArgs,nonEmptyOutRegionIndices] = nMakePlotArgs(...
		        outReg(:,1),outReg(:,2),mags.unselected,magconfig);
		    [inRegArgs,nonEmptyInRegionIndices] = nMakePlotArgs(...
		        inReg(:,1),inReg(:,2),mags.selected,magconfig);
		    
		    	%only in profiles
		    	%if (isfield(eventStruct,'FilterRange') && strcmp(eventStruct.FilterRange,'line'))
		    	%	bdryArgs = {rBdry(:,1),rBdry(:,2),'ko-'};
		    	%	plotArgs = {outRegArgs{:},...
		        %				inRegArgs{:},bdryArgs{:}};
		        %	sliceplot = true;			
		        	
		    	%else
		     		plotArgs = {outRegArgs{:},...
		        				inRegArgs{:}};
		        	sliceplot = false;			
		    	%end
		  
		    if isfield(eventStruct,'grid')
		        gPts = eventStruct.grid;
		        gridArgs = {gPts(:,1),gPts(:,2),'+','color',[0.6 0.6 0.6]};
		        plotArgs = {plotArgs{:},gridArgs{:}};
		    end
		    
		    
		    try
		    	if eventStruct.plotCoastline
		        % Only plot the coastline for the main plot axis, as the change to
		        % pcolor mode in the calculation grid plot crashes otherwise.
		        % TODO : see why this is.
		        	Coast_PlotArgs = eventStruct.dataStore.getUserData('Coast_PlotArgs');
		        	if ~isempty(Coast_PlotArgs)
		            plotArgs = {plotArgs{:}, Coast_PlotArgs{:}};
		        	end
		        end	
		    end    
		       
		     try
		        if eventStruct.plotBorder
		        	InternalBorder_PlotArgs = eventStruct.dataStore.getUserData('InternalBorder_PlotArgs');
		        	if ~isempty(InternalBorder_PlotArgs)
		            	plotArgs = {plotArgs{:}, InternalBorder_PlotArgs{:}};
		        	end
		        end
		        
		        
		    end
		    
		    %% Plot the results
		    % Plot the points inside the region of interest, outside the region of
		    % interest and the boundary
		    % Keep the handles of the data lines for setting symbol shapes and
		    % other properties
		    lHnds = nPlotEvents(plotAxis,plotArgs);
		    % Count of number of data lines in the region and outside the region
		    dataInRegionCount = numel(nonEmptyInRegionIndices);
		    dataOutRegionCount = numel(nonEmptyOutRegionIndices);
		    
		    % Handles to the lines for events outside the region of interest
		    outRegHnds = lHnds(1:dataOutRegionCount);
		    % Handles to the lines for events inside the region of interest
		    inRegHnds = lHnds((1:dataInRegionCount)+dataOutRegionCount);
		    % Handles to the line for boundary points
		    boundaryHnd = lHnds(1+dataInRegionCount+dataOutRegionCount);
		    nSetBoundaryProperties(boundaryHnd);
		    nSetPlotProperties(inRegHnds,nonEmptyInRegionIndices,'in',magconfig);
		    nSetPlotProperties(outRegHnds,nonEmptyOutRegionIndices,'out',magconfig);
		    nSetAxisLimits(plotAxis,[inReg;outReg]);
		    
		    %build legend
		    BuildLegend(plotAxis,magconfig,true,sliceplot,mags,eventStruct.plotCoastline,eventStruct.plotBorder);
		  	try
		    	if eventStruct.showlegend
		    		legend(plotAxis,'show');
		    	end
			end

%--------------------------------------------------------------------------------------
		case '2Dwhole'
			inReg = eventStruct.inRegion;
			 [inRegArgs,nonEmptyInRegionIndices] = nMakePlotArgs(...
		        inReg(:,1),inReg(:,2),mags.selected, magconfig);
		    
		   	 plotArgs = {inRegArgs{:}};
			
			if isfield(eventStruct,'grid')
		        gPts = eventStruct.grid;
		        gridArgs = {gPts(:,1),gPts(:,2),'+','color',[0.6 0.6 0.6]};
		        plotArgs = {plotArgs{:},gridArgs{:}};
		    end


			try
		    	if eventStruct.plotCoastline
		        % Only plot the coastline for the main plot axis, as the change to
		        % pcolor mode in the calculation grid plot crashes otherwise.
		        % TODO : see why this is.
		        	Coast_PlotArgs = eventStruct.dataStore.getUserData('Coast_PlotArgs');
		        	if ~isempty(Coast_PlotArgs)
		            plotArgs = {plotArgs{:}, Coast_PlotArgs{:}};
		        	end
		        end	
		    end    
		       
		     try
		        if eventStruct.plotBorder
		        	InternalBorder_PlotArgs = eventStruct.dataStore.getUserData('InternalBorder_PlotArgs');
		        	if ~isempty(InternalBorder_PlotArgs)
		            	plotArgs = {plotArgs{:}, InternalBorder_PlotArgs{:}};
		        	end
		        end
		        
		        
		    end

			 %% Plot the results
		    % Plot the points inside the region of interest, outside the region of
		    % interest and the boundary
		    % Keep the handles of the data lines for setting symbol shapes and
		    % other properties
		    lHnds = nPlotEvents(plotAxis,plotArgs);
			
			% Count of number of data lines in the region and outside the region
			dataInRegionCount = numel(nonEmptyInRegionIndices);
			
			% Handles to the lines for events inside the region of interest
		    inRegHnds = lHnds((1:dataInRegionCount));
		    % Handles to the line for boundary points
		    nSetPlotProperties(inRegHnds,nonEmptyInRegionIndices,'in',magconfig);
		    nSetAxisLimits(plotAxis,[inReg]);
		    
		    %build legend
		    BuildLegend(plotAxis,magconfig,false,false,mags,eventStruct.plotCoastline,eventStruct.plotBorder);
		    
		    try
		    	if eventStruct.showlegend
		    		legend(plotAxis,'show');
		    	end
			end

		
%----------------------------------------------------------------------------------------
		%Depth plot		
		case 'DepthSlice'
			
			inReg = [eventStruct.inRegion,eventStruct.Depths];
			 [inRegArgs,nonEmptyInRegionIndices] = nMakePlotArgs(...
		        inReg(:,1),inReg(:,2),mags.selected, magconfig);
		    
		   	 plotArgs = {inRegArgs{:}};
			
			if isfield(eventStruct,'grid')
		        gPts = eventStruct.grid;
		        gridArgs = {gPts(:,1),gPts(:,2),'+','color',[0.6 0.6 0.6]};
		        plotArgs = {plotArgs{:},gridArgs{:}};
		    end


			 %% Plot the results
		    % Plot the points inside the region of interest, outside the region of
		    % interest and the boundary
		    % Keep the handles of the data lines for setting symbol shapes and
		    % other properties
		    lHnds = nPlotEventsSlice(plotAxis,plotArgs);
			
			% Count of number of data lines in the region and outside the region
			dataInRegionCount = numel(nonEmptyInRegionIndices);
			
			% Handles to the lines for events inside the region of interest
		    inRegHnds = lHnds((1:dataInRegionCount));
		    % Handles to the line for boundary points
		    nSetPlotProperties(inRegHnds,nonEmptyInRegionIndices,'in',magconfig);
		    nSetAxisLimitsDepths(plotAxis,[inReg]);

%--------------------------------------------------------------------------------------		
		%plot two catalogs with identical events
		case 'Compare'
		 
		 %DELETE AFTER THE TASK IS DONE
%		  PlotRegion(plotAxis,...
%                    struct('uniqA',inReg,...
%                    'uniqB'
%                    'indentical'
%                    'plotCoastline',obj.CoastToggle,...
%                    'plotBorder',obj.BorderToggle,...
%                    'showlegend',obj.LegendToggle,...
%                    'Filenames',
%                    'dataStore',obj.DataStore,...
%                    'Quality', 'lowcomp',...
%                    
%                    'PlotType','Compare'),mags);
		 
		 	%similar like normal whole mode, but threetimes for all the parts
		 	 inReg = eventStruct.uniqA;
			 [inRegArgs,uniqARegionIndices] = nMakePlotArgs(...
		        inReg(:,1),inReg(:,2),mags(1).selected, magconfig(1));
		    
		   	 plotArgs = {inRegArgs{:}};
			
			inReg = eventStruct.uniqB;
			 [inRegArgs,uniqBRegionIndices] = nMakePlotArgs(...
		        inReg(:,1),inReg(:,2),mags(2).selected, magconfig(2));
		    
		   	 plotArgs = {plotArgs{:}, inRegArgs{:}};
		   	 
		   	 inReg = eventStruct.indentical;
			 [inRegArgs,IndiRegionIndices] = nMakePlotArgs(...
		        inReg(:,1),inReg(:,2),mags(3).selected, magconfig(3));
		    
		   	 plotArgs = {plotArgs{:}, inRegArgs{:}};
		   	 
			try
		    	if eventStruct.plotCoastline
		        % Only plot the coastline for the main plot axis, as the change to
		        % pcolor mode in the calculation grid plot crashes otherwise.
		        % TODO : see why this is.
		        	Coast_PlotArgs = eventStruct.dataStore.getUserData('Coast_PlotArgs');
		        	if ~isempty(Coast_PlotArgs)
		            plotArgs = {plotArgs{:}, Coast_PlotArgs{:}};
		        	end
		        end	
		    end    
		       
		     try
		        if eventStruct.plotBorder
		        	InternalBorder_PlotArgs = eventStruct.dataStore.getUserData('InternalBorder_PlotArgs');
		        	if ~isempty(InternalBorder_PlotArgs)
		            	plotArgs = {plotArgs{:}, InternalBorder_PlotArgs{:}};
		        	end
		        end
		        
		        
		    end

			 %% Plot the results
		    % Plot the points inside the region of interest, outside the region of
		    % interest and the boundary
		    % Keep the handles of the data lines for setting symbol shapes and
		    % other properties
		    lHnds = nPlotEvents(plotAxis,plotArgs);
			
			% Count of number of data lines in the region and outside the region
			uniqARegionCount = numel(uniqARegionIndices);
			uniqBRegionCount = numel(uniqBRegionIndices);
			IndiRegionCount = numel(IndiRegionIndices);
			
			% Handles to the lines for events inside the region of interest
		    uniqA_inRegHnds = lHnds((1:uniqARegionCount));
		    uniqB_inRegHnds = lHnds((1:uniqBRegionCount)+uniqARegionCount);
		    Indi_inRegHnds = lHnds((1:IndiRegionCount)+uniqARegionCount+uniqBRegionCount);
		    
		    % Handles to the line for boundary points
		    nSetPlotProperties(uniqA_inRegHnds,uniqARegionIndices,'in',magconfig(1));
		    nSetPlotProperties(uniqB_inRegHnds,uniqBRegionIndices,'in',magconfig(2));
		    nSetPlotProperties(Indi_inRegHnds,IndiRegionIndices,'in',magconfig(3));
		    
		    nSetAxisLimits(plotAxis,[eventStruct.uniqA; eventStruct.uniqB; eventStruct.indentical]);
		    
		    %build legend
		    inHandles=plotAxis;
		    names=eventStruct.Filenames;
		    coast=eventStruct.plotCoastline;
		    border=eventStruct.plotBorder;
		    %BuildCompareLegend(plotAxis, eventStruct.Filenames, eventStruct.plotCoastline, eventStruct.plotBorder)
		    BuildCompareLegend(inHandles,names,coast,border);
		    %BuildLegend(plotAxis,magconfig,false,false,mags,eventStruct.plotCoastline,eventStruct.plotBorder);
		    
		    try
		    if eventStruct.showlegend
		    		legend(plotAxis,'show');
		    	end
			end

		
		end
%--------------------------------------------------------------------------------------

else %error message
    error('PlotRegion:unknown_argument',...
        'PlotRegion expects a matrix of event locations or a struct')
end



%==============================================================================================

    function nSetAxisLimits(plotAxis,regionData)
        % Set the axis limits to just the event data limits
        minLims = 1*min(regionData);
        maxLims = 1*max(regionData);
        axis(plotAxis,[minLims(1) maxLims(1) minLims(2) maxLims(2)]);
        latlim = get(plotAxis,'Ylim');
        set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
        set(plotAxis,'DrawMode','fast');
            
    end

	
    function nSetAxisLimitsDepths(plotAxis,regionData)
        % Set the axis limits to just the event data limits
        minLims = 1*min(regionData);
        maxLims = 1*max(regionData);
        axis(plotAxis,[minLims(1) maxLims(1) minLims(2) maxLims(2)]);
        depthlim = get(plotAxis,'Ylim');
        set(plotAxis,'dataaspect',[2 1 1]);
        set(plotAxis, 'YDir', 'reverse');
        set(plotAxis,'DrawMode','fast');
            
    end



    function lHnds = nPlotEvents(plotAxis,plotArgs)
        lHnds = plot(plotAxis,plotArgs{:});
        hXLabel = xlabel(plotAxis,'Longitude ');
        hYLabel = ylabel(plotAxis,'Latitude');
        hTitle = title(plotAxis,' ');
        
        
        set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
        set([hXLabel, hYLabel       ]        , 'FontSize'   , 12          );
        set( hTitle                          , 'FontSize'   , 12          , ...
            'FontWeight' , 'bold'      );
        
        set(plotAxis, ...
            'Box'         , 'on'     , ...
            'TickDir'     , 'out'     , ...
            'Fontsize'    , 10    , ...
            'TickLength'  , [.015 .01] , ...
            'XMinorTick'  , 'on'      , ...
            'YMinorTick'  , 'on'      , ...
            'YGrid'       , 'off'      , ...
            'XGrid'       , 'off'      , ...
            'XColor'      , [.3 .3 .3], ...
            'YColor'      , [.3 .3 .3], ...
            'LineWidth'   , 1         );
    end

	
    function lHnds = nPlotEventsSlice(plotAxis,plotArgs)
        lHnds = plot(plotAxis,plotArgs{:});
        hXLabel = xlabel(plotAxis,'Distance [km]');
        hYLabel = ylabel(plotAxis,'Depth [km]');
        hTitle = title(plotAxis,' ');
        
        
        set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
        set([hXLabel, hYLabel       ]        , 'FontSize'   , 12          );
        set( hTitle                          , 'FontSize'   , 12          , ...
            'FontWeight' , 'bold'      );
        
        set(plotAxis, ...
            'Box'         , 'on'     , ...
            'TickDir'     , 'out'     , ...
            'Fontsize'    , 10    , ...
            'TickLength'  , [.015 .01] , ...
            'XMinorTick'  , 'on'      , ...
            'YMinorTick'  , 'on'      , ...
            'YGrid'       , 'off'      , ...
            'XGrid'       , 'off'      , ...
            'XColor'      , [.3 .3 .3], ...
            'YColor'      , [.3 .3 .3], ...
            'LineWidth'   , 1         );
    end

	
    function [plotArgCell,nonEmptyIndices]=nMakePlotArgs(lon,lat,mags,magconfig)
        % Define the cutoff magnitudes for each partition
        % magconfig is a structur with the configuretion of the different magnitude slots
        
        %magRangeLower = [-Inf,1,2,3,4,5,6,7,8];
        %magRangeUpper = [1,2,3,4,5,6,7,8,Inf];
        magRangeLower = magconfig.LowerRange;
        magRangeUpper = magconfig.UpperRange;
        magRangeCount = numel(magRangeLower);
        % Define a vector of logicals to represent if a given magnitude
        % range has any elements in it.  If it doesn't then the eventual
        % call to plot will not create a line handle for that magnitude
        % range, so setting the plot properties in the later stage will
        % cause a crash
        rangeNotEmpty = true(magRangeCount,1);
        % Define the symbols to use
        %magSymbols = {'.','ok','ok','sk','sk','sk','sk','dk','hk'};
        magSymbols = magconfig.MagSymbols;
        
        % Define the longitudes and latitudes
        plotArgCell = cell(1,3*numel(magSymbols));
        for partitionIndex=1:magRangeCount
            logicalIndexVect = ...
                mags >= magRangeLower(partitionIndex) & ...
                mags < magRangeUpper(partitionIndex);
            plotArgCell{1+(partitionIndex-1)*3}=...
                lon(logicalIndexVect);
            plotArgCell{2+(partitionIndex-1)*3}=...
                lat(logicalIndexVect);
            plotArgCell{3+(partitionIndex-1)*3}=...
                magSymbols{partitionIndex};
            if all(~logicalIndexVect)
                rangeNotEmpty(partitionIndex)=false;
            end
        end
        nonEmptyIndices = (1:magRangeCount)';
        nonEmptyIndices = nonEmptyIndices(rangeNotEmpty);
    end

	
		
	

    function nSetPlotProperties(inHandles,nonEmptyIndices,regionSelect,magconfig)
        % Given dataset handles set the plot properties
        % magconfig is a structur with the configuretion of the different magnitude slots
        
        if isempty(inHandles)
            return
        end
        
        % Define the marker sizes to use
        %magMarkerSizes = [2,3,4,5,6,7,8,9,10];
        magMarkerSizes = magconfig.MarkerSize;
        
        switch regionSelect
            case 'in'
                % Define the marker face colors to use
                %magMarkerFaceColors = {'w','w','w','w','w','y','y','w','w'};
                magMarkerFaceColors = magconfig.MarkerFaceColorIn;
                
                % Define the marker colors
                %blackCol = [0 0 0];
                %redCol = [0.8 0 0];
                %magColors = {blackCol,blackCol,blackCol,blackCol,redCol,redCol,redCol,redCol,redCol};
            	magColors = magconfig.FaceColorIn;
           
            case 'out'
                % Define the marker face colors to use
                %magMarkerFaceColors = {'w','w','w','w','w','w','w','w','w'};
                magMarkerFaceColors = magconfig.MarkerFaceColorOut;
                
                % Define the marker colors
                %greyCol = [0.8 0.8 0.8];
                %magColors = {greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol};
                magColors = magconfig.FaceColorOut;
        end
        for hndIndex=1:numel(nonEmptyIndices)
            markerIndex = nonEmptyIndices(hndIndex);
            set(inHandles(hndIndex),...
                'MarkerSize',magMarkerSizes(markerIndex),...
                'MarkerFaceColor',magMarkerFaceColors{markerIndex},...
                'Color',magColors{markerIndex});
        end
        %         if strcmp(get(gcf,'Name'),'MapSeis Main Window')
        %
        %             legend(inHandles,'M < 1 ','1 <= M < 2','2<= M <3',...
        %                 '3 <= M < 4','4 <= M < 5',' 5 <= M < 6', ' 6 <= M < 7',' 7 <= M < 8',' M > 8','Location','NorthEast');
        %         end
        
    end




    function nSetBoundaryProperties(bdryHnd)
        import mapseis.region.*;
        
        % Set the properties of the boundary points, including callback
        % functions to execute eg resize boundary
        set(bdryHnd,'MarkerFaceColor',[0 0 0],'MarkerSize',2);
        if isfield(eventStruct,'regionFilter')
            %  Create a context menu to use to modify selection region
            %CreateRegionModifyMenu(bdryHnd,eventStruct);
        end
    end
    
    
    function magconfig = HiQualConfig
    	 	%creates the High Quality Magnitude Configuration
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.8 0.8 0.8];
    	 	
    	 	
    	 	%Structur
    	 	magconfig =struct('LowerRange',[-Inf,1,2,3,4,5,6,7,8],...
    	 					  'UpperRange',[1,2,3,4,5,6,7,8,Inf],...	
    	 					  'MagSymbols',{{'.','sk','sk','sk','sk','sk','sk','dk','hk'}},...	
    	 					  'MarkerSize',[2,3,4,5,6,7,8,9,10],...
                              'MarkerFaceColorIn',{{'w','w','w','w','w','y','r','w','w'}},...
                              'FaceColorIn',{{blackCol,blackCol,blackCol,blackCol,redCol,redCol,redCol,redCol,redCol}},...
                              'MarkerFaceColorOut',{{'w','w','w','w','w','w','w','w','w'}}, ...
                              'FaceColorOut',{{greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol}});
    
    
    
    end
    
    
    function magconfig = MedQualConfig
    	 	%creates the High Quality Magnitude Configuration
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.8 0.8 0.8];
    	 	
    	 	
    	 	%Structur
    	 	magconfig =struct('LowerRange',[-Inf,3,6],...
    	 					  'UpperRange',[3,6,Inf],...	
    	 					  'MagSymbols',{{'.','sk','hk'}},...	
    	 					  'MarkerSize',[4,5,6],...
                              'MarkerFaceColorIn',{{'w','w','y'}},...
                              'FaceColorIn',{{blackCol,blackCol,redCol}},...
                              'MarkerFaceColorOut',{{'w','w','w'}}, ...
                              'FaceColorOut',{{greyCol,greyCol,greyCol}});
    
    
    
    end

    
	function magconfig = LowQualConfig
    	 	%creates the High Quality Magnitude Configuration
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.8 0.8 0.8];
    	 	
    	 	
    	 	%Structur
    	 	magconfig =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[3],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'b'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});
    
    
    
    end

	
	function magconfig = LowCompareConfig
    	 	%creates the Low Quality Magnitude Configuration
    	 	%For the compare algo it creates three different settings, 
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.7 0.7 0.7];
    	 	
    	 	
    	 	%Structur
    	 	magconfig(1) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[7],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'r'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});
    
    		magconfig(2) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[7],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'b'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});

    		magconfig(3) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[7],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'g'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{'g'}});

    end

	
	function magconfig = HiCompareConfig
    	 	%creates the Low Quality Magnitude Configuration
    	 	%For the compare algo it creates three different settings, 
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.6 0.6 0.6];
    	 	
    	 	
    	 	%Structur
    	 	magconfig(1) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'o'}},...	
    	 					  'MarkerSize',[5],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'r'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});
    
    		magconfig(2) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'o'}},...	
    	 					  'MarkerSize',[5],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'b'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});

    		magconfig(3) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'o'}},...	
    	 					  'MarkerSize',[5],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'g'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{'g'}});

    end




	function BuildLegend(inHandles,magconfig,polyg,sliceplot,mags,coast,border)
			%creates a legend with the given input configurations
			
			if polyg
				%get lowest and highest drawn magnitude
				nu = numel(magconfig.LowerRange);
				
				%counter for magsl
				co = 1;
				
				
				%unselected
				%----------

				if nu == 1 & numel(mags.unselected)>0
					% for the zmap style
					magsl{co} = 'Earthquakes';
					co=co+1;
				else
				
					%lowermost magnitude
					num_mags = numel(mags.unselected(mags.unselected<magconfig.UpperRange(1)));
					
					if num_mags>=1				
						magsl{co} = ['M < ',num2str(magconfig.UpperRange(1))];	
						co=co+1;
					end
									
					%and the rest
					for c=2:nu-1
						num_mags = numel(mags.unselected(mags.unselected>magconfig.LowerRange(c) & ...
										mags.unselected<magconfig.UpperRange(c)));
						
						if num_mags>=1
							magsl{co} =  [num2str(magconfig.LowerRange(c)),...
										' < M < ',num2str(magconfig.UpperRange(c))];
							co=co+1;
						end				
					
					end
					
					%highest magnitude
					num_mags = numel(mags.unselected(mags.unselected>magconfig.LowerRange(nu)));
					
					if num_mags>=1				
						magsl{co} = [num2str(magconfig.UpperRange(nu)),' < M'];
						co=co+1;
					end
						
				end	


				
				%selected
				%--------
				
				if nu == 1
					% for the zmap style
					magsl{co} = 'Earthquakes';
				else
				
					%lowermost magnitude
					num_mags = numel(mags.selected(mags.selected<magconfig.UpperRange(1)));
					
					if num_mags>=1				
						magsl{co} = ['M < ',num2str(magconfig.UpperRange(1))];	
						co=co+1;
					end
									
					%and the rest
					for c=2:nu-1
						num_mags = numel(mags.selected(mags.selected>magconfig.LowerRange(c) & ...
										mags.selected<magconfig.UpperRange(c)));
						
						if num_mags>=1
							magsl{co} =  [num2str(magconfig.LowerRange(c)),...
										' < M < ',num2str(magconfig.UpperRange(c))];
							co=co+1;
						end				
					
					end
					
					%highest magnitude
					num_mags = numel(mags.selected(mags.selected>magconfig.LowerRange(nu)));
					
					if num_mags>=1				
						magsl{co} = [num2str(magconfig.LowerRange(nu)),' < M'];
						co=co+1;
					end
						
				end	
%===============================================================================================
				
			else
				
				%get lowest and highest drawn magnitude
				nu = numel(magconfig.LowerRange);
				
				%counter for magsl
				co = 1;

				
				%selected
				%--------
				
				if nu == 1
					% for the zmap style
					magsl{co} = 'Earthquakes';
				else
				
					%lowermost magnitude
					num_mags = numel(mags.selected(mags.selected<magconfig.UpperRange(1)));
					
					if num_mags>=1				
						magsl{co} = ['M < ',num2str(magconfig.UpperRange(1))];	
						co=co+1;
					end
									
					%and the rest
					for c=2:nu-1
						num_mags = numel(mags.selected(mags.selected>magconfig.LowerRange(c) & ...
										mags.selected<magconfig.UpperRange(c)));
						
						if num_mags>=1
							magsl{co} =  [num2str(magconfig.LowerRange(c)),...
										' < M < ',num2str(magconfig.UpperRange(c))];
							co=co+1;
						end				
					
					end
					
					%highest magnitude
					num_mags = numel(mags.selected(mags.selected>magconfig.LowerRange(nu)));
					
					if num_mags>=1				
						magsl{co} = [num2str(magconfig.LowerRange(nu)),' < M'];
						co=co+1;
					end
						
				end	
		
			
			end
			
			if sliceplot
				magsl{end+1}='Polygon';
			end	
			
				
				
						
			if (coast && border)
				legend(inHandles,magsl{:},'Coastline','Border','Location','NorthEast');
				
			elseif (coast && ~border)
				legend(inHandles,magsl{:},'Coastline','Location','NorthEast');	
			
			elseif (~coast && border)
				legend(inHandles,magsl{:},'Border','Location','NorthEast');
				
			else
				legend(inHandles,magsl{:},'Location','NorthEast');
			end	
			
			%set to invisible by default
			legend(inHandles,'hide');
			
	end
	
	
		

end


function BuildCompareLegend(inHandles,names,coast,border)
		%builds a special version of the legend for the compare plot 
		%(may be included in the normal legendbuilder later)
			
			
			magsl{1} =  [names{1} 'unique'];
			magsl{2} = [names{2} 'unique'];
			magsl{3} = 'identical';
			
		
		
			if (coast && border)
				legend(inHandles,magsl{:},'Coastline','Border','Location','NorthEast');
				
			elseif (coast && ~border)
				legend(inHandles,magsl{:},'Coastline','Location','NorthEast');	
			
			elseif (~coast && border)
				legend(inHandles,magsl{:},'Border','Location','NorthEast');
				
			else
				legend(inHandles,magsl{:},'Location','NorthEast');
			end	
			
			%set to invisible by default
			legend(inHandles,'hide');

		
	
	end
	

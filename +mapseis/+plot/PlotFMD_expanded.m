function [handle legendentry] = PlotFMD_expanded(plotAxis,ResStruct)
% PlotFMD : Plot histogram of event magnitudes

%Converted version of PlotFMD for the ResultGUI


% bvalsum is the cum. sum in each bin
% bval2 is number events in each bin, in reverse order
% bvalsum3 is reverse order cum. sum.
% xt3 is the step in magnitude for the bins == .1
%

%This version can do an curve fitting and return b-value and Mc, but it either needs a datastore or a zmap catalog;



%This function is a genericplot used by the ResultGUI
	%possible config fields for this function:
	%Data:				Can either be a datastore or a Zmap catalog
	%Selected:			The events used for the calculation, has to be a logical vector same length as the catalog
	%BinSize:			If not specified a binsize of 0.1 will be used
	%b_line:			if set to true, a interpolation will be done and b-value line will be plotted.
	%Mc_Method:			Sets the Mc determination method max. curvature will be used if not specified.
	%CustomCalcParameter:		Allows to set the CalcParameters of Calc_Master_Mc_bval used for the interpolation.
	%Write_Text:			If set to true (and b_line=true) the Mc values and b values will be written under the graphic.
	%Value_Output:			Only if b_line is set to true. can be 'none', in this case only the legend is written to legendentry
	%				,'Text' will write the text normaly written under to plot into the legendentry output, 'Value', will write
	%				the values (Mc, a, b, etc) into the legendentry out, and 'all' will write everything (legend,text,value) into
	%				legendentry;
	%X_Axis_Label:			The label under the X-Axis, if empty (not specified) it 
	%				will be set to 'Magnitude', if the field is missing, nothing will be
	%				done
	%Y_Axis_Label:			The label under the Y-Axis, if empty (not specified) it 
	%				will be set to 'Cumulative number of events', if the field is missing, nothing will be
	%				done			
	%Line_or_Point:			Defines the plotting style, left empty or set to 'line' will 
	%					draw only the line, set to 'point' will only draw markers and
	%					set to 'both' will draw markers and the line
	%LineStylePreset:		This allows to change the style of the plotted line (if plotted)
	%					with simple presets if not not specified the default style ('normal') 
	%					will be used. The presets are: 'normal','dotted','slashed','Fatline'
	%					'Fatdot','Fatslash and the two automatic settings which try use different
	%					LineStyle every plot: 'automatic' and 'Fatauto' for wider lines.
	%CustomLine:	 		This allows to specify the line properties freely, CustomLine{1} defines
	%					LineStyle and CustomLine{2} defines the LineWidth. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomLine should be missing in
	%					ResStruct
	%MarkerStylePreset:		This allows to change the Style of the Marker (if plotted) with simple Presets 
	%					if not not specified the default style ('normal') will be used. The presets 
	%					are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%					which try use different MarkerStyles every plot: 'automatic' and 'auto_nodot' which 
	%					does not use the point ('.').
  	%CustomMarker:	 		This allows to specify the Marker properties freely, CustomMarker{1} defines
	%					the Marker and CustomMarker{2} defines the MarkerSize. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomMarker should be missing in
	%					ResStruct
	%Colors:			This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%					if not not specified the default Color ('blue') will be used. The presets 
	%					are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%					which try use different Color every plot.
	%MarkerColors:			If specified the Color of the Markers will be set different than the line, {1} for the MarkerEdgeColor
	%				{2} for the MarkerFaceColor, the entries have to be standard matlab color option (e.g. 'r')
	%CustomColors:	 		This allows to specify the Color properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColor should be missing in
	%					ResStruct
	%X_Axis_Limit: 			Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 			Same for the Y-Axis
	%LegendText:			In this case it will just be written to he output variable legendentry if specified, else
	%					'Cum. Number of EQ per Magnitude' will be used as LegendEntry.
	
	
	import mapseis.calc.Calc_Master_Mc_bval;
	import mapseis.export.Export2Zmap;
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	if isobject(ResStruct.Data)
		 ZmapCatalog=Export2Zmap(ResStruct.Data,ResStruct.Selected);
		 
	else
		ZmapCatalog=ResStruct.Data(ResStruct.Selected,:);
	end
	
	
	nolegend=false;
	
	
	%Build some internal data settings
	wheelLine={'-','--',':'};
	wheelMarker={'.','+','o','x','diamond'}
	wheelMarker_nodot={'+','o','x','diamond'}
	wheelColor={'b','r','g','k','c','m'}
	
	
	%set BinSize
	if isfield(ResStruct,'BinSize')
		if ~isempty(ResStruct.BinSize)
			Binning=ResStruct.BinSize
		else
			Binning=0.1;
			
		end
	else
		Binning=0.1;				
	end
	
	
	
	if isfield(ResStruct,'Value_Output')
		if ~isempty(ResStruct.Value_Output)
			ValOut=ResStruct.Value_Output
		else
			ValOut='none';
			
		end
	else
		ValOut='none';				
	end
	
	
	
	Mc_Method=1;
	if isfield(ResStruct,'Mc_Method')
			if isempty(ResStruct.Mc_Method)
				Mc_Method=ResStruct.Mc_Method;
			
			end
	end		
	
	
	CalcConfig=struct(	'Mc_method',Mc_Method,...
				'Use_bootstrap',false,...
				'MinNumber',10,...
				'Mc_Binning',Binning,...
				'Nr_bootstraps',100,...
				'Mc_correction',0,...
				'NoCurve',false);
		
	if isfield(ResStruct,'CustomCalcParameter')
		if ~isempty(ResStruct.CustomCalcParameter)			
			CalcConfig=ResStruct.CustomCalcParameter;
		end
	end	
					
	CalcRes=Calc_Master_Mc_bval(ZmapCatalog,CalcConfig);			
	
	
	
	
	
	
	if ResStruct.b_line
		%more than one thing has to be plotted
		cumHandle = semilogy(plotAxis,CalcRes.mag_bins,CalcRes.cum_eq_mag);
		hold(plotAxis,'on');
		magHandle = semilogy(plotAxis,CalcRes.mag_bins,CalcRes.eq_mag);
		blineHandle = semilogy(plotAxis,CalcRes.b_val_line(:,1),CalcRes.b_val_line(:,2));
		
		try
			McPointHandle = semilogy(plotAxis ,CalcRes.Mc_point(:,1),CalcRes.Mc_point(:,2));
		catch
			McPointHandle=[];
		end
		
		%now texts
		try
			axes(plotAxis)
			Mctext = text(CalcRes.Mc_point(:,1)+0.2,CalcRes.Mc_point(:,2),'Mc');
		catch
			Mctext=[];
		end	
		
		if ResStruct.Write_Text
				
			if isnan(CalcRes.Mc_bootstrap)
				txt1=text(.16, .11,['b-value = ',num2str(round(100*CalcRes.b_value)/100),' +/- ',...
					num2str(round(100*CalcRes.b_value_bootstrap)/100),...
					',  a value = ',num2str(CalcRes.a_value,3) ',  a value (annual) = ',...
					num2str(CalcRes.a_value_annual,3)],'FontSize',10);
				set(txt1,'FontWeight','normal')
				sol_type = 'Maximum Likelihood Solution';
				text(.16, .14,sol_type,'FontSize',10);
				text(.16, .08,['Magnitude of Completeness = ',num2str(CalcRes.Mc)],'FontSize',10);
			else
				txt1=text(.16, .11,['b-value = ',num2str(round(100*CalcRes.b_value)/100),' +/- ',...
					num2str(round(100*CalcRes.b_value_bootstrap)/100),...
					',  a value = ',num2str(CalcRes.a_value,3) ',  a value (annual) = ',...
					num2str(CalcRes.a_value_annual,3)],'FontSize',10);
				set(txt1,'FontWeight','normal')
				sol_type = 'Maximum Likelihood Estimate, Uncertainties by bootstrapping';
				text(.16, .14,sol_type,'FontSize',10 );
				text(.16, .08,['Magnitude of Completeness = ',num2str(CalcRes.Mc) ' +/- ',...
					num2str(round(100*CalcRes.Mc_bootstrap)/100)],'FontSize',10);
			end
		
			
			
		end	
				
		hold(plotAxis,'off');
		
		
		
		switch ValOut
			case 'none'
				%do nothing
			case 'Text'
				if isnan(CalcRes.Mc_bootstrap)
					TheText{1}=['b-value = ',num2str(round(100*CalcRes.b_value)/100),' +/- ',...
						num2str(round(100*CalcRes.b_value_bootstrap)/100),...
						',  a value = ',num2str(CalcRes.a_value,3) ',  a value (annual) = ',...
						num2str(CalcRes.a_value_annual,3)];
					TheText{2} = 'Maximum Likelihood Solution';
					TheText{3} = ['Magnitude of Completeness = ',num2str(CalcRes.Mc)];
					
				else
					TheText{1} = ['b-value = ',num2str(round(100*CalcRes.b_value)/100),' +/- ',...
						num2str(round(100*CalcRes.b_value_bootstrap)/100),...
						',  a value = ',num2str(CalcRes.a_value,3) ',  a value (annual) = ',...
						num2str(CalcRes.a_value_annual,3)]
					TheText{2} = 'Maximum Likelihood Estimate, Uncertainties by bootstrapping';
					TheText{3} = ['Magnitude of Completeness = ',num2str(CalcRes.Mc) ' +/- ',...
						num2str(round(100*CalcRes.Mc_bootstrap)/100)];
				
				end	
				
				legendentry.Text=TheText;
				nolegend=true;
			case 'Value'
				legendentry.Values=CalcRes;
				nolegend=true;
				
			case 'all'
				if isnan(CalcRes.Mc_bootstrap)
					TheText{1}=['b-value = ',num2str(round(100*CalcRes.b_value)/100),' +/- ',...
						num2str(round(100*CalcRes.b_value_bootstrap)/100),...
						',  a value = ',num2str(CalcRes.a_value,3) ',  a value (annual) = ',...
						num2str(CalcRes.a_value_annual,3)];
					TheText{2} = 'Maximum Likelihood Solution';
					TheText{3} = ['Magnitude of Completeness = ',num2str(CalcRes.Mc)];
					
				else
					TheText{1} = ['b-value = ',num2str(round(100*CalcRes.b_value)/100),' +/- ',...
						num2str(round(100*CalcRes.b_value_bootstrap)/100),...
						',  a value = ',num2str(CalcRes.a_value,3) ',  a value (annual) = ',...
						num2str(CalcRes.a_value_annual,3)]
					TheText{2} = 'Maximum Likelihood Estimate, Uncertainties by bootstrapping';
					TheText{3} = ['Magnitude of Completeness = ',num2str(CalcRes.Mc) ' +/- ',...
						num2str(round(100*CalcRes.Mc_bootstrap)/100)];
				
				end	
				
				legendentry.Text=TheText;
				legendentry.Values=CalcRes;
				
				if isfield(ResStruct,'LegendText');	
					if ~isempty(ResStruct.LegendText)
						legendentry.Legend=ResStruct.LegendText;
					else	
						legendentry.Legend='Cum. Number of Eq per Magnitude';
					end
				else
		
					legendentry.Legend=[];
					
				end		
				
				nolegend=true;
			
			
		end	

				    
		%formating every thing
		%default
		set(Mctext,'FontWeight','bold','FontSize',10,'Color','b');
		set(McPointHandle,'Color','b','Marker','v','MarkerSize',7);
		set(cumHandle,'LineStyle','none','Marker','s','MarkerFaceColor','w','MarkerEdgeColor','k','MarkerSize',6);
		set(magHandle,'LineStyle','none','Marker','^','MarkerFaceColor','w','MarkerEdgeColor','k','MarkerSize',4);
		set(blineHandle,'LineStyle','-','LineWidth',1,'Color','r');
		
		
		%Now allow to change it
		%at the moment only Color and LineStyle (works only on b-line) 
		%every thing else "does not make too much sense"
		if isfield(ResStruct,'LineStylePreset')
			if ~isempty(ResStruct.LineStylePreset)
				switch ResStruct.LineStylePreset
					case 'normal'
						set(blineHandle,'LineStyle','-','LineWidth',1);
					case 'dotted'
						set(blineHandle,'LineStyle',':','LineWidth',1);
					case 'slashed'
						set(blineHandle,'LineStyle','--','LineWidth',1);
					case 'Fatline'
						set(blineHandle,'LineStyle','-','LineWidth',1.5);
					case 'Fatdot'
						set(blineHandle,'LineStyle',':','LineWidth',1.5);
					case 'Fatslash'
						set(blineHandle,'LineStyle','--','LineWidth',1.5);
					case 'automatic'
						%experimental but should work
						childish=get(plotAxis,'Children');
						existPlots=numel(findobj(childish,'Type','line'));
						set(blineHandle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1);
					case 'Fatautomatic'
						childish=get(plotAxis,'Children');
						existPlots=numel(findobj(childish,'Type','line'));
						set(blineHandle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1.5);
			
				end
			end
		end	
		
		
		%Customization Line
		if isfield(ResStruct,'CustomLine')
			if ~isempty(ResStruct.CustomLine)
				%This option allows to customize the line with the common matlab commands
				set(blineHandle,'LineStyle',ResStruct.CustomLine{1},'LineWidth',ResStruct.CustomLine{2});
			end
		end	
		
		%Colors
		%change the color to the preset if needed
		if isfield(ResStruct,'Colors')
			if ~isempty(ResStruct.Colors) 
				
				switch ResStruct.Colors
					case {'blue','b'}
						set(blineHandle,'Color','b');
						set(cumHandle,'MarkerFaceColor','b');
						set(magHandle,'MarkerFaceColor','b');
					case {'red','r'}
						set(blineHandle,'Color','r');
						set(cumHandle,'MarkerFaceColor','r');
						set(magHandle,'MarkerFaceColor','r');
					case {'green','g'}
						set(blineHandle,'Color','g');
						set(cumHandle,'MarkerFaceColor','g');
						set(magHandle,'MarkerFaceColor','g');
					case {'black','k'}
						set(blineHandle,'Color','k');
						set(cumHandle,'MarkerFaceColor','k');
						set(magHandle,'MarkerFaceColor','k');
					case {'cyan','c'}
						set(blineHandle,'Color','c');
						set(cumHandle,'MarkerFaceColor','c');
						set(magHandle,'MarkerFaceColor','c');
					case {'magenta','m'}
						set(blineHandle,'Color','m');
						set(cumHandle,'MarkerFaceColor','m');
						set(magHandle,'MarkerFaceColor','m');
					case 'automatic'
						%experimental but should work
						childish=get(plotAxis,'Children');
						existPlots=numel(findobj(childish,'Type','line'));
						set(blineHandle,'Color',wheelColor{mod(existPlots,6)+1});
						set(cumHandle,'MarkerFaceColor',wheelColor{mod(existPlots,6)+1});
						set(magHandle,'MarkerFaceColor',wheelColor{mod(existPlots,6)+1});
								
				end
			end
		end
		
		%Customization Color
		if isfield(ResStruct,'CustomColors')
			if ~isempty(ResStruct.CustomColors) 
				%This option allows to customize the line with the common matlab commands
				set(blineHandle,'Color',ResStruct.CustomColors);
				set(cumHandle,'MarkerFaceColor',ResStruct.CustomColors);
				set(magHandle,'MarkerFaceColor',ResStruct.CustomColors);
			end
		end	
				
		
		
		handle={Mctext,McPointHandle,cumHandle,magHandle,blineHandle};
	else
		%classic way	
		handle = semilogy(plotAxis,CalcRes.mag_bins,CalcRes.cum_eq_mag);
		
		%change to default linewitdth of 1
		set(handle,'LineStyle','-','LineWidth',1)
		
		%set to points and/or line
		if ~isempty(ResStruct.Line_or_Point) & strcmp(ResStruct.Line_or_Point,'points')
			set(handle,'LineStyle','none');
			set(handle,'Marker','.','MarkerSize',5);
		
		elseif ~isempty(ResStruct.Line_or_Point) & strcmp(ResStruct.Line_or_Point,'both')
			set(handle,'Marker','.','MarkerSize',5);
		end	

		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.LineStylePreset) & ~strcmp(ResStruct.Line_or_Point,'points')
			switch ResStruct.LineStylePreset
				case 'normal'
					set(handle,'LineStyle','-','LineWidth',1);
				case 'dotted'
					set(handle,'LineStyle',':','LineWidth',1);
				case 'slashed'
					set(handle,'LineStyle','--','LineWidth',1);
				case 'Fatline'
					set(handle,'LineStyle','-','LineWidth',1.5);
				case 'Fatdot'
					set(handle,'LineStyle',':','LineWidth',1.5);
				case 'Fatslash'
					set(handle,'LineStyle','--','LineWidth',1.5);
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1);
				case 'Fatautomatic'
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1.5);
			
			end
		end
		
		
		
		%Customization Line
		if isfield(ResStruct,'CustomLine')
			%This option allows to customize the line with the common matlab commands
			set(handle,'LineStyle',ResStruct.CustomLine{1},'LineWidth',ResStruct.CustomLine{2});
		end	
		
		
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.MarkerStylePreset) & ...
			(strcmp(ResStruct.Line_or_Point,'points') | strcmp(ResStruct.Line_or_Point,'both'))
			
			switch ResStruct.MarkeStylePreset
				case 'normal'
					set(handle,'Marker','.','MarkerSize',5)
				case 'plus'
					set(handle,'Marker','+','MarkerSize',5)
				case 'circle'
					set(handle,'Marker','o','MarkerSize',5)
				case 'cross'
					set(handle,'Marker','x','MarkerSize',5)
				case 'diamond'
					set(handle,'Marker','diamond','MarkerSize',5)
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1},'MarkerSize',5);
				case 'auto_nodot'
					%maybe usefull to avoid the small dot symbol
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker_nodot{mod(existPlots,4)+1},'MarkerSize',5);
			
			end
		end

		
		%Customization Line
		if isfield(ResStruct,'CustomMarker')
			%This option allows to customize the line with the common matlab commands
			set(handle,'MarkerStyle',ResStruct.CustomMarker{1},'MarkerSize',ResStruct.CustomMarker{2});
		end	

		
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
			
			switch ResStruct.Colors
				case 'blue'
					set(handle,'Color','b')
				case 'red'
					set(handle,'Color','r')
				case 'green'
					set(handle,'Color','g')
				case 'black'
					set(handle,'Color','k')
				case 'cyan'
					set(handle,'Color','c')
				case 'magenta'
					set(handle,'Color','m')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Color',wheelColor{mod(existPlots,6)+1});
							
			end
		end
		
		%Customization Color
		if isfield(ResStruct,'CustomColors')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Color',ResStruct.CustomColors);
		end	

		if isfield(ResStruct,'MarkerColors')
			if ~isempty(ResStruct.MarkerColors)
				set(handle,'MarkerEdgeColor',ResStruct.MarkerColors{1},...
					'MarkerFaceColor',ResStruct.MarkerColors{1});
			end		
		end

		
	end
	
	
	
	
	%now change the parameters of the plot
	%-------------------------------------
	
		%the labels: if not set, set to 'Magnitude' and 'Cumulative Number'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='Magnitude';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Cumulative Number';
			end
			
			hYLabel = ylabel(plotAxis,Y_axis_name);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
		
		


		%set limits if needed
		if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_Axis_Limit);	
		end
		
		
		%build legend (if not empty it will just copy the entry from ResStruct)
		if ~nolegend
			if isfield(ResStruct,'LegendText');	
				if ~isempty(ResStruct.LegendText)
					legendentry=ResStruct.LegendText;
				else	
					legendentry='Cum. Number of Eq per Magnitude';
				end
			else
			
			legendentry=[];
						
			end		
		end
		
		
		
end		

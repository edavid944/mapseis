function  [handle legendentry] = PlotBorder(plotAxis,ResStruct)	
	%This function belongs to the new earthquake plot function package
	%and plots only the Country Borders
		
	%Data:				This can either be a matrix with two coloumb (lon, lat) or 
	%					a datastore object.
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'Longitude'
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Latitude'
	%LineStyle:			This allows to select different style of lines: available are:
	%					'normal', 'Fatline' and 'dotted'
	%Color:				With this entry the color of the line can be selected, possible are:
	%					'blue','red','green','black','cyan','magenta'
	
	
	%New fields v1.1
	%Use_Offset:		This field turns the longitude and latitude offset in the plotting on or off
	%			It can be 'off','on','auto' and 'manual', if the fields is set to 'on' the datastore
	%			internal offset parameters are used. 'auto also uses the internal parameter but on/off
	%			is depended on the on/off variable of the datastore 
	%Manual_Offset:		If Use_Offset is set to 'manual' the here defined parameters will be used
	%			First value sets the longitude offset, second on set latitude offset ([lon_off lat_off])
	
	%use the last plot if not specified	
	if nargin<2
    	plotAxis=gca;	
	end	
	
	import mapseis.util.*;
	
	%check if Data is datastore or a matrix (it is only checked if it is an object) 
	if isobject(ResStruct.Data)
		try
			border_Args=ResStruct.Data.getUserData('InternalBorder_PlotArgs');
			lon=border_Args{1};
			lat=border_Args{2};
		
		catch
			lon=[];
			lat=[];
		end
	
	else %data is a matrix
		lon=ResStruct.Data(:,1);
		lat=ResStruct.Data(:,2);
	
	end
	
	
	%set the Use_Offset
	if ~isfield(ResStruct,'Use_Offset')
		OffSetSwitch='auto';
	elseif isempty(ResStruct.Use_Offset)
		OffSetSwitch='auto';
	else
		OffSetSwitch=ResStruct.Use_Offset;
	
	end
	
	if strcmp(OffSetSwitch,'auto')
		OffSetSwitch='off';
		try
			DataStoreSwitcher=ResStruct.Data.UseShift;
		catch
			DataStoreSwitcher=false;
		end	
		
		if DataStoreSwitcher
			OffSetSwitch='on';
		end	
		
	end
	
	
	%Set the offset if needed
	if strcmp(OffSetSwitch,'on')
		[LonOff,LatOff]=ResStruct.Data.getLocationOffset;
		OffSets=[LonOff,LatOff];
		ShiftedLocations=ShiftCoords([lon',lat'],OffSets);
		lon=ShiftedLocations(:,1)';
		lat=ShiftedLocations(:,2)';
		
	elseif strcmp(OffSetSwitch,'manual')
		OffSets=ResStruct.Manual_Offset;
		ShiftedLocations=ShiftCoords([lon',lat'],OffSets);
		lon=ShiftedLocations(:,1)';
		lat=ShiftedLocations(:,2)';
		
	end
	
	
	
	%now plot it (ask questions later)
	handle = plot(plotAxis,lon,lat);
	
	
	%change to default linewitdth of 1
	set(handle,'LineStyle','-','LineWidth',1, 'Color','k')

	
	%now change plot style
	%---------------------
	
		
	%set the axis labels (should not be needed but it is a good idea anyway
	if isfield(ResStruct,'X_Axis_Label')
		X_axis_name=ResStruct.X_Axis_Label;
		
		if isempty(X_axis_name)
			X_axis_name='Longitude';
		end
		
		hXLabel = xlabel(plotAxis,X_axis_name);
		set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
	end
	
	
	if isfield(ResStruct,'Y_Axis_Label')
		Y_axis_name=ResStruct.Y_Axis_Label; 	
		
		if isempty(Y_axis_name)
			Y_axis_name='Latitude';
		end
		
		hYLabel = ylabel(plotAxis,Y_axis_name);
		set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
	
	
	%change the linestyle with a preset if needed 
	if ~isempty(ResStruct.LineStylePreset) 
		switch ResStruct.LineStylePreset
			case 'normal'
				set(handle,'LineStyle','-','LineWidth',1);
			case 'dotted'
				set(handle,'LineStyle',':','LineWidth',1);
			case 'Fatline'
				set(handle,'LineStyle','-','LineWidth',1.5);

		
		end
	end

	
	%change the color to the preset if needed
	if ~isempty(ResStruct.Colors) 
		
		switch ResStruct.Colors
			case 'blue'
				set(handle,'Color','b')
			case 'red'
				set(handle,'Color','r')
			case 'green'
				set(handle,'Color','g')
			case 'black'
				set(handle,'Color','k')
			case 'cyan'
				set(handle,'Color','c')
			case 'magenta'
				set(handle,'Color','m')
			case 'white'
				set(handle,'Color','w')

		end
	end

	%Set the axis right in case of an offset
	if strcmp(OffSetSwitch,'on')|strcmp(OffSetSwitch,'manual')
			%The Xlim
			xticktator=get(plotAxis,'XTick');
			
			%myLim=xlim(plotAxis);
			xlim(plotAxis,'manual');
			%ShiftedLocations=ShiftCoords([myLim',[0;0]],OffSets);
			ShiftedLocations=ShiftCoords([xticktator',zeros(numel(xticktator),1)],OffSets);
			ShiftedLocations=sort(ShiftedLocations(:,1));
			NewXLim=[ShiftedLocations(1),ShiftedLocations(end)];
			disp(NewXLim);
			xlim(plotAxis,NewXLim);	
			
			%The Xlim
			myLim=ylim(plotAxis);
			ylim(plotAxis,'manual');
			ShiftedLocations=ShiftCoords([[0;0],myLim'],OffSets);
			NewYLim=ShiftedLocations(:,2)';
			ylim(plotAxis,NewYLim);	
			
			%get all ticks and convert them
			%get all ticks and convert them
			xticktator=get(plotAxis,'XTick');
			xticker=(-180+OffSets(1)):abs(xticktator(2)-xticktator(1)):(180+OffSets(1));
			ShiftedLocations=ShiftCoords([xticker',zeros(size(xticker'))],-OffSets);
			set(plotAxis,'XTick',xticker,'XTickLabel',num2str(ShiftedLocations(:,1)));
			
			yticktator=get(plotAxis,'YTick');
			yticker=(-90+OffSets(2)):abs(yticktator(2)-yticktator(1)):(90+OffSets(2));
			ShiftedLocations=ShiftCoords([zeros(size(yticker')),yticker'],-OffSets);
			set(plotAxis,'YTick',yticker,'YTickLabel',num2str(ShiftedLocations(:,2)));
			
	end
	
	
	%Legendentry
	legendentry='Internal Borders'
	
	
	

end
function  [handle legendentry] = PlotStreamLine2D(plotAxis,ResStruct)
	
	%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	%!Prototype function based on Vector2D function. I have no clue if it works.!
	%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	%This function is a genericplot used by the ResultGUI	
	%It Draws a vector field with the plotfunction quiver
	
	%possible config fields for this function:
	%Data:			Not optional, the data to plot Data(:,1) for the x-values
	%			Data(:,2) for the y-values, Data(:,3) and Data(:,4) for x vector component and
	%			y vector component or Angles and absolute Value with the AngleMode.
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%			will be set to 'X',if the field is missing, nothing will be
	%			done
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%			will be set to 'Y',if the field is missing, nothing will be
	%			done
	%AngleMode:		if set to 'none' Data(:,3) is for the arrow x-component and Data(:,4) 
	%			for the arrow y-component, if set to 'rad' or 'degree' Data(:,3) describes
	%			the angle (in rad or degree) and Datat(:,4) is the absolut value of the arrow
	%SeedPoints:		2D Vector with position of the seed points	
	%StepSize:		Defines the stepsize for the integration of the line
	%MaxStep:		Maximum number of steps
	%LineOutPut:		If set to true the legendentry output will be used to output the streamline data.
	%AutoScale:		leave empty to turn it off, include a numeric value, large than 1 increase the 
	%			relativ size of the vectors, values smaller than 1 decreases them.
	%Markers:		If set to true the Markers will be drawn, else not
	%LineStylePreset:	This allows to change the style of the plotted line (if plotted)
	%			with simple presets if not not specified the default style ('normal') 
	%			will be used. The presets are: 'normal','dotted','slashed','Fatline'
	%			'Fatdot','Fatslash' 
	%CustomLine:	 	This allows to specify the line properties freely, CustomLine{1} defines
	%			LineStyle and CustomLine{2} defines the LineWidth. CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomLine should be missing in
	%			ResStruct
	%MarkerStylePreset:	This allows to change the Style of the Marker (if plotted) with simple Presets 
	%			if not not specified the default style ('normal') will be used. The presets 
	%			are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%			which try use different MarkerStyles every plot: 'automatic' and 'auto_nodot' which 
	%			does not use the point ('.').
  	%CustomMarker:	 	This allows to specify the Marker properties freely, CustomMarker{1} defines
	%			the Marker and CustomMarker{2} defines the MarkerSize. CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomMarker should be missing in
	%			ResStruct
	%Colors:		This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%			if not not specified the default Color ('blue') will be used. The presets 
	%			are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%			which try use different Color every plot.
	%CustomColors:	 	This allows to specify the Color properties freely, CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomColor should be missing in
	%			ResStruct
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%			the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%			'Data' will be used as LegendEntry.
	
	
	
	%use the last plot if not specified
	if nargin<2
    		plotAxis=gca;	
	end	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end
	
	handle=[];
	
	
	%Build some internal data settings
	wheelMarker={'.','+','o','x','diamond'}
	wheelMarker_nodot={'+','o','x','diamond'}
	wheelColor={'b','r','g','k','c','m'}
	
	%xPos=ResStruct.Data(:,1);
	%yPos=ResStruct.Data(:,2);
	
	%default case
	%vector_x=ResStruct.Data(:,3);
	%vector_y=ResStruct.Data(:,4);
	
	xPos=ResStruct.Data{1};
	yPos=ResStruct.Data{2};	
	vector_x=ResStruct.Data{3};
	vector_y=ResStruct.Data{4};
	
	
	%default values
	StepSize=0.1;
	MaxStep=10000;
	LineOutPut=false;
	
	if isfield(ResStruct,'StepSize')
		if ~isempty(ResStruct.StepSize)
			StepSize=ResStruct.StepSize;
		end
	end	
	
	if isfield(ResStruct,'MaxStep')
		if ~isempty(ResStruct.MaxStep)
			MaxStep=ResStruct.MaxStep;
		end
	end		
	
	if isfield(ResStruct,'LineOutPut')
		if ~isempty(ResStruct.LineOutPut)
			LineOutPut=ResStruct.LineOutPut;
		end
	end	
	
	
	%calculate u and v of the vectors if wanted
	if isfield(ResStruct,'AngleMode')
		if ~isempty(ResStruct.AngleMode)
			switch ResStruct.AngleMode
				case 'none'
					%do nothing
					
					
				case 'rad'
					absValue=ResStruct.Data(:,4);
					RawAngles=ResStruct.Data(:,3);
					
					%correct negativ angles
					negAngles=RawAngles<0;
					RawAngles(negAngles)=2*pi+RawAngles(negAngles);
				
					%correct all angles >360 (just for safety)
					RawAngles=mod(RawAngles,2*pi);
					
					%now the different cases
					%normal 0-90 (0-pi/2)
					ang90=RawAngles<=pi/2;
					vector_x(ang90)=sin(RawAngles(ang90))*absValue(ang90);
					vector_y(ang90)=cos(RawAngles(ang90))*absValue(ang90);
					
					%y negativ 90-180 (pi/2 to pi): 
					ang180=RawAngles>pi/2 & RawAngles<=pi;
					vector_x(ang180)=sin(RawAngles(ang180))*absValue(ang180);
					vector_y(ang180)=-cos(RawAngles(ang180))*absValue(ang180);
					
					%y & x negativ 180-270 (pi to 3/2*pi:
					ang270=RawAngles>pi & RawAngles<=3/2*pi;
					vector_x(ang270)=-sin(RawAngles(ang270))*absValue(ang270);
					vector_y(ang270)=-cos(RawAngles(ang270))*absValue(ang270);
					
					%x-negativ 270-360 (3/2*pi to 2pi:
					ang360=RawAngles>3/2*pi;
					vector_x(ang360)=-sin(RawAngles(ang360))*absValue(ang360);
					vector_y(ang360)=cos(RawAngles(ang360))*absValue(ang360);


				case 'degree'
					absValue=ResStruct.Data(:,4);
					RawAngles=ResStruct.Data(:,3);
					
					%correct negativ angles
					negAngles=RawAngles<0;
					RawAngles(negAngles)=360+RawAngles(negAngles);
				
					%correct all angles >360 (just for safety)
					RawAngles=mod(RawAngles,360);
					
					%now the different cases
					%normal 0-90 
					ang90=RawAngles<=90;
					vector_x(ang90)=sin(deg2rad(RawAngles(ang90)))*absValue(ang90);
					vector_y(ang90)=cos(deg2rad(RawAngles(ang90)))*absValue(ang90);
					
					%y negativ 90-180: 
					ang180=RawAngles>90 & RawAngles<=180;
					vector_x(ang180)=sin(deg2rad(RawAngles(ang180)))*absValue(ang180);
					vector_y(ang180)=-cos(deg2rad(RawAngles(ang180)))*absValue(ang180);
					
					%y & x negativ 180-270:
					ang270=RawAngles>180 & RawAngles<=270;
					vector_x(ang270)=-sin(deg2rad(RawAngles(ang270)))*absValue(ang270);
					vector_y(ang270)=-cos(deg2rad(RawAngles(ang270)))*absValue(ang270);
					
					%x-negativ 270-360:
					ang360=RawAngles>270;
					vector_x(ang360)=-sin(deg2rad(RawAngles(ang360)))*absValue(ang360);
					vector_y(ang360)=cos(deg2rad(RawAngles(ang360)))*absValue(ang360);

					
					
			end
		end
	end			
	
	%Here comes the streamline part 
	%just use the default matlabe stream2 method at the moment, will improve it if needed.
	startx=ResStruct.SeedPoints(:,1);
	starty=ResStruct.SeedPoints(:,2);
	TheStreams=stream2(xPos,yPos,vector_x,vector_y,startx,starty,[StepSize,MaxStep]); 
	
	%plot them 
	handle=streamline(TheStreams);
	
	
	
	
	
	%handle=quiver(ResStruct.Data(:,1),ResStruct.Data(:,2),vector_x,vector_y);
		
	

	
	
	%change to default linewitdth of 1
	set(handle,'LineStyle','-','LineWidth',1)
	
	
	
	
	%now change the parameters of the plot
	%-------------------------------------
		

	
		%the labels: if not set, set to 'X' and 'Y'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Y';
			end
			
			hYLabel = ylabel(plotAxis,Y_axis_name);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end
		
		
		
		%Use Markers if wanted
		if isfield(ResStruct,'Markers')
			if ~isempty(ResStruct.Markers) & ResStruct.Markers
				set(handle,'Marker','.','MarkerSize',5);
				
			end
		end	
		
		
		%use autoscaling if wanted
		if isfield(ResStruct,'AutoScale')
			if ~isempty(ResStruct.AutoScale)
				set(handle,'AutoScaleFactor',ResStruct.AutoScale)
				
			end
		end	
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.LineStylePreset)
			switch ResStruct.LineStylePreset
				case 'normal'
					set(handle,'LineStyle','-','LineWidth',1);
				case 'dotted'
					set(handle,'LineStyle',':','LineWidth',1);
				case 'slashed'
					set(handle,'LineStyle','--','LineWidth',1);
				case 'Fatline'
					set(handle,'LineStyle','-','LineWidth',1.5);
				case 'Fatdot'
					set(handle,'LineStyle',':','LineWidth',1.5);
				case 'Fatslash'
					set(handle,'LineStyle','--','LineWidth',1.5);
			
			end
		end
		
		
		
		%Customization Line
		if isfield(ResStruct,'CustomLine')
			%This option allows to customize the line with the common matlab commands
			set(handle,'LineStyle',ResStruct.CustomLine{1},'LineWidth',ResStruct.CustomLine{2});
		end	
		
		
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.MarkerStylePreset)			
			switch ResStruct.MarkerStylePreset
				case 'none'
					set(handle,'Marker','none','MarkerSize',5)
				case 'normal'
					set(handle,'Marker','.','MarkerSize',5)
				case 'plus'
					set(handle,'Marker','+','MarkerSize',5)
				case 'circle'
					set(handle,'Marker','o','MarkerSize',5)
				case 'cross'
					set(handle,'Marker','x','MarkerSize',5)
				case 'diamond'
					set(handle,'Marker','diamond','MarkerSize',5)
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1},'MarkerSize',5);
				case 'auto_nodot'
					%maybe usefull to avoid the small dot symbol
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Marker',wheelMarker_nodot{mod(existPlots,4)+1},'MarkerSize',5);
			
			end
		end

		
		%Customization Line
		if isfield(ResStruct,'CustomMarker')
			%This option allows to customize the line with the common matlab commands
			set(handle,'MarkerStyle',ResStruct.CustomMarker{1},'MarkerSize',ResStruct.CustomMarker{2});
		end	

		
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
			
			switch ResStruct.Colors
				case 'blue'
					set(handle,'Color','b')
				case 'red'
					set(handle,'Color','r')
				case 'green'
					set(handle,'Color','g')
				case 'black'
					set(handle,'Color','k')
				case 'cyan'
					set(handle,'Color','c')
				case 'magenta'
					set(handle,'Color','m')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'Color',wheelColor{mod(existPlots,6)+1});
							
			end
		end

		%Customization Color
		if isfield(ResStruct,'CustomColors')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Color',ResStruct.CustomColors);
		end	




		%set limits if needed
		if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_Axis_Limit);	
		end
		
		
		%build legend (if not empty it will just copy the entry from ResStruct)
		if isfield(ResStruct,'LegendText')
			
			if ~isempty(ResStruct.LegendText)
				legendentry=ResStruct.LegendText;
			else	
				legendentry='Data';
			end
	
									
		else
			
			legendentry=[];
			
		end		
			
		
		if LineOutPut
			legendentry=TheStreams;
		end	
	
end

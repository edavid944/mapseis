function [handle legendentry] = PlotCsepTest(plotAxis,ResStruct,Results)
	%This function plots a csep testing results from a result structure produced
	%TestSuiteProto_mk2
	%No data needed in the ResStruct, the data will extracted from Results
	%Config fields:
	%--------------
	%Name:		Name of the tested model (has to suite the name used in the testing 
	%		(two names in cell are needed for some test)
	%TestType:	Sets which test should be draw ('Ntest','Stest','Mtest','Ltest','Rtest','Ttest',
	%		'Wtest') (Mtest not yet implemented.
	%PlotType:	Can be 'Hist', 'CDF','HScore','LScore','Score' for most test.
	%PrintText:	Writes the quantil score in the plot 
	%ScoreSwitch:	Sets which score will be used in a plot, an be 'mean','median','original','meanorg'
	%		for 'mean' and 'both', 'medianorg'for median and original score and 'all' for all three 
	%		values, For Mean and median a monte-carlo catalog pertubation calculation has to be 
	%		present.
	%DisplayName:	Will be used for the title if left empty the Name(s) will be used instead.
	%Colors:	Sets the color of the Line or Histogram
	%LineStyle:	Sets the linestyle of the Line can 
	%LineWidth: 	Sets the width of the line
	%ScoreWidth:	Sets the linewidth of the Quantil line
	%MiniHist:	if set to true, MiniHistogramms will be used instead of bars
	%PaperBoy	if set to true, Lines will be a bit thicker meant for plots used in papers
	%ModDispName	if the field exists this will be used instead of the name for the ylabel or xlabel
	%		in some of the plots (Ttest, Wtest, Multiple Histogram plots
	
	%The last for parameter use standard matlab plot option, conversion to the typical mapseis plot style might 
	%be added later.
	
	import mapseis.plot.*;
	import mapseis.util.*;
	
	%use the last plot if not specified
	if nargin<3
    		plotAxis=gca;	
	end	
	
	handle=[];
	legendentry=[];
	
	
	%set some needed parameters
	Colors=[0.5 0.5 0.7];
	LineStyler='-';
	MainWidth=3;
	ScoreWidth=2;
	DisplayName=[];
	ModDispName=ResStruct.Name;
	MiniHist=false;
	PaperBoy=false;
	WideBar=false;
	MiniHistHigh=0.4;
	BarTrail=false;
	
	if isfield(ResStruct,'Colors')
		if ~isempty(ResStruct.Colors)	
			Colors=ResStruct.Colors;
		end	
	end
	
	if isfield(ResStruct,'LineStyle')
		if ~isempty(ResStruct.LineStyle)	
			LineStyler=ResStruct.LineStyle;
		end	
	end
	
	if isfield(ResStruct,'LineWidth')
		if ~isempty(ResStruct.LineWidth)	
			MainWidth=ResStruct.LineWidth;
		end	
	end
	
	if isfield(ResStruct,'ScoreWidth')
		if ~isempty(ResStruct.ScoreWidth)	
			ScoreWidth=ResStruct.ScoreWidth;
		end	
	end
	
	if isfield(ResStruct,'DisplayName')
		if ~isempty(ResStruct.DisplayName)	
			DisplayName=ResStruct.DisplayName;
		end	
	end
	
	
	if isfield(ResStruct,'MiniHist')
		if ~isempty(ResStruct.MiniHist)	
			MiniHist=ResStruct.MiniHist;
		end	
	end
	
	if isfield(ResStruct,'PaperBoy')
		if ~isempty(ResStruct.PaperBoy)	
			PaperBoy=ResStruct.PaperBoy;
		end	
	end
	
	if isfield(ResStruct,'ModDispName')
		if ~isempty(ResStruct.ModDispName)	
			ModDispName=ResStruct.ModDispName;
		end	
	end
	
	if isfield(ResStruct,'WideBar')
		if ~isempty(ResStruct.WideBar)	
			WideBar=ResStruct.WideBar;
		end	
	end
	
	if isfield(ResStruct,'BarTrail')
		if ~isempty(ResStruct.BarTrail)	
			BarTrail=ResStruct.BarTrail;
		end	
	end
	
	
	%set which score should be plotted
	switch ResStruct.ScoreSwitch
		case 'mean'
			UseMean=true;
			UseOrg=false;
			UseMed=false;
			
		case 'median'
			UseMean=false;
			UseOrg=false;		
			UseMed=true;
			
		case 'original'
			UseMean=false;
			UseOrg=true;
			UseMed=false;
			
		case {'meanorg','both'}
			UseMean=true;
			UseOrg=true;
			UseMed=false;
			
		case 'medianorg'
			UseMean=false;
			UseOrg=true;
			UseMed=true;
			
		case 'all'
			UseMean=true;
			UseOrg=true;	
			UseMed=true;
	
	end
	
	if PaperBoy
		AxisWidth=3;
		AxisTexSize=14;
		DesTexSize=11;
		TitTexSize=18;
		LabTexSize=15;
		WeightAxis='bold';
		WeightLab='bold';
		
	else
		AxisWidth=2;
		AxisTexSize=12;
		DesTexSize=10;
		TitTexSize=15;
		LabTexSize=13;
		WeightAxis='demi';
		WeightLab='demi';	
		
	end
	
	
	if WideBar
		BarWider=0.9;
	else
		BarWider=0.25;
	end
	
	
	switch ResStruct.TestType
		case 'Ntest'
			%get Data:
			if iscell(ResStruct.Name)
				PlotData=Results.(['Ntest_',ResStruct.Name{1}]);
			else
				PlotData=Results.(['Ntest_',ResStruct.Name]);
			end
			
			MrHolden=ishold;
			
			%create the strings
			QuantString={};
			if UseOrg
				QuantString={['$$\delta_1o=$$ ',num2str(round(1000*PlotData.QLowMonte(1))/1000)],...
				['$$\delta_2o=$$ ',num2str(round(1000*PlotData.QHighMonte(1))/1000)]};
			end
			
			if UseMean
				QuantString(end+1)={['$$\delta_1m=$$ ',num2str(round(1000*PlotData.QScoreLow)/1000),' +/-',num2str(round(1000*PlotData.QScoreLow_std_dev)/1000)]};
				QuantString(end+1)={['$$\delta_2m=$$ ',num2str(round(1000*PlotData.QScoreHigh)/1000),' +/-',num2str(round(1000*PlotData.QScoreHigh_std_dev)/1000)]};
			end
			
			if UseMed
				QuantString(end+1)={['$$\delta_1m=$$ ',num2str(round(1000*PlotData.QScoreLowMedian)/1000),...
							' Q16: ',num2str(round(1000*PlotData.QScoreLow68Per(1))/1000),...
							' Q84: ',num2str(round(1000*PlotData.QScoreLow68Per(2))/1000)]};
				QuantString(end+1)={['$$\delta_2m=$$ ',num2str(round(1000*PlotData.QScoreHighMedian)/1000),...
							' Q16: ',num2str(round(1000*PlotData.QScoreHigh68Per(1))/1000),...
							' Q84: ',num2str(round(1000*PlotData.QScoreHigh68Per(2))/1000)]};	
			end
			
			
			switch ResStruct.PlotType
				case 'Hist'
					minH=min(PlotData.LogLikeModel);
					maxH=max(PlotData.LogLikeModel);
					hist(PlotData.LogLikeModel,minH:maxH);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Likelihood model'
					
					if UseOrg
						handle(end+1)=plot([PlotData.LogLikeCatalog,PlotData.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						%not available
					end
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.85,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test: ',DisplayName]);
					else
						theTit=title(['N-Test: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Number of Earthquakes   ');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'CDF'
					handle(1)=plotCDF(PlotData.LogLikeModel, plotAxis,'k');
					set(handle(1),'LineWidth',MainWidth,'Color',Colors);
					hold on;
					xlimer=xlim;
					
					legendentry{1}='Likelihood model';
					
					%now plot the quantil score
					if UseOrg
						handle(end+1)=plot([xlimer(1) PlotData.LogLikeCatalog PlotData.LogLikeCatalog],[PlotData.QLowMonte(1) PlotData.QLowMonte(1) 0],...
						'LineStyle','--','Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.25,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['CDF N-Test: ',DisplayName]);
					else
						theTit=title(['CDF N-Test: ',ResStruct.Name]);
					end
					
					
					%format the plot
					xlab=xlabel('Number of Earthquakes   ');
					ylab=ylabel('Empirical CDF   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'HScore'
					hist(PlotData.QHighMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QHighMonte(1),PlotData.QHighMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScoreHigh,PlotData.QScoreHigh],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScoreHigh-PlotData.QScoreHigh_std_dev,PlotData.QScoreHigh-PlotData.QScoreHigh_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScoreHigh+PlotData.QScoreHigh_std_dev,PlotData.QScoreHigh+PlotData.QScoreHigh_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreHighMedian,PlotData.QScoreHighMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScoreHigh68Per(1),PlotData.QScoreHigh68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScoreHigh68Per(2),PlotData.QScoreHigh68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.85,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test QScore High: ',DisplayName]);
					else
						theTit=title(['N-Test QScore High: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil High');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
				
				
				
				case 'LScore'
					hist(PlotData.QLowMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QLowMonte(1),PlotData.QLowMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScoreLow,PlotData.QScoreLow],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow-PlotData.QScoreLow_std_dev,PlotData.QScoreLow-PlotData.QScoreLow_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow+PlotData.QScoreLow_std_dev,PlotData.QScoreLow+PlotData.QScoreLow_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreLowMedian,PlotData.QScoreLowMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow68Per(1),PlotData.QScoreLow68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow68Per(2),PlotData.QScoreLow68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					if ResStruct.PrintText
						leText=text(0.65,0.85,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test QScore Low: ',DisplayName]);
					else
						theTit=title(['N-Test QScore Low: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil Low');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
				
					
					
					
					
				case 'Score'
					hist(PlotData.QLowMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QLowMonte(1),PlotData.QLowMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScoreLow,PlotData.QScoreLow],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow-PlotData.QScoreLow_std_dev,PlotData.QScoreLow-PlotData.QScoreLow_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow+PlotData.QScoreLow_std_dev,PlotData.QScoreLow+PlotData.QScoreLow_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreLowMedian,PlotData.QScoreLowMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow68Per(1),PlotData.QScoreLow68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScoreLow68Per(2),PlotData.QScoreLow68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.85,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test QScore Low: ',DisplayName]);
					else
						theTit=title(['N-Test QScore Low: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil Low');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
					
				case {'CompBoth','CompScore'}
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						NtestNow=Results.(['Ntest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=NtestNow.LogLikeModel;
						SynthMin(i)=min(NtestNow.LogLikeModel);
						SynthMax(i)=max(NtestNow.LogLikeModel);
						LowQScore(i)=NtestNow.QLowMonte(1);
						LowQMean(i)=NtestNow.QScoreLow;
						LowQSTD(i)=NtestNow.QScoreLow_std_dev;
						HiQScore(i)=NtestNow.QHighMonte(1);
						HiQMean(i)=NtestNow.QScoreHigh;
						HiQSTD(i)=NtestNow.QScoreHigh_std_dev;
						
						%Medians are not present in old Result calculations
						try
							LowQMed(i)=NtestNow.QScoreLowMedian;
							LowQQ16(i)=NtestNow.QScoreLow68Per(1);
							LowQQ84(i)=NtestNow.QScoreLow68Per(2);
							HiQMed(i)=NtestNow.QScoreHighMedian;
							HiQQ16(i)=NtestNow.QScoreHigh68Per(1);
							HiQQ84(i)=NtestNow.QScoreHigh68Per(2);
							
						catch
							LowQMed(i)=NaN;
							LowQQ16(i)=NaN;
							LowQQ84(i)=NaN;
							HiQMed(i)=NaN;
							HiQQ16(i)=NaN;
							HiQQ84(i)=NaN;
							MedianExist=false;
						end
						
				
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
					
					LQPoint=SynthMin+LowQScore.*DistSynth;
					LQMeanPoint=SynthMin+LowQMean.*DistSynth;
					LQSTDmin=SynthMin+(LowQMean-LowQSTD).*DistSynth;
					LQSTDmax=SynthMin+(LowQMean+LowQSTD).*DistSynth;
					
					HQPoint=SynthMin+HiQScore.*DistSynth;
					HQMeanPoint=SynthMin+HiQMean.*DistSynth;
					HQSTDmin=SynthMin+(HiQMean-HiQSTD).*DistSynth;
					HQSTDmax=SynthMin+(HiQMean+HiQSTD).*DistSynth;
					
					
					if MedianExist
						
						LQMedPoint=SynthMin+LowQMed.*DistSynth;
						LQQmin=SynthMin+(LowQQ16).*DistSynth;
						LQQmax=SynthMin+(LowQQ84).*DistSynth;
					
						
						HQMedPoint=SynthMin+HiQMed.*DistSynth;
						HQQmin=SynthMin+(HiQQ16).*DistSynth;
						HQQmax=SynthMin+(HiQQ84).*DistSynth;
					
					end
					
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					%now plot it
					for i=1:numel(Names)
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on
						
						if UseMean
							
							%Low Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LQSTDmin(i),LQSTDmin(i),LQSTDmax(i),LQSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LQSTDmin(i),LQSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LQSTDmax(i),LQSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[LQMeanPoint(i),LQMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error LowQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error LowQ ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean low quantil ',indexLabel{i}];
							
							
							
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[HQSTDmin(i),HQSTDmin(i),HQSTDmax(i),HQSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[HQSTDmin(i),HQSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[HQSTDmax(i),HQSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[HQMeanPoint(i),HQMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error HiwQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error HiQ ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean high quantil ',indexLabel{i}];
							
							
						end
						
						
						if UseMed&MedianExist
							
							%Low Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LQQmin(i),LQQmin(i),LQQmax(i),LQQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LQQmin(i),LQQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LQQmax(i),LQQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[LQMedPoint(i),LQMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16 LowQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84 LowQ ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median low quantil ',indexLabel{i}];
							
							
							
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[HQQmin(i),HQQmin(i),HQQmax(i),HQQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[HQQmin(i),HQQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[HQQmax(i),HQQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[HQMedPoint(i),HQMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16 HiwQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84 HiQ ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median high quantil ',indexLabel{i}];
							
							
						end
						
						
						if UseOrg
							%low quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[LQPoint(i),LQPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. N-quantil low ',indexLabel{i}];
							
							%hi quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[HQPoint(i),HQPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. N-quantil high ',indexLabel{i}];
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test: ',DisplayName]);
					else
						theTit=title(['N-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('Number of Earthquakes ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				
				case 'CompLow'
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						NtestNow=Results.(['Ntest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=NtestNow.LogLikeModel;
						SynthMin(i)=min(NtestNow.LogLikeModel);
						SynthMax(i)=max(NtestNow.LogLikeModel);
						LowQScore(i)=NtestNow.QLowMonte(1);
						LowQMean(i)=NtestNow.QScoreLow;
						LowQSTD(i)=NtestNow.QScoreLow_std_dev;
						
						%Medians are not present in old Result calculations
						try
							LowQMed(i)=NtestNow.QScoreLowMedian;
							LowQQ16(i)=NtestNow.QScoreLow68Per(1);
							LowQQ84(i)=NtestNow.QScoreLow68Per(2);
							
							
						catch
							LowQMed(i)=NaN;
							LowQQ16(i)=NaN;
							LowQQ84(i)=NaN;
														MedianExist=false;
						end
				
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
					
					LQPoint=SynthMin+LowQScore.*DistSynth;
					LQMeanPoint=SynthMin+LowQMean.*DistSynth;
					LQSTDmin=SynthMin+(LowQMean-LowQSTD).*DistSynth;
					LQSTDmax=SynthMin+(LowQMean+LowQSTD).*DistSynth;
					
					if MedianExist
						
						LQMedPoint=SynthMin+LowQMed.*DistSynth;
						LQQmin=SynthMin+(LowQQ16).*DistSynth;
						LQQmax=SynthMin+(LowQQ84).*DistSynth;
																					
					end
					
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					%now plot it
					for i=1:numel(Names)
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on
						
						if UseMean
							
							%Low Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LQSTDmin(i),LQSTDmin(i),LQSTDmax(i),LQSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LQSTDmin(i),LQSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LQSTDmax(i),LQSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[LQMeanPoint(i),LQMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error LowQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error LowQ ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean low quantil ',indexLabel{i}];
							
							
							
							
							
						end
						
						if UseMed&MedianExist
							
							%Low Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[LQQmin(i),LQQmin(i),LQQmax(i),LQQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[LQQmin(i),LQQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[LQQmax(i),LQQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[LQMedPoint(i),LQMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16 LowQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84 LowQ ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median low quantil ',indexLabel{i}];
																																			
							
						end
						
						
						if UseOrg
							%low quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[LQPoint(i),LQPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. N-quantil low ',indexLabel{i}];
							
							
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test: ',DisplayName]);
					else
						theTit=title(['N-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('Number of Earthquakes ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
				
					
					
					
					
				case 'CompHigh'
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						NtestNow=Results.(['Ntest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=NtestNow.LogLikeModel;
						SynthMin(i)=min(NtestNow.LogLikeModel);
						SynthMax(i)=max(NtestNow.LogLikeModel);
						HiQScore(i)=NtestNow.QHighMonte(1);
						HiQMean(i)=NtestNow.QScoreHigh;
						HiQSTD(i)=NtestNow.QScoreHigh_std_dev;
						
						%Medians are not present in old Result calculations
						try
							HiQMed(i)=NtestNow.QScoreHighMedian;
							HiQQ16(i)=NtestNow.QScoreHigh68Per(1);
							HiQQ84(i)=NtestNow.QScoreHigh68Per(2);
							
						catch
							HiQMed(i)=NaN;
							HiQQ16(i)=NaN;
							HiQQ84(i)=NaN;
							MedianExist=false;
						end
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
														
					HQPoint=SynthMin+HiQScore.*DistSynth;
					HQMeanPoint=SynthMin+HiQMean.*DistSynth;
					HQSTDmin=SynthMin+(HiQMean-HiQSTD).*DistSynth;
					HQSTDmax=SynthMin+(HiQMean+HiQSTD).*DistSynth;
					
					if MedianExist
											
						HQMedPoint=SynthMin+HiQMed.*DistSynth;
						HQQmin=SynthMin+(HiQQ16).*DistSynth;
						HQQmax=SynthMin+(HiQQ84).*DistSynth;
					
					end
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					%now plot it
					for i=1:numel(Names)
						
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on;
						
						if UseMean
							
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[HQSTDmin(i),HQSTDmin(i),HQSTDmax(i),HQSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[HQSTDmin(i),HQSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[HQSTDmax(i),HQSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[HQMeanPoint(i),HQMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error HiwQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error HiQ ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean high quantil ',indexLabel{i}];
							
							
						end
						
						if UseMed&MedianExist
							
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[HQQmin(i),HQQmin(i),HQQmax(i),HQQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[HQQmin(i),HQQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[HQQmax(i),HQQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[HQMedPoint(i),HQMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Quantilbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16 HiwQ ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84 HiQ ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median high quantil ',indexLabel{i}];
							
							
						end
						
						if UseOrg
							%hi quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[HQPoint(i),HQPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. N-quantil high ',indexLabel{i}];
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test: ',DisplayName]);
					else
						theTit=title(['N-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('Number of Earthquakes ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
				
					
					
				case 'CompNumber'
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						NtestNow=Results.(['Ntest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=NtestNow.LogLikeModel;
						SynthMin(i)=min(NtestNow.LogLikeModel);
						SynthMax(i)=max(NtestNow.LogLikeModel);
						NumbOrg(i)=NtestNow.LikeCatMonte(1);
						NumbMean(i)=NtestNow.LogLikeCatalog;
						NumbSTD(i)=NtestNow.LogLikeCatalog_std_dev;
						
						%Medians are not present in old Result calculations
						try
							NumMed(i)=NtestNow.LogLikeCatalogMedian;
							NumQ16(i)=NtestNow.LogLikeCatalog68Per(1);
							NumQ84(i)=NtestNow.LogLikeCatalog68Per(2);
												
						catch
							NumMed(i)=NaN;
							NumQ16(i)=NaN;
							NumQ84(i)=NaN;
						
							MedianExist=false;
						end
				
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
														
					
					NumSTDmin=NumbMean-NumbSTD;
					NumSTDmax=NumbMean+NumbSTD;
					
					if MedianExist
						Nummin=NumQ16;
						Nummax=NumQ84;
									
					end
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					
					%now plot it
					for i=1:numel(Names)
						
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},floor(SynthMin(i)):ceil(SynthMax(i)));
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on;
						
						if UseMean
							
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[NumSTDmin(i),NumSTDmin(i),NumSTDmax(i),NumSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[NumSTDmin(i),NumSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[NumSTDmax(i),NumSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[NumbMean(i),NumbMean(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error Number ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error Number ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean Number of Events ',indexLabel{i}];
							
							
						end
						
						if UseMed&MedianExist
							
							%High Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[Nummin(i),Nummin(i),Nummax(i),Nummax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[Nummin(i),Nummin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[Nummax(i),Nummax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[NumMed(i),NumMed(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16 Number ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84 Number ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median Number of Events ',indexLabel{i}];
							
							
						end
						
						if UseOrg
							%hi quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[NumbOrg(i),NumbOrg(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. Number of Events ',indexLabel{i}];
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['N-Test: ',DisplayName]);
					else
						theTit=title(['N-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('Number of Earthquakes ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
				
					
					
			end
			
			if ~MrHolden
				hold off;
			end
			
		
			
			
			
		case 'Stest'
			%get Data:
			if iscell(ResStruct.Name)
				PlotData=Results.(['Stest_',ResStruct.Name{1}]);
			else
				PlotData=Results.(['Stest_',ResStruct.Name]);
			end
			MrHolden=ishold;
			
			%create the strings
			QuantString={};
			if UseOrg
				QuantString={['$$\zeta_o=$$ ',num2str(round(1000*PlotData.QScoreMonte(1))/1000)]};
			end
			
			if UseMean
				QuantString(end+1)={['$$\zeta_m=$$ ',num2str(round(1000*PlotData.QScore)/1000),' +/-',num2str(round(1000*PlotData.QScore_std_dev)/1000)]};
				
			end
			
			if UseMed
				QuantString(end+1)={['$$\zeta_m=$$ ',num2str(round(1000*PlotData.QScoreMedian)/1000),...
					' Q16: ',num2str(round(1000*PlotData.QScore68Per(1))/1000),...
					' Q84: ',num2str(round(1000*PlotData.QScore68Per(2))/1000)]};
				
			end
			
			%define text position
			if PlotData.QScore>0.8
				tpos=[0.05,0.85];
			else
				tpos=[0.65,0.85];
			end
			
			switch ResStruct.PlotType
				case 'Hist'
					hist(PlotData.LogLikeModel,50);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Likelihood model'
					
					if UseOrg
						handle(end+1)=plot([PlotData.LogLikeCatalog,PlotData.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['S-Test: ',DisplayName]);
					else
						theTit=title(['S-Test: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('spatial joint log-likelihood   ');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'CDF'
					handle(1)=plotCDF(PlotData.LogLikeModel, plotAxis,'k');
					set(handle(1),'LineWidth',MainWidth,'Color',Colors);
					hold on;
					xlimer=xlim;
					
					legendentry{1}='Likelihood model';
					
					%now plot the quantil score
					if UseOrg
						handle(end+1)=plot([xlimer(1) PlotData.LogLikeCatalog PlotData.LogLikeCatalog],[PlotData.QScoreMonte(1) PlotData.QScoreMonte(1) 0],...
						'LineStyle','--','Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.25,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['CDF S-Test: ',DisplayName]);
					else
						theTit=title(['CDF S-Test: ',ResStruct.Name]);
					end
					
					
					%format the plot
					xlab=xlabel('spatial joint log-likelihood   ');
					ylab=ylabel('Empirical CDF   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'Score'
					hist(PlotData.QScoreMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QScoreMonte(1),PlotData.QScoreMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScore,PlotData.QScore],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScore-PlotData.QScore_std_dev,PlotData.QScore-PlotData.QScore_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScore+PlotData.QScore_std_dev,PlotData.QScore+PlotData.QScore_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreMedian,PlotData.QScoreMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScore68Per(1),PlotData.QScore68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScore68Per(2),PlotData.QScore68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['S-Test QScore: ',DisplayName]);
					else
						theTit=title(['S-Test QScore: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil Score');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
				case 'CompScore'
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						StestNow=Results.(['Stest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=StestNow.LogLikeModel;
						SynthMin(i)=min(StestNow.LogLikeModel);
						SynthMax(i)=max(StestNow.LogLikeModel);
						QScore(i)=StestNow.QScoreMonte(1);
						QMean(i)=StestNow.QScore;
						QSTD(i)=StestNow.QScore_std_dev;
						
						%Medians are not present in old Result calculations
						try
							QMed(i)=StestNow.QScoreMedian;
							QQ16(i)=StestNow.QScore68Per(1);
							QQ84(i)=StestNow.QScore68Per(2);

							
						catch
							QMed(i)=NaN;
							QQ16(i)=NaN;
							QQ84(i)=NaN;
							MedianExist=false;
						end
						
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
					
					QPoint=SynthMin+QScore.*DistSynth;
					QMeanPoint=SynthMin+QMean.*DistSynth;
					QSTDmin=SynthMin+(QMean-QSTD).*DistSynth;
					QSTDmax=SynthMin+(QMean+QSTD).*DistSynth;
				
					if MedianExist
						QMedPoint=SynthMin+QMed.*DistSynth;
						QQmin=SynthMin+(QQ16).*DistSynth;
						QQmax=SynthMin+(QQ84).*DistSynth;

					
					end
					
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},50);
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					%now plot it
					for i=1:numel(Names)
						
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},50);
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on;
						
						if UseMean
							
							%Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[QSTDmin(i),QSTDmin(i),QSTDmax(i),QSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[QSTDmin(i),QSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[QSTDmax(i),QSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[QMeanPoint(i),QMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean quantil ',indexLabel{i}];
							
							
							
															
						end
						
						
						if UseMed
							
							%Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[QQmin(i),QQmin(i),QQmax(i),QQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[QQmin(i),QQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[QQmax(i),QQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[QMedPoint(i),QMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16: ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84: ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median quantil ',indexLabel{i}];
							
							
							
														
							
						end
						
						
						if UseOrg
							%low quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[QPoint(i),QPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. S-quantil ',indexLabel{i}];
							
							
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['S-Test: ',DisplayName]);
					else
						theTit=title(['S-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('spatial joint log-likelihood   ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
			end
			
			if ~MrHolden
				hold off;
			end
		
		
		
		
			
			
		case 'Mtest'
			
			%get Data:
			if iscell(ResStruct.Name)
				PlotData=Results.(['Mtest_',ResStruct.Name{1}]);
			else
				PlotData=Results.(['Mtest_',ResStruct.Name]);
			end
			MrHolden=ishold;
			
			%create the strings
			QuantString={};
			if UseOrg
				QuantString={['$$\kappa_o=$$ ',num2str(round(1000*PlotData.QScoreMonte(1))/1000)]};
			end
			
			if UseMean
				QuantString(end+1)={['$$\kappa_m=$$ ',num2str(round(1000*PlotData.QScore)/1000),' +/-',num2str(round(1000*PlotData.QScore_std_dev)/1000)]};
				
			end
			
			if UseMed
				QuantString(end+1)={['$$\kappa_m=$$ ',num2str(round(1000*PlotData.QScoreMedian)/1000),...
					' Q16: ',num2str(round(1000*PlotData.QScore68Per(1))/1000),...
					' Q84: ',num2str(round(1000*PlotData.QScore68Per(2))/1000)]};
				
			end
			
			%define text position
			if PlotData.QScore>0.8
				tpos=[0.05,0.85];
			else
				tpos=[0.65,0.85];
			end
			
			switch ResStruct.PlotType
				case 'Hist'
					hist(PlotData.LogLikeModel,50);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Likelihood model'
					
					if UseOrg
						handle(end+1)=plot([PlotData.LogLikeCatalog,PlotData.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['M-Test: ',DisplayName]);
					else
						theTit=title(['M-Test: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('magnitude joint log-likelihood   ');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'CDF'
					handle(1)=plotCDF(PlotData.LogLikeModel, plotAxis,'k');
					set(handle(1),'LineWidth',MainWidth,'Color',Colors);
					hold on;
					xlimer=xlim;
					
					legendentry{1}='Likelihood model';
					
					%now plot the quantil score
					if UseOrg
						handle(end+1)=plot([xlimer(1) PlotData.LogLikeCatalog PlotData.LogLikeCatalog],[PlotData.QScoreMonte(1) PlotData.QScoreMonte(1) 0],...
						'LineStyle','--','Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.25,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['CDF M-Test: ',DisplayName]);
					else
						theTit=title(['CDF M-Test: ',ResStruct.Name]);
					end
					
					
					%format the plot
					xlab=xlabel('magnitude joint log-likelihood   ');
					ylab=ylabel('Empirical CDF   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'Score'
					hist(PlotData.QScoreMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QScoreMonte(1),PlotData.QScoreMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScore,PlotData.QScore],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScore-PlotData.QScore_std_dev,PlotData.QScore-PlotData.QScore_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScore+PlotData.QScore_std_dev,PlotData.QScore+PlotData.QScore_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreMedian,PlotData.QScoreMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScore68Per(1),PlotData.QScore68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScore68Per(2),PlotData.QScore68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['M-Test QScore: ',DisplayName]);
					else
						theTit=title(['M-Test QScore: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil Score');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
				case 'CompScore'
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						StestNow=Results.(['Mtest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=StestNow.LogLikeModel;
						SynthMin(i)=min(StestNow.LogLikeModel);
						SynthMax(i)=max(StestNow.LogLikeModel);
						QScore(i)=StestNow.QScoreMonte(1);
						QMean(i)=StestNow.QScore;
						QSTD(i)=StestNow.QScore_std_dev;
						
						%Medians are not present in old Result calculations
						try
							QMed(i)=StestNow.QScoreMedian;
							QQ16(i)=StestNow.QScore68Per(1);
							QQ84(i)=StestNow.QScore68Per(2);

							
						catch
							QMed(i)=NaN;
							QQ16(i)=NaN;
							QQ84(i)=NaN;
							MedianExist=false;
						end
						
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
					
					QPoint=SynthMin+QScore.*DistSynth;
					QMeanPoint=SynthMin+QMean.*DistSynth;
					QSTDmin=SynthMin+(QMean-QSTD).*DistSynth;
					QSTDmax=SynthMin+(QMean+QSTD).*DistSynth;
				
					if MedianExist
						QMedPoint=SynthMin+QMed.*DistSynth;
						QQmin=SynthMin+(QQ16).*DistSynth;
						QQmax=SynthMin+(QQ84).*DistSynth;

					
					end
					
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},50);
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					%now plot it
					for i=1:numel(Names)
						
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},50);
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on;
						
						if UseMean
							
							%Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[QSTDmin(i),QSTDmin(i),QSTDmax(i),QSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[QSTDmin(i),QSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[QSTDmax(i),QSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[QMeanPoint(i),QMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean quantil ',indexLabel{i}];
							
							
							
															
						end
						
						
						if UseMed
							
							%Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[QQmin(i),QQmin(i),QQmax(i),QQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[QQmin(i),QQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[QQmax(i),QQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[QMedPoint(i),QMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16: ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84: ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median quantil ',indexLabel{i}];
							
							
							
														
							
						end
						
						
						if UseOrg
							%low quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[QPoint(i),QPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. M-quantil ',indexLabel{i}];
							
							
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['M-Test: ',DisplayName]);
					else
						theTit=title(['M-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('magnitude joint log-likelihood   ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
			end
			
			if ~MrHolden
				hold off;
			end
			
			
			
			
			
			
			
			
			
		case 'Ltest'
			%get Data:
			if iscell(ResStruct.Name)
				PlotData=Results.(['Ltest_',ResStruct.Name{1}]);
			else
				PlotData=Results.(['Ltest_',ResStruct.Name]);
			end
			MrHolden=ishold;
			
			%create the strings
			QuantString={};
			if UseOrg
				QuantString={['$$\gamma_o=$$ ',num2str(round(1000*PlotData.QScoreMonte(1))/1000)]};
			end
			
			if UseMean
				QuantString(end+1)={['$$\gamma_m=$$ ',num2str(round(1000*PlotData.QScore)/1000),' +/-',num2str(round(1000*PlotData.QScore_std_dev)/1000)]};
				
			end
			
			if UseMed
				QuantString(end+1)={['$$\gamma_m=$$ ',num2str(round(1000*PlotData.QScoreMedian)/1000),...
					' Q16: ',num2str(round(1000*PlotData.QScore68Per(1))/1000),...
					' Q84: ',num2str(round(1000*PlotData.QScore68Per(2))/1000)]};
				
			end
			
			
			%define text position
			if PlotData.QScore>0.8
				tpos=[0.05,0.85];
			else
				tpos=[0.65,0.85];
			end
			
			switch ResStruct.PlotType
				case 'Hist'
					hist(PlotData.LogLikeModel,50);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Likelihood model'
					
					if UseOrg
						handle(end+1)=plot([PlotData.LogLikeCatalog,PlotData.LogLikeCatalog],[0,ylimer(2)],'LineStyle','--',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['L-Test: ',DisplayName]);
					else
						theTit=title(['L-Test: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('joint log-likelihood   ');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'CDF'
					handle(1)=plotCDF(PlotData.LogLikeModel, plotAxis,'k');
					set(handle(1),'LineWidth',MainWidth,'Color',Colors);
					hold on;
					xlimer=xlim;
					
					legendentry{1}='Likelihood model';
					
					%now plot the quantil score
					if UseOrg
						handle(end+1)=plot([xlimer(1) PlotData.LogLikeCatalog PlotData.LogLikeCatalog],[PlotData.QScoreMonte(1) PlotData.QScoreMonte(1) 0],...
						'LineStyle','--','Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.25,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['CDF L-Test: ',DisplayName]);
					else
						theTit=title(['CDF L-Test: ',ResStruct.Name]);
					end
					
					
					%format the plot
					xlab=xlabel('joint log-likelihood   ');
					ylab=ylabel('Empirical CDF   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'Score'
					hist(PlotData.QScoreMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QScoreMonte(1),PlotData.QScoreMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScore,PlotData.QScore],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScore-PlotData.QScore_std_dev,PlotData.QScore-PlotData.QScore_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScore+PlotData.QScore_std_dev,PlotData.QScore+PlotData.QScore_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreMedian,PlotData.QScoreMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScore68Per(1),PlotData.QScore68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScore68Per(2),PlotData.QScore68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['L-Test QScore: ',DisplayName]);
					else
						theTit=title(['L-Test QScore: ',ResStruct.Name]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil Score');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
				
				
				case 'CompScore'
					Names=ResStruct.Name;
										
					redOne=[0.8 0.5 0.5];
					greyHound=[0.88 0.88 0.88];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						LtestNow=Results.(['Ltest_',Names{i}]);
												
						indexLabel(i)=ModDispName(i);
						LogLiker{i}=LtestNow.LogLikeModel;
						SynthMin(i)=min(LtestNow.LogLikeModel);
						SynthMax(i)=max(LtestNow.LogLikeModel);
						QScore(i)=LtestNow.QScoreMonte(1);
						QMean(i)=LtestNow.QScore;
						QSTD(i)=LtestNow.QScore_std_dev;
						
						%Medians are not present in old Result calculations
						try
							QMed(i)=LtestNow.QScoreMedian;
							QQ16(i)=LtestNow.QScore68Per(1);
							QQ84(i)=LtestNow.QScore68Per(2);

							
						catch
							QMed(i)=NaN;
							QQ16(i)=NaN;
							QQ84(i)=NaN;
							MedianExist=false;
						end
				
					end
					
					handle=[];
					legendentry={};
					
					%additional needed values
					DistSynth=SynthMax-SynthMin;
					
					QPoint=SynthMin+QScore.*DistSynth;
					QMeanPoint=SynthMin+QMean.*DistSynth;
					QSTDmin=SynthMin+(QMean-QSTD).*DistSynth;
					QSTDmax=SynthMin+(QMean+QSTD).*DistSynth;
					
					if MedianExist
						QMedPoint=SynthMin+QMed.*DistSynth;
						QQmin=SynthMin+(QQ16).*DistSynth;
						QQmax=SynthMin+(QQ84).*DistSynth;

					
					end
					
					
					if MiniHist
						%Determine maximum peak of all hists
						for i=1:numel(Names)
							[countHist{i} bins{i}]=hist(LogLiker{i},50);
							PeakMax(i)=max(countHist{i});
							PeakMin(i)=min(countHist{i});
						end
						
						MaxPeak=max(PeakMax);
						MinPeak=min(PeakMin);
						YBound=[0, MaxPeak];
					end
					
					
					%now plot it
					for i=1:numel(Names)
						
						%first plot the bar with the synthetic likelihoods
						if ~MiniHist
							SynthBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							SynthBoxX=[SynthMin(i),SynthMin(i),SynthMax(i),SynthMax(i)];
							
							handle(end+1)=fill(SynthBoxX,SynthBoxY,greyHound);
						else
							%[countHist bins]=hist(LogLiker{i},50);
							[xData yData] = BarManFiller(countHist{i},bins{i},i-0.2,MiniHistHigh,YBound);
							handle(end+1)=fill(xData,yData,greyHound);
						end
						
						set(handle(end),'EdgeColor',greyHound-0.25);
						legendentry{end+1}=['Synthetics ',indexLabel{i}];
						
						hold on
						
						if UseMean
							
							%Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[QSTDmin(i),QSTDmin(i),QSTDmax(i),QSTDmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[QSTDmin(i),QSTDmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[QSTDmax(i),QSTDmax(i)];
							
							MeanBarY=[i-0.2,i+0.2];
							MeanBarX=[QMeanPoint(i),QMeanPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['lower error ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','r','LineWidth',1);
							legendentry{end+1}=['upper error ',indexLabel{i}];
							
							handle(end+1)=plot(MeanBarX,MeanBarY);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['mean quantil ',indexLabel{i}];
							
							
							
														
							
						end
						
						
						if UseMed&MedianExist
							
							%Quantil 
							ErrorBoxY=[i-0.2,i+0.2,i+0.2,i-0.2];
							ErrorBoxX=[QQmin(i),QQmin(i),QQmax(i),QQmax(i)];
							
							LowErrorBarY=[i-0.2,i+0.2];
							LowErrorBarX=[QQmin(i),QQmin(i)];
							
							HiErrorBarY=[i-0.2,i+0.2];
							HiErrorBarX=[QQmax(i),QQmax(i)];
							
							MedBarY=[i-0.2,i+0.2];
							MedBarX=[QMedPoint(i),QMedPoint(i)];
							
							%plot it
							if ~MiniHist
								handle(end+1)=fill(ErrorBoxX,ErrorBoxY,redOne);
								set(handle(end),'LineStyle','none');
								legendentry{end+1}=['Errorbox ',indexLabel{i}];
							end
							
							handle(end+1)=plot(LowErrorBarX,LowErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q16: ',indexLabel{i}];
							handle(end+1)=plot(HiErrorBarX,HiErrorBarY);
							set(handle(end),'LineStyle','--','Color','b','LineWidth',1);
							legendentry{end+1}=['Q84: ',indexLabel{i}];
							
							handle(end+1)=plot(MedBarX,MedBarY);
							set(handle(end),'LineStyle','-','Color','b','LineWidth',MainWidth);
							legendentry{end+1}=['median quantil ',indexLabel{i}];
							
							
							
														
							
						end
						
						
						if UseOrg
							%low quantil
							OrgBarY=[i-0.2,i+0.2];
							OrgBarX=[QPoint(i),QPoint(i)];
							handle(end+1)=plot(OrgBarX,OrgBarY);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org.L-quantil ',indexLabel{i}];
							
							
							
							
						end
						
					
					end
					
					
					%set x axis
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					xlim(xlimer);
					
					%set y axis
					ModelsTested=numel(Names);
					ylim([0 ModelsTested+1]);
					
					if ~isempty(DisplayName)
						theTit=title(['L-Test: ',DisplayName]);
					else
						theTit=title(['L-Test']);
					end
					
					
					
					%format the plot
					%ylabel('Models   ');
					xlab=xlabel('joint log-likelihood  ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					indexer=0:ModelsTested;
					indexLabel={'',indexLabel{:}};
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
			end
					
					
						
			if ~MrHolden
				hold off;
			end
		
		
		
		
		
		
		
		case 'Rtest'
			%get Data:
			PlotData=Results.(['Rtest_',ResStruct.Name{1},'_',ResStruct.Name{2}]);
			MrHolden=ishold;
			
			%create the strings
			QuantString={};
			if UseOrg
				QuantString={['$$\alpha_o=$$ ',num2str(round(1000*PlotData.QScoreMonte(1))/1000)]};
			end
			
			if UseMean
				QuantString(end+1)={['$$\alpha_m=$$ ',num2str(round(1000*PlotData.QScoreRatioAB)/1000),' +/-',num2str(round(1000*PlotData.QScoreRatioAB_std_dev)/1000)]};
				
			end
			
			
			if UseMed
				QuantString(end+1)={['$$\alpha_m=$$ ',num2str(round(1000*PlotData.QScoreRatioABMedian)/1000),...
					' Q16: ',num2str(round(1000*PlotData.QScoreRatioAB68Per(1))/1000),...
					' Q84: ',num2str(round(1000*PlotData.QScoreRatioAB68Per(2))/1000)]};
				
			end
			
			%define text position
			if PlotData.QScoreRatioAB>0.8
				tpos=[0.05,0.85];
			else
				tpos=[0.65,0.85];
			end
			
			switch ResStruct.PlotType
				case 'Hist'
					hist(PlotData.Ratio_AB,50);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Ratio Likelihood models'
					
					if UseOrg
						handle(end+1)=plot([PlotData.ObsRatio_AB,PlotData.ObsRatio_AB],[0,ylimer(2)],'LineStyle','--',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['R-Test: ',DisplayName]);
					else
						theTit=title(['R-Test: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Simulated ratio of log-likelihood R   ');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'CDF'
					handle(1)=plotCDF(PlotData.Ratio_AB, plotAxis,'k');
					set(handle(1),'LineWidth',MainWidth,'Color',Colors);
					hold on;
					xlimer=xlim;
					
					legendentry{1}='Ratio Likelihood models';
					
					%now plot the quantil score
					if UseOrg
						handle(end+1)=plot([xlimer(1) PlotData.ObsRatio_AB PlotData.ObsRatio_AB],[PlotData.QScoreMonte(1) PlotData.QScoreMonte(1) 0],...
						'LineStyle','--','Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					if UseMean
						%not available
					end
					
					
					if UseMed
						%not available
					end
					
					
					if ResStruct.PrintText
						leText=text(0.65,0.25,QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['CDF R-Test: ',DisplayName]);
					else
						theTit=title(['CDF R-Test: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					%format the plot
					xlab=xlabel('Simulated ratio of log-likelihood R   ');
					ylab=ylabel('Empirical CDF   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'Score'
					hist(PlotData.QScoreMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='QuantilScore'
					
					if UseOrg
						handle(end+1)=plot([PlotData.QScoreMonte(1),PlotData.QScoreMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Quantil org. Catalog'
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.QScoreRatioAB,PlotData.QScoreRatioAB],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Quantil Score'
						handle(end+1)=plot([PlotData.QScoreRatioAB-PlotData.QScoreRatioAB_std_dev,PlotData.QScoreRatioAB-PlotData.QScoreRatioAB_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Quantil Score'
						handle(end+1)=plot([PlotData.QScoreRatioAB+PlotData.QScoreRatioAB_std_dev,PlotData.QScoreRatioAB+PlotData.QScoreRatioAB_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Quantil Score'
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.QScoreRatioABMedian,PlotData.QScoreRatioABMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Quantil Score'
						handle(end+1)=plot([PlotData.QScoreRatioAB68Per(1),PlotData.QScoreRatioAB68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 Quantil Score'
						handle(end+1)=plot([PlotData.QScoreRatioAB68Per(2),PlotData.QScoreRatioAB68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 Quantil Score'
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['R-Test Q-Ratio-Score: ',DisplayName]);
					else
						theTit=title(['R-Test Q-Ratio-Score: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Quantil Ratio Score');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
			end
			
			if ~MrHolden
				hold off;
			end	
		
		
		
		
		
		
		case 'Ttest'
			%normaly more than one model is used so Names has to be a cell, first one is the reference
			Names=ResStruct.Name;
			MrHolden=ishold;
			
			switch ResStruct.PlotType
				case 'HistIConf'
					redOne=[0.8 0.5 0.5];
					greenOne=[0.5 0.7 0.5];
					greyHound=[0.85 0.85 0.85];
					
					MedianExist=true;
					
					%create the data
					for i=2:numel(Names)
						TtestNow=Results.(['Ttest_',Names{1},'_',Names{i}]);
						
						indexer(i-1)=2*(i-1);
						indexLabel(i-1)=ModDispName(i);
						Ival(i-1)=TtestNow.InAB(1)
						IvalOrg(i-1)=TtestNow.InABMonte(1);
						IvalSTD(i-1)=TtestNow.InAB_Mostd;
						IConf(i-1)=TtestNow.IConf(1);
						IConfOrg(i-1)=TtestNow.IConfMonte(1);
						IConfSTD(i-1)=TtestNow.IConf_std(1);
						
						%Medians are not present in old Result calculations
						try
							IvalMed(i-1)=TtestNow.InABMedian;
							IvalQ16(i-1)=TtestNow.InAB68Per(1);
							IvalQ84(i-1)=TtestNow.InAB68Per(2);
							
							IConfMed(i-1)=TtestNow.IConfMedian;
							IConfQ16(i-1)=TtestNow.IConf68Per(1);
							IConfQ84(i-1)=TtestNow.IConf68Per(2);

							
						catch
							IvalMed(i-1)=NaN;
							IvalQ16(i-1)=NaN;
							IvalQ84(i-1)=NaN;
							IConfMed(i-1)=NaN;
							IConfQ16(i-1)=NaN;
							IConfQ84(i-1)=NaN;
							
							MedianExist=false;
						end
						
						
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if UseMean
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];
							
							MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MeanboxX=[Ival(i)-IvalSTD(i),Ival(i)+IvalSTD(i),...
								  Ival(i)+IvalSTD(i),Ival(i)-IvalSTD(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)-IConf(i)+IConfSTD(i),...
								  Ival(i)-IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[Ival(i)+IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)+IConf(i)-IConfSTD(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
							legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([Ival(i),Ival(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)-IConf(i),Ival(i)-IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)+IConf(i),Ival(i)+IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end
							
						elseif 	UseMed&MedianExist
							%Difficult part because Q16 and Q84 are always fixed points, not differences
							%So check if it is right
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalMed(i)-IConfQ84(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)-IConfQ84(i)];
							%both Q84, because it is the larger value
								  
							MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MedboxX=[IConfQ16(i),IConfQ84(i),...
								  IConfQ84(i),IConfQ16(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[IvalMed(i)-IConfQ84(i),IvalMed(i)-IConfQ16(i),...
								  IvalMed(i)-IConfQ16(i),IvalMed(i)-IConfQ84(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[IvalMed(i)+IConfQ16(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)+IConfQ16(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MedboxX,MedboxY,greenOne);
							legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([IvalMed(i),IvalMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)-IConfMed(i),IvalMed(i)-IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q16 Error ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)+IConfMed(i),IvalMed(i)+IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q84 Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end
						
						else
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalOrg(i)-IConfOrg(i),IvalOrg(i)+IConfOrg(i),...
								  IvalOrg(i)+IConfOrg(i),IvalOrg(i)-IConfOrg(i)];
							
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
						
						end
					end
					
							
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
					
					if ~isempty(DisplayName)
						theTit=title(['T-Test Information Gain: ',DisplayName]);
					else
						theTit=title(['T-Test Information Gain Reference: ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I  ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
			
				case 'HistI'
					%Median not supported here currently, may add it later
					redOne=[0.8 0.5 0.5];
					blueOne=[0.5 0.5 0.7];
					%create the data
					
					for i=2:numel(Names)
						TtestNow=Results.(['Ttest_',Names{1},'_',Names{i}]);
						
						indexer(i-1)=2*(i-1);
						indexLabel(i-1)=ModDispName(i);
						Ival(i-1)=TtestNow.InAB(1);
						ISTD(i-1)=sqrt(TtestNow.VarianceI(1));
						
				
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						
						ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
						ErrorboxY=[Ival(i)-ISTD(i),Ival(i)-ISTD(i),Ival(i)+ISTD(i),Ival(i)+ISTD(i)];
						if Ival(i)>0
							handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
						else
							handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
						end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
						hold on;
						handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Ival(i),Ival(i)]);
						set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
						legendentry{end+1}=['Information gain ',indexLabel{i}];
						
					
					end
					
					%plot zeroline									
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.2;
					xlimer(2)=xlimer(2)+0.2;
					ttable=TtestNow.ttable;
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.9 0.9 0.9],'LineWidth',0.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
			
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['T-Test: ',DisplayName]);
					else
						theTit=title(['T-Test Reference: ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					ylab=ylabel('Information Gain I  ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'HistT'
					redOne=[0.8 0.5 0.5];
					blueOne=[0.5 0.5 0.7];
					
					MedianExist=true;
					
					%create the data
					for i=2:numel(Names)
						TtestNow=Results.(['Ttest_',Names{1},'_',Names{i}]);
						
						indexer(i-1)=2*(i-1);
						indexLabel(i-1)=ModDispName(i);
						Tval(i-1)=TtestNow.TValMonte(1);
						TvalMean(i-1)=TtestNow.T;
						TvalSTD(i-1)=TtestNow.T_std_dev;
						
						%Medians are not present in old Result calculations
						try
							TMed(i-1)=TtestNow.TMedian;
							TQ16(i-1)=TtestNow.T68Per(1);
							TQ84(i-1)=TtestNow.T68Per(2);
							
							
							
						catch
							TMed(i-1)=NaN;
							TQ16(i-1)=NaN;
							TQ84(i-1)=NaN;
							
							
							MedianExist=false;
						end
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if UseMean
							ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorboxY=[TvalMean(i)-TvalSTD(i),TvalMean(i)-TvalSTD(i),TvalMean(i)+TvalSTD(i),TvalMean(i)+TvalSTD(i)];
							if TvalMean(i)>0
								handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
							else
								handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
							end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							hold on;
							handle(end+1)=plot([indexer(i)-0.25,indexer(i)+0.25],[TvalMean(i),TvalMean(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['mean t-value ',indexLabel{i}];
							
						end
						
						
						if UseMed&MedianExist
							ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorboxY=[TQ16(i),TQ16(i),TQ84(i),TQ84(i)];
							if TvalMean(i)>0
								handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
							else
								handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
							end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							hold on;
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[TMed(i),TMed(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['mean t-value ',indexLabel{i}];
							
						end
						
						if UseOrg
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Tval(i),Tval(i)]);
							set(handle(end),'LineStyle',':','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. t-value ',indexLabel{i}];
							hold on;
						end
						
					
					end
					
					
					%plot the ttable it should be equal for each test anyway
					TtestNow=Results.(['Ttest_',Names{1},'_',Names{2}]);
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					ttable=TtestNow.ttable;
					handle(end+1)=plot([xlimer(1);xlimer(2)],[ttable;ttable])
					set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
					xlim(xlimer);
					legendentry{end+1}='significant t-value';
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.9 0.9 0.9],'LineWidth',0.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
					
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[-ttable;-ttable])
					set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
					xlim(xlimer);
					legendentry{end+1}='significant t-value';
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['T-Test: ',DisplayName]);
					else
						theTit=title(['T-Test Reference: ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					ylab=ylabel('T-value ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
						
			
					
					
				case 'ScoreT'	
					PlotData=Results.(['Ttest_',Names{1},'_',Names{2}]);
					
					%create the strings
					QuantString={['$$T_{table}=$$ ',num2str(round(1000*PlotData.ttable)/1000)]};
					
					if UseOrg
						QuantString(end+1)={['$$T_o=$$ ',num2str(round(1000*PlotData.TValMonte(1))/1000)]};
					end
					
					if UseMean
						QuantString(end+1)={['$$T_m=$$ ',num2str(round(1000*PlotData.T)/1000),' +/-',num2str(round(1000*PlotData.T_std_dev)/1000)]};
						
					end
					
					if UseMed
						QuantString(end+1)={['$$T_m=$$ ',num2str(round(1000*PlotData.TMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.T68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.T68Per(2))/1000)]};
						
					end
					
					
					%define text position
					if PlotData.T>5
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.TValMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='T-values';
					
					handle(end+1)=plot([PlotData.ttable,PlotData.ttable],[0,ylimer(2)],'LineStyle','-',...
					'Color','k','LineWidth',3);
					legendentry{end+1}='T-value table';
					
					
					if UseOrg
						handle(end+1)=plot([PlotData.TValMonte(1),PlotData.TValMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='T-value org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.T,PlotData.T],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean T-value';
						handle(end+1)=plot([PlotData.T-PlotData.T_std_dev,PlotData.T-PlotData.T_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std T-value';
						handle(end+1)=plot([PlotData.T+PlotData.T_std_dev,PlotData.T+PlotData.T_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std T-value';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.TMedian,PlotData.TMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='mean T-value';
						handle(end+1)=plot([PlotData.T68Per(1),PlotData.T68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='-std T-value';
						handle(end+1)=plot([PlotData.T68Per(2),PlotData.T68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='+std T-value';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['T-values: ',DisplayName]);
					else
						theTit=title(['T-values: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('T-value');
					ylab=ylabel('Number of occurence   ');
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
				case 'ScoreI'	
					
					PlotData=Results.(['Ttest_',Names{1},'_',Names{2}]);
					
					%create the strings
					QuantString={};
					
					if UseOrg
						QuantString(end+1)={['$$I_o=$$ ',num2str(round(1000*PlotData.InABMonte(1))/1000)]};
					end
					
					if UseMean
						QuantString(end+1)={['$$I_m=$$ ',num2str(round(1000*PlotData.InAB)/1000),' +/-',num2str(round(1000*PlotData.InAB_Mostd)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$I_m=$$ ',num2str(round(1000*PlotData.InABMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.InAB68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.InAB68Per(2))/1000)]};						
					end
					
					
					%define text position
					if PlotData.InAB>1
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.InABMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Information Gain';
					
										
					
					if UseOrg
						handle(end+1)=plot([PlotData.InABMonte(1),PlotData.InABMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Information Gain org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.InAB,PlotData.InAB],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Information Gain';
						handle(end+1)=plot([PlotData.InAB-PlotData.InAB_Mostd,PlotData.InAB-PlotData.InAB_Mostd],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Information Gain';
						handle(end+1)=plot([PlotData.InAB+PlotData.InAB_Mostd,PlotData.InAB+PlotData.InAB_Mostd],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Information Gain';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.InABMedian,PlotData.InABMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Information Gain';
						handle(end+1)=plot([PlotData.InAB68Per(1),PlotData.InAB68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='Q16 Information Gain';
						handle(end+1)=plot([PlotData.InAB68Per(2),PlotData.InAB68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='Q84 Information Gain';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['Information Gain: ',DisplayName]);
					else
						theTit=title(['Information Gain: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
			end	
			
			
			
			if ~MrHolden
				hold off;
			end	
			
		
			
			
		case 'TRtest'
			%normaly more than one model is used so Names has to be a cell, first one is the reference
			Names=ResStruct.Name;
			MrHolden=ishold;
			
			switch ResStruct.PlotType
				case 'HistIConf'
					redOne=[0.8 0.5 0.5];
					greenOne=[0.5 0.7 0.5];
					greyHound=[0.85 0.85 0.85];
					
					%They do not exist in every calc run, so they need default data
					MaxGain=NaN;
					IGainPerc=NaN;
					IConfPerc=NaN;
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1)
						IvalOrg(i)=TtestNow.InABMonte(1);
						IvalSTD(i)=TtestNow.InAB_Mostd;
						IConf(i)=TtestNow.IConf(1);
						IConfOrg(i)=TtestNow.IConfMonte(1);
						IConfSTD(i)=TtestNow.IConf_std(1);
						
						try
							MaxGain(i)=TtestNow.MaxGainMonte(1);
							IGainPerc(i)=TtestNow.IGainPercMonte(1);
							IConfPerc(i)=TtestNow.IConfPercMonte(1);
						end
						
						
						%Medians are not present in old Result calculations
						try
							IvalMed(i)=TtestNow.InABMedian;
							IvalQ16(i)=TtestNow.InAB68Per(1);
							IvalQ84(i)=TtestNow.InAB68Per(2);
							
							IConfMed(i)=TtestNow.IConfMedian;
							IConfQ16(i)=TtestNow.IConf68Per(1);
							IConfQ84(i)=TtestNow.IConf68Per(2);

							
						catch
							IvalMed(i)=NaN;
							IvalQ16(i)=NaN;
							IvalQ84(i)=NaN;
							IConfMed(i)=NaN;
							IConfQ16(i)=NaN;
							IConfQ84(i)=NaN;
							
							MedianExist=false;
						end
					end
					
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
					
						if BarTrail
							%Dotted line from 0 to OrgVal
							TrailerX=[0, IvalOrg(i)];
							TrailerY=[indexer(i), indexer(i)];
							
							handle(end+1)=plot(TrailerX,TrailerY);
							
							set(handle(end),'LineStyle',':','Color','k','LineWidth',2);
							legendentry{end+1}=['TrailLine ',indexLabel{i}];
							hold on;
						end
						
					
						if UseMean
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];
							
							MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MeanboxX=[Ival(i)-IvalSTD(i),Ival(i)+IvalSTD(i),...
								  Ival(i)+IvalSTD(i),Ival(i)-IvalSTD(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)-IConf(i)+IConfSTD(i),...
								  Ival(i)-IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[Ival(i)+IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)+IConf(i)-IConfSTD(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
							legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([Ival(i),Ival(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)-IConf(i),Ival(i)-IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)+IConf(i),Ival(i)+IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end
						
						elseif 	UseMed&MedianExist
							%Difficult part because Q16 and Q84 are always fixed points, not differences
							%So check if it is right
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalMed(i)-IConfQ84(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)-IConfQ84(i)];
							%both Q84, because it is the larger value
								  
							MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MedboxX=[IConfQ16(i),IConfQ84(i),...
								  IConfQ84(i),IConfQ16(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[IvalMed(i)-IConfQ84(i),IvalMed(i)-IConfQ16(i),...
								  IvalMed(i)-IConfQ16(i),IvalMed(i)-IConfQ84(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[IvalMed(i)+IConfQ16(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)+IConfQ16(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MedboxX,MedboxY,greenOne);
							legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([IvalMed(i),IvalMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)-IConfMed(i),IvalMed(i)-IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q16 Error ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)+IConfMed(i),IvalMed(i)+IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q84 Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end	
							
						else
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalOrg(i)-IConfOrg(i),IvalOrg(i)+IConfOrg(i),...
								  IvalOrg(i)+IConfOrg(i),IvalOrg(i)-IConfOrg(i)];
							
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
						
						end
						
						
						
						
						
					end
					
							
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
					
					%The maximum line is used in both versions, but only the original one, they should not deviate to much I hope 
					if ~isnan(MaxGain(1))
						handle(end+1)=plot([MaxGain(1);MaxGain(1)],[ylimer(1);ylimer(2)])
						set(handle(end),'LineStyle','-','Color','b','LineWidth',2);
						legendentry{end+1}='Maximum Possible information gain';								
					end
					
					
					if ~isempty(DisplayName)
						theTit=title(['TRand-Test Information Gain: ',DisplayName]);
					else
						theTit=title(['TRand-Test Information Gain Reference: Uniform']);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I  ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'HistINoConf'
					redOne=[0.8 0.5 0.5];
					greenOne=[0.5 0.7 0.5];
					greyHound=[0.85 0.85 0.85];
					
					%They do not exist in every calc run, so they need default data
					MaxGain=NaN;
					IGainPerc=NaN;
					IConfPerc=NaN;
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1)
						IvalOrg(i)=TtestNow.InABMonte(1);
						IvalSTD(i)=TtestNow.InAB_Mostd;
						%IConf(i)=TtestNow.IConf(1);
						%IConfOrg(i)=TtestNow.IConfMonte(1);
						%IConfSTD(i)=TtestNow.IConf_std(1);
						
						try
							MaxGain(i)=TtestNow.MaxGainMonte(1);
							IGainPerc(i)=TtestNow.IGainPercMonte(1);
						%	IConfPerc(i)=TtestNow.IConfPercMonte(1);
						end
						
						
						%Medians are not present in old Result calculations
						try
							IvalMed(i)=TtestNow.InABMedian;
							IvalQ16(i)=TtestNow.InAB68Per(1);
							IvalQ84(i)=TtestNow.InAB68Per(2);
							
						%	IConfMed(i)=TtestNow.IConfMedian;
						%	IConfQ16(i)=TtestNow.IConf68Per(1);
						%	IConfQ84(i)=TtestNow.IConf68Per(2);

							
						catch
							IvalMed(i)=NaN;
							IvalQ16(i)=NaN;
							IvalQ84(i)=NaN;
						%	IConfMed(i)=NaN;
						%	IConfQ16(i)=NaN;
						%	IConfQ84(i)=NaN;
							
							MedianExist=false;
						end
					end
					
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if BarTrail
							%Dotted line from 0 to OrgVal
							TrailerX=[0, IvalOrg(i)];
							TrailerY=[indexer(i), indexer(i)];
							
							handle(end+1)=plot(TrailerX,TrailerY);
							
							set(handle(end),'LineStyle',':','Color','k','LineWidth',2);
							legendentry{end+1}=['TrailLine ',indexLabel{i}];
							hold on;
						end
					
					
						if UseMean
							MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MeanboxX=[Ival(i)-IvalSTD(i),Ival(i)+IvalSTD(i),...
								  Ival(i)+IvalSTD(i),Ival(i)-IvalSTD(i)];
							  
							hold on
						
							%first the boxes
							handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
							legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
								
														
							%now the lines
							handle(end+1)=plot([Ival(i),Ival(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
															
							end
						
						elseif 	UseMed&MedianExist
							%Difficult part because Q16 and Q84 are always fixed points, not differences
							%So check if it is right								  
							MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MedboxX=[IvalQ16(i),IvalQ84(i),...
								  IvalQ84(i),IvalQ16(i)];
							hold on
								
								  
							handle(end+1)=fill(MedboxX,MedboxY,greenOne);
							legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
								
							
							%now the lines
							handle(end+1)=plot([IvalMed(i),IvalMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
																				
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
																								
							end	
							
						else
						
							
							handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							hold on;
							
						end
						
						
						
						
						
					end
					
							
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
					
					%The maximum line is used in both versions, but only the original one, they should not deviate to much I hope 
					if ~isnan(MaxGain(1))
						handle(end+1)=plot([MaxGain(1);MaxGain(1)],[ylimer(1);ylimer(2)])
						set(handle(end),'LineStyle','-','Color','b','LineWidth',2);
						legendentry{end+1}='Maximum Possible information gain';								
					end
					
					
					if ~isempty(DisplayName)
						theTit=title(['TRand-Test Information Gain: ',DisplayName]);
					else
						theTit=title(['TRand-Test Information Gain Reference: Uniform']);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I  ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					%Sometimes the box was turned off, no clue why
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel,'Box','on');
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'HistIPerc'
					redOne=[0.8 0.5 0.5];
					greenOne=[0.5 0.7 0.5];
					greyHound=[0.85 0.85 0.85];
					
					%Same as the HistIConf but uses percents for the values instead of Info gain.
					
					%They do not exist in every calc run, so they need default data
					MaxGain=NaN;
					IGainPerc=NaN;
					IConfPerc=NaN;
					MaxGainOrg=NaN;
					IGainPercOrg=NaN;
					IConfPercOrg=NaN;
					MaxGainSTD=NaN;
					IGainPercSTD=NaN;
					IConfPercSTD=NaN;
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1)
						IvalOrg(i)=TtestNow.InABMonte(1);
						IvalSTD(i)=TtestNow.InAB_Mostd;
						IConf(i)=TtestNow.IConf(1);
						IConfOrg(i)=TtestNow.IConfMonte(1);
						IConfSTD(i)=TtestNow.IConf_std(1);
						
						try
							MaxGainOrg(i)=TtestNow.MaxGainMonte(1);
							IGainPercOrg(i)=TtestNow.IGainPercMonte(1);
							IConfPercOrg(i)=TtestNow.IConfPercMonte(1);
							MaxGain(i)=TtestNow.MaxGain(1);
							IGainPerc(i)=TtestNow.IGainPerc(1);
							IConfPerc(i)=TtestNow.IConfPerc(1);
							MaxGainSTD(i)=TtestNow.MaxGain_Mostd(1);
							IGainPercSTD(i)=TtestNow.IGainPerc_Mostd(1);
							IConfPercSTD(i)=TtestNow.IConfPerc_Mostd(1);
						end
						
						%Medians are not present in old Result calculations
						try
							IvalMed(i)=TtestNow.InABMedian;
							IvalQ16(i)=TtestNow.InAB68Per(1);
							IvalQ84(i)=TtestNow.InAB68Per(2);
							
							IConfMed(i)=TtestNow.IConfMedian;
							IConfQ16(i)=TtestNow.IConf68Per(1);
							IConfQ84(i)=TtestNow.IConf68Per(2);

							IGainPercMed(i)=TtestNow.IGainPercMedian;
							IGainPercQ16(i)=TtestNow.IGainPerc68Per(1);
							IGainPercQ84(i)=TtestNow.IGainPerc68Per(2);
							
							IConfPercMed(i)=TtestNow.IConfPercMedian;
							IConfPercQ16(i)=TtestNow.ICOnfPerc68Per(1);
							IConfPercQ84(i)=TtestNow.ICOnfPerc68Per(2);
							
							MaxGainMed(i)=TtestNow.MaxGainMedian;
							MaxGainQ16(i)=TtestNow.MaxGain68Per(1);
							MaxGainQ84(i)=TtestNow.MaxGain68Per(2);
							
							
						catch
							IvalMed(i)=NaN;
							IvalQ16(i)=NaN;
							IvalQ84(i)=NaN;
							
							IConfMed(i)=NaN;
							IConfQ16(i)=NaN;
							IConfQ84(i)=NaN;
							
							IGainPercMed(i)=NaN;
							IGainPercQ16(i)=NaN;
							IGainPercQ84(i)=NaN;
							
							IConfPercMed(i)=NaN;
							IConfPercQ16(i)=NaN;
							IConfPercQ84(i)=NaN;
							
							MaxGainMed(i)=NaN;
							MaxGainQ16(i)=NaN;
							MaxGainQ84(i)=NaN;
							
							MedianExist=false;
						end
						
					end
					
					
					%Set the right parameters
					if all(~isnan(IConfPercSTD))
						IConf=IConfPerc;
						Ival=IGainPerc;
						
						IConfOrg=IConfPercOrg;
						IvalOrg=IGainPercOrg;
						
						IConfSTD=IConfPercSTD;
						IvalSTD=IGainPercSTD;
						
						MaxGainLine=100;
						
						RawTitle1='TRand-Test Information Gain: ';
						RawTitle2='TRand-Test Information Gain Reference: Uniform';
						
						%No if needed here, they set to NaN anyway
						IvalMed=IGainPercMed;
						IvalQ16=IGainPercQ16;
						IvalQ84=IGainPercQ84;
						
						IConfMed=IConfPercMed;
						IConfQ16=IConfPercQ16;
						IConfQ84=IConfPercQ84;
						
					else				
						MaxGainLine=MaxGainOrg(1);
						RawTitle1='TRand-Test Information Gain (in %): ';
						RawTitle2='TRand-Test Information Gain (in %) Reference: Uniform';
						
					end
					
				
					
					
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if BarTrail
							%Dotted line from 0 to OrgVal
							TrailerX=[0, IvalOrg(i)];
							TrailerY=[indexer(i), indexer(i)];
							
							handle(end+1)=plot(TrailerX,TrailerY);
							
							set(handle(end),'LineStyle',':','Color','k','LineWidth',2);
							legendentry{end+1}=['TrailLine ',indexLabel{i}];
							hold on;
						end
					
						if UseMean
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];
							
							MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MeanboxX=[Ival(i)-IvalSTD(i),Ival(i)+IvalSTD(i),...
								  Ival(i)+IvalSTD(i),Ival(i)-IvalSTD(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)-IConf(i)+IConfSTD(i),...
								  Ival(i)-IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[Ival(i)+IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)+IConf(i)-IConfSTD(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
							legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([Ival(i),Ival(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)-IConf(i),Ival(i)-IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)+IConf(i),Ival(i)+IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end
						
						elseif 	UseMed&MedianExist
							%Difficult part because Q16 and Q84 are always fixed points, not differences
							%So check if it is right
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalMed(i)-IConfQ84(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)-IConfQ84(i)];
							%both Q84, because it is the larger value
								  
							MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MedboxX=[IConfQ16(i),IConfQ84(i),...
								  IConfQ84(i),IConfQ16(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[IvalMed(i)-IConfQ84(i),IvalMed(i)-IConfQ16(i),...
								  IvalMed(i)-IConfQ16(i),IvalMed(i)-IConfQ84(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[IvalMed(i)+IConfQ16(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)+IConfQ16(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MedboxX,MedboxY,greenOne);
							legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([IvalMed(i),IvalMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)-IConfMed(i),IvalMed(i)-IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q16 Error ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)+IConfMed(i),IvalMed(i)+IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q84 Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end		
							
						else
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalOrg(i)-IConfOrg(i),IvalOrg(i)+IConfOrg(i),...
								  IvalOrg(i)+IConfOrg(i),IvalOrg(i)-IConfOrg(i)];
							
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
						
						end
						
						
						
						
						
					end
					
							
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
					
					%The maximum line is used in both versions, but only the original one, they should not deviate to much I hope 
					if ~isnan(MaxGain(1))
						handle(end+1)=plot([MaxGainLine;MaxGainLine],[ylimer(1);ylimer(2)])
						set(handle(end),'LineStyle','-','Color','b','LineWidth',2);
						legendentry{end+1}='Maximum Possible information gain';		

						xlim([-1 105]);						
					end
					
					
					if ~isempty(DisplayName)
						theTit=title([RawTitle1,DisplayName]);
					else
						theTit=title(RawTitle2);
					end
					
					
					
					%format the plot
					if ~isnan(MaxGain(1))
						xlab=xlabel('Information Gain I (%) ');
					else
						xlab=xlabel('Information Gain I ');
					end
					
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				case 'HistINoPerc'
					redOne=[0.8 0.5 0.5];
					greenOne=[0.5 0.7 0.5];
					greyHound=[0.85 0.85 0.85];
					
					%Same as the HistINoConf but uses percents for the values instead of Info gain.
					
					%They do not exist in every calc run, so they need default data
					MaxGain=NaN;
					IGainPerc=NaN;
					
					MaxGainOrg=NaN;
					IGainPercOrg=NaN;
					
					MaxGainSTD=NaN;
					IGainPercSTD=NaN;
					
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1)
						IvalOrg(i)=TtestNow.InABMonte(1);
						IvalSTD(i)=TtestNow.InAB_Mostd;
						
						
						try
							MaxGainOrg(i)=TtestNow.MaxGainMonte(1);
							IGainPercOrg(i)=TtestNow.IGainPercMonte(1);
							
							MaxGain(i)=TtestNow.MaxGain(1);
							IGainPerc(i)=TtestNow.IGainPerc(1);
							
							MaxGainSTD(i)=TtestNow.MaxGain_Mostd(1);
							IGainPercSTD(i)=TtestNow.IGainPerc_Mostd(1);
							
						end
						
						%Medians are not present in old Result calculations
						try
							IvalMed(i)=TtestNow.InABMedian;
							IvalQ16(i)=TtestNow.InAB68Per(1);
							IvalQ84(i)=TtestNow.InAB68Per(2);
							
							IGainPercMed(i)=TtestNow.IGainPercMedian;
							IGainPercQ16(i)=TtestNow.IGainPerc68Per(1);
							IGainPercQ84(i)=TtestNow.IGainPerc68Per(2);
							
							MaxGainMed(i)=TtestNow.MaxGainMedian;
							MaxGainQ16(i)=TtestNow.MaxGain68Per(1);
							MaxGainQ84(i)=TtestNow.MaxGain68Per(2);
							
							
						catch
							IvalMed(i)=NaN;
							IvalQ16(i)=NaN;
							IvalQ84(i)=NaN;
														
							IGainPercMed(i)=NaN;
							IGainPercQ16(i)=NaN;
							IGainPercQ84(i)=NaN;
														
							MaxGainMed(i)=NaN;
							MaxGainQ16(i)=NaN;
							MaxGainQ84(i)=NaN;
							
							MedianExist=false;
						end
						
					end
					
					
					%Set the right parameters
					if all(~isnan(IGainPerc))
						Ival=IGainPerc;
						
						IvalOrg=IGainPercOrg;
						
						IvalSTD=IGainPercSTD;
						
						MaxGainLine=100;
						
						RawTitle1='Information Gain: ';
						RawTitle2='Information Gain Reference: Uniform';
						
						%No if needed here, they set to NaN anyway
						IvalMed=IGainPercMed;
						IvalQ16=IGainPercQ16;
						IvalQ84=IGainPercQ84;
																		
					else				
						MaxGainLine=MaxGainOrg(1);
						RawTitle1='Information Gain (in %): ';
						RawTitle2='Information Gain (in %) Reference: Uniform';
						
					end
					
				
					
					
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if BarTrail
							%Dotted line from 0 to OrgVal
							TrailerX=[0, IvalOrg(i)];
							TrailerY=[indexer(i), indexer(i)];
							
							handle(end+1)=plot(TrailerX,TrailerY);
							
							set(handle(end),'LineStyle',':','Color','k','LineWidth',2);
							legendentry{end+1}=['TrailLine ',indexLabel{i}];
							hold on;
						end
						
					
						if UseMean
							MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MeanboxX=[Ival(i)-IvalSTD(i),Ival(i)+IvalSTD(i),...
								  Ival(i)+IvalSTD(i),Ival(i)-IvalSTD(i)];
							
								
								  
							%first the boxes
													
							handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
							legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
							hold on
							
							%now the lines
							handle(end+1)=plot([Ival(i),Ival(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
																
							end
						
						elseif 	UseMed&MedianExist
							%Difficult part because Q16 and Q84 are always fixed points, not differences
							%So check if it is right
															  
							MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MedboxX=[IvalQ16(i),IvalQ84(i),...
								  IvalQ84(i),IvalQ16(i)];
							
								
								  
							%first the boxes
							handle(end+1)=fill(MedboxX,MedboxY,greenOne);
							legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
							hold on;
							
							%now the lines
							handle(end+1)=plot([IvalMed(i),IvalMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
							
														
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
														
							end		
							
						else
							handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							hold on;
							
							
						
						end
						
						
						
						
						
					end
					
							
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
					
					%The maximum line is used in both versions, but only the original one, they should not deviate to much I hope 
					if ~isnan(MaxGain(1))
						handle(end+1)=plot([MaxGainLine;MaxGainLine],[ylimer(1);ylimer(2)])
						set(handle(end),'LineStyle','-','Color','b','LineWidth',2);
						legendentry{end+1}='Maximum Possible information gain';		

						xlim([-1 105]);						
					end
					
					
					if ~isempty(DisplayName)
						theTit=title([RawTitle1,DisplayName]);
					else
						theTit=title(RawTitle2);
					end
					
					
					
					%format the plot
					if ~isnan(MaxGain(1))
						xlab=xlabel('Information Gain I (%) ');
					else
						xlab=xlabel('Information Gain I ');
					end
					
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
						
					
					
			
				case 'HistI'
					%Median not supported here currently, may add it later
					redOne=[0.8 0.5 0.5];
					blueOne=[0.5 0.5 0.7];
					%create the data
					
					for i=1:numel(Names)
						TtestNow=Results.(['TRtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1);
						ISTD(i)=sqrt(TtestNow.VarianceI(1));
						
				
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						
						ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
						ErrorboxY=[Ival(i)-ISTD(i),Ival(i)-ISTD(i),Ival(i)+ISTD(i),Ival(i)+ISTD(i)];
						if Ival(i)>0
							handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
						else
							handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
						end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
						hold on;
						handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Ival(i),Ival(i)]);
						set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
						legendentry{end+1}=['Information gain ',indexLabel{i}];
						
					
					end
					
					%plot zeroline									
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.2;
					xlimer(2)=xlimer(2)+0.2;
					ttable=TtestNow.ttable;
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.9 0.9 0.9],'LineWidth',0.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
			
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['TRand-Test: ',DisplayName]);
					else
						theTit=title(['TRand-Test: Reference Uniform']);
					end
					
					
					
					%format the plot
					ylab=ylabel('Information Gain I  ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'HistT'
					redOne=[0.8 0.5 0.5];
					blueOne=[0.5 0.5 0.7];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Tval(i)=TtestNow.TValMonte(1);
						TvalMean(i)=TtestNow.T;
						TvalSTD(i)=TtestNow.T_std_dev;
						
						%Medians are not present in old Result calculations
						try
							TMed(i)=TtestNow.TMedian;
							TQ16(i)=TtestNow.T68Per(1);
							TQ84(i)=TtestNow.T68Per(2);
							
							
							
						catch
							TMed(i)=NaN;
							TQ16(i)=NaN;
							TQ84(i)=NaN;
							
							
							MedianExist=false;
						end
						
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if UseMean
							ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorboxY=[TvalMean(i)-TvalSTD(i),TvalMean(i)-TvalSTD(i),TvalMean(i)+TvalSTD(i),TvalMean(i)+TvalSTD(i)];
							if TvalMean(i)>0
								handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
							else
								handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
							end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							hold on;
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[TvalMean(i),TvalMean(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['mean t-value ',indexLabel{i}];
							
						end
						
						if UseMed&MedianExist
							ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorboxY=[TQ16(i),TQ16(i),TQ84(i),TQ84(i)];
							if TvalMean(i)>0
								handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
							else
								handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
							end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							hold on;
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[TMed(i),TMed(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['mean t-value ',indexLabel{i}];
							
						end
						
						if UseOrg
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Tval(i),Tval(i)]);
							set(handle(end),'LineStyle',':','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. t-value ',indexLabel{i}];
							hold on;
						end
						
					
					end
					
					
					%plot the ttable it should be equal for each test anyway
					TtestNow=Results.(['TRtest_',Names{1}]);
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					ttable=TtestNow.ttable;
					handle(end+1)=plot([xlimer(1);xlimer(2)],[ttable;ttable])
					set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
					xlim(xlimer);
					legendentry{end+1}='significant t-value';
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.9 0.9 0.9],'LineWidth',0.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
					
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[-ttable;-ttable])
					set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
					xlim(xlimer);
					legendentry{end+1}='significant t-value';
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['T-Test: ',DisplayName]);
					else
						theTit=title(['T-Test Reference: Uniform']);
					end
					
					
					
					%format the plot
					ylab=ylabel('T-value ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
						
			
					
					
				case 'ScoreT'	
					PlotData=Results.(['TRtest_',Names{1}]);
					
					%create the strings
					QuantString={['$$T_{table}=$$ ',num2str(round(1000*PlotData.ttable)/1000)]};
					
					if UseOrg
						QuantString(end+1)={['$$T_o=$$ ',num2str(round(1000*PlotData.TValMonte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$T_m=$$ ',num2str(round(1000*PlotData.T)/1000),' +/-',num2str(round(1000*PlotData.T_std_dev)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$T_m=$$ ',num2str(round(1000*PlotData.TMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.T68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.T68Per(2))/1000)]};
						
					end
					
					%define text position
					if PlotData.T>5
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.TValMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='T-values';
					
					handle(end+1)=plot([PlotData.ttable,PlotData.ttable],[0,ylimer(2)],'LineStyle','-',...
					'Color','k','LineWidth',3);
					legendentry{end+1}='T-value table';
					
					
					if UseOrg
						handle(end+1)=plot([PlotData.TValMonte(1),PlotData.TValMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='T-value org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.T,PlotData.T],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean T-value';
						handle(end+1)=plot([PlotData.T-PlotData.T_std_dev,PlotData.T-PlotData.T_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std T-value';
						handle(end+1)=plot([PlotData.T+PlotData.T_std_dev,PlotData.T+PlotData.T_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std T-value';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.TMedian,PlotData.TMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='mean T-value';
						handle(end+1)=plot([PlotData.T68Per(1),PlotData.T68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='-std T-value';
						handle(end+1)=plot([PlotData.T68Per(2),PlotData.T68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='+std T-value';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['T-values: ',DisplayName]);
					else
						theTit=title(['T-values: Uniform vs. ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('T-value');
					ylab=ylabel('Number of occurence   ');
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
				
				case 'ScoreI'	
					PlotData=Results.(['TRtest_',Names{1}]);
					
					%create the strings
					QuantString={};
					
					if UseOrg
						QuantString(end+1)={['$$I_o=$$ ',num2str(round(1000*PlotData.InABMonte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$I_m=$$ ',num2str(round(1000*PlotData.InAB)/1000),' +/-',num2str(round(1000*PlotData.InAB_Mostd)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$I_m=$$ ',num2str(round(1000*PlotData.InABMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.InAB68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.InAB68Per(2))/1000)]};						
					end
					
					
					
					%define text position
					if PlotData.InAB>1
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.InABMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Information Gain';
					
										
					
					if UseOrg
						handle(end+1)=plot([PlotData.InABMonte(1),PlotData.InABMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Information Gain org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.InAB,PlotData.InAB],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Information Gain';
						handle(end+1)=plot([PlotData.InAB-PlotData.InAB_Mostd,PlotData.InAB-PlotData.InAB_Mostd],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Information Gain';
						handle(end+1)=plot([PlotData.InAB+PlotData.InAB_Mostd,PlotData.InAB+PlotData.InAB_Mostd],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Information Gain';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.InABMedian,PlotData.InABMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Information Gain';
						handle(end+1)=plot([PlotData.InAB68Per(1),PlotData.InAB68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='Q16 Information Gain';
						handle(end+1)=plot([PlotData.InAB68Per(2),PlotData.InAB68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='Q84 Information Gain';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['Information Gain: ',DisplayName]);
					else
						theTit=title(['Information Gain: Uniform vs. ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
			end	
			
			
			
			if ~MrHolden
				hold off;
			end	
			

			
			
		case 'TRInvtest'
			%normaly more than one model is used so Names has to be a cell, first one is the reference
			Names=ResStruct.Name;
			MrHolden=ishold;
			
			switch ResStruct.PlotType
				case 'HistIConf'
					redOne=[0.8 0.5 0.5];
					greenOne=[0.5 0.7 0.5];
					greyHound=[0.85 0.85 0.85];
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRInvtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1)
						IvalOrg(i)=TtestNow.InABMonte(1);
						IvalSTD(i)=TtestNow.InAB_Mostd;
						IConf(i)=TtestNow.IConf(1);
						IConfOrg(i)=TtestNow.IConfMonte(1);
						IConfSTD(i)=TtestNow.IConf_std(1);
						
						%Medians are not present in old Result calculations
						try
							IvalMed(i)=TtestNow.InABMedian;
							IvalQ16(i)=TtestNow.InAB68Per(1);
							IvalQ84(i)=TtestNow.InAB68Per(2);
							
							IConfMed(i)=TtestNow.IConfMedian;
							IConfQ16(i)=TtestNow.IConf68Per(1);
							IConfQ84(i)=TtestNow.IConf68Per(2);

							
						catch
							IvalMed(i)=NaN;
							IvalQ16(i)=NaN;
							IvalQ84(i)=NaN;
							IConfMed(i)=NaN;
							IConfQ16(i)=NaN;
							IConfQ84(i)=NaN;
							
							MedianExist=false;
						end
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if UseMean
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];
							
							MeanboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MeanboxX=[Ival(i)-IvalSTD(i),Ival(i)+IvalSTD(i),...
								  Ival(i)+IvalSTD(i),Ival(i)-IvalSTD(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[Ival(i)-IConf(i)-IConfSTD(i),Ival(i)-IConf(i)+IConfSTD(i),...
								  Ival(i)-IConf(i)+IConfSTD(i),Ival(i)-IConf(i)-IConfSTD(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[Ival(i)+IConf(i)-IConfSTD(i),Ival(i)+IConf(i)+IConfSTD(i),...
								  Ival(i)+IConf(i)+IConfSTD(i),Ival(i)+IConf(i)-IConfSTD(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MeanboxX,MeanboxY,greenOne);
							legendentry{end+1}=['Info. Gain MeanBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([Ival(i),Ival(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)-IConf(i),Ival(i)-IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
							
							handle(end+1)=plot([Ival(i)+IConf(i),Ival(i)+IConf(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end
							
						elseif 	UseMed&MedianExist
							%Difficult part because Q16 and Q84 are always fixed points, not differences
							%So check if it is right
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalMed(i)-IConfQ84(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)-IConfQ84(i)];
							%both Q84, because it is the larger value
								  
							MedboxY=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							MedboxX=[IConfQ16(i),IConfQ84(i),...
								  IConfQ84(i),IConfQ16(i)];
								  
							Errorbox1Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox1X=[IvalMed(i)-IConfQ84(i),IvalMed(i)-IConfQ16(i),...
								  IvalMed(i)-IConfQ16(i),IvalMed(i)-IConfQ84(i)];	  
								  
							Errorbox2Y=[indexer(i)-BarWider,indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider];
							Errorbox2X=[IvalMed(i)+IConfQ16(i),IvalMed(i)+IConfQ84(i),...
								  IvalMed(i)+IConfQ84(i),IvalMed(i)+IConfQ16(i)];
								
								  
							%first the boxes
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=fill(MedboxX,MedboxY,greenOne);
							legendentry{end+1}=['Info. Gain MediannBox ',indexLabel{i}];
								
							handle(end+1)=fill(Errorbox1X,Errorbox1Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							handle(end+1)=fill(Errorbox2X,Errorbox2Y,redOne);
							legendentry{end+1}=['lower Error Box ',indexLabel{i}];
							
							%now the lines
							handle(end+1)=plot([IvalMed(i),IvalMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Median Information Gain ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)-IConfMed(i),IvalMed(i)-IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q16 Error ',indexLabel{i}];
							
							handle(end+1)=plot([IvalMed(i)+IConfMed(i),IvalMed(i)+IConfMed(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Q84 Error ',indexLabel{i}];
							
							if UseOrg
								handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','-','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
								handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
								set(handle(end),'LineStyle','--','Color','k','LineWidth',ScoreWidth);
								legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
								
							end	
							
							
						else
							MainboxY=[indexer(i)-BarWider/2,indexer(i)-BarWider/2,indexer(i)+BarWider/2,indexer(i)+BarWider/2];
							MainboxX=[IvalOrg(i)-IConfOrg(i),IvalOrg(i)+IConfOrg(i),...
								  IvalOrg(i)+IConfOrg(i),IvalOrg(i)-IConfOrg(i)];
							
							handle(end+1)=fill(MainboxX,MainboxY,greyHound);
							set(handle(end),'LineStyle','none');
							legendentry{end+1}=['MainBox ',indexLabel{i}];
							hold on;
							
							handle(end+1)=plot([IvalOrg(i),IvalOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','g','LineWidth',MainWidth);
							legendentry{end+1}=['Mean Information Gain ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)-IConfOrg(i),IvalOrg(i)-IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Lower Mean Error ',indexLabel{i}];
								
							handle(end+1)=plot([IvalOrg(i)+IConfOrg(i),IvalOrg(i)+IConfOrg(i)],[indexer(i)-BarWider,indexer(i)+BarWider]);
							set(handle(end),'LineStyle','-','Color','r','LineWidth',MainWidth);
							legendentry{end+1}=['Upper Mean Error ',indexLabel{i}];
						
						end
					end
					
							
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					handle(end+1)=plot([0;0],[ylimer(1);ylimer(2)])
					set(handle(end),'LineStyle','--','Color',[0.7 0.7 0.7],'LineWidth',1);
					legendentry{end+1}='Zero Line';
					
					
					if ~isempty(DisplayName)
						theTit=title(['TRand-Inverted Test Information Gain: ',DisplayName]);
					else
						theTit=title(['TRand-Inverted Information Gain Reference: Uniform']);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I  ');
					
					set(xlab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'YTick',indexer,'YTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
			
				case 'HistI'
					%Median not supported here currently, may add it later
					redOne=[0.8 0.5 0.5];
					blueOne=[0.5 0.5 0.7];
					%create the data
					
					for i=1:numel(Names)
						TtestNow=Results.(['TRInvtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Ival(i)=TtestNow.InAB(1);
						ISTD(i)=sqrt(TtestNow.VarianceI(1));
						
				
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						
						ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
						ErrorboxY=[Ival(i)-ISTD(i),Ival(i)-ISTD(i),Ival(i)+ISTD(i),Ival(i)+ISTD(i)];
						if Ival(i)>0
							handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
						else
							handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
						end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
						hold on;
						handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Ival(i),Ival(i)]);
						set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
						legendentry{end+1}=['Information gain ',indexLabel{i}];
						
					
					end
					
					%plot zeroline									
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.2;
					xlimer(2)=xlimer(2)+0.2;
					ttable=TtestNow.ttable;
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.9 0.9 0.9],'LineWidth',0.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
			
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['TRand-Inverted-Test: ',DisplayName]);
					else
						theTit=title(['TRand-Inverted-Test: Reference Uniform']);
					end
					
					
					
					%format the plot
					ylab=ylabel('Information Gain I  ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
				
					
				case 'HistT'
					redOne=[0.8 0.5 0.5];
					blueOne=[0.5 0.5 0.7];
					%create the data
					
					MedianExist=true;
					
					%create the data
					for i=1:numel(Names)
						TtestNow=Results.(['TRInvtest_',Names{i}]);
						
						indexer(i)=2*(i);
						indexLabel(i)=ModDispName(i);
						Tval(i)=TtestNow.TValMonte(1);
						TvalMean(i)=TtestNow.T;
						TvalSTD(i)=TtestNow.T_std_dev;
						
						%Medians are not present in old Result calculations
						try
							TMed(i)=TtestNow.TMedian;
							TQ16(i)=TtestNow.T68Per(1);
							TQ84(i)=TtestNow.T68Per(2);
							
							
							
						catch
							TMed(i)=NaN;
							TQ16(i)=NaN;
							TQ84(i)=NaN;
							
							
							MedianExist=false;
						end
				
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if UseMean
							ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorboxY=[TvalMean(i)-TvalSTD(i),TvalMean(i)-TvalSTD(i),TvalMean(i)+TvalSTD(i),TvalMean(i)+TvalSTD(i)];
							if TvalMean(i)>0
								handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
							else
								handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
							end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							hold on;
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[TvalMean(i),TvalMean(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['mean t-value ',indexLabel{i}];
							
						end
						
						
						if UseMed&MedianExist
							ErrorboxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorboxY=[TQ16(i),TQ16(i),TQ84(i),TQ84(i)];
							if TvalMean(i)>0
								handle(end+1)=fill(ErrorboxX,ErrorboxY,redOne);
							else
								handle(end+1)=fill(ErrorboxX,ErrorboxY,blueOne);
							end
							legendentry{end+1}=['Errorbox ',indexLabel{i}];
							
							hold on;
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[TMed(i),TMed(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['mean t-value ',indexLabel{i}];
							
						end
						
						
						if UseOrg
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Tval(i),Tval(i)]);
							set(handle(end),'LineStyle',':','Color','g','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. t-value ',indexLabel{i}];
							hold on;
						end
						
					
					end
					
					
					%plot the ttable it should be equal for each test anyway
					TtestNow=Results.(['TRInvtest_',Names{1}]);
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
					ttable=TtestNow.ttable;
					handle(end+1)=plot([xlimer(1);xlimer(2)],[ttable;ttable])
					set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
					xlim(xlimer);
					legendentry{end+1}='significant t-value';
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.9 0.9 0.9],'LineWidth',0.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
					
					
					handle(end+1)=plot([xlimer(1);xlimer(2)],[-ttable;-ttable])
					set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
					xlim(xlimer);
					legendentry{end+1}='significant t-value';
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['T-Inverted-Test: ',DisplayName]);
					else
						theTit=title(['T-Inverted-Test Reference: Uniform']);
					end
					
					
					
					%format the plot
					ylab=ylabel('T-value ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
						
			
					
					
				case 'ScoreT'	
					PlotData=Results.(['TRInvtest_',Names{1}]);
					
					%create the strings
					QuantString={['$$T_{table}=$$ ',num2str(round(1000*PlotData.ttable)/1000)]};
					
					if UseOrg
						QuantString(end+1)={['$$T_o=$$ ',num2str(round(1000*PlotData.TValMonte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$T_m=$$ ',num2str(round(1000*PlotData.T)/1000),' +/-',num2str(round(1000*PlotData.T_std_dev)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$T_m=$$ ',num2str(round(1000*PlotData.TMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.T68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.T68Per(2))/1000)]};	
					end
					
					
					%define text position
					if PlotData.T>5
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.TValMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='T-values';
					
					handle(end+1)=plot([PlotData.ttable,PlotData.ttable],[0,ylimer(2)],'LineStyle','-',...
					'Color','k','LineWidth',3);
					legendentry{end+1}='T-value table';
					
					
					if UseOrg
						handle(end+1)=plot([PlotData.TValMonte(1),PlotData.TValMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='T-value org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.T,PlotData.T],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean T-value';
						handle(end+1)=plot([PlotData.T-PlotData.T_std_dev,PlotData.T-PlotData.T_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std T-value';
						handle(end+1)=plot([PlotData.T+PlotData.T_std_dev,PlotData.T+PlotData.T_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std T-value';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.TMedian,PlotData.TMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='mean T-value';
						handle(end+1)=plot([PlotData.T68Per(1),PlotData.T68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='-std T-value';
						handle(end+1)=plot([PlotData.T68Per(2),PlotData.T68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='+std T-value';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['T-Inverted-values: ',DisplayName]);
					else
						theTit=title(['T-Inverted-values: Uniform vs. ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('T-value');
					ylab=ylabel('Number of occurence   ');
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
				
				case 'ScoreI'	
					PlotData=Results.(['TRInvtest_',Names{1}]);
					
					%create the strings
					QuantString={};
					
					if UseOrg
						QuantString(end+1)={['$$I_o=$$ ',num2str(round(1000*PlotData.InABMonte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$I_m=$$ ',num2str(round(1000*PlotData.InAB)/1000),' +/-',num2str(round(1000*PlotData.InAB_Mostd)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$I_m=$$ ',num2str(round(1000*PlotData.InABMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.InAB68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.InAB68Per(2))/1000)]};						
					end
					
					
					%define text position
					if PlotData.InAB>1
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.InABMonte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='Information Gain';
					
										
					
					if UseOrg
						handle(end+1)=plot([PlotData.InABMonte(1),PlotData.InABMonte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='Information Gain org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.InAB,PlotData.InAB],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean Information Gain';
						handle(end+1)=plot([PlotData.InAB-PlotData.InAB_Mostd,PlotData.InAB-PlotData.InAB_Mostd],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std Information Gain';
						handle(end+1)=plot([PlotData.InAB+PlotData.InAB_Mostd,PlotData.InAB+PlotData.InAB_Mostd],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std Information Gain';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.InABMedian,PlotData.InABMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median Information Gain';
						handle(end+1)=plot([PlotData.InAB68Per(1),PlotData.InAB68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='Q16 Information Gain';
						handle(end+1)=plot([PlotData.InAB68Per(2),PlotData.InAB68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='Q84 Information Gain';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['Information Gain (Inverted): ',DisplayName]);
					else
						theTit=title(['Information Gain (Inverted): Uniform vs. ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('Information Gain I');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
			end	
			
			
			
			if ~MrHolden
				hold off;
			end				
			
			
			
			
			
		case 'Wtest'
			%normaly more than one model is used so Names has to be a cell, first one is the reference
			Names=ResStruct.Name;
			MrHolden=ishold;
			
			switch ResStruct.PlotType
				case 'Hist'
					redOne=[1 0.7 0.7];
					blueOne=[0.7 0.7 0.9];
					redTwo=[0.8 0.5 0.5];
					blueTwo=[0.5 0.5 0.7];
					
					
					MedianExist=true;
					
					%create the data
					for i=2:numel(Names)
						WtestNow=Results.(['Wtest_',Names{1},'_',Names{i}]);
						
						indexer(i-1)=2*(i-1);
						indexLabel(i-1)=ModDispName(i);
						Wplus(i-1)=WtestNow.W_plus_Monte(1);
						WplusMean(i-1)=WtestNow.W_plus;
						WplusSTD(i-1)=WtestNow.W_plus_std_dev;
						
						Wminus(i-1)=-WtestNow.W_minus_Monte(1);
						WminusMean(i-1)=-WtestNow.W_minus;
						WminusSTD(i-1)=WtestNow.W_minus_std_dev;
						
						
						%Medians are not present in old Result calculations
						try
							WplusMed(i-1)=WtestNow.W_PlusMedian;
							WplusQ16(i-1)=WtestNow.W_Plus68Per(1);
							WplusQ84(i-1)=WtestNow.W_Plus68Per(2);
							
							WminusMed(i-1)=-WtestNow.W_MinusMedian;
							WminusQ16(i-1)=-WtestNow.W_Minus68Per(1);
							WminusQ84(i-1)=-WtestNow.W_Minus68Per(2);

							
						catch
							WplusMed(i-1)=NaN;
							WplusQ16(i-1)=NaN;
							WplusQ84(i-1)=NaN;
							WminusMed(i-1)=NaN;
							WminusQ16(i-1)=NaN;
							WminusQ84(i-1)=NaN;
							
							MedianExist=false;
						end
					end
					
					handle=[];
					legendentry={};
					
					%now plot it
					for i=1:numel(indexer)
						if UseMean
							PlusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							PlusBoxY=[0,0,WplusMean(i)+WplusSTD(i),WplusMean(i)+WplusSTD(i)];
							MinusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							MinusBoxY=[WminusMean(i)-WminusSTD(i),WminusMean(i)-WminusSTD(i),0,0];
							
							ErrorPlusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorPlusBoxY=[WplusMean(i)-WplusSTD(i),WplusMean(i)-WplusSTD(i),WplusMean(i)+WplusSTD(i),WplusMean(i)+WplusSTD(i)];
							ErrorMinusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorMinusBoxY=[WminusMean(i)-WminusSTD(i),WminusMean(i)-WminusSTD(i),WminusMean(i)+WminusSTD(i),WminusMean(i)+WminusSTD(i)];
							
							
							handle(end+1)=fill(PlusBoxX,PlusBoxY,redOne);
							set(handle(end),'LineStyle','None');
							hold on
							handle(end+1)=fill(MinusBoxX,MinusBoxY,blueOne);
							set(handle(end),'LineStyle','None');
							handle(end+1)=fill(ErrorPlusBoxX,ErrorPlusBoxY,redTwo);
							set(handle(end),'LineStyle','None');
							handle(end+1)=fill(ErrorMinusBoxX,ErrorMinusBoxY,blueTwo);
							set(handle(end),'LineStyle','None');
							
							legendentry{end+1}=['W+ box',indexLabel{i}];
							legendentry{end+1}=['W- box',indexLabel{i}];
							legendentry{end+1}=['W+ errorbox',indexLabel{i}];
							legendentry{end+1}=['W- errorbox',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WplusMean(i)+WplusSTD(i),WplusMean(i)+WplusSTD(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W+ Std+',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WplusMean(i)-WplusSTD(i),WplusMean(i)-WplusSTD(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W+ Std-',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WplusMean(i),WplusMean(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['W+',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WminusMean(i)+WminusSTD(i),WminusMean(i)+WminusSTD(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W- Std+',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WminusMean(i)-WminusSTD(i),WminusMean(i)-WminusSTD(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W- Std-',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WminusMean(i),WminusMean(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['W-',indexLabel{i}];
							
						end
						
						
						if UseMed&MedianExist
							%Again this has to be verified
							%Also Mean and Median should not be used together
							PlusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							PlusBoxY=[0,0,WplusQ84(i),WplusQ84(i)];
							MinusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							MinusBoxY=[WminusQ84(i),WminusQ84(i),0,0];
							
							ErrorPlusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorPlusBoxY=[WplusQ16(i),WplusQ16(i),WplusQ84(i),WplusQ84(i)];
							ErrorMinusBoxX=[indexer(i)-BarWider,indexer(i)+BarWider,indexer(i)+BarWider,indexer(i)-BarWider];
							ErrorMinusBoxY=[WminusQ16(i),WminusQ16(i),WminusQ84(i),WminusQ84(i)];
							
							
							handle(end+1)=fill(PlusBoxX,PlusBoxY,redOne);
							set(handle(end),'LineStyle','None');
							hold on
							handle(end+1)=fill(MinusBoxX,MinusBoxY,blueOne);
							set(handle(end),'LineStyle','None');
							handle(end+1)=fill(ErrorPlusBoxX,ErrorPlusBoxY,redTwo);
							set(handle(end),'LineStyle','None');
							handle(end+1)=fill(ErrorMinusBoxX,ErrorMinusBoxY,blueTwo);
							set(handle(end),'LineStyle','None');
							
							legendentry{end+1}=['W+ box',indexLabel{i}];
							legendentry{end+1}=['W- box',indexLabel{i}];
							legendentry{end+1}=['W+ errorbox',indexLabel{i}];
							legendentry{end+1}=['W- errorbox',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WplusQ84(i),WplusQ84(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W+ Q84 ',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WplusQ16(i),WplusQ16(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W+ Q16 ',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WplusMed(i),WplusMed(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['W+ Median ',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WminusQ84(i),WminusQ84(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W- Q84 ',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WminusQ16(i),WminusQ16(i)]);
							set(handle(end),'LineStyle',':','Color','k','LineWidth',1);
							legendentry{end+1}=['W- Q16 ',indexLabel{i}];
							
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[WminusMed(i),WminusMed(i)]);
							set(handle(end),'LineStyle','-','Color','k','LineWidth',MainWidth);
							legendentry{end+1}=['W- Median ',indexLabel{i}];
							
						end
						
						
						if UseOrg
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Wplus(i),Wplus(i)]);
							set(handle(end),'LineStyle',':','Color','r','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. W+ ',indexLabel{i}];
							hold on;
							handle(end+1)=plot([indexer(i)-BarWider,indexer(i)+BarWider],[Wminus(i),Wminus(i)]);
							set(handle(end),'LineStyle',':','Color','b','LineWidth',ScoreWidth);
							legendentry{end+1}=['org. W- ',indexLabel{i}];
						end
						
					
					end
					
					%plot zeroline									
					xlimer=xlim;
					xlimer(1)=xlimer(1)-0.5;
					xlimer(2)=xlimer(2)+0.5;
										
					handle(end+1)=plot([xlimer(1);xlimer(2)],[0;0])
					set(handle(end),'LineStyle','--','Color',[0.5 0.5 0.5],'LineWidth',1.5);
					xlim(xlimer);
					legendentry{end+1}='Zero Line';
			
					
					ylimer=ylim;
					ylimer(1)=ylimer(1)-0.5;
					ylimer(2)=ylimer(2)+0.5;
					ylim(ylimer);
					
					if ~isempty(DisplayName)
						theTit=title(['W-Test: ',DisplayName]);
					else
						theTit=title(['W-Test Reference: ',ResStruct.Name{1}]);
					end
					
					
					
					%format the plot
					ylab=ylabel('W-value  ');
					
					set(ylab,'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(plotAxis,'XTick',indexer,'XTickLabel',indexLabel);
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
						
		
					
					
					
				case 'W+Score'	
					PlotData=Results.(['Wtest_',Names{1},'_',Names{2}]);
					
					QuantString={};
					
					if UseOrg
						QuantString(end+1)={['$$W+_o=$$ ',num2str(round(1000*PlotData.W_plus_Monte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$W+_m=$$ ',num2str(round(1000*PlotData.W_plus)/1000),' +/-',num2str(round(1000*PlotData.W_plus_std_dev)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$W+_m=$$ ',num2str(round(1000*PlotData.W_PlusMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.W_Plus68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.W_Plus68Per(2))/1000)]};						
					end
					
					
					%define text position
					if PlotData.W_plus<250
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.W_plus_Monte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='W+';
										
					
					if UseOrg
						handle(end+1)=plot([PlotData.W_plus_Monte(1),PlotData.W_plus_Monte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='W+ org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.W_plus,PlotData.W_plus],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean W+';
						handle(end+1)=plot([PlotData.W_plus-PlotData.W_plus_std_dev,PlotData.W_plus-PlotData.W_plus_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std W+';
						handle(end+1)=plot([PlotData.W_plus+PlotData.W_plus_std_dev,PlotData.W_plus+PlotData.W_plus_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std W+';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.W_PlusMedian,PlotData.W_PlusMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median W+';
						handle(end+1)=plot([PlotData.W_Plus68Per(1),PlotData.W_Plus68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 W+';
						handle(end+1)=plot([PlotData.W_Plus68Per(2),PlotData.W_Plus68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 W+';
					end
					
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['W+: ',DisplayName]);
					else
						theTit=title(['W+: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('W-value');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');
					
					
					
				case 'W-Score'	
					PlotData=Results.(['Wtest_',Names{1},'_',Names{2}]);
					
					QuantString={};
					
					if UseOrg
						QuantString(end+1)={['$$W-_o=$$ ',num2str(round(1000*PlotData.W_minus_Monte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$W-_m=$$ ',num2str(round(1000*PlotData.W_minus)/1000),' +/-',num2str(round(1000*PlotData.W_minus_std_dev)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$W-_m=$$ ',num2str(round(1000*PlotData.W_MinusMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.W_Minus68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.W_Minus68Per(2))/1000)]};						
					end
					
					
					%define text position
					if PlotData.W_minus<250
						tpos=[0.05,0.85];
					else
						tpos=[0.65,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.W_minus_Monte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='W-';
										
					
					if UseOrg
						handle(end+1)=plot([PlotData.W_minus_Monte(1),PlotData.W_minus_Monte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='W- org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.W_minus,PlotData.W_minus],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean W-';
						handle(end+1)=plot([PlotData.W_minus-PlotData.W_minus_std_dev,PlotData.W_minus-PlotData.W_minus_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std W-';
						handle(end+1)=plot([PlotData.W_minus+PlotData.W_minus_std_dev,PlotData.W_minus+PlotData.W_minus_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std W-';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.W_MinusMedian,PlotData.W_MinusMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median W-';
						handle(end+1)=plot([PlotData.W_Minus68Per(1),PlotData.W_Minus68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 W-';
						handle(end+1)=plot([PlotData.W_Minus68Per(2),PlotData.W_Minus68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 W-';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['W-: ',DisplayName]);
					else
						theTit=title(['W-: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('W-value');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');		
					
					
					
			
				case 'WScore'	
					PlotData=Results.(['Wtest_',Names{1},'_',Names{2}]);
					
					
					QuantString={};
					
					
					if UseOrg
						QuantString(end+1)={['$$W_o=$$ ',num2str(round(1000*PlotData.W_overall_Monte(1))/1000)]};
					end
					
					
					if UseMean
						QuantString(end+1)={['$$W_m=$$ ',num2str(round(1000*PlotData.W_overall)/1000),' +/-',num2str(round(1000*PlotData.W_overall_std_dev)/1000)]};
						
					end
					
					
					if UseMed
						QuantString(end+1)={['$$W_m=$$ ',num2str(round(1000*PlotData.W_OverallMedian)/1000),...
								' Q16: ',num2str(round(1000*PlotData.W_Overall68Per(1))/1000),...
								' Q84: ',num2str(round(1000*PlotData.W_Overall68Per(2))/1000)]};						
					end
					
					
					
					%define text position
					if PlotData.W_overall<500
						tpos=[0.05,0.85];
					else
						tpos=[0.6,0.85];
					end
					disp(tpos)
				
					
					hist(PlotData.W_overall_Monte);
					handle(1) = findobj(gca,'Type','patch');
					set(handle(1),'FaceColor',Colors,'EdgeColor','k');
					hold on 
					ylimer=ylim;
					
					legendentry{1}='W-';
										
					
					if UseOrg
						handle(end+1)=plot([PlotData.W_overall_Monte(1),PlotData.W_overall_Monte(1)],[0,ylimer(2)],'LineStyle','--',...
						'Color','g','LineWidth',1.5);
						legendentry{end+1}='W-value org. Catalog';
					end
					
					
					if UseMean
						handle(end+1)=plot([PlotData.W_overall,PlotData.W_overall],[0,ylimer(2)],'LineStyle','-',...
						'Color','r','LineWidth',ScoreWidth);
						legendentry{end+1}='mean W-value';
						handle(end+1)=plot([PlotData.W_overall-PlotData.W_overall_std_dev,PlotData.W_overall-PlotData.W_overall_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='-std W-value';
						handle(end+1)=plot([PlotData.W_overall+PlotData.W_overall_std_dev,PlotData.W_overall+PlotData.W_overall_std_dev],[0,ylimer(2)],'LineStyle',':',...
						'Color','r','LineWidth',0.5);
						legendentry{end+1}='+std W-value';
					end
					
					
					if UseMed
						handle(end+1)=plot([PlotData.W_OverallMedian,PlotData.W_OverallMedian],[0,ylimer(2)],'LineStyle','-',...
						'Color','b','LineWidth',ScoreWidth);
						legendentry{end+1}='median W-value';
						handle(end+1)=plot([PlotData.W_Overall68Per(1),PlotData.W_Overall68Per(1)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q16 W-value';
						handle(end+1)=plot([PlotData.W_Overall68Per(2),PlotData.W_Overall68Per(2)],[0,ylimer(2)],'LineStyle',':',...
						'Color','b','LineWidth',0.5);
						legendentry{end+1}='Q84 W-value';
					end
					
					
					if ResStruct.PrintText
						leText=text(tpos(1),tpos(2),QuantString,'Interpreter','Latex','Units','normalized');
						set(leText,'FontSize',DesTexSize);
					end
					
					if ~isempty(DisplayName)
						theTit=title(['W-value: ',DisplayName]);
					else
						theTit=title(['W-value: ',ResStruct.Name{1},' vs. ',ResStruct.Name{2}]);
					end
					
					
					
					%format the plot
					xlab=xlabel('W-value');
					ylab=ylabel('Number of occurence   ');
					
					set([xlab,ylab],'FontSize',LabTexSize,'FontWeight',WeightLab);
					set(gca,'LineWidth',AxisWidth,'FontSize',AxisTexSize,'FontWeight',WeightAxis);
					
					set(theTit,'FontSize',TitTexSize,'FontWeight','bold','Interpreter','none');	
					
		end	
			
			
			
		if ~MrHolden
			hold off;
		end	
			
		
		
	
	end


end

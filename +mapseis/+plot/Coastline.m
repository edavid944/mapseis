function plotArgs=Coastline(plotAxis,filename)
% coastline : exctract coastline from data
%

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Stefan Wiemer
% Last-Modified: Matt McDonnell

%% Map Limits

latlim = get(plotAxis,'ylim');
lonlim = get(plotAxis,'xlim');


if nargin<2
%     filename = 'gshhs_l.b';
    filename = 'gshhs_c.b';
end

%% get data
CoastStruc = gshhs(filename, latlim, lonlim);

plotArgs = {[CoastStruc.Lon],[CoastStruc.Lat],'k','tag','coastline'};
% set(plotAxis,'xlim',lonlim,'ylim',latlim);



%figure
%geoshow([CoastStruc.Lat],[CoastStruc.Lon]);

%plot(plotAxis,[CoastStruc.Lon],[CoastStruc.Lat],'k');
%set(plotAxis,'xlim',lonlim,'ylim',latlim);


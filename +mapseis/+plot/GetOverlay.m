function [Coast_PlotArgs, InternalBorder_PlotArgs] = GetOverlay(evStruct,MapConfig)
	% GetOverlay : exctract coastline from data
	%
	% writes the newest coastline and border into the datastore
	% and allows to change the color and resolution of the overlay
	
	% This file is part of MapSeis.
	
	% MapSeis is free software: you can redistribute it and/or modify
	% it under the terms of the GNU General Public License as published by
	% the Free Software Foundation version 3 of the License.
	
	% MapSeis is distributed in the hope that it will be useful,
	% but WITHOUT ANY WARRANTY; without even the implied warranty of
	% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	% GNU General Public License for more details.
	
	%You should have received a copy of the GNU General Public License
	% along with MapSeis.  If not, see <http://www.gnu.org/licenses/>.
	
	% Author: Stefan Wiemer
	% Last-Modified: David Eberhard
	% Copyright 2013 David Eberhard
	
	
	%% Map Limits
	import mapseis.projector.*;
	
	if nargin<2
		MapConfig=struct('Resolution',3,...
				'Coast_Color',2,...
				'Border_Color',1);
	end
	
	Colorlist={'k';'b';'r';'g';'c';'m'};
	%to come as soon as the plot function can support it
	%Colorlist={'k';'b';'r';'g';'c';'m';[0.8 0.8 0.8]};
	ResListRaw={'gshhs_c.b';'gshhs_l.b';'gshhs_i.b';'gshhs_h.b';'gshhs_f.b'};
	ResListIndexed={'gshhs_c.i';'gshhs_l.i';'gshhs_i.i';'gshhs_h.i';'gshhs_f.i'};
	
	% Locs = evStruct.getLocations();
	Locs = getLocations(evStruct);
	latlim =  [min(Locs(:,2)) max(Locs(:,2)) ];
	lonlim = [min(Locs(:,1)) max(Locs(:,1)) ];
	
	%Files
	RawFile=ResListRaw{MapConfig.Resolution};
	IndexedFile=ResListIndexed{MapConfig.Resolution};
	
	%Check if resolution is available
	filSelect=MapConfig.Resolution;
	while filSelect>1
		if exist(ResListRaw{filSelect},'file') == 0
			filSelect=filSelect-1;
		else
			break
		end	
	end
	
	
	%Files
	RawFile=ResListRaw{filSelect};
	IndexedFile=ResListIndexed{filSelect};
	disp(['used ',RawFile,' file']);
	
	%Colors
	CoastColor=Colorlist{MapConfig.Coast_Color};
	BorderColor=Colorlist{MapConfig.Border_Color};
	
	%% Get Coastline
	try
	
	    if exist(IndexedFile,'file') == 0
	        gshhs(RawFile,'createindex');
	    end
	    CoastStruc = gshhs(RawFile, latlim, lonlim);
	    Coast_PlotArgs =  {[CoastStruc.Lon],[CoastStruc.Lat],CoastColor};
	
	catch
	    Coast_PlotArgs= [];
	end
	
	%% Get Internal Borders
	try 
	%load Internal_Border.dat
	load('./AddOneFiles/Internal_Border/Internal_Border.mat')
	
	% l = isnan(Internal_Border(:,1)) | Internal_Border(:,1) > lonlim(2) | Internal_Border(:,1) < lonlim(1) ...
	%      | Internal_Border(:,2) > latlim(2) | Internal_Border(:,2) < latlim(1); 
	%  
	%  Internal_Border(l,:) = [];
	 InternalBorder_PlotArgs =  {[Internal_Border(:,1)],[Internal_Border(:,2)],BorderColor};
	catch
	     InternalBorder_PlotArgs= [];
	end

end
 
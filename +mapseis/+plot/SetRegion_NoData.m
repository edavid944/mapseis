function SetRegion_NoData(plotAxis,regionFilter,regionType,AddPar)
% SetRegion_NoData : set the region of interest for the analysis

%Similar to the normal SetRegion, but does not need a datastore, this allows
%to use this routine in datastore-free enviroment or in cases where more than
%one datastore are used.

%AddPar has to be used in some cases for the radius, the profil width and the 
%overall boundary box in selecting 'all'.


import mapseis.util.*;
import mapseis.region.*;
import mapseis.projector.*;
import mapseis.util.gui.SetImMenu;


if ~isempty(plotAxis)
	axes(plotAxis);	
end

try
    switch regionType
        case {'in','out'}
            %uistate = uisuspend(gcf);
            %pts = Ginput()
            %polyg=polyselect;
            polyg=impoly;
            newRegion = Region(polyg.getPosition);
            %newRegion = Region(pts);
            regionFilter.setRegion(regionType,newRegion);
            %uirestore(uistate);
            
        
        case {'polygon','line'}
            %uistate = uisuspend(gcf);
            %Xsec_pos = Ginput(2);
            polyg=imline;
            pline=polyg.getPosition;
            wid = regionFilter.Width;
            if isempty(wid)
            	wid = AddPar.width
            end
            
            Regio=Slicer(pline, wid);
            
            %Create DepthRegion
           % [Selected,Unselected] = getDepths(dataStore);
         	[lat lon dep] = depthpoly(pline,AddPar.depths);
         	Dregion = DepthRegion(lat,lon,dep);
         	
            regionFilter.setRegion('line',Regio.pRegion);
            regionFilter.setDepthRegion(Dregion,wid);

            %uirestore(uistate);

        case {'circle'}
        	%creates a circle for the selection
        	%polyg=impoint;
        	circ=Ginput(1);%polyg.getPosition;
        	
        	%correct the position to the lower left corner (imellipse)
        	if isempty(regionFilter.Radius)
        		datagrid = AddPar;
			regionFilter.setRadius(datagrid.rad);
		end
		
        	newRegion = [circ(1),circ(2)];
        	
        	regionFilter.setRegion(regionType,newRegion);
        	
        case {'all'}
            % Get all the event locations and define the region as the
            % bounding box       
            %LonLatMatrix = getLocations(dataStore);
            %lons = LonLatMatrix(:,1);
            %lats = LonLatMatrix(:,2);
            
            minLat = AddPar.minLat;
            maxLat = AddPar.maxLat;
            minLon = AddPar.minLon;
            maxLon = AddPar.maxLon;
            pRegion = Region([minLon minLat; maxLon minLat;...
                maxLon maxLat; minLon maxLat]);
            %regionFilter.setRegion('in',pRegion);
            regionFilter.setRegion('all',pRegion);
    end
    % Run the filter
   % regionFilter.execute(dataStore);
catch ME
    warning('SetRegion:region_not_set','Unable to set region');
end

		
	function [lat,lon,dep] = depthpoly(pos, depths)
		%creates the input for the depth region
						
		%create a new retangular for the depthRegion with 
		%minmax depths from the catalog
        	depmin = min(depths);
        	depmax = max(depths);
	
        	%first point
		lat(1) = pos(1,1);
	 	lon(1) = pos(1,2);
	 	dep(1) = depmin;
	 			
	 	%second dpoint
	 	lat(2) = pos(1,1);
		lon(2) = pos(1,2);
		dep(2) = depmax;
		
		%third dpoint
		lat(3) = pos(2,1);
		lon(3) = pos(2,2);
		dep(3) = depmax;
		
		%last dpoint
		lat(4) = pos(2,1);
		lon(4) = pos(2,2);
		dep(4) = depmin;
	end	




end
function  [handle legendentry] = PlotLandMass(plotAxis,ResStruct)	
	%This function belongs to the new earthquake plot function package
	%and plots only the coastline
	
	%Data:				This can either be a matrix with two coloumb (lon, lat) or 
	%					a datastore object.
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'Longitude'
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Latitude'
	%X_Axis_Limit: 		The limit is only used in case 'CutArea' is true it can also set to 'auto'
	%			in this case the maximum lon of the datastore will be used to cut the area
	%Y_Axis_Limit: 		Same for the Y-Axis
	%LineStylePreset:	This allows to select different style of lines: available are:
	%					'normal', 'Fatline' and 'dotted'
	%Colors:				With this entry the color of the line can be selected, possible are:
	%					'blue','red','green','black','cyan','magenta','landgreen'
	%SmallestMass:		Areas with less corners will not be drawn
	%Transparence:		Says it all [0-1]
	%Dimension:		Use 2D for 2d plot and 3D for 3d plot
	%CutArea:		if set to true only parts specified in the X_Axis_Limit and Y_Axis_Limit, should 
	%			save some computing power
	
	import mapseis.projector.*;
	
	
	%use the last plot if not specified	
	if nargin<2
    	plotAxis=gca;	
	end	
	
	landgreen=[0.7 1 0.8];
	
	SmallestMass=1000;
	AlphaMan=0.3;
	CutArea=false;
	
	if isfield(ResStruct,'SmallestMass')
		if ~isempty(ResStruct.SmallestMass)
			SmallestMass=ResStruct.SmallestMass;
		end
	end

	if isfield(ResStruct,'Transparence')
		if ~isempty(ResStruct.Transparence)
			AlphaMan=ResStruct.Transparence;
		end
	end
	
	if isfield(ResStruct,'CutArea')
		if ~isempty(ResStruct.CutArea)
			CutArea=ResStruct.CutArea;
		end
	end
	
	
	
	%check if Data is datastore or a matrix (it is only checked if it is an object) 
	if isobject(ResStruct.Data)
		try
			coast_Args=ResStruct.Data.getUserData('Coast_PlotArgs');
			lon=coast_Args{1};
			lat=coast_Args{2};
		
		catch
			lon=[];
			lat=[];
		end
		
	else %data is a matrix
		lon=ResStruct.Data(:,1);
		lat=ResStruct.Data(:,2);
	
	end
	
	
	if CutArea
		disp('Cut the area')
		if strcmp(ResStruct.X_Axis_Limit,'auto')
			[selectedLonLat,unselectedLonLat] = getLocations(ResStruct.Data);
			lonLimit=[min(selectedLonLat(:,1))-1,max(selectedLonLat(:,1))+1];
			%disp('automatic x');
			%disp(lonLimit)
		else
			lonLimit=ResStruct.X_Axis_Limit;
		end
	
		if strcmp(ResStruct.Y_Axis_Limit,'auto')
			[selectedLonLat,unselectedLonLat] = getLocations(ResStruct.Data);
			latLimit=[min(selectedLonLat(:,2))-1,max(selectedLonLat(:,2))+1];
			%disp('automatic y');
			%disp(latLimit)
		else
			latLimit=ResStruct.Y_Axis_Limit;
		end
		
		SelCriterion=((lon>=lonLimit(1)&lon<=lonLimit(2))&(lat>=latLimit(1)&lat<=latLimit(2)))|isnan(lon);
		lon=lon(SelCriterion);
		lat=lat(SelCriterion);
		%disp(sum(~isnan(lon)))
	
	end
	
	
	%now split it into blocks
	Sepi=isnan(lon);
	CountArr=1:numel(lon);
	Separators=CountArr(Sepi);
	
	%only use the largest
	DiffSep=diff(Separators);
	UseIt=DiffSep>SmallestMass;
	CountArr=1:numel(DiffSep);
	UsedSep=Separators(CountArr(UseIt));
	UsedSep=[1,UsedSep];
	disp(UsedSep)
	
	%change the color to the preset if needed
	if ~isempty(ResStruct.Colors) 
		
		switch ResStruct.Colors
			case 'blue'
				TheCol='b';
			case 'red'
				TheCol='r';
			case 'green'
				TheCol='g';
			case 'black'
				TheCol='k';
			case 'cyan'
				TheCol='c';
			case 'magenta'
				TheCol='m';
			case 'landgreen'
				TheCol=landgreen;

		end
	end

	
	
	
	
	switch ResStruct.Dimension
		case '2D'
			%first one
			%plotArr=[lon(1:UsedSep(1)-1)',lat(1:UsedSep(1)-1)'];
			%notNaN=~isnan(plotArr(:,1));
			%plotArr=plotArr(notNaN,:);
			%disp(size(plotArr))
			%handle(1)=fill(plotArr(:,1),plotArr(:,2),TheCol);
			%hold on
			verts={};
			
			for i=1:(numel(UsedSep)-1)	
				plotArr=[lon(UsedSep(i)+1:UsedSep(i+1)-1)',...
					lat(UsedSep(i)+1:UsedSep(i+1)-1)'];
				notNaN=~isnan(plotArr(:,1));
				plotArr=plotArr(notNaN,:);	
				%handle(i)=patch(plotArr(:,1),plotArr(:,2),TheCol);
				verts={verts{:},plotArr(:,1), plotArr(:,2),TheCol};				
			end
			axes(plotAxis)
			handle=fill(verts{:},'LineStyle','none');
			
			%hold off
			
		case '3D'
			disp('full 3D')
			%first one
			%plotArr=[lon(1:UsedSep(1)-1)',lat(1:UsedSep(1)-1)',zeros(numel(lon(1:UsedSep(1)-1)),1)];
			%notNaN=~isnan(plotArr(:,1));
			%plotArr=plotArr(notNaN,:);
			%handle(1)=fill3(plotArr(:,1),plotArr(:,2),plotArr(:,3),TheCol);
			
			
			verts={};
			for i=1:(numel(UsedSep)-1)
				plotArr=[lon(UsedSep(i)+1:UsedSep(i+1)-1)',...
					lat(UsedSep(i)+1:UsedSep(i+1)-1)',...
					zeros(numel(lon(UsedSep(i)+1:UsedSep(i+1)-1)),1)];
				notNaN=~isnan(plotArr(:,1));
				plotArr=plotArr(notNaN,:);	
				verts={verts{:},plotArr(:,1), plotArr(:,2),plotArr(:,3),TheCol};
				%handle(i)=patch(plotArr(:,1),plotArr(:,2),plotArr(:,3),TheCol);
			end
			
			axes(plotAxis)
			handle=fill3(verts{:},'LineStyle','none');
		
	end	
	
	%set alpha
	if ~strcmp(AlphaMan,'none')
		alpha(handle,AlphaMan);
	end
	
	%now plot it (ask questions later)
	%handle = plot(plotAxis,lon,lat);
	
	
	%change to default linewitdth of 1
	%set(handle,'LineStyle','-','LineWidth',1);

	
	%now change plot style
	%---------------------
	
		
	%set the axis labels (should not be needed but it is a good idea anyway
	if isfield(ResStruct,'X_Axis_Label')
		X_axis_name=ResStruct.X_Axis_Label;
		
		if isempty(X_axis_name)
			X_axis_name='Longitude';
		end
		
		hXLabel = xlabel(plotAxis,X_axis_name);
		set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
	end
	
	
	if isfield(ResStruct,'Y_Axis_Label')
		Y_axis_name=ResStruct.Y_Axis_Label; 	
		
		if isempty(Y_axis_name)
			Y_axis_name='Latitude';
		end
		
		hYLabel = ylabel(plotAxis,Y_axis_name);
		set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end
	
	
	%change the linestyle with a preset if needed 
	if ~isempty(ResStruct.LineStylePreset) 
		switch ResStruct.LineStylePreset
			case 'none'
				set(handle,'LineStyle','none','LineWidth',1);
			case 'normal'
				set(handle,'LineStyle','-','LineWidth',1);
			case 'dotted'
				set(handle,'LineStyle',':','LineWidth',1);
			case 'Fatline'
				set(handle,'LineStyle','-','LineWidth',1.5);

		
		end
	end

	
	
	
	%Legendentry
	legendentry='Coastline'
	
	
			
end
function ShiftAxis(plotAxis,Offsets)
	%Retags the axis according to the Offset
        
        import mapseis.util.ShiftCoords;
     
	%get all ticks and convert them
	xticktator=get(plotAxis,'XTick');
	xticker=(-180+OffSets(1)):abs(xticktator(2)-xticktator(1)):(180+OffSets(1));
	ShiftedLocations=ShiftCoords([xticker',zeros(size(xticker'))],-OffSets);
	set(plotAxis,'XTick',xticker,'XTickLabel',num2str(ShiftedLocations(:,1)));
				
	yticktator=get(plotAxis,'YTick');
	yticker=(-90+OffSets(2)):abs(yticktator(2)-yticktator(1)):(90+OffSets(2));
	ShiftedLocations=ShiftCoords([zeros(size(yticker')),yticker'],-OffSets);
	set(plotAxis,'YTick',yticker,'YTickLabel',num2str(ShiftedLocations(:,2)));
	
	
	
end

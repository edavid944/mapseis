function h=PlotDepthHistDiff(plotAxis,evDepths)
% PlotMagHist : Plot histogram of event magnitudes

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if nargin<2
    plotAxis=gca;
end

h=[];

if ~isempty(evDepths)
		%get minimal maximal depth
		%depthmin = floor(min([evDepths(:,1) ; evDepths(:,2)])) ;   
   		%depthmax = ceil(max([evDepths(:,1) ; evDepths(:,2)])) ;
		DepthDiff=evDepths(:,1)-evDepths(:,2);
		depthmin = floor(min(DepthDiff)) ;   
   		depthmax = ceil(max(DepthDiff)) ;

		
		
		binvec = depthmin:1:depthmax;

		hist(DepthDiff,binvec);
		%h2 = hist(evDepths(:,2),binvec);
		h = findobj(gca,'Type','patch');
		%h=bar(abs(h1-h2));
		%h=area(binvec,abs(h1-h2));
		xlim([depthmin,depthmax]);
		
		
		hXLabel = xlabel(plotAxis,'Depth Difference (A-B) in [km]');
		hYLabel = ylabel(plotAxis,'No. of Events');
		hTitle = title(plotAxis,' ');
		
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 14,'FontWeight' , 'bold');
		set( hTitle                          , 'FontSize'   , 16          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'off'     , ...
		  'TickDir'     , 'out'     , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'of'      , ...
		  'XGrid'       , 'of'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1.5,         ...
		  'FontWeight' , 'bold',     ...
		  'FontSize'   , 14);

else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end

end
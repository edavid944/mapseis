function  [handle legendentry] = PlotScatter(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	
	%the Scatter plot allows to plot up to 4 values, it may be a replacement for earthquake plot
	%possible config fields for this function:
	%Data:				Not optional, the data to plot Data(:,1) for the x-values
	%					Data(:,2) for the y-values, Data(:,3) for the Area Value and
	%					Data(:,4) for Colordata (can left empty) 
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'X', if the field is missing, nothing will be
	%					done
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y', if the field is missing, nothing will be
	%					done			
	%FilledMarkers:		Fills the marker with the color if set to true, should not be used with some Markers
	%MarkerStylePreset:	This allows to change the Style of the Marker (if plotted) with simple Presets 
	%					if not not specified the default style ('normal') will be used. The presets 
	%					are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%					which try use different MarkerStyles every plot: 'automatic' and 'auto_filled' which 
	%					does not use the point ('.').
  	%CustomMarker:	 	This allows to specify the Marker properties freely, CustomMarker{1} defines
	%					the Marker and CustomMarker{2} defines the MarkerSize. CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomMarker should be missing in
	%					ResStruct
	%MarkerSizeLimits:	First value set the minimum Size of the Marker and second value sets the maximum size:
	%					default values if left empty are 1 and 100
	%MarkerRange:		This sets the minimum value for the smallest marker (1st value) and the maximum value for the
	%					biggest Marker, by default the minimum and maximum of the data(:,3) will be used.
	%SizeStep:			If left empty the size of the markers will be varied over the whole range, use a number to less
	%					different sizes of markers.  
	%Colors:			This defines either the ColorMap or the Color if only 3 coloumbs are defined
	%					Option for the ColorMap are: 'jet', 'hsv', 'hot', 'gray', 'cool', 'bone', 'autumn', 'summer'
	%					In case of a single colorthe presets are: 'blue','red','green','black','cyan','magenta' and 
	%					the automatic settings which try use different Color every plot.
	%					default for the ColorMap is 'jet'and for a single Color it is 'blue'		
	%CustomColors:		This allows to specify the ColorMap properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColorMap should be missing in
	%					ResStruct
	%Colorize:			This option uses an alternative colorisation algorithm, which also to use differnt colormaps
	%					for every data set, the disadvantage is, that the color cannot be changed with a set command
	%					Set to 'true' uses the alternative function, 'false' the normal internal function.
	%					Works only with 4 dataparameters.
	%ColorStep:			Needed for Colorize, determines how many colors are used, default value is 256.
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%C_Axis_Limit:		Limits the Range of the Colorbar, syntax, the same as X_Axis_Limit
	%ColorToggle:		Set to true will show the ColorBar
	%LegendText:		In this case it will just be written to he output variable legendentry if specified, else
	%					'Data' will be used as LegendEntry.
	
	import mapseis.util.*;
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	
	disp(ResStruct);
	
	
	%Build some internal data settings
	wheelMarker={'.','+','o','x','diamond'}
	wheelMarker_nodot={'+','o','x','diamond'}
	wheelMarker_filled={'o','<','s','diamond','p'}
	wheelColor={'b','r','g','k','c','m'}
	
	
	
	%Prepare data
	%--------------
	
	minSize=1;
	maxSize=100;
	
	
	Colorize=false;
	if isfield(ResStruct,'Colorize')
		if ~isempty(ResStruct.Colorize)
			Colorize=ResStruct.Colorize;
		end
	end
	
	ColorRes=256;
	if isfield(ResStruct,'Colorize')
		if ~isempty(ResStruct.Colorize)
			Colorize=ResStruct.Colorize;
		end
	end	
		
	
	if isfield(ResStruct,'MarkerSizeLimits')
		if ~isempty(ResStruct.MarkerSizeLimits)
			minSize=ResStruct.MarkerSizeLimits(1);
			maxSize=ResStruct.MarkerSizeLimits(2);
		end
	end	
	
	disp(minSize)
	disp(maxSize)
	
	
	minRange=min(ResStruct.Data(:,3));
	maxRange=max(ResStruct.Data(:,3));		
	
	if isfield(ResStruct,'MarkerRange')
		if ~isempty(ResStruct.MarkerRange)
			minRange=ResStruct.MarkerRange(1);
			maxRange=ResStruct.MarkerRange(2);
		end
	end	
	
	MarkerNr=numel(ResStruct.Data(:,3));
	
	if isfield(ResStruct,'SizeStep')
		if ~isempty(ResStruct.SizeStep)
			MarkerNr=ResStruct.SizeStep;
		end
	end	

	%build size array
	StepWidth=abs(maxSize-minSize)/MarkerNr;
	MarkerSizeArray=minSize:StepWidth:maxSize;
	
	%In case of very large MarkerNr, numbers of steps can change (error of rounding) to prevent 
	%errors, the MarkerNr is set again.
	MarkerNr=numel(MarkerSizeArray);
	
	%normalize the data to values between 0 and 1
	NormData = (ResStruct.Data(:,3)- minRange) / abs(maxRange-minRange);
	%set lower and upper bound to 0 and 1
	NormData(NormData<0)=0;
	NormData(NormData>1)=1;
	
	%get index numbers
	SizeIndex=round(NormData*(MarkerNr-1)+1);
	
	%finally the Sizes
	DataArea=MarkerSizeArray(SizeIndex);
		
	
	
	%plot the data
	%--------------
	if numel(ResStruct.Data(1,:))==4
		if ~isempty(ResStruct.FilledMarkers) & ResStruct.FilledMarkers
			handle = scatter(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2), DataArea, ResStruct.Data(:,4), 'filled');
		else
			handle = scatter(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2), DataArea, ResStruct.Data(:,4));
		end
	else
		if ~isempty(ResStruct.FilledMarkers) & ResStruct.FilledMarkers
			handle = scatter(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2), DataArea, 'filled');
		else
			handle = scatter(plotAxis,ResStruct.Data(:,1),ResStruct.Data(:,2), DataArea);
		end
	end
	
	%now change the parameters of the plot
	%-------------------------------------
	
		%the labels: if not set, set to 'X' and 'Y'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Y';
			end
			
			hYLabel = ylabel(plotAxis,Y_axis_name);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
		

		
		
		
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.MarkerStylePreset) 			
			switch ResStruct.MarkerStylePreset
				case 'normal'
					set(handle,'Marker','.')
				case 'plus'
					set(handle,'Marker','+')
				case 'circle'
					set(handle,'Marker','o')
				case 'cross'
					set(handle,'Marker','x')
				case 'diamond'
					set(handle,'Marker','diamond')
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','hggroup'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1});
				case 'auto_filled'
					%maybe usefull to avoid the small dot symbol
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','hggroup'));
					set(handle,'Marker',wheelMarker_filled{mod(existPlots,5)+1});
			
			end
		end

		
		%Customization Line
		if isfield(ResStruct,'CustomMarker')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Marker',ResStruct.CustomMarker);
		end	

		if ~Colorize
		
			%change the color to the preset if needed
			if ~isempty(ResStruct.Colors) 
				
				if numel(ResStruct.Data(1,:))==4
					switch ResStruct.Colors
						case 'jet'
							colormap(plotAxis,jet);
						case 'hsv'
							colormap(plotAxis,hsv);
						case 'hot'
							colormap(plotAxis,hot);
						case 'cool'
							colormap(plotAxis,cool);
						case 'gray'
							colormap(plotAxis,gray);
						case 'bone'
							colormap(plotAxis,bone);
						case 'summer'
							colormap(plotAxis,summer);
						case 'autumn'
							colormap(plotAxis,autumn);
									
					end
					
				else
					switch ResStruct.Colors
						case 'blue'
							set(handle,'Color','b')
						case 'red'
							set(handle,'Color','r')
						case 'green'
							set(handle,'Color','g')
						case 'black'
							set(handle,'Color','k')
						case 'cyan'
							set(handle,'Color','c')
						case 'magenta'
							set(handle,'Color','m')
						case 'automatic'
							%experimental but should work
							childish=get(plotAxis,'Children');
							existPlots=numel(findobj(childish,'Type','hggroup'));
							set(handle,'Color',wheelColor{mod(existPlots,6)+1});
									
					end
				end	
			end
	
			%Customization Color
	
			if isfield(ResStruct,'CustomColors')
				if numel(ResStruct.Data(1,:))==4
					colormap(plotAxis,ResStruct.CustomColors);
				else		
					%This option allows to customize the line with the common matlab commands
					set(handle,'Color',ResStruct.CustomColors);
				end
			end	
			
			
		else
			%alternative color function
			
			%get a default Colormap
			switch ResStruct.Colors
				case 'jet'
					MyMap=jet(ColorRes);
				case 'hsv'
					MyMap=hsv(ColorRes);
				case 'hot'
					MyMap=hot(ColorRes);
				case 'cool'
					MyMap=cool(ColorRes);
				case 'gray'
					MyMap=gray(ColorRes);
				case 'bone'
					MyMap=bone(ColorRes);
				case 'summer'
					MyMap=summer(ColorRes);
				case 'autumn'
					MyMap=autumn(ColorRes);
									
			end
		
			if isfield(ResStruct,'CustomColors')
				
				MyMap=ResStruct.CustomColors;
				ColorRes=numel(MyMap(:,1));		

			end
			
			%edit later
			%Some Lines needed for ColorMapping

			%set the plot color
			set(scatMan,'CData',NCData)
			
			
			protoMaop(:,:,1)=uint8(round(NewCMap(:,1)*255))
			protoMaop(:,:,2)=uint8(round(NewCMap(:,2)*255))
			protoMaop(:,:,3)=uint8(round(NewCMap(:,3)*255))
			
			
			%used to set the axis ticks right
			caxis([min max])
			
			thebar=colorbar
			realbar=get(thebar,'Children')
			set(realbar,'CData',protoMaop)
					
			
		end
		
		
		
		


		%set limits if needed
		if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.C_Axis_Limit) | strcmp(ResStruct.C_Axis_Limit,'auto')
			caxis(plotAxis,'manual');
			caxis(plotAxis,ResStruct.C_Axis_Limit);	
		end

		
		if isfield(ResStruct,'ColorToggle');
			if ~isempty(ResStruct.ColorToggle) & ResStruct.ColorToggle
				colorbar;
			end
		end	
		
		
		%build legend (if not empty it will just copy the entry from ResStruct)
		if isfield(ResStruct,'LegendText');	
			if ~isempty(ResStruct.LegendText)
				legendentry=ResStruct.LegendText;
			else	
				legendentry='Data';
			end
		else
		legendentry=[];
					
		end		
	
	
end
function  [handle legendentry] = PlotText3D(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	%It allows to plot any text somewhere on the specified subplot
	%The Legendentry is only for consistence witgh the other generic 
	%plot functions
	
	%possible Configs
	%Position:		The Position of the text in the plot (x,y,z)
	%CoordType:		The Type of the Coordinates in the plot: 'normalized' for 
	%				x and y coordinates between 0 and 1 and 'real' for coordinates
	%				in the real dimension of the plot (km, degree, angle, whatever)
	%				for the text arrow it is at the moment not possible to use real 
	%				coordinates, the normalized coordinates are in this casefor the 
	%				whole figure and not only a a subfigure				
	%String:		The actual text, in order to have more than one line
	%				use a cell array with strings
	%TextType:		The wanted TextStyle: available are 'normal', an ordinary text
	%				'textbox', text in box (done by text) and 'arrowbox'
	%				an arrow with a textbox attached to it (can not be left empty)
	%TextStyle:		With this parameter different preset can be selected for the text 
	%				(containing: fonts, size, weight, color, etc)
	%				The presets: 'normal' ,'small', 'fat' ,'big' 'red' , 'green'					
	%CustomStyle:	Changes Property of the text object. This entry has to be a 
	%				Cell array with the first coloumb describing the property 
	%				and the second coloumb describing the value 

	
	
	
		
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	

	  
	%active the wanted axis (only way to do it)
	axes(plotAxis);
	
	%get limits of the axes
	x_axis_limit=xlim;
	y_axis_limit=ylim;
	z_axis_limit=zlim;
	
	%change coordinates if needed
	if (strcmp(ResStruct.CoordType,'normalized')  | isempty(ResStruct.CoordType)) &...
		~strcmp(ResStruct.TextType,'arrowbox')
		%texts need real coords --> change them
		x_coord=(ResStruct.Position(1)-x_axis_limit(1))/ abs(x_axis_limit(2)-x_axis_limit(1));
		y_coord=(ResStruct.Position(2)-y_axis_limit(1))/ abs(y_axis_limit(2)-y_axis_limit(1));
		z_coord=(ResStruct.Position(3)-z_axis_limit(1))/ abs(z_axis_limit(2)-z_axis_limit(1));
	else
		%The coordinates should be just right
		x_coord=ResStruct.Position(1);	
		y_coord=ResStruct.Position(2);
		z_coord=ResStruct.Position(3);
	end
	
	
	%Now draw the text
	switch ResStruct.TextType
		case 'normal'
			if (strcmp(ResStruct.CoordType,'normalized')  | isempty(ResStruct.CoordType))
				%has to been done, because in some case text will use the wrong coordinates
				handle=text(x_coord,y_coord,z_coord,ResStruct.String,'Units','normalized');
			else	
				handle=text(x_coord,y_coord,z_coord,ResStruct.String);
			end
			
		case 'textbox'
			if (strcmp(ResStruct.CoordType,'normalized')  | isempty(ResStruct.CoordType))
				%has to been done, because in some case text will use the wrong coordinates
				handle=text(x_coord,y_coord,z_coord,ResStruct.String,'Units','normalized');
			else	
				handle=text(x_coord,y_coord,z_coord,ResStruct.String);
			end

			set(handle, 'EdgeColor','k');
		
		case 'arrowbox'
			handle=annotation('textarrow',[x_coord y_corrd]);
			set(handle, 'String',ResStruct.String);
				
	end	
	
	
	%defaults setting for the text is like the normal labels
	set(handle,'FontName', 'AvantGarde');
	set(handle, 'FontSize', 12);
		
	%the presets (presets have to be tested to see if they work
	switch ResStruct.TextStyle
		case 'normal'
			%nothing to do already set
		
		case 'small'
			set(handle, 'FontSize', 8);
			
		case 'fat'
			set(handle, 'FontWeight', 'bold');
			
		case 'big'
			set(handle, 'FontWeight', 'demi','FontSize',14);
			
		case 'red'
			set(handle,'Color','r');
			%for boxes and arrows
			if ~strcmp(ResStruct.TextType,'normal')
				set(handle,'EdgeColor','r');
			end
			
		case 'green'
			set(handle,'Color','g');
				%for boxes and arrows
				if ~strcmp(ResStruct.TextType,'normal')
					set(handle,'EdgeColor','g');
				end
	end	
	
	
	%Check for customStyling
	if isfield(ResStruct,'CustomStyle')
		if ~isempty(ResStruct.CustomStyle);		
			
			for i=1:numel(ResStruct.CustomStyle(:,1));
				set(handle,ResStruct.CustomStyle(i,1),ResStruct.CustomStyle(i,1));	
			end			
		end
	end
		
	
	legendentry=[];
	
	
	
	
end
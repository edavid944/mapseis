function SetRegion(MainGUI,regionFilter,regionType,dataStore)
% SetRegion : set the region of interest for the analysis

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 73 $    $Date: 2008-10-15 14:19:56 +0100 (Wed, 15 Oct 2008) $
% Author: Matt McDonnell

import mapseis.util.*;
import mapseis.region.*;
import mapseis.projector.*;
import mapseis.util.gui.SetImMenu;
try
    switch regionType
        case {'in','out'}
            %uistate = uisuspend(gcf);
            %pts = Ginput()
            %polyg=polyselect;
            polyg=impoly;
            newRegion = Region(polyg.getPosition);
            %newRegion = Region(pts);
            regionFilter.setRegion(regionType,newRegion);
            %uirestore(uistate);
            
        
        case {'polygon','line'}
            %uistate = uisuspend(gcf);
            %Xsec_pos = Ginput(2);
            polyg=imline;
            pline=polyg.getPosition;
            wid = regionFilter.Width;
            if isempty(wid)
            	wid = GetProfileWidth(dataStore);
            end
            
            Regio=Slicer(pline, wid);
            
            %Create DepthRegion
            [Selected,Unselected] = getDepths(dataStore);
         	[lat lon dep] = depthpoly(pline,Selected);
         	Dregion = DepthRegion(lat,lon,dep);
         	
            regionFilter.setRegion('line',Regio.pRegion);
            regionFilter.setDepthRegion(Dregion,wid);

            %uirestore(uistate);

        case {'circle'}
        	%creates a circle for the selection
        	%polyg=impoint;
        	circ=Ginput(1);%polyg.getPosition;
        	
        	%correct the position to the lower left corner (imellipse)
        	if isempty(regionFilter.Radius)
        		datagrid = dataStore.getUserData('gridPars');
			regionFilter.setRadius(datagrid.rad);
		end
		
        	newRegion = [circ(1),circ(2)];
        	
        	regionFilter.setRegion(regionType,newRegion);
        	
        case {'all'}
            % Get all the event locations and define the region as the
            % bounding box       
            LonLatMatrix = getLocations(dataStore);
            lons = LonLatMatrix(:,1);
            lats = LonLatMatrix(:,2);
            minLat = min(lats);
            maxLat = max(lats);
            minLon = min(lons);
            maxLon = max(lons);
            pRegion = Region([minLon minLat; maxLon minLat;...
                maxLon maxLat; minLon maxLat]);
            %regionFilter.setRegion('in',pRegion);
            regionFilter.setRegion('all',pRegion);
    end
    % Run the filter
    regionFilter.execute(dataStore);
catch ME
    warning('SetRegion:region_not_set','Unable to set region');
end

		
		 function [lat,lon,dep] = depthpoly(pos, depths)
	 					%creates the input for the depth region
	 					
	 					%create a new retangular for the depthRegion with 
	 					%minmax depths from the catalog
            					depmin = min(depths);
            					depmax = max(depths);

	 							%first point
	 							lat(1) = pos(1,1);
	 							lon(1) = pos(1,2);
	 							dep(1) = depmin;
	 							
	 							%second dpoint
	 							lat(2) = pos(1,1);
	 							lon(2) = pos(1,2);
	 							dep(2) = depmax;
	 							
	 							%third dpoint
	 							lat(3) = pos(2,1);
	 							lon(3) = pos(2,2);
	 							dep(3) = depmax;
	 							
	 							%last dpoint
	 							lat(4) = pos(2,1);
	 							lon(4) = pos(2,2);
	 							dep(4) = depmin;
				 end	




end
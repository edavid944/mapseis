function [handles legendentry] = PlotRegionLight3D(plotAxis,eventStruct,mags)
%A clone of PlotRegion like PlotRegionLight is but 3D, it is used by PlotEarthquake3D
%The differences are very small between the  2D and the 3D version.

%may have to be cleaned up a bit later, but it should be fine, just a lot of code not
%needed anymore in this function.



if nargin<2
    plotAxis=gca;
end

if nargin<3
    mags=[];
end

   	%if isempty(mags)
   	%	mags=ones(numel(eventStruct.inRegion(:,1),1));
   	%
   	%end
   	
   	%create configuration of the magnitude
   	qualityselector=eventStruct.Quality;
   	switch qualityselector
   		case 'low'
   			    magconfig = LowQualConfig;
   		case 'med'
			    magconfig = MedQualConfig;
		case 'hi'
   				magconfig = HiQualConfig;
   		case 'lowcomp'
   				magconfig = LowCompareConfig;
 		case 'hicomp'
   				magconfig = HiCompareConfig;
		
   		end		
 	

   		%inRegion has to be 3D with third column being the depths
		inReg = eventStruct.inRegion;
		 [inRegArgs,nonEmptyInRegionIndices] = nMakePlotArgs(...
		       inReg(:,1),inReg(:,2),inReg(:,3),mags, magconfig);
		   
		 plotArgs = {inRegArgs{:}};
			


			 %% Plot the results
		    % Plot the points inside the region of interest, outside the region of
		    % interest and the boundary
		    % Keep the handles of the data lines for setting symbol shapes and
		    % other properties
		    lHnds = nPlotEvents(plotAxis,plotArgs);
			
			% Count of number of data lines in the region and outside the region
			dataInRegionCount = numel(nonEmptyInRegionIndices);
			
			% Handles to the lines for events inside the region of interest
		    inRegHnds = lHnds((1:dataInRegionCount));
		    % Handles to the line for boundary points
		    nSetPlotProperties(inRegHnds,nonEmptyInRegionIndices,eventStruct.inout,magconfig);
		   % try
		    	%nSetAxisLimits(plotAxis,[inReg]);
		    %end
		    %build legend
		    legendentry = [];
		    %BuildLegend(plotAxis,magconfig,false,false,mags,false,false);
		    handles =lHnds;
		    
		    
		    %try
		    %	if eventStruct.showlegend
		    %		legend(plotAxis,'show');
		    %	end
			%end

		
%----------------------------------------------------------------------------------------
	%==============================================================================================

    function nSetAxisLimits(plotAxis,regionData)
        % Set the axis limits to just the event data limits
        minLims = 1*min(regionData);
        maxLims = 1*max(regionData);
        axis(plotAxis,[minLims(1) maxLims(1) minLims(2) maxLims(2)]);
        latlim = get(plotAxis,'Ylim');
        set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
        set(plotAxis,'DrawMode','fast');
            
    end

	
    function nSetAxisLimitsDepths(plotAxis,regionData)
        % Set the axis limits to just the event data limits
        minLims = 1*min(regionData);
        maxLims = 1*max(regionData);
        axis(plotAxis,[minLims(1) maxLims(1) minLims(2) maxLims(2)]);
        depthlim = get(plotAxis,'Ylim');
        set(plotAxis,'dataaspect',[2 1 1]);
        set(plotAxis, 'YDir', 'reverse');
        set(plotAxis,'DrawMode','fast');
            
    end



    function lHnds = nPlotEvents(plotAxis,plotArgs)
        lHnds = plot3(plotAxis,plotArgs{:});
        hXLabel = xlabel(plotAxis,'Longitude ');
        hYLabel = ylabel(plotAxis,'Latitude');
        hZLabel = zlabel(plotAxis,'Depth');
        hTitle = title(plotAxis,' ');
        
        
        set([hTitle, hXLabel, hYLabel hZLabel],       'FontName'   , 'AvantGarde');
        set([hXLabel, hYLabel hZLabel      ]        , 'FontSize'   , 12          );
        set( hTitle                          , 'FontSize'   , 12          , ...
            'FontWeight' , 'bold'      );
        
        set(plotAxis, ...
            'Box'         , 'on'     , ...
            'TickDir'     , 'out'     , ...
            'Fontsize'    , 10    , ...
            'TickLength'  , [.015 .01] , ...
            'XMinorTick'  , 'on'      , ...
            'YMinorTick'  , 'on'      , ...
            'YGrid'       , 'off'      , ...
            'XGrid'       , 'off'      , ...
            'XColor'      , [.3 .3 .3], ...
            'YColor'      , [.3 .3 .3], ...
            'LineWidth'   , 1         );
    end

	
    function lHnds = nPlotEventsSlice(plotAxis,plotArgs)
        lHnds = plot(plotAxis,plotArgs{:});
        hXLabel = xlabel(plotAxis,'Distance [km]');
        hYLabel = ylabel(plotAxis,'Depth [km]');
        hTitle = title(plotAxis,' ');
        
        
        set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
        set([hXLabel, hYLabel       ]        , 'FontSize'   , 12          );
        set( hTitle                          , 'FontSize'   , 12          , ...
            'FontWeight' , 'bold'      );
        
        set(plotAxis, ...
            'Box'         , 'on'     , ...
            'TickDir'     , 'out'     , ...
            'Fontsize'    , 10    , ...
            'TickLength'  , [.015 .01] , ...
            'XMinorTick'  , 'on'      , ...
            'YMinorTick'  , 'on'      , ...
            'YGrid'       , 'off'      , ...
            'XGrid'       , 'off'      , ...
            'XColor'      , [.3 .3 .3], ...
            'YColor'      , [.3 .3 .3], ...
            'LineWidth'   , 1         );
    end

	
    function [plotArgCell,nonEmptyIndices]=nMakePlotArgs(lon,lat,depth,mags,magconfig)
        % Define the cutoff magnitudes for each partition
        % magconfig is a structur with the configuretion of the different magnitude slots
        
        %magRangeLower = [-Inf,1,2,3,4,5,6,7,8];
        %magRangeUpper = [1,2,3,4,5,6,7,8,Inf];
        magRangeLower = magconfig.LowerRange;
        magRangeUpper = magconfig.UpperRange;
        magRangeCount = numel(magRangeLower);
        % Define a vector of logicals to represent if a given magnitude
        % range has any elements in it.  If it doesn't then the eventual
        % call to plot will not create a line handle for that magnitude
        % range, so setting the plot properties in the later stage will
        % cause a crash
        rangeNotEmpty = true(magRangeCount,1);
        % Define the symbols to use
        %magSymbols = {'.','ok','ok','sk','sk','sk','sk','dk','hk'};
        magSymbols = magconfig.MagSymbols;
        
        % Define the longitudes and latitudes
        plotArgCell = cell(1,4*numel(magSymbols));
        for partitionIndex=1:magRangeCount
            logicalIndexVect = ...
                mags >= magRangeLower(partitionIndex) & ...
                mags < magRangeUpper(partitionIndex);
            plotArgCell{1+(partitionIndex-1)*4}=...
                lon(logicalIndexVect);
            plotArgCell{2+(partitionIndex-1)*4}=...
                lat(logicalIndexVect);
            plotArgCell{3+(partitionIndex-1)*4}=...
                depth(logicalIndexVect);    
            plotArgCell{4+(partitionIndex-1)*4}=...
                magSymbols{partitionIndex};
            if all(~logicalIndexVect)
                rangeNotEmpty(partitionIndex)=false;
            end
        end
        nonEmptyIndices = (1:magRangeCount)';
        nonEmptyIndices = nonEmptyIndices(rangeNotEmpty);
    end

	
		
	

    function nSetPlotProperties(inHandles,nonEmptyIndices,regionSelect,magconfig)
        % Given dataset handles set the plot properties
        % magconfig is a structur with the configuretion of the different magnitude slots
        
        if isempty(inHandles)
            return
        end
        
        % Define the marker sizes to use
        %magMarkerSizes = [2,3,4,5,6,7,8,9,10];
        magMarkerSizes = magconfig.MarkerSize;
        
        switch regionSelect
            case 'in'
                % Define the marker face colors to use
                %magMarkerFaceColors = {'w','w','w','w','w','y','y','w','w'};
                magMarkerFaceColors = magconfig.MarkerFaceColorIn;
                
                % Define the marker colors
                %blackCol = [0 0 0];
                %redCol = [0.8 0 0];
                %magColors = {blackCol,blackCol,blackCol,blackCol,redCol,redCol,redCol,redCol,redCol};
            	magColors = magconfig.FaceColorIn;
           
            case 'out'
                % Define the marker face colors to use
                %magMarkerFaceColors = {'w','w','w','w','w','w','w','w','w'};
                magMarkerFaceColors = magconfig.MarkerFaceColorOut;
                
                % Define the marker colors
                %greyCol = [0.8 0.8 0.8];
                %magColors = {greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol};
                magColors = magconfig.FaceColorOut;
        end
        for hndIndex=1:numel(nonEmptyIndices)
            markerIndex = nonEmptyIndices(hndIndex);
            set(inHandles(hndIndex),...
                'MarkerSize',magMarkerSizes(markerIndex),...
                'MarkerFaceColor',magMarkerFaceColors{markerIndex},...
                'Color',magColors{markerIndex});
        end
        %         if strcmp(get(gcf,'Name'),'MapSeis Main Window')
        %
        %             legend(inHandles,'M < 1 ','1 <= M < 2','2<= M <3',...
        %                 '3 <= M < 4','4 <= M < 5',' 5 <= M < 6', ' 6 <= M < 7',' 7 <= M < 8',' M > 8','Location','NorthEast');
        %         end
        
    end




    function nSetBoundaryProperties(bdryHnd)
        import mapseis.region.*;
        
        % Set the properties of the boundary points, including callback
        % functions to execute eg resize boundary
        set(bdryHnd,'MarkerFaceColor',[0 0 0],'MarkerSize',2);
        if isfield(eventStruct,'regionFilter')
            %  Create a context menu to use to modify selection region
            %CreateRegionModifyMenu(bdryHnd,eventStruct);
        end
    end
    
    
    function magconfig = HiQualConfig
    	 	%creates the High Quality Magnitude Configuration
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.8 0.8 0.8];
    	 	
    	 	
    	 	%Structur
    	 	magconfig =struct('LowerRange',[-Inf,1,2,3,4,5,6,7,8],...
    	 					  'UpperRange',[1,2,3,4,5,6,7,8,Inf],...	
    	 					  'MagSymbols',{{'.','sk','sk','sk','sk','sk','sk','dk','hk'}},...	
    	 					  'MarkerSize',[2,3,4,5,6,7,8,9,10],...
                              'MarkerFaceColorIn',{{'w','w','w','w','w','y','y','w','w'}},...
                              'FaceColorIn',{{blackCol,blackCol,blackCol,blackCol,redCol,redCol,redCol,redCol,redCol}},...
                              'MarkerFaceColorOut',{{'w','w','w','w','w','w','w','w','w'}}, ...
                              'FaceColorOut',{{greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol,greyCol}});
    
    
    
    end
    
    
    function magconfig = MedQualConfig
    	 	%creates the High Quality Magnitude Configuration
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.8 0.8 0.8];
    	 	
    	 	
    	 	%Structur
    	 	magconfig =struct('LowerRange',[-Inf,3,6],...
    	 					  'UpperRange',[3,6,Inf],...	
    	 					  'MagSymbols',{{'.','sk','hk'}},...	
    	 					  'MarkerSize',[4,5,6],...
                              'MarkerFaceColorIn',{{'w','w','y'}},...
                              'FaceColorIn',{{blackCol,blackCol,redCol}},...
                              'MarkerFaceColorOut',{{'w','w','w'}}, ...
                              'FaceColorOut',{{greyCol,greyCol,greyCol}});
    
    
    
    end

    
	function magconfig = LowQualConfig
    	 	%creates the High Quality Magnitude Configuration
    	 		%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.8 0.8 0.8];
    	 	
    	 	
    	 	%Structur
    	 	magconfig =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[3],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'b'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});
    
    
    
    end

	
	function magconfig = LowCompareConfig
    	 	%creates the Low Quality Magnitude Configuration
    	 	%For the compare algo it creates three different settings, 
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.7 0.7 0.7];
    	 	
    	 	
    	 	%Structur
    	 	magconfig(1) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[7],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'r'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});
    
    		magconfig(2) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[7],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'b'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});

    		magconfig(3) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'.'}},...	
    	 					  'MarkerSize',[7],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'g'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{'g'}});

    end

	
	function magconfig = HiCompareConfig
    	 	%creates the Low Quality Magnitude Configuration
    	 	%For the compare algo it creates three different settings, 
    	 	
    	 	%Colors
    	 	blackCol = [0 0 0];
            redCol = [0.8 0 0];
            greyCol = [0.6 0.6 0.6];
    	 	
    	 	
    	 	%Structur
    	 	magconfig(1) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'o'}},...	
    	 					  'MarkerSize',[5],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'r'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});
    
    		magconfig(2) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'o'}},...	
    	 					  'MarkerSize',[5],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'b'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{greyCol}});

    		magconfig(3) =struct('LowerRange',[-Inf],...
    	 					  'UpperRange',[Inf],...	
    	 					  'MagSymbols',{{'o'}},...	
    	 					  'MarkerSize',[5],...
                              'MarkerFaceColorIn',{{'w'}},...
                              'FaceColorIn',{{'g'}},...
                              'MarkerFaceColorOut',{{'w'}}, ...
                              'FaceColorOut',{{'g'}});

    end




	function legendentry = BuildLegend(inHandles,magconfig,polyg,sliceplot,mags,coast,border)
			%creates a legend with the given input configurations
			
			if polyg
				%get lowest and highest drawn magnitude
				nu = numel(magconfig.LowerRange);
				
				%counter for magsl
				co = 1;
				
				
				
				%--------
				
				if nu == 1
					% for the zmap style
					magsl{co} = 'Earthquakes';
				else
				
					%lowermost magnitude
					num_mags = numel(mags(mags<magconfig.UpperRange(1)));
					
					if num_mags>=1				
						magsl{co} = ['M < ',num2str(magconfig.UpperRange(1))];	
						co=co+1;
					end
									
					%and the rest
					for c=2:nu-1
						num_mags = numel(mags.selected(mags>magconfig.LowerRange(c) & ...
										mags<magconfig.UpperRange(c)));
						
						if num_mags>=1
							magsl{co} =  [num2str(magconfig.LowerRange(c)),...
										' < M < ',num2str(magconfig.UpperRange(c))];
							co=co+1;
						end				
					
					end
					
					%highest magnitude
					num_mags = numel(mags(mags>magconfig.LowerRange(nu)));
					
					if num_mags>=1				
						magsl{co} = [num2str(magconfig.LowerRange(nu)),' < M'];
						co=co+1;
					end
						
				end	
%===============================================================================================
				
			else
				
				%get lowest and highest drawn magnitude
				nu = numel(magconfig.LowerRange);
				
				%counter for magsl
				co = 1;

				
				%selected
				%--------
				
				if nu == 1
					% for the zmap style
					magsl{co} = 'Earthquakes';
				else
				
					%lowermost magnitude
					num_mags = numel(mags(mags<magconfig.UpperRange(1)));
					
					if num_mags>=1				
						magsl{co} = ['M < ',num2str(magconfig.UpperRange(1))];	
						co=co+1;
					end
									
					%and the rest
					for c=2:nu-1
						num_mags = numel(mags(mags>magconfig.LowerRange(c) & ...
										mags<magconfig.UpperRange(c)));
						
						if num_mags>=1
							magsl{co} =  [num2str(magconfig.LowerRange(c)),...
										' < M < ',num2str(magconfig.UpperRange(c))];
							co=co+1;
						end				
					
					end
					
					%highest magnitude
					num_mags = numel(mags(mags>magconfig.LowerRange(nu)));
					
					if num_mags>=1				
						magsl{co} = [num2str(magconfig.LowerRange(nu)),' < M'];
						co=co+1;
					end
						
				end	
		
			
			end
			
			if sliceplot
				magsl{end+1}='Polygon';
			end	
			
				
				
			%rewired the output to a variable, actually only the last entry is needed at the moment			
			if (coast && border)
				%legend(inHandles,magsl{:},'Coastline','Border','Location','NorthEast');
				legendentry={magsl{:},'Coastline'};
			elseif (coast && ~border)
				%legend(inHandles,magsl{:},'Coastline','Location','NorthEast');	
				legendentry={magsl{:},'Border','Coastline'};
			elseif (~coast && border)
				%legend(inHandles,magsl{:},'Border','Location','NorthEast');
				legendentry={magsl{:},'Border'};
			else
				%legend(inHandles,magsl{:},'Location','NorthEast');
				legendentry=magsl{:};
			end	
			
			%set to invisible by default
			%legend(inHandles,'hide');
			
	end
	
	
		

end


function BuildCompareLegend(inHandles,names,coast,border)
		%builds a special version of the legend for the compare plot 
		%(may be included in the normal legendbuilder later)
			
			
			magsl{1} =  [names{1} 'unique'];
			magsl{2} = [names{2} 'unique'];
			magsl{3} = 'identical';
			
		
		
			if (coast && border)
				legend(inHandles,magsl{:},'Coastline','Border','Location','NorthEast');
				
			elseif (coast && ~border)
				legend(inHandles,magsl{:},'Coastline','Location','NorthEast');	
			
			elseif (~coast && border)
				legend(inHandles,magsl{:},'Border','Location','NorthEast');
				
			else
				legend(inHandles,magsl{:},'Location','NorthEast');
			end	
			
			%set to invisible by default
			legend(inHandles,'hide');

		
	
	end
	

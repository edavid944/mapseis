 function h = PlotMagHist(plotAxis,evMags)
% PlotMagHist : Plot histogram of event magnitudes

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if nargin<2
    plotAxis=gca;
end

h = [];

if ~isempty(evMags)
		%Just to be sure in cases where the values are have higher precision
		%than 0.1
		minMag=floor(min(evMags)*10)/10;	
		maxMag=ceil(max(evMags)*10)/10;
		shiftfact=0.5;
		
		hist(plotAxis,evMags,minMag-shiftfact:0.1:maxMag+shiftfact);
		h = findobj(plotAxis,'Type','patch');
		hXLabel = xlabel(plotAxis,'Magnitude');
		hYLabel = ylabel(plotAxis,'No. of Events');
		hTitle = title(plotAxis,' ');
		
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 12          );
		set( hTitle                          , 'FontSize'   , 12          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'off'     , ...
		  'TickDir'     , 'out'     , ...
		  'Xlim'        , [min(evMags)-0.1 max(evMags)+0.1 ] , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'off'      , ...
		  'XGrid'       , 'off'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1         );
else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end

end
function handle = drawBox(plotAxis,geomode,corners)

	%disp('Draw the box')

	
	
	latlim = get(plotAxis,'Ylim');
	
	if geomode
		xmod=1/cos(pi/180*mean(latlim));
	else
		xmod=1;
	end
	
	%disp(xmod)
	handle(1) = plot( xmod*corners(:,1), corners(:,2));
	handle(2) = patch( xmod*corners(:,1), corners(:,2),...
			ones(numel(corners(:,2)),1),'FaceColor','none');
	
	set(handle(1), 'LineWidth',1.5,'Color','r');
	
end

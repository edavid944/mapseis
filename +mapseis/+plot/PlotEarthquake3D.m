function [handle legendentry] = PlotEarthquake3D(plotAxis,ResStruct)
	%Modernized simpltified version of PlotRegion, containing only the earthquake
	%plot function
	
	% ------------
	%| 3D version |
	% ------------
	
	
	%Data:			This can either be a matrix with four coloumb (lon, lat, depth, mag) or 
	%			a datastore object.
	%PlotMode:		Selects the plot mode (style) of the earthquakeplot, 'old' for the style like
	%			original MapSeis plot function and 'new' for the plot based on the scatter plot
	%			which allows to plot an additional data Parameter.
	%PlotQuality:		Only needed in case of the PlotMode: 'old', selects the quality like in the
	%			original code
	%SelectionSwitch:	This can be either 'selected' or 'unselected', default is (left empty) is 'selected'
	%			'selected' will draw only the events marked with ResStruct.SelectedEvents in the Style 
	%			of the normal selected events. 'unselected' will draw all events NOT marked with 
	%			ResStruct.SelectedEvents, the plotting style will be change, different colors and 
	%			in some case different Symbols. 
	%SelectedEvents:	This is an logical vector for the Selection switch, practical for plotting 
	%			a selected region and an unselected part (PlotEarthquake has to be called twice
	%			for this). If left empty every event in the data will be selected (set to true).
	%AdditonalData:		This feature can only be used in the PlotMode 'new'. The applied additional data will
	%			plotted colorcode with the points. The entry can either an vector with the length data
	%			or a string with the name of the data entry in the datastore. If no additional dat is wanted 
	%			The field can just be left empty (or be missing).
	%AddDataName:		Name of the additional data (needed for legend and similar stuff)
	%MarkedEQ:		This feature allows to mark large earthquake with a star and a magnitude label if wanted. 
	%			left empty (or missing) or set to 'off' the feature is turned off. Set to 'max', the function
	%			will search the event (or events) with the maximum (Magnitude) and mark them. If a number is used 
	%			instead, the function will mark all events greater-equal than this number.
	%			The events will only be searched and marked in the selected part of the catalog.
	%CustomMarkerRange:	Can be used to change the normal [1 8] Range of the Magnitude to any range wanted
	%			the default would be [1 8 8],means [min max steps];
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%			will be set to 'Longitude', if the field is missing, nothing will be
	%			done	
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%			will be set to 'Latitude', if the field is missing, nothing will be
	%			done			
	%Y_Axis_Label:		The label under the Z-Axis, if empty (not specified) it 
	%			will be set to 'Depth', if the field is missing, nothing will be
	%			done			
	%MarkerSize		different sizes of markers 'normal', 'small' and 'large' (works only with the 'new' setting).  
	%MarkerStylePreset: 	This allows to change the Style of the Marker (if plotted) with simple Presets 
	%			if not not specified the default style ('normal') will be used. The presets 
	%			are: 'normal','plus','circle','cross','diamond', and the two automatic settings 
	%			which try use different MarkerStyles every plot: 'automatic'
	%Colors:		This defines either the ColorMap or the Color if only 3 coloumbs are defined
	%			Option for the ColorMap are: 'jet', 'hsv', 'hot', 'gray', 'cool', 'bone', 'autumn', 'summer'
	%			In case of a single colorthe presets are: 'blue','red','green','black','cyan','magenta' and 
	%			the automatic settings which try use different Color every plot.
	%			default for the ColorMap is 'jet'and for a single Color it is 'blue'		
	%CustomColors:		This allows to specify the ColorMap properties freely, CAUTION: This field can not
	%			be left empty, to not use this option, the field CustomColorMap should be missing in
	%			ResStruct
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%			the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%Z_Axis_Limit: 		Same for the Z-Axis
	%C_Axis_Limit:		Limits the Range of the Colorbar, syntax, the same as X_Axis_Limit
	%DepthReverse:		If set to true, the z-axis will be reversed, usefull for positives depths. 
	%FastMode:		Works in the old method an bypasses everything and goes directly to a normal 3d plot
	%ColorToggle:		Set to true will show the ColorBar
	%LegendEntry:		This will be sent to legendentry, if not specified, 'Earthquakes' will be used instead
	%SizeLegend:		This will draw a small text box with the explanations about the size and the colorbar  
	
	%Example how to use in case of a marked polygon area:
	% get a vector with the selected from the filter
	% plot the selected earthquake (plotEarthquake with the option 'selected')
	% plot the unselected earthquakes (plotEarthquake with the option 'unselected')
	% plot the polygon with the PlotPolygon function
	
	%Comments:
	%---------
	% The scatter plot, plots every point as an object of the type 'patch'
	% this means the scatter plot can take quite while to plot all earthquake in 
	% case of a large earthquake catalogs.
	% The "classic" mapseis catalog plot can maybe improved to use also a fourth parameter as color. The resulting
	% function may would have a bit a better performance than the scatter plot, this feature will be implemented 
	% if the scatter plot is actually used and there is a performance problem.
	
	%Further improvement ideas:
	%--------------------------
	% The textbox describing the size parameter and the value of the colorbar are a bit pathetic, and have to be changed
	% in one or the other way. 
	% Ticks to mark the colorbar are also missing.
	% Only one colorscheme per figure can be used for the colormap, this may not be enough for some cases (all subplot
	% have the same colorscheme). If the rgb code of the value would be code by hand, it my be possible to overcome this
	% limitation. 
	   
	
	  
	
	
	import mapseis.plot.*;
	import mapseis.projector.*;
	
	
	if isempty(plotAxis)
		plotAxis=gca;
	end	
	
	
	%ColorDefinition (for the unmarked part)
	UnMarkedRed=[1 0.7 0.7];
	UnMarkedGreen=[0.7 1 0.7];
	UnMarkedBlue=[0.7 0.7 1];
	UnMarkedBlack=[0.7 0.7 0.7];
	Grey=[0.7 0.7 0.7];
	UnMarkedCyan=[0.7 1 1];
	UnMarkedMagenta=[1 0.7 1];
	
	%Build some internal data settings
	wheelMarker={'.','+','o','x','diamond'};
	wheelMarker_nodot={'+','o','x','diamond'};
	wheelColor={'b','r','g','k','c','m'};
	wheelColorUnMarked={UnMarkedBlue,UnMarkedRed,UnMarkedGreen,...
						UnMarkedBlack,UnMarkedCyan,UnMarkedMagenta};

	
	MarkerRange=[1 8];					
	MarkerNr=8;					
	
	FastMode=false;	
	if isfield(ResStruct,'FastMode')
		if ~isempty(ResStruct.FastMode)
			FastMode=ResStruct.FastMode;
			%disp('do it fast baby')
		end
	end
	
	
	%check for CustomMarkerRange
	if isfield(ResStruct,'CustomMarkerRange')
		if ~isempty(ResStruct.CustomMarkerRange)
			MarkerRange=ResStruct.CustomMarkerRange(1:2);					
			MarkerNr=ResStruct.CustomMarkerRange(3);		
		end
	end	
	
	%Check if a Selection of the events is wanted
	if isobject(ResStruct.Data)
		if isfield(ResStruct,'SelectedEvents')
				if ~isempty(ResStruct.SelectedEvents)
					LogicSelect=ResStruct.SelectedEvents;
				else
					LogicSelect=true(ResStruct.Data.getRowCount,1);
				end
		else
			LogicSelect=true(ResStruct.Data.getRowCount,1);
		end
	
	
	else %it is a matrix
		if isfield(ResStruct,'SelectedEvents')
				if ~isempty(ResStruct.SelectedEvents)
					LogicSelect=ResStruct.SelectedEvents;
				else
					LogicSelect=true(numel(ResStruct.Data(:,1)),1);
				end
		else
			LogicSelect=true(numel(ResStruct.Data(:,1)),1);
		end
	end
	
	
						
	%check if Data is datastore or a matrix (it is only checked if it is an object) 
	if isobject(ResStruct.Data)
		
	
		try
			[selectedLonLat,unselectedLonLat] = getLocations(ResStruct.Data,LogicSelect);
			[selectedMag,unselectedMag] = getMagnitudes(ResStruct.Data,LogicSelect);				
			[selectedDepth,unselectedDepth] = getDepths(ResStruct.Data,LogicSelect);
		
		catch
			selectedLonLat=[];
			unselectedLonLat=[];
			selectedMag=[];
			unselectedMag=[];
			selectedDepth=[];
			unselectedDepth=[];
		end
	
	
	
	else %data is a matrix
		selectedLonLat=[ResStruct.Data(LogicSelect,1),ResStruct.Data(LogicSelect,2)];
		unselectedLonLat=[ResStruct.Data(~LogicSelect,1),ResStruct.Data(~LogicSelect,2)];
		selectedDepth=ResStruct.Data(LogicSelect,3);
		unselectedDepth=ResStruct.Data(~LogicSelect,3);
		selectedMag=ResStruct.Data(LogicSelect,4);
		unselectedMag=ResStruct.Data(~LogicSelect,4);
	
	end
	
	
	%check for additional data
	if isfield(ResStruct,'AdditionalData')
		if ~isempty(ResStruct.AdditionalData)
			if strcmp(ResStruct.AdditionalData,'off')
				SeladdData=[];
				UnSeladdData=[];
				
			elseif isstr(ResStruct.AdditionalData) & ~strcmp(ResStruct.AdditionalData,'off') 
				SeladdData = ResStruct.Data.getFields(ResStruct.AdditionalData,LogicSelect);
				SeladdData=SeladdData.(ResStruct.AdditionalData);
				UnSeladdData = ResStruct.Data.getFields(ResStruct.AdditionalData,~LogicSelect);
				UnSeladdData=UnSeladdData.(ResStruct.AdditionalData);
			elseif isnumeric(ResStruct.AdditionalData)
				SeladdData = ResStruct.AdditionalData(LogicSelect); 
				UnSeladdData = ResStruct.AdditionalData(~LogicSelect);
				
			end
		
		else
				SeladdData=[];
				UnSeladdData=[];
				
		
		end
	
	
	else
		SeladdData=[];
		UnSeladdData=[];
				
	
	end


	
	%Decide which plotting is wanted and plot
	if ~isfield(ResStruct,'PlotMode')
		PlotMode='new';
	elseif isempty(ResStruct.PlotMode)
		PlotMode='new';
	else
		PlotMode=ResStruct.PlotMode;
	end
	
	
	%set the selectionswitch
	if ~isfield(ResStruct,'SelectionSwitch')
		SelectionSwitch='selected';
	elseif isempty(ResStruct.SelectionSwitch)
		SelectionSwitch='selected';
	else
		SelectionSwitch=ResStruct.SelectionSwitch;
	
	end
	
	
	
	
	switch PlotMode
		
		%classic view
		%------------
		case 'old'
			%Check if PlotQuality is specified, else set to high
			
			if ~FastMode
			
				if ~isfield(ResStruct,'PlotQuality')
					PlotQuality='hi';
				elseif isempty(ResStruct.PlotQuality)
					PlotQuality='hi';
				else
					PlotQuality=ResStruct.PlotQuality;
				end
				
				%decide events are needed (selected or unselected)
				if strcmp(SelectionSwitch,'selected')
					%mags.selected=selectedMag;
					PlotStructur=struct('inRegion',[selectedLonLat,selectedDepth],...
							'Quality',PlotQuality,...
							'inout','in');
					[handle legendentry] = PlotRegionLight3D(plotAxis,PlotStructur,selectedMag);
					legendentry = StandardEqLegendBuilder(PlotStructur,selectedMag);
					
					
				elseif strcmp(SelectionSwitch,'unselected')
					PlotStructur=struct('inRegion',[unselectedLonLat,unselectedDepth],...
							'Quality',PlotQuality,...
							'inout','out');
					[handle legendentry] = PlotRegionLight3D(plotAxis,PlotStructur,unselectedMag);   
					legendentry = StandardEqLegendBuilder(PlotStructur,unselectedMag);
					
				end
			
			else
				%disp('use fastmode')
				%direct mode maybe a bit faster
				
				
				if strcmp(SelectionSwitch,'selected')
					PlotStructur = struct(	'Data',[selectedLonLat,selectedDepth],...
								'X_Axis_Label','Longitude',...
								'Y_Axis_Label','Latitude',...
								'Z_Axis_Label','Depth',...
								'Line_or_Point','points',...
								'MarkerStylePreset','normal',...
								'LineStylePreset','normal',...			
								'Colors','blue',...
								'X_Axis_Limit',ResStruct.X_Axis_Limit,...
								'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
								'Z_Axis_Limit',ResStruct.Z_Axis_Limit,...
								'LegendText','Earthquakes');
				else
					PlotStructur = struct(	'Data',[unselectedLonLat,unselectedDepth],...
								'X_Axis_Label','Longitude',...
								'Y_Axis_Label','Latitude',...
								'Z_Axis_Label','Depth',...
								'Line_or_Point','points',...
								'MarkerStylePreset','normal',...
								'LineStylePreset','normal',...			
								'Colors','blue',...
								'X_Axis_Limit',ResStruct.X_Axis_Limit,...
								'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
								'Z_Axis_Limit',ResStruct.Z_Axis_Limit,...
								'LegendText','Earthquakes');
				
				end
				
				[handle legendentry] = PlotGeneric3D(plotAxis,PlotStructur)
				
				
				
			end
			
			if strcmp(SelectionSwitch,'selected')
					switch ResStruct.Colors
						case 'blue'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor','b')
							end	
							
						case 'red'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor','r')
							end
							
						case 'green'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor','g')
							end
							
						case 'black'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor','k')
							end	
							
						case 'cyan'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor','c')
							end	
							
						case 'magenta'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor','m')
							end	
							
							
						case 'automatic'
							%experimental but should work
							childish=get(plotAxis,'Children');
							existPlots=numel(findobj(childish,'Type','hggroup'));
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',wheelColor{mod(existPlots,6)+1})
							end	
							
									
					end
				
			else %the unselected
					switch ResStruct.Colors
						case 'blue'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',UnMarkedBlue)
								set(handle(i),'MarkerFaceColor','none')
							end	
							
						case 'red'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',UnMarkedRed)
								set(handle(i),'MarkerFaceColor','none')
							end
							
						case 'green'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',UnMarkedGreen)
								set(handle(i),'MarkerFaceColor','none')
							end
							
						case 'black'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',UnMarkedBlack)
								set(handle(i),'MarkerFaceColor','none')
							end
							
						case 'cyan'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',UnMarkedCyan)
								set(handle(i),'MarkerFaceColor','none')
							end
							
						case 'magenta'
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',UnMarkedMagenta)
								set(handle(i),'MarkerFaceColor','none')
							end
							
						case 'automatic'
							%experimental but should work
							childish=get(plotAxis,'Children');
							existPlots=numel(findobj(childish,'Type','hggroup'));
							%has to be done different here, because normal there is already
							%a selected part (to have the same color)
							wantColor=mod(existPlots,6);
							
							if wantColor==0
								wantColor=1;
							end	
							
							for i=1:numel(handle)
								set(handle(i),'MarkerEdgeColor',wheelColorUnMarked{wantColor})
								set(handle(i),'MarkerFaceColor','none')
							end
							
									
					end

			
			end
			
			
			
			%set limits if needed (only old mode, in the scatter, the scatter modul does it)
			if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
				xlim(plotAxis,'manual');
				xlim(plotAxis,ResStruct.X_Axis_Limit);	
			end
		
			if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
				ylim(plotAxis,'manual');
				ylim(plotAxis,ResStruct.Y_Axis_Limit);	
			end
			
			if ~isempty(ResStruct.Z_Axis_Limit) | strcmp(ResStruct.Z_Axis_Limit,'auto')
				zlim(plotAxis,'manual');
				zlim(plotAxis,ResStruct.Z_Axis_Limit);	
			end
			
			
		%scatter plot
		%------------
		case 'new'
			MarkedEQ = false;
			%check if marked events are wanted 
			%'off' not existing and empty will set the parameter to false for every event
			if ~isfield(ResStruct,'MarkedEQ') 
				MarkedEQ=false(numel(selectedMag),1);
			
			elseif isempty(ResStruct.MarkedEQ) | strcmp(ResStruct.MarkedEQ,'off')
				MarkedEQ=false(numel(selectedMag),1);
			
			elseif ~isempty(ResStruct.MarkedEQ) & strcmp(ResStruct.MarkedEQ,'max')
				%mark only the earthquake with the highest magnitude
				
				MarkedEQ=false(numel(selectedMag),1);
				[tempval,maxpos] = max(selectedMag);
				MarkedEQ(maxpos)=true;
			
			elseif ~isempty(ResStruct.MarkedEQ) & isnumeric(ResStruct.MarkedEQ)
				%mark all earthquake bigger than MarkedEQ
	 			MarkedEQ = selectedMag>=ResStruct.MarkedEQ;
				
			end
	
			
			
			
			%determine  which marker size is wanted
			%set the selectionswitch
			if ~isfield(ResStruct,'MarkerSize')
				WhichSize='normal';
			elseif isempty(ResStruct.MarkerSize)
				WhichSize='normal';
			else
				WhichSize=ResStruct.MarkerSize;
			
			end
			
			%values for the size may have to be ajusted
			switch WhichSize
				case 'normal'
					SizeLimitNorm=[10 100];
					SizeLimitMarked=[50 100];
					SizeLimitUnSel=[1 50];
				case 'small'
					SizeLimitNorm=[5 50];
					SizeLimitMarked=[1 100];
					SizeLimitUnSel=[1 25];
				case 'large'
					SizeLimitNorm=[50 150];
					SizeLimitMarked=[100 250];
					SizeLimitUnSel=[10 100];
			end		
	
				
			
			if isempty(SeladdData) & isempty(UnSeladdData) & strcmp(SelectionSwitch,'selected')
				%no additional data
				
				%build structur for scatter plot (excluding the marked eqs
				PlotStructur = struct(	'Data',[selectedLonLat(~MarkedEQ,:),selectedDepth(~MarkedEQ,:),selectedMag(~MarkedEQ)],...
										'FilledMarkers',true,...
										'MarkerStylePreset','circle',...
										'MarkerSizeLimit',SizeLimitNorm,...
										'MarkerRange',MarkerRange,...
										'SizeStep',MarkerNr,...
										'Colors','black',...
										'C_Axis_Limit',ResStruct.C_Axis_Limit,...
										'X_Axis_Limit',ResStruct.X_Axis_Limit,...
										'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
										'Z_Axis_Limit',ResStruct.Z_Axis_Limit);
				
				[handle legendentry] = PlotScatter3D(plotAxis,PlotStructur);
				set(handle,'MarkerEdgeColor','k');
										
				%now plot the events which are marked (if existing
				if any(MarkedEQ)						
					PlotStructur = struct(	'Data',[selectedLonLat(MarkedEQ,:),selectedDepth(MarkedEQ,:),selectedMag(MarkedEQ)],...
											'FilledMarkers',true,...
											'MarkerStylePreset','circle',...
											'CustomMarker','p',...
											'MarkerSizeLimit',SizeLimitMarked,...
											'MarkerRange',MarkerRange,...
											'SizeStep',MarkerNr,...
											'Colors','red',...
											'C_Axis_Limit',ResStruct.C_Axis_Limit,...
											'X_Axis_Limit',ResStruct.X_Axis_Limit,...
											'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
											'Z_Axis_Limit',ResStruct.Z_Axis_Limit);
						
					[handle_marked legendentry_marked] = PlotScatter3D(plotAxis,PlotStructur);
					set(handle_marked,'MarkerEdgeColor','r');
				end
				
			
			
			
			elseif isempty(SeladdData) & isempty(UnSeladdData) & strcmp(SelectionSwitch,'unselected')		
				%no additional data (unselected)
				
				%build structur for scatter plot (excluding the marked eqs
				PlotStructur = struct(	'Data',[unselectedLonLat,unselectedDepth,unselectedMag],...
										'FilledMarkers',true,...
										'MarkerStylePreset','circle',...
										'MarkerSizeLimit',SizeLimitNorm,...
										'MarkerRange',MarkerRange,...
										'SizeStep',8,...
										'Colors',[],...
										'CustomColors',UnMarkedBlack,...
										'C_Axis_Limit',ResStruct.C_Axis_Limit,...
										'X_Axis_Limit',ResStruct.X_Axis_Limit,...
										'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
										'Z_Axis_Limit',ResStruct.Z_Axis_Limit);
				[handle legendentry] = PlotScatter3D(plotAxis,PlotStructur);
				set(handle,'MarkerEdgeColor','k');						
								
			
			
			
			
			elseif (~isempty(SeladdData) | ~isempty(UnSeladdData)) & strcmp(SelectionSwitch,'selected')
				%Additional Data included
				
				%build structur for scatter plot (excluding the marked eqs
				PlotStructur = struct(	'Data',[selectedLonLat(~MarkedEQ,:),selectedDepth(~MarkedEQ,:),selectedMag(~MarkedEQ),SeladdData(~MarkedEQ)],...
										'FilledMarkers',true,...
										'MarkerStylePreset','circle',...
										'MarkerSizeLimit',SizeLimitNorm,...
										'MarkerRange',MarkerRange,...
										'SizeStep',MarkerNr,...
										'Colors','jet',...
										'X_Axis_Limit',ResStruct.X_Axis_Limit,...
										'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
										'Z_Axis_Limit',ResStruct.Z_Axis_Limit,...
										'C_Axis_Limit',ResStruct.C_Axis_Limit,...
										'ColorToggle',ResStruct.ColorToggle);
														
				[handle legendentry] = PlotScatter3D(plotAxis,PlotStructur);
				set(handle,'MarkerEdgeColor','k');
				
				%now plot the events which are marked (if existing
				if any(MarkedEQ)						
					PlotStructur = struct(	'Data',[selectedLonLat(MarkedEQ,:),selectedDepth(MarkedEQ,:),selectedMag(MarkedEQ),SeladdData(MarkedEQ)],...
											'FilledMarkers',true,...
											'MarkerStylePreset',[],...
											'CustomMarker','p',...
											'MarkerSizeLimit',SizeLimitMarked,...
											'MarkerRange',MarkerRange,...
											'SizeStep',MarkerNr,...
											'Colors','jet',...
											'X_Axis_Limit',ResStruct.X_Axis_Limit,...
											'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
											'Z_Axis_Limit',ResStruct.Z_Axis_Limit,...
											'C_Axis_Limit',ResStruct.C_Axis_Limit,...
											'ColorToggle',ResStruct.ColorToggle);

						
					[handle_marked legendentry_marked] = PlotScatter3D(plotAxis,PlotStructur);
					set(handle_marked,'MarkerEdgeColor','r');
				end

			
			
				
			elseif (~isempty(SeladdData) | ~isempty(UnSeladdData)) & strcmp(SelectionSwitch,'unselected')
				%Additional Data included, unselected data wanted.
				%Colormap can only be choosen per figure and not per data, the Symbol will thus be drawn smaller
				%COMMENT: Maybe it is possible to build a custom colorisation function to allow different colormaps
				
				%build structur for scatter plot (excluding the marked eqs
				PlotStructur = struct(	'Data',[unselectedLonLat,unselectedDepth,unselectedMag,UnSeladdData],...
										'FilledMarkers',false,...
										'MarkerStylePreset','normal',...
										'MarkerSizeLimit',SizeLimitUnSel,...
										'MarkerRange',MarkerRange,...
										'SizeStep',MarkerNr,...
										'Colors','jet',...
										'X_Axis_Limit',ResStruct.X_Axis_Limit,...
										'Y_Axis_Limit',ResStruct.Y_Axis_Limit,...
										'Z_Axis_Limit',ResStruct.Z_Axis_Limit,...
										'C_Axis_Limit',ResStruct.C_Axis_Limit,...
										'ColorToggle',ResStruct.ColorToggle);
				
				[handle legendentry] = PlotScatter3D(plotAxis,PlotStructur);	
				set(handle,'MarkerEdgeColor','k');
			
			
			end
			
			
			%Write the labels for the marked events
			if any(MarkedEQ)
				MarkLonLat = selectedLonLat(MarkedEQ,:);
				MarkDepth = selectedDepth(MarkedEQ);
				MarkMag = selectedMag(MarkedEQ)
				
				%build raw entry for PlotText
				TextStruct=struct(	'Position',[],...
									'CoordType','real',...
									'String',[],...
									'TextType','normal',...
									'TextStyle','small');
				
				for i=1:numel(MarkMag)
					TextStruct.Position = [MarkLonLat(i,1),MarkLonLat(i,2),MarkDepth(i)];
					TextStruct.String = ['M ', num2str(MarkMag(i))];
					[texthandle(i) legtemp] = PlotText3D(plotAxis,TextStruct);
				
				end
			end				
			
			
	end
	
	
	%Change symbols if wanted ( works for both type of plots, but may deformes a bit the format of the 'old' plot
	%if the plot is unselected and uses the additional data parameter the, symbols should not be changed
	donotcrit=isempty(SeladdData) & isempty(UnSeladdData) & strcmp(SelectionSwitch,'unselected');
	
	if ~isempty(ResStruct.MarkerStylePreset) & ~donotcrit &	~strcmp(PlotMode,'new')		
			switch ResStruct.MarkerStylePreset
				case 'normal'
					set(handle,'Marker','.','MarkerSize',5)
				case 'plus'
					set(handle,'Marker','+','MarkerSize',5)
				case 'circle'
					set(handle,'Marker','o','MarkerSize',5)
				case 'cross'
					set(handle,'Marker','x','MarkerSize',5)
				case 'diamond'
					set(handle,'Marker','diamond','MarkerSize',5)
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','hggroup'));
					set(handle,'Marker',wheelMarker{mod(existPlots,5)+1},'MarkerSize',5);
							
			end
	end


	%Change Color if wanted
	%The color change can be done for both plotTypes
	if ~isempty(ResStruct.Colors) &	strcmp(PlotMode,'new')	
			
			if (~isempty(SeladdData) | ~isempty(UnSeladdData))
				if isstr(ResStruct.Colors)
					switch ResStruct.Colors
						case 'jet'
							colormap(plotAxis,jet);
						case 'hsv'
							colormap(plotAxis,hsv);
						case 'hot'
							colormap(plotAxis,hot);
						case 'cool'
							colormap(plotAxis,cool);
						case 'gray'
							colormap(plotAxis,gray);
						case 'bone'
							colormap(plotAxis,bone);
						case 'summer'
							colormap(plotAxis,summer);
						case 'autumn'
							colormap(plotAxis,autumn);
									
					end
				else
					colormap(plotAxis,ResStruct.Colors);
				end	
				
			else
				if ~donotcrit %see before 
					switch ResStruct.Colors
						case 'blue'
							set(handle,'MarkerFaceColor','b')
						case 'red'
							set(handle,'MarkerFaceColor','r')
						case 'green'
							set(handle,'MarkerFaceColor','g')
						case 'black'
							set(handle,'MarkerFaceColor','k')
						case 'cyan'
							set(handle,'MarkerFaceColor','c')
						case 'magenta'
							set(handle,'MarkerFaceColor','m')
						case 'automatic'
							%experimental but should work
							childish=get(plotAxis,'Children');
							existPlots=numel(findobj(childish,'Type','hggroup'));
							set(handle,'MarkerFaceColor',wheelColor{mod(existPlots,6)+1});
									
					end
				
				else %the unselected
					switch ResStruct.Colors
						case 'blue'
							set(handle,'MarkerFaceColor',UnMarkedBlue)
						case 'red'
							set(handle,'MarkerFaceColor',UnMarkedRed)
						case 'green'
							set(handle,'MarkerFaceColor',UnMarkedGreen)
						case 'black'
							set(handle,'MarkerFaceColor',UnMarkedBlack)
						case 'cyan'
							set(handle,'MarkerFaceColor',UnMarkedCyan)
						case 'magenta'
							set(handle,'MarkerFaceColor',UnMarkedMagenta)
						case 'automatic'
							%experimental but should work
							childish=get(plotAxis,'Children');
							existPlots=numel(findobj(childish,'Type','hggroup'));
							%has to be done different here, because normal there is already
							%a selected part (to have the same color)
							wantColor=mod(existPlots,6);
							
							if wantColor==0
								wantColor=1;
							end	
							
							set(handle,'MarkerFaceColor',wheelColorUnMarked{wantColor});
									
					end

			
				end
					
			end	
	end

	
	if isfield(ResStruct,'ColorToggle');
		if ~isempty(ResStruct.ColorToggle) & ResStruct.ColorToggle
				colorbar;
		end
	end	
	
	%the labels: if not set, set to 'X' and 'Y'
	if isfield(ResStruct,'X_Axis_Label')
		X_axis_name=ResStruct.X_Axis_Label;
		
		if isempty(X_axis_name)
			X_axis_name='Longitude';
		end
		
		hXLabel = xlabel(plotAxis,X_axis_name);
		set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
	end
		
		
	if isfield(ResStruct,'Y_Axis_Label')
		Y_axis_name=ResStruct.Y_Axis_Label; 	
		
		if isempty(Y_axis_name)
			Y_axis_name='Latitude';
		end
		
		hYLabel = ylabel(plotAxis,Y_axis_name);
		set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end

	if isfield(ResStruct,'Z_Axis_Label')
		Z_axis_name=ResStruct.Z_Axis_Label; 	
		
		if isempty(Z_axis_name)
			Z_axis_name='Depth';
		end
		
		hZLabel = zlabel(plotAxis,Z_axis_name);
		set(hZLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
	end	

	
		%build legend (if not empty it will just copy the entry from ResStruct)
	if isfield(ResStruct,'LegendText');	
		if ~isempty(ResStruct.LegendText)
			legendentry=ResStruct.LegendText;
		else	
			if ~isempty(legendentry)
				legendentry='Data';
			end	
		end
	else
	%legendentry=[];
					
	end
	
	
	if isfield(ResStruct,'DepthReverse')
		if ~isempty(ResStruct.DepthReverse)
			if ResStruct.DepthReverse
				set(plotAxis,'ZDir','reverse');
			
			end
		end
		
		
	end	
	
	
	%build 'Legend' for the size and the colorbar (colorbar setting may be separated later)
	if isfield(ResStruct,'SizeLegend')
		if ResStruct.SizeLegend;
			%build raw entry for PlotText
			TextStruct=struct(	'Position',[0.05 0.05],...
								'CoordType','normalized',...
								'String',[],...
								'TextType','textbox',...
								'TextStyle','normal');
	
		
			if ~donotcrit
				TextStruct.String={'Markersize: Magnitude',['Color: ',ResStruct.AddDataName]};
			else
				TextStruct.String={'Markersize: Magnitude'};
			end	
			
			
			[boxhandle legtemp] = PlotText(plotAxis,TextStruct);
		end
			
	end
	
		

end

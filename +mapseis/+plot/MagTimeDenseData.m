function [DenseData,MagTicks,TimeTicks,MagCDF,TimeCDF] = MagTimeDenseData(RefData,ActualData,StepSize,minVal,maxVal,MagVector,TimeVector)
	
	%This function uses the EqualDenseData to produce equal density data in
	%time and magnitude.	

	import mapseis.plot.*;
	
	
	%MagVector and TimeVector which should be vector giving the axis ticks wanted
	%RefData should be a structure containing the fields 'Mag' and 'Time', any
	%of them can be left empty in this case data itself is used as reference
	%The same is true for StepSize,minVal and maxVal
	%ActualData should be a double vector (Time,Mag)
	
                            
	%unpack data
	MagData=ActualData(:,2);
	TimeData=ActualData(:,1);
	
	RefMag=ActualData(:,2);
	RefTime=ActualData(:,1);
	
	if ~isempty(RefData)
		if isfield(RefData,'Time')
			if ~isempty(RefData.Time)
				RefTime=RefData.Time;
				disp('Found Time Reference');
			end	
		end
		
		if isfield(RefData,'Mag')
			if ~isempty(RefData.Mag)
				RefMag=RefData.Mag;
				disp('Found Magnitude Reference');
			end
		end
		
	end
	
	MagStep=StepSize.Mag;
	TimeStep=StepSize.Time;
	
	MinMag=[];
	MaxMag=[];
	MinTime=[];
	MaxTime=[];
	
	if ~isempty(minVal)
		if isfield(minVal,'Time')
			if ~isempty(minVal.Time)
				MinTime=minVal.Time;
			end	
		end
		
		if isfield(minVal,'Mag')
			if ~isempty(minVal.Mag)
				MinMag=minVal.Mag;
			end
		end
		
	end
	
	if ~isempty(maxVal)
		if isfield(maxVal,'Time')
			if ~isempty(maxVal.Time)
				MaxTime=maxVal.Time;
			end	
		end
		
		if isfield(maxVal,'Mag')
			if ~isempty(maxVal.Mag)
				MaxMag=maxVal.Mag;
			end
		end
		
	end
	
	%Now calculate
	
	%Time first
	[DenseTime,TrefCDF,TcountCDF] = EqualDenseData(RefTime,TimeData,TimeStep,MinTime,MaxTime);
	
	%Magnitude
	[DenseMag,MrefCDF,McountCDF] = EqualDenseData(RefMag,MagData,MagStep,MinMag,MaxMag);
	
	%Time Ticks if existing
	if ~isempty(TimeVector)
		[TTick,refCDF,countCDF] = EqualDenseData(RefTime,TimeVector,TimeStep,MinTime,MaxTime);
	end
	
	%Mag Ticks if existing
	if ~isempty(MagVector)
		[MTick,refCDF,countCDF] = EqualDenseData(RefMag,MagVector,MagStep,MinMag,MaxMag);
	end	
	
	%prepare output
	DenseData=[DenseTime,DenseMag];
	TimeTicks=[TimeVector,TTick];
	MagTicks=[MagVector,MTick];
	MagCDF=[MrefCDF,McountCDF];
	TimeCDF=[TrefCDF,TcountCDF];
	
end

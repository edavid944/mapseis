 function h = PlotMagHistDiff(plotAxis,evMags)
% PlotMagHist : Plot histogram of event magnitudes

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

if nargin<2
    plotAxis=gca;
end

h = [];

if ~isempty(evMags)
		%get minimal maximal depth
		%magmin = floor(min([evMags(:,1) ; evMags(:,2)]))    
   		%magmax = ceil(max([evMags(:,1) ; evMags(:,2)])) 
		evMagDiff=evMags(:,1)-evMags(:,2);
		
		magmin = floor(min(evMagDiff)) ;   
   		magmax = ceil(max(evMagDiff)) ;

		
		binvec = magmin:0.1:magmax;

		hist(evMagDiff,binvec);
		
		%h2 = hist(evMags(:,2),binvec);
		%h=area(binvec,abs(h1-h2));
		%xlim([magmin,magmax]);
		%set(h,'xtick',magmin:1:magmax);
		
		%hist(plotAxis,evMags,min(evMags)-0.05:0.1:max(evMags)+0.05);
		h = findobj(plotAxis,'Type','patch');
		hXLabel = xlabel(plotAxis,'Magnitude Difference (A-B)');
		hYLabel = ylabel(plotAxis,'No. of Events');
		hTitle = title(plotAxis,' ');
		
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 14,'FontWeight' , 'bold');
		set( hTitle                          , 'FontSize'   , 16          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'off'     , ...
		  'TickDir'     , 'out'     , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'off'      , ...
		  'XGrid'       , 'off'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1.5,         ...
		  'FontWeight' , 'bold',     ...
		  'FontSize'   , 14);

else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end

end
function h = PlotTime(plotAxis,evTimes)
% PlotTime : Plots the cumulative time distribution of events

% Copyright 2007-2008 The MathWorks, Inc.
% $Revision: 65 $    $Date: 2008-10-13 16:52:37 +0100 (Mon, 13 Oct 2008) $
% Author: Matt McDonnell

import mapseis.calc.CalcTimeDist;
import mapseis.util.decyear;

if nargin<2
    plotAxis=gca;
end

h=[];

if ~isempty(evTimes)
		h = plot(plotAxis,evTimes,CalcTimeDist(evTimes),'Linewidth',2,'color','r');
		hXLabel = xlabel(plotAxis,'Time');
		hYLabel = ylabel(plotAxis,'Cumulative No. of Events');
		hTitle = title(plotAxis,' ');
		%datetick(plotAxis,'x');
		
		set([hTitle, hXLabel, hYLabel ],       'FontName'   , 'AvantGarde');
		set([hXLabel, hYLabel       ]        , 'FontSize'   , 12          );
		set( hTitle                          , 'FontSize'   , 12          , ...
		                                       'FontWeight' , 'bold'      );
		
		set(plotAxis, ...
		  'Box'         , 'off'     , ...
		  'TickDir'     , 'out'     , ...
		  'TickLength'  , [.02 .02] , ...
		  'XMinorTick'  , 'on'      , ...
		  'YMinorTick'  , 'on'      , ...
		  'YGrid'       , 'of'      , ...
		  'XGrid'       , 'of'      , ...
		  'XColor'      , [.3 .3 .3], ...
		  'YColor'      , [.3 .3 .3], ...
		  'LineWidth'   , 1         );
else
		cla(plotAxis);
		hTitle = title(plotAxis, 'No Data Selected');
		set( hTitle                          , 'FontSize'   , 16          , ...
			                                   'FontWeight' , 'bold'      );
end


end
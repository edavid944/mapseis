function  [handle legendentry] = PlotPolygon(plotAxis,ResStruct)
%This function plots polygons, used by the ResultGUI (Polygons of the RegionFilter) 
%possible Configs
	%Data:			This can either be a RegionFilter of Mapseis or a matrix, with 
	%				first coulomn Data(:,1) being the X coordinates (often the longitude)
	%				and the second coloumn being the Y coordinate (often the latitude)
	%				in case of a circle the Data has to be a bitdifferent (if the data is not 
	%				a regionfilter), the coloumns are similar to the normal case but, the first
	%				coordinate pair describes th bottom left corner and the second pair the upper
	%				right corner.		
	%RegionType:	This can be either 'in','out','line' or 'circle', this field is only needed
	%				if the input is not a region filter. 'in' and 'out' results in a polygone, 
	%				'line' builds a line for a profile and 'circle' builds a circlular area.
	%Colors:		This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%				if not not specified the default Color ('blue') will be used. The presets 
	%				are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%				which try use different Color every plot.
	%LineStylePreset:	Only needed for "Plain" mode
	%Plain:			Not implemented jet, will draw the polygon as a simple line and not as an object.
	
	
	import mapseis.plot.*;
	
	%some needed constants
	wheelColor={'b','r','g','k','c','m'}
	boxhandle=[];
	PlainPlot=false;
	
	
	legendentry='Regionfilter';
	
	if isfield(ResStruct,'Plain')
		if ~isempty(ResStruct.Plain) 
			PlainPlot=ResStruct.Plain;	
		
		end
	end	
	
	%check if a data is a filterregion
	if isobject(ResStruct.Data)
		filterRegion = getRegion(ResStruct.Data);
        
        pRegion = filterRegion{2};
        RegRange=filterRegion{1};
		
		if ~isempty(pRegion)
                if isobject(pRegion)
                	rBdry = pRegion.getBoundary();	
                else 
                	rBdry = pRegion; %circle
                	%modify on radius
                	latlim = get(plotAxis,'Ylim');
                	rBdry(4)= rBdry(3)*cos(pi/180*mean(latlim));
                end
		end
	
	
	elseif isnumeric(ResStruct.Data)
				
		rBdry = ResStruct.Data;
		RegRange = ResStruct.RegionType;
		
	end
	
	
	if ~PlainPlot
		
		switch RegRange
			case {'in','out'}
			handle=impoly(plotAxis,[rBdry(:,1),rBdry(:,2)]);
		
			case 'line'
				handle=imline(plotAxis, [rBdry(1,1),rBdry(1,2);rBdry(4,1),rBdry(4,2)]);
				boxhandle=plot(rBdry(:,1),rBdry(:,2));
					
			case 'circle'
				handle = imellipse(plotAxis,rBdry);
				handle.setFixedAspectRatioMode(1)   			
		
		end	
		
		
		%set the color if wanted
		if isfield(ResStruct,'Colors')
			if ~isempty(ResStruct.Colors)          
				switch ResStruct.Colors
					case 'blue'
						handle.setColor('b')
					case 'red'
						handle.setColor('r')
					case 'green'
						handle.setColor('g')
					case 'black'
						handle.setColor('k')
					case 'cyan'
						handle.setColor('c')
					case 'magenta'
						handle.setColor('m')
					case 'automatic'
						%experimental but should work,
						childish=get(plotAxis,'Children');
						existPlots=numel(findobj(childish,'Type','hggroup'));
						handle.setColor(wheelColor{mod(existPlots,6)+1});
			
												
				end
			end
		end	
		
		%set the color if wanted
		if isfield(ResStruct,'Colors')
			if ~isempty(ResStruct.Colors) & ~isempty(boxhandle)
				switch ResStruct.Colors
					case 'blue'
						set(boxhandle,'Color','b');
					case 'red'
						set(boxhandle,'Color','r');
					case 'green'
						set(boxhandle,'Color','g');
					case 'black'
						set(boxhandle,'Color','k');
					case 'cyan'
						set(boxhandle,'Color','c');
					case 'magenta'
						set(boxhandle,'Color','m');
					case 'automatic'
						%experimental but should work,
						handle.setColor(wheelColor{mod(existPlots,6)+1});
			
												
				end
			end
		end	

	else
		%plot blank polys
		switch RegRange
			case {'in','out'}
				handle=plot(plotAxis,rBdry(:,1),rBdry(:,2));
		
			case 'line'
				handle=plot(plotAxis, [rBdry(1,1),rBdry(1,2)]',[rBdry(4,1),rBdry(4,2)]');
				boxhandle=plot(rBdry(:,1),rBdry(:,2));
					
			case 'circle'			
				bothhandle=drawCircle(plotAxis,true,rBdry(1:2),rBdry(3));
				handle=bothhandle(1);
		
		end	
		
		%set the color if wanted
		if isfield(ResStruct,'Colors')
			if ~isempty(ResStruct.Colors)
				switch ResStruct.Colors
					case 'blue'
						set(handle,'Color','b');
					case 'red'
						set(handle,'Color','r');
					case 'green'
						set(handle,'Color','g');
					case 'black'
						set(handle,'Color','k');
					case 'cyan'
						set(handle,'Color','c');
					case 'magenta'
						set(handle,'Color','m');
					case 'automatic'
						%experimental but should work,
						handle.setColor(wheelColor{mod(existPlots,6)+1});
			
												
				end
			end
		end	
		
		
		
		%set the color if wanted
		if isfield(ResStruct,'Colors')
			if ~isempty(ResStruct.Colors) & ~isempty(boxhandle)
				switch ResStruct.Colors
					case 'blue'
						set(boxhandle,'Color','b');
					case 'red'
						set(boxhandle,'Color','r');
					case 'green'
						set(boxhandle,'Color','g');
					case 'black'
						set(boxhandle,'Color','k');
					case 'cyan'
						set(boxhandle,'Color','c');
					case 'magenta'
						set(boxhandle,'Color','m');
					case 'automatic'
						%experimental but should work,
						handle.setColor(wheelColor{mod(existPlots,6)+1});
			
												
				end
			end
		end	
		
		
	
		%change the linestyle with a preset if needed 
		if ~isempty(ResStruct.LineStylePreset) 
			switch ResStruct.LineStylePreset
				case 'normal'
					set(handle,'LineStyle','-','LineWidth',1.5);
					if ~isempty(boxhandle)
						set(handle,'LineStyle','-','LineWidth',2);
						set(boxhandle,'LineStyle','-','LineWidth',1.5);
					end
					
				case 'dotted'
					set(handle,'LineStyle',':','LineWidth',1.5);
					if ~isempty(boxhandle)
						set(handle,'LineStyle',':','LineWidth',2);
						set(boxhandle,'LineStyle',':','LineWidth',1.5);
					end
					
				case 'slashed'
					set(handle,'LineStyle','--','LineWidth',1.5);
					if ~isempty(boxhandle)
						set(handle,'LineStyle','--','LineWidth',2);
						set(boxhandle,'LineStyle','-','LineWidth',1.5);
					end
					
				case 'Fatline'
					set(handle,'LineStyle','-','LineWidth',2);
					if ~isempty(boxhandle)
						set(handle,'LineStyle','-','LineWidth',2.5);
						set(boxhandle,'LineStyle','-','LineWidth',1.5);
					end
					
				case 'Fatdot'
					set(handle,'LineStyle',':','LineWidth',2);
					if ~isempty(boxhandle)
						set(handle,'LineStyle',':','LineWidth',2.5);
						set(boxhandle,'LineStyle',':','LineWidth',1.5);
					end
					
				case 'Fatslash'
					set(handle,'LineStyle','--','LineWidth',2);
					if ~isempty(boxhandle)
						set(handle,'LineStyle','--','LineWidth',2.5);
						set(boxhandle,'LineStyle','--','LineWidth',1.5);
					end
					
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',1.5);
					if ~isempty(boxhandle)
						set(handle,'LineStyle','-','LineWidth',2);
						set(boxhandle,'LineStyle','-','LineWidth',1.5);
					end
					
				case 'Fatautomatic'
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','line'));
					set(handle,'LineStyle',wheelLine{mod(existPlots,3)+1},'LineWidth',2);
					if ~isempty(boxhandle)
						set(handle,'LineStyle','-','LineWidth',2.5);
						set(boxhandle,'LineStyle','-','LineWidth',1.5);
					end
					
			
			end
		end
		
		
	
	
	end
	
	
	
	
end
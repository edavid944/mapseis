function  [handle legendentry] = PlotHistogram(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	%possible config fields for this function:
	%Data:				Not optional, the data to plot 
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'X',if the field is missing, nothing will be
	%					done
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Number',if the field is missing, nothing will be
	%					done			
	%Binning:			Sets the Binning Type: empty or 'default' the default value of 30 bins will be used 
	%					'BinNr' uses the number of bins specified under BinOption, 'BinVector' uses the vector
	%					specified under BinOption as bins, 'BinSize' greats the bin slef with the binsitz specified 
	%					under BinOption
	%BinOption:			is either an integer incase of 'BinNr', a Vector in case of 'BinVector' or a 3 number array in
	%					case of 'BinSize' with [BinSize Minimum Maximum] while the two later parameters are optional
	%					when not specified the rounded max min of the data will be used
	%RoundFactor:		allows to round the values found in max and min, leave empty or set to 0 if no rounding wished
	%		 			The rounding: max(ceil(data*1/Randfactor)) 0.1 will round to one decimal after comma, 1 will round
	%					to next integer.
	%FatLine:			Sets the lines around the bars to 2 instead of 1 (normally not needed) value type: logical 
	%LineColors:			Sets the LineColor: allowed are: 'same' -> sames as faceColor, 'black' and 'white'  
	%CustomLineColors;	Same as CustomColor but for the line
	%Colors:				This allows to change the Color of the line and the Marker (if plotted) with simple Presets 
	%					if not not specified the default Color ('blue') will be used. The presets 
	%					are: 'blue','red','green','black','cyan','magenta' and the automatic settings 
	%					which try use different Color every plot.
	%CustomColors:	 	This allows to specify the Color properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColor should be missing in
	%					ResStruct
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%LegendText:		Passthrough value if specified, else
	%					'Histogram' will be used as LegendEntry.
	
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	
	
	
	%Build some internal data settings
	wheelColor={'b','r','g','k','c','m'}
	
	
	%build bins if necessary
	TheBins=30;
	if ~isempty(ResStruct.Binnings)
		switch ResStruct.Binnings
			case 'default'
				TheBins=30;			
				
			case 'BinNr'
				TheBins=ResStruct.BinOption;
			
			case 'BinVector'
			 	if numel(ResStruct.BinOption)>1 %pre defined max and mins
			 		TheBins=ResStruct.BinOption(2):ResStruct.BinOption(1):ResStruct.BinOption(3);	
				else
					%do rounding if wanted 
					if isempty(ResStruct.RoundFactor) | ResStruct.RoundFactor==0 
						maxvalue=max(ResStruct.Data);
						minvalue=min(ResStruct.Data);	
					
					else
						maxvalue=max(ceil(ResStruct.Data*1/ResStruct.RoundFactor));
						minvalue=min(floor(ResStruct.Data*1/ResStruct.RoundFactor));	
					end
					
					%finally the bins
					TheBins=minvalue:ResStruct.BinOption(1):maxvalue;
					
				end	
		end
	end
	
	
	
	%plot the data
	hist(plotAxis,ResStruct.Data,TheBins);
	
	%get handles
	h = findobj(plotAxis,'Type','patch');
	
	%in case there are old plots of the Type 'patch' it is the first value in h
	handle = h(1);
	
	%change to default linewitdth of 1 (yes works here too)
	set(handle,'LineStyle','-','LineWidth',1)
	
	
	%use fat line if wanted
	if isfield(ResStruct,'FatLine') %written in this form to allow it to be left empty
		if FatLine
			set(handle,'LineStyle','-','LineWidth',2);			
		end		
	end

	
	
	%now change the parameters of the plot
	%-------------------------------------
	
		%the labels: if not set, set to 'X' and 'Number'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Number';
			end
			
			hYLabel = ylabel(plotAxis,Y_axisname);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
		
		
	
				
		
	
		
		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
			
			switch ResStruct.Colors
				case 'blue'
					set(handle,'FaceColor','b')
					sameColor='b';
				
				case 'red'
					set(handle,'FaceColor','r')
					sameColor='r';
				
				case 'green'
					set(handle,'FaceColor','g')
					sameColor='g';
				
				case 'black'
					set(handle,'FaceColor','k')
					sameColor='k';
				
				case 'cyan'
					set(handle,'FaceColor','c')
					sameColor='c';
				
				case 'magenta'
					set(handle,'FaceColor','m')
					sameColor='m';
				
				case 'automatic'
					%experimental but should work
					childish=get(plotAxis,'Children');
					existPlots=numel(findobj(childish,'Type','patch'));
					set(handle,'Color',wheelColor{mod(existPlots,6)+1});
					sameColor=wheelColor{mod(existPlots,6)+1};		
			end
		end

		%Customization Color
		if isfield(ResStruct,'CustomColors')
			%This option allows to customize the line with the common matlab commands
			set(handle,'Color',ResStruct.CustomColors);
			sameColor=ResStruct.CustomColors;
		end	
	
		
		%The LineColoring
		if isfield(ResStruct,'LineColorss')
			if ~isempty(ResStruct.LineColors)
				switch ResStruct.LineColors
					case 'black'
						set(handle,'EdgeColor','k');
					case 'white'
						set(handle,'EdgeColor','w');
					case 'same'
						set(handle,'EdgeColor',same);
				
				end
			
			end
		end	
	
		%CustomLineColor if wanted 
		if isfield(ResStruct,'CustomLineColors')
			if ~isempty(ResStruct.CustomLineColors)
				set(handle,'EdgeColor',ResStruct.CustomLineColors);
			end	
		end
		
		
		
		%set limits if needed
		if ~isempty(ResStruct.X_AxisLimit) | strcmp(ResStruct.X_AxisLimit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_AxisLimit);	
		end
		
		if ~isempty(ResStruct.Y_AxisLimit) | strcmp(ResStruct.Y_AxisLimit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_AxisLimit);	
		end
		
		if isfield(ResStruct,'LegendText');	
			if ~isempty(ResStruct.LegendText)
				legendentry=ResStruct.LegendText;
			else	
				legendentry='Histogram';
			end
		else
		legendentry=[];
					
		end				

end
function  [handle legendentry] = PlotColor(plotAxis,ResStruct)
	%This function is a genericplot used by the ResultGUI
	
	%the colorplot allows to plot a data value spatially coded as color
	%possible config fields for this function:
	%Data:				Not optional, the data to plot, in this case the data has to be a bit
	%					different, needed is a cell array with 3 entries, 1st one is a vector or matrix
	%					x-coordinates, 2nd one is a vector or matrix with y-coordinates and the 3rd element					
	%					has matrix with the color information (either size=x*y in the normal case or 
	%					size=x*y*3 in the case rgb plotting is wanted).
	%MapStyle:			Defines the plotting style wanted: 'smooth', uses the image plot (no lines between the gridcells
	%					'gridded' uses the pcolor plot, with drawn grid between the cells, and 'interpol' pcolor plot with
	%					with interpolation. 
	%AlphaData:			With the MapStyle 'smooth', Transparency can be used, the AlphaData has to be a matrix the same size 
	%					as the valuematrix with values between 0 (transparent) and 1 (solid). Alternativ also the keyword
	%					'auto' can be used, this will turn any cell having the value NaN in the Valuematrix to transparent.
	%X_Axis_Label:		The label under the X-Axis, if empty (not specified) it 
	%					will be set to 'X',if the field is missing, nothing will be
	%					done
	%Y_Axis_Label:		The label under the Y-Axis, if empty (not specified) it 
	%					will be set to 'Y',if the field is missing, nothing will be
	%					done			
	%Colors:			This defines either the ColorMap or the Color if only 3 coloumbs are defined
	%					Option for the ColorMap are: 'jet', 'hsv', 'hot', 'gray', 'cool', 'bone', 'autumn', 'summer'	
	%CustomColors:		This allows to specify the ColorMap properties freely, CAUTION: This field can not
	%					be left empty, to not use this option, the field CustomColorMap should be missing in
	%					ResStruct
	%X_Axis_Limit: 		Limits the range of the X-Axis, optional command (if left empty or set to 'auto'
	%					the automaticly defined ranges will be used)
	%Y_Axis_Limit: 		Same for the Y-Axis
	%C_Axis_Limit:		Limits the Range of the Colorbar, syntax, the same as X_Axis_Limit
	%ColorToggle:		Set to true will show the ColorBar
	%LegendText:		Write anything in it, it will not be send, Colormap do not support Legend entries
	
	
	
	%use the last plot if not specified
	if nargin<2
    	plotAxis=gca;	
	end	
	
	
	

	
	
	
	%Prepare data
	%--------------
	%The pcolor plot takes vectors and matrices as input for x and y coordinates, not so 
	%the image plot, only vectors are allowed --> correct if needed
	
	%default
	x_coords=ResStruct.Data{1};
	y_coords=ResStruct.Data{2};
	DataMatrix=ResStruct.Data{3};
	
	if isempty(ResStruct.MapStyle) | strcmp(ResStruct.MapStyle,'smooth')
		xSize=size(x_coords);
		ySize=size(y_coords);
		
		%check if both size components are not 1 for x and y
		if xSize(1)>1 & xSize(2)>1
			x_coords=x_coords(1,:);
		end
		
		
		if ySize(1)>1 & ySize(2)>1
			y_coords=y_coords(:,1);
		end			
	end
	
		
	
	
	%plot the data
	%--------------
	if isempty(ResStruct.MapStyle) | strcmp(ResStruct.MapStyle,'smooth')
		axes(plotAxis)
		handle = image(x_coords,y_coords,DataMatrix);
		set(plotAxis,'YDir','normal');
	
	else
		handle = pcolor(plotAxis,x_coords,y_coords,DataMatrix);	
		
		if strcmp(ResStruct.MapStyle,'interpol')
			shading interp;
		end
	end
		
		
	%now change the parameters of the plot
	%-------------------------------------
	
		%the labels: if not set, set to 'X' and 'Y'
		if isfield(ResStruct,'X_Axis_Label')
			X_axis_name=ResStruct.X_Axis_Label;
			
			if isempty(X_axis_name)
				X_axis_name='X';
			end
			
			hXLabel = xlabel(plotAxis,X_axis_name);
			set(hXLabel,'FontName', 'AvantGarde', 'FontSize', 12);
		end
		
		
		if isfield(ResStruct,'Y_Axis_Label')
			Y_axis_name=ResStruct.Y_Axis_Label; 	
			
			if isempty(Y_axis_name)
				Y_axis_name='Y';
			end
			
			hYLabel = ylabel(plotAxis,Y_axis_name);
			set(hYLabel,'FontName', 'AvantGarde', 'FontSize', 12);	
		end

		
		

		
		
		%Add AlphaMap if wanted
		if (isempty(ResStruct.MapStyle) | strcmp(ResStruct.MapStyle,'smooth')) & isfield(ResStruct,'AlphaData') 
			if ~isempty(ResStruct.AlphaData) & strcmp(ResStruct.AlphaData,'auto')
				AlphaNaN=double(~isnan(DataMatrix));
				%set(handle,'AlphaData',AlphaNaN);
				alpha(handle,AlphaNaN);
				set(handle,'AlphaDataMapping','none');
				
			elseif ~isempty(ResStruct.AlphaData) & isnumeric(ResStruct.AlphaData)
				%set(handle,'AlphaData',ResStruct.AlphaData);
				alpha(handle,double(ResStruct.AlphaData));
				set(handle,'AlphaDataMapping','none');
			end
		end	

		
		%change the color to the preset if needed
		if ~isempty(ResStruct.Colors) 
				switch ResStruct.Colors
					case 'jet'
						colormap(plotAxis,jet);
					case 'hsv'
						colormap(plotAxis,hsv);
					case 'hot'
						colormap(plotAxis,hot);
					case 'cool'
						colormap(plotAxis,cool);
					case 'gray'
						colormap(plotAxis,gray);
					case 'bone'
						colormap(plotAxis,bone);
					case 'summer'
						colormap(plotAxis,summer);
					case 'autumn'
						colormap(plotAxis,autumn);
								
				end
				

		end


		%Customization Color
		if isfield(ResStruct,'CustomColors')
				colormap(plotAxis,ResStruct.CustomColors);
		end	

		set(handle,'CDataMapping','scaled');


		%set limits if needed
		if ~isempty(ResStruct.X_Axis_Limit) | strcmp(ResStruct.X_Axis_Limit,'auto')
			xlim(plotAxis,'manual');
			xlim(plotAxis,ResStruct.X_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.Y_Axis_Limit) | strcmp(ResStruct.Y_Axis_Limit,'auto')
			ylim(plotAxis,'manual');
			ylim(plotAxis,ResStruct.Y_Axis_Limit);	
		end
		
		if ~isempty(ResStruct.C_Axis_Limit) | strcmp(ResStruct.C_Axis_Limit,'auto')
			caxis(plotAxis,'manual');
			caxis(plotAxis,ResStruct.C_Axis_Limit);	
		end

		
		if ~isempty(ResStruct.ColorToggle) & ResStruct.ColorToggle
			colorbar;
		end
		
		
		
		%build legend (if not empty it will just copy the entry from ResStruct)
		if isfield(ResStruct,'LegendText');	
			if ~isempty(ResStruct.LegendText)
				legendentry=[];
			else	
				legendentry=[];
			end
		else
		legendentry=[];
					
		end		
	
end
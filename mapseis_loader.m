function LoaderLink = mapseis_loader
% runme : start the MapSeis application
import mapseis.loader.*;


% Add the present working directory to get the +mapseis package
addpath(pwd);
% Add the Import directory for testing
addpath(fullfile(pwd,'Import',''));
addpath(fullfile(pwd,'AddOneFiles','gshhs',''));
addpath(fullfile(pwd,'AddOneFiles','Internal_Border',''))

LoaderLink = loaderInterface;


end
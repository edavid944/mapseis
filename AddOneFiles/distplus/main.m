%MAIN: shows the use of classWaitbardist.
%
% see also classWaitbardist worker_waitbardist
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
clc;

%% adding the JAVA package
classWaitbardist.initJAVA();

%% creating a waitbar
awaitbar = classWaitbardist();

if isempty( ver('distcomp'))
    %% updating the waitbar through TCP/IP sockets locally
    worker_waitbardist(awaitbar);
else
    %% creating a distributed job
    job = createJob('name','dctjavacom');
    set(job,'FileDependencies',{'communication.jar', ...
                                'worker_waitbardist.m',...
                                'classWaitbardist', ...
                                '@classTCPIPSender'});       
    createTask(job, 'worker_waitbardist', 0, {awaitbar});
    submit(job);

    %err = get(jm.jobs(1).tasks, 'error')
end
disp(' and now... the waitbar should be updated in few seconds.');

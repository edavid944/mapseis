%GETAVAILABLECONFIGTCPIP: retreive local hostname and available port.
%
%   [PORT, HOSTNAME] = GETAVAILABLECONFIGTCPIP() uses JAVA sockets to test
%   the availability of the port.
%   Notice: see
%   http://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers for port
%   numbers.
%
%   Example
%      [port, hostname] = getAvailableConfigTCPIP()
%
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
function [port, hostname] = getAvailableConfigTCPIP()
port = 30000; 
trynb = 0;
while ~isPortAvailable(port)
    port = port + 2;
    trynb = trynb + 1;
    if trynb > 100
        error('No available port.');
    end
end

[status, hostname] = system('hostname');
hostname(end) = '';

end

% Return true if and only if port is available.
function test = isPortAvailable(port)
    test = true;
    try
        ssock = java.net.ServerSocket(port);
        ssock.close();
    catch
        test = false;
    end
end

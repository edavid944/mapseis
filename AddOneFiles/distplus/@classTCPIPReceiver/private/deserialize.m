%DESERIALIZE: deserializes a string to a MATLAB data.
%
%   DATA = DESERIALIZE( STR ) transforms the string STR to a MATLAB data
%   DATA.
%
%   Example
%      s = struct('a', 22, 'b', 'foo'); 
%      str = serialize(s);
%      sd = deserialize(str);
%
% see also serialize
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
function out = deserialize(str)
d = sscanf(str, '%d,');
id = sprintf('%s.mat', tempname);
fid = fopen(id, 'w');
fwrite(fid, d);
fclose(fid);
out = load(id);
out = out.data;
delete(id);

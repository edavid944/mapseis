% CLASSTCPIPRECEIVER: Creates a TCPIP server object that can receive data.
%
%   CLASSTCPIPRECEIVER( PORT ) creates a server on local host on port PORT.
%   CLASSTCPIPRECEIVER() creates a server on local host on port available.
%   CLASSTCPIPRECEIVER is not blocking as it creates a thread that waits
%   for client to connect. Each time a client connects to the server,
%   another thread is created to allow the communication between the server
%   and the client.
%   The class is built upon custom JAVA code in communication.jar using JAVA sockets and JAVA threads.
%
%   Example
%       classTCPIPReceiver.initJAVA();
%       %% creating server (receiver)
%       r = classTCPIPReceiver();
%       %% receive callback
%       r.DataReceivedCallback = @disp;
%       r.start();
%       %% creating client (sender)
%       s = classTCPIPSender(r.hostname, r.port);
%       %% sending data
%       s.send('hello!');
%       delete(r);
%       delete(s);
%
% See also CLASSTCPIPSENDER
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
classdef classTCPIPReceiver
    properties
        port = NaN;
        hostname = '';
        DataReceivedCallback = [];
        javaObject = [];
    end

    methods(Static)
        function initJAVA()
            % adding the JAVA package
            path = fileparts(fileparts(mfilename('fullpath')));
            javaaddpath(fullfile(path, 'communication.jar'));
        end
    end
    
    methods
       function Object = classTCPIPReceiver(port)
            [Object.port, Object.hostname] = getAvailableConfigTCPIP();
            if nargin == 1
                Object.port = port;
            end
            try
                Object.javaObject = javaObject('mathworks.custom.com.Provider', Object.port); % PUT communication.jar in classpathtxt or adding it with javaaddpath
            catch ME
                if strcmp(ME.identifier, 'MATLAB:Java:ClassLoad')
                    error(ME.identifier, 'You should need to run "classTCPIPReceiver.initJAVA()" before using this class.');
                else
                    rethrow(ME);
                end
            end
        end
        
        function start(this)
            javaMethod('start', this.javaObject);
        end
        
        function stop(this)
            javaMethod('stop', this.javaObject);
        end
        
        function Object = set(Object, field, value)
            if strcmpi(field, 'DataReceivedCallback')
                Object.DataReceivedCallback = value;
                set(Object.javaObject, 'ActionPerformedCallback', @internalCallback);
%                 set(Object.javaObject, 'ActionPerformedCallback', @(x,y)disp(x));
            end

            function internalCallback(source, event)
%                 disp(event);
                if ~isempty(Object.DataReceivedCallback)
                    Object.DataReceivedCallback(deserialize(get(event, 'ActionCommand')));
                end
            end
        end

        function Object = subsasgn(Object, S, value)
            if S.type == '.' 
                Object = set(Object, S.subs, value);
            end
        end
        
        function delete(this)
            try
                javaMethod('stop', this.javaObject);
                set(this.javaObject, 'ActionPerformedCallback', '');
                clear this.javaObject;
            catch ME
                warning(ME.message);
            end
        end
        
    end

end
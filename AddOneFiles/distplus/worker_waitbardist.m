%WORKER_WAITBARDIST: updates a distant classWaitbardist object via TCP/IP sockets.
%
%   WORKER_WAITBARDIST()
%
%   Example
%      receiver_awaitbar = classWaitbardist();
%      sender_awaitbar = classWaitbardist(receiver_awaitbar);
%      worker_waitbardist(sender_awaitbar);
%
%   NOTICE: When used with the Parallel Computing Toolbox�, the sender_awaitbar variable doesn't need to be
%   instanciated as the Parallel Computing Toolbox� will use the saveobj and loadobj methods of
%   classWaitbardist to transmit the object to the MATLAB� Distributed Computing Server�. The loadobj method
%   will automatically transform the (TCPIP server) awaitbardist from client machine
%   to a worker (TCPIP client) awaitbardist.
%
% see also classWaitbardist main
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
function worker_waitbardist(awaitbardist)

awaitbardist.update(0, 'I am the worker!');

for i=0:0.01:1
    awaitbardist.update(i, 'Me, the worker, I work!');
    pause(1);
end
awaitbardist.close();

end
%SERIALIZE: serializes a MATLAB data into a string.
%
%   STR = SERIALIZE( DATA ) transforms the MATLAB data
%   DATA to the string STR.
%
%   Example
%      s = struct('a', 22, 'b', 'foo'); 
%      str = serialize(s);
%      sd = deserialize(str);
%
% see also deserialize
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
function str = serialize(data)
if nargin ~= 1
    error('Only one argument allowed.');
end
id = sprintf('%s.mat', tempname);
evalin('caller', sprintf('save(''%s'', ''data'');', id ));
fid = fopen(id, 'r');
d = fread(fid);
fclose(fid);
delete(id);
str = sprintf('%d,', d);

% CLASSTCPIPSENDER: Creates a TCPIP client object that can send data to a TCP/IP server.
%
%   CLASSTCPIPSENDER( HOST, PORT ) creates a client that connects to hostname
%   HOST on port PORT.
%   MATLAB data can be send with SEND method. If the send fails, no error
%   is raised but the client try to connect again to the server. 
%   The class is built upon custom JAVA code in communication.jar using JAVA sockets and JAVA threads.
%
%   Example
%       classTCPIPReceiver.initJAVA();
%       %% creating server (receiver)
%       r = classTCPIPReceiver();
%       %% receive callback
%       r.DataReceivedCallback = @disp;
%       r.start();
%       %% creating client (sender)
%       s = classTCPIPSender(r.hostname, r.port);
%       %% sending data
%       s.send('hello!');
%       delete(r);
%       delete(s);
%
% See also CLASSTCPIPRECEIVER
%
%   $Author: cpouillo $
%   Copyright 2009 - 2009 The MathWorks, Inc.
%   $Rev: 22 $    $Date: 2009-09-03 09:43:07 +0200 (jeu., 03 sept. 2009) $
classdef classTCPIPSender

    properties
        javaObject = [];
        hostname;
        port;
        period = -1; % in seconds
        lastSendingTime = 0; 
    end
    
    methods(Static)
        function initJAVA()
            % adding the JAVA package
            path = fileparts(fileparts(mfilename('fullpath')));
            javaaddpath(fullfile(path, 'communication.jar'));
        end
    end
    
    methods
        function Object = classTCPIPSender(hostname, port)
            try
                Object.hostname = hostname;
                Object.port = port;
                Object.javaObject = javaObject('mathworks.custom.com.Requester', hostname, port);
            catch ME
                if strcmp(ME.identifier, 'MATLAB:Java:ClassLoad')
                    error(ME.identifier, 'You should need to run "classTCPIPSender.initJAVA()" before using this class.');
                else
                    rethrow(ME);
                end
            end
            Object.lastSendingTime = clock;
        end
        
        function this = send(this, data)
            if etime(clock, this.lastSendingTime) > this.period
                javaMethod('send', this.javaObject, serialize(data));
                this.lastSendingTime = clock;
            end
        end
        
        function delete(this)
            this.javaObject.close();
        end
    end

end
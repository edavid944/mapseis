function TestDateStr2Num(doSpeed)
% Automatic test: DateStr2Num
% This is a routine for automatic testing. It is not needed for processing and
% can be deleted or moved to a folder, where it does not bother.
%
% TestDateStr2Num(doSpeed)
% INPUT:
%   doSpeed: Optional logical flag to trigger time consuming speed tests.
%            Default: TRUE. If no speed test is defined, this is ignored.
% OUTPUT:
%   On failure the test stops with an error.
%
% Tested: Matlab 6.5, 7.7, 7.8, WinXP
% Author: Jan Simon, Heidelberg, (C) 2009-2010 matlab.THISYEAR(a)nMINUSsimon.de

% $JRev: R0c V:002 Sum:lFmmU0gSFEZp Date:30-Jun-2010 16:03:53 $
% $License: BSD $
% $File: Tools\UnitTests_\TestDateStr2Num.m $

% Initialize: ==================================================================
if nargin == 0
   doSpeed = true;
end

NRandTest = 10000;

isMatlab7 = sscanf(version, '%f', 1) >= 7.0;

% Do the work: =================================================================
% Hello:
disp(['==== Test DateStr2Num:  ', datestr(now, 0)]);
disp(['  File: ', which('DateStr2Num')]);
fprintf('\n');

% Number format:
NowNum  = now;
NowTime = datenum(floor(datevec(NowNum)));  % Integer seconds only!
NowDate = floor(NowNum);

N0  = DateStr2Num(datestr(NowTime, 0), 0);
N1  = DateStr2Num(datestr(NowTime, 1), 1);
N29 = DateStr2Num(datestr(NowTime, 29), 29);
N30 = DateStr2Num(datestr(NowTime, 30), 30);
N31 = DateStr2Num(datestr(NowTime, 31), 31);

if isequal(NowTime, N0) == 0
   error(['*** ', mfilename, ': DATESTR(0) format failed.']);
end
if isequal(NowDate, N1) == 0
   error(['*** ', mfilename, ': DATESTR(1) format failed.']);
end
if isequal(NowDate, N29) == 0
   error(['*** ', mfilename, ': DATESTR(29) format failed.']);
end
if isequal(NowTime, N30) == 0
   error(['*** ', mfilename, ': DATESTR(30) format failed.']);
end
if isequal(NowTime, N31) == 0
   error(['*** ', mfilename, ': DATESTR(31) format failed.']);
end
disp('  ok: Current date converted to 0,1,29,30,31 format.');

disp('== Random test data...');
drawnow;
maxDate = datenum('31-Dec-9999');
minDate = datenum('01-Jan-0100');
NumPool = minDate + rand(NRandTest, 1) * (maxDate - minDate);
NumPool = datenummx(floor(datevec(NumPool)));
for iFormat = [0, 1, 29, 30, 31]
   if iFormat == 1 || iFormat == 29  % Without time:
      Want = floor(NumPool);
   else                              % With time
      Want = NumPool;
   end
   StrPool = cellstr(datestr(Want, iFormat));
   ConvC   = DateStr2Num(StrPool, iFormat);
   ConvS   = zeros(size(Want));
   for iP = 1:NRandTest
      ConvS(iP) = DateStr2Num(StrPool{iP}, iFormat);
   end
   
   if any(abs(Want - ConvS) > 131072 * eps)
      error(['*** ', mfilename, ': Failed to convert: "', ...
         StrPool{iP}, '" in format ' sprintf('%d', iFormat)]);
   end
   if ~isequal(ConvS, ConvC)
      error(['*** ', mfilename, ...
         ': Conversion differs for string an cell strings: "', char(10), ...
         StrPool{iP}, '" in format ' sprintf('%d', iFormat)]);
   end
end
fprintf('  ok: converted %d dates to formats 0, 1, 29, 30, 31\n', NRandTest);

% Speed: -----------------------------------------------------------------------
disp([char(10), '== Speed tests...']);
formatList = { ...
   0,  'dd-mmm-yyyy HH:MM:SS'; ...
   1,  'dd-mmm-yyyy'; ...
   29, 'yyyy-mm-dd'; ...
   30, 'yyyymmddTHHMMSS'; ...
   31, 'yyyy-mm-dd HH:MM:SS'};

% Find a suiting number of loops:
C = datestr(floor(datevec(now)), 0);
if doSpeed
   iLoop     = 0;
   startTime = cputime;
   while cputime - startTime < 1.0
      v = datenum(C);     %#ok<NASGU>
      clear('v');
      iLoop = iLoop + 1;
   end
   nLoops = 100 * ceil(iLoop / ((cputime - startTime) * 50));
   disp([sprintf('  %d', nLoops) ' loops on this machine.']);
else
   disp('  Use at least 2 loops (displayed times are random!)');
   nLoops = 2;
end

disp('  Single string:');
if isMatlab7
   knownFormats = [0, 1, 29, 30, 31];
else  % Matlab 6:
   knownFormats = [0, 1];
end

for iFormat = knownFormats
   C = datestr(floor(datevec(now)), iFormat);
   
   if isMatlab7
      thisFormat = formatList{[formatList{:, 1}] == iFormat, 2};
      tic;
      for i = 1:nLoops
         v = datenum(C, thisFormat);  %#ok<NASGU>
         clear('v');
      end
      tDatenum = toc + eps;
   else
      tic;
      for i = 1:nLoops
         v = datenum(C);  %#ok<NASGU>, no Format in Matlab 6
         clear('v');
      end
      tDatenum = toc + eps;
   end
   
   tic;
   for i = 1:nLoops * 10
      v = DateStr2Num(C, iFormat);  %#ok<NASGU>
      clear('v');
   end
   tNum = toc / 10;
   
   disp(['    Format ', sprintf('%2d', iFormat), ...
      '   DATENUM:  ', sprintf('%-7.3f', tDatenum), ...
      '   DateStr2Num:  ', sprintf('%-7.3g', tNum), ...
      '   ==> ', sprintf('%.2f', 100 * tNum / tDatenum), '%']);
end

% Cell string:
fprintf('\n');
nDate = 10000;
CList = cell(nDate, 4);
for iC = 1:nDate
   CList{iC, 1} = datestr(now, 0);
   CList{iC, 2} = datestr(now, 29);
   CList{iC, 3} = datestr(now, 30);
   CList{iC, 4} = datestr(now, 31);
end

% Find a suiting number of loops:
if doSpeed
   iLoop      = 0;
   startTime  = cputime;
   thisFormat = formatList{[formatList{:, 1}] == 0, 2};
   C          = CList(:, 1);
   while cputime - startTime < 1.0
      v = datenum(C, thisFormat);     %#ok<NASGU>
      clear('v');
      iLoop = iLoop + 1;
   end
   nLoops = ceil(iLoop / (cputime - startTime));
   disp([sprintf('  %d', nLoops) ' loops on this machine.']);
else
   disp('  Use at least 2 loops (displayed times are random!)');
   nLoops = 2;
end

disp('  {1 x 10000} cell string:');
if isMatlab7
   knownFormats = [0, 29, 30, 31];
else
   knownFormats = 0;
end

for jFormat = 1:length(knownFormats)
   iFormat = knownFormats(jFormat);
   C = CList(:, jFormat);
   
   if isMatlab7
      thisFormat = formatList{[formatList{:, 1}] == iFormat, 2};
      tic;
      for i = 1:nLoops
         v = datenum(C, thisFormat);  %#ok<NASGU>
         clear('v');
      end
      tDatenum = toc + eps;
   else
      tic;
      for i = 1:nLoops
         v = datenum(C);  %#ok<NASGU>, no Format in Matlab 6
         clear('v');
      end
      tDatenum = toc + eps;
   end
   
   tic;
   for i = 1:nLoops * 10
      v = DateStr2Num(C, iFormat);  %#ok<NASGU>
      clear('v');
   end
   tNum = toc / 10;
   
   disp(['    Format ', sprintf('%2d', iFormat), ...
      '   DATENUM:  ', sprintf('%-7.3f', tDatenum), ...
      '   DateStr2Num:  ', sprintf('%-7.3g', tNum), ...
      '   ==> ', sprintf('%.1f', 100 * tNum / tDatenum), '%']);
end

% Goodbye:
disp([char(10), 'DateStr2Num passed the tests.']);

return;

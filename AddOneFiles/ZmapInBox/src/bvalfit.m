%
%   Calculates Freq-Mag functions (b-value) for two time-segments
%   finds best fit to the foreground for a modified background
%   assuming a change in time of the following types:
%   Mnew = Mold + d     , i.e. Simple magnitude shift
%   Mnew = c*Mold + d   , i.e. Mag stretch plus shift 
%   Nnew = fac*Nold     , i.e. Rate change (N = number of events)
%                                      R. Zuniga IGF-UNAM/GI-UAF  6/94
%                                      Rev. 4/2001
disp('This is /src/bvalfit.m')

% This is the info window text
%
   ttlStr='Comparing Seismicity rates ';
    hlpStr1map= ...
        ['                                                '
         ' To be Implemented                              '
         '                                                '];


if ic == 0 
global p
backg = [ ] ;
foreg = [ ] ;
format short;
fac = 1.0;

figure
bvfig = gcf;
set(bvfig,'Units','normalized','NumberTitle','off','Name','b-value curves');
set(gcf,'pos',[ 0.435  0.3 0.5 0.5])

if length(newcat) == 0, newcat = a; end;
maxmag = max(newcat(:,6));
mima = min(newcat(:,6));
if mima > 0 ; mima = 0 ; end
t0b = min(newcat(:,3));
teb = max(newcat(:,3));
n = length(newcat(:,1));
tdiff = round(teb - t0b);

% number of mag units
nmagu = (maxmag-mima*10)+1;
 
bval = zeros(1,nmagu);
bval2 = zeros(1,nmagu);
bvalsum = zeros(1,nmagu);
bvalsum2 = zeros(1,nmagu);
bvalsum3 = zeros(1,nmagu);
bvalsum4 = zeros(1,nmagu);
backg_ab = [ ];
foreg_ab = [ ];
backg_be = [ ];
foreg_be = [ ];
backg = [ ];
foreg = [ ];
backg_beN = [ ];          
backg_abN = [ ];           
td12 = t2p(1) - t1p(1);              
td34 = t4p(1) - t3p(1);

l = newcat(:,3) > t1p(1) & newcat(:,3) < t2p(1) ;
backg =  newcat(l,:);
[bval,xt2] = hist(backg(:,6),(mima:0.1:maxmag));
bval = bval/td12;                      % normalization
bvalsum = cumsum(bval);                        % N for M <=
bvalsum3 = cumsum(bval(length(bval):-1:1));    % N for M >= (counted backwards)
xt3 = (maxmag:-0.1:mima);
[cumux xt] = hist(newcat(l,3),t1p(1):par1/365:t2p(1));

l = newcat(:,3) > t3p(1) & newcat(:,3) < t4p(1) ;
foreg = newcat(l,:);
bval2 = hist(foreg(:,6),(mima:0.1:maxmag));
bval2 = bval2/td34;                     % normallization
bvalsum2 = cumsum(bval2);
bvalsum4 = cumsum(bval2(length(bval2):-1:1));
[cumux2 xt] = hist(newcat(l,3),t3p(1):par1/365:t4p(1));
 mean1 = mean(cumux);
 mean2 = mean(cumux2);
 var1 = cov(cumux);
 var2 = cov(cumux2);
zscore = (mean1 - mean2)/(sqrt(var1/length(cumux)+var2/length(cumux2)));

backg_be = log10(bvalsum);
backg_ab = log10(bvalsum3);
foreg_be = log10(bvalsum2);
foreg_ab = log10(bvalsum4);

orient landscape
rect = [0.2,  0.2, 0.70, 0.70];           % plot Freq-Mag curves
axes('position',rect)
semilogy(xt3,bvalsum3,'om')
%semilogy(xt2,bvalsum,'om')
hold on 
%semilogy(xt2,bvalsum,'-.m')
%semilogy(xt2,bvalsum2,'xb')
%semilogy(xt2,bvalsum2,'b')
semilogy(xt3,bvalsum4,'xb')
%semilogy(xt3,bvalsum4,'b')

%semilogy(xt3,bvalsum3,'-.m')
te1 = max([bvalsum  bvalsum2 bvalsum4 bvalsum3]);
te1 = te1 - 0.2*te1;
title2([file1 '   o: ' num2str(t1p(1)) ' - ' num2str(t2p(1)) '     x: ' num2str(t3p(1)) ' - '  num2str(t4p(1)) ],'FontSize',fs10,'FontWeight','bold')

xlabel2('Magnitude','FontSize',fs10,'FontWeight','bold')
ylabel2('Cum. Number -normalized','FontSize',fs10,'FontWeight','bold')
                                             %  find b-values;
set(gca,'box','on',...
        'DrawMode','fast','TickDir','out','FontWeight',...
        'bold','FontSize',fs10,'Linewidth',[1.2])

%set(gca,'Color',[1 1 0.7])

%hld = helpdlg('Please select two magnitudes to be used in the calculation of straight line fit i.e.  b value of BACKGROUND (o) ',...
%   'Magnitude Range Selection');
%set(hld,'pos',[50 500 400 70]);
%msgbox('Please select two magnitudes to be used in the calculation of straight line fit i.e.  b value of BACKGROUND (o) ',...
%   ,'Magnitude Range Selection','help')
figure(mess)
 clf;
 cla; 
  set(gcf,'Name','Magnitude selection ');
  set(gca,'visible','off');
  txt5 = text(...
                'EraseMode','normal',...
                'Position',[.01 0.99 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold',...
                'String','Please select two magnitudes to be used');
  txt1 = text('Position',[.01 0.84 0 ],...
                'FontSize',fs12 ,...
                'FontWeight','bold',...
                'String','in the calculation of straight line fit i.e.');
  txt2 = text('Position',[.01 0.66 0 ],...
                'FontSize',fs12 ,...
                'FontWeight','bold',...
                'String','b value of BACKGROUND (o)');
                         
figure(bvfig)
seti = uicontrol('BackGroundColor','c',...
   'Units','normal','Position',[.4 .01 .2 .05],...
   'String','Select Mag1 ');

pause(1)

par2 = 0.1 * max(bvalsum3);
par3 = 0.12 * max(bvalsum3);
M1b = [];
M1b = ginput(1);
tx1 = text( M1b(1),M1b(2),['M1'] );
set(seti,'String','Select Mag2');

pause(0.1)

M2b = [];
M2b = ginput(1);
tx2 = text( M2b(1),M2b(2),['M2'] );

pause(0.1)
delete(seti)

ll = xt3 > M1b(1) & xt3 < M2b(1);
x = xt3(ll);
y = backg_ab(ll);
 p  = polyfit(x,y,1);                  % fit a line to background
f = polyval(p,x);
f = 10.^f;
hold on
semilogy(x,f,'r')                         % plot linear fit to backg
r = corrcoef(x,y);
r = r(1,2);
std_backg = std(y - polyval(p,x));      % standard deviation of fit

%hld = helpdlg('Please select two magnitudes to be used in the calculation of straight line fit i.e.  b value of FOREGROUND (x) ','Magnitude Range Selection');
%set(hld,'pos',[50 500 400 70]);
%msgbox('Please select two magnitudes to be used in the calculation of straight line fit i.e.  b value of BACKGROUND (o) ',...
%  ,'Magnitude Range Selection','help')
figure(mess)
 clf;
 cla; 
  set(gcf,'Name','Magnitude selection ');
  set(gca,'visible','off');
  txt5 = text(...
                'EraseMode','normal',...
                'Position',[.01 0.99 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold',...
                'String','Please select two magnitudes to be used');
  txt1 = text('Position',[.01 0.84 0 ],...
     				'FontSize',fs12 ,...
                'FontWeight','bold',...
                'String','in the calculation of straight line fit i.e.');
  txt2 = text(...
     'Position',[.01 0.66 0 ],...
                'FontSize',fs12 ,...
                'FontWeight','bold',...
                'String','b value of FOREGROUND (x)');
     
figure(bvfig)
seti = uicontrol('BackGroundColor','c','Units','normal',...
                 'Position',[.4 .01 .2 .05],'String','Select Mag1 ');

pause(1)

par2 = 0.1 * max(bvalsum3);
par3 = 0.12 * max(bvalsum3);
M1f = [];
M1f = ginput(1);
tx3 = text( M1f(1),M1f(2),['M1'] )
set(seti','String','Select Mag2');

pause(0.1)

M2f = [];
M2f = ginput(1);
tx4 = text( M2f(1),M2f(2),['M2'] )

pause(0.1)
delete(seti)

l = xt3 > M1f(1) & xt3 < M2f(1);
x = xt3(l);
y = foreg_ab(l);                
pp = polyfit(x,y,1); 
            % fit a line to foreground
f = polyval(pp,x);
f = 10.^f;                    
semilogy(x,f,'r')                   % plot fit to foreg
rr = corrcoef(x,y);
rr = rr(1,2);
std_foreg = std(y - polyval(pp,x));      % standard deviation of fit

figure(mess)
clf
set(gca,'visible','off')
set(gcf,'Units','normalized','pos',[ 0.03  0.1 0.4 0.7])
set(gcf,'Name','Compare Results');
orient tall
te = uimultitext(0.,0.99, ['   Catalogue : ' file1]) ;
set(te,'FontSize',fs10);
stri = [ 'Background (o):   ' num2str(t1p(1)) '  to  ' num2str(t2p(1)) ];
te = text(0.01,0.93, stri) ;
set(te,'FontSize',fs10);
aa = p(2) *1000.0;
aa = round(aa);
aa = aa/1000.0;         
bb = p(1) *1000.0;
bb = round(bb);
bb = bb/1000.0;          % round to 0.001
stri = [' Log N = ' num2str(aa)  num2str(bb) '*M ' ];
te = text(0.01,0.88, stri) ;
set(te,'FontSize',fs10);
stri = [ 'Foreground (x):   ' num2str(t3p(1)) '  to  ' num2str(t4p(1)) ];
te = text(0.01,0.83, stri) ;
set(te,'FontSize',fs10);
aa = pp(2) *1000.0;
aa = round(aa);
aa = aa/1000.0;         
bb = pp(1) *1000.0;
bb = round(bb);
bb = bb/1000.0;          % round to 0.001
stri = [' Log N = ' num2str(aa) num2str(bb) '*M '];
te = text(0.01,0.78, stri) ;
set(te,'FontSize',fs10);
disp([' Correlation coefficient for background = ', num2str(r) ]);                                disp([' Correlation coefficient for foreground = ', num2str(rr) ]);
%clear aa bb;   
%%dM = sum(backg_ab(ll) - foreg_ab(ll))/(length(backg_ab(ll))*p(1)) ; 
%  find simple shift                 
                      % first find Mmin ( M for which the background relation 
                      % departs from straight line by more than std ) 
ld = abs(backg_ab - polyval(p,xt3)) <= std_backg;
[min_backg, ldb] = min(xt3(ld));        % Mmin of background
n1 = backg_ab(ld);
n1 = n1(ldb);                           % Cum number for Mmin background
magi = (n1 - pp(2))/pp(1)  % magi is intercept of n1 with foreground linear fit 
dM = magi - min_backg;        % magnitude shift
ld = abs(foreg_ab - polyval(pp,xt3)) <= std_foreg;
[min_foreg, ldf] = min(xt3(ld));        % min_foreg is Mmin of foreground
disp([' Mmin for background = ', num2str(min_backg) ]);                                disp([' Mmin for foreground = ', num2str(min_foreg) ]);
stri = [ 'Minimum magnitude for Background = ' num2str(min_backg) ]; 
te = text(0.01,0.73, stri) ;
set(te,'FontSize',fs10);
stri = [ 'Minimum magnitude for Foreground = ' num2str(min_foreg) ];
te = text(0.01,0.68, stri) ;
set(te,'FontSize',fs10);
stri = ['Z score between both rates: '];
te = text(0.01,0.63, stri) ;
set(te,'FontSize',fs10);
stri = [' Z = ' num2str(zscore) ];
te = text(0.01,0.58, stri) ;
set(te,'FontSize',fs10);
%%figure(bvfig)
%%set(gcf,'pos',[ 0.200  0.2 0.8 0.8])
%%set(gca,'pos',[0.10 0.70 0.35 0.20])

%%set(tx1,'visible','off');
%%set(tx2,'visible','off');
%%set(tx3,'visible','off');
%%set(tx4,'visible','off');
dM = (round(dM *10.0))/10;     % round to 0.1 
backg_new = [backg(:,1:5), backg(:,6)+dM, backg(:,7)];    %  add shift
%backg_new(:,6) = (round(backg_new(:,6)*10))/10;         % round to 0.1

[bvalN,xt2] = hist(backg_new(:,6),(mima:0.1:maxmag));
bvalN = bvalN/td12;                               % normalize
bvalsumN = cumsum(bvalN);
bvalsum3N = cumsum(bvalN(length(bvalN):-1:1));
backg_beN = log10(bvalsumN);
backg_abN = log10(bvalsum3N);

%ld = [ ];                        % calculate residual in b-value curves
%ld = xt2 <= max(min_foreg, min_backg);
%res1 = abs(foreg_be(ld) - backg_beN(ld));     % absolute residuals
%ld = 1 - (isnan(res1) + isinf(res1));     % avoid no information in logarthmic
%res1 = sum(res1(ld))/length(res1(ld));

%ld = [ ];
%ld = xt3 >= min_foreg;
%res2 = abs(foreg_ab(ld) - backg_abN(ld));
%ld = 1 - (isnan(res2) + isinf(res2));
%res2 = sum(res2(ld))/length(res2(ld));
%res = (res1 + res2)/2;
                                               
res =  (sum((bvalN - bval2).^2)/length(bval2))^0.5 ; % residual in histograms
%%disp(['Average residual of simple shift = ', num2str(res)]);

figure(mess)
stri = [ 'Suggested single magnitude shift (d):']
te = uimultitext(0.01,0.50, stri) ;
set(te,'FontSize',fs10,'FontWeight','bold');
stri = ['Mx = Mo + (', num2str(dM),')']
te = uimultitext(0.01,0.45, stri) ;
set(te,'FontSize',fs10,'FontWeight','bold');
                               %  compute magnitude stretch and shift
pause(0.1)                 

mf = p(1)/pp(1);            % factor is calculated from ratio of b values
mf = (round(mf *100.0))/100.0;    % round to 0.01 
dM = -mf*(pp(2) - p(2))/p(1);   %  find shift by diff of zero ordinates
dM = (round(dM *100.0))/100.0;    % round to 0.01
stri = [ 'Linear Mag correction (stretch, c, and shift, d):' ];
te = uimultitext(0.01,0.38, stri) ;
set(te,'FontSize',fs10,'FontWeight','bold');
stri = [ 'Mx = ',num2str(mf), '* Mo + (', num2str(dM),')' ];
te = uimultitext(0.01,0.33, stri) ;
set(te,'FontSize',fs10,'FontWeight','bold'); 
%figure
%hisfg = gcf;
%set(hisfg,'Units','normalized','NumberTitle','off','Name','Histograms of Last Correction');
%set(hisfg,'pos',[ 0.4  0.01 0.45 0.55])
hold on
end   % if ic   

if ic == 0 | ic == 2 ,
 
figure(bvfig)
if ic == 2, clf, end;
%%rect = [0.10,  0.41 0.350, 0.20];
%%axes('position',rect)
bvalsumN = [ ];
bvalsum3N = [ ];
                                                         % Modify Magnitudes
backg_new = [backg(:,1:5), (mf*backg(:,6))+dM, backg(:,7)];
%backg_new(:,6) = (round(backg_new(:,6)*10))/10;         % round to 0.1

[bvalN,xt2] = hist(backg_new(:,6),(mima:0.1:maxmag));
bvalN = bvalN/td12;                              % normalize
bvalsumN = cumsum(bvalN);
bvalsum3N = cumsum(bvalN(length(bvalN):-1:1));
backg_beN = log10(bvalsumN);
backg_abN = log10(bvalsum3N);

%ld = [ ];                        % calculate residual in b-value curves
%ld = xt2 <= max(min_foreg, min_backg);
%res1 = abs(foreg_be(ld) - backg_beN(ld)); 
%ld = 1 - (isnan(res1) + isinf(res1));
%res1 = sum(res1(ld))/length(res1(ld));
%ld = [ ];
%ld = xt3 >= min_foreg;
%res2 = abs(foreg_ab(ld) - backg_abN(ld));
%ld = 1 - (isnan(res2) + isinf(res2));
%res2 = sum(res2(ld))/length(res2(ld));
%res = (res1 + res2)/2;
                                         % residual in histograms
res =  (sum((bvalN - bval2).^2)/length(bval2))^0.5 ;	    
%%disp(['Average residual of shift and stretch = ', num2str(res)]);

                                  % find rate increase_decrease 
if ic ==0 |ic ==1; 
%fac = max(bvalsum2)/max(bvalsum) ; % by diff in total cumulative number
%fac1 = 10^(pp(2) - p(2)) ;           % by zero ordinates 
%fac1 = fac1 *100.0;
%fac1 = round(fac1);
%fac1 = fac1/100.0;               % round to 0.01                
%fac = fac1;

%fac = foreg_be - backg_be;
%l = 1 - (isnan(fac) + isinf(fac));
%ll = xt2 <= magi;
%ll = xt2;
rat = [ ];
rat = bval2/bvalN;            % by mean of ratios
l = 1 - (isnan(rat) + isinf(rat));
fac1 = mean(rat(l));
fac1 = fac1 *100.0;
fac1 = round(fac1);
fac1 = fac1/100.0;               % round to 0.01                
fac = fac1;
end;     % if ic 
                             
ind = 0;                      %  find minimum magnitude for the rate change
resm = [ ];
%for magx = minma:0.1:magi      % start from min mag of catalogue
%bvalNN = bvalN;
%l = xt2 <= magx;
%ind = ind +1;
% = length(bvalNN(l));
%bvalNN(1:ind2) = bvalN(1:ind2)*fac ;    % apply rate correction up to magx
%bvalsumN = cumsum(bvalNN);
%l = xt2 <= magi;
%resm(ind) = (sum((bvalsumN - bvalsum2).^2)/length(bvalsum2))^0.5  ;
%resm(ind) = (sum((bvalNN - bval2).^2)/length(bvalNN))^0.5 ;
%disp(['magnitude = ', num2str(magx), ' residual =  ',num2str(resm(ind))]);
%end     % for magx
if ic ==0; fac = 1.0 ; end
%magx = minma:0.1:magi;
%[res,ll] = min(resm); % apply rate correction to previous data
%l = xt2 <= magx(ll);  
%ind2 = length(bvalN(l));    
%bvalN(1:ind2) = bvalN(1:ind2)*fac; % up to magnitude found above
bvalN = bvalN*fac ;    % apply rate to all data
                                     
bvalsumN = cumsum(bvalN);
bvalsum3N = cumsum(bvalN(length(bvalN):-1:1));
backg_beN = log10(bvalsumN);
backg_abN = log10(bvalsum3N);

%ld = [ ];                        % calculate residual in b-value curves
%ld = xt2 <= max(min_foreg, min_backg);
%res1 = abs(foreg_be(ld) - backg_beN(ld)); 
%ld = 1 - (isnan(res1) + isinf(res1));
%res1 = sum(res1(ld))/length(res1(ld));
%ld = [ ];
%ld = xt3 >= min_foreg;
%res2 = abs(foreg_ab(ld) - backg_abN(ld));
%ld = 1 - (isnan(res2) + isinf(res2));
%res2 = sum(res2(ld))/length(res2(ld));
%res = (res1 + res2)/2;
magi = magi *10.0;
magi = round(magi);
magi = magi/10.0;               % round to 0.1
                                          % residual in histograms
res =  (sum((bvalN - bval2).^2)/length(bval2))^0.5 ;

figure(mess)
if ic == 0 | ic == 1,
%magix = magx(ll) *100.0;
%magix = round(magix);
%magix = magix/100.0;               % round to 0.01
stri = [ 'Suggested rate change (Nx = fac*No): # fac = ' num2str(fac1) ];
te = uimultitext(0.01,0.27, stri) ;
set(te,'FontSize',fs10,'Visible','on');
%stri = 'Change: ';
%%te = text(0.01,0.17, stri) ;
end   % if ic
%magis = magx(ll) *10.0;           %   save last magnitude found
%magis = round(magis);
%magis = magis/10.0;               % round to 0.1
magis = maxmag;

uicontrol('BackGroundColor','y','Units','normal','Position',[.88 .9 .11 .06],'String','Print  ','callback','myprint')
uicontrol('BackGroundColor','y','Units','normal','Position',[.88 .80 .11 .06],'String','Close  ','callback','welcome;done')
                                           
freq_field1=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.30 .16 .13 .07],...
        'Units','normalized','String',num2str(dM),...
        'CallBack','dM=str2num(get(freq_field1,''String'')); set(freq_field1,''String'',num2str(dM));');

    freq_field2=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.75 .16 .13 .07],...
        'Units','normalized','String',num2str(mf),...
        'CallBack','mf=str2num(get(freq_field2,''String'')); set(freq_field2,''String'',num2str(mf));');
  
    freq_field3=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.30 .05 .13 .07],...
        'Units','normalized','String',num2str(fac),...
        'CallBack','fac=str2num(get(freq_field3,''String'')); set(freq_field3,''String'',num2str(fac));');

  %freq_field4=uicontrol('BackGroundColor','g','Style','edit',...
 %       'Position',[.75 .05 .13 .07],...
 %       'Units','normalized','String',num2str(magi),...
 %       'CallBack','magi=str2num(get(freq_field4,''String'')); set(freq_field4,''String'',num2str(magi));');



  txt1 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[.01 0.11 0 ],...
                'Rotation',0 ,...
                'FontSize',fs10 ,...
                'String','Shift (d)');

  txt2 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[.44 0.11 0 ],...
                'Rotation',0 ,...
                'FontSize',fs10 ,...
                'String','  Stretch factor (c)');
 
  txt3 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[.01 0.0 0 ],...
                'Rotation',0 ,...
                'FontSize',fs10 ,...
                'String','Rate factor');

 % txt4 = text(...
 %               'Color',[0 0 0 ],...
 %               'EraseMode','normal',...
 %               'Position',[.67 0.0 0 ],...
 %               'Rotation',0 ,...
 %               'FontSize',fs10 ,...
 %               'String','Mmax');

  go_button=uicontrol('BackGroundColor','y','Style','Pushbutton',...
        'Position',[.52 .01 .10 .07 ],...
        'Units','normalized',...
        'Callback','ic = 2, bvalfit',...
        'String','Go');
res = bval2 - bvalN ;
%%uicontrol('BackGroundColor','y','Units','normal','Position',[.75 .95 .20 .03],'String','Magnitude Signature ','callback','synsig')


%uicontrol('BackGroundColor','c','Units','normal','Position',[.90 .01 .10 .05],'String','Back  ','callback','close, ic = 0; dispma2')

%%set(gca,'box','on',...
%%        'DrawMode','fast','TickDir','out','FontWeight',...
%%        'bold','FontSize',fs10,'Linewidth',[1.2])
 
%set(gca,'Color',[1 1 0.7])
close(bvfig) 
% Find out of figure already exists and plot results of fit
%
[existFlag,figNumber]=figflag('Compare and fit two rates',1);
newCompWindowFlag=~existFlag;
ms3 = 5;

%if newCompWindowFlag,
bvfig= figure( ...
        'Name','Compare and fit two rates',...
        'NumberTitle','off', ...
        'backingstore','on',...
        'Visible','on', ...
        'Position',[ fipo(3)-600 fipo(4)-600 winx winy+200]);
 

uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .93 .08 .06],'String','Print ',...
          'callback','myprint')
 
 callbackStr= ...
        ['f1=gcf; f2=gpf;set(f1,''Visible'',''off'');', ...
         'if f1~=f2, welcome;done; end'];
 
uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .75 .08 .06],'String','Close ',...
          'callback',callbackStr)
 
uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .85 .08 .06],'String','Info ',...
          'callback','zmaphelp(ttlStr,hlpStr1map,hlpStr2map,hlpStr3map)')
axis off
matdraw

%end % if figure exits

%%figure(bvfig)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
% plot b-value plot
%
orient tall
set(gcf,'PaperPosition',[2 1 5.5 7.5])
rect = [0.20,  0.7, 0.70, 0.25];           % plot Freq-Mag curves
axes('position',rect)
hold on
figure(bvfig)
%pl = semilogy(xt2,bvalsum,'om');
%set(pl,'MarkerSize',[ms3])
%semilogy(xt2,bvalsum,'-.m')
hold on
%pl = semilogy(xt2,bvalsum2,'xb');
%set(pl,'MarkerSize',[ms3])
%semilogy(xt2,bvalsum2,'b')
pl = semilogy(xt3,bvalsum4,'xb');
set(gca,'Yscale','log')
hold on
set(pl,'MarkerSize',[ms3])
semilogy(xt3,bvalsum4,'-.b')
pl = semilogy(xt3,bvalsum3N,'om');
set(pl,'MarkerSize',[ms3])
semilogy(xt3,bvalsum3N,'m')
te1 = max([bvalsum  bvalsum2 bvalsum4 bvalsum3]);
te1 = te1 - 0.2*te1;

%xlabel2('Magnitude','FontSize',fs10,'FontWeight','bold')
ylabel2('Cum. rate/year','FontSize',fs10,'FontWeight','bold')
%title([file1 '   o: ' num2str(t1p(1),6) ' - ' num2str(t2p(1),6) '     x: ' num2str(t3p(1),6) ' - '  num2str(t4p(1),6) ],'FontSize',fs10,'FontWeight','bold','Color','k')
str = [ '   o: ' num2str(t1p(1),6) ' - ' num2str(t2p(1),4) '     x: ' num2str(t3p(1),6) ' - '  num2str(t4p(1),6) ];

title(str,'FontSize',fs10,'FontWeight','bold')
                                             %  find b-values;
set(gca,'box','on',...
        'DrawMode','fast','TickDir','out','FontWeight',...
        'bold','FontSize',fs10,'Linewidth',[1.0])
p1 = gca;


% Plot histogram 
%
%set(gca,'Color',[cb1 cb2 cb3])

rect = [0.20,  0.40 0.70, 0.25];
axes('position',rect)
pl = plot(xt2,bvalN,'om');
set(pl,'MarkerSize',[ms3],'LineWidth',[1.0])
hold on
pl = plot(xt2,bval2,'xb');
set(pl,'MarkerSize',[ms3],'LineWidth',[1.0])
pl = plot(xt2,bval2,'-.b');
set(pl,'MarkerSize',[ms3],'LineWidth',[1.0])
pl = plot(xt2,bvalN,'m');
set(pl,'MarkerSize',[ms3],'LineWidth',[1.0])
display([' Summation: ' num2str(sum(bval-bval2))])
%bar(xt2,bval,'om')
%bar(xt2,bval,'-.m')
%bar(xt2,bval2,'b')
v = axis;
xlabel2('Magnitude ','FontSize',fs10,'FontWeight','bold')
ylabel2('rate/year','FontSize',fs10,'FontWeight','bold')
set(gca,'box','on',...
        'DrawMode','fast','TickDir','out','FontWeight',...
        'bold','FontSize',fs10,'Linewidth',[1.0])

uic = uicontrol('BackGroundColor','y','Units','normal','Position',[.35 .15 .30 .07],'String','Magnitude Signature? ','callback','delete(uic);synsig');

end   % if ic 

%clear l ll backg_ab foreg_ab backg_be foreg_be  backg_beN backg_abN  mean1 %mean2 cumux cumux2 bvalNN ld p std_backg min_backg ldb n1 std_foreg min_foreg %fac fac1 dM mf rat 
clear rat bvalNN mean1 mean2 ld l ll txt1 txt2 txt3 txt4 M1b M2b M1f M2f tx1 tx2 tx3 tx4;                          
ic = 0;
format;
%whitebg(mess);
%%watchoff;watchoff(mess)


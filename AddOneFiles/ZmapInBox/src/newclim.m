function CLim = newclim(BeginSlot,EndSlot,CDmin,CDmax,CmLength) 

disp('This is /src/newclim.m');

PBeginSlot = (BeginSlot - 1) / (CmLength - 1); 
PEndSlot = (EndSlot - 1) / (CmLength - 1); 
PCmRange = PEndSlot - PBeginSlot; 
DataRange = CDmax - CDmin; 
ClimRange = DataRange / PCmRange; 
NewCmin = CDmin - (PBeginSlot * ClimRange); 
NewCmax = CDmax + (1 - PEndSlot) * ClimRange; 
CLim = [NewCmin,NewCmax]; 

%  earthquake_depth.m
% August 95 by Zhong Lu

disp('This is /src/eq_dep.m');

[existFlag,figNumber]=figflag('Depth vs Earthquake Number',1);

newWindowFlag=~existFlag;

if newWindowFlag,
  mif66 = figure( ...
        'Name','Depth vs Earthquake Number',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'NextPlot','add', ...
        'Visible','off', ...
        'Position',[ fipo(3)-300 fipo(4)-500 winx winy]);
 
  makebutt
  matdraw
  hold on

end
figure(mif66)
hold on

x = [1:length(mi)]';
[ss,ssi]=sort(a(:,7));
plot(x,ss,'go');

grid
%set(gca,'box','on',...
%        'DrawMode','fast','TickDir','out','FontWeight',...
%        'bold','FontSize',fs12,'Linewidth',[1.2]);

ylabel2('Depth of Earthquake','FontWeight','bold','FontSize',fs12);
xlabel2('Earthquake Number','FontWeight','bold','FontSize',fs12);
hold off;

done

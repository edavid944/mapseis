disp('This is /src/conhypoe.m');

s = int2str(test(:,1));
s =reshape(s',6,length(s)/6);
yr = str2num(s(1:2,:)');
mo = str2num(s(3:4,:)');
da = str2num(s(5:6,:)');

hr = floor(test(:,2)/100);
min= test(:,2) - hr*100;

a = [ test(:,5) test(:,4) yr mo da test(:,7) test(:,6) hr min];
clear s yr mo da hr min

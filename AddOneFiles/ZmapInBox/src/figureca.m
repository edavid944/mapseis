function FigureCallback(command,item)
% This function is a callback for makemenus.
% It should not be called from the command line.
%
% Keith Rogers 11/30/93

disp('This is /src/figureca.m');

if (command == 1)
	if(strcmp(get(gcm2,'Label'),'Landscape'))
		set(gcf,'PaperOrientation','landscape');
		set(gcm2,'Label','Portrait');
	else
		set(gcf,'PaperOrientation','portrait');
		set(gcm2,'Label','Landscape');
	end
end	

%  misfit_magnitude
% August 95 by Zhong Lu

disp('This is /src/mi_dep.m');

[existFlag,figNumber]=figflag('Misfit as a Function of Depth',1);

newWindowFlag=~existFlag;

if newWindowFlag,
  mif77 = figure( ...
        'Name','Misfit as a Function of Depth',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'NextPlot','add', ...
        'Visible','off', ...
        'Position',[ fipo(3)-300 fipo(4)-500 winx winy]);
 
  makebutt
  matdraw
  hold on

end
figure(mif77)
hold on


plot(a(:,7),mi(:,2),'go');

grid
%set(gca,'box','on',...
%        'DrawMode','fast','TickDir','out','FontWeight',...
%        'bold','FontSize',fs12,'Linewidth',[1.2]);

xlabel2('Depth of Earthquake','FontWeight','bold','FontSize',fs12);
ylabel2('Misfit Angle ','FontWeight','bold','FontSize',fs12);
hold off;

done

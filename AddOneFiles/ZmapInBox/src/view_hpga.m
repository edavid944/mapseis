% This .m file "view_maxz.m" plots the maxz LTA values calculated 
% with maxzlta.m or other similar values as a color map 
% needs re3, gx, gy, stri
%
% define size of the plot etc. 
% 

if exist('Prmap')  == 0 
    Prmap = re3*nan;
end
if isempty(Prmap) >  0 
    Prmap = re3*nan;
end

if isempty(name) >  0 
    name = '  '
end
think
disp('This is /src/view_hpga.m')
%co = 'w';


% Find out of figure already exists
%
[existFlag,figNumber]=figflag('Hazard-map',1);
newhazmapWindowFlag=~existFlag;


% Set up the Seismicity Map window Enviroment
%
if newhazmapWindowFlag,
    hmap = figure( ...
        'Name','Hazard-map',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'NextPlot','new', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
    % make menu bar
    matdraw
    
    lab1 = 'HPGA - value:';
    hmap = gcf;
    
    symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
    SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
    TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
    ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
    
    uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
    uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
    uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
    uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
    uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
    uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
    uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');
    
    uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
    uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
    uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
    uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
    uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
    uimenu(TypeMenu,'Label','none','Callback','vi=''off'';set(ploeq,''visible'',''off''); ');
    
    uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''r'';eval(cal9)');
    uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
    
    cal9 = ...
        [ 'vi=''on'';set(ploeq,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];
    
  
    options = uimenu('Label',' Select ','BackgroundColor','y');
    uimenu(options,'Label','Refresh ','callback','view_hpga')
      uimenu(options,'Label','Compute hazard curve at one location',...
        'callback','watchon ; newhazfig = 0; ;hazcurve;watchoff  ')
      uimenu(options,'Label','Compute hazard curve at one location (overlay existing graph)',...
        'callback','watchon ; newhazfig = 1; ;hazcurve;watchoff  ')
    
    
    % Map menu
    op1 = uimenu('Label',' Maps ','BackgroundColor','y');
    
    uimenu(op1,'Label','Re-compute map with new parameters ',...
        'callback','dohaz=''in1'';hazmap')
    
   
    
    % Display menu
    op2e = uimenu('Label',' Display ','BackgroundColor','y');
    uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
    uimenu(op2e,'Label','Plot Map in lambert projection using m_map ','callback','plotmap ')
     uimenu(op2e,'Label','Plot map on top of topography (white background)',...
        'callback','colback = 1; dramap2_z')
    uimenu(op2e,'Label','Plot map on top of topography (black background)',...
        'callback','colback = 2; dramap2_z')
    uimenu(op2e,'Label','Show Grid ',...
        'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
    uimenu(op2e,'Label','Show Circles ','callback','plotci2')
    uimenu(op2e,'Label','Colormap InvertGray',...
        'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
    uimenu(op2e,'Label','Colormap Invertjet',...
        'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
    uimenu(op2e,'Label','shading flat',...
        'callback','axes(hzma); shading flat;sha=''fl'';')
    uimenu(op2e,'Label','shading interpolated',...
        'callback','axes(hzma); shading interp;sha=''in'';')
    uimenu(op2e,'Label','Brigten +0.4',...
        'callback','axes(hzma); brighten(0.4)')
    uimenu(op2e,'Label','Brigten -0.4',...
        'callback','axes(hzma); brighten(-0.4)')
    uimenu(op2e,'Label','Redraw Overlay',...
        'callback','hold on;overlay')
    
    tresh = nan; re4 = re3;
    
    colormap(jet)
    tresh = nan; minpe = nan; Mmin = nan;    
    
end   % This is the end of the figure setup

% Now lets plot the color-map of the z-value
%
figure(hmap)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','drawmode','fast')

rect = [0.18,  0.10, 0.7, 0.75];
rect1 = rect;

% find max and min of data for automatic scaling
% 
maxc = max(max(re3));
maxc = fix(maxc)+1;
minc = min(min(re3));
minc = fix(minc)-1;

% set values gretaer tresh = nan
%
re4 = re3;
l = r > tresh; 
re4(l) = zeros(1,length(find(l)))*nan;
l = Prmap < minpe; 
re4(l) = zeros(1,length(find(l)))*nan;
l = old1 <  Mmin; 
re4(l) = zeros(1,length(find(l)))*nan;

% plot image
% 
orient landscape
%set(gcf,'PaperPosition', [0.5 1 9.0 4.0])

axes('position',rect)
hold on
pco1 = pcolor(gx,gy,re4);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on
if sha == 'fl'
    shading flat
else
    shading interp
end
% make the scaling for the recurrence time map reasonable
if lab1(1) =='T'
    l = isnan(re3);
    re = re3;
    re(l) = [];
    caxis([min(re) 5*min(re)]);
end
if fre == 1
    caxis([fix1 fix2])
end


title2([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
    'Color','r','FontWeight','bold')

xlabel2('Longitude [deg]','FontWeight','bold','FontSize',fs10)
ylabel2('Latitude [deg]','FontWeight','bold','FontSize',fs10)

% plot overlay
% 
hold on
overlay_
ploeq = plot(a(:,1),a(:,2),'k.');
set(ploeq,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)

set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
h5 = colorbar('horiz');
set(h5,'Pos',[0.35 0.05 0.4 0.02],...
    'FontWeight','bold','FontSize',fs10,'TickDir','out')

rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')
%  Text Object Creation 
txt1 = text(... 
    'Color',[ 0 0 0 ],... 
    'EraseMode','normal',... 
    'Units','normalized',...
    'Position',[ 0.33 0.06 0 ],...
    'HorizontalAlignment','right',...
    'Rotation',[ 0 ],...
    'FontSize',fs10,.... 
    'FontWeight','bold',...
    'String',lab1); 

% Make the figure visible
% 
set(gca,'FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
figure(hmap);
%sizmap = signatur('ZMAP','',[0.01 0.04]);
%set(sizmap,'Color','k')
axes(h1)
watchoff(hmap)
%whitebg(gcf,[ 0 0 0 ])
done

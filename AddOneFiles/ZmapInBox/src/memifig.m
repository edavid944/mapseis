disp('This is /src/memifig.m');


%input window
%
%default parameters

%make a color map
% Find out of figure already exists
%
[existFlag,figNumber]=figflag('Misfit-Map 2',1);
newlapWindowFlag=~existFlag;
% Set up the Seismicity Map window Enviroment
%
if newlapWindowFlag,
  mifmap = figure( ...
        'Name','Misfit-Map 2',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'NextPlot','replace', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ 600 400 500 350]);
% make menu bar
matdraw
makebutt

hold on 
end

[existFlag,mifmap]=figflag('Misfit-Map 2',1);
figure(mifmap)

delete(gca);delete(gca); delete(gca);delete(gca);
delete(gca);delete(gca); delete(gca);delete(gca);

set(gca,'visible','off','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','drawmode','fast')

%minimum and maximum of normlap2 for automatic scaling
maxc = max(normlap2);
minc = min(normlap2);

%construct a matrix for the color plot
normlap1=ones(length(tmpgri(:,1)),1);
normlap2=ones(length(tmpgri(:,1)),1)*nan;
normlap3=ones(length(tmpgri(:,1)),1)*nan;
normlap1(ll)=me1;
normlap2(ll)=normlap1(ll);
normlap1(ll)=va1;
normlap3(ll)=normlap1(ll);

normlap2=reshape(normlap2,length(yvect),length(xvect));
normlap3=reshape(normlap3,length(yvect),length(xvect));

%plot color image
orient tall
gx = xvect; gy = yvect;

hold on
pco1 = pcolor(xvect,yvect,normlap2);
shading interp
j = jet(64);
j = j(64:-1:1,:);
colormap(j);
%colormap(gray(10))
%brighten(0.7)
%caxis([3 10])
%axis([ s2 s1 s4 s3])
axis([ min(gx) max(gx) min(gy) max(gy)])
axis image

hold on
 h5 = colorbar('vert');
set(h5,'Pos',[0.82 0.46 0.03 0.10],...
      'FontSize',[2])
 


if exist('maex') > 0
 hold on
 pl = plot(maex,-maey,'*w');
 set(pl,'MarkerSize',[6],'LineWidth',[1])
end

%overlay
title('Mean of the Misfit','FontWeight','bold','FontSize',fs10)
xlabel2('Distance in [km]','FontWeight','bold','FontSize',fs10)
ylabel2('Depth in [km]','FontWeight','bold','FontSize',fs10)

set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')



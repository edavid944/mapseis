%  plot a circle containing ni events
%  around each grid point

disp('This is /src/plotcirc.m');

st = 2;
[X,Y] = meshgrid(gx,gy);
[m,n]= size(r);
hold on
 x = -pi-0.1:0.1:pi;
for i = 1:st:m
 for k = 1:st:n
  if r(i,k) <= tresh;
   plot(X(i,k)+r(i,k)*sin(x),Y(i,k)+r(i,k)*cos(x),'b')
   plot(X(i,k),Y(i,k),'+k')
  end
 end
end

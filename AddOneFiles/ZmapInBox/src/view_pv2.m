% This .m file "view_x
% maxz.m" plots the maxz LTA values calculated 
% with maxzlta.m or other similar values as a color map 
% needs re3, gx, gy, stri
%
% define size of the plot etc. 
% 
if isempty(name) >  0 
 name = '  '
end
think
 disp('This is /src/view_pv2.m')
co = 'w';


% Find out of figure already exists
%
[existFlag,figNumber]=figflag('p-value cross-section',1);
newpmapcWindowFlag=~existFlag;
 
% This is the info window text
%
   ttlStr='The Z-Value Map Window                        ';
    hlpStr1zmap= ...
        ['                                                '
         ' This window displays seismicity rate changes   '
         ' as z-values using a color code. Negative       '
         ' z-values indicate an increase in the seismicity'
         ' rate, positive values a decrease.              '
         ' Some of the menu-bar options are               '
         ' described below:                               '
         '                                                '
         ' Threshold: You can set the maximum size that   '
         '   a volume is allowed to have in order to be   '
         '   displayed in the map. Therefore, areas with  '
         '   a low seismicity rate are not displayed.     '
         '   edit the size (in km) and click the mouse    '
         '   outside the edit window.                     '
         'FixAx: You can chose the minimum and maximum    '
         '        values of the color-legend used.        '
         'Polygon: You can select earthquakes in a        '
         ' polygon either by entering the coordinates or  ' 
         ' defining the corners with the mouse            '];                                         
 hlpStr2zmap= ...
        ['                                                '
         'Circle: Select earthquakes in a circular volume:'
         '      Ni, the number of selected earthquakes can'
         '      be edited in the upper right corner of the'
         '      window.                                   '
         ' Refresh Window: Redraws the figure, erases     '
         '       selected events.                         '

         ' zoom: Selecting Axis -> zoom on allows you to  '
         '       zoom into a region. Click and drag with  '
         '       the left mouse button. type <help zoom>  ' 
         '       for details.                             '
         ' Aspect: select one of the aspect ratio options '
         ' Text: You can select text items by clicking.The'
         '       selected text can be rotated, moved, you '
         '       can change the font size etc.            '
         '       Double click on text allows editing it.  '        
         '                                                '
         '                                                '];                                        

% Set up the Seismicity Map window Enviroment
%
if newpmapcWindowFlag,
  pmapc = figure( ...
        'Name','p-value cross-section',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
% make menu bar
matdraw
lab1 = 'p-value';
 
symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');

uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');

uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
uimenu(TypeMenu,'Label','none','Callback','vi = ''off'';set(ploeqc,''visible'',''off''); ');

uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
uimenu(ColorMenu,'Label','red','Callback','co=''r'';eval(cal9)');
uimenu(ColorMenu,'Label','blue','Callback','co=''b'';eval(cal9)');
uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
  
cal9 = ...
[ 'vi=''on'';set(ploeqc,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];

   
         
uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .93 .08 .06],'String','Print ',...
          'callback','myprint')
 
 callbackStr= ...
        ['f1=gcf; f2=gpf; set(f1,''Visible'',''off'');close(pmapc);', ...
         'if f1~=f2, figure(map);done; end'];
 
uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .75 .08 .06],'String','Close ',...
          'callback','eval(callbackStr)')
 
uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .85 .08 .06],'String','Info ',...
          'callback','zmaphelp(ttlStr,hlpStr1zmap,hlpStr2zmap)')

options = uimenu('Label',' Select-b ','BackgroundColor','y');
  uimenu(options,'Label','Select EQ in Circle (const N)',...
      'callback',' h1 = gca;ho = ''noho'';ic = 1;cicros;')
  uimenu(options,'Label','Select EQ in Circle (const R)',...
      'callback',' h1 = gca;ho = ''noho'';ic = 2;cicros;')
  uimenu(options,'Label','Select EQ in Circle - Overlay existing plot',...
       'callback','h1 = gca;ho = ''hold'';cicros;')
  uimenu(options,'Label','Select Eqs in Polygon - new',...
          'callback','ho = ''noho'';polyb;');
  uimenu(options,'Label','Select Eqs in Polygon - hold',...
          'callback','ho = ''hold'';polyb;');


  options = uimenu('Label',' Select-p ','BackgroundColor','y');
  uimenu(options,'Label','Refresh ','callback','view_pv2')
  uimenu(options,'Label','Select EQ in Circle (const N)',...
      'callback',' h1 = gca;ho2 = ''noho'';ic = 1;cicros2;')
  uimenu(options,'Label','Select EQ in Circle - Overlay existing plot',...
       'callback','h1 = gca;ho2 = ''hold'';cicros2;')
 
op1 = uimenu('Label',' Maps ','BackgroundColor','y');
 uimenu(op1,'Label',' 7 day aftyershock Probability  Map ',...
        'callback','lab1=''P(M>Mm-1, 7 days)''; re3 = Pv; view_pv2')
 uimenu(op1,'Label','b-value Map (weighted LS)',...
        'callback','lab1=''b-value''; re3 = bv; view_pv2')
 uimenu(op1,'Label','p-value map',...
         'callback',' lab1=''p-value'';re3 = old; view_pv2')
 uimenu(op1,'Label','Slip-value map',...
         'callback',' lab1=''slip-value'';re3 = sl2; view_pv2')
makert2 = ...
['m = input(''Magnitude of projected mainshock? (e.g.6)'');', ...
 'lab1 = ''Tr in yrs. (only smallest values shown)'';',...
 're3 =(teb - t0b)./(10.^(avm-m*old)); view_pv2']; 

 uimenu(op1,'Label','recurrence time map ',...
         'callback',makert2) 

 uimenu(op1,'Label','resolution Map',...
         'callback','lab1=''Radius in [km]'';re3 = r; view_pv2')
 uimenu(op1,'Label','Histogram ','callback','zhist')

op2e = uimenu('Label',' Display ','BackgroundColor','y');
 uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
 uimenu(op2e,'Label','Show Grid ',...
    'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
 uimenu(op2e,'Label','Show Circles ','callback','plotci3')
 uimenu(op2e,'Label','Colormap InvertGray',...
      'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
 uimenu(op2e,'Label','Colormap Invertjet',...
      'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
 uimenu(op2e,'Label','shading flat',...
        'callback','axes(hzma); shading flat;sha=''fl'';')
 uimenu(op2e,'Label','shading interpolated',...
        'callback','axes(hzma); shading interp;sha=''in'';')
 uimenu(op2e,'Label','Brigten +0.4',...
        'callback','axes(hzma); brighten(0.4)')
 uimenu(op2e,'Label','Brigten -0.4',...
        'callback','axes(hzma); brighten(-0.4)')
 uimenu(op2e,'Label','Redraw Overlay',...
        'callback','hold on;overlay_')

uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.92 .80 .08 .05],'String','set ni',...
          'callback','ni=str2num(get(set_nia,''String''));''String'',num2str(ni);')
 

set_nia = uicontrol('style','edit','value',ni,'string',num2str(ni));
set(set_nia,'CallBack',' ');
set(set_nia,'units','norm','pos',[.94 .85 .06 .05],'min',10,'max',10000);
nilabel = uicontrol('style','text','units','norm','pos',[.90 .85 .04 .05]);
set(nilabel,'string','ni:','background',[.7 .7 .7]);

% tx = text(0.07,0.95,[name],'Units','Norm','FontSize',[18],'Color','k','FontWeight','bold');

 tresh = max(max(r)); re4 = re3;
nilabel2 = uicontrol('style','text','units','norm','pos',[.60 .92 .25 .06]);
set(nilabel2,'string','MinRad (in km):','background',[c1 c2 c3]);
set_ni2 = uicontrol('style','edit','value',tresh,'string',num2str(tresh),...
          'background','y');
set(set_ni2,'CallBack','tresh=str2num(get(set_ni2,''String'')); set(set_ni2,''String'',num2str(tresh))');
set(set_ni2,'units','norm','pos',[.85 .92 .08 .06],'min',0.01,'max',10000);

uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.95 .93 .05 .05],'String','Go ',...
          'callback','think;pause(1);re4 =re3; view_pv2')

  colormap(jet)
 end   % This is the end of the figure setup

% Now lets plot the color-map of the z-value
%
figure(pmapc)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','drawmode','fast')

rect = [0.18,  0.10, 0.7, 0.75];
rect1 = rect;
 
% set values greater tresh = nan
%
re4 = re3;
l = r > tresh;
re4(l) = zeros(1,length(find(l)))*nan;

%l = re4 > min(bvgr(:,1)) &  re4 < max(bvgr(:,1)) ;
%l = re4 > mean(bvgr(:,1))-2*std(bvgr(:,1)) &  re4 <  mean(bvgr(:,1))+2*std(bvgr(:,1));
%re4(l) = zeros(1,length(find(l)))*nan;
%re4(l) = zeros(1,length(find(l)))+ mean(bvgr(:,1));
 
% plot image
% 
orient portrait
%set(gcf,'PaperPosition', [2. 1 7.0 5.0])

axes('position',rect)
hold on
pco1 = pcolor(gx,gy,re4);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on
if sha == 'fl'
  shading flat
else
  shading interp
end

if exist('pro') > 0
  %l = pro < 0;
  %pro2 = pro;
  %pro2(l) = pro2(l)*nan;
  %cs =contour(gx,gy,pro,[95 99.9],'k');
  %cs =contour(gx,gy,pro,[ 99 100],'w--');
  % clabel(cs)
  %l = pro > 0;
  %pro2 = pro;
  %pro2(l) = pro2(l)*nan;
  %cs =contour(gx,gy,pro2,[-95 -99 -99.9],'w');
  %clabel(cs)
end % if exist pro
if term == 1 
 colormap(gray)
else
% h = hsv(64);
 %h = h(57:-1:1,:);
 %colormap(jet)
end

% make the scaling for the recurrence time map reasonable
if lab1(1) =='T'
  fre = 0;
  l = isnan(re3);
  re = re3;
  re(l) = [];
  caxis([min(re) 5*min(re)]);
end 
if fre == 1
 caxis([fix1 fix2])
end

title2([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
             'Color','r','FontWeight','bold')

xlabel2('Distance in [km]','FontWeight','bold','FontSize',fs10)
ylabel2('depth in [km]','FontWeight','bold','FontSize',fs10)

% plot overlay
% 
 ploeqc = plot(newa(:,length(newa(1,:))),-newa(:,7),'.w');
 set(ploeqc,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)

if exist('vox') > 0
 plovo = plot(vox,voy,'*b');
 set(plovo,'MarkerSize',[6],'LineWidth',[1])
end

if exist('maix') > 0
 pl = plot(maix,maiy,'*k');
 set(pl,'MarkerSize',[12],'LineWidth',[2])
end

if exist('maex') > 0
 pl = plot(maex,-maey,'hm');
 set(pl,'LineWidth',[1.5],'MarkerSize',[12],...
   'MarkerFaceColor','w','MarkerEdgeColor','k')

end



set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
h5 = colorbar('horz');
set(h5,'Pos',[0.35 0.05 0.4 0.04],...
       'FontWeight','bold','FontSize',fs10)
 
rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')
	%  Text Object Creation 
	txt1 = text(... 
		'Color',[ 0 0 0 ],... 
		'EraseMode','normal',... 
		'Position',[ 0.33 0.07 0 ],... 
		'HorizontalAlignment','right',...
		'Rotation',[ 0 ],...
                 'FontSize',fs10,.... 
                 'FontWeight','bold',...
		'String',lab1); 

% Make the figure visible
% 
axes(h1)
set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
figure(pmapc);
whitebg(gcf,[0 0 0])
watchoff(pmapc)
done

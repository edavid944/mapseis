% This .m file "view_maxz.m" plots the maxz LTA values calculated 
% with maxzlta.m or other similar values as a color map 
% needs re3, gx, gy, stri
%
% define size of the plot etc. 
% 
if isempty(name) >  0 
   name = '  '
end
think
disp('This is /src/view_qva.m')
%co = 'w';


% Find out of figure already exists
%
[existFlag,figNumber]=figflag('q-detect-map',1);
newbmapWindowFlag=~existFlag;

% This is the info window text
%
ttlStr='The Z-Value Map Window                        ';
hlpStr1zmap= ...
   ['                                                '
   ' This window displays seismicity rate changes   '
   ' as z-values using a color code. Negative       '
   ' z-values indicate an increase in the seismicity'
   ' rate, positive values a decrease.              '
   ' Some of the menu-bar options are               '
   ' described below:                               '
   '                                                '
   ' Threshold: You can set the maximum size that   '
   '   a volume is allowed to have in order to be   '
   '   displayed in the map. Therefore, areas with  '
   '   a low seismicity rate are not displayed.     '
   '   edit the size (in km) and click the mouse    '
   '   outside the edit window.                     '
   'FixAx: You can chose the minimum and maximum    '
   '        values of the color-legend used.        '
   'Polygon: You can select earthquakes in a        '
   ' polygon either by entering the coordinates or  ' 
   ' defining the corners with the mouse            '];                                         
hlpStr2zmap= ...
   ['                                                '
   'Circle: Select earthquakes in a circular volume:'
   '      Ni, the number of selected earthquakes can'
   '      be edited in the upper right corner of the'
   '      window.                                   '
   ' Refresh Window: Redraws the figure, erases     '
   '       selected events.                         '
   
   ' zoom: Selecting Axis -> zoom on allows you to  '
   '       zoom into a region. Click and drag with  '
   '       the left mouse button. type <help zoom>  ' 
   '       for details.                             '
   ' Aspect: select one of the aspect ratio options '
   ' Text: You can select text items by clicking.The'
   '       selected text can be rotated, moved, you '
   '       can change the font size etc.            '
   '       Double click on text allows editing it.  '        
   '                                                '
   '                                                '];                                        

% Set up the Seismicity Map window Enviroment
%
if newbmapWindowFlag,
   qmap = figure( ...
      'Name','q-detect-map',...
      'NumberTitle','off', ...
      'MenuBar','none', ...
      'NextPlot','new', ...
      'backingstore','on',...
      'Visible','off', ...
      'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
   % make menu bar
   matdraw
   
   lab1 = 'day/night ratio';
   
   symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
   SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
   TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
   ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
   
   uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
   uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
   uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
   uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
   uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
   uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
   uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');
   
   uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
   uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
   uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
   uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
   uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
   uimenu(TypeMenu,'Label','none','Callback','vi=''off'';set(ploeq,''visible'',''off''); ');
   
   uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
   uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
   uimenu(ColorMenu,'Label','white','Callback','co=''r'';eval(cal9)');
   uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
   
   cal9 = ...
      [ 'vi=''on'';set(ploeq,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];
   
   
   uicontrol('BackGroundColor','w','Units','normal',... 
      'Position',[.0 .93 .08 .06],'String','Info ',...
      'callback',' web([''file:'' hodi ''/help/quarry.htm'']) '); 
   
   
   
   options = uimenu('Label',' Select ','BackgroundColor','y');
   uimenu(options,'Label','Refresh ','callback','delete(gca);delete(gca);delete(gca);delete(gca); view_qva')
   uimenu(options,'Label','Select EQ in Circle','callback','h1 = gca;circle;watchoff(qmap);global histo;hisgra(newt2(:,8),''Hr '');')
   uimenu(options,'Label','Select EQ in Polygon ','callback',' stri = ''Polygon'';h1 = gca;cufi = gcf;selectp; global histo;hisgra(newt2(:,8),''Hr '');')  
   
   op1 = uimenu('Label',' Maps ','BackgroundColor','y');
   uimenu(op1,'Label','day/night value map',...
      'callback','lab1 =''day/night ratio''; re3 = old; view_qva')
   
   
   op2e = uimenu('Label',' Display ','BackgroundColor','y');
   uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
   uimenu(op2e,'Label','Plot Map in Lambert Projection using m_map ','callback','plotmap ')
   uimenu(op2e,'Label','Show Grid ',...
      'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
   uimenu(op2e,'Label','Show Circles ','callback','plotci2')
   uimenu(op2e,'Label','Colormap InvertGray',...
      'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
   uimenu(op2e,'Label','Colormap Invertjet',...
      'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
   uimenu(op2e,'Label','shading flat',...
      'callback','axes(hzma); shading flat;sha=''fl'';')
   uimenu(op2e,'Label','shading interpolated',...
      'callback','axes(hzma); shading interp;sha=''in'';')
   uimenu(op2e,'Label','Brigten +0.4',...
      'callback','axes(hzma); brighten(0.4)')
   uimenu(op2e,'Label','Brigten -0.4',...
      'callback','axes(hzma); brighten(-0.4)')
   uimenu(op2e,'Label','Redraw Overlay',...
      'callback','hold on;overlay')
   
   
   uicontrol('BackGroundColor','y','Units','normal',...
      'Position',[.92 .80 .08 .05],'String','set ni',...
      'callback','ni=str2num(get(set_nia,''String''));''String'',num2str(ni);')
   
   
   set_nia = uicontrol('style','edit','value',ni,'string',num2str(ni));
   set(set_nia,'CallBack',' ');
   set(set_nia,'units','norm','pos',[.94 .85 .06 .05],'min',10,'max',10000);
   nilabel = uicontrol('style','text','units','norm','pos',[.90 .85 .04 .05]);
   set(nilabel,'string','ni:','background',[.7 .7 .7]);
   
   % tx = text(0.07,0.95,[name],'Units','Norm','FontSize',[18],'Color','k','FontWeight','bold');
   
   tresh = nan; re4 = re3;
   nilabel2 = uicontrol('style','text','units','norm','pos',[.60 .92 .25 .06]);
   set(nilabel2,'string','MinRad (in km):','background',[c1 c2 c3]);
   set_ni2 = uicontrol('style','edit','value',tresh,'string',num2str(tresh),...
      'background','y');
   set(set_ni2,'CallBack','tresh=str2num(get(set_ni2,''String'')); set(set_ni2,''String'',num2str(tresh))');
   set(set_ni2,'units','norm','pos',[.85 .92 .08 .06],'min',0.01,'max',10000);
   
   uicontrol('BackGroundColor','y','Units','normal',...
      'Position',[.95 .93 .05 .05],'String','Go ',...
      'callback','think;pause(1);re4 =re3; view_bva')
   
   colormap(cool)
   
end   % This is the end of the figure setup

% Now lets plot the color-map of the z-value
%
figure(qmap)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs10,'FontWeight','bold',...
   'FontWeight','bold','LineWidth',[1.5],...
   'Box','on','drawmode','fast')

rect = [0.18,  0.10, 0.7, 0.75];
rect1 = rect;

% find max and min of data for automatic scaling
% 
maxc = max(max(re3));
maxc = fix(maxc)+1;
minc = min(min(re3));
minc = fix(minc)-1;

% set values gretaer tresh = nan
%
re4 = re3;
l = r > tresh;
re4(l) = zeros(1,length(find(l)))*nan;

% plot image
% 
orient landscape
%set(gcf,'PaperPosition', [0.5 1 9.0 4.0])

axes('position',rect)
hold on
pco1 = pcolor(gx,gy,re4);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on
if sha == 'fl'
   shading flat
else
   shading interp
end

if fre == 1
   caxis([fix1 fix2])
end


title2([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
   'Color','r','FontWeight','bold')

xlabel2('Longitude [deg]','FontWeight','bold','FontSize',fs10)
ylabel2('Latitude [deg]','FontWeight','bold','FontSize',fs10)

% plot overlay
% 
hold on
overlay_
ploeq = plot(a(:,1),a(:,2),'k.');
set(ploeq,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)



set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
   'FontWeight','bold','LineWidth',[1.5],...
   'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
h5 = colorbar('horiz');
set(h5,'Pos',[0.35 0.05 0.4 0.02],...
   'FontWeight','bold','FontSize',fs10)

rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')
%  Text Object Creation 
txt1 = text(... 
   'Color',[ 0 0 0 ],... 
   'EraseMode','normal',... 
   'Units','normalized',...
   'Position',[ 0.33 0.07 0 ],...
   'HorizontalAlignment','right',...
   'Rotation',[ 0 ],...
   'FontSize',fs10,.... 
   'FontWeight','bold',...
   'String',lab1); 

% Make the figure visible
% 
set(gca,'FontSize',fs10,'FontWeight','bold',...
   'FontWeight','bold','LineWidth',[1.5],...
   'Box','on','TickDir','out')
figure(qmap);
axes(h1)
watchoff(qmap)
whitebg(gcf,[ 0 0 0 ])
done

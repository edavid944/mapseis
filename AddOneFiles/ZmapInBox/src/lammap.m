% This is  the m file lammap.m. It will display a map view of the 
% seismicity in Lambert projection and ask for two input 
% points select with the cursor. These input points are 
% the endpoints of the crossection. 
% 
% Stefan Wiemer 2/95
global mapl
disp('This is /src/lammamp.m')
%
% Find out of figure already exists
%
[existFlag,figNumber]=figflag('Seismicity Map (Lambert)',1);
newMapLaWindowFlag=~existFlag;

global h2 xsec_fig newa
% Set up the Seismicity Map window Enviroment
%
if newMapLaWindowFlag,
  mapl = figure( ...
        'Name','Seismicity Map (Lambert)',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
  makebutt
  matdraw
  drawnow
end % if figure exist

figure(mapl)
delete(gca)
delete(gca)
delete(gca)
delete(gca)
if length(coastline) == 0
  coastline = [ a(1,1) a(1,2)]
end
hold on
if length(coastline) > 1
  lc_map(coastline(:,2),coastline(:,1),s3,s4,s1,s2)
  g = get(gca,'Children');
  set(g,'Color','k')
end
hold on
if length(faults) > 10;
  lc_map(faults(:,2),faults(:,1),s3,s4,s1,s2)
end
hold on
if length(mainfault) > 0
  lc_map(mainfault(:,2),mainfault(:,1),s3,s4,s1,s2)
end
lc_event(a(:,2),a(:,1),'.k')
if length(maepi) > 0 
 lc_event(maepi(:,2),maepi(:,1),'xm')
end
if length(main) > 0 
 lc_event(main(:,2),main(:,1),'+b')
end
%title2(strib,'FontWeight','bold',...
             %'FontSize',fs12,'Color','k')

uic = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.05 .00 .40 .06],'String','Select Endpoints with cursor');

 titStr ='Create Crossection                      ';

    messtext= ...
        ['                                                '
         '  Please use the LEFT mouse button              '
         ' to select the two endpoints of the             '
         ' crossection                                    '
];
 
welcome(titStr,messtext);
if term == 1 ; whitebg([0 0 0 ]);end


[xsecx xsecy inde] = mysect(a(:,2)',a(:,1)',a(:,7),wi);

%if length(maepi) > 0
% [maex maey ] = lc_xsec2(maepi(:,2)',maepi(:,1)',maepi(:,7),wi,leng,lat1,lon1,lat2,lon2);
%end

if length(main) > 0
 [maix maiy ] = lc_xsec2(main(:,2)',main(:,1)',main(:,3),wi,leng,lat1,lon1,lat2,lon2);
 maiy = -maiy;
end
delete(uic)

uic3 = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.80 .88 .20 .10],'String','Make Grid',...
           'callback','sel = ''in'';magrcros');

uic4 = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.80 .68 .20 .10],'String','Make b cross ',...
           'callback','sel = ''in'';bcross');
uic5 = uicontrol('BackGroundColor','y','Units','normal',...
          'position',[.8 .48 .2 .1],'String','Select Eqs',...
          'callback','crosssel;newcat=newa;a=newa;subcata;');

figure(mapl)
uic2 = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.70 .92 .30 .06],'String','New selection ?',...
           'callback','delete(uic2),lammap');
set_width = uicontrol('style','edit','value',wi,...
              'string',num2str(wi), 'background','y',...
              'units','norm','pos',[.90 .00 .08 .06],'min',0,'max',10000,...
       'callback','wi=str2num(get(set_width,''String''));');
 
wilabel = uicontrol('style','text','units','norm','pos',[.60 .00 .30 .06]);
set(wilabel,'string','Width in km:','background','y');
if term == 1 ; whitebg([0 0 0 ]);end

% create the selected catalog
%
newa  = a(inde,:);
newa = [ newa xsecx'];
% call the m script that produces a grid 
sel = 'in';
%magrcros

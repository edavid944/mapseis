function ci =  cusum(cat)

disp('This is /src/cusum2.m');

% This function calculates the CUMSUm function (Page 1954).
% 


m  = cat(:,6);
me = mean(m);
i = (1:1:length(m));
ci = cumsum(m)' - i.*me;



function d = degrees(r)
% d = degrees(r)

disp('This is /src/degrees.m');

%
% degrees converts radians to degrees
%
% Argument definitions:
%
% r = a number in radians
d = 180*r/pi;

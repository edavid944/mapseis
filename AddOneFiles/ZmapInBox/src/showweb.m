function showweb(action)
%  M file that starts a Mosaic Browser displaying
%  the HTML version of the ZMAP Users Guide
%  
%  Stefan Wiemer   6/96

%    !netscape /Seis/A/stefan/zmapwww/title.htm  & 

disp('This is /src/showweb.m');
disp('Attempting to open browser - please be patient...');

switch(action)
    
    
case 'new' 
    
    web(['file:' which('IntrotoZMAP6.htm')]);
    s = which('IntrotoZMAP6.htm');
    do = ['web  ' s '   -browser  , web ' s ] ;
    err = [' disp(''Error opening browser  ... please open the file help/IntrotoZMAP6.htm manually'') '];
    eval(do,err),
    
    
case 'stress'
    
    web(['file:' which('stressinversions.htm')]);
    s = which('stressinversions.pdf');
    do = ['web  ' s '   -browser  , web ' s ] ;
    err = [' disp(''Error opening browser  ... please open the file help/stressinversions.pdf manually'') '];
    eval(do,err),
    
case 'data'
    s = [' http://seismo.ethz.ch/staff/stefan/zmap6/help/onlinedata.htm '];
    do = ['web  ' s '   -browser  , web ' s ] ;
    err = [' disp(''Error opening browser  ... please open the file help/onlinedata.htm '') '];
    eval(do,err);
    
case 'fractal'
    s = which('FDH0.htm');
    do = ['web  ' s '   -browser  '] ;
    err = [' disp(''Error opening browser  ... please open the file help/FDH0.htm '') '];
    eval(do,err);
     
case 'explproba'
    s = which('explproba.htm');
    do = ['web  ' s '   -browser  , web ' s ] ;
    err = [' disp(''Error opening browser  ... please open the file help/explproba.htm '') '];
    eval(do,err);
    
 case '3dgrids'
    s = which('3dgrid.htm');
    do = ['web  ' s '   -browser  , web ' s ] ;
    err = [' disp(''Error opening browser  ... please open the file help/3dgrids.htm '') '];
    eval(do,err);
    
 case 'topo'
    s = which('plottopo.htm');
    do = ['web  ' s '   -browser  , web ' s ] ;
    err = [' disp(''Error opening browser  ... please open the file help/plottope.htm '') '];
    eval(do,err);
end


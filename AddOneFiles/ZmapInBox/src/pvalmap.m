% This subroutine assigns creates a grid with 
% spacing dx,dy (in degreees). The size will 
% be selected interactiVELY. The bvalue in each 
% volume around a grid point containing ni earthquakes
% will be calculated as well as the magnitude
% of completness
%   Stefan Wiemer 1/95

disp('This is /src/pvalmap.m');

global no1 bo1 inb1 inb2 ttcat var1

global action_button file1 term wex wey welx wely             %welcome
global mess ccum bgevent equi file1 clust original cluslength newclcat
global backcat ttcat cluscat
global winx winy sys minmag clu te1 fs12 fs10 fs14
global clu1 pyy tmp1 tmp2 tmp3 tmp4 difp
global xt par3 cumu cumu2 mess
global close_p_button pplot
global freq_field1 freq_field2 freq_field3 freq_field4 Go_p_button
global h2 cplot Info_p close_p pplot print_p
global p c dk tt pc loop nn pp nit t err1x err2x ieflag isflag
global cstep pstep tmpcat ts tend eps1 eps2
global sdc sdk sdp cof qp cog aa bb pcheck loopcheck
global ppc cplot cplot2 hndl1
global autop tmeqtime tmvar

if sel == 'in'
  % get the grid parameter
  % initial values
  % 
  dx = 0.02;
  dy = 0.02 ;
  ni = 500;
  
  
def = {num2str(maepi(1,3))};
ni2 = inputdlg('Input Time of Mainshock ?','Input',1,def);
l = ni2{:};
mati = str2num(l);

  % make the interface 
  % 
  figure(...
          'Name','Grid Input Parameter',...
          'NumberTitle','off', ...
          'MenuBar','none', ...
          'NextPlot','new', ...
          'units','points',...
          'Visible','off', ...
          'Position',[ wex+200 wey-200 450 250]);
  axis off
labelList2=['Weighted LS - automatic Mcomp | Weighted LS - no automatic Mcomp '];
  labelPos=[ 0.2 0.7  0.6  0.08];
  hndl2=uicontrol(...
      'Style','popup',...
      'Position',labelPos,...
      'Units','normalized',...
      'String',labelList2,...
      'BackgroundColor','y',...
      'Callback','inb2 =get(hndl2,''Value''); ');
 
 
 
labelList=['Maximum likelihood - automatic Mcomp | Maximum likelihood  - no automatic Mcomp '];
  labelPos=[ 0.2 0.8  0.6  0.08];
  hndl1=uicontrol(...
      'Style','popup',...
      'Position',labelPos,...
      'Units','normalized',...
      'String',labelList,...
      'BackgroundColor','y',...
      'Callback','inb1 =get(hndl1,''Value''); ');

  
  % creates a dialog box to input grid parameters
  %
      freq_field=uicontrol('BackGroundColor','g','Style','edit',...
           'Position',[.60 .50 .22 .10],...
          'Units','normalized','String',num2str(ni),...
          'CallBack','ni=str2num(get(freq_field,''String'')); set(freq_field,''String'',num2str(ni));');
  
      freq_field2=uicontrol('BackGroundColor','g','Style','edit',...
          'Position',[.60 .40 .22 .10],...
          'Units','normalized','String',num2str(dx),...
          'CallBack','dx=str2num(get(freq_field2,''String'')); set(freq_field2,''String'',num2str(dx));');
  
      freq_field3=uicontrol('BackGroundColor','g','Style','edit',...
          'Position',[.60 .30 .22 .10],...
          'Units','normalized','String',num2str(dy),...
          'CallBack','dy=str2num(get(freq_field3,''String'')); set(freq_field3,''String'',num2str(dy));');
  
      close_button=uicontrol('BackGroundColor','y','Style','Pushbutton',...
          'Position',[.60 .05 .15 .12 ],...
          'Units','normalized','Callback','close;done','String','Cancel');
  
      go_button1=uicontrol('BackGroundColor','y','Style','Pushbutton',...
          'Position',[.20 .05 .15 .12 ],...
          'Units','normalized',...
          'Callback','inb1 =get(hndl1,''Value'');inb2 =get(hndl2,''Value'');close,sel =''ca'', pvalmap',...
          'String','Go');
  
 text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0.20 1.0 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs14 ,...
                  'FontWeight','bold',...
                  'String','Automatically estimate magn. of completeness?   ');
    txt3 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0.30 0.64 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs14 ,...
                  'FontWeight','bold',... 
                  'String',' Grid Parameter');
    txt5 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0. 0.42 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs12 ,...
                  'FontWeight','bold',... 
                  'String','Spacing in x (dx) in deg:');
  
    txt6 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0. 0.32 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs12 ,...
                  'FontWeight','bold',... 
                  'String','Spacing in y (dy) in deg:');
  
    txt1 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0. 0.53 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs12,...
                  'FontWeight','bold',...
                  'String','Number of Events (Ni):');
    set(gcf,'visible','on');
    watchoff
  
end   % if nargin ==0
 
% get the grid-size interactively and 
% calculate the b-value in the grid by sorting 
% thge seimicity and selectiong the ni neighbors
% to each grid point

if sel == 'ca'
  selgp
  itotal = length(newgri(:,1));
  welcome(' ','Running... ');think
  %  make grid, calculate start- endtime etc.  ...
  % 
  t0b = a(1,3)  ;
  n = length(a(:,1));
  teb = a(n,3) ;
  tdiff = round((teb - t0b)*365/par1);
  loc = zeros(3,length(gx)*length(gy));
  
  % loop over  all points
  % 
  i2 = 0.;
  i1 = 0.;
  bvg = [];
  allcount = 0.;
  wai = waitbar(0,' Please Wait ...  ');
  set(wai,'NumberTitle','off','Name','b-value grid - percent done');;
  drawnow
   dt = 1;
  % 
% overall b-value
      [bv magco stan av me mer me2 pr] =  bvalca3(a,inb1,inb2);
      bo1 = bv; no1 = length(a(:,1));

% loop over all points
  for i= 1:length(newgri(:,1))
      x = newgri(i,1);y = newgri(i,2);
      allcount = allcount + 1.;
      i2 = i2+1;

      % calculate distance from center point and sort wrt distance
      l = sqrt(((a(:,1)-x)*cos(pi/180*y)*111).^2 + ((a(:,2)-y)*111).^2) ;
      [s,is] = sort(l);
      b = a(is(:,1),:) ;       % re-orders matrix to agree row-wise

      % take first ni points
      b = b(1:ni,:);      % new data per grid point (b) is sorted in distance

      [st,ist] = sort(b);   % re-sort wrt time for cumulative count
      b = b(ist(:,3),:);
      newt2 = b;
      calcp
      
      bvg = [ bvg ; bv c x y A p P  ];


      waitbar(allcount/itotal)
   end  % for newgr
   
  % save data
  %
    catSave3 =...
  [ 'welcome(''Save Grid'',''  '');think;',...
   '[file1,path1] = uiputfile([hodi fs ''eq_data'' fs ''*.mat''], ''Grid Datafile Name?'') ;',...
   ' sapa2 = [''save '' path1 file1 '' bvg gx gy dx dy par1 tdiff t0b teb a main faults mainfault coastline yvect xvect tmpgri ll''];',...
   ' if length(file1) > 1 ,eval(sapa2),end , done';]; eval(catSave3)
  
   close(wai)
   watchoff
  
  % plot the results 
  % old and re3 (initially ) is the b-value matrix
  % 
  normlap2=ones(length(tmpgri(:,1)),1)*nan;
  normlap2(ll)= bvg(:,1);
  re3=reshape(normlap2,length(yvect),length(xvect));

  normlap2(ll)= bvg(:,5);
  r=reshape(normlap2,length(yvect),length(xvect));
 
  normlap2(ll)= bvg(:,6);
  pmap=reshape(normlap2,length(yvect),length(xvect));
 
  normlap2(ll)= bvg(:,2);
  old1=reshape(normlap2,length(yvect),length(xvect));

  normlap2(ll)= bvg(:,7);
  Pmap=reshape(normlap2,length(yvect),length(xvect));
   
  old = re3;
 
  % View the b-value map
  view_pva
  
end   % if sel = na

% Load exist b-grid
if sel == 'lo'
  [file1,path1] = uigetfile(['*.mat'],'b-value gridfile');
  if length(path1) > 1
    think
    load([path1 file1])
  normlap2=ones(length(tmpgri(:,1)),1)*nan;
  normlap2(ll)= bvg(:,1);
  re3=reshape(normlap2,length(yvect),length(xvect));
 
  normlap2(ll)= bvg(:,5);
  r=reshape(normlap2,length(yvect),length(xvect));
 
  normlap2(ll)= bvg(:,6);
  meg=reshape(normlap2,length(yvect),length(xvect));
 
  normlap2(ll)= bvg(:,2);
  old1=reshape(normlap2,length(yvect),length(xvect));
 
  normlap2(ll)= bvg(:,7);
  pro=reshape(normlap2,length(yvect),length(xvect));
  
  normlap2(ll)= bvg(:,8);
  avm=reshape(normlap2,length(yvect),length(xvect));
  
  normlap2(ll)= bvg(:,9);
  stanm=reshape(normlap2,length(yvect),length(xvect));
  
  normlap2(ll)= bvg(:,10);
  maxm=reshape(normlap2,length(yvect),length(xvect));
  
  old = re3;

    view_bva
  else
    return
  end
end

% This .m file "view_x
% maxz.m" plots the maxz LTA values calculated 
% with maxzlta.m or other similar values as a color map 
% needs re3, gx, gy, stri
% Called from Dcross.m
%
% define size of the plot etc. 
% 
if isempty(name) >  0 
    name = '  '
end
think
disp('This is fractal/codes/view_Dv.m')
co = 'w';


% Find out of figure already exists
%
[existFlag,figNumber]=figflag('D-value cross-section',1);
newbmapcWindowFlag=~existFlag;



% Set up the Seismicity Map window Enviroment
%
if newbmapcWindowFlag,
    bmapc = figure( ...
        'Name','D-value cross-section',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
    % make menu bar
    matdraw
    lab1 = 'D-value';
    
    symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
    SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
    TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
    ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
    
    uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
    uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
    uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
    uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
    uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
    uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
    uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');
    
    uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
    uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
    uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
    uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
    uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
    uimenu(TypeMenu,'Label','none','Callback','vi = ''off'';set(ploeqc,''visible'',''off''); ');
    
    uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
    uimenu(ColorMenu,'Label','red','Callback','co=''r'';eval(cal9)');
    uimenu(ColorMenu,'Label','blue','Callback','co=''b'';eval(cal9)');
    uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
    
    cal9 = ...
        [ 'vi=''on'';set(ploeqc,''MarkerSize'',4,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];
    
    
    uicontrol('BackGroundColor','w','Units','normal',...
        'Position',[.0 .95 .08 .06],'String','Info ',...
        'callback','zmaphelp(ttlStr,hlpStr1zmap,hlpStr2zmap)')
    
    
    
    options = uimenu('Label',' Select ','BackgroundColor','y');
    uimenu(options,'Label','Refresh ','callback','view_Dv')
   
    uimenu(options,'Label','Select EQ in Sphere (const N)',...
        'callback',' h1 = gca;ho = ''nohold'';ic = 1; org = [5]; startfd;')
    uimenu(options,'Label','Select EQ in Sphere (const R)',...
        'callback',' h1 = gca;ho = ''nohold'';icCircl = 2; org = [5]; startfd;')
    uimenu(options,'Label','Select EQ in Sphere (N) - Overlay existing plot',...
        'callback','h1 = gca;ho = ''hold'';ic = 1; org = [5]; startfd;')
    %
    %
    
    op1 = uimenu('Label',' Maps ','BackgroundColor','y');
    
    uimenu(op1,'Label','D-value Map (weighted LS)',...
        'callback','lab1=''D-value''; re3 = old; view_Dv');
    
    %  uimenu(op1,'Label','Goodness of fit  map',...
    %     'callback','lab1=''%''; re3 = Prmap; view_Dv');
    
    uimenu(op1,'Label','b-value Map',...
        'callback','lab1=''b-value'';re3 = BM; view_Dv');
    
    uimenu(op1,'Label','resolution Map',...
        'callback','lab1=''Radius in [km]'';re3 = reso; view_Dv');
    
    uimenu(op1,'Label','Histogram ','callback','zhist');
    
    uimenu(op1,'Label','D versus b',...
        'callback','Dvbspat;');
    
    uimenu(op1,'Label','D versus Resolution',...
        'callback','Dvresfig;')
    %
    %
    
    op2e = uimenu('Label',' Display ','BackgroundColor','y');
    
    uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
    
    uimenu(op2e,'Label','Show Grid ',...
        'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
    
    uimenu(op2e,'Label','Show Circles ','callback','plotci3')
    
    uimenu(op2e,'Label','Colormap InvertGray',...
        'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
    
    uimenu(op2e,'Label','Colormap Invertjet',...
        'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
    
    uimenu(op2e,'Label','shading flat',...
        'callback','axes(hzma); shading flat;sha=''fl'';')
    
    uimenu(op2e,'Label','shading interpolated',...
        'callback','axes(hzma); shading interp;sha=''in'';')
    
    uimenu(op2e,'Label','Brigten +0.4',...
        'callback','axes(hzma); brighten(0.4)')
    
    uimenu(op2e,'Label','Brigten -0.4',...
        'callback','axes(hzma); brighten(-0.4)')
    
    uimenu(op2e,'Label','Redraw Overlay',...
        'callback','hold on;overlay_')
    
  %  uicontrol('BackGroundColor','y','Units','normal',...
  %      'Position',[.92 .80 .08 .05],'String','set ni',...
  %      'callback','ni=str2num(get(set_nia,''String''));''String'',num2str(ni);')
    
    
  %  set_nia = uicontrol('style','edit','value',ni,'string',num2str(ni));
  %  set(set_nia,'CallBack',' ');
  %  set(set_nia,'units','norm','pos',[.94 .85 .06 .05],'min',10,'max',10000);
  %  nilabel = uicontrol('style','text','units','norm','pos',[.90 .85 .04 .05]);
  %  set(nilabel,'string','ni:','background',[.7 .7 .7]);
    
    % tx = text(0.07,0.95,[name],'Units','Norm','FontSize',[18],'Color','k','FontWeight','bold');
    
   % tresh = max(max(r)); re4 = re3;
   % nilabel2 = uicontrol('style','text','units','norm','pos',[.60 .92 .25 .06]);
   % set(nilabel2,'string','MinRad (in km):','background',[c1 c2 c3]);
   % set_ni2 = uicontrol('style','edit','value',tresh,'string',num2str(tresh),...
   %     'background','y');
   % set(set_ni2,'CallBack','tresh=str2num(get(set_ni2,''String'')); set(set_ni2,''String'',num2str(tresh))');
   % set(set_ni2,'units','norm','pos',[.85 .92 .08 .06],'min',0.01,'max',10000);
    
  %  uicontrol('BackGroundColor','y','Units','normal',...
  %      'Position',[.95 .93 .05 .05],'String','Go ',...
  %      'callback','think;pause(1);re4 =re3; view_Dv')
    
    colormap(jet)
end   % This is the end of the figure setup

% Now lets plot the color-map of the D-value
%
figure(bmapc)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.],...
    'Box','on','drawmode','fast')

rect = [0.10,  0.10, 0.8, 0.75];
rect1 = rect;

% set values greater tresh = nan
%
re4 = re3;
l = r > tresh;
re4(l) = zeros(1,length(find(l)))*nan;

% plot image
% 
orient portrait
%set(gcf,'PaperPosition', [2. 1 7.0 5.0])

axes('position',rect)
hold on
% Here is the importnnt  line ...  
pco1 = pcolor(gx,gy,re4);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on;

if sha == 'fl'
    shading flat
else
    shading interp
end

end


if fre == 1
    caxis([fix1 fix2])
end

title2([name],'FontSize',12,...
    'Color','w','FontWeight','bold')
%num2str(t0b,4) ' to ' num2str(teb,4)
xlabel2('Distance in [km]','FontWeight','bold','FontSize',12)
ylabel2('Depth in [km]','FontWeight','bold','FontSize',12)

% plot overlay
% 
ploeqc = plot(Da(:,1),-Da(:,7),'.k');
set(ploeqc,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)


set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
h5 = colorbar('horz');
apo = get(h1,'pos');
set(h5,'Pos',[0.3 0.1 0.4 0.02],...
    'FontWeight','bold','FontSize',fs12,'TickDir','out')

rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')

%  Text Object Creation 

txt1 = text(... 
    'Color',[ 1 1 1 ],... 
    'EraseMode','normal',... 
    'Position',[0.55 0.03],... 
    'HorizontalAlignment','right',...
    'Rotation',[ 0 ],...
    'FontSize',fs12,.... 
    'FontWeight','bold',...
    'String',lab1); 

% Make the figure visible

axes(h1)
set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
whitebg(gcf,[0 0 0])
set(gcf,'Color',[ 0 0 0 ])
figure(bmapc);
watchoff(bmapc)
done

%   "circle0"  selects events by :
%   the Ni closest earthquakes to the center
%   the maximum radius of a circle.
%   the center point can be interactively selected or fixed by given
%   coordinates (as given by incircle).  
%   Resets newcat and newt2.     Operates on the map window on  "a".
%                                                  R.Z. 6/94
% last change 8/95 

disp('This is /src/circle0.m');

 if exist('plos1') > 0 ; clear plos1 ; end
new = a;
figure(mess)
clf
set(gca,'visible','off')

if ic == 1 | ic == 0
te = uimultitext(0.01,0.90,'#Please use the LEFT mouse button or the cursor to #select the center point. The coordinates of the center #will be displayed on the control window.# #Operates on the main subset of the catalogue. #Events selected form the new subset to operate on (newcat).');
  set(te,'FontSize',[12]);

% Input center of circle with mouse
%
axes(h1)

[xa0,ya0]  = ginput(1);

stri1 = [ 'Circle: ' num2str(xa0,6) '; ' num2str(ya0,6)];
stri = stri1;
pause(0.1)
set(gcf,'Pointer','arrow')
plot(xa0,ya0,'+c','EraseMode','back');
incircle


 elseif ic == 2;
  figure(map)
  axes(h1)
%  calculate distance for each earthquake from center point 
%  and sort by distance
%
ll = sqrt(((a(:,1)-xa0)*cos(pi/180*ya0)*111).^2 + ((a(:,2)-ya0)*111).^2) ;

  l = ll < rad;
  newt2 = a(l,:);
% 
% plot events on map as 'x':

hold on 
plos1 = plot(newt2(:,1),newt2(:,2),'xk','EraseMode','back');
set(gcf,'Pointer','arrow')


% Call program "timeplot to plot cummulative number
% 
stri1 = [ 'Circle: ' num2str(xa0,6) '; ' num2str(ya0,6) '; R = ' num2str(rad) ' km'];
stri = stri1;

[s,is] = sort(newt2(:,3));
newt2 = newt2(is(:,1),:) ;
newcat = newt2;                   % resets newcat and newt2
timeplot

ic = 1;
 
 elseif ic == 3
  figure(map)
  axes(h1)
%  calculate distance for each earthquake from center point 
%  and sort by distance
%
l = sqrt(((a(:,1)-xa0)*cos(pi/180*ya0)*111).^2 + ((a(:,2)-ya0)*111).^2) ;

  [s,is] = sort(l);            % sort by distance
  new = a(is(:,1),:) ;
  l =  sort(l);
messtext = ['Radius of selected Circle: ' num2str(l(ni))  ' km' ];
           disp(messtext)

                             
  newt = new(1:ni,:);          % take first ni and sort by time
  [st,ist] = sort(newt);
  newt2 = newt(ist(:,6),:);
% 
% plot events on map as 'x':

hold on 
plos1 = plot(newt2(:,1),newt2(:,2),'xk','EraseMode','back');
set(gcf,'Pointer','arrow')

newcat = newt2;                   % resets newcat and newt2

% Call program "timeplot to plot cumulative number
% 
stri1 = [ 'Circle: ' num2str(xa0,6) '; ' num2str(ya0,6)];
stri = stri1;
timeplot

ic = 1;

end      % if ic


function y = frac(x)

%FRAC
%
%	FRAC(X) returns the fractional part of a real number.

disp('This is /src/frac.m');

y = x - fix(x);
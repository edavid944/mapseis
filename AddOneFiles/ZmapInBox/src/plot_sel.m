% This function selects events around a seismic 
% station that fullfill certain criteria;

disp('This is /src/plot_sel.m');

load /Seis/obelix/stefan/split_data2/ak8897.mat
ty1 = '+'
ty2 = 'o'
ty2 = 'x'
par1 = 100;minmag = 4;

los = input('Longitude: ')
las = input('Latitude: ')

pl = plot(los,las,'rs')
set(pl,'LineWidth',[1.0],'MarkerSize',[10],...
   'MarkerFaceColor','y','MarkerEdgeColor','k');


l = sqrt(((a(:,1)-los)*cos(pi/180*las)*111).^2 + ((a(:,2)-las)*111).^2) ;
l2 = a(:,6) >= 0.0 & a(:,7) >= l;
%l2 = a(:,6) >=2.0 & a(:,7) <= 30 & l < 100;
a = a(l2,:);
subcata
pl = plot(los,las,'rs')
set(pl,'LineWidth',[1.0],'MarkerSize',[10],...
   'MarkerFaceColor','r','MarkerEdgeColor','y');

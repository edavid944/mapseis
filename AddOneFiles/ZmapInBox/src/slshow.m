disp('This is /src/slshow.m');

% read the welcome screen

switch(dosl) 
    
case 'newf'
    do = ['cd  ' hodi fs 'slides'];
    eval(do)
    fsl = figure('Menubar','none','NumberTitle','off'); 
    axes('pos',[0 0 1 1]);axis off; axis ij
    ga = gca;
    
    i = 1; 
    
    uicontrol(gcf,...
        'Units','normalized',...
        'Callback','close(fsl);cd ..;return',...     
        'String','Close',...
        'BackgroundColor' ,[0.8 0.8 0.8]',...
        'Position',[0.90 0.01 0.10 0.06]);
    
    uicontrol(gcf,...
        'Units','normalized',...
        'Callback','i = i+1; slshow ',...     
        'String','Next ',...
        'BackgroundColor' ,[0.8 0.8 0.8]',...
        'Position',[0.90 0.12 0.10 0.06]);
    
    dosl = 'news'; slshow; 
    
case 'news'
    
    if i > 22; i = 1; end; 
    figure(fsl); cla
    
    if i <10
        str = [ 'Slide' num2str(i,1) '.JPG'];
    else
        str = [ 'Slide' num2str(i,2) '.JPG'];
    end
    
    
    do = [ '[x,imap] = imread(str);']; err =  ''; 
    eval(do,err); 
    
    image(x)
    drawnow
    
end



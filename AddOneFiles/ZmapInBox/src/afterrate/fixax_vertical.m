% Script: fixax_vertical.m
% Make dialog interface for the fixing of vertical colomap
%
% last update: 07.07.2004
% jochen.woessner@sed.ethz.ch

disp('This is /src/fixax_verticcal.m')

% Initialize
fre = 0;

% Figure setup
figure(mess)
clf
set(gca,'visible','off')
set(gcf,'Units','pixel','NumberTitle','off','Name','Input Parameters');
set(gcf,'pos',[ wex  wey welx+200 wely-50])

l = isnan(re3);
re = re3;
re(l) = [];

% Create a dialog box to input some parameters
%

inp2_field  = uicontrol('BackGroundColor','g','Style','edit',...
    'Position',[.80 .775 .18 .15],...
    'Units','normalized','String',num2str(min(min(re))),...
    'CallBack','fix1=str2num(get(inp2_field,''String''));set(inp2_field,''String'',num2str(fix1));');

txt2 = text(...
    'Color',[0 0 0 ],...
    'EraseMode','normal',...
    'Position',[0. 0.9 0 ],...
    'Rotation',0 ,...
    'FontWeight','bold',...
    'FontSize',fs12 ,...
    'String','Please input minimum of z-axis:');


txt3 = text(...
    'Color',[0 0 0 ],...
    'EraseMode','normal',...
    'Position',[0. 0.65 0 ],...
    'Rotation',0 ,...
    'FontWeight','bold',...
    'FontSize',fs12 ,...
    'String','Please input maximum of z(or b)-values:');

inp3_field=uicontrol('BackGroundColor','g','Style','edit',...
    'Position',[.80 .575 .18 .15],...
    'Units','normalized','String',num2str(max(max(re))),...
    'CallBack','fix2=str2num(get(inp3_field,''String'')); set(inp3_field,''String'',num2str(fix2));');

close_button=uicontrol('BackGroundColor','y','Style','Pushbutton',...
    'Position', [.60 .07 .15 .15 ],...
    'Units','normalized','Callback','welcome','String','Cancel');

go_button=uicontrol('BackGroundColor','y','Style','Pushbutton',...
    'Position',[.25 .05 .15 .15 ],...
    'Units','normalized',...
    'Callback','fix2=str2num(get(inp3_field,''String''));fix1=str2num(get(inp2_field,''String''));welcome;axes(h1);caxis([fix1 fix2]);hold off;',...
    'String','Go');

freeze_button = uicontrol(...
    'BackgroundColor',[ 0.7 0.7 0.7 ],...
    'CallBack','fre = get(freeze_button,''Value'');',...
    'ForegroundColor',[ 0 0 0 ],...
    'Position',[ 0.25 0.30 0.4 0.15 ],...
    'String','Freeze Colorbar? ',...
    'Style','checkbox',...
    'Units','normalized',...
    'Visible','on');

set(gcf,'visible','on');watchoff

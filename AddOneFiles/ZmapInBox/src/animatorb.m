
disp('This is /src/animatorz.m');

global ps1 ps2 plin pli slfig
  
   switch(action) 
    case 'start',
      msgbox('Please select the starting point o fthe x-section with a left mouseclick, then drag the mouse to terminal location and release the button','Info')
      disp('waiting for button press')
      axis manual; hold on
      set(gcf,'Pointer','cross');
      waitforbuttonpress 
      point1 = get(gca,'CurrentPoint'); % button down detected 
      ps1 = plot(point1(1,1),point1(1,2),'ws');
     
     set(gcf,'WindowButtonMotionFcn',' action = ''move''; animatorz') 
     set(gcf,'WindowButtonUpFcn','action = ''stop''; animatorb ') 

      point2 = get(gca,'CurrentPoint');
      ps2 = plot(point2(1,1),point2(1,2),'w^','era','xor');
      plin = [point1(1,1) point1(1,2) ; point2(2,1) point2(2,2)];
      pli = plot(plin(:,1),plin(:,2),'w-','era','xor');
      set(pli,'LineWidth',[2])

    case 'move' 
     currPt=get(gca,'CurrentPoint'); 
     set(ps2,'XData',currPt(1,1)) 
     set(ps2,'YData',currPt(1,2)) 
     set(pli,'XData',[ plin(1,1) currPt(1,1)]); 
     set(pli,'YData',[ plin(1,2) currPt(1,2)]);
     
    case 'stop'     
     set(gcf,'Pointer','arrow'); 
     set(gcbf,'WindowButtonMotionFcn','') 
     set(gcbf,'WindowButtonUpFcn','')
     slm = 'newslice'; slicemap
     
   end
   
   
   
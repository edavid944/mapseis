% This subroutine  creates a grid with
% spacing dx,dy (in degreees). The size will
% be selected interactively or grids the entire cross section.
% The b-value in each volume is computed around a grid point containing ni earthquakes
% or in between a certain radius
% The standard deviation is calcualted either with the max. likelihood or by bootstrapping, when that box is checked.
% If not, both options can be assigned by the additional run assignment.
% Standard deviation of b-value in non-bootstrapping case is calculated from Aki-formula!
% Org: Stefan Wiemer 1/95
% last update: J. Woessner, 02.04.2005

% JW: Removed Additional random runs for uncertainty determination since
% this is incorporated in new functions to determine Mc and B with
% bootstrapping
disp('This is /src/bcross.m');

global no1 bo1 inb1 inb2

% Do we have to create the dialogbox?
if sel == 'in'
    % Set the grid parameter
    % initial values
    %
    dd = 1.00;
    dx = 1.00 ;
    ni = 100;
    bv2 = nan;
    Nmin = 50;
    stan2 = nan;
    stan = nan;
    prf = nan;
    av = nan;
    %nRandomRuns = 1000;
    bGridEntireArea = 0;
    nBstSample = 100;
    fMccorr = 0;
    fBinning = 0.1;
    bBst_button = 0;
    fMcFix = 1.5;

    % Get list of Mc computation possibilities
    [labelList2] = calc_Mc;
    % Create the dialog box
    figure(...
        'Name','Grid Input Parameter',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'units','points',...
        'Visible','on', ...
        'Position',[ wex+200 wey-200 550 300], ...
        'Color', [0.8 0.8 0.8]);
    axis off

    % Dropdown list
    hndl2=uicontrol(...
        'Style','popup',...
        'Position',[ 0.2 0.77  0.6  0.08],...
        'Units','normalized',...
        'String',labelList2,...
        'BackgroundColor','w',...
        'Callback','inb2 =get(hndl2,''Value''); ');

    % Set selection to 'Best combination'
    set(hndl2,'value',[1]);

    % Edit fields, radiobuttons, and checkbox
    freq_field=uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.30 .60 .12 .10],...
        'Units','normalized','String',num2str(ni),...
        'FontSize',fs12 ,...
        'CallBack','ni=str2num(get(freq_field,''String'')); set(freq_field,''String'',num2str(ni));set(tgl2,''value'',[0]); set(tgl1,''value'',[1])');

    freq_field0=uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.80 .60 .12 .10],...
        'Units','normalized','String',num2str(ra),...
        'FontSize',fs12 ,...
        'CallBack','ra=str2num(get(freq_field0,''String'')); set(freq_field0,''String'',num2str(ra)) ; set(tgl2,''value'',[1]); set(tgl1,''value'',[0])');

    freq_field2=uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.30 .40 .12 .10],...
        'Units','normalized','String',num2str(dx),...
        'FontSize',fs12 ,...
        'CallBack','dx=str2num(get(freq_field2,''String'')); set(freq_field2,''String'',num2str(dx));');

    freq_field3=uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.30 .30 .12 .10],...
        'Units','normalized','String',num2str(dd),...
        'FontSize',fs12 ,...
        'CallBack','dd=str2num(get(freq_field3,''String'')); set(freq_field3,''String'',num2str(dd));');

    tgl1 = uicontrol('BackGroundColor', [0.8 0.8 0.8], ...
        'Style','radiobutton',...
        'string','Number of events:',...
        'FontSize',fs12 ,...
        'FontWeight','bold',...
        'Position',[.02 .6 .28 .10],'callback','set(tgl2,''value'',[0])',...
        'Units','normalized');

    % Set to constant number of events
    set(tgl1,'value',[1]);

    % Checkbox and radiobuttons
    tgl2 =  uicontrol('BackGroundColor',[0.8 0.8 0.8],'Style','radiobutton',...
        'string','Constant radius [km]:',...
        'FontSize',fs12 ,...
        'FontWeight','bold',...
        'Position',[.52 .60 .28 .10],'callback','set(tgl1,''value'',[0])',...
        'Units','normalized');

    %     chkRandom = uicontrol('BackGroundColor',[0.8 0.8 0.8],'Style','checkbox',...
    %         'String', 'Additional random simulation',...
    %         'FontSize',fs12 ,...
    %         'FontWeight','bold',...
    %         'Position',[.52 .29 .40 .10],...
    %         'Units','normalized');

    chkGridEntireArea = uicontrol('BackGroundColor', [0.8 0.8 0.8], ...
        'Style','checkbox',...
        'string','Create grid over entire area',...
        'FontSize',fs12 ,...
        'FontWeight','bold',...
        'Position',[.02 .005 .40 .10], 'Units','normalized', 'Value', 0);

    chKBst_button = uicontrol('BackGroundColor', [0.8 0.8 0.8],'Style','checkbox',...
        'FontSize',fs12 ,'FontWeight','bold','string','Bootstraps:',...
        'Position',[.52 .3 .28 .10],'Units','normalized', 'Value', 0);

    % Editable fields
    %     txtRandomRuns = uicontrol('BackGroundColor','w','Style','edit',...
    %         'Position',[.80 .18 .12 .10],...
    %         'Units','normalized','String',num2str(nRandomRuns),...
    %         'FontSize',fs12 ,...
    %         'CallBack','nRandomRuns=str2num(get(txtRandomRuns,''String'')); set(txtRandomRuns,''String'',num2str(nRandomRuns));');

    freq_field4 =  uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.30 .20 .12 .10],...
        'Units','normalized','String',num2str(Nmin),...
        'FontSize',fs12 ,...
        'CallBack','Nmin=str2num(get(freq_field4,''String'')); set(freq_field4,''String'',num2str(Nmin));');

    freq_field5 =  uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.80 .30 .12 .10],...
        'Units','normalized','String',num2str(nBstSample),...
        'FontSize',fs12 ,...
        'CallBack','nBstSample=str2num(get(freq_field5,''String'')); set(freq_field5,''String'',num2str(nBstSample));');

    freq_field6 =  uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.80 .40 .12 .10],...
        'Units','normalized','String',num2str(fMccorr),...
        'FontSize',fs12 ,...
        'CallBack','fMccorr=str2num(get(freq_field6,''String'')); set(freq_field6,''String'',num2str(fMccorr));');

    freq_field7 =  uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.30 .50 .12 .10],...
        'Units','normalized','String',num2str(fMcFix),...
        'FontSize',fs12 ,...
        'CallBack','fMcFix=str2num(get(freq_field7,''String'')); set(freq_field7,''String'',num2str(fMcFix));');

    freq_field8 =  uicontrol('BackGroundColor','w','Style','edit',...
        'Position',[.80 .50 .12 .10],...
        'Units','normalized','String',num2str(fBinning),...
        'FontSize',fs12 ,...
        'CallBack','fBinning=str2num(get(freq_field8,''String'')); set(freq_field8,''String'',num2str(fBinning));');

    uicontrol('BackGroundColor', [0.8 0.8 0.8], 'Style', 'pushbutton', ...
        'Units', 'normalized', 'Position', [.80 .005 .15 .12], ...
        'Callback', 'close;done', 'String', 'Cancel');

    uicontrol('BackGroundColor', [0.8 0.8 0.8], 'Style', 'pushbutton', ...
        'Units', 'normalized', 'Position', [.60 .005 .15 .12], ...
        'Callback', 'inb1 =get(hndl2,''Value'');tgl1 =get(tgl1,''Value'');tgl2 =get(tgl2,''Value''); bBst_button = get(chKBst_button, ''Value''); bGridEntireArea = get(chkGridEntireArea, ''Value'');close,sel =''ca'', bcross',...
        'String', 'OK');

    % Labels
    text('Color', [0 0 0], 'EraseMode', 'normal', 'Units', 'normalized', ...
        'Position', [0.2 1 0], 'HorizontalAlignment', 'left', 'Rotation', 0, ...
        'FontSize', fs14, 'FontWeight', 'bold', 'String', 'Please select a Mc estimation option');

    text('Color',[0 0 0], 'EraseMode','normal', 'Units', 'normalized', ...
        'Position', [-.14 .54 0], 'HorizontalAlignment', 'left', 'Rotation', 0, ...
        'FontSize', fs12, 'FontWeight', 'bold', 'String','Fixed Mc:');

    text('Color',[0 0 0], 'EraseMode','normal', 'Units', 'normalized', ...
        'Position', [-.14 .42 0], 'HorizontalAlignment', 'left', 'Rotation', 0, ...
        'FontSize', fs12, 'FontWeight', 'bold', 'String','Horizontal spacing [km]:');

    text('Color', [0 0 0], 'EraseMode', 'normal', 'Units', 'normalized', ...
        'Position', [-0.14 0.30 0], 'Rotation', 0, 'HorizontalAlignment', 'left', ...
        'FontSize', fs12, 'FontWeight', 'bold', 'String', 'Depth spacing [km]:');

    text('Color', [0 0 0], 'EraseMode', 'normal', 'Units', 'normalized', ...
        'Position', [-0.14 0.18 0], 'Rotation', 0, 'HorizontalAlignment', 'left', ...
        'FontSize', fs12, 'FontWeight', 'bold', 'String', 'Min. number of events:');

    text('Color', [0 0 0], 'EraseMode', 'normal', 'Units', 'normalized', ...
        'Position', [0.52 0.42 0], 'Rotation', 0, 'HorizontalAlignment', 'left', ...
        'FontSize', fs12, 'FontWeight', 'bold', 'String', 'Mc correction:');

    %     text('Color', [0 0 0], 'EraseMode', 'normal', 'Units', 'normalized', ...
    %         'Position', [0.5 0.17 0], 'Rotation', 0, 'HorizontalAlignment', 'left', ...
    %         'FontSize', fs12, 'FontWeight', 'bold', 'String', 'Number of runs');

    text('Color', [0 0 0], 'EraseMode', 'normal', 'Units', 'normalized', ...
        'Position', [0.52 0.535 0], 'Rotation', 0, 'HorizontalAlignment', 'left', ...
        'FontSize', fs12, 'FontWeight', 'bold', 'String', 'Magnitude binning:');

    if term == 1 ; whitebg(gcf,[1 1 1 ]);end
    set(gcf,'visible','on');
    watchoff
end   % if sel == in

% get the grid-size interactively and
% calculate the b-value in the grid by sorting
% the seimicity and selecting the ni neighbors
% to each grid point

if sel == 'ca'

    % Select and reate grid
    [newgri, xvect, yvect, ll] = ex_selectgrid(xsec_fig, dx, dd, bGridEntireArea);

    % Plot all grid points
    plot(newgri(:,1),newgri(:,2),'+k','era','normal')


    welcome(' ','Running... ');think
    %  make grid, calculate start- endtime etc.  ...
    %
    t0b = newa(1,3)  ;
    n = length(newa(:,1));
    teb = newa(n,3) ;
    tdiff = round((teb - t0b)*365/par1);

    % loop over  all points
    % Set size for output matrix
    bvg = zeros(length(newgri),12)*nan;
    allcount = 0.;
    wai = waitbar(0,' Please Wait ...  ');
    set(wai,'NumberTitle','off','Name','b-value grid - percent done');;
    drawnow
    itotal = length(newgri(:,1));
    %
    % loop
    %
    for i= 1:length(newgri(:,1))
        x = newgri(i,1);y = newgri(i,2);
        allcount = allcount + 1.;

        % calculate distance from center point and sort wrt distance
        l = sqrt(((xsecx' - x)).^2 + ((xsecy + y)).^2) ;
        [s,is] = sort(l);
        b = newa(is(:,1),:) ;       % re-orders matrix to agree row-wise


        if tgl1 == 0   % take point within r
            l3 = l <= ra;
            b = newa(l3,:);      % new data per grid point (b) is sorted in distanc
            rd = ra;
        else
            % take first ni points
            b = b(1:ni,:);      % new data per grid point (b) is sorted in distance
            rd = s(ni);
        end

        % Number of earthquakes per node
        [nX,nY] = size(b);

        %estimate the completeness and b-value
        newt2 = b;

        if length(b) >= Nmin  % enough events?
            % Added to obtain goodness-of-fit to powerlaw value
            mcperc_ca3;
            [fMc] = calc_Mc(b, inb1, fBinning, fMccorr);
            l = b(:,6) >= fMc-(fBinning/2);
            if length(b(l,:)) >= Nmin
                [fMeanMag, fBValue, fStd_B, fAValue] =  calc_bmemag(b(l,:), fBinning);
            else
                %fMc = nan;
                fBValue = nan; fStd_B = nan; fAValue= nan;
            end

            % Bootstrap uncertainties
            if bBst_button == 1
                % Check Mc from original catalog
                l = b(:,6) >= fMc-(fBinning/2);
                if length(b(l,:)) >= Nmin
                    [fMc, fStd_Mc, fBValue, fStd_B, fAValue, fStd_A, vMc, mBvalue] = calc_McBboot(b, fBinning, nBstSample, inb1);
                else
                    %fMc = nan; 
                    %fStd_Mc = nan; 
                    fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A= nan;
                end;
            else
                % Set standard deviation of a-value to nan;
                fStd_A= nan; fStd_Mc = nan;
            end;

        else % of if length(b) >= Nmin
            fMc = nan; fStd_Mc = nan; fBValue = nan; fStd_B = nan; fAValue= nan; fStd_A = nan;
            %bv = nan; bv2 = nan; stan = nan; stan2 = nan; prf = nan; magco = nan; av = nan; av2 = nan;
            prf = nan;
            b = [ nan nan nan nan nan nan nan nan nan];
            nX = nan;
        end
        mab = max(b(:,6)) ; if isempty(mab)  == 1; mab = nan; end

        % Result matrix
        %bvg(allcount,:)  = [ bv magco x y rd bv2 stan2 av stan prf  mab av2 fStdDevB fStdDevMc nX];
        bvg(allcount,:)  = [ fMc fStd_Mc x y rd fBValue fStd_B fAValue fStd_A prf mab nX];
        waitbar(allcount/itotal)
    end  % for  newgri

    drawnow
    gx = xvect;gy = yvect;

    catSave3 =...
        [ 'welcome(''Save Grid'',''  '');think;',...
        '[file1,path1] = uiputfile([ ''*.mat''], ''Grid Datafile Name?'') ;',...
        'sapa2=[''save '' path1 file1 '' ll a newgri lat1 lon1 lat2 lon2 wi  bvg xvect yvect gx gy dx dd par1 newa maex maey maix maiy ''];',...
        ' if length(file1) > 1 ,eval(sapa2),end , done';]; eval(catSave3)
%corrected window positioning error
    close(wai)
    watchoff

    % reshape a few matrices
    %
    normlap2=ones(length(xvect)*length(yvect),1)*nan;
    % Mc map
    normlap2(ll)= bvg(:,1);
    mMc =reshape(normlap2,length(yvect),length(xvect));
    % Standard deviation Mc
    normlap2(ll)= bvg(:,2);
    mStdMc=reshape(normlap2,length(yvect),length(xvect));
    % Radius resolution
    normlap2(ll)= bvg(:,5);
    mRadRes=reshape(normlap2,length(yvect),length(xvect));
    % b-value
    normlap2(ll)= bvg(:,6);
    mBvalue=reshape(normlap2,length(yvect),length(xvect));
    % Standard deviation b-value
    normlap2(ll)= bvg(:,7);
    mStdB=reshape(normlap2,length(yvect),length(xvect));
    % a-value M(0)
    normlap2(ll)= bvg(:,8);
    mAvalue=reshape(normlap2,length(yvect),length(xvect));
    % Standard deviation a-value
    normlap2(ll)= bvg(:,9);
    mStdA=reshape(normlap2,length(yvect),length(xvect));
    % Goodness of fit to power-law map
    normlap2(ll)= bvg(:,10);
    Prmap=reshape(normlap2,length(yvect),length(xvect));
    % Whatever this is
    normlap2(ll)= bvg(:,11);
    ro=reshape(normlap2,length(yvect),length(xvect));
    %   % Additional runs
    %   normlap2(ll)= bvg(:,12);
    %   mStdDevB = reshape(normlap2,length(yvect),length(xvect));
    %   normlap2(ll)= bvg(:,13);
    %   mStdDevMc = reshape(normlap2,length(yvect),length(xvect));
    % Number of events
    normlap2(ll)= bvg(:,12);
    mNumEq = reshape(normlap2,length(yvect),length(xvect));

    re3 = mBvalue;
    kll = ll;
    % View the b-value map
    view_bv2

end   %  if sel = ca

% Load exist b-grid
if sel == 'lo'
    [file1,path1] = uigetfile(['*.mat'],'b-value gridfile');
    if length(path1) > 1
        think
        load([path1 file1])
        xsecx = newa(:,length(newa(1,:)))';
        xsecy = newa(:,7);
        xvect = gx; yvect = gy;
        tmpgri=zeros((length(xvect)*length(yvect)),2);

        normlap2=ones(length(tmpgri(:,1)),1)*nan;
        % Magnitude of completness
        normlap2(ll)= bvg(:,1);
        mMc =reshape(normlap2,length(yvect),length(xvect));
        % Standard deviation Mc
        normlap2(ll)= bvg(:,2);
        mStdMc=reshape(normlap2,length(yvect),length(xvect));
        % Radius resolution
        normlap2(ll)= bvg(:,5);
        r=reshape(normlap2,length(yvect),length(xvect));
        % b-value
        normlap2(ll)= bvg(:,6);
        mBvalue=reshape(normlap2,length(yvect),length(xvect));
        % Standard deviation b-value
        normlap2(ll)= bvg(:,7);
        mStdB=reshape(normlap2,length(yvect),length(xvect));
        % a-value M(0)
        normlap2(ll)= bvg(:,8);
        mAvalue=reshape(normlap2,length(yvect),length(xvect));
        % Standard deviation a-value
        normlap2(ll)= bvg(:,9);
        mStdA=reshape(normlap2,length(yvect),length(xvect));
        % Goodness of fit to power-law map
        normlap2(ll)= bvg(:,10);
        Prmap=reshape(normlap2,length(yvect),length(xvect));
        % Whatever this is
        normlap2(ll)= bvg(:,11);
        ro=reshape(normlap2,length(yvect),length(xvect));
        % Number of events
        normlap2(ll)= bvg(:,12);
        mNumEq = reshape(normlap2,length(yvect),length(xvect));
        %     try
        %       normlap2(ll)= bvg(:,14);
        %       mNumEq = reshape(normlap2,length(yvect),length(xvect));
        %     catch
        %       normlap2(ll) = zeros(length(bvg(:,1)),1);
        %       mNumEq = reshape(normlap2,length(yvect),length(xvect));
        %     end

        re3 = mBvalue;

        nlammap
        [xsecx xsecy inde] =mysect(a(:,2)',a(:,1)',a(:,7),wi,0,lat1,lon1,lat2,lon2);
        % Plot all grid points
        hold on
        plot(newgri(:,1),newgri(:,2),'+k','era','back')
        view_bv2
    else
        return
    end
end


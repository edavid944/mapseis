%  misfit_magnitude
% August 95 by Zhong Lu

disp('This is /src/mi_ma.m');

[existFlag,figNumber]=figflag('Misfit as a Function of Magnitude',1);

newWindowFlag=~existFlag;

if newWindowFlag,
  mif88 = figure( ...
        'Name','Misfit as a Function of Magnitude',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'NextPlot','add', ...
        'Visible','off', ...
        'Position',[ fipo(3)-300 fipo(4)-500 winx winy]);
 
  makebutt
  matdraw
  hold on

end
figure(mif88)
hold on


plot(a(:,6),mi(:,2),'go');

grid
%set(gca,'box','on',...
%        'DrawMode','fast','TickDir','out','FontWeight',...
%        'bold','FontSize',fs12,'Linewidth',[1.2]);

xlabel2('Magnitude of Earthquake','FontWeight','bold','FontSize',fs12);
ylabel2('Misfit Angle ','FontWeight','bold','FontSize',fs12);
hold off;

done

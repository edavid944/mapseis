% this script will plot the cumulative moment 
% release as a function of time

%  Stefan Wiemer  2/95

disp('This is /src/morel.m');

figure
set(gcf,'PaperPosition',[2 1 5.5 7.5])
makebutt
matdraw

%  Do the calculation
c = cumsum( 10.^(1.5*newt2(:,6) + 16.1));


pl = plot(newt2(:,3),c)
set(pl,'LineWidth',[2.0])
xlabel2('Time in years ','FontWeight','bold','FontSize',fs12)
ylabel2('Cumulative Moment ','FontWeight','bold','FontSize',fs12)

%te = text(0.1,0.9,'log10(Mo) = 1.5Ms + 16.1;','Units','normalized','FontWeight','bold')   

set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on')

hold on 
grid



% This script file is supposed to find an anomaly
% and estimet the exstend in tame and space
% 
% Stefan Wiemer  11/94

disp('This is /src/findano.m');

[i,j] = find(pr == max(max(pr)));
X = reshape(loc(1,:),length(gy),length(gx));
Y = reshape(loc(2,:),length(gy),length(gx));

%figure(zmap)
%hold on
%pla = plot(X(i,j),Y(i,j),'*w')
%set(pla,'MarkerSize',[12],'LineWidth',[1.5],'EraseMode','xor')

l = a(:,3) < td | a(:,3) > ted;
ba = a(l,:);
l = a(:,3) >= td & a(:,3) <= ted;
an = a(l,:);


figure
plot(ba(:,1),ba(:,2),'rx')
hold on
plot(an(:,1),an(:,2),'bo')
axis([ s2 s1 s4 s3])
xlabel2('Longitude [deg]','FontWeight','bold','FontSize',fs12)
ylabel2('Latitude [deg]','FontWeight','bold','FontSize',fs12)
strib = [  ' Map of   '  name '; '  num2str(t0b) ' to ' num2str(teb) ];

title2(strib,'FontWeight','bold',...
             'FontSize',fs12,'Color','r')
h1 = gca;
set(gca,'box','on',...
        'DrawMode','fast','TickDir','out','FontWeight',...
        'bold','FontSize',fs12,'Linewidth',[1.2])


overlay_
matdraw




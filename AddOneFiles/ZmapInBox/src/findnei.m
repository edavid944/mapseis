function [l,m] =  findnei(k) 
% This script finds overlapping alarms in space-time
% and groups them togetehr
% 
% Stefan Wiemer    4/95
global abo iala

disp('This is /src/findnei.m');

   d = sqrt(((abo(k,1) - abo(:,1))*cos(pi/180*34)*111).^2 + ((abo(k,2) - abo(:,2))*111).^2);
   m = d < abo(:,3)+abo(k,3) &  abs(abo(:,5)-abo(k,5)) < iala;
   l = find(m == 1);

disp('This is /src/nicetomap.m');

figure

[xx,yy]=meshgrid(vlon,vlat);
   surfl(yy,xx,tmap/1000),shading interp;

   li = light('Position',[ 0 0 100],'Style','infinite');
   material shiny
   lighting gouraud
% axis([ min(vlat) max(vlat) min(vlon) max(vlon) ]);
 
   set(gca,'FontSize',[12],'FontWeight','bold',...
     'LineWidth',[1.5],...
     'Box','on','drawmode','fast','TickDir','out')
  axis ij
  view([ -90 90])
  colormap(gray)
  
  set(gca,'Ylim',[min(vlon) max(vlon)]);
  set(gca,'Xlim',[min(vlat) max(vlat)]);
  
  hold on
  
  %pl = plot3(a(:,2),a(:,1),a(:,1)*0+6000,'or');
  


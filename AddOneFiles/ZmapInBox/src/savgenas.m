% Matlab script to write output from genas to a file.
% writes two files: one for results for magnitudes and below
% another for magnitudes and above.
%

disp('This is /src/savgenas.m');

figure(mess)
 clf ;
 set(mess,'Name','Messages');
 set(gca,'visible','off');
 set(mess,'pos',[ 0.02  0.9 0.3 0.2])
format short

[tbin,zmag,zval] = find(ZABO);           % deal with sparse matrix results
xtz = t0b + (tbin*par1/365);
zmag = minmg+(zmag-1)*magstep;
[xx,l] = sort(xtz);                     % sort in time
xtz = xtz(l);
zmag = zmag(l);
zval = zval(l);
tbin = tbin(l);
Z = [tbin'; xtz'; zmag'; zval'];

[newmatfile, newpath] = uiputfile([hodi  ], 'Above -Save As'); %Syntax change Matlab Version 7, no window positioning on macs 
fid = fopen([newpath newmatfile],'w');
fprintf(fid,'%3.0f %4.2f  %3.2f+  %6.4f\n',Z);

[tbin,zmag,zval] = find(ZBEL);
xtz = t0b + (tbin*par1/365);
zmag = minmg+(zmag-1)*magstep;
[xx,l] = sort(xtz);                     % sort in time
xtz = xtz(l);
zmag = zmag(l);
zval = zval(l);
tbin = tbin(l);
Z = [tbin'; xtz'; zmag'; zval'];

[newmatfile, newpath] = uiputfile([hodi  ], 'Below -Save As'); %Syntax change Matlab Version 7, no window positioning on macs 
fid = fopen([newpath newmatfile],'w');
fprintf(fid,'%3.0f %4.2f  %3.2f-  %6.4f\n',Z);

te = uimultit(0.01,0.60,'Output was saved in files # #below.out and above.out,# #Please rename them if desired. ');
set(te,'FontSize',[16]);

pause(5.0);
welcome;



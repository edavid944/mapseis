% This .m file "view_maxz.m" plots the maxz LTA values calculated 
% with maxzlta.m or other similar values as a color map 
% needs re3, gx, gy, stri
%
% define size of the plot etc. 
% 
if isempty(name) >  0 
   name = '  '
end
think
disp('This is /src/view_predi.m')
%co = 'w';


% Find out of figure already exists
%
[existFlag,figNumber]=figflag('b-value-map',1);
newbmapWindowFlag=~existFlag;

% This is the info window text
%
ttlStr='The Z-Value Map Window                        ';
hlpStr1zmap= ...
   ['                                                '
   ' This window displays seismicity rate changes   '
   ' as z-values using a color code. Negative       '
   ' z-values indicate an increase in the seismicity'
   ' rate, positive values a decrease.              '
   ' Some of the menu-bar options are               '
   ' described below:                               '
   '                                                '
   ' Threshold: You can set the maximum size that   '
   '   a volume is allowed to have in order to be   '
   '   displayed in the map. Therefore, areas with  '
   '   a low seismicity rate are not displayed.     '
   '   edit the size (in km) and click the mouse    '
   '   outside the edit window.                     '
   'FixAx: You can chose the minimum and maximum    '
   '        values of the color-legend used.        '
   'Polygon: You can select earthquakes in a        '
   ' polygon either by entering the coordinates or  ' 
   ' defining the corners with the mouse            '];                                         
hlpStr2zmap= ...
   ['                                                '
   'Circle: Select earthquakes in a circular volume:'
   '      Ni, the number of selected earthquakes can'
   '      be edited in the upper right corner of the'
   '      window.                                   '
   ' Refresh Window: Redraws the figure, erases     '
   '       selected events.                         '
   
   ' zoom: Selecting Axis -> zoom on allows you to  '
   '       zoom into a region. Click and drag with  '
   '       the left mouse button. type <help zoom>  ' 
   '       for details.                             '
   ' Aspect: select one of the aspect ratio options '
   ' Text: You can select text items by clicking.The'
   '       selected text can be rotated, moved, you '
   '       can change the font size etc.            '
   '       Double click on text allows editing it.  '        
   '                                                '
   '                                                '];                                        

% Set up the Seismicity Map window Enviroment
%
if newbmapWindowFlag,
   bmap = figure( ...
      'Name','b-value-map',...
      'NumberTitle','off', ...
      'MenuBar','none', ...
      'NextPlot','new', ...
      'backingstore','on',...
      'Visible','off', ...
      'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
   % make menu bar
   matdraw
   
   lab1 = 'b-value:';
   
   symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
   SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
   TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
   ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
   
   uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
   uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
   uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
   uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
   uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
   uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
   uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');
   
   uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
   uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
   uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
   uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
   uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
   uimenu(TypeMenu,'Label','none','Callback','vi=''off'';set(ploeq,''visible'',''off''); ');
   
   uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
   uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
   uimenu(ColorMenu,'Label','white','Callback','co=''r'';eval(cal9)');
   uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
   
   cal9 = ...
      [ 'vi=''on'';set(ploeq,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];
   
   
   uicontrol('BackGroundColor','w','Units','normal',... 
      'Position',[.0 .93 .08 .06],'String','Info ',...
      'callback',' web([''file:'' hodi ''/zmapwww/chp11.htm#996756'']) '); 
   
   
   
   options = uimenu('Label',' Select ','BackgroundColor','y');
   uimenu(options,'Label','Refresh ','callback','view_bva')
   uimenu(options,'Label','Select EQ in Circle',...
      'callback','h1 = gca;met = ''ni''; ho=''noho'';cirbva;watchoff(bmap)')
   uimenu(options,'Label','Select EQ in Circle - Constant R',...
      'callback','h1 = gca;met = ''ra''; ho=''noho'';cirbva;watchoff(bmap)')
   uimenu(options,'Label','Select EQ in Circle - Overlay existing plot',...
      'callback','h1 = gca;ho = ''hold'';cirbva;watchoff(bmap)')
   
   uimenu(options,'Label','Select EQ in Polygon -new ',...
      'callback','cufi = gcf;ho = ''noho'';selectp2')  
   uimenu(options,'Label','Select EQ in Polygon - hold ',...
      'callback','cufi = gcf;ho = ''hold'';selectp2')  
   
   
   op1 = uimenu('Label',' Maps ','BackgroundColor','y');
   
   adjmenu =  uimenu(op1,'Label','Adjust Map Display Parameters'),...
      uimenu(adjmenu,'Label','Adjust Mmin cut',...
      'callback','asel = ''mag''; adju; view_bva ')
   uimenu(adjmenu,'Label','Adjust Rmax cut',...
      'callback','asel = ''rmax''; adju; view_bva')
   uimenu(adjmenu,'Label','Adjust goodness of fit cut',...
      'callback','asel = ''gofi''; adju; view_bva ')
   
   
   uimenu(op1,'Label','b-value map (WLS)',...
      'callback','lab1 =''b-value''; re3 = old; view_bva')
   uimenu(op1,'Label','b(max likelihood) map',...
      'callback','lab1=''b-value''; re3 = meg; view_bva')
   uimenu(op1,'Label','mag of completness map',...
      'callback','lab1 = ''Mcomp''; re3 = old1; view_bva')
   uimenu(op1,'Label','Goodness of fit to power law map',...
      'callback','lab1 = '' % ''; re3 = Prmap; view_bva')
   
   uimenu(op1,'Label',' a-value map',...
      'callback','lab1=''a-value'';re3 = avm; view_bva')
   uimenu(op1,'Label','standard error map',...
      'callback',' lab1=''error in b'';re3 = stanm; view_bva')
   uimenu(op1,'Label','(WLS-Max like) map',...
      'callback',' lab1=''differnce in b'';re3 = old-meg; view_bva')
   
   makert = ...
      ['def = {''6''};m = inputdlg(''Magnitude of projected mainshock?'',''Input'',1,def);',...
         'm1 = m{:}; m = str2num(m1);',...
         'lab1 = ''Tr (yrs) (sm. values only)'';',...
         're3 =(teb - t0b)./(10.^(avm-m*old)); mrt = m; view_bva'];
   
   
   makertp = ...
      ['def = {''6''};m = inputdlg(''Magnitude of projected mainshock?'',''Input'',1,def);',...
         'm1 = m{:}; m = str2num(m1);',...
         'lab1 = ''1/Tr/area '';',...
         're3 =(teb - t0b)./(10.^(avm-m*old)); re3 = 1./re3/(2*pi*ra*ra);  mrt = m; view_bva'];
   
   
   recmenu =  uimenu(op1,'Label','recurrence time map '),...
      
   uimenu(recmenu,'Label','recurrence time map ',...
      'callback',makert)
   
   uimenu(recmenu,'Label','(1/Tr)/area map ',...
      'callback',makertp)
   
   uimenu(recmenu,'Label','recurrence time percentage ',...
      'callback','recperc')
   
   
   uimenu(op1,'Label','resolution Map',...
      'callback','lab1=''Radius in [km]'';re3 = r; view_bva')
   uimenu(op1,'Label','Histogram ','callback','zhist')
   uimenu(op1,'Label','Reccurrence Time Histogram ','callback','rechist')
   
   op2e = uimenu('Label',' Display ','BackgroundColor','y');
   uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
   uimenu(op2e,'Label','Plot Map in lambert projection using m_map ','callback','plotmap ')
   uimenu(op2e,'Label','Show Grid ',...
      'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
   uimenu(op2e,'Label','Show Circles ','callback','plotci2')
   uimenu(op2e,'Label','Colormap InvertGray',...
      'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
   uimenu(op2e,'Label','Colormap Invertjet',...
      'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
   uimenu(op2e,'Label','shading flat',...
      'callback','axes(hzma); shading flat;sha=''fl'';')
   uimenu(op2e,'Label','shading interpolated',...
      'callback','axes(hzma); shading interp;sha=''in'';')
   uimenu(op2e,'Label','Brigten +0.4',...
      'callback','axes(hzma); brighten(0.4)')
   uimenu(op2e,'Label','Brigten -0.4',...
      'callback','axes(hzma); brighten(-0.4)')
   uimenu(op2e,'Label','Redraw Overlay',...
      'callback','hold on;overlay')
   
   %tresh = nan; re4 = re3;
   
   colormap(jet)
   tresh = nan; minpe = nan; Mmin = nan;    
   
end   % This is the end of the figure setup

% Now lets plot the color-map of the z-value
%
figure(bmap)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs10,'FontWeight','bold',...
   'FontWeight','bold','LineWidth',[1.5],...
   'Box','on','drawmode','fast')

rect = [0.18,  0.10, 0.7, 0.75];
rect1 = rect;



def = {'4'};
ni2 = inputdlg('Which depth level ','Input',1,def);
l = ni2{:};
dez = str2num(l);

re3 = testhypo(:,:,dez); 
re3 = re3';
% find max and min of data for automatic scaling
% 
maxc = max(max(re3));
maxc = fix(maxc)+1;
minc = min(min(re3));
minc = fix(minc)-1;

% set values gretaer tresh = nan
%
re4 = re3;

% plot image
% 
orient landscape
%set(gcf,'PaperPosition', [0.5 1 9.0 4.0])

axes('position',rect)
hold on
pco1 = pcolor(gx,gy,re4);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on
if sha == 'fl'
   shading flat
else
   shading interp
end
% make the scaling for the recurrence time map reasonable
if lab1(1) =='T'
   l = isnan(re3);
   re = re3;
   re(l) = [];
   caxis([min(re) 5*min(re)]);
end
if fre == 1
   caxis([fix1 fix2])
end


title2([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
   'Color','r','FontWeight','bold')

xlabel2('Longitude [deg]','FontWeight','bold','FontSize',fs10)
ylabel2('Latitude [deg]','FontWeight','bold','FontSize',fs10)

% plot overlay
% 
hold on
overlay_
ploeq = plot(a(:,1),a(:,2),'k.');
set(ploeq,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)



set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
   'FontWeight','bold','LineWidth',[1.5],...
   'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
h5 = colorbar('horiz');
set(h5,'Pos',[0.35 0.05 0.4 0.02],...
   'FontWeight','bold','FontSize',fs10,'TickDir','out')

rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')
%  Text Object Creation 
txt1 = text(... 
   'Color',[ 0 0 0 ],... 
   'EraseMode','normal',... 
   'Units','normalized',...
   'Position',[ 0.33 0.06 0 ],...
   'HorizontalAlignment','right',...
   'Rotation',[ 0 ],...
   'FontSize',fs10,.... 
   'FontWeight','bold',...
   'String',lab1); 

% Make the figure visible
% 
set(gca,'FontSize',fs10,'FontWeight','bold',...
   'FontWeight','bold','LineWidth',[1.5],...
   'Box','on','TickDir','out')
figure(bmap);
%sizmap = signatur('ZMAP','',[0.01 0.04]);
%set(sizmap,'Color','k')
axes(h1)
watchoff(bmap)
%whitebg(gcf,[ 0 0 0 ])
done

figure

[m,n] = size(re3);
ro = reshape(re3,m*n,1);
l = isnan(ro);

ro(l) = [];
mean(ro)

hist(ro,30);



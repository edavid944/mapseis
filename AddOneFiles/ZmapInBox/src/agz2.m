% This script evaluates the percentage of space time coevered by 
%alarms
% 
re = [];

% Stefan Wiemer    4/95

disp('This is /src/agz2.m');

abo = abo2;

for tre2 = min(abo(:,4))+1.5:0.2:max(abo(:,4)-0.1)
  tre2
  abo = abo2;
  abo(:,5) = abo(:,5)* par1/365 + a(1,3);
  l = abo(:,4) >= tre2;
  abo = abo(l,:);
  l = abo(:,3) < tresh;
  abo = abo(l,:);
  size(abo)
  hold on
  
  j = 0;
  tmp = abo;
  
  while length(abo) > 1;
     j = j+1;
     global abo iala
     [k,m] = findnei(1);
     po = [k];
     for i = 1:length(k)
        [k2,m2]  = findnei(k(i));
        po = [po ; k2];
         po = sort(po);
         po2 = [ 0;  po(1:length(po)-1)] ;
         l = find(po-po2 > 0) ;
         po = [ po(l) ] ;
     end
    do = ['an' num2str(j) ' = abo(po3,:);';];
    disp([num2str(j) '  Anomalie groups  found'])
    eval(do)
    abo(po3,:) =[];
  end   % while j
  
  
  re = [ re ; tre2 j ];
  end   % for tre2
  
  
  figure
  makebutt
  matdraw
  axis off

calSave8 =...
[ 'welcome(''Save Data'',''  '');think;',...
  '[file1,path1] = uigetfile([ hodi fs ''out'' fs ''*.dat''], ''Filename ? '');',...
  's=[re(:,1) re(:,2) ];',...
  'fid = fopen([path1 file1],''w'') ;',...
  'fprintf(fid,''%6.2f  %6.2f\n'',s'');',...
  'fclose(fid) ;',...
  'done';];
 
uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0 .65 .08 .06],'String','Save ',...
          'callback','eval(calSave8)')

rect = [0.20,  0.10, 0.70, 0.60];
axes('position',rect)
hold on
pl = plot(re(:,1),re(:,2),'r');
set(pl,'LineWidth',[1.5])
pl = plot(re(:,1),re(:,2),'ob');
set(pl,'LineWidth',[1.5],'MarkerSize',[10])

set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on')
grid

ylabel('Number of Alarm Groups')
xlabel('Zalarm ')
watchoff


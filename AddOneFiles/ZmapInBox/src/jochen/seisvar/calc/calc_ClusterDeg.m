function [fClusterDeg] = calc_ClusterDeg(mCatalog, vCluster);
% [fClusterDeg] = calc_ClusterDeg(mCatalog, vCluster);
%---------------------------------------------------
% Function to determine degree of clustering
%
% Incoming variables
% mCatalog : EQ catalog in ZMAP format
% vCluster : Vector of cluster numbers
%
% Outgiong variables:
% fClusterDeg : Percentage of clustering
%
% J. Woessner, woessner@seismo.ifg.ethz.ch
% last update: 19.08.02


global bDebug;
if bDebug
    disp('This is /src/jochen/calc/calc_ClusterDeg.m');
end  

vSel = (vCluster(:,1) > 0);
mCatalogDecl = mCatalog(vSel,:);
if isempty(mCatalogDecl) % This means no events in cluster!
    fClusterDeg = NaN;
else
    fClusterDeg = length(mCatalogDecl(:,1))/length(mCatalog(:,1));
end;

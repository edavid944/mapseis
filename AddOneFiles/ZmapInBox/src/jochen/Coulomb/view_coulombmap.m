% Script: view_coulomb.m
% Display Coulomb stress change map together with seismicity
% Allow to analyze seismicity rate changes using the Ztools
% 
% J. Woessner, jochen.woessner@sed.ethz.ch, 05.07.2004

disp('This is /src/jochen/Coulomb/view_coulomb.m')

% Default value for map view
bMap =1;
 
% Load CFS file
[sFilename, sPathname] = uigetfile('*.mat', 'Pick CFS change MAT-file');
sHelp = [sPathname sFilename];
sFile = [sFilename(1:length(sFilename)-4)];
load(sHelp)
rCfs = eval(sFile);

% Load rate change file
[sFilename1, sPathname1] = uigetfile('*.mat', 'Pick Rate Change MAT-file');
sHelp1 = [sPathname1 sFilename1];
sFile1 = [sFilename1(1:length(sFilename1)-4)];
load(sHelp1)


% Set up the Seismicity Map window Enviroment
% Find out of figure already exists
[existFlag,figNumber]=figflag('Coulomb-map',1);
newcfsmapWindowFlag=~existFlag;

if newcfsmapWindowFlag,
    oldfig_button = 0;
end

if oldfig_button == 0
    cfsmap = figure( ...
        'Name','Coulomb-map',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'NextPlot','add', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
    % make menu bar
    matdraw
    
    % Display
    symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
    SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
    TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
    ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
    
    uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
    uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
    uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
    uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
    uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
    uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
    uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');
    
    uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
    uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
    uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
    uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
    uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
    uimenu(TypeMenu,'Label','none','Callback','vi=''off'';set(ploeq,''visible'',''off''); ');
    
    uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''r'';eval(cal9)');
    uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
    
    cal9 = ...
        [ 'vi=''on'';set(ploeq,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];      
    
    % Menu: Ztools  
    op1 = uimenu('Label','Ztools ','BackgroundColor','y');
    uimenu(op1,'Label','Rate change, p-,c-,k-value map in aftershock sequence (MLE) ',...
        'Callback','sel= ''in'';,rcvalgrid_a2');
    uimenu(op1,'Label','Load existing  Rate change, p-,c-,k-value map (MLE)',...
        'Callback','sel= ''lo'';rcvalgrid_a2');
    
    % Menu: ZAnalyze
    op2 = uimenu('Label',' ZAnalyze ','BackgroundColor','y');
    uimenu(op2,'Label','Refresh ','callback','view_coulombmap')
    uimenu(op2,'Label','Select EQ in Circle - Constant R',...
        'callback','h1 = gca;met = ''ra''; ho=''noho'';plot_circbootfit_a2;watchoff(cfsmap)')
    uimenu(op2,'Label','Select EQ with const. number',...
        'callback','h1 = gca;ho2=''hold'';ho = ''hold'';plot_constnrbootfit_a2;watchoff(cfsmap)')
    
    % Set default colormap
    colormap(jet)    
end;  % This is the end of the figure setup.

% Load the data from the Coulomb data file
% Define colormap
% mColormap = gui_Colormap_Rastafari(256);
colormap(jet);

% Get the gridding
vY = linspace(rCfs.fMinLat,rCfs.fMaxLat,rCfs.nNy);
vY = fliplr(vY);
vX = linspace(rCfs.fMinLon,rCfs.fMaxLon,rCfs.nNx);

% Plot Coulomb stress map with seismicity
figure(cfsmap)
% Fix color scale for imagesc
vClims = [-2 2];
hCoulomb = imagesc(vX,vY,rCfs.mCfs,vClims);
shading interp;
set(gca,'Ydir','normal');
% Colorbar
hColor = colorbar;
chl = get(hColor,'Ylabel'); 
set(chl,'String','\Delta CFS [bar]','FontS',10,'Rot',270);

% Labeling
ylabel('Latitude [deg]');
xlabel('Longitude [deg]');

% Add rate change map
hold on;
normlap2=ones(length(ll),1)*nan;
% Relative rate change
normlap2(ll)= mRcGrid(:,15);
mRelchange = reshape(normlap2,length(yvect),length(xvect));
hRate = pcolor(xvect,yvect,mRelchange)
set(hRate, 'AlphaData', 0.6, 'AlphaDataMapping', 'none');
shading(gca,'flat')
% Plot seismicity
plot(a(:,1),a(:,2),'Markersize',3,'Marker','o','Linestyle','none','Color',[0 0 0])
hold off;

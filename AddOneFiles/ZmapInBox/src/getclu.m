% get the selected cluster

disp('This is /src/getclu.m');

switch  gecl
   
case 'mouse'
   disp(['Click with the left mouse button #next to the equivalent event #of the cluster you want to examine']);
   
   figure(clmap)
   [tmp2,tmp1]=ginput(1);
   
   x=tmp2;y=tmp1; 
   
   l=sqrt(((equi(:,1)-x)*cos(pi/180*y)*111).^2 + ((equi(:,2)-y)*111).^2) ;
   [s,is] = sort(l);            % sort by distance
   new = equi(is(1),:);
   
   str = ['selected: Cluster # 'num2str(is(1)) ]; 
   set(te,'string',str); 
   
   if exist('val0') == 0 ; val0 = 1; end
   j = findobj('tag',num2str(val0));
   do = ['set(j,''MarkerSize'',[6],''Linewidth'',[1.]);'];er = ' ';
   eval(do,err)
   
   val = is(1); 
   
   j = findobj('tag',num2str(val));
   set(j,'MarkerSize',[22],'Linewidth',[4]);
   
    
case 'large'   
   val = find(cluslength == max(cluslength)); 
end


l = clus == val;
newt2 = original(l,:);

if exist('tiplo') == 0; timeplot; end
nu = (1:length(newt2(:,1))) ;nu = nu';
set(tiplo2,'Xdata',newt2(:,3),'Ydata',nu); figure(cum); 


val0 = val;;

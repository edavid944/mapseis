% tHis subroutine assigns creates a grid with 
% spacing dx,dy (in degreees). The size will 
% be selected interactiVELY. The bvalue in each 
% volume around a grid point containing ni earthquakes
% will be calculated as well as the magnitude
% of completness
%   Stefan Wiemer 1/95

disp('This is /src/pcross_old.m');

global no1 bo1 inb1 inb2

if sel == 'in'
  % get the grid parameter
  % initial values
  % 
  dd = 1.00;
  dx = 1.00 ;
  ni = 500;
  
def = {num2str(maepi(1,3))};
ni2 = inputdlg('Input Time of Mainshock ?','Input',1,def);
l = ni2{:};
mati = str2num(l);

  % make the interface 
  % 
  figure(...
          'Name','Grid Input Parameter',...
          'NumberTitle','off', ...
          'MenuBar','none', ...
          'units','points',...
          'Visible','off', ...
          'Position',[ wex+200 wey-200 550 300]);
  axis off

  %
      freq_field=uicontrol('BackGroundColor','g','Style','edit',...
           'Position',[.60 .50 .22 .10],...
          'Units','normalized','String',num2str(ni),...
          'CallBack','ni=str2num(get(freq_field,''String'')); set(freq_field,''String'',num2str(ni));');
  
      freq_field2=uicontrol('BackGroundColor','g','Style','edit',...
          'Position',[.60 .40 .22 .10],...
          'Units','normalized','String',num2str(dx),...
          'CallBack','dx=str2num(get(freq_field2,''String'')); set(freq_field2,''String'',num2str(dx));');
  
      freq_field3=uicontrol('BackGroundColor','g','Style','edit',...
          'Position',[.60 .30 .22 .10],...
          'Units','normalized','String',num2str(dd),...
          'CallBack','dd=str2num(get(freq_field3,''String'')); set(freq_field3,''String'',num2str(dd));');
  
      close_button=uicontrol('BackGroundColor','y','Style','Pushbutton',...
          'Position',[.60 .05 .15 .12 ],...
          'Units','normalized','Callback','close;done','String','Cancel');
  
      go_button1=uicontrol('BackGroundColor','y','Style','Pushbutton',...
          'Position',[.20 .05 .15 .12 ],...
          'Units','normalized',...
          'Callback','close,sel =''ca''; pcross',...
          'String','Go');
  

    txt3 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0.30 0.65 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs14 ,...
                  'FontWeight','bold',... 
                  'String',' Grid Parameter');
    txt5 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0. 0.42 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs12 ,...
                  'FontWeight','bold',... 
                  'String','Spacing along projection [km]');
  
    txt6 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0. 0.32 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs12 ,...
                  'FontWeight','bold',... 
                  'String','Spacing in depth in km:');
  
    txt1 = text(...
                  'Color',[0 0 0 ],...
                  'EraseMode','normal',...
                  'Position',[0. 0.53 0 ],...
                  'Rotation',0 ,...
                  'FontSize',fs12,...
                  'FontWeight','bold',...
                  'String','Number of Events (Ni):');
    if term == 1 ; whitebg(gcf,[1 1 1 ]);end
    set(gcf,'visible','on');
    watchoff
  
end   % if sel == in
 
% get the grid-size interactively and 
% calculate the b-value in the grid by sorting 
% thge seimicity and selectiong the ni neighbors
% to each grid point

if sel == 'ca'

figure(xsec_fig)
hold on

messtext=...
 ['To select a polygon for a grid.       '
  'Please use the LEFT mouse button of   '
  'or the cursor to the select the poly- '
  'gon. Use the RIGTH mouse button for   '
  'the final point.                      '
  'Mac Users: Use the keyboard "p" more  '
  'point to select, "l" last point.      '
  '                                      '];

welcome('Select Polygon for a grid',messtext);

x = [];
y = [];
hold on
but=1;
while but==1 | but == 112
    [xi,yi,but] = ginput(1);
mark1 =    plot(xi,yi,'ob','era','back'); % doesn't matter what erase mode is
                                         % used so long as its not NORMAL
set(mark1,'MarkerSize',[8],'LineWidth',[1.0])
   n = n + 1;
% mark2 =     text(xi,yi,[' ' int2str(n)],'era','normal');
% set(mark2,'FontSize',[15],'FontWeight','bold')

   x = [x; xi];
   y = [y; yi];

end  % while but 
welcome('Message',' Thank you .... ')

x = [ x ; x(1)];
y = [ y ; y(1)];     %  closes polygon

plos2 = plot(x,y,'b-','era','xor');        % plot outline 
sum3 = 0.;
pause(0.3)

%create a rectangular grid
xvect=[min(x):dx:max(x)];
yvect=[min(y):dd:max(y)];
gx = xvect;gy = yvect;
tmpgri=zeros((length(xvect)*length(yvect)),2);
n=0;
for i=1:length(xvect)
  for j=1:length(yvect)
    n=n+1;
   tmpgri(n,:)=[xvect(i) yvect(j)];
  end
end
%extract all gridpoints in chosen polygon
XI=tmpgri(:,1);
YI=tmpgri(:,2);

m = length(x)-1;      %  number of coordinates of polygon
l = 1:length(XI);
l = (l*0)';
ll = l;               %  Algorithm to select points inside a closed
                      %  polygon based on Analytic Geometry    R.Z. 4/94
      for i = 1:m;

l= ((y(i)-YI < 0) & (y(i+1)-YI >= 0)) & ...
(XI-x(i)-(YI-y(i))*(x(i+1)-x(i))/(y(i+1)-y(i)) < 0) | ...
((y(i)-YI >= 0) & (y(i+1)-YI < 0)) & ...
(XI-x(i)-(YI-y(i))*(x(i+1)-x(i))/(y(i+1)-y(i)) < 0);
     
       if i ~= 1 
         ll(l) = 1 - ll(l);
       else
         ll = l; 
       end;         % if i
    
      end;         % 
%grid points in polygon
newgri=tmpgri(ll,:);

% Plot all grid points
  plot(newgri(:,1),newgri(:,2),'+k')

  if length(xvect) < 2 | length(yvect) < 2; 
   errordlg('Selection too small! (not a matrix)');
   return
  end

  itotal = length(newgri(:,1));
  	
  welcome(' ','Running... ');think
  %  make grid, calculate start- endtime etc.  ...
  % 
  t0b = newa(1,3)  ;
  n = length(newa(:,1));
  teb = newa(n,3) ;
  tdiff = round((teb - t0b)*365/par1);
  loc = zeros(3,length(gx)*length(gy));
  
  % loop over  all points
  % 
  i2 = 0.;
  i1 = 0.;
  bvg = [];
  allcount = 0.;
  wai = waitbar(0,' Please Wait ...  ');
  set(wai,'NumberTitle','off','Name','p-value grid - percent done');;
  drawnow
  % 
  % loop 
dm = 0.1; 
dt = 0.01;

  % 
for i= 1:length(newgri(:,1))
      x = newgri(i,1);y = newgri(i,2);
      allcount = allcount + 1.;
      i2 = i2+1;

      % calculate distance from center point and sort wrt distance
      l = sqrt(((xsecx' - x)).^2 + ((xsecy + y)).^2) ;
      [s,is] = sort(l);
      b = newa(is(:,1),:) ;       % re-orders matrix to agree row-wise

      % take first ni points
      b = b(1:ni,:);      % new data per grid point (b) is sorted in distance
      l2 = sort(l); di = l2(ni);

      [st,ist] = sort(b);   % re-sort wrt time for cumulative count
      b = b(ist(:,3),:);

      % call the p-value function
      ttcat = b;
      %[p,sdp] = mypval(3,mati);
      [p,sdp,c,sdc,dk,sdk,aa,bb]=mypval2(3, mati);

      [bv magco stan av me mer me2 pr] =  bvalca3(b,1,1);
      A = log10(av/dk)
      la = 0;
      
     if isnan(p) == 0
       M = max(b(:,6)) - 5;
      t0 = (-mati + max(newa(:,3)))*365; 
      for t = t0:dt:t0+365
          la = la + (10^(A + bv*(M)) * (t + c)^(-p))  *dt;
      end 
      P = 1- exp(-la);
     else
       P = nan;
     end

      bvg = [ bvg ; p sdp x y di bv P ];
      waitbar(allcount/itotal)
  end  % for  newgri
   
% save data
%
%  set(txt1,'String', 'Saving data...')
drawnow
gx = xvect;gy = yvect;
 
  catSave3 =...
[ 'welcome(''Save Grid'',''  '');think;',...
 '[file1,path1] = uiputfile([hodi fs ''eq_data'' fs ''*.mat''], ''Grid Datafile Name?'') ;',...
 ' sapa2 = [''save '' path1 file1 '' ll tmpgri bvg xvect yvect gx gy dx dd par1 ni newa maex maey maix maiy ''];',...
 ' if length(file1) > 1 ,eval(sapa2),end , done';]; eval(catSave3)
 
 close(wai)
 watchoff

 % reshape a few matrices
 % 
normlap2=ones(length(tmpgri(:,1)),1)*nan;
normlap2(ll)= bvg(:,1);
re3=reshape(normlap2,length(yvect),length(xvect));

normlap2(ll)= bvg(:,2);
old1 =reshape(normlap2,length(yvect),length(xvect));

normlap2(ll)= bvg(:,5);
r=reshape(normlap2,length(yvect),length(xvect));
 
normlap2(ll)= bvg(:,6);
bv=reshape(normlap2,length(yvect),length(xvect));
 
normlap2(ll)= bvg(:,7);
Pv=reshape(normlap2,length(yvect),length(xvect));
 
old = re3;

% View the b-value map
view_pv2

end   %  if sel = ca
 
% Load exist b-grid
if sel == 'lo'
  [file1,path1] = uigetfile(['*.mat'],'b-value gridfile');
  if length(path1) > 1
    think
    load([path1 file1])
    xsecx = newa(:,length(newa(1,:)))';
    xsecy = newa(:,7);
    xvect = gx; yvect = gy;
    tmpgri=zeros((length(xvect)*length(yvect)),2);

    normlap2=ones(length(tmpgri(:,1)),1)*nan;
    normlap2(ll)= bvg(:,1);
    re3=reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,5);
    r=reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,6);
    meg=reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,2);
    old1 =reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,7);
    pro=reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,8);
    avm=reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,9);
    stanm=reshape(normlap2,length(yvect),length(xvect));
     
    normlap2(ll)= bvg(:,10);
    maxm=reshape(normlap2,length(yvect),length(xvect));
 
    old = re3;

    view_bv2
  else
    return
  end
end


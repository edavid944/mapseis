%  This is subroutine " displayas.m". A as(t) value is calculated for 
%  a given cummulative number curve and displayed in the plot.
%  Operates on catalogue newcat

% 
% start and end time
% 
think
disp('This is /src/newsta.m')
b = newcat;
%select big evenets
l = newt2(:,6) > minmag;
big = newt2(l,:);

def = {num2str(iwl2),num2str(par1)};

tit ='beta computation input parameters';
prompt={ 'Compare window length (years)',...
        'bin length (days)',...
    };
ni2 = inputdlg(prompt,tit,1,def);

l = ni2{1}; iwl2= str2num(l);
l = ni2{2}; par1= str2num(l);

[cumu xt] = hist(newt2(:,3),(t0b:par1/365:teb));
cumu2=cumsum(cumu); 

% 
%  iwl is the cutoff at the beginning and end of the analyses
%  to avoid spikes at the end
% iwl = 10; 

%
% calculate mean and z value
%
ncu = length(xt);
as = zeros(1,ncu)*nan;

t0b = a(1,3);
n = length(a(:,1));
teb = max(a(:,3));
tdiff = round((teb - t0b)*365/par1);

if sta == 'rub';
    iwl = floor(iwl2*365/par1);
    for i = iwl:1:tdiff-iwl,
        mean1 = mean(cumu(1:i));
        mean2 = mean(cumu(i+1:i+iwl));
        var1 = cov(cumu(1:i));
        var2 = cov(cumu(i+1:i+iwl));
        as(i) = (mean1 - mean2)/(sqrt(var1/i+var2/iwl));
    end     % for i 
end % if sta = rub;

if sta == 'ast';
    iwl = iwl2*365/par1;
    for i = floor(iwl):floor(tdiff-iwl),
        mean1 = mean(cumu(1:i));
        mean2 = mean(cumu(i+1:ncu));
        var1 = cov(cumu(1:i));
        var2 = cov(cumu(i+1:ncu));
        as(i) = (mean1 - mean2)/(sqrt(var1/i+var2/(tdiff-i)));
    end     % for i 
end % if sta == ast

if sta == 'lta';
    iwl = floor(iwl2*365/par1);
    %for i = 1:tdiff-iwl-1,
    for i = 1:length(cumu)-iwl;
        cu = [cumu(1:i-1) cumu(i+iwl+1:ncu)];
        mean1 = mean(cu);
        mean2 = mean(cumu(i:i+iwl));
        var1 = cov(cu);
        var2 = cov(cumu(i:i+iwl));
        as(i) = (mean1 - mean2)/(sqrt(var1/(ncu-iwl)+var2/iwl));
    end     % for i 
end % if sta == lta

if sta == 'bet';
    
    Catalog=newcat;
    NumberBins = length(xt);
    BetaValues = zeros(1,NumberBins)*NaN;
    TimeBegin = Catalog(1,3);
    NumberEQs = length(Catalog(:,1));
    TimeEnd = max(Catalog(:,3));
    
    iwl = floor(iwl2*365/par1);
    if (iwl2 >= TimeEnd-TimeBegin) | (iwl2 <= 0)
        errordlg('iwl is either too long or too short.');
        return;
    end
    
    for i = 1:length(cumu)-iwl
        EQIntervalReal=sum(cumu(i:i+(iwl-1)));
        NormalizedIntervalLength=iwl/NumberBins;
        STDTheor=sqrt(NormalizedIntervalLength*NumberEQs*(1-NormalizedIntervalLength));
        BetaValues(i) = (EQIntervalReal-(NumberEQs*NormalizedIntervalLength))/STDTheor;
    end     % for i=1:length(cumu)-iwl
    as = BetaValues;
end

% 
%  Plot the as(t) 
% 
% Find out of figure already exists
%
[existFlag,figNumber]=figflag('Cumulative Number Statistic',1);
newCumWindowFlag=~existFlag;

% Set up the Cumulative Number window 


figure(cum)
delete(gca)
delete(gca)
tet1 = '';
dele = 'delete(sinewsta);';er = 'disp(''  '')'; eval(dele,er);
dele = 'delete(te2)';er = 'disp('' '')'; eval(dele,er);
dele = 'delete(ax1);';er = 'disp('' '')'; eval(dele,er);
%clf
hold on
set(gca,'visible','off','FontSize',fs12,...
    'LineWidth',[1.5],...
    'Box','on')

% orient tall
set(gcf,'PaperPosition',[2 1 5.5 7.5])
rect = [0.2,  0.15, 0.65, 0.75];
axes('position',rect)
[pyy,ax1,ax2] = plotyy(xt,cumu2,xt,as);

set(pyy(2),'YLim',[min(as)-2  max(as)+5],'XLim',[t0b teb],...
    'XTicklabel',[],'TickDir','out')
xl = get(pyy(2),'XLim');
set(pyy(1),'XLim',xl);

set(ax1,'LineWidth',[2.0],'Color','b')
set(ax2,'LineWidth',[1.0],'Color','r')
xlabel2('Time in years ','FontWeight','normal','FontSize',fs12)
ylabel2('Cumulative Number ','FontWeight','normal','FontSize',fs12)

if sta == 'ast'
    title(['AS(t) Function; wl = ' num2str(iwl2)],'FontWeight','bold',...
        'FontSize',fs12,'Color','k');
end

if sta == 'rub'
    title(['Rubberband Function; wl = ' num2str(iwl2)],'FontWeight','bold',...
        'FontSize',fs12,'Color','k');
end

if sta == 'lta'
    title(['LTA(t) Function; wl = ' num2str(iwl2)],'FontWeight','bold',...
        'FontSize',fs12,'Color','k');
    
  probut =   uicontrol('BackGroundColor','w','Units','normal',...
    'Position',[.35 .0 .3 .05],'String','Translate into probabilities',...
    'callback',' assignin(''base'', ''value2trans'', ''z''); translating;')

end

if sta == 'bet'
   
    title(['LTA(t) Function; \beta-values; wl = ' num2str(iwl2)],'FontWeight','bold',...
        'FontSize',fs12,'Color','k');
    
 probut =  uicontrol('BackGroundColor','w','Units','normal',...
    'Position',[.35 .0 .3 .05],'String','Translate into probabilities',...
    'callback',' assignin(''base'', ''value2trans'', ''beta''); translating;')

end


i = find(as == max(as));
if length(i) > 1 ; i = i(1) ;  end;

tet1 =sprintf('Zmax: %3.1f at %3.1f ',max(as),xt(i));

v = axis;
axis([ v(1) ceil(teb) v(3)  v(4)+0.05*v(4)]);
te2 = text(v(1)+0.5, v(4)*0.9,tet1);
set(te2,'FontSize',fs12,'Color','k','FontWeight','normal')

grid
set(gca,'Color',[cb1 cb2 cb3])

hold on;


% plot big events on curve
% 
if length(big) > 0;
    %if ceil(big(:,3) -t0b) > 0 
        %f = cumu2(ceil((big(:,3) -t0b)*365/par1));
        l = newt2(:,6) > minmag;
        f = find( l  == 1);
        bigplo = plot(big(:,3),f,'hm');
        set(bigplo,'LineWidth',[1.0],'MarkerSize',[10],...
            'MarkerFaceColor','y','MarkerEdgeColor','k')
        stri4 = [];
        [le1,le2] = size(big);
        for i = 1:le1;
            s = sprintf('  M=%3.1f',big(i,6));
            stri4 = [ stri4 ; s];
        end   % for i
        
        %te1 = text(big(:,3),f,stri4);
        %set(te1,'FontWeight','normal','Color','k','FontSize',[8])
        %end
        
        %option to plot the location of big events in the map
        %
        % figure(map)
        % plog = plot(big(:,1),big(:,2),'or','EraseMode','xor');
        %set(plog,'MarkerSize',fs10,'LineWidth',[2.0])
        %figure(cum)
end %if big



% repeat button

uicontrol('BackGroundColor','w','Units','normal',...
    'Position',[.25 .0 .08 .05],'String','New',...
    'callback','newsta')

if exist('stri') > 0
    v = axis;
   
    tea = uimultitext(v(1)+0.5,v(4)*0.9,stri) ;
    set(tea,'FontSize',fs12,'Color','k','FontWeight','normal')
else
    strib = [file1];
end %% if stri

strib = [name];

set(cum,'Visible','on');
figure(cum);
watchoff
watchoff(cum)
done

xl = get(pyy(2),'XLim');
set(pyy(1),'XLim',xl);

% This .m file plots the differential b values calculated 
% with bvalmapt.m or other similar values as a color map 
% needs re3, gx, gy, stri
%
% define size of the plot etc. 
% 
if isempty(name) >  0 
 name = '  '
end
think
 disp('This is /src/view_bvtmap.m')
%co = 'w';


% Find out of figure already exists
%
[existFlag,figNumber]=figflag('differential b-value-map',1);
newbmapWindowFlag=~existFlag;
 

% Set up the Seismicity Map window Enviroment
%
if newbmapWindowFlag,
  bmap = figure( ...
        'Name','differential b-value-map',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'NextPlot','new', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
% make menu bar
matdraw

lab1 = 'Db';
 
symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');

        uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');

uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
uimenu(TypeMenu,'Label','none','Callback','vi=''off'';set(ploeq,''visible'',''off''); ');

uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
uimenu(ColorMenu,'Label','white','Callback','co=''r'';eval(cal9)');
uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
  
cal9 = ...
[ 'vi=''on'';set(ploeq,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];
  
 
uicontrol('BackGroundColor','w','Units','normal',... 
          'Position',[.0 .93 .08 .06],'String','Info ',...
          'callback',' web([''file:'' hodi ''/zmapwww/chp11.htm#996756'']) '); 
 


  options = uimenu('Label',' Select ','BackgroundColor','y');
  uimenu(options,'Label','Refresh ','callback','view_bvtmap')
  uimenu(options,'Label','Select EQ in Circle',...
        'callback','h1 = gca;met = ''ni''; ho=''noho'';cirbva;watchoff(bmap)')
  uimenu(options,'Label','Select EQ in Circle - Constant R',...
        'callback','h1 = gca;met = ''ra''; ho=''noho'';cirbva;watchoff(bmap)')
  uimenu(options,'Label','Select EQ in Circle - Time split',...
        'callback','h1 = gca;met = ''ti''; ho=''noho'';cirbvat;watchoff(bmap)')
  uimenu(options,'Label','Select EQ in Circle - Overlay existing plot',...
       'callback','h1 = gca;ho = ''hold'';cirbva;watchoff(bmap)')

  uimenu(options,'Label','Select EQ in Polygon -new ',...
         'callback','cufi = gcf;ho = ''noho'';selectp2')  
  uimenu(options,'Label','Select EQ in Polygon - hold ',...
         'callback','cufi = gcf;ho = ''hold'';selectp2')  


op1 = uimenu('Label',' Maps ','BackgroundColor','y');
 uimenu(op1,'Label','Differential b-value map ',...
         'callback','lab1 =''b-value''; re3 = db12; view_bvtmap')
     uimenu(op1,'Label','b change in percent map  ',...
         'callback','lab1 =''b-value change''; re3 = dbperc; view_bvtmap')
 uimenu(op1,'Label','b-value map first period',...
         'callback','lab1 =''b-value''; re3 = bm1; view_bvtmap')
 uimenu(op1,'Label','b-value map second period',...
         'callback','lab1 =''b-value''; re3 = bm2; view_bvtmap')
 uimenu(op1,'Label','Probability Map (Utsus test for b1 and b2) ',...
         'callback','lab1 =''P''; re3 = pro; view_bvtmap')
 uimenu(op1,'Label','Earthquake probability change map (M5) ',...
         'callback','lab1 =''dP''; re3 = log10(maxm); view_bvtmap')
 uimenu(op1,'Label','standard error map',...
         'callback',' lab1=''error in b'';re3 = stanm; view_bvtmap')

 uimenu(op1,'Label','mag of completeness map - period 1',...
          'callback','lab1 = ''Mcomp1''; re3 = magco1; view_bvtmap')
 uimenu(op1,'Label','mag of completeness map - period 2',...
          'callback','lab1 = ''Mcomp2''; re3 = magco2; view_bvtmap')
 uimenu(op1,'Label','differential completeness map ',...
          'callback','lab1 = ''DMc''; re3 = dmag; view_bvtmap')
 uimenu(op1,'Label','resolution Map - number of events ',...
         'callback','lab1=''# of events'';re3 = r; view_bvtmap')
 uimenu(op1,'Label','Histogram ','callback','zhist')

op2e = uimenu('Label',' Display ','BackgroundColor','y');
 uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
 uimenu(op2e,'Label','Plot Map in lambert Projection using m_map ','callback','plotmap ')
 uimenu(op2e,'Label','Show Grid ',...
    'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
 uimenu(op2e,'Label','Show Circles ','callback','plotci2')
 uimenu(op2e,'Label','Colormap InvertGray',...
      'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
 uimenu(op2e,'Label','Colormap Invertjet',...
      'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
 uimenu(op2e,'Label','shading flat',...
        'callback','axes(hzma); shading flat;sha=''fl'';')
 uimenu(op2e,'Label','shading interpolated',...
        'callback','axes(hzma); shading interp;sha=''in'';')
 uimenu(op2e,'Label','Brigten +0.4',...
        'callback','axes(hzma); brighten(0.4)')
 uimenu(op2e,'Label','Brigten -0.4',...
        'callback','axes(hzma); brighten(-0.4)')
 uimenu(op2e,'Label','Redraw Overlay',...
        'callback','hold on;overlay')


 tresh = nan; re4 = re3;
nilabel2 = uicontrol('style','text','units','norm','pos',[.60 .92 .25 .04],'backgroundcolor','w');
set(nilabel2,'string','Min Probability:');
set_ni2 = uicontrol('style','edit','value',tresh,'string',num2str(tresh),...
          'background','y');
set(set_ni2,'CallBack','tresh=str2num(get(set_ni2,''String'')); set(set_ni2,''String'',num2str(tresh))');
set(set_ni2,'units','norm','pos',[.85 .92 .08 .04],'min',0.01,'max',10000);

uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.95 .93 .05 .05],'String','Go ',...
          'callback','think;pause(1);re4 =re3; view_bvtmap')

colormap(jet)

 end   % This is the end of the figure setup

% Now lets plot the color-map of the z-value
%
figure(bmap)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs10,'FontWeight','normal',...
    'LineWidth',[1.],...
    'Box','on','drawmode','fast')

rect = [0.18,  0.10, 0.7, 0.75];
rect1 = rect;
 
% find max and min of data for automatic scaling
% 
maxc = max(max(re3));
maxc = fix(maxc)+1;
minc = min(min(re3));
minc = fix(minc)-1;

% set values gretaer tresh = nan
%
re4 = re3;
l = pro < tresh;
re4(l) = zeros(1,length(find(l)))*nan;
 
% plot image
% 
orient landscape
%set(gcf,'PaperPosition', [0.5 1 9.0 4.0])

axes('position',rect)
hold on
pco1 = pcolor(gx,gy,re4);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on
if sha == 'fl'
  shading flat
else
  shading interp
end
% make the scaling for the recurrence time map reasonable
if lab1(1) =='T'
  l = isnan(re3);
  re = re3;
  re(l) = [];
  caxis([min(re) 5*min(re)]);
end
if fre == 1
 caxis([fix1 fix2])
end


title([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
             'Color','k','FontWeight','normal')

xlabel2('Longitude [deg]','FontWeight','normal','FontSize',fs10)
ylabel2('Latitude [deg]','FontWeight','normal','FontSize',fs10)

% plot overlay
% 
hold on
overlay_
ploeq = plot(a(:,1),a(:,2),'k.');
set(ploeq,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)



set(gca,'visible','on','FontSize',fs10,'FontWeight','normal',...
    'LineWidth',[1.],...
    'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
 h5 = colorbar('horiz');
set(h5,'Pos',[0.35 0.05 0.4 0.02],...
      'TickDir','out','FontWeight','normal','FontSize',fs10)
 
rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')
	%  Text Object Creation 
	txt1 = text(... 
		'Color',[ 0 0 0 ],... 
		'EraseMode','normal',... 
                'Units','normalized',...
                'Position',[ 0.33 0.07 0 ],...
                'HorizontalAlignment','right',...
		'Rotation',[ 0 ],...
                'FontSize',fs10,.... 
                'FontWeight','normal',...
		'String',lab1); 

%RZ make  reset button 
%    uicontrol('BackGroundColor','c','Units','normal','Position',...
%  [.85 .10 .15 .05],'String','Reset Catalog','callback','think;clear plos1 mark1 conca ; a = org2; newcat = org2; newt2= org2; stri = ['' '']; stri1 = ['' '']');

%resets catalog  (useful for the random b map)
%clear plos1 mark1 conca ; a = org2; newcat = org2; newt2= org2; stri = ['' '']; stri1 = ['' ''];

% Make the figure visible
% 
set(gca,'FontSize',fs10,'FontWeight','normal',...
    'FontWeight','normal','LineWidth',[1.],...
    'Box','on','TickDir','out','Ticklength',[0.02 0.02])
figure(bmap);
%sizmap = signatur('ZMAP','',[0.01 0.04]);
%set(sizmap,'Color','k')
axes(h1)
watchoff(bmap)
%whitebg(gcf,[ 0 0 0 ])
set(gcf,'Color','w')
done

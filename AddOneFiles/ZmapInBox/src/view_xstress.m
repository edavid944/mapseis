% Script: view_xstress.m
% Script to display results creates with cross_stress.m
%
% Needs re3, gx, gy, stri
%
% last modified: J. Woessner, 02.2004

if isempty(name) >  0 
    name = '  '
end
think
disp('This is /src/view_xstress.m')
% Color shortcut
co = 'w';

% Find out if figure already exists
[existFlag,figNumber]=figflag('Stress-section',1);
newstressmapWindowFlag=~existFlag;                          

% Set up the Seismicity Map window Enviroment
if newstressmapWindowFlag,
    stressmap = figure( ...
        'Name','Stress-section',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'NextPlot','new', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
    % make menu bar
    matdraw
     
    symbolmenu = uimenu('Label',' Symbol ','BackgroundColor','y');
    SizeMenu = uimenu(symbolmenu,'Label',' Symbol Size ');
    TypeMenu = uimenu(symbolmenu,'Label',' Symbol Type ');
    ColorMenu = uimenu(symbolmenu,'Label',' Symbol Color ');
    
    uimenu(SizeMenu,'Label','3','Callback','ms6 =3;eval(cal9)');
    uimenu(SizeMenu,'Label','6','Callback','ms6 =6;eval(cal9)');
    uimenu(SizeMenu,'Label','9','Callback','ms6 =9;eval(cal9)');
    uimenu(SizeMenu,'Label','12','Callback','ms6 =12;eval(cal9)');
    uimenu(SizeMenu,'Label','14','Callback','ms6 =14;eval(cal9)');
    uimenu(SizeMenu,'Label','18','Callback','ms6 =18;eval(cal9)');
    uimenu(SizeMenu,'Label','24','Callback','ms6 =24;eval(cal9)');
    
    uimenu(TypeMenu,'Label','dot','Callback','ty =''.'';eval(cal9)');
    uimenu(TypeMenu,'Label','+','Callback','ty=''+'';eval(cal9)');
    uimenu(TypeMenu,'Label','o','Callback','ty=''o'';eval(cal9)');
    uimenu(TypeMenu,'Label','x','Callback','ty=''x'';eval(cal9)');
    uimenu(TypeMenu,'Label','*','Callback','ty=''*'';eval(cal9)');
    uimenu(TypeMenu,'Label','none','Callback','vi=''off'';set(ploeq,''visible'',''off''); ');
    
    uimenu(ColorMenu,'Label','black','Callback','co=''k'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''w'';eval(cal9)');
    uimenu(ColorMenu,'Label','white','Callback','co=''r'';eval(cal9)');
    uimenu(ColorMenu,'Label','yellow','Callback','co=''y'';eval(cal9)');
    
    cal9 = ...
        [ 'vi=''on'';set(ploeqc,''MarkerSize'',ms6,''LineStyle'',ty,''Color'',co,''visible'',''on'')'];  
    
    % Menu Select
    options = uimenu('Label',' Select ','BackgroundColor','y');
    uimenu(options,'Label','Refresh ','callback','re3 = r;view_xstress')
    uimenu(options,'Label','Select N closest EQs',...
        'callback','h1 = gca;ic=1; ho=''noho'';cicros;watchon;doinvers_michael;watchoff')
    uimenu(options,'Label','Select EQ in Circle - Constant R',...
        'callback','h1 = gca;ic=2; ho=''noho'';cicros;watchon;doinvers_michael;watchoff')
    uimenu(options,'Label','Select EQ in Polygon',...
        'callback','h1=gca;ic=3;ho = ''noho'';cicros;watchon;doinvers_michael;watchoff')  
    
    % Menu Maps 
    op1 = uimenu('Label',' Maps ','BackgroundColor','y');
    uimenu(op1,'Label','Variance',...
        'callback','lab1=''\sigma'';re3 = mVariance; view_xstress')
    uimenu(op1,'Label','Phi',...
        'callback','lab1=''\Phi'';re3 = mPhi; view_xstress')
    uimenu(op1,'Label','Trend S1',...
        'callback','lab1=''S1 trend [deg]'';re3 = mTS1; view_xstress')
    uimenu(op1,'Label','Plunge S1',...
        'callback','lab1=''S1 plunge [deg]'';re3 = mPS1; view_xstress')
    uimenu(op1,'Label','Trend S2',...
        'callback','lab1=''S2 trend [deg]'';re3 = mTS2; view_xstress')
    uimenu(op1,'Label','Plunge S2',...
        'callback','lab1=''S2 plunge [deg]'';re3 = mPS2; view_xstress')
    uimenu(op1,'Label','Trend S3',...
        'callback','lab1=''S3 trend [deg]'';re3 = mTS3; view_xstress')
    uimenu(op1,'Label','Plunge S3',...
        'callback','lab1=''S3 plunge [deg]'';re3 = mPS3; view_xstress')
    uimenu(op1,'Label','Angular misfit',...
        'callback','lab1=''\beta [deg]'';re3 = mBeta; view_xstress')
    uimenu(op1,'Label','\tau spread',...
        'callback','lab1=''\tau [deg]'';re3 = mTau; view_xstress')    
    uimenu(op1,'Label','Resolution map (const. Radius)',...
        'callback','lab1=''Radius in [km]'';re3 = mResolution; view_xstress')
     uimenu(op1,'Label','Resolution map',...
        'callback','lab1=''Number of events'';re3 = mNumber; view_xstress')
    uimenu(op1,'Label','Trend S1 relative to fault strike',...
        'callback','lab1=''S1 trend to strike [deg]'';re3 = mTS1Rel; view_xstress')
    %uimenu(op1,'Label','Histogram ','callback','zhist')

    % Menu Display
    op2e = uimenu('Label',' Display ','BackgroundColor','y');
    uimenu(op2e,'Label','Fix color (z) scale','callback','fixax2 ')
    uimenu(op2e,'Label','Plot Map in lambert projection using m_map ','callback','plotmap ')
    uimenu(op2e,'Label','Show Grid ',...
        'callback','hold on;plot(newgri(:,1),newgri(:,2),''+k'')')
    uimenu(op2e,'Label','Show Circles ','callback','plotci2')
    uimenu(op2e,'Label','Colormap InvertGray',...
        'callback','g=gray; g = g(64:-1:1,:);colormap(g);brighten(.4)')
    uimenu(op2e,'Label','Colormap Invertjet',...
        'callback','g=jet; g = g(64:-1:1,:);colormap(g)')
    uimenu(op2e,'Label','shading flat',...
        'callback','axes(hzma); shading flat;sha=''fl'';')
    uimenu(op2e,'Label','shading interpolated',...
        'callback','axes(hzma); shading interp;sha=''in'';')
    uimenu(op2e,'Label','Brigten +0.4',...
        'callback','axes(hzma); brighten(0.4)')
    uimenu(op2e,'Label','Brigten -0.4',...
        'callback','axes(hzma); brighten(-0.4)')
    uimenu(op2e,'Label','Redraw Overlay',...
        'callback','hold on;overlay')
    
    tresh = nan; re4 = re3;
    
    colormap(jet)
    tresh = nan; minpe = nan; Mmin = nan;    
    
end   % This is the end of the figure setup

% Now lets plot the color-maps
figure(stressmap)
delete(gca)
delete(gca)
delete(gca)
dele = 'delete(sizmap)';er = 'disp('' '')'; eval(dele,er);
reset(gca)
cla
hold off
watchon;
set(gca,'visible','off','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','drawmode','fast')

% Figure position
rect = [0.18,  0.10, 0.7, 0.75];

% Find max and min of data for automatic scaling
maxc = max(max(re3));
maxc = fix(maxc)+1;
minc = min(min(re3));
minc = fix(minc)-1;

% Plot image
orient landscape

axes('position',rect)
hold on
pco1 = pcolor(gx,gy,re3);

axis([ min(gx) max(gx) min(gy) max(gy)])
axis image
hold on
if sha == 'fl'
    shading flat
else
    shading interp
end

if fre == 1
    caxis([fix1 fix2])
end


% title2([name ';  '   num2str(t0b) ' to ' num2str(teb) ],'FontSize',fs10,...
%     'Color','r','FontWeight','bold')

xlabel2('Distance in [km]','FontWeight','bold','FontSize',fs12)
ylabel2('Depth in [km]','FontWeight','bold','FontSize',fs12)

% plot overlay
% 
ploeqc = plot(newa(:,length(newa(1,:))),-newa(:,7),'.k');
set(ploeqc,'MarkerSize',ms6,'LineStyle',ty,'Color',co,'Visible',vi)

if exist('vox') > 0
    plovo = plot(vox,voy,'*b');
    set(plovo,'MarkerSize',[6],'LineWidth',[1])
end

if exist('maix') > 0
    pl = plot(maix,maiy,'*k');
    set(pl,'MarkerSize',[12],'LineWidth',[2])
end

if exist('maex') > 0
    pl = plot(maex,-maey,'hm');
    set(pl,'LineWidth',[1.5],'MarkerSize',[12],...
        'MarkerFaceColor','w','MarkerEdgeColor','k')
    
end


set(gca,'visible','on','FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
h1 = gca;
hzma = gca;

% Create a colorbar
%
h5 = colorbar('horiz');
set(h5,'Pos',[0.35 0.2 0.4 0.02],...
    'FontWeight','bold','FontSize',fs10,'TickDir','out')

rect = [0.00,  0.0, 1 1];
axes('position',rect)
axis('off')
%  Text Object Creation 
txt1 = text(... 
    'Color',[ 0 0 0 ],... 
    'EraseMode','normal',... 
    'Units','normalized',...
    'Position',[ 0.33 0.21 0 ],...
    'HorizontalAlignment','right',...
    'Rotation',[ 0 ],...
    'FontSize',fs10,.... 
    'FontWeight','bold',...
    'String',lab1); 

% Make the figure visible
set(gca,'FontSize',fs10,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on','TickDir','out')
% Print orientation
orient portrait
figure(stressmap);
axes(h1)
watchoff(stressmap)
done

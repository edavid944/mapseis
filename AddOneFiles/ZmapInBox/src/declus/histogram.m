function histogram(vari1,stri2) 
%histogram.m                               A.Allmann 
%plots histogram in cumulative number window
%vari1 depends on input parameter
% 
%Last modification 6/95                               
global mess  ccum freq_field histo hisvar strii1 strii2 fs12
stri1='Histogram';
strii1=stri1;
strii2=stri2;
hisvar=vari1;
tm1=[];
% Find out of figure already exists
%
[existFlag,figNumber]=figflag('Histogram',1);
newHistoFlag=existFlag;
if newHistoFlag
 figure(histo) 
 cla
 cla
 delete(gca)
else
 histo= figure( ... 
 'NumberTitle','off','Name',stri1,...
 'MenuBar','none', ...
 'NextPlot','new', ...
 'Visible','off')

%Menuline for options
%
%matdraw

op1 = uimenu('Label','Display');
   uimenu(op1,'Label','Bin Number','Callback','inpubin(1);');
   uimenu(op1,'Label','Bin Vector','Callback','inpubin(2);');
   uimenu(op1,'Label','Default','Callback','hist(hisvar);');     

 callbackStr= ...
        ['newcat=a;f1=gcf; f2=gpf; set(f1,''Visible'',''off'');', ...
         'if f1~=f2, figure(f2); end'];

 uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0  .83 .08 .06],'String','Close ',...
          'callback','close;done')

uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.0  .93 .08 .06],'String','Print ',...
          'callback','myprint')

end

orient tall
rect = [0.25,  0.18, 0.60, 0.70];
axes('position',rect)
hold on 

hist(vari1,50);
title2([stri2,stri1],'FontWeight','bold','FontSize',fs12,'Color','k')
set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],'Box','on')

xlabel2(stri2,'FontWeight','bold','FontSize',fs12)
ylabel2('  Number ','FontWeight','bold','FontSize',fs12)
set(gcf,'Visible','on')

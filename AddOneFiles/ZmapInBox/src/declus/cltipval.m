 function cltipval(var1)
% cltipvla.m                            A.Allmann  
% function to calculate P-values for different time or magnitude windows
% Last modification 10/95
 
% this function is a modification of a program by Paul Raesenberg
% that is based on Programs by Carl Kisslinger and Yoshi Ogata

% function finds the maximum liklihood estimates of p,c and k, the
% parameters of the modifies Omori equation
% it also finds the standard deviations of these parameters
% all values are calculated for different time or magnitude windows 

% Input: Earthquake Catalog of an Cluster Sequence

% Output: p c k values of the modified Omori Law with respective
%         standard deviations
%         A and B values of the Gutenberg Relation based on k
%         Plots to compare different magnitude or time windows

% Create an input window for magnitude thresholds and
% plot cumulative number versus time to allow input of start and end
% time


global action_button file1 term wex wey welx wely             %welcome 
global mess ccum bgevent equi file1 clust original cluslength newclcat
global backcat ttcat cluscat
global winx winy sys minmag clu te1 fs12 fs10 fs14
global xt par3 cumu cumu2 mess 
global freq_field1 freq_field2 freq_field3 freq_field4 Go_p_button
global p c dk tt pc loop nn pp nit t err1x err2x ieflag isflag
global cstep pstep tmpcat ts tend eps1 eps2
global sdc sdk sdp cof qp cog aa bb pcheck loopcheck
global callcheck mtpl tmm
global freq_field5 callcheck
global magn mp mc mk msdk msdp msdc ctiplo
global tmp1 tmp2 tmp3 tmp4 omori hpndl1 ttcat


if var1==1 | var1==2              %magnitude or time estimate
 [existFlag,figNumber]=figflag('P-Value Estimate');
 if existFlag
  figure(mtpl);
  clf
 else 
  mtpl=figure(...
   'Name','P-Value Estimate',...
   'NumberTitle','off',...
   'MenuBar','none',...
   'NextPlot','new',...
   'visible','off',...
   'Units','normalized',... 
   'Position',[ 0.435  0.8 0.5 0.8]);
 end; 
 matdraw;
 newt2=ttcat;
%calculate start -end time of overall catalog
 t0b = newt2(1,3);
 n = length(newt2(:,1));
 teb = newt2(n,3);
 tdiff=(teb-t0b)*365;       %time difference in days
 par3=tdiff/100; 
 par5=par3;
 if par5>.5
  par5=par5/5;
 end

% calculate cumulative number versus time and bin it
%
 n = length(newt2(:,1));
 if par3>=1
  [cumu xt] = hist(newt2(:,3),(t0b:par3/365:teb));
 else
  [cumu xt] = hist((newt2(:,3)-newt2(1,3))*365,(0:par5:tdiff));
 end
 cumu2 = cumsum(cumu);
 
% plot time series
%
 orient tall
 rect = [0.22,  0.5, 0.55, 0.45];
 ctiplo=axes('position',rect);
 hold on
 cplot = plot(xt,cumu2,'ob');
 set(gca,'visible','off')
 ctiplo2 = plot(xt,cumu2,'r'); 
 if exist('stri') > 0
  v = axis;
  tea = uimultitext(v(1)+0.5,v(4)*0.9,stri) ;
  set(tea,'FontSize',fs12,'Color','k','FontWeight','bold');
 end
 
 strib = [file1];
 
 title2(strib,'FontWeight','bold',...
      'FontSize',fs14,...
      'Color','r')
 
 grid
 if par3>=1
  xlabel2('Time in years ','FontWeight','bold','FontSize',fs12)
 else
  xlabel2(['Time in days relative to ',num2str(t0b)],'FontWeight','bold','FontSize',fs12)
 end
 ylabel2('Cummulative Number ','FontWeight','bold','FontSize',fs12)
 
% Make the figure visible
%
 set(gca,'visible','on','FontSize',fs12,'FontWeight','bold',...
    'FontWeight','bold','LineWidth',[1.5],...
    'Box','on')

 gcf;
 rect=[0 0 1 1];
 h2=axes('Position',rect);
 set(h2,'visible','off');

 str =  ['# # #Please select start and end time of the P-Value plot#Click first with the left Mouse Button at your start position#and then with the left Mouse Button at the end position'];
 te = uimultitext(0.2,0.37,str) ;

 set(te,'FontSize',[14]);
 set(mtpl,'Visible','on');

 disp('Please select start and end time of the P-value plot. Click first with the left Mouse Button at your start position and then at the end position.')
 

 seti = uicontrol('BackGroundColor','c','Units','normal',...
                 'Position',[.4 .01 .2 .05],'String','Select Time1 ');

 
 XLim=get(ctiplo,'XLim');
 M1b = [];
 M1b= ginput(1);
 tt3= M1b(1);
 tt4=num2str((tt3-0.22)*(1/.55)*(XLim(2)-XLim(1))+XLim(1));
 text( M1b(1),M1b(2),['|: T1=',tt4] )
 set(seti,'String','Select Time2');

 pause(0.1)
 M2b = [];
 M2b = ginput(1);
 tt3= M2b(1);
 tt5=num2str((tt3-0.22)*(XLim(2)-XLim(1))*(1/.55)+XLim(1));
 text( M2b(1),M2b(2),['|: T2=',tt5] )
 
 pause(0.1)
 delete(seti)

 watchoff
 watchoff

 set(te,'visible','off');
 
 tmp2=min(ttcat(:,6));
 freq_field1= uicontrol('BackGroundColor','g','Style','edit',...
              'Position',[.43 .35 .1 .04],...
              'Units','normalized','String',num2str(tmp2),...
              'CallBack','tmp2=str2num(get(freq_field1,''String''));     set(freq_field1,''String'',num2str(tmp2));');
 
 tmp1=max(ttcat(:,6));
 freq_field2=uicontrol('BackGroundColor','g','Style','edit',...
              'Position',[.76 .35 .1 .04],...
              'Units','normalized','String',num2str(tmp1),...
              'CallBack','tmp1=str2num(get(freq_field2,''String''));   set(freq_field2,''String'',num2str(tmp1));');
 tmp3=str2num(tt4);
 if tmp3 < 0
  tmp3=0;
 end
  
 freq_field3=uicontrol('BackGroundColor','g','Style','edit',...
              'Position',[.43 .28 .1 .04],...
              'Units','normalized','String',num2str(tmp3),...
              'CallBack','tmp3=str2num(get(freq_field3,''String''));  set(freq_field3,''String'',num2str(tmp3));');

 tmp4=str2num(tt5);
 freq_field4=uicontrol('BackGroundColor','g','Style','edit',...
              'Position',[.76 .28 .1 .04],...
              'Units','normalized','String',num2str(tmp4),...
              'CallBack','tmp4=str2num(get(freq_field4,''String'')); set(freq_field4,''String'',num2str(tmp4));');
 

 txt1 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.1 0.37   ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold' ,...
                'String','Magnitude:    Min: ');
 
 txt2 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.6 0.37  ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold' ,...
               'String','Max :');
 

 txt3 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.1 0.3 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold' ,...
                'String','Time:             Min:');
 
 txt4 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.6 0.3 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold' ,...
               'String','Max :');
 

 if var1==1                        %magnitude window
  
  txt5 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.2 0.22 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold' ,...
               'String','Magnitude Steps: ');
  magn=tmp2;
  freq_field5=uicontrol('BackGroundColor','g','Style','edit',...
              'Position',[.55 .2 .2 .04],...
              'Units','normalized','String',num2str(magn),...
              'CallBack','magn=get(freq_field5,''String'');magn=eval(magn);');
  set(freq_field5,'String',num2str(magn)); 
  tmm=0; 
 text(0.54,0.17,'Vector (e.g. 1: 0.1: 3): ');
 elseif var1==2            %time windows
  txt5 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.2 0.22 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold' ,...
               'String','End Times : ');
      
  magn=tmp4;
  freq_field5=uicontrol('BackGroundColor','g','Style','edit',...
              'Position',[.55 .2 .2 .04],...
              'Units','normalized','String',num2str(magn),...
              'CallBack','magn=get(freq_field5,''String'');magn=eval(magn);');
  set(freq_field5,'String',num2str(magn)); 
  tmm=3;
  text(0.54,0.17,'Vector (e.g. 1: 0.5: 7): ');
 end
 
 Info_p = uicontrol('BackGroundColor','y','Style','Pushbutton',...
                'String','Info ',...
                'Position',[.3 .05 .10 .06],...
                 'Units','normalized','Callback','clinfo(18)');
 if var1==2
  set(Info_p,'callback','clinfo(19);');
 end
 close_p =uicontrol('BackGroundColor','y','Style','Pushbutton',...
                'Position', [.45 .05 .10 .06 ],...
         'Units','normalized','Callback','set(mtpl,''visible'',''off'')',...
          'String','Close');
 print_p = uicontrol('BackGroundColor','y','Style','Pushbutton',...
            'Position',[.15 .05 .1 .06],...
            'Units','normalized','Callback', 'myprint',...
            'String','Print');
 labelPos= [.6 .05 .2 .06];
 labelList=['Mainshock| Main-Input| Sequence' ];
 hpndl1 =uicontrol(...
        'style','popup',...
        'units','normalized',...
        'position',labelPos,...
        'string', labelList,...
        'Backgroundcolor','y',...
        'Callback','in2=get(hpndl1,''Value'')+2+tmm;cltipval(in2);');
 figure(mess);
 clf;
 str =  ['# # #Please give in parameters in green fields#This parameters will be used as the threshold# for the P-Value.#After input push GO to continue. '];
 te = uimultitext(0.01,0.9,str) ;
 
 set(te,'FontSize',[12]);
 set(gca,'visible','off');
 
elseif var1==3 | var1==4 | var1==5  %Mainshock/Maininput/Sequence
 
  mp=zeros(length(magn),1);mc=mp;mk=mp;msdp=mp;msdc=mp;msdk=mp;

  wai=waitbar(0,' Please Wait ...  ');
  set(wai,'NumberTitle','off','Name','Omori-Parameters - Percent done');
  drawnow

 for i=1:length(magn)       %different magnitude steps
 waitbar(i/length(magn))
 %set the error test values
  eps1=.0005;
  eps2=.0005;

%set the parameter starting values
  PO=1.1;
  CO=0.1;

%set the initial step size
  pstep=.05;
  cstep=.1;
  pp=PO;
  pc=CO;
  nit=0;
  ieflag=0;
  isflag=0;
  pcheck=0;
  err1x=0;
  err2x=0;
  ts=0.0000001;
  
%Build timecatalog

  mains=find(ttcat(:,6)==max(ttcat(:,6))); 
  mains=ttcat(mains(1),:);         %biggest shock in sequence 
  if var1==4  %input of maintime of sequence(normally onset of high seismicity)
   if i==1 
    figure(mtpl) 
    seti = uicontrol('BackGroundColor','c','Units','normal',...
                 'Position',[.4 .01 .2 .05],'String','Select Maintime');
    XLim=get(ctiplo,'XLim');
    M1b = [];
    M1b= ginput(1);
    tt3= M1b(1);
    tt4=num2str((tt3-0.22)*(1/.55)*(XLim(2)-XLim(1))+XLim(1));
    text( M1b(1),M1b(2),['|: T1=',tt4] )
    tt4=str2num(tt4);
    delete(seti);
    

    if tt4>tmp3          %maintime after selected starttime of sequence
     tt4=tmp3;
     disp('maintime was set to starttime of estimate')
    end 
    figure(wai)
   end
  end                   %end of input maintime        
  
  if par3<1             %if cumulative number curve is in days 
   
   if var1==4        %first event in sequence is mainevent if maininput         
    mains=find(ttcat(:,3)>(tt4/365+ttcat(1,3)));
    mains=ttcat(mains(1),:);
   end

   tmpcat=ttcat(find(ttcat(:,3)>=tmp3/365+ttcat(1,3) &    ttcat(:,3)<=tmp4/365+ttcat(1,3)),:);
   tmp6=tmp3/365+ttcat(1,3);

  else                 %cumulative number curve is in  years
 
   if var1==4           %first event in sequence in mainevent if maininput 
    mains=find(ttcat(:,3)>tt4);
    mains=ttcat(mains(1),:);
   end
  
   tmpcat=ttcat(find(ttcat(:,3)>=tmp3 & ttcat(:,3)<=tmp4),:);
   tmp6=tmp3;
  end
    tmp2=magn(i);
    tmpcat=tmpcat(find(tmpcat(:,6)>=tmp2 & tmpcat(:,6)<=tmp1),:);

  if var1 ==3 | var1==4
   ttt=find(tmpcat(:,3)>mains(1,3));
   tmpcat=tmpcat(ttt,:);
   tmpcat=[mains; tmpcat];
   ts=(tmp6-mains(1,3))*365;
   if ts<=0
    ts=0.0000001;
   end
  end
  tmeqtime=clustime(3); 
  tmeqtime=tmeqtime-tmeqtime(1);     %time in days relative to first eq
  tmeqtime=tmeqtime(2:length(tmeqtime)); 

  tend=tmeqtime(length(tmeqtime)); %end time 
  %Loop begins here
  nn=length(tmeqtime);
  loop=0;
  loopcheck=0;
  tt=tmeqtime(nn);
  t=tmeqtime;
  ploop(1);           %call of function who calculates parameters 
  if loopcheck<499
   mp(i)=p;              %storage of p,k,c +standard deviations
   msdp(i)=sdp;
   mk(i)=dk;
   msdk(i)=sdk;
   mc(i)=c;
   msdc(i)=sdc;
  else
   mp(i)=NaN;
   msdp(i)=NaN;
   mk(i)=NaN;
   msdk(i)=NaN;
   mc(i)=NaN;
   msdc(i)=NaN;
  end
  if msdp>mp
   msdp=mp;
  elseif msdk>mk
   msdk=mk;
  elseif msdc>mc
   msdc=mc;
  end 
 end                    %end for

 delete(wai)
 cltipval(9);
elseif var1==6 | var1==7 | var1==8

 mp=zeros(length(magn),1);mc=mp;mk=mp;msdp=mp;msdc=mp;msdk=mp;
 wai=waitbar(0,' Please Wait ...  ');
 set(wai,'NumberTitle','off','Name','Omori-Parameters - Percent done');
 drawnow
 for i=1:length(magn)       %different magnitude steps
  waitbar(i/length(magn))
%set the error test values
  eps1=.0005;
  eps2=.0005;

%set the parameter starting values
  PO=1.1;
  CO=0.1;

%set the initial step size
  pstep=.05;
  cstep=.1;
  pp=PO;
  pc=CO;
  nit=0;
  ieflag=0;
  isflag=0;
  pcheck=0;
  err1x=0;
  err2x=0;
  ts=0.0000001;
  
%Build timecatalog

  mains=find(ttcat(:,6)==max(ttcat(:,6))); 
  mains=ttcat(mains(1),:);         %biggest shock in sequence 
  if var1==7  %input of maintime of sequence(normally onset of high seismicity)
   if i==1 
    figure(mtpl) 
    seti = uicontrol('BackGroundColor','c','Units','normal',...
                 'Position',[.4 .01 .2 .05],'String','Select Maintime');
    XLim=get(ctiplo,'XLim');
    M1b = [];
    M1b= ginput(1);
    tt3= M1b(1);
    tt4=num2str((tt3-0.22)*(1/.55)*(XLim(2)-XLim(1))+XLim(1));
    text( M1b(1),M1b(2),['|: T1=',tt4] )
    tt4=str2num(tt4);
    delete(seti);

    if tt4>tmp3          %maintime after selected starttime of sequence
     tt4=tmp3;
     disp('maintime was set to starttime of estimate')
    end 
    figure(wai);
   end
  end                   %end of input maintime        
  
  if par3<1             %if cumulative number curve is in days 
   
   if var1==7        %first event in sequence is mainevent if maininput         
    mains=find(ttcat(:,3)>(tt4/365+ttcat(1,3)));
    mains=ttcat(mains(1),:);
   end
 
   tmpcat=ttcat(find(ttcat(:,3)>=tmp3/365+ttcat(1,3) &    ttcat(:,3)<=magn(i)/365+ttcat(1,3)),:);
   tmp6=tmp3/365+ttcat(1,3);

  else                 %cumulative number curve is in  years
 
   if var1==7           %first event in sequence in mainevent if maininput 
    mains=find(ttcat(:,3)>tt4);
    mains=ttcat(mains(1),:);
   end
  
   tmpcat=ttcat(find(ttcat(:,3)>=tmp3 & ttcat(:,3)<=magn(i)),:);
   tmp6=tmp3;
  end
    tmpcat=tmpcat(find(tmpcat(:,6)>=tmp2 & tmpcat(:,6)<=tmp1),:);

  if var1 ==6 | var1==7
   ttt=find(tmpcat(:,3)>mains(1,3));
   tmpcat=tmpcat(ttt,:);
   tmpcat=[mains; tmpcat];
   ts=(tmp6-mains(1,3))*365;
   if ts<=0
    ts=0.0000001;
   end
  end
  tmeqtime=clustime(3); 
  tmeqtime=tmeqtime-tmeqtime(1);     %time in days relative to first eq
  tmeqtime=tmeqtime(2:length(tmeqtime)); 

  tend=tmeqtime(length(tmeqtime)); %end time 
  %Loop begins here
  nn=length(tmeqtime);
  loop=0;
  loopcheck=0;
  tt=tmeqtime(nn);
  t=tmeqtime;
  ploop(1);           %call of function who calculates parameters 
  if loopcheck<499
   mp(i)=p;              %storage of p,k,c +standard deviations
   msdp(i)=sdp;
   mk(i)=dk;
   msdk(i)=sdk;
   mc(i)=c;
   msdc(i)=sdc;
  else
   mp(i)=NaN;
   msdp(i)=NaN;
   mk(i)=NaN;
   msdk(i)=NaN;
   mc(i)=NaN;
   msdc(i)=NaN;
  end
  if msdp>mp
   msdp=mp;
  elseif msdk>mk
   msdk=mk;
  elseif msdc>mc
   msdc=mc;
  end 
end                    %end for
 delete(wai)
 cltipval(9);

  
elseif var1==9              %plot the results
 
 [existFlag,figNumber]=figflag('Omori-Parameters');
   
 if existFlag
  figure(omori);
  clf;
 else
  omori=figure(....
   'Name','Omori-Parameters',...
   'NumberTitle','off',...
   'MenuBar','none',...
   'NextPlot','new',...
   'visible','off',...
   'Units','normalized',... 
   'Position',[ 0.435  0.8 0.5 0.8]);
  end; 
  matdraw;
%plot p-value + standard deviation
 rect = [0.15,  0.7, 0.65, 0.26];
 mpplot=axes('position',rect,'box','on');
 hold on
 plot(magn,mp,'ob')
 if tmm==0
  xlabel('Minimum Magnitude ');
 else
  xlabel('End Time of Estimate');
 end
 ylabel('p-value');
 errorbar(magn,mp,msdp);
 grid; 
    
%plot  k-value + standard deviation
 rect= [0.15,  0.38, 0.65, 0.26];
 mkplot=axes('position',rect,'box','on');
 hold on
 plot(magn,mk,'ob')
if tmm==0
  xlabel('Minimum Magnitude ');
 else
  xlabel('End Time of Estimate');
 end
 ylabel('k-value');
 errorbar(magn,mk,msdk)
 grid
 

%plot c-value +  standard deviation
 rect=[0.15,  0.06, 0.65, 0.26];
 mctiplo=axes('position',rect,'box','on');
 hold on
 plot(magn,mc,'ob');
if tmm==0
  xlabel('Minimum Magnitude ');
 else
  xlabel('End Time of Estimate');
 end
 ylabel('c-value');
 errorbar(magn,mc,msdc)
 grid
 end

  




end
  

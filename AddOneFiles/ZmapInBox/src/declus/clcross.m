%function clcross(a)
% plots current cluster catalog in a lambert-map and plots cross-sections
% postition input for cross-sections is interactive
%
% Alexander Allmann 8/95

disp('This is /src/declus/clcross.m')


global fipo  lclu winx winy wi

al=cluscat;

% Build figure lambert-map
%
[existFlag,figNumber]=figflag('Cluster Map (Lambert)',1);
newMapLaWindowFlag=~existFlag;
 
if newMapLaWindowFlag,
  lclu = figure( ...
        'Name','Cluster Map (Lambert)',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'backingstore','on',...
        'Visible','off', ...
        'Position',[ fipo(3)-600 fipo(4)-400 winx winy]);
  makebutt
  matdraw
  drawnow
end % if figure exist

figure(lclu)
delete(gca);

lc_event(al(:,2),al(:,1),'.k');

uic = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.05 .00 .40 .06],'String','Select Endpoints with cursor');

 titStr ='Create Crossection                      ';

    messtext= ...
        ['                                                '
         '  Please use the LEFT mouse button              '
         ' to select the two endpoints of the             '
         ' crossection                                    '
];
 
welcome(titStr,messtext);
%if term == 1 ; whitebg([0 0 0 ]);end

[xsecx xsecy inde] = mysect(al(:,2)',al(:,1)',al(:,7),wi);

delete(uic)

uic3 = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.80 .88 .20 .10],'String','Make Grid',...
           'callback','sel = ''in'';magrcros');

uic4 = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.80 .68 .20 .10],'String','Make b cross ',...
           'callback','sel = ''in'';bcross');

figure(lclu)
uic2 = uicontrol('BackGroundColor','y','Units','normal',...
          'Position',[.70 .92 .30 .06],'String','New selection ?',...
           'callback','delete(uic2),lammap');
set_width = uicontrol('style','edit','value',wi,...
              'string',num2str(wi), 'background','y',...
              'units','norm','pos',[.90 .00 .08 .06],'min',0,'max',10000,...
       'callback','wi=str2num(get(set_width,''String''));');
 
wilabel = uicontrol('style','text','units','norm','pos',[.60 .00 .30 .06]);
set(wilabel,'string','Width in km:','background','y');
if term == 1 ; whitebg([0 0 0 ]);end

% create the selected catalog
%
newa  = a(inde,:);
newa = [ newa xsecx'];
% call the m script that produces a grid 
sel = 'in';
%magrcros

function plotmi(var1)

global a mi fs12 term cb1 cb2 cb3 mif2 mif1

disp('This is /src/backplotmi.m');

newcat2 = a;
[existFlag,figNumber]=figflag('Misfit ',1);
figure(figNumber)

delete(gca);delete(gca);
delete(gca);delete(gca);
 
rect = [0.15,  0.15, 0.75, 0.65];
axes('position',rect)
 
if var1 == 1

  [s,is] = sort(newcat2(:,1));
  newcat2 = newcat2(is(:,1),:) ;
  mi2 = mi(is(:,1),:) ;
  pl = plot(newcat2(:,1),cumsum(mi2(:,2)),'b')
  set(pl,'LineWidth',[2.0])
  grid
  if term > 1; set(gca,'Color',[cb1 cb2 cb3]); end
  set(gca,'box','on',...
          'DrawMode','fast','TickDir','out','FontWeight',...
          'bold','FontSize',fs12,'Linewidth',[1.2])
  xlabel2('Longitude ','FontWeight','bold','FontSize',fs12)
  ylabel2('Cumulative Misfit ','FontWeight','bold','FontSize',fs12)

elseif var1 == 3;
  [s,is] = sort(newcat2(:,3));
  newcat2 = newcat2(is(:,1),:) ;
  mi2 = mi(is(:,1),:) ;
  pl = plot(newcat2(:,3),cumsum(mi2(:,2)),'b')
  set(pl,'LineWidth',[2.0])
  grid
  if term > 1; set(gca,'Color',[cb1 cb2 cb3]); end
  set(gca,'box','on',...
          'DrawMode','fast','TickDir','out','FontWeight',...
          'bold','FontSize',fs12,'Linewidth',[1.2])

  xlabel2('Time in [Years]','FontWeight','bold','FontSize',fs12)
  ylabel2('Cumulative Misfit ','FontWeight','bold','FontSize',fs12)

elseif var1 == 2;
  [s,is] = sort(newcat2(:,var1));
  newcat2 = newcat2(is(:,1),:) ;
  mi2 = mi(is(:,1),:) ;
  pl = plot(newcat2(:,var1),cumsum(mi2(:,2)),'b')
  set(pl,'LineWidth',[2.0])
  grid
  if term > 1; set(gca,'Color',[cb1 cb2 cb3]); end
  set(gca,'box','on',...
          'DrawMode','fast','TickDir','out','FontWeight',...
          'bold','FontSize',fs12,'Linewidth',[1.2])

  xlabel2('Latitude ','FontWeight','bold','FontSize',fs12)
  ylabel2('Cumulative Misfit ','FontWeight','bold','FontSize',fs12)

elseif var1 == 4;
  [s,is] = sort(newcat2(:,6));
  newcat2 = newcat2(is(:,1),:) ;
  mi2 = mi(is(:,1),:) ;
  pl = plot(newcat2(:,6),cumsum(mi2(:,2)),'b')
  set(pl,'LineWidth',[2.0])
  grid
  if term > 1; set(gca,'Color',[cb1 cb2 cb3]); end
  set(gca,'box','on',...
          'DrawMode','fast','TickDir','out','FontWeight',...
          'bold','FontSize',fs12,'Linewidth',[1.2])

  xlabel2('Magnitude ','FontWeight','bold','FontSize',fs12)
  ylabel2('Cumulative Misfit ','FontWeight','bold','FontSize',fs12)

elseif var1 == 5;
  [s,is] = sort(newcat2(:,7));
  newcat2 = newcat2(is(:,1),:) ;
  mi2 = mi(is(:,1),:) ;
  pl = plot(newcat2(:,7),cumsum(mi2(:,2)),'b')
  set(pl,'LineWidth',[2.0])
  grid
  if term > 1; set(gca,'Color',[cb1 cb2 cb3]); end
  set(gca,'box','on',...
          'DrawMode','fast','TickDir','out','FontWeight',...
          'bold','FontSize',fs12,'Linewidth',[1.2])

  xlabel2('Depth in [km] ','FontWeight','bold','FontSize',fs12)
  ylabel2('Cumulative Misfit ','FontWeight','bold','FontSize',fs12)


end   % if var1 


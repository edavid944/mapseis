function ci =  cusum(cat)
% This function calculates the CUMSUm function (Page 1954).
% 

disp('This is /src/cusum.m')

[existFlag,figNumber]=figflag('CUSUM',1);
if existFlag
 cfig = figNumber;
else
 cfig=figure(...                  %build figure for plot
          'Units','normalized','NumberTitle','off',...
          'Name','CUSUM',...
          'MenuBar','none',...
          'visible','off',...
          'pos',[ 0.300  0.3 0.4 0.6]);
  ho = 'noho';
  makebut2
  matdraw
end   % if fig exist


m  = cat(:,6);
me = mean(m);
i = (1:1:length(m));
ci = cumsum(m)' - i.*me;

figure(cfig)
delete(gca);delete(gca);
plot(cat(:,3),ci,'o')
%plot(i,ci,'o')
set(gca,'visible','on','FontSize',[10],'FontWeight','normal',...
    'FontWeight','normal','LineWidth',[1.0],...
    'Box','on')
xlabel('Time [yrs]')
ylabel('CUSUM [yrs]')
grid



% This subroutine assigns Parameter values for the grid
% which is used to calculate a Max Z value map.
% dx, dy is the grid spacing in degrees.
% For each one of the grid points, Ni events are counted.
% 

disp('This is /src/inmagr3d.m');

% initial values
% 
dx = 1.00;
dy = 1.00 ;
dz = 10.00 ;
ni = 100;

%
% make the interface 
% 
figure(...
        'Name','Grid Input Parameter',...
        'NumberTitle','off', ...
        'MenuBar','none', ...
        'NextPlot','new', ...
        'units','points',...
        'Visible','off', ...
        'Position',[ wex+200 wey-200 450 250]);
axis off

% creates a dialog box to input grid parameters
%
    freq_field=uicontrol('BackGroundColor','g','Style','edit',...
         'Position',[.60 .50 .22 .10],...
        'Units','normalized','String',num2str(ni),...
        'CallBack','ni=str2num(get(freq_field,''String'')); set(freq_field,''String'',num2str(ni));');

    freq_field2=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.60 .40 .22 .10],...
        'Units','normalized','String',num2str(dx),...
        'CallBack','dx=str2num(get(freq_field2,''String'')); set(freq_field2,''String'',num2str(dx));');

    freq_field3=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.60 .30 .22 .10],...
        'Units','normalized','String',num2str(dy),...
        'CallBack','dy=str2num(get(freq_field3,''String'')); set(freq_field3,''String'',num2str(dy));');

    freq_field6=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.60 .20 .22 .10],...
        'Units','normalized','String',num2str(par1),...
        'CallBack','par1=str2num(get(freq_field6,''String'')); set(freq_field6,''String'',num2str(par1));');

    freq_field7=uicontrol('BackGroundColor','g','Style','edit',...
        'Position',[.60 .60 .22 .10],...
        'Units','normalized','String',num2str(dz),...
        'CallBack','dz=str2num(get(freq_field7,''String'')); set(freq_field7,''String'',num2str(dz));');

uicontrol('BackGroundColor','y','Units','normal','Position',[.1 .90 .15 .12],'String','LoadGrid','callback','close;loadgrid')

    close_button=uicontrol('BackGroundColor','y','Style','Pushbutton',...
        'Position',[.60 .05 .15 .12 ],...
        'Units','normalized','Callback','close;done','String','Cancel');

    go_button1=uicontrol('BackGroundColor','y','Style','Pushbutton',...
        'Position',[.20 .05 .15 .12 ],...
        'Units','normalized',...
        'Callback',' gomakegr',...
        'String','ZmapGrid');

    go_button2=uicontrol('BackGroundColor','y','Style','Pushbutton',...
        'Position',[.40 .05 .15 .12 ],...
        'Units','normalized',...
        'Callback','close;think; genascum',...
        'String','GenasGrid');
 

  txt3 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0.30 0.94 0 ],...
                'Rotation',0 ,...
                'FontSize',fs14 ,...
                'FontWeight','bold',... 
                'String',' Grid Parameter ');


  txt5 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0. 0.42 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold',... 
                'String','Spacing in x (dx) in deg:');

  txt6 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0. 0.32 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold',... 
                'String','Spacing in y (dy) in deg:');

   text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0. 0.20 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12 ,...
                'FontWeight','bold',... 
                'String','Time steps in days:');


  txt1 = text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0. 0.53 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12,...
                'FontWeight','bold',...
                'String','Number of Events (Ni):');

         text(...
                'Color',[0 0 0 ],...
                'EraseMode','normal',...
                'Position',[0. 0.63 0 ],...
                'Rotation',0 ,...
                'FontSize',fs12,...
                'FontWeight','bold',...
                'String','Depth increment in (km) :');


  if term == 1 ; whitebg(gcf,[1 1 1 ]);end
  set(gcf,'visible','on');
  watchoff

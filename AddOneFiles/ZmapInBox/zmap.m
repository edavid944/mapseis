% This files start up ZMAP. 
%  
% The matlab searchpathes are updated, existing windows closed. 
% 
%  Stefan Wiemer  12/94

%system_dependent(14,'on')
disp('This is zmap.m - version 6.0 ')

% read the welcome screen
rand('state',sum(100*clock));
r = ceil(rand(1,1)*21);
cd slides
if r <10
    str = [ 'Slide' num2str(r,1) '.JPG'];
else
    str = [ 'Slide' num2str(r,2) '.JPG'];
end

do = [ '[x,imap] = imread(str);']; err =  ''; 
eval(do,err); 

if exist('x') == 1  
    figure('Menubar','none','NumberTitle','off'); fi0 = gcf;
    axes('pos',[0 0 1 1]);axis off; axis ij
    image(x)
    drawnow
    close
end

cd ..

% Get the screensizxe and color
global hodi c1 c2 c3 sys fs12 bfig xsec_fig teb t0b ho a sax1 sax2
global mess  cum freq_field histo hisvar strii1 strii2 fs12 fs10 fs14
global torad Re scale cb1 cb2 cb3 lat1 lon1 lat2 lon2 leng pos calSave9
global freq_field1 freq_field2 freq_field3 freq_field4 Go_p_button maepi
global seismap dx dy ni xt3 bvalsum3 bmapc newa2 b1 b2 n1 n2 aw bw ho
%set up global variables
global mess term sys wex wey welx wely fs10 fs12 fs14 c1 c2 c3 action_button
global c1 c2 c3 hodi sys cputype figp ptt pri cb1 cb2 cb3
global newcat equi clus eqtime bg original ttcat file1 cputype


% temporarily turn off all warnings...
warning off

fipo = get(0,'ScreenSize');
hodi = cd;
fipo(4) = fipo(4)-150;
term = get(0,'ScreenDepth');

% Set up the different compuer systems
sys = computer;
cputype = computer;
cputype = sys;

ve = version;
ve = str2num(ve(1:3));
if ve < 5
    messtext = [' Warning: You are running a version of matlab '
        ' older than 5.0. ZMAP may not always be       '
        ' compatibel with your version!                '];
    errordlg(messtext,'Warning!')
    pause(5)
end
if sys(1:3) ~= 'PCW' & sys(1:3) ~= 'SOL' & sys(1:3) ~= 'SUN' & sys(1:3) ~= 'MAC'   & sys(1:3) ~= 'HP7' & sys(1:3) ~='LNX'
    errordlg(' Warning: ZMAP has not been tested on this computer type!','Warning!')
    pause(5)
    sys(1:3) == 'SOL';
end
   
% set some of the paths 
    fs = filesep;
    hodo = [hodi fs 'out' fs];
    hoda = [hodi fs 'eq_data' fs];
    p = path;
    addpath([hodi fs 'myfiles'],[hodi fs 'src'],[hodi fs 'src' fs 'utils'],[hodi fs 'src' fs 'declus'],...
        [hodi fs 'src' fs 'fractal'], [hodi fs 'help'],[hodi fs 'dem'],[hodi fs 'zmapwww'],[hodi fs 'importfilters'],...
        [hodi fs  fs 'src' fs 'utils' fs 'eztool'],[hodi fs 'm_map'],[hodi fs 'src' fs 'pvals'],[hodi], [hodi fs 'src' fs 'synthetic'], ...
        [hodi fs 'src' fs 'movies'],[hodi fs 'src' fs 'danijel'],[hodi fs 'src' fs 'danijel' fs 'calc'],...
        [hodi fs 'src' fs 'danijel' fs 'ex'],[hodi fs 'src' fs 'danijel' fs 'gui'],...
        [hodi fs 'src' fs 'danijel' fs 'focal'],...
        [hodi fs 'src' fs 'danijel' fs 'plot'],[hodi fs 'src' fs 'danijel' fs 'probfore'],...
        [hodi fs 'src' fs 'jochen'], [hodi fs 'src' fs 'jochen' fs 'seisvar' fs 'calc'],...
        [hodi fs 'src' fs 'jochen' fs 'seisvar'], [hodi fs 'src' fs 'jochen' fs 'ex'],...
        [hodi fs 'src' fs 'thomas' fs 'slabanalysis'], [hodi fs 'src' fs 'thomas' fs 'seismicrates'],[hodi fs 'src' fs 'thomas' fs 'montereason'],...
        [hodi fs 'src' fs 'thomas' fs 'gui'],...
        [hodi fs 'src' fs 'jochen' fs 'plot'], [hodi fs 'src' fs 'jochen' fs 'stressinv'], [hodi fs 'src' fs 'jochen' fs 'auxfun'],...
        [hodi fs 'src' fs 'thomas'], ...
        [hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
        [hodi fs 'src' fs 'thomas' fs 'montereason'], ...
        [hodi fs 'src' fs 'thomas' fs 'etas'],...
        [hodi fs 'src' fs 'thomas' fs 'decluster'],...
        [hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
        [hodi fs 'src' fs 'thomas'], ...
        [hodi fs 'src' fs 'thomas' fs 'seismicrates'], ...
        [hodi fs 'src' fs 'thomas' fs 'montereason'], ...
        [hodi fs 'src' fs 'thomas' fs 'etas'],...
        [hodi fs 'src' fs 'thomas' fs 'decluster'],...
        [hodi fs 'src' fs 'thomas' fs 'decluster' fs 'reasen'],...
        [hodi fs 'src' fs 'juerg' fs 'misc'],...
        [hodi fs 'src' fs 'afterrate']);

 % set some initial variables 
 ini_zmap  


%Create the 5 data categories
main = [];
mainfault = [];
coastline = [];
well = [];
stat = [];
a = [];
faults = [];


% set a whitebackground if the terminal is black and white
% Does not alway work

if term  == 1 ; 
    whitebg([0 0 0 ])
    c1 = 0;
    c2 = 0;
    c3 = 0;
    cb1 = 0;
    cb2 = 0;
    cb3 = 0;
end

%set(0,'DefaultFigurePosition',[wex wey welx wely]')
%set(0,'DefaultFigureColor',[1,1,1]) %%N.B this has side %%effects. 
set(0,'DefaultAxesFontName','Arial') 
set(0,'DefaultTextFontName','Arial') 
%set(0,'DefaultAxesFontSize',12) 
%set(0,'DefaultTextFontSize',12) 
%set(0,'DefaultAxesTickDir','out') 
set(0,'DefaultAxesTickLength',[0.01 0.01]) 

set(0,'DefaultFigurePaperPositionMode','auto')


% find out what computer we are on
% open message window

message_zmap 
think
echo off
my_dir = hodi;
% open selection window
startmen
done; %close(fi0)
%set(gcf,'Units','pixel','position', [100 200 300 250])

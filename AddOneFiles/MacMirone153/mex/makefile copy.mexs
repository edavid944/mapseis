#		$Id: makefile.in,v 1.3 22/08/2007
#
#	Makefile for mirone mexs
#	make -f makefile.mexs
#	Use with GMT 4.5
#

GMTSRCDIR = /Users/j/programs/GMT/src/
#include $(GMTSRCDIR)makegmt.macros
#include $(GMTSRCDIR)gmtalldeps.macros
include $(GMTSRCDIR)config.mk
include $(GMTSRCDIR)common.mk


#----------------------------------------------------------------------------
# Where install will place the files.  Notice that it must allways be ..../"mirone root"/lib_mex

mirone_mex_dir	= /Users/j/programs/mironeTrunk/lib_mex
MATLAB		= /Applications/MATLAB_R2009b.app

MATLAB_MEX	= maci64
MEX_EXT		= mexmaci64

#----------------------------------------------------------------------------
#	The rest should should be ok. WELL, IF ON *NIX THEY WERE NOT CHANGING THINGS ALL THE TIME
#----------------------------------------------------------------------------

MEX	= $(MATLAB)/bin/mex
#MEXLIB	= -L$(MATLAB)/extern/lib/$(MATLAB_MEX) -lmat
MEXLIB	= -L$(MATLAB)/bin/$(MATLAB_MEX) -lmat
GMT     = -L.. -L$(libdir) -lgmt -lpsl
GMT_MGG = -L.. -L$(libdir) -lgmt_mgg
CDF     = $(NETCDF_LIB)
FLAGS	= -I$(srcdir) -I$(MATLAB)/extern/include $(NETCDF_INC)
MGG_FLAGS	= -I$(srcdir) -I$(srcdir)/mgg -I$(srcdir)/x_system -I$(MATLAB)/extern/include $(NETCDF_INC)
GDAL_LIB = -L/usr/local/lib/ -lgdal
GDAL_FLAGS = -I/usr/local/include -I$(MATLAB)/extern/include
OCV_LIB = -L/usr/local/lib/ -lcv -lcxcore #-lcvhaartraining      # used on v1.0
OCV_FLAGS = -I/usr/local/include/opencv -I$(MATLAB)/extern/include
SHAPE_LIB = -L/usr/local/lib/ -lshp
SHAPE_FLAGS = -I/usr/local/include -I$(MATLAB)/extern/include
MEXNC_FLAGS = $(NETCDF_INC) -I$(MATLAB)/extern/include

PROG_GMT	= grdinfo_m grdproject_m grdread_m grdsample_m grdtrend_m grdwrite_m mapproject_m shoredump surface_m nearneighbor_m grdfilter_m cpt2cmap grdlandmask_m grdppa_m gmtlist_m

# Non LIB dependent mexs (besides matlab libs, of course)
PROG_SIMPLE	= test_gmt igrf_m scaleto8 tsun2 wave_travel_time mansinha_m telha_m range_change country_select \
		mex_illuminate grdutils read_isf alloc_mex susan set_gmt mxgridtrimesh houghmex trend1d_m \
		gmtmbgrid_m grdgradient_m grdtrack_m mirblock akimaspline applylutc bwlabel1 bwlabel2 bwlabelnmex \
		bwboundariesmex cq ditherc imhistc intlutc inv_lwm grayto8 grayto16 grayxform morphmex \
		ordf parityscan resampsep

# netCDF mexs (other than GMT ones)
PROG_withCDF	= swan
PROG_GDAL		= gdalread gdalwrite ogrproj gdalwarp_mex gdaltransform_mex
PROG_MEXNC	= mex_nc
PROG_OCV		= cvlib_mex
PROG_SHAPE	= mex_shape
PROG_EDISON	= edison_

all:			$(PROG_GMT) $(PROG_GDAL) $(PROG_SIMPLE) $(PROG_withCDF) $(PROG_OCV) $(PROG_SHAPE) $(PROG_MEXNC) $(PROG_EDISON)
gmt:			$(PROG_GMT)
gdal:		$(PROG_GDAL)
mexnc:		$(PROG_MEXNC)
opencv:		$(PROG_OCV)
shape:		$(PROG_SHAPE)
simple:		$(PROG_SIMPLE)
withCDF:		$(PROG_withCDF)
edison:		$(PROG_EDISON)

install:
		for f in *.$(MEX_EXT); do \
			$(INSTALL) $$f $(mirone_mex_dir); \
		done

cpt2cmap:	
		$(MEX) $(FLAGS) cpt2cmap.c $(GMT) $(CDF) $(MEXLIB)
gmtlist_m:	
		$(MEX) $(MGG_FLAGS) gmtlist_m.c $(GMT) $(GMT_MGG) $(MEXLIB)
grdinfo_m:	
		$(MEX) $(FLAGS) grdinfo_m.c $(GMT) $(CDF) $(MEXLIB) 
grdread_m:	
		$(MEX) $(FLAGS) grdread_m.c $(GMT) $(CDF) $(MEXLIB) 
grdwrite_m:	
		$(MEX) $(FLAGS) grdwrite_m.c $(GMT) $(CDF) $(MEXLIB)
grdsample_m:	
		$(MEX) $(FLAGS) grdsample_m.c $(GMT) $(CDF) $(MEXLIB)
grdppa_m:	
		$(MEX) $(FLAGS) grdppa_m.c $(GMT) $(CDF) $(MEXLIB)
grdfilter_m:	
		$(MEX) $(FLAGS) grdfilter_m.c $(GMT) $(CDF) $(MEXLIB)
grdlandmask_m:	
		$(MEX) $(FLAGS) grdlandmask_m.c $(GMT) $(CDF) $(MEXLIB)
grdproject_m:	
		$(MEX) $(FLAGS) grdproject_m.c $(GMT) $(CDF) $(MEXLIB)
grdtrend_m:	
		$(MEX) $(FLAGS) grdtrend_m.c $(GMT) $(CDF) $(MEXLIB)
mapproject_m:	
		$(MEX) $(FLAGS) mapproject_m.c $(GMT) $(CDF) $(MEXLIB)
nearneighbor_m:	
		$(MEX) $(FLAGS) nearneighbor_m.c $(GMT) $(CDF) $(MEXLIB)
shoredump:	
		$(MEX) $(FLAGS) shoredump.c $(GMT) $(CDF) $(MEXLIB)
surface_m:	
		$(MEX) $(FLAGS) surface_m.c $(GMT) $(CDF) $(MEXLIB)

# -- "PROG_withCDFs" netCDF mexs (other than GMT ones)

swan:	
		$(MEX) -I$(MATLAB)/extern/include  $(NETCDF_INC) swan.c $(CDF) $(MEXLIB)

# ---------------------------------------------------------------------------------------
# -- "Simple progs" that is, those that don't link agains nothing else than matlab itself

test_gmt:	
		$(MEX) -I$(MATLAB)/extern/include test_gmt.c $(MEXLIB)
igrf_m:	
		$(MEX) -I$(MATLAB)/extern/include igrf_m.c $(MEXLIB)
scaleto8:	
		$(MEX) -I$(MATLAB)/extern/include scaleto8.c $(MEXLIB)
tsun2:	
		$(MEX) -I$(MATLAB)/extern/include tsun2.c $(MEXLIB)
wave_travel_time:	
		$(MEX) -I$(MATLAB)/extern/include wave_travel_time.c $(MEXLIB)
mansinha_m:	
		$(MEX) -I$(MATLAB)/extern/include mansinha_m.c $(MEXLIB)
telha_m:	
		$(MEX) -I$(MATLAB)/extern/include telha_m.c $(MEXLIB)
range_change:	
		$(MEX) -I$(MATLAB)/extern/include range_change.c $(MEXLIB)
country_select:	
		$(MEX) -I$(MATLAB)/extern/include country_select.c $(MEXLIB)
mex_illuminate:	
		$(MEX) -I$(MATLAB)/extern/include mex_illuminate.c $(MEXLIB)
grdutils:	
		$(MEX) -I$(MATLAB)/extern/include grdutils.c $(MEXLIB)
read_isf:	
		$(MEX) -I$(MATLAB)/extern/include read_isf.c $(MEXLIB)
ind2rgb8:	
		$(MEX) -I$(MATLAB)/extern/include ind2rgb8.c $(MEXLIB)
alloc_mex:	
		$(MEX) -I$(MATLAB)/extern/include alloc_mex.c $(MEXLIB)
susan:	
		$(MEX) -I$(MATLAB)/extern/include susan.c $(MEXLIB)
set_gmt:	
		$(MEX) -I$(MATLAB)/extern/include set_gmt.c $(MEXLIB)
houghmex:	
		$(MEX) -I$(MATLAB)/extern/include houghmex.cpp $(MEXLIB)
mxgridtrimesh:	
		$(MEX) -I$(MATLAB)/extern/include mxgridtrimesh.c $(MEXLIB)
grdgradient_m:
		$(MEX) -I$(MATLAB)/extern/include grdgradient_m.c $(MEXLIB)
grdtrack_m:
		$(MEX) -I$(MATLAB)/extern/include grdtrack_m.c $(MEXLIB)
gmtmbgrid_m:
		$(MEX) -I$(MATLAB)/extern/include gmtmbgrid_m.c $(MEXLIB)
trend1d_m:
		$(MEX) -I$(MATLAB)/extern/include trend1d_m.c $(MEXLIB)
PolygonClip:
		$(MEX) -I$(MATLAB)/extern/include PolygonClip.c gpc.c $(MEXLIB)
akimaspline:
		$(MEX) -I$(MATLAB)/extern/include akimaspline.cpp $(MEXLIB)
mirblock:
		$(MEX) -I$(MATLAB)/extern/include mirblock.c $(MEXLIB)
applylutc:
		$(MEX) -I$(MATLAB)/extern/include applylutc.c $(MEXLIB)
bwlabel1:
		$(MEX) -I$(MATLAB)/extern/include bwlabel1.c $(MEXLIB)
bwlabel2:
		$(MEX) -I$(MATLAB)/extern/include bwlabel2.c $(MEXLIB)
cq:
		$(MEX) -I$(MATLAB)/extern/include cq.c $(MEXLIB)
ditherc:
		$(MEX) -I$(MATLAB)/extern/include ditherc.c invcmap.c $(MEXLIB)
grayto8:
		$(MEX) -I$(MATLAB)/extern/include grayto8.c $(MEXLIB)
grayto16:
		$(MEX) -I$(MATLAB)/extern/include grayto16.c $(MEXLIB)
imhistc:
		$(MEX) -I$(MATLAB)/extern/include imhistc.c $(MEXLIB)
intlutc:
		$(MEX) -I$(MATLAB)/extern/include intlutc.c $(MEXLIB)
inv_lwm:
		$(MEX) -I$(MATLAB)/extern/include inv_lwm.c $(MEXLIB)
ordf:
		$(MEX) -I$(MATLAB)/extern/include ordf.c $(MEXLIB)
parityscan:
		$(MEX) -I$(MATLAB)/extern/include parityscan.c $(MEXLIB)
bwlabelnmex:
		$(MEX) -I$(MATLAB)/extern/include bwlabelnmex.cpp neighborhood.cpp unionfind.c $(MEXLIB)
bwboundariesmex:
		$(MEX) -I$(MATLAB)/extern/include bwboundariesmex.cpp boundaries.cpp $(MEXLIB)
grayxform:
		$(MEX) -I$(MATLAB)/extern/include grayxform.cpp $(MEXLIB)
imreconstructmex:
		$(MEX) -I$(MATLAB)/extern/include imreconstructmex.cpp $(MEXLIB)
morphmex:
		$(MEX) -I$(MATLAB)/extern/include morphmex.cpp dilate_erode_gray_nonflat.cpp dilate_erode_packed.cpp dilate_erode_binary.cpp neighborhood.cpp vectors.cpp $(MEXLIB)
resampsep:
		$(MEX) -I$(MATLAB)/extern/include resampsep.cpp $(MEXLIB)

#imfilter_mex:

# ------------------------- GDAL progs -----------------------------------
gdalread:
		$(MEX) $(GDAL_FLAGS) gdalread.c $(GDAL_LIB) $(MEXLIB)
gdalwrite:
		$(MEX) $(GDAL_FLAGS) gdalwrite.c $(GDAL_LIB) $(MEXLIB)
gdalwarp_mex:
		$(MEX) $(GDAL_FLAGS) gdalwarp_mex.cpp $(GDAL_LIB) $(MEXLIB)
ogrproj:
		$(MEX) $(GDAL_FLAGS) ogrproj.cpp $(GDAL_LIB) $(MEXLIB)
gdaltransform_mex:
		$(MEX) $(GDAL_FLAGS) gdaltransform_mex.cpp $(GDAL_LIB) $(MEXLIB)


# ------------------------- MEXNC progs -----------------------------------
mex_nc:
		$(MEX) $(MEXNC_FLAGS) mexnc/mexgateway.c mexnc/netcdf2.c mexnc/netcdf3.c mexnc/common.c -output mexnc $(CDF) $(MEXLIB)

# ------------------------- OPEN_CV progs ---------------------------------
cvlib_mex:
		$(MEX) $(OCV_FLAGS) cvlib_mex.c sift/sift.c sift/imgfeatures.c sift/kdtree.c sift/minpq.c  $(OCV_LIB) $(MEXLIB)

# ------------------------- SHAPELIB progs ---------------------------------
mex_shape:	
		$(MEX) $(SHAPE_FLAGS) mex_shape.c $(SHAPE_LIB) $(MEXLIB)

# ------------------------- EDISON prog ------------------------------------
edison_:
		$(MEX) edison/edison_wrapper_mex.cpp edison/segm/ms.cpp  edison/segm/msImageProcessor.cpp \
		edison/segm/msSysPrompt.cpp edison/segm/RAList.cpp edison/segm/rlist.cpp edison/edge/BgEdge.cpp \
		edison/edge/BgImage.cpp edison/edge/BgGlobalFc.cpp edison/edge/BgEdgeList.cpp edison/edge/BgEdgeDetect.cpp $(MEXLIB)

spotless:	clean
		\rm -f makefile config.cache config.log config.status

clean:
		\rm -f *.$(MEX_EXT) .gmt* .mexrc.sh
		for f in $(PROG); do \
			rm -f $$.$(MEX_EXT); \
		done

#!/usr/bin/perl -w
use Math::Trig;

# This script inverts for a 2D spatially and/or temporally-varying stress field.  The "input" file should contain one 
# header line, and then a line for each event, with: x grid node, y grid node, dip direction, dip, rake.

# parameters to be set by user:

# damping parameter
$csm=1.4;

# input and output file names
$infile="test_data.txt";
$outfile="test_data.oput";

# end of parameters to set

# invert for best stress tensor
system("rm Xtemp*");
system("cp $infile Xtemp");
system("satsi_2D Xtemp Xtemp.oput $csm");
system("mv Xtemp.oput $outfile");

system("rm Xtemp*");


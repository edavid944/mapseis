#!/usr/bin/perl -w
use Math::Trig;

# This script tests a suite of damping parameters.  The resulting "summary" file contains the data misfit and model
# variance for each trial damping value, which can be plotted to determine the damping value that best jointly
# minimizes the misfit and variance.  The "input" file should contain one header line, and then a line for each
# event, with: x grid node, y grid node, dip direction, dip, rake.

# parameters to be set by user:

# list of trial damping values
@csm_list=(0,0.02,0.05,0.1,0.2,0.3,0.4,0.6,0.8,1,1.2,1.4,1.6,1.8,2,2.25,2.5,2.75,3,3.5,4,5,6,8,10,20,50);

# input and output file names
$infile="test_data.txt";
$outfile="test_data.summary";

# end of parameters to set

system("rm Xtemp*");
system("cp $infile Xtemp");
open(OUTFILE,">>Xtemp.outfile");
print OUTFILE "damping_parameter data_misfit model_length\n";
close(OUTFILE);
foreach $csm (@csm_list)
  {
    open(OUTFILE,">>Xtemp.outfile");
    print OUTFILE "$csm ";
    close(OUTFILE);
    print "$csm\n";
    system("satsi_2D_tradeoff Xtemp Xtemp.outfile $csm");
  }
system("mv Xtemp.outfile $outfile");
system("rm Xtemp*");
#!/usr/bin/perl -w
use Math::Trig;

# This script inverts for a 2D spatially and/or temporally-varying stress field.  The "input" file should contain one 
# header line, and then a line for each event, with: x grid node, y grid node, dip direction, dip, rake.

# parameters to be set by user:

# damping parameter
$csm=1.4;

# number of bootstap resamplings for uncertainty estimates (1000 recommended)
$nboot=30;

# fraction of correctly-picked fault planes (0.5 if chosen randomly)
$fplane=0.5;

# confidence level (e.g. 95 for 95%)
$conf=95;

# minimum number of earthquake per node to return stress tensor
$evmin=8;

# range of values of grid nodes
$minx=0;   # range of x grid nodes
$maxx=13;
$miny=0;   # range of y grid nodes
$maxy=6;

# input and output file names
$infile="test_data.txt";
$outfile="test_data_tp.smooth";

# end of parameters to set


# count how many earthquakes at each node
$nx=$maxx-$minx+1;
$ny=$maxy-$miny+1;
for ($ix=0;$ix<$nx;$ix++)
  {
    for ($iy=0;$iy<$ny;$iy++)
      {
        $index=$ix*$ny+$iy;
        $count[$index]=0;
      }
  }
system("rm Xtemp*");
system("cp $infile Xtemp");
open(INFILE,"Xtemp");
<INFILE>;
while(<INFILE>)
  {
    chomp;
    ($ix,$iy,@grbg)=split;
    $index=$ix*$ny+$iy;
    $count[$index]=$count[$index]+1;
  }
close(INFILE);

# invert for best stress tensor, and perform bootstrap
system("satsifast_2D Xtemp $csm");
system("bootmech_2D Xtemp $nboot $fplane $csm");

# compute confidence regions for principal axes from bootstrap results
open(OUTFILE,">Xtemp.outfile1");
for ($ix=0;$ix<$nx;$ix++)
  {
    for ($iy=0;$iy<$ny;$iy++)
      {
        print "$ix $iy\n";
        system("awk \'\$1==$ix && \$2==$iy {print \$3,\$4,\$5,\$6,\$7,\$8,\$9}\' Xtemp.slboot > Xtemp3.slboot");
        $index=$ix*$ny+$iy;
        if ($count[$index]>=$evmin)
          {
            print OUTFILE "$ix $iy\n";
            system("boot_uncert Xtemp3.slboot Xtemp.outfile2 $conf");
          }
      }
  }
close(OUTFILE);
system("paste Xtemp.outfile1 Xtemp.outfile2 > $outfile");
system("rm Xtemp*");



SATSI (Spatial And Temporal Stress Inversion)
by Jeanne Hardebeck and Andy Michael

README - 12/26/2006

SATSI (Spatial And Temporal Stress Inversion) is a modified version of Michael's code (JGR 1984, 1987), that inverts focal
mechanism data for a spatially and/or temporally varying stress field.  The inversion finds the least complex stress field
model that is consistent with the data, using an adaptive smoothing method that discriminates between variations that are
or aren't strongly required by the data and retains only variations that are well-resolved.  The technique is described
and validated in Hardebeck and Michael (JGR 111, B11310, doi:10.1029/2005JB004144, 2006.)  Please cite this publication if
you use SATSI in your research.

This directory contains C codes implementing the inversion method for 2D and 4D stress fields (1D and 3D fields can be
treated as simplified cases, e.g. a 1D problem can be posed as a 2D problem with one coordinate held constant), and
example data and Perl scripts.

This directory contains the following files:

satsi_2D.c              - finds best-fit stress field for 2D case, full output
satsi_2D_tradeoff.c     - finds model length vs variance trade-off for 2D case
satsi_4D.c              - finds best-fit stress field for 4D case, full output
satsi_4D_tradeoff.c     - finds model length vs variance trade-off for 4D case
satsifast_2D.c          - finds best-fit stress field for 2D case, short output
satsifast_4D.c          - finds best-fit stress field for 4D case, short output
bootmech_2D.c           - bootstrap resampling for uncertainty estimation, 2D case
bootmech_4D.c           - bootstrap resampling for uncertainty estimation, 4D case
boot_uncert.c           - translates bootstrap results to uncertainty in trend and plunge

dirplg.c          - subroutines (called by the above programs)
eigen.c
leasq_sparse.c
myrand.c
slfast_2D.c
slfast_4D.c
sort.c
stridip.c
switchsub.c

Makefile - Unix makefile to compile the source code

invert_damping_parameter.pl  - example Perl script to determine the model length vs variance trade-off curve
invert_smooth.pl             - example Perl script to find the best-fit stress field
invert_smooth_uncert.pl      - example Perl script to find the best-fit stress field and uncertainty

test_data.txt          - example input data for the example Perl scripts
 
test_data.summary_annotated      - annotated output from invert_damping_parameter.pl
test_data.oput_annotated         - annotated output from invert_smooth.pl
test_data_tp.smooth_annotated    - annotated output from invert_smooth_uncert.pl (Your results will not _exactly_ match 
                                                          this file, due to the random nature of bootstrap resampling.)



EXAMPLE INPUT:

2D CASE:

header line
0 1 215 63 161
...
0: x-coordinate (all indices can start at 0)
1: y-coordinate 
215: dip-direction 
63: dip 
161: rake
(for a 1D problem, just set the y-coordinate to 0 for all data)

4D CASE:

header line
0 1 3 2 215 63 161
...
0: x-coordinate 
1: y-coordinate 
3: z-coordinate
2: time-coordinate
215: dip-direction 
63: dip 
161: rake
(for a 3D problem, just set the z-coordinate or time-coordinate to 0 for all data)


OUTPUT:
- annotated output file are included
- stress orientations can be plotted using GMT (http://gmt.soest.hawaii.edu/) functions: 
   - for bars: psxy -SV
   - for "strain" crosses: psvelo -Sx
   - for uncertainty wedges:  psvelo -Sw

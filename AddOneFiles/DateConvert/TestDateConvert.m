function TestDateConvert(doSpeed)
% Automatic test: DateConvert
% This is a routine for automatic testing. It is not needed for processing and
% can be deleted or moved to a folder, where it does not bother.
%
% TestDateConvert(doSpeed)
% INPUT:
%   doSpeed: Optional logical flag to trigger time consuming speed tests.
%            Default: TRUE. If no speed test is defined, this is ignored.
% OUTPUT:
%   On failure the test stops with an error.
%
% Tested: Matlab 6.5, 7.7, 7.8, WinXP
% Author: Jan Simon, Heidelberg, (C) 2009-2010 matlab.THISYEAR(a)nMINUSsimon.de

% $JRev: R0f V:005 Sum:m/0kCSlQm6w2 Date:02-Jul-2010 08:50:10 $
% $License: NOT_RELEASED $
% $File: Tools\UnitTests_\TestDateConvert.m $
% History:
% 005: 02-Jul-2010 08:37, BUGFIX: No call to isEqualTol anymore.

% Initialize: ==================================================================
FuncName = mfilename;

if nargin == 0
   doSpeed = true;
end

% Do the work: =================================================================
% Hello:
disp(['==== Test DateConvert:  ', datestr(now, 0), char(10)]);

% Number format:
C  = datestr(now, 0);
N  = DateConvert(C, 'number');
Nm = datenum(C);
if isequal(N, Nm) == 0
   error(['*** ', FuncName, ': "number" failed for current date.']);
end

maxDate = datenum('31-Dec-9999');
minDate = datenum('01-Jan-0100');
Npool   = minDate + rand(10000, 1) * (maxDate - minDate);
Npool   = datenummx(fix(datevec(Npool)));  % Clean invalid dates as min >= 60
Strpool = datestr(Npool);
for iPool = 1:length(Npool)
   if isequal(Npool(iPool), DateConvert(Strpool(iPool, :), 'number')) == 0
      error(['*** ', FuncName, ': Failed to convert to "number": ', ...
            Strpool(iPool, :)]);
   end
end
disp('  ok: convert string -> number');

% Number format:
N  = DateConvert(C, 'vector');
Nm = datevec_fix(C);  % FIX DATEVEC BUG
if isequal(N, Nm) == 0
   error(['*** ', FuncName, ': "vector" failed for current date.']);
end

Vpool = datevecmx(Npool, 1);
for iPool = 1:length(Npool)
   if max(abs(Vpool(iPool, :) - DateConvert(Strpool(iPool, :), 'vector'))) > ...
         1.1574e-8  % 1/1000 sec
      error(['*** ', FuncName, ': Failed to convert to "vector": ', ...
            Strpool(iPool, :)]);
   end
end
disp('  ok: convert string -> vector');

% String format:
Nnum = DateConvert(datenum(C), 'string');
Nvec = DateConvert(datevec_fix(C), 'string');
if strcmp(Nnum, C)== 0
   error(['*** ', FuncName, ': "string" failed for current date as number.']);
end
if strcmp(Nvec, C)== 0
   error(['*** ', FuncName, ': "string" failed for current date as vector.']);
end

for iPool = 1:length(Npool)
   if strcmp(Strpool(iPool, :), DateConvert(Vpool(iPool, :), 'string')) == 0
      error(['*** ', FuncName, ': Failed to convert to "string": ', ...
            '[', sprintf('%d ', Vpool(iPool, :)), ']']);
   end
   if strcmp(Strpool(iPool, :), DateConvert(Npool(iPool), 'string')) == 0
      error(['*** ', FuncName, ': Failed to convert to "string": ', ...
            '[', sprintf('%d ', Npool(iPool)), ']']);
   end
end
disp('  ok: convert vector -> string');
disp('  ok: convert number -> string');

% Speed: -----------------------------------------------------------------------
disp([char(10), 'Speed tests...']);

% Find a suiting number of loops:
if doSpeed
   iLoop     = 0;
   startTime = cputime;
   while cputime - startTime < 1.0
      v     = datenum(C);
      clear('v');
      iLoop = iLoop + 1;
   end
   nLoops = 100 * ceil(iLoop / ((cputime - startTime) * 50));
   disp([sprintf('  %d', nLoops) ' loops on this machine.']);
else
   disp('  Use at least 2 loops (displayed times are random!)');
   nLoops = 2;
end

% To number:
if sscanf(version, '%f', 1) >= 7.7  % I don't know the exact version number!
   tic;
   for i = 1:nLoops
      v = datenum(C, 'dd-mmm-yyyy HH:MM:SS');
      clear('v');
   end
   tDatenum = toc + eps;
else  % Old and slow style:
   tic;
   for i = 1:nLoops
      v = datenum(C);
      clear('v');
   end
   tDatenum = toc + eps;
end

tic;
for i = 1:nLoops
   v = DateConvert(C, 'number');
   clear('v');
end
tNum = toc;

disp(['  String -> number:', char(10), ...
      '     DATENUM:     ', sprintf('%.2f', tDatenum), char(10), ...
      '     DateConvert: ', sprintf('%.2f', tNum), ...
      '   ==> ', sprintf('%.1f', 100 * tNum / tDatenum), '%']);

% To vector:
tic;
for i = 1:nLoops
   v = datevec(C);
   clear('v');
end
tDatevec = toc + eps;

tic;
for i = 1:nLoops
   v = DateConvert(C, 'vector');
   clear('v');
end
tVec = toc;

disp(['  String -> vector:', char(10), ...
      '     DATEVEC:     ', sprintf('%.2f', tDatevec), char(10), ...
      '     DateConvert: ', sprintf('%.2f', tVec), ...
      '   ==> ', sprintf('%.1f', 100 * tVec / tDatevec), '%']);

% To string:
tic;
for i = 1:nLoops
   v = datestr(N, 0);
   clear('v');
end
tDatestr = toc + eps;

tic;
for i = 1:nLoops
   v = DateConvert(N, 'string');
   clear('v');
end
tString = toc;

disp(['  Number -> string:', char(10), ...
      '     DATESTR:     ', sprintf('%.2f', tDatestr), char(10), ...
      '     DateConvert: ', sprintf('%.2f', tString), ...
      '   ==> ', sprintf('%.1f', 100 * tString / tDatestr), '%']);

V = datevec_fix(N);
for i = 1:nLoops
   v = datestr(V, 0);
   clear('v');
end
tDatestr = toc + eps;

tic;
for i = 1:nLoops
   v = DateConvert(V, 'string');
   clear('v');
end
tString = toc;

disp(['  Vector -> string:', char(10), ...
      '     DATESTR:     ', sprintf('%.2f', tDatestr), char(10), ...
      '     DateConvert: ', sprintf('%.2f', tString), ...
      '   ==> ', sprintf('%.1f', 100 * tString / tDatestr), '%']);

% Goodbye:
disp([char(10), 'DateConvert passed the test.']);

return;

% Fixed bug in DATEVEC: ********************************************************
function V = datevec_fix(S)
V = floor(datevec(datenum(S) + 0.5 / 86400));

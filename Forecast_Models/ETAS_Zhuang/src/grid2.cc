//       grid.cc 
//       version 1.0
//       used to calculate the grid probability for CSEPETAS 1.0.0
//

using namespace std;

#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <cctype>
#include <cstdlib>

#define Pi 3.14159265358979

double pnorm5(double, double, double);

int main(int argc, const char* argv[])
{
  if(argc<8){
    cout<<"Usage: grid dumpfile templatefile center.latitude beta magnitude.threshold #simulations outputfile!"<<endl;
    exit(1);
  }

  double CenterLatitude = atof(argv[3]);
  double Beta = atof(argv[4]);
  double MagnitudeThreshold=atof(argv[5]);
  double Scale = cos(CenterLatitude/180.0*Pi);
  double NumberOfSimulations = atof(argv[6]);  
  
  //  cout << Scale <<endl; 

  vector <double> latitude, longitude, magnitude, eqtime;

  ifstream in1(argv[1]);

  int count1=0;

  if(!in1){
    cout<<"Failed to open file: "<<argv[1]<<'!'<<endl;
    exit(1);
  }
  else{
    while(!in1.eof()){
        int index;
        double temp1, temp2, temp3, temp4;
        in1 >>index>> temp1 >> temp2>> temp3>>temp4;

        longitude.push_back(temp1);
        latitude.push_back(temp2);
        magnitude.push_back(temp3);
        eqtime.push_back(temp4);
/*	
        cout<<latitude[count1]<<' '
	    <<longitude[count1]<<' '
            <<magnitude[count1]<<' '
            <<eqtime[count1]<<endl;
*/	
        count1++;
      }
  }

  ifstream in2(argv[2]);
  int is=0, count21=0, count22=0;

    string str;

  if(!in2){
    cout<<"Failed to open the  file."<<endl;
    exit(1);
  }
  else{
    while(!in2.eof()){
     getline(in2, str,'\n');
     string::size_type i1 = str.find_first_not_of(" \t");
     char ch=str[i1];
     if(ch=='-' || ch=='.' || ( '0'<=ch && ch<='9')){
        is=1;
        count22++;
     }
     if(is==0) count21++;
    }
  }

  //  cout<<"count2: "<<count2 <<' '<<atof(&(ch[0]))<<endl;
  in2.close();

  ifstream in3(argv[2]);    
  vector <double> latitudeR1,latitudeR2,
    longitudeR1,longitudeR2,
    depthR1, depthR2,
    magnitudeR1, magnitudeR2;

  if(!in3){
    cout<<"Failed to open the  file."<<endl;
    exit(1);
  }
  else{
    string ch;
    for(int i=0; i<count21; i++)getline(in3, ch,'\n');
    for(int i=0; i<count22; i++){
      double temp[8];
      in3 >>temp[0] >> temp[1]
          >>temp[2] >> temp[3]
          >>temp[4] >> temp[5];
      longitudeR1.push_back(temp[0]);
      longitudeR2.push_back(temp[1]);
      latitudeR1.push_back(temp[2]);
      latitudeR2.push_back(temp[3]);
      depthR1.push_back(temp[4]);
      depthR2.push_back(temp[5]);
    }
 //    See whether file is read correctly
  /*
     for(int i=0; i<count22; i++){
       cout<< latitudeR1[i] <<' '
          << latitudeR2[i] <<' '
          << longitudeR1[i] <<' '
	  << longitudeR2[i] <<endl;
      }
   */   
  }

  in3.close();

  vector<double> rate;
  ofstream out1(argv[7], ios::app);

  if(!out1){
    cout<<"Failed to open output file: "<<argv[7]<<endl;
    exit(1);
  }
  else{ 
    double oldlonR1 = longitudeR1[0] ;

    double oldlatR1 = latitudeR1[0] ;

    out1 <<"    <cell lat=\'"<< latitudeR1[0]/2 + latitudeR2[0]/2
         <<"\' lon=\'" << longitudeR1[0] /2 + longitudeR2[0]/2
         <<"\'>"<<endl;
   for(int i=0; i<count22; i++) {
    if(abs(longitudeR1[i]-oldlonR1)+abs(latitudeR1[i]-oldlatR1) > 1e-8){
       oldlonR1 = longitudeR1[i] ;

       oldlatR1 = latitudeR1[i] ;

       out1 <<"    </cell>"<<endl;
       out1 <<"    <cell lat=\'"<< latitudeR1[i]/2 + latitudeR2[i]/2
         <<"\' lon=\'" << longitudeR1[i] /2 + longitudeR2[i]/2
         <<"\'>"<<endl;
    } 
    rate.push_back(0.0);
    for(int j=0; j<count1; j++){
      rate[i] += (pnorm5(latitudeR2[i], latitude[j], 0.2)
	-pnorm5(latitudeR1[i], latitude[j], 0.2))*
	(pnorm5(longitudeR2[i]*Scale, longitude[j]*Scale, 0.2)
	 -pnorm5(longitudeR1[i]*Scale, longitude[j]*Scale, 0.2));
     }

    for(double MagR=MagnitudeThreshold-0.05; MagR<=9.05000001; MagR +=0.1){ 
       double RateMagR = rate[i] * (exp(-Beta*(MagR-MagnitudeThreshold))
        -exp(-Beta*(MagR+0.1-MagnitudeThreshold)))
        /(1-exp(-Beta*(10.0-MagnitudeThreshold)))/NumberOfSimulations;
       out1 << "            <bin m=\'" 
	   << MagR + 0.05 <<"\'>" 
           << RateMagR <<"</bin>"<<endl; 
    }
   }
    out1 <<"</cell>"<<endl;
  }

 
  out1.close();
  return 0;
}


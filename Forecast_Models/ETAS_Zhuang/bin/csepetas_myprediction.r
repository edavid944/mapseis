#### CSEP ETAS ############
## Version 1.0.2 ##########

runif(1)

current.version = '1.0.2'

csepetas <- system('echo $CSEPETAS', intern=T)
csepetas.testrandom <- system('echo $ETAS_FIX_RANDOM', intern=T)


csepetas.input <- system('echo $CSEPETASINPUT', intern=T)


if(csepetas==''){
   print('CSEPETAS not defined, use default ./')
   csepetas<-'./'
 }

if(csepetas.testrandom=='1'){
.Random.seed <- scan(csepetas.testrandom)
}

if(csepetas.testrandom!='' &csepetas.testrandom!='1'){
.Random.seed <- scan(csepetas.testrandom)
}


csepetas.saverandom <- system('echo $ETAS_SAVE_RANDOM', intern=T)

if(csepetas.saverandom!=''){
write(.Random.seed, file=csepetas.saverandom)
}


if(csepetas.input==''){
   print('CSEPETASINPUT not defined, use default ./')
   csepetas<-'./'
 }

ierror = rep('',20) 

if(system(paste('ls ', csepetas.input,sep=''), intern=T)==csepetas.input){
    control.input <- scan(csepetas.input, what='')
    }else{
    ierror[1] =  "Control file not found" 
}

if(sum(ierror != '') == 0 ){
    if(control.input[1] != current.version) ierror [2] =  
         paste("Required version", control.input[1], "does not match current version", current.version)
} 

if( sum(ierror != '') == 0 ){    
    if(system(paste('ls ', control.input[2],sep=''), intern=T)!=control.input[2]){        
      ierror [3] =  paste("Input catalog file not found")
    }
    if(is.na(as.double(control.input[3]))) ierror [4] = paste("Magnitude threshold not a number")

    if(control.input[6]==0 & system(paste('ls ', control.input[7],sep=''), 
                                          intern=T)!=control.input[7]){
       ierror[5] = paste("Input parameter file not found")
    }

    if(system(paste('ls ', control.input[8],sep=''), 
                                          intern=T)!=control.input[8]){
       ierror[6] = paste("Input forecast template file not found")
    }

}


if( sum(ierror!='')==0 ){
    cata.history <-NULL
    magthreshold <- as.double(control.input[3])

    if(substr(control.input[10],1,1)=='1') {
         history.file <- paste(csepetas,'/historical.california.raw',sep='')
         cata.history.raw <- read.fwf(history.file,
                                   widths=c(4,-1, 2, -1, 2, 3, -1,2, -1, 5,9,10,-9, 5),
                                   skip=20)
         cata.history.raw <- cata.history.raw [cata.history.raw[,9] >= magthreshold,]

         cata.history.day <- (julian(as.Date(paste(cata.history.raw[,1],
                                                   cata.history.raw[,2], 
                                                   cata.history.raw[,3],sep='-')
                                     ),
                                    orig=as.Date('1985-1-1'))
                                +cata.history.raw[,4]/24+
                                cata.history.raw[,5]/24/60+ cata.history.raw[,6]/24/60/60)

         cata.history <- cbind(1:nrow(cata.history.raw),  cata.history.raw[,c(8,7,9)],
                            cata.history.day, 1,1,0)
       }
  
    if(substr(control.input[10],1,1)!='1'& control.input[10]!='0')  {
         history.file <- control.input[10]

         cata.history <- matrix(scan(control.input[2], skip=0), ncol=15, byrow=T)
         cata.history <- cata.history [cata.history[,6] >= magthreshold,]

         cata.history <-cbind(1:nrow(cata.history), cata.history[,1:2], cata.history[,6], 
                 julian(as.Date(paste(as.integer(cata.history[,3]), cata.history[,4], 
                 cata.history[,5], sep='-')), orig=as.Date('1985-1-1'))+
          cata.history[,8]/24+cata.history[,9]/24/60+cata.history[,10]/24/60/60, 1, 1, 0)
      }
}


if(sum(ierror !='')== 0){
 
    starting.day <-julian(as.Date(control.input[4]), orig=as.Date('1985-1-1'))
    ending.day <-julian(as.Date(control.input[5]), orig=as.Date('1985-1-1'))
    magthreshold <- as.double(control.input[3])

    cata.current <- matrix(scan(control.input[2], skip=0), ncol=15, byrow=T)
    cata.current <- cata.current [cata.current[,6] >= magthreshold,]

    cata.current <-cbind(1:nrow(cata.current), cata.current[,1:2], cata.current[,6], 
                 julian(as.Date(paste(as.integer(cata.current[,3]), cata.current[,4], 
                 cata.current[,5], sep='-')), orig=as.Date('1985-1-1'))+
          cata.current[,8]/24+cata.current[,9]/24/60+cata.current[,10]/24/60/60, 1, 1, 0)
    
    cata.all <- rbind(as.matrix(cata.history), cata.current)

    cata.all <-cata.all[order(cata.all[,5]),]
    cata.all[,1] <- 1:nrow(cata.all)
}


##### Read polygon file ##########

if(sum(ierror!='')==0){
  poly.file <- paste(csepetas, 'template.poly', sep='/')
  if(control.input[11]!='0') poly.file <- control.input[11]

  poly <-matrix(scan(poly.file), ncol=2, byrow=T)

}
##### converting the format to etas format ######################

    write("'CSEP ETAS input file'", file='input.dat')
    write(as.character(t(cbind(cata.all))), file='input.dat',
          ncol=8, append=T)



#
#    a<-scan(paste(csepetas, '/template.etas.in',sep=''),sep='\n', what='')

    a<-''
    a[1] <- "'input.dat'"
    a[2] <- '*'
    a[3] <- starting.day
    a[4] <- 10000
    a[5] <- 0.0
    a[6] <- magthreshold
    a[7]<-0

    a[8] <- nrow(poly)

    kk<- 8

    for(i in 1:nrow(poly)) a[(kk+i)] <- paste(poly[i,1], poly[i,2])

    kk<- (kk+nrow(poly))
    a[kk+1] =' '
    kk<- kk+1
    a[kk+1] <- '401 401'

      a[kk+2]  <-  1.005516303
      a[kk+3]  <- 0.4988
      a[kk+4]  <- 0.00309
      a[kk+5]  <- 1.35
      a[kk+6]  <- 1.0572
      a[kk+7]  <- 0.00036
      a[kk+8]  <- 1.7
      a[kk+9]  <- 1.46268

    a[kk+10] <- '4 0.1'
    a[kk+11]<-''
    a[kk+12]='1'

    if(control.input[6] =='0'){
      a[kk+12] = '0'
      parameter<- read.table(paste(control.input[7],sep=''))
      parameter<-parameter[nrow(parameter),]
      a[kk+2]  <- parameter$V1
      a[kk+3]  <- parameter$V2
      a[kk+4]  <- parameter$V3
      a[kk+5]  <- parameter$V4
      a[kk+6]  <- parameter$V5
      a[kk+7]  <- parameter$V6
      a[kk+8]  <- parameter$V7
      a[kk+9]  <- parameter$V8
    }

    write(a, file='csep.in', ncol=1)


 #   system(paste('mpirun -np 8', csepetas,'/bin/etas8pcsep < csep.in >etas8pcsep.log', sep=''))

 # for ismaltx   system('qsub cal.pbs')

   system(paste('bsub -I -q q8 -n 8 mpirun -srun ',  csepetas,
          '/bin/etas8pcsep < csep.in', sep=''))

#  system('sleep 10')
#   while(length(system('bjobs|grep zhuangjc', inter=T))==1)system('sleep 5')


    if(control.input[6]!=0)system(paste('cp para', control.input[7], '.old', sep=''))
              
 ### Simulation ###  


# Read parameters ####

   parameter<- read.table(paste(control.input[7],sep=''))
   parameter<-parameter[nrow(parameter),]

   parameter<-list(mu=parameter$V1, A=parameter$V2, cc=parameter$V3, 
                  alpha=parameter$V4, 
                    p = parameter$V5, d =parameter$V6,
                   q=parameter$V7, gamma=parameter$V8)


####read catalog ####

### set catalog to be the input catalog


bgprobs <- read.table(paste('probs.dat',sep=''))$V2

bgbands <- read.table(paste('probs.dat',sep=''))$V3
 

cata <- cata.all[,2:5]
#bgcata<- cata[runif(nrow(cata))<bgprobs]



source(paste(csepetas, '/bin/prediction.r',sep=''))


mid.latitude <- sum(range(cata[,2]))/2

bb <- (predict.etas(cata=cata, para=parameter, 
     tlim=c(0, 0, starting.day, ending.day), base.cata=cata,
     magthreshold=magthreshold, mid.latitude=mid.latitude))


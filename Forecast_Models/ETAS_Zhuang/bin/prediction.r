

random.shuffle <- function(a) {
 a[order(runif(length(a)))]
}

Bootstrap.bg <- function(cata, bgprobs, bandwidth=NULL){
   a<-cata[runif(nrow(cata))<bgprobs, ]
   
   cbind(a[random.shuffle(1:nrow(a)),1:2],
         a[random.shuffle(1:nrow(a)),3],
           a[,4])
}

rdiscrete<-function(n, x, p)
{
  
#  print(c(length(p), length(cumsum(p))))
  kk <-stepfun(cumsum(p)/sum(p), 1:(length(p)+1))

  x[kk(runif(n))]

}

#mytlim <- c(0, 9500, 24150, 24160)


rmag <- function(n,base.cata,tlim){

    id <- base.cata[,4] <tlim[3] & base.cata[,4]>tlim[2]

    rdiscrete(n,  base.cata[id, 3], rep(1, sum(id)))

}

simulate.etas8<-function(para, start.cata, base.cata,
                           xlim, ylim, tlim,  magthreshold=0,
                         mid.latitude)
{

  if(missing(base.cata)) base.cata = start.cata

  if(missing(xlim))xlim<-c(-Inf, Inf)
  if(missing(ylim))ylim<-c(-Inf, Inf)
   
 #  print('here 1')
 #  print (paste('starting catalogue has a dimension of', nrow(start.cata)))
 
  initial.cata <- start.cata   
     
 # print('here2')
 
  Noff <- rpois(n=nrow(initial.cata),
                 lambda=para$A*exp(para$alpha*(start.cata[,3]-magthreshold)))
  temp<-NULL
   Noff[is.null(Noff) | is.na(Noff)] <- 0

#  print(sort(Noff))
 # print(cbind(Noff, para$A*exp(para$alpha*(start.cata[,3]-magthreshold))))
 #  print(c(nrow(start.cata),length(Noff))) 

  if(sum(Noff)>0){
     print(paste(sum(Noff), "events generated."))
     dd <- rep(para$d*exp(para$gamma*(start.cata[,3]-magthreshold)), 
               Noff)
     RR <- sqrt(dd*(runif(sum(Noff))^(1/(1-para$q))-1))

     Theta <- runif(sum(Noff))*pi*2

     offspring.X <- rep(start.cata[,1],Noff)
     offspring.X <- offspring.X  + RR*cos(Theta)/cos(mid.latitude/180*pi)

     offspring.Y <- rep(start.cata[,2],Noff)
     offspring.Y <- offspring.Y  +RR*sin(Theta)

     offspring.M <- rmag(n=sum(Noff), base.cata=base.cata, tlim)
#     hist(offspring.M)
     offspring.T <- rep(start.cata[,4],Noff)
     offspring.T <- offspring.T + (runif(sum(Noff))^(1/(1-para$p))-1)*para$cc

#     print(offspring.T)
     temp<-data.frame(cbind(offspring.X, offspring.Y, 
                      offspring.M, offspring.T))
      
 #    print(tlim)
 
     itag <- temp[,4]>=tlim[3] & temp[,4] <=tlim[4] 

     print(paste(sum(itag),'selected in', sum(Noff), 'events generated.'))

      if(sum(itag)==0)temp <- NULL
      if(sum(itag)>0){
          temp <-temp[itag,]
  #       print(paste(sum(itag), "events selected."))
         rm(Noff, offspring.X, offspring.Y, offspring.T, offspring.M)
         temp1 <- simulate.etas8(para=para, start.cata=temp,
                             tlim=tlim, magthreshold= magthreshold,
                             base.cata=base.cata, mid.latitude=mid.latitude)
         if(!is.null(temp1)) temp <- data.frame(rbind(temp,temp1))
      }
    }
    temp
} 



predict.bg <- function(cata, para, bgprobs, bgbands, tlim,
           base.cata)
{
   if(missing(base.cata))base.cata=cata
  
   (mu <- para$mu  *(tlim[4]-tlim[3])/(tlim[3]-tlim[2])
          *sum(bgprobs))
#   print(para)
#   print(mu)#
 #   print(tlim)
    n <- rpois(1, mu)
   print(paste(n,'events generated in background.'))
#   print(sum(n))

   
   bgcata<- NULL
   
   if(n>0){

     bg.id <-  rdiscrete(n, 1:nrow(cata), bgprobs)
       
     bgcata <- cbind(rnorm(n, cata[bg.id,1], bgbands[bg.id]),
                   rnorm(n, cata[bg.id,2], bgbands[bg.id]),
                   rmag(n,base.cata, tlim),
                   runif(n, tlim[3], tlim[4]))
   }
  #print(bgcata)
  bgcata  

}


predict.etas <- function(cata, para, tlim,   base.cata, magthreshold,
          mid.latitude, number.simulation=1000)
{


if(missing(base.cata)) base.cata = cata
#print(para)
write('', file='dump.dat')

bb<-NULL
 for(i in 1:number.simulation){
    print('########################################################')
    print(paste('Generate the ', i, '-th simulations', sep=''))
    bgcata<-(predict.bg(cata, para, bgprobs, 
          bgbands, tlim=tlim, base.cata=base.cata))

    if(!is.null(bgcata)) { 
                          s.cata <- rbind(as.matrix(cata),as.matrix(bgcata))
                          }
    if(is.null(bgcata)) s.cata <- data.frame(cata)

    bb1<-simulate.etas8(start.cata=s.cata, para=para, 
       tlim=tlim, 
       magthreshold=magthreshold, base.cata=base.cata, 
       mid.latitude=mid.latitude)
   
   if(!is.null(bgcata))write(file='dump.dat', t(cbind(i, bgcata)), ncol=5,append=(i!=1))
   if(!is.null(bb1)) write(file='dump.dat', t(cbind(i,bb1)), ncol=5,append=T)
 }
}

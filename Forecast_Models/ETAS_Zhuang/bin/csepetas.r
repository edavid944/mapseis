#### CSEP ETAS ############
## CSEPJapan Version J1.1 ##########

runif(1)

current.version = 'J1.1'

csepetas <- system('echo $CSEPETAS', intern=T)
csepetas.input <- system('echo $CSEPETASINPUT', intern=T)

## Check environments (CSEPETAS and CSEPETASINPUT) being defined or not ######
if(csepetas==''){
   print('CSEPETAS not defined, use default ./')
   csepetas<-'./'
 }


if(csepetas.input==''){
   print('CSEPETASINPUT not defined, use default ./control.etas.in')
   csepetas.input<-'./control.etas.in'
 }


csepetas.testrandom=system('echo $TESTRANDOM',intern=T)

####  Set up or save random seeds ##########################
if(csepetas.testrandom=='1'){
.Random.seed <- scan(csepetas.testrandom)
}

if(csepetas.testrandom!='' &csepetas.testrandom!='1'){
.Random.seed <- scan(csepetas.testrandom)
}


csepetas.saverandom <- system('echo $ETAS_SAVE_RANDOM', intern=T)

if(csepetas.saverandom!=''){
write(.Random.seed, file=csepetas.saverandom)
}


################ Check existence of input control file #################

ierror = rep('',20) 

if(system(paste('ls ', csepetas.input,sep=''), intern=T)==csepetas.input){
    control.input <- scan(csepetas.input, what='')
    }else{
    ierror[1] =  "Control file not found" 
    print(ierror[1])
}

################ First line: Compare versions #####################
if(sum(ierror != '') == 0 ){
    if(control.input[1] != current.version) ierror [2] =  
         paste("Required version", control.input[1], "does not match current version", current.version)
    print(ierror[2])
} 

############### 2nd, 3rd, 4th, 7th and 8th lines#########################
## 3: data file name
## Line 2: parameter file name
#  Line 3: magnitude threshold
#  Line 4: depth threshold
#  Line 7: 0, to do optimisationo; 1 not to do optimisation
#  Line 8: file name for model parameters 
#  Line 9: forecast template file

if( sum(ierror != '') == 0 ){    
    if(system(paste('ls ', control.input[2],sep=''), intern=T)!=control.input[2]){        
      ierror [3] =  paste("Input catalog file not found")
    }

    if(is.na(as.double(control.input[3]))) ierror [4] = paste("Magnitude threshold or depth threshold is not a number")
 
    if(is.na(as.double(control.input[4]))) ierror [4] = paste("Magnitude threshold or depth threshold is not a number")
  
    if(control.input[7]==0 & system(paste('ls ', control.input[8],sep=''), 
                                          intern=T)!=control.input[8]){
       ierror[5] = paste("Input parameter file not found")
    }

    if(system(paste('ls ', control.input[9],sep=''), 
                                          intern=T)!=control.input[9]){
       ierror[6] = paste("Input forecast template file not found")
    }

}

############### 5th and 6th lines#############################
# Line 5: starting date for prediction
# Line 6: ending date for prediction
#

print(sum(ierror !=''))

if(sum(ierror !='')== 0){
 
    starting.day <-julian(as.Date(control.input[5]), orig=as.Date('1965-1-1'))
    ending.day <-julian(as.Date(control.input[6]), orig=as.Date('1965-1-1'))
    magthreshold <- as.double(control.input[3])
    depthreshold <- as.double(control.input[4])

#   cata.current <- matrix(scan(control.input[2], skip=1), ncol=11, byrow=T)
     
    cata.current <- read.table(control.input[2], skip=0)


    cata.current <- cata.current [cata.current[,6] >= magthreshold & cata.current[,7] <= depthreshold ,]

    cata.all <-cbind(1:nrow(cata.current), cata.current[,1:2], cata.current[,6], 
                 julian(as.Date(paste(as.integer(cata.current[,3]), cata.current[,4], 
                 cata.current[,5], sep='-')), orig=as.Date('1965-1-1'))+
          cata.current[,8]/24+cata.current[,9]/24/60+cata.current[,10]/24/60/60, 1, 1, 0)
    
     cata.all <- cata.all[order(cata.all[,5]),]
     cata.all <- cata.all[cata.all[,5]<= starting.day,]
     cata.all[,1] <- 1:nrow(cata.all)
}


##### Line 12: polygon file ##########

if(sum(ierror!='')==0){
  poly.file <- paste(csepetas, 'template.poly', sep='/')
  if(control.input[12]!='0') poly.file <- control.input[11]

  poly <-matrix(scan(poly.file), ncol=2, byrow=T)

}


##### converting the format to etas format ######################

    write("'CSEP ETAS input file'", file='input.dat')
    write(as.character(t(cata.all)), file='input.dat',
          ncol=ncol(cata.all), append=T)



#
#    a<-scan(paste(csepetas, '/template.etas.in',sep=''),sep='\n', what='')

    a<-''
    a[1] <- "'input.dat'"
    a[2] <- '*'
    a[3] <- starting.day
    a[4] <- 36000
    a[5] <- 0.0
    a[6] <- magthreshold
    a[7]<-0

    a[8] <- nrow(poly)

    kk<- 8

    for(i in 1:nrow(poly)) a[(kk+i)] <- paste(poly[i,1], poly[i,2])

    kk<- (kk+nrow(poly))
    a[kk+1] =' '
    kk<- kk+1
    a[kk+1] <- '41 41'

      a[kk+2]  <-  1.005516303
      a[kk+3]  <- 0.4988
      a[kk+4]  <- 0.00309
      a[kk+5]  <- 1.35
      a[kk+6]  <- 1.0572
      a[kk+7]  <- 0.00036
      a[kk+8]  <- 1.7
      a[kk+9]  <- 1.46268

    a[kk+10] <- '4 0.1'
    a[kk+11]<-''
    a[kk+12]='1'

    if(control.input[7] =='0'){
      a[kk+12] = '0'
      parameter<- read.table(paste(control.input[8],sep=''))
      parameter<-parameter[nrow(parameter),]
      a[kk+2]  <- parameter$V1
      a[kk+3]  <- parameter$V2
      a[kk+4]  <- parameter$V3
      a[kk+5]  <- parameter$V4
      a[kk+6]  <- parameter$V5
      a[kk+7]  <- parameter$V6
      a[kk+8]  <- parameter$V7
      a[kk+9]  <- parameter$V8
    }

    write(a, file='csep.in', ncol=1)


# start mpi program
 #   system('mpd &')

    system(paste('mpirun -np 4 ', csepetas,'/bin/etas8pcsep < csep.in >etas8pcsep.log', sep=''))
    #system(paste(csepetas,'/bin/etas8pcsep < csep.in >etas8pcsep.log', sep=''))
    if(control.input[7]!=0)system(paste('cp para', control.input[8], '.old', sep=''))
              
 ### Simulation ###  


# Read parameters ####

   parameter<- read.table(paste(control.input[8],sep=''))
   parameter<-parameter[nrow(parameter),]

   parameter<-list(mu=parameter$V1, A=parameter$V2, cc=parameter$V3, alpha=parameter$V4, 
                p = parameter$V5, d =parameter$V6, q=parameter$V7, gamma=parameter$V8)


####read catalog ####

### set catalog to be the input catalog


bgprobs <- read.table(paste('probs.dat',sep=''))$V2

bgbands <- read.table(paste('probs.dat',sep=''))$V3
 

cata <- cata.all[,2:5]

#bgcata<- cata[runif(nrow(cata))<bgprobs]



  source(paste(csepetas, '/bin/prediction.r',sep=''))


mid.latitude <- sum(range(cata[,2]))/2

bb <- (predict.etas(cata=cata, para=parameter, 
     tlim=c(0, 0, starting.day, ending.day), base.cata=cata,
     magthreshold=magthreshold, mid.latitude=mid.latitude, number.simulation=1000))



############################ Converting Nanjo's format into Old format#################################

grid.centers <- read.table(control.input[9])

ngrids <- nrow(grid.centers)



grid.XY <- cbind(grid.centers[,1]-0.05, grid.centers[,1] + 0.05, 
                 grid.centers[,2]-0.05, grid.centers[,2] + 0.05,
                 0, depthreshold )

write(t(grid.XY), file='grid.dat.temp', ncol=ncol(grid.XY))

#make grid prediction

##### copy output to web server or other necessary places ####


parameter$beta <- 1/mean(cata[cata[,4]<starting.day & cata[,4]>0,
                     3]-magthreshold)


### Scale correction for longitude ###

write("<?xml version='1.0' encoding='UTF-8'?>\n<CSEPForecast xmlns='http://www.scec.org/xml-ns/csep/forecast/0.1'>\n<forecastData publicID='smi:org.scec/csep/forecast/1'>", file='etas.output')

write(paste(" <modelName>Etas</modelName>\n",
    " <version> ", current.version, " </version>\n",
    " <author>Jiancang Zhuang</author> \n", 
    " <issueDate> ", date(), " </issueDate>\n",
    " <forecastStartDate> ", control.input[5], " </forecastStartDate> \n",
    " <forecastEndDate> ", control.input[6], " </forecastEndDate> \n",
    " <defaultCellDimension latRange='0.1' lonRange='0.1'/>\n",
    " <defaultMagBinDimension>0.1</defaultMagBinDimension>\n",
    " <lastMagBinOpen>1</lastMagBinOpen>\n",
    " <depthLayer max='100.0' min='0.0'>"),  file='etas.output',append=T)



#write('modelname = ETAS', file='etas.output')

#write('version = 1.0.2', file='etas.output',append=T)

#write('author = Jiancang Zhuang', file='etas.output',append=T)
#write(paste('issue_date = ', date()), file='etas.output',append=T)
#write(paste('forecast_start_date=',control.input[4]), file='etas.output',append=T)

# write(paste('forecast_duration=', ending.day-starting.day, 'day(s)'), file='etas.output',append=T)

# write('begin_forecast', file='etas.output',append=T)

if( is.na(as.integer(control.input[11])) ) {control.input[11]=1000; print("Warning: Number of simulation invalid! Set 1000") }
if( as.integer(control.input[11]) <=0 ) {control.input [11]=1000; print("Warning: Number of simulation invalid! Set 1000") }

# using 5 and 0.02 for smoothing, you can change it if you like. 

system(paste(paste(csepetas, '/bin/grid3 dump.dat',sep=''),
             'grid.dat.temp', mid.latitude, 
             parameter$beta, magthreshold, 5, 0.02, 
             control.input[11], 'etas.output')) 

write(paste('      </depthLayer>\n    </forecastData>\n </CSEPForecast>\n'),  file='etas.output', append=T)

#write('end_forecast', file='etas.output',append=T)

if(sum(ierror!='')!=0){
  print("Program ended with error(s):")
  print(ierror[ierror!=''])
}
       

 

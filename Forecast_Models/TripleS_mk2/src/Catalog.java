import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;
import java.util.Date;

public class Catalog {

    protected String[] times;
    protected float[] lats;
    protected float[] lons;
    protected float[] depths;
    protected float[] mags;
    public static final short CMT_CSEP_ONE_LINE_FORMAT = 1;
    public static final short JEREMY_GENERATED = 37; // my personal format
    public static final short ZMAP = 69;

    /**
     * Get the latitude of each event's epicenter
     */
    public float[] lats() {
        return this.lats;
    }

    /**
     * Get the longitude of each event's epicenter
     */
    public float[] lons() {
        return this.lons;
    }

    /**
     * Get the depth of each event's hypocenter
     */
    public float[] depths() {
        return this.depths;
    }

    /**
     * Get the magnitude of each event
     */
    public float[] mags() {
        return this.mags;
    }

    /**
     * Get the origin time of each event
     */
    public String[] times() {
        return this.times;
    }

    public Catalog() {
    }

    /**
     * In the simplest case, we can create a catalog from arrays of data
     * 
     * @param times array of strings of the form yyyy/MM/dd HH:mm:ss
     * @param lats array of epicentral latitude points
     * @param lons array of epicentral longitude points
     * @param depths array of hypocentral depths
     * @param mags array of magnitudes
     * @param start start date of the catalog, in the form yyyy/MM/dd HH:mm:ss
     * @param end end date of the catalog, in the form yyyy/MM/dd HH:mm:ss
     */
    public Catalog(String[] times, float[] lats, float[] lons, float[] depths, float[] mags) {
        this.times = new String[times.length];
        this.lats = new float[lats.length];
        this.lons = new float[lons.length];
        this.depths = new float[depths.length];
        this.mags = new float[mags.length];

        System.arraycopy(times, 0, this.times, 0, times.length);
        System.arraycopy(lats, 0, this.lats, 0, lats.length);
        System.arraycopy(lons, 0, this.lons, 0, lons.length);
        System.arraycopy(depths, 0, this.depths, 0, depths.length);
        System.arraycopy(mags, 0, this.mags, 0, mags.length);
    }

    /**
     * Count the number of events in a catalog file, neglecting comment lines (marked by starting a line w/ a non-numeric character).
     *
     * @param catalogFile path to the catalog of interest
     * @param catalogType type of catalog
     * @return the number of events in the specified catalog file
     */
    protected int numberOfEqksInFile(String catalogFile, short catalogType) {
        int numberOfEqks = 0;

        try {
            String sRecord = null;

            // Get a handle to the catalog file
            FileInputStream oFIS = new FileInputStream(catalogFile);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(new InputStreamReader(oBIS));

            // pass through the file once quickly to see how many events there are
            while ((sRecord = oReader.readLine()) != null) {
                // if the first character of this line is a letter, we consider this a comment; otherwise, it is an event
                while (Character.isLetter(sRecord.charAt(0))) {
                    sRecord = oReader.readLine();

                    if (sRecord == null) {
                        break;
                    }
                }
                if (sRecord == null) {
                    break;
                }

                numberOfEqks++;
            }

            oReader.close();
            oReader = null;
            oBIS.close();
            oBIS = null;
            oFIS.close();
            oFIS = null;
        } catch (Exception ex) {
            System.err.println("Trouble counting the number of events in " + catalogFile);
            ex.printStackTrace();
            System.exit(-1);
        }

        return numberOfEqks;
    }

    /**
     * We can parse certain types of catalog files to create a catalog.  In particular, we here parse a catalog file that is of the common form for ANSS or one that I've generated 
     * (JEREMY_GENERATED).
     *
     * @param catalogFile file containing earthquake events
     * @param catalogType type of earthquake catalog (determines parsing details)
     * @param start start date of the catalog (in my favorite format)
     * @param end end date of the catalog (in my favorite format)
     */
    public Catalog(String catalogFile, short catalogType) {
        try {
            int numberOfEvents = numberOfEqksInFile(catalogFile, catalogType);
            this.times = new String[numberOfEvents];
            this.lats = new float[numberOfEvents];
            this.lons = new float[numberOfEvents];
            this.depths = new float[numberOfEvents];
            this.mags = new float[numberOfEvents];

            String sRecord = null;

            // Get a handle to the input catalog file
            FileInputStream oFIS = new FileInputStream(catalogFile);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(new InputStreamReader(oBIS));

            int eventNumber = 0;

            String time = "";
            String sLatitude = "0";
            String sLongitude = "0";
            String sDepth = "0";
            String sMagnitude = "0";

            float lat = 0.0f;
            float lon = 0.0f;
            float depth = 0.0f;
            float mag = 0.0f;

            // Parse each line of the catalog file
            while ((sRecord = oReader.readLine()) != null) {
                // skip over any comment lines
                while (Character.isLetter(sRecord.charAt(0))) {
                    sRecord = oReader.readLine();

                    if (sRecord == null) {
                        break;
                    }
                }
                if (sRecord == null) {
                    break;
                }

                // parse the details
                String[] eqkDetails = eqkParametersFromRecord(sRecord, catalogType);

                time = eqkDetails[0];
                sLatitude = eqkDetails[1];
                sLongitude = eqkDetails[2];
                sMagnitude = eqkDetails[3];
                sDepth = eqkDetails[4];

                if (sLatitude.trim().length() > 0) {
                    lat = Float.parseFloat(sLatitude);
                }
                if (sLongitude.trim().length() > 0) {
                    lon = Float.parseFloat(sLongitude);
                }
                if (sDepth.trim().length() > 0) {
                    depth = Float.parseFloat(sDepth);
                }
                if (sMagnitude.trim().length() > 0) {
                    mag = Float.parseFloat(sMagnitude);
                }

                this.times[eventNumber] = time;
                this.lats[eventNumber] = lat;
                this.lons[eventNumber] = lon;
                this.depths[eventNumber] = depth;
                this.mags[eventNumber] = mag;

                eventNumber++;
            }
        } catch (Exception e) {
            System.out.println("error in Catalog(" + catalogFile + ")");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Create a subcatalog containing only events falling within the given magnitude range
     * 
     * @param minMag minimum magnitude of event we want in the subcatalog
     * @param maxMag maximum magnitude of event we want in the subcatalog
     */
    public Catalog subcatalogByMagnitude(float minMag, float maxMag) {
        int numberOfQualifyingEvents = 0;
        int[] indicesOfQualifyingEvents = new int[this.lats.length]; // make room for the special case where every event in the catalog belongs to the subcatalog

        for (int i = 0; i < this.times.length; i++) {
            float currentMag = this.mags[i];
            if (currentMag >= minMag && currentMag <= maxMag) {
                indicesOfQualifyingEvents[numberOfQualifyingEvents] = i;
                numberOfQualifyingEvents++;
            }
        }
        return this.subcatalogByIndices(indicesOfQualifyingEvents, numberOfQualifyingEvents);
    }

    /**
     * Create a subcatalog containing only events falling within the given spatial domain
     * 
     * @param minLat minimum latitude of event we want in the subcatalog
     * @param maxLat maximum latitude of event we want in the subcatalog
     * @param minLon minimum longitude of event we want in the subcatalog
     * @param maxLon maximum longitude of event we want in the subcatalog
     * @param minDepth minimum depth of event we want in the subcatalog
     * @param maxDepth maximum depth of event we want in the subcatalog
     */
    public Catalog subcatalogBySpace(float minLat, float maxLat, float minLon, float maxLon, float minDepth, float maxDepth) {
        int numberOfQualifyingEvents = 0;
        int[] indicesOfQualifyingEvents = new int[this.lats.length]; // make room for the special case where every event in the catalog belongs to the subcatalog

        for (int i = 0; i < this.times.length; i++) {
            float currentLat = this.lats[i];
            float currentLon = this.lons[i];
            float currentDepth = this.depths[i];
            if (currentLat >= minLat && currentLat <= maxLat && currentLon >= minLon && currentLon <= maxLon &&
                    ((currentDepth >= minDepth && currentDepth <= maxDepth) || (Float.isNaN(currentDepth)))) {
                indicesOfQualifyingEvents[numberOfQualifyingEvents] = i;
                numberOfQualifyingEvents++;
            }
        }
        return this.subcatalogByIndices(indicesOfQualifyingEvents, numberOfQualifyingEvents);
    }

    /**
     * Create a subcatalog with the specified number of eqks containing only the eqks that are specified by the array of event indices and the number
     * 
     * @param indices indices of events to include in the subcatalog
     * @param numberOfEqks number of eqks to include in the subcatalog
     * @return subset of events corresponding to the specified eqk indices
     */
    private Catalog subcatalogByIndices(
            int[] indices, int numberOfEqks) {
        String[] timesLocal = new String[numberOfEqks];
        float[] latsLocal = new float[numberOfEqks];
        float[] lonsLocal = new float[numberOfEqks];
        float[] depthsLocal = new float[numberOfEqks];
        float[] magsLocal = new float[numberOfEqks];

        for (int i = 0; i <
                numberOfEqks; i++) {
            int indexOfQualifyingEvent = indices[i];
            timesLocal[i] = this.times[indexOfQualifyingEvent];
            latsLocal[i] = this.lats[indexOfQualifyingEvent];
            lonsLocal[i] = this.lons[indexOfQualifyingEvent];
            depthsLocal[i] = this.depths[indexOfQualifyingEvent];
            magsLocal[i] = this.mags[indexOfQualifyingEvent];
        }

        Catalog subCatalog = new Catalog(timesLocal, latsLocal, lonsLocal, depthsLocal, magsLocal);
        return subCatalog;
    }

    /**
     * Create a subcatalog containing only events falling within the given date range
     * 
     * @param minDate minimum origin time of event we want in the subcatalog
     * @param maxDate maximum origin time of event we want in the subcatalog
     */
    public Catalog subcatalogByTime(
            String minDate, String maxDate) {
        int numberOfQualifyingEvents = 0;
        int[] indicesOfQualifyingEvents = new int[this.lats.length]; // make room for the special case where every event in the catalog belongs to the subcatalog

        Date eventTime = new Date();
        Date start = DateUtil.dateFromString(minDate);
        Date end = DateUtil.dateFromString(maxDate);

        for (int i = 0; i <
                this.times.length; i++) {
            eventTime = DateUtil.dateFromString(this.times[i]);
            //System.out.println("eventTime=" +eventTime.toString());
            if (eventTime.after(start) && eventTime.before(end)) {
                indicesOfQualifyingEvents[numberOfQualifyingEvents] = i;
                numberOfQualifyingEvents++;

            }
            // Assume the catalog is ordered by time; the first event that occurs after the end period of interest marks the line after which no events will fall in the subcatalog
            if (eventTime.after(end)) {
                break;
            }

        }

        return this.subcatalogByIndices(indicesOfQualifyingEvents, numberOfQualifyingEvents);
    }

    /**
     * Find the min/max lat/lon/depth.:
     * 
     * @return array containing min/max lat/lon/depth points of events in the catalog      in the format
     *      [0] = minLat
     *      [1] = maxLat
     *      [2] = minLon
     *      [3] = maxLon
     *      [4] = minDepth
     *      [5] = maxDepth
     */
    protected float[] region() {
        float[] region = new float[6];

        // get the min/max lats and lons
        float minLat = 90.0f;
        float maxLat = -90.0f;
        float minLon = 180.0f;
        float maxLon = -180.0f;
        float minDepth = 0.0f;
        float maxDepth = 0.0f;
        int numberOfEvents = this.lats.length;

        for (int counter = 0; counter < numberOfEvents; counter++) {
            float currentLat = this.lats[counter];
            float currentLon = this.lons[counter];
            float currentDepth = this.depths[counter];

            if (currentLat < minLat) {
                minLat = currentLat;
            }
            if (currentLat > maxLat) {
                maxLat = currentLat;
            }
            if (currentLon < minLon) {
                minLon = currentLon;
            }
            if (currentLon > maxLon) {
                maxLon = currentLon;
            }
            if (currentDepth < minDepth) {
                minDepth = currentDepth;
            }
            if (currentDepth > maxDepth) {
                maxDepth = currentDepth;
            }
        }

        region[0] = minLat;
        region[1] = maxLat;
        region[2] = minLon;
        region[3] = maxLon;
        region[4] = minDepth;
        region[5] = maxDepth;
        return region;
    }

    /**
     * Find the min/max magnitude in the catalog
     * 
     * @return array containing min/max magnitude in the format
     *      [0] = minMag
     *      [1] = maxMag       
     */
    protected float[] magnitudeRange() {
        float[] magnitudeRange = new float[2];

        // get the min/max mag
        float minMag = 10.0f;
        float maxMag = 0.0f;

        int numberOfEvents = this.mags.length;
        for (int i = 0; i < numberOfEvents; i++) {
            float currentMag = this.mags[i];

            if (currentMag < minMag) {
                minMag = currentMag;
            }
            if (currentMag > maxMag) {
                maxMag = currentMag;
            }
        }

        magnitudeRange[0] = minMag;
        magnitudeRange[1] = maxMag;

        return magnitudeRange;
    }

    /**
     * Save the current catalog events to a file w/ some minimal metadata; this catalog will be saved in the Catalog.JEREMY_GENERATED format
     *
     * @param outputFile logical path to catalog file
     * @param comments any notes that should precede the earthquake listing
     */
    public void save(String outputFile, String comments) {
//        System.out.println("going to save " + outputFile);
        float[] region = region();
        float[] magnitudeRange = magnitudeRange();
        try {
            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            oWriter.write("This catalog was automatically generated using predictionTools.Catalog.save with the following parameters:\n");
            oWriter.write("minLat=" + region[0] + "\n");
            oWriter.write("maxLat=" + region[1] + "\n");
            oWriter.write("minLon=" + region[2] + "\n");
            oWriter.write("maxLon=" + region[3] + "\n");
            oWriter.write("minDepth=" + region[4] + "\n");
            oWriter.write("maxDepth=" + region[5] + "\n");
            oWriter.write("minMag=" + magnitudeRange[0] + "\n");
            oWriter.write("maxMag=" + magnitudeRange[1]);

            if (comments.length() > 0) {
                oWriter.write("\n" + comments.trim());
            }

            for (int counter = 0; counter <
                    this.times.length; counter++) {
                oWriter.write("\n" + this.times[counter] + "\t" + this.lats[counter] + "\t" + this.lons[counter] + "\t" + this.depths[counter] + "\t" + this.mags[counter]);
            }

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in Catalog.save(" + outputFile + ")");
            ex.printStackTrace();
        }

    }

    /**
     * Parse earthquake parameters from a single line of a catalog file
     * 
     * @param sRecord chunk of text containing information on a single earthquake
     * @param catalogType type of catalog
     * @return array of earthquake parameters in the following form:
     * [0]=origin time
     * [1]=latitude (in decimal degrees) of epicenter
     * [2]=longitude (in decimal degrees) of epicenter
     * [3]=magnitude
     * [4]=depth
     */
    protected String[] eqkParametersFromRecord(String sRecord, int catalogType) {
        String[] eqkParameters = new String[5];
        StringTokenizer st = new StringTokenizer(sRecord);
        String time = "";
        String sLatitude = "0";
        String sLongitude = "0";
        String sDepth = "0";
        String sMagnitude = "0";

        if (catalogType == Catalog.JEREMY_GENERATED) {
            time = st.nextToken(); // date
            time = time.concat(" " + st.nextToken()); // origin time
            sLatitude = st.nextToken();
            sLongitude = st.nextToken();
            sDepth = st.nextToken();
            sMagnitude = st.nextToken();
        } else if (catalogType == Catalog.CMT_CSEP_ONE_LINE_FORMAT) {
//            System.out.println(sRecord);
            st.nextToken(); // skip the event id

            String year = st.nextToken(); // 2 digit year

            int iYear = Integer.parseInt(year);
            if (iYear > 75) {
                year = "19" + year;
            } else {
                year = "20" + year;
            }

            String month = st.nextToken();
            String day = st.nextToken();
            String timeOfDay = st.nextToken();
            time = year + "/" + month + "/" + day + " " + timeOfDay + "0";
            st.nextToken(); // skip centroid time offset

            sLatitude = st.nextToken();
            sLongitude = st.nextToken();
            float fLon = Float.parseFloat(sLongitude);
            if (fLon < 0f) {
                fLon += 360f;
            }
            sLongitude = String.valueOf(fLon);
            sDepth = st.nextToken();

            int momentExponent = Integer.parseInt(st.nextToken());
            float momentBase = Float.parseFloat(st.nextToken());
            float scalarMoment = momentBase * (float) Math.pow(10, momentExponent);
            float fMagnitude = GeoUtil.momentMagnitudeFromScalarMoment(scalarMoment);
            // Round magnitude to the nearest tenth as this is what is done in the CMT catalog
            fMagnitude =
                    Math.round(fMagnitude * 10f) / 10f;
            sMagnitude =
                    String.valueOf(fMagnitude);
        }
        else if (catalogType == Catalog.ZMAP){
            sLongitude = String.valueOf(Float.parseFloat(st.nextToken()));
            sLatitude = String.valueOf(Float.parseFloat(st.nextToken()));
            
            int iYear = (int)Math.floor(Float.parseFloat(st.nextToken()));
            String year = String.valueOf(iYear);

            int iMonth = (int)Math.floor(Float.parseFloat(st.nextToken()));
            String month = String.valueOf(iMonth);
            if (month.length() == 1){
                month = "0" + month;
            }            
            
            int iDay = (int)Math.floor(Float.parseFloat(st.nextToken()));
            String day = String.valueOf(iDay);
            if (day.length() == 1){
                day = "0" + day;
            }            
            
            sMagnitude = String.valueOf(Float.parseFloat(st.nextToken()));
            sDepth = String.valueOf(Float.parseFloat(st.nextToken()));

            int iHour = (int)Math.floor(Float.parseFloat(st.nextToken()));
            String hour = String.valueOf(iHour);
            if (hour.length() == 1){
                hour = "0" + hour;
            }            

            int iMinute = (int)Math.floor(Float.parseFloat(st.nextToken()));
            String minute = String.valueOf(iMinute);
            if (minute.length() == 1){
                minute = "0" + minute;
            }            

            int iSecond = (int)Math.floor(Float.parseFloat(st.nextToken()));
            String second = String.valueOf(iSecond);
            if (second.length() == 1){
                second = "0" + second;
            }            
            time = year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
        }

        eqkParameters[0] = time;
        eqkParameters[1] = sLatitude;
        eqkParameters[2] = sLongitude;
        eqkParameters[3] = sMagnitude;
        eqkParameters[4] = sDepth;
        return eqkParameters;
    }

    /**
     * Count the number of earthquakes in the current catalog.
     *
     * @return the number of events in the catalog
     */
    public int numberOfEqks() {
        return this.mags.length;
    }
    
    /**
     * Use Aki 1965 Maximum Likelihood Estimate method to find the b-value, which assumes infinite maximum magnitude.
     *
     * @return MLE G-R b value
     */
    public float akiMLEBValue() {
        float averageMagnitude = ArrayUtil.average(this.mags);
        float minMagnitude = ArrayUtil.minimum(this.mags);
        float logBase10OfE = (float) Math.log10(Math.E);

        float bValue = logBase10OfE / (averageMagnitude - minMagnitude);
        return bValue;
    }

    /**
     * Append the events in the specified catalog, assuming that the catalogs are chronologically ordered and all the events in this catalog preceed those in catalogB
     *
     * @param catalogB catalog to append
     * @return catalog containing all events in the current catalog and catalogB
     */
    public void appendCatalog(Catalog catalogB){
        int numberOfEventsInA = this.numberOfEqks();
        int numberOfEventsInB = catalogB.numberOfEqks();
        int totalNumberOfEvents = numberOfEventsInA + numberOfEventsInB;
        float[] tempLats = new float[totalNumberOfEvents];
        float[] tempLons = new float[totalNumberOfEvents];
        float[] tempDepths = new float[totalNumberOfEvents];
        float[] tempMags = new float[totalNumberOfEvents];
        String[] tempTimes = new String[totalNumberOfEvents];

        System.arraycopy(this.depths, 0, tempDepths, 0, numberOfEventsInA);
        System.arraycopy(catalogB.depths, 0, tempDepths, numberOfEventsInA, numberOfEventsInB);
        System.arraycopy(this.lats, 0, tempLats, 0, numberOfEventsInA);
        System.arraycopy(catalogB.lats, 0, tempLats, numberOfEventsInA, numberOfEventsInB);
        System.arraycopy(this.lons, 0, tempLons, 0, numberOfEventsInA);
        System.arraycopy(catalogB.lons, 0, tempLons, numberOfEventsInA, numberOfEventsInB);
        System.arraycopy(this.mags, 0, tempMags, 0, numberOfEventsInA);
        System.arraycopy(catalogB.mags, 0, tempMags, numberOfEventsInA, numberOfEventsInB);
        System.arraycopy(this.times, 0, tempTimes, 0, numberOfEventsInA);
        System.arraycopy(catalogB.times, 0, tempTimes, numberOfEventsInA, numberOfEventsInB);

        this.times = new String[tempTimes.length];
        this.lats = new float[tempLats.length];
        this.lons = new float[tempLons.length];
        this.depths = new float[tempDepths.length];
        this.mags = new float[tempMags.length];

        System.arraycopy(tempTimes, 0, this.times, 0, tempTimes.length);
        System.arraycopy(tempLats, 0, this.lats, 0, tempLats.length);
        System.arraycopy(tempLons, 0, this.lons, 0, tempLons.length);
        System.arraycopy(tempDepths, 0, this.depths, 0, tempDepths.length);
        System.arraycopy(tempMags, 0, this.mags, 0, tempMags.length);
    }

    /**
     * Save the current catalog events to a file in the ZMAP format
     *
     * @param outputFile logical path to catalog file
     */
    public void saveAsZMAP(String outputFile) {
        try {
            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));
            int numberOfEqks = numberOfEqks();
            for (int i = 0; i < numberOfEqks; i++) {
                String time = this.times[i];
                String year = time.substring(0, 4);
                String month = time.substring(5, 7);
                String day = time.substring(8, 10);
                String hour = time.substring(11, 13);
                String min = time.substring(14, 16);
                String sec = time.substring(17, 19);

                oWriter.write(this.lons[i] + "\t" + this.lats[i] + "\t" + year + "\t" + month + "\t" + day + "\t" + this.mags[i] + "\t" + this.depths[i] + "\t" + hour + "\t" +
                        min + "\t" + sec + "\n");
            }
            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in Catalog.saveAsZMAP(" + outputFile + ")");
            ex.printStackTrace();
        }
    }
}


import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Properties;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.Locale;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class AlarmFunction {

    protected float[] alarmFunctionValues;
    protected float minimumLatitude;
    protected float maximumLatitude;
    protected float minimumLongitude;
    protected float maximumLongitude;
    protected float sizeOfSpaceCell;
    protected int numberOfLatBoxes;
    protected int numberOfLonBoxes;

    public AlarmFunction() {
    }

    public float[] values() {
        return this.alarmFunctionValues;
    }

    /**
     * Bin the given alarm function values into the given grid.  The result is a grid with the alarm function value in each grid box.  The alarm function value file should be 
     * provided in tab-separated format w/ each line being of the form:
     *
     * lon lat alarmFunctionValue
     *
     * Here the experiment parameters are inline.
     *
     * @param alarmFunctionValueFile path to file containing alarm function values
     * @param useMarginOfError should we grant a margin of error in computing nu and tau?
     */
    public AlarmFunction(String alarmFunctionValueFile) {

        // find the min/max lat/lon and grid spacing values
        float[] gridSpecs = AlarmFunction.gridSpecifications(alarmFunctionValueFile);
        this.minimumLatitude = gridSpecs[0];
        this.maximumLatitude = gridSpecs[1];
        this.minimumLongitude = gridSpecs[2];
        this.maximumLongitude = gridSpecs[3];
        this.sizeOfSpaceCell = gridSpecs[4];

        this.numberOfLatBoxes = Math.round((this.maximumLatitude - this.minimumLatitude) / this.sizeOfSpaceCell);
        this.numberOfLonBoxes = Math.round((this.maximumLongitude - this.minimumLongitude) / this.sizeOfSpaceCell);

        // initialize the alarm function vector to the appropriate size
        this.alarmFunctionValues = new float[this.numberOfLatBoxes * this.numberOfLonBoxes];

        try {
            String sRecord = null;

            // Get a handle to the file
            FileInputStream oFIS = new FileInputStream(alarmFunctionValueFile);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(new InputStreamReader(oBIS));
            StringTokenizer st;

            // pass through the file and bin each value
            while ((sRecord = oReader.readLine()) != null) {
                // skip over all the metadata
                while (Character.isLetter(sRecord.charAt(0))) {
                    sRecord = oReader.readLine();

                    if (sRecord == null) {
                        break;
                    }
                }
                if (sRecord == null) {
                    break;
                }

                st = new StringTokenizer(sRecord);

                // translate the lon/lat into a cell value
                float lon = Float.parseFloat(st.nextToken());
                float lat = Float.parseFloat(st.nextToken());

                int cellPosition = ArrayUtil.cellToWhichValueBelongs(this.minimumLongitude, this.maximumLongitude, this.minimumLatitude,
                        this.maximumLatitude, this.sizeOfSpaceCell, this.sizeOfSpaceCell, lon, lat);

                // set the alarm function value in the appropriate vector position
                float alarmFunctionValue = Float.parseFloat(st.nextToken());
                this.alarmFunctionValues[cellPosition] = alarmFunctionValue;
            }
            oReader.close();
            oBIS.close();
            oFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction(" + alarmFunctionValueFile + ")");
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Read the min/max lat/lon and grid spacing from the given experiment parameter file.  We obtain the min/max lat/lon and boxSizes by reading the metadata.
     *
     * @param experimentParameterFile path to file containing experiment parameters
     * @return array containing min/max lat/lon values in the format:
     * [0] = minimumLatitude
     * [1] = maximumLatitude
     * [2] = minimumLongitude
     * [3] = maximumLongitude
     * [4] = sizeOfSpaceCell
     */
    protected static float[] gridSpecifications(String experimentParameterFile) {
        Properties Props = new Properties();
        float minLat = 90.0f;
        float maxLat = -90.0f;
        float minLon = 180.0f;
        float maxLon = -180.0f;
        float boxSize = 0.0f;

        try {
            FileInputStream in = new FileInputStream(experimentParameterFile);
            Props.load(in);
            in.close();
            in = null;

            minLat = Float.parseFloat((Props.getProperty("minLat")));
            maxLat = Float.parseFloat((Props.getProperty("maxLat")));
            minLon = Float.parseFloat((Props.getProperty("minLon")));
            maxLon = Float.parseFloat((Props.getProperty("maxLon")));
            boxSize = Float.parseFloat((Props.getProperty("boxSize")));

        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.gridSpecifications(" + experimentParameterFile + ")");
            ex.printStackTrace();
            System.exit(-1);
        }

        float[] gridSpecs = new float[8];
        gridSpecs[0] = minLat;
        gridSpecs[1] = maxLat;
        gridSpecs[2] = minLon;
        gridSpecs[3] = maxLon;
        gridSpecs[4] = boxSize;

        return gridSpecs;
    }

    /**
     * Save the current alarm function values and experiment parameter values.  This should be used when the alarm function is only varying in space (i.e., lat/lon only).
     *
     * @param outputFile path to alarm function value file
     */
    public void save(String outputFile) {
        int bins = this.alarmFunctionValues.length;

        String appropriateFormat = "%.3f\t%.3f";
        try {
            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            Locale us = new Locale("en", "us");
            oWriter.write("minLat=" + this.minimumLatitude);
            oWriter.write("\nmaxLat=" + this.maximumLatitude);
            oWriter.write("\nminLon=" + this.minimumLongitude);
            oWriter.write("\nmaxLon=" + this.maximumLongitude);
            oWriter.write("\nboxSize=" + this.sizeOfSpaceCell + "\n");

            for (int i = 0; i < bins; i++) {
                // factor the vector position into its corresponding lat/lon position
                float[] latLon = Utility.latlonFromCellPosition(i, this.minimumLatitude, this.minimumLongitude, this.maximumLongitude, this.sizeOfSpaceCell);

                float currentLat = latLon[0];
                float currentLon = latLon[1];
                float alarmFunctionValue = this.alarmFunctionValues[i];
                if (alarmFunctionValue > 0f) {
                    String lonLat = String.format(us, appropriateFormat, currentLon, currentLat);
                    oWriter.write(lonLat + "\t" + alarmFunctionValue + "\n");
                }
            }

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.save(" + outputFile + ")");
            ex.printStackTrace();
        }
    }

    /**
     * Construct an alarm function from the given values and specifications.  Here the experiment parameters are specifed by an array of floats that is of the form:
     *
     * [0] = minimumLatitude
     * [1] = maximumLatitude
     * [2] = minimumLongitude
     * [3] = maximumLongitude
     * [4] = sizeOfSpaceCell
     *
     * @param alarmFunctionValues array of alarm function values
     * @param experimentSpecs array of experiment specifications
     */
    public AlarmFunction(float[] alarmFunctionValues, float[] experimentSpecs) {
        // set the min/max lat/lon and grid spacing values
        this.minimumLatitude = experimentSpecs[0];
        this.maximumLatitude = experimentSpecs[1];
        this.minimumLongitude = experimentSpecs[2];
        this.maximumLongitude = experimentSpecs[3];
        this.sizeOfSpaceCell = experimentSpecs[4];

        this.numberOfLatBoxes = Math.round((this.maximumLatitude - this.minimumLatitude) / this.sizeOfSpaceCell);
        this.numberOfLonBoxes = Math.round((this.maximumLongitude - this.minimumLongitude) / this.sizeOfSpaceCell);

        this.alarmFunctionValues = new float[alarmFunctionValues.length];
        // copy the alarm function values
        System.arraycopy(alarmFunctionValues, 0, this.alarmFunctionValues, 0, alarmFunctionValues.length);
    }

    /**
     * Save the current alarm function values in ForecastML format, but only save the values for cells that are in the testing region, which is specified by the forecast nodes file
     *
     * @param outputFile path to alarm function value file
     * @param forecastStartDate start date of prospective forecast
     * @param forecastEndDate end date of prospective forecast
     * @param forecastNodesPath path to forecast nodes file, which lists all the cells in the testing region
     * @param b regional Gutenberg-Richter b-value, used to decompose alarm function value rate into discrete bins (that is, the alarm function value is the forecast rate of 
     * all eqks above the minimum target magnitude, but we want to write the forecast in discrete magnitude bins)
     * @param minTargetMagnitude minimum target magnitude of interest
     * @param maxTargetMagnitude maximum target magnitude of interest
     * @param magnitudeBinSize how wide is each magnitude bin?
     */
    public void saveAsRateForecast(String outputFile, String forecastStartDate, String forecastEndDate, String forecastNodesPath, float b, float minDepth, float maxDepth,
            float minTargetMagnitude, float maxTargetMagnitude, float magnitudeBinSize) {
        String appropriateFormat = "%.1f";
        float totalForecastRate = 0f;
        try {
            // Determine the issue date (current system time) and format the start/end date
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            String issueDate = sdf.format(cal.getTime());
            SimpleDateFormat jzdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            String startDate = sdf.format(jzdf.parse(forecastStartDate));
            String endDate = sdf.format(jzdf.parse(forecastEndDate));

            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            Locale us = new Locale("en", "us");
            oWriter.write("<?xml version='1.0' encoding='UTF-8'?>\n");
            oWriter.write("<CSEPForecast xmlns='http://www.scec.org/xml-ns/csep/forecast/0.1'>\n");
            oWriter.write(" <forecastData publicID='smi:org.scec/csep/forecast/1'>\n");
            oWriter.write("   <modelName>zechar.triple_s</modelName>\n");
            oWriter.write("   <version>1.0</version>\n");
            oWriter.write("   <author>CSEP</author>\n");
            oWriter.write("   <issueDate>" + issueDate + "</issueDate>\n");
            oWriter.write("   <forecastStartDate>" + startDate + "</forecastStartDate>\n");
            oWriter.write("   <forecastEndDate>" + endDate + "</forecastEndDate>\n");
            oWriter.write("   <defaultMagBinDimension>" + magnitudeBinSize + "</defaultMagBinDimension>\n");
            oWriter.write("   <lastMagBinOpen>1</lastMagBinOpen>\n");
            oWriter.write("   <defaultCellDimension latRange='0.1' lonRange='0.1'/>\n");
            oWriter.write("   <depthLayer max='" + maxDepth + "' min='" + minDepth + "'>\n");

            String sRecord = null;

            // Get a handle to the file
            FileInputStream oFIS = new FileInputStream(forecastNodesPath);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(new InputStreamReader(oBIS));
            StringTokenizer st;

//            System.out.println("lat\tlon\tmag\trate");
            // pass through the forecast nodes file and save the corresponding alarm function values
            while ((sRecord = oReader.readLine()) != null) {
                st = new StringTokenizer(sRecord);

                // translate the lon/lat from the center of the cell to the SW corner
                String sLon = st.nextToken();
                String sLat = st.nextToken();
                oWriter.write("     <cell lat='" + sLat + "' lon='" + sLon + "'>\n");
                float lon = Float.parseFloat(sLon) - 0.5f * this.sizeOfSpaceCell;
                float lat = Float.parseFloat(sLat) - 0.5f * this.sizeOfSpaceCell;

                // Determine which cell in the alarm function value array this lon/lat point corresponds to
                int cellPosition = ArrayUtil.cellToWhichValueBelongs(this.minimumLongitude, this.maximumLongitude, this.minimumLatitude,
                        this.maximumLatitude, this.sizeOfSpaceCell, this.sizeOfSpaceCell, lon, lat);

                // Determine N(minTargetMagnitude) by looking up the rate
                float forecastRateForThisCell = this.alarmFunctionValues[cellPosition];
                float a = (float) Math.log10(forecastRateForThisCell) + b * minTargetMagnitude;

                int numberOfMagnitudeBins = Math.round((maxTargetMagnitude - minTargetMagnitude) / magnitudeBinSize);
                for (int i = 0; i < numberOfMagnitudeBins; i++) {
                    float binMinMag = minTargetMagnitude + i * magnitudeBinSize;
                    float binMaxMag = binMinMag + magnitudeBinSize;
                    String binCenterMag = String.format(us, appropriateFormat, binMinMag + 0.5f * magnitudeBinSize);

                    double forecastRateForThisBin = Math.pow(10, a - b * binMinMag) - Math.pow(10, a - b * binMaxMag);
                    if (forecastRateForThisCell == 0.0f) {
                        forecastRateForThisBin = 0f;
                    }
                    oWriter.write("       <bin m='" + binCenterMag + "'>" + forecastRateForThisBin + "</bin>\n");
                    totalForecastRate += forecastRateForThisBin;

                //                    System.out.println(sLat + "\t" + sLon + "\t" + binCenterMag + "\t" + forecastRateForThisBin);
                }

                oWriter.write("     </cell>\n");

            }
            oReader.close();
            oBIS.close();

            oWriter.write("    </depthLayer>\n");
            oWriter.write("  </forecastData>\n");
            oWriter.write("</CSEPForecast>\n");

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
            System.out.println("total forecast rate: " + totalForecastRate);
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.saveAsRateForecast(" + outputFile + ")");
            ex.printStackTrace();
        }
    }

    /**
     * Save the current alarm function values in ForecastML format
     *
     * @param outputFile path to alarm function value file
     * @param forecastStartDate start date of prospective forecast
     * @param forecastEndDate end date of prospective forecast
     * @param b regional Gutenberg-Richter b-value, used to decompose alarm function value rate into discrete bins (that is, the alarm function value is the forecast rate of
     * all eqks above the minimum target magnitude, but we want to write the forecast in discrete magnitude bins)
     * @param minTargetMagnitude minimum target magnitude of interest
     * @param maxTargetMagnitude maximum target magnitude of interest
     * @param magnitudeBinSize how wide is each magnitude bin?
     */
    public void saveAsRateForecast(String outputFile, String forecastStartDate, String forecastEndDate, float b, float minDepth, float maxDepth,
            float minTargetMagnitude, float maxTargetMagnitude, float magnitudeBinSize) {
        String appropriateFormat = "%.1f";
        try {
            // Determine the issue date (current system time) and format the start/end date
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            String issueDate = sdf.format(cal.getTime());
            SimpleDateFormat jzdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            String startDate = sdf.format(jzdf.parse(forecastStartDate));
            String endDate = sdf.format(jzdf.parse(forecastEndDate));

            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            Locale us = new Locale("en", "us");
            oWriter.write("<?xml version='1.0' encoding='UTF-8'?>\n");
            oWriter.write("<CSEPForecast xmlns='http://www.scec.org/xml-ns/csep/forecast/0.1'>\n");
            oWriter.write(" <forecastData publicID='smi:org.scec/csep/forecast/1'>\n");
            oWriter.write("   <modelName>zechar.triple_s</modelName>\n");
            oWriter.write("   <version>1.0</version>\n");
            oWriter.write("   <author>CSEP</author>\n");
            oWriter.write("   <issueDate>" + issueDate + "</issueDate>\n");
            oWriter.write("   <forecastStartDate>" + startDate + "</forecastStartDate>\n");
            oWriter.write("   <forecastEndDate>" + endDate + "</forecastEndDate>\n");
            oWriter.write("   <defaultMagBinDimension>" + magnitudeBinSize + "</defaultMagBinDimension>\n");
            oWriter.write("   <lastMagBinOpen>1</lastMagBinOpen>\n");
            oWriter.write("   <defaultCellDimension latRange='0.1' lonRange='0.1'/>\n");
            oWriter.write("   <depthLayer max='" + maxDepth + "' min='" + minDepth + "'>\n");

            int numberOfCells = this.alarmFunctionValues.length;
            // pass through each cell and figure out the bin forecast rate, save these values
            float totalForecastRate = 0f;

            for (int j = 0; j < numberOfCells; j++) {
                // factor the vector position into its corresponding lat/lon position
                float[] latLon = Utility.latlonFromCellPosition(j, this.minimumLatitude, this.minimumLongitude, this.maximumLongitude, this.sizeOfSpaceCell);
                float currentCell_swCornerLat = latLon[0];
                float currentCell_swCornerLon = latLon[1];
                float currentCell_centerLat = currentCell_swCornerLat + 0.5f * this.sizeOfSpaceCell;
                float currentCell_centerLon = currentCell_swCornerLon + 0.5f * this.sizeOfSpaceCell;

                String sLat = String.format(us, appropriateFormat, currentCell_centerLat);
                String sLon = String.format(us, appropriateFormat, currentCell_centerLon);

                // translate the lon/lat from the center of the cell to the SW corner
                oWriter.write("     <cell lat='" + sLat + "' lon='" + sLon + "'>\n");

                // Determine N(minTargetMagnitude) by looking up the rate
                float forecastRateForThisCell = this.alarmFunctionValues[j];
                float a = (float) Math.log10(forecastRateForThisCell) + b * minTargetMagnitude;

                int numberOfMagnitudeBins = Math.round((maxTargetMagnitude - minTargetMagnitude) / magnitudeBinSize);
                for (int i = 0; i < numberOfMagnitudeBins; i++) {
                    float binMinMag = minTargetMagnitude + i * magnitudeBinSize;
                    float binMaxMag = binMinMag + magnitudeBinSize;
                    String binCenterMag = String.format(us, appropriateFormat, binMinMag + 0.5f * magnitudeBinSize);

                    double forecastRateForThisBin = Math.pow(10, a - b * binMinMag) - Math.pow(10, a - b * binMaxMag);
                    totalForecastRate += forecastRateForThisBin;
                    if (forecastRateForThisCell == 0.0f) {
                        forecastRateForThisBin = 0f;
                    }
                    oWriter.write("       <bin m='" + binCenterMag + "'>" + forecastRateForThisBin + "</bin>\n");
                }

                oWriter.write("     </cell>\n");

            }

            oWriter.write("    </depthLayer>\n");
            oWriter.write("  </forecastData>\n");
            oWriter.write("</CSEPForecast>\n");

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
            System.out.println("total forecast rate:" + totalForecastRate);
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.saveAsRateForecast(" + outputFile + ")");
            ex.printStackTrace();
        }
    }

    /**
     * Save the current alarm function values in ASCII column format, but 
     * only save the values for cells that are in the testing region, which is
     * specified by the forecast nodes file
     *
     * @param outputFile path to alarm function value file
     * @param forecastNodesPath path to forecast nodes file, which lists all
     *  the cells in the testing region
     * @param b regional Gutenberg-Richter b-value, used to decompose alarm
     *  function value rate into discrete bins (that is, the alarm function
     *  value is the forecast rate of all eqks above the minimum target
     *  magnitude, but we want to write the forecast in discrete magnitude bins)
     * @param minTargetMagnitude minimum target magnitude of interest
     * @param maxTargetMagnitude maximum target magnitude of interest
     * @param magnitudeBinSize how wide is each magnitude bin?
     * @param rateVariance overall rate variance to be used for generating
     *  negative binomial forecast
     */
    public void saveAsASCII(String outputFile, String forecastNodesPath,
            float b, float minTargetMagnitude, float maxTargetMagnitude,
            float magnitudeBinSize, float minDepth, float maxDepth,
            float rateVariance) {
        String appropriateFormat = "%.3f";
        float totalRate = ArrayUtil.sum(this.alarmFunctionValues);
        try {

            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(
                    new OutputStreamWriter(oOutBIS));

            Locale us = new Locale("en", "us");
            String sRecord = null;

            // Get a handle to the file
            FileInputStream oFIS = new FileInputStream(forecastNodesPath);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(
                    new InputStreamReader(oBIS));
            StringTokenizer st;

//            float fTotalForecastRate = 0f;

            // pass through the forecast nodes file and save the corresponding
//            alarm function values
            while ((sRecord = oReader.readLine()) != null) {
                st = new StringTokenizer(sRecord);

                // translate the lon/lat from the cell center to the SW corner
                String sLon = st.nextToken();
                String sLat = st.nextToken();
                float lon= Float.parseFloat(sLon) - 0.5f * this.sizeOfSpaceCell;
                float lat= Float.parseFloat(sLat) - 0.5f * this.sizeOfSpaceCell;
                String sBinMinLon = String.format(us, appropriateFormat, lon);
                String sBinMaxLon = String.format(us, appropriateFormat, lon +
                        this.sizeOfSpaceCell);
                String sBinMinLat = String.format(us, appropriateFormat, lat);
                String sBinMaxLat = String.format(us, appropriateFormat, lat +
                        this.sizeOfSpaceCell);
                String sMinDepth=String.format(us, appropriateFormat, minDepth);
                String sMaxDepth=String.format(us, appropriateFormat, maxDepth);

                // Determine which cell in the alarm function value array this
                // lon/lat point corresponds to
                int cellPosition = ArrayUtil.cellToWhichValueBelongs(
                        this.minimumLongitude, this.maximumLongitude,
                        this.minimumLatitude, this.maximumLatitude,
                        this.sizeOfSpaceCell, this.sizeOfSpaceCell, lon, lat);
//                System.out.println("cellPosition = " + cellPosition);

                // Determine N(minTargetMagnitude) by looking up the rate
                float forecastRateForThisCell =
                        this.alarmFunctionValues[cellPosition];
                float a = (float) Math.log10(forecastRateForThisCell) + b *
                        minTargetMagnitude;
                int numberOfMagnitudeBins = Math.round(
                        (maxTargetMagnitude - minTargetMagnitude) /
                        magnitudeBinSize);
//                double sumOfBinRates = 0f;
                for (int i = 0; i < numberOfMagnitudeBins; i++) {
                    float binMinMag = minTargetMagnitude + i * magnitudeBinSize;
                    float binMaxMag = binMinMag + magnitudeBinSize;
                    String binCenterMag = String.format(us, appropriateFormat,
                            binMinMag + 0.5f * magnitudeBinSize);
                    String sBinMinMag = String.format(us, appropriateFormat,
                            binMinMag);
                    String sBinMaxMag = String.format(us, appropriateFormat,
                            binMaxMag);

                    double forecastRateForThisBin = 
                            Math.pow(10, a - b * binMinMag) -
                            Math.pow(10, a - b * binMaxMag);
                    if (forecastRateForThisCell == 0.0f) {
                        forecastRateForThisBin = 0f;
                    }

                    // scale the variance so that the ratio of mean to variance
                    // is the same in every bin
                    double varianceForThisBin = rateVariance *
                            forecastRateForThisBin / totalRate;
//                    sumOfBinRates += forecastRateForThisBin;
//                    fTotalForecastRate += forecastRateForThisBin;
//                    oWriter.write(sLat + "\t" + sLon + "\t" + binCenterMag +
//                    "\t" + forecastRateForThisBin + "\n");
                    oWriter.write(sBinMinLon + "\t" + sBinMaxLon + "\t" +
                            sBinMinLat + "\t" + sBinMaxLat + "\t" + sMinDepth +
                            "\t" + sMaxDepth + "\t" + sBinMinMag + "\t" +
                            sBinMaxMag + "\t" + forecastRateForThisBin + "\t1"
                            +"\t" + varianceForThisBin + "\n");
                }
//                if ((sumOfBinRates - forecastRateForThisCell) > 0.000000000001){
//                    System.out.println("got here");
//                }

            }
            oReader.close();
            oBIS.close();

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
//            System.out.println("fTotalForecastRate = " + fTotalForecastRate);
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.saveAsASCII(" +
                    outputFile + ")");
            ex.printStackTrace();
        }
    }

    /**
     * Save the current alarm function values in Jochen's preferred  column format, but only save the values for cells that are in the testing region, which is specified by the forecast nodes file
     *
     * @param outputFile path to alarm function value file
     * @param forecastNodesPath path to forecast nodes file, which lists all the cells in the testing region
     * @param b regional Gutenberg-Richter b-value, used to decompose alarm function value rate into discrete bins (that is, the alarm function value is the forecast rate of
     * all eqks above the minimum target magnitude, but we want to write the forecast in discrete magnitude bins)
     * @param minTargetMagnitude minimum target magnitude of interest
     * @param maxTargetMagnitude maximum target magnitude of interest
     * @param magnitudeBinSize how wide is each magnitude bin?
     */
    public void saveAsJochensFormat(String outputFile, String forecastNodesPath, float b, float minTargetMagnitude, float maxTargetMagnitude, float magnitudeBinSize) {
        String appropriateFormat = "%.3f";
        try {

            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            Locale us = new Locale("en", "us");
            String sRecord = null;

            // Get a handle to the file
            FileInputStream oFIS = new FileInputStream(forecastNodesPath);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(new InputStreamReader(oBIS));
            StringTokenizer st;

            oWriter.write("lon\tlat\tminDepth\tmaxDepth\tminMag\tmaxMag\trate\n");
            // pass through the forecast nodes file and save the corresponding alarm function values
            while ((sRecord = oReader.readLine()) != null) {
                st = new StringTokenizer(sRecord);

                // translate the lon/lat from the center of the cell to the SW corner
                String sLon = st.nextToken();
                String sLat = st.nextToken();
                float lon = Float.parseFloat(sLon) - 0.5f * this.sizeOfSpaceCell;
                float lat = Float.parseFloat(sLat) - 0.5f * this.sizeOfSpaceCell;

                // Determine which cell in the alarm function value array this lon/lat point corresponds to
                int cellPosition = ArrayUtil.cellToWhichValueBelongs(this.minimumLongitude, this.maximumLongitude, this.minimumLatitude,
                        this.maximumLatitude, this.sizeOfSpaceCell, this.sizeOfSpaceCell, lon, lat);

                // Determine N(minTargetMagnitude) by looking up the rate
                float forecastRateForThisCell = this.alarmFunctionValues[cellPosition];
                float a = (float) Math.log10(forecastRateForThisCell) + b * minTargetMagnitude;
                oWriter.write(sLon + "\t" + sLat + "\t0\t30\t" + minTargetMagnitude + "\t" + maxTargetMagnitude);

                int numberOfMagnitudeBins = Math.round((maxTargetMagnitude - minTargetMagnitude) / magnitudeBinSize);
                for (int i = 0; i < numberOfMagnitudeBins; i++) {
                    float binMinMag = minTargetMagnitude + i * magnitudeBinSize;
                    float binMaxMag = binMinMag + magnitudeBinSize;

                    double forecastRateForThisBin = Math.pow(10, a - b * binMinMag) - Math.pow(10, a - b * binMaxMag);
                    if (forecastRateForThisCell == 0.0f) {
                        forecastRateForThisBin = 0f;
                    }
//                    oWriter.write(sLat + "\t" + sLon + "\t" + binCenterMag + "\t" + forecastRateForThisBin + "\n");
                    oWriter.write("\t" + forecastRateForThisBin);
                }
                oWriter.write("\n");
            }
            oReader.close();
            oBIS.close();

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.saveAsASCII(" + outputFile + ")");
            ex.printStackTrace();
        }
    }

    /**
     * Save the current alarm function values in ASCII format, but only save the values for cells that are in the testing region, which is specified by the forecast nodes file
     *
     * @param outputFile path to alarm function value file
     * @param forecastStartDate start date of prospective forecast
     * @param forecastEndDate end date of prospective forecast
     * @param forecastNodesPath path to forecast nodes file, which lists all the cells in the testing region
     * @param b regional Gutenberg-Richter b-value, used to decompose alarm function value rate into discrete bins (that is, the alarm function value is the forecast rate of 
     * all eqks above the minimum target magnitude, but we want to write the forecast in discrete magnitude bins)
     * @param minTargetMagnitude minimum target magnitude of interest
     * @param maxTargetMagnitude maximum target magnitude of interest
     * @param magnitudeBinSize how wide is each magnitude bin?
     */
    public void saveAsASCII(String outputFile, String forecastNodesPath) {
        try {
            // Determine the issue date (current system time) and format the start/end date
            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            String sRecord = null;

            // Get a handle to the file
            FileInputStream oFIS = new FileInputStream(forecastNodesPath);
            BufferedInputStream oBIS = new BufferedInputStream(oFIS);
            BufferedReader oReader = new BufferedReader(new InputStreamReader(oBIS));
            StringTokenizer st;

            // pass through the forecast nodes file and save the corresponding alarm function values
            while ((sRecord = oReader.readLine()) != null) {
                st = new StringTokenizer(sRecord);

                // translate the lon/lat from the center of the cell to the SW corner
                String sLon = st.nextToken();
                String sLat = st.nextToken();
                float lon = Float.parseFloat(sLon) - 0.5f * this.sizeOfSpaceCell;
                float lat = Float.parseFloat(sLat) - 0.5f * this.sizeOfSpaceCell;

                // Determine which cell in the alarm function value array this lon/lat point corresponds to
                int cellPosition = ArrayUtil.cellToWhichValueBelongs(this.minimumLongitude, this.maximumLongitude, this.minimumLatitude,
                        this.maximumLatitude, this.sizeOfSpaceCell, this.sizeOfSpaceCell, lon, lat);

                // Determine N(minTargetMagnitude) by looking up the rate
                float forecastRateForThisCell = this.alarmFunctionValues[cellPosition];
                oWriter.write(sLon + "\t" + sLat + "\t" + forecastRateForThisCell + "\n");
            }
            oReader.close();
            oBIS.close();

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.saveAsASCII(" + outputFile + ")");
            ex.printStackTrace();
        }
    }

    /**
     * Construct a uniform alarm function from the specifications.  Here the experiment parameters are specifed by an array of floats that is of the form:
     *
     * [0] = minimumLatitude
     * [1] = maximumLatitude
     * [2] = minimumLongitude
     * [3] = maximumLongitude
     * [4] = sizeOfSpaceCell
     *
     * @param experimentSpecs array of experiment specifications
     */
    public AlarmFunction(float[] experimentSpecs) {
        // set the min/max lat/lon and grid spacing values
        this.minimumLatitude = experimentSpecs[0];
        this.maximumLatitude = experimentSpecs[1];
        this.minimumLongitude = experimentSpecs[2];
        this.maximumLongitude = experimentSpecs[3];
        this.sizeOfSpaceCell = experimentSpecs[4];

        this.numberOfLatBoxes = Math.round((this.maximumLatitude - this.minimumLatitude) / this.sizeOfSpaceCell);
        this.numberOfLonBoxes = Math.round((this.maximumLongitude - this.minimumLongitude) / this.sizeOfSpaceCell);

        this.alarmFunctionValues = new float[this.numberOfLatBoxes * this.numberOfLonBoxes];
        for (int i = 0; i < this.alarmFunctionValues.length; i++) {
            this.alarmFunctionValues[i] = 1f;
        }
    }

    /**
     * Compute the area (in square km) of each cell in the current alarm function and save this information to the specified file.
     *
     * @param outputFile path to file containing area information
     */
    public void saveAreaInformation(String outputFile) {
        int bins = this.alarmFunctionValues.length;

        String appropriateFormat = "%.1f";
        try {
            FileOutputStream oOutFIS = new FileOutputStream(outputFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            Locale us = new Locale("en", "us");

            for (int i = 0; i < bins; i++) {
                // factor the vector position into its corresponding lat/lon position
                float[] latLon = Utility.latlonFromCellPosition(i, this.minimumLatitude, this.minimumLongitude, this.maximumLongitude, this.sizeOfSpaceCell);

                float currentCell_swCornerLat = latLon[0];
                float currentCell_swCornerLon = latLon[1];
                float currentCell_nwCornerLat = currentCell_swCornerLat + this.sizeOfSpaceCell;
                float currentCell_nwCornerLon = currentCell_swCornerLon;
                float currentCell_seCornerLat = currentCell_swCornerLat;
                float currentCell_seCornerLon = currentCell_swCornerLon + this.sizeOfSpaceCell;
                float currentCell_centerLat = currentCell_swCornerLat + 0.5f * this.sizeOfSpaceCell;
                float currentCell_centerLon = currentCell_swCornerLon + 0.5f * this.sizeOfSpaceCell;
                float cellArea = GeoUtil.areaOfRectangularRegion(currentCell_nwCornerLat, currentCell_nwCornerLon, currentCell_seCornerLat, currentCell_seCornerLon);

                String lat = String.format(us, appropriateFormat, currentCell_centerLat);
                String lon = String.format(us, appropriateFormat, currentCell_centerLon);
                oWriter.write(lat + "\t" + lon + "\t" + cellArea + "\n");
            }
            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in AlarmFunction.saveAreaInformation(" + outputFile + ")");
            ex.printStackTrace();
        }
    }
}
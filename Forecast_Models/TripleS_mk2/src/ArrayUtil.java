import java.util.Random;
import java.util.Arrays;

/**
 *
 * @author jzechar has now made a minor change
 */
public class ArrayUtil {

    public ArrayUtil() {
    }

    /**
     * Normalize the specified array so that the sum of its values is unity.
     * We do this by finding the sum of the original contents and dividing each
     * entry by this sum
     * @param array array to normalize
     */
    public static float[] normalize(float[] array) {
        float[] arrayCopy = new float[array.length];
        System.arraycopy(array, 0, arrayCopy, 0, array.length);
        float sum = ArrayUtil.sum(array);

        // divide each entry by this sum
        for (int i = 0; i < array.length; i++) {
            arrayCopy[i] /= sum;
        }
        return arrayCopy;
    }

    /**
     * Given an array of floats, find and return the minimum value greater
     * than zero.
     *
     * @param array array of floats
     * @return minimum positive value in array
     */
    public static float minimumPositiveValue(float[] array) {
        int entries = array.length;
        float min = Float.POSITIVE_INFINITY;

        for (int i = 0; i < entries; i++) {
            if ((array[i] < min) && (array[i] > 0.0f)) {
                min = array[i];
            }
        }

        return min;
    }

    /**
     * Compute sum of array values.
     * @param array over which to sum
     * @return sum of array values.
     */
    public static float sum(float[] array) {
        float sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    /**
     * Compute sum of array values.
     * 
     * @param array over which to sum
     * @return sum of array values.
     */
    public static short sum(short[] array) {
        short sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    /**
     * Given a binning specified by minimum value, maximum value, and bin size,
     * determine to which bin a given value belongs.
     *
     * @param gridMin minimum grid value
     * @param gridMax maximum grid value
     * @param binSize size of each bin
     * @param value value which we want to bin
     * @param allowOverflow answer to the question of whether the value should
     * be allowed to reside outside the grid.  If allowOverflow is false and
     * the value doesn't belong in the grid, -1 is returned.  Otherwise, the
     * bin number is returned, regardless of whether it falls outside the grid
     * limits.
     * @return the bin to which the value of interest belongs
     */
    public static int binToWhichValueBelongs(float gridMin, float gridMax, float binSize, float value, boolean allowOverflow) {
        int bigNumber = 10000;

        // Scale the minimum bin value.  To avoid rounding errors, we floor the absolute value of the scaled value, then multiply this by the sign of the original value
        int min_int = (int) (Math.signum(gridMin) * (float) Math.floor(Math.abs(gridMin * bigNumber)));
        // Scale the cell size.
        int cellSize_int = Math.round(binSize * bigNumber);
        //  Scale the value to be binned.  Avoid rounding errors as above
        int value_int = (int) (Math.signum(value) * (float) Math.floor(Math.abs(value * bigNumber)));
//        int value_int = (int) Math.floor(value * bigNumber);
        if ((value < gridMin) || (value >= gridMax) && !allowOverflow) {
            return -1;
        }
        float position_f = (float) (value_int - min_int) / (float) cellSize_int;
        int position = (int) Math.floor(position_f);
        return position;
    }

    /**
     * We want to pick out and return the unique values in an array.  We start
     * by assuming that all values are unique, making an array that can contain
     * all the values in the original array. Then we'll pass over the array and
     * add the array value only if it doesn't already exist in the list of uniques.
     * We'll keep track of how many uniques we found and return only these
     * values
     */
    public static float[] uniqueValues(float[] array) {
        int entries = array.length;
        float[] arrayCopy = new float[entries];
        System.arraycopy(array, 0, arrayCopy, 0, array.length);
        Arrays.sort(arrayCopy);

        // we have to assume that each value is unique, so make room for them all
        float[] uniqueValues = new float[entries];

        // we know the first value is unique, so we add it
        uniqueValues[0] = arrayCopy[0];
        int numberOfUniqueValues = 1;
        // store the remaining unique values
        for (int i = 1; i < entries; i++) {
            float potentialUniqueValue = arrayCopy[i];
            float lastValue = arrayCopy[i - 1];

            // Because we've sorted the values, we only have a unique value
            // if the current value is strictly greater than the last
            if (potentialUniqueValue > lastValue) {
                uniqueValues[numberOfUniqueValues] = potentialUniqueValue;
                numberOfUniqueValues++; // increment the number of uniques
            }
        }

        // "shrink" the array of unique values by creating a new array w/
        //  just enough room to hold them all, then copy them over
        float[] trulyUniqueValues = new float[numberOfUniqueValues];
        for (int i = 0; i < numberOfUniqueValues;i++){
            trulyUniqueValues[i] = uniqueValues[i];
        }

        return trulyUniqueValues;
    }

    /**
     * Given a 2D gridding specified by minimum values, maximum values, and grid
     * sizes, determine to which grid cell a given (xValue, yValue) belongs.
     * We do this by determining the appropriate row and appropriate column
     * based on their respective discretization, then we map this row and column
     * to a cell number based on the number of columns in the grid.  We deal
     * with values that don't fit into the grid by bringing them to the closest
     * cell inside the grid.  For example, if a value falls to the "west" of the
     * beginning of the 24th row, we bin this value into the first column of the
     * 24th row.  If the value falls to the east of the 17th row, we bin it into
     * the last column of the 17th row.  The same method is applied to values
     * north or south of the grid.
     *
     * @param xMin minimum x value
     * @param xMax maximum x value
     * @param yMin minimum y value
     * @param yMax maximum y value
     * @param xBinSize discretization in x direction
     * @param yBinSize discretization in y direction
     * @param xValue x value which we want to bin
     * @param yValue y value which we want to bin
     * @return the cell to which (xValue, yValue) belongs
     */
    public static int cellToWhichValueBelongs(float xMin, float xMax, float yMin, float yMax, float xBinSize, float yBinSize, float xValue, float yValue) {
        int numberOfColumns = Math.round((xMax - xMin) / xBinSize);
        int numberOfRows = Math.round((yMax - yMin) / yBinSize);

//        if (xValue == -0.42f && yValue == -0.18f){
//            System.out.println("aim...");
//        }
        int column = ArrayUtil.binToWhichValueBelongs(xMin, xMax, xBinSize, xValue, true);
        // if the value falls outside the grid to the west, put it in the first column
        if (column < 0) {
            column = 0;
//            System.out.println("Overflow to the west");
        }
        // if the value falls outside the grid to the east, put it in the last column
        if (column >= numberOfColumns) {
            column = numberOfColumns - 1;
//            System.out.println("overflow to the east");
        }

        int row = ArrayUtil.binToWhichValueBelongs(yMin, yMax, yBinSize, yValue, true);
        // if the value falls outside the grid to the south, put it in the first row
        if (row < 0) {
            row = 0;
//            System.out.println("overflow to the south");
        }
        // if the value falls outside the grid to the north, put it in the last row
        if (row >= numberOfRows) {
            row = numberOfRows - 1;
//            System.out.println("overflow to the north");
        }

        // transform the (row, col) point into the cell number
        int cell = row * numberOfColumns + column;
        return cell;
    }
    
    /**
     * Modify an array so that it has no entries less than or equal to 0.  For
     * those entries that are less than or equal to zero, replace these with a
     * random number b/w 0 and the previously minimum positive entry.
     *
     * @param array array to modify
     * @return modified array containing no entries that are less than or equal
     * to zero.
     */
    public static float[] arrayWithNoEntriesLessThanOrEqualToZero(float[] array) {
        int entries = array.length;
        float[] modifiedArray = new float[entries];
        System.arraycopy(array, 0, modifiedArray, 0, entries);

        float minPositiveValue = ArrayUtil.minimumPositiveValue(modifiedArray);
        Random rndgen = new Random();

        for (int i = 0; i < entries; i++) {
            if (modifiedArray[i] < minPositiveValue) {
                // set the value to a random number b/w 0 and minPositiveValue
                //  by multiplying minPositiveValue by a random number b/w 0 and 1
                float coefficient = rndgen.nextFloat();
                modifiedArray[i] = coefficient * minPositiveValue;
            }
        }

        return modifiedArray;
    }    
    
    /**
     * Given an array of floats, find and return the minimum value.
     *
     * @param array array of floats
     * @return minimum value in array
     */
    public static float minimum(float[] array) {
        int entries = array.length;
        float min = array[0];

        for (int i = 1; i < entries; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }

        return min;
    }    
    
/**
     * Given an array of floats, find and return the maximum value.
     *
     * @param array array of floats
     * @return minimum value in array
     */
    public static float maximum(float[] array) {
        int entries = array.length;
        float max = array[0];

        for (int i = 1; i < entries; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        return max;
    }       
    
    /**
     * Given a float array, return the average of the entries
     *
     * @param array array in which we're interested
     * @return average of all entry values
     */
    public static float average(float[] array) {
        float sum = ArrayUtil.sum(array);
        float avg = sum / (float) array.length;
        return avg;
    }    

    /**
     * Compute sum of array values.
     * @param array over which to sum
     * @return sum of array values.
     */
    public static int sum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    /**
     * Given a float array, return the average of the entries
     *
     * @param array array in which we're interested
     * @return average of all entry values
     */
    public static float average(int[] array) {
        int sum = ArrayUtil.sum(array);
        float avg = (float) sum / (float) array.length;
        return avg;
    }

    /**
     * Given a float array, return the average of the entries
     *
     * @param array array in which we're interested
     * @return average of all entry values
     */
    public static float variance(int[] array) {
        float avg = ArrayUtil.average(array);
        float var = 0;
        for (int i = 0; i < array.length; i++){
            var += Math.pow(array[i] - avg, 2);
        }
        var /= (float) (array.length - 1);
        return var;
    }

    /**
     * Given a set of numbers and the index of the number to be treated as the
     * central point, generate a new set of numbers centered around this one and
     * having the specified number of intermediate values b/w the center and the
     * center's previous neighbors. In other words, if we have 1, 4, 7, 10, 13
     * and are told the center should be 7 with 2 new neighbors on each side,
     * the new set is 4, 5, 6, 7, 8, 9, 10. If the central number is at the end
     * of the list, use the step between it and its neigbor to extrapolate.
     * For example, if we were told that 13 should be
     * the new center, the new set is 10, 11, 12, 13, 14, 15, 16. Don't allow
     * numbers smaller than 1, so if the center point should be 1, the new set is
     * 1, 1, 1, 1, 2, 3, 4 (it's okay that these are not equally spaced)
     *
     * One application of this is to refine estimates of minimization.
     * @param x the original values of interest, sorted in ascending order
     * @param centerIndex the index of the value that should be the center
     * @param newNeighbors the number of new neighbors (in each direction) for
     *  the specified center
     */
    public static float[] refined_discretization(float[] x, int centerIndex,
            int newNeighbors){
        // You should always have the center, it's 2 previous neighbors, and
        // 2 times the number of new neighbors
        int numberOfValuesInNewSet = 3 + newNeighbors * 2;
        int centralIndex = (numberOfValuesInNewSet - 1) / 2;
        float[] refined_xs = new float[numberOfValuesInNewSet];
        float centerValue = x[centerIndex];
        float min_x = 0;
        float max_x = Integer.MAX_VALUE;
        if (centerIndex > 0){
            min_x = x[centerIndex - 1];
        }
        else{
            float distanceToNextNeighbor = x[centerIndex + 1] - centerValue;
            min_x = centerValue - (float)newNeighbors * distanceToNextNeighbor;
            if (min_x < 1){
                min_x = 1;
            }
        }
        if (centerIndex < (x.length - 1)){
            max_x = x[centerIndex + 1];
        }
        else{
            float distanceToNextNeighbor = centerValue - x[centerIndex - 1];
//            max_x = centerValue + (float)newNeighbors * distanceToNextNeighbor;
            max_x = centerValue + distanceToNextNeighbor;
        }

        float lower_step = (centerValue - min_x) / (float) (newNeighbors + 1);
        float upper_step = (max_x - centerValue) / (float) (newNeighbors + 1);
        refined_xs[0] = min_x;
        refined_xs[centralIndex] = centerValue;
        refined_xs[refined_xs.length - 1] = max_x;

        for (int i = 1; i <= newNeighbors;i++){
            refined_xs[i] = min_x + i * lower_step;
        }
        for (int i = 1; i <= newNeighbors;i++){
            refined_xs[numberOfValuesInNewSet - 1 - i] = max_x - i * upper_step;
        }


        return refined_xs;
    }
}
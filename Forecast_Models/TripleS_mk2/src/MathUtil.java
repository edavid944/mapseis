import java.util.Random;

public class MathUtil {

    public MathUtil() {
    }

    /**
     * erf approximated by Horner's method (fractional error in math formula less than 1.2 * 10 ^ -7).  Although subject to catastrophic cancellation when z in very close to 0
     * from http://www.cs.princeton.edu/introcs/home/
     *
     * @param z value at which we want to evaluate erf
     * @return erf(z)
     */
    public static double erf(double z) {
        if (Math.abs(z) > 5.92158719165) {
            // if z > 5.9215 erf(z) is, for all intents and purposes, unity
            // if z < -3.86 erf(z) is, for all intents and purposes, -unity
            return Math.signum(z);
        } else if (z == 0) {
            return 0;
        }
        double t = 1.0 / (1.0 + 0.5 * Math.abs(z));

        // use Horner's method
        double ans = 1 - t * Math.exp(-z * z - 1.26551223 +
                t * (1.00002368 +
                t * (0.37409196 +
                t * (0.09678418 +
                t * (-0.18628806 +
                t * (0.27886807 +
                t * (-1.13520398 +
                t * (1.48851587 +
                t * (-0.82215223 +
                t * (0.17087277))))))))));
        if (z >= 0) {
            return ans;
        } else {
            return -ans;
        }
    }
    
 /**
     * Determine if the ray specified by the two ordered pairs--the first ordered pair being the ray endpoint-- intersects the line segment by the second set of two ordered
     * pairs.  To do this, we determine the intersection of the two lines corresponding to the ray and the segment and determine if this intersection point falls on the line segment
     * and on the ray.  The line corresponding to the ray is given:
     * 
     * y = (y4 - y1) / (x4 - x1) * x + y4 - (y4 - y1) / (x4 - x1) * x4
     * 
     * and the line corresponding to the line segment is given:
     * 
     * y = (y3 - y2) / (x3 - x2) * x + y3 - (y3 - y2) / (x3 - x2) * x3
     * 
     * We determine the intersection (x*, y*) of these two lines and determine if this point falls on the ray and on the line segment by checking the following conditions:
     *
     * min(x2, x3) <= x* <= max(x2, x3)
     * min(y2, y3) <= y* <= max(y2, y3)
     *
     * x* >= x1 if x1 < x4
     * x* <= x1 if x1 > x4
     * y* >= y1 if y1 < y4
     * y* <= y1 if y1 > y4
     * 
     * @param x1 x-value of the ray endpoint
     * @param y1 y-value of the ray endpoint
     * @param x4 x-value of arbitrary point on the ray
     * @param y4 y-value of arbitrary point on the ray
     * @param x2 x-value of the first line segment endpoint
     * @param y2 y-value of the first line segment endpoint
     * @param x3 x-value of the second  line segment endpoint
     * @param y3 y-value of the second  line segment endpoint
     * @return boolean answering the question "Does the specified ray intersect the specified line segment?"
     */
    public static boolean doesRayIntersectLineSegment(float x1, float y1, float x4, float y4, float x2, float y2, float x3, float y3) {
        // determine the intersection
        float xStar = (y3 - (y3 - y2) / (x3 - x2) * x3 - y4 + (y4 - y1) / (x4 - x1) * x4) / ((y4 - y1) / (x4 - x1) - (y3 - y2) / (x3 - x2));
        // If the line segment is vertical, then x* is just x2
        if (x2 == x3) {
            xStar = x2;
        }
        float yStar = (y4 - y1) / (x4 - x1) * xStar + y4 - (y4 - y1) / (x4 - x1) * x4;

//        if (Float.isNaN(xStar)  || Float.isNaN(yStar)){
//            return false;
//        }

        // determine if the intersection falls on the line segment
        if (xStar < (float) Math.min(x2, x3)) {
            return false;
        }
        if (xStar > (float) Math.max(x2, x3)) {
            return false;
        }
        if (yStar < (float) Math.min(y2, y3)) {
            return false;
        }
        if (yStar > (float) Math.max(y2, y3)) {
            return false;
        }

        // determine if the intersection falls on the ray
        if (x1 < x4 && xStar < x1) {
            return false;
        }
        if (x1 > x4 && xStar > x1) {
            return false;
        }
        if (y1 < y4 && yStar < y1) {
            return false;
        }
        if (y1 > y4 && yStar > y1) {
            return false;
        }


        // If we're here, it means the intersection falls on the line segment and on the ray, indicating that the ray does intersect the line segment
        return true;
    }
    
/**
     * Determine if the specified point falls within the specified polygon.  To do this, we construct an arbitrary ray for which the point of interest serves as the endpoint and
     * count the number of edges that the ray intersects.  If the number of intersections is odd, this indicates that the point is inside the polygon, otherwise it is outside.
     * 
     * @param x1 x-value of the point of interest
     * @param y1 y-value of the point of interest
     * @param polygonXs x-values of the polygon vertices
     * @param polygonYs y-values of the polygon vertices
     * @return boolean answering the question "Does the specified point lie within the specified polygon?"
     */
    public static boolean isPointInsidePolygon(float x, float y, float[] polygonXs, float[] polygonYs) {
        if (polygonXs[0] != polygonXs[polygonXs.length - 1] || polygonYs[0] != polygonYs[polygonYs.length - 1]) {
            System.err.println("The first and last points of the polygon must be the same in order for it to be considered closed.");
            System.exit(-1);
        }

        short numberOfIntersections = 0;
        int numberOfEdges = polygonXs.length - 1;
        Random rndgen = new Random();

        // Pick a random point to construct the ray
        float rayPointX = rndgen.nextFloat();
        float rayPointY = rndgen.nextFloat();

        for (int i = 0; i < numberOfEdges; i++) {
            float edgePointX1 = polygonXs[i];
            float edgePointY1 = polygonYs[i];
            float edgePointX2 = polygonXs[i + 1];
            float edgePointY2 = polygonYs[i + 1];
            if (doesRayIntersectLineSegment(x, y, rayPointX, rayPointY, edgePointX1, edgePointY1, edgePointX2, edgePointY2)) {
                numberOfIntersections++;
            }
        }
//        System.out.println("# intersections = " + numberOfIntersections);
        if (numberOfIntersections % 2 == 1) {
            return true;
        }
        return false;
    }

    /**
     * Compute the factorial of the specified number in the simplest fashion
     *
     * @param n the number for which we want to compute the factorial
     * @return n! = n * (n - 1) * (n - 2) * ... * 2
     */
    public static int factorial(short n) {
        if (n < 0 || n > 12) {
            // n < 0 is undefined and n > 12 causes overflow
            System.err.println("Error in MathUtil.factorial: 0 <= n <= 12");
            System.exit(-1);
        }
        int result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    /**
     * Compute the negative binomial probability density at the specified point
     * with the specified parameter values, following Kagan 2010 GJI Eq'n 15
     *
     * @param k the value at which we want to compute the negative binomial
     *      probability density
     * @param tau unnamed negative binomial parameter
     * @param theta unnamed negative binomial parameter
     * @return gamma(z)
     */
    public static double negative_binomial_density(int k, double tau,
            double theta){
        if (k < 0){
            System.err.println("negative_binomial_density: k must be "
                    + "non-negative. k = " + k);
            System.exit(-1);
        }
        if (tau <= 0 || tau >= 1){
            System.err.println("negative_binomial_density: tau must be b/w "
                    + "0 and 1, exclusive. tau = " + tau);
            System.exit(-1);
        }
        if (theta >= 1){
            System.err.println("negative_binomial_density: theta must be less"
                    + " than 1. theta = " + theta);
            System.exit(-1);
        }
        if (k == 0){
            return Math.pow(theta, tau);
        }

        return gamma(tau + k) / gamma(tau) / factorial(k) *
                Math.pow(theta, tau) * Math.pow(1- theta, k);
    }

    /**
     * Estimate the value of the Gamma function at the specified value, using
     * the Lanczos approximation, from
     *
     * http://en.wikipedia.org/wiki/Lanczos_approximation
     *
     * @param z the value at which we want to compute the gamma function
     * @return gamma(z)
     */
    public static double gamma(double z){
        // A Python to Java translation of the example implementation from
        // Wikipedia
        /**
        g = 7
        p = [0.99999999999980993, 676.5203681218851, -1259.1392167224028,
             771.32342877765313, -176.61502916214059, 12.507343278686905,
             -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7]

        def gamma(z):
            z = complex(z)
            if z.real < 0.5:
                return pi / (sin(pi*z)*gamma(1-z))
            else:
                z -= 1
                x = p[0]
                for i in range(1, g+2):
                    x += p[i]/(z+i)
                t = z + g + 0.5
                return sqrt(2*pi) * t**(z+0.5) * exp(-t) * x
         **/
        int g = 7;
        double[] p = {0.99999999999980993, 676.5203681218851, -1259.1392167224028,
             771.32342877765313, -176.61502916214059, 12.507343278686905,
             -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7};

        if (z < 0.5){
            return Math.PI / Math.sin(Math.PI * z) * gamma(1 - z);
        }
        else{
            z -= 1;
            double x = p[0];
            for (int i = 1; i < g + 2; i++){
                x += p[i] / (z + i);
            }
            double t = z + g + 0.5;
            return Math.sqrt(2 * Math.PI) * Math.pow(t, z + 0.5) *
                    Math.exp(-t) * x;
        }
    }

    /**
     * Compute the factorial of the specified number in the simplest fashion
     *
     * @param n the number for which we want to compute the factorial
     * @return n! = n * (n - 1) * (n - 2) * ... * 2
     */
    public static int factorial(int n) {
        if (n < 0 || n > 12) {
            // n < 0 is undefined and n > 12 causes overflow
            System.err.println("0 <= n <= 12");
            System.exit(-1);
        }
        int result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}

import java.util.Calendar;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class OptimizationTools {

    public OptimizationTools() {
    }

    /**
     * Execute processing steps required to generate a forecast for the
     * specified testing regions.  Namely, for each region, we:
     * 
     * 1. Generate the catalogs used to construct and test retrospective 
     *  forecasts; also generate the catalog used to construct the prospective
     *  forecast.
     * 2. Construct retrospective forecasts using different parameter values.
     * 3. Evaluate the retrospective forecasts and determine the optimal
     *  parameter values.
     * 4. Construct a prospective forecast using the optimal parameter value
     *  determined in Step 3 and the catalog generated in Step 1.
     */
    public static void main(String[] args) {
        String parameterFilePath = args[0];
        Properties Props = new Properties();
        String inputCatalogPath = "";
        String inputCatalogStartDate = "";
        String inputCatalogEndDate = "";
        String rateForecastMLPath = "";
        String collectionRegionCatalogPath = "";
        String retrospectiveSmoothingCatalogPath = "";
        String retrospectiveTargetCatalogPath = "";
        String prospectiveSmoothingCatalogPath = "";
        String afvPath = "";
        String prospectiveForecastStartDate = "";
        String prospectiveForecastEndDate = "";
        String prospectiveSmoothingPeriodStart = "";
        String prospectiveSmoothingPeriodEnd = "";
        String retrospectiveSmoothingPeriodStart = "";
        String retrospectiveSmoothingPeriodEnd = "";
        String retrospectiveTargetPeriodStart = "";
        String retrospectiveTargetPeriodEnd = "";
        String forecastNodesPath = "";
        float minLat = 0f;
        float maxLat = 0f;
        float minLon = 0f;
        float maxLon = 0f;
        float boxSize = 0f;
        float minTargetMagnitude = 0f;
        float maxTargetMagnitude = 0f;
        float minDepth = 0f;
        float maxDepth = 0f;
        float magnitudeBinSize = 0f;
        float [] smoothingLengthscales = null;

        try {
            FileInputStream in = new FileInputStream(parameterFilePath);
            Props.load(in);
            in.close();
            in = null;

            inputCatalogPath = Props.getProperty("inputCatalogPath");
            inputCatalogStartDate = Props.getProperty("inputCatalogStartDate");
            inputCatalogEndDate = Props.getProperty("inputCatalogEndDate");
            rateForecastMLPath = Props.getProperty("rateForecastMLPath");
            collectionRegionCatalogPath =
                    Props.getProperty("collectionRegionCatalogPath");
            retrospectiveSmoothingCatalogPath =
                    Props.getProperty("retrospectiveSmoothingCatalogPath");
            retrospectiveTargetCatalogPath =
                    Props.getProperty("retrospectiveTargetCatalogPath");
            prospectiveSmoothingCatalogPath =
                    Props.getProperty("prospectiveSmoothingCatalogPath");
            afvPath = Props.getProperty("afvPath");
            prospectiveForecastStartDate =
                    Props.getProperty("forecastStartDate");
            prospectiveForecastEndDate = Props.getProperty("forecastEndDate");
            prospectiveSmoothingPeriodStart =
                    Props.getProperty("prospectiveSmoothingPeriodStart");
            prospectiveSmoothingPeriodEnd =
                    Props.getProperty("prospectiveSmoothingPeriodEnd");
            retrospectiveSmoothingPeriodStart =
                    Props.getProperty("retrospectiveSmoothingPeriodStart");
            retrospectiveSmoothingPeriodEnd =
                    Props.getProperty("retrospectiveSmoothingPeriodEnd");
            retrospectiveTargetPeriodStart =
                    Props.getProperty("retrospectiveTargetPeriodStart");
            retrospectiveTargetPeriodEnd =
                    Props.getProperty("retrospectiveTargetPeriodEnd");
            forecastNodesPath = Props.getProperty("forecastNodesPath");

            minLat = Float.parseFloat(Props.getProperty("minLat"));
            maxLat = Float.parseFloat(Props.getProperty("maxLat"));
            minLon = Float.parseFloat(Props.getProperty("minLon"));
            maxLon = Float.parseFloat(Props.getProperty("maxLon"));

            boxSize = Float.parseFloat(Props.getProperty("boxSize"));
            minTargetMagnitude = Float.parseFloat(
                    Props.getProperty("minTargetMagnitude"));
            maxTargetMagnitude = Float.parseFloat(
                    Props.getProperty("maxTargetMagnitude"));
            magnitudeBinSize = Float.parseFloat(
                    Props.getProperty("magnitudeBinSize"));
            minDepth = Float.parseFloat(Props.getProperty("minDepth"));
            maxDepth = Float.parseFloat(Props.getProperty("maxDepth"));
            String allLengthscales = Props.getProperty("smoothingLengthscales");
            if (allLengthscales != null){
                String [] lengthscales = allLengthscales.split(",");
                smoothingLengthscales = new float[lengthscales.length];
                for (int i = 0; i < lengthscales.length; i++){
                    smoothingLengthscales[i] = Float.parseFloat(lengthscales[i]);
                }
            }
            else{
                float[] sigmas = {5f, 10f, 20f, 25f, 30f, 50f, 75f, 100f, 200f};
                smoothingLengthscales = sigmas;
            }
        } catch (Exception ex) {
            System.out.println("Error in OptimizationTools.main(" +
                    parameterFilePath + ")");
            ex.printStackTrace();
            System.exit(-1);
        }

        System.out.println("Generating catalogs...");
        catalogs(inputCatalogPath, collectionRegionCatalogPath, retrospectiveSmoothingCatalogPath,
                retrospectiveTargetCatalogPath, prospectiveSmoothingCatalogPath,
                prospectiveSmoothingPeriodStart, prospectiveSmoothingPeriodEnd,
                retrospectiveSmoothingPeriodStart,
                retrospectiveSmoothingPeriodEnd,
                retrospectiveTargetPeriodStart, retrospectiveTargetPeriodEnd,
                minTargetMagnitude, maxTargetMagnitude, minLat, maxLat, minLon,
                maxLon, minDepth, maxDepth);

        System.out.println("Constructing retrospective forecasts...");
        retrospectiveForecasts(retrospectiveSmoothingCatalogPath, afvPath,
                minLat, maxLat, minLon, maxLon, boxSize, smoothingLengthscales);

        System.out.println("Estimating variance of the number of "
                + "target eqks...");
        float rateVariance = estimateAnnualVarianceFromCatalog(inputCatalogPath,
                inputCatalogStartDate, inputCatalogEndDate, 
                minLat, maxLat, minLon, maxLon, minDepth, maxDepth, 
                minTargetMagnitude, maxTargetMagnitude);
        System.out.println("Annual variance estimated from the catalog is " +
                rateVariance);
        // scale the variance to the length of the retro-experiment
        float retroExperimentDuration = DateUtil.durationInOrderedDays(
                retrospectiveSmoothingPeriodStart,
                retrospectiveSmoothingPeriodEnd) / 365.25f;
        float retroRateVariance = rateVariance * retroExperimentDuration *
                retroExperimentDuration;

        System.out.println("Evaluating retrospective forecasts and determining "
                + "rough optimal parameter values...");
        int optimalSigmaIndex = retrospectiveEvaluation_negative_binomial(
                retrospectiveTargetCatalogPath, afvPath, minLat, maxLat, minLon,
                maxLon, boxSize, retroRateVariance, smoothingLengthscales);
        float optimalSigma = smoothingLengthscales[optimalSigmaIndex];
                
        //  Consider a finer discretization of sigmas around the current
        //  optimal value
        float[] fineSmoothingLengthscales = ArrayUtil.refined_discretization(
                smoothingLengthscales, optimalSigmaIndex, 2);
        System.out.println("Constructing finer retrospective forecasts...");
        retrospectiveForecasts(retrospectiveSmoothingCatalogPath, afvPath,
                minLat, maxLat, minLon, maxLon, boxSize,
                fineSmoothingLengthscales);

        System.out.println("Evaluating retrospective forecasts and "
                + "determining more refined optimal parameter values...");
        optimalSigmaIndex = retrospectiveEvaluation_negative_binomial(
                retrospectiveTargetCatalogPath, afvPath, minLat, maxLat,
                minLon, maxLon, boxSize, retroRateVariance,
                fineSmoothingLengthscales);

        optimalSigma = fineSmoothingLengthscales[optimalSigmaIndex];

        // scale the variance to the length of the retro-experiment
        float proExperimentDuration = DateUtil.durationInOrderedDays(
                prospectiveSmoothingPeriodStart, prospectiveSmoothingPeriodEnd)
                / 365.25f;
        float proRateVariance = rateVariance * proExperimentDuration *
                proExperimentDuration;

        System.out.println("Constructing prospective forecast with sigma = " +
                optimalSigma + " km...");
        prospectiveForecasts(prospectiveSmoothingCatalogPath, inputCatalogPath, 
                afvPath, optimalSigma, rateForecastMLPath,
                prospectiveForecastStartDate, prospectiveForecastEndDate,
                minTargetMagnitude, maxTargetMagnitude, minLat, maxLat, minLon,
                maxLon, boxSize, forecastNodesPath, minDepth, maxDepth,
                magnitudeBinSize, proRateVariance);
    }

    /**
     * Estimate the variance of the annual number of target eqks in the
     * specified catalog
     *
     * @param targetCatalogFile path to the retrospective target catalog
     * @param afvPath path where the alarm function value files are stored
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     * @return lengthscale that minimizes our misfit statistic
     */
    private static float estimateAnnualVarianceFromCatalog(String catalogPath,
            String catalogStart, String catalogEnd, float minLat, float maxLat, 
            float minLon, float maxLon, float minDepth, float maxDepth,
            float minMag, float maxMag) {
        Catalog historicalTargetEqks = new Catalog(catalogPath,
                Catalog.ZMAP);
        historicalTargetEqks = historicalTargetEqks.subcatalogByMagnitude(
                minMag, maxMag);
        historicalTargetEqks = historicalTargetEqks.subcatalogBySpace(minLat,
                maxLat, minLon, maxLon, minDepth, maxDepth);
        float intervalOfInterest = 365.25f;
        int intervalInYears = (int) Math.round(intervalOfInterest / 365.25f);
        float catalogDuration = DateUtil.durationInOrderedDays(catalogStart,
                catalogEnd);
        int subcatalogs = (int) Math.floor(catalogDuration / intervalOfInterest);
        int[] eqksPerPeriod = new int[subcatalogs];

        for (int i = 0; i < eqksPerPeriod.length; i++){
            String minDate = DateUtil.offsetDate(catalogStart, Calendar.YEAR,
                    i * intervalInYears);
            String maxDate = DateUtil.offsetDate(minDate, Calendar.YEAR,
                    intervalInYears);

            Catalog period = historicalTargetEqks.subcatalogByTime(minDate,
                    maxDate);
            eqksPerPeriod[i] = period.numberOfEqks();
        }

//        System.out.println("eqksPerPeriod = " + Arrays.toString(eqksPerPeriod));
        float variance = ArrayUtil.variance(eqksPerPeriod);

        return variance;
    }

    /**
     * Evaluate the retrospective forecasts for the region of interest and 
     * determine which one yields the minimum "misfit." To do this, we find the
     * forecast which obtains the maximum spatial joint likelihood
     *
     * @param targetCatalogFile path to the retrospective target catalog
     * @param afvPath path where the alarm function value files are stored
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     * @return lengthscale that minimizes our misfit statistic
     */
    private static int retrospectiveEvaluation_negative_binomial(
            String retrospectiveTargetCatalogPath, String afvPath, float minLat,
            float maxLat, float minLon, float maxLon, float boxSize,
            float variance, float[] sigmas) {
//            Load the target catalog.
        short[] targetEqkDistribution =
                Utility.eventMapFromCatalog(retrospectiveTargetCatalogPath,
                    minLat, maxLat, minLon, maxLon, boxSize);

        double[] lls = new double[sigmas.length];

        System.out.println("sigma\tspatial joint LL");

        // For each lengthscale, compute the spatial joint log likelihood
        for (int i = 0; i < sigmas.length; i++) {
            float sigma = sigmas[i];
            String afvFile = afvPath + "retro" + sigma + ".afv";
            AlarmFunction af = new AlarmFunction(afvFile);
            float[] afValues = af.values();
            lls[i] = LikelihoodTools.spatialJointLL_negative_binomial(afValues,
                        targetEqkDistribution, variance);
            System.out.println(sigma + "\t" + lls[i]);
        }

        // Determine the optimal lengthscale (the one that maximizes ll)
        double maxLL = Double.NEGATIVE_INFINITY;
        int optimalSigmaIndex = 0;

        for (int i = 0; i < lls.length; i++) {
            double ll = lls[i];
            if (ll > maxLL) {
                maxLL = ll;
                optimalSigmaIndex = i;
            }
        }

        return optimalSigmaIndex;
    }

    /**
     * Construct several retrospective forecasts for the specified testing 
     * region, using a Gaussian smoothing kernel and varying the smoothing
     * lengthscale.
     * 
     * @param smoothingCatalogPath path to the retrospective smoothing catalog
     * @param afvPath path where the retrospective forecasts are stored
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     */
    private static void retrospectiveForecasts(String smoothingCatalogPath, 
            String afvPath, float minLat, float maxLat, float minLon,
            float maxLon, float boxSize, float[] sigmas) {
        float[] gridParameters = {minLat, maxLat, minLon, maxLon, boxSize};

        Catalog smoothingCatalog = new Catalog(smoothingCatalogPath,
                Catalog.JEREMY_GENERATED);

//        Generate Gaussian smoothed alarm functions with 
        for (int i = 0; i < sigmas.length; i++) {
            float sigma = sigmas[i];
            float[] spatialSmoothingParameters = {sigma, 0f};
            String afvFilePath = afvPath + "retro" + sigma + ".afv";
            float[] afv = SmoothingTools.smoothedEventMapFromCatalog(
                    smoothingCatalog, minLat, maxLat, minLon, maxLon, boxSize,
                    spatialSmoothingParameters);

        // For the bins that are currently zero, put a very small value in there
        // In particular, find the smallest value in nonzero bins and put half
        // that value in all the currently zero bins
        float min = 0.5f * ArrayUtil.minimumPositiveValue(afv);
        for (int j = 0; j< afv.length; j++){
            if (afv[j] == 0f){
                afv[j] = min;
            }
        }
            // From the smoothed seismicity map, generate and save the corresponding alarm function value file
            AlarmFunction af = new AlarmFunction(afv, gridParameters);
            af.save(afvFilePath);
        }

    }

    /**
     * Construct prospective forecasts for the specified testing region, using
     * a Gaussian smoothing kernel and the specified smoothing lengthscale.
     * One forecast is saved in scaled format, such that the total number of
     * earthquakes forecast is equal to the average number of target earthquakes
     * per year over the duration of the catalog, and the other is saved in
     * unscaled format such that it is more of an alarm-based forecast as
     * opposed to a rate forecast.
     * 
     * @param prospectiveSmoothingCatalogPath path to the prospective smoothing catalog
     * @param afvPath path where the prospective forecast will be stored
     * @param sigma lengthscale to be used for smoothing
     * @param forecastMLPath path to which we'll write output forecast file, in ForecastML format
     * @param forecastStartDate start date of prospective forecast
     * @param forecastEndDate end date of prospective forecast
     * @param minTargetMagnitude minimum target magnitude
     * @param maxTargetMagnitude maximum target magnitude  
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     * @param forecastNodesPath path to the forecast nodes of interest, we'll
     *          save the forecast only at these nodes
     * @param minDepth minimum depth of the study region
     * @param maxDepth maximum depth of the study region
     * @param magnitudeBinSize magnitude discretization employed for the 
     *  forecast
     * @param rateVariance overall rate variance to be used for generating
     *  negative binomial forecast
     */
    private static void prospectiveForecasts(
            String prospectiveSmoothingCatalogPath, String entireCatalogPath,
            String afvPath, float sigma, String rateForecastMLPath,
            String forecastStartDate, String forecastEndDate,
            float minTargetMagnitude, float maxTargetMagnitude, float minLat,
            float maxLat, float minLon, float maxLon, float boxSize,
            String forecastNodesPath, float minDepth, float maxDepth,
            float magnitudeBinSize, float rateVariance) {
        // Determine the average number of target earthquakes per year
        Catalog prospectiveSmoothingCatalog = new Catalog(
                prospectiveSmoothingCatalogPath, Catalog.JEREMY_GENERATED);
        Catalog historicalTargetEqks = new Catalog(entireCatalogPath,
                Catalog.ZMAP);
        historicalTargetEqks = historicalTargetEqks.subcatalogByMagnitude(
                minTargetMagnitude, maxTargetMagnitude);
        historicalTargetEqks = historicalTargetEqks.subcatalogBySpace(minLat,
                maxLat, minLon, maxLon, minDepth, maxDepth);
        String[] historicalCatalogTimes = historicalTargetEqks.times();
        String historicalCatalogStart = historicalCatalogTimes[0];
        String historicalCatalogEnd =
                historicalCatalogTimes[historicalCatalogTimes.length - 1];
        float historicalCatalogLengthInDays = DateUtil.durationInOrderedDays(
                historicalCatalogStart, historicalCatalogEnd);
        float averageNumberOfTargetEqksPerYear = 
                (float) historicalTargetEqks.numberOfEqks() /
                (historicalCatalogLengthInDays / 365.25f);
        System.out.println("average # of eqks/yr: " +
                averageNumberOfTargetEqksPerYear);

        float[] gridParameters = {minLat, maxLat, minLon, maxLon, boxSize};

        // Smooth the catalog over the testing region
        float[] spatialSmoothingParameters = {sigma, 0f};
        float[] scaledAFV = SmoothingTools.smoothedEventMapFromCatalog(
                prospectiveSmoothingCatalog, minLat, maxLat, minLon, maxLon,
                boxSize, spatialSmoothingParameters);

        // For the bins that are currently zero, put a very small value in there
        // In particular, find the smallest value in nonzero bins and put half
        // that value in all the currently zero bins
        float min = 0.5f * ArrayUtil.minimumPositiveValue(scaledAFV);
        for (int j = 0; j< scaledAFV.length; j++){
            if (scaledAFV[j] == 0f){
                scaledAFV[j] = min;
            }
        }

        float[] unscaledAFV = new float[scaledAFV.length];
        System.arraycopy(scaledAFV, 0, unscaledAFV, 0, unscaledAFV.length);

        float unscaledSum = ArrayUtil.sum(scaledAFV);
        // The AF values are now the sum over the length of the historical catalog.  Now we scale them for a forecast of the appropriate duration
        float forecastExperimentDurationInDays = 
                DateUtil.durationInOrderedDays(forecastStartDate,
                    forecastEndDate);
        System.out.println("experiment duration:" +
                forecastExperimentDurationInDays);
        for (int i = 0; i < scaledAFV.length; i++) {
            scaledAFV[i] *= averageNumberOfTargetEqksPerYear / unscaledSum *
                    forecastExperimentDurationInDays / 365.25f;
        }

// From the smoothed seismicity map, generate and save the corresponding alarm function value file
        AlarmFunction rateForecast=new AlarmFunction(scaledAFV, gridParameters);
//        float b = prospectiveSmoothingCatalog.akiMLEBValue();
        float b = 1;

        // If the forecast node path is not specified, save the entire rate forecast
        if (forecastNodesPath == null) {
            rateForecast.saveAsRateForecast(rateForecastMLPath,
                    forecastStartDate, forecastEndDate, b, minDepth, maxDepth,
                    minTargetMagnitude, maxTargetMagnitude, magnitudeBinSize);
            // If the forecast node path is empty, save the entire rate forecast
        } else if (forecastNodesPath.isEmpty()) {
            rateForecast.saveAsRateForecast(rateForecastMLPath,
                    forecastStartDate, forecastEndDate, b, minDepth, maxDepth,
                    minTargetMagnitude, maxTargetMagnitude, magnitudeBinSize);
            // Otherwise, save the rate forecast only for the cells of interest
        } else {
            rateForecast.saveAsRateForecast(rateForecastMLPath, 
                    forecastStartDate, forecastEndDate, forecastNodesPath, b,
                    minDepth, maxDepth, minTargetMagnitude, maxTargetMagnitude,
                    magnitudeBinSize);
        }

        rateForecast.saveAsASCII(rateForecastMLPath.replace(".xml", ".tsv"),
                forecastNodesPath, b, minTargetMagnitude, maxTargetMagnitude,
                magnitudeBinSize, minDepth, maxDepth, rateVariance);
//        rateForecast.saveAsASCII(rateForecastMLPath.replace(".xml", ".tsv"), forecastNodesPath);
    }

    /**
     * Generate the catalogs to be used for retrospective and prospective smoothing experiments for the testing region of interest
     * 
     * @param entireCatalogPath path to the complete collection region JMA catalog in ZMAP format
     * @param collectionRegionCatalogPath path to which we'll save the catalog that has been filtered to the collection region
     * @param retrospectiveSmoothingCatalogPath path to which we'll save the retrospective smoothing catalog
     * @param retrospectiveTargetCatalogPath path to which we'll save the retrospective target catalog
     * @param prospectiveSmoothingCatalogPath path to which we'll save the prospective smoothing catalog
     * @param prospectiveSmoothingPeriodStart start date of the prospective smoothing period
     * @param prospectiveSmoothingPeriodEnd end date of the prospective smoothing period
     * @param retrospectiveSmoothingPeriodStart start date of the retrospective smoothing period
     * @param retrospectiveSmoothingPeriodEnd end date of the retrospective smoothing period  
     * @param minTargetMagnitude minimum target magnitude
     * @param maxTargetMagnitude maximum target magnitude  
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region 
     */
    private static void catalogs(String entireCatalogPath, String collectionRegionCatalogPath, String retrospectiveSmoothingCatalogPath,
            String retrospectiveTargetCatalogPath, String prospectiveSmoothingCatalogPath, String prospectiveSmoothingPeriodStart, String prospectiveSmoothingPeriodEnd,
            String retrospectiveSmoothingPeriodStart, String retrospectiveSmoothingPeriodEnd, String retrospectiveTargetPeriodStart, String retrospectiveTargetPeriodEnd,
            float minTargetMagnitude, float maxTargetMagnitude, float minLat, float maxLat, float minLon, float maxLon, float minDepth, float maxDepth) {
        // Load the entire ZMAP catalog
        Catalog entireCatalog = new Catalog(entireCatalogPath, Catalog.ZMAP);
        entireCatalog = entireCatalog.subcatalogByMagnitude(minTargetMagnitude - 2f, 10f); // filter to only the magnitude events in which we're interested

        // For the collection region, we'll extend the testing region one degree in each direction to reduce edge effects
        float minLat_collection = minLat - 1f;
        float maxLat_collection = maxLat + 1f;
        float minLon_collection = minLon - 1f;
        float maxLon_collection = maxLon + 1f;

        Catalog collectionRegionCatalog = entireCatalog.subcatalogBySpace(minLat_collection, maxLat_collection, minLon_collection, maxLon_collection, minDepth, maxDepth);
        collectionRegionCatalog.save(collectionRegionCatalogPath, "Catalog generated by loading catalog from " + entireCatalogPath + " and filtering by "
                + "min/max lat/lon/depth to the collection region");

        // Generate the retrospective smoothing catalog, which contains all independent events in the collection region during the retrospective smoothing period
        Catalog retrospectiveSmoothingCatalog = collectionRegionCatalog.subcatalogByTime(retrospectiveSmoothingPeriodStart, retrospectiveSmoothingPeriodEnd);
        retrospectiveSmoothingCatalog = retrospectiveSmoothingCatalog.subcatalogByMagnitude(minTargetMagnitude - 2f, maxTargetMagnitude);
        retrospectiveSmoothingCatalog.save(retrospectiveSmoothingCatalogPath, "Catalog generated by loading catalog from "
                + entireCatalogPath + " and filtering by date");

        // Generate the retrospective target catalog, which contains all target eqks in the testing region in the retrospective target period
        Catalog retrospectiveTargetCatalog = collectionRegionCatalog.subcatalogByTime(retrospectiveTargetPeriodStart, retrospectiveTargetPeriodEnd);
        retrospectiveTargetCatalog = retrospectiveTargetCatalog.subcatalogByMagnitude(minTargetMagnitude, maxTargetMagnitude);
        retrospectiveTargetCatalog = retrospectiveTargetCatalog.subcatalogBySpace(minLat, maxLat, minLon, maxLon, minDepth, maxDepth);
        retrospectiveTargetCatalog.save(retrospectiveTargetCatalogPath, "Catalog generated by loading catalog from "
                + collectionRegionCatalogPath + " and filtering by date, min/max lat/lon/depth, and by magnitude");

        // Generate a prospective smoothing catalog
        Catalog prospectiveSmoothingCatalog = collectionRegionCatalog.subcatalogByTime(prospectiveSmoothingPeriodStart, prospectiveSmoothingPeriodEnd);
        prospectiveSmoothingCatalog = prospectiveSmoothingCatalog.subcatalogByMagnitude(minTargetMagnitude - 2f, maxTargetMagnitude);
        prospectiveSmoothingCatalog.save(prospectiveSmoothingCatalogPath, "Catalog generated by loading catalog from "
                + entireCatalogPath + " and filtering by date, yielding all independent events in testing region during the prospective smoothing period");
    }
}

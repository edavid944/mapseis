/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jzechar
 */
public class LikelihoodTools {

    /**
     * Calculate the spatial joint log-likelihood for the given target eqk catalog and using the specified rate forecast as the predictor; each of these is provided as array that represents
     * a lat-lon-mag gridding of the study region.  predictor contains the rate forecast in each bin, and targetEqks contains the number of target eqks in each bin.
     * The idea here is to scale the rate forecast so that the total forecast rate over all bins is the same as the number of observed target eqks, and then compute the joint log-likelihood
     * of the observations given the forecast
     *
     * @param predictor alarm function values
     * @param targetEqks representation of target eqk distribution, each entry contains the number of target epicenters contained w/i this bin
     * @param numberOfTargetEqks total number of target eqks in the target catalog
     * @return spatial joint log likelihood of the observations given the forecast
     */
    public static double spatialJointLL(float[] predictor, short[] targetEqks, int numberOfTargetEqks) {
        float totalRateForecast = ArrayUtil.sum(predictor);
        float[] normalizedRateForecast = new float[predictor.length];

        for (int i = 0; i < targetEqks.length; i++) {
            normalizedRateForecast[i] = predictor[i] / totalRateForecast * (float) numberOfTargetEqks;
        }

        double jointLL = 0;

        for (int i = 0; i < normalizedRateForecast.length; i++) {
            float forecastRate = normalizedRateForecast[i];
            short observations = targetEqks[i];
            double ll = -forecastRate;
            if (observations > 0){
//                System.out.println("shite");
                ll = -forecastRate + observations * Math.log(forecastRate) - Math.log(MathUtil.factorial(observations));
            }

            
//            if (ll == Double.NaN){
//                System.out.println("shite");
//            }
            jointLL += ll;
        }
        return jointLL;
    }

    /**
     * Calculate the negative binomial spatial joint log-likelihood for the
     * given target eqk catalog and the specified forecast; each of these
     * is provided as array that represents a lat-lon-mag gridding of the study
     * region.  The forecast vector contains the expected number of target eqks
     * in each bin, and the observation vector contains the number of target
     * eqks in each bin.
     * The idea here is to scale the rate forecast so that the total forecast
     * rate over all bins is the same as the number of observed target eqks,
     * and then compute the joint log-likelihood of the observations given the
     * forecast
     *
     * @param forecast vector of forecast rate expectations
     * @param variance variance of total number of earthquakes, to be
     *      distributed over all bins
     * @param observation representation of target eqk distribution, each entry
     *      contains the number of target epicenters contained w/i this bin
     * @param numberOfTargetEqks total number of target eqks in the target
     *      catalog
     * @return spatial joint log likelihood of the observations given the forecast
     */
    public static double spatialJointLL_negative_binomial(float[] forecast,
            short[] observation, float variance) {
        float totalRateForecast = ArrayUtil.sum(forecast);
        short numberOfTargetEqks = ArrayUtil.sum(observation);
        float theta = (float) numberOfTargetEqks / variance;
        float[] normalizedRateForecast = new float[forecast.length];

        for (int i = 0; i < observation.length; i++) {
            normalizedRateForecast[i] = forecast[i] / totalRateForecast *
                    (float) numberOfTargetEqks;
        }

        double jointLL = 0;
//        double sumFromNontargetBins = 0;
//        double sumFromTargetBins = 0;
        for (int i = 0; i < normalizedRateForecast.length; i++) {
            float forecastRate = normalizedRateForecast[i];
            float tau = forecastRate * theta / (1f - theta);
            short numberOfEqks = observation[i];
            double ll = 0;
            if (numberOfEqks > 0 && forecastRate == 0){
                return Double.NEGATIVE_INFINITY;
            }
            if (!(forecastRate == 0 && numberOfEqks == 0)){
//                if (numberOfEqks > 0){
//                    System.out.println("");
//                }
                ll = Math.log(MathUtil.negative_binomial_density(numberOfEqks,
                        tau, theta));
//                if (numberOfEqks == 0){
//                    sumFromNontargetBins += ll;
//                }
//                else{
//                    sumFromTargetBins += ll;
//                    System.out.println(forecastRate + ", " + numberOfEqks);
//                }
            }
            jointLL += ll;
        }
//        System.out.println("from bins with target eqks: " + sumFromTargetBins);
//        System.out.println("from bins without target eqks: " + sumFromNontargetBins);
//        System.out.println("");
        return jointLL;
    }

}

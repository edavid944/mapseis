public class SmoothingTools {

    public SmoothingTools() {
    }

    /**
     * Compute the contribution of an eqk at (eqkLat, eqkLon) to a cell with lat/lon range cellLat1 - cellLat2, cellLon1 - cellLon2, using a bivariate normal distribution with the specified standard
     * deviation.  See "Bivariate normal distributioncontribution.doc" for notes on algorithm.
     *
     * @param eqkLat latitude of epicenter
     * @param eqkLon longitude of epicenter
     * @param cellLat1 minimum latitude of cell under consideration
     * @param cellLat2 maximum latitude of cell under consideration
     * @param cellLon1 minimum longitude of cell under consideration
     * @param cellLon2 maximum longitude of cell under consideration
     * @param lengthScale standard deviation of bivariate normal distribution used to smooth (specified in km)
     * @return contribution of epicenter at specified location to cell at specified location
     * using a bivariate normal smoothing kernel with specified standard deviation
     */
    private static double contributionFromSmoothedEqk_Gaussian(float eqkLat, float eqkLon, float cellLat1, float cellLat2, float cellLon1, float cellLon2,
            float lengthScale) {
//        if (cellLat1 == 42.2f && cellLon1 == 13.8f){
//            System.out.println("here");
//        }
        float xLengthScaleInDegrees = GeoUtil.degreesFromKilometers(lengthScale, eqkLat);
        double xFactor = xLengthScaleInDegrees * Math.sqrt(2);
        float yLengthScaleInDegrees = GeoUtil.degreesFromKilometers(lengthScale);
        double yFactor = yLengthScaleInDegrees * Math.sqrt(2);

        double contribution = 0;
        contribution = 0.25 *
                (MathUtil.erf((eqkLat - cellLat2) / yFactor) -
                MathUtil.erf((eqkLat - cellLat1) / yFactor)) *
                (MathUtil.erf((eqkLon - cellLon2) / xFactor) -
                MathUtil.erf((eqkLon - cellLon1) / xFactor));
        return contribution;
    }

    /**
     * Determine which cells will obtain some contribution from an eqk at the specified lat/lon location, assuming that this epicenter is smoothed by the specified smoothing kernel with the
     * specified smoothing lengthscale and cells are distributed according to the specified gridding parameters
     *
     * @param eqkLat latitude of eqk epicenter
     * @param eqkLon longitude of eqk epicenter
     * @param smoothingKernel type of kernel used to smooth the specified epicenter
     * @param smoothingLengthscale smoothing lengthscale (in km) of smoothing kernel
     * @param latBinSize size of latitude bins for grid
     * @param lonBinSize size of longitude bins for grid
     * @param minLat minimum latitude of grid
     * @param maxLat maximum latitude of grid
     * @param minLon minimum longitude of grid
     * @param maxLon maximum longitude of grid
     * @return array of cell indices that will have some contribution from the
     * smoothed epicenter
     */
    private static int[] cellsObtainingContributionFromSmoothedEqk(float eqkLat, float eqkLon, float smoothingLengthscale, float latBinSize,
            float lonBinSize, float minLat, float maxLat, float minLon, float maxLon) {
        int numberOfLonBoxes = Math.round((maxLon - minLon) / lonBinSize);
        float maximumSmoothingDistance = smoothingLengthscale;

        // If the smoothing kernel is the bivariate Gaussian, the smoothing lengthscale specified is 1 sigma.  For practical purposes, the maximum smoothing distance in this case is
        // 3.86 * sqrt(2) * sigma
        float sigmaInDegrees = GeoUtil.degreesFromKilometers(smoothingLengthscale, eqkLat);

        float magicNumberInDegrees = 5.92158719165f * sigmaInDegrees * (float) Math.sqrt(2);
        maximumSmoothingDistance = GeoUtil.kilometersFromDegrees(magicNumberInDegrees, eqkLat);

        float[] minMaxLatsAffected = GeoUtil.latitudesAtDistanceFromPoint(eqkLat, maximumSmoothingDistance);
        float[] minMaxLonsAffected = GeoUtil.longitudesAtDistanceFromPoint(eqkLat, eqkLon, maximumSmoothingDistance);

        int nwCell = ArrayUtil.cellToWhichValueBelongs(minLon, maxLon, minLat, maxLat, lonBinSize, latBinSize, minMaxLonsAffected[0], minMaxLatsAffected[1]);
        int neCell = ArrayUtil.cellToWhichValueBelongs(minLon, maxLon, minLat, maxLat, lonBinSize, latBinSize, minMaxLonsAffected[1], minMaxLatsAffected[1]);
        int swCell = ArrayUtil.cellToWhichValueBelongs(minLon, maxLon, minLat, maxLat, lonBinSize, latBinSize, minMaxLonsAffected[0], minMaxLatsAffected[0]);
        int seCell = ArrayUtil.cellToWhichValueBelongs(minLon, maxLon, minLat, maxLat, lonBinSize, latBinSize, minMaxLonsAffected[1], minMaxLatsAffected[0]);
        int numberOfColumnsAffected = neCell - nwCell + 1;
        int numberOfRowsAffected = Math.round((nwCell - swCell) / numberOfLonBoxes) + 1;
        int[] cellsAffected = new int[numberOfColumnsAffected * numberOfRowsAffected];
        int affectedCellCounter = 0;

        for (int i = 0; i < numberOfRowsAffected; i++) {
            for (int j = 0; j < numberOfColumnsAffected; j++) {
                int cellAffected = swCell + i * numberOfLonBoxes + j;
                cellsAffected[affectedCellCounter] = cellAffected;
                affectedCellCounter++;
            }
        }
        return cellsAffected;
    }

    /**
     * Smooth the given catalog over the given grid using the specified space/time/magnitude smoothing.  The result is a grid with the smoothed contribution from all epicenters in
     * each grid box.
     *
     * @param catalogOfInterest eqk catalog
     * @param minLat minimum latitude of grid
     * @param maxLat maximum latitude of grid
     * @param minLon minimum longitude of grid
     * @param maxLon maximum longitude of grid
     * @param boxSize grid spacing
     * @param spatialSmoothingParameters array of spatial smoothing parameters (e.g., sigma for Gaussian, l for Epanechnikov, r_1, r_2 for powerlaw)
     * @return array representing the grid with each entry denoting the number of epicenters occuring in the grid box
     */
    public static float[] smoothedEventMapFromCatalog(Catalog catalogOfInterest, float minLat, float maxLat, float minLon, float maxLon, float boxSize,
            float[] spatialSmoothingParameters) {
        int numberOfLatBoxes = Math.round((maxLat - minLat) / boxSize);
        int numberOfLonBoxes = Math.round((maxLon - minLon) / boxSize);
        float[] eventMap = new float[numberOfLatBoxes * numberOfLonBoxes];

        int numberOfEqks = catalogOfInterest.numberOfEqks();
        float[] lats = catalogOfInterest.lats();
        float[] lons = catalogOfInterest.lons();

        for (int i = 0; i < numberOfEqks; i++) { // for each eqk

            float eqkLat = lats[i];
            float eqkLon = lons[i];

            // determine which cells are affected
            int[] cellsAffected = cellsObtainingContributionFromSmoothedEqk(eqkLat, eqkLon, spatialSmoothingParameters[0], boxSize, boxSize, minLat, maxLat,
                    minLon, maxLon);
            int numberOfCellsAffected = cellsAffected.length;

            for (int j = 0; j < numberOfCellsAffected; j++) { // for each affected cell

                int cell = cellsAffected[j];
                float[] latLon = Utility.latlonFromCellPosition(cell, minLat, minLon, maxLon, boxSize);
                float cellMinLat = latLon[0];
                float cellMaxLat = cellMinLat + boxSize;
                float cellMinLon = latLon[1];
                float cellMaxLon = cellMinLon + boxSize;

                double spatialContribution = 0;
                // compute contribution
                float sigma = spatialSmoothingParameters[0];
                spatialContribution = contributionFromSmoothedEqk_Gaussian(eqkLat, eqkLon, cellMinLat, cellMaxLat, cellMinLon, cellMaxLon, sigma);

                double contribution = spatialContribution;
                eventMap[cell] += contribution;
            }
        }
        return eventMap;
    }
}
import java.io.OutputStreamWriter;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedWriter;
import java.util.Arrays;

public class MolchanTools {

    public MolchanTools() {
    }

    /**
     * Given the specified error trajectory (a 2D array of (nu,tau) points sorted in ascending nu, return the sorted list of tau values at which there is a jump.  We record a 
     * jump whenever contiguous values of nu are unequal; this gives us the explicit jumps; the implicit jumps (that is, where we might have more than one event captured 
     * in a single tau increment) can be inferred by the missing jumps (those jump values that haven't been set by the above procedure).  For clarity, a jump occurs 
     * whenever the trajectory drops in nu.  In other words, whenever more target events are captured by an increase in tau.  The jump point is the tau value at which this 
     * drop occurs.
     *
     * @param trajectory complete Molchan trajectory containing all (tau, nu) points, sorted in nu in the form [0] = nu, [1] = tau
     * @param numberOfTargetEqks number of target eqks occurring during this experiment.
     * @return array of sorted tau values representing the jumps of the given trajectory (jumps[1] is the minimum value of tau for which there are 1 hits)
     */
    public static float[] sortedJumps(float[][] trajectory, int numberOfTargetEqks) {
        int numberOfTrajectoryPoints = trajectory.length;

        // we will return N+1 jumps (jumping to 0 hits at tau = 0 all the way to jumping from N-1 hits to N hits)
        float[] jumps = new float[numberOfTargetEqks + 1];

        // To be safe, make all jumps occur @ tau = 1.0 by default (otherwise, they're all @ tau=0.0 by default
        Arrays.fill(jumps, 1.0f);


        // Now we have a 2D array of (tau, nu) couples sorted by nu.  The jumps occur when two consecutive values of nu are unequal
        for (int i = 0; i < numberOfTrajectoryPoints - 1; i++) {
            float currentNu = trajectory[i][0];
            float currentTau = trajectory[i][1];
            float nextNu = trajectory[i + 1][0];
            if (currentNu < nextNu) {
                // This means that at currentTau, there was a jump to having nextNu miss rate; we can also think of this as the point at which we got 1 - nextNu hit rate; 
                //                we translate this to the number of hits
                int numberOfHits = numberOfTargetEqks - (int) (currentNu * numberOfTargetEqks);
                jumps[numberOfHits] = currentTau;
            }
        }

        // The 0th jump, which is not really a jump but merely used to keep notation clear, must be set to 0
        jumps[0] = 0.0f;

        // We now have all the explicit jumps; in order to infer the other jumps, we check to see where there is no jump value (that is, the value is currently set to 1), then set 
        //        the empty values equal to the proceeding jump value
        for (int j = jumps.length - 2; j >= 0; j--) {
            if (jumps[j] == 1.0f) {
                jumps[j] = jumps[j + 1];
            }
        }

        System.out.println("jumps = " + Arrays.toString(jumps));
        return jumps;
    }

    /**
     * Given the specified trajectory, determine on which leg of the trajectory tau falls.  In particular, this answers the question: How many jumps have occurred up to tau?
     * 
     * @param trajectory sorted list of tau values at which there's been a jump
     * @param tau value up to which we're evaluating
     * @return the leg of the specified trajectory at the specified tau
     */
    public static int whichLegAreWeOn(float[] trajectory, float tau) {
        // By default, we'll start on the 0th leg
        int n = 0;

        // If tau > jump_i, then we've obtained at least i hits, keep looking
        for (int i = 1; i < trajectory.length; i++) {
            if (trajectory[i] < tau) {
                n = i;
            } else {
                return i - 1;
            }
        }
        return n;
    }

    /**
     * Calculate nu, the miss rate, from the given alarm map and the target eqk map.  To do this, we pick the non-alarm boxes and sum the corresponding number of target 
     * eqks in these boxes.  This gives us the total number of misses.  Then nu is just the number of misses divided by the number of target eqks.
     * 
     * @param alarmMap 3-D representation of alarms in the study region
     * @param targetEqks 3-D map representation of spatial eqk distribution, each cell contains the number of target epicenters contained w/i this spatial cell
     * @param numberOfTargetEqks total number of target eqks in the experiment 
     * @return miss rate (nu) for the given target eqks and alarms
     */
    private static float nu(float[] predictor, short[] targetEqks, int numberOfTargetEqks, float threshold) {
        int numberOfBins = predictor.length;
        int numberOfMisses = 0;

        for (int i = 0; i < numberOfBins; i++) {
            int eqksInThisBox = targetEqks[i];
            if ((eqksInThisBox > 0) && (predictor[i] < threshold)) {
                numberOfMisses += eqksInThisBox;
            }
        }

        float nu = (float) numberOfMisses / (float) numberOfTargetEqks;
        return nu;
    }

    /**
     * Calculate tau, the fraction of space covered by alarm, from the given predictor map.  Each box's weight is specified in the cost map.  We have normalized the cost map so 
     * the computation of tau is simple; it's simply the sum of costs for boxes with alarm function value greater than the threshold
     *
     * @param predictor alarm function values
     * @param costMap representation of the cost of occupying a given bin w/ an alarm, normalized
     * @return the fraction of space covered by the given alarm map, using cost values specified in the costMap to measure space
     */
    private static float tau(float[] predictor, float[] costMap, float threshold) {
        int bins = predictor.length;

        // Tau is sum of the cost of alarm boxes.
        float tau = 0.0f;

        for (int i = 0; i < bins; i++) {
            if (predictor[i] >= threshold) {
                tau += costMap[i];
            }
        }

        return tau;
    }

    /**
     * Calculate the error trajecory for the given target eqk catalog and using the specified alarm function as the predictor; each of these is provided as array that represents 
     * a lat-lon-mag gridding of the study region.  predictor contains the alarm function value in each bin, and targetEqks contains the number of target eqks in each bin.  
     * The idea here is to take the alarm function and choose a threshold; any bin in predictor with a value higher than the threshold is considered to be an alarm; any box 
     * below the threshold is not an alarm.  We then compute the miss rate and fraction of alarm-space time covered by alarms.  This gives us one (tau,nu) point.  We repeat 
     * this process for all threshold values.  We determine the appropriate threshold values from the predictor alarm function values.  In particular, we pick out all the unique 
     * alarm function values and use each one of them as a threshold.  Note that the resultant error trajectory will be sorted in ascending nu (b/c the thresolds are sorted
     * in ascending order).
     *
     * @param predictor alarm function values
     * @param costMap representation of the "cost" for occupying each box with an alarm.
     * @param targetEqks representation of target eqk distribution, each entry contains the number of target epicenters contained w/i this bin
     * @param enforceMinWeightRequirement Should we require that every box cost some minimum, nonzero amount?
     * @param numberOfTargetEqks total number of target eqks in the target catalog
     * @return error trajectory sorted in nu containing, for each unique alarm function value, 2 values: 
     *      [0] = nu, 
     *      [1] = tau determined by using the cost map
     */
    public static float[][] molchanTrajectory(float[] predictor, float[] costMap, short[] targetEqks, boolean enforceMinWeightRequirement, int numberOfTargetEqks) {
        // this will hold the error trajectory (nu, weightedTau) points
        float[][] trajectory;

        // Determine each of the unique values in the predictor AlarmFunction, sorted in ascending order; we will use each value as a threshold in order to obtain a complete 
        //        error trajectory.  This yields the minimum number of computations required to ensure that all possible error trajectory points have been computed.  Because 
        //        these thresholds are sorted in ascending order, we are assured that the resultant error trajectory is sorted as well.
//        System.out.println("getting unique thresholds");
        float[] thresholds = ArrayUtil.uniqueValues(predictor);
//        System.out.println("done (there are " + thresholds.length + ")");

        // Each threshold will yield a single error trajectory point, and we will have an additional point at (nu, tau) = (1, 0)
        int numberOfMolchanCouples = thresholds.length + 1;

        // Allocate room to store (nu, weighted_tau) values at each threshold step
        trajectory = new float[numberOfMolchanCouples][2];

        if (enforceMinWeightRequirement) {
//            System.out.println("enforcing min weight");
            costMap = ArrayUtil.arrayWithNoEntriesLessThanOrEqualToZero(costMap);
//            System.out.println("done");
        }

        // We normalize the cost map (its sum is unity) in order to simplify the tau computation
//        System.out.println("normalizing cost map");
        float[] normalizedCostMap = ArrayUtil.normalize(costMap);
//        System.out.println("done");

//        System.out.println("predictor = " + Arrays.toString(predictor));
//        System.out.println("costMap = " + Arrays.toString(costMap));
//        System.out.println("targetEqks = " + Arrays.toString(targetEqks));

//        System.out.println("\n\n=====================================");
//        System.out.println("bin\tpredictor\teqks\tcost\ttau\tnu");

        // step through the threshold values and compute the corresponding (nu, weighted_tau) point
//        System.out.println("computing trajectory points");
        for (int i = 0; i < thresholds.length; i++) {
            float threshold = thresholds[i];

            // compute the miss rate for this threshold
            trajectory[i][0] = MolchanTools.nu(predictor, targetEqks, numberOfTargetEqks, threshold);

            // given the alarm bitmap and the costmap, compute weighted tau
            trajectory[i][1] = MolchanTools.tau(predictor, normalizedCostMap, threshold);

//            if ((i % 99) == 0)
//                System.out.println("done with point #" + (i +1));
//            System.out.println(i + "\t" + predictor[i] + "\t" + targetEqks[i]
//                    + "\t" + costMap[i] + "\t" + trajectory[i][1] + "\t"
//                    + trajectory[i][0]);
        }
//        System.out.println("done");

        // Set the final trajectory point to (nu, weighted_tau)=(1,0)
        trajectory[numberOfMolchanCouples - 1][0] = 1.0f;
        trajectory[numberOfMolchanCouples - 1][1] = 0.0f;

        return trajectory;
    }

    /**
     * Generate a matlab data file that will be used to plot a Molchan trajectory for the specified AlarmFunction file and specified target eqk catalog file.  To do this, we just need to compute the trajectory and then
     * write out the trajectory in Matlab format.
     *
     * Use the predictor AlarmFunction to predict the target catalog
     * Generate a Molchan trajectory for this AlarmFunction strategy, using the weighted tau specified by the cost map, and save the trajectory in Matlab format
     *
     * @param predictorAFVs array of predictor AlarmFunction values
     * @param targetEqks array of number of target eqks per cell
     * @param matlabFile path to file to which we'll be writing the matlab data
     * @param costMapFile array of weight alarm function values
     * @param useMarginOfError Should a target event within one box of an alarm region be counted as a hit?
     * @param enforceMinWeightRequirement Should we require that every box cost some minimum, nonzero amount?
     */
    public static void generateMatlabForTrajectoryPlot(float[] predictorAFVs, short[] targetEqks, String matlabFile, float[] costMap, boolean useMarginOfError,
            boolean enforceMinWeightRequirement) {
        int N = ArrayUtil.sum(targetEqks);
        // Given the predictor values, the cost map values, and the target eqk map, compute the complete error trajectory
        float[][] trajectory = MolchanTools.molchanTrajectory(predictorAFVs, costMap, targetEqks, enforceMinWeightRequirement, N);
        float[] jumps = MolchanTools.sortedJumps(trajectory, N);

        // Write out the trajectory information to the Matlab script file
        try {
            FileOutputStream oOutFIS = new FileOutputStream(matlabFile);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            oWriter.write("nu = [1:-1/" + N + ":0];\n");

            oWriter.write("tau = [");
            for (int i = 0; i < jumps.length; i++) {
                oWriter.write(jumps[i] + ";");
            }
            oWriter.write("];\n");

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();
        } catch (Exception ex) {
            System.out.println("Error in MolchanTools.generateMatlabFileForTrajectoryPlot()");
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}

import java.io.File;

public class Utility {
    public static String SEP = File.separator; // the platform-dependent directory name separator

    /**
     * Bin the given catalog into the given grid.  The result is a grid with the number of epicenters occurring in each grid box. USE THIS IMPLEMENTATION ONLY IF THE 
     * CATALOG IS NOT ALREADY IN MEMORY!
     *
     * @param catalogFile path to eqk catalog (must be JEREMY_GENERATED)
     * @param minLat minimum latitude of grid
     * @param maxLat maximum latitude of grid
     * @param minLon minimum longitude of grid
     * @param maxLon maximum longitude of grid
     * @param boxSize grid spacing
     * @return array representing the grid with each entry denoting the number of epicenters occuring in the grid box
     */
    public static short[] eventMapFromCatalog(String catalogFile, float minLat, float maxLat, float minLon, float maxLon, float boxSize) {
        int numberOfLatBoxes = Math.round((maxLat - minLat) / boxSize);
        int numberOfLonBoxes = Math.round((maxLon - minLon) / boxSize);
        short[] eventMap = new short[numberOfLatBoxes * numberOfLonBoxes];
        
        Catalog catalogOfInterest = new Catalog(catalogFile, Catalog.JEREMY_GENERATED);
        int numberOfEqks = catalogOfInterest.numberOfEqks();
        float[] lats = catalogOfInterest.lats();
        float[] lons = catalogOfInterest.lons();
        
        for (int i = 0;i < numberOfEqks;i++){
            int latPosition = ArrayUtil.binToWhichValueBelongs(minLat, maxLat, boxSize, lats[i], false);
            int lonPosition = ArrayUtil.binToWhichValueBelongs(minLon, maxLon, boxSize, lons[i], false);
            
            // check to make sure this event is in the study region
            if(latPosition > -1 && lonPosition > -1){
                int cellPosition = latPosition * numberOfLonBoxes + lonPosition;
                // add this event in the appropriate box
                eventMap[cellPosition]++;
            }
        }
        return eventMap;
    }
    
    /**
     * Given the specified grid parameters, determine the latitude/longitude at the specified grid position.
     *
     * @param cellPosition integer position w/i the grid
     * @param minLat minimum latitude of the grid
     * @param minLon minimum longitude of the grid
     * @param maxLon maximum longitude of the grid
     * @param boxSize grid spacing
     * @return 2 element array containing
     * [0] = latitude at grid position
     * [1] = longitude at grid position
     */
    public static float[] latlonFromCellPosition(int cellPosition, float minLat, float minLon, float maxLon, float boxSize){
        int numberOfLonBoxes = Math.round((maxLon - minLon) / boxSize);
        int latPosition = cellPosition / numberOfLonBoxes;
        int lonPosition = cellPosition % numberOfLonBoxes;
        
        float lat = minLat + latPosition * boxSize;
        float lon = minLon + lonPosition * boxSize;
        float[] latLon = {lat, lon};
        return latLon;
    }
    
    /**
     * Given the specified grid parameters, determine the latitude/longitude/magnitude at the specified grid position.
     *
     * @param cellPosition integer position w/i the grid
     * @param minLat minimum latitude of the grid
     * @param maxLat maximum latitude of the grid
     * @param minLon minimum longitude of the grid
     * @param maxLon maximum longitude of the grid
     * @param minMag minimum magnitude of the grid
     * @param spatialCellSize spatial grid spacing
     * @param magCellSize magnitude grid spacing
     * @return 3 element array containing
     * [0] = latitude at grid position
     * [1] = longitude at grid position
     * [2] = magnitude at grid position
     */
    public static float[] latlonMagFromCellPosition(int cellPosition, float minLat, float maxLat, float minLon, float maxLon, float minMag, float spatialCellSize, 
            float magCellSize){
        int numberOfLatBoxes = Math.round((maxLat - minLat) / spatialCellSize);
        int numberOfLonBoxes = Math.round((maxLon - minLon) / spatialCellSize);
        
        int magPosition = cellPosition / (numberOfLonBoxes * numberOfLatBoxes);
        int latPosition = (cellPosition - (magPosition * numberOfLatBoxes * numberOfLonBoxes)) / numberOfLonBoxes;
        int lonPosition = cellPosition % numberOfLonBoxes;
        
        float lat = minLat + latPosition * spatialCellSize;
        float lon = minLon + lonPosition * spatialCellSize;
        float mag = minMag + magPosition * magCellSize;
        float[] latLonMag = {lat, lon, mag};
        return latLonMag;
    }
}
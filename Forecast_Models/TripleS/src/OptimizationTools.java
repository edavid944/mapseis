
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

public class OptimizationTools {

    public OptimizationTools() {
    }

    /**
     * Execute processing steps required to generate a forecast for the specified testing regions.  Namely, for each region, we:
     * 
     * 1. Generate the catalogs used to construct and test retrospective forecasts; also generate the catalog used to construct the prospective forecast.
     * 2. Construct retrospective forecasts using different parameter values.
     * 3. Evaluate the retrospective forecasts and determine the optimal parameter values.
     * 4. Construct a prospective forecast using the optimal parameter value determined in Step 3 and the catalog generated in Step 1.
     */
    public static void main(String[] args) {
        String parameterFilePath = args[0];
        Properties Props = new Properties();
        String inputCatalogPath = "";
        String rateForecastMLPath = "";
        String collectionRegionCatalogPath = "";
        String retrospectiveSmoothingCatalogPath = "";
        String retrospectiveTargetCatalogPath = "";
        String prospectiveSmoothingCatalogPath = "";
        String afvPath = "";
        String prospectiveForecastStartDate = "";
        String prospectiveForecastEndDate = "";
        String prospectiveSmoothingPeriodStart = "";
        String prospectiveSmoothingPeriodEnd = "";
        String retrospectiveSmoothingPeriodStart = "";
        String retrospectiveSmoothingPeriodEnd = "";
        String retrospectiveTargetPeriodStart = "";
        String retrospectiveTargetPeriodEnd = "";
        String forecastNodesPath = "";
        float minLat = 0f;
        float maxLat = 0f;
        float minLon = 0f;
        float maxLon = 0f;
        float boxSize = 0f;
        float minTargetMagnitude = 0f;
        float maxTargetMagnitude = 0f;
        float minDepth = 0f;
        float maxDepth = 0f;
        float magnitudeBinSize = 0f;
        try {
            FileInputStream in = new FileInputStream(parameterFilePath);
            Props.load(in);
            in.close();
            in = null;

            inputCatalogPath = Props.getProperty("inputCatalogPath");
            // If the path to the input catalog contains a comma, this indicates that we'll be merging catalogs to create the input catalog
            if (inputCatalogPath.contains(",")) {
                String[] inputCatalogPaths = inputCatalogPath.split(",");
                String[] inputCatalogStartDates = Props.getProperty("inputCatalogStartDates").split(",");
                String[] inputCatalogEndDates = Props.getProperty("inputCatalogEndDates").split(",");
                int numberOfCatalogs = inputCatalogPaths.length;
                Catalog catalogA = new Catalog(inputCatalogPaths[0], Catalog.ZMAP);
                catalogA = catalogA.subcatalogByTime(inputCatalogStartDates[0], inputCatalogEndDates[0]);
                // I assume the first catalog is the CPTI catalog and we want to update the magnitude to Ml from Mw
                catalogA.convertCPTIMagnitudes();

                // Now append all other catalogs
                for (int i = 1; i < numberOfCatalogs; i++) {
                    Catalog catalogB = new Catalog(inputCatalogPaths[i], Catalog.ZMAP);
                    catalogB = catalogB.subcatalogByTime(inputCatalogStartDates[i], inputCatalogEndDates[i]);
                    catalogA.appendCatalog(catalogB);
                }
                String inputCatalogPathToWrite = Props.getProperty("inputCatalogPathToWrite");
                catalogA.saveAsZMAP(inputCatalogPathToWrite);
                inputCatalogPath = inputCatalogPathToWrite;
            }

            rateForecastMLPath = Props.getProperty("rateForecastMLPath");
            collectionRegionCatalogPath = Props.getProperty("collectionRegionCatalogPath");
            retrospectiveSmoothingCatalogPath = Props.getProperty("retrospectiveSmoothingCatalogPath");
            retrospectiveTargetCatalogPath = Props.getProperty("retrospectiveTargetCatalogPath");
            prospectiveSmoothingCatalogPath = Props.getProperty("prospectiveSmoothingCatalogPath");
            afvPath = Props.getProperty("afvPath");
            prospectiveForecastStartDate = Props.getProperty("forecastStartDate");
            prospectiveForecastEndDate = Props.getProperty("forecastEndDate");
            prospectiveSmoothingPeriodStart = Props.getProperty("prospectiveSmoothingPeriodStart");
            prospectiveSmoothingPeriodEnd = Props.getProperty("prospectiveSmoothingPeriodEnd");
            retrospectiveSmoothingPeriodStart = Props.getProperty("retrospectiveSmoothingPeriodStart");
            retrospectiveSmoothingPeriodEnd = Props.getProperty("retrospectiveSmoothingPeriodEnd");
            retrospectiveTargetPeriodStart = Props.getProperty("retrospectiveTargetPeriodStart");
            retrospectiveTargetPeriodEnd = Props.getProperty("retrospectiveTargetPeriodEnd");
            forecastNodesPath = Props.getProperty("forecastNodesPath");

            minLat = Float.parseFloat(Props.getProperty("minLat"));
            maxLat = Float.parseFloat(Props.getProperty("maxLat"));
            minLon = Float.parseFloat(Props.getProperty("minLon"));
            maxLon = Float.parseFloat(Props.getProperty("maxLon"));

            boxSize = Float.parseFloat(Props.getProperty("boxSize"));
            minTargetMagnitude = Float.parseFloat(Props.getProperty("minTargetMagnitude"));
            maxTargetMagnitude = Float.parseFloat(Props.getProperty("maxTargetMagnitude"));
            magnitudeBinSize = Float.parseFloat(Props.getProperty("magnitudeBinSize"));
            minDepth = Float.parseFloat(Props.getProperty("minDepth"));
            maxDepth = Float.parseFloat(Props.getProperty("maxDepth"));
        } catch (Exception ex) {
            System.out.println("Error in OptimizationTools.main(" + parameterFilePath + ")");
            ex.printStackTrace();
            System.exit(-1);
        }

        System.out.println("Generating catalogs...");
        catalogs(inputCatalogPath, collectionRegionCatalogPath, retrospectiveSmoothingCatalogPath, retrospectiveTargetCatalogPath, prospectiveSmoothingCatalogPath,
                prospectiveSmoothingPeriodStart, prospectiveSmoothingPeriodEnd, retrospectiveSmoothingPeriodStart, retrospectiveSmoothingPeriodEnd,
                retrospectiveTargetPeriodStart, retrospectiveTargetPeriodEnd, minTargetMagnitude, maxTargetMagnitude, minLat, maxLat, minLon, maxLon, minDepth, maxDepth);

        System.out.println("Constructing retrospective forecasts...");
        retrospectiveForecasts(retrospectiveSmoothingCatalogPath, afvPath, minLat, maxLat, minLon, maxLon, boxSize);

        //CHANGE: Also return Index of optimalSigma
        //System.out.println("Evaluating retrospective forecasts and determining optimal parameter values...");
        //float[] optimalSigmaWithIndex = retrospectiveEvaluation(retrospectiveTargetCatalogPath, afvPath, minLat, maxLat, minLon, maxLon, boxSize);
        //float optimalSigma = optimalSigmaWithIndex[0];
        //int optimalSigmaIndex = (int)(optimalSigmaWithIndex[1] + 0.5f);

        //NEW: using joint-spatial-log-Likelihood for evaluation
        System.out.println("Evaluating retrospective forecasts and determining optimal parameter values...");
        float[] optimalSigmaWithIndex = retrospectiveSJLLEvaluation(retrospectiveTargetCatalogPath, afvPath, minLat, maxLat, minLon, maxLon, boxSize);
        float optimalSigma = optimalSigmaWithIndex[0];
        int optimalSigmaIndex = (int)(optimalSigmaWithIndex[1] + 0.5f);


        //NEW: sigmas values given outside the functions "retrospectiveFineForecasts" and "retrospectiveFineEvaluation"
        //System.out.println("Refining the evaluated optimal sigma value...");
        //float[] sigmasOrig = {5f, 10f, 20f, 25f, 30f, 50f, 75f, 100f, 200f};
        //float[] sigmasFine = refineSigma(sigmasOrig,optimalSigmaIndex);
        //NEW!!!
        //System.out.println("Constructing refined retrospective forecasts...");
        //retrospectiveFineForecasts(retrospectiveSmoothingCatalogPath, afvPath, minLat, maxLat, minLon, maxLon, boxSize, sigmasFine);
        //System.out.println("Evaluating retrospective forecasts and refining optimal parameter values...");
        //float optimalSigmaFine = retrospectiveFineEvaluation(retrospectiveTargetCatalogPath, afvPath, minLat, maxLat, minLon, maxLon, boxSize, sigmasFine);


                optimalSigma = 20;
                System.out.println("Constructing prospective forecast with sigma = " + optimalSigma + " km...");
                prospectiveForecasts(prospectiveSmoothingCatalogPath, afvPath, optimalSigma, rateForecastMLPath, prospectiveForecastStartDate, prospectiveForecastEndDate,
                        minTargetMagnitude, maxTargetMagnitude, minLat, maxLat, minLon, maxLon, boxSize, forecastNodesPath, minDepth, maxDepth, magnitudeBinSize);
                System.out.println("z\terf(z)");
                for (int i = 0;i < 100; i++){
                    double z = 5.9215871915900005 + i * 0.00000000001;
      
                    System.out.println(z + "\t" + MathUtil.erf(z));
                }

    }



    //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    private static float[] refineSigma(float[] sigmasOrig, int optimalSigmaIndex) {

        float stepDiffDown = (sigmasOrig[optimalSigmaIndex]-sigmasOrig[optimalSigmaIndex-1])/3f ;
        float stepDiffUp = (sigmasOrig[optimalSigmaIndex+1]-sigmasOrig[optimalSigmaIndex])/3f ;

        float[] sigmasFine = new float[7];
        sigmasFine[0] = sigmasOrig[optimalSigmaIndex-1];
        sigmasFine[3] = sigmasOrig[optimalSigmaIndex];
        sigmasFine[6] = sigmasOrig[optimalSigmaIndex+1];

        sigmasFine[1] = sigmasOrig[optimalSigmaIndex] - Math.round(2f * stepDiffDown);
        sigmasFine[2] = sigmasOrig[optimalSigmaIndex] - Math.round(1f * stepDiffDown);
        sigmasFine[4] = sigmasOrig[optimalSigmaIndex] + Math.round(1f * stepDiffUp);
        sigmasFine[5] = sigmasOrig[optimalSigmaIndex] + Math.round(2f * stepDiffUp);

        return sigmasFine;
    }





    /**
     * Evaluate the retrospective forecasts for the region of interest and determine which one yields the minimum "misfit."    To do this, we fix one of the forecasts as the 
     * reference and compare all others to it.  We repeat this procedure, allowing each forecast to serve as the reference forecast.  If we have a very good reference forecast, 
     * all other forecasts will obtain an area skill score near 1/2.  Therefore, we define misfit to be the average distance of all forecasts from 1/2.  
     * 
     * @param targetCatalogFile path to the retrospective target catalog
     * @param afvPath path where the alarm function value files are stored
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     * @return lengthscale that minimizes our misfit statistic
     */
    private static float[] retrospectiveEvaluation(String retrospectiveTargetCatalogPath, String afvPath, float minLat, float maxLat, float minLon, float maxLon,
            float boxSize) {
//            Load the target catalog.
        short[] targetEqkDistribution = Utility.eventMapFromCatalog(retrospectiveTargetCatalogPath, minLat, maxLat, minLon, maxLon, boxSize);
        short numberOfTargetEqks = ArrayUtil.sum(targetEqkDistribution);

        float[] sigmas = {5f, 10f, 20f, 25f, 30f, 50f, 75f, 100f, 200f};
        //TEST:: float[] sigmas = {25f, 30f, 50f, 75f, 100f, 200f};

        float[] chis = new float[sigmas.length];
//        double[] lls = new double[sigmas.length];

        System.out.println("sigma\tchi");
//        System.out.println("sigma\tspatial joint LL");

        // For each lengthscale, compute the Molchan trajectory and ASS trajectory with each lengthscale playing the part of reference model
        // For each lengthscale, compute the spatial joint log likelihood
        for (int i = 0; i < sigmas.length; i++) {
            float costMapSigma = sigmas[i];
//            float sigma = sigmas[i];
            String costMapFile = afvPath + "retro" + costMapSigma + ".afv";
            AlarmFunction costMap = new AlarmFunction(costMapFile);
            float[] costMapValues = costMap.values();


            for (int j = 0; j < sigmas.length; j++) {
                float afSigma = sigmas[j];
                String afvFile = afvPath + "retro" + afSigma + ".afv";
//            String afvFile = afvPath + "retro" + sigma + ".afv";
            AlarmFunction af = new AlarmFunction(afvFile);
            float[] afValues = af.values();
                chis[i] += Math.abs(0.5f - ASSTools.areaSkillScore(afValues, targetEqkDistribution, costMapValues, false)) / (float) sigmas.length;
//            lls[i] = LikelihoodTools.spatialJointLL(afValues, targetEqkDistribution, numberOfTargetEqks);
//            System.out.println(sigma + "\t" + lls[i]);
            
//            String integerSigma = Integer.toString((int)sigma);
//            String gmtEqkFile = "eqks.cat";
//            String gmtAFVFile = "forecast" + integerSigma + ".afv";
//            String gmtScriptFile = "makeMap" + integerSigma + ".gmt";
//            String experimentMapFile = "map" + integerSigma + ".ps";
//            OptimizationTools.generateExperimentMapGMTScript(afvFile, retrospectiveTargetCatalogPath, gmtEqkFile, gmtAFVFile, gmtScriptFile, experimentMapFile);
            }
            System.out.println(costMapSigma + "\t" + chis[i]);


        }

        // Determine the optimal lengthscale (the one that minimizes chi)
        float minChi = Float.MAX_VALUE;
//        double maxLL = Double.MIN_VALUE;
        float optimalSigma = 0f;
        float[] optimalSigmaWithIndex = new float[2];

        for (int i = 0; i < chis.length; i++) {
            float chi = chis[i];
            if (chi < minChi) {
                minChi = chi;
//        for (int i = 0; i < lls.length; i++) {
//            double ll = lls[i];
//            if (ll > maxLL) {
//                maxLL = ll;
                optimalSigma = sigmas[i];
                optimalSigmaWithIndex[0] = sigmas[i];
                optimalSigmaWithIndex[1] = i;
            }
        }
        System.out.println("minChi = " + minChi);
        System.out.println("optimal Sigma = " + optimalSigma);
        return optimalSigmaWithIndex;
    }





      //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        private static float[] retrospectiveSJLLEvaluation(String retrospectiveTargetCatalogPath, String afvPath, float minLat, float maxLat, float minLon, float maxLon,
            float boxSize) {
//            Load the target catalog.
        short[] targetEqkDistribution = Utility.eventMapFromCatalog(retrospectiveTargetCatalogPath, minLat, maxLat, minLon, maxLon, boxSize);
        short numberOfTargetEqks = ArrayUtil.sum(targetEqkDistribution);

        float[] sigmas = {5f, 10f, 20f, 25f, 30f, 50f, 75f, 100f, 200f};
        double[] lls = new double[sigmas.length];
        System.out.println("sigma\tspatial joint LL");

        // For each lengthscale, compute the spatial joint log likelihood
        for (int i = 0; i < sigmas.length; i++) {
            float sigma = sigmas[i];
            String afvFile = afvPath + "retro" + sigma + ".afv";
            AlarmFunction af = new AlarmFunction(afvFile);
            float[] afValues = af.values();
            lls[i] = LikelihoodTools.spatialJointLL(afValues, targetEqkDistribution, numberOfTargetEqks);
            System.out.println(sigma + "\t" + lls[i]);
        }

        // Determine the optimal lengthscale
        //double maxLL = Double.MIN_VALUE;
        double maxLL = -1.0*Double.MAX_VALUE;
        float optimalSigma = 0f;
        float[] optimalSigmaWithIndex = new float[2];


        for (int i = 0; i < lls.length; i++) {
            double ll = lls[i];
            if (ll > maxLL) {
                maxLL = ll;
                optimalSigma = sigmas[i];
                optimalSigmaWithIndex[0] = sigmas[i];
                optimalSigmaWithIndex[1] = i;
            }
        }
        System.out.println("maxLL = " + maxLL);
        System.out.println("optimal Sigma = " + optimalSigma);
        return optimalSigmaWithIndex;
    }





    //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    private static float retrospectiveFineEvaluation(String retrospectiveTargetCatalogPath, String afvPath, float minLat, float maxLat, float minLon, float maxLon,
            float boxSize, float[] sigmasFine) {
//            Load the target catalog.
        short[] targetEqkDistribution = Utility.eventMapFromCatalog(retrospectiveTargetCatalogPath, minLat, maxLat, minLon, maxLon, boxSize);
        short numberOfTargetEqks = ArrayUtil.sum(targetEqkDistribution);


        float[] chis = new float[sigmasFine.length];
        double[] lls = new double[sigmasFine.length];
        System.out.println("sigmasfine\tspatial joint LL");


        // For each lengthscale, compute the spatial joint log likelihood
        for (int i = 0; i < sigmasFine.length; i++) {
            float sigma = sigmasFine[i];
            String afvFile = afvPath + "retro" + sigma + ".afv";
            AlarmFunction af = new AlarmFunction(afvFile);
            float[] afValues = af.values();
            lls[i] = LikelihoodTools.spatialJointLL(afValues, targetEqkDistribution, numberOfTargetEqks);
            System.out.println(sigma + "\t" + lls[i]);
        }

        // Determine the optimal lengthscale
        //double maxLL = Double.MIN_VALUE;
        double maxLL = -1.0*Double.MAX_VALUE;
        float optimalSigmaFine = 0f;
        float[] optimalSigmaWithIndex = new float[2];


        for (int i = 0; i < lls.length; i++) {
            double ll = lls[i];
            if (ll > maxLL) {
                maxLL = ll;
                optimalSigmaFine = sigmasFine[i];
                optimalSigmaWithIndex[0] = sigmasFine[i];
                optimalSigmaWithIndex[1] = i;
            }
        }
        System.out.println("maxLL = " + maxLL);
        System.out.println("optimal Sigma Fine = " + optimalSigmaFine);
        return optimalSigmaFine;
    }





    /**
     * Construct several retrospective forecasts for the specified testing region, using a Gaussian smoothing kernel and varying the smoothing lengthscale.
     * 
     * @param smoothingCatalogPath path to the retrospective smoothing catalog
     * @param afvPath path where the retrospective forecasts are stored
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     */
    private static void retrospectiveForecasts(String smoothingCatalogPath, String afvPath, float minLat, float maxLat, float minLon, float maxLon, float boxSize) {
        float[] sigmas = {5f, 10f, 20f, 25f, 30f, 50f, 75f, 100f, 200f};
        //float[] sigmas = {25f, 30f, 50f, 75f, 100f, 200f};

        float[] gridParameters = new float[5];
        gridParameters[0] = minLat;
        gridParameters[1] = maxLat;
        gridParameters[2] = minLon;
        gridParameters[3] = maxLon;
        gridParameters[4] = boxSize;

        Catalog smoothingCatalog = new Catalog(smoothingCatalogPath, Catalog.JEREMY_GENERATED);
        
        // Only keep the first event in the catalog
//        String[] times = {smoothingCatalog.times[0]};
//        float[] lats = {smoothingCatalog.lats[0]};
//        float[] lons = {smoothingCatalog.lons[0]};
//        float[] depths = {0f};
//        float[] mags = {0f};
//        smoothingCatalog = new Catalog(times, lats, lons, depths, mags);

//        Generate Gaussian smoothed alarm functions with sigma=10, 50-100, 150, 200, 500km
        for (int i = 0; i < sigmas.length; i++) {
            float sigma = sigmas[i];
            System.out.println("Constructing prospective forecast with sigma = " + sigma + " km...");
            float[] spatialSmoothingParameters = {sigma, 0f};
            String afvFilePath = afvPath + "retro" + sigma + ".afv";
            float[] afv = SmoothingTools.smoothedEventMapFromCatalog(smoothingCatalog, minLat, maxLat, minLon, maxLon, boxSize, spatialSmoothingParameters);

            // From the smoothed seismicity map, generate and save the corresponding alarm function value file
            AlarmFunction af = new AlarmFunction(afv, gridParameters);
            af.save(afvFilePath);
        }

    }



        //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //NEW!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        private static void retrospectiveFineForecasts(String smoothingCatalogPath, String afvPath, float minLat, float maxLat, float minLon, float maxLon, float boxSize, float[] sigmasFine) {

        float[] gridParameters = new float[5];
        gridParameters[0] = minLat;
        gridParameters[1] = maxLat;
        gridParameters[2] = minLon;
        gridParameters[3] = maxLon;
        gridParameters[4] = boxSize;

        Catalog smoothingCatalog = new Catalog(smoothingCatalogPath, Catalog.JEREMY_GENERATED);

//        Generate Gaussian smoothed alarm functions with sigma=10, 50-100, 150, 200, 500km
        for (int i = 0; i < sigmasFine.length; i++) {
            float sigma = sigmasFine[i];
            System.out.println("Constructing prospective forecast with sigma = " + sigma + " km...");
            float[] spatialSmoothingParameters = {sigma, 0f};
            String afvFilePath = afvPath + "retro" + sigma + ".afv";
            float[] afv = SmoothingTools.smoothedEventMapFromCatalog(smoothingCatalog, minLat, maxLat, minLon, maxLon, boxSize, spatialSmoothingParameters);

            // From the smoothed seismicity map, generate and save the corresponding alarm function value file
            AlarmFunction af = new AlarmFunction(afv, gridParameters);
            af.save(afvFilePath);
        }

    }




    /**
     * Construct prospective forecasts for the specified testing region, using a Gaussian smoothing kernel and the specified smoothing lengthscale.  One forecast is saved in
     * scaled format, such that the total number of earthquakes forecast is equal to the average number of target earthquakes per year over the duration of the catalog, and
     * the other is saved in unscaled format such that it is more of an alarm-based forecast as opposed to a rate forecast.
     * 
     * @param prospectiveSmoothingCatalogPath path to the prospective smoothing catalog
     * @param afvPath path where the prospective forecast will be stored
     * @param sigma lengthscale to be used for smoothing
     * @param forecastMLPath path to which we'll write output forecast file, in ForecastML format
     * @param forecastStartDate start date of prospective forecast
     * @param forecastEndDate end date of prospective forecast
     * @param minTargetMagnitude minimum target magnitude
     * @param maxTargetMagnitude maximum target magnitude  
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region
     * @param boxSize size of testing region cells (in degrees)
     * @param forecastNodesPath path to the forecast nodes of interest, we'll save the forecast only at these nodes
     * @param minDepth minimum depth of the study region
     * @param maxDepth maximum depth of the study region
     * @param magnitudeBinSize magnitude discretization employed for the forecast
     */
    private static void prospectiveForecasts(String prospectiveSmoothingCatalogPath, String afvPath, float sigma, String rateForecastMLPath, String forecastStartDate,
            String forecastEndDate, float minTargetMagnitude, float maxTargetMagnitude, float minLat, float maxLat, float minLon, float maxLon, float boxSize,
            String forecastNodesPath, float minDepth, float maxDepth, float magnitudeBinSize) {
        // Determine the average number of target earthquakes per year
        Catalog prospectiveSmoothingCatalog = new Catalog(prospectiveSmoothingCatalogPath, Catalog.JEREMY_GENERATED);
        Catalog historicalTargetEqks = prospectiveSmoothingCatalog.subcatalogByMagnitude(minTargetMagnitude, maxTargetMagnitude);
        String[] historicalCatalogTimes = prospectiveSmoothingCatalog.times();
        String historicalCatalogStart = historicalCatalogTimes[0];
        String historicalCatalogEnd = historicalCatalogTimes[historicalCatalogTimes.length - 1];
        float historicalCatalogLengthInDays = DateUtil.durationInOrderedDays(historicalCatalogStart, historicalCatalogEnd);
        float averageNumberOfTargetEqksPerYear = (float) historicalTargetEqks.numberOfEqks() / (historicalCatalogLengthInDays / 365.25f);
        System.out.println("average # of eqks/yr: " + averageNumberOfTargetEqksPerYear);

        float[] gridParameters = new float[5];
        gridParameters[0] = minLat;
        gridParameters[1] = maxLat;
        gridParameters[2] = minLon;
        gridParameters[3] = maxLon;
        gridParameters[4] = boxSize;

        // Smooth the catalog over the testing region
        float[] spatialSmoothingParameters = {sigma, 0f};
        float[] scaledAFV = SmoothingTools.smoothedEventMapFromCatalog(prospectiveSmoothingCatalog, minLat, maxLat, minLon, maxLon, boxSize, spatialSmoothingParameters);
        float[] unscaledAFV = new float[scaledAFV.length];
        System.arraycopy(scaledAFV, 0, unscaledAFV, 0, unscaledAFV.length);

        float unscaledSum = ArrayUtil.sum(scaledAFV);
        // The AF values are now the sum over the length of the historical catalog.  Now we scale them for a forecast of the appropriate duration
        float forecastExperimentDurationInDays = DateUtil.durationInOrderedDays(forecastStartDate, forecastEndDate);
        System.out.println("experiment duration:" + forecastExperimentDurationInDays);
        for (int i = 0; i < scaledAFV.length; i++) {
            scaledAFV[i] *= averageNumberOfTargetEqksPerYear / unscaledSum * forecastExperimentDurationInDays / 365.25f;
        }

// From the smoothed seismicity map, generate and save the corresponding alarm function value file
        AlarmFunction rateForecast = new AlarmFunction(scaledAFV, gridParameters);
//        rateForecast.save(afvPath + "pro" + sigma + ".afv");


//        Compute the regional b-value
//        float b = prospectiveSmoothingCatalog.akiMLEBValue();
        float b = 1;

        // If the forecast node path is not specified, save the entire rate forecast
        if (forecastNodesPath == null) {
            rateForecast.saveAsRateForecast(rateForecastMLPath, forecastStartDate, forecastEndDate, b, minDepth, maxDepth, minTargetMagnitude, maxTargetMagnitude,
                    magnitudeBinSize);
            // If the forecast node path is empty, save the entire rate forecast
        } else if (forecastNodesPath.isEmpty()) {
            rateForecast.saveAsRateForecast(rateForecastMLPath, forecastStartDate, forecastEndDate, b, minDepth, maxDepth, minTargetMagnitude, maxTargetMagnitude,
                    magnitudeBinSize);
            // Otherwise, save the rate forecast only for the cells of interest
        } else {
            rateForecast.saveAsRateForecast(rateForecastMLPath, forecastStartDate, forecastEndDate, forecastNodesPath, b, minDepth, maxDepth, minTargetMagnitude, maxTargetMagnitude,
                    magnitudeBinSize);
        }

//            rateForecast.saveAsASCII(rateForecastMLPath.replace(".xml", ".tsv"), forecastNodesPath, b, minTargetMagnitude, maxTargetMagnitude, magnitudeBinSize);
//        rateForecast.saveAsJochensFormat(rateForecastMLPath.replace(".xml", ".tsv"), forecastNodesPath, b, minTargetMagnitude, maxTargetMagnitude, magnitudeBinSize);
    }

    /**
     * Generate the catalogs to be used for retrospective and prospective smoothing experiments for the testing region of interest
     * 
     * @param entireCatalogPath path to the complete collection region JMA catalog in ZMAP format
     * @param collectionRegionCatalogPath path to which we'll save the catalog that has been filtered to the collection region
     * @param retrospectiveSmoothingCatalogPath path to which we'll save the retrospective smoothing catalog
     * @param retrospectiveTargetCatalogPath path to which we'll save the retrospective target catalog
     * @param prospectiveSmoothingCatalogPath path to which we'll save the prospective smoothing catalog
     * @param prospectiveSmoothingPeriodStart start date of the prospective smoothing period
     * @param prospectiveSmoothingPeriodEnd end date of the prospective smoothing period
     * @param retrospectiveSmoothingPeriodStart start date of the retrospective smoothing period
     * @param retrospectiveSmoothingPeriodEnd end date of the retrospective smoothing period  
     * @param minTargetMagnitude minimum target magnitude
     * @param maxTargetMagnitude maximum target magnitude  
     * @param minLat minimum latitude of the testing region
     * @param maxLat maximum latitude of the testing region
     * @param minLon minimum longitude of the testing region
     * @param maxLon maximum longitude of the testing region 
     */
    private static void catalogs(String entireCatalogPath, String collectionRegionCatalogPath, String retrospectiveSmoothingCatalogPath,
            String retrospectiveTargetCatalogPath, String prospectiveSmoothingCatalogPath, String prospectiveSmoothingPeriodStart, String prospectiveSmoothingPeriodEnd,
            String retrospectiveSmoothingPeriodStart, String retrospectiveSmoothingPeriodEnd, String retrospectiveTargetPeriodStart, String retrospectiveTargetPeriodEnd,
            float minTargetMagnitude, float maxTargetMagnitude, float minLat, float maxLat, float minLon, float maxLon, float minDepth, float maxDepth) {
        // Load the entire ZMAP catalog
        Catalog entireCatalog = new Catalog(entireCatalogPath, Catalog.ZMAP);

        // For the collection region, we'll extend the testing region one degree in each direction to reduce edge effects
        float minLat_collection = minLat - 1f;
        float maxLat_collection = maxLat + 1f;
        float minLon_collection = minLon - 1f;
        float maxLon_collection = maxLon + 1f;

        Catalog collectionRegionCatalog = entireCatalog.subcatalogBySpace(minLat_collection, maxLat_collection, minLon_collection, maxLon_collection, minDepth, maxDepth);
        collectionRegionCatalog.save(collectionRegionCatalogPath, "Catalog generated by loading catalog from " + entireCatalogPath + " and filtering by " +
                "min/max lat/lon/depth to the collection region");

        // Generate the retrospective smoothing catalog, which contains all events in the collection region during the retrospective smoothing period
        Catalog retrospectiveSmoothingCatalog = collectionRegionCatalog.subcatalogByTime(retrospectiveSmoothingPeriodStart, retrospectiveSmoothingPeriodEnd);
        retrospectiveSmoothingCatalog = retrospectiveSmoothingCatalog.subcatalogByMagnitude(minTargetMagnitude - 2f, maxTargetMagnitude);
        retrospectiveSmoothingCatalog.save(retrospectiveSmoothingCatalogPath, "Catalog generated by loading catalog from " +
                collectionRegionCatalogPath + " and filtering by date");

        // Generate the retrospective target catalog, which contains all target eqks in the testing region in the retrospective target period
        Catalog retrospectiveTargetCatalog = collectionRegionCatalog.subcatalogByTime(retrospectiveTargetPeriodStart, retrospectiveTargetPeriodEnd);
        retrospectiveTargetCatalog = retrospectiveTargetCatalog.subcatalogByMagnitude(minTargetMagnitude, maxTargetMagnitude);
        retrospectiveTargetCatalog = retrospectiveTargetCatalog.subcatalogBySpace(minLat, maxLat, minLon, maxLon, minDepth, maxDepth);
        retrospectiveTargetCatalog.save(retrospectiveTargetCatalogPath, "Catalog generated by loading catalog from " +
                collectionRegionCatalogPath + " and filtering by date, min/max lat/lon/depth, and by magnitude");

        // Generate a prospective smoothing catalog
        Catalog prospectiveSmoothingCatalog = collectionRegionCatalog.subcatalogByTime(prospectiveSmoothingPeriodStart, prospectiveSmoothingPeriodEnd);
        prospectiveSmoothingCatalog = prospectiveSmoothingCatalog.subcatalogByMagnitude(minTargetMagnitude - 2f, maxTargetMagnitude);
        prospectiveSmoothingCatalog.save(prospectiveSmoothingCatalogPath, "Catalog generated by loading catalog from " +
                collectionRegionCatalogPath + " and filtering by date, yielding all events in testing region during the prospective smoothing period");
    }

    /**
     * From the given testing region polygon, generate a forecast nodes file
     * 
     * @param lats array of testing region vertex latitudes
     * @param lons array of testing region vertex longitudes
     * @param boxSize cell spatial discretization
     * @param outputFilePath path to file to which the forecast nodes will be written
     */
    /*
    public static void forecastNodesFile(float[] lats, float[] lons, float boxSize, String outputFilePath) {
    float minLat = ArrayUtil.minimum(lats);
    float maxLat = ArrayUtil.maximum(lats);
    float minLon = ArrayUtil.minimum(lons);
    float maxLon = ArrayUtil.maximum(lons);

    try {
    FileOutputStream oOutFIS = new FileOutputStream(outputFilePath);
    BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
    BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

    for (float lat = minLat; lat <= maxLat; lat += boxSize) {
    for (float lon = minLon; lon <= maxLon; lon += boxSize) {
    if (MathUtil.isPointInsidePolygon(lat, lon, lats, lons)) {
    oWriter.write(lon + " " + lat + "\n");
    }
    }
    }

    oWriter.close();
    oOutBIS.close();
    oOutFIS.close();
    } catch (Exception ex) {
    System.err.println("Trouble generating the forecast nodes file");
    ex.printStackTrace();
    System.exit(-1);
    }
    }
     */
//    /**
//     * The CPTI catalog was given in Mw format, this lil guy will convert all
//     * magnitudes to Ml, using the relation mentioned in Annemarie's catalog
//     * summary document (http://www.cseptesting.org/sites/default/files/SummaryItalyCataloguesUpdate.pdf)
//     */
//    private static void convertCPTIMagnitudes() {
//        String catalogPath = "catalogs\\Italy CPTI 1901 to 2006 inclusive (ZMAP).cat";
//        Catalog cpti = new Catalog(catalogPath, Catalog.ZMAP);
//        float[] mws = cpti.mags();
//        float[] mls = new float[mws.length];
//        for (int i = 0; i < mws.length; i++) {
//            float mw = mws[i];
//            mls[i] = (mw - 1.145f) / 0.812f;
//        }
//
//        Catalog cptiMagConverted = new Catalog(cpti.times(), cpti.lats(), cpti.lons(), cpti.depths(), mls);
//        cptiMagConverted.saveAsZMAP(catalogPath);
//    }
    /**
     * Given the specified alarm function and a target eqk catalog, generate a GMT script and the required files to produce a map representation:
     *
     *  1. Determine appropriate color spectrum limits
     *  2. Generate .dat containing lat_lon for each target eqk
     *  3. Generate .dat containing latTransformed_lonTransformed_afv where xTransformed = x + 0.5 * cellsize
     *
     * @param afvFile path to alarm function value file of interest
     * @param catalogFile path to target eqk catalog of interest
     * @param gmtEqkFile name of GMT-formatted eqk .dat file to produce
     * @param gmtAFVFile name of GMT-formatted afv .dat file to produce
     * @param gmtScriptFile name of GMT script file to produce
     * @param experimentMapFile name of Postscript map file to produce
     * @param includeMarginOfError flag whether or not the margin of error should be included
     */
    public static void generateExperimentMapGMTScript(String afvFile, String catalogFile, String gmtEqkFile, String gmtAFVFile,
            String gmtScriptFile, String experimentMapFile) {
        AlarmFunction af = new AlarmFunction(afvFile);
        float[] afValues = af.values();
        float boxSize = af.sizeOfSpaceCell;
        float minLat = af.minimumLatitude;
        float maxLat = af.maximumLatitude;
        float minLon = af.minimumLongitude;
        float maxLon = af.maximumLongitude;
        float minAFV = ArrayUtil.minimum(afValues);
        float maxAFV = ArrayUtil.maximum(afValues);
        float afvStep = (maxAFV - minAFV) / 100f;

//        CatalogNoTime cat = new CatalogNoTime(catalogFile, Catalog.JEREMY_GENERATED_NO_TIME);
        Catalog cat = new Catalog(catalogFile, Catalog.JEREMY_GENERATED);
        float[] lats = cat.lats();
        float[] lons = cat.lons();

        try {
            // Write out the transformed afv values to a file
            String gmtAFVFilePath = "in response to yan/" + gmtAFVFile;
            FileOutputStream oOutFIS = new FileOutputStream(gmtAFVFilePath);
            BufferedOutputStream oOutBIS = new BufferedOutputStream(oOutFIS);
            BufferedWriter oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            for (int i = 0; i < afValues.length; i++) {
                if (afValues[i] > minAFV) {
                    float[] latLon = Utility.latlonFromCellPosition(i, minLat, minLon, maxLon, boxSize);
                    float latTranslated = latLon[0] + 0.5f * boxSize;
                    float lonTranslated = latLon[1] + 0.5f * boxSize;
                    oWriter.write(latTranslated + " " + lonTranslated + " " + afValues[i] + "\n");
                }
            }

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();

            // Write out the eqk data
            String gmtEqkFilePath = "in response to yan/" + gmtEqkFile;
            oOutFIS = new FileOutputStream(gmtEqkFilePath);
            oOutBIS = new BufferedOutputStream(oOutFIS);
            oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            for (int i = 0; i < lats.length; i++) {
                oWriter.write(lats[i] + " " + lons[i] + "\n");
            }

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();

            // Write out the script
            String gmtScriptFilePath = "in response to yan/" + gmtScriptFile;
            oOutFIS = new FileOutputStream(gmtScriptFilePath);
            oOutBIS = new BufferedOutputStream(oOutFIS);
            oWriter = new BufferedWriter(new OutputStreamWriter(oOutBIS));

            String commonString = " -JM6.5i " + "-R" + minLon + "/" + maxLon + "/" + minLat + "/" + maxLat;
//            oWriter.write("makecpt -CMaxSpectrum.cpt -T" + minAFV + "/" + maxAFV + "/" + afvStep + " -Z > temp.cpt\n");
            oWriter.write("makecpt -T" + minAFV + "/" + maxAFV + "/" + afvStep + " -Z > temp.cpt\n");
            oWriter.write("gmtset TICK_PEN 0/0/0 LABEL_FONT_SIZE 18p PAGE_ORIENTATION portrait PAPER_MEDIA letter\n");
            oWriter.write("gmtset FRAME_WIDTH 0.1i\n");
            oWriter.write("psxy " + gmtAFVFile + commonString + " -Y2.0i -K -Ss0.1i -Ctemp.cpt -: > " + experimentMapFile + "\n");
//            oWriter.write("psxy " + gmtEqkFile + commonString + " -G0/0/0 -K -Sa0.1i -: -O >> " + experimentMapFile + "\n");
            oWriter.write("psxy " + gmtEqkFile + commonString + " -Wthicker -G255/255/0 -K -Sa0.2i -: -O >> " + experimentMapFile + "\n");
            oWriter.write("pscoast " + commonString + " -K -W -P -Dh -O >> " + experimentMapFile + "\n");
//            oWriter.write("psscale -Ba0.5:AFV: -D3.25i/-0.5i/6i/0.3ih -Ctemp.cpt -K -O -N70 >> " + experimentMapFile + "\n");
            oWriter.write("psbasemap -B2.0/1.0WeSn" + commonString + " -O >> " + experimentMapFile + "\n");
            oWriter.write("convert " + experimentMapFile + " " + experimentMapFile.replace(".ps", ".png") + "\n");
//            oWriter.write("rm " + experimentMapFile + "\n");
            oWriter.write("rm temp.cpt");

            oWriter.close();
            oOutBIS.close();
            oOutFIS.close();

        } catch (Exception ex) {
            System.out.println("Error in Utility.generateExperimentMapGMTScript()");
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}

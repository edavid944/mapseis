/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jzechar
 */
public class LikelihoodTools {

    /**
     * Calculate the spatial joint log-likelihood for the given target eqk catalog and using the specified rate forecast as the predictor; each of these is provided as array that represents
     * a lat-lon-mag gridding of the study region.  predictor contains the rate forecast in each bin, and targetEqks contains the number of target eqks in each bin.
     * The idea here is to scale the rate forecast so that the total forecast rate over all bins is the same as the number of observed target eqks, and then compute the joint log-likelihood
     * of the observations given the forecast
     *
     * @param predictor alarm function values
     * @param targetEqks representation of target eqk distribution, each entry contains the number of target epicenters contained w/i this bin
     * @param numberOfTargetEqks total number of target eqks in the target catalog
     * @return spatial joint log likelihood of the observations given the forecast
     */
    public static double spatialJointLL(float[] predictor, short[] targetEqks, int numberOfTargetEqks) {
        float totalRateForecast = ArrayUtil.sum(predictor);
        float[] normalizedRateForecast = new float[predictor.length];

        for (int i = 0; i < targetEqks.length; i++) {
            normalizedRateForecast[i] = predictor[i] / totalRateForecast * (float) numberOfTargetEqks;
        }

        double jointLL = 0;

        for (int i = 0; i < normalizedRateForecast.length; i++) {
            float forecastRate = normalizedRateForecast[i];
            short observations = targetEqks[i];
            double ll = -forecastRate;
            if (observations > 0){
//                System.out.println("shite");
                ll = -forecastRate + observations * Math.log(forecastRate) - Math.log(MathUtil.factorial(observations));
            }

            
//            if (ll == Double.NaN){
//                System.out.println("shite");
//            }
            jointLL += ll;
        }
        return jointLL;
    }

}

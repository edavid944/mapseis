import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DateUtil {
    private static SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    
    /** Creates a new instance of DateUtil */
    public DateUtil() {
    }
    
    /**
     * Parse a date from the specified string
     * 
     * @param sDate string representation of the date, of the form yyyy/MM/dd HH:mm:ss
     * @return corresponding Date object
     */
    public static Date dateFromString(String sDate) {
        Date date = new Date();
        try {
            date = DateUtil.df.parse(sDate);
        } catch (Exception ex) {
            System.out.println("Error in DateUtil.getDateFromString(" + sDate + ")");
            ex.printStackTrace();
        }
        return (date);
    }
    
    /**
     * get the duration between two dates in days, requiring that the start occur before the end.  If the specified start date is after the specified end date, throw up.
     * 
     * @param start start of range, of the form yyyy/MM/dd HH:mm:ss
     * @param end end of range, of the form yyyy/MM/dd HH:mm:ss
     * @return the number of days b/w the specified start and end range
     */
    public static float durationInOrderedDays(String start, String end) {
        float duration = 0;
        
        // Calculate the number of days elapsed between the start and end
        try {
            Calendar startTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
            startTime.setTime(DateUtil.dateFromString(start));
            endTime.setTime(DateUtil.dateFromString(end));
            long endTimeMillis = endTime.getTimeInMillis();
            long startTimeMillis = startTime.getTimeInMillis();
            if (endTimeMillis < startTimeMillis){
                System.err.println("Start date is after end date!");
                System.err.println("Start = " + start);
//                System.exit(-1);
            }
            duration = Math.abs(endTimeMillis - startTimeMillis) / 1000.0f / 60.0f / 60.0f / 24.0f;
        } catch (Exception ex) {
            System.err.println("Error in DateUtil.durationInDays(" + start + ", " + end + ")");
            ex.printStackTrace();
        }
        return duration;
    }
}
#########################################################
######### OpenSha project						#########
######### step aftershock forecast application	#########
######### 8/9/2010							#########
#########################################################

-----------------------------------------------------
CSEP Step application:
---
1. run app
$ cd bin
## to generate step rates:
$ ./run_step 0 {-f param-file-path}
## to genrate step probabilities
$ ./run_step 1  {-f param-file-path}

2. app parameters (start and end time, forecast length, I/O files):
data/csep/CSEP_params.txt, which is specified by either commadline args or in default.properties


-----------------------------------------------------

1. this application calculates step aftershock rates and probabilities 
   based on the stochastic models derived from the Gutenberg–Richter relationship
   and the modified Omori law. This is a part of the opensha project.
   
2. default properties are set in /config/defaults.properties
   e.g. specify forecast/event time and data source 
   -- start.forecast.current
   -- forecast.start.time   
   -- event.start.time
   -- quake.datasource
   ...

3. input files are in /data/mattg_test, and can be changed in the config file
   output files are in output/ and can be  changed in the config file
   
4. to run this application, cd bin, and run the scripts:
   run_step {args[0]} {args[1]} {args[2]} {args[3]}
   examples:  
   run_step 0 -- run STEP_main (for specified event.start.time, and forecast.start.time)
   run_step 1 -- run STEP_HazardDataSet (for specified event.start.time, and forecast.start.time)
   run_step 1 s -- run the application as scheduled operation, e.g. once an hour, step models are saved
                    as serialized object and read back next time.   
   run_step 1 c  -- run STEP_HazardDataSet continuously for default.test.times in config/default.properties
   run_step 1 c n m  -- run STEP_HazardDataSet continuously for n times at m minutes interval
   run_step h -- get usage help
   
   or run java -jar from the root directory:
   java -jar  lib/step-aftershock.jar {args[0]} {args[1]} {args[2]} {args[3]}
   
5.notes
command examples:
5.1 for running the application normally (run_step 0, run_step 1) 
   -- forecast.start.time must be specified if start.forecast.current is set to no (testing),
   -- event.start.time must be specified if datasource is GeoNet quakeML,
   -- if datasource is text file, if forecast.start.time is not specified, the app will start reading
      events from the first record of the file.
   
5.2 for running the application for continuous testing (run_step 1 c, run_step 1 c n m) 
  -- event.start.time must be specified
  -- default.test.times must be specified for default times(run_step 1 c).
  -- default.test.read.back must be specified for get events N days before main event(run_step 1 c).
  -- delete STEP_AftershockObj in output directory at start (if you don't want read the serialized data from previous session).
   
5.3 for running the application as a scheduled operation (run_step 1 s, 
      usually scheduled as a cron job under Linux)
  -- delete STEP_AftershockObj in output directory at the first run
  -- days.from.qdm must be specified (days within which events are subject to change, events
      within this period are not saved and should always be read from catalogue)
  -- days.from.qdm: uncertain time period needs to be specified to purge events from step models
     to be saved as serialized java objects.
     
6. ant build notes
	cd opensha/src/org/opensha/step/
	ant init
	ant package
	-- the archived jar and tar files are in opensha/build directory
	


   
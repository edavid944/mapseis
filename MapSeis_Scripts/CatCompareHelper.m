function FuncOUT = CatCompareHelper(Commander)
	%Merges catalogs compared by the compare tool into a new one.
	%currently the catalogs can only be merged by there routing and not 
	%on the raw content.
	%also any Userdata content will be lost!!!
	
	import mapseis.util.emptier;
	import mapseis.projector.*;
	import mapseis.datastore.*;
	import mapseis.catalogtools.*;
	
	FuncOUT=[];
		
	%get MainWindow
	mainGUI=Commander.MainGUI;
	
	ComparePresent=false;
	ResultExist=false;
	CompareObj=[];
	CalcRes=[];
	
	%check if any calc window is open
	try
		
		CompareObj = mainGUI.Wrapper;
		
	catch
		
	end
	
	if ~isempty(CompareObj)
		try
			CalcRes=CompareObj.CalcRes
		catch
			
		end
	
	end
		
	if ~isempty(CalcRes)
		theFields=fieldnames(CalcRes);
		
		if any(strcmp(theFields,'identicalA'))
			ComparePresent=true;
			
			if ~isempty(CalcRes.identicalA);
				ResultExist=true;
			end
			
		
		end
	end	
	
		
	if ~ComparePresent
		msgbox('Catalog Compare is not running, please run first','Catalog Compare Script','error')
		return;
	end	
	
	if ~ResultExist
		msgbox('Catalog Compare Result have to be calculated first','Catalog Compare Script','warn')
		return;
	end
	
	
	%Build GUI
	Title = 'Select Master catalog';
	Prompt={'Master Catalog:', 'MasterCatSelect'};
	CatList={'DataStore_A';'DataStore_B'};
	
	%MasterCatSelect
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=CatList;
	Formats(1,1).size = [-1 0];
	
		
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	%default values
	defval.MasterCatSelect=1;
		
		
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
	
	if Canceled==1
		return;
	end
	
	CatSelect=false(2,1);
	CatSelect(NewParameter.MasterCatSelect)=true;
	
	%get the catalogs
	MasterData=CompareObj.(CatList{CatSelect});
	SecondData=CompareObj.(CatList{~CatSelect});
	
	%selections from compare
	UniKey= {'uniqA';'uniqB'};
	IdentKey={'identicalA';'identicalB'};
	
		
	MasterUni=CalcRes.(UniKey{CatSelect});
	SecondUni=CalcRes.(UniKey{~CatSelect});
	MasterIdent=CalcRes.(IdentKey{CatSelect});
	SecondIdent=CalcRes.(IdentKey{~CatSelect});
	
	
	%names for the tag
	NameTagMaster=MasterData.getUserData('Name');
	NameTagSecond=SecondData.getUserData('Name');
	
	NameTagMaster(NameTagMaster=='.'|NameTagMaster==' ')='';
	NameTagSecond(NameTagSecond=='.'|NameTagSecond==' ')='';
	
	
	%build part A
	MasterSelection=MasterUni|MasterIdent;
	MasterStruct=MasterData.getFields({},MasterSelection);
	MasterStruct.OriginCat=repmat({NameTagMaster},size(MasterStruct.dateNum));
	
	
	%build part B
	SecondSelection=SecondUni;
	SecondStruct=SecondData.getFields({},SecondSelection);
	SecondStruct.OriginCat=repmat({NameTagSecond},size(SecondStruct.dateNum));
	
	
	%Merge them
	MergedCats = catalogmerger(MasterStruct, SecondStruct);	
	
	%build new datastore catalog
	dataStore=DataStore(MergedCats);
	
	setDefaultUserData(dataStore);
	dataStore.setUserData('Name','Merged_Catalog');
	
	
	%push it to the Commander
	newdata.Datastore=dataStore;
	Commander.pushIt(newdata);
	
	
	
	

	
end

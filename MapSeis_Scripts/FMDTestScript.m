function FuncOUT = FMDTestScript(Commander)
   %This script plots the the location as points with size giving there
   %magnitude and color giving any parameter from the catalog wanted
   %This function maybe added later into a new plot plugin System.
   
   %not finished, the script uses at the moment time as fourth parameter
   %will later use a input screen
   
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;
	
    
    %get the plotaxis of the mainPlot in the mainGUI
    plotAxis=findobj(Commander.MainGUI.MainGUI,'Tag','axis_bottom_right')
    
    
   
    
    %
    %now the data
    FMDConfig	= struct(	'PlotType','FMDex',...
			 	'Data',Datastore,...
				'BinSize',0.1,...
				'b_line',true,...
				'Selected',selected,...
				'Mc_Method',1,...
				'Write_Text',false,...
				'LineStylePreset','normal',...
				'X_Axis_Label','Magnitude ',...
				'Y_Axis_Label','cum. Number of Events ',...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','FMD');
 
 
                        
    [handle entry] = PlotFMD_expanded(plotAxis,FMDConfig);


    Commander.MainGUI.reTag('right_bottom');
    
    FuncOUT=plotAxis;

	
	
end

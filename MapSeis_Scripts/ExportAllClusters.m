function FuncOUT = ExportAllClusters(Commander)
	%This scripts plots writes all clusters with more than one earthquake 
	%into a single earthquake catalogue file.
	
	import mapseis.projector.*;
	import mapseis.datastore.*;
	
	FuncOUT=[];

    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
	Filterlist = Commander.getCurrentFilterlist; 
	
    
  	%check if declustering is existing
  	[EqType ClustID]=Datastore.getDeclusterData;
  	
  	if isempty(ClustID)
  		errordlg('No declustering information available in this catalogue');
  		return
  	end
    
	%GUI
	Title = 'Cluster Exporter';
	Prompt={'Filetype:', 'Filetype';...
			'Aftershocks?','AftershockToggle';...
			'Mainshocks?','MainshockToggle';...
			'Past Cluster Filter','PastFilter'};
			
	MethodList =  {	'ZMAP binary'; ...
					'ZMAP ascii'};
					%'Datastore (caution large files)'}; 

	%default values
	defval =struct('Filetype',1,...
				'AftershockToggle',1,...
				'MainshockToggle',1,...
				'PastFilter',1);
	
	
	% Filetype
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=MethodList;
	Formats(1,2).type='none';
	Formats(1,1).size = [-1 0];
	Formats(1,2).limits = [0 1];
    
    % Aftershocks
	Formats(2,1).type='check';   
    
    % Mainshocks
	Formats(3,1).type='check';                      

	% Past Filter
	Formats(4,1).type='check';     
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';

	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
	
	
	if Canceled==1
		return
	end
	
	%get filename
	[fName,pName] = uiputfile('.*','Enter save file name...');
	[pathstr,name,ext] = fileparts(fName);

	
	%%turn of declustering 
	ClusterSetting=Datastore.getUsedType;
	ClassIf=Datastore.ShowUnclassified;
	Datastore.setUsedType(true(size(ClusterSetting)));
	Datastore.ShowUnclassified=true;
	
	%get filter selection
	selected=Filterlist.getSelected;
	
	%get the clusters suitable
	IDList=unique(ClustID(selected&~isnan(ClustID)));
	
	%get selected options
	PastFilter=~(logical(NewParameter.PastFilter));
	MainshockToggle =logical(NewParameter.MainshockToggle);
	AftershockToggle =logical(NewParameter.AftershockToggle);	
	
	
	for cC=1:numel(IDList)
		%prepare selection array
		ThisSelection=(PastFilter|selected)&...
						((MainshockToggle&(EqType==2))|(AftershockToggle&(EqType==3)))&...
						(ClustID==IDList(cC));
		if sum(ThisSelection)==0
			disp(['Cluster ',num2str(IDList(cC)) ,' was empty']);
			continue
		end				
						
		%prepare and save				
		if NewParameter.Filetype==1
			a=getZMAPFormat(Datastore, ThisSelection,true);
			filename=[pName,name,'_',num2str(IDList(cC)),'.mat'];
			save(filename,'a');
			
		elseif NewParameter.Filetype==2
			a=getZMAPFormat(Datastore, ThisSelection,true);
			filename=[pName,name,'_',num2str(IDList(cC)),'.dat'];
			dlmwrite(FileName, a, 'delimiter', '\t','precision', 12);
	
		elseif NewParameter.Filetype==3	
			%currently not supported
	
		end
	
	end
	
	%return to previous state
	Datastore.setUsedType(ClusterSetting);
	Datastore.ShowUnclassified= ClassIf;

end

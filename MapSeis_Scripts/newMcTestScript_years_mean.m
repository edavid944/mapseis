function FuncOUT = newMcTestScript_years_mean(Commander)
	%Do the same but over all year, only a testscript works once only
	
	import mapseis.util.emptier;
	import mapseis.region.*;
	import mapseis.plot.*;
	  
	FuncOUT=[];
	
	%check for existing McMaps, let the user select one.
	
	%get DataStore
	Datastore = Commander.getCurrentDatastore;
	
	%get previous calc
	try
		TheMaps=Datastore.getUserData('zmap-bval-calc');
	catch
		TheMaps=[];
	end
	
	%try get the stationlist
	try
		StationData=Datastore.getUserData('StationList');
	catch
		StationData=[];
	end
		
		
		
		
	if ~isempty(StationData)&~isempty(TheMaps)
		startDates={'1995-01-01';'1991-01-01';'1992-01-01';...
				'1993-01-01';'1994-01-01';'1996-01-01';...
				'1997-01-01';'1998-01-01';'1999-01-01';...
				'2000-01-01';'2001-01-01';'2002-01-01';...
				'2003-01-01';'2004-01-01';'2005-01-01';...
				'2006-01-01';'2007-01-01';'2008-01-01';...
				'2009-01-01'};
		endDates={'1995-12-31';'1991-12-31';'1992-12-31';...		
				'1993-12-31';'1994-12-31';'1996-12-31';...
				'1997-12-31';'1998-12-31';'1999-12-31';...
				'2000-12-31';'2001-12-31';'2002-12-31';...
				'2003-12-31';'2004-12-31';'2005-12-31';...
				'2006-12-31';'2007-12-31';'2008-12-31';...
				'2009-12-31'};
		yearList=[datenum(startDates,'yyyy-mm-dd'),datenum(endDates,'yyyy-mm-dd')];
		
		AvailCalc=TheMaps(14:32,:);	
		
		OverAllDist=[];
		for j=1:numel(yearList(:,1))
		
			%get filterlist
			FilterList=AvailCalc{j,3};
			ZmapDataDump=AvailCalc{j,2};

			%get years 
			minyear=yearList(j,1);
			maxyear=yearList(j,2);
			
			if strcmp(StationData.ListMode,'timevar')
				%get stations for the time
				%find the not set entries
				NotSetStart=isnan(StationData.starttime);
				NotSetEnd=isnan(StationData.endtime);
			
				%lower bound & upper bound
				LowTime=StationData.starttime<=minyear|NotSetStart;
				HightTime=StationData.endtime>=maxyear|NotSetEnd;
			
				InTime=LowTime&HightTime;
			else
				InTime=logical(ones(numel(StationData.lat),1));
				
				
			end	
			
			
			%stationlist
			stationlist=[StationData.lon(InTime),StationData.lat(InTime)];
			
			
			%get grid and map
			xvect=ZmapDataDump.xvect;
			yvect=ZmapDataDump.yvect;
			rawMcMap=ZmapDataDump.mMc;
			
			totalCells=numel(rawMcMap);
			
			%mesh it & reform it
			[newx newy]=meshgrid(xvect,yvect);
			
			McDataArray=[reshape(newx,1,totalCells)',reshape(newy,1,totalCells)',reshape(rawMcMap,1,totalCells)'];
			
			%go and process the data
			for i=1:totalCells
				if ~isnan(McDataArray(i,3))
					TheDistances=distance(McDataArray(i,1:2),stationlist);
					
					if numel(TheDistances)>=5
						distSelect=5;
					else
						distSelect=numel(TheDistances);
					end
					
					McDist(i,:)=[mean(TheDistances),McDataArray(i,3)];
				end
				
			end
			OverAllDist=[OverAllDist;McDist];
			
			
		end

		FuncOUT=McDist;
			
		%plot the whole stuff
		PlotData=[deg2km(OverAllDist(:,1)),OverAllDist(:,2)];
		
		figure
		plotAxis=gca;
		
		PlotConfig= struct(	'PlotType','generic2D',...
			'Data',PlotData,...
			'Line_or_Point','points',...
			'X_Axis_Label','Distance [km] ',...
			'Y_Axis_Label','Mc ',...
			'LineStylePreset','Fatline',...
			'MarkerStylePreset','cross',...
			'CustomMarker',{{'x',6}},...
			'Colors','red',...
			'X_Axis_Limit','auto',...
			'Y_Axis_Limit','auto',...
			'LegendText','Mc with distance');
				
		[handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);   
			
	else
		errordlg('No Mc map or Stationlist included in the datastore');	
		return
	end
		
		
end

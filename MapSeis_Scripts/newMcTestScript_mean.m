function FuncOUT = newMcTestScript_mean(Commander)
	%This small script allows to test Arnauds new Mc determination method. 
	%At the moment it is only a script but it will be transfered into a 
	%modul with full functionality later. It is at the moment a bit 
	%unclear how to handle certain input of parameter and the calculation
	%in the finally module (classic, interactiv or more than one step 
	%selectable?)
	
	import mapseis.util.emptier;
	import mapseis.region.*;
	import mapseis.plot.*;
	  
	FuncOUT=[];
	
	%check for existing McMaps, let the user select one.
	
	%get DataStore
	Datastore = Commander.getCurrentDatastore;
	
	%get previous calc
	try
		TheMaps=Datastore.getUserData('zmap-bval-calc');
	catch
		TheMaps=[];
	end
	
	%try get the stationlist
	try
		StationData=Datastore.getUserData('StationList');
	catch
		StationData=[];
	end
		
		
		
		
	if ~isempty(StationData)&~isempty(TheMaps)
		AvailCalc=TheMaps(:,1);	
		
		%Build GUI
		Title = 'Which HiRes Mc Map should be used';
		Prompt={'Mc Map:', 'MapSelector'};
		
		%Mc Method
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		%default values
		defval.MapSelector=1;
		
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
		
		
		MapSelector=NewParameter.MapSelector;
		
		
		
		if Canceled~=1
			%get filterlist
			FilterList=TheMaps{MapSelector,3};
			ZmapDataDump=TheMaps{MapSelector,2};
			
			%Apply FilterList on dataStore
			FilterList.changeData(Datastore);
			FilterList.update;
			
			
			timefilter=FilterList.getByName('Time');
			timeSelect=timefilter.getSelected;
			thetimes=Datastore.getFields('dateNum',timeSelect);
			minyear=min(thetimes.dateNum);
			maxyear=max(thetimes.dateNum);

			FilterList.PackIt;
			
			if strcmp(StationData.ListMode,'timevar')
				%get stations for the time
				%find the not set entries
				NotSetStart=isnan(StationData.starttime);
				NotSetEnd=isnan(StationData.endtime);
			
				%lower bound & upper bound
				LowTime=StationData.starttime<=minyear|NotSetStart;
				HightTime=StationData.endtime>=maxyear|NotSetEnd;
			
				InTime=LowTime&HightTime;
			else
				InTime=logical(ones(numel(StationData.lat),1));
				
				
			end	
			
			
			%stationlist
			stationlist=[StationData.lon(InTime),StationData.lat(InTime)];
			
			
			%get grid and map
			xvect=ZmapDataDump.xvect;
			yvect=ZmapDataDump.yvect;
			rawMcMap=ZmapDataDump.mMc;
			
			totalCells=numel(rawMcMap);
			
			%mesh it & reform it
			[newx newy]=meshgrid(xvect,yvect);
			
			McDataArray=[reshape(newx,1,totalCells)',reshape(newy,1,totalCells)',reshape(rawMcMap,1,totalCells)'];
			
			%go and process the data
			for i=1:totalCells
				if ~isnan(McDataArray(i,3))
					TheDistances=distance(McDataArray(i,1:2),stationlist);
					
					if numel(TheDistances)>=5
						distSelect=5;
					else
						distSelect=numel(TheDistances);
					end
					
					McDist(i,:)=[mean(TheDistances),McDataArray(i,3)];
				end
				
			end
			
			FuncOUT=McDist;
			
			%plot the whole stuff
			PlotData=[deg2km(McDist(:,1)),McDist(:,2)];
			
			figure
			plotAxis=gca;
			
			PlotConfig= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Distance [km] ',...
				'Y_Axis_Label','Mc ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',6}},...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Mc with distance');
				
			[handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);   
		
		end
	
	else
		errordlg('No Mc map or Stationlist included in the datastore');	
		return
	end
		
		
end

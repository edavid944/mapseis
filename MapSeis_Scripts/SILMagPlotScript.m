function FuncOUT = SILMagPlotScript(Commander)
	%This scripts plots a scatter plot with MW versus ML of the SeismoXML
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
	Filterlist = Commander.getCurrentFilterlist; 
	selected=Filterlist.getSelected;
    
    %get the mags
    eventStruct=Datastore.getRawFields({'Ml_from_amp' 'Ml_from_moment'},selected)
    
	
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
        
    %plot 1:1 line
    minval=floor(min([min(eventStruct.Ml_from_moment) min(eventStruct.Ml_from_amp)]));
    maxval=ceil(max([max(eventStruct.Ml_from_moment) max(eventStruct.Ml_from_amp)]));
    
    
    PlotData(1,:)=[minval minval];
    PlotData(2,:)=[maxval maxval];
    
     PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','line',...
				'X_Axis_Label','Ml from moment',...
				'Y_Axis_Label','Ml from amplitude ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Ml versus Ml');
    
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);
    
    hold on			
    
    clear PlotData;
    %plot scatter
    PlotData(:,1)=eventStruct.Ml_from_moment;
    PlotData(:,2)=eventStruct.Ml_from_amp;
    
    
    PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Ml from moment',...
				'Y_Axis_Label','Ml from amplitude ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',6}},...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Ml versus Ml');
    
                        
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
    
    
    hold off
    
    FuncOUT=plotAxis;

	
	
end

function FuncOUT = bval_trio_plotter(Commander)
	%Plots location in bvalue and sign. b-value in 3 different subplots from
	%a saved new bval-calc.
	
	import mapseis.util.emptier;
	import mapseis.util.AutoBiColorScale;
	import mapseis.region.*;
	import mapseis.plot.*;
	import mapseis.calc.signcalc.*;
	import mapseis.projector.*;
	import mapseis.filter.*;
	
	FuncOUT=[];
	
	%check for existing McMaps, let the user select one.
	
	%get DataStore
	Datastore = Commander.getCurrentDatastore;
	
	%get previous calc
	try
		TheMaps=Datastore.getUserData('new-bval-calc');
	catch
		TheMaps=[];
	end
	
		
		
		
		
	if ~isempty(TheMaps)
		AvailCalc=TheMaps(:,1);	
		
		%Build GUI
		Title = 'Which calculation should be used';
		Prompt={'Calculation:', 'MapSelector';
			'Super Elevation (Profile):','TheAspect'
			'Use Smoothing','SmoothMode'};
		
		%CalcSelect
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
	
		%SuperElevation
		Formats(2,1).type='edit';
		Formats(2,1).format='integer';
		Formats(2,1).limits = [0 9999];	
		
		%Smoothing
		Formats(3,1).type='check';
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		%default values
		defval.MapSelector=1;
		defval.TheAspect=2;
		defval.SmoothMode=false;
		
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
		
		
		
		
		
		if Canceled~=1
		
			
			%UNFINISHED
			
			MapSelector=NewParameter.MapSelector;
			TheAspect=NewParameter.TheAspect;
			
			%Load Data
			TheCalc=TheMaps{MapSelector,2};
				
					
			SendedEvents=TheCalc.Selected;
			CalcParameter=TheCalc.CalcParameter;
			ProfileSwitch=TheCalc.ProfileSwitch;
			ThirdDimension=TheCalc.ThirdDimension;
			CalcName=TheCalc.CalcName;
			AlphaMap=TheCalc.AlphaMap;
			CalcRes=TheCalc.CalcResult;
			CalcParameter.SmoothMode=NewParameter.SmoothMode;
			
			if strcmp(TheCalc.VersionCode,'v1.5')
				%add missing parameter if needed
				CalcParameter.Overall_bval=1;
				CalcParameter.Calc_sign_bval=0;
				MapSize=size(obj.CalcRes.Mc);
				
				CalcRes.sign_bvalue=NaN(MapSize);
				CalcRes.sign_WinningModel=NaN(MapSize);
				CalcRes.sign_likelihood1=NaN(MapSize);
				CalcRes.sign_likelihood2=NaN(MapSize);
				CalcRes.sign_akaike1=NaN(MapSize);
				CalcRes.sign_akaike2=NaN(MapSize);
				
				msgbox('Missing results were added as NaN')
				
			end
			
			if any(strcmp(TheCalc.VersionCode,{'v1.55','v1.5'}))
				ProfileWidth=[];
				ProfileLine=[];
			
			else
				ProfileWidth=TheCalc.ProfileWidth;
				ProfileLine=TheCalc.ProfileLine;
			
			end
			
			
			
			%check if 3D and break
			if ThirdDimension
				errordlg('3D is not implemented yet');
				return;
			end
			
			
			
			%create window
			newFig=figure;
			set(newFig,'Pos',[250 50 1000 1200],'Name','Trio Plot');
			plotAxis1=subplot(3,1,1);
			plotAxis2=subplot(3,1,2);
			plotAxis3=subplot(3,1,3);
			
			
			%get data
			xdir=CalcRes.X;
			ydir=CalcRes.Y;
			b_value=CalcRes.b_value';
			sign_bvalue=CalcRes.sign_bvalue';
			
			AlphaMap_bval=~isnan(b_value);
			AlphaMap_sign=~isnan(sign_bvalue);
			
			%This is to prevent colorscale differences after smoothing;
			
			
			if ~isempty(AlphaMap)
					minVal_b=min(min(b_value(~AlphaMap&AlphaMap_bval)));	
					maxVal_b=max(max(b_value(~AlphaMap&AlphaMap_bval)));
					minVal_s=min(min(sign_bvalue(~AlphaMap&AlphaMap_sign)));	
					maxVal_s=max(max(sign_bvalue(~AlphaMap&AlphaMap_sign)));
			else
					minVal_b=min(min(b_value(AlphaMap_bval)));
					maxVal_b=max(max(b_value(AlphaMap_bval)));
					minVal_s=min(min(sign_bvalue(AlphaMap_sign)));
					maxVal_s=max(max(sign_bvalue(AlphaMap_sign)));
			end
			
			caxislimit_b=[floor(10*minVal_b)/10 ceil(10*maxVal_b)/10];
			caxislimit_s=[floor(10*minVal_s)/10 ceil(10*maxVal_s)/10];
			
			if isempty(caxislimit_b)|any(isnan(caxislimit_b))
				caxislimit_b='auto';
			end
			
			if isempty(caxislimit_s)|any(isnan(caxislimit_s))
				caxislimit_s='auto';
			end
			
			%disp(caxislimit)
			
			if CalcParameter.SmoothMode==1;
				%Smooth the map before plotting
				TheValNaN_b=isnan(b_value);
				TheValNaN_s=isnan(sign_bvalue);
				
				
				%use the mean of the data instead of 0
				if ~isempty(AlphaMap)
					ValMean_b=mean(mean(b_value(~AlphaMap&AlphaMap_bval)));
					ValMean_s=mean(mean(sign_bvalue(~AlphaMap&AlphaMap_sign)));
				else
					ValMean_b=mean(mean(b_value(AlphaMap_bval)));
					ValMean_s=mean(mean(sign_bvalue(AlphaMap_sign)));
				end
				
				
				
				if ~isempty(ValMean_b)
					b_value(TheValNaN_b)=ValMean_b;
				else
					b_value(TheValNaN_b)=0;
				end
				
				if ~isempty(ValMean_s)
					sign_bvalue(TheValNaN_s)=ValMean_s;
				else
					sign_bvalue(TheValNaN_s)=0;
				end
				
				
				hFilter = fspecial('gaussian', 5, 2.5);
				
				b_value= imfilter((b_value + CalcParameter.SmoothFactor), hFilter, 'replicate');
				sign_bvalue= imfilter((sign_bvalue + CalcParameter.SmoothFactor), hFilter, 'replicate');
				
				
				%Smoothing adds to the total sum, subtract that to compensate.
				%TheVal=TheVal-obj.CalcParameter.SmoothFactor;
				
				b_value(TheValNaN_b)=NaN;
				sign_bvalue(TheValNaN_s)=NaN;
				
				if ~isstr(caxislimit_b)
					%add the factor to caxislimit
					caxislimit_b=caxislimit_b+CalcParameter.SmoothFactor;
				end
				
				if ~isstr(caxislimit_s)
					%add the factor to caxislimit
					caxislimit_s=caxislimit_s+CalcParameter.SmoothFactor;
				end
				
				%override the caxisstuff, not needed anyway
				if caxislimit_b(1)<=0.6
					caxislimit_b(1)=0.6;
					caxislimit_s(1)=0.6;
				end
				
				if caxislimit_b(2)>=1.6
					caxislimit_b(2)=1.6;
					caxislimit_s(2)=1.6;
				end
				
				
			end
			
			%limits
			xlimiter=[(min(xdir))-0.1 (max(xdir))+0.1];
			ylimiter=[(min(ydir))-0.1 (max(ydir))+0.1];
			
			%disp(xlimiter)
			%disp(ylimiter)
			if ProfileSwitch
				xlab='Profile [km] ';
				ylab='Depth [km] ';
			else
				xlab='Longitude ';
				ylab='Latitude ';
			end	
			
			
			
			%set all axes right
			xlim(plotAxis1,xlimiter);
			ylim(plotAxis1,ylimiter);
			xlim(plotAxis2,xlimiter);
			ylim(plotAxis2,ylimiter);
			xlim(plotAxis3,xlimiter);
			ylim(plotAxis3,ylimiter);
			
			%Will set differently later if needed
			latlim = get(plotAxis1,'Ylim');
			set(plotAxis1,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis1,'DrawMode','fast');
			
			set(plotAxis2,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis2,'DrawMode','fast');
			
			set(plotAxis3,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis3,'DrawMode','fast');
			
			
			if ~isempty(AlphaMap)
				b_value(AlphaMap)=NaN;
				sign_bvalue(AlphaMap)=NaN;
			end	
			
			
			
			
		
			
			
			%now go plotting
			
			if ~ProfileSwitch
				%plot 1 Earthquake
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',Datastore,...
							'PlotMode','old',...
							'PlotQuality','hi',...
							'SelectionSwitch','selected',...
							'SelectedEvents',SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
				
					
				
				
				BorderConf= struct('PlotType','Border',...
							'Data',Datastore,...
							'X_Axis_Label','Longitude ',...
							'Y_Axis_Label','Latitude ',...
							'LineStylePreset','normal',...
							'Colors','black');
				[BorderHand BorderEntry] = PlotBorder(plotAxis1,BorderConf);
				hold(plotAxis1,'on');
				
				CoastConf= struct( 'PlotType','Coastline',...
						   'Data',Datastore,...
						   'X_Axis_Label','Longitude ',...
						   'Y_Axis_Label','Latitude ',...
						   'LineStylePreset','Fatline',...
						   'Colors','blue');
				[CoastHand CoastEntry] = PlotCoastline(plotAxis1,CoastConf);
				hold(plotAxis1,'on');
				[handle1 entry1] = PlotEarthquake(plotAxis1,EqConfig);
				hold(plotAxis1,'off');
				
				%plot 2 b-value
				
				
				PlotData={xdir,ydir,b_value};
				PlotConfig	= struct(	'PlotType','ColorPlot',...
								'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData','auto',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit_b,...
								'ColorToggle',true,...
								'LegendText','b_value');
				%disp(plotAxis)				
				[handle legendentry] = PlotColor(plotAxis2,PlotConfig); 
				hold on
				[BorderHand BorderEntry] = PlotBorder(plotAxis2,BorderConf);
				
				[CoastHand CoastEntry] = PlotCoastline(plotAxis2,CoastConf);
				
				freezeColors;
				cbfreeze(plotAxis2);
				hold off
				
				
				%plot 3 sign. b-value
				
				
				PlotData={xdir,ydir,sign_bvalue};
					
				NewAlpha=ones(size(sign_bvalue));
				NewAlpha(isnan(sign_bvalue))=0;
				RegVal=sign_bvalue==CalcParameter.Overall_bval;
				NewAlpha(RegVal)=0.5;
									
					
					
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
								'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData',NewAlpha,...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit_s,...
								'ColorToggle',true,...
								'LegendText','sign. b-value');
				%disp(plotAxis)				
				[handle legendentry] = PlotColor(plotAxis3,PlotConfig); 
				hold on
				[BorderHand BorderEntry] = PlotBorder(plotAxis3,BorderConf);
				
				[CoastHand CoastEntry] = PlotCoastline(plotAxis3,CoastConf);
				
				prevCaxis=caxis(plotAxis3);
				minCal=floor(prevCaxis(1)*10)/10;
				maxCal=ceil(prevCaxis(2)*10)/10;
				overallVal=CalcParameter.Overall_bval;
				
				if CalcParameter.SmoothMode==1;	
					overallVal=overallVal+CalcParameter.SmoothFactor;
				end	
					 
					 
				%build scale
				biScale=AutoBiColorScale(minCal,maxCal,overallVal);
				
				%set axis
				caxis(plotAxis3,[minCal maxCal]);
				colormap(plotAxis3,biScale);
				
				hold off
				
				
				
				%set all axes right
				xlim(plotAxis1,xlimiter);
				ylim(plotAxis1,ylimiter);
				xlim(plotAxis2,xlimiter);
				ylim(plotAxis2,ylimiter);
				xlim(plotAxis3,xlimiter);
				ylim(plotAxis3,ylimiter);
			
				%Will set differently later if needed
				latlim = get(plotAxis1,'Ylim');
				set(plotAxis1,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
				set(plotAxis1,'DrawMode','fast','LineWidth',2,'FontSize',12);
			
				set(plotAxis2,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
				set(plotAxis2,'DrawMode','fast','LineWidth',2,'FontSize',12);
			
				set(plotAxis3,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
				set(plotAxis3,'DrawMode','fast','LineWidth',2,'FontSize',12);
				
								
				%set title
				theTit1=title(plotAxis1,'Earthquake Locations  ');
				theTit2=title(plotAxis2,'B-values  ');
				theTit3=title(plotAxis3,'significant b-values  ');
				set(theTit1,'FontSize',14,'FontWeight','bold','Interpreter','none');
				set(theTit2,'FontSize',14,'FontWeight','bold','Interpreter','none');
				set(theTit3,'FontSize',14,'FontWeight','bold','Interpreter','none');
				
				
				
			else
			
				%plot 1 Earthquake
				EqConfig= struct(	'PlotType','Earthquakes',...
							'Data',Datastore,...
							'PlotMode','old',...
							'ProfileLine',ProfileLine,...
							'ProfileWidth',ProfileWidth,...
							'PlotQuality','hi',...
							'SelectionSwitch','selected',...
							'SelectedEvents',SendedEvents,...
							'MarkedEQ','max',...
							'X_Axis_Label','Distance [km] ',...
							'Y_Axis_Label','Depth [km] ',...
							'MarkerSize','none',...
							'MarkerStylePreset','none',...
							'Colors','black',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'LegendText','Earthquakes');
				
					
				[handle1 entry1] = PlotEarthquakeSlice(plotAxis1,EqConfig);
				
				
				
				PlotData={xdir,ydir,b_value};
				PlotConfig	= struct(	'PlotType','ColorPlot',...
								'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData','auto',...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit_b,...
								'ColorToggle',true,...
								'LegendText','b_value');
				%disp(plotAxis)				
				[handle legendentry] = PlotColor(plotAxis2,PlotConfig); 
				freezeColors;
				cbfreeze(plotAxis2);
				
				
				PlotData={xdir,ydir,sign_bvalue};
					
				NewAlpha=ones(size(sign_bvalue));
				NewAlpha(isnan(sign_bvalue))=0;
				RegVal=sign_bvalue==CalcParameter.Overall_bval;
				NewAlpha(RegVal)=0.5;
									
					
					
				%build config for the plot
				PlotConfig	= struct(	'PlotType','ColorPlot',...
								'Data',{PlotData},...
								'MapStyle','smooth',...
								'AlphaData',NewAlpha,...
								'X_Axis_Label',xlab,...
								'Y_Axis_Label',ylab,...
								'Colors','jet',...
								'X_Axis_Limit',xlimiter,...
								'Y_Axis_Limit',ylimiter,...
								'C_Axis_Limit',caxislimit_s,...
								'ColorToggle',true,...
								'LegendText','sign. b-value');
				%disp(plotAxis)				
				[handle legendentry] = PlotColor(plotAxis3,PlotConfig); 
				
				prevCaxis=caxis(plotAxis3);
				minCal=floor(prevCaxis(1)*10)/10;
				maxCal=ceil(prevCaxis(2)*10)/10;
				overallVal=CalcParameter.Overall_bval;
				
				if CalcParameter.SmoothMode==1;	
					overallVal=overallVal+CalcParameter.SmoothFactor;
				end	
					 
					 
				%build scale
				biScale=AutoBiColorScale(minCal,maxCal,overallVal);
				
				%set axis
				caxis(plotAxis3,[minCal maxCal]);
				colormap(plotAxis3,biScale);
				
				
				
				%set all axes right
				xlim(plotAxis1,xlimiter);
				ylim(plotAxis1,ylimiter);
				xlim(plotAxis2,xlimiter);
				ylim(plotAxis2,ylimiter);
				xlim(plotAxis3,xlimiter);
				ylim(plotAxis3,ylimiter);
			
				set(plotAxis1,'dataaspect',[TheAspect 1 1]);
				set(plotAxis1,'YDir','reverse');
				set(plotAxis1,'LineWidth',2,'FontSize',12);
				
				set(plotAxis2,'dataaspect',[TheAspect 1 1]);
				set(plotAxis2,'YDir','reverse');
				set(plotAxis2,'LineWidth',2,'FontSize',12);
				
				set(plotAxis3,'dataaspect',[TheAspect 1 1]);
				set(plotAxis3,'YDir','reverse');
				set(plotAxis3,'LineWidth',2,'FontSize',12);
				
				
				%set title
				theTit1=title(plotAxis1,'Earthquake Locations  ');
				theTit2=title(plotAxis2,'B-values  ');
				theTit3=title(plotAxis3,'Significant b-values  ');
				set(theTit1,'FontSize',14,'FontWeight','bold','Interpreter','none');
				set(theTit2,'FontSize',14,'FontWeight','bold','Interpreter','none');
				set(theTit3,'FontSize',14,'FontWeight','bold','Interpreter','none');
				
			end
			
			
				
			%set positions of the plotaxis
			set(plotAxis1,'pos',[0.1 0.73 0.7 0.23],'Box','on','TickDir','out','TickLength'  ,[.015 .01] ,'XMinorTick','on','YMinorTick' ,...
						'on', 'YGrid','off','XGrid', 'off', 'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3]);
			set(plotAxis2,'pos',[0.1 0.41 0.7 0.23],'Box','on','TickDir','out','TickLength'  ,[.015 .01] ,'XMinorTick','on','YMinorTick' ,...
						'on', 'YGrid','off','XGrid', 'off', 'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3]);
			set(plotAxis3,'pos',[0.1 0.09 0.7 0.23],'Box','on','TickDir','out','TickLength'  ,[.015 .01] ,'XMinorTick','on','YMinorTick' ,...
						'on', 'YGrid','off','XGrid', 'off', 'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3]);
			
			%correct the colorbar of plot2
			Child=get(newFig,'children');
			Thebar2=Child(4);
			axis(plotAxis3)
			Thebar3=colorbar;
			
			posi=get(Thebar3,'pos');
			posi(2)=0.41;
			set(Thebar2,'pos',posi');
						
			
	else
		errordlg('No saved Calculations');	
		return
	end
		
		
end

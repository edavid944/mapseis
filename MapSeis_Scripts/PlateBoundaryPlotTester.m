function FuncOUT = PlateBoundaryPlotTester(Commander)
	%This scripts plots a scatter plot with MW versus ML of the SeismoXML
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    
    FuncOUT=[];
  
    
   
    
    TheAxes=findobj('type','axes');
	AxesName=get(TheAxes,'Tag');
	
	%get the parent figures
	ThePars=get(TheAxes,'Parent');
	TheParents(:,1)=[ThePars{:}];
	NameofParent=get(TheParents,'Name');
	
	%check if any of the tags is empty
	NotTagged=emptier(AxesName);
	
	if any(NotTagged)
		count=1;
		lastfig=-1;
		for i=1:numel(TheAxes)
			if ~isempty(AxesName{i});
				NameList(i)=AxesName(i);
			else	
				if lastfig~=TheParents(i)
					count=1;
					lastfig=TheParents(i);
				else
					count=count+1;
				end
				
				NameList{i}=[NameofParent{i},' ',num2str(count)];
			end	
		end
	
	else
		NameList=AxesName;
	
	end
    
    %Build GUI
	Title = 'Select the Axes which should be used for the Plot';
	Prompt={'Plot Axis:', 'AxSelector'};
	
	%Mc Method
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=NameList;
	Formats(1,1).size = [-1 0];
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	%default values
	defval.AxSelector=1;
	
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
	
	AxSelector=NewParameter.AxSelector;
	
	if Canceled~=1
    
	
	   
	    
	    LineStyles={'normal','Fineline','dotted','Fatline'};
	    ColorList={'blue','red','green','black','cyan','magenta'}; 
	    
	    %plot symbols
	    %PlotData(:,1)=eventStruct.Ml_from_moment;
	    %PlotData(:,2)=eventStruct.Ml_from_amp;
		
	    %Default Parameter:
	    defaultPlate= struct(	'PlotType','PlateBoundary',...
					'Ridge',true,...
					'Transform',true,...
					'Trench',true,...
					'RidgeLineStylePreset',1,...
					'TransformLineStylePreset',1,...
					'TrenchLineStylePreset',1,...
					'RidgeColors',2,...
					'TransformColors',3,...
					'TrenchColors',1)
					
					
	    Title = 'Select Plot Config';
	    Prompt={	'Plot Ridges:', 'Ridge';...
	    		'Ridge Color:','RidgeColors';...
	    		'Ridge LineStyle:','RidgeLineStylePreset';...
	    		'Plot Transforms:', 'Transform';...
	    		'Transforms Color:','TransformColors';...
	    		'Transforms LineStyle:','TransformLineStylePreset';...
	    		'Plot Trenches:', 'Trench';...
	    		'Trenches Color:','TrenchColors';...
	    		'Trenches LineStyle:','TrenchLineStylePreset'};			
	    %Ridge
	    Formats2(1,1).type='check';		
	    
	    Formats2(1,2).type='list';
	    Formats2(1,2).style='popupmenu';
	    Formats2(1,2).items=ColorList;
	    
	    Formats2(1,3).type='list';
	    Formats2(1,3).style='popupmenu';
	    Formats2(1,3).items=LineStyles;
	    
	    %Transform
	    Formats2(2,1).type='check';		
	    
	    Formats2(2,2).type='list';
	    Formats2(2,2).style='popupmenu';
	    Formats2(2,2).items=ColorList;
	    
	    Formats2(2,3).type='list';
	    Formats2(2,3).style='popupmenu';
	    Formats2(2,3).items=LineStyles;
	    
	    %Trench
	    Formats2(3,1).type='check';		
	    
	    Formats2(3,2).type='list';
	    Formats2(3,2).style='popupmenu';
	    Formats2(3,2).items=ColorList;
	    
	    Formats2(3,3).type='list';
	    Formats2(3,3).style='popupmenu';
	    Formats2(3,3).items=LineStyles;
					
	    
	    [NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats2,defaultPlate,Options); 	
	    
	    if Canceled~=1
	    
	    
	    	PlotConfig=NewParameter;
	    	
	    	
	    	
	    	PlotConfig.RidgeLineStylePreset=LineStyles{NewParameter.RidgeLineStylePreset};
	    	PlotConfig.TransformLineStylePreset=LineStyles{NewParameter.TransformLineStylePreset};
	    	PlotConfig.TrenchLineStylePreset=LineStyles{NewParameter.TrenchLineStylePreset};
	    	PlotConfig.RidgeColors=ColorList{NewParameter.RidgeColors};
	    	PlotConfig.TransformColors=ColorList{NewParameter.TransformColors};
	    	PlotConfig.TrenchColors=ColorList{NewParameter.TrenchColors};
	    	
	    	PlotConfig.X_Axis_Limit='Longitude';
	    	PlotConfig.Y_Axis_Limit='Latitude';
	    		    
		plotAxis=TheAxes(AxSelector);
	    
		xlimiter=get(plotAxis,'xlim');
		ylimiter=get(plotAxis,'ylim');
		    
		    
		%axes(plotAxis)
		axes(plotAxis)
		hold on			
					
					
					
		[handle legendentry] = PlotPlateBoundaries(plotAxis,PlotConfig);               
		
		xlim(xlimiter);
		ylim(ylimiter);
		latlim = get(plotAxis,'Ylim');
		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
		set(plotAxis,'DrawMode','fast');
		axes(plotAxis)
		hold off
	    
		FuncOUT=plotAxis;

	end
	
end

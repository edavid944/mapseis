function FuncOUT = sign_bval_test(Commander)
	%This script uses the AkiLikelihood to check for signifcant changes in
	%the bvalues
	%this will be later used in a new Mc- bval- mapping modul
	
	import mapseis.util.emptier;
	import mapseis.util.AutoBiColorScale;
	import mapseis.region.*;
	import mapseis.plot.*;
	import mapseis.calc.signcalc.*;
	import mapseis.projector.*;
	import mapseis.filter.*;
	
	FuncOUT=[];
	
	%check for existing McMaps, let the user select one.
	
	%get DataStore
	Datastore = Commander.getCurrentDatastore;
	
	%get previous calc
	try
		TheMaps=Datastore.getUserData('zmap-bval-calc');
	catch
		TheMaps=[];
	end
	
		
		
		
		
	if ~isempty(TheMaps)
		AvailCalc=TheMaps(:,1);	
		
		%Build GUI
		Title = 'Which HiRes Mc Map should be used';
		Prompt={'Mc Map:', 'MapSelector';...
			'Overall b-value','overall_bval'};
		
		%Mc Map
		Formats(1,1).type='list';
		Formats(1,1).style='popupmenu';
		Formats(1,1).items=AvailCalc;
		Formats(1,1).size = [-1 0];
		
		%Overall b
		Formats(2,1).type='edit';
		Formats(2,1).format='float';
		Formats(2,1).limits = [0 10];
		Formats(2,1).size = [-1 0];
		
		
		%%%% SETTING DIALOG OPTIONS
		Options.WindowStyle = 'modal';
		Options.Resize = 'on';
		Options.Interpreter = 'tex';
		Options.ApplyButton = 'off';
		
		%default values
		defval.MapSelector=1;
		defval.overall_bval=1;
		
		%open the dialog window
		[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
		
		
		MapSelector=NewParameter.MapSelector;
		overall_bval=NewParameter.overall_bval;
		
		
		if Canceled~=1
			%get filterlist
			FilterList=TheMaps{MapSelector,3};
			ZmapDataDump=TheMaps{MapSelector,2};
			
			%Apply FilterList on dataStore
			if FilterList.Packed
				Packed=true;
			else
				Packed=false;
			end	
			FilterList.changeData(Datastore);
			FilterList.update;
			
			%get the selection
			selected=FilterList.getSelected;
			
			%pack it again
			%FilterList.PackIt;
			
			%get all the necessary data from the zmap data dump
			%get grid and map
			xvect=ZmapDataDump.xvect';
			yvect=ZmapDataDump.yvect';
			dx=ZmapDataDump.dx;
			dy=ZmapDataDump.dy;
			SelMethod=ZmapDataDump.SelMethod;
			SelNumber=ZmapDataDump.ni;
			SelRadius=km2deg(ZmapDataDump.ra);
			rawbvalMap=ZmapDataDump.mBvalue;
			
			
			if SelMethod==1
				tgl1=1;
				tgl2=0;
				SelMe='Number';
				
			elseif SelMethod==2
				tgl1=0;
				tgl2=1;
				SelMe='Radius';
				
			end	
			
			
			totalCells=numel(rawbvalMap);
			rowCount = numel(xvect);
			colCount = numel(yvect);
			
			%empty map
			rawMap=nan(rowCount,colCount)';
			
			modelinfo.nWinningModel=rawMap;
			modelinfo.bpreferred=rawMap;
			modelinfo.likelihood1=rawMap;
			modelinfo.likelihood2=rawMap;
			modelinfo.nevents=rawMap;
			modelinfo.akaike1=rawMap;
			modelinfo.akaike2=rawMap;
			
			%mesh it & reform it
			%[newx newy]=meshgrid(xvect,yvect);
			
			%McDataArray=[reshape(newx,1,totalCells)',reshape(newy,1,totalCells)',reshape(rawbvalMap,1,totalCells)'];
			
			h = waitbar(0,'Please wait...');
			
			%modelinfo.nWinningModel=modelinfo.nWinningModel';
			%modelinfo.bpreferred=modelinfo.bpreferred';
			%modelinfo.likelihood1=modelinfo.likelihood1';
			%modelinfo.likelihood2=modelinfo.likelihood2';
			%modelinfo.nevents=modelinfo.nevents';
			%modelinfo.akaike1=modelinfo.akaike1';
			%modelinfo.akaike2=modelinfo.akaike2';
			%rawbvalMap=rawbvalMap';
			
			RawzmapCat=getZMAPFormat(Datastore);
			%go and process the data
			%slower with two loops, but at least it works for the old zmap type bvalue maps
			%in later version it has to be changed anyway
			for i=1:rowCount
				for j=1:colCount
					thisGridPoint = [xvect(i), yvect(j)];
				
					if ~isnan(rawbvalMap(j,i))
						filterFun = RadNumberFilter(thisGridPoint,SelRadius,SelNumber,SelMe,'RadDist');
						filterFun.execute(Datastore);
						logicalIndices = filterFun.getSelected() & selected;
						
						%now the manual calc
						
						%build catalog
						zmapCat=RawzmapCat(logicalIndices,:);
						
						rawmodel=winningmodel(zmapCat, overall_bval, rawbvalMap(j,i));
						
						%write in structur
						
						modelinfo.nWinningModel(j,i)=rawmodel.nWinningModel;
						modelinfo.bpreferred(j,i)=rawmodel.bpreferred;
						modelinfo.likelihood1(j,i)=rawmodel.likelihood1;
						modelinfo.likelihood2(j,i)=rawmodel.likelihood2;
						modelinfo.nevents(j,i)=rawmodel.nevents;
						modelinfo.akaike1(j,i)=rawmodel.akaike1;
						modelinfo.akaike2(j,i)=rawmodel.akaike2;
							
						
					end
					
					 if rem(i,5) == 0
						waitbar(i./rowCount,h);
					 end
				
				end
				
			end
			% Make the matrix have the correct orientation again.
			%modelinfo.nWinningModel=modelinfo.nWinningModel';
			%modelinfo.bpreferred=modelinfo.bpreferred';
			%modelinfo.likelihood1=modelinfo.likelihood1';
			%modelinfo.likelihood2=modelinfo.likelihood2';
			%modelinfo.nevents=modelinfo.nevents';
			%modelinfo.akaike1=modelinfo.akaike1';
			%modelinfo.akaike2=modelinfo.akaike2';
			
			%rawbvalMap=rawbvalMap';
			% Close the waitbar
			close(h)
			
			
			
			
			
			%try rebuild the Alphamap
			regionFilter = FilterList.getByName('Region');
			regionParms = regionFilter.getRegion();
			AlphaMap=[];
			
			if ~strcmp(regionParms{1},'all')
				theRegion = regionParms{2};
				Region=theRegion;
				
				RawAlpha=[];
				newgri=ZmapDataDump.newgri;
				ll=ZmapDataDump.ll;
				disp('Please wait, Alphamap is rebuild')
				 %for i= 1:length(newgri(:,1))
				 
				 	x = newgri(:,1);y = newgri(:,2);
				 RawAlpha=Region.isInside([x,y]);
				 %end
				 normlap2=ones(length(ll),1)*nan;
				 if ~isempty(RawAlpha)
				 	
				 	alphaint(ll) = RawAlpha;
				 	try
				 		AlphaMap=~logical(reshape(alphaint,length(yvect),length(xvect)));
				 	catch
				 		%means the polygon is badly shaped
				 		AlphaMap='auto';
				 		disp('could not restore AlphaMap')
				 	end	
				 end
			else
				Region=[];
				AlphaMap=[];
			end
			
			modelinfo.AlphaMap=AlphaMap;
			
			if ~isempty(AlphaMap)
				bval=modelinfo.bpreferred;
				bval(AlphaMap)=NaN;
				
			else
				bval=modelinfo.bpreferred;
			end
			
			FuncOUT=modelinfo;
			
			
			%limits
			xlimiter=[floor(min(xvect)) ceil(max(xvect))];
			ylimiter=[floor(min(yvect)) ceil(max(yvect))];
			
			xlab='Longitude ';
			ylab='Latitude ';
			
			figure
			plotAxis=gca;
			
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
		
			latlim = get(plotAxis,'Ylim');
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis,'DrawMode','fast');
			
			
			PlotData={xvect,yvect,bval};
    

			PlotConfig	= struct(	'PlotType','ColorPlot',...
							'Data',{PlotData},...
							'MapStyle','smooth',...
							'AlphaData','auto',...
							'X_Axis_Label',xlab,...
							'Y_Axis_Label',ylab,...
							'Colors','jet',...
							'X_Axis_Limit',xlimiter,...
							'Y_Axis_Limit',ylimiter,...
							'C_Axis_Limit','auto',...
							'ColorToggle',true,...
							'LegendText','signifcant b-values');
			    
						
			[handle legendentry] = PlotColor(plotAxis,PlotConfig);       
			
			hold on
			BorderConf= struct(	'PlotType','Border',...
						'Data',Datastore,...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'LineStylePreset','normal',...
						'Colors','blue');
			[BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
				
			CoastConf= struct( 	'PlotType','Coastline',...
						'Data',Datastore,...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'LineStylePreset','normal',...
						'Colors','black');
			[CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
				
			hold off	
			
			xlim(plotAxis,xlimiter);
			ylim(plotAxis,ylimiter);
		
			latlim = get(plotAxis,'Ylim');
			set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
			set(plotAxis,'DrawMode','fast');
			
			
			%set colorbar right (assume b-values between 0 and 9)
			prevCaxis=caxis(plotAxis);
			minVal=floor(prevCaxis(1)*10)/10;
			maxVal=ceil(prevCaxis(2)*10)/10;
			
			%build scale
			biScale=AutoBiColorScale(minVal,maxVal,overall_bval);
			
			%set axis
			caxis(plotAxis,[minVal maxVal]);
			colormap(plotAxis,biScale);
			
			
			%pack it again
			if Packed
				FilterList.PackIt;	
			end
		end
		
	else
		errordlg('No Mc map or Stationlist included in the datastore');	
		return
	end
		
		
end

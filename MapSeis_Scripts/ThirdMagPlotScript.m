function FuncOUT = ThirdMagPlotScript(Commander)
   %This script plots the the location as points with size giving there
   %magnitude and color giving any parameter from the catalog wanted
   %This function maybe added later into a new plot plugin System.
   
   %not finished, the script uses at the moment time as fourth parameter
   %will later use a input screen
   
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;
	
    %get the mags
    eventStruct=Datastore.getFields({'lat' 'lon' 'mag' 'depth'},selected);
    xrange=[min(eventStruct.lon)-0.1, max(eventStruct.lon)+0.1]
    yrange=[min(eventStruct.lat)-0.1, max(eventStruct.lat)+0.1]
    
    dectime = Datastore.getUserData('DecYear');
    
    RawStruct=Datastore.getRawFields;
    RawStruct.DecYear=dectime;
    
    TheFields=fieldnames(RawStruct);
    %TheFields{end+1}='DecYear';
    
    %Build GUI
    Title = 'Select the additional parameter to plot';
    Prompt={'Additional Parameter:', 'ParSelect';...
    		'External Window:', 'ExtWin';...
    		'Big Symbols:', 'BigSym'};
		
    %Mc Method
    Formats(1,1).type='list';
    Formats(1,1).style='popupmenu';
    Formats(1,1).items=TheFields;
    Formats(1,1).size = [-1 0];
    
    Formats(2,1).type='check';
    
    Formats(3,1).type='check';
		
    %%%% SETTING DIALOG OPTIONS
    Options.WindowStyle = 'modal';
    Options.Resize = 'on';
    Options.Interpreter = 'tex';
    Options.ApplyButton = 'off';

    %default values
    defval.ParSelect=1;
    defval.ExtWin=0;
    defval.BigSym=0;
    
    %open the dialog window
    [NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
		
		
    ParSelect=NewParameter.ParSelect;
    ExtWin=logical(NewParameter.ExtWin);
    BigSym=logical(NewParameter.BigSym);
    
    
    if Canceled~=1
    
	    %get additional field
	    AddOnField=RawStruct.(TheFields{ParSelect});
	    %(selected);    	    

	    %get the plotaxis of the mainPlot in the mainGUI
	    if ~ExtWin
	    	plotAxis=findobj(Commander.MainGUI.MainGUI,'Tag','axis_left');
	    else
	    	figure;
	    	plotAxis=gca;
	    end
	    
	    
	    
	    %frist try to plot coastline and border
	    CoastConf= struct(	'PlotType','Coastline',...
				'Data',Datastore,...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','normal',...
				'Colors','blue');
				
	    BorderConf= struct(	'PlotType','Border',...
				'Data',Datastore,...
				'X_Axis_Label','Longitude ',...
				'Y_Axis_Label','Latitude ',...
				'LineStylePreset','dotted',...
				'Colors','black');
				
	    [CoastHand CoastEntry] = PlotCoastline(plotAxis,CoastConf);
	    hold on
	    [BorderHand BorderEntry] = PlotBorder(plotAxis,BorderConf);
				
	    
	    hotter=colormap(copper);
	    hotter=flipud(hotter);
	    
	    if ~BigSym
	    	MarkSize='normal'
	    	Ranger=[0 8 9];
	    else
	  	MarkSize='xxlarge'
	  	Ranger=[3 7 8];
	    end
	    
	    %now the data
	    EqConfigSel	= struct(	'PlotType','Earthquakes',...
					'Data',Datastore,...
					'PlotMode','new',...
					'SelectionSwitch','selected',...
					'SelectedEvents',selected,...
					'AdditionalData',AddOnField,...
					'AddDataName','Time',...
					'MarkedEQ','max',...
					'X_Axis_Label','Longitude ',...
					'Y_Axis_Label','Latitude ',...
					'MarkerSize',MarkSize,...
					'MarkerStylePreset','normal',...
					'CustomMarkerRange',[Ranger],...
					'Colors','jet',...
					'X_Axis_Limit',xrange,...
					'Y_Axis_Limit',yrange,...
					'C_Axis_Limit','auto',...
					'ColorToggle',true,...
					'SizeLegend',true,...
					'LegendText','Earthquakes');
	    EqConfigUnsel = EqConfigSel;
	    %EqConfigUnsel.SelectionSwitch='unselected';
				
	    [handle1 entry1] = PlotEarthquake(plotAxis,EqConfigSel);
	    latlim = get(plotAxis,'Ylim');
		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
		set(plotAxis,'DrawMode','fast');
	    
	    
	    hold off
	
	    
	    FuncOUT=plotAxis;

    end
	
end

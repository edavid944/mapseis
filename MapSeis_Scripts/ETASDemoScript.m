function FuncOUT = ETASDemoScript(Commander)
   %This script produces a one-day ETAS forecast for a fixed day with a fixed 
   %paramater set. It only uses the Catalog and the Filterlist directly from
   %MapSeis. An extension to various different dates and so one is relatively 
   %easy.

   
   
    import mapseis.ETAS.*;
    import mapseis.forecast.*;
    import mapseis.forecast.ModelPlugIns.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;

    
    %data needed for the forecast region
    MagRange = [3.5 7.6];
    DepthRange = [0 20];
    GridPrecision=false;
    
    %Spacings=[lon_space, lat_space, mag_bin, depth_bin];
    ToSet.Spacings = [0.025 0.025 0.1 NaN];
    ToSet.ForecastLength = 1;
    ToSet.PredictionIntervall = 1;
    ToSet.StartTime = 733558; %datenum time format
    ToSet.EndTime = 733558; %datenum time format
    ToSet.LearningPeriod = [727380 733558];
    ToSet.Datastore = Datastore;
    ToSet.Filterlist = Filterlist;
    ToSet.LearningFilter = Filterlist;
    
    %where the result should be stored
    ToSet.StoreProject = './Forecast_Results/Forecast_ETASDemo.mat';
    
    %store the parameters into a structure makes it easier to automatically 
    %assign them to the Forecast project (loopable)
    ToSetList=fieldnames(ToSet);
    
    %Generate a Forecast Project
    ForecastProject = ForecastProject('ETASDemo');
    
    %Set the parameters of the project
    for pC=1:numel(ToSetList)
    	ForecastProject.setParameter(ToSetList{pC},ToSet.(ToSetList{pC}));
    end
    
    %setup test region
    ForecastProject.autoSetupRegion(MagRange,DepthRange,GridPrecision);

    %setup time intervals
    ForecastProject.autoSetupTime;

    %now the model plugin
    EtasModel = ETAS_mat_mk1Plugin('v1');

    %configure it
    %get Config first
    ConfigData = EtasModel.getModelConfig
    
    %rewrite them (we already have the parameters estimate to save time)
    ConfigData.b = 0.974;
    ConfigData.K = 0.6586;
    ConfigData.c = 0.0318;
    ConfigData.p = 1.4888;
    ConfigData.a = 0.1723
    ConfigData.d = 4.1601e-05;
    ConfigData.q = 2.5633;
    ConfigData.MinMag = 1;
    ConfigData.MinMainMag = 2;
    ConfigData.Est_Param = false;
    ConfigData.ResEst_Param = false;
    ConfigData.BG_Prob = 0.1;
    ConfigData.BG_SmoothParam = [0.05 10 0];
    ConfigData.Res_SmoothMode = 'ClassicETAS'
    ConfigData.Res_SmoothParam = [0.05 10 1];
    
    EtasModel.ConfigureModel(ConfigData);
    
    %set frameconfig (no config needed)
    EtasModel.ConfigureFramework([]);
    
    %add the region config to the ETAS model
    RegionConfig = ForecastProject.buildRegionConfig;
    EtasModel.ConfigureTestRegion(RegionConfig);

    %add model to ForecastProject
    ForecastProject.addModel(EtasModel);
	
    %finalize the Forecast project
    ForecastProject.cleanForecasts;
    ForecastProject.correctProgress;
    ForecastProject.checkState;

    save(ForecastProject.StoreProject,'ForecastProject');
    
    %run it
    CastMaker = ForecastCalculator('DoesNotMatter');
    CastMaker.setProject(ForecastProject);
    CastMaker.generateCalcSteps;
    CastMaker.startCalc;

    msgbox('Forecast Finished')
    FuncOUT=0;
    
    
end


function FuncOUT = TripelSDemoScript(Commander)
   %This script produces a one-day TripleS forecast for a fixed day with a fixed 
   %paramater set. It only uses the Catalog and the Filterlist directly from
   %MapSeis. An extension to various different dates and so one is relatively 
   %easy.

   %not yet finished
   
    import mapseis.ETAS.*;
    import mapseis.forecast.*;
    import mapseis.forecast.ModelPlugIns.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;

    
    %data needed for the forecast region
    MagRange = [3.5 7.6];
    DepthRange = [0 20];
    GridPrecision=false;
    
    %Spacings=[lon_space, lat_space, mag_bin, depth_bin];
    ToSet.Spacings = [0.025 0.025 0.1 NaN];
    ToSet.ForecastLength = 1;
    ToSet.PredictionIntervall = 1;
    ToSet.StartTime = 733558; %datenum time format
    ToSet.EndTime = 733558; %datenum time format
    ToSet.LearningPeriod = [727380 733558];
    ToSet.Datastore = Datastore;
    ToSet.Filterlist = Filterlist;
    ToSet.LearningFilter = Filterlist;
    
    %where the result should be stored
    fs=filesep;
    ToSet.StoreProject = ['.',fs,'Forecast_Results',fs,'Forecast_Demo.mat'];
    
    %store the parameters into a structure makes it easier to automatically 
    %assign them to the Forecast project (loopable)
    ToSetList=fieldnames(ToSet);
    
    %Generate a Forecast Project
    ForecastProject = ForecastProject('StepDemo');
    
    %Set the parameters of the project
    for pC=1:numel(ToSetList)
    	ForecastProject.setParameter(ToSetList{pC},ToSet.(ToSetList{pC}));
    end
    
    %setup test region
    ForecastProject.autoSetupRegion(MagRange,DepthRange,GridPrecision);

    %setup time intervals
    ForecastProject.autoSetupTime;

    %now the model plugin
    TripModel = TripleSPlugin_mk2('v1');

    %configure it
    %the frame config is needed in this case
    FrameConfig = TripModel.getFrameConfig;
  
    Frameconfig.TemporaryFolder=['.',fs,'Temporary_Files'];
    Frameconfig.ModelConfigFile=['.',fs,'Forecast_Models',fs,'TripleS_mk2',fs,'FrameConfig.conf'];
    
    TripModel.ConfigureFramework(Frameconfig);
    
    
    %actual model config (get config first)
    ConfigData = TripModel.getModelConfig
    
    %rewrite them (we already have the parameters estimate to save time)
    ConfigData.FixedTime = false;
    ConfigData.StartRetroLearning = '1991/07/01 00:00:00';
    ConfigData.EndRetroLearning = '1999/01/01 00:00:00';
    ConfigData.StartRetroTesting = '1999/01/01 00:00:00';
    ConfigData.EndRetroTesting = '2007/01/01 00:00:00';
    ConfigData.ForecastType = 'Poisson';

  
    
    TripModel.ConfigureModel(ConfigData);
    
    
    %add the region config to the ETAS model
    RegionConfig = ForecastProject.buildRegionConfig;
    TripModel.ConfigureTestRegion(RegionConfig);

    %add model to ForecastProject
    ForecastProject.addModel(TripModel);
	
    %finalize the Forecast project
    ForecastProject.cleanForecasts;
    ForecastProject.correctProgress;
    ForecastProject.checkState;

    save(ForecastProject.StoreProject,'ForecastProject');
    
    %run it
    CastMaker = ForecastCalculator('DoesNotMatter');
    CastMaker.setProject(ForecastProject);
    CastMaker.generateCalcSteps;
    CastMaker.startCalc;

    msgbox('Forecast Finished')
    FuncOUT=0;
    
    
end


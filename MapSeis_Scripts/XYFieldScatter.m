function FuncOUT = XYFieldScatter(Commander)
	%This scripts plots two fields of catalogue as X-Y scatter plot.
	%It is meant as demonstration script for the manual to give an
	%overview over common commands needed.

	%for this script the plot library of MapSeis is needed
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
	Filterlist = Commander.getCurrentFilterlist; 
	selected=Filterlist.getSelected;
    
    %get datafields of the catalogue
    DataFieldsRaw=Datastore.getFieldNames;
    
    %get all fields
    eventStruct=Datastore.getFields({});
    
    %because none numeric fields are allowed in the datastore,
    %we have to check if a field is numeric
    DataFields={};
    for fC=1:numel(DataFieldsRaw)
    	if ~iscell(eventStruct.(DataFieldsRaw{fC})(1))
    		DataFields{end+1}=DataFieldsRaw{fC};
    	end
    end
    
    %built GUI
    Title = 'Select two Fields';
	Prompt={'Data Field A:', 'FieldA';...
			'Data Field B:','FieldB'};
			
	%default values
	defval =struct(	'FieldA',1,...
					'FieldB',1);
    
	%Field A
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items= DataFields;
	Formats(1,2).type='none';
	Formats(1,1).size = [-1 0];
	Formats(1,2).limits = [0 1];
	
	%Field B
	Formats(2,1).type='list';
	Formats(2,1).style='popupmenu';
	Formats(2,1).items= DataFields;
	Formats(2,2).type='none';
	Formats(2,1).size = [-1 0];
	Formats(2,2).limits = [0 1];
		
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';	
	
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 
		
	if Canceled==1
		%nothing to do
		return
	end
		
	%prepare data
	FieldNameA= DataFields{NewParameter.FieldA};
	FieldNameB= DataFields{NewParameter.FieldB};
	PlotData(:,1)=eventStruct.(FieldNameA)(selected);
    PlotData(:,2)=eventStruct.(FieldNameB)(selected);

	%make a plot window
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    %configure plot			
    PlotConfig	= struct(	'PlotType','generic2D',...
							'Data',PlotData,...
							'Line_or_Point','points',...
							'X_Axis_Label', FieldNameA,...
							'Y_Axis_Label', FieldNameB,...
                            'LineStylePreset','Fatline',...
                            'MarkerStylePreset','cross',...
                            'CustomMarker',{{'x',6}},...
							'Colors','black',...
							'X_Axis_Limit','auto',...
							'Y_Axis_Limit','auto',...
							'LegendText','Mw versus Ml');
    
	%plot the data
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
	
	%return the handle for the plot, might be needed later.
    FuncOUT=plotAxis;

end
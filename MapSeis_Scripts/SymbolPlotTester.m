function FuncOUT = SymbolPlotTester(Commander)
	%This scripts plots a scatter plot with MW versus ML of the SeismoXML
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    
    FuncOUT=[];
  
    
    %open load dialog if no filename specified
    if nargin<2
	[fName,pName] = uigetfile('*.dat','Enter ASCII *.dat file name...');
    	 if isequal(fName,0) || isequal(pName,0)
        	 disp('No filename selected. ');
         else
		 filename=fullfile(pName,fName);
	 end
    else
	%filename is defined
	 [pName,fName,extStr,verStr] = fileparts(filename);
    end
	
    PlotData = load(filename);
    
    
    
    TheAxes=findobj('type','axes');
	AxesName=get(TheAxes,'Tag');
	
	%get the parent figures
	ThePars=get(TheAxes,'Parent');
	TheParents(:,1)=[ThePars{:}];
	NameofParent=get(TheParents,'Name');
	
	%check if any of the tags is empty
	NotTagged=emptier(AxesName);
	
	if any(NotTagged)
		count=1;
		lastfig=-1;
		for i=1:numel(TheAxes)
			if ~isempty(AxesName{i});
				NameList(i)=AxesName(i);
			else	
				if lastfig~=TheParents(i)
					count=1;
					lastfig=TheParents(i);
				else
					count=count+1;
				end
				
				NameList{i}=[NameofParent{i},' ',num2str(count)];
			end	
		end
	
	else
		NameList=AxesName;
	
	end
    
    %Build GUI
	Title = 'Select the Axes which should be used for the Plot';
	Prompt={'Plot Axis:', 'AxSelector'};
	
	%Mc Method
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=NameList;
	Formats(1,1).size = [-1 0];
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	%default values
	defval.AxSelector=1;
	
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
	
	AxSelector=NewParameter.AxSelector;
	
	if Canceled~=1
    
	%plotAxis=findobj(Commander.MainGUI.MainGUI,'Tag','axis_left');
	    plotAxis=TheAxes(AxSelector);
	    
	    xlimiter=get(plotAxis,'xlim');
	    ylimiter=get(plotAxis,'ylim');
	    
	    
	    %axes(plotAxis)
	    axes(plotAxis)
	    hold on			
	    
	    
	    %plot symbols
	    %PlotData(:,1)=eventStruct.Ml_from_moment;
	    %PlotData(:,2)=eventStruct.Ml_from_amp;
	    
	    
	    PlotConfig	= struct(	'PlotType','generic2D',...
					'Data',PlotData,...
					'Line_or_Point','points',...
					'X_Axis_Label','Longitude',...
					'Y_Axis_Label','Latitude ',...
					'LineStylePreset','Fatline',...
					'MarkerStylePreset','cross',...
					'CustomMarker',{{'^',6}},...
					'Colors','red',...
					'X_Axis_Limit',xlimiter,...
					'Y_Axis_Limit',ylimiter,...
					'LegendText',fName);
	    
				
	    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
	    
	    latlim = get(plotAxis,'Ylim');
		set(plotAxis,'dataaspect',[1 cos(pi/180*mean(latlim)) 1]);
		set(plotAxis,'DrawMode','fast');
	    axes(plotAxis)
	    hold off
	    
	    FuncOUT=plotAxis;

	end
	
end

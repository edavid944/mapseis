function FuncOUT = SILMagOrgMwDenseScript(Commander)
    %This scripts plots a density plot with MW versus ML of the SIL
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    import mapseis.converter.*;
     
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;
	
    %get the mags
    eventStruct=Datastore.getRawFields({'Ml_from_amp' 'Ml_from_moment'},selected);
    orgMw=IcelandMwToMw(eventStruct.Ml_from_moment,'Catalog2Map');
    
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    spacingPara=0.05
    
    minval=floor(min([min(orgMw) min(eventStruct.Ml_from_amp)]));
    maxval=ceil(max([max(orgMw) max(eventStruct.Ml_from_amp)]));
    
    xlimiter=[floor(min(orgMw)) ceil(max(orgMw))];
    ylimiter=[floor(min(eventStruct.Ml_from_amp)) ceil(max(eventStruct.Ml_from_amp))];
    
    RawData(:,1)=orgMw;
    RawData(:,2)=eventStruct.Ml_from_amp;
    
    %create a grid
    MwSpace=min(orgMw):0.1:max(orgMw);
    MlSpace=min(eventStruct.Ml_from_amp):0.1:max(eventStruct.Ml_from_amp);
    
    %go through the elements
    for i=1:numel(MwSpace)
    	for j=1:numel(MlSpace)
    		EventsFound=(orgMw>=(MwSpace(i)-spacingPara)&...
    			orgMw<(MwSpace(i)+spacingPara))&...
    			(eventStruct.Ml_from_amp>=(MlSpace(j)-spacingPara)&...
    			eventStruct.Ml_from_amp<(MlSpace(j)+spacingPara));
    		MwFound=(orgMw>=(MwSpace(i)-spacingPara)&...
    			orgMw<(MwSpace(i)+spacingPara));
    		if any(MwFound)&any(EventsFound)	
    			DenseMatrix(j,i)=sum(EventsFound)/sum(MwFound);
    		else
    			DenseMatrix(j,i)=0;
    		end	
    	
    	end
    end
    
    %norm the matrix
    %maxval=max(max(DenseMatrix));
    %DenseMatrix=DenseMatrix/maxval;
    
    AlphaData=double(~(DenseMatrix==0));
    
    PlotData={MwSpace,MlSpace,DenseMatrix};
    

    PlotConfig	= struct(	'PlotType','ColorPlot',...
				'Data',{PlotData},...
				'MapStyle','smooth',...
				'AlphaData',AlphaData,...
				'X_Axis_Label','org Mw',...
				'Y_Axis_Label','normed(Ml from amplitude) ',...
				'Colors','jet',...
				'X_Axis_Limit',xlimiter,...
				'Y_Axis_Limit',ylimiter,...
				'C_Axis_Limit','auto',...
				'ColorToggle',true,...
				'LegendText','Ml versus Mw');
    
                        
    [handle legendentry] = PlotColor(plotAxis,PlotConfig);       
    
    %set(handle,'LineStyle','none');
        
    %plot 1:1 line
    
    hold on
    clear PlotData;
    PlotData(1,:)=[minval minval];
    PlotData(2,:)=[maxval maxval];
    
     PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','line',...
				'X_Axis_Label','org Mw',...
				'Y_Axis_Label','normed(Ml from amplitude) ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit',xlimiter,...
				'Y_Axis_Limit',ylimiter,...
				'LegendText','Ml versus Mw');
    
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);
    
    
    hold off
    
    FuncOUT=plotAxis;

	
	
end

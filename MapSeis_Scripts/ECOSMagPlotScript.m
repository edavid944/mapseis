function FuncOUT = ECOSMagPlotScript(Commander)
    %This scripts plots a scatter plot with MW versus ML of the ECOS 09 catalog
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;
    
    %get the mags
    eventStruct=Datastore.getRawFields({'MWECOS09' 'MLECOS09'},selected)
    
	
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    PlotData(:,1)=eventStruct.MWECOS09;
    PlotData(:,2)=eventStruct.MLECOS09;
				
    PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Mw ',...
				'Y_Axis_Label','Ml ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',10}},...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Mw versus Ml');
    
                        
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
                        
    FuncOUT=plotAxis;

	
	
end

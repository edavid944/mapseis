function FuncOUT = SILMagToMwPlotScript(Commander)
    %This scripts compares the Iceland Moment Magnitude with
    %"original" one (Mw=2/3*log10(M0)-10.7) of the paper
    %"A moment magnitude scale" by Hanks, Thomac C.; Kanamori, Hiroo 1979
    %The Iceland Mag is converted back into the seismic Moment and then into
    %org Mw
    
    
    import mapseis.plot.*;
    
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    %create Mag in 0.1 steps
    Ice_Mw=(-10:0.1:8)';
    
    DyneFactor=1/(10^(-7));
    
    %Iceland constanst
    a = 2;
    b = 1/0.9;  
    c = 1.6/0.8;  
    d = 0.8/0.7;
    e = 1;
    f = 1;
    
    %select the ranges
    sel_low2=Ice_Mw<=2;
    sel_2to3=Ice_Mw>2&Ice_Mw<=3;
    sel_3to46=Ice_Mw>3&Ice_Mw<=4.6;
    sel_46to54=Ice_Mw>4.6&Ice_Mw<=5.4;
    sel_54to59=Ice_Mw>5.4&Ice_Mw<=5.9;
    sel_59to63=Ice_Mw>5.9&Ice_Mw<=6.3;
    sel_hi63=Ice_Mw>6.3;
    
    % m = log10(Mo) - 10  
    % M = m                               if          M <= 2.0
    % M = 2.0 + (m-a)*0.9                       2.0 < M <= 3.0
    %	m=(M-2.0)/0.9+a
    
    % M = 3.0 + (m-a-b)*0.8                     3.0 < M <= 4.6
    %	m=(M-3)/0.8+a+b
    
    % M = 4.6 + (m-a-b-c)*0.7                   4.6 < M <= 5.4
    %	m=(M-4.6)/0.7+a+b+c
    
    % M = 5.4 + (m-a-b-c-d)*0.5                 5.4 < M <= 5.9
    %	m=(M-5.4)/0.5+a+b+c+d		
    
    % M = 5.9 + (m-a-b-c-d-e)*0.4               5.9 < M >= 6.3
    %	m=(M-5.9)/0.4+a+b+c+d+e
    
    % M = 6.3 + (m-a-b-c-d-e-f)*0.35            6.3 < M
    %	m=(M-6.3)/0.35+a+b+c+d+e+f
    
   
    %      a = 2,  b = 1/0.9,  c = 1.6/0.8,  d = 0.8/0.7,  e = f = 1

    %create seismic moment (in dyne)
    M0=zeros(numel(Ice_Mw),1);
    small_m=M0;
    small_m(sel_low2)=Ice_Mw(sel_low2);
    small_m(sel_2to3)=(Ice_Mw(sel_2to3)-2)/0.9+a;
    small_m(sel_3to46)=(Ice_Mw(sel_3to46)-3)/0.8+a+b;
    small_m(sel_46to54)=(Ice_Mw(sel_46to54)-4.6)/0.7+a+b+c;
    small_m(sel_54to59)=(Ice_Mw(sel_54to59)-5.4)/0.5+a+b+c+d;
    small_m(sel_59to63)=(Ice_Mw(sel_59to63)-5.9)/0.4+a+b+c+d+e;
    small_m(sel_hi63)=(Ice_Mw(sel_hi63)-6.3)/0.35+a+b+c+d+e+f;
    
    M0=(10.^(small_m+10))*DyneFactor;
   	
    orgMw=(2/3)*log10(M0)-10.7;
    
    
    Mw_diff=Ice_Mw-orgMw;
    
    PlotData(1,:)=[-10 0];
    PlotData(2,:)=[8 0];
    
     PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','line',...
				'X_Axis_Label','Ml ',...
				'Y_Axis_Label','Ml - Mw ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Ml versus Ml');
    
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);
    
    hold on			
    
    clear PlotData;
    %plot scatter
    PlotData(:,1)=Ice_Mw;
    PlotData(:,2)=Ice_Mw-orgMw;
    
    
    PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Mw Iceland ',...
				'Y_Axis_Label','Mw Iceland - org Mw ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',6}},...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','IceLand Mw vs org Mw');
    
                        
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
    
    
    hold off
    
    FuncOUT=plotAxis;
    
    %normal plots without difference
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    clear PlotData;
    PlotData(1,:)=[-10 -10];
    PlotData(2,:)=[8 8];
    
     PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','line',...
				'X_Axis_Label','Iceland Mw ',...
				'Y_Axis_Label','org Mw ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Ml versus Ml');
    
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);
    
    
     hold on			
    
    clear PlotData;
    %plot scatter
    PlotData(:,1)=Ice_Mw;
    PlotData(:,2)=orgMw;
    
    
    PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Mw Iceland ',...
				'Y_Axis_Label','org Mw ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',6}},...
				'Colors','blue',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Iceland Mw vs org Mw');
    
                        
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
    
    
    hold off
    
	
end

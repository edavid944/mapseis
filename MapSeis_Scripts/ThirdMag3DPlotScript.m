function FuncOUT = ThirdMag3DPlotScript(Commander)
   %This script plots the the location as points with size giving there
   %magnitude and color giving any parameter from the catalog wanted
   %This function maybe added later into a new plot plugin System.
   
   %not finished, the script uses at the moment time as fourth parameter
   %will later use a input screen
   
   %Same as the normal version but 3D
   
   %It uses the ThirdSpaceCarrier gui but it could be done otherwise. It is also
   %meant a bit as an experiment and test.
   
   
    import mapseis.plot.*;
    import mapseis.gui.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;
	
    %get the mags
    eventStruct=Datastore.getFields({'lat' 'lon' 'mag' 'depth'},selected);
    xrange=[min(eventStruct.lon)-0.1, max(eventStruct.lon)+0.1]
    yrange=[min(eventStruct.lat)-0.1, max(eventStruct.lat)+0.1]
    
    dectime = Datastore.getUserData('DecYear');
    
    RawStruct=Datastore.getRawFields;
    RawStruct.DecYear=dectime;
    
    TheFields=fieldnames(RawStruct);
    %TheFields{end+1}='DecYear';
    
    %Build GUI
    Title = 'Select the additional parameter to plot';
    Prompt={'Additional Parameter:', 'ParSelect'};
		
    %Mc Method
    Formats(1,1).type='list';
    Formats(1,1).style='popupmenu';
    Formats(1,1).items=TheFields;
    Formats(1,1).size = [-1 0];
		
    %%%% SETTING DIALOG OPTIONS
    Options.WindowStyle = 'modal';
    Options.Resize = 'on';
    Options.Interpreter = 'tex';
    Options.ApplyButton = 'off';

    %default values
    defval.ParSelect=1;

    %open the dialog window
    [NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
		
		
    ParSelect=NewParameter.ParSelect;
    
    
    
    if Canceled~=1
    	    
            %build gui
    	    FuncOUT = ThirdSpaceCarrier(Commander.ListProxy,Commander,Commander.MainGUI,'light');
    
            %get additional field
	    AddOnField=TheFields{ParSelect};
	    
	    %set it as spareparameter in ThirdSpaceCarrier gui.
	    FuncOUT.SpareParameter=AddOnField;
	    
	    TheCustomHandle=@CustomUpdater;
	    
	    %Now set the GUI so it can be used
	    FuncOUT.SetUpdateFunction(TheCustomHandle);
	    FuncOUT.BuildResultGUI;
	    FuncOUT.updateGUI;
    end
	
end


function CustomUpdater(TheGUI)
	    import mapseis.plot.*;
	    import mapseis.projector.*;
	    
	    %get Data
	    Datastore=TheGUI.Datastore;	    
	    selected=TheGUI.SendedEvents;
	    [xlimiter ylimiter zlimiter Selected UnSelected] = TheGUI.ReturnLimits;
	    dectime = Datastore.getUserData('DecYear');
	    
	    RawStruct=Datastore.getRawFields;
	    RawStruct.DecYear=dectime;
	    
	    %get addonddata
	    AddOnField=RawStruct.(TheGUI.SpareParameter);
	   
	    
	    %Now plot da shit
	    plotAxis=getPlotAxis(TheGUI);
	    
	    %too be sure
	    TheGUI.PlotMode=light;
	    
	    %clean plot
	    try
	    	plot3(plotAxis,mean(xlimiter),mean(ylimiter),0,'Color','w');
	    	
	    catch
	    	disp('There is a problem with the limiters')
	    	plot3(plotAxis,0,0,0,'Color','w');
	    	
	    end
	    
	    
	    hold on
	    TheGUI.PlotLandMasses;
	    
	    %Now plot the earthquakes (only selected mode allowed at the moment)
	    %now the data
	    EqConfigSel	= struct(	'PlotType','Earthquakes',...
					'Data',Datastore,...
					'PlotMode','new',...
					'SelectionSwitch','selected',...
					'SelectedEvents',selected,...
					'AdditionalData',AddOnField,...
					'AddDataName','TheGUI.SpareParameter',...
					'MarkedEQ','max',...
					'X_Axis_Label','Longitude ',...
					'Y_Axis_Label','Latitude ',...
					'MarkerSize','normal',...
					'MarkerStylePreset','normal',...
					'CustomMarkerRange',[0 8 9],...
					'Colors','jet',...
					'X_Axis_Limit','auto',...
					'Y_Axis_Limit','auto',...
					'Z_Axis_Limit','auto',...
					'C_Axis_Limit','auto',...
					'ColorToggle',true,...
					'DepthReverse',true,...
					'FastMode',false,...
					'SizeLegend',true,...
					'LegendText','Earthquakes');
	    [handle2 entry2] = PlotEarthquake3D(plotAxis,EqConfigSel);
	    set(handle2,'MarkerEdgeColor','none');
	    hold on
	    TheGUI.plotCoastBorder;
	    hold on
	    %TheGUI.plotShadow;
	    
	    %custom shadows (scatter)
	    if TheGUI.ShadowToogle
	    		[selectedLonLat,unselectedLonLat] = getLocations(Datastore,selected);
			[selectedDepth,unselectedDepth] = getDepths(Datastore,selected);
			[selectedMag,unselectedMag] = getMagnitudes(Datastore,selected);
			
			maxDepth=max(selectedDepth)+1;
			NrEvents=numel(selectedDepth);
			NrNot=numel(unselectedDepth);
			
			seldata=[selectedLonLat(:,1),selectedLonLat(:,2),ones(NrEvents,1)*maxDepth,selectedMag];
			
			ShadConfig= struct(	'PlotType','Earthquakes',...
						'Data',seldata,...
						'PlotMode','new',...
						'SelectionSwitch','unselected',...
						'SelectedEvents',logical(zeros(NrEvents,1)),...
						'MarkedEQ','max',...
						'X_Axis_Label','Longitude ',...
						'Y_Axis_Label','Latitude ',...
						'MarkerSize','normal',...
						'MarkerStylePreset','normal',...
						'CustomMarkerRange',[0 8 9],...
						'Colors','black',...
						'X_Axis_Limit','auto',...
						'Y_Axis_Limit','auto',...
						'Z_Axis_Limit','auto',...
						'C_Axis_Limit','auto',...
						'DepthReverse',true,...
						'FastMode',false,...
						'LegendText','Earthquakes');
						
			[shadhand entry2] = PlotEarthquake3D(plotAxis,ShadConfig);
			 set(shadhand,'MarkerEdgeColor','none');
						
	    end
	    
	    
	    hold on
	    TheGUI.FinalizePlot;
	    hold off
	    
	    
	
	
	
end


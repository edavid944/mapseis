function FuncOUT = newMcTestScript_RefGrid(Commander)
	%This small script allows to test Arnauds new Mc determination method. 
	%At the moment it is only a script but it will be transfered into a 
	%modul with full functionality later. It is at the moment a bit 
	%unclear how to handle certain input of parameter and the calculation
	%in the finally module (classic, interactiv or more than one step 
	%selectable?)
	
	import mapseis.util.emptier;
	import mapseis.region.*;
	import mapseis.plot.*;
	  
	FuncOUT=[];
	
	%check for existing McMaps, let the user select one.
	
	%get DataStore
	Datastore = Commander.getCurrentDatastore;
	
		
	%try get the stationlist
	try
		StationData=Datastore.getUserData('StationList');
	catch
		StationData=[];
	end
		
		
		
		
	if ~isempty(StationData)
		
		 %open load dialog if no filename specified
		
		[fName,pName] = uigetfile('*.mat','Enter MapSeis *.mat file name...');
          		if isequal(fName,0) || isequal(pName,0)
          		  	disp('No filename selected. ');
           		else
				filename=fullfile(pName,fName);
			end	     
								
			%load catalog
			prevData = load(filename);
			
			AllOk=true
			try 
				RefGrid=prevData.RefGrid;
				McMap=prevData.Mc;
				Selected=prevData.selected;
			catch
				AllOk=false;
				errordlg('The file did not contain the necessary data');
			end	
		
		if AllOk
			%get filterlist
			
			%get years 
			TheDates=Datastore.getFields('dateNum');
			TheDates=TheDates.dateNum;
			
			minyear=floor(min(TheDates(Selected)));
			maxyear=ceil(max(TheDates(Selected)));
			
			if strcmp(StationData.ListMode,'timevar')
				%get stations for the time
				%find the not set entries
				NotSetStart=isnan(StationData.starttime);
				NotSetEnd=isnan(StationData.endtime);
			
				%lower bound & upper bound
				LowTime=StationData.starttime<=minyear|NotSetStart;
				HightTime=StationData.endtime>=maxyear|NotSetEnd;
			
				InTime=LowTime&HightTime;
			else
				InTime=logical(ones(numel(StationData.lat),1));
				
				
			end	
			
			
			%stationlist
			stationlist=[StationData.lon(InTime),StationData.lat(InTime)];
			
			
			
			totalCells=numel(McMap);
			
						
			McDataArray=[RefGrid(:,1),RefGrid(:,2),McMap];
			
			%go and process the data
			for i=1:totalCells
				if ~isnan(McDataArray(i,3))
					TheDistances=distance(McDataArray(i,1:2),stationlist);
					
					if numel(TheDistances)>=5
						distSelect=5;
					else
						distSelect=numel(TheDistances);
					end
					
					McDist(i,:)=[TheDistances(distSelect),McDataArray(i,3)];
				end
				
			end
			
			FuncOUT=McDist;
			
			%plot the whole stuff
			PlotData=[deg2km(McDist(:,1)),McDist(:,2)];
			
			figure
			plotAxis=gca;
			
			PlotConfig= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Distance [km] ',...
				'Y_Axis_Label','Mc ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',6}},...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Mc with distance');
				
			[handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);   
		
		end
	
	else
		errordlg('No Mc map or Stationlist included in the datastore');	
		return
	end
		
		
end

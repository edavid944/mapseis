function FuncOUT = MarkPolyRegion(Commander)
	%This function allows to select a plot and mark a region with a polygon
	%the coordinates will be saved in FuncOUT and polygon routed to the 
	%regionfilter of the current filterlist
	
	import mapseis.util.emptier;
	import mapseis.region.*;
	
	FuncOUT=[];
	
	%get all axes
	TheAxes=findobj('type','axes');
	AxesName=get(TheAxes,'Tag');
	
	%get the parent figures
	ThePars=get(TheAxes,'Parent');
	TheParents(:,1)=[ThePars{:}];
	NameofParent=get(TheParents,'Name');
	
	%check if any of the tags is empty
	NotTagged=emptier(AxesName);
	
	if any(NotTagged)
		count=1;
		lastfig=-1;
		for i=1:numel(TheAxes)
			if ~isempty(AxesName{i});
				NameList(i)=AxesName(i);
			else	
				if lastfig~=TheParents(i)
					count=1;
					lastfig=TheParents(i);
				else
					count=count+1;
				end
				
				NameList{i}=[NameofParent{i},' ',num2str(count)];
			end	
		end
	
	else
		NameList=AxesName;
	
	end
	
	
	%Build GUI
	Title = 'Select the Axes which should be used for the Polygon';
	Prompt={'Plot Axis:', 'AxSelector'};
	
	%Mc Method
	Formats(1,1).type='list';
	Formats(1,1).style='popupmenu';
	Formats(1,1).items=NameList;
	Formats(1,1).size = [-1 0];
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	%default values
	defval.AxSelector=1;
	
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
	
	AxSelector=NewParameter.AxSelector;
	
	if Canceled~=1
		
		%Set it to the Filter
		newpoly=impoly(TheAxes(AxSelector));
		newRegion = Region(newpoly.getPosition);
		TheFilterList=Commander.getCurrentFilterlist;
		regionFilter=TheFilterList.getByName('Region');
		regionFilter.setRegion('in',newRegion);
		FuncOUT=newpoly.getPosition;
	
	end
	
	
end

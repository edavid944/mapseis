function FuncOUT = SILMagDiffPlotScript(Commander)
	%This scripts plots a scatter plot with MW versus ML of the SeismoXML
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
	Filterlist = Commander.getCurrentFilterlist; 
	selected=Filterlist.getSelected;
    
    %get the mags
    eventStruct=Datastore.getRawFields({'Ml_from_amp' 'Ml_from_moment'},selected)
    
     
    Ml=eventStruct.Ml_from_amp;
    Ml_diff_Mw=eventStruct.Ml_from_moment-eventStruct.Ml_from_amp;
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
        
    %plot 1:1 line
    minval=floor(min([min(Ml) min(Ml_diff_Mw)]));
    maxval=ceil(max([max(Ml) max(Ml_diff_Mw)]));
    
    
    PlotData(1,:)=[minval 0];
    PlotData(2,:)=[maxval 0];
    
     PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','line',...
				'X_Axis_Label','Ml ',...
				'Y_Axis_Label','Ml - Mw ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Ml versus Ml');
    
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);
    
    hold on			
    
    clear PlotData;
    %plot scatter
    PlotData(:,1)=Ml;
    PlotData(:,2)=Ml_diff_Mw;
    
    
    PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','points',...
				'X_Axis_Label','Ml ',...
				'Y_Axis_Label','Ml - Mw ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','cross',...
				'CustomMarker',{{'x',6}},...
				'Colors','red',...
				'X_Axis_Limit','auto',...
				'Y_Axis_Limit','auto',...
				'LegendText','Ml versus Ml');
    
                        
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
    
    
    hold off
    
    FuncOUT=plotAxis;

	
	
end

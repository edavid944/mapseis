function FuncOUT = SILMagDiffDensePlotScript(Commander)
    %This scripts plots a density plot with MW versus ML of the SIL
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;
	
    %get the mags
    eventStruct=Datastore.getRawFields({'Ml_from_amp' 'Ml_from_moment'},selected);
    
    Ml=eventStruct.Ml_from_amp;
    Ml_diff_Mw=eventStruct.Ml_from_moment-eventStruct.Ml_from_amp;
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    spacingPara=0.05
    
    minval=floor(min([min(Ml) min(Ml_diff_Mw)]));
    maxval=ceil(max([max(Ml) max(Ml_diff_Mw)]));
    
    xlimiter=[floor(min(Ml)) ceil(max(Ml))];
    ylimiter=[floor(min(Ml_diff_Mw)) ceil(max(Ml_diff_Mw))];
    
    %RawData(:,1)=eventStruct.Ml_from_moment;
    %RawData(:,2)=eventStruct.Ml_from_amp;
    
    %create a grid
    MlSpace=min(Ml):0.1:max(Ml);
    MldiffSpace=min(Ml_diff_Mw):0.1:max(Ml_diff_Mw);
    
    %go through the elements
    for i=1:numel(MlSpace)
    	for j=1:numel(MldiffSpace)
    		EventsFound=(Ml>=(MlSpace(i)-spacingPara)&...
    			Ml<(MlSpace(i)+spacingPara))&...
    			(Ml_diff_Mw>=(MldiffSpace(j)-spacingPara)&...
    			Ml_diff_Mw<(MldiffSpace(j)+spacingPara));
    		
    		%total in Ml (for the norm)
    		MlFound=(Ml>=(MlSpace(i)-spacingPara)&...
    			Ml<(MlSpace(i)+spacingPara));
    		
    		if any(MlFound)&any(EventsFound)	
    			DenseMatrix(j,i)=sum(EventsFound)/sum(MlFound);
    		
    		else
    			DenseMatrix(j,i)=0;
    		end	
    	end
    end
    
    %norm the matrix
    %maxval=max(max(DenseMatrix));
    %DenseMatrix=DenseMatrix/maxval;
    
    AlphaData=double(~(DenseMatrix==0));
    
    PlotData={MlSpace,MldiffSpace,DenseMatrix};
    

    PlotConfig	= struct(	'PlotType','ColorPlot',...
				'Data',{PlotData},...
				'MapStyle','smooth',...
				'AlphaData',AlphaData,...
				'X_Axis_Label','Ml ',...
				'Y_Axis_Label','normed(Ml - Mw) ',...
				'Colors','jet',...
				'X_Axis_Limit',xlimiter,...
				'Y_Axis_Limit',ylimiter,...
				'C_Axis_Limit','auto',...
				'ColorToggle',true,...
				'LegendText','Ml versus Ml');
    
                        
    [handle legendentry] = PlotColor(plotAxis,PlotConfig);       
    
    %set(handle,'LineStyle','none');
        
    %plot 1:1 line
    
    hold on
    clear PlotData;
    PlotData(1,:)=[minval 0];
    PlotData(2,:)=[maxval 0];
    
     PlotConfig	= struct(	'PlotType','generic2D',...
				'Data',PlotData,...
				'Line_or_Point','line',...
				'X_Axis_Label','Ml ',...
				'Y_Axis_Label','normed(Ml - Mw) ',...
				'LineStylePreset','Fatline',...
				'MarkerStylePreset','none',...
				'Colors','black',...
				'X_Axis_Limit',xlimiter,...
				'Y_Axis_Limit',ylimiter,...
				'LegendText','Ml versus Ml');
    
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);
    
    
    hold off
    
    FuncOUT=plotAxis;

	
	
end

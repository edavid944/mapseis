function FuncOUT = ForecastTestScript(Commander)
   %This script tests the with ETASDemoScript.m produce forecast with a L-test.
   %Also it shows the forecast as colormap
   
   
    import mapseis.ETAS.*;
    import mapseis.forecast.*;
    import mapseis.forecast_testing.*;
    import mapseis.forecast_testing.TestPlugIns.*;
    import mapseis.forecast_testing.PlotPlugIns.*;
    import mapseis.forecast_testing.QuickPlots.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
    Filterlist = Commander.getCurrentFilterlist; 
    selected=Filterlist.getSelected;

    %stored project
    StoreProject = './Forecast_Results/Forecast_ETASDemo.mat';
    
    %load project
    Temp=load(StoreProject);
    ForecastProject=Temp.ForecastProject;
    
    %get the results
    EtasCast=ForecastProject.getForecast(1);
    
    %now the testing
    Tester=TesterObject('ETASDemo');
    Tester.addProject(ForecastProject);
    Tester.JumpTime=false;
    
    %add some Tests
    Ltest = LTestPlugin('');
    Ntest = NTestPlugin('');
    Stest = STestPlugin('');
    Mtest = MTestPlugin('');
    CLtest = CLTestPlugin('');
    
    Tester.addTestPluging(Ltest);
    Tester.addTestPluging(Ntest);
    Tester.addTestPluging(Stest);
    Tester.addTestPluging(Mtest);
    Tester.addTestPluging(CLtest);
    
    %check what can be tested
    Tester.generateModTime;
    
    %choose everything
    Tester.setTestSelection('all','all');
    
    %finalize
    Tester.generateWorkList;
    
    save('ZZZTemp.mat','Tester');
    
    %Test it
    Tester.startCalc;
    
    %get results of the test
    Tester.buildTestResultObj;
    ResObj=Tester.getTestObject;
    
    %make a summary of the result 
    TestTypes={'Likelihood','Likelihood','Likelihood','Likelihood','Likelihood'}';
    TestSummarizer(ResObj,TestTypes);
    
    %plot test results
    TestNames = {'Ltest_v1__','Ntest_v1__','Stest_v1__','Mtest_v1__','CLtest_v1__'}';
    Models={'ETAS_matlab_vproto_v1_v1'};
    TheTitle='ETAS Demo Forecast: Iceland';
    PlotModName={'ETAS'};
    plotSummaryScore(ResObj,TestNames,Models,TheTitle,PlotModName);
    
    %show results
    Slicer=SliceViewer_mk3('ShowTime');
    Slicer.TypicalStart(EtasCast,Datastore,Filterlist);
    
    msgbox('Testing Finished')
    
    FuncOUT=0;
    
end

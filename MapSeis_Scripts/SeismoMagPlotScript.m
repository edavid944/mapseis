function FuncOUT = SeismoMagPlotScript(Commander)
	%This scripts plots a scatter plot with MW versus ML of the SeismoXML
    %Catalog, it only works with that catalog
	
    import mapseis.plot.*;
    
    %get current Catalog & Filter
    Datastore = Commander.getCurrentDatastore;
	Filterlist = Commander.getCurrentFilterlist; 
	selected=Filterlist.getSelected;
    
    %get the mags
    eventStruct=Datastore.getRawFields({'MW' 'ML'},selected)
    
	
    theplot=figure;
    plotAxis=subplot(1,1,1);
    
    PlotData(:,1)=eventStruct.MW;
    PlotData(:,2)=eventStruct.ML;
				
    PlotConfig	= struct(	'PlotType','generic2D',...
							'Data',PlotData,...
							'Line_or_Point','points',...
							'X_Axis_Label','Mw ',...
							'Y_Axis_Label','Ml ',...
                            'LineStylePreset','Fatline',...
                            'MarkerStylePreset','cross',...
                            'CustomMarker',{{'x',6}},...
							'Colors','black',...
							'X_Axis_Limit','auto',...
							'Y_Axis_Limit','auto',...
							'LegendText','Mw versus Ml');
    
                        
    [handle legendentry] = PlotGeneric2D(plotAxis,PlotConfig);               
                        
    FuncOUT=plotAxis;

	
	
end

function FuncOUT = SeismicReplay(Commander)
	%great a movie which plays all earthquakes in chronologic order.
	
	import mapseis.util.emptier;
	import mapseis.region.*;
	import mapseis.export.Export2Zmap;
	
	FuncOUT=[];
	
	Datastore = Commander.getCurrentDatastore;
	Filterlist = Commander.getCurrentFilterlist; 
	selected=Filterlist.getSelected;
	[Coast_PlotArgs, InternalBorder_PlotArgs] = GetOverlay(Datastore);
	
	
	%use a zmap catalog for now, if other parameters are needed
	%get them later
	ZmapCatalog = Export2Zmap(obj.Datastore,selected);
	
	%get the parameters for the animate
	%Build GUI
	Title = 'Enter Animation Parameters';...
	Prompt={'Animation Style', 'AniStyle';...
		'Earthquake per second (fixed rate)','Eq_rate';...
		'Year Length in seconds (fixed year)','year_length';...
		'Decay Rate','decay_rate',...
		'Frame per Second','framerate'};
	
	%Style
	Formats(1,1).type='list';
	Formats(1,1).style='togglebutton';
	Formats(1,1).items={'fixed rate','fixed year'};

	Formats(2,1).type='edit';
	Formats(2,1).format='integer';
	Formats(2,1).limits = [0 9999];
	
	Formats(3,1).type='edit';
	Formats(3,1).format='integer';
	Formats(3,1).limits = [0 9999];
	
	Formats(4,1).type='edit';
	Formats(4,1).format='integer';
	Formats(4,1).limits = [0 Inf];
	
	Formats(5,1).type='edit';
	Formats(5,1).format='integer';
	Formats(5,1).limits = [0 999];
	
	%%%% SETTING DIALOG OPTIONS
	Options.WindowStyle = 'modal';
	Options.Resize = 'on';
	Options.Interpreter = 'tex';
	Options.ApplyButton = 'off';
	
	%default values
	defval=struct(	'AniStyle',1,...
			'Eq_rate',10,...
			'year_length',30,...
			'decay_rate',48,...
			'framerate',24);
	
	%open the dialog window
	[NewParameter,Canceled] = inputsdlg(Prompt,Title,Formats,defval,Options); 	
		
	
	AniStyle=NewParameter.AniStyle;
	Eq_rate=NewParameter.Eq_rate;
	year_length=NewParameter.year_length;
	decay_rate=NewParameter.decay_rate;
	framerate=NewParameter.framerate;
	
	if Canceled~=1
		moviescreen=figure;
		plotAxis=subplot(1,1,1);
		
		%create vector with decay and rate
		stateArray=ones(numel(ZmapCatalog(:,1)),1)*(-1);
		
		%create overlay setting
		coastplot=struct(	'Data',[Coast_PlotArgs{1},Coast_PlotArgs{2}],...
					'X_Axis_Label','Longitude',...
					'Y_Axis_Label','Latitude',...
					'LineStylePreset','normal',...
					'Colors','blue');
					
		borderplot=struct(	'Data',[InternalBorder_PlotArgs{1},InternalBorder_PlotArgs{2}],...
					'X_Axis_Label','Longitude',...
					'Y_Axis_Label','Latitude',...
					'LineStylePreset','dotted',...
					'Colors','black');
					
		
		%the rawsettings of earthquake plots
		
		%new events
		newEq= struct(	'Data',[ZmapCatalog(:,1) ZmapCatalog(:,2) ZmapCatalog(:,6)],...
				'PlotMode','new',...
				'SelectionSwitch','selected',...
				'SelectedEvents',,...
				'MarkedEQ',,...
				'X_Axis_Label','Longitude',...
				'Y_Axis_Label','Latitude',...
				'MarkerSize','large',...
				'Colors',,...
				'X_Axis_Limit',,...
				'Y_Axis_Limit',,...
				'C_Axis_Limit',,...
				'ColorToggle',,...
				'LegendEntry',,...
				'SizeLegend',);
				
		recentEq= struct('Data',[ZmapCatalog(:,1) ZmapCatalog(:,2) ZmapCatalog(:,6)],...
				'PlotMode','new',...
				'SelectionSwitch','selected',...
				'SelectedEvents',,...
				'MarkedEQ',,...
				'X_Axis_Label','Longitude',...
				'Y_Axis_Label','Latitude',...
				'MarkerSize','normal',...
				'Colors',,...
				'X_Axis_Limit',,...
				'Y_Axis_Limit',,...
				'C_Axis_Limit',,...
				'ColorToggle',,...
				'LegendEntry',,...
				'SizeLegend',);		
				
				
		oldEq= struct(	'Data',[ZmapCatalog(:,1) ZmapCatalog(:,2) ZmapCatalog(:,6)],...
				'PlotMode','new',...
				'SelectionSwitch','unselected',...
				'SelectedEvents',,...
				'MarkedEQ',,...
				'X_Axis_Label','Longitude',...
				'Y_Axis_Label','Latitude',...
				'MarkerSize','small',...
				'Colors',,...
				'X_Axis_Limit',,...
				'Y_Axis_Limit',,...
				'C_Axis_Limit',,...
				'ColorToggle',,...
				'LegendEntry',,...
				'SizeLegend',);		
		
				
		%to be continued....

		
		if AniStyle==1
		
		
		
		
		elseif AniStyle==2
		
		
		
		
		end
		
	
	end
	
	
end

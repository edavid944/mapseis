function [guistuff listproxy]=runme
% runme : start the MapSeis application

% Add the present working directory to get the +mapseis package
addpath(pwd);
% Add the Import directory for testing
addpath(fullfile(pwd,'Import',''));
addpath(fullfile(pwd,'AddOneFiles','gshhs',''));
addpath(fullfile(pwd,'AddOneFiles','Internal_Border',''));
addpath(fullfile(pwd,'AddOneFiles','xmltree',''));
addpath(fullfile(pwd,'AddOneFiles','inputsdlg_v1.12',''));
addpath(fullfile(pwd,'AddOneFiles','DateStr2Num',''));
addpath(fullfile(pwd,'AddOneFiles','DateConvert',''));
addpath(fullfile(pwd,'AddOneFiles','freezeColors',''));
warning off


%check if DateStr2Num is compiled
%try
%        XTree=xmltree(inStr);
%catch
        	%maybe the tool was not compiled, so compile it
%        	oldpath=pwd
%        	cd('./AddOneFiles/xmltree/@xmltree/private');
%        	mex -O xml_findstr.c
%        	cd(oldpath);
        	%try again
%        	XTree=xmltree(inStr);
% end	
% Start the application

%Important:::
%==================================
%the DateStr2Num command might need complilation 
%(it works without but might be slow)
%use   
%   Windows: mex -O DateStr2Num.c
%   Linux:   mex -O CFLAGS="\$CFLAGS -std=C99" DateStr2Num.c
%or some *nux systems: mex -O CFLAGS="\$CFLAGS -std=c99" DateStr2Num.c
%automatic complition migth be featured later

%and for DateConvert
%-> not needed
%==================================



[guistuff listproxy]=mapseis.startApp;
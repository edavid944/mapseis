function CalcRes=MagMax(eventStruct)
	%A test function for the CustomCalc option in the until modul
	%It simply sums up all magnitudes and energy (iceland) in the send catalog
	%used in SIL
	
	CalcRes.StdOut=NaN;
	%CalcRes.MagnitudeMax=NaN;
	%CalcRes.MagnitudeMin=NaN;
	
	if ~isempty(eventStruct)
	
		TheMax=nanmax(eventStruct.mag);
		try
			CalcRes.MagnitudeMax=TheMax(1);
		catch
			CalcRes.MagnitudeMax=NaN;
		end
		
		TheMin=nanmin(eventStruct.mag);
		try
			CalcRes.MagnitudeMin=TheMin(1);
		catch
			CalcRes.MagnitudeMin=NaN;
		end	
	end
	
	
end

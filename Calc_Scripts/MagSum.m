function CalcRes=MagSum(eventStruct)
	%A test function for the CustomCalc option in the until modul
	%It simply sums up all magnitudes and energy (iceland) in the send catalog
	%used in SIL
	
	CalcRes.MagSum=NaN;
	
	
	
	CalcRes.MagSum=nansum(eventStruct.mag);
	MagIsMw=false;
	MagIsMl=false;
	
	try
		CalcRes.MlSum=nansum(eventStruct.Ml_from_amp);
		MagIsMw=true;
	end
	
	try
		CalcRes.MwSum=nansum(eventStruct.Ml_from_moment);
		MagIsMl=true;
	end
	
	
	if MagIsMw|MagIsMl
		%calc energy
		if MagIsMw
			Ice_Mw=eventStruct.mag;
		else
			Ice_Mw=eventStruct.Ml_from_moment;
		end
		
		DyneFactor=1/(10^(-7));
		
		%Iceland constanst
		a = 2;
		b = 1/0.9;  
		c = 1.6/0.8;  
		d = 0.8/0.7;
		e = 1;
		f = 1;
			 
		%select the ranges
		sel_low2=Ice_Mw<=2;
		sel_2to3=Ice_Mw>2&Ice_Mw<=3;
		sel_3to46=Ice_Mw>3&Ice_Mw<=4.6;
		sel_46to54=Ice_Mw>4.6&Ice_Mw<=5.4;
		sel_54to59=Ice_Mw>5.4&Ice_Mw<=5.9;
		sel_59to63=Ice_Mw>5.9&Ice_Mw<=6.3;
		sel_hi63=Ice_Mw>6.3;
		
		M0=zeros(numel(Ice_Mw),1);
		small_m=M0;
		small_m(sel_low2)=Ice_Mw(sel_low2);
		small_m(sel_2to3)=(Ice_Mw(sel_2to3)-2)/0.9+a;
		small_m(sel_3to46)=(Ice_Mw(sel_3to46)-3)/0.8+a+b;
		small_m(sel_46to54)=(Ice_Mw(sel_46to54)-4.6)/0.7+a+b+c;
		small_m(sel_54to59)=(Ice_Mw(sel_54to59)-5.4)/0.5+a+b+c+d;
		small_m(sel_59to63)=(Ice_Mw(sel_59to63)-5.9)/0.4+a+b+c+d+e;
		small_m(sel_hi63)=(Ice_Mw(sel_hi63)-6.3)/0.35+a+b+c+d+e+f;
		
		M0=(10.^(small_m+10))*DyneFactor;
		
		CalcRes.EnergySum=nansum(M0);
		try
			CalcRes.EnergyEq=CalcRes.EnergySum/numel(eventStruct.mag);
		end
		
	end
	
end
